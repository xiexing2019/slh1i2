"use strict";
//resultObj为返回的当前库存数组
var str = prev.getResponseDataAsString();
var resultObj = JSON.parse(str);

var data = resultObj.dataList; //明细数组
var sumrow = resultObj.sumrow; //汇总值

var keys = []; //未验证键名
//汇总值验证
var sumObj = addObject(format(data));

var ret = isApproximatelyObject(sumObj, sumrow);

//数据验证
log.info("\n未验证键名有: " + JSON.stringify(keys));

AssertionResult.setFailure(!ret);

function addObject(arr) {
	var jo1 = arr[0];
	for(var j = 1; j < arr.length; j++) {
		var jo2 = arr[j];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (Number(value) + Number(jo2[i])).toString(); //.toFixed(3) 小数保留3位
				
			}
		}
	}
	return jo1;
};
//收支列表，支出数据需要取反
function format(arr) {
  for(var i = 0; i < arr.length; i++) {
    if(arr[i].inouttype == 2) {
		arr[i].totalmoney = -arr[i].totalmoney;
	}
  }
  return arr
};
//比较两个对象之间是否相等(非严格意义上相等)
//返回是否相等，并将具有相同键且相同键的值相同 的 键存放于数组中返回
function isApproximatelyObject(expecteded, actual) {
	var ret = true;
	var ignore = ['modelClass', 'id', 'alipay_status', 'weixinpay_status', 'flag'];
	for(var i in expecteded) {
		if(!actual[i] || ignore.indexOf(i) != -1) {
			keys.push(i);
			continue;
		}

		if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = actual[i].indexOf(expecteded[i]) != -1;
		} else {
			ret = expecteded[i] == actual[i];
		}

		if(!ret) {
			AssertionResult.setFailureMessage("\nexpecteded = " + JSON.stringify(expecteded) + "\n actual = " + JSON.stringify(actual) + "\nkey = " + i + "  expecteded=" + expecteded[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
};
