"use strict";

/*
 * 组装开单(sf接口)的jsonparam
 * 有一个参数 args[0] 为“balance”的时候表示结余，不传表示找零
 */

//获取当前接口
// var urlStr = prev.getUrlAsString();
// log.info('---------------------------\n' + urlStr);
// var interfaceid = urlStr.split("interfaceid=")[1];

(function jsonparamFormat() {
	//获取数据
	var json = vars.getObject('jsonparam');
	var loginObj = vars.getObject('loginObj');

	//通用部分
	//支付
	if (json.cash && !json.cashaccountid) json.cashaccountid = vars.get('cashaccountid_cqd');
	if (json.card && !json.cardaccountid) json.cardaccountid = vars.get('cardaccountid_cqd');
	if (json.remit && !json.remitaccountid) json.remitaccountid = vars.get('agencyaccountid_cqd');
	//唯一码hashkey
	var ms = new Date().getMilliseconds();
	json.hashkey = vars.get("deviceno") + "-" + vars.get("currentTime") + ":" + ms + Math.random().toFixed(3);
	//基本信息
	if (!json.action) json.action = 'add';
	if (!json.invid) json.invid = loginObj.invid;
	if (!json.invalidflag) json.invalidflag = "0"; //状态 0正常 1作废 9挂单
	if (!json.prodate) json.prodate = vars.get('today'); //发生日期
	if (json.action == 'edit' && !json.pk) json.pk = vars.get('pk') || vars.getObject('billInfo').pk;
	var totalsum = 0,
		totalnum = 0;
	for (var i = 0; i < json.details.length; i++) {
		//有整单折扣时，取整单折扣(无论是否设置货品折扣)
		if (json.maindiscount) {
			json.details[i].discount = Number(json.maindiscount);
		}
		if (!json.details[i].price) json.details[i].price = 100; //进货价
		var discount = json.details[i].discount ? Number(json.details[i].discount) : 1;
		json.details[i].total = Number(json.details[i].num) * Number(json.details[i].price) * discount;
		totalnum += Number(json.details[i].num);
		totalsum += json.details[i].total;

		// json.details[i].styleid = vars.get('styleid' + json.details[i].styleid) || json.details[i].styleid;
		if (!json.details[i].styleid) {
			json.details[i].styleid = vars.get('styleid' + json.details[i].matCode);
			delete json.details[i].matCode;
		};

		json.details[i].colorid = vars.get('colorid' + json.details[i].colorid) || json.details[i].colorid; //传入BaiSe，输出coloridBaiSe
		json.details[i].sizeid = vars.get('sizeid' + json.details[i].sizeid) || json.details[i].sizeid; //
		if (!json.details[i].rowid) json.details[i].rowid = i;
	}

	json.totalnum = totalnum; //总数量
	json.totalsum = totalsum; //总金额

	//处理接口号之间的差异字段
	switch (json.interfaceid) {
		case "sf-14212-1": //采购入库单
			json.actid = 10; //操作类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.type = 10; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.typeid = 10; //单据类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.inoutflag = 1; //出入类型 1为入库,2为出库,必填
			// json.orderversion //创建为0，每改一次加1
			if (json.orderversion && json.orderversion > 0 && !json.billid) json.billid = vars.getObject('billInfo').pk;
			if (!json.dwid) json.dwid = vars.get('dwidVell');
			if (!json.shopid) json.shopid = loginObj.invid;
			if (!json.deliver) json.deliver = loginObj.id;
			break;

		case 'sf-14401-1': //销售订单
			if (!json.shopid) json.shopid = loginObj.invid;
			if (!json.sellerid) json.sellerid = loginObj.id;
			if (!json.clientid) json.clientid = vars.get('dwidXW');
			break;

		case 'sf-14211-1': //新销售开单接口
		case 'sf-1421-1': //老销售开单
			if (!json.shopid) json.shopid = loginObj.invid;
			if (!json.deliver) json.deliver = loginObj.id;
			if (json.dwid == undefined) json.dwid = vars.get('dwidXW'); //客户id值 非必填
			json.actid = 21; //操作类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.type = 21;
			// json.typeid = 21;//服务端自行处理
			json.inoutflag = 2;
			//差额
			var shortOf = Number(json.card || 0) + Number(json.cash || 0) + Number(json.remit || 0) + Number(json.agency || 0) - json.totalsum;
			if ((JSON.stringify(args) && args[0] == "balance") || shortOf < 0) {
				json.balance = Number(json.finpayVerifysum || 0) + shortOf; //结余=核销金额+差额
			} else {
				//找零
				//只有现金支付  现金填了500 实际的传值是cash:400(应付)，cashchange:100(找零)
				json.cashchange = shortOf;
				json.cash = Number(json.cash - json.cashchange || 0);
			}
			//总金额
			json.totalmoney = json.totalsum;
			delete json.totalsum;
			break;

		case 'sf-1862-1': //调拨出库
			json.totalmoney = json.totalsum;
			delete json.totalsum;
			json.type = 25; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			if (!json.deliver) json.deliver = loginObj.id; //送货人
			if (!json.shopid) json.shopid = loginObj.invid; //调出门店
			if (!json.invidref) { //调入门店
				var shopidZzd = vars.get('shopidZzd');
				json.invidref = json.shopid == shopidZzd ? vars.get('shopidCqd') : shopidZzd;
			}
			break;

		case 'sf-22101-1': //采购订单
			if (!json.dwid) json.dwid = vars.get('dwidVell');
			if (!json.respopid) json.respopid = loginObj.id; //默认当前登录角色
			break;

		case 'sf-22401-1': //批量入库
			if (!json.deliver) json.deliver = loginObj.id;
			// if(!json.opid) json.opid = loginObj.id;
			json.type = 10; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.inoutflag = 1; //出入类型 1为入库,2为出库,必填
			for (var i = 0; i < json.details.length; i++) {
				json.details[i].mainDwid = vars.get('dwid' + json.details[i].mainDwid) || json.details[i].mainDwid;
			};
			break;

		case 'sf-1924-1':
			delete json.totalsum;
			if (!json.deliver) json.deliver = loginObj.id;
			break;

		default:
			break;
	}

	delete json.interfaceid;
	vars.putObject('jsonparamObj', json);
	var str = JSON.stringify(json);
	log.info('\njsonparam = ' + str);
	vars.put('jsonparam', str); //参数需要用string传递
})();
