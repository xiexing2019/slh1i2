"use strict";

/*
 * 校验处理记录列表 的结果是否正确
 *
 */

var processListStr = prev.getResponseDataAsString();
var processList = JSON.parse(processListStr);
var batchList = vars.getObject('batchData');

AssertionResult.setFailure(!processRecordsCheck());

function processRecordsCheck() {
	var isRight = true;
	var batchInfo = batchList.dataList[0];
	var processInfo = processList.dataList[0];
	var ignore = ['modelClass', 'id', 'billno', 'flag', 'rem', 'staffname'];
	for(var key in batchInfo) {
		if(!batchInfo[key] || ignore.indexOf(key) != -1) continue;
		if(key == 'optime' || key == 'processtime') {
			isRight = batchInfo[key] == processInfo['optime'];
		} else if(key == 'prodate') {
			isRight = batchInfo[key].indexOf(processInfo[key]) != -1;
		} else if(key == 'opstaffname' || key == 'opstaffName') {
			isRight = batchInfo[key] == processInfo['opname'];
		} else {
			isRight = batchInfo[key] == processInfo[key];
		}

		if(!isRight) {
			AssertionResult.setFailureMessage("\n(按批次查)差异key:" + key + "\n按批次查数据:\n" + JSON.stringify(batchInfo) + "\n 处理记录数据:\n" + JSON.stringify(processInfo));
			break;
		}
	}

	return isRight;
}
