"use strict"

/*
 *  盘点处理后，校验当前库存列表，库存的变化
 *  agrs[0] 全盘：checkall   部分盘点：checkpart
 */

var resultStr = prev.getResponseDataAsString();
var afterInvNum = JSON.parse(resultStr);
var beforeInvNum = vars.getObject('beforeInvNum');
var jsonparamObj = vars.getObject('jsonparamObj');

vars.putObject('origBerforeInvNum', beforeInvNum);

AssertionResult.setFailure(!currentInvNumCheck());

function currentInvNumCheck() {
	var ret = hadInventoryListCheck(afterInvNum.dataList, jsonparamObj.details);
	if(args[0] == "checkall") {
		ret = ret && isOtherInvnumZero(afterInvNum.dataList, jsonparamObj.details);
	} else {
		ret = ret && haveNotInventoryColorSize(afterInvNum.dataList, jsonparamObj.details);
		ret = ret && otherMatInvnumCheck(beforeInvNum.dataList, jsonparamObj.details, afterInvNum.dataList);
	}
	if(!ret) {
		AssertionResult.setFailureMessage("\n beforeInvNum.dataList : \n" + JSON.stringify(beforeInvNum.dataList) + "\n jsonparamObj.details : \n" + JSON.stringify(jsonparamObj.details) + "\n afterInvNum.dataList : \n" + JSON.stringify(afterInvNum.dataList));
	}
	return ret;
}

//------------分割线(全盘时的验证逻辑)--------------------

//全部盘点时 验证未盘点的款号当前库存是否都为0
function isOtherInvnumZero(afterList, jsonparamList) {
	var isInvnumRight = true;
	for(var i = 0; i < afterList.length; i++) {
		if(afterList[i].invnum != '0') {
			isInvnumRight = false;
			break;
		}
	}
	return isInvnumRight;
}

//------------分割线(部分盘点时的验证逻辑)--------------------

//部分盘点 当前库存验证思路
/*
 * 1、验证盘点 款号颜色尺码 的库存是否正确，正确之后将该条当前库存记录删除
 * 2、验证盘点款号中没设置库存的颜色尺码的款号库存是否为0，验证通过之后将该条当前库存记录删掉
 * 3、验证没盘点到的款号的库存是否没有变动
 */

//相同款中未验证到的颜色尺码 盘点过后库存为0 验证
//例如:agc001 白色 M码 90件，那么agc001 其他颜色尺码的就为0件
function haveNotInventoryColorSize(afterList, jsonparamList) {
	var isInvnumRight = true;
	for(var i = 0; i < jsonparamList.length; i++) {
		isInvnumRight = sameMatOtherColorSize(afterList, jsonparamList[i]);
		if(!isInvnumRight) break;
	}
	return isInvnumRight;
}

function sameMatOtherColorSize(list, obj) {
	var isInvnumRight = true;
	for(var i = list.length; i > 0; i--) {
		var item = list[i - 1];
		if(obj.styleid == item.styleid) {
			if(item.invnum == "0") {
				list.splice(i - 1, 1);
			} else {
				isInvnumRight = false;
				break;
			}
		}
	}

	return isInvnumRight;
}

//部分盘点中，未盘点款号当前库存验证(未盘点款号当前库存应保持不变)
/*
 * 验证思路，盘点后当前库存列表中，已经将盘点过的款号的当前库存记录都删掉了；
 * 因此，未盘点前的当前库存列表中，也要将相应的款号记录删掉；
 * 然后再比较未盘点的款号当前库存是否没有变化
 */
function otherMatInvnumCheck(beforeList, jsonparamList, afterList) {
	for(var i = 0; i < jsonparamList.length; i++) {
		deleteBeforeInvnum(beforeList, jsonparamList[i]);
	}

	var isTwoListEqual = true;
	for(var i = 0; i < beforeList.length; i++) {
		if(!isListContainObject(afterList, beforeList[i])) {
			isTwoListEqual = false;
			break;
		}
	}
	return isTwoListEqual;
}

//未盘点前的当前库存列表中，将已经将盘点过的款号记录删掉
function deleteBeforeInvnum(list, obj) {
	for(var i = list.length; i > 0; i--) {
		var item = list[i - 1];
		if(obj.styleid == item.styleid) {
			list.splice(i - 1, 1);
		}
	}
}

//判断一个数组中是否包含一个对象
function isListContainObject(afterList, beforeObj) {
	var isContain = false;
	for(var i = 0; i < afterList.length; i++) {
		if(isApproximatelyObject(beforeObj, afterList[i])) {
			isContain = true;
			break;
		}
	}
	return isContain;
}
//判断两个对象是否相等
function isApproximatelyObject(beforeObj, afterObj) {
	var isApproximately = true;
	for(var key in beforeObj) {
		if(key == "id") continue;
		if(beforeObj[key] != afterObj[key]) {
			isApproximately = false;
			break;
		}
	}
	return isApproximately;
}



//------------公用函数部分(验证盘点的款号的当前库存是否正确)--------------------

//验证 盘点处理后的当前库存数是否正确 (仅验证盘点的款号)
function hadInventoryListCheck(afterList, jsonparamList) {
	var isInvnumRight = true;
	for(var i = 0; i < jsonparamList.length; i++) {
		isInvnumRight = hadInventoryObjectCheck(afterList, jsonparamList[i]);
		if(!isInvnumRight) break;
	}
	return isInvnumRight;
}

function hadInventoryObjectCheck(list, obj) {
	var isInvnumRight = true;
	//盘点到的 款号的颜色尺码 的 当前库存验证
	//例如:agc001 白色 M码 90件，那么agc001 其他颜色尺码的就为0件
	for(var i = list.length; i > 0; i--) {
		var item = list[i - 1];
		if(obj.styleid == item.styleid && obj.sizeid == item.sizeid && obj.colorid == item.colorid) {
			if(obj.num == item.invnum) {
				list.splice(i - 1, 1);
			} else {
				isInvnumRight = false;
				break;
			}
		}
	}

	return isInvnumRight;
}
