"use strict";

var str = prev.getResponseDataAsString();
var cancelInvNum = JSON.parse(str);
var berforeInvNum = vars.getObject('origBerforeInvNum');

AssertionResult.setFailure(!isApproximatelyObject(berforeInvNum, cancelInvNum));

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['actid', 'version']; //忽略的key
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		}
		// else if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
		// 	ret = ret && (actual[i].indexOf(expected[i]) != -1);
		// }
		else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
}
