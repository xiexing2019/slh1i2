/*
 * 组装物流商核销的jsonparam
 * details:[]
 */
(function jsonparamFormat() {
	var json = vars.getObject('jsonparam');
	var loginObj = vars.getObject('loginObj');
	// json.details = [];
	// "details": [
	//     {
	//         "total": "金额",
	//         "num": "数量",
	//         "price": "价格",
	//         "styleid": "款号ID值",
	//         "rem": "备注"
	// }
	// json.totalmoney = 0; //特殊货品
	//支付
	if(json.cash && !json.cashaccountid) json.cashaccountid = vars.get('cashaccountid_cqd');
	if(json.card && !json.cardaccountid) json.cardaccountid = vars.get('cardaccountid_cqd');
	if(json.remit && !json.remitaccountid) json.remitaccountid = vars.get('agencyaccountid_cqd');
	if(!json.prodate) json.prodate = vars.get('today'); //发生日期
	if(!json.shopid) json.shopid = loginObj.invid;
	if(!json.deliver) json.deliver = loginObj.id; //业务员ID值
	json.action = 'add';

	vars.putObject('jsonparamObj', json);
	var str = JSON.stringify(json);
	log.info('\n物流商核销 sf-1452 jsonparam = ' + str);
	vars.put('jsonparam', str); //参数需要用string传递
})();
