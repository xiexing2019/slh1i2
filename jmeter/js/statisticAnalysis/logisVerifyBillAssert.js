/*
 * 收支流水 代收收款 数据验证
 */
var str = prev.getResponseDataAsString();
var resultObj = JSON.parse(str);
var jsonparam = vars.getObject('jsonparamObj');
var billInfo = vars.getObject('billInfo');

var expected = [{
		"billno": billInfo.billno,
		"accountName": "东灵测试-现金账户",
		"money": jsonparam.cash,
	},
	{
		"billno": billInfo.billno,
		"accountName": "东灵测试-银行账户",
		"money": jsonparam.card,
	}, {
		"billno": billInfo.billno,
		"accountName": "东灵测试-代收账户",
		"money": jsonparam.remit,
	}];


var ret = true;
var msg = '找不到数据:';
for(var i = 0; i < expected.length; i++) {
	ret = assert(expected[i]);
	if(!ret) msg += '\n' + JSON.stringify(expected[i]);
}
AssertionResult.setFailure(!ret);
AssertionResult.setFailureMessage(msg);

function assert(expected) {
	var ret = false;
	for(var i = 0; i < resultObj.dataList.length; i++) {
		var ret = isApproximatelyObject(expected, resultObj.dataList[i]);
		if(ret) {
			resultObj.dataList.splice(i, 1);
			break;
		}
	}
	return ret;
};

function isApproximatelyObject(expected, actual) {
	var ret = true;
	for(var i in expected) {
		if(!actual[i]) continue; //只判断相同键值
		if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			if(actual[i].length < expected[i].length) {
				ret = expected[i].indexOf(actual[i]) != -1;
			} else {
				ret = actual[i].indexOf(expected[i]) != -1;
			}
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) break;
	}
	return ret;
};
