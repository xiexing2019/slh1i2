"use strict";
//订单按订货入库后，采购入库-按批次查界面验证
//按订货的qf与按批次查ql的第一条数据比对

//获取数据
var str = prev.getResponseDataAsString();
var actualObj = JSON.parse(str);
var billInfo = vars.getObject('billInfo');
var expObj = jsonFormat(billInfo);
var ret = !actualObj.hasOwnProperty('error') && isApproximatelyObject(actualObj, expObj);
AssertionResult.setFailure(!ret);

//obj 入库单
//orderObj 订货单
function jsonFormat(obj) {
	obj.shopname = obj.show_invid;
	obj.puredmaincode = obj.billno;
	delete obj.billno;
	var totalmoney = 0,
		totalnum = 0;
	for(var i = 0; i < obj.details.length; i++) {
		totalnum += Number(obj.details[i].recvnum);
		totalmoney += Number(obj.details[i].recvnum) * Number(obj.details[i].price)
	}
	obj.totalnum = totalnum;
	obj.paysum = obj.actualpay;
	obj.balance = obj.paysum - obj.totalmoney;
	return obj;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['actid', 'version']; //忽略的key
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = ret && (expected[i].indexOf(actual[i]) != -1);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
}
