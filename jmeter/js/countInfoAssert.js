"use strict";
var preObj = vars.get(''); //起始值
// var billInfo = vars.getObject('billInfo');
var diff = vars.get('num');

var str = prev.getResponseDataAsString();
var actualObj = JSON.parse(str); //实际值

var expect = preObj + diff
var ret = isApproximatelyObject(expect, actualObj);
AssertionResult.setFailure(!ret);

function isApproximatelyObject(expArr, actArr) {
	var ret = true;
	var ignore = ['modelClass', 'id']; //忽略的key
	for(var j = 0; j < count; j++) {
		var expecteded = expArr[i];
		var actual = actArr[i];
		for(var i in expecteded) {
			if(!actual[i] || ignore.indexOf(i) != -1) {
				keys.push(i);
				continue;
			}
			if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
				ret = actual[i].indexOf(expecteded[i]) != -1;
			} else {
				ret = expecteded[i] == actual[i];
			}
			if(!ret) {
				AssertionResult.setFailureMessage("key = " + i + "  expecteded=" + expecteded[i] + "  actual=" + actual[i]);
				break;
			}
		}
		if(!ret) break;
	}
	return ret;
};

function addObject(arr) {
	var jo1 = arr[0];
	for(var j = 1; j < arr.length; j++) {
		var jo2 = arr[i];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (value + jo2[i]).toString(); //.toFixed(3) 小数保留3位
			}
		}
	}
	return jo1;
};
