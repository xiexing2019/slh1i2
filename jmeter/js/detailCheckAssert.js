"use strict"
//按明细查 列表验证过程
//用qf返回结果的details 跟按明细查的数据做比较 验证数据的正确性
var str = prev.getResponseDataAsString();
var resultObj = JSON.parse(str);

var count = resultObj.count; //总记录数
var data = resultObj.dataList; //明细数组
var sumrow = resultObj.sumrow; //汇总值
var keys = []; //未验证键名
var billInfo = vars.getObject("billInfo"); //调用qf的返回结果

//数据验证
var ret = false,
	failMsg = '';
if(resultObj.hasOwnProperty('error')) {
	failMsg += '查询出错';
} else if(data.length < 1) {
	failMsg += '查询无结果';
} else {
	ret = isApproximatelyObject(data, billInfo.details);
	log.info("\n未验证键名有: " + JSON.stringify(keys));
}

//汇总值验证
var sumObj = addObject(data);
ret = ret && isApproximatelyObject(sumObj, sumrow);
AssertionResult.setFailure(!ret);

function isApproximatelyObject(expArr, actArr) {
	var ret = true;
	var ignore = ['modelClass', 'id']; //忽略的key
	for(var j = 0; j < count; j++) {
		var expecteded = expArr[i];
		var actual = actArr[i];
		for(var i in expecteded) {
			if(!actual[i] || ignore.indexOf(i) != -1) {
				keys.push(i);
				continue;
			}
			if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
				ret = actual[i].indexOf(expecteded[i]) != -1;
			} else {
				ret = expecteded[i] == actual[i];
			}
			if(!ret) {
				failMsg += "key = " + i + "  expecteded=" + expecteded[i] + "  actual=" + actual[i];
				break;
			}
		}
		if(!ret) break;
	}
	return ret;
};

function addObject(arr) {
	var jo1 = arr[0];
	for(var j = 1; j < arr.length; j++) {
		var jo2 = arr[i];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (value + jo2[i]).toString(); //.toFixed(3) 小数保留3位
			}
		}
	}
	return jo1;
};
