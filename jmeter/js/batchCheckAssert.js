"use strict";
//按批次查这类一条数据对应一个单据的数据验证
//args[]用于额外的数据验证
//获取interfaceid
var urlStr = prev.getUrlAsString();
var interfaceid = urlStr.split("interfaceid=")[1];
if(interfaceid.length > 0) interfaceid = interfaceid.split("&")[0];
var failMsg = 'interfaceid = ' + interfaceid; //收集错误信息
//获取查询结果与单据信息
var str = prev.getResponseDataAsString();
var resultObj = JSON.parse(str);
var data = resultObj.dataList; //明细数组
var billInfo = vars.getObject("billInfo"); //调用qf的返回结果
var keys = []; //未验证键名

//数据验证 按批次查 单据生成的明细只有一条
var ret = false;
if(resultObj.hasOwnProperty('error')) {
	failMsg += '查询出错';
} else if(data.length < 1) {
	failMsg += '查询无结果';
} else {
	ret = true;
}

//根据用例，额外需要验证的内容,一般是那些字段相同，但是显示不同
// e.g. invalidflag=='否' invalidflag=='0'
//写在JSR223中的parameters中，参数用空格分隔，因此传入的string中不能带空格
// e.g. data[0].diffnum==35 data[0].flag==0
if(ret && args.length > 0) {
	for(var i = 0; i < args.length; i++) {
		ret = eval(args[i]);
		eval('delete ' + args[i].split('==')[0]); //验证完删除相应字段
		if(!ret) {
			failMsg += '\n error : ' + args[i];
			break;
		}
	}
}

ret = ret && isApproximatelyObject(data[0], billInfo);
log.info("\ninterfaceid = " + interfaceid + "  未验证键名有: " + JSON.stringify(keys));

//汇总值验证
if(interfaceid != "ql-14222") { //ql-14222销售开单-挂单查询
	var sumObj = addObject(excludeFutility(data));
	ret = ret && isApproximatelyObject(sumObj, resultObj.sumrow);
}

//断言
AssertionResult.setFailure(!ret);
AssertionResult.setFailureMessage(failMsg);


//比较两个对象之间是否相等(非严格意义上相等)
//返回是否相等，并将具有相同键且相同键的值相同 的 键存放于数组中返回
function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['modelClass', 'id', 'alipay_status', 'weixinpay_status', 'flag', 'orderversion'];
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) {
			keys.push(i);
			continue;
		}

		if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			if(actual[i].length < expected[i].length) {
				ret = expected[i].indexOf(actual[i]) != -1;
			} else {
				ret = actual[i].indexOf(expected[i]) != -1;
			}
		} else {
			ret = expected[i] == actual[i];
		}

		if(!ret) {
			failMsg += "\nexpected = " + JSON.stringify(expected) + "\n\n actual = " + JSON.stringify(actual) + "\n\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i];
			break;
		}
	}
	return ret;
};

//invalidflag=1 作废数据不统计在汇总值中
function addObject(arr) {
	var jo1 = {};
	for(var j = 0; j < arr.length; j++) {
		var jo2 = arr[j];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (Number(value) + Number(jo2[i])).toString(); //.toFixed(3) 小数保留3位
			}
		}
	}
	return jo1;
};

//删除返回dataList中的作废数据，用于汇总值验证
function excludeFutility(arr) {
	var ret = [];
	for(var i = 0; i < arr.length; i++) {
		if(arr[i].invalidflag && arr[i].invalidflag == 1) ret.push(arr[i]);
	}
	return ret;
};
