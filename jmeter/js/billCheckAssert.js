"use strict";
//根据sf的jsonparam参数和返回结果验证相应qf的返回值
//获取数据
var str = prev.getResponseDataAsString();
var actualObj = JSON.parse(str);
var jsonparam = vars.getObject('jsonparamObj'); //开单参数jsonparam
var billInfo = vars.getObject('billInfo'); //开单接口返回结果
delete billInfo.modelClass; //

var ret = !actualObj.hasOwnProperty('error') && isApproximatelyObject(jsonparam, actualObj) && isApproximatelyObject(billInfo, actualObj);

vars.putObject('billInfo', actualObj); //更新billInfo，变为详细数据

AssertionResult.setFailure(!ret);

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['actid', 'version', 'orderversion', 'modelClass']; //忽略的key
	if(args.length > 0 && args[0] == "ignoreRowid") ignore.push('rowid');
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else if(i.indexOf("time") != -1 || i.indexOf("date") != -1 || i == 'show_styleid') {
			if(actual[i].length < expected[i].length) {
				ret = expected[i].indexOf(actual[i]) != -1;
			} else {
				ret = actual[i].indexOf(expected[i]) != -1;
			}
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + JSON.stringify(expected[i]) + "  actual=" + JSON.stringify(actual[i]));
			break;
		}
	}
	return ret;
}
