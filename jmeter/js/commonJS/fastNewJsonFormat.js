/*
 * 快速新增货品、客户、厂商...等基本信息的 jsonparam组装
 * 必传一个参数 args[0]表示种类
 */


var loginObj = vars.getObject('loginObj');

(function makeJsonparam() {

	var jsonparam;
	var type = args[0];

	switch(type) {
	case "customer":
		jsonparam = {
			"action": "add",
			"name": customerName(),
			"creditbalance": "1",
			"discount": "1",
		};
		break;
	case "mat":
		jsonparam = {
			"action": "add",
			"name": matName(),
			"code": matCode(),
			"colorids": vars.get("coloridBaiSe") + "," + vars.get("coloridTuoSe") + "," + vars.get("coloridHongSe"),
			"sizeids": vars.get("sizeidM") + "," + vars.get("sizeidXL") + "," + vars.get("sizeidL"),
			"invid": loginObj.invid,
			"shopid": loginObj.invid,
			"marketdate": vars.get("today"),
			"purprice": "400",
			"stdprice1": "520",
			"stdprice2": "480",
			"stdprice3": "640",
			"stdprice4": "600"
		};
		break;
	default:
		break;
	}

	var obj = JSON.stringify(jsonparam);
	vars.putObject('jsonparam', obj);
	vars.putObject('jsonparamObj', jsonparam);
})();


function customerName() {
	var name = randomString(5, '赵钱孙李周吴郑王金木水火土梅花竹子兰欣雨诗白晓玉宇宋剑志娇春明莹薛学雪海国建兵卫士嘉媛家abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	return "cust" + name;
}

function matName() {
	var name = randomString(8, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	return "mat" + name;
}

function matCode() {
	var name = randomString(8, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	return "matcode" + name;
}

function factoryName() {
	var name = randomString(5, '阿乔斯迪克灵特达邦威秀斯戴女娃娃人乖乖狗秋魅水力伊美鸿百衣丽顺香可人艾奈爱厄尔裤业红袖红玖星质地色月奇凰活大地鞋雪俪柜ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
	return "厂商" + name;
}

function randomString(len, area) {　　
	len = len || 32;　　
	var $chars = area; /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/ 　　
	var maxPos = $chars.length;　　
	var pwd = '';　　
	for(i = 0; i < len; i++) {　　　　
		pwd += $chars.charAt(Math.floor(Math.random() * maxPos));　　
	}　　
	return pwd;
}
