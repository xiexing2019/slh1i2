map_request: {
	epid = 5541,
	jsonparam = {
		"card": "200",
		"cash": "1000",
		"remit": "300",
		"balance": "0",
		"remark": "备注",
		"invalidflag": "9",
		"details": [{
			"num": "5",
			"sizeid": "2",
			"rem": "明细1",
			"colorid": "3",
			"price": 100,
			"total": 500,
			"styleid": "10015278269",
			"rowid": 0
		}, {
			"num": "10",
			"sizeid": "3",
			"rem": "明细2",
			"colorid": "3",
			"price": 100,
			"total": 1000,
			"styleid": "10015278269",
			"rowid": 1
		}],
		"cashaccountid": "15317",
		"cardaccountid": "15317",
		"remitaccountid": "301801",
		"hashkey": "A4:d1:8c:1e:19:85-2017-10-26 20:26:20:373",
		"action": "add",
		"invid": "10193",
		"prodate": "2017-10-26",
		"totalnum": 15,
		"totalsum": 1500,
		"actid": 10,
		"type": 10,
		"typeid": 10,
		"inoutflag": 1,
		"dwid": "592217",
		"shopid": "10193",
		"deliver": "30725"
	},
	slh_version = 9.1001,
	sessionid = 03 D21938 - 12 D8 - 34 C9 - C810 - 7 B306EC157D2,
	interfaceid = sf - 14212 - 1
}
