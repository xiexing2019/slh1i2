//常青店开单 中洲店发货
var jsonparam = {
	"interfaceid": "sf-14211-1",
	"srcType": "1",
	"remark": "异地发货",
	"agency": 2000,
	"logisDwid": vars.get('dwidSFKD'), //物流商ID值
	"logisBillno": "123456",
	"details": [{
		"num": "5",
		"sizeid": "M",
		"matCode": "agc001",
		"rem": "明细1",
		"colorid": "BaiSe",
		"price": "200",
		"discount": "1"
  }, {
		"num": "5",
		"sizeid": "L",
		"matCode": "agc001",
		"rem": "明细2",
		"colorid": "BaiSe",
		"price": "200",
		"discount": "1"
  }],
	"invid": vars.get('shopidZzd'),
};
vars.putObject('jsonparam', jsonparam);
