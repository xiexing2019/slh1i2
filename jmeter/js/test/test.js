var jsonparam = {
	"interfaceid": "sf-14211-1",
	"srcType": "1",
	"remark": "备注",
	"maindiscount": "0.9",
	"agency": 900,
	"logisDwid": vars.get('dwidSFKD'), //物流商ID值
	"logisBillno": "123456",
	"details": [{
		"num": "2",
		"sizeid": "M",
		"matCode": "agc001",
		"rem": "明细1",
		"colorid": "BaiSe",
		"price": "200",
  }, {
		"num": "3",
		"sizeid": "L",
		"matCode": "agc001",
		"rem": "明细2",
		"colorid": "BaiSe",
		"price": "200",
  }]
};
vars.putObject('jsonparam', jsonparam);
