/*
 * 按厂商报单 ql qf 接口校验脚本
 */
//验证思路  batchData1 + jsonparam = batchData2;   detailData1 + jsonparam = detailData2;
var jsonparam = vars.getObject('jsonparamObj');
var batchData1 = vars.getObject('batchData1');
var batchData2 = vars.getObject('batchData2');
var detailData1 = vars.getObject('detailData1');
var detailData2 = vars.getObject('detailData2');

//batchData1 + jsonparam = batchData2;
var num = Number(batchData1.dataList[0].num) + getTotalNum(jsonparam.details);
batchData1.dataList[0].num = num + "";
var ret = isApproximatelyObject(batchData1, batchData2);
AssertionResult.setFailure(!ret);

//detailData1 + jsonparam = detailData2;
var expectNum = getTotalNum(jsonparam.details) + getTotalNum(detailData1.details);
var actualNum = getTotalNum(detailData2.details);
var ret2 = (expectNum == actualNum);
var ret2 = ret2 && isApproximatelyObject(jsonparam, detailData2) && isApproximatelyObject(detailData1, detailData2);
AssertionResult.setFailure(!ret);

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['actid', 'version']; //忽略的key
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = ret && (actual[i].indexOf(expected[i]) != -1);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
}

function getTotalNum(details) {
	var num = 0;
	for(var i = 0; i < details.length; i++) {
		num = num + Number(details[i].num);
	}
	return num;
}
