"use strict";

/*
 * 挂单转销售单的前置参数处理
 */

//hashkey 唯一值 防止订单重复提交
// var date = new Date();
// var ms = date.getMilliseconds();
// var hashkey = vars.get("deviceno") + "-" + vars.get("currentTime") + ":" + ms;
// vars.put("hashkey", hashkey);
//
// var billInfo = vars.getObject("billInfo");
// billInfo.invalidflag = "0";
// vars.putObject("jsonparamPending", JSON.stringify(billInfo));

//直接修改挂单的jsonparam作为正式单的jsonparam使用
var jsonparam = vars.getObject('jsonparamObj');
log.info('-----\n' + JSON.stringify(jsonparam));
jsonparam.action = 'edit';
jsonparam.invalidflag = '0';
jsonparam.pk = vars.get('pk');
//更新hashkey
var ms = new Date().getMilliseconds();
jsonparam.hashkey = vars.get("deviceno") + "-" + vars.get("currentTime") + ":" + ms;
vars.put('jsonparam', JSON.stringify(jsonparam));
