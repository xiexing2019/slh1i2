"use strict";
//按订货入库单据明细验证

//获取数据
var str = prev.getResponseDataAsString();
var actualObj = JSON.parse(str);
var orderObj = vars.getObject('jsonparamOrder'); //订单未格式化过的jsonparam
var jsonparam = vars.getObject('jsonparamObj'); //订货入库单
jsonparam = jsonFormat(jsonparam, orderObj);
var failMsg = '单据明细验证error:\n';
var ret = !actualObj.hasOwnProperty('error') && isApproximatelyObject(jsonparam, actualObj);

//根据用例，额外需要验证的内容
//写在JSR223中的parameters中，参数用空格分隔，因此传入的string中不能带空格
// e.g. actualObj.details[i].recvnum==30 actualObj.purcost==60
if(ret && args.length > 0) {
	for(var i = 0; i < args.length; i++) {
		if(!eval(args[i])) {
			ret = false;
			failMsg += '\n error : ' + args[i];
			break;
		}
	}
}

vars.putObject('billInfo', actualObj); //更新billInfo，变为详细数据
// log.info('----------\nbillinfo = ' + JSON.stringify(actualObj));
AssertionResult.setFailure(!ret);
AssertionResult.setFailureMessage(failMsg);
//obj 入库单
//orderObj 订货单
//因为涉及部分入库、全部入库 分次入库的情况，已入库数和已到数量获取数据验证比较麻烦，放到额外验证种
function jsonFormat(obj, orderObj) {
	delete obj.orderversion;
	var totalnum = 0,
		totalsum = 0;
	for(var i = 0; i < obj.details.length; i++) {
		delete obj.details[i].recvnum; //已入库数
		obj.details[i].num = orderObj.details[i].num;
		totalnum += Number(orderObj.details[i].num);
		obj.details[i].total = obj.details[i].num * obj.details[i].price;
		totalsum += Number(obj.details[i].total);
		obj.details[i].pk = obj.details[i].billid;
	}
	delete obj.purcost; //已到数量
	obj.totalnum = totalnum;
	obj.totalsum = totalsum;
	return obj;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['actid', 'version']; //忽略的key
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = ret && (actual[i].indexOf(expected[i]) != -1);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			failMsg += "\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i];
			break;
		}
	}
	return ret;
}
