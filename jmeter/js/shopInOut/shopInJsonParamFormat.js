"use strict";
//根据调出单的jsonparam和billInfo生成调入单的jsonparam
var jsonparam = vars.getObject('jsonparamObj'); //获取调出单的jsonparam
var billInfo = vars.getObject('billInfo'); //qf-14213-1的返回数据

jsonparam.billid = billInfo.pk;
jsonparam.type = '16';
jsonparam.invid = jsonparam.invidref;
//
var shop = jsonparam.shopid;
jsonparam.shopid = jsonparam.invidref;
jsonparam.invidref = shop;

var staffid000 = vars.get('staffid000');
jsonparam.deliver = jsonparam.deliver == staffid000 ? vars.get('staffid200') : staffid000;
//更新hashkey
var ms = new Date().getMilliseconds();
jsonparam.hashkey = vars.get("deviceno") + "-" + vars.get("currentTime") + ":" + ms;

// jsonparam.details[i].moverecvnum //验货数
// jsonparam.details[i].checkprenum //差异数
for(var i = 0; i < jsonparam.details.length; i++) {
	jsonparam.details[i].billid = billInfo.details[i].pk;
}

//更新jsonparam和jsonparamObj
var shopOut = vars.getObject('jsonparamObj');
vars.putObject('shopOutJsonparamObj', jsonparam); //与调出单区别 需要用到其中的调出批次
vars.putObject('jsonparamObj', jsonparam);
vars.put('jsonparam', JSON.stringify(jsonparam));
