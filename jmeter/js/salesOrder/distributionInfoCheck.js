"use strict";

/*
 * 验证按配货开单列表点进去的明细界面
 * 这里只验证了款号、颜色、尺码、数量、客户是否相同，至于价格、店员、总价、付款方式等一系列字段没有校验
 * args[0] 必填 存客户id的变量名
 */

var clientid = vars.getObject(args[0]);
var loginObj = vars.getObject('loginObj');
var resultObj = JSON.parse(prev.getResponseDataAsString());
var distributionObj = vars.getObject("distOrderData");

AssertionResult.setFailure(!isApproximatelyObject(distributionInfoFormat(), resultObj));

function distributionInfoFormat() {
	var distributionInfo = {
		'shopid': loginObj.invid,
		'dwid': clientid
	};

	var details = [];
	for(var i = 0; i < distributionObj.listData.length; i++) {
		if(distributionObj.listData[i].clientid != clientid) continue;
		var item = {
			'styleid': distributionObj.styleid,
			'sizeid': distributionObj.listData[i].sizeid,
			'colorid': distributionObj.listData[i].colorid,
			'num': distributionObj.listData[i].num
		};
		details.push(item);
	}
	distributionInfo.details = details;

	return distributionInfo;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	for(var i in expected) {
		if(!actual[i]) continue; //只判断相同键值
		if(expected[i] instanceof Array) {
			ret = isApproximatelyArray(expected[i], actual[i]);
		} else if(expected[i] instanceof Object) {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + JSON.stringify(expected[i]) + "  actual=" + JSON.stringify(actual[i]));
			break;
		}
	}
	return ret;
}

function isArrayContainObject(array, obj) {
	var isContin = false;
	for(var i = 0; i < array.length; i++) {
		if(obj.clientid == array[i].clientid) {
			isContin = isApproximatelyObject(obj, array);
		}
	}
	return isContin;
}

function isApproximatelyArray(expected, actual) {
	var isApproximately = expected.length == actual.length;
	for(var i = 0; i < expected.length; i++) {
		isApproximately = isApproximately && isArrayContainObject(actual, expected[i]);
		if(!isApproximately) break;
	}
	return isApproximately;
}
