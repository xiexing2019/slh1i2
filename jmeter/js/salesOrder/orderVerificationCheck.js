"use strict";

/*
 * 销售订单自动核销验证
 * type【batch：按批次查    detail：按明细查】
 */

var result = dataFormat();
AssertionResult.setFailure(!isApproximatelyObject(result.orderMainInfo, result.orderMainInfo2));

function dataFormat() {
	//获取基本数据
	var type = "batch";
	var orderMainInfo = vars.getObject("orderMainInfo");
	if(args.length > 0 && args[0] == "detail") {
		type = args[0];
		orderMainInfo = vars.getObject("orderDetailInfo");
	}
	var jsonparamObj = vars.getObject("jsonparamObj");
	var str = prev.getResponseDataAsString();
	var orderMainInfo2 = JSON.parse(str); //eval("(" + str + ")");

	//相同部分数据组装
	var isAllSend = true;
	if(orderMainInfo.dataList[0].restnum > jsonparamObj.totalnum) isAllSend = false;
	var sendNum = Number(jsonparamObj.totalnum);
	orderMainInfo.dataList[0].diffnum = Number(orderMainInfo.dataList[0].diffnum) - sendNum + "";
	orderMainInfo.sumrow.diffnum = Number(orderMainInfo.sumrow.diffnum) - sendNum + "";
	orderMainInfo.dataList[0].restnum = Number(orderMainInfo.dataList[0].restnum) - sendNum + "";
	orderMainInfo.sumrow.restnum = Number(orderMainInfo.sumrow.restnum) - sendNum + "";

	//特殊部分数据组装
	switch(type) {
	case "batch":
		orderMainInfo.dataList[0].recvnum = Number(orderMainInfo.dataList[0].recvnum) + sendNum + "";
		orderMainInfo.sumrow.recvnum = Number(orderMainInfo.sumrow.recvnum) + sendNum + "";
		if(isAllSend) {
			orderMainInfo.dataList[0].sendflag = "2";
			orderMainInfo.dataList[0].flag = "全部发货";
		} else {
			orderMainInfo.dataList[0].sendflag = "1";
			orderMainInfo.dataList[0].flag = "部分发货";
		}
		break;
	case "detail":
		orderMainInfo.dataList[0].delivernum = Number(orderMainInfo.dataList[0].delivernum) + sendNum + "";
		orderMainInfo.sumrow.delivernum = Number(orderMainInfo.sumrow.delivernum) + sendNum + "";
		if(isAllSend) {
			orderMainInfo.dataList[0].flag = "2";
			orderMainInfo.dataList[0].mainFlag = "全部发货";
		} else {
			orderMainInfo.dataList[0].flag = "1";
			orderMainInfo.dataList[0].mainFlag = "部分发货";
		}
		break;
	default:
		break;
	}

	return {
		"orderMainInfo": orderMainInfo,
		"orderMainInfo2": orderMainInfo2
	};
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	for(var i in expected) {
		if(!actual[i]) continue; //只判断相同键值
		if(typeof (expected[i]) == 'object') {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = ret && (actual[i].indexOf(expected[i]) != -1);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
}
