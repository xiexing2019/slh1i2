/*
 * 组装订单配货数据
 */

(function distributionFormat() {
	var baseObj = vars.getObject("distributeObj");
	var loginObj = vars.getObject('loginObj');

	var jsonparamObj = {
		"invid": loginObj.invid,
		"styleid": vars.get("styleid")
	};

	var listData = [];
	for(var key in baseObj.mapData) {
		var array = baseObj.mapData[key];
		for(var i = 0; i < array.length; i++) {
			var item = array[i];
			item.colorid = key.split("-")[0];
			item.sizeid = key.split("-")[1];
			item.num = Math.floor(Math.random() * Number(item.ordernum) + 1);
			listData.push(item);
		}
	}

	jsonparamObj.listData = listData;
	vars.put("jsonparam", JSON.stringify(jsonparamObj));
	vars.putObject("distOrderData", jsonparamObj);
})();
