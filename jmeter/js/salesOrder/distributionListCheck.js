"use strict";

/*
 * 验证安配货开单列表数据的正确性
 * args[0] 必填 存客户id的变量名
 */

var clientid = vars.getObject(args[0]);
var loginObj = vars.getObject('loginObj');

var resultObj = JSON.parse(prev.getResponseDataAsString());
var distributionObj = vars.getObject("distOrderData");

var ret = resultObj.count == 1;
ret = ret && isApproximatelyObject(expectedFormat(distributionObj.listData), resultObj.dataList[0]);

AssertionResult.setFailure(!ret);

function expectedFormat(resourceList) {
	var expectedObj = {
		'id': clientid,
		'invid': loginObj.invid,
		'invname': loginObj.invname,
		'prodate': vars.getObject('today'),
		'opname': loginObj.name
	};

	var distributionNum = 0;
	for(var i = 0; i < resourceList.length; i++) {
		if(resourceList[i].clientid == clientid) {
			distributionNum += Number(resourceList[i].num);
			expectedObj.dwname = resourceList[i].clientname;
		}
	}
	expectedObj.distributenum = distributionNum;

	return expectedObj;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	for(var i in expected) {
		if(!actual[i]) continue; //只判断相同键值
		if(i == 'prodate') {
			ret = expected[i].indexOf(actual[i]) != -1;
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + JSON.stringify(expected[i]) + "  actual=" + JSON.stringify(actual[i]));
			break;
		}
	}
	return ret;
}
