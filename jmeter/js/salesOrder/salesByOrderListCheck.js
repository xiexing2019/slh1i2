"use strict";

/*
 * 按订货开单列表数据正确性验证验证
 *
 */

var result = prev.getResponseDataAsString();
var resultList = JSON.parse(result);
var batchList = vars.getObject('batchList');
var failMsg;

var ret = salesByOrderListCheck() && isApproximatelyObject(addObject(resultList.dataList), resultList.sumrow);

AssertionResult.setFailure(!ret);
AssertionResult.setFailureMessage(failMsg);

function salesByOrderListCheck() {
	var isRight = true;
	var actual = resultList.dataList[0];
	var expected = batchList.dataList[0];
	var ignore = ['sendflagid', 'orderversion', 'modelClass'];
	for(var key in actual) {
		if(ignore.indexOf(key) != -1) continue;
		if(key == 'delivernum') {
			isRight = actual[key] == expected['recvnum'];
		} else if(key == 'orderno') {
			isRight = actual[key] == expected['billno'];
		} else if(key == 'shopname') {
			isRight = actual[key] == expected['invname'];
		} else if(key == 'totalnum') {
			isRight = actual[key] == expected['num'];
		} else if(key == 'dwxxname') {
			isRight = actual[key] == expected['dwname'];
		} else if(key == 'paddiffnum' || key == 'diffnum') {
			isRight = actual[key] == expected['diffnum'];
		} else if(key = 'sellername') {
			isRight = (actual[key].indexOf(expected['deliver']) != -1);
		} else if(key == 'diffsum') {
			isRight = actual[key] == (Number(actual['totalsum']) - Number(actual['receiptsum']));
		} else if(key == 'sendflag') {
			isRight = actual[key] == expected['flag'];
		} else {
			isRight = actual[key] == expected[key];
		}

		if(!isRight) {
			failMsg += "\ndifferent key = " + key + "; actual=" + actual[key];
			break;
		}
	}

	return isRight;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	var ignore = ['modelClass', 'id'];
	for(var i in expected) {
		if(!actual[i] || ignore.indexOf(i) != -1) continue;
		ret = expected[i] == actual[i];

		if(!ret) {
			failMsg += "\nexpected = " + JSON.stringify(expected) + "\n\n actual = " + JSON.stringify(actual) + "\n\nkey = " + i + "  expected=" + expected[i] + "  actual=" + actual[i];
			break;
		}
	}
	return ret;
};

//invalidflag=1 作废数据不统计在汇总值中
function addObject(arr) {
	var jo1 = arr[0];
	for(var j = 1; j < arr.length; j++) {
		var jo2 = arr[j];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (Number(value) + Number(jo2[i])).toString(); //.toFixed(3) 小数保留3位
			}
		}
	}
	return jo1;
};
