/*
 * 按订单配货数据验证
 *
 */

var actual = prev.getResponseDataAsString();
var actualObj = JSON.parse(actual);

var jsonparam2 = vars.getObject("jsonparamObj2");
var jsonparam = vars.getObject("jsonparamObj");
var expectedObj = mapDataFormat(jsonparam, jsonparam2);

var ret = isApproximatelyObject(expectedObj.mapData, actualObj.mapData);
AssertionResult.setFailure(!ret);

function mapDataFormat(jsonparam, jsonparam2) {
	var mapDataObj = {};
	for(var i = 0; i < jsonparam.details.length; i++) {
		var key = jsonparam.details[i].colorid + "-" + jsonparam.details[i].sizeid;
		var itemDict = itemDictFormat(jsonparam, i);
		var array = [];
		array.push(itemDict);
		mapDataObj[key] = array;
	}

	for(var i = 0; i < jsonparam2.details.length; i++) {
		var key = jsonparam2.details[i].colorid + "-" + jsonparam2.details[i].sizeid;
		var itemDict = itemDictFormat(jsonparam2, i);
		if(mapDataObj[key]) {
			mapDataObj[key].push(itemDict);
		} else {
			var array = new Array();
			array.push(itemDict);
			mapDataObj[key] = array;
		}
	}

	return {
		"mapData": mapDataObj
	};
}

function itemDictFormat(jsonparam, index) {
	var value = jsonparam.details[index];
	var itemDict = {
		"clientid": jsonparam.clientid,
		"ordernum": value.num,
		"clientname": jsonparam.show_clientid,
		"orderdetids": value.pk
	};
	return itemDict;
}

function isApproximatelyObject(expected, actual) {
	var ret = true;
	for(var i in expected) {
		if(!actual[i]) continue; //只判断相同键值
		if(expected[i] instanceof Array) {
			ret = isApproximatelyArray(expected[i], actual[i]);
		} else if(expected[i] instanceof Object) {
			ret = isApproximatelyObject(expected[i], actual[i]);
		} else {
			ret = ret && (expected[i] == actual[i]);
		}
		if(!ret) {
			AssertionResult.setFailureMessage("\nexpected = " + JSON.stringify(expected) + "\nactual = " + JSON.stringify(actual) + "\nkey = " + i + "  expected=" + JSON.stringify(expected[i]) + "  actual=" + JSON.stringify(actual[i]));
			break;
		}
	}
	return ret;
}

function isArrayContainObject(array, obj) {
	var isContin = false;
	for(var i = 0; i < array.length; i++) {
		if(obj.clientid == array[i].clientid) {
			isContin = isApproximatelyObject(obj, array);
		}
	}
	return isContin;
}

function isApproximatelyArray(expected, actual) {
	var isApproximately = expected.length == actual.length;
	for(var i = 0; i < expected.length; i++) {
		isApproximately = isApproximately && isArrayContainObject(actual, expected[i]);
		if(!isApproximately) break;
	}
	return isApproximately;
}
