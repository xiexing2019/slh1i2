"use strict";
//resultObj为返回的当前库存数组
var str = prev.getResponseDataAsString();
var resultObj = JSON.parse(str);

var data1= dataFormat(resultObj);//调用格式化
var data = data1.dataList; //明细数组
var sumrow = data1.sumrow; //汇总值


var keys = []; //未验证键名
//汇总值验证
var sumObj = addObject(data);
var ret = isApproximatelyObject(sumObj, sumrow);

//数据验证
log.info("\n未验证键名有: " + JSON.stringify(keys));

AssertionResult.setFailure(!ret);

function addObject(arr) {
	var jo1 = arr[0];
	for(var j = 1; j < arr.length; j++) {
		var jo2 = arr[j];
		for(var i in jo2) {
			if(isNaN(jo2[i])) {
				jo1[i] = jo2[i];
			} else {
				var value = isNaN(jo1[i]) ? 0 : jo1[i];
				jo1[i] = (Number(value) + Number(jo2[i])).toString(); //.toFixed(3) 小数保留3位
				log.info(jo1[i]);
			}
		}
	}
	return jo1;
};

//比较两个对象之间是否相等(非严格意义上相等)
//返回是否相等，并将具有相同键且相同键的值相同 的 键存放于数组中返回
function isApproximatelyObject(expecteded, actual) {
	var ret = true;
	var ignore = ['modelClass', 'id', 'alipay_status', 'weixinpay_status', 'flag'];
	for(var i in expecteded) {
		if(!actual[i] || ignore.indexOf(i) != -1) {
			keys.push(i);
			continue;
		}

		if(i.indexOf("time") != -1 || i.indexOf("date") != -1) {
			ret = actual[i].indexOf(expecteded[i]) != -1;
		} else {
			ret = expecteded[i] == actual[i];
		}

		if(!ret) {
			AssertionResult.setFailureMessage("\nexpecteded = " + JSON.stringify(expecteded) + "\n actual = " + JSON.stringify(actual) + "\nkey = " + i + "  expecteded=" + expecteded[i] + "  actual=" + actual[i]);
			break;
		}
	}
	return ret;
};



/*将返回列表数组中含有数组的列表格式化,例子：库存分布*/
function dataFormat(data) {
  var title = data.dynamicTitle;
  for(var i = 0; i < data.dataList.length; i++) {
    for(var j = 0; j < title.length; j++) {
      data.dataList[i][title[j]] = data.dataList[i].dynamicData[j];
    }
    delete data.dataList[i].dynamicData;
  }
  delete data.dynamicTitle;

  for(var i = 0; i < title.length; i++) {
    data.sumrow[title[i]] = data.sumrow.dynamicData[i];
  }
  delete data.sumrow.dynamicData;
  return data;
}