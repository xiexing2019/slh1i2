require('../lib/global');
const fs = require('fs');
const path = require('path');

const docData = fs.readFileSync(path.join(__dirname, 'ddl.txt')).toString().split(`\n`);
let doc = '';



for (let index = 0; index < docData.length; index++) {
    const data = docData[index];
    if (!data.includes('COMMENT')) continue;

    let [str, comment] = data.split('COMMENT');
    // 备注
    doc += `/** ${comment.replace(/'|,/g, '')} */\n`;

    let [key, type] = str.trim().split(' ');
    const defValue = getDefValue(type);
    key = key.replace(/`/g, '').toLowerCase().split('_').map((val, index) => {
        return index == 0 ? val : _.upperFirst(val);
    }).join('');
    // constructor
    doc += `this.${key}=${defValue};\n`;
    // json
    // doc += `${key}:${defValue},\n`;
}

console.log(doc);

function getDefValue(type) {
    let [_type] = type.split('(');
    switch (_type) {
        case 'decimal':
        case 'tinyint':
        case 'int':
            _type = 0;
            break;
        case 'json':
            _type = '{}';
            break;
        default:
            _type = '\'\'';
            break;
    };
    return _type;
};