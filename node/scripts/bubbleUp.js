'use strict';
const basiceJson = require('../slh1/help/basiceJsonparam');
const common = require('../lib/common');
const getBasicData = require('../data/getBasicData');
const interfaceList = require('../slh1/help/listCheckHelp/interfaceList');
const reqHandler = require('../slh1/help/reqHandlerHelp');
let concurrentNum = 20;
//简单的冒泡,就检查下是否有服务端错误
describe('test', function () {
	before(async () => {
		await common.loginDo();

		//获取常用信息id
		BASICDATA = await getBasicData.getBasicID();
	});
	it('基础数据同步', async () => {
		await testSyncData();
	});
	it(`ql接口`, async () => {
		await testQlRes();
	});
	it('销售单', async () => {
		let sfRes = await common.editBilling(basiceJson.salesJson());
		let qfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-14211-1',
			pk: sfRes.result.pk,
		});
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});
});

//hr_staff：人员、sc_dwxx：往来单位、 sc_dict：字典相关、dres_style：款号,
//dres_style_detail：条码, sc_dict_color：颜色,sc_dict_size：尺码,
//sc_dict_color_group：颜色组,sc_dict_size_group：尺码组 sc_dict_brand：品牌,
//sc_dict_unit：计量单,brand_discount：折扣, sc_voicemsg:语音语义表,user_self_view个性化配置表,sc_param_device设备参数表
async function testSyncData() {
	const types = ['hr_staff', 'sc_dwxx', 'sc_dict', 'dres_style', 'dres_style_detail', 'sc_dict_color', 'sc_dict_size', 'sc_dict_color_group'
	, 'sc_dict_size_group', 'sc_dict_brand', 'sc_dict_unit', 'brand_discount', 'sc_voicemsg', 'user_self_view', 'sc_param_device'];
	let params = {
		lastSyncTime: common.getCurrentTime(), //不填同步所有数据，填写同步时间之后的数据 获取当前时间用于冒泡
		offset: 0, //偏移量，0从第一条开始同步，10：从第10条开始同步
	};
	const promises = types.map((value) => {
		params.type = value;
		return common.callInterface('cs-syncdata', params);
	});
	const res = await Promise.all(promises);
	res.forEach((value, index) => {
		if(value.error || !value.dataList) console.log(`type=${types[index]}基础数据同步失败:${value}`);
	});
};

async function testQlRes() {
	const dyadicArr = _.chunk(interfaceList.qlListInterfaceId, concurrentNum);
	const qlParams = {
		search_list: 0,
		prodate1: common.getDateString([0, 0, -7]),
		optime1: common.getDateString([0, 0, -7]),
		optime2: common.getCurrentDate(),
	};
	let qlParams2 = {};
	for(let interfaceid in interfaceList.qlIFCParams) {
		qlParams2[interfaceid] = {};
		let arr = interfaceList.qlIFCParams[interfaceid].split(';');
		arr.forEach((str) => {
			let [key, value] = str.split('=');
			qlParams2[interfaceid][key] = eval(value);
		});
	};

	let time = Date.now();
	for(let arr of dyadicArr) {
		let promises = arr.map((interfaceid) => {
			let params = Object.assign(qlParams, qlParams2[interfaceid] || {});
			return common.callInterface(interfaceid, qlParams);
		});
		let datas = await Promise.all(promises);

		datas.forEach((data, index) => {
			if(data.error) {
				let errorMsg = {
					opTime: new Date(),
					params: qlParams,
					result: data,
				};
				console.log(`${arr[index]}请求出错:${JSON.stringify(errorMsg)}}`);
			} else if(data.count == 0) {
				// console.log(`${arr[index]}查询无结果`);
			};
		});
	};
	let duration = Date.now() - time;
	console.log(`duration=${duration}ms`);
};
