const common = require('../lib/common');
const getBasicData = require('../data/getBasicData');
const readline = require('readline');
// const sspdJson = require('../shopDiary/help/json/basicJson');


const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.setPrompt('请输入开单的数量与明细条目数(默认2),以逗号分隔:');
rl.prompt();

rl.on('line', async function (line) {
	const [count, detailsCount = 2, interfaceId] = line.trim().split(',');
	//采购入库,销售开单,期初入库,库存调整,盘点录入,调拨
	const interfaceIds = interfaceId ? [`${interfaceId}`] : ['sf-14212-1', 'sf-14211-1', 'ec-inv-init-bill-save', 'ec-inv-adjust-bill-save', 'ec-inv-check-bill-save', 'ec-inv-move-out-bill-save', 'ec-mdm-org-saveFull'];

	//退出进程
	if (count == 'close') rl.close();

	if (isNaN(count)) {
		process.stdout.write('请输入数字:');
		return;
	};

	//用除000以外的角色登陆获取基础数据
	//否则后续用例不会重新登陆
	await common.loginDo({
		logid: '200'
	});
	BASICDATA = await getBasicData.getBasicID();
	const styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
	const styleidAgc003 = await common.callInterface('cs-getpk-style', {
		key: 'styleidAgc003', //show_styleidAgc002:'agc002' id
		code: 'agc003',
	}).then((res) => res.id);
	const styleInfo2 = await common.fetchMatInfo(styleidAgc003);
	console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
	const barCodes = getBarCodes(styleInfo);
	barCodes.push(...getBarCodes(styleInfo2));
	// console.log(`styleInfo=${JSON.stringify(styleInfo)}`);

	//并发开单
	for (let index = 0; index < interfaceIds.length; index++) {
		const element = interfaceIds[index];
		let params = { count: detailsCount, styleInfo: styleInfo, barCodes: barCodes };

		const promises = new Array(Number(count)).fill({}).map(async (obj, index) => {
			let sfRes;
			if (element.startsWith('sf')) {
				params.interfaceid = element;
				sfRes = await common.editBilling(getJson(params), false);
			} else if (element == 'ec-mdm-org-saveFull') {
				sfRes = await common.apiDo({
					apiKey: element,
					jsonParam: sspdJson.addSupplierJson(),
					check: false,
				});
			} else {
				params.apiKey = element;
				sfRes = await common.apiDo({
					apiKey: element,
					jsonParam: getJsonBySLH2(params),
					check: false
				});
			};
			if (sfRes.result.error) {
				console.log();
				console.log();
				console.log();
				common.post2('http://172.81.235.142:8081/conf/devops/dubbo/threadList.do');
				console.log();
				console.log();
				console.log();
				common.post2('http://172.81.210.252:8081/conf/devops/dubbo/threadList.do');
			};

			let ret = {
				no: index,
				result: sfRes.result,
				duration: sfRes.duration,
			};
			if (sfRes.result.error || sfRes.result.code < 0) {
				// ret.msg = sfRes.result.error || sfRes.result.msg;
				ret.code = sfRes.result.errorCode || sfRes.result.code;
			};
			return ret;
		});
		const res = await Promise.all(promises);

		let [totalTime, minTime, maxTime, passes] = [0, 5000, 0, 0];
		for (let i = 0; i < res.length; i++) {
			if (res[i].code < 0) {
				continue;
			};
			const time = res[i].duration;
			maxTime = time > maxTime ? time : maxTime;
			minTime = time < minTime ? time : minTime;
			totalTime += time || 0;
			passes++;
		};
		if (res.length == 1) {
			minTime = maxTime;
		};
		console.log(`\ninterfaceid = ${element}`);
		console.log(`最大开单时间为${maxTime}ms  最小开单时间为${minTime}ms\n平均开单时间为${common.div(totalTime, res.length).toFixed(0)}ms\n开单总数=${count}, 成功=${passes}, 失败=${count - passes}`);
		console.log(`result=${JSON.stringify(res)}\n`);
	};

	// let params = { count: detailsCount, styleInfo: styleInfo2, barCodes: barCodes };
	// params.interfaceid = interfaceIds[0];
	// //连续开单
	// for (let index = 0; index < count; index++) {
	// 	const sfRes = await common.editBilling(getJson(params), false);

	// 	// if (sfRes.result.error) {
	// 	// 	console.log();
	// 	// 	console.log();
	// 	// 	console.log();
	// 	// 	common.post2('http://172.81.235.142:8081/conf/devops/dubbo/threadList.do');
	// 	// 	console.log();
	// 	// 	console.log();
	// 	// 	console.log();
	// 	// 	common.post2('http://172.81.210.252:8081/conf/devops/dubbo/threadList.do');
	// 	// };

	// 	let ret = {
	// 		no: index,
	// 		result: sfRes.result,
	// 		duration: sfRes.duration,
	// 	};
	// 	if (sfRes.result.error || sfRes.result.code < 0) {
	// 		// ret.msg = sfRes.result.error || sfRes.result.msg;
	// 		ret.code = sfRes.result.errorCode || sfRes.result.code;
	// 		if (sfRes.result.error.includes('服务器忙')) {
	// 			await common.delay(5000);
	// 		};
	// 	};
	// 	console.log(`${JSON.stringify(ret)}`);
	// 	await common.delay(100);
	// };


	rl.prompt();
});

rl.on('close', function () {
	console.log('结束!');
	process.exit(0);
});

function getJson(params) {
	//货品信息，明细数量
	const [styleInfo, count = 2] = [params.styleInfo, params.count];
	let priceKey = 'purprice';
	if (params.interfaceid == 'sf-14211-1') {
		params.dwid = BASICDATA.dwidXw;
		priceKey = 'stdprice1';
	};

	let jsonParam = {
		interfaceid: params.interfaceid,
		dwid: params.dwid || BASICDATA.dwidVell,
		cash: 0,
		srcType: 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
	};
	let barCodes = params.barCodes || getBarCodes(styleInfo);
	barCodes = common.randomSort(barCodes);
	// console.log(`barCodes=${JSON.stringify(barCodes)}`);

	jsonParam.details = new Array(Number(count)).fill({}).map((detail, index) => {
		detail = {
			styleid: barCodes[index].styleid,
			colorid: barCodes[index].colorId,
			sizeid: barCodes[index].sizeId,
			price: styleInfo[priceKey],
			discount: 1,
			num: common.getRandomNum(1, 100),
		};
		jsonParam.cash += common.mul(detail.price, detail.num);
		return detail;
	});

	return jsonParam;
};

function getJsonBySLH2(params) {
	const [styleInfo, count = 2] = [params.styleInfo, params.count];
	let priceKey = 'purprice';
	let jsonParam = {
		unitId: LOGINDATA.epid,
		shopId: LOGINDATA.invid,
		invId: LOGINDATA.invid,
		deliver: LOGINDATA.id,
		totalNum: 0,
		totalMoney: 0,
		bizDate: common.getCurrentDate(),
	};
	switch (params.apiKey) {
		case 'ec-inv-check-bill-save'://盘点
			// priceKey = 'stdprice1';
			break;
		case 'ec-inv-init-bill-save'://期初入库
			break;
		case 'ec-inv-move-out-bill-save'://调拨
			jsonParam.otherShopId = BASICDATA.shopidCqd;
			jsonParam.otherInvId = BASICDATA.shopidCqd;
			// jsonParam.receiver//接受人
			break;
		case 'ec-inv-adjust-bill-save'://库存调整
			break;
		default:
			break;
	};

	let barCodes = params.barCodes || getBarCodes(styleInfo);
	// barCodes = common.randomSort(barCodes);
	jsonParam.billDetails = Array(Number(count)).fill({}).map((detail, index) => {
		detail = {
			tenantSpuId: BASICDATA.styleidAgc001,
			colorid: barCodes[index].colorId,
			sizeid: barCodes[index].sizeId,
			price: styleInfo[priceKey],
			shopSpuId: LOGINDATA.invid,
			discount: 1,
			num: common.getRandomNum(1, 100),
		};
		detail.commitNum = detail.num;//提交数量*
		detail.money = common.mul(detail.num, detail.price);
		jsonParam.totalNum += detail.num;
		jsonParam.totalMoney += detail.money;
		return detail;
	});
	return jsonParam;
};

function getBarCodes(styleInfo) {
	const colorIds = styleInfo.colorids.split(','),
		sizeIds = styleInfo.sizeids.split(',');
	let barCodes = [];
	for (let index = 0; index < colorIds.length; index++) {
		const colorId = colorIds[index];
		sizeIds.forEach(sizeId => {
			barCodes.push({ styleid: styleInfo.pk, colorId, sizeId });
		});
	};
	return barCodes;
};