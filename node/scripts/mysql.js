'use strict';
const mysql = require('mysql');
const readline = require('readline');

let rl = readline.createInterface(process.stdin, process.stdout);

rl.setPrompt('请输入测试环境名称与帐套名称(以逗号分隔)> ');
rl.prompt();

rl.on('line', function (line) {
	let envObj = {};
	let [envName, name] = line.trim().split(',');
	switch(envName) {
	case 'adev1':
	case 'adev3':
		envObj = {
			host: '101.132.186.208',
			port: '3366',
		};
		getEpid(envObj, name);
		break;
	case 'app1':
		envObj = {
			host: '101.132.108.126',
			port: '3366',
		};
		getEpid(envObj, name);
		break;
	case 'close':
		rl.close();
		break;
	default:
		console.log(`未知环境=${envName}`);
		break;
	}
	rl.prompt();
});

rl.on('close', function () {
	console.log('结束!');
	process.exit(0);
});

//一代 http://c.hzdlsoft.com:7082/Wiki.jsp?page=Dev_db
//二代 http://c.hzdlsoft.com:7082/Wiki.jsp?page=Slh2_devtest
function getEpid(envObj, name) {
	const param = Object.assign(envObj, {
		user: 'myslh',
		password: 'CottonXu',
		database: 'slh_test'
	});

	const connection = mysql.createConnection(param);
	connection.connect();
	connection.query(`SELECT * FROM sc_accountset WHERE NAME='${name}' AND isdead='0'`, function (error, results, fields) {
		if(error) console.log(error);

		let epidArr = [];
		results.map((data) => epidArr.push(data.id));
		console.log(`epidArr = ${JSON.stringify(epidArr)}`);
	});
	connection.end();
};
