const moment = require('moment');
// const basicInfoReqHandler = require('../help/basicInfoHelp/basicInfoReqHandler');
const basicInfoReqHandler = require('../slh1/help/basicInfoHelp/basicInfoReqHandler');
const basiceJsonparam = require('../slh1/help/basiceJsonparam');
const common = require('../lib/common')
const getBasticData = require('../data/getBasicData')
const caps = require('../data/caps')
const custManager = require('../reqHandler/slh1/index');


/**
 * addStyle    根据环境批量新增货品
 * @param {string} envName  caps中配置的环境名称,不传递使用caps中默认账套
 * @param {number} n    需要添加货品的数量
 * @return {string} number:pk
 */

async function addStyles(n, envName) {
    if (envName) {
        await caps.updateEnvByName({
            name: envName
        });
    }
    await common.loginDo();
    await getBasticData.getBasicID();
    let i = 17466;
    try {
        while (i < n) {
            await common.delay(200);
            style = await basicInfoReqHandler.editStyle({
                jsonparam: basiceJsonparam.addGoodJson({ name: `A${i}`, code: `A${i}`, fileid: 173285937 })

            });
            i++;
            console.log(`${i}:${style.pk}`);
        }
    } catch (error) { console.log(error) }
}

// addStyles(20000, 'app1');


/**
 * 新增客户
 * @param {number} n
 * @param {string} envName
 */
async function addCustomers(n, envName) {
    if (envName) {
        await caps.updateEnvByName({
            name: envName
        });
    }
    await common.loginDo();
    await getBasticData.getBasicID();
    let i = 0;
    try {
        while (i < n) {
            await common.delay(200);
            style = await custManager.slh1CustomerManager.saveCustomerFull(basiceJsonparam.addCustJson());
            i++;
            console.log(`${i}:${style}`);
        }
    } catch (error) { console.log(error) }
}

// addCustomers(750, 'app1')


async function addPruchase(n, envName) {
    if (envName) {
        await caps.updateEnvByName({
            name: envName
        });
    }
    await common.loginDo();
    await getBasticData.getBasicID();

    let i = 356;
    try {
        while (i < n) {
            await common.delay(200);
            let style = await basicInfoReqHandler.editStyle({
                jsonparam: basiceJsonparam.addGoodJson({ name: `A${i}`, code: `A${i}`, fileid: 173285937 })
            });
            console.log(`${i}:${style.pk}`);

            let json = basiceJsonparam.purchaseJson();
            json.details.forEach(ele => {
                ele.styleid = style.pk;
                ele.matCode = `A${i}`;
            });
            let res = await common.editBilling(json);
            console.log(res.result.billno);
            i++;
        }
    } catch (error) { console.log(error) }
}
// addPruchase(10000, 'app1')



async function addSalesBill(n, envName) {
    if (envName) {
        await caps.updateEnvByName({
            name: envName
        });
    }
    await common.loginDo();
    await getBasticData.getBasicID();
    let i = 14;
    try {
        while (i < n) {
            await common.delay(200);
            let style = await custManager.slh1CustomerManager.saveCustomerFull(basiceJsonparam.addCustJson());
            console.log(`${i}:${style.result.val}`);

            let json = basiceJsonparam.salesJson();
            json.dwid = style.result.val;
            let res = await common.editBilling(json);
            console.log(`billno:${res.result.billno}`);
            i++;
        }
    } catch (error) { console.log(error) }
}

addSalesBill(2000, 'app1')