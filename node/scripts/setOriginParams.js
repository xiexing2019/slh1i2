const common = require('../lib/common');

/*
 * 设置帐套起始参数
 * 若是用例中修改为其他值,需要在结束时改回来
 */
(async () => {
	await common.loginDo();
	//颜色尺码模式
	await common.setGlobalParam('ignorecolorsize', 0);
	// 开单模式2 现金+刷卡+汇款+代收
	await common.setGlobalParam('paymethod', 2);

})();
