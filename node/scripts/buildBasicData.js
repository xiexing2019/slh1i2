'use strict';
const common = require('../lib/common');
const basicInfoReqHandler = require('../slh1/help/basicInfoHelp/basicInfoReqHandler');
const mainReqHandler = require('../slh1/help/basicInfoHelp/mainDataReqHandler');
const caps = require('../data/caps');

(async () => {
	// await caps.updateEnvByName({ name: 'yans1 ' });
	await common.loginDo();
	BASICDATA = {
		color: [],
		size: [],
		season: {},
		brand: {},
		styleClass: {},
		dep: {}, //门店
		customer: {},
		supplier: {},
		logistics: {},
		style: {},
		staff: {},
	};
	let seasonList = await common.callInterface('ql-1526', {});
	for (let value of seasonList.dataList) {
		BASICDATA.season[value.name] = value.sid;
	}
	//console.log(`BASICDATA['season'] : ${JSON.stringify(BASICDATA['season'])}`);
	let basicTypeId = {
		color: 601,
		size: 605,
	}; //颜色，尺码，季节
	let colorAndSize = {
		color: {},
		size: {}
	};
	let colorName = ['均色', '白色', '驼色', '杏色', '红色', '米色', '黑色',];
	let sizeName = ['均码', 'S', 'M', 'L', 'XL', '2XL', '3XL', '28', '29', '30', '31', '32'];
	for (let key of Object.keys(basicTypeId)) {
		let list = await basicInfoReqHandler.getDictList({
			typeid: basicTypeId[key]
		});
		colorAndSize[key] = list.result.dataList;
	};
	for (let i = 0; i < colorName.length; i++) {
		for (let j = 0; j < colorAndSize['color'].length; j++) {
			if (colorAndSize['color'][j].name == colorName[i]) {
				BASICDATA['color'].push(colorAndSize['color'][j].sid);
				continue;
			}
		};
	};
	for (let i = 0; i < sizeName.length; i++) {
		for (let j = 0; j < colorAndSize['size'].length; j++) {
			if (colorAndSize['size'][j].name == sizeName[i]) {
				BASICDATA['size'].push(colorAndSize['size'][j].sid);
				continue;
			};
		};
	};
	const mainData = {
		brand: [{
			name: 'Adidas',
			typeid: 606,
		}, {
			name: 'hu',
			typeid: 606,
		}],
		execstd: [{
			name: 'gb2312',
			typeid: 752,
		}, {
			name: 'gbk',
			typeid: 752,
		}, {
			name: 'utf-8',
			typeid: 752,
		}], //执行标准
		safetycat: [{
			name: 'A类',
			typeid: 753,
		}, {
			name: 'B类',
			typeid: 753,
		}, {
			name: 'C类',
			typeid: 753,
		}], //安全类别
		grade: [{
			name: '一级',
			typeid: 754
		}, {
			name: '二级',
			typeid: 754
		}, {
			name: '三级',
			typeid: 754
		}], //等级
		washingrem: [{
			name: '机洗',
			typeid: 755,
		}, {
			name: '干洗',
			typeid: 755,
		}, {
			name: '手洗',
			typeid: 755
		}], //洗涤说明
		styleClass: [{
			name: '登山服'
		}, {
			name: '鞋'
		}],
		priceGroup: [{
			name: 'one',
			typeid: 764,
			//check: true
		}, {
			name: 'two',
			typeid: 764,
			//check: true
		}],
		depGroup: [{
			name: '常青店',
			typeid: 10
		}, {
			name: '中洲店',
			typeid: 10
		}, {
			name: '仓库店',
			typeid: 10
		}, {
			name: '文一店',
			typeid: 10
		}],
		customer: [{
			nameshort: '小王',
			vipcode: 'xw001',
		}, {
			nameshort: '李四',
			pricetype: 2, //适用价格
			discount: 0.76
		}],
		supplier: [{
			name: 'Vell',
		}, {
			name: 'Rt',
		}],
		logistics: [{
			nameshort: '顺丰快递'
		}, {
			nameshort: '圆通快递'
		}],
		style: [{
			code: 'agc001',
			name: 'auto001',
			colorids: `${BASICDATA['color'].slice(0, 5)}`,
			sizeids: `${BASICDATA['size'].slice(0, 7)}`,
			purprice: 100, //采购价
			stdprice1: 200, //销售价1
			stdprice2: 180,
			stdprice3: 160,
			stdprice4: 140,
			isallowreturn: 1, //是否允许退货 是
			discount: 1,
		}, {
			code: 'agc002',
			name: '加工款a',
			colorids: `${BASICDATA['color'].slice(0, 5)}`,
			sizeids: `${BASICDATA['size'].slice(0, 7)}`,
			purprice: 100, //采购价
			stdprice1: 170, //销售价1
			stdprice2: 180,
			stdprice3: 190,
			stdprice4: 200,
			processunitprice: 50,
			isallowreturn: 1, //是否允许退货 是
			discount: 1,
		}, {
			code: '3035',
			name: 'jkk',
			colorids: `${BASICDATA['color'][0]}`,
			sizeids: `${BASICDATA['size'][0]}`,
			purprice: 100, //采购价
			stdprice1: 200, //销售价1
			stdprice2: 180,
			stdprice3: 160,
			stdprice4: 140,
			isallowreturn: 1, //是否允许退货 是
			discount: 1,
		}],
		specialStyle: [{
			code: 66666,
			name: '积分抵现',
			typeid: 9, //必填
			negative: 1 //是否为负值，1为是
		}]
	};
	await basicInfoReqHandler.maintainData(mainData);

})();
