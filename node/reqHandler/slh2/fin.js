'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const sspdAccount = require('../../shopDiary/help/sspdAccount');

let fin = module.exports = {};

/**
 * ec-fin-acct-list-by-tenantReal 租户的实体账户列表
 * @param {Object} params 
 * @return {Object} {params:请求参数,result:{code,data:{total,count,sum,rows},msg}}
 */
fin.getAccountListByTenantReal = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        // name: "账户名",
        // traderId: "属主；不填则为当前登录用户所属的租户组织机构id",
        shopId: LOGINDATA.shopId,//门店id:不传默认是当前登录用户的门店；传0为总店
        flag: 1,//状态:-1已删除、0禁用、1正常
        // side: "会计等式的哪侧：0左侧、1右侧",
        // acctNo: "账号：银行卡号/第三方账号。acctType=13/14时必填",
        // nameAbbr: "账户名缩写",
        // purpose: "用途：1销售/2采购/4其他。acctType=13/14时必填",
        // chanId: "渠道id：渠道名对应的id。acctType=13/14时必填",
        // defaultFlag: "缺省状态：0非缺省，1缺省"
    }, params));
    const res = await common.apiDo({ apiKey: 'ec-fin-acct-list-by-tenantReal', ...params });
    if (res.result.data.rows.length < 1) {
        throw new Error(`${res.params.apiKey},租户的实体账户列表查询无数据`);
    };
    return res;
};

/**
 * 账户新增
 */
fin.saveAccount = async function (params) {
    // params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-fin-simple-acct-save', ...params });
};

/**
 *  账户流水
 *
 * @param  {object} params 
 * @param  {string} params.id
 * @return {object}         
 */
fin.getAccountFlowList = async function (params) {
    params = Object.assign({
        shopId: LOGINDATA.shopId,
        // acctId: params.id,
        proDateGte: common.getCurrentDate(),
        proDateLte: common.getCurrentDate(),
        traderCap: 1024
    }, params);
    return common.apiDo({
        apiKey: 'ec-fin-acct-flow-list-report',
        jsonParam: params,
        pageSize: 0,
        wrapper: true,
        orderBy: 'proDate desc,id desc'
    });
};

/**
 * 账户详情
 */
fin.getAccountInfo = async function (params) {
    return common.apiDo({
        apiKey: 'ec-fin-acct-getById',
        id: params.id,
        wrapper: true,
    })
};

/**
 * 账户列表
 * @param {object} jsonParam
 * @param {boolean} flag 0:,1:
 * 
 */
fin.getAccountList = async function (params = {}) {
    params = Object.assign({
        pageSize: 0,
        orderBy: 'showOrder',
        wrapper: true,
        traderCap: 1024,//交易方能力位，固定传1024
    }, sspdAccount.defParams, params);
    return fin.getAccountListByTenantReal(params);
};

/**
 * 账户停用
 */
fin.disableAccount = async function (params) {
    return common.apiDo({
        apiKey: 'ec-fin-acct-disable',
        id: params.id,
    });
};

/**
 * 账户启用
 */
fin.enableAccount = async function (params) {
    return common.apiDo({
        apiKey: 'ec-fin-acct-enable',
        id: params.id,
    });
};

/**
 * 设置/取消默认账户
 * @param  {object} params
 * @param {string} params.id 账户id
 * @param {string} params.defaultFlag 1:设置为默认；0:取消默认
 */
fin.setDefaultAccount = async function (params) {
    // params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-fin-acct-update-default', ...params });
};