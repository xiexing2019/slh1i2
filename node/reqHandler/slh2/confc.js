'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const slh2Caps = require('../slh2/slh2Caps');

let confc = module.exports = {
    url: slh2Caps.confcUrl
};

/**
 * 获取产品信息
 * @param {Object} params
 */
confc.getServerURLByDevice = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-config-ugr-tenant-device-getServerURLByDevice', ...params });
}