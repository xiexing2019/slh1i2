'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let bizhelper = module.exports = {};


/**
 * 根据手机号查询验证码
 * @param {Object} params
 * @param {string} params.sessionKey 手机号
 */
bizhelper.getVerifyCode = async function (params) {
    return common.apiDo({ apiKey: 'ec-bizhelper-captcha-getVerifyCode', ...params });
};