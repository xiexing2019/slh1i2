'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let inv = module.exports = {};

/**
 * 撤销调出单(未入库)
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}        
 */
inv.cancelMoveOutBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-out-bill-cancel', ...params });
};

/**
 * 撤销调入单(未入库)
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}        
 */
inv.cancelPreinMoveBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-prein-bill-cancel', ...params });
};

/**
 * 撤销调入单(已入库)
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}        
 */
inv.cancelMoveInBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-in-bill-cancel', ...params });
};

/**
 * 调拨单列表
 * @param  {object} params
 * @param  {string} params.inOutType 调入调出类型* 1调拨出库 2调拨入库
 */
inv.getMoveBillList = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        inOutType: params.inOutType || 1,
        flagIn: "1,2",
        //wrapper: true,
        shopId: LOGINDATA.shopId,
        bizDateS: common.getCurrentDate(),
        bizDateE: common.getCurrentDate(),
        searchToken: "",
        orderBy: "billNo desc"
    }, params));
    return common.apiDo({ apiKey: 'ec-inv-move-bill-list', ...params });
};

/**
 * 接收调入单
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}
 */
inv.saveMoveInBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-in-bill-receive', ...params });
};

/**
 * 调拨单详情
 *
 * @param  {object} params
 * @param  {object} params.wrapper
 * @param  {object} params.jsonParam
 * @return {object}        
 */
inv.getMoveBillFull = async function (params) {
    params = format.packJsonParam(params);
    params.wrapper = true;
    return common.apiDo({
        apiKey: 'ec-inv-move-out-bill-getBeanById',
        ...params
    });
};

/**
 * 保存调出单
 */
inv.saveMoveOutBill = async function (params) {
    params = format.packJsonParam(params);
    //params.jsonParam.hashKey = `ShopDiary-${LOGINDATA.deviceNo}-${Date.now()}-${common.getRandomNum(100, 999)}`;
    const res = await common.apiDo({
        apiKey: 'ec-inv-move-out-bill-save',
        ...params
    });
    await common.delay(500);
    return res;
};

/**
 * 跨门店查看库存
 * @param {Object} params
 */
inv.getStockNumByOverStore = async function (params = {}) {
    Object.assign({ tenantSpuId: 277021, colorId: 2, size: 1, orderByDesc: true, wrapper: true }, params);
    return common.apiDo({ apiKey: 'ec-inv-sku-realTime-getStockNumByOverStore', ...params });
};

/**
 * 获取款号实时库存
 * @description  "jsonParam" : [18609]
 * @param {string} styleId 款号id 逗号分隔
 * @return {Object} {params,result:}
 */
inv.getInvSkuByStyleId = async function (params) {
    return common.apiDo({
        apiKey: 'ec-inv-sku-realTime-findWithInvKey',
        jsonParam: [params]
    });
};

/**
 * 新增期初入库单
 * @param {object} params 
 */
inv.saveInitBill = async function (params) {
    return common.apiDo({
        apiKey: 'ec-inv-init-bill-save',
        jsonParam: params,
        wrapper: true,
    });
};

/**
 * @param {object} params
 * @param {object} params.id 单据id
 * 
 */
inv.getInitBillInfoById = async function (params) {
    return common.apiDo({
        apiKey: 'ec-inv-init-bill-getBeanById',
        wrapper: true,
        jsonParam: params,
    });
};

/**
 * 期初入库列表查询
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
inv.getInitBillList = async function (params = {}) {
    let qlParams = Object.assign({
        wrapper: true,
        pageSize: 20,
        pageNo: 1,
    }, params);
    qlParams.jsonParam = Object.assign({
        bizDateS: common.getCurrentDate(),
        bizDateE: common.getCurrentDate(),
        // flag: 1,
        invId: LOGINDATA.invid,
        orderBy: 'bizDate desc,billNo desc',
    }, params.jsonParam);
    // console.log(`qlParams=${JSON.stringify(qlParams)}`);
    return common.apiDo({
        apiKey: 'ec-inv-init-bill-list',
        ...qlParams
    });
};

/**
 * 作废期初入库单
 * @param {object} params
 * @param {object} params.id 单据id 
 */
inv.disableInitBill = async function (params) {
    return common.apiDo({
        apiKey: 'ec-inv-init-bill-disable',
        wrapper: true,
        jsonParam: params,
    });
};



// 盘点

/**
 * 保存盘点计划
 * @param params
 * @return {object}
 */
inv.saveCheckPlan = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-check-plan-create', ...params });
};

/**
 * 撤销盘点计划
 * @param params
 * @return {object}
 */
inv.deleteCheckPlan = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-check-plan-delete', ...params });
};

/**
 * 处理盘点计划
 * @param params
 * @return {object}
 */
inv.execCheckPlan = async function (params) {
    params.bizDate = common.getCurrentDate();
    return common.apiDo({ apiKey: 'ec-inv-check-bill-execCheckPlan', ...params });
};

/**
 * 盘点计划详情
 * @param params
 * @return {object}
 */
inv.getCheckPlanDetail = async function (params) {
    params.pageNo = 1;
    params.pageSize = 15;
    return common.apiDo({ apiKey: 'ec-inv-check-plan-detail', ...params });
};


/**
 * 盘点计划列表
 *
 * @param  {object} params "searchToken": "关键字 名称、款号、条码、备注",
 * @return {object}              
 */
inv.getCheckPlanList = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        shopId: LOGINDATA.shopId,
        wrapper: true,
        //flag: 2,//1未处理 2已处理
        //hasDiff: 1,//1有盈亏/2无盈亏
        proDateGte: common.getCurrentDate(),
        proDateLte: common.getCurrentDate(),
        searchKey: ''//输单号、货品名称、款号、备注
    }, params));
    return common.apiDo({
        pageNo: 1,
        pageSize: 15,
        apiKey: 'ec-inv-check-plan-listForSspd',
        ...params
    });
};

/**
 * 作废盘点单
 * @param params
 * @return {object}
 */
inv.deleteCheckBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-check-bill-delete', ...params });
};

/**
 * 保存盘点单
 * @param params
 * @return {object}
 */
inv.saveInvCheck = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-check-bill-save', jsonParam: params });
};

/**
 * 盘点单详情
 *
 * @param  {object} params
 * @return {object}        
 */
inv.getCheckFull = async function (params) {
    return common.apiDo({
        apiKey: 'ec-inv-check-bill-getBeanById',
        ...params
    });
};

/**
 * 期初入库单按明细查询 默认查当天数据
 * @param {object} params
 * @param {object} params.jsonParam
 * @description jsonParam tenantSpuId,invId,deliver,colorId,sizeId,bizDateGte,bizDateLte
 * @return {object} {params,result}
 */
inv.getInitBillDetList = async function (params = {}) {
    let qlParams = Object.assign({
        wrapper: true,
        pageSize: 20,
        pageNo: 1,
    }, params);
    qlParams.jsonParam = Object.assign({
        bizDateGte: common.getCurrentDate(),//起始日期
        bizDateLte: common.getCurrentDate(),
    }, params.jsonParam);

    return common.apiDo({
        apiKey: 'ec-inv-init-detail-list',
        ...qlParams,
    })
}

/**
 * 库存流水
 * @param {object} params
 * @param {object} params.tenantSpuId
 * @param {object} params.bizDateStart
 */
inv.getInvFlowList = async function (params) {
    params = Object.assign({
        shopId: LOGINDATA.shopId,
        pageNo: 1,
        pageSize: 0,
        //bizDateStart: common.getCurrentDate(),
        bizDateEnd: common.getCurrentDate()
    }, params);
    return common.apiDo({ apiKey: 'ec-inv-spu-flow-list', ...params });
}

/**
 * 获取商品进销存数量
 * @param {object} params
 * @param {object} params.tenantSpuId 商品款号id
 */
inv.findInvoicingNum = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-spu-flow-findInvoicingNum', ...params });
};

/**
 * 查询款号的库存数，调拨在途数，订货数
 * @param {object} params
 * @param {object} params.tenantSpuId 商品款号id
 */
inv.getInvTransitOrderNum = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-sku-realTime-queryList', ...params });
};

/**
 * 保存库存调整单
 * @param {object} params
 */
inv.saveInvAdjustBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-adjust-bill-save', ...params });
};