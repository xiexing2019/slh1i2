'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let acct = module.exports = {};


/**
 * ec-acct-get-balanceAndScore 获取积分和余额
 * @param {Object} params
 * @param {number} params.traderKind 组织类型，1：组织机构，2：个人
 * @param {number} params.traderCap 能力位* traderKind = 1时 1: 客户, 2: 供应商 traderKind = 1时 2: 顾客(会员)
 * @param {string} params.traderId 交易人*
 * @param {string} params.shopId 门店
 */
acct.getBalanceAndScore = async function (params) {
    params.shopId = params.shopId || LOGINDATA.shopId;
    params.traderKind = params.traderKind || 2;
    params.traderCap = params.traderKind == 2 ? 2 : params.traderCap;
    return common.apiDo({
        apiKey: 'ec-acct-get-balanceAndScore',
        ...params
    });
};