'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let config = module.exports = {};



/**
 * 检查当前企业在线支付开通状态
 */
config.checkOnlinePayChannel = async function () {
	return common.apiDo({ apiKey: 'ec-config-ugr-tenant-checkOnlinePayChannel' });
};

config.getDictListWithDresNum = async function (params) {
    params = Object.assign({ pageSize: 0, wrapper: true, orderBy: 'updatedDate', orderByDesc: true }, format.packJsonParam(params));
    return common.apiDo({ apiKey: 'ec-slh2-conf-dictListWithDresNum', ...params });
};

/**
 * 获取当前登录用户可访问功能
 */
config.getUserFuncs = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-priv-userFuncs', ...params });
};

/**
 * syncUnitId - 更新udid
 * @description 根据手机号获取可访问的单元id；对于笑铺来说通常就一个单元
 * @param {Object} deviceNo 对于笑铺而言为手机号
 * @param {Object} unitName 对于笑铺而言为账套名称
 */
config.findDeviceUnit = async function (params) {
    params = format.packJsonParam(Object.assign({ bdomainCode: 'slh' }, params), ['productCode', 'productVersion']);
    return common.apiDo({ apiKey: 'ec-config-ugr-unit-findDeviceUnit', ...params });
};


/**
 * ec-config-list-dict 获取字典类列表
 * @param {object} params 
 * @param {string} params.typeId 字典类型*
 * @param {string} params.unitId 单元id，也叫账套id
 */
config.getDictList = async function (params) {
    params = Object.assign({
        pageNo: 1,
        pageSize: 0,
    }, params);
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-list-dict', ...params });
};

/**
 * 条码打印配置保存
 * @param {object} params 
 */
config.savePrintBarcodeConfig = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-print-barcode-save', ...params });
};

/**
 * 获取条码打印配置
 * @param {object} params 
 * @param {string} params.shopId 门店id
 */
config.getPrintBarcodeConfig = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-getPrintBarcodeConfig', ...params });
};

/**
 * 获取条码打印配置列表
 */
config.getPrintBarcodeList = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-List', ...params });
};

//系统参数列表

/**
 * 系统参数列表
 * @param {object} params 
 * @param {string} params.domainKind 配置域* business 业务参数 system 系统参数
 * @param {string} params.bdomainBits 业务域。1 商陆花saas，2 商陆花平台
 * @param {string} params.nameOrCode 名称或编码模糊匹配
 */
config.getParamList = async function (params) {
    params = Object.assign({ orderBy: 'showOrder', orderByDesc: false }, params);
    return common.post(`/confc/api.do`, { apiKey: 'ec-config-param-list', ...params });
};

/**
 * 修改参数基本信息
 * @description sp需要使用中心区会话
 * @param {object} params 
 * @param {string} params.id *
 * @param {string} params.name 参数名称
 * @param {string} params.val 参数值
 */
config.updateParamInfo = async function (params) {
    return common.post(`/confc/api.do`, { apiKey: 'ec-config-param-updateBaseInfo', ...params });
};

/**
 * 字典类新增
 * @param {object} params
 * @param {object} params.jsonParam
 */
config.saveDict = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-save-dict', ...params, });
};


/**
 * 品牌，材质，风格详情
 * @param {object} params
 * @param {object} params.id
 * @param {object} params.typeId
 */
config.getDictInfo = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-getById-dict', ...params });
};

/**
 * 停用字典
 * @param {object} params
 * @param {object} params.id
 */
config.disableDict = async function (params) {
    return common.apiDo({
        apiKey: 'ec-config-disable-dict', ...params
    });
};

/**
 * 启用字典
 * @param {object} params
 * @param {object} params.id
 */
config.enableDict = async function (params) {
    return common.apiDo({
        apiKey: 'ec-config-enable-dict', ...params
    });
};

/**
 * 保存打印标签配置
 * @param {object} params 
 * @param {string} params.shopId 门店id
 * @param {string} params.id 打印配置记录id   //传id是修改
 */
config.savePrintBarcodeConfig = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-print-barcode-save', ...params });
};

/**
 * 获取标签打印配置
 * @param {object} params 
 * @param {long} params.shopId 门店id
 * @param {long} params.id 打印配置记录id
 */
config.getPrintBarcodeConfig = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-print-barcode-getPrintBarcodeConfig', ...params });
};

/**
 * 获取本门店已设置的模板列表
 * @param {object} params 
 * @param {long} params.shopId 门店id
 * @param {string} params.nameLike 名称模糊查询
 * @param {integer} params.flag 状态，1：有效，0：停用
 */
config.getPrintBarcodeTempList = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-config-print-barcode-findConfigNames', ...params });
};

/**
 * 停用标签打印模板
 * @param {String} params.id 打印配置记录id
 */
config.disablePrintTemp = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-disable', ...params });
};

/**
 * 启用标签打印模板
 * @param {String} params.id 打印配置记录id
 */
config.enablePrintTemp = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-enable', ...params });
};

/**
 * 设置打印标签模板为默认值
 * @param {String} params.id 打印配置记录id
 */
config.setDefaultPrintTemp = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-setDefault', ...params });
};

/**
 * 删除标签打印模板
 * @param {String} params.id 打印配置记录id
 */
config.deletePrintTemp = async function (params) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-delete', ...params });
};

/**
 * 获取条码打印配置列表
 */
config.getPrintBarcodeList = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-config-print-barcode-List', ...params });
};

/**
 * 行业列表
 * @param {object} params jsonParam
 * @param {string} params.codeId 父级id 第一级传0 之后的传上一级的code
 */
config.sysDictTree = async function (params) {
    params.hierId = 2011; //字典定义id(2011写死)
    params.includeSubs = true;
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-config-sysDictTree',
        ...params
    });
};

/**
 * 初始化行业
 * @param {object} params jsonParam
 * @param {string} params.industryId 行业id
 */
config.transUpdateIndustry = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-config-ugr-unit-transUpdateIndustry',
        ...params
    });
};


/**
 * 可批量获取参数，也可获取一个参数，批量获取就要jsonParam
 * @description
 * @param {object} params
 * @param {object} params.domainKind 配置域 系统:system,业务:business,移动:frontEnd,个人:personal,角色:role
 */
config.getParamValues = async function (params = {}) {
    params = Object.assign({
        ownerKind: params.ownerKind || 7,
    }, params);
    return common.apiDo({ apiKey: 'ec-config-param-getParamVals', ...params });
};

/**
 * 保存参数
 * @description data: [{code:"" ,domainKind: "business",ownerId: LOGINDATA.unitId,val: "1"}]
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
config.saveParamValue = async function (params) {
    params = format.packJsonParam(Object.assign({ ownerKind: 9 }, params));
    return common.apiDo({ apiKey: 'ec-config-param-saveOwnerVal', ...params });
};

/**
 * 获取系统参数
 * @description {"code":"favor_money_ceil","domainKind":"business","ownerKind":"7","ownerId":23093}
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
config.getParamInfo = async function (params) {
    params = format.packJsonParam(Object.assign({ ownerKind: 7 }, params));
    return common.apiDo({ apiKey: 'ec-config-param-getParamInfo', ...params });
};

/**
 * 获取设备参数
 * @description{"ownerKind":"7","data":[{"code":"invinout_checknum","domainKind":"business","ownerId":22157,"val":"1"}]}
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
config.findProductParams = async function (params) {
    params = format.packJsonParam(params);
    params.productType = 'slh';
    params.pageSize = 20;
    params.pageNo = 1;
    params.wrapper = true;
    return common.apiDo({ apiKey: 'ec-config-param-findProductParams', ...params });
};