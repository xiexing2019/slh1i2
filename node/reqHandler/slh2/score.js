"use strict";
const caps = require('../../data/caps');
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');
let score = module.exports = {};

/**
 * 积分明细
 * @param {Object} params 
 * @param {Object} params.shopId 门店ID
 * @param {Object} params.traderId 客户ID
 */
score.getCustScoreDetail = async function (params) {
    params = Object.assign({
        wrapper: true,
        traderKind: 2, //交易方类型, 固定为2
        pageSize: 0,
        shopId: LOGINDATA.shopId,
        flag: 1,
        proDateGte: common.getDateString([0, 0, -1]),
        proDateLte: common.getCurrentDate(),
        orderBy: "proDate desc,createdDate desc,billNo desc"
    }, params);
    return common.apiDo({
        apiKey: 'ec-score-scoreAcctFlow-listByFlowCust',
        ...params
    });
};

/**
 * 积分规则保存
 */
score.saveScoreRule = async function (params) {
    return common.apiDo({
        apiKey: 'ec-score-rule-scoreRule-saveRuleFromMeta',
        jsonParam: {
            domainKind: params.domainKind || 21, //积分规则类型：21(按客户消费)/23(按客户充值)
            money: params.money || 10, //表示M钱产生N积分的M金额；缺省为1
            score: params.score || 1, //表示M钱产生N积分的N积分
        },
    });
};

/**
 * 获取积分规则 无参数
 */
score.getScoreRule = async function () {
    return common.apiDo({ apiKey: 'ec-score-rule-scoreRule-getRuleToMeta' });
};

/**
 * 积分调整
 */
score.saveScoreAdjustBill = async function (params) {
    return common.apiDo({
        apiKey: 'ec-score-scoreAdjustBill-save',
        jsonParam: {
            traderKind: 2, //固定组织结构
            ...params
        }
    })
}