'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let reqHandler = module.exports = {};

/**
 * 新增款号
 * @param {object} params
 * @param {object} params.jsonParam
 */
reqHandler.saveStyle = async function (params, directInv = false) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-dres-spu-save',
		directInv,//建款直接入库传true,建款需要入账传false,不传服务端默认false
		...params
	});
};


/**
 * 款号详情
 * @param {object} params
 * @param {object} params.id 租户spu编号
 */
reqHandler.getStyleInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-dres-spu-getBeanById', wrapper: true, ...params });
};


/**
 * 款号搜索
 */
reqHandler.getStyleInfoList = async function (params) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, {
		//orderByDesc: true,
		//needPurPrice: true,
		needStockNum: true,
		wrapper: true,
		shopId: LOGINDATA.shopId,
		flag: 1, //默认为有效款号
		//marketDateStart: common.getCurrentDate(),
		//marketDateEnd: common.getCurrentDate(),
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-dres-spu-list', ...params });
};

/**
 * 款号停用
 * @param {object} params
 * @param {object} params.ids 租户款号编号列表，多个时以”,”分割
 */
reqHandler.disableStyle = async function (params) {
	params = Object.assign({ shopId: LOGINDATA.shopId, }, params);
	return common.apiDo({ apiKey: 'ec-dres-spu-disable', ...params });
};

/**
 * 款号启用
 * @param {object} params
 * @param {object} params.ids 租户款号编号列表，多个时以”,”分割
 */
reqHandler.enableStyle = async function (params) {
	params = Object.assign({ shopId: LOGINDATA.shopId, }, params);
	return common.apiDo({ apiKey: 'ec-dres-spu-enable', ...params });
};

/**
 * 新增类别
 * @param {object} params
 * @param {object} params.jsonParam
 * @param {object} params.jsonParam.name
 * @param {object} params.jsonParam.parentId 上级分类id
 * @param {object} params.jsonParam.rem
 */
reqHandler.saveClass = async function (params = {}) {
	params = format.packJsonParam(Object.assign({
		name: '类别' + common.getRandomStr(5),
		rem: '类别备注' + common.getRandomStr(10),
	}, params));
	return common.apiDo({
		apiKey: 'ec-dres-class-save', ...params
	});
};

/**
 * 类别详情
 * @param {object} params
 * @param {object} params.id
 *
 */
reqHandler.getClassInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-getById',
		wrapper: true,
		...params
	});
};


/**
 * 类别列表
 * @param {object} params
 * @param {object} params.jsonParam 默认显示启用的类别，显示停用的类别jsonParam 传空
 *
 */
reqHandler.getClassList = async function (params = {}) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-dres-queryClassTree',
		wrapper: true,
		includeSubs: true,
		...params
	});
};


/**
 * 停用类别
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.disableClass = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-disable',
		...params
	})
};

/**
 * 启用类别
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.enableClass = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-enable',
		...params
	})
};



/**
 * 获取款号自动停用参数
 * 
 */
reqHandler.getStyleAutoStop = async function () {
	return common.apiDo({ apiKey: 'ec-dres-autoStop-get' });
};

/**
 * 保存款号自动停用参数
 * @param {object} params
 * @param {object} params.flag
 * @param {object} params.marketDay 上架天数且库存为0
 * @param {object} params.stockZeroDay 库存无变动
 *  @param {object} params.rem 备注信息
 */
reqHandler.updataStyleAutoStopParam = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-dres-autoStop-save', ...params })
}


/**
 * 获取条码自动生成参数
 * 
 */
reqHandler.getScodeAutoSet = async function () {
	return common.apiDo({ apiKey: 'ec-dres-fetchSimpleRuleSetConfig' });
};

/**
 * 保存条码自动生成参数
 * @param {object} params
 * @param {object} params.styleUseBarcode 1 --是否启用 0关闭 1启用
 */
reqHandler.updataScodeAutoSet = async function (params) {
	params = format.packJsonParam(Object.assign({ unitId: LIGINDATA.unitId }, params));
	return common.apiDo({ apiKey: 'ec-dres-saveRuleSetConfig', ...params });
};

/**
 * 批量修改货品属性
 * @param {object} params
 * @param {object} params.tenantSpuIds
 */
reqHandler.batchUpdateProp = async function (params) {
	params = Object.assign({
		val: params.val || 1,     //special仅能输入0 or 1,discount:支持小数
		tenantSpuIds: params.tenantSpuIds,
		propName: params.propName || 'special'   //目前支持discount:折扣,special:是否特价
	}, params);
	return common.apiDo({ apiKey: 'ec-dres-branchSpu-batchUpdateProp', ...params });
}

/**
 * 批量调价
 * @param {object} params
 */
reqHandler.adjustPriceInBatch = async function (params) {
	params = format.packJsonParam(Object.assign({
		stdprice1: params.stdprice1 || "300",
		stdprice2: params.stdprice2 || "300",
		stdprice3: params.stdprice3 || "300",
		stdprice4: params.stdprice4 || "300",
		stdprice5: params.stdprice5 || "300",
		spuIds: params.spuIds || "",
		typeid: params.typeid || 0,   //0统一价格，1批量加，2批量乘
		rem: params.rem || "批量调价",
	}, params));
	return common.apiDo({ apiKey: 'ec-dres-spu-adjustPriceInBatch-save', ...params });
}

/**
 * 作废批量调价记录
 * @param {object} params
 */
reqHandler.adjustPriceRevoke = async function (params) {
	params = format.packJsonParam(Object.assign({
		id: params.id
	}, params));
	return common.apiDo({ apiKey: 'ec-dres-spu-adjustPriceInBatch-revoke', ...params });
}

/**
 * 批量调价记录列表
 * @param {object} params
 */
reqHandler.adjustPriceInBatchList = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-dres-spu-adjustPriceInBatch-list2',
		pageNo: params.pageNo || 1,
		pageSize: params.pageSize || 20,
		wrapper: true,
		...params
	});
};

/**
 * 查询特殊货品列表
 * @param {object} params
 * @param {object} params.codeIn 特殊货品类型
 */
reqHandler.getSpeicalSpuList = async function (params) {
	return common.apiDo({ apiKey: 'ec-dres-speicalSpu-list', ...params });
};

/**
 * 保存特殊货品
 * @param {object} params
 * /*"id":"修改必填，获取特殊货品时返回的id",
    "code": "66666",
    "name": "积分抵扣",
    "negative": "1, 是否负值",
    "profitcount": "是否核算到利润，1:是，0：否",
    "scoreCount": "是否核算到积分，1：是，0：否， 当设置为1是，抵扣掉的金额不产生积分，反之产生"*/
reqHandler.saveSpeicalSpu = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-dres-speicalSpu-save', ...params });
};