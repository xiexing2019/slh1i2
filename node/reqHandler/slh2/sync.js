'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let sync = module.exports = {};


/**
 * 数据同步
 * @param {Object} params
 * @param {string} params.lastSyncTime 上次同步时间戳，结构为yyyy-MM-dd HH:mm:ss
 * @param {string} params.type 同步哪个表数据,见下面字典
 */
sync.syncData = async function (params) {
    params = Object.assign({ pageNo: 1, pageSize: 2000 }, params);
    return common.apiDo({ apiKey: 'ec-syncdata', ...params });
};