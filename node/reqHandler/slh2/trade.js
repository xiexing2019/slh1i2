'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');
let trade = module.exports = {};


/**
 * 检测销售或采购单明细补货标志
 *
 * @param  {object} params
 * @return {object}              
 */
trade.findRepFlag = async function (params = {}) {
	params = format.packJsonParam(Object.assign({
		compId: params.compId || 894049,
		details: [{
			tenantSpuId: params.tenantSpuId || 301669,
			sizeId: params.sizeId || 2,
			colorId: params.colorId || 2,
			num: params.num || 1
		}]
	}, params));
	return common.apiDo({
		bizType: params.bizType || 1100,
		isPend: params.isPend || 0,
		apiKey: 'ec-trade-common-findRepFlag',
		...params
	});
};

/**
 * 查询列表接口的params
 */
const qlDefParams = {
	orderBy: 'proDate,billNo',
	orderByDesc: true,
	pageNo: 1,
	pageSize: 15,
	//shopId: LOGINDATA.shopId,
	wrapper: true,
	// flag: 1,
	proDateGte: moment().format('YYYY-MM-DD'),
	proDateLte: moment().format('YYYY-MM-DD'),
};

/**
 * ec-trade-pendBill-findPendBill 查询挂单列表
 * @description typeId 销售单：1100 销售订单：1200 采购单：1300 采购订单：1400 销售发货单：1500 销售订单配货单：1600 采购收货单：1700 期初入库单：3100 其他入库单：3200 其他出库单：3300 调整出入库单：3400 盘点单：3500 盘点计划：3600 调拨入库单：3700 要货单：3900
 * @param {Object} params
 * @param {string} params.jsonParam
 * @param {string} params.jsonParam.typeId 单据类型
 */
trade.getPendBillList = async function (params = {}) {
	return common.apiDo({ apiKey: 'ec-trade-pendBill-findPendBill', ...params });
};


/**
 *  获取扫码入库数据
 *
 * @param  {object} params
 *  @param  {object} params.jsonParam
 * @return {object} {params,result}
 */
trade.getDataFromScanCode = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-trade-purBill-preViewFromScanCode', ...params });
};


/**
 * 扫码入库保存
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object} {params,result}
 */
trade.savePurBillFromScanCode = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-trade-purBill-saveBillFromScanCode', ...params });
};

// 进货

/**
 * ec-trade-purBill-saveBill 保存进货单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
trade.savePurchaseBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-purBill-saveBill', ...params });
};

/**
 * ec-trade-purBill-getBillFull 查询采购单完整信息
 * @param {object} params
 * @param {number} params.isPend 是否挂单单据(1 是/0 否。指定要作废的单据是否处于挂单状态)*
 * @param {string} params.id 采购单主表ID或挂单ID（isPend=0时，设置为采购单主表ID。isPend=1时，设置为挂单ID）。不传时，返回新增单据时的默认值。
 */
trade.getPurchaseBillFull = async function (params) {
	if (!params.hasOwnProperty('isPend')) params.isPend = 0;
	params.wrapper = true;
	return common.apiDo({ apiKey: 'ec-trade-purBill-getBillFull', ...params });
};

/**
 * 进货单列表查询
 * @param  {object} params = {} 
 * @return {object}             
 */
trade.getPurchaseBillList = async function (params = {}) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, qlDefParams, {
		orderBy: 'proDate,billNo'
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-trade-purBill-findBillWithPayWay', ...params });
};

/**
 * ec-trade-purBill-deleteBill 作废采购单
 * @param {Object} params
 */
trade.deletePurchaseBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-purBill-deleteBill', ...params });
};

/**
 * 供应商对账单
 */
trade.getSuppSimpleAcctCheck = async function (params) {
	let qlParams = Object.assign({
		orderBy: 'createdDate',
		orderByDesc: true,
		// pageNo: 1,
		// pageSize: 15,
		// shopId: params.shopId,
		wrapper: true,
		// compId: params.compId,
		proDateGte: common.getDateString([0, 0, -1]),
		proDateLte: common.getCurrentDate(),
		jsonParam: {}
	}, params);
	return common.apiDo({
		apiKey: 'ec-trade-accCheck-findSuppSimpleAcctCheck',
		...qlParams
	});
};


/**
 * 付款
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}
 */
trade.savePurOutBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-save', ...params });
};


/**
 * 付款单明细
 * @param  {object} params
 * @param  {object} params.id 收款单id
 */
trade.getPurOutBillInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-getById', ...params });
};

/**
 *  付款列表
 * @param  {object} params
 * @param  {object} params.compId
 * @return {object}
 */
trade.getPurOutBillList = async function (params) {
	params = format.packJsonParam(Object.assign({
		// flag: 1,
		wrapper: true,
		shopId: LOGINDATA.shopId,
		orderBy: "proDate desc,id desc",
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
	}, params));
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-list', ...params });
};

/**
 * 作废付款单
 * @param  {object} params
 * @param  {object} params.id
 */
trade.deletePurOutBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-delete', ...params });
};