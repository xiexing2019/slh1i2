'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const sspdAccount = require('../../shopDiary/help/sspdAccount');
const caps = require('../../data/caps');

let mdm = module.exports = {};



/**
 * 已授权的员工列表
 * @description 获取当前租户下所有已授权员工列表，按门店(账套)分组
 */
mdm.getAuthorizedStaffList = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-staff-authorizedStaffListForSspd', ...params });
};


/**
 * 判断一个session是否有效
 * @param {Object} params
 * @param {string} params.sessionId
 */
mdm.isSessionValid = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-session-valid', ...params });
};

/**
 * sendCode 发送验证码
 * @param {Object} params
 * @param {string} params.mobile 手机号
 */
mdm.sendCode = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-staff-sendCode', ...params });
};

/**
 * mdmUserLogin - 笑铺后台登录
 * @description  笑铺后台登录
 * @param {Object} params
 */
mdm.userLogin = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-user-login', ...params });
};

/**
 * sspdLogin - 店员登录
 * @description 笑铺员工根据手机号和验证码登录
 * @param {Object} params
 * @param {string} params.mobile 手机号
 * @param {string} [params.verifyCode='0000'] 验证码
 * @param {boolean} [params.mustVerify=false] 默认不会真正发送短信验证码
 */
mdm.sspdLogin = async function (params) {
    params = format.packJsonParam(Object.assign({ verifyCode: '0000', mustVerify: false }, params));
    return common.apiDo({ apiKey: 'ec-mdm-sspd-login', ...params });
};



/**
 * slh2Login-ipad店员登录
 * @param {Object} params
 * @param {string} params.logid 
 * @param {string} params.pass
 * @param {string} params.deviceno 
 * @param {string} dlProductCode 
 * @param {string} dlProductVersion 
 * @param {string} epid 
 * @param {string} language 
 * @param {string} regoptime 
 * */
mdm.slh2Login = async function (params) {

    const res = await common.post('/slh/login.do', params);
    if (res.result.hasOwnProperty('sessionid')) {
        LOGINDATA = Object.assign({}, res.params, res.result);
        await common.delay(500);

    }
    return res;

}
mdm.sessionidValid = async function (params) {
    const apiKey = 'ec-mdm-session-valid';
    return common.post(`/slh/api.do?apiKey=${apiKey}`, params)
}








/**
 * 员工详情(笑铺前端没调这个接口)
 * @param {Object} params
 * @param {Object} params.id
 */
mdm.getStaffInfo = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-staff-getDtoById',
        id: params.id,
        wrapper: true,
    })
};

/**
 * 个人信息-上传头像
 * @param {Object} params
 * @param {Object} params.id 更新的用户id
 * @param {Object} params.avatar 头像地址， uploadMultiFile 通过上传文件接口获取上传返回的数据赋值给avatar 停用传个空过来
 */
mdm.updateAvatar = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-user-update', ...params });
};

/**
 * 门店列表
 * 
 */
mdm.getShopList = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-org-shop-list', ...params });
};

/**
 * 分销商列表
 * @param {Object} params
 * @param {Object} params.nameLike
 */
mdm.findSubFran = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-org-fran-findSubFran', ...params });
};

/**
 * 新增客户
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object} {params,result}
 */
mdm.addCust = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-mdm-user-cust-save',
        ...params
    });
};

/**
 * 客户列表
 * @param {object} params
 * @return {object} {params,result}
 */
mdm.getCustList = async function (params) {
    params = format.packJsonParam(Object.assign({ wrapper: true, flag: 1, pageSize: 20, pageNo: 1, orderBy: "updatedDate", }, params));
    return common.apiDo({ apiKey: 'ec-mdm-user-cust-list', ...params });
};

/**
 * 客户详情
 *
 * @param  {object} params
 * @param  {string} params.id
 * @return {object} {params,result}
 */

mdm.getCustInfo = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-user-cust-getFullDtoById', ...params });
};

/**
 * 停用客户
 * @param  {object} params
 * @param  {string} params.id
 */
mdm.disableCust = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-user-cust-disable', ...params });
};

/**
 * 启用客户
 * @param  {object} params
 * @param  {string} params.id
 */
mdm.enableCust = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-user-cust-enable',
        ...params
    });
};


/**
 *新增供应商
 **/
mdm.addSupplier = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-mdm-org-saveFull',
        ...params
    });
};

/**
 * 生日列表
 * @param  {object} params
 * @param  {object} params.jsonParam
 */
mdm.listByBirth = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-mdm-user-cust-listByBirth',
        ...params
    });
};

/**
 *供应商详情
 **/
mdm.getSupplierInfo = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-org-getFull',
        wrapper: true,
        ...params
    });
};

/**
 * 供应商列表
 */
mdm.getSupplierList = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-org-supp-list',
        wrapper: true,
        ...params
    });
};

/**
 * 获取VIP等级列表 
 */
mdm.getRankList = async function () {
    return common.apiDo({
        apiKey: 'ec-mdm-cust-rule-get',
    });
}

/**
 * 等级规则
 * 获取列表
 */
mdm.saveRankRule = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-cust-rule-save', ...params, });
};

/**
 * 门店(修改)
 *
 */
mdm.saveShop = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-mdm-org-saveFull',
        ...params
    });
};


/**
 *  获取能切换门店的门店列表
 *
 * @param  {object} params
 * @return {object} {params,result}
 */
mdm.getShopListByDevice = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-mdm-org-shop-list-by-device',
        deviceNo: LOGINDATA.deviceNo, //登录的手机号
        pageSize: 0,
        bdomainCode: 'slh',
        jsonParam: {
            wrapper: true,
            flag: 1
        },
        ...params
    });
};


/**
 * 切换门店
 * @param  {object} params
 * @param  {string} params.unitId 帐套
 * @param  {string} params.shopId 门店
 * @return {object} {params,result}
 */
mdm.changeUnit = async function (params) {
    const res = await common.apiDo({
        apiKey: 'ec-mdm-sspd-change-unit',
        mobile: LOGINDATA.deviceNo,
        unitId: LOGINDATA.unitId,
        ...params
    });
    //切换成功后 更新登录信息
    //console.log(res);
    LOGINDATA = res.result.data;
    return res;
};


/**
 * 员工新增
 */
mdm.saveStaff = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-staff-save',
        jsonParam: {
            staff: params,
        },
    });
};

/**
 * 员工列表（低于自己角色等级的员工列表 ）
 * @description 接口说明同ec-mdm-staff-list
 * @param {string} flag  是否停用，1：启用，0：停用
 */
mdm.getStaffList = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-staff-listCtlLevel',
        pageSize: 0,
        jsonParam: {
            wrapper: true,
            ...params
        },
    });
};



/**
 * anonymous function - 停用员工
 *
 * @param  {object} params
 * @return {object}
 */
mdm.disableStaff = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-staff-disable',
        id: params.id,
    });
};


/**
 * anonymous function - 启用员工
 *
 * @param  {object} params
 * @return {object}
 */
mdm.enableStaff = async function (params) {
    return common.apiDo({
        apiKey: 'ec-mdm-staff-enable',
        id: params.id,
    });
};

/**
 * 对员工授权
 * @param {object} params jsonParam
 * @param {object} params.staffId 授权的员工ID
 * @param {object} params.authPhone 授权手机号
 * @param {object} params.verifyCode 验证码
 */
mdm.authorizeStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-mdm-staff-authorizeStaff',
        ...params
    })
};

/**
 * 撤销员工授权
 * @param {object} params jsonParam
 * @param {object} params.staffId 需要被撤销授权的员工ID
 */
mdm.unauthorizeStaff = async function (params) {
    params = Object.assign({}, sspdAccount.defParams, format.packJsonParam(params));
    return common.apiDo({
        apiKey: 'ec-mdm-staff-unauthorizeStaff',
        shopId: LOGINDATA.shopId,
        ...params
    });
};

/**
 * 判断客户是否购买特殊授权(有session即可)
 */
mdm.checkSpeAuth = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-chkSpeAuth', ...params });
};

/**
 * 获取当前租户下所有已特殊授权员工列表，按门店(账套)分组
 */
mdm.getSpeAuthedStaffList = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-staff-speAuthedStaffListForSspd', ...params });
};

/**
 * 获取员工特殊授权信息，返回员工授权号，授权门店信息等 "authPhone":"员工授权号"
 * @param  {object} params
 */
mdm.getSpeAuthedStaffInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-staff-editSpeAuthedStaff', ...params });
};

/**
 * 保存特殊授权
 * @param  {objeect} params
 */
mdm.authorizeSpeStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-staff-authorizeSpeStaff', ...params });
};

/**
 * 撤销特殊授权
 * @param  {object} params
 */
mdm.unathorizeSpeStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-staff-unauthorizeSpeStaff', ...params });
};