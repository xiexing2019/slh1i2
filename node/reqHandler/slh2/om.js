const common = require('../../lib/common');
const format = require('../../data/format');

let om = module.exports = {};


/**
 *  新增职位同时赋予职位权限
 *
 * @param  {object} params
 * @return {object}
 */
om.saveWithPrivs = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-om-role-saveWithPrivs',
        ...params,
    });
};


/**
 * 职位详情
 *
 * @param  {object} params
 * @param  {object} params.id
 * @return {object}
 */
om.getPrivsInfo = async function (params) {
    await common.apiDo({
        apiKey: 'ec-om-role-getWithPrivs', //ec-om-role-getById
        id: params.id,
        wrapper: true,
    });
};



/**
 * 获取职位列表
 *
 * @param  {object} params = {}
 * @return {object}
 */
om.getPrivsList = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        pageSize: 0,
        wrapper: true,
        flag: 1,
        showFlag: 1,
    }, params));
    return common.apiDo({ apiKey: 'ec-om-role-list', ...params });
};

/**
 * reqHandler - 停用职位
 *
 * @param  {object} params description
 * @return {object}        description
 */
om.privsDisenable = async function (params) {
    return common.apiDo({
        apiKey: 'ec-om-role-disable',
        id: params.id,
    })
};

/**
 * reqHandler - 启用职位
 *
 * @param  {object} params description
 * @return {object}        description
 */
om.privsEnable = async function (params) {
    return common.apiDo({
        apiKey: 'ec-om-role-enable',
        id: params.id,
    })
};