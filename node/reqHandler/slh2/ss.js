'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let ss = module.exports = {};


/**
 * 商城跳转登录
 * @description 笑铺内部跳转登录微商城
 */
ss.trustLoginMall = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-ss-mall-trustLoginMall', ...params });
};