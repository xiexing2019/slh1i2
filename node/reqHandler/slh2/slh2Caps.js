const caps = require('../../data/caps');

// 获取命令行参数指定的环境变量名
const envName = caps.name;

const urlCaps = {
    cs3d2: {
        confcUrl: `http://cc.hzecool.com:7180/confc/api.do`
    }
};

module.exports = {
    url: urlCaps[envName]
};