'use strict';
const common = require('../../lib/common');
const crmCaps = require('../crm/crmCaps');

let sspdCrm = module.exports = {
    url: crmCaps.url
};

/**
 * 获取产品信息
 * @param {Object} params
 * @param {string} params.uniCode 商陆花编号
 * @param {string} params.productId   crm产品id
 */
sspdCrm.getInstallInfo = async function (params = {}) {
    return common.post(this.url, { interfaceid: 'cs-getspShopDailyInstallInfo', ...params });
};