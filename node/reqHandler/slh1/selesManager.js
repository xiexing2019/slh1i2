'use strict';
const customerManager = module.exports = {};

const format = require('../../data/format');
const reqHandler = require('../../slh1/help/reqHandlerHelp');
const common = require('../../lib/common');
const caps = require('../../data/caps');

/**
 * 销售开单列表查询 按批次查
 */
customerManager.querySelesList = async function (params = {}) {
    let qlParams = Object.assign({
        'interfaceid': 'ql-142201',
    }, format.qlParamsFormat(params));
    return reqHandler.qlIFCHandler(qlParams);
};