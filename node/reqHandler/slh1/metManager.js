'use strict';

const metManager = module.exports = {};
const format = require('../../data/format');
const reqHandler = require('../../slh1/help/reqHandlerHelp');
const common = require('../../lib/common');
const basicJsonParam = require('../../slh1/help/basiceJsonparam');

/**
 * 新增/修改货品-sf-1511
 * *@description
 * 一代 result:{val:pk}
 * 二代款号pk有区别 result:{val:款号在当前用户门店的id,tenantSpuId:租户级的款号ID}
 * 获取货品信息qf-1511需要用款号在当前用户门店的id,其他(如开单)使用租户级的款号ID
 * @param {object} params
 * @param {object} [jsonParam={}]
 * @param {object} [check=true]
 * @return {object} {param:{},result:{},pk:'',optime:''}
 */
metManager.saveGoodFull = async function ({
    jsonparam = {},
    check = true
}) {
    if (jsonparam.pk) jsonparam.action = 'edit';
    let result = await common.callInterface('sf-1511', {
        'jsonparam': jsonparam
    });
    if (jsonparam && check) {
        expect(result, `新增/修改货品sf-1511失败:\njsonparam:${JSON.stringify(jsonparam)}\nresult:${JSON.stringify(result)}`).not.to.have.property('error');
        if (USEECINTERFACE == 2) await common.delay(100);
    };
    return {
        'param': jsonparam,
        'result': result,
        'pk': result.tenantSpuId || result.val || '', //开单等操作时需要用到的styleid(一二代兼容)
        'optime': common.getCurrentTime()
    };
};


/**
 * 新增/修改类别-sf-1509
 * @param {object} params{name:名称,parentid:父类别id}
 * @param {string} params.pk 传pk表示修改
 * @return {object} pk
 */
metManager.saveGoodClassFull = async function (params = {}) {
    let jsonparam = Object.assign({
        'name': 'cls_' + common.getRandomStr(5),
    }, params);
    jsonparam.action = params.pk ? 'edit' : 'add';
    return reqHandler.sfIFCHandler({
        interfaceid: 'sf-1509',
        jsonparam: jsonparam
    });
};

/**
 * 查询款号类别列表ql-1509
 * @param {object} params
 * @param {string} params.name
 * @return {object} {count:'',dataList:[]...}
 */
metManager.queryGoodClassList = async function (params = {}, needLog = false) {

    let jsonparams = format.qlParamsFormat(params, needLog);
    return common.callInterface('ql-1509', jsonparams);
};

/**
 * 查询款号类别详情-qf-1509
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {...}
 */
metManager.queryGoodClassFull = async function (params = {}) {
    return common.callInterface('qf-1509', params);
};

/**
 * 停用款号类别
 * @param {object} params
 * @param {string} params.pk
 * @return {object} {"val" : "ok"}
 */
metManager.disableGoodClass = async function (params = {}) {
    return common.callInterface('cs-15091', params)
};

/**
 * 启用款号类别
 * @param {object} params
 * @param {string} params.pk
 * @return {object} {"val" : "ok"}
 */
metManager.enableGoodClass = async function (params = {}) {
    return common.callInterface('cs-15092', params)
};


/**
 * 新增品牌 cs-sava-dict-full(全量保存字典) typeid:606：代表品牌
 * @param {object} params 可不传
 * @param {object} params.name {name,typeid,id}
 * @returns {object} {params,result}
 *
 */
metManager.saveBrandFull = async function (params = {}) {
    const brandTypeId = '606';
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `brand${common.getRandomStr(5)}`,
            typeid: brandTypeId
        }, params)
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 停用品牌 cs-disable-dict
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {"val": "ok"}
 */
metManager.disableBrand = async function (params = {}) {
    return common.callInterface('cs-disable-dict', params);
};


/**
 * 停用字典 cs-disable-dict
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {"val": "ok"}
 */
metManager.disableDict = async function (params = {}) {
    return common.callInterface('cs-disable-dict', params);
}


/**
 * 查询品牌列表 cs-dict-list (字典列表) typeid:606：代表品牌
 * @param {object} params
 * @param {string} parmas.name
 * @returns {object} {count,dataList...}
 */
metManager.queryBrandList = async function (params = {}) {
    const brandTypeId = '606';
    return common.callInterface('cs-dict-list', Object.assign({
        typeid: brandTypeId,
        check: true
    }, params));
};

/**
 * 查询品牌详情 cs-get-dict-full (字典列表) typeid:606：代表品牌
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {}
 */

metManager.queryBrandFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
}

/**
 * 新增品牌折扣 sf-15142
 * @param {object} {jsonaram:''}
 * @returns {object} {params, result}
 */
metManager.saveBrandDiscountFull = async function (params = {}) {
    let saveParam = {
        interfaceid: 'sf-15142',
        jsonparam: params.jsonparam,
        check: true
    };
    // return common.callInterface('sf-15142', saveParam);
    return reqHandler.sfIFCHandler(saveParam);
};

/**
 * 查询品牌折扣列表 ql-15142
 * @param {object} params
 * @param {string} params.brandid
 * @return {object} {params, result}
 */
metManager.queryBrandDiscountList = async function (params = {}) {
    return reqHandler.qlIFCHandler(Object.assign({
        interfaceid: 'ql-15142',
    }, params));
};

/**
 * 查询品牌折扣详情 qf-15142
 * @param {object} params {pk:''}
 * @returns {object}
 */
metManager.queryBrandDiscountFull = async function (params = {}) {
    return common.callInterface('qf-15142', params)
};

/**
 * 停用-批量停用货品-cs-1511-33
 * @param {Object} param {pk:'pk1,pk2,pk3...}
 * @param {string} param.pk 款号id，传多个表示批量启用停用
 */
metManager.disableGoods = async function (params = {}) {
    params = Object.assign({
        interfaceid: 'cs-1511-33',
        pk: ''
    }, params)
    return reqHandler.csIFCHandler(params);
}

/**
 * 启用-批量启用货品-cs-1511-32
 * @param {Object} [param={}] pk:'pk1,pk2,pk3...
 * @param {string} [param.pk] 款号id，传多个表示批量启用停用
 */
metManager.enableGoods = async function (param = {}) {
    let params = Object.assign({
        interfaceid: 'cs-1511-32',
        pk: ''
    }, param)
    return reqHandler.csIFCHandler(params);
};

/**
 * 停用-停用单个货品cs-15113
 * @param {Object} param {pk:'}
 * @param {string} param.pk 款号id，
 */
metManager.disableGood = async function (params = {}) {
    params = Object.assign({
        interfaceid: 'cs-15113',
        pk: ''
    }, params)
    return reqHandler.csIFCHandler(params);
}

/**
 * 启用-启用单个货品 cs-15113
 * @param {Object} [param={}] pk:''
 * @param {string} [param.pk] 款号id，传多个表示批量启用停用
 * @returns {object} params,result
 */
metManager.enableGood = async function (param = {}) {
    let params = Object.assign({
        interfaceid: 'cs-15112',
        pk: ''
    }, param)
    return reqHandler.csIFCHandler(params);
};

/**
 * 查询款号管理列表 qd-1511
 * @param {object} params
 * @param {string} params.name
 * @returns {object} params,sesult
 */
metManager.queryGoodManageList = async function (params = {}) {
    return reqHandler.qlIFCHandler(Object.assign({
        interfaceid: 'qd-1511',
        pagesize: 15
    }, params));
}






/**
 * 货品查询列表-ql-15110
 * @param {Object} param ql接口的param
 * @param {boolean} [tody=false] 是否当天
 * @param {needLog} [needLog=false] 是否需要导出拼接后的params
 * @returns {object} {count:"总记录数",dataList:[{},...]}
 */
metManager.queryGoodList = async function (param = {}, tody = false, needLog = false) {
    let params = format.qlParamsFormat(param, tody, needLog);
    return common.callInterface('ql-15110', params);
};

/**
 * 货品查询详情（新增货品界面）-qf-1511
 * @param {Object} [param={}]  {pk:''}
 * @returns {object} {货品详情}
 */
metManager.queryGoodFull = async function (param = {}) {
    return common.callInterface('qf-1511', param);
};



/**
 * 新增颜色组 cs-sava-dict-full(全量保存字典) typeid:631：代表颜色组
 * @param {object} 可不传
 * @param {object} {name,id} 传id代表修改
 * @returns {object} {params,result}
 *
 */
metManager.saveColorGroupFull = async function (params = {}) {
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `颜色组${common.getRandomStr(5)}`,
            typeid: 631
        }, params)
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 查询颜色组列表 cs-dict-list (字典列表) typeid:631：代表颜色组
 * @param {object} {name:""}
 * @returns {object} {count,dataList...}
 */
metManager.queryColorGroupList = async function (params = {}) {
    let qlparam = Object.assign({
        interfaceid: 'cs-dict-list',
        typeid: 631,
    }, params)
    return reqHandler.qlIFCHandler(qlparam);
};

/**
 * 查询颜色组详情 cs-get-dict-full (字典列表) typeid:631：代表品牌
 * @param {object} {pk:}
 * @returns {object} {}
 */

metManager.queryColorGroupFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
}

/**
 * 新增颜色 cs-sava-dict-full(全量保存字典) typeid:601：代表颜色
 * @param {object}
 * @param {object} {name随机,parentid,id} 传id代表修改
 * @param {object} {parentid:颜色组id}
 * @param {object} {parentidname:颜色组名称}
 * @returns {object} {params,result}
 *
 */
metManager.saveColorFull = async function (params = {}) {
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `颜色${common.getRandomStr(5)}`,
            typeid: 601,
            parentid: '',
            parentidname: '',
        }, params)
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 查询颜列表 cs-dict-list (字典列表) typeid:601：代表颜色
 * @param {object} {name:""}
 * @returns {object} {count,dataList...}
 */
metManager.queryColorList = async function (params = {}) {
    let qlparam = Object.assign({
        interfaceid: 'cs-dict-list',
        typeid: 601,
    }, params)
    return reqHandler.qlIFCHandler(qlparam);
};

/**
 * 查询颜色详情 cs-get-dict-full (字典列表) typeid:601：代表颜色
 * @param {object} {pk:}
 * @returns {object} {}
 */

metManager.queryColorFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
}

/**
 * 新增尺码组 cs-sava-dict-full(全量保存字典) typeid:604：代表尺码组
 * @param {object}
 * @param {object} {name随机,id} 传id代表修改
 * @returns {object} {params,result}
 *
 */
metManager.saveSizeGroupFull = async function (params = {}) {
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `尺码组${common.getRandomStr(5)}`,
            typeid: 604,
        }, params)
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 查询尺码组列表 cs-dict-list (字典列表) typeid:604：代表尺码组
 * @param {object} {name:""}
 * @returns {object} {count,dataList...}
 */
metManager.querySizeGroupList = async function (params = {}) {
    let qlparam = Object.assign({
        interfaceid: 'cs-dict-list',
        typeid: 604,
    }, params)
    return reqHandler.qlIFCHandler(qlparam);
};

/**
 * 查询尺码组详情 cs-get-dict-full (字典列表) typeid:604：代表尺码组
 * @param {object} {pk:}
 * @returns {object} {}
 */

metManager.querySizeGroupFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
};



/**
 * 新增尺码 cs-sava-dict-full(全量保存字典) typeid:605：代表尺码
 * @param {object}
 * @param {object} {name随机,parentid,id} 传id代表修改
 * @param {object} {parentid:尺码组id}
 * @param {object} {parentidname:尺码组名称}
 * @returns {object} {params,result}
 *
 */
metManager.saveSizeFull = async function (params = {}) {
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `尺码${common.getRandomStr(5)}`,
            typeid: 605,
            parentid: '',
            parentidname: '',
        }, params)
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 查询尺码列表 cs-dict-list (字典列表) typeid:605：代表尺码
 * @param {object} {name:""}
 * @returns {object} {count,dataList...}
 */
metManager.querySizeList = async function (params = {}) {
    let qlparam = Object.assign({
        interfaceid: 'cs-dict-list',
        typeid: 605,
    }, params)
    return reqHandler.qlIFCHandler(qlparam);
};

/**
 * 查询尺码详情 cs-get-dict-full (字典列表) typeid:605：代表尺码
 * @param {object} {pk:}
 * @returns {object} {}
 */
metManager.querySizeFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
};



/**
 * 新增价格名称
 */


/**
 * 新增价格名称 cs-sava-dict-full(全量保存字典) typeid:402：代表价格名称
 * @param {object}
 * @param {object} {name随机,parentid,id,flag:0停用 1启用} 传id代表修改
 * @returns {object} {params,result}
 *
 */
metManager.savePriceNameFull = async function (params = {}) {
    let jsonParams = {
        interfaceid: 'cs-save-dict-full',
        jsonparam: Object.assign({
            name: `价${common.getRandomNumStr(5)}`,
            typeid: 402,
        }, params),
        check: false
    };
    return reqHandler.csIFCHandler(jsonParams);
};

/**
 * 查询价格名称列表 cs-dict-list (字典列表) typeid:402：代表价格名称
 * @param {object} {name:""}
 * @returns {object} {count,dataList...}
 */
metManager.queryPriceNameList = async function (params = {}) {
    let qlparam = Object.assign({
        interfaceid: 'cs-dict-list',
        typeid: 402,
    }, params)
    return reqHandler.qlIFCHandler(qlparam);
};

/**
 * 查询价格名称详情 cs-get-dict-full (字典列表) typeid:402：代表尺码
 * @param {object} {pk:}
 * @returns {object} {}
 */
metManager.queryPriceNameFull = async function (params = {}) {
    return common.callInterface('cs-get-dict-full', params)
};




/**
 * 批量调价 sf-15120-1
 * @param {object} params {pk,action}
 * @param {string} params.pk pk1,pk2,...
 * @param {string} param.action add/edit
 *@returns {object} {params,result}
 */
metManager.saveChangePriceFull = async function (params = {}) {
    let json = Object.assign(basicJsonParam.changePriceJson(), params)
    let param = {
        interfaceid: 'sf-15120-1',
        jsonparam: json
    };
    return reqHandler.sfIFCHandler(param);
};

/**
 * 查询批量调价列表 ql-15110
 * @param {object} params
 * @param {string} params.stylename
 */
metManager.queryChangePriceList = async function (params = {}) {
    return reqHandler.qlIFCHandler(Object.assign({
        interfaceid: 'ql-15110',
        pagesize: '15'
    }, params))
};

/**
 * 查询批量调价详情 ql-15121
 * @param {object} params
 * @param {string} params.id 调价记录id
 */
metManager.queryChangePriceFull = async function (params = {}) {
    return common.callInterface('ql-15121', params)
};

/**
 * 批量调价-折扣  sf-15120-1
 */
metManager.saveChangeDiscountFull = async function (params = {}) {
    let param = Object.assign({
        interfaceid: 'sf-15120-1',
        jsonparam: basicJsonParam.changeDiscountJson(params)
    });
    return reqHandler.sfIFCHandler(param);
};

/**
 * 撤销批量调价 cs-revokeChangePrice
 * @param {object} params
 * @param {string} params.pk 批量调价单id
 * @returns {object} val:
 */
metManager.revokeChangePrice = async function (params = {}) {
    return common.callInterface('cs-revokeChangePrice', params)
};

/**
 * 库存调整-当前库存-新增/修改保存 sf-1932
 */
metManager.saveInvReviseFull = async function (params = {}) {
    let param = {
        interfaceid: 'sf-1932',
        jsonparam: {
            action: 'add', //修改edit
            invnum: '', // 原库存必传
            pk: '', //库存id 必传
            recvnum: '', // 调整数量 必传
        }
    }
};

/**
 * 同步商品到微商城
 * @param {object} params
 * @param {object} params.pk slh货品id
 */
metManager.asyncDataToSS = async function (params = {}) {
    return common.callInterface('cs-1511-35', params);
};