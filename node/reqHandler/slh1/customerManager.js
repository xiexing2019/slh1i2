'use strict';
const customerManager = module.exports = {};

const format = require('../../data/format');
const reqHandler = require('../../slh1/help/reqHandlerHelp');
const common = require('../../lib/common');
const caps = require('../../data/caps');
const slhCaps = require('./slhCaps');
//------客户、厂商、物流商


/**
 * 获取门店绑定关联
 */
customerManager.getShopBind = async function (params = {}) {
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-mall-getShopBind', interfaceid: 'ec-mall-getShopBind' });
}

/**
 * 手动做线上客户与本地客户的关联
 * @param {object} params
 * @param {number} params.shopId  是  商陆花门店id
 * @param {number} params.custId  是  商城客户id
 * @param {number} params.dwId    是  本地客户id
 */
customerManager.linkDwxxWithCust = async function (params = {}) {
    let param = Object.assign({
        epid: caps.epid,
        slh_version: caps.slhVersion,
        testTaskId: caps.testTaskId,
        interfaceid: 'ec-mall-link-dwxx-with-cust'
    }, params)
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-mall-link-dwxx-with-cust', ...param });
};


/**
 * 新增/修改客户  sf-1401
 * @param {object} params
 * @param {string} params.pk 传pk表示修改
 * @returns {object} {params,result}
 */
customerManager.saveCustomerFull = async function (params = {}) {
    params.action = params.pk ? 'edit' : 'add';
    params = format.packJsonParam(params);
    params.jsonparam = params.jsonParam;
    delete params.jsonParam;
    return reqHandler.sfIFCHandler({ interfaceid: 'sf-1401', ...params });
};

/**
 * 查询客户列表  ql-1339
 * @param {object} params
 * @param {string}
 * @returns {object} {params,result}
 */
customerManager.queryCustomerList = async function (params = {}) {
    let qlParams = Object.assign({
        'interfaceid': 'ql-1339',
        'pagesize': '15'
    }, format.qlParamsFormat(params));
    return reqHandler.qlIFCHandler(qlParams);
};

/**
 * 查询客户详情  qf-1401
 * @param {object} params
 * @param {string} params.pk
 *
 */
customerManager.queryCustomerFull = async function (params = {}) {
    return common.callInterface('qf-1401', params)
};

/**
 * 停用厂商/客户  cs-2241-2
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {val:''}
 */
customerManager.disableCustomer = async function (params = {}) {
    return common.callInterface('cs-2241-2', params)
};

/**
 * 启用厂商/客户 cs-2241-3
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {val:''}
 */
customerManager.enableCustomer = async function (params = {}) {
    return common.callInterface('cs-2241-3', params)
};

//------------------厂商---------------
/**
 * 新增/修改厂商 sf-2241
 * @param {object} params
 * @param {string} params.pk 传pk表示修改
 * @returns {object} {params,result}
 */
customerManager.saveFactoryFull = async function (params = {}) {
    params.action = params.pk ? 'edit' : 'add';
    params = format.packJsonParam(params);
    params.jsonparam = params.jsonParam;
    delete params.jsonParam;
    return reqHandler.sfIFCHandler({ interfaceid: 'sf-2241', ...params });
};

/**
 * 查询厂家列表  qd-2241
 * @param {object} params
 * @param {string} params.id 厂商id
 * @param {string} params.name 厂家name
 * @param {string} params.invid 门店
 * @return {object} {params,result}
 */
customerManager.queryFactoryList = async function (params = {}) {
    let qlParams = Object.assign({
        'interfaceid': 'qd-2241',
    }, format.qlParamsFormat(params));
    return reqHandler.qlIFCHandler(qlParams);
};

/**
 * 查询厂家详情 qf-2241
 * @param {object} params
 * @param {string} params.pk 厂商id
 */
customerManager.queryFactoryFull = async function (params = {}) {
    return common.callInterface('qf-2241', params);
};

/**
 * 积分调整新增 sf-14541-1
 * @param {object} params
 * @param {string} params.pk 传pk表示修改
 * @param {string} params.invid 门店id
 * @param {string} params.dwid 客户id
 * @param {string} params.finpayScore 调整数额（+-）
 * @returns {object} {params, result}
 */
customerManager.saveReviseScoreFull = async function (params = {}) {
    params.action = params.pk ? 'edit' : 'add';
    params = format.packJsonParam(params);
    params.jsonparam = params.jsonParam;
    delete params.jsonParam;
    return reqHandler.sfIFCHandler({ interfaceid: 'sf-14541-1', ...params });
};

/**
 * 查询积分调整列表 ql-14541
 * @param {object} params
 * @param {string} params.dwid 客户id
 * @returns {object} {params, result}
 */
customerManager.queryReviseScoreList = async function (params = {}) {
    params = format.qlParamsFormat(params);
    params.interfaceid = 'ql-14541';
    return reqHandler.qlIFCHandler(params);
};

/**
 * 积分查询 ql-14510
 * @param {object} params
 * @param {string} params.dwid
 * @param {string} ...
 * @returns {object} {params,result}
 */
customerManager.queryScoreList = async function (params = {}) {
    params = format.qlParamsFormat(params);
    params.interfaceid = 'ql-14510';
    return reqHandler.qlIFCHandler(params);
};

/**
 * 新增客户区域 sf-1413-1
 * @param {object} params
 * @param {string} params.pk pk传pk表示修改
 * @param {string} params.name
 * @param {string} parmas.parentid
 * @returns {object} {params, result}
 */
customerManager.saveCustomerDistrictFull = async function (params = {}) {
    params.action = params.pk ? 'edit' : 'add';
    params = format.packJsonParam(params);
    params.jsonparam = params.jsonParam;
    delete params.jsonParam;
    return reqHandler.sfIFCHandler({ interfaceid: 'sf-1413-1', ...params });
}

/**
 * 查询客户区域列表 ql-1413
 * @param {object} params
 * @param {string} params.name
 * @returns {object} {params, result}
 */
customerManager.queryCustomerDistrictList = async function (params = {}) {
    params = format.qlParamsFormat(params);
    params.interfaceid = 'ql-1413';
    return reqHandler.qlIFCHandler(params);
};

/**
 * 查询客户区域详情 qf-1413-1
 * @param {object} params
 * @param {string} params.pk
 */
customerManager.queryCustomerDistrictFull = async function (params = {}) {
    return common.callInterface('qf-1413-1', params)
};

/**
 * 停用客户区域 cs-1413-31
 * @param {object} params
 * @param {string} params.pk
 * @returns {object} {"val" : "ok"}
 */
customerManager.disableCustomerDistrict = async function (params = {}) {
    return common.callInterface('cs-1413-31', params)
}