'use strict';
const customerManager = module.exports = {};

const format = require('../../data/format');
const reqHandler = require('../../slh1/help/reqHandlerHelp');
const common = require('../../lib/common');
const caps = require('../../data/caps');

/**
 * 销售订货列表查询 按批次查
 */
customerManager.querySelesOrderList = async function (params = {}) {
    let qlParams = Object.assign({
        'interfaceid': 'ql-14433',
    }, format.qlParamsFormat(params));
    return reqHandler.qlIFCHandler(qlParams);
};