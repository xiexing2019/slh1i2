const common = require('../../lib/common');
const phoneApi = module.exports = {};

/**
 * 获取手机号码归属地
 * @param {object} params
 * @param {string} params.number 手机号
 */
phoneApi.getPhoneCity = async function (params) {
    return common.get('https://cx.shouji.360.cn/phonearea.php', { number: params.number });
};