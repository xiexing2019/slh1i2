const common = require('../../lib/common');
const format = require('../../data/format');

let ecBi = module.exports = {};

function getAnalysisParams(params) {
    // 1-按日 2-按周 3-按月 4-按年 5-自定义
    const fmt = [3, 4].includes(params.timeKind) ? 'YYYY-MM' : 'YYYY-MM-DD';

};

/**
 * 门店分析首页
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.analysisByShopHomePage = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesShopDaily-analysisByShopHomePage', ...params });
};

/**
 * 门店商品分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.commodityAnalysisByShop = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesSpuDaily-commodityAnalysisByShop', ...params });
};

/**
 * 门店销售分析
 * @param {Object} params
 * @param {Object} params.branchId 必填 分销商id
 * @param {Object} params.timeKind 必填 1-按日 2-按周 3-按月 4-按年 5-自定义
 * 
 */
ecBi.salesAnalysisByShop = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesShopDaily-salesAnalysisByShop', ...params });
};

/**
 * 门店会员分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.memberAnalysisByShop = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesCustDaily-memberAnalysisByShop', ...params });
};

/**
 * 时段分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.timeSlotAnalysisByShop = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesShopTimeSlot-timeSlotAnalysisByShop', ...params });
};

/**
 * 支付统计
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.payAnalysisByShop = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesShopPayDaily-payAnalysisByShop', ...params });
};

/**
 * 库存结构
 * 无参数
 */
ecBi.invAnalysis = async function () {
    return common.apiDo({ apiKey: 'ec-bi-factInvSkuRealTime-invAnalysis' });
};

/**
 * 类别分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.spuAnalysisByClass = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesSpuDaily-spuAnalysisByClass', ...params });
};

/**
 * 导购排名
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.ownerSalesRankAnalysis = async function (params) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesOwnerDaily-ownerSalesRankAnalysis', ...params });
};


/**
 * 品牌分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.brandSpuAnalysis = async function (params) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesSpuDaily-brandSpuAnalysis', ...params });
};



/**
 * 区域分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.salesAnalysisByArea = async function (params) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesShopDaily-salesAnalysisByArea', ...params });
};

/**
 * 整体分析
 * @param {Object} params
 * @param {Object} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
ecBi.memberGlobalAnalysis = async function (params) {
    return common.apiDo({ apiKey: 'ec-bi-factSalesCustDaily-memberGlobalAnalysis', ...params });
};

