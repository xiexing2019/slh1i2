const caps = require('../../data/caps');

module.exports = caps.name.includes('sp') ? require('./_spCaps') : require('../ss/ssCaps');
