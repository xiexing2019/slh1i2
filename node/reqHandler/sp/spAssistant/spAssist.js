const common = require('../../../lib/common');
const caps = require('../../../data/caps');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spAssist = module.exports = {
    busiUrl: ''
};

/**
 * 获取验证码
 * @param {object} params jsonParam
 * @param {string} params.mobile 手机号
 */
spAssist.getValidateCode = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`http://122.112.218.102:6001/confc/api.do`, { apiKey: 'ec-sp-getValidateCode', ...params });
};

/**
 * 获取好店专员信息
 * @description 获取请求地址
 * @param {object} params
 * @param {string} params.mobile 手机号
 * @param {string} params.verifyCode 验证码
 */
spAssist.getSpStaffForLogin = async function (params) {
    // params = format.packJsonParam(params);
    params.verifyCode = '111111';//先写死
    const res = await common.post(spCaps.confcUrl, { apiKey: 'ec-sp-getSpStaffForLogin', ...params });
    this.busiUrl = res.result.data.busiUrl;
    return res;
};

/**
 * 好店专员登陆
 * @description 需要ec-sp-getSpStaffForLogin的返回值
 * @param {object} params  
 * @param {string} params.mobile 手机号
 * @param {string} params.spStaffInfo ec-sp-getSpStaffForLogin返回值
 */
spAssist.loginForSpServiceStaff = async function (params) {
    // params = format.packJsonParam(params);
    const _params = Object.assign({
        dlProductCode: 'slhGoodShopAssistIOS',
        deviceType: 20,// 产品类型 ios:20,安卓:21
        dlProductVersion: '1.0.0',
        mobile: params.mobile,
        osType: 'ios',
        osVerison: 1.1,
    }, format.dataFormat(params.spStaffInfo, 'token;epid;staffId'));
    const res = await common.post(`${this.busiUrl}/api.do`, { apiKey: 'loginForSpServiceStaff', ..._params });
    LOGINDATA = res.result.data;
};

/**
 * 会话校验
 * @param {object} params
 * @param {string} params.mobile 手机号
 * @param {string} params.sessionid 一代会话 
 */
spAssist.checkSpSession = async function (params) {
    if (!params.sessionid) params.sessionid = LOGINDATA.sessionId;
    return common.post(`${this.busiUrl}/checkSpSession.do`, params);
};

/**
 * 好店助手缓存失效
 * @description 登出
 * @param {object} params
 * @param {string} params.mobile 手机号
 */
spAssist.invalidSpAssistSession = async function (params) {
    return common.post(`${this.busiUrl}/invalidSpAssistSession.do`, params);
};

/**
 * 注册并更新消息桥端点
 * @description 登出
 * @param {object} params
 * @param {string} params.pushChannelCode 个推:getui,极光:jpush
 */
spAssist.registerMessageCid = async function (params = {}) {
    params = Object.assign({ slh_version: caps.slhVersion, pushChannelCode: 'jpush', cid: LOGINDATA.spClusterCode }, params);
    return common.post(`${this.busiUrl}/callInterface.do`, { interfaceid: 'registerMessageCid', ...params });
};

