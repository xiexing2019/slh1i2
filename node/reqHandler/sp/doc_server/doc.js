const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const request = require('superagent');
// const path = require('path');

/**
 * 文档管理
 * http://c.hzdlsoft.com:7082/Wiki.jsp?page=文档服务器使用说明
 */
let docManager = module.exports = {};

/**
 * 上传文件
 * @param {object} params 
 * @param {object} params.path 文件路径
 */
docManager.upload = async function (params) {
    let docId;
    await request.post(`${spCaps.docUrl}/v2/upload.do`)
        .attach('image', params.path)
        .then((response) => {
            docId = JSON.parse(response.text).data.val;
        });
    let docUrl = `${spCaps.docUrl}/v2/content.do?id=${docId}`;
    return {
        docId,
        docUrl,
        docCdnUrl: {
            org: docUrl,
            h120: docUrl,
            h240: docUrl,
            h360: docUrl,
        }
    };
};

/**
 * 获取部分文件列表
 * @description 当前文件服务上传过的部分文件列表，暂时是存储在内存中的，上限100
 */
docManager.getDocListSome = () => {
    return common.get(`${spCaps.docUrl}/v2/listSome.do`, {});
};

/**
 * 获取图片
 * @param {object} params 
 * @param {string} params.id 文件id
 * @param {string} params.thumbFlag 
 * @param {string} params.thumbType h120,h240,h360
 */
docManager.getDoc = async function (params) {
    if (params.thumbType) params.thumbFlag = 1;
    return common.get(`${spCaps.docUrl}/v2/url.do`, params);
};

/**
 * 获取图片(不下载)
 * @param {object} params 
 * @param {string} params.docUrl 文件url
 */
docManager.getDocByDocUrl = async function (params) {
    // console.log(params.docUrl);
    const time = Date.now();
    await request.get(params.docUrl).catch(err => {
        throw new Error(`请求失败:\n${JSON.stringify(err)}`);
    });
    const duration = Date.now() - time;
    return { url: params.docUrl, duration }
};

/**
 * 获取图片
 * @param {object} params 
 * @param {string} params.id 文件id
 * @param {string} params.thumbFlag 0:原图 缩略图:1
 * @param {string} params.thumbType 缩略图类型 h120、h240、h360、h480、h640、h1k 
 */
docManager.getDocStream = async function (params) {
    const time = Date.now();
    let duration;
    await request.get(`${spCaps.docUrl}/v2/content.do`)
        .query(params)
        .accept('*/*')
        .then(response => {
            // console.log(response);
            // console.log(`\n response=${JSON.stringify(response)}`);
            const headers = response.headers;
            // console.log(headers);
            expect(Number(headers['content-length'])).to.above(1);
            // common.isImage(headers['content-type']);
        })
        .catch(err => {
            // console.log(err);
            throw new Error(`请求失败:\n${JSON.stringify(err)}`);
        }).finally(() => {
            duration = Date.now() - time;
            // if (duration > 7000) console.warn(`响应时间慢 duration=${duration}`);
            // console.log(`duration=${JSON.stringify(duration)}`);
        });
    return { duration };
};