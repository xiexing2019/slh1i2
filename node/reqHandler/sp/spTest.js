const common = require('../../lib/common');
const spCaps = require('./spCaps');


const spTest = module.exports = {};

/** 
 * 发送mq消息
 * @description 
 * @param {object} params
 * @param {string} params.type 0:字符串 1:单个对象 2:数组
 * @param {string} params.bizKeys 
 * @param {boolean} params.p2p 是否单对单传递(不同集群传递) e.g 买家传给卖家为true 买家传给自己为false
 * @param {string} params.cid p2p=true时 传对应集群编号
 * @param {string} params.tags  
 * @param {string} params.className 对应tags
 * @param {string} params.topic
 * @param {string|object}  params.jsonParam 数据类型对应type的值
 */
spTest.sendMq = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spTest-mqSendTest-sendMq', ...params });
};