const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spdresg = module.exports = {};

/**
 * 保存平台分发规则
 * @description rules已废弃 目前使用channelId代替
 * @param {object} params 
 * @param {string} params.id 主键，修改时必填
 * @param {string} params.masterSaasId 主saasId
 * @param {string} params.slaveSaasId 从saasId
 * @param {string} params.channelId 频道id
 */
spdresg.saveSpSaasDresSyncRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-dresg-spSaasDresSync-save', ...params });
};

/**
 * 手动触发同步saas分发商品
 * @description 
 * 1. 同步数据生成在dresb.spu
 * 2. 基本数据依然使用源商品数据,时间使用触发同步时的时间
 * @param {object} params 
 * @param {string} params.saasSyncId 分发规则id spdresg.sp_saas_dres_sync 主键id
 * @param {string} params.updateDate 更新时间
 */
spdresg.manualSyncSaasDres = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-dresg-spSaasDresSync-manualSyncSaasDres', ...params });
};

/**
 * saas频道商品迁移
 * @description 频道商品复制(只能复制到saas类型频道)
 * @param {object} params 
 * @param {string} params.masterChannelId 主频道id
 * @param {string} params.slaveChannelId 从频道id 必须为saas类型频道(channel_type=4)
 */
spdresg.channelDresCopy = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-channelDresCopy', ...params });
};