const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');


const spConfig = module.exports = {
    url: spCaps.spgUrl,
};

/**
 * 查询参数列表
 * @param {object} params jsonParam
 * @param {string} params.domainKind 配置域类型  system、business、role
 * @param {string} params.ownerKind 属主类型 1 全局, 5 集群, 6 租户, 7 单元
 * @param {string} params.nameOrCode 
 */
spConfig.findProductParams = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spgUrl, { apiKey: 'ec-config-param-findProductParams', ...params });
};

/**
 * 保存参数值
 * @param {object} params jsonParam
 * @param {string} params.ownerKind 所属者类型 1 全局, 5 集群, 6 租户, 7 单元
 * @param {Array} params.data
 * @param {string} params.data.code 参数代码
 * @param {string} params.data.domainKind 配置域类型  system、business、role
 * @param {string} params.data.ownerId 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID
 * @param {string} params.data.val 参数值
 */
spConfig.saveOwnerVal = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-config-param-saveOwnerVal', ...params });
};

/**
 * 取参数详细信息
 * @param {object} params jsonParam
 * @param {string} params.domainKind 配置域类型  system、business、role
 * @param {string} params.ownerKind 属主类型 1 全局, 5 集群, 6 租户, 7 单元
 * @param {string} params.code 
 */
spConfig.getParamInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-config-param-getParamInfo', ...params });
};

/**
 * 取参数值
 * @param {object} params jsonParam
 * @param {string} params.code
 * @param {string} params.domainKind 配置域类型  system、business、role
 * @param {string} params.ownerKind 属主类型 1 全局, 5 集群, 6 租户, 7 单元
 */
spConfig.getParamVal = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-config-param-getParamVal', ...params });
};

/**
 * 系统字典树形结构
 * @param {object} params 
 * @param {string} params.hierId 字典定义id
 * @param {boolean} params.includeSubs 是否包含子节点 默认为true
 * @param {string} params.codeId 父级id 第一级传0 之后的传上一级的code
 */
spConfig.findDictTree = async function (params) {
    return common.post(this.url, { apiKey: 'ec-config-dictTree', ...params });
};

/**
 * 获取字典类别列表
 * @param {object} params 
 * @param {string} params.typeId 字典类别
 */
spConfig.findDictList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-config-list-dict', ...params });
};

/**
 * 获取标准类目列表
 * @param {object} params 
 * @param {string} params.flag 
 * @param {string} params.parentId 
 */
spConfig.getDresClassList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-config-dresClass-list', ...params });
};

/**
 * 导出所有地区
 * @param {object} params 
 * @param {string} params.regionVer 客户端版本号
 */
spConfig.exportDistrictRegion = async function (params) {
    return common.post(this.url, { apiKey: 'ec-config-spRegion-exportDistrictRegion', ...params });
};

/**
 * 物流商列表
 * @param {object} params jsonParam
 * @param {string} params.stdSource 2 ‘第三方公司标准数据，1表示快递100,2表示快递鸟,3表示商陆花支持的物流商
 */
spConfig.findLogisCompanyList = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-stdlogiscompany-list', ...params });
};

// 首页配置

/**
 * 查询首页配置方案详情 买家端
 * @param {object} params jsonParam
 * @param {string} params.homePageVersion 0:2019年元旦临时方案 1:以后的正式方案
 */
spConfig.findPageFragment = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-scPageFragment-findPage', ...params });
};

/**
 * 查询首页配置方案详情 平台端
 * @param {object} params jsonParam
 * @param {string} params.id 配置id
 */
spConfig.getPageFragmentById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-scPageFragment-findPageWithVersion', ...params });
};

/**
 * 保存首页配置方案
 * @description content需要在jsonParam内外都传一次
 * @param {object} params jsonParam
 * @param {string} params.typeId 配置类型：6 活动配置
 * @param {object} params.content 配置内容
 */
spConfig.savePageFragment = async function (params) {
    params = format.packJsonParam(params);
    params.content = params.jsonParam.content;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-scPageFragment-savePage', ...params });
};

/**
 * 保存修改市场,必须在这个接口里面有市场，sp才能够添加市场
 * @param {object} params
 * 
 */
spConfig.saveMarket = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-market-save', ...params });
};

/**
 * 查询市场
 * @param {object} params
 */
spConfig.getMarketList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-market-list', ...params });
};

/**
 * 启用市场
 * @param {object} params
 * @param {Number} params.id
 */
spConfig.enableMarket = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-market-enable', ...params });
};

