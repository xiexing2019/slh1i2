const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spbi = module.exports = {};


// 客户统计

/**
 * 买家数据统计--统计面板
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.getBuyersAnalysis = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-purBillDaily-buyersAnalysis', ...params });
};

/**
 * 买家数据查询
 * @param {object} params 
 * @param {string} params.dataType 1 拿款金额（万元），2 按件数
 * @param {string} params.searchType 查询方式，1 按月 2 按年
 * @param {string} params.orderType 排序方式，1 从高到低、 2 从低到高
 */
spbi.conQueryByBuyer = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-purBillDaily-conQueryByBuyer', ...params });
};

/**
 * 买家数据查询--按消费
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.conDataAnalysisByBuyer = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-purBillDaily-conDataAnalysisByBuyer', ...params });
};

/**
 * 买家数据查询--按地区
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.buyerAnalysisByArea = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-buyerInfo-buyerAnalysisByArea', ...params });
};

/**
 * 买家数据查询--按时间段
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.purAnalysisByTimeSlot = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-purBillDailyTimeSlot-purAnalysisByTimeSlot', ...params });
};

// 订单统计

/**
 * 订单统计--统计面板
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.salesAnalysisOverall = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-salesBillDaily-salesAnalysisOverall', ...params });
};

/**
 * 订单统计--按销售
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.salesAnalysisBySeller = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-salesBillDaily-salesAnalysisBySeller', ...params });
};

/**
 * 订单统计--按时段
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.salesAnalysisByTimeSlot = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-salesBillDailyTimeSlot-salesAnalysisByTimeSlot', ...params });
};


// 地推统计

/**
 * 业务统计
 * @param {object} params 
 * @param {string} params.bdNameLike 市场BD名称 oa中业务员名称
 * @param {string} params.timeKind 必填
 * @param {string} params.now 查询日期，按日与按周的格式为yyyy-MM-dd，按月与按年的格式为yyyy-MM
 */
spbi.crmAnalysisByBd = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-crmBdInfo-crmAnalysisByBd', ...params });
};

/**
 * 业务统计-查看客户
 * @param {object} params 
 * @param {string} params.bdId 市场BD的id
 * @param {string} params.timeKind 必填
 * @param {string} params.now 查询日期，按日与按周的格式为yyyy-MM-dd，按月与按年的格式为yyyy-MM
 */
spbi.custAnalysisByBd = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-crmBdInfo-custAnalysisByBd', ...params });
};

// 整体分析/转化率

/**
 * 整体分析/转化率 -- 统计面板
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.buyerAnalysisOverall = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-buyerInfo-buyerAnalysisOverall', ...params });
};

/**
 * 整体分析/转化率 -- 列表
 * @param {object} params 
 * @param {string} params.timeKind 1-按日 2-按周 3-按月 4-按年 5-自定义
 */
spbi.buyerAnalysisDaily = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-buyerInfo-buyerAnalysisDaily', ...params });
};


// 搜索相关

/**
 * 搜索历史查看列表
 * @param {object} params 
 * @param {string} params.searchToken 关键词
 * @param {string} params.typeId 类别。0 商品 1店铺
 */
spbi.getSpSearchList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSearch-list', ...params });
};

/**
 * 批量保存卖家店铺数据分析（内部调用接口  es那边统计好数据，通过这个接口保存到sp）
 */
spbi.saveSpShopData = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spShopDataAnaly-saveFull', ...params });
};

/**
 * 查询卖家店铺数据分析
 * @param {object} params
 * @param {string} params.name 店铺名字
 */
spbi.getSpShopData = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spShopDataAnaly-findSpShopDataAnaly', ...params });
};

// spbig.sp_search_history_detail
/**
 * 搜索历史明细查询
 * @param {object} params 
 * @param {string} params.searchToken 关键词
 * @param {string} params.removeInner 1代表排除内部人员，0代表不排除内部人员，默认排除
 * @param {string} params.removeTest 1代表排除测试人员，0代表不排除测试人员，默认排除
 */
spbi.findSpSearchHistoryDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSearchHistoryDetail-findSpSearchHistoryDetail', ...params });
};

/**
 * 搜索人+搜索词统计搜索历史明细
 * @param {object} params 
 * @param {string} params.searchToken 关键词
 * @param {string} params.removeInner 1代表排除内部人员，0代表不排除内部人员，默认排除
 * @param {string} params.removeTest 1代表排除测试人员，0代表不排除测试人员，默认排除
 */
spbi.findSpSearchHistoryDetailByTenantAndToken = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSearchHistoryDetail-findSpSearchHistoryDetailByTenantAndToken', ...params });
};

/**
 * 搜索词统计搜索历史明细
 * @param {object} params 
 * @param {string} params.searchToken 关键词
 * @param {string} params.removeInner 1代表排除内部人员，0代表不排除内部人员，默认排除
 * @param {string} params.removeTest 1代表排除测试人员，0代表不排除测试人员，默认排除
 */
spbi.findSpSearchHistoryDetailByToken = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSearchHistoryDetail-findSpSearchHistoryDetailByToken', ...params });
};


// 上新统计

/**
 * 查询卖家店铺数据分析
 * @param {object} params jsonParam
 * @param {string} params.firstMarketDateGte 首次上架开始日期，格式为yyyy-MM-dd
 * @param {string} params.firstMarketDateLte 首次上架结束日期，格式为yyyy-MM-dd
 */
spbi.newSpuTenantAnalysOverall = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-dresSpuSet-newSpuTenantAnalysOverall', ...params });
};

/**
 * 上新门店统计
 * @param {object} params
 * @param {string} params.shopUnitIds 门店unitId列表，用,隔开
 * @param {string} params.firstMarketDateGte 首次上架开始日期，格式为yyyy-MM-dd
 * @param {string} params.firstMarketDateLte 首次上架结束日期，格式为yyyy-MM-dd
 * @param {string} params.spuCountGte 最小上新数
 * @param {string} params.spuCountLte 最大上新数
 */
spbi.newSpuTenantAnalys = async function (params) {
    params = format.packJsonParam(params, ['shopUnitIds']);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-dresSpuSet-newSpuTenantAnalys', ...params });
};

/**
 * 上新货品列表
 * @param {object} params
 * @param {string} params.shopUnitId 门店unitId
 * @param {string} params.firstMarketDateGte 首次上架开始日期，格式为yyyy-MM-dd
 * @param {string} params.firstMarketDateLte 首次上架结束日期，格式为yyyy-MM-dd
 */
spbi.tenantSpuList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-dresSpuSet-tenantSpuList', ...params });
};

/**
 * 大数据同步综合评分以及系统评分到sp
 * @param {Array} params jsonParam
 * @param {string} params.unitId 租户单元id
 * @param {Array} params.dresSpuScoreDTOs 
 * @param {string} params.dresSpuScoreDTOs.spuId
 * @param {string} params.dresSpuScoreDTOs.colligateScore 商品评分
 * @param {string} params.dresSpuScoreDTOs.systemScore 系统评分
 */
spbi.batchUpdateScore = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-dresSpuScore-batchUpdateScore', ...params });
};

/**
 * 活动概览
 * @param {object} params 
 * @param {string} params.tagKind 活动类型
 * @param {string} params.tagId 子活动id
 */
spbi.getActOverview = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spActDailySlot-getActOverview', ...params });
};

/**
 * 获取活动订单明细表
 * @param {object} params 
 * @param {string} params.tagKind 活动类型
 * @param {string} params.tagId 子活动id
 */
spbi.getActPurBillDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-purBillDetail-getPurBillDetail', ...params });
};

/**
 * 商品每日统计接口一
 * @param {object} params 
 * @param {string} params.dateType 日期查询方式 1-按天查询 2按月查询
 * @param {string} params.dataType 查询指标类型 1-商品数 2-新增商品数 3-被查看商品数 4-被收藏商品数 5-被分享商品数 6-加购商品数 7动销商品数
 * @param {string} params.startDate 开始时间
 * @param {string} params.endDate 结束时间
 */
spbi.findSpuSnapshot = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSpuSnapshot-findSpuSnapshot', ...params });
};

/**
 * 商品每日统计接口二
 * @description 可与商品标签维度一起搜索
 * @param {object} params 
 * @param {string} params.dateType 日期查询方式 1-按天查询 2按月查询
 * @param {string} params.dataType 查询指标类型 1-商品数 2-新增商品数 3-被查看商品数 4-被收藏商品数 5-被分享商品数 6-加购商品数 7动销商品数
 * @param {string} params.startDate 开始时间
 * @param {string} params.endDate 结束时间
 */
spbi.findSpuLabelsSnapshot = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spbi-spSpuLabelsSnapshot-findSpuSnapshot', ...params });
};