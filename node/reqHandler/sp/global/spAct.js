const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spAct = module.exports = {};

/**
 * 获取活动报名规则字段和能力位对应关系
 */
spAct.exportEntryRuleFieldBit = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-actg-actInfo-exportEntryRuleFieldBit', ...params });
};

/**
 * 获取活动报名规则元数据
 */
spAct.getRule = async function (params = {}) {
    params.saasKind = 3;
    params.domainKind = 25;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-rule-get', ...params });
};

/**
 * 活动门店报名规则检查
 * @param {object} params
 * @param {string} params.id 活动ID
 * @param {string} params.sallerShopId 卖家门店id
 */
spAct.checkEntryLimit = async function (params = {}) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-actg-actInfo-checkEntryLimit', ...params });
};

/**
 * 卖家门店报名活动
 * @param {object} params jsonParam
 * @param {string} params.actId 活动ID
 * @param {string} params.shopId 门店ID
 */
spAct.sellerShopSignUpAct = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-actg-actShop-sellerShopSignUpAct', ...params });
};

// /**
//  * 保存活动 (已废弃)
//  * @param {object} params jsonParam
//  */
// spAct.saveAct = async function (params) {
//     params = format.packJsonParam(params);
//     return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-save', ...params });
// };

/**
 * 保存活动(新)
 * @description 新增时同时创建活动频道
 * @param {object} params jsonParam
 */
spAct.saveActAndChannel = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-saveActAndChannel', ...params });
};

/**
 * 活动列表-平台端
 * @param {object} params jsonParam
 * @param {Number} params.actSort 0特殊活动 1普通活动(商铺集)
 * @param {Number} params.actNameLike 活动名称
 * @param {Number} params.orgType 组织形式
 * @param {Number} params.flag 活动状态 0停用 1活动创建 2活动报名中 3活动报名结束暂未开始 4活动进行中 5活动结束 6活动强制开始 7活动强制结束
 */
spAct.getActList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-getList', ...params });
};

/**
 * 活动列表-卖家端
 * @param {object} params jsonParam
 * @param {Number} params.queryType 1:新活动 2:审核中 3:进行中 4:已结束 5:已参加
 * @param {Number} params.shopId 门店ID
 */
spAct.getActListBySaler = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-getListOnSaler', ...params });
};

/**
 * 更新活动状态
 * @param {object} params jsonParam
 * @param {string} params.actId 活动ID
 * @param {Number} params.flag 活动状态：开始4、停止0、删除-1
 */
spAct.updateActFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-update', ...params });
};

/**
 * 活动详情
 * @param {object} params jsonParam
 * @param {string} params.id 活动ID
 */
spAct.getActDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-getDetail', ...params });
};

// /**
//  * 绑定门店 (已废弃)
//  * @param {object} params jsonParam
//  * @param {string} params.actId 活动ID
//  * @param {string} params.shopIds 绑定门店ID，使用逗号分割
//  * @param {string} params.delShopIds 解绑门店ID，使用逗号分割
//  */
// spAct.rebindShops = async function (params) {
//     params = format.packJsonParam(params);
//     return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actShop-rebindShops', ...params });
// };

// /**
//  * 生成商品库 (已废弃)
//  * @param {object} params jsonParam
//  * @param {string} params.actId 活动id:修改活动时必填 
//  * @param {string} params.actName 活动名称:修改活动时必填
//  * @param {string} params.channelId 频道id:修改活动时必填
//  * @param {Array} params.addSpus 添加商品对象列表（不包含已存在的商品），对象属性包含：spuId, tenantId, clusterCode（集群）, channelId(没有就不传)，例如：[{“spuId”:1, “tenantId”:1, “clusterCode”:1, “channelId”:1}, {“spuId”:2, “tenantId”:2, “clusterCode”:2, “channelId”:2}]
//  * @param {Array} params.delSpus 删除商品对象列表，对象属性包含：spuId, channelId
//  */
// spAct.saveChannelWithAct = async function (params) {
//     params = format.packJsonParam(params);
//     return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-saveWithAct', ...params });
// };

/**
 * 查询活动频道商品 
 * @param {object} params jsonParam
 * @param {string} params.channelId 频道id
 */
spAct.getActChannelSearchDres = async function (params) {
    params = format.packJsonParam(params, ['channelId', 'flag']);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-actChannelSearchDres', ...params });
};

// /**
//  * 活动门店列表
//  * @description 已废弃
//  * @param {object} params jsonParam
//  * @param {string} params.actId 活动id
//  * @param {Number} params.queryFrom 1:卖家端 2:买家端 3:平台端
//  */
// spAct.getDetailWithShop = async function (params) {
//     params = format.packJsonParam(params);
//     return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-getDetailWithShop', ...params });
// };

/**
 * 活动门店列表
 * @param {object} params jsonParam
 * @param {string} params.actId 活动id 
 * @param {Number} params.flag 活动门店状态 -1:删除 0:停用 1:审核通过 2:审核中 
 */
spAct.getActShopList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actShop-findBindShops', ...params });
};

/**
 * 校验活动状态-买家端
 * @param {object} params jsonParam
 * @param {string} params.actId 活动id 
 */
spAct.checkActStatus = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actInfo-checkActStatus', ...params });
};

/**
 * 根据活动id获取门店列表
 * @param {object} params 
 * @param {string} params.actId 活动id
 */
spAct.getListByActId = async function (params) {
    // return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actTenantShareRatio-getListByActId', ...params });
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actDresAction-findActTenantInfos', ...params });
};

/**
 * 查询商品最优活动
 * @param {Array} params jsonParam
 * @param {string} params.spuId 商品id
 * @param {string} params.spuPrice 商品价格 
 */
spAct.findDresOptimalAct = async function (params) {
    params = format.packJsonParam(params)
    params.saasId = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actDres-findDresOptimalAct', ...params });
};

/**
 * 判断限时秒杀支付是否在有效时间内
 * @description kind=2的时候，actId是act_flash_sale的主键
 * @param {Array} params jsonParam
 * @param {string} params.actKind 活动类型
 * @param {string} params.actId 子活动id
 * @param {string} params.spuId 商品id
 */
spAct.checkPayIntime = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actDres-checkPayIntime', ...params });
};

/**
 * 根据活动商家配置id更新分成比率profitRate
 * @param {object} params 
 * @param {string} params.id 
 * @param {string} params.actId 活动id
 * @param {string} params.profitRate 分润比例
 */
spAct.updateProfitRate = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actTenantShareRatio-updateProfitRate', ...params });
};

/**
 * 批量查询活动商家分成比例
 * @description 内部使用
 * @param {object} params jsonParam
 * @param {array} params.actTenants  
 * @param {string} params.actTenants.actId 活动id  
 * @param {string} params.actTenants.spuId 商品id  
 * @param {string} params.actTenants.tenantId 卖家租户id  
 */
spAct.findTenantShareRatio = async function (params) {
    params = format.packJsonParam(params);
    // return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-findTenantShareRatio', ...params });
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actDresAction-findTenantShareRatio', ...params });
};

/**
 * 首页活动商品top3查询
 * @param {object} params jsonParam
 * @param {string} params.type 类型 11-特殊活动-普通活动， 13-今日新款 15-特殊活动-秒杀活动 19-普通活动(商品集)
 * @param {string} params.typeObjId 今日新款传买家租户id，其他传活动id
 */
spAct.findHomeShowDres = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actDresAction-findHomeShowDres', ...params });
};

/**
 * 限时购列表
 * @param {object} params 
 * @param {string} params.actId 活动id
 */
spAct.getActFlashSaleList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actFlashSale-list', ...params });
};

/**
 * 限时活动商品配置
 * @description 活动设置-商品审核-限时配置
 * @param {array} params 
 * @param {string} params.spuId 
 * @param {string} params.spuTenantId 
 * @param {string} params.channelId
 * @param {array} params.jsonParam 
 */
spAct.saveActFlashSaleDetail = async function (params) {
    params = format.packJsonParam(params, ['spuId', 'channelId', 'spuTenantId']);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actFlashSaleDetail-saveBatch', ...params });
};

/**
 * 查询商品已配置限时抢购信息
 * @param {array} params jsonParam
 * @param {string} params.actId 活动id
 * @param {string} params.spuId 商品id
 */
spAct.getFlashSaleDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actFlashSaleDetail-list', ...params });
};

/**
 * 买家端限时抢购商品列表
 * @param {array} params jsonParam
 * @param {string} params.actId 活动id
 * @param {string} params.flashSaleId 限时购id，不传的话就是首页的查询，传的话就是具体某个限时购的商品列表
 */
spAct.getFlashSaleDetailBuyer = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spact-actFlashSaleDetail-list4Buyer', ...params });
};

