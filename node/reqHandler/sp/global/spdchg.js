const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');


let spdchg = module.exports = {};

/**
 * 新建频道
 * @param {object} params jsonParam
 * @param {string} params.name 
 * @param {number} params.flag 是否启用（0：停用，1：启用 
 */
spdchg.saveChannelWithRule = async function (params) {
    params = format.packJsonParam(params);
    params._master = 0;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-saveWithRule', ...params });
};

/**
 * 活动频道详情
 * @param {object} params 
 * @param {string} params.id 
 */
spdchg.getChannelById = async function (params) {
    params._master = 0;
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-getById', ...params });
};

/**
 * 活动频道列表
 * @param {object} params 
 * @param {string} params.id 
 * @param {number} params.flag 是否启用（0：停用，1：启用 
 */
spdchg.getChannelList = async function (params = {}) {
    params = format.packJsonParam(params);
    params._master = 0;
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-list', ...params });
};

/**
 * 编辑频道
 * @param {object} params jsonParam
 * @param {string} params.id
 * @param {string} params.name 
 * @param {number} params.flag 是否启用（0：停用，1：启用 
 */
spdchg.updateChannelWithRule = async function (params) {
    params = format.packJsonParam(params);
    params._master = 0;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-updateWithRule', ...params });
};

/**
 * 停用频道
 * @param {object} params 
 * @param {string} params.id 
 */
spdchg.stopChannel = async function (params) {
    params._master = 0;
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-stop', ...params });
};

/**
 * 启用频道
 * @param {object} params 
 * @param {string} params.id 
 */
spdchg.startChannel = async function (params) {
    params._master = 0;
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-start', ...params });
};

/**
 * 后台查询活动频道商品
 * @param {object} params 
 * @param {string} params.channelId 频道id
 * @param {string} params.flag -1:手动下架(对应平台端的驳回),0:下架(对应平台端的删除),1:正常/上架(对应平台端的通过),2:待审核。不传递查询所有
 * @param {boolean} [params.shadowAll] 不传默认查询在线商品，传true查询在线和测试所有商品
 */
spdchg.getActChannelDresList = async function (params) {
    params = format.packJsonParam(params, ['channelId', 'flag']);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spdchg-channel-platFindActDres', ...params });
};

/**
 * 修改频道商品状态
 * @param {object} params 
 * @param {string} params.spuId 商品ID
 * @param {string} params.flagToChange -1：手动下架（对应平台端的驳回），0：下架（对应平台端的删除），1：正常/上架（对应平台端的通过），2：待审核
 * @param {string} params.channelId 频道ID
 * @param {string} params.auditRemark 审核意见。该字段只针对频道类型是活动时，进行存储 驳回时，必选
 */
spdchg.changeChannelDresFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channelDetail-changeDresFlag', ...params });
};

/**
 * 频道商品批量添加上架
 * @param {object} params 
 * @param {string} params.channelId 频道id
 * @param {Array} params.spuIds 商品ids
 */
spdchg.saveChannelDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channelDetail-saveDetail', ...params });
};

/**
 * 频道商品批量添加下架
 * @param {object} params 
 * @param {string} params.channelId 频道id
 * @param {Array} params.spuIds 商品ids
 */
spdchg.deleteChannelDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channelDetail-deleteDetail', ...params });
};

/**
 * 活动商品修改优先值
 * @param {object} params 
 * @param {string} params.id 活动商品id，取查询接口返回的actDres.id
 * @param {string} params.channelId 频道id
 * @param {string} params.showOrder 优先级
 * @param {string} params.purLimitNum 限购数量
 */
spdchg.updateChannelDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channelDetail-update', ...params });
};

/**
 * 货品批量加入多个活动
 * @description 异步处理
 * @param {object} params 
 * @param {Array} params.actIds 活动ids
 * @param {Array} params.spuIds 商品ids
 */
spdchg.saveBatchActDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdchg-channelDetail-saveBatchActDetail', ...params });
};

