'use strict';
const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');
const caps = require('../../../data/caps');

let spugr = module.exports = {
    url: spCaps.spgUrl,
    captcha: '000000'
};

//注册

/**
 * 用户注册
 * @param {Object} params jsonParam
 */
spugr.userRegister = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spRegister-userRegister', ...params });
};

/**
 * 注册企业租户
 * @description 需要先用户注册-再企业注册
 * @param {Object} params jsonParam
 */
spugr.createEntTenant = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-createEntTenant', ...params });
};

/**
 * 商陆花老板注册
 * @description 需要先用户注册-再企业注册
 * @param {Object} params jsonParam
 */
spugr.slhBossRegister = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spRegister-slhBossRegister', ...params });
};

/**
 * 商陆花企业租户注册（已废弃）
 * @description 商陆花企业租户注册接口（同一个slhId和slhSn 只能注册一个）同时创建待审核的门店
 * @param {Object} params jsonParam
 */
spugr.createEntTenantFromSlh = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-createEntTenantFromSlh', ...params });
};

/**
 * 商陆花开通sp
 * @param {Object} params jsonParam
 * 
 */
spugr.creatShopFromSlh = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-createEntTenantFromSlhSync', ...params });
};

/**
 * 注销卖家
 * 需要卖家登陆
 * @param {Object} params
 * @param {Number} params.id 需要注销的租户id
 */
spugr.cancelShopById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-cancelShopById', ...params });
};

//登录

/**
 * 获取验证码
 * @param {Object} params
 * @param {string} params.mobile 登录名：手机号或邮箱等*
 */
spugr.sendCode = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spSmsCaptcha-sendCode', ...params });
};

/**
 * 登录
 * @description 成功后会更新信息到LOGINDATA
 * @param {Object} params jsonParam
 * @param {string} params.code 登录名：手机号或邮箱等*
 * @param {string} params.pass 密码
 * @param {string} params.captcha 验证码
 * @param {string} param.authcType 验证方式：1:密码 2.验证码
 */
spugr.spLogin = async function (params) {
    params.saasClusterId = 10;// saas簇：1由她系列，10由她测试系列；自动化测试传10 写死
    params = format.packJsonParam(params);
    // params.dlProductCode = 'slhMallAssistIOS';
    const res = await common.post(this.url, { apiKey: 'ec-spugr-spLogin-userLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId

        //防止请求间隔过短,单元区会话失效的错误
        //全局区登录，通过mq通知单元区；单元区还没处理好，就请求到了单元区
        //暂时加延迟处理
        await common.delay(500);
    };
    return res;
};

/**
 * 校验并返回会话
 * @param {object} params
 * @param {String} params.sessionId sessionId,用来查询该sessionId的会话信息
 * @description 校验并返回会话
 */
spugr.fetchUserSessionLite = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spLogin-fetchSession', ...params });
};

/**
 * 商陆花老板登录
 * @param {Object} params 登录参数的json
 * @param {String} params.token 令牌，用来做身份验证
 * @param {String} params.mobile 手机号码
 * @param {String} params.slhSessionId slh的sessionid
 * @param {String} params.tenantId  企业租户id
 */
spugr.slhBossLogin = async function (params) {
    params = format.packJsonParam(params);
    let res = await common.post(this.url, { apiKey: 'ec-spugr-spLogin-slhBossLogin', ...params });
    if (res.result.code == 0) {
        LOGINDATA = Object.assign(res.params.jsonParam, res.result.data);
        await common.delay(500);
    };
    return res;
};

/**
 * slh员工模拟登陆
 * @param {Object} params 登录参数的json
 * @param {String} params.token 令牌，用来做身份验证
 * @param {String} params.code  工号
 * @param {String} params.slhSessionid slh的sessionid
 * @param {String} params.tenantId  企业租户id
 */
spugr.slhUserLogin = async function (params) {
    params = format.packJsonParam(params);
    let res = await common.post(this.url, { apiKey: 'ec-spugr-spLogin-slhUserLogin', ...params });
    if (res.result.code == 0) {
        LOGINDATA = Object.assign(res.params.jsonParam, res.result.data);
        await common.delay(500);
    };
    return res;
};

/**
 * 登出
 */
spugr.spLogOut = async function (params) {
    const res = await common.post(this.url, { apiKey: 'ec-spugr-spLogin-logout', ...params });
    LOGINDATA = {};//登出成功后清除登录信息
    return res;
};

/**
 * 获取焦点
 * @description 前端获取焦点后调用
 */
spugr.getSpStateFocus = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spugr-spState-focus', ...params });
};

//slh_sp相关接口
/**
 * 创建租户(开通商陆好店)--商陆花   有新接口了，但是还没废弃
*/
spugr.createTenantBySlh = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-tenant-create', ...params });
};

/**
 * 查询银行接口
 * @param {Object} jsonParam
 * @param {String} province 省份
 * @param {String} city 城市
 * @param {bank}   银行名称
 * @param {key}    其他关键词，例如地址
*/
spugr.getBank = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'findBanks', ...params });
};

/**
 * 平台登录代理接口
 * @param {String} spTenantId 平台租户id，如果不传会登陆当前用户所属们门店关联的平台租户
*/
spugr.slhStaffLoginSp = async function (params) {
    params = format.packJsonParam(params);
    let res = await common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-staff-login', ...params });
    if (res.result.code == 0) {
        Object.assign(LOGINDATA, res.params.jsonParam, res.result.data);
    };
    return res;
};

/**
 * 获取商陆花门店是否开通好店
 * @param {Number} slhShopId  商陆花门店店铺id
 */
spugr.isOpen = async function (params) {
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-check-shop', ...params });
};

/**
 * 获取开通商陆好店的门店列表
 * 
*/
spugr.hasOpenShopList = async function (params) {
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-list-shop', ...params });
};

/**
 * 平台商品发布参数设置
 * @param {Object} jsonParam 
 * @param {String} slhShopId    商陆花门店id
 * @param {Number} pubDelay     是否延迟发布 1:是 0:不是
 * @param {Number} pubDelayDay  延迟发布天数，非延迟发布时为0
 * @param {Number} pubPriceType 发布价格类型
*/
spugr.savePublishRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-publish-setting-save', ...params });
};

/**
 * 平台商品发布参数获取
 * @param {String} slhShopId 商陆花门店id
 */
spugr.getPublishRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-publish-setting-get', ...params });
};

/**
 * 新开通商陆好店(创建平台租户) 新接口
*/
spugr.createGoodShopBySlh = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-tenant-createNew', ...params });
};

/**
 * 好店门店仓库绑定修改
 * @param {String} spTenantId 平台租户id
 * @param {String} slhShopId  商陆花门店id
 * @param {String} storeSyncInvIds  商陆花仓库门店, 多个门店逗号隔开
 */
spugr.updateGoodShopStoreInv = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-tenant-update', ...params });
};

/**
 * 门店关联好店信息查询
 * @param {String} slhShopId 商陆花门店id
*/
spugr.getShopBySlhShopId = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${caps.url}/slh/api.do`, { apiKey: 'ec-sp-tenant-get', ...params });
};


//租户管理

/**
 * 修改租户
 * @param {Object} params jsonParam
 * @param {string} params.id id
 */
spugr.updateTenant = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-updateTenant', ...params });
};

/**
 * 租户添加员工
 */
spugr.creatStaff = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spUserTenant-create', ...params });
};

/**
 * 用户关联的租户列表
 * @description 当前用户关联的所有租户，包括个人租户和企业租户
 */
spugr.findAvailableTenantIds = async function () {
    return common.post(this.url, { apiKey: 'ec-spugr-spUserTenant-findAvailableTenantIds' });
};

/**
 * 切换用户所属的租户
 * @description 切换用户所属的租户;前置操作时根据“用户关联的租户列表”中选择要切换到的租户
 * @param {object} params
 * @param {string} params.tenantId 租户id
 */
spugr.changeTenantInSession = async function (params) {
    params = format.packJsonParam(params);
    const res = await common.post(this.url, { apiKey: 'ec-spugr-spLogin-changeTenantInSession', ...params });
    //切换成功后 覆盖更新登录信息
    if (res.result.code == 0) {
        Object.assign(LOGINDATA, res.result.data);
        await common.delay(500);
    };
    return res;
};

/**
 * 返回买家部分信息
 * @param {object} params
 * @param {string} [params.nameLike] 租户名，模糊查询
 * @param {string} [params.mobile] 租户手机号，模糊查询
 * @param {string} [params.searchToken] 搜索词，支持租户名，手机号
 * @param {string} [params.auditFlag] 不传默认查询所有买家（过滤禁用），传auditFlag查询相应auditFlag买家，若查未认证买家auditFlag传3
 */
spugr.findSubmitBuyer = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-findSimpleBuyer', ...params });
};

//店铺管理

/**
 * 根据租户新增店铺
 * @description 测试用
 * @param {Object} params 
 * @param {string} params.tenantId 租户主键id
 */
spugr.addShopByTenantId = async function (params) {
    params.authCode = this.captcha;
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-addShopByTenantId', ...params });
};

/**
 * 店铺认证信息保存
 */
spugr.authcShop = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-authcShop', ...params });
};

/**
 *获取当前登录的店铺详情接口（包括认证信息）
 */
spugr.getShopDetail = async function () {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getDetail' });
};

/**
 * 修改店铺基本信息
 * @param {Object} params 
 * @param {string} params.id 要修改的店铺主键id
 */
spugr.updateShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-updateShop', ...params });
};

/**
 * 获取店铺详情
 * @param {object} params 
 * @param {string} params.id 门店主键
 */
spugr.getShop = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getShop', ...params });
};

/**
 * 获取店铺详情(买家游客)
 * @param {obkect} params 
 * @param {string} params.id 门店主键
 */
spugr.getShopSpb = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spugr-spShop-getShop', ...params });
};

/**
 * 卖家提交社会信用代码
 * @param {obkect} params 
 * @param {string} params.id 卖家店铺id
 * @param {string} params.licence 营业执照
 * @param {string} params.socialCreditCode 社会信用代码
 */
spugr.submitAuditMsg = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-submitAuditMsg', ...params });
};


/**
 * 平台审核卖家社会信用代码相关信息
 * @param {obkect} params 
 * @param {string} params.id 卖家店铺id
 * @param {string} params.auditFlag 0代表被提交的审核信息被驳回，1代表提交的审核信息通过，2代表提交审核信息，3代表未提交审核信息
 * @param {string} params.auditAdvice 审核建议
 */
spugr.auditSubmitAuditMsg = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-auditSubmitAuditMsg', ...params });
};


/**
 * 获取店铺联系人信息
 * @param {object} params 
 * @param {string} params.id 要显示的店铺id
 */
spugr.getShopContractMsg = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getShopContractMsg', ...params });
};

/**
 * 作废店铺所有商品
 * @param {obkect} params 
 * @param {string} params.id 卖家tenantId
 */
spugr.cancelAllDresSpu = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-cancelAllDresSpu', ...params });
};

/**
 * 手机号精准查询店铺
 * @param {Object} params 
 * @param {string} params.mobile 手机号
 */
spugr.findShopByMobile = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findByMobile', ...params });
};

/**
 * 名字模糊查找店铺（买家游客）
 * @param {Object} params 
 * @param {string} params.nameLike 名字的模糊值
 */
spugr.findShopByNameLike = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findByNameLike', ...params });
};

/**
 * 根据地址编码查找店铺（买家游客）
 * @param {Object} params 
 * @param {string} params.cityCode 城市编码
 * @param {string} params.marketId 市场id
 * @param {string} params.floor 楼层-2F -1F 1F 2F
 * @param {string} params.doorNoLike 档口模糊搜索
 */
spugr.findShopByAddr = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findByAddr', ...params });
};

/**
 * 混合搜索名称 + 手机号 +档口
 * @param {Object} params 
 * @param {Object} params.searchToken
 */
spugr.findShopBySearchToken = async function (params) {
    let url = this.url;
    //非管理员
    if (!LOGINDATA.sessionId.match(/^ctr/)) {
        params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
        url = spCaps.spbUrl;
    } else {
        params = format.packJsonParam(params);
    };
    return common.get(url, { apiKey: 'ec-spugr-spShop-findBySearchToken', ...params });
};



/**
 * 店铺多搜索值组合搜索（买家游客）
 * @param {Object} params 
 */
spugr.findShopByParams = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findByParams', ...params });
};

/**
 * 店铺多搜索值组合搜索（平台）
 * @param {Object} params 
 */
spugr.findAllShopByParams = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findAllByParams', ...params });
};

/**
 * 店铺审核状态修改接口(平台)
 * @param {Object} params 
 * @param {string} params.id 要操作启用的店铺id
 * @param {string} params.checkFlag 0 设置成自动审核 1设置成人工审核
 */
spugr.updateShopSpuCheckFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-updateShopSpuCheckFlag', ...params });
};


/**
 * 店铺下线
 * @param {Object} params 
 * @param {string} params.id 要操作下线的店铺id
 */
spugr.offlineShopById = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-offlineShopById', ...params });
};

/**
 * 店铺上线（通过审核）
 * @param {Object} params 
 * @param {string} params.id 要操作上线-通过审核的店铺id
 */
spugr.onlineShopById = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-onlineShopById', ...params });
};

/**
 * 管理员店铺禁用
 * @param {Object} params
 * @param {String} params.id 要操作禁用的店铺id
 * @param {String} params.unUseReason 禁用理由
*/
spugr.disableShopByAdmin = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-unUseShopById', ...params });
};

/**
 * 管理员店铺禁用
 * @param {Object} params
 * @param {String} params.id 要操作禁用的店铺id
*/
spugr.enableShopByAdmin = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-inUseShopById', ...params });
};

/**
 * 查看登录状态
 * @param {Object} params
 * @param {String} params.id 要操作禁用的店铺id
*/
spugr.getShopLoginStatus = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getShopLoginStatus', ...params });
};

/**
 * 店铺点赞（买家）
 * @param {object} params 
 * @param {string} params.id 店铺id
 */
spugr.likeShop = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-likeShop', ...params });
};

/**
 * 查询店铺点赞数
 * @param {object} params 
 * @param {string} params.id 店铺id
 */
spugr.getShopLikeNum = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getShopLikeNum', ...params });
};

/**
 * 店铺混批设置保存
 * @param {object} params jsonParam
 * @param {string} params.id 店铺id
 */
spugr.saveMixBatchNum = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-saveMixBatchNum', ...params });
};

/**
 * 店铺混批能力查询
 * @param {object} params jsonParam
 * @param {string} params.ids 店铺id
 */
spugr.findSpShopMixBatchDTOs = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findSpShopMixBatchDTOs', ...params });
};

/**
 * 店铺禁用上新能力接口(平台)
 * @param {Object} params
 * @param {String} params.id 要操作禁用的店铺id
*/
spugr.unUseNewShopById = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-unUseNewShopById', ...params });
};

/**
 * 启用店铺上新能力接口（平台）
 * @param {Object} params
 * @param {String} params.id 要操作禁用的店铺id
 */
spugr.inUseNewShopById = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-inUseNewShopById', ...params });
};

/**
 * 平台下新商品
 * @param {Object} params
 * @param {String} params.spuId spuid
 * @param {String} params.tenantId 租户id
 * @param {String} params.unitId 租户单元ID
 * @param {String} params.clusterCode  租户集群编码
*/
spugr.offDresByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spSpu-offRemmondSpu', ...params });
};

/**
 * 批量同步虚拟销量
 * @description 废弃 使用ec-spugr-dresSpuSaleNum-batchUpdateSaleNum
 */
spugr.batchUpdateVirtualSaleNum = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-dresSpuSaleNum-batchUpdateVirtualSaleNum', ...params });
};

/**
 * 批量同步商路花销量
 * @param {object} params jsonParam
 * @param {string} params.tenantId 租户id
 * @param {Array} params.dresSpuSalesDTOS 
 * @param {string} params.dresSpuSalesDTOS.spuId 
 * @param {string} [params.dresSpuSalesDTOS.slhSalesNums] 销量
 * @param {string} [params.dresSpuSalesDTOS.virtualSalesNum] 虚拟销量
 * @param {string} [params.dresSpuSalesDTOS.slhSalesNums15] 15天销量
 * @param {string} [params.dresSpuSalesDTOS.slhSalesNums30] 30天销量
 */
spugr.batchUpdateSaleNum = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-dresSpuSaleNum-batchUpdateSaleNum', ...params });
};

/**
 * 设置店铺发货地址(平台)
 * @param {Object} params
 * @param {String} params.id 
 */
spugr.updateShopAddr = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShopAddr-updateAddr', ...params });
};

/**
 * 取消店铺发货地址不同步(平台) 
 * @param {Object} params
 * @param {String} params.id 
 */
spugr.cancelShopAddrFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShopAddr-cancelAddrFlag', ...params });
};

/**
 * 批量设置退货标签
 * @param {Array} params jsonParam
 * @param {String} params.id 店铺id
 * @param {String} params.labels 标签值存于code = sp_return_label的系统参数
 */
spugr.batchSetShopLebal = async function (params) {
    params._master = 1;
    return common.get(this.url, { apiKey: 'ec-spugr-spShop-batchSetShopLebal', ...params });
};

/**
 * web修改自定义排序规则
 * @param {Object} params jsonParam
 * @param {String} params.unitId 
 * @param {String} params.tenantId 
 * @param {String} params.goodsSort 店铺排序规则，采用字典类型,typeId= 2022,没有排序规则传0,(1代表按价格排序，2代表按销量排序,3代表按上架日期排序,4按照showOrder排序)
 * @param {String} params.sortType 是否降序，1代表降序，2代表升序,不传默认降序 
 */
spugr.updateWebShopSortRule = async function (params) {
    params = format.packJsonParam(params, ['unitId', 'tenantId']);
    params._master = 1;
    return common.get(this.url, { apiKey: 'ec-spugr-spShop-updateWebShopSortRule', ...params });
};

/**
 * web端查询店铺的排序
 * @param {Object} params
 * @param {String} params.unitId 
 * @param {String} params.tenantId 
 */
spugr.findWebShopWithRule = async function (params) {
    return common.get(this.url, { apiKey: 'ec-spugr-spShop-findWebShopWithRule', ...params });
};

/**
 * 设置店铺自定义标签
 * @param {Object} params jsonParam
 * @param {String} params.id 店铺id
 * @param {String} params.labels 不传代表去除标签，1,2格式,字典2020
 */
spugr.setShopLebal = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-setShopLebal', ...params });
};

//用户管理

/**
 * 修改密码
 * @param {Object} params jsonParam
 */
spugr.changePass = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        mobile: LOGINDATA.code,
        captcha: this.captcha,//验证码(未登录用户必填)
        oldPass: LOGINDATA.pass,//"旧密码（登录用户必填）",
        // newPass: "新密码*",
    }, params));
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-changePass', ...params });
};

/**
 * 密码重置
 * @param {Object} params jsonParam
 */
spugr.resetPass = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        mobile: LOGINDATA.code,
        captcha: this.captcha,
    }, params));
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-resetPass', ...params });
};

/**
 * 更改手机号
 * @param {Object} params jsonParam
 */
spugr.changeMobile = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        // code: LOGINDATA.code,
        captcha: this.captcha,
    }, params));
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-changeMobile', ...params });
};

/**
 * 修改用户信息
 * @param {Object} params jsonParam
 */
spugr.updateUser = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-updateUser', ...params });
};

/**
 * 获取用户信息
 */
spugr.getUser = async function () {
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-getUser' });
};

/**
 * 注销用户
 */
spugr.deleteUser = async function () {
    return common.post(this.url, { apiKey: 'ec-spugr-spUserManage-deleteUser' });
};

//字典配置 
//暂时放在这里，后续有需要再移
/**
 * 系统字典树形结构
 * @param {Object} params 
 * @param {string} params.hierId 字典定义id
 * @param {boolean} params.includeSubs 是否包含子节点 默认为true
 * @param {string} params.codeId 父级id 第一级传0 之后的传上一级的code
 */
spugr.sysDictTree = async function (params) {
    return common.post(this.url, { apiKey: 'ec-config-dictTree', ...params });
};

/**
 * 获取字典类别列表
 * @param {Object} params 
 * @param {string} params.typeId 字典类别
 */
spugr.getDictList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-config-list-dict', ...params });
};

//门店商户管理

/**
 * 门店商户查询
 * @param {object} params 
 */
spugr.findMerchant = async function (params = {}) {
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-findMerchant', ...params });
};

/**
 * 更新门店商户
 * @param {object} params jsonParam
 * @param {string} params.id 门店商户主键id
 */
spugr.updateMerchant = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-updateMerchant', ...params });
};

/**
 * 人工修改门店商户状态
 * @param {object} params jsonParam
 */
spugr.updateSpMerchatFlagByPerson = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spMerchant-updateSpMerchatFlagByPerson', ...params });
};

/**
 * 创建商户
 * @description 好店app使用
 */
spugr.createMerchat = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-createOrUpdateMerchant', ...params });
};

/**
 * 保存邀请买家开通任务
 * @deprecated 同一天只能操作一次，默认上限是3次，可后台扩展，手机号有校验格式，注册过的不加入
 */
spugr.saveInviteTask = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-inviteTask-saveFull', ...params });
};

/**
 * 客户通知查询
 */
spugr.getInviteTaskMsg = async function (params = {}) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-inviteTask-getMsg', ...params });
};

/**
 * 客户通知列表
 * @param {object} params jsonParam
 * @param {string} params.shopName 店铺名称模糊搜索
 */
spugr.getInviteTaskList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-inviteTask-listTasks', ...params });
};

/**
 * 允许客户再次通知（一次）
 * @param {object} params jsonParam
 * @param {string} params.id 客户通知任务id
 */
spugr.restartInviteTask = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-inviteTask-restartInvite', ...params });
};

/**
 * 关闭客户通知
 * @param {object} params jsonParam
 * @param {string} params.id 客户通知任务id
 */
spugr.stopInviteTask = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-inviteTask-stopInvite', ...params });
};

/**
 * 检查是否允许买家试用
 * @description 试用类似游客功能 允许搜索等
 */
spugr.verifyTryUse = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spTenant-verifyTryUse', ...params });
};

/**
 * 修改店铺内排序规则
 * @param {object} params jsonParam
 * @param {string} params.goodsSort 店铺排序规则，采用字典类型,typeId= 2022,没有排序规则传0,(1代表按价格排序，2代表按销量排序,3代表按上架日期排序,4按照showOrder排序)
 * @param {string} params.sortType 是否降序，1代表降序，2代表升序,不传默认降序
 */
spugr.updateShopSortRule = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-updateShopSortRule', ...params });
};

/**
 * 查询店铺内排序规则
 */
spugr.findShopWithRule = async function () {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-findShopWithRule', });
};

/**
 * 为店铺设置退换货规则
 * @param {object} params jsonParam
 * @param {string} params.shopId 店铺Id
 * @param {string} params.exChnageRuleContent 退换货规则内容
 */
spugr.setShopExchangeRule = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-setExchangeRule', ...params });
};

/**
 * 对店铺设置商品是否店铺外隐藏
 * @param {object} params jsonParam
 * @param {string} params.id 店铺Id
 * @param {string} params.hideFlag 隐藏状态0不隐藏,1商品店铺外隐藏(对应商品隐藏3买家隐藏-店铺内可见)
 */
spugr.setShopSpuHideFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-setShopSpuHideFlag', ...params });
};

/**
 * 设置租户的影子标志
 * @param {Number} id 
 * @param {Number} shadowFlag 影子标志 0:非白名单 1:白名单
 * @param {Number} tenantType 租户类型 0:买家 1:卖家
*/
spugr.setShadowFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spTenant-setShadowFlag', ...params });
};

/**
 * 查询买家
 * @param {Object} params
 * @param {String} params.nameLike 卖家租户名称
 * @param {Number} params.flag 状态 1代表启用，0代表停用
 * @param {String} params.mobile 手机号
 * @param {Number} params.shadowFlag 影子标志 1白名单 0非白名单
 * 
*/
spugr.getBuyerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spTenant-findBuyerList', ...params });
};

/**
 * 邀请店铺库保存数据
 */
spugr.saveInviteShop = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-slhShop-saveFull', ...params });
};

/**
 * 邀请店铺库查询列表
 */
spugr.getInviteShopList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-slhShop-findByPage', ...params });
};

/**
 * 邀请店铺 发送短信
 */
spugr.inviteShopByMsg = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-slhShop-inviteSlhShop', ...params });
};


// wxMp

function getOpenIdByCode(code) {
    return `wxMp${code}`;
};

/**
 * 绑定微信并登录
 * @param {Object} params 
 * @param {String} params.code 手机号
 */
spugr.userLoginWithBindOpenId = async function (params) {
    params.productCode = 'slhGoodShopWxApplet';
    params.captcha = this.captcha;
    params.authcType = 2;
    if (!params.hasOwnProperty('openId')) params.openId = getOpenIdByCode(params.code);

    params = format.packJsonParam(params);
    params.sessionId = '';// 防止其他角色的影响
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-ugr-wxMp-userLoginWithBindOpenId', ...params });
    // console.log(`res=${JSON.stringify(res)}`);
    LOGINDATA = res.result.code == 0 ? Object.assign({}, params.jsonParam, res.result.data) : {};
    return res;
};

/**
 * 获取会话
 * @description 获取会话，已绑定会静默登录；根据authCode、openId、sessionId
 * 
 */
spugr.wxFetchSessionWithLogin = async function (params = {}) {
    params.productCode = 'slhGoodShopWxApplet';
    params = format.packJsonParam(params);
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-ugr-wxMp-fetchSessionWithLogin', ...params });
    LOGINDATA = res.result.data;
};

/**
 * 修改状态账号设备在线状态
 * @param {Object} params
 * @param {string} params.onlineFlag 账号产品客户端在线，如账号小程序在线；在线-1，离线-0，客户端隐藏至后台-2
 */
spugr.updateOnlineFlag = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-ugr-wxMp-updateOnlineFlag', ...params });
};

// 用户偏好
// sp_user_hobby

/**
 * 设置用户编好
 */
spugr.saveOrUpdateSpUserHobby = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpUserHobby-saveOrUpdateSpUserHobby', ...params });
};

/**
 * 判断用户偏好信息是否完善
 */
spugr.verifySpUserHobbyPerfect = async function (params = {}) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpUserHobby-verifySpUserHobbyPerfect', ...params });
};

/**
 * 查询资源管理列表
 * @param {object} params jsonParam
 * @param {string} params.nameLike 名字
 * @param {string} params.sourceType 流量来源类型 1:app 2:wx_software 3:h5
 * @param {number} params.flag 状态 1-正常 0-禁用 -1删除
 */
spugr.getSpSourceEntryList = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpSourceEntry-list', ...params });
};

// 分销

/**
 * 分销商保存
 * @param {object} params
 * @param {string} params.id 
 */
spugr.saveSpDis = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spDis-saveSpDis', ...params });
};

/**
 * 分销商列表
 * @param {object} params
 * @param {string} params.nameLike 名称
 * @param {string} params.channelId 频道
 */
spugr.getSpDisList = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spDis-findSpDis', ...params });
};

/**
 * 分销商商户信息保存
 */
spugr.saveDisMerchant = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spDis-saveDisMerchant', ...params });
};

/**
 * 获取分销商商户信息
 * @param {object} params
 * @param {string} params.id 分销商id
 */
spugr.getDisMerchant = async function (params) {
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spDis-getDisMerchant', ...params });
};

/**
 * 渠道用户创建门店商户
 * @description 渠道版使用
 */
spugr.saveChannelMerchant = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-saveChannelMerchant', ...params });
};

/**
 * web端支持创建修改门店商户
 * @description 渠道版使用 暂主要用于分销商商户信息保存
 * @param {object} params
 * @param {string} params.id 门店商户id,修改时需传 
 * @param {string} params.unitId 租户单元id
 * @param {string} params.tenantType 1代表为分销商创建账户，不传默认分销商
 */
spugr.saveChannelMerchantForWeb = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-saveChannelMerchantForWeb', ...params });
};

/**
 * 管理员修改门店商户
 */
spugr.updateMerchantByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spMerchant-updateMerchantByAdmin', ...params });
};

/**
 * 渠道用户查询渠道内用户账户
 * @param {object} params jsonParam
 */
spugr.findChannelMerchant = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-findChannelMerchant', ...params });
};

/**
 * 渠道用户修改渠道内用户账户
 * @param {object} params jsonParam
 */
spugr.updateChannelMerchantByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-updateChannelMerchantForAdmin', ...params });
};

/**
 * 渠道根据买家id查找账号详细
 * @param {object} params 
 * @param {string} params.buyerId 买家租户ID
 */
spugr.getChannelMerchantDetail = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-getChannelMerchant', ...params });
};

/**
 * 渠道买家认证
 * @param {object} params 
 * @param {string} params.id 
 * @param {string} params.buyerTenantId 买家租户id
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} params.authFlag 认证状态位 -1-拒绝 1-已认证 0-待审核 
 */
spugr.authenticateForSource = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-authenticateForSource', ...params });
};

/**
 * 是否认证
 * @param {object} params 
 * @param {string} params.mobile 手机号
 */
spugr.hasAuthenticatedForSource = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-hasAuthenticatedForSource', ...params });
};

/**
 * 渠道分销商修改用户手机号
 * @param {object} params 
 * @param {string} params.mobile 手机号
 */
spugr.changeMobileForSource = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-changeMobileForSource', ...params });
};

/**
 * 获取店铺简要信息
 * @param {object} params 
 * @param {string} params.buyerUnitId 买家租户id
 */
spugr.getShopDetailByBuyerSeller = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-getShopDetail', ...params });
};

/**
 * 获取渠道合伙人列表
 * @param {object} params jsonParam
 * @param {string} params.mobile 手机号
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} params.flag 1-启用 0-禁用 -1删除
 * @param {string} params.auditFlag 审核状态 1-审核通过 0-待审核 -1已驳回
 */
spugr.findListUserByTenant = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-findListUserByTenant', ...params });
};

/**
 * 修改合伙人状态
 * @param {object} params 
 * @param {string} params.id
 * @param {string} params.flag  状态 1-启用 0-禁用 -1删除
 */
spugr.changeDisPartnerFlag = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-changeFlag', ...params });
};

/**
 * 渠道用户绑定商户信息
 * @description 基本不用 而是用ec-spugr-spMerchant-saveChannelMerchant
 */
spugr.saveSpMerchantMsg = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-saveSpMerchantMsg', ...params });
};

/**
 * 渠道用户绑定银行账号信息
 * @description 基本不用 而是用ec-spugr-spMerchant-saveChannelMerchant
 */
spugr.saveSpMerchantAccountMsg = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-saveAccountMsg', ...params });
};

// 会员等级
/**
 * 获取会员等级列表
 * @param {object} params jsonParam
 * @param {string} params.tenantId 租户id 
 * @param {string} params.vipLevel 会员等级 
 */
spugr.findVipList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-findVipList', ...params });
};

/**
 * 修改会员级别
 * @param {object} params jsonParam
 * @param {string} params.tenantId 租户id 
 * @param {string} params.vipLevel 会员等级 
 */
spugr.changeVipLevel = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-changeVipLevel', ...params });
};

/**
 * 获取saas列表
 * @param {object} params jsonParam
 * @param {string} params.tenantId 租户id 
 */
spugr.getSaasList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spSaasAction-list', ...params });
};

/**
 * 新增/编辑用户级别划分
 * @param {object} params jsonParam
 * @param {string} [params.id] id 
 * @param {string} [params.saasId] saasId 
 * @param {string} params.levelCode 等级code 
 * @param {string} params.levelName 等级名称 
 */
spugr.saveSpSaasLevel = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpSaasLevel-save', ...params });
};

/**
 * 启动禁用删除等级
 * @param {object} params jsonParam
 * @param {string} params.id id 
 * @param {string} params.flag 1-启用 0-禁用 -1-删除 
 */
spugr.changeSpSaasLevelFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpSaasLevel-changeFlag', ...params });
};

/**
 * 用户级别划分
 * @param {object} params jsonParam
 * @param {string} params.saasId  
 */
spugr.findSaasLevelList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpSaasLevel-findSaasLevelList', ...params });
};

// 买家卖家关系管理
/**
 * 查询买家卖家关系
 * @param {object} params 
 * @param {string} params.buyerId 买家用户id
 */
spugr.getBuyerSellerRef = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-getBuyerSellerRef', ...params });
};

/**
 * 修改渠道关系表saasId
 * @param {object} params 
 * @param {string} params.id
 * @param {string} params.saasId
 */
spugr.changeSaasId = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-changeSaasId', ...params });
};


/**
 * 获取买家卖家关系列表
 * @param {object} params 
 * @param {string} params.buyerTenantId
 * @param {string} params.sellerTenantId
 * @param {string} params.saasId
 */
spugr.getSpBuyerSellerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-SpBuyerSeller-getList', ...params });
};

// 租户员工管理

// 租户员工
/**
 * 保存租户员工
 */
spugr.saveTenantStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-spUserManage-saveTenantStaff', ...params });
};

/**
 * 租户员工列表
 */
spugr.getTenantStaffList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spauth-spStaff-list', ...params });
};

/**
 * 清除openId
 * @description 内部使用
 */
spugr.clearOpenId = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-ugrDevops-clearOpenId', ...params });
};
