const common = require('../../../lib/common');
const format = require('../../../data/format');
const spCaps = require('../spCaps');

const spCoupon = module.exports = {};

/**
 * 
 * @param {object} params
 * @param {object} params.domain
 */
spCoupon.getDcodes = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-cardCoupons-dcodes', ...params });
};

/**
 * 维度编码集查询
 * @param {object} params
 * @param {object} params.viewId 维度值
 */
spCoupon.getDimensionCodes = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-dimension-codes', ...params });
};

/**
 * 保存卡券
 * @description
 * @param {object} params
 */
spCoupon.saveCardCoupon = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spcoupg-cardCoupons-saveWithRuleMeta', ...params });
};

/**
 * 获取卡券列表
 */
spCoupon.getCardCouponsList = async function (params) {
    params = format.packJsonParam(params, ['searchList']);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spcoupg-cardCoupons-list', ...params });
};

/**
 * 用户领用卡券
 * @param {object} params
 * @param {array} params.coupons
 */
spCoupon.receiveMyCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcoupb-mycoupons-revice', ...params });
};

/**
 * 我的卡劵查询
 * @param {object} params
 */
spCoupon.findMyCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcoupb-mycoupons-findCoupons', ...params });
};

/**
 * 领券中心卡券列表
 * @description 全局
 * @param {object} params
 * @param {string} params.belongPackageType 卡券类型，0 全部，1 不包含新人大礼包卡券，2 新人大礼包卡券，默认为0
 * @param {string} params.coupType 新版客户端筛选卡券类型，1：平台券 2：店铺券
 */
spCoupon.findCouponsFromCouponCenter = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['isGlobal']), spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spcoupg-couponCenter-findCoupons', ...params });
};

/**
 * 领券中心卡券列表
 * @description 单元
 * @param {object} params
 * @param {string} params.belongPackageType 卡券类型，0 全部，1 不包含新人大礼包卡券，2 新人大礼包卡券，默认为0
 * @param {string} params.coupType 新版客户端筛选卡券类型，1：平台券 2：店铺券
 */
spCoupon.findCouponsFromMyCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['isGlobal']), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcoupb-mycoupons-findCenterCoupons', ...params });
};

/**
 * 可领取卡券列表
 * @param {object} params
 * @param {string} params.searchList 是否查询列表；0表示否，即只有个数
 * @param {string} params.shopId 卖家的tenantId或门店id
 */
spCoupon.findRecvableCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['searchList']), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcoupb-spCardCouponsComb-findRecvableCoupons', ...params });
};