const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');


const spconfg = module.exports = {};

/**
 * 新增/修改自检关键词
 * @param {object} params
 * @param {string} params.id
 * @param {string} params.scword
 */
spconfg.saveSpSearchScword = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSearchScword-save', ...params });
};

/**
 * 自检关键词列表查询
 * @param {object} params
 */
spconfg.getSpSearchScwordList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSearchScword-list', ...params });
};

/**
 * 通过id查询自检关键词
 * @param {object} params
 */
spconfg.getSpSearchScwordById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSearchScword-getById', ...params });
};

/**
 * 删除自检关键词
 * @description 物理删除
 * @param {object} params
 * @param {string} params.id
 */
spconfg.deleteSpSearchScword = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSearchScword-delete', ...params });
};


// 混批

/**
 * 设置混批模板
 * @description batchNum1必须存在且必须小于batchNum2
 * @param {object} params
 * @param {string} params.name 名字
 * @param {object} params.batchNum 
 * @param {string} params.batchNum.batchNum1 
 * @param {string} params.batchNum.batchNum2 
 */
spconfg.saveSpBatchTemplate = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchTemplate-saveSpBatchTemplate', ...params });
};

/**
 * 停用启用删除混批模板
 * @param {object} params
 * @param {string} params.id 模板主键Id
 * @param {string} params.flag 1代表启用,0代表停用,-1代表删除
 */
spconfg.updateSpBatchTemplateFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchTemplate-updateSpBatchTemplateFlag', ...params });
};

/**
 * 平台查询混批模板
 * @param {object} params
 * @param {string} params.name 
 * @param {string} params.flag 不传默认查询有效模板,(始终查询未删除模板)
 */
spconfg.findSpBatchTemplateList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchTemplate-findSpBatchTemplateList', ...params });
};

/**
 * 批量设置自定义混批价
 * @param {object} params
 * @param {string} params.tenantId 
 * @param {string} params.unitId 
 * @param {string} params.name 
 * @param {string} params.priceRate 代表与商陆花价格相乘的比例，若为百分数直接传小数，必须大于0
 * @param {string} [params.priceAddNum] 与商陆花价格相加的数值，必须大于等于0
 * @param {string} params.priceType 商陆花价格类型,取值范围1-5
 */
spconfg.batchSaveSpBatchPrice = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchPrice-batchSaveSpBatchPrice', ...params });
};

/**
 * 设置单个混批价
 * @param {object} params
 * @param {string} params.tenantId 
 * @param {string} params.unitId 
 * @param {string} params.name 
 * @param {string} params.priceRate 代表与商陆花价格相乘的比例，若为百分数直接传小数，必须大于0
 * @param {string} [params.priceAddNum] 与商陆花价格相加的数值，必须大于等于0
 * @param {string} params.priceType 商陆花价格类型,取值范围1-5
 */
spconfg.saveSpBatchPrice = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchPrice-saveSpBatchPrice', ...params });
};

/**
 * 停用启用混批价
 * @param {object} params
 * @param {string} params.id Id
 * @param {string} params.flag 1代表启用,0代表停用,-1代表删除
 */
spconfg.updateSpBatchPriceFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchPrice-updateSpBatchPriceFlag', ...params });
};

/**
 * 查询混批价
 * @param {object} params
 * @param {string} params.unitId 
 * @param {string} params.tenantId 
 * @param {string} params.flag 不传默认查询处于停用,启用状态 
 */
spconfg.findSpBatchPriceList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-spBatchPrice-findSpBatchPriceList', ...params });
};

/**
 * 设置混批规则
 * @param {object} params
 * @param {string} params.planType 计划类型,1代表混批计划类型，不传默认1
 * @param {string} params.planType 计划类型,1代表混批计划类型，不传默认1
 */
spconfg.saveMktSalesPlan = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spConfg-mktSalesPlan-saveMktSalesPlan', ...params });
};



