const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

/**
 * sp通用接口
 */
let spCommon = module.exports = {
    url: spCaps.spgUrl,
};

/**
 * 分类配置查询
 * @param {object} params 
 * @param {string} params.type 分类类型:1-城市；2-风格；3-市场；4-类目; 5-区域； 6-推荐城市 7-主营类目，8-店铺标签,9-商圈,13-新主营类目
 * @param {string} params.typeParentId 对应分类的id: [type=4时，根据typeParentId作为dres_class的id查询得到accode]
 */
spCommon.findCatConfig = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-findCatConfig', ...params });
};

/**
 * 保存分类配置
 * @param {object} params jsonParam
 */
spCommon.saveCatConfig = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-saveCatConfig', ...params });
};

/**
 * 分类配置删除
 * @param {object} params 
 * @param {object} params.id 分类配置ID
 */
spCommon.delCatConfig = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-delCatConfig', ...params });
};


/**
 * 分类配置分页查询（地区、市场、风格、其他等过滤）
 * @param {object} params 
 * @param {Number} params.type 分类类型: 1-城市；2-风格；3-市场；4-类目； 5-区域；6-推荐城市
 */
spCommon.getCatConfigList = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-list', ...params });
};

//商圈相关

/**
 * 关联市场与商圈
 * @param {object} params 
 * @param {string} params.marketId 市场id
 * @param {string} params.tradeId 商圈id
 */
spCommon.relateMarketAndTrade = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-relateMarketAndTrade', ...params });
};

/**
 * 商圈下所有市场
 * @param {object} params 
 * @param {string} params.tradeId 商圈id
 * @param {string} params.showFlag 市场是否可见,显示标志，位操作:0-不可见 1-卖家可见 2-买家可见 3-卖家买家都可见
 * @param {string} params.flag 市场是否停用，0停用1启用
 * @param {string} params.showFlagGt 市场是否可见,填0代表showFlag>0
 */
spCommon.findAllMarketByTrade = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-findAllMarket', ...params });
};

/**
 * 商圈列表
 * @param {object} params jsonParam
 * @param {string} params.typeName 商圈名字
 * @param {string} params.flag 0代表停用1代表启用
 * @param {string} params.showFlag 商圈可见,0-不可见 1-卖家可见 2-买家可见 3-卖家买家都可见
 */
spCommon.getCatConfigTradeList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spCatConfig-findAllTrade', ...params });
};



//品牌

/**
 * 保存品牌
 * @param {String} name 品牌名称
 * @param {Integer} banGroundFlag 是否禁止卖家将该品牌上架，1代表禁止，0代表不禁止
 * @param {Integer} banSearchFlag 是否禁止买家搜索该品牌，1代表禁止，0代表不禁止
*/
spCommon.saveBrand = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBrandProtect-saveBrand', ...params });
};

/**
 * 修改品牌
 * @param {Number} id 
 * @param {String} name 品牌名称
 * @param {Integer} banGroundFlag 是否禁止卖家将该品牌上架，1代表禁止，0代表不禁止
 * @param {Integer} banSearchFlag 是否禁止买家搜索该品牌，1代表禁止，0代表不禁止
*/
spCommon.updateBrand = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBrandProtect-updateBrand', ...params });
};

/**
 * 获取品牌列表
 * @param {String} name 品牌名称
 * @param {Integer} banGroundFlag 是否禁止卖家将该品牌上架，1代表禁止，0代表不禁止
 * @param {Integer} banSearchFlag 是否禁止买家搜索该品牌，1代表禁止，0代表不禁止
*/
spCommon.getBrandList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBrandProtect-findBrandList', ...params });
};

/**
 * 删除品牌
 * @param {Number} 主键
*/
spCommon.delBrand = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBrandProtect-deleteBrand', ...params });
};


//首页Banner

/**
 * 保存横幅banner
 * @param {object} params 保存横幅需要传递的参数
*/
spCommon.saveBanner = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-saveBanner', ...params });
};

/**
 * 横幅banner查询
*/
spCommon.findBanner = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-findBanner', ...params });
};

/**
 * 删除横幅banner
 * @param {Object} param.id 主键，需要删除的Banner记录对应的主键
*/
spCommon.deleteBanner = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-delBanner', ...params });
};

/**
 * 启用横幅Banner
 * @param {string} param.id 需要启用的主键
*/
spCommon.enableBanner = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-enableBanner', ...params });
};

/**
 * 停用横幅Banner
 * @param {string} param.id 需要停用的主键
*/
spCommon.disableBanner = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-disableBanner', ...params });
};


/**
 * 首页banner查询
*/
spCommon.findBannerByKind = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-findBannerByKind', ...params });
};

/**
 * banner图库新增banner
 * @param {Objcet} params 
 */
spCommon.saveBannerInPool = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-saveOrUpdateSpBannerInfo', ...params });
};

/**
 * 查询banner图库列表
 */
spCommon.getBannerPoolList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-findSpBannerInfoList', ...params });
};

/**
 * 查询单个图库
 */
spCommon.getBannerPoolInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-getSpBannerInfo', ...params });
};

/**
 * 删除图库中的Banner
 */
spCommon.delBannerPool = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBanner-deleteSpBanner', ...params });
};


//买家版背景图片管理

/**
 * 背景图片查询
 * @param {object} params 
 * @param {number} params.pageNo
 * @param {number} params.pageSize
 */
spCommon.getBackGroundPhotoList = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBackGroundPhoto-listBackGroundPhoto', ...params })
};

/**
 * 背景图片保存
 * @param {object} params jsonParam
 * @param {string} params.name 图片名称，必选,名称现只有newSpuDocId ,hotSpuDocId,newShopDocId
 * @param {string} params.url 图片内容
 * @param {string} params.type 图片类型，1代表今日上新门店，2代表今日爆款，3代表今日新款 *
 */
spCommon.saveBackGroundPhoto = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBackGroundPhoto-saveBackGroundPhoto', ...params })
};

/**
 * 背景图片修改
 * @param {object} params jsonParam
 * @param {string} params.id 
 * @param {string} params.url 图片内容
 * @param {string} params.type 图片类型，1代表今日上新门店，2代表今日爆款，3代表今日新款 *
 * @param {string} params.name 图片名称，必选,名称现只有newSpuDocId ,hotSpuDocId,newShopDocId
 */
spCommon.updateBackGroundPhoto = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBackGroundPhoto-updateBackGroundPhoto', ...params })
};

/**
 * 删除背景图片(自动化用)
 * @param {Object} params
 * @param {Number} params.id
 * @param {Number} params.deleteIsTrue 1物理删除-直接删除  2业务删除-修改flag     
 */
spCommon.delBackGroundPhoto = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBackGroundPhoto-deleteBackGroundPhoto', ...params })
};

//市场管理

/**
 * 查询市场列表
 */
spCommon.getMarket = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-config-scMarket-list', ...params });
};

//平台审核

/**
 * 买家提交店铺审核
 */
spCommon.submitAuditRecord = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-submitAuditRecord', ...params });
};

/**
 * 审批审核记录(管理员)
 */
spCommon.changeAuditRecord = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-changeAuditRecordFlag', ...params });
};

/**
 * 搜索查看买家店铺审核记录列表（管理员）
 * @param {object} params 
 * @param {number} params.flag 审批状态，0-已提交 1-已通过 4-禁用 9-驳回 99-未认证
 */
spCommon.findBuyerShopAudits = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-findBuyerShopAudits', ...params });
};

/**
 * 买家查看审批信息
 */
spCommon.findShopAuditsByBuyer = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-getBuyerShopInfo', ...params });
};

/**
 * 查询特定认证来源的买家店铺列表
 * @param {object} params 
 * @param {number} params.flag 审批状态，0-已提交 1-已通过 9-驳回 99-未认证
 * @param {string} [params.summitDateGte] 提交起始时间
 * @param {string} [params.summitDateLte] 提交结束时间
 * @param {array} [params.auditSrcs] 认证来源，1代表平台认证，2代表bd认证街边码，3代表bd认证市场码，4代表卖家推荐，0认证来源未知（暂定自主认证),5买家推荐
 */
spCommon.findAuditSrcBuyerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-findAuditSrcBuyerList', ...params });
};

/**
 * 查询特定认证来源的买家店铺统计数据
 * @param {object} params 
 * @param {string} [params.summitDateGte] 提交起始时间
 * @param {string} [params.summitDateLte] 提交结束时间
 */
spCommon.getQRAuditBuyerStatistic = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-getQRAuditBuyerStatistic', ...params });
};

/**
 * 对买家店铺进行标记
 * @param {Object} params
 * @param {String} params.id 买家店铺id
 * @param {String} params.tags 不传代表去除标记，1,2格式,字典2024
*/
spCommon.setTagsForBuyer = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-setBuyerShopTags', ...params });
};

/**
 * 修改买家用户店铺信息
 * 
 */
spCommon.updateBuyerShopAuditMessage = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-updateBuyerShopAuditMessage', ...params });
};

/**
 * 查看买家店铺列表
 * @param {Object} params
 * @param {String} params.shopNameLike 买家店铺名模糊搜索
 * @param {String} params.tagsAll 买家标记的筛选条件，如传递[1,2]，表示tag有1和2的买家门店
 */
spCommon.findBuyerShops = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-findBuyerShops', ...params });
};


/**
 * 保存验证码
 * @param {object} params
 * @param {string} params.action 新增add,修改edit，不传默认新增
 * @param {string} params.id  验证码
 * @param {string} params.rem 备注
 * @param {Integer} params.flag 是否有效
 */
spCommon.saveAuthCode = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spAuthCode-saveAuthCode', ...params });
};

/**
 * 查找验证码列表
*/
spCommon.findAuthCodes = async function (params = { pageSize: 10, pageNo: 1, jsonParam: {} }) {
    return common.post(this.url, { apiKey: 'ec-spugr-spAuthCode-findAuthCodes', ...params });
};

/**
 * 店铺评分维护
 * @param {Object} params 
 * @param {Integer} params.id 要修改的店铺主键id
 * @param {Integer} params.score 店铺评分   大于0就算好店，等于0就删除好店
*/
spCommon.saveShopScore = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-maintainShopScore', ...params });
};

/**
 * 店铺混批能力查询
 * @param {Object} params
 * @param {string} params.ids 店铺主键id中间用逗号分隔
 */
spCommon.findSpShopMixBatchDTOs = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findSpShopMixBatchDTOs', ...params });
};

/**
 * 查找商陆花平台好店
 * @param {object} params
 * @param {string} params.nameLike 店铺名称模糊搜索
 * @param {Integer} params.flag 店铺状态 
*/
spCommon.findSpGoodShops = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-findSpGoodShops', ...params });
};


//勋章管理

/**
 * 保存勋章
 * @param {object} params 
 * @param {object} params.jsonParam
 */
spCommon.saveMedalFull = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spMedal-saveFull', ...params });
};

/**
 * 启用勋章
 * @param {object} params
 * @param {object} params.id 
 */
spCommon.enableMedal = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spMedal-enable', ...params })
};

/**
 * 停用勋章
 * @param {object} params
 * @param {object} params.id 
 */
spCommon.disableMedal = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spMedal-disable', ...params })
};

/**
 * 删除勋章
 * @param {object} params
 * @param {object} params.id 
 */
spCommon.deleteMedal = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spMedal-delete', ...params })
};

/**
 * 勋章列表查询
 * @param {object} params
 * @param {object} params.jsonParam
 */
spCommon.getMedalList = async function (params) {
    params = format.packJsonParam(Object.assign({ pageNo: 1, pageSize: 0 }, params));
    return common.post(this.url, { apiKey: 'ec-spadmin-spMedal-findMedal', ...params })
};

/**
 * 查询卖家勋章列表
 * @param {object} params
 * @param {object} params.id 店铺ID 必填
 * @param {boolean} params.checkUnit 默认传空（卖家看），平台端查看传true
 */
spCommon.getSellerMedalList = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-getMedal', ...params })
};

/**
 * 授予勋章
 * @param {object} params
 * @param {object} params.id 店铺ID 必填
 * @param {object} params.code 勋章唯一编码，可多值，逗号间隔 必填
 * @param {object} params.showFlag 是否授予：1是，0 否 必填
 * @param {object} params.endDate 有效期，当授予为1时有效  必填
 */
spCommon.setMedal = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-setMedal', ...params });
};

/**
 * 批量更新勋章信息到es
 * @description 修复数据用
 */
spCommon.batcShopMedalListtoEs = async function () {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-batcShopMedalListtoEs' })
};

//管理员下架商品

/**
 * 下架商品
 * @param {Object} params
 * @param {array} params.spus List列表，spu信息
 * @param {string} params.spus.spuId spuId
 * @param {string} params.spus.tenantId 租户ID
 * @param {string} params.spus.unitId 租户单元ID
 * @param {string} params.spus.clusterCode 租户集群编码
 */
spCommon.offMarket = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: "ec-spadmin-spSpu-offMarket", ...params });
};

/**
 * 商品上架（平台）
 * @description 只能上架平台下架的商品
 * @param {Object} params
 * @param {array} params.ids SPU ID列表
 * @param {string} params.spus.tenantId 卖家租户ID
 * @param {string} params.spus.unitId 卖家单元ID
 */
spCommon.onMarketDres = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-onMarket-from-plat', ...params });
};

/**
 * 管理员查询商品上架申请列表
 * @param {Object}  params
 * @param {date}    params.proDateGte 申请日期起（格式：yyyy-MM-dd)
 * @param {date}    params.proDateLte 申请日期止（格式：yyyy-MM-dd)
 * @param {Integer} params.flag 状态(2 待审核，3 审核通过，99 审核不通过)
*/
spCommon.findOnMarketReqListByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-dresOnmarketReq-findReq', ...params });
};

/**
 * 管理员查询商品上架申请详情
 * @param {Integer} id
*/
spCommon.findOnMarketReqDetailByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-dresOnmarketReq-getFullById', ...params });
};

/**
 * 审核商品上架申请
 * @param {Object}   params
 * @param {Integer}  params.id 申请id
 * @param {String}   params.checkRem 审核结果备注
 * @param {Integer}  params.flag  状态(3 审核通过，99 审核不通过)
 */
spCommon.checkOnMarketReq = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-dresOnmarketReq-checkReq', ...params });
};

// 异常下架商品管理

/**
 * 人工下架异常价格商品
 */
spCommon.offMarketDresSpuUnusual = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spDresSpuUnusual-offMarket', ...params });
};

/**
 * 获取异常价格商品列表
 */
spCommon.findSpuUnusualList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spDresSpuUnusual-findSpuUnusualList', ...params });
};

/**
 * 异常价格数据推送存储
 * @param {object} params jsonParam
 * @param {array} params.dresSpuUnusualList 
 */
spCommon.saveUnusualPriceDres = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spDresSpuUnusual-saveUnusualPriceDres', ...params });
};

//公司信息管理

/**
 * 公司信息查询
 * @description 
 * 1. spadmin sp_ecool_info 
 * 2. 取flag=1的第一条数据
 */
spCommon.getEcoolInfo = async function () {
    return common.post(this.url, { apiKey: 'ec-config-spParam-getEcoolInfo' });
};

/**
 * 保存公司信息
 */
spCommon.saveEcoolInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-SpEcoolInfo-saveEcoolInfo', ...params });
};

/**
 * 修改公司信息
 */
spCommon.updateEcoolInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-SpEcoolInfo-updateEcoolInfo', ...params });
};

/**
 * 删除公司信息
 * @param {Object} params
 * @param {Object} params.id 公司对象主键id
 */
spCommon.deleteEcoolInfo = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-SpEcoolInfo-deleteEcoolInfo', ...params });
};


// 刷数据

/**
 * 保存刷数据任务
 * @param {object} params jsonParam
 */
spCommon.saveRefreshDataTask = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spRefreshDataTask-saveFull', ...params });
};

/**
 * 删除刷数据任务信息
 * @param {object} params
 * @param {object} params.id 刷数据任务ID
 */
spCommon.deleteRefreshDataTask = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spRefreshDataTask-delete', ...params });
};

/**
 * 停用刷数据任务信息
 * @param {object} params
 * @param {object} params.id 刷数据任务ID
 */
spCommon.stopRefreshDataTask = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spRefreshDataTask-stop', ...params });
};

/**
 * 查询刷数据任务列表
 * @param {object} params jsonParam
 * @param {Date} params.searchDateGte 操作时间起（格式：yyyy-MM-dd HH:mm:ss)
 * @param {Date} params.searchDateLte 操作时间止（格式：yyyy-MM-dd HH:mm:ss)
 * @param {string} params.flag 状态。0 停止，1 进行中，2 已完成
 */
spCommon.getRefreshDataTaskList = async function (params) {
    // const searchDateGte = searchDateLte = common.getCurrentTime();
    // params = Object.assign({ searchDateGte, searchDateLte }, format.packJsonParam(params) );
    params = format.packJsonParam(params)
    return common.post(this.url, { apiKey: 'ec-spadmin-spRefreshDataTask-list', ...params });
};


/**
 * 查询刷数据任务信息
 * @param {object} params
 * @param {object} params.id 刷数据任务ID
 */
spCommon.getRefreshDataTaskFull = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spRefreshDataTask-getById', ...params });
};

// 隐私权限

/**
 * 获取角色隐私上限
 * @param {object} params
 * @param {string} params.id 更新时需要
 * @param {string} params.roleId 角色id
 * @param {string} params.funcCode 权限code,查看电话次数：checkMobile，查看地址次数：checkAddress
 * @param {string} params.timesMax 上限次数
 */
spCommon.saveRolePrivacySet = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confg-rolePrivacySet-save', ...params });
};

/**
 * 获取角色隐私上限
 * @param {object} params
 * @param {string} params.roleId 角色id
 * @param {string} params.code 权限code,查看电话次数：checkMobile，查看地址次数：checkAddress
 */
spCommon.getRolePrivacySetByBizKeys = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confg-rolePrivacySet-getByBizKeys', ...params });
};

/**
 * 获取隐私权限次数接口
 * @param {object} params
 * @param {string} params.code 权限code,查看电话次数：checkMobile，查看地址次数：checkAddress
 */
spCommon.getPrivacyTimes = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-PrivacyOpTimesAction-getPrivacyTimes', ...params });
};

/**
 * 查看隐私信息接口
 * @param {object} params
 * @param {string} params.code 权限code,查看电话次数：checkMobile，查看地址次数：checkAddress
 */
spCommon.operatePrivate = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-PrivacyOpTimesAction-operatePrivate', ...params });
};

/**
 * 设置增加当日查看隐私上限的验证码
 * @param {object} params
 * @param {string} params.authCode 验证码
 */
spCommon.setPrivacyOpTimesAuthCode = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-PrivacyOpTimesAction-setAuthCode', ...params });
};

/**
 * 增加当日查看隐私上限
 * @param {object} params
 * @param {string} params.code 权限code,查看电话次数：checkMobile，查看地址次数：checkAddress
 */
spCommon.increasePrivateTimes = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-PrivacyOpTimesAction-increasePrivateTimes', ...params });
};

//商户管理 

/**
 * 搜索商户
 * @param {Object} params
 */
spCommon.findMerchantByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spMerchant-findMerchantListBySearchMap', ...params });
};

/**
 * 管理员修改门店商户
 * @param {Object} params
 */
spCommon.updateMerchantByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spMerchant-updateMerchantByAdmin', ...params });
};

/**
 * 管理员根据id搜索门店商户
 */
spCommon.getMerchantInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spMerchant-findMerchantById', ...params });
};

/**
 * 管理员重置门店商户状态
 */
spCommon.resetMerchantFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spMerchant-resetFlagByAdmin', ...params });
};

/**
 * 管理员修改店铺
 * @param {Object} params
 * @param {string} params.id 店铺主键id
 */
spCommon.updateShopByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spShop-updateShopByAdmin', ...params });
};

/**
 * 配置推荐店铺
 * @param {Object} params
 * @param {string} params.shopName 店铺名字
 * @param {string} params.shopId 店铺id(已审核通过店铺id)
 * @param {string} params.masterClassId 主营类目
 * @param {string} params.showOrder 排序位
 */
spCommon.saveSuggestShop = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spSuggFocus-saveSuggestShop', ...params });
};

/**
 * 删除关注店铺
 * @param {Object} params
 * @param {string} params.id 店铺配置表id
 */
spCommon.deleteSuggestShop = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spSuggFocus-deleteSuggestShop', ...params });
};

/**
 * 查询配置店铺
 * @param {Object} params
 * @param {string} params.shopName 店铺名字
 * @param {number} params.type 不传默认运营配置关注店铺，type=2为运营配置推荐关注，1为手机号
 * @param {string} params.masterClassId 店铺主营类目
 */
spCommon.findSpSuggFocus = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spSuggFocus-findSpSuggFocus', ...params });
};

//运费模板

/**
 * 管理员保存运费规则模板
 * @param {Object}  jsonParam
 * @param {Integer} feeType 计费类型，1-计件 2-计重 3-计体积，默认计重2
 * @param {String}  startProvCode  始发省代码
 * @param {Object}  params.rules 规则列表list
*/
spCommon.saveFeeRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-saveFeeRuleInBatch', ...params });
};

/**
 * 管理员查看运费模板列表
 * @param {Object}  jsonParam 
 * @param {Integer} feeType 计费类型，1-计件 2-计重 3-计体积 0-包邮
 * @param {String}  startProvCode 始发省代码
 * @param {String}  provId 目的地省代码
*/
spCommon.findFeeRuleList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-findTemplateFeeRuleList', ...params });
};

/**
 * 管理员查看运费详情
 * @param {Object}  jsonParam
 * @param {String}  startProvCode 始发省代码
 * @param {Integer} feeType 计费类型，1-计件 2-计重 3-计体积
*/
spCommon.findFeeRuleInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-getTemplateFeeRuleDetails', ...params });
};

/**
 * 管理员删除运费规则
 * @param {object}  jsonParam
 * @param {Integer} params.feeType
 * @param {String}  params.startProvCode
*/
spCommon.deleteFeeRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-deleteTemplateFeeRule', ...params });
};

/**
 * 管理员保存包邮规则模板
 * @param {Object}  jsonParam
 * @param {Integer} freeType  计费类型，1-满件数 2-满金额
 * @param {String}  startProvCode 始发省代码
*/
spCommon.saveFreeFule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFreeShipRule-saveFreeRuleInBatch', ...params });
};

/**
 * 管理员查看包邮规则列表
 * @param {Object} jsonParam
 * @param {String} params.startProvCode 始发省代码
 * @param {String} params.provId 目的地省代码
 * @param {Integer} params.freeType  计费类型，1-满件数 2-满金额
*/
spCommon.findFreeRuleList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFreeShipRule-findTemplateFreeRuleList', ...params });
};

/**
 * 管理员查看包邮规则详情
 * @param {Object} jsonParam
 * @param {String} params.startProvCode 始发省代码
 * @param {Integer} params.freeType 包邮类型，1-满件数 2-满金额
 * 
*/
spCommon.findFreeRuleInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFreeShipRule-getTemplateFreeRuleDetails', ...params });
};

/**
 * 删除包邮模板
 * @param {Object} params jsonParam
 * @param {String} params.startProvCode 始发省代码
 * @param {Integer} params.freeType 包邮类型，1-满件数 2-满金额
*/
spCommon.deleteFreeRule = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfg-spFreeShipRule-deleteTemplateFreeRule', ...params });
};

// 同义词 

/**
 * 保存搜索同义词
 * @param {Object} params jsonParam
 * @param {String} params.searchName 搜索名
 * @param {String} params.stdName 标准名
 * @param {String} params.id 修改时传入
 * @param {array} params.synonymList 修改时传入
 */
spCommon.saveSpSearchSynonym = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spConfg-spSearchSynonym-saveFull', ...params });
};

/**
 * 查看搜索同义词
 * @param {Object} params jsonParam
 * @param {String} params.id 
 */
spCommon.getSpSearchSynonym = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spConfg-spSearchSynonym-getSpSearchSynonym', ...params });
};

/**
 * 启用搜索同义词
 * @param {Object} params jsonParam
 * @param {String} params.id 
 */
spCommon.enableSpSearchSynonym = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spConfg-spSearchSynonym-enableSpSearchSynonym', ...params });
};

/**
 * 停用搜索同义词
 * @param {Object} params jsonParam
 * @param {String} params.id 
 */
spCommon.disableSpSearchSynonym = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spConfg-spSearchSynonym-disableSpSearchSynonym', ...params });
};

/**
 * 查看搜索同义词
 * @param {Object} params jsonParam
 * @param {String} params.searchName 搜索词 
 */
spCommon.getSpSearchSynonymList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spConfg-spSearchSynonym-list', ...params });
};

/**
 * 修改买家用户店铺信息
 * @param {Object} params
 * @param {String|Object} params.priceRange 价格区间（若想更新价格区间为null,传“”）
 * @param {string} params.masterClassId 主营类目
 */
spCommon.updateBuyerShopMessage = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-updateBuyerShopMessage', ...params });
};

/**
 * 保存运费监控
 * @param {Object} params
 * @param  {Object} params.targetPoses 目的地数组
*/
spCommon.saveMonitor = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-logisFeeMonitorStandard-saveStandard', ...params });
};

/**
 * 查看运费监控列表
 * @param {Objcet} params
 * @param {Number} params.typeId 运费类型
*/
spCommon.getMonitorList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-logisFeeMonitorStandard-listStandardOrigCity', ...params });
};


/**
 * 查看运费监控详情
 * @param {Object}  params
 * @param {origPos} params.origPos 始发地
 * @param {typeId}  params.typeId  类型，1:按重量，2:按件数
*/
spCommon.getMonitorInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-logisFeeMonitorStandard-list', ...params });
};

/**
 * 删除运费监控
 * @param {Number}  origPos 起始城市
 * @param {Number}  typeId  运费类型
*/
spCommon.delMonitorById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-logisFeeMonitorStandard-deleteByStartPosId', ...params });
};

/**
 * 监控到的运费列表
 * @param {String}  shopNameLike 门店名
 * @param {Number}  status 状态,-1:未刷新，0:正常,1:异常
*/
spCommon.monitoredShopsList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-logisFeeMonitorStatus-findMonitorDto', ...params });
};

/**
 * 查看卖家运费模板
 * @param {Number} unitId 卖家单元id
*/
spCommon.getShopsLogisInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spconfb-spFeeRule-getFeeRuleDataByUnit', ...params });
};

/**
 * 手动刷新卖家运费
 * @param {Number} unitId 卖家单元id
*/
spCommon.refreshLogis = async function (params) {
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spconfb-spFeeRule-refreshMonitorInfo', ...params });
};


/**
 * 验证买家店铺信息是否完善
 */
spCommon.verifyShopMessage = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-verifyShopMessage', ...params });
};


//平台消息

/**
 * 保存消息通知任务
 * @param {object} params jsonParam
 */
spCommon.saveSpBroadcastTask = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-saveFull', ...params });
};

/**
 * 查询消息通知任务列表
 * @param {object} params jsonParam
 * @param {string} params.flag 状态。1 创建 2 生效 3 推送中 4 暂停 5结束
 */
spCommon.getSpBroadcastTaskList = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-listTask', ...params });
};

/**
 * 查询消息通知任务信息
 * @param {object} params 
 * @param {string} params.id 消息通知任务ID
 */
spCommon.getSpBroadcastTaskMsg = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-getMsg', ...params });
};

/**
 * 停用消息通知任务
 * @param {object} params 
 * @param {string} params.id 消息通知任务ID
 */
spCommon.stopSpBroadcastTask = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-stopTask', ...params });
};

/**
 * 启用消息通知任务
 * @param {object} params 
 * @param {string} params.id 消息通知任务ID
 */
spCommon.startSpBroadcastTask = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-startTask', ...params });
};

/**
 * 删除消息通知任务
 * @param {object} params 
 * @param {string} params.id 消息通知任务ID
 */
spCommon.deleteSpBroadcastTask = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBroadcastTask-deleteTask', ...params });
};

/**
 * 混合搜索名称查询买家认证店铺接口（平台）
 * @param {object} params
 * @param {object} params.searchToken 名称模糊
 */
spCommon.findShopLite = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-findShopLite', ...params });
};

// 资源位配置

/**
 * 获取推荐位列表接口
 * @param {object} params
 */
spCommon.getRecommendList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-sp-admin-mcr-listRecommend', ...params });
};

/**
 * 资源位保存接口
 * @param {object} params
 */
spCommon.saveRecommend = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-sp-admin-mcr-saveRecommend', ...params });
};

/**
 * 获取推荐banner列表接口
 * @param {object} params
 * @param {string} params.masterClassId 主营类目id
 * @param {string} params.cityCode 城市编码
 */
spCommon.getRecommendBannerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spMainclassCityRecommend-findRecommendBanner', ...params });
};


//标签值

/**
 * 标签选项值列表
 * @param {object} params
 * @param {string} params.typeId 类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充）
 * @param {boolean} params.flag 1，启用 0 禁用
 * @param {string} params.labelNameLike 模糊名称，拼音模糊
 */
spCommon.getSpLabelItemList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spLabelItem-list', ...params });
};

/**
 * 标签选项值启用
 * @param {object} params
 * @param {string} params.id 要操作启用的标签选项值id
 */
spCommon.onlineSpLabelItem = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spLabelItem-online', ...params });
};

/**
 * 标签选项值禁用接口(平台)
 * @param {object} params
 * @param {string} params.id 要操作禁用的标签选项值id
 */
spCommon.offlineSpLabelItem = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spLabelItem-offline', ...params });
};

/**
 * 保存标签选项值
 * @param {object} params
 * @param {string} params.id 
 * @param {string} params.typeId 类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充）
 * @param {string} params.labelName 标签选项值 
 * @param {string} params.labelNamePy 标签选项值拼音 
 * @param {string} params.rem 备注 
 * @param {string} params.showOrder 排序（升序）默认0 
 */
spCommon.saveSpLabelItemFull = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spLabelItem-saveFull', ...params });
};

/**
 * 管理员查看反馈列表 
 * @param {Object} params
*/
spCommon.getFeeBacks = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-buyerSearchFeedback-findBuyerFeedbacksByParams', ...params });
};

/**
 * 买家咨询失败情况统计 
 * @param {Object} params
 * @param {string} params.sellerShopName 卖家店铺名称
 * @param {string} params.imFlag im状态，0代表im未安装，1代表im未激活，2代表im已激活
 */
spCommon.findSpConsultFailCount = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spConsultFailCount-findSpConsultFailCount', ...params });
};

// 管理员商品管理


/**
 * @param {Object} params
 * @param {String} sellerUnitId  店铺单元id
 * @param {Object} platSpus 商品列表
 * @param {String} spuId 款号id
 * @param {String} spuTitle 款号标题
 * @param {String}  spuDocId  款号封面图片id
 * @param {String}  showOrder 显示顺序
*/
spCommon.setGoodShopSpus = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spPlatHotSpu-savePlatHotSpu', ...params });
};

/**
 * 查询店铺平台配置商品列表
 * @param {Object} params
 * @param {String} params.sellerUnitId 店铺单元id
*/
spCommon.getGoodShopSpus = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spPlatHotSpu-findShopPlatHotSpu', ...params });
};

/**
 * 查询筛选配置数据
 * @param {Object} params
 * @param {String} params.filterType 筛选对象， 1 推荐下拉， 2 商品搜索， 3 店铺搜索， 4 分类
 */
spCommon.getFilterConfigData = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spFilterConfig-getFilterConfigData', ...params });
};

/**
 * 保存筛选配置
 * @param {Objcet} params
 * @param {Number} params.filterType
 * @param {Number} params.typeValue
 * @param {String} params.typeName
 * @param {Number} params.showOrder
 */
spCommon.saveSpFilterConfig = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spFilterConfig-save', ...params });
};

/**
 * 查询筛选配置
 * @param {Objcet} params
 * @param {Number} params.filterType
 */
spCommon.getSpFilterConfigList = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spFilterConfig-list', ...params });
};

/**
 * 删除筛选配置
 * @param {Objcet} params
 * @param {Number} params.id
 */
spCommon.delSpFilterConfig = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spadmin-spFilterConfig-delete', ...params });
};

// 调研信息

/**
 * 买家提交调研信息
 * @param {Objcet} params
 */
spCommon.fillBuyerSurvery = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-fillBuyerSurvery', ...params });
};

/**
 * 买家验证调研信息是否完善
 * @param {Objcet} params
 */
spCommon.verifyBuyerSurvery = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-verifyBuyerSurvery', ...params });
};


/**
 * 初始化买家店铺标准类目
 * @param {Objcet} params
 */
spCommon.initBuyerShopStandardClassId = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spadmin-spBuyerShop-initStandardClassId', ...params });
};

/**
 * 初始化卖家店铺标准类目
 * @param {Objcet} params
 */
spCommon.initShopStandardClassId = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spugr-spShop-initStandardClassId', ...params });
};
