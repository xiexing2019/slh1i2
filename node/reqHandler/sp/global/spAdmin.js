const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

const spAdmin = module.exports = {};

// 商品尺码表

/**
 * 平台给货品更新尺码图
 * @param {object} params
 * @param {string} params.id spuId
 */
spAdmin.updateSpuYardage = async function (params) {
    params = format.packJsonParam(params, ['_cid', '_tid']);
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateSpuYardageByFlat', ...params });
};

/**
 * 查询尺码表列表
 * @param {object} params
 */
spAdmin.findDresYardageList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-dresSpuYardage-findDresYardageList', ...params });
};

/**
 * 新增/修改尺码表
 * @param {object} params
 * @param {string} params.name 名字
 * @param {object|string} params.docId 文档id
 * @param {string} params.classId 商品类型
 * @param {object} params.props 尺码数据
 */
spAdmin.saveOrUpdateYardage = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-dresSpuYardage-saveOrUpdateYardage', ...params });
};

/**
 * 获取单条记录详情
 * @param {object} params
 * @param {string} params.id 
 */
spAdmin.getYardageDetailInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-dresSpuYardage-getDetailInfo', ...params });
};

/**
 * 删除单条记录详情
 * @param {object} params
 * @param {string} params.id 
 */
spAdmin.deleteYardageById = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-dresSpuYardage-deleteById', ...params });
};


// 热词 关键词管理/查询

/**
 * 查询关键词列表(平台)
 * @param {object} params
 * @param {string} params.wordNameLike 名字搜索 
 * @param {string} params.flag 1有效 0无效 
 * @param {string} params.typeId 插入类型 0自动 1人工 
 */
spAdmin.findHotWordsList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-findHotWordsList', ...params });
};

/**
 * 新增/修改关键词
 * @param {object} params
 * @param {string} params.wordName 关键字名称 
 */
spAdmin.saveHotWords = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-saveHotWords', ...params });
};

/**
 * 获取关键词详情
 * @param {object} params
 * @param {string} params.id  关键词id  
 */
spAdmin.getHotWordDetailInfo = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-getDetailInfo', ...params });
};

/**
 * 获取关键词
 * @param {object} params
 * @param {string} params.size 需要展示的关键词数量  
 */
spAdmin.findEveHotWords = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-findEveHotWords', ...params });
};

/**
 * 下架关键词平台
 * @param {object} params
 * @param {string} params.id 下架关键词id  
 */
spAdmin.offLineHotWord = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-offLineWord', ...params });
};

/**
 * 上新关键词
 * @param {object} params
 * @param {string} params.id 上新关键词id  
 */
spAdmin.onLineHotWord = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-onLineWord', ...params });
};

/**
 * 获取推荐时间平台
 */
spAdmin.getHotWordsSuggestTime = async function (params) {
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spHotWords-getSuggestTime', ...params });
};

/**
 * 动销数据推送存储
 * @description 场景为 es向服务端推送数据
 * @param {object} params
 * @param {array} params.dresSpuUnusualList   
 */
spAdmin.offLineDynamicSell = async function (params) {
    params.reason = 'a';
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spDresSpuUnusual-offLineDynamicSell', ...params });
};

// 货品标签

// 标签类型

/**
 * 保存标签类型
 * @param {object} params
 * @param {string} params.name 标签类别 名
 */
spAdmin.saveLableType = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelType-saveLableType', ...params });
};

/**
 * 标签类型列表
 * @param {object} params
 * @param {string} params.name 标签类别名
 * @param {string} params.flag 状态
 */
spAdmin.getLableTypeList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelType-list', ...params });
};

/**
 * 标签选项类型启用接口(平台)
 * @param {object} params
 * @param {string} params.id 要操作启用的标签类型id
 */
spAdmin.ableLableTypet = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelType-ableLableTypet', ...params });
};

/**
 * 标签选项类型禁用接口(平台)
 * @param {object} params
 * @param {string} params.id 要操作启用的标签类型id
 */
spAdmin.disableLableType = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelType-disableLableType', ...params });
};

/**
 * 同步字典标签
 * @description 校验confc表 sc_dict 类型id2020的数据是否进入表sp_label_type和sp_label_item
 */
spAdmin.synDictToLabel = async function (params = {}) {
    // params = format.packJsonParam(params);
    // params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelType-synDictToLabel', ...params });
};

//标签值

/**
 * 保存标签选项值
 * @param {object} params
 * @param {string} params.id 
 * @param {string} params.typeId 类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充）
 * @param {string} params.labelName 标签选项值 
 * @param {string} params.labelNamePy 标签选项值拼音 
 * @param {string} params.rem 备注 
 * @param {string} params.showOrder 排序（升序）默认0 
 */
spAdmin.saveSpLabelItemFull = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelItem-saveFull', ...params });
};

/**
 * 标签选项值列表
 * @param {object} params
 * @param {string} params.typeId 类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充）
 * @param {string} params.flag 1，启用 0 禁用
 * @param {string} params.labelNameLike 模糊名称，拼音模糊
 */
spAdmin.getSpLabelItemList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelItem-list', ...params });
};

/**
 * 标签选项值启用
 * @param {object} params
 * @param {string} params.id 要操作启用的标签选项值id
 */
spAdmin.onlineSpLabelItem = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelItem-online', ...params });
};

/**
 * 标签选项值禁用接口(平台)
 * @param {object} params
 * @param {string} params.id 要操作禁用的标签选项值id
 */
spAdmin.offlineSpLabelItem = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelItem-offline', ...params });
};

/**
 * 有效标签选项值列表
 * @description 类型停用，小类停用
 * @param {object} params 
 */
spAdmin.getSpLabelItemListValid = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelItem-listValid', ...params });
};

// 类别标签关系

/**
 * 保存标签类别关系值（平台）
 * @param {object} params
 * @param {string} params.classId 类别id
 * @param {array} params.labelList 标签列表
 * @param {array} params.labelList.labelTypeId 标签类别id
 * @param {array} params.labelList.labelItemId Long的list，标签值list
 */
spAdmin.saveSpLabelClassRelation = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.confcUrl, { apiKey: 'ec-config-spLabelClassRelation-save', ...params });
};

/**
 * 分类下标签选项值列表
 * @param {object} params
 * @param {string} [params.typeId] 标签类别id
 * @param {string} [params.classId] 分类id
 */
spAdmin.getSpLabelClassRelationList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.confcUrl, { apiKey: 'ec-config-spLabelClassRelation-list', ...params });
};


/**
 * 获取买手说（商品和店铺）
 * @param {object} params
 * @param {string} params.type 2-商品 1-店铺
 */
spAdmin.getPlatBuyerEvaluateList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spPlatBuyerEvaluate-getPlatBuyerEvaluateList', ...params });
};

// 标签审核

/**
 * 标签审核通过
 * @param {object} params
 * @param {string} params.id 审核记录id
 */
spAdmin.passLabelAudit = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-auditRecord-auditPass', ...params });
};

/**
 * 标签审核驳回
 * @param {object} params
 * @param {string} params.id 审核记录id
 * @param {string} params.auditRemark 驳回理由 
 */
spAdmin.rejectLabelAudit = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-auditRecord-auditReject', ...params });
};

/**
 * 新增标签审核
 * @param {object} params
 * @param {string} params.type 审核类型,1-卖家店铺标签 2-买家标记 3-商品标签
 * @param {string} params.auditObjId 审核对象id
 * @param {string} params.labels 修改后的标签id，用,隔开
 */
spAdmin.addLabelAuditRecord = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-auditRecord-addRecord', ...params });
};

/**
 * 标签审核列表查询
 * @param {object} params
 * @param {string} params.type 审核类型,1-卖家店铺标签 2-买家标记 3-商品标签
 * @param {string} params.mobileLike 手机号模糊搜索
 * @param {string} params.cityCode 城市id
 */
spAdmin.getLabelAuditRecordList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-auditRecord-listRecord', ...params });
};

/**
 * 标签审核查询
 * @param {object} params
 * @param {string} params.id 审核记录id
 */
spAdmin.getLabelAuditRecordById = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-auditRecord-getById', ...params });
};

/**
 * 获取标准类目关系列表
 * @param {object} params
 * @param {string} params.mainClassId 主营类目id
 */
spAdmin.getSpMainStandardClassRefList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spMainStandardClassRef-list', ...params });
};

// 最高价配置

/**
 * 商品最高价配置新增或修改
 * @param {object} params
 * @param {string} params.confType 应用类型 1-搜索限价 2-上新限价
 * @param {string} params.classId 类目 0=默认通用
 * @param {string} params.cityId 城市 0=默认通用
 * @param {string} params.topPrice 最高价
 * @param {string} params.flag 状态
 */
spAdmin.saveSpTopPriceConfig = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spTopPriceConfig-save', ...params });
};

/**
 * 商品最高价配置查询
 * @param {object} params
 * @param {string} [params.confType] 应用类型 1-搜索限价 2-上新限价
 * @param {string} [params.classId] 类目 0=默认通用
 * @param {string} [params.cityId] 城市 0=默认通用
 * @param {string} params.flag 状态
 */
spAdmin.getSpTopPriceConfigList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spTopPriceConfig-list', ...params });
};

/**
 * 新增修改驳回理由
 * @param {object} params
 * @param {string} [params.id] 编辑时需要
 * @param {string} [params.rejectReason] 理由
 * @param {string} [params.showOrder] 编辑时需要
 */
spAdmin.saveSpRejectReason = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spRejectReason-save', ...params });
};

/**
 * 驳回理由列表
 * @param {object} params
 * @param {string} [params.rejectReason] 驳回理由
 * @param {string} [params.rejectReasonLike] 驳回理由
 * @param {string} [params.flag] 状态 1-正常 0-禁用 -1 删除
 */
spAdmin.getSpRejectReasonList = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spRejectReason-list', ...params });
};

/**
 * 启用驳回理由
 * @param {object} params
 * @param {string} params.id
 */
spAdmin.enableSpRejectReason = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spRejectReason-enable', ...params });
};

/**
 * 禁用驳回理由
 * @param {object} params
 * @param {string} params.id
 */
spAdmin.disableSpRejectReason = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spRejectReason-disable', ...params });
};

/**
 * 删除驳回理由
 * @param {object} params
 * @param {string} params.id
 */
spAdmin.deleteSpRejectReason = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-spRejectReason-delete', ...params });
};

// /**
//  * 设置商品驳回理由
//  * @param {object} params
//  * @param {string} params.id spuId
//  * @param {string} params.rejectReason 拒绝理由
//  */
// spAdmin.updateRejectReason = async function (params) {
//     params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
//     return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateRejectReason', ...params });
// };

// /**
//  * 查看驳回理由
//  * @param {object} params
//  * @param {string} params.id
//  */
// spAdmin.checkRejectReason = async function (params) {
//     params = format.packJsonParam(params);
//     return common.get(spCaps.spgUrl, { apiKey: 'ec-spdresb-dresSpu-checkRejectReason', ...params });
// };


