const caps = require('../../data/caps');

const spCaps = {
    sp_dev: {
        spgMysql: {
            host: '192.168.0.9',
            port: '3366'
        },
        spbMysql: {
            host: '192.168.0.37',
            port: '3366'
        },
    },
    sp_test: {
        esUrl: 'http://172.81.239.141:5200',
        spgMysql: {
            host: '212.64.95.208',
            port: '3366'
        },
        spbMysql: {
            host: '212.64.87.170',
            port: '3366'
        },
    },
    sp_chk: {
        esUrl: 'http://212.64.51.239:5200',
        spgMysql: {
            host: '212.64.57.11',
            port: '3366'
        },
        spbMysql: {
            host: '212.64.43.96',
            port: '3366'
        },
    },
    sp_pre: {
        esUrl: 'http://129.211.38.22:5200',
        spgMysql: {
            host: '212.64.94.66',
            port: '3366'
        },
        spbMysql: {
            host: '212.64.90.68',
            port: '3366'
        },
    },
    sp_online: {
        spdresb: {
            host: '212.64.113.123',
            port: '3366',
            user: 'mychk',
        }
    }
};

const spbDefParams = () => {
    return {
        // saasId: LOGINDATA.saasId || 1,//saas改造 
        _cid: LOGINDATA.clusterCode || 0,
        _tid: LOGINDATA.tenantId || 0
    }
};

/**
 * 是否全局会话
 */
const isSpgSessionId = () => LOGINDATA.sessionId.includes('ctr-');

module.exports = {
    envName: caps.name,
    url: `${caps.url}`,
    confcUrl: `${caps.url}/confc/api.do`,
    // spgUrl: `http://192.168.0.18:8081/spg/api.do`,//sp_dev
    // spbUrl: `http://192.168.0.54:8081/spb/api.do`,//sp_dev
    spgUrl: `${caps.url}/spg/api.do`,
    spbUrl: `${caps.url}/spb/api.do`,
    // spbUrl: `http://152.136.38.172:8081/spb/api.do`,
    wmsbUrl: `${caps.url}/wmsb/api.do`,
    wmsgUrl: `${caps.url}/wmsg/api.do`,
    docUrl: `${caps.url}/doc`,
    // esUrl: spCaps[caps.name].esUrl || '',
    db: spCaps[caps.name],

    /** 版本号 部分业务细节会有所不同 */
    productVersion: '2.9.0',

    spbDefParams,
    isSpgSessionId,
};
