const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');


const bizOrderDetail = module.exports = {};

/**
 * 根据支付ID查入账记录
 * @param {object} params jsonParam
 * @param {string} params.bizSubOrderId 订单编号
 */
bizOrderDetail.getBizOrderDetailBySubBizOrderId = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    // params.sessionId = '';
    return common.get(spCaps.spbUrl, { apiKey: 'ec-acct-bizOrderDetail-getBizOrderDetailBySubBizOrderId', ...params });
};