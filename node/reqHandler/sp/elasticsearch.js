const elasticsearch = require('elasticsearch');

const client = new elasticsearch.Client({
    host: 'http://172.81.239.141:5200',
    // //将日志信息显示在控制台，默认level:"console"
    // log: "console",
    //将日志信息写入文件中
    // log:{
    //     type:'file',
    //     level:"trace",
    //     path:"url"
    // }
    //设置不同等级输出到不同的地方
    // log:[
    //     {
    //         type:'console',
    //         level:"error",
    //     },
    //     {
    //         type:"file",
    //         level:"trace",
    //         path:"url"
    //     }
    // ]
});

/**
 * 分词
 * @param {string} text 
 * @return {Array} tokens
 */
const analyze = async function (text) {
    let tokens = [];
    try {
        tokens = await client.indices.analyze({
            body: {
                analyzer: 'ik_smart',
                text: [text]
            }
        }).then(res => res.tokens);
    } catch (e) {
        tokens = null;
    }
    return tokens;
}

module.exports = {
    analyze
};


// (async function (params) {
//     const res = await analyze('意法新款');
//     console.log(`res=${JSON.stringify(res)}`);

// })();