const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');
const spAccount = require('../../../sp/data/spAccount');
let spAuth = module.exports = {
    url: spCaps.confcUrl
};

/**
 * 衣科员工登录
 * @param {object} params jsonParam
 * @param {string} params.code 工号
 * @param {string} params.pass 密码
 */
spAuth.staffLogin = async function (params = {}) {
    params = format.packJsonParam(Object.assign({}, spAccount.ecStaff, params));
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};

/**
 * 衣科员工退出登录
 */
spAuth.staffLogout = async function (params) {
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogout', ...params });
    LOGINDATA = {};//登出成功后清除登录信息
    return res;
};

/**
 * 保存衣科员工（管理员）
 * @param {Object} params
 */
spAuth.saveStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-saveStaff', ...params });
};

/**
 * 查看员工列表
 * @param {Object} params jsonParam
 */
spAuth.getStaffList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-list', ...params });
};

/**
 * 查询员工详情
 * @param {Object} params
 * @param {Number} params.id
 * @param {Boolean} params.wrapper true: 需要编码转换； false:不需要
 */
spAuth.getStaffInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-getStaff', ...params });
};


/**
 * 启用员工
 * @param {Object} params
 * @param {Number} params.id 员工id
 */
spAuth.enableStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-enable', ...params });
};

/**
 * 停用员工
 * @param {Object} params
 * @param {Number} params.id 员工id
 */
spAuth.disableStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-disable', ...params });
};

/**
 * 修改当前用户密码
 * @param {Object} params
 * @param {string} params.oldPwd 旧密码
 * @param {string} params.newPwd 新密码
 */
spAuth.changePwd = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecCred-changePwd', ...params });
};

/**
 * 查看角色下的员工列表
 * @param {Object} params
 * @param {string} params.roleCode 角色类型，wmsManager仓管
 */
spAuth.getStaffListByRoleCode = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-staffRole-getAllStaffByRoleCode', ...params });
};

//角色管理

/**
 * 保存角色
 * @param {Object} params
 */
spAuth.saveRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-save', ...params });
};

/**
 * 获取角色详情
 * @param {Object} params
 * @param {Number} params.id
 * @param {string} params.wrapper  true: 需要编码转换； false:不需要
 */
spAuth.getRoleInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-get', ...params });
};


/**
 * 获取角色列表
 * @param {Object} params
 */
spAuth.getRoleList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-list', ...params });
};

/**
 * 角色启用
 * @param {Object} params
 * @param {Number} params.id
 */
spAuth.enableRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-enable', ...params });
};

/**
 * 停用角色 
 * @param {Object} params
 * @param {Number} params.id
 */
spAuth.disableRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-disable', ...params });
};

//权限管理

/**
 * 获取权限列表
 */
spAuth.getPowerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFunc-list', ...params });
};

/**
 * 保存角色权限
 * @param {Object} params
 * @param {Number} params.roleId
 * @param {Array}  params.funcIds 权限列表
 */
spAuth.savePower = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spRoleFunc-saveRoleFunc', ...params });
};

/**
 * 获取角色的功能项
 */
spAuth.getPowerInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spRoleFunc-findFuncIdsByRoleId', ...params });
};

/**
 * 获取当前会话所有权限
 */
spAuth.getCurrentPower = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spRoleFunc-getCurrentFunc', ...params });
};


/**
 * 白名单-新增
 * @description 
 * 1. sessionId使用中心区的会话 
 * 2. scopeCap 白名单范围，现在固定1
 * @param {string} mobile 
 */
spAuth.saveWhiteList = async function (mobile) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-save', jsonParam: { mobile, scopeCap: 1 } });
};

/**
 * 白名单-删除
 * @description sessionId使用中心区的会话 
 * @param {object} params
 * @param {string} params.id
 */
spAuth.deleteWhiteList = async function (params) {
    if (!params.hasOwnProperty('id')) {
        console.error(`白名单-删除 参数错误: ${JSON.stringify(params)}`);
    }
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-delete', ...params });
};

/**
 * 白名单列表查询
 * @description sessionId使用中心区的会话 
 */
spAuth.getWhiteList = async function () {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-list' });
};

/**
 * 保存租户的参数
 * @param {Object} params
 * @param {string} params.tenantId 
 * @param {string} params.code 参数代码
 */
spAuth.saveUnitParamVal = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-globalUnitParam-saveUnitParamVal', ...params });
};

/**
 * 查看字典树
 * @param {Object} params
 * @param {string} params.hierId 字典编码，如省市编码为850
 * @param {string} params.codeId parent_code
 */
spAuth.getDictTree = async function (params) {
    return common.post(this.url, { apiKey: 'ec-config-dict-tree', ...params });
};

/**
 * 租户员工登录
 * @param {Object} params
 * @param {string} params.code 工号
 * @param {string} params.pass 密码
 * @param {string} params.tenantCode 租户的code，限定卖家
 */
spAuth.spStaffAuthcLogin = async function (params) {
    params = format.packJsonParam(params);
    params._cid = 0;
    const res = await common.get(spCaps.spbUrl, { apiKey: 'ec-spauth-spStaffAuthc-staffLogin', ...params });
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};

/**
 * 新增修改内部人员信息
 * @param {Object} params
 * @param {string} params.mobile  
 * @param {string} params.typeId 1代表测试人员，2代表内部人员
 * @param {string} params.name
 */
spAuth.saveInnerStaffMsg = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-InnerStaffMsg-saveInnerStaffMsg', ...params });
};

/**
 * 停用启用内部人员信息
 * @param {Object} params
 * @param {string} params.id 内部人员信息id  
 * @param {string} params.flag 0代表停用，1代表启用
 */
spAuth.updateInnerStaffMsgFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-InnerStaffMsg-updateInnerStaffMsgFlag', ...params });
};

/**
 * 查询内部人员信息表
 * @param {Object} params
 * @param {string} params.nameLike 人员名字  
 * @param {string} params.mobileLike 手机
 * @param {string} params.flag 1代表启用，0代表停用，默认查启用状态人员信息
 */
spAuth.findInnerStaffMsg = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-InnerStaffMsg-findInnerStaffMsg', ...params });
};

// 类目集

/**
 * 保存类目集信息 
 * @param {Object} params
 * @param {string} params.id 修改时传
 * @param {string} params.name 类目集名称
 * @param {string} params.flag 0 无效 ，1 有效
 */
spAuth.saveSpCategoryGroup = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroup-save', ...params });
};

/**
 * 类目集列表查询 
 * @param {Object} params
 * @param {string} params.nameLike 名称模糊查询
 * @param {string} params.flag 
 */
spAuth.findSpCategoryGroupList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroup-findPage', ...params });
};

/**
 * 通过id查询类目集信息 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.getSpCategoryGroupById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroup-getById', ...params });
};

/**
 * 保存类目商品集 
 * @param {Object} params
 * @param {string} params.groupId 类目集id
 * @param {string} params.classIds 商品集id，用,隔开
 * @param {string} [params.autoflag] 是否自动添加
 * @param {string} [params.flag] 
 */
spAuth.saveSpCategoryGroupDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroupDetail-saveAndUpdate', ...params });
};

/**
 * 启用类目集 
 * @param {Object} params
 * @param {string} params.id 类目集id
 */
spAuth.enableSpCategoryGroup = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroup-enable', ...params });
};

/**
 * 关闭类目集 
 * @param {Object} params
 * @param {string} params.id 类目集id
 */
spAuth.disableSpCategoryGroup = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroup-disable', ...params });
};

/**
 * 废弃类目商品集 
 * @param {Object} params
 * @param {string} params.groupId 类目集id
 * @param {string} params.classIds 商品id，用,隔开
 */
spAuth.deleteSpCategoryGroup = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spconfig-spCategoryGroupDetail-delete', ...params });
};

// 前台类目

/**
 * 前台类目新增 
 * @param {Object} params
 * @param {string} params.id 修改时传
 * @param {string} params.name 前台类目名称
 * @param {string} params.rem 备注
 */
spAuth.saveSpFrontCategoryAction = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryAction-save', ...params });
};

/**
 * 前台类目列表查询 
 * @param {Object} params
 * @param {string} params.nameLike 商品集名称模糊查询
 * @param {string} params.remLike 备注模糊查询
 * @param {string} params.flag 状态，0 无效 ，1 有效
 */
spAuth.getSpFrontCategoryActionList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryAction-list', ...params });
};

/**
 * 通过id查询前台类目 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.getSpFrontCategoryActionById = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryAction-getById', ...params });
};

/**
 * 前台类目启用 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.enableSpFrontCategoryAction = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryAction-enable', ...params });
};

/**
 * 前台类目停用 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.disableSpFrontCategoryAction = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryAction-disable', ...params });
};


/**
 * 保存前台类目项目信息 
 * @param {Object} params
 * @param {string} params.id 修改时传
 * @param {string} params.name 前台类目名称
 * @param {string} params.rem 备注
 */
spAuth.saveSpFrontCategoryDetailAction = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-save', ...params });
};

/**
 * 前台类目项目列表查询 
 * @param {Object} params
 * @param {string} [params.parentId] 父id
 * @param {string} params.categoryId 前台类目id
 * @param {string} [params.flag] 状态，0 无效 ，1 有效
 * @param {string} [params.nameLike] 类目集名称模糊搜索
 */
spAuth.getSpFrontCategoryDetailActionList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-list', ...params });
};

/**
 * 前台类目项目列表查询 
 * @param {Object} params
 * @param {string} [params.parentId] 父id
 * @param {string} params.categoryId 前台类目id
 * @param {string} [params.flag] 状态，0 无效 ，1 有效
 * @param {string} [params.incDirectSub] 是否子查询，true是，false否
 */
spAuth.getSpFrontCategoryDetailActionListByTree = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-listByTree', ...params });
};

/**
 * 前台类目项目启用 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.enableSpFrontCategoryDetailAction = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-enable', ...params });
};

/**
 * 前台类目项目停用 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.disableSpFrontCategoryDetailAction = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-disable', ...params });
};

/**
 * 更新前台类目项目信息 
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.updateSpFrontCategoryDetailAction = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-update', ...params });
};

/**
 * 前台类目项目删除
 * @param {Object} params
 * @param {string} params.id 
 */
spAuth.deleteSpFrontCategoryDetailAction = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-delete', ...params });
};

/**
 * 初始化前台类目项目信息
 * @description 经洛兴栋确认 本接口已经不再使用 相关脚本已去除
 * @param {Object} params
 * @param {string} params.categoryId 前台类目集id 
 */
spAuth.initSpFrontCategoryDetailAction = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spFrontCategoryDetailAction-initSpCategoryDetail', ...params });
};

// 仓库类目

/**
 * 仓库类目列表 
 * @param {Object} params jsonParam
 * @param {string} params.warehouseNameLike 仓库名字
 * @param {string} params.flag 0-禁用 1-启用
 */
spAuth.getSpSaasCategoryRefList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSaasCategoryRefAction-list', ...params });
};

/**
 * 仓库类目编辑新增 
 * @param {Object} params jsonParam
 */
spAuth.saveSpSaasCategoryRef = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSaasCategoryRefAction-save', ...params });
};

/**
 * 仓库类目启用/禁用 
 * @param {Object} params jsonParam
 * @param {string} params.id 
 * @param {string} params.flag 0-禁用 1-启用
 */
spAuth.changeSpSaasCategoryRefFlag = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSaasCategoryRefAction-changeFlag', ...params });
};

/**
 * 根据仓库id获取类目id 
 * @param {Object} params jsonParam
 * @param {string} params.warehouseId 仓库id
 * @param {string} [params.saasId] 
 */
spAuth.getSpSaasCategoryRefByWareHouseId = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-confg-spSaasCategoryRefAction-getByWareHouseId', ...params });
};

