
// confc
exports.spAuth = require('./confc/spAuth');

//doc
exports.doc = require('./doc_server/doc');

// spg
exports.spAdmin = require('./global/spAdmin');
exports.spbi = require('./global/spbi');
exports.spugr = require('./global/spugr');

// spb
exports.esSearch = require('./biz_server/esSearch');
exports.spconfb = require('./biz_server/spconfb');
exports.spdresb = require('./biz_server/spdresb');
exports.spIM = require('./biz_server/spIM');
exports.spmdm = require('./biz_server/spmdm');
exports.spTrade = require('./biz_server/spTrade');
exports.spUp = require('./biz_server/spUp');

// 测试类
exports.spTest = require('./spTest');
exports.creatSqlPool = require('./mysql');