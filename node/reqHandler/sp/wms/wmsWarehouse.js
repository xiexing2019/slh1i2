const spCaps = require('../spCaps');
const format = require('../../../data/format');
const common = require('../../../lib/common');
const spAccount = require('../../../sp/data/spAccount');

let wmsWarehouse = module.exports = {
    url: spCaps.spgUrl,
};

//仓管员
/**
 * @description 仓管员登陆
 */
wmsWarehouse.wmsStaffLogin = async function (params) {
    params = format.packJsonParam(Object.assign({}, spAccount.wmsStaff, params, { roleCode: 'wmsManager' }));
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};


//仓管人员管理

/**
 * @description 新增骑手
 * @param {Object} params
 * @param {String} params.name 名称
 * @param {String} params.mobile 手机号
 * @param {String} params.idCardNo 身份证
 * @param {String} params.warehouseId  仓库id 
*/
wmsWarehouse.saveUser = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsUser-save', ...params })
};

/**
 * @description 获取人员列表
*/
wmsWarehouse.getUserList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsUser-findByWarehouseAndMarket', ...params })
};

/**
 * @description 删除人员
 * @param {Number} id 
*/
wmsWarehouse.delUser = async function (params) {
    return common.post(this.url, { apiKey: 'ec-wms-wmsUser-delete', ...params })
};


/**
 * @description 用户绑定市场
 * @param {Object} params 
 * @param {String} userId 用户id
 * @param {String} marketId 市场id
*/
wmsWarehouse.saveUserMarket = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsUserMarket-save', ...params })
};

/**
 * @description 解绑
 * @param {Object} params
 * @param {String} params.id
*/
wmsWarehouse.delUserMarket = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsUserMarket-delete', ...params })
};

/**
 * @description 批量绑定和解绑
 * @param {Object}   params
 * @param {String}   userId 用户id
 * @param {marketId} marketId 市场id
 * @param {Number}   flag  新增：1，删除：-1
*/
wmsWarehouse.setUserMarketInBatch = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsUserMarket-saveInBatch', ...params })
};

//仓位管理

/**
 * 创建货架
 * @param {Object} params
 * @param {String} params.shellName 货架名
 * @param {Number} params.horizontal 横
 * @param {Number} params.vertical 纵
*/
wmsWarehouse.saveShelves = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsShelfAction-createShelves', ...params })
};

/**
 * @description 货架列表
 * @param {Object} params
 * @param {String} params.shelfNoPrefix 货架名
*/
wmsWarehouse.getShelvesList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsShelfAction-list', ...params })
};

/**
 * @description 删除货架
 * @param {Object} params
 * @param {String} params.shelfName
*/
wmsWarehouse.delShelf = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsShelfAction-deleteShelf', ...params });
};

/**
 * 创建仓库
 * @param {Object} params
 */
wmsWarehouse.saveWareHouse = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-createWarehouse', ...params });
};

/**
 * 修改仓库
 * @param {Object} params
 */
wmsWarehouse.updateWareHouse = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-updateWarehouse', ...params });
};

/**
 * 查询仓库列表
 * @param {Object} params
 * @param {string} params.name 名字，模糊检索
 * @param {string} params.city 城市编码
 */
wmsWarehouse.getWareHouseList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-spugr-spTenant-findWarehouse', ...params });
};

/**
 * 员工绑定仓库
 */
wmsWarehouse.setStaffWareHouse = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-wms-wmsWhStaff-save', ...params });
};

/**
 * 市场绑定仓库
 * @param {Object} params
 * @param {Number} params.whTenantId
 * @param {Number} params.marketId
 */
wmsWarehouse.bindMarketToWh = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-wmsWhMarket-bindMarketToWarehouse', ...params });
};

/**
 * @description 仓库绑定市场列表
 * @param {Object} params
 * @param {String} marketName 市场名字
 * @param {String} whTenantId 仓库id
 * @param {Number} flag  	1:启用的，0：停用的
*/
wmsWarehouse.getWareHouseMarket = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-wmsWhMarket-findWarehouseMarketList', ...params })
};

/**
 * 修改市场绑定的仓库
 * @param {Number} params.whTenantId
 * @param {Number} params.marketId
 */
wmsWarehouse.updateWareHouseMarket = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-wmsWhMarket-update', ...params })
};

/**
 * 删除市场仓库绑定
 * @param {Object} params
 * @param {Number} params.id
 */
wmsWarehouse.delWareHouseMarket = async function (params) {
    params = format.packJsonParam(params);
    params._master = 1;
    return common.post(this.url, { apiKey: 'ec-spugr-wmsWhMarket-invalid', ...params })
};


/**
 * 今日订单/待办订单/历史订单接口
 * @param {object} params
 * @param {string} params.orderType 1:今日订单 2：待办订单 3：历史订单
 * @param {string} params.unitId 仓库对应的单元ID
 * @param {string} params.warehouseId 仓库ID
 */
wmsWarehouse.getMissionList = async function (params) {
    params = Object.assign(format.packJsonParam({
        unitId: LOGINDATA.unitId,
        warehouseId: LOGINDATA.warehouseId,
        ...params
    }), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsConsigneeMissionAction-getMissionList', ...params })
};

/**
 * @description 仓管员登陆
 */
wmsWarehouse.wmsStaffLogin = async function (params) {
    params = format.packJsonParam(Object.assign({}, spAccount.wmsStaff, params, { roleCode: 'wmsManager' }));
    const res = await common.post(spCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};
