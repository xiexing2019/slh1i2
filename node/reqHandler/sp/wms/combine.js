const spCaps = require('../spCaps');
const format = require('../../../data/format');
const common = require('../../../lib/common');
const spAccount = require('../../../sp/data/spAccount');

// http://106.15.226.60:8080/showdoc/index.php?s=/44
// http://106.15.226.60:8080/showdoc/index.php?s=/Home/Item/show/item_id/51
let combine = module.exports = {};

// 合包查询

/**
 * 合包列表查询
 * @param {Object} params jsonParam
 * @param {string} [params.combBillId] 合包单号
 * @param {string} [params.billNo] 订单号
 */
combine.findCombBills = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-combine-combBillAction-findCombBills', ...params });
};

/**
 * 合单订单明细
 * @param {Object} params 
 * @param {string} params.combBillId 合包单号
 */
combine.getCombBillFull = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-combine-combBillAction-getCombBillFull', ...params });
};

/**
 * 合单商品明细
 * @param {Object} params 
 * @param {string} params.id combDetail的id
 */
combine.getCombDetailInfo = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-combine-combBillDetail-getCombDetailInfo', ...params });
};

/**
 * 创建合包单(mock)
 * @description 测试用 正常流程校验误用
 */
combine.newCombBillByMock = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-combine-combBillAction-newCombBillByMock', ...params });
};



// 云端

/**
 * 能否合包
 * @param {Object} params jsonParam
 * @param {Array} params.marketIdList 市场列表
 */
combine.combinative = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'wms-combine-combinative', ...params });
};

// 仓管端

/**
 * 合单列表接口
 * @param {Object} params jsonParam
 * @param {String} params.unitId 当前仓库单元ID
 * @param {String} params.combType 1:配货中 2:已配齐 3:超时订单 4:异常处理
 */
combine.getCombBillList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-wms-combine-combBillAction-getCombBillList', ...params });
};


// 卖家端

/**
 * 卖家备货完成
 * @param {Object} params jsonParam
 * @param {String} params.unitId 仓库unitId
 * @param {String} params.tenantId 仓库tenantId
 * @param {String} params.warehouseId 仓库id
 * @param {String} params.salesBillId 销售单id
 * @param {String} params.billType 目前固定填1
 */
combine.combBillPrepared = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-wms-combine-combBillDetailAction-prepared', ...params });
};
