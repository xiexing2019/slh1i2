const spCaps = require('../spCaps');
const format = require('../../../data/format');
const common = require('../../../lib/common');
// const spAccount = require('../../../sp/data/spAccount');

// http://106.15.226.60:8080/showdoc/index.php?s=/44
// http://106.15.226.60:8080/showdoc/index.php?s=/Home/Item/show/item_id/51
let wmsStorage = module.exports = {};

// 骑手相关

/**
 * 收货数统计
 * @param {Object} params 
 * @param {string} params.riderId 骑手ID
 * @param {string} params.unitId 骑手所属仓库unitId
 */
wmsStorage.getParcelRiderStatistic = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelRider-getStatistic', ...params });
};

/**
 * 收货列表
 * @param {Object} params jsonParam
 * @param {string} params.riderId 骑手ID
 * @param {string} params.unitId 骑手所属仓库unitId
 * @param {string} params.searchFlag 包裹状态筛选(可默认传10),1待取货(不包括取货超时),2取货超时,3待入库已取货,4已入库,5已取消,10待取货(包括取货超时)
 */
wmsStorage.getParcelRiderMissionList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelRider-getMissionList', ...params });
};

/**
 * 查询包裹详情
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 */
wmsStorage.getParcelFull = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelRider-getParcelFull', ...params });
};

/**
 * 包裹确认取货
 * @param {Object} params jsonParam
 * @param {string} params.id parcelBill的id
 * @param {string} params.pickNum 确认总取货数量
 * @param {Object[]} params.parcelDetails 包裹明细
 * @param {string} params.parcelDetails[].id parcelDetail的id
 * @param {string} params.parcelDetails[].pickNum 确认取货数量（不能为负数，不能超过当前数量）
 */
wmsStorage.confirmPick = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-confirmPick', ...params });
};

// 取货管理

/**
 * 包裹创建
 * @param {Object} params jsonParam
 */
wmsStorage.createParcelBill = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsParcelBillAction-createFull', ...params });
};

/**
 * 包裹创建(mock)
 * @param {Object} params jsonParam
 */
wmsStorage.createParcelBillMock = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-createFullMock', ...params });
};

/**
 * 查询包裹列表
 * @param {Object} params 
 * @param {string} params.searchFlag 包裹状态筛选(可默认传10),1待取货(不包括取货超时),2取货超时,3待入库已取货,4已入库,5已取消,10待取货(包括取货超时)
 * @param {string} [params.parcelBillNo] 包裹号
 * @param {string} [params.collectionNo] 收货号
 * @param {string} [params.salesBillNo] 订单编号
 */
wmsStorage.getParcelBillList = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-findBill', ...params });
};

/**
 * 查询包裹详情
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 */
wmsStorage.getParcelBillFull = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-getFull', ...params });
};

/**
 * 包裹分配骑手
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 * @param {string} params.riderId 骑手的id
 */
wmsStorage.allotParcelBillRider = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-allotRider', ...params });
};

/**
 * 包裹入库
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 * @param {string} params.type 入库选择货架号类型：0或空 系统分配，1 手动选择
 * @param {string} params.shelfId 货架号的id, 当手动选择时候，必传
 */
wmsStorage.consigneeParcelBill = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-consignee', ...params });
};

/**
 * 包裹取消
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 * @param {string} params.cancelRem 取消的原因理由
 */
wmsStorage.cancelParcelBill = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-cancelBill', ...params });
};

/**
 * 包裹删除
 * @param {Object} params 
 * @param {string} params.id parcelBill的id
 */
wmsStorage.deleteParcelBill = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-parcelBill-deleteBill', ...params });
};




/**
 * 确认收货
 * @description 废弃
 * @param {Object} params jsonParam
 * @param {string} params.id missionId
 * @param {string} params.unitId 骑手所属仓库unitId
 * @param {string} params.consigneeId 收货人ID
 */
wmsStorage.getGoods = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsConsigneeMissionAction-getGoods', ...params });
};

/**
 * 今日订单/待办订单/历史订单
 * @description 废弃
 * @param {Object} params jsonParam
 * @param {string} params.orderType 1:今日订单 2：待办订单 3：历史订单
 * @param {string} params.unitId 仓库对应的单元ID
 * @param {string} params.warehouseId 仓库ID (仓管登录后的tenantId)
 */
wmsStorage.getManageMissionList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.wmsbUrl, { apiKey: 'ec-wms-storage-wmsConsigneeMissionAction-getManageMissionList', ...params });
};

