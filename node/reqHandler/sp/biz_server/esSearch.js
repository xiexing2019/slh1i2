'use strict';
const common = require('../../../lib/common');

let reqHandler = module.exports = {
    url: 'http://192.168.0.38:8081/spb/api.do',
};

//商品类别

/**
 * 商品搜索
 * @description 内部私有接口，不对外提供公开调用，仅用于测试
 */
reqHandler.searchList = async function (params) {
    return common.post(this.url, { apiKey: 'ec-spes-esSearchService-search', ...params });
};