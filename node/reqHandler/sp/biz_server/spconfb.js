'use strict';
const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spconfb = module.exports = {
    url: spCaps.spbUrl,
};

/**
 * 新增运费规则
 * @param {object}  jsonParam 保存运费的参数
 * 该接口 修改，删除，保存都用此接口
 * */

spconfb.saveFeeRule = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spconfb-freightManage-saveFeeRuleInBatch', ...params });
};


/**
 * 删除运费规则，
 * 此接口已经废弃，通过ec-spconfb-freightManage-saveFeeRuleInBatch这个接口进行删除
*/
// spconfb.deleteFeeRule = async function (params) {
//     params._cid = LOGINDATA.clusterCode;
//     params._tid = LOGINDATA.tenantId;
//     params = format.packJsonParam(params);
//     return common.post(this.url, { apiKey: 'ec-spconfb-freightManage-deleteFeeRule', ...params });
// };

/**
 * 查看运费规则列表
 * @param {object} jsonParam 
 * @param {Integer} params
*/
spconfb.findFeeRuleList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spconfb-freightManage-findFeeRuleList', ...params });
};

/**
 * 保存运费、包邮规则
*/
spconfb.saveShipRule = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spconfb-spFeeRule-saveShipRuleData', ...params });
};

/**
 * 获取运费、包邮规则数据
*/
spconfb.findShipRule = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spconfb-spFeeRule-getShipRuleData', ...params });
};

/**
 * 估算运费
 * @param {object} jsonParam
 * @param {String} params.provinceCode 收货地址省份代码
 * @param {object} params.orders 订单列表,list结构
 * @param {Long} params.orders.sellerId 卖家租户id
 * @param {object} params.orders.orderSpus 订货商品SPU列表。list数据结构
 */
spconfb.evalShipFee = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-freightManage-evalShipFee', ...params });
};

/**
 * 运费合包计算
 * @param {object} params jsonParam
 * @param {Array} params.orders 订单列表
 * @param {string} params.orders.sellerId 卖家租户id
 * @param {Array} params.orders.orderSpus 订货商品SPU列表
 * @param {string} params.orders.orderSpus.spuId 商品SPU ID
 * @param {string} params.orders.orderSpus.orderNum 订货数量
 * @param {string} params.orders.orderSpus.orderMoney 订货金额
 */
spconfb.defevalShipFee = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-freightManage-defevalShipFee', ...params });
};

/**
 * 估算商品运费-分销
 * @description 分销模式估算商品运费（买家）—-强制合包
 * @param {object} jsonParam
 * @param {String} params.provinceCode 收货地址省份代码
 * @param {object} params.orders 订单列表,list结构
 * @param {Long} params.orders.warehouseId 仓库id
 * @param {object} params.orders.orderSpus 订货商品SPU列表。list数据结构
 */
spconfb.evalDisShipFee = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-freightManage-evalDisShipFee', ...params });
};

/**
 * 获取类别运输属性列表（卖家）
 * @param {object}  jsonParam
 * @param {Long}    params.parentId  上级类别。不传查所有
*/
spconfb.findClassShipProps = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-spClassShipProp-findShipPropsClassList', ...params });
};

/**
 * 卖家查询运输属性类别树
 * @param {object}   jsonParam
 * @param {long} params.parentId 上级类别。查询顶层时传0或不传。
 * @param {String} params.incDirectSub
 * @param {nameLike} params.name 名称 
*/
spconfb.findShipPropsClassTree = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-spClassShipProp-findShipPropsClassTree', ...params });
};

/**
 * 物流商查询(单元区)
 * @param {object} jsonParam 物流查询参数
 * @param {String} params.stdSource 2 ‘第三方公司标准数据，1表示快递100,2表示快递鸟,3表示商陆花支持的物流商’
 * @param {String} params.name 物流商名称
*/
spconfb.findLogisList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfb-stdlogiscompany-list', ...params });
};

/**目前物流商列表查询用的就是这个接口
 * 物流商查询，优先级高(单元区)
 * @param {object} jsonParam 物流查询参数
 * @param {Integer} params.cap 1 固定值
 * @param {String} params.name 物流商名称
*/
spconfb.findLogisList1 = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spconfb-logis-provider-list', ...params });
};

/**
 * 物流轨迹
 * @param {object} jsonParam
 * @param {String} params.waybillNo 物流单号
 * @param {String} params.logisCompId 快递公司id
*/
spconfb.getLogisTrace = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-spLogisTrack-getTrackInfo', ...params });
};

/**
 * 手动修改物流单状态
 * @description 自动化使用
 * @param {object} params
 */
spconfb.changeLogisStatus = async function (params) {
    params = format.packJsonParam(params);
    return common.post(`${spCaps.url}/logis/api.do`, { apiKey: 'ec-logis-logisticsTrack-updateTestLogisBill', ...params });
};





