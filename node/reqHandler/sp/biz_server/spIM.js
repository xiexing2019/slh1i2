const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spIM = module.exports = {};

/**
 * 开始沟通
 */
spIM.startConv = async function (params) {
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcsb-nimCustSvcComb-startConv', ...params });
};

/**
 * 拉取im登陆信息
 */
spIM.getNimLoginInfo = async function (params = {}) {
    params._master = 1;
    params.sessionId = LOGINDATA.spSessionId || LOGINDATA.sessionId;
    return common.get(spCaps.spgUrl, { apiKey: 'ec-cust-spNimAccount-getNimLoginInfo', ...params });
};

/**
 * 重新开始聊天
 */
spIM.reStartConv = async function (params) {
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcsb-nimCustSvcComb-reStartConv', ...params });
};


/**
 * 模拟聊天消息
 * @description 测试内部使用
 * @param {object} params jsonParam
 * @param {object} params.body 消息属性
 * @param {string} params.mobilesTo 接收消息的手机列表，多个以逗号分隔
 * @param {string} params.tenantId 消息发送者的租户id
 */
spIM.testTransferMsg = async function (params) {
    if (!params.hasOwnProperty('tenantId')) params.tenantId = LOGINDATA.tenantId;
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-ugr-wxMp-testTransferMsg', ...params });
};

/**
 * 平台客服介入会话
 * @param {object} params
 * @param {string} params.teamId 易信群id
 * @param {string} params.salesUnitId 卖家单元ID 
 * @param {string} params._cid 卖家集群
 * @param {string} params._tid 卖家租户ID
 */
spIM.testTransferMsg = async function (params) {
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spcsb-nimCustSvcComb-joinPlatCustSvc', ...params });
};