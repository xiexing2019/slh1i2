const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spmdm = module.exports = {};

/**
 * 保存用户收货地址信息
 * @param {object} params 
 * @param {object} params.recInfo 用户联系信息结构
 * @param {address} params.address 收货地址信息结构
 */
spmdm.saveUserRecInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-saveUserRecInfo', ...params });
};

/**
 * 设置默认用户收货地址信息
 * @param {object} params 
 * @param {object} params.id 用户收货地址信息ID
 */
spmdm.setDefaultUserRecInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-setDefaultUserRecInfo', ...params });
};

/**
 * 获取用户收货地址列表
 */
spmdm.getUserRecInfoList = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-getUserRecInfoList', ...params });
};

/**
 * 根据id查询用户收货地址的详细地址
 * @param {object} jsonParam
 * @param {Long} params.id 用户联系信息id
*/
spmdm.getUserRecInfoById = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-getUserRecInfoById', ...params });
};

/**
 * 删除用户收货地址列表
 * @param {object} jsonParam
 * @param {Long} params.id 用户联系信息id
 */
spmdm.deleteUserRecInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-deleteUserRecInfo', ...params });
};

/**
 * 获取客户默认收货地址
 * **/
spmdm.getUserDefaultRecInfo = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-getUserDefaultRecInfo', ...params });
};

/**
 * 批量删除用户收货地址信息
 * @param {object} jsonParam
 * @param {String} params.ids 用户收获地址id，之间用逗号隔开
 **/
spmdm.deleteUserRecInfoInBatch = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserManage-deleteUserRecInfoInBatch', ...params });
};

/**
 * 注册绑定embGuid
 * @description productCode 买家slhGoodShopIOS  卖家ipadslh
 */
spmdm.registerUserMessageCid = async function (params = {}) {
    params = format.packJsonParam({ machineCode: 'autotest', productCode: 'slhGoodShopIOS', productVersion: '1.0.1', productType: 2, ...params });
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserGuid-registerUserMessageCid', ...params });
};

/**
 * 查询消息列表
 * @param {object} params 
 * @param {string} params.tagOneIn 客户端支持的所有类型（逗号分隔），默认70,71,80
 */
spmdm.pullMessagesList = async function (params = {}) {
    await common.delay(500);
    params = format.packJsonParam({
        bizType: 0,
        ...params
    });
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserGuid-pullMessagesList', ...params });
};

/**
 * 查看未读消息数
 * @param {object} params 
 * @param {string} params.tagOneIn 客户端支持的所有类型（逗号分隔），默认70,71,80
 */
spmdm.getPullUnreadCount = async function (params = {}) {
    params = format.packJsonParam({
        bizType: 0,
        ...params
    });
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserGuid-pullUnreadCount', ...params });
}

/**
 * 更新消息为已读
 * @param {object} params 
 * @param {array} params.msgIds 消息ID 多个用逗号隔开
 */
spmdm.updateReadStatus = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserGuid-updateReadStatus', ...params });
};

/**
 * 更新所有未读消息为已读
 * @param {object} params 
 */
spmdm.updateAllUnreadMsgs = async function (params = {}) {
    params = Object.assign(format.packJsonParam({ bizType: 0, ...params }), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spmdm-spUserGuid-updateAllUnreadMsgs', ...params });
};