const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spDresb = module.exports = {};

//商品类别

/**
 * 查询商品类别树(卖家)
 * @param {object} params
 * @param {number} params.parentId 上级类别。查询顶层时传0或不传
 * @param {boolean} params.includeSubs 是否取所有下级节点 默认为false
 * @param {number} params.isGlobal 是否查询全局商品类别
 */
spDresb.findClassListForSeller = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresClassFavor-codes', ...params });
};

/**
 * 查询商品类别列表(平台)
 * @param {object} params
 * @param {number} params.parentId 上级类别。查询顶层时传0或不传
 * @param {boolean} params.includeSubs 是否取所有下级节点 默认为false
 * @param {number} params.isGlobal 是否查询全局商品类别
 */
spDresb.findClassList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-config-dresClass-findList', ...params });
};

/**
 * 查询商品类别树列表(买卖家用)-租户
 * @param {object} params
 * @param {number} params.parentId 上级类别。查询顶层时传1或不传
 * @param {boolean} params.incDirectSub 是否取所有下级节点 默认为false
 * @param {number} params.showFlag * 1-卖家可见 2-买家可见 3-卖家买家都可见
 */
spDresb.findClassConfigList = async function (params) {
    if (!params.unitId) params.unitId = LOGINDATA.unitId;
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresClassConfig-treeList', ...params });
};

/**
 * 查询全局区商品类别树列表
 * @param {object} params
 * @param {number} params.parentId 上级类别。查询顶层时传0或不传
 * @param {boolean} params.incDirectSub 是否取直属下级类别 默认为false
 * @param {string} params.name 名称(模糊搜索名称和拼音)
 */
spDresb.findClassTreeList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-config-dresClass-treeList', ...params });
};

/**
 * 查询商品类别配置列表
 * @param {object} params
 * @param {number} params.parentId 上级类别。查询所有时不传
 * @param {number} params.name 名称（模糊搜索名称和拼音）
 * @param {number} params.flag 状态
 */
spDresb.dresClassConfigList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-config-dresClassConfig-findList', ...params });
};

/**
 * 查询商品类别属性定义
 * @param {object} params
 * @param {number} params.classId 商品类别id
 */
spDresb.findByClass = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdresg-dresClassProp-findByClass', ...params })
};

/**
 * 卖家查询运输属性类别树
 * @param {object} params
 * @param {string} params.parentId 上级类别。查询顶层时传1
 * @param {boolean} params.incDirectSub 是否取下级类别。true/false，默认为false。false 仅查询上级类别为parentId的一层类别。true 查询上级类别为parentId的类别和每个类别的子类别
 * @param {string} params.nameLike 名称（模糊搜索名称和拼音）
 */
spDresb.findShipPropsClassTree = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-spClassShipProp-findShipPropsClassTree', ...params })
};

/**
 * 保存常用商品类别(卖家)
 * @param {object} params
 * @param {string} params.codes 商品类别id
 */
spDresb.saveLikeClass = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresClassFavor-saveLikeClass', ...params })
};

/**
 * 获取常用商品类别(卖家)
 * @param {object} params
 * @param {string} params.code 商品类别id
 */
spDresb.findLikeClass = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresClassFavor-findLikeClass', ...params })
};

//租户商品

/**
 * 商品全量同步到es
 * @param {object} params
 * @param {string} params.unitId 需要同步商品所属的单元id
 */
spDresb.exportEs = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-spu-start-exportEs', ...params });
};

/**
 * 保存商品完整信息
 * @param {object} params
 */
spDresb.saveFull = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    // return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-saveFull', ...params });
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-saveFull', ...params });
};

/**
 * 修改卖家商品
 * @description 修改的信息不会影响slh的商品信息(为了方便前端专门放出来的接口)
 * @param {object} params
 * @param {string} params.id 租户商品ID
 */
spDresb.updateSpuForSeller = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateSpuForSeller', ...params });
};

/**
 * 校验买家下单时间是否在卖家放假时间内
 * @description idTenantBeans不传默认当前卖家
 * @param {object} params
 * @param {Array} params.idTenantBeans
 * @param {string} params.idTenantBeans.tenantId
 * @param {string} params.idTenantBeans.unitId
 * @param {string} params.idTenantBeans.tenantName
 */
spDresb.verifyProTime = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-verifyProTime', ...params });
};

/**
 * 商品下架
 * @param {object} params
 * @param {Object} params.ids list。SPU ID列表 例如：{“ids”:[1,2,3,4,5,6]}
 */
spDresb.offMarket = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-offMarket', ...params });
};

/**
 * 设置商品审核状态接口
 * @param {object} params
 * @param {string} params.tenantId 商品所在的租户id
 * @param {string} params.unitId 商品所在的单元id
 * @param {string} params.id 商品id
 * @param {string} params.flag 0去除审核状态 1已审核
 */
spDresb.updateSpuCheckFlag = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateSpuCheckFlag', ...params });
};

/**
 * 商品上架（卖家）
 * @param {object} params
 * @param {object} params.ids list。SPU ID列表 例如：{“ids”:[1,2,3,4,5,6]}
 */
spDresb.onMarket = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-onMarket', ...params });
};

/**
 * 提交被管理员下架的商品重新上架申请
 * @param {Object} params
 * @param {Array}  params.spuIds 以逗号分隔的申请上架商品ID列表
 * @param {reqRem} params.reqRem 申请上架说明
*/
spDresb.saveOnMarketReq = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-createOnmarketReq', ...params });
};

/**
 * 查询上架申请列表
 * @param {Object} params
 * @param {date}   params.proDateGte 申请日期起（格式：yyyy-MM-dd)
 * @param {date}   params.proDateLte 申请日期止（格式：yyyy-MM-dd)
 * @param {Integer} params.flag 状态(2 待审核，3 审核通过，99 审核不通过)
*/
spDresb.findOnMarketReqList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findOnmarketReq', ...params });
};

/**
 * 查询商品上架申请详情
 * @param {string} id 申请id
*/
spDresb.findOnMarketReqDetail = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-getOnmarketReqById', ...params });
};

/**
 * 平台商品批量上下架
 * @param {Object} params
 * @param {Object} params.unitId 卖家单元id
 * @param {Object} params.tenantId 卖家租户id
 * @param {Object} params.flag 状态：1 上架，2 待上架，0 下架
 * @param {Object} params.opType 操作类型：1 上架，0 下架
 * @param {Object} params.id 商品id，执行商品上下架 不是必填
*/
spDresb.autoDealDresSpuMarket = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-autoDealDresSpuMarket', ...params });
};

/**
 *  买家提醒卖家上架商品
 * @param {Object} params
 * @param {Object} params.buyerId 	买家租户id
*/
spDresb.remindSeller = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-remindSeller', ...params });
};

/**
 * 查询租户商品信息
 * @param {object} params
 * @param {string} params.id 租户商品ID
 */
spDresb.getFullById = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-getFullById', ...params });
};

/**
 * 卖家商品spu信息修改
 * @param {object} params
*/
spDresb.editDresSpu = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-editDresSpu', ...params });
};

/**
 * 卖家商品sku库存添加
 * @param {object} params
 * @param {Array} params.skus sku集合
 */
spDresb.addDresSkuNum = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-addDresSkuNum', ...params });
};

/**
 * 卖家查询商品列表
 * @param {object} params
 * @param {string} params.flags 商品状态列表，逗号隔开，-4 卖家下架 -2 平台下架， 0 无效（商陆花下架），1 有效，2 待上架，不传显示全部
 */
spDresb.findSellerSpuList = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findSellerSpuList', ...params })
};

/**
 * 管理员修改商品类别
 * @param {object} params
 * @param {string} params._cid 要修改的商品的clusterCode
 * @param {string} params.unitId 商品归属的单元id
 * @param {string} params.tenantId 商品归属的租户id
 * @param {string} params.id 租户商品ID/spuId
 */
spDresb.updateSpuClassByAdmin = async function (params) {
    if (params.hasOwnProperty('platSeason')) {
        params.props = { platSeason: params.platSeason };
        // delete params.platSeason;
    }
    params = format.packJsonParam(params, ['unitId', 'tenantId', '_cid']);
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateSpuForWeb', ...params });
};

/**
 * 卖家设置运输属性
*/
spDresb.saveShipProps = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-spClassShipProp-saveShipPropsInBatch', ...params })
};

/**
 * 获取商品运输属性
 */
spDresb.getShipProp = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-spClassShipProp-getObjShipProp', ...params })
};

/**
 * 买家查询商品详情
 * @description
 * 1. 根据全局商品搜索所得的detailUrl拼接参数 使用卖家的_cid,_tid
 * 2. 2.0.8版本 添加最优活动字段
 * @param {object} params jsonParam
 * @param {string} params.spuId 租户商品ID
 * @param {string} params.buyerId 买家的租户ID
 */
spDresb.getFullForBuyer = async function (params = {}) {
    params.cacheFlag = 0;
    params.productVersion = spCaps.productVersion;
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-getFullForBuyer', ...params });
};

/**
 * 买家收藏商品详情
 * @param {Objcet} params
 * @param {String} params.id 商品spuId
*/
spDresb.searchFavorDresById = async function (params) {
    // params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-searchFavorDresById', ...params });
};

/**
 * 买家收藏列表
 * @description 2.0.8 展示隐藏的商品
 */
spDresb.getMyFavorDres = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    params.productVersion = spCaps.productVersion;
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-searchMyFavorDres', ...params });
};


/**
 * 查询订单预览的商品信息
 * @param {object} params
 * @param {string} params.tenantId 卖家租户ID
 * @param {string} params.buyerId 买家租户ID
 * @param {object} params.skus 订货商品SKU列表。list数据结构
 */
spDresb.getForOrderPreview = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-getForOrderPreview', ...params });
};

/**
 * 卖家变更价格类型到发布价
 * @param {object} params
 * @param {string} params.tenantId 卖家租户ID
 * @param {string} params.priceType 价格类型(取值1~5，分别对应价格1到价格5)
 * @param {string} params.discount 折扣
 * @param {string} params.discountType 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认 1
 */
spDresb.changePubPriceByPriceType = async function (params) {
    params = Object.assign({
        tenantId: LOGINDATA.tenantId,
    }, params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-changePubPriceByPriceType', ...params });
};

/**
 * web查询卖家商品列表
 * @param {object} params
 * @param {string} params.flags 商品状态列表，逗号隔开，-4 卖家下架 -2 平台下架， 0 无效（商陆花下架）， 1 有效，2 待上架，不传显示全部
 * @param {string} params.unitId 单元unitId
 */
spDresb.findWebSellerSpuList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findWebSellerSpuList', ...params });
};

/**
 * web更新商品排序
 * @param {Array} params
 * @param {string} params.id 租户商品ID
 * @param {string} params.showOrder 租户商品排序（创建商品默认为0），正序，越大越后面
 */
spDresb.updateWebDresSpuShowOrder = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateWebDresSpuShowOrder', ...params });
};

/**
 * 提供给订单系统http查询混批量
 * @description 兼容老混批，若新的与起批价相关的新混批开启且混批规则真正使用则老混批失效
 * @param {object} params
 * @param {string} params.shopId 店铺id
 * @param {object[]} params.dresSpuMixBatchDTOs
 * @param {string} params.dresSpuMixBatchDTOs.spuId spuId
 * @param {object[]} params.dresSpuMixBatchDTOs.dresSkuBatchDTO
 * @param {string} params.dresSpuMixBatchDTOs.dresSkuBatchDTO.skuId skuId
 */
spDresb.findMixBatchNumForSales = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spdresb-dresSpu-findMixBatchNumForSales', ...params });
};

//全局商品

/**
 * 全局商品搜索
 * @description 平台端查询使用游客查询
 * @param {object} params
 * @param {string} params.id 租户商品ID
 * @param {Number} [params.needWait] 是否需要等待 1--需要  0--不需要
 */
spDresb.searchDres = async function (params) {
    params = Object.assign({ shadowFlag: 2, queryType: 0 }, params);

    if (params.needWait == 1) {
        await common.delay(2000);
        delete params.needWait;
    };
    // es内部接口
    // params = format.packJsonParam(params);
    // return common.post(spCaps.spgUrl, { apiKey: 'ec-spessearch-dresSearch-searchDres', ...params });

    //排除爆款
    if (params.queryType != 3) {
        params.endTime = common.getCurrentTime(); //为了免等1分钟延迟
    };
    if (params.queryType != 4) {
        params.endUpdatedTime = common.getCurrentTime();
    };

    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), spCaps.spbDefParams());
    if (params._tid == 0 && params._cid == 0) {
        const sessionId = LOGINDATA.sessionId;
        LOGINDATA.sessionId = '';
        const res = await common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-searchDres', ...params });
        LOGINDATA.sessionId = sessionId;
        return res;
    };
    const res = common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-searchDres', ...params });
    if (res.duration > 1500) {
        console.log(`optime=${res.opTime}\nurl=${res.reqUrl}\nduration=${res.duration}`);
    }
    return res;
};

spDresb.searchDresInc = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spessearch-dresSearch-searchDres', ...params });
}

/**
 * 首页商品门店聚合搜索(相当于es商品搜索+门店混合搜索)
 * @param {Object} jsonParam
 * @param {String} searchToken 搜索串。搜索商品标题和拼音内容
 */
spDresb.searchDresAndShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopSearch-searchDresAndShop', ...params });
};


spDresb.esBaseSearch = async function (params) {
    let _response = {};
    await common.superagent.post(`${spCaps.esUrl}/dres_style_sp_base/_search`)
        .timeout(0)
        .send(params)
        .then((response) => {
            //response.status
            // let date = moment.utc(response.header.date);
            // header = response.header;
            // result = response.body;
            _response = JSON.parse(response);
        }).catch((err) => {
            // err.message, err.response
            // console.log(`err=${JSON.stringify(err)}`);
            // err.response.req.url = decodeURI(err.response.req.url);
            throw new Error(`请求失败:\n${JSON.stringify(err)}`);
        });
    return _response;
};


//商品增量 spdresb001 sp_stat

/**
 * 商品查看数自增(记录)
 * @param {object} params jsonParam
 * @param {string} params.spuId 商品spu的ID
 * @param {string} params.sellerId 卖家租户ID
 * @param {string} params.sellerUnitId 卖家单元ID
 */
spDresb.saveBuyerView = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerView-saveBuyerView', ...params });
};

/**
 * 商品点赞数自增(记录)
 * @param {object} params jsonParam
 * @param {string} params.spuId 商品spu的ID
 * @param {string} params.sellerId 卖家租户ID
 * @param {string} params.sellerUnitId 卖家单元ID
 */
spDresb.saveBuyerPraise = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerPraise-saveBuyerPraise', ...params });
};

//爆款频道

/**
 *  卖家设置爆款
 * @param {object} jsonParam
 * @param {Object} param.config  爆款设置集合
 * @param {object} param.configDetails 爆款设置列表
 *
*/
spDresb.saveDresHotConfig = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresHotConfig-saveDresHotConfig', ...params });
};

/**
 * 卖家获取爆款设置信息
 */
spDresb.getDresHotConfig = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresHotConfig-getDresHotConfig', ...params });
};

//今日上新门店列表

/**
 * 今日上新门店列表
 * @param {Object} jsonParam
 * @param {Integer} param.showSpus 是否显示新款或爆款，1 显示，0或者不传 不显示
 * @param {string}  param.nameLike 店铺名模糊搜索
 */
spDresb.getTodayNewShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-shopSearch-findTodayNewShops', ...params });
};

/**
 * 好店列表（买家游客）
 * @param {object} params
 * @param {Integer} params.showSpus 是否显示新款或爆款，1 显示，0或者不传 不显示
*/
spDresb.getGoodShopByBuyer = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-shopSearch-goodShopsForBuyer', ...params });
};

/**
 * 首页数据统计
 */
spDresb.findHomePageData = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-shopSearch-findHomePageStatistics', ...params });
};

/**
 * 今日上新统计
 */
spDresb.getTodayNewDresData = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-statTodayNewDres', ...params });
};

/**
 * 获取买家城市及推荐城市--买家登录
 */
spDresb.getRecommendCity = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-shopSearch-getRecommendCity', ...params });
};

/**
 * 查询搜索关联词列表
 * @param {object} params
 * @param {string} params.searchToken 搜索串
 */
spDresb.searchAssoWord = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopSearch-searchAssoWord', ...params });
};



// 分享

/**
 * 通过商品门店分享信息生成口令
 * @param {object} params
 * @param {Integer} params.type 类型，商品-101，门店-102
 */
spDresb.getSharePasswordByParam = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopShare-getPasswordByParam', ...params });
};

/**
 * 通过口令获取商品门店分享信息
 * @param {object} params
 * @param {Integer} params.password 分享的口令
 */
spDresb.getShareDtoByPassword = async function (params) {
    params = Object.assign(spCaps.spbDefParams(), params);
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopShare-getDtoByPassword', ...params });
};

/**
 * 通过商品门店分享信息生成口令二维码
 * @param {object} params
 * @param {Integer} params.type 类型，商品-101，门店-102
 */
spDresb.getShareQrCodeByParam = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    params.jsonParam = JSON.stringify(params.jsonParam);
    params.apiKey = 'ec-spchb-dresShopShare-getQrCode';
    params.sessionId = LOGINDATA.sessionId;
    await common.superagent.get(`${spCaps.url}/spb/stream.do`)
        .timeout(0)
        .type('form')
        .query(params)
        .accept('image/png')
        .then(response => {
            // console.log(`response=${JSON.stringify(response)}`);
            expect(response.header).to.includes({ 'content-disposition': 'attachment;filename=qrCode' });
        })
        .catch(err => {
            throw new Error(`请求失败:\n${JSON.stringify(err)}`);
        });
};

/**
 * 生成商品门店小程序分享二维码的BASE64字符串
 * @param {object} params
 * @param {string} params.page 分享商品-“goods”，分享门店-“shop”
 * @param {string} params.tenantId 租户id，卖家租户id，即卖家门店id
 * @param {string} params.spuId 商品的款号id
 */
spDresb.getShareQrCodeBase64ForMiniProgram = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopShare-getShareQrCodeBase64ForMiniProgram', ...params });
};

/**
 * 生成分享商品二维码的BASE64字符串
 * @param {object} params
 * @param {string} params._cid 集群id
 * @param {string} params._tid 租户id
 * @param {string} params.spuId 商品的款号id
 */
spDresb.getDresShareQrCodeBase64 = async function (params) {
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopShare-getDresShareQrCodeBase64', ...params });
};

/**
 * 通过商品门店分享信息生成口令二维码
 * @param {object} params
 * @param {string} params.shopId 门店id
 */
spDresb.getShopShareSMS = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresShopShare-getShopShareSMS', ...params });
};

/**
 * 获取我的商品分享列表
 * @description 弃用 功能集成到全局商品搜索 queryType=13
 * @param {object} params
 * @param {string} params.shopId 门店id
 */
spDresb.getDresShareList = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-shareSpu-findByParams', ...params });
};

/**
 * 新增通过分享进来的店铺记录
 * @description 自动化新增数据用
 * @param {object} params
 * @param {string} params.srcType 分享来源
 * @param {string} params.srcId 分享来源id
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} params.spuId 款号id
 */
spDresb.createShareIn = async function (params = {}) {
    params.srcId = LOGINDATA.tenantId;
    // params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spgUrl, { apiKey: 'ec-ugr-wxMp-createShareIn', ...params });
};

/**
 * 通过分享访问过的店铺
 * @param {object} params
 * @param {string} params.shopId 门店id
 */
spDresb.getShareInShopList = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-shareInShop-listShareInShop', ...params });
};

/**
 * 停用我的分享的门店记录
 * @param {object} params
 * @param {string} params.shopId 分享门店id
 */
spDresb.disableShareIn = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-shareInShop-disable', ...params });
};

// 商品尺码表

/**
 * 卖家货品更新尺码图
 * @param {object} params
 * @param {string} params.id spuId
 */
spDresb.updateSpuYardage = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateSpuYardage', ...params });
};

/**
 * 查询所有可用尺码表
 * @param {object} params
 */
spDresb.findDresYardageList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSellerYardage-findDresYardageList', ...params });
};

/**
 * 新增/修改尺码表
 * @param {object} params
 * @param {string} params.name 名字
 * @param {object|string} params.docId 文档id
 * @param {string} params.classId 商品类型
 * @param {object} params.props 尺码数据
 */
spDresb.saveOrUpdateYardage = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSellerYardage-saveOrUpdateYardage', ...params });
};

/**
 * 获取单条记录详情
 * @param {object} params
 * @param {string} params.id
 */
spDresb.getYardageDetailInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSellerYardage-getDetailInfo', ...params });
};

/**
 * 删除单条记录详情
 * @param {object} params
 * @param {string} params.id
 */
spDresb.deleteYardageById = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSellerYardage-deleteById', ...params });
};


/**
 * 平台批量给商品打分
 * @param {object} params
 * @param {string} params.tenantId 租户id
 * @param {string} params.unitId 单元id
 * @param {Array} params.spuIds spu的主键id的list集合
 * @param {string} params.platScore 平台评分，限制0-100，包含0和100
 */
spDresb.batchMarkSpu = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-batchMarkSpu', ...params });
};

/**
 * 修改商品详情图
 * @param {object} params jsonParam
 * @param {string} params.spuId 商品spu主键Id
 * @param {Array} params.pictureContent 详情图属性 {docId,docUrl}
 */
spDresb.updateDetailProps = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-updateDetailProps', ...params });
};


/**
 * 卖家设置单元性sku,spu混批量
 * @param {object} params jsonParam
 * @param {string} params.type 1代表sku类型,2代表spu类型
 * @param {string} params.mixNum 当type= 1代表sku混批量,当type =2代表spu混批量
 * @param {string} params.id 当type= 1代表sku主键,当type =2代表spu主键
 */
spDresb.setMinSpuOrSkuNum = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-setMinSpuOrSkuNum', ...params });
};

/**
 * 批量设置sku混批
 * @param {object} params jsonParam
 * @param {string} params.mixNum 混批量
 * @param {string} params.id skuId
 */
spDresb.batchSetSkuMinNum = async function (params) {
    params = Object.assign({
        tenantId: LOGINDATA.tenantId,
        unitId: LOGINDATA.unitId,
    }, format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-batchSetSkuMinNum', ...params });
};


// 特殊库存

/**
 * 根据spuId获取sku
 * @param {object} params jsonParam
 * @param {string} params.spuId spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 * @param {string} params.inventedNum 虚拟库存
 */
spDresb.findSkusBySpuIds = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-findSkusBySpuIds', ...params });
};

/**
 * 设置特殊库存
 * @param {object} params jsonParam
 * @param {string} params.spuId spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 * @param {string} params.inventedNum 虚拟库存
 * @param {string} params.id skuid
 */
spDresb.setSpecialStockSkus = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['spuId', 'unitId', 'tenantId']), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-specialStockSkus', ...params });
};

/**
 * 取消设置特殊库存
 * @param {object} params jsonParam
 * @param {string} params.spuId spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 */
spDresb.cancelSpecialStockSkus = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-cancelSpecialStockSkus', ...params });
};

/**
 * 特殊颜色尺码设置
 * @param {object} params jsonParam
 * @param {string} params.spuId spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 * @param {string} params.spec1
 * @param {string} params.spec2
 */
spDresb.setSpecialColorSkus = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['spuId', 'unitId', 'tenantId']), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-specialColorSkus', ...params });
};

/**
 * 取消特殊尺码颜色
 * @param {object} params jsonParam
 * @param {string} params.spuId spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 */
spDresb.cancelSpecialColorSkus = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-cancelSpecialColorSkus', ...params });
};


// 商品隐藏状态

/**
 * 设置商品隐藏状态
 * @param {object} params jsonParam
 * @param {string} params.id spuId
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 * @param {string} params.typeId 0无隐藏 1搜索隐藏 2买家隐藏-店铺不可见 3买家隐藏-店铺可见
 */
spDresb.hideSpuShow = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-hideSpuShow', ...params });
};

/**
 * 批量设置商品隐藏状态
 * @param {object} params jsonParam
 * @param {Array} params.ids 商品id 的list
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 * @param {string} params.typeId 0无隐藏 1搜索隐藏 2买家隐藏-店铺不可见 3买家隐藏-店铺可见
 */
spDresb.hideSpuShowBatch = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-hideSpuShowBatch', ...params });
};

/**
 * 隐藏店铺内所有商品
 * @param {object} params jsonParam
 * @param {string} params.id
 * @param {string} params.hideFlag 隐藏状态0 不隐藏 ， 1 搜索隐藏 2 买家隐藏-店铺内不可见 3 买家隐藏-店铺内可见 。默认3
 */
spDresb.hideAllDresSpu = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spugr-spShop-hideAllDresSpu', ...params });
};

/**
 * 购物车获取商品详情
 * @description 内部接口 wjw负责
 * @param {Array} params jsonParam
 * @param {Array} params.spuIds 商品id 的list
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 */
spDresb.findSpuFullByCarts = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findSpuFullByCarts', ...params });
};


/**
 * 购物车获取商品详情
 * @description 内部接口 wjw负责
 * @param {Object} params jsonParam
 * @param {string} params.spuIds 商品id
 * @param {string} params.unitId 单元id
 * @param {string} params.tenantId 租户id
 */
spDresb.findSpuFullBySpuIds = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findSpuFullBySpuIds', ...params });
};

/**
 * 寻找相似商品
 * @param {object} params jsonParam
 * @param {string} params.spuId
 * @param {string} params.tenantId 卖家租户id
 * @param {string} params.unitId 卖家单元id
 * @param {string} [params.pageRows] 记录数，默认20
 * @param {string} [params.pageOffset] 默认0,偏移量，使用本接口返回的偏移量
 * @param {string} [params.type] 搜索相似条件类型 1搜索以该商品价格为基准，上下浮动一定百分比（参数）的商品 ，2搜索相同类别的其他商品，3搜索价格范围内，不同类别的商品，4搜索价格范围外，不同类别的商品
 * @param {string} [params.sourceId] 入口id，不同渠道指定
 * @param {string} [params.sourceType] 来源类型，默认没值，1代表渠道版
 */
spDresb.searchSimilarDres = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-searchSimilarDres', ...params });
};

/**
 * 智能进货车
 * @param {object} params jsonParam
 * @param {string} [params.restartType] 传值 1，代表换一批
 */
spDresb.intelligentCart = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-dresSearch-intelligentCart', ...params });
};

/**
 * 商圈查看商品
 * @param {object} params jsonParam
 * @param {string} params.type 1代表市场，2代表商圈
 * @param {string} params.id type = 1 代表市场id,type = 2 代表商圈id
 */
spDresb.findTradeAreaShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spchb-spShop-findTradeAreaShop', ...params });
};

/**
 * 最低价状态同步
 * @param {object[]} params
 * @param {string} params.unitId 单元id
 * @param {string} params.spuId 商品id
 * @param {string} params.reducePriceDate 最低价时间
 * @param {string} params.reducePriceFlag 最低价状态
 */
spDresb.syncReducePrice = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-synReducePrice', ...params });
};

/**
 * 批量更新sku价格
 * @param {object} params
 * @param {string} params.spuId 否 修改时传spuId
 * @param {string} params.pubPrice 是 发布价（平台发布的公开价格)
 * @param {string} params.skuSpecialFlag 否 价格设置方式，0 统一设置 1 高级设置，默认高级设置
 * @param {Array} params.skus 否 sku属性列表
 */
spDresb.updateSkuPrice = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSku-updateSkuPrice', ...params });
};

/**
 * 设置拆色拆码库存
 * @param {object} params
 * @param {Array} params.skus sku属性，Array结构
 * @param {string} params.skus.id skuId
 * @param {string} params.skus.strSpec1 拆出的尺码
 * @param {string} params.skus.strSpec2 拆出的颜色
 * @param {string} params.skus.stockNum 库存值
 * @param {string} params.skus.stockPercent 库存占比，不传默认根据sku自动计算，保留两位小数
 */
spDresb.setSplitSkuStockNum = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-setSplitSkuStockNum', ...params });
};

/**
 * 设置拆色拆码售罄标识
 * @param {object} params
 * @param {Array} params.skus
 * @param {string} params.skus.id skuid
 * @param {string} params.skus.strSpec1 拆出的尺码
 * @param {string} params.skus.strSpec2 拆出的尺码
 * @param {string} params.skus.soldOutFlag 售罄表识
 */
spDresb.setSplitSoldFlag = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-setSplitSoldFlag', ...params });
}

/**
 * 批量设置商品重量
 * @param {object} params
 * @param {Array} params.spuIds 商品id
 * @param {number} params.weight 重量/不传
 */
spDresb.batchSetGoodsWeight = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-batchSetGoodsWeight', ...params });
}

/**
 * 商品分类变化调用（同步数据）
 * @param {object} params
 * @param {object} params.syncType 固定为1
 * @param {object} params.unitId 单元id
 */
spDresb.startExportEs = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.post(spCaps.spbUrl, { apiKey: 'ec-spdresb-spu-start-exportEs', ...params });
}

