const common = require('../../../lib/common');
const format = require('../../../data/format');
const spCaps = require('../spCaps');
const spAccount = require('../../../sp/data/spAccount');
const caps = require('../../../../node/data/caps');

const spTrade = module.exports = {
    url: spCaps.spbUrl,
};

//购物车

/**
 * 新增购物车
 * @param {object} params jsonParam
 */
spTrade.saveCart = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-addCart', ...params });
};

/**
 * 更新购物车
 * @param {object} params jsonParam
 */
spTrade.updateCart = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-updateCart', ...params });
};

/**
 * 删除购物车
 * @param {object} params
 * @param {string} params.id 要删除的购物车的主键id
 */
spTrade.deleteCart = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-deleteCart', ...params });
};

/**
 * 查看购物车
 * @param {object} params
 * @param {string} params.pageSize 每页条数，默认20
 * @param {string} params.pageNo 当前页，默认1
 */
spTrade.showCartList = async function (params = { pageNo: 1, pageSize: 0 }) {
    //params = format.packJsonParam(params);
    params = Object.assign(params, spCaps.spbDefParams());
    // return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-findCart', ...params });
    return common.post(this.url, { apiKey: 'ec-spcart-shoppingCart-findCart', ...params });
};

/**
 * 查询购物车商品总数
 */
spTrade.showCartNum = async function (params = { pageSize: 10, pageNo: 1 }) {
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-getTotalNum', ...params });
};


/***
 * 批量添加购物车
 * @param {object} jsonParam
 * */
spTrade.saveCartInBatchs = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.post(this.url, { apiKey: 'ec-spcart-shoppingCart-addCartInBatch', ...params });
};


/**
 * 批量删除购物车
*/
spTrade.deleteCartInBatch = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-deleteCartInBatch', ...params });
};

/**
 * 清空购物车
 */
spTrade.emptyCart = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spcart-shoppingCart-emptyCart', ...params });
};

/**
 * 混批校验
 * @param {array} jsonParam
 * @param {string} jsonParam.tenantId
 * @param {string} jsonParam.spuId
 * @param {string} jsonParam.skuId
 * @param {string} jsonParam.currentOrderNum 当前下单数量
 */
spTrade.verifyOrderLimit = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spcart-orderLimit-verify', ...params });
};

/**
 * 创建订单综合校验
 * @param {object} params
 * @param {string} params.tenantId 租户号
 * @param {string} params.spuId 商品号
 * @param {string} params.spuName 商品名
 * @param {string} params.skuId 款号
 * @param {string} params.tagId 活动id
 * @param {string} params.tagKind 活动类型
 * @param {string} params.currentOrderNum 当前下单数量
 */
spTrade.verifyCreateOrder = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spcart-createorder-verify', ...params });
};

//采购

/**
 * 创建采购订单
 * @param {object} params
 */
spTrade.savePurBill = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    params.productVersion = '2.9.0';
    const res = await common.post(this.url, { apiKey: 'ec-sppur-purBill-createFull', ...params });
    if (!params.hasOwnProperty('check')) {
        res.result.data.rows.forEach(row => expect(row, `opTime:${res.opTime}\n\t下单失败:\n\t${JSON.stringify(res.result)}`).to.includes({ isSuccess: 1 }));
    }
    await common.delay(500);
    return res;
};

/**
 * 创建采购订单-分销
 * @description 按仓库分组创建订单接口
 * @param {object} params
 */
spTrade.saveDisPurBill = async function (params) {
    params.orders.forEach(order => order.main.extProps = { autoTest: true });
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    const res = common.get(this.url, { apiKey: 'ec-sppur-purDisBill-createFull', ...params });
    await common.delay(500);
    return res;
};

/**
 * 查询所有的采购单
 * @param {object} params jsonParam
 * @param {string} params.statusType 订单状态类型，0 返回全部订单， 1 返回待付款订单，2 返回待发货订单， 3 返回已发货订单， 4 返回已完成订单，5 返回退货订单 其他待定
 */
spTrade.purFindBills = async function (params = { pageSize: 20, pageNo: 1 }) {
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findBills', ...params });
};

/**
 * 查询单据详情
 * @param {object} params
 * @param {string} params.id
 */
spTrade.findPurBillFull = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findBillFull', ...params });
};


/**
 * 支付
 * @param {object} params
 * @param {string} params.id
 */
spTrade.getJsApiPay = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    const payUrl = caps.name.includes('online') ? 'http://pay.hzdlsoft.com/pay/api.do' : 'https://hzdev.hzdlsoft.com/pay/api.do';
    return common.post(payUrl, { apiKey: 'ec-pay-getJsApiPay', ...params });
};

/**
 * 订单支付
 * @param {object} params jsonParam
 * @param {Array} params.orderIds orderIds
 * @param {string} params.payMoney 此笔支付金额
 * @param {string} params.appId 当前小程序的appId，没有的话按照原逻辑门店类型来拉起支付
 */
spTrade.createPay = async function (params = {}) {
    params = Object.assign(format.packJsonParam({
        hashKey: `${Date.now()}${common.getRandomNumStr(5)}`,
        appId: BASICDATA.appId || '',
        payerOpenId: LOGINDATA.wxOpenId,
        ...params
    }), spCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-payBill-createPay', ...params });
};

/**
 * 支付回调
 * @description 测试内部调用
 * @param {object} params jsonParam
 * @param {string} params.mainId payDetailId
 * @param {string} params.amount 此笔支付金额(元) 传参时会转化为分
 */
spTrade.receivePayResult = async function (params) {
    params.amount = common.mul(params.amount, 100);//单位 元转化为分
    params = format.packJsonParam(Object.assign({
        msg: '交易支付成功',
        code: 0,
        payId: Date.now(),
        payer: '',
        payDate: common.getCurrentTime(),
        // 线上入账需要排除自动化数据 必须传负
        // 清分相关需要为正
        payNumber: -common.getRandomNumStr(10),//spCaps.envName.includes('online') ? -common.getRandomNumStr(10) : common.getRandomNumStr(10),
        extra: '',//暂时没用到
        bizType: '11300',//暂时没用到
        payType: '2097152',// 中信微信小程序支付:1048576 支付宝APP支付:4096, 微信APP支付:2097152
        feeRate: 0.3,//平台收取千分之6手续费
        merchantEnable: 1,// 支付改造 兼容
    }, params));

    // 支付测试环境 使用新的支付方式时,资产服务会自动调用回调方法,自动化不用调用
    if (params.jsonParam.merchantEnable == 1 && ['ss_dev', 'ss_test', 'ss_test2', 'ss_chk', 'ss_pre'].includes(caps.name)) {
        await common.delay(8000);
        return;
    }

    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    params._unitId = LOGINDATA.unitId;
    params._saasId = LOGINDATA.saasId;
    return common.get(this.url, { apiKey: 'ec-sppur-payBill-receivePayResult', ...params });
};

/**
 * 取消支付（买家|平台）
 * @description 未支付状态下取消支付  买家取消只需要传订单id
 * @param {object} params
 * @param {string} params.id 采购订单id
 * @param {string} params.billNo 订单编号（平台取消必传)
 * @param {string} params.tenantId 买家租户id
 * @param {string} params.unitId 买家单元id
 * @param {string} params.cancelFrom 取消来源，0 买家 1平台 ，不传默认买家自己
 */
spTrade.cancelBillPayStatus = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-cancelBillPayStatus', ...params });
};

/**
 * 支付查询
 * @description 微商城已废弃使用
 * @param {object} params jsonParam
 * @param {string} params.id payDetailId，该笔支付对应的支付明细id
 */
spTrade.checkQueryPayResult = async function (params = {}) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-sppur-payBill-checkQueryPayResult', ...params });
};

/**
 * 卖家按款配货
 * @param {object} params jsonParam
 * @param {object} params.billId 销售单ID
 * @param {object} params.salesDetailPickList 配货详情
 */
spTrade.pickDetail = async function (params = {}) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-salesDetail-pickDetail', ...params });
};

/**
 * 提醒发货
 * @param {object} params jsonParam
 * @param {string} params.billId 采购订单ID
 */
spTrade.remindBillDeliver = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-remindBillDeliver', ...params });
};

/**
 * 延迟收货
 * @param {object} params jsonParam
 * @param {string} params.id 采购订单ID
 */
spTrade.extendedReturn = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-sppur-purBill-extendedReturn', ...params });
};

/**
 * 买家确定收货
 * @param {object} params jsonParam
 * @param {array} params.purBillIds 确认收货的采购订单ID列表
 */
spTrade.confirmReceipt = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-confirmReceipt', ...params });
};

/**
 * 买家取消订单
 * @param {object} params jsonParam
 * @param {string} params.id 采购订单ID
 * @param {string} params.buyerRem 买家备注。没有上传时，不修改买家备注
 */
spTrade.cancelPurBill = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-cancelBill', ...params });
};


/**
 * 买家我的统计数据
 */
spTrade.getPurStat = async function (params = {}) {
    params = spCaps.spbDefParams();
    return common.get(this.url, { apiKey: 'ec-sppur-spPurStat-getMineStatDTO', ...params });
};

/**
 * 买家查看采购订单的售后列表
 * @param {object} params
 * @param {string} params.purId 采购单id
 */
spTrade.getPurReturnBills = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-getBillReturnBills', ...params });
};

/**
 * 创建退款退货申请单
 * @description 平台用户创建需要传卖家的_tid,_cid
 * @param {object} params
 */
spTrade.saveReturnBill = async function (params) {
    if (spCaps.envName != 'sp_online') {
        params.main.extProps = { autoTest: true };
    }
    params = format.packJsonParam(params, ['_tid', '_cid']);
    if (spCaps.isSpgSessionId()) {
        params.pass = spAccount.ecStaff.pass;
    } else {
        Object.assign(params, spCaps.spbDefParams());
    }
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-createFull', ...params });
};

/**
 * 保存退货单买家发货信息
 * @param {object} params
 * @param {string} params.id 退货申请单ID
 * @param {string} params.logisCompName 物流公司名称
 * @param {string} params.waybillNo 运单号
 */
spTrade.saveReturnDeliver = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-saveDeliver', ...params });
};


/**
 * 买家获取已购买过的店铺
 */
spTrade.getRecentBuyShopList = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-getRecentShopList', ...params });
};

/**
 * 买家评论
 * @param {Object} params
 * @param {String} id 采购订单id
 * @param {String} rem 评论内容
 * @param {Number} val 商品评分，限制1-5
 * @param {Number} logisScore  物流评分限制1-5
 * @param {Number} sendFlowerIs  是否送花给店家，1代表送花，0代表不送，默认，不传该字段，则送花
*/
spTrade.addComment = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-evalBill', ...params });
};

/**
 * 买家查看评论
 * @param {String} billId订单主键id
*/
spTrade.getCommentById = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-up-upEvaluate-getUpEval', ...params });
};

/**
 * 订单查询
*/
spTrade.searchSalesBills = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-spessearch-salesBillSearch-searchSalesBill', ...params });
};


//销售

/**
 * 卖家修改订单金额
 * @param {object} params
 */
spTrade.changeBillMoney = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-changeMoney', ...params });
};

/**
 * 修改订单明细金额
 * @param {object} params
 * @param {string} params.id 销售订单ID
 * @param {string} params.shipFeeMoney 运费
 * @param {object[]} params.details
 * @param {string} params.details.id 销售订单明细ID
 * @param {string} params.details.newMoney 修改后价
 */
spTrade.changeBillDetailMoney = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-newChangeMoney', ...params });
};

/**
 * 订单金额修改还原
 * @param {object} params
 * @param {string} params.id 销售订单ID
 */
spTrade.resetChangeMoney = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-resetChangeMoney', ...params });
};

/**
 * 查询改价订单详情
 * @param {object} params
 * @param {string} params.id 销售订单ID
 */
spTrade.getChangeMoneyBill = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-getChangeMoneyBill', ...params });
};

/**
 * 查询销售单
 * @param {object} params
 */
spTrade.salesFindBills = async function (params = {}) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBills', ...params });
};

/**
 * 查询单据详情
 * @param {object} params
 * @param {string} params.id
 */
spTrade.salesFindBillFull = async function (params) {
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBillFull', ...params });
};

/**
 * 平台端查询订单详情
 * @param {object} params
 * @param {string} params.billNo 订单号
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid
 */
spTrade.findBillFullByAdmin = async function (params) {
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBillFullByAdmin', ...params });
};

/**
 * 设置卖家备注
 * @param {object} params
 * @param {string} params.id 销售订单ID
 * @param {string} params.sellerRem 卖家备注（没有上传或上传空串时，将清空卖家备注）
 */
spTrade.saveSellerRem = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-saveSellerRem', ...params });
};

/**
 * 通知取合包件
 * @param {object} params
 * @param {string} params.id 销售订单ID
 */
spTrade.pickUpGood = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-deliverBill-pickUpGood', ...params });
};

/**
 * 卖家发货
 * @param {object} params jsonParam
 */
spTrade.deliverSalesBill = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-deliverBill-saveFull', ...params });
};

/**
 * 卖家发货物流更新
 * @param {object} params
 * @param {string} params.deliverId 发货单id
 * @param {string} params.waybillNo 物流号
 * @param {string} params.logisCompId 物流公司id
 * @param {string} params.logisCompName 物流公司名称
 */
spTrade.updateDeliverBill = async function (params) {
    Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-deliverBill-updateBill', ...params });
};

/**
 * 取消(关闭)订单
 * @param {object} params jsonParam
 * @param {string} params.id 销售订单ID
 * @param {string} params.sellerRem 卖家备注。没有上传时，不修改卖家备注。
 */
spTrade.cancelSalseBill = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-cancelBill', ...params });
};

/**
 * 订单数量统计--卖家
 */
spTrade.findBillsCountForSeller = async function () {
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBillsCount', ...spCaps.spbDefParams() });
};


/**
 * 查询清分提现申请信息
 * @param {object} params
 * @param {object} params.unitId 单元id
 * @param {object} params.taskCode 定时任务的task_code
 */
spTrade.findLiquidation = async function (params) {
    params = Object.assign({}, params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findLiquidation', ...params });
};

/**
 * 处理清分提现申请
 * @param {object} params
 * @param {object} params.unitId 单元id
 * @param {object} params.taskCode 定时任务的task_code
 */
spTrade.dealLiquidation = async function (params) {
    params = Object.assign({}, params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-dealLiquidation', ...params });
};

/**
 * 重新清分提现申请
 * @param {object} params
 * @param {object} params.unitId 单元id
 * @param {object} params.tenantId 卖家租户id
 * @param {object} params.billNos 订单编号，多值逗号间隔
 * @param {object} params.opType 是否重新生成批次号：1 是，0 否（默认）
 */
spTrade.againDealLiquidation = async function (params) {
    params = Object.assign({}, params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-againDealLiquidation', ...params });
};


/**
 * 退货退款列表
 * @param {object} params
 */
spTrade.findSalesReturnBills = async function (params = {}) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-findReturnBills', ...params });
};


/**
 * 退货单详情
 * @param {object} params
 * @param {string} params.id 退货单id
 */
spTrade.getReturnBillDetail = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-getReturnBillDetail', ...params });
};

/**
 * 卖家审核退货申请
 * @param {object} params
 * @param {string} params.id 退货单id
 * @param {string} params.checkResult 审核结果。1 同意，0 不同意
 * @param {string} params.rejectReason 不同意原因。checkResult=0时，必须选择。取值见字典类别2006 商城：字典类别3004
 */
spTrade.checkReturnBill = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-checkBillReturn', ...params });
};

/**
 * 根据退款单号获取待退款编号列表
 * @param {object} params
 * @param {string} params.returnBillId 退单编号
 */
spTrade.getToRefundPayDetailNo = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-getToRefundPayDetailNo', ...params });
};


/**
 * 退款回调
 * @description 测试内部调用
 * @param {object} params jsonParam
 * @param {string} params.mainId payDetailId
 * @param {string} params.amount 此笔支付金额(元) 传参时会转化为分
 */
spTrade.receiveReturnResult = async function (params) {
    params.amount = common.mul(params.amount, 100);//单位 元转化为分
    params = format.packJsonParam(Object.assign({
        payDate: common.getCurrentTime(),
        code: 0,//0：成功，其他：失败
        ...params
    }, params));

    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    params._unitId = LOGINDATA.unitId;
    params._saasId = LOGINDATA.saasId;
    return common.get(this.url, { apiKey: 'ec-sppur-payBill-receiveReturnResult', ...params });
};

/**
 * 退货单卖家确认收货
 * @param {object} params
 * @param {string} params.id 退货单id
 */
spTrade.confirmReturnBillReceipt = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-confirmReceipt', ...params });
};

/**
 * 平台端退货单查询
 * @param {object} params
 * @param {string} params.startTime 起始时间（精度，到秒）
 * @param {string} params.endTime 截止时间（精度，到秒）
 * @param {string} params.startDate 起始日期
 * @param {string} params.endDate 	截止日期
 * @param {object} params.keyWords 需要精确匹配的属性。key=属性名，value=需要搜索的属性列表
 */
spTrade.getReturnBillListByAdmin = async function (params) {
    params = format.packJsonParam(params);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spessearch-salesBillSearch-searchReturnBill', ...params });
};


/**
 * 平台端退货单详情
 * @param {object} params
 * @param {string} params.id 退货单id
 * @param {string} params._tid 租户id
 */
spTrade.getReturnBillDetailByAdmin = async function (params) {
    params = format.packJsonParam(params, ['_tid', '_cid']);
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-getReturnBillDetailByAdmin', ...params });
};

/**
 * 订单是否支付后超时x小时未发货
 * @param {object} params
 * @param {string} params.id 采购单id
 */
spTrade.getReturnDirect = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-getReturnDirect', ...params });
};

/**
 * 买家撤消退货退款申请
 * @param {object} params
 * @param {object} params.id 退货申请单ID
 */
spTrade.cancelReturn = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-cancelReturn', ...params });
};


/**
 * 订单数量统计--买家
 */
spTrade.findBillsCountForClient = async function () {
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findBillsCount', ...spCaps.spbDefParams() });
};

/**
 * 订单支付状态查询
 * @param {object} params
 * @param {object} params.id
 */
spTrade.getPayStatus = async function (params) {
    params = Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-getPayStatus', ...params });
}

/**
 * 查询单据退货预览详情
 * @param {object} params
 * @param {object} params.id
 */
spTrade.findBillBackViewFull = async function (params) {
    if (!spCaps.isSpgSessionId()) Object.assign(params, spCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findBillBackViewFull', ...params });
};