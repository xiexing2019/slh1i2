const common = require('../../../lib/common');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spUp = module.exports = {
    url: spCaps.spbUrl,
};

/**
 * 买家关注店铺
 * @param {Object} params
 * @param {String} params.shopId 需要关注的店铺id
 * @param {String} params.shopName 需要关注的店铺名称
 * @param {String} params.rem 备注信息
 */
spUp.addFavorShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorShop-addFavorShop', ...params });
};

/**
 * 买家取消关注店铺
 * @param {object} params
 * @param {String} params.shopId 需要取消关注的店铺id
 */
spUp.cancelFavorShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorShop-cancelFavorShop', ...params });
};

/**
 * 已关注店铺列表
 * @param {object} params
 * @param {String} params.shopNameLike 店铺名称，用来模糊查询关注的店铺
 */
spUp.getFavorShopList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorShop-listFavorShop', ...params });
};

/**
 * 店铺关注者列表
 * @param {object} params
 * @param {String} params.favorerNameLike 关注者名称，用来模糊查询店铺关注者
 */
spUp.getShopFavorerList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-shopFavorer-listShopFavorer', ...params });
};

/**
 * 买家第一次点关注时弹出运营端配置店铺
 * @param {object} params
 */
spUp.findSpSuggesFocusShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-spUpFavorShop-findSpSuggesFocusShop', ...params });
};


// 商品收藏

/**
 * 买家收藏商品
 * @param {Object} params
 * @param {String} params.spuId 商品ID
 * @param {String} params.spuTitle 商品标题
 * @param {String} params.shopId 店铺ID
 * @param {String} params.shopName 店铺名称
 */
spUp.addFavorSpu = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorSpu-addFavorSpu', ...params });
};

/**
 * 买家取消收藏商品
 * @param {Object} params
 * @param {String} params.spuId 商品ID
 */
spUp.cancelFavorSpu = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorSpu-cancelFavorSpu', ...params });
};

/**
 * 买家批量取消收藏商品
 * @param {Object} params
 * @param {Object} params.spuIds 商品ID数组，如 “spuIds”:[1,2]
*/
spUp.cancelFavorShopInBatch = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorSpu-cancelFavorSpus', ...params });
};


/**
 * 买家已关注商品列表
 * @param {Object} params
 * @param {String} params.spuTitleLike 商品标题 模糊搜索
 */
spUp.getFavorSpuList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-favorSpu-listFavorSpu', ...params });
};

/**
 * 商品收藏者列表
 * @param {Object} params
 * @param {String} params.spuId 商品ID
 * @param {String} params.favorerNameLike 关注者名称 模糊匹配
 */
spUp.getSpuFavorerList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-spuFavorer-listSpuFavorer', ...params });
};



/**
 * 店铺日志流查询
 * @param {Object} params jsonparam
 * @param {String} params.buyerNameLike 客户名称,模糊查询
 * @param {String} params.buyerMobileLike 客户手机,模糊查询
 * @param {String} params.flowType 日志流类型：1-商品查看 2-商品点赞 3-下单记录 4-支付记录 5-店铺到访记录 6-店铺关注记录 7-收藏商品记录
 */
spUp.getShopLogList = async function (params) {
    await common.delay(100);//不加延迟可能返回结果为空
    params = Object.assign({
        pageSize: 10,
        pageNo: 1,
        orderBy: 'createdDate',
        orderByDesc: true,
    }, format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-shopLogFlow-list', ...params });
};


/**
 * 新增买家搜索反馈
 * @param {Object} typeIds 买家搜索反馈的字典的值，多个以“,”隔开，如“5,6”
 * @param {String} rem 买家输入的反馈内容，1000个字符长度限制
 */
spUp.saveFeeBacks = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerSearchFeedback-createFull', ...params });
};


/**
 * 新增买家反馈
 * @param {Object} params 
 * @param {String} params.functionType 反馈的功能类型，1-买家商品分享
 */
spUp.saveBuyerFeedback = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerFeedback-createFull', ...params });
};

/**
 * 买家反馈查看列表
 * @param {Object} params 
 * @param {String} params.functionType 反馈的功能类型，1-买家商品分享
 */
spUp.getBuyerFeedbacksList = async function (params) {
    params = format.packJsonParam(params, ['wrapper']);
    return common.get(spCaps.spgUrl, { apiKey: 'ec-spadmin-buyerFeedback-listBuyerFeedbacks', ...params });
};

//用户倾向
/**
 * 保存用户倾向
 * @param {Object} params
 * @param {Number} params.id 修改时传
 * @param {Number} params.typeId 倾向类别 0 不感兴趣 1 感兴趣
 * @param {Number} params.tendencyType 倾向种类1、商品 2、类别 3、板式 4、风格 5 店铺
 * @param {Number} params.tendencyId  倾向值id
 * @param {String} params.tendencyStr 倾向值字符说明
*/
spUp.saveUserTendency = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-userTendency-saveFull', ...params });
};

/**
 * 获取用户倾向
 * @param {Object} params
 * @param {Number} params.id 
*/
spUp.getUserTendencyById = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-userTendency-getFullById', ...params });
};

/**
 * 获取用户倾向列表 
 * @param {Object} params
 * @param {Number} params.flag 状态
 * @param {Number} params.tendencyType 倾向类型
 * @param {Number} params.typeId 倾向类别
*/
spUp.getUserTendencyList = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-userTendency-listFull', ...params });
};

/**
 * 删除用户倾向
 * @param {Object} params
 * @param {Number} params.id 
*/
spUp.delUserTendencyById = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-up-userTendency-deleteById', ...params });
};

//用户足迹

/**
 * 删除用户足迹
 * @param {Object} params
 * @param {Array} params.ids 商品id列表 
 */
spUp.deleteBuyerView = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerView-deleteBuyerView', ...params });
};

/**
 * 查询足迹日期
 * @param {Object} params
 */
spUp.selectBuyerViewDate = async function (params) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams());
    return common.get(spCaps.spbUrl, { apiKey: 'ec-spup-upBuyerView-selectBuyerViewDate', ...params });
};
