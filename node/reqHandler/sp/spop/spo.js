const common = require('../../../lib/common');
const caps = require('../../../data/caps');
const spCaps = require('../spCaps');
const format = require('../../../data/format');

let spop = module.exports = {
    opUrl: `${spCaps.url}/spo/api.do`,

};

/**
 * 创建应用
 * @param {object} params 
 * @param {string} params.appName 应用名称
 * @param {string} params.appTypeId 应用类型(1.买家应用2.卖家应用)
 */
spop.saveOpApp = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-op-app-save', ...params });
};


/**
 * 查看应用
 * @param {object} params 
 * @param {string} params.appName 应用名称
 * @param {string} params.appTypeId 应用类型(1.买家应用2.卖家应用)
 */
spop.getOpAppById = async function (params) {
    return common.post(spCaps.spgUrl, { apiKey: 'ec-op-app-getById', ...params });
};


/**
 * 开放平台应用列表
 * @param {object} params 
 * @param {string} params.name 渠道名称
 * @param {boolean} params.flag 是否启用（0：停用，1：启用）
 */
spop.getOpAppList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(spCaps.spgUrl, { apiKey: 'ec-op-app-list', ...params });
};



// 好店开放接口

/**
 * 获取accessToken
 * @description accessToken存在有效期，默认7200秒，在到期之前需要重新请求获取
 * @param {object} params 
 * @param {string} params._cid 机房代码，应用申请后平台提供，必须放在url中
 * @param {string} params.appKey 开放应用appKey
 * @param {string} params.appSecret 开放应用appSecret
 */
spop.refreshToken = async function (params) {
    return common.post(this.opUrl, { apiKey: 'refreshToken', ...params });
};


/**
 * 推荐商品查询(渠道)
 * @param {object} params 
 * @param {string} params.startTime 增量更新时间,首次更新不传 格式2018-10-01 13:00:00
 */
spop.getSpuList = async function (params) {
    return common.post(this.opUrl, { apiKey: 'ec-op-recommend-spuList', ...params });
};




