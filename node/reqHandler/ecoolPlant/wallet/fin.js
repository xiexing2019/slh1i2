const common = require('../../../lib/common');
const format = require('../../../data/format');
const plantCaps = require('../../ecoolPlant/plantCaps');

const fin = module.exports = {};

/**
 * 账户余额变更
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.tenantId 租户id
 * @param {object} params.acctType 账户类型：1301-直播时长，1302-短信余额，1303-快递余额
 * @param {object} params.name 交易说明
 * @param {object} params.amount 交易单位(正数)
 * @param {object} params.bizType 业务类型
 * @param {object} params.bizOrderId 业务编号
 * @param {object} params.ioFlag 收支标记：0-收入；1-支出
 * @param {object} params.tradeType 交易类型
 */
fin.saveFinAcctFlowInfo = async function (params) {
    params.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
    params = format.packJsonParam(params);
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcct-saveFinAcctFlowInfo', ...params });
};

/**
 * 创建收款单
 * @param {object} params
 * @param {object} [params.bizType] 交易类型
 * @param {object} [params.bizOrderId] 关联业务单据
 * @param {object} params.merchantId 商户id
 * @param {object} params.hashKey 请求hash值
 * @param {object} params.payMoney 此笔支付金额
 * @param {object} [params.payType] 支付类别（1、扫码聚合 2、app支付 3、钱包支付 4、公众号JS支付 5、微信小程序支付），-99、线下支付； -98、余额支付 不传默认app支付
 * @param {object} [params.payerOpenId] 支付方式（1、支付宝，2 微信），不传默认支付宝
 * @param {object} [params.channelCodeSuffix] 渠道标识，用以在同一个payType下区分不同的实际支付渠道。小程序中，商陆好店买家版传buyer,商陆好店渠道版传channel
 */
fin.createPayBill = async function (params) {
    params.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
    params = format.packJsonParam(params, ['bizType', 'bizOrderId']);
    params.merchantId = params.jsonParam.merchantId;
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-payBill-createPay', ...params });
};

/**
 * 支付入账解冻
 * @param {object} params.jsonParam json数据结构(List集合)
 * @param {object} params.jsonParam.merchantId 商户id（集合中，商户id都相同的才处理）
 * @param {object} [params.jsonParam.tenantId] 租户id
 * @param {object} [params.jsonParam.saasId] saasId
 * @param {object} params.jsonParam.payType 类型： 1 支付，2 退款，不传默认为1支付
 * @param {object} params.jsonParam.bizType 交易类型
 * @param {object} params.jsonParam.bizOrderId 关联业务id
 * @param {object} [params.jsonParam.bizSubOrderId] 关联业务子id
 */
fin.unfreezeBizOrder = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctFlow-unfreezeBizOrder', ...params });
};
/**
 * 获取钱包首页数据
 * @param {object} params
 * @param {object} params.merchantId 商户id
 */
fin.getHomePageMoney = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcct-getHomePageMoney', ...params });
};

/**
 * 获取账户流水
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.orderId] bizOrderDetailId
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 * @param {object} [params.pageNo] 分页页码
 * @param {object} [params.pageSize] 分页条数
 */
fin.findFinAcctFlow = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctFlow-findFinAcctFlow', ...params });
};

/**
 * 按日期获取账户日结总金额
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.pageNo] 分页页码
 * @param {object} [params.pageSize] 分页条数
 */
fin.findFinAcctDaily = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctDaily-findFinAcctDaily', ...params });
};

/**
 * 获取财务流水日结概览
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.ioFlag] 0-收入；1-支出
 * @param {object} [params.dateType] 1-日，2-月， 3-年
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 */
fin.findFinAcctFlowDailyOverview = async function (params) {
    console.log(params);
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctDaily-findFinAcctFlowDailyOverview', ...params });
};

/**
 * 获取财务流水日结
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.ioFlag] 0-收入；1-支出
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 * @param {object} [params.pageNo] 分页页码
 * @param {object} [params.pageSize] 分页条数
 */
fin.findFinAcctFlowDaily = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctDaily-findFinAcctFlowDaily', ...params });
};

/**
 * 获取流水总金额
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.ioFlag] 0-收入；1-支出
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 */
fin.getTotalFlowMoney = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctDaily-getTotalFlowMoney', ...params });
};

/**
 * 创建提现申请单
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.tenantId] 租户id
 * @param {object} params.hashKey 
 * @param {object} params.money 提现金额
 */
fin.createCashBill = async function (params) {
    params.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
    params = format.packJsonParam(params, ['merchantId']);
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finCashBill-create', ...params });
};

/**
 * 重新提现
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.id 提现单id
 */
fin.afreshWithdrawCashBill = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finCashBill-afreshWithdraw', ...params });
};

/**
 * 获取提现记录
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} [params.billNo] 提现编号
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 */
fin.findFinCashBill = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finCashBill-findFinCashBill', ...params });
};

/**
 * 获取提现详情
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.id 提现单id
 */
fin.getfinCashBillById = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finCashBill-getById', ...params });
};

//附属账号(直播、短信)

/**
 * 账户额度变更
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.tenantId 租户id
 * @param {object} params.acctType  账户类型: 1301 - 直播时长余额(单位分），1302-短信余额，1303-快递余额
 * @param {object} params.name 交易说明
 * @param {object} params.amount 交易单位(正数)
 * @param {object} params.bizType 业务类型
 * @param {object} params.bizOrderId 业务编号
 * @param {object} params.ioFlag 收支标记：0-收入；1-支出
 * @param {object} params.tradeType 交易类型：根据acctType自行定义
 * @param {object} [params.bizSubOrderId] 交易子编号
 * @param {object} [params.proTime] 交易时间
 * @param {object} [params.payType] 支付类型
 * @param {object} [params.payWayId] 支付方式id
 * @param {object} [params.ext] 扩展信息（json字符串)
 * @param {object} params.hashKey 哈希值（防去重）
 */
fin.saveFinAcctFlowInfo = async function (params) {
    params.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
    params = format.packJsonParam(params);
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcct-saveFinAcctFlowInfo', ...params });
};

/**
 * 获取账户信息数据（总数量）
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.acctType  账户类型: 1301 - 直播时长余额(单位分），1302-短信余额，1303-快递余额
 */
fin.getAcctStatistics = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcct-getAcctStatistics', ...params });
};

/**
 * 获取账户(直播、短信)流水
 * @param {object} params
 * @param {object} params.merchantId 商户id
 * @param {object} params.acctType  账户类型: 1301 - 直播时长余额(单位分），1302-短信余额，1303-快递余额
 * @param {object} [params.startDate] 起始日期
 * @param {object} [params.endDate] 结束日期
 * @param {object} [params.bizType] 交易类型
 * @param {object} [params.orderId] 业务主键
 */
fin.findFinAcctFlowTypes = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-fin-finAcctFlow-findFinAcctFlowTypes', ...params });
};



