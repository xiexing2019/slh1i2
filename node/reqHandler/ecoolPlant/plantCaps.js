const caps = require('../../data/caps');

// 获取命令行参数指定的环境变量名
const envName = caps.name;

const urlCaps = {
    plant_test: {
        confcUrl: `https://hzdev.hzdlsoft.com/confc/api.do`,
        merfinUrl: `https://hzdev.hzdlsoft.com/merfin/api.do`,
        payUrl: `https://hzdev.hzdlsoft.com/pay/api.do`
    },
    plant_online: {
        merfinUrl: `https://merfin.hzdlsoft.com/merfin/api.do`,
        merfinUrl: `http://119.3.128.221:8081/merfin/api.do`, // hws2_merfin02web1
        // merfinUrl: `http://119.3.131.224:8081/merfin/api.do`, // hws2_merfin02web2
        payUrl: `https://pay.hzdlsoft.com/pay/api.do`
    }
};

const dbCaps = {
    plant_test: {
        merfinMySql: {
            host: '119.3.0.138',
            port: '3366'
        },
        lqdMysql: {
            host: '172.81.235.150',
            port: '3366'
        }
    },
    plant_online: {
        // ts_my02i11_merfin01db11
        merfinMySql: {
            user: 'mychk',
            host: '115.159.50.200',
            port: '30211'
        },
        // hws2_pay02db11
        payMySql: {
            user: 'mychk',
            host: '119.3.92.72',
            port: '30111'
        },
        // cs3_lqd01db11-cs3_lqd01_edb1
        lqdMysql: {
            user: 'mychk',
            host: '172.81.203.22',
            port: '39211'
        }
    }
};

module.exports = {
    db: dbCaps[envName],
    ...urlCaps[envName]
};
