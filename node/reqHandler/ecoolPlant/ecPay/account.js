const common = require('../../../lib/common');
const crypto = require('crypto');
const format = require('../../../data/format');
const moment = require('moment');
const plantCaps = require('../../ecoolPlant/plantCaps');

const account = module.exports = {
    url: plantCaps.merfinUrl,
};

/**
 * 提交结算申请
 * @description 把入账数据提交到lqd清算中心，进行对账操作
 * @param {object} params
 * @param {string} params.merchantId 商户id
 * @param {string} [params.bizOrderIds] id集合
 * @param {string} [params.bizType] 业务类型（传bizOrderIds，则不用传bizType、bizOrderId、bizSubOrderId）
 * @param {string} [params.bizOrderId] 业务主键
 * @param {string} [params.bizSubOrderId] 业务子键
 */
account.commitBizOrderDetail = async function (params = {}) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-accounting-commitBizOrderDetail', ...params });
};

/**
 * 对账数据结果回调反馈
 * @param {object} params
 * @param {string} params.unitId 商户id
 * @param {string} params.transCode 入账提交清分时传的流水号
 * @param {string} params.result 对账结果, 0 成功，其他失败
 * @param {string} [params.tenantId] 租户id
 * @param {string} [params.saasId] 集群saasId
 * @param {string} [params.msg] 错误信息，如果有的话
 */
account.receiveRecordedResult = async function (params = {}) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-accounting-receiveRecordedResult', ...params });
};

/**
 * 提现结果回调反馈
 * @description 提现结果回调反馈（适用中信）
 * @param {object} params
 * @param {string} params.transCode 提现申请时传的流水号
 * @param {string} [params.failedList] 失败的bizOrderId集合
 * @param {string} [params.clusterCode] 无
 */
account.receiveWithdrawResult = async function (params = {}) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-accounting-receiveWithdrawResult', ...params });
};


