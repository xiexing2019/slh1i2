const common = require('../../../lib/common');
const crypto = require('crypto');
const format = require('../../../data/format');
const moment = require('moment');
const plantCaps = require('../../ecoolPlant/plantCaps');

const cspos = module.exports = {
    url: plantCaps.payUrl,
};

/**
 * 设置授权验证参数
 * @param {object} params 
 * @param {string} params.appKey 开发者appKey
 * @param {string} params.appSecret 开发者appSecret
 */
cspos.setAuthParams = function (params) {
    const { appKey, appSecret } = params;
    const timestamp = moment().unix(); // 需要为秒级时间戳
    const nonce = common.getRandomStr(32);
    const stringSignTemp = `appKey=${appKey}&nonce=${nonce}&timestamp=${timestamp}${appSecret}`;
    const sign = crypto.createHash('md5').update(stringSignTemp).digest('hex').toUpperCase();
    // console.log(sign);
    return {
        appKey,
        appSecret,
        timestamp,
        nonce,
        sign
    };
};

/**
 * POS商户首次绑定
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号
 * @param {string} params.saasShopId 集群门店id
 * @param {string} params.slhSn 客户编号（扫描获取）
 */
cspos.bindPos = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-bind', ...params });
};


/**
 * POS支付单创建
 * @param {object} params mockPosOrderJson
 */
cspos.createOrder = async function (params) {
    return common.post(this.url, { apiKey: 'ec-cspos-order-create', ...params });
};

/**
 * pos查询支付单列表
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号
 */
cspos.findOrderList = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-list', ...params });
};

/**
 * pos获取支付单详情
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号 
 * @param {string} params.payOrderId 支付单id
 */
cspos.getOrderDetail = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-get', ...params });
};

/**
 * pos支付回调
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号 
 * @param {string} params.payOrderId 支付单id
 * @param {string} params.payId 支付流水号
 * @param {string} params.endTime 支付完成时间, 字符格式 yyyy-MM-dd hh:mm:ss
 * @param {string} params.money 订单金额，单位为分
 * @param {string} [params.posPayType] pos机支付方式:1：刷卡 2.扫码支付
 */
cspos.orderPayCb = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-pay-callback', ...params });
};

/**
 * POS退款单创建
 * @param {object} params 
 * @param {string} params.bizOrderId 支付单业务id
 * @param {string} params.bizRefundId 退款业务id
 * @param {string} params.bizType 业务类型
 * @param {string} params.refundAmount 退款金额，单位分
 * @param {string} params.callbackUrl 回调地址
 */
cspos.createRefund = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-cspos-order-refund-create', ...params });
};

/**
 * pos查询退款单列表
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号
 */
cspos.findRefundList = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-refund-list', ...params });
};

/**
 * pos获取退款单详情
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号 
 * @param {string} params.refundOrderId 退款单id
 */
cspos.getRefundDetail = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-refund-get', ...params });
};

/**
 * pos退款回调
 * @param {object} params 
 * @param {string} params.merchantNo 商户编号（第三方的商户编号） 
 * @param {string} params.deviceNo 设备编号 
 * @param {string} params.refundOrderId 退款单id
 * @param {string} params.refundId 退款流水号
 * @param {string} params.endTime 退款完成时间, 字符格式 yyyy-MM-dd hh:mm:ss
 * @param {string} params.money 订单金额，单位为分
 */
cspos.orderRefundCb = async function (params) {
    Object.assign(params, this.setAuthParams(params));
    return common.post(this.url, { apiKey: 'ec-cspos-order-refund-callback', ...params });
};