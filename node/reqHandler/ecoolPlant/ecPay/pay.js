const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const crypto = require('crypto');
const format = require('../../../data/format');
const moment = require('moment');
const plantCaps = require('../../ecoolPlant/plantCaps');

const pay = module.exports = {
    url: plantCaps.payUrl,
};

/**
 * 支付
 * @description 支付mock开关 pay_request_mock  1开启mock  0 关闭mock
 * @param {object} params
 * @param {string} params.id
 */
pay.getJsApiPay = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-pay-getJsApiPay', ...params });
};

/**
 * 聚合支付
 * @param {object} params
 * @param {string} params.openId
 * @param {string} params.key
 * @param {string} params.payType
 */
pay.getUnionPay = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-pay-getUnionPay', ...params });
};