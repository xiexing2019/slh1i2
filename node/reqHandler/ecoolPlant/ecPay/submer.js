const common = require('../../../lib/common');
const format = require('../../../data/format');
const plantCaps = require('../plantCaps');

const submer = module.exports = {};


/**
 * 子商户列表查询
 * @param {object} params 
 * @param {object} [params.merchantId] 主商户商户号
 * @param {object} [params.ownerName] 主商户名称（不提供模糊搜索）
 * @param {object} [params.subName] 子成员商户名称
 * @param {object} [params.subId] 业务层标识
 * @param {object} [params.linkStartTime] 关联日期-开始日期,例如：2019-11-11
 * @param {object} [params.linkEndTime] 关联日期-结束日期,例如：2019-11-11
 * @param {object} [params.pageNo] 当前页，默认1
 * @param {object} [params.pageSize] 每页条数，默认20
 */
submer.finSubMerchantsByWeb = async function (params) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-submer-finSubMerchantsByWeb', ...params });
};

