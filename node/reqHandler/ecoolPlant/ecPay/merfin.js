const common = require('../../../lib/common');
const crypto = require('crypto');
const format = require('../../../data/format');
const moment = require('moment');
const plantCaps = require('../../ecoolPlant/plantCaps');

const merfin = module.exports = {
    url: plantCaps.merfinUrl,
};

/**
 * 退款
 * @param {object} params
 * @param {string} params.bizRefundId 退款业务单号
 * @param {string} params.bizOrderId 业务单号，和付款时一致
 * @param {string} params.bizType 业务类型，和付款时一致
 * @param {string} params.payType 付款类型，在支付成功时异步回调
 * @param {string} params.refundAmount 退款金额，单位为分
 * @param {string} params.callbackUrl 回调url
 * @param {string} params.merchantId 商户号
 * @param {object[]} [params.refundList] 退款明细
 * @param {string} params.refundList[].subMerchantId 子成员的商户号（例如：推广员等）
 * @param {string} params.refundList[].refundKind 0-按比例 1-绝对值
 * @param {string} params.refundList[].refundValue 退款比例refundKind=0时代表退款比例 =1时代表退款金额
 */
merfin.refund = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-refund', ...params });
};


/**
 * 退款余额校验
 * @param {object} params
 * @param {string} params.bizRefundId 退款业务单号
 * @param {string} params.bizOrderId 业务单号，和付款时一致
 * @param {string} params.bizType 业务类型，和付款时一致
 * @param {string} params.payType 付款类型，在支付成功时异步回调
 * @param {string} params.refundAmount 退款金额，单位为分
 * @param {string} params.callbackUrl 回调url
 * @param {string} params.merchantId 商户号
 */
merfin.refundCheckBalance = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-refundCheckBalance', ...params });
};

/**
 * 支付回调
 * @param {object} params
 */
merfin.receivePayResult = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-receivePayResult', ...params });
};

/**
 * 退款回调
 * @param {object} params
 */
merfin.receiveRefundResult = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-receiveRefundResult', ...params });
};

/**
 * 聚合支付(C扫B)缓存参数
 * @param {object} params
 */
merfin.cachePayParam = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-cachePayParam', ...params });
};

/**
 * 支付
 * @param {object} params
 */
merfin.createPay = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-fin-payAction-pay', ...params });
};

/**
 * 商户禁用、启用
 * @param {object} params 
 * @param {object} params.merchantIds 商户Id， 多个以逗号间隔
 * @param {object} params.status 状态变更： 0 禁用，1 启用
 * @param {object} params.msg 说明，解释
 */
merfin.changeMerchantStatus = async function (params) {
    // params = Object.assign(format.packJsonParam(params));
    return common.post(this.url, { apiKey: 'ec-mer-changeMerchantStatus', ...params });
};

/**
 * 保存商户和账户信息
 * @param {object} params jsonParam
 */
merfin.saveMerchantAndAccount = async function (params) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(this.url, { apiKey: 'ec-mer-saveMerchantAndAccount', ...params });
};

/**
 * 商户列表查询
 * @param {object} params 
 */
merfin.findFinMerchants = async function (params) {
    return common.post(this.url, { apiKey: 'ec-mer-findFinMerchants', ...params });
};

/**
 * 商户详情列表查询
 * @param {object} params 
 */
merfin.findFinMerchantFulls = async function (params) {
    return common.post(this.url, { apiKey: 'ec-mer-findFinMerchantFulls', ...params });
};

/**
 * 获取渠道平台的排序
 * @param {object} params 
 */
merfin.findPlatformOrder = async function (params) {
    return common.post(this.url, { apiKey: 'ec-mer-findPlatformOrder', ...params });
};

/**
 * 渠道平台排序保存
 * @description 默认支付平台 CITIC-中信 MSFPAY-中投科信 TDEA-腾讯聚鑫 JL-嘉联
 * @param {object} params 
 */
merfin.changePlatformOrder = async function (params) {
    return common.post(this.url, { apiKey: 'ec-mer-changePlatformOrder', ...params });
};

/**
 * 商户认证流程执行
 * @param {object} params 
 */
merfin.executeMerchantCert = async function (params) {
    return common.post(this.url, { apiKey: 'ec-mer-executeMerchantCert', ...params });
};

/**
 * 保存账户信息
 * @param {object} params jsonParam
 */
merfin.saveBankAccount = async function (params) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(this.url, { apiKey: 'ec-mer-saveBankAccount', ...params });
};

/**
 * 商户创建
 * @param {object} params 
 * @param {object} params.idCards 身份证信息，如果有多个，用英文的逗号隔开
 */
merfin.findMerchantInfo = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-findMerchantInfo', ...params });
};

/**
 * 商户认证流程执行
 * @param {object} params 
 * @param {object} params.platforms 渠道平台排序，逗号间隔
 * @param {object} params.merchantId 商户id
 * @param {object} params.isAfresh 是否重复申请（0 否，则已审核通过的商户只异常提醒；1 是，重新再次执行），默认为否
 * @param {object} params.merBankId 商户账户id, 不传则默认取默认账户
 */
merfin.executeMerchantCert = async function (params) {
    // params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-executeMerchantCert', ...params });
};

/**
 * 进件的签约状态
 * @param {object} params 
 * @param {object} params.merchantId 商户表（fin_merchant）的主键
 * @param {object} [params.returnUrl] 签约成功后，页面跳转的url,不填默认为about:blank
 */
merfin.signContract = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-mer-signContract', ...params });
};


// 自检对账
/**
 * [对账]清分入账状态异常
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 */
merfin.ledgOrderFlagCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-ledgOrderFlag', ...params });
};
/**
 * [对账]清分提现状态异常
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 * @param {string} params.startDate 开始日期，例如：2019-10-01
 * @param {string} params.endDate 结束日期，例如：2019-10-19
 */
merfin.finCashBillDataCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-finCashBillData', ...params });
};
/**
 * [对账]入账与账户流水状态异常
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 * @param {string} params.startDate 开始日期，例如：2019-10-01
 * @param {string} params.endDate 结束日期，例如：2019-10-19
 */
merfin.ledgAndFlowDataCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-ledgAndFlowData', ...params });
};
/**
 *[对账] 账户流水余额异常
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 * @param {string} params.startDate 开始日期，例如：2019-10-01
 * @param {string} params.endDate 结束日期，例如：2019-10-19
 */
merfin.finAcctflowDataCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-finAcctflowData', ...params });
};
/**
 * [对账]账户结算金额异常
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 */
merfin.finAcctDataCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-finAcctData', ...params });
};
/**
 * [对账]账户可提金额比对
 * @description 服务端每日早上自行校验
 * @param {object} params
 * @param {string} params.unitIds 商户id,多个可逗号间隔
 */
merfin.useAcctDataCheck = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-useAcctData', ...params });
};
/**
 * 获取所有单元集合
 */
merfin.getAllUnitIds = async function () {
    return common.post(this.url, { apiKey: 'ec-merfin-check-getAllUnitIds' });
};
/**
 * [对账]6个对账集成执行
 * @param {object} params
 * @param {string} params.unitId 商户id
 * @param {string} [params.startDate] 开始日期（不传，默认往前推15天）
 * @param {string} [params.endDate] 结束日期
 */
merfin.checkLedgValid = async function (params) {
    return common.post(this.url, { apiKey: 'ec-merfin-check-checkLedgValid', ...params });
};