const common = require('../../../lib/common');
const format = require('../../../data/format');
const plantCaps = require('../../ecoolPlant/plantCaps');

/**
 * 图形文字识别
 * @description
 * 1. 增加灾备平台，原先身份证识别、银行卡识别都借助腾讯云-文字识别OCR，现增加华为云-文字识别OCR，以防腾讯云无法提供服务（系统参数来进行开关切换）
 */
const img = module.exports = {};

/**
 * 身份证图片识别
 * @description 
 * 1. 系统参数 merfin\_img\_idcard\_platform————身份证识别渠道：1 腾讯云，2 华为云
 * 2. 目前身份证识别，在商户银行卡绑定中使用，且切换到"华为云--OCR"中，身份证识别率远远提升（比腾讯云效果好点）
 * @param {Object} params 
 * @param {string} params.frontUrl 身份证正面url
 * @param {string} params.backUrl 身份证背面url
 */
img.idCardDetect = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-img-idCardDetect', ...params });
};

/**
 * 店铺门头照
 * @description 
 * 1. 传入店铺名字获取店铺门头照
 * 2. 增加灾备平台，原先店铺门头照借助腾讯云-文字水印实现，现增加内部图片url地址提供，以防腾讯云无法提供服务（系统参数来进行开关切换）
 * 3. 系统参数 merfin_img_shopout_platform————店铺门头照来源渠道：1 腾讯云，0 内部
 * 4. 表 bzh_shop_img 增加type类型,来区别店铺内部照与门头照，类型：0 店内照，1 门头照
 * @param {Object} params 
 * @param {string} params.shopName 店铺名称
 */
img.getRandomShopOutImg = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-img-getRandomShopOutImg', ...params });
};

/**
 * 银行卡图片识别
 * @description 
 * 1. 系统参数 merfin\_img\_bankcard\_platform————银行卡识别渠道：1 腾讯云，2 华为云
 * 2. 银行卡识别，目前不清产品端使用情况（可能没有使用）
 * 3. bankCardUrl和docId至少选填一个，都填则以bankCardUrl为准
 * @param {Object} params 
 * @param {string} params.bankCardUrl 银行卡的具体的url
 * @param {string} params.docId 银行卡的docId
 */
img.bankCardDetect = async function (params) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-img-bankCardDetect', ...params });
};