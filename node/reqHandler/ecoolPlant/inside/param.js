const common = require('../../../lib/common');
const format = require('../../../data/format');
const plantCaps = require('../../ecoolPlant/plantCaps');

const param = module.exports = {};

/**
 * 获取系统参数值
 * @param {object} params
 * @param {string} params.code 参数编码
 */
param.getParamInfo = async function (params = {}) {
    return common.post(plantCaps.merfinUrl, { apiKey: 'ec-config-param-getParamInfo', ...params });
};

/**
 * 全局参数变更后置处理
 * @description 主要功能是向所有业务子系统广播消息，通知业务子系统刷新本地参数缓存。（没有请求和返回数据）
 */
param.doGlobalParamChanged = async function () {
    return common.post(plantCaps.confcUrl, { apiKey: 'ec-param-doGlobalParamChanged' });
};

/**
 * 修改参数基本信息
 * @param {object} params
 * @param {string} params.id 参数ID
 * @param {string} params.name 参数名称
 * @param {string} params.val 参数值
 * @param {string} [params.remark] 备注
 */
param.updateBaseInfo = async function (params = {}) {
    return common.post(plantCaps.confcUrl, { apiKey: 'ec-config-param-updateBaseInfo', ...params });
};