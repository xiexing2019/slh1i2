const common = require('../../../lib/common');
const format = require('../../../data/format');
const plantCaps = require('../../ecoolPlant/plantCaps');

const auth = module.exports = {};

/**
 * 登录
 * @description 衣科员工登录
 * @param {object} params
 * @param {string} params.code 工号
 * @param {string} params.pass 密码
 * @param {string} params.saasId 1-商陆好店管理；2-衣科微商城管理；3-衣科运维平台；4-衣科中台管理； 10-衣科新版CRM
 */
auth.staffLogin = async function (params = {}) {
    params = format.packJsonParam(params);
    const res = await common.post(plantCaps.confcUrl, { apiKey: 'ec-config-auth-ecAuthc-staffLogin', ...params });
    LOGINDATA = res.result.data;
    return res;
};

/**
 * 登出
 * @param {object} params
 * @param {string} params.sessionId 
 */
auth.staffLogout = async function (params = {}) {
    return common.post(plantCaps.confcUrl, { apiKey: 'ec-config-auth-ecAuthc-staffLogout', ...params });
};