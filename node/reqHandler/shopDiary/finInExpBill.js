"use strict";
const common = require('../../lib/common');
const format = require('../../data/format');

let bill = module.exports = {};



/**
 * ec-sspd-finExpBill-deleteBill 作废支出单
 * @param {Object} params
 * @param {Object} params.id
 */
bill.deleteFinExpBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-sspd-finExpBill-deleteBill',
		...params
	});
};

/**
 * ec-sspd-finExpBill-saveBill 保存支出单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
bill.saveFinExpBill = async function (params) {
    params = format.packJsonParam(params);
    const res = await common.apiDo({
        apiKey: 'ec-sspd-finExpBill-saveBill',
        ...params
    });
    await common.delay(500);
    return res;
};

/**
 *  查询收支单完整信息
 * @param {object} params
 * @param {string} params.id 收支单id
 */
bill.getFinInExpBillFull = async function (params) {
    params.wrapper = true;
    return common.apiDo({
        apiKey: 'ec-sspd-finInExpBIll-getBillFull',
        ...params
    });
};

/**
 * 收支列表
 * @description ec-sspd-finInExpBill-findBillWithOneDet
 * @param {Object} params
 * @param {Object} params.jsonParam
 */
bill.getFinInExpBillList = async function (params = {}) {
    params.wrapper = true;
    params.pageNo = 1;
    params.pageSize = 20;
    params.jsonParam = Object.assign({}, {
        flag: params.flag || 1,//1不显示作废
        proDateGte: params.proDate || common.getCurrentDate(),
        proDateLte: params.proDate || common.getCurrentDate(),
    }, params.jsonParam);
    return common.apiDo({ apiKey: 'ec-sspd-finInExpBill-findBillWithOneDet', ...params });
};

/**
 * 收支汇总列表
 * @param {object} params
 * @param {object} params.jsonParam
 */
bill.finInExpSumList = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-finInExpBill-findClsSumList',
        pageSize: params.pageSize || 20,
        pageNo: params.pageNo || 1,
        wrapper: true,
        jsonParam: {
            proDateGte: params.proDateGte || '2019-01-01',
            proDateLte: params.proDateLte || common.getCurrentDate(),
            flag: 1,
            inExpType: params.inExpType || 1//1收入2支出
        },
        ...params
    });
};

/**
 * 收支图表
 * @param {object} params
 * @param {object} params.jsonParam
 */
bill.finInExpSumChart = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-finInExpBill-findClsSumChart',
        wrapper: true,
        jsonParam: {
            proDateGte: params.proDateGte || '2019-01-01',
            proDateLte: params.proDateLte || common.getCurrentDate(),
            flag: 1,
            inExpType: params.inExpType || 1//1收入2支出
        },
        ...params
    });
};