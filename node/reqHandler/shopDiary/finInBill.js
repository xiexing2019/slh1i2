"use strict";
const common = require('../../lib/common');
const format = require('../../data/format');

let finInBill = module.exports = {};


/**
 * ec-sspd-finInBill-saveBill 保存收入单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
finInBill.saveFinInBill = async function (params) {
    params = format.packJsonParam(params);
    const res = await common.apiDo({
        apiKey: 'ec-sspd-finInBill-saveBill',
        ...params
    });
    await common.delay(500);
    return res;
};

/**
 * ec-sspd-finInBill-deleteBill 作废收入单
 * @param {Object} params
 * @param {Object} params.id
 */
finInBill.deleteFinInBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-sspd-finInBill-deleteBill',
        ...params
    });
};