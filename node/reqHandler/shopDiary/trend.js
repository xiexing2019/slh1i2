'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let trend = module.exports = {};


/**
 * 智能引擎
 * ec-sspd-trend-diary
 */
trend.trendDiary = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-trend-diary',
        timeKind: 1, //1-按日 2-按周 3-按月 4-按年
        now: common.getCurrentDate(),
        shopId: LOGINDATA.shopId,
        ...params
    })
};

/**
 * 笑铺销售挂单列表接口不走原来二代的挂单接口
 * ec-trade-pendBill-findPendBill 查询销售单挂单列表
 * @param {Object} params
 * @param {Object} params.jsonParam
 */
trend.getSalesPendingList = async function (params = {}) {
    params = format.packJsonParam(params);
    params.jsonParam = Object.assign({}, {
        orderBy: 'createdDate',
        flag: 1,
        orderByDesc: true,
        shopId: LOGINDATA.shopId,
    }, params.jsonParam);
    return common.apiDo({
        apiKey: 'ec-trade-pendBill-findPendSimpleBill', wrapper: true, ...params
    });
};

/**
 * ec-trade-pendBill-findPendBill 查询挂单列表
 * @description typeId 销售单：1100 销售订单：1200 采购单：1300 采购订单：1400 销售发货单：1500 销售订单配货单：1600 采购收货单：1700 期初入库单：3100 其他入库单：3200 其他出库单：3300 调整出入库单：3400 盘点单：3500 盘点计划：3600 调拨入库单：3700 要货单：3900
 * @param {Object} params
 * @param {string} params.jsonParam
 * @param {string} params.jsonParam.typeId 单据类型
 */
trend.getPendBillList = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-trade-pendBill-findPendBill',
        orderBy: 'proDate',
        flag: 1,
        orderByDesc: true,
        typeId: 1300,
        ...params
    });
};


/**
 * ec-trade-purBill-deleteBill 作废采购单
 * @param {Object} params
 */
trend.deletePurchaseBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-trade-purBill-deleteBill', ...params });
};