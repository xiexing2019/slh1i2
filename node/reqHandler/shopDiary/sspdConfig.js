const common = require('../../lib/common');
const format = require('../../data/format');


let sspdConfig = module.exports = {};

/**
 * 行业列表
 * @param {object} params jsonParam
 * @param {string} params.codeId 父级id 第一级传0 之后的传上一级的code
 */
sspdConfig.sysDictTree = async function (params) {
    params.hierId = 2011; //字典定义id(2011写死)
    params.includeSubs = true;
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-config-sysDictTree',
        ...params
    });
};

/**
 * 初始化行业
 * @param {object} params jsonParam
 * @param {string} params.industryId 行业id
 */
sspdConfig.transUpdateIndustry = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-config-ugr-unit-transUpdateIndustry',
        ...params
    });
};


/**
 * 可批量获取参数，也可获取一个参数，批量获取就要jsonParam
 * @description
 * @param {object} params
 * @param {object} params.domainKind 配置域 系统:system,业务:business,移动:frontEnd,个人:personal,角色:role
 */
sspdConfig.getParamValues = async function (params = {}) {
    params = Object.assign({
        ownerKind: params.ownerKind || 7,
    }, params);
    return common.apiDo({ apiKey: 'ec-config-param-getParamVals', ...params });
};

/**
 * 保存参数
 * @description data: [{code:"" ,domainKind: "business",ownerId: LOGINDATA.unitId,val: "1"}]
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
sspdConfig.saveParamValue = async function (params) {
    params = format.packJsonParam(Object.assign({ ownerKind: 9 }, params));
    return common.apiDo({ apiKey: 'ec-config-param-saveOwnerVal', ...params });
};

/**
 * 获取系统参数
 * @description {"code":"favor_money_ceil","domainKind":"business","ownerKind":"7","ownerId":23093}
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
sspdConfig.getParamInfo = async function (params) {
    params = format.packJsonParam(Object.assign({ ownerKind: 7 }, params));
    return common.apiDo({ apiKey: 'ec-config-param-getParamInfo', ...params });
};

/**
 * 获取设备参数
 * @description{"ownerKind":"7","data":[{"code":"invinout_checknum","domainKind":"business","ownerId":22157,"val":"1"}]}
 * domainKind:配置域，ownerId-ownerKind为7时，值为unitId
 * @param {object} params
 * @param {Array} params.data
 */
sspdConfig.findProductParams = async function (params) {
    params = format.packJsonParam(params);
    params.productType = 'slh';
    params.pageSize = 20;
    params.pageNo = 1;
    params.wrapper = true;
    return common.apiDo({ apiKey: 'ec-config-param-findProductParams', ...params });
};