const name = require('../../data/caps').name;

/**
 *
 * confc.ec_staff
 *
 * seller6 绑定saas
 * seller7 独立app
 */
const account = {
    //对应商城sst
    cs3d2: {
        seller1: { mobile: '18760953333', shopName: '商城5', appId: 'wxbf7dc3cc240d8d1e', tenantId: 10034, unitId: 10030, appKey: 'W7wWVgwkMHx9ukq0pF', appSecret: '1f2e5c19-558a-4865-a383-0d0be861b12e' },
        seller7: { mobile: '12907099151', shopName: '店铺hwF5V', appId: 'wxbf7dc3cc240d8d1e', tenantId: 8586 },
        // client: { mobile: '13221197273', openId: 'oJM315WVczcaeN9j6UAklC_sKE5g' },
        disSeller6: { mobile: '18880000000', appId: 'wx4bcc55bd3ce7e912' },
        // client: { mobile: '13175051890', openId: 'oJM315T183V7yCCzQsuGYKTDyDYg' },
        client: { mobile: '13429157392', openId: 'ooaMf0YMsKi4h5it4kdQWUDy84LA' },

        ecStaff: { code: 'autotest', pass: '000000' },
        sellerPerformance: { mobile: '12907174928', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 10222 }
    },
    //对应商城sst
    cs3d3: {
        seller1: { mobile: '18588888881', shopName: 'qiuD31', appId: 'wx4bcc55bd3ce7e912', tenantId: 12346, unitId: 12342, appKey: 'cO6r5H0XHQSs7A9Mbx', appSecret: '79f8e154-376f-4c66-92e6-b41875912e01' },
        client: { mobile: '13429157392', openId: 'ooaMf0YMsKi4h5it4kdQWUDy84LA' }
    },
    //对应商城ssbei//北方
    cs3d1: {
        seller1: { mobile: '12908087593', shopName: '小王', appId: 'wxc09fcf8155edcecd', tenantId: 499325, unitId: 499325, appKey: 'NSpwu4fWPcj7Ikskf9', appSecret: '35cf8254-b239-4cbd-8f23-7cd7b18a7e2c' },
        seller2: { mobile: '13112345678', shopName: '商城5', appId: 'wx4bcc55bd3ce7e912', tenantId: 1575, appKey: 'HDNdQO4UUikVjRSwvZ', appSecret: '4c16d32b-704d-4bb6-a9b7-9bb1d268b786' },
        seller4: { mobile: '15121212323', shopName: '可惜啦' },//审核环境测试店铺
        seller6: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929, appKey: 'IIaOK7BuKseHJN24b0', appSecret: '281f57de-a3ff-41e7-8191-d896efc157c1' },
        seller7: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929 },
        client: { mobile: '13429157392', openId: 'oWy_j5LrjntUbfUB9XFRho0R7Zfs' },
        ecStaff: { code: 'admin', pass: '000000' },
        sellerPerformance: { mobile: '12907188577', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 6787 },
    },
    //对应商城ss//南方
    sd_cg4: {
        seller1: { mobile: '18760959806', shopName: '笑铺微商城', appId: 'wxde3cf9698772f7a8', tenantId: 455629, unitId: 455629, appKey: 'LsGjZVSlFriKqNJZxh', appSecret: '9b057326-20a7-4d88-9c07-4fa6780825fc' },
    },
};



module.exports = account[name];