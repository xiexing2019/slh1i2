'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let finIo = module.exports = {};


/**
 * 收付款单列表查询
 * @description ec-sspd-finIo-findCompVerify
 * @param {Object} params
 */
finIo.findCompVerifyList = async function (params = {}) {
    // params.bizType = params.bizType || 1100;    //1100：销售核销 ，1300：采购核销
    // params.queryType = params.queryType || 1;//1：查询本门店未核销，4：本单已核销
    parasm = Object.assign({ wrapper: true, queryType: 1, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId }, params);
    return common.apiDo({ apiKey: 'ec-sspd-finIo-findCompVerify', ...params });
};

