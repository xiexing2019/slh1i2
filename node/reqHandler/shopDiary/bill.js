'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');
const sspd = require('./../shopDiary');
const trade = require('../slh2/trade');

const bill = module.exports = {};

/**
 * 拼接jsonParam
 * @param {object} jsonParam
 */
bill.jsonParamFormat = (jsonParam) => {
	let totalMoney = 0,
		totalNum = 0,
		actualPay = 0;
	//更新hashKey moment().format('YYYY-MM-DD HH:mm:ss:SSS')
	jsonParam.main.hashKey = `ShopDiary-${LOGINDATA.deviceNo}-${Date.now()}-${common.getRandomNum(100, 999)}`;

	//明细
	jsonParam.details.forEach(element => {
		//实际价格 可自行改动
		if (!element.realPrice) element.realPrice = common.mul(element.price, jsonParam.main.mainDiscount);

		element.money = common.mul(element.realPrice, element.num);
		totalMoney += element.money;
		totalNum += element.num;
	});

	//支付  payFlag 支付状态（1 支付完成，0 支付中，-1 支付失败），默认为支付完成
	jsonParam.payways.forEach(element => {
		actualPay += element.money;//if (element.payFlag == 1)
	});

	//main
	jsonParam.main.totalNum = totalNum;
	jsonParam.main.totalMoney = totalMoney - jsonParam.main.favorMoney;

	//fin balance=实付+抹零-总额 （笑铺现在没有核销功能）
	if (!jsonParam.specials) {
		jsonParam.fin.balance = actualPay + jsonParam.main.favorMoney - totalMoney;
	} else {
		jsonParam.main.totalMoney = totalMoney + jsonParam.specials[0].money;
	};
	console.log(jsonParam);
	return jsonParam;
};

/**
 * 查询列表接口的params
 */
const qlDefParams = {
	orderBy: 'proDate,billNo',
	orderByDesc: true,
	pageNo: 1,
	pageSize: 15,
	//shopId: LOGINDATA.shopId,
	wrapper: true,
	// flag: 1,
	proDateGte: moment().format('YYYY-MM-DD'),
	proDateLte: moment().format('YYYY-MM-DD'),
};


//进货

/**
 * ec-trade-purBill-saveBill 保存进货单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
bill.savePurchaseBill = async function (params) {
	params.jsonParam = bill.jsonParamFormat(params.jsonParam);
	const res = await trade.savePurchaseBill(params);
	await common.delay(500);
	return res;
};

/**
 * 查询挂单列表
 * @param {Object} params
 */
bill.getPurchasePendingList = async function (params = {}) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, qlDefParams, {
		orderBy: 'proDate',
		typeId: 1300
	}, params.jsonParam);
	return trade.getPendBillList(params);
};




//在线支付


bill.saveSalesBillPayOnline = async function (params) {
	const sfRes = await sspd.salesBill.saveSalesBill(params);
	await sspd.salesBill.createSalesBillPay({ billId: sfRes.result.data.val });
	await sspd.salesBill.misSalesBillPayCallback({ mainId: sfRes.result.data.val });
	return sfRes;
};


/**
 * 同步商城订单
 * @param {object} params
 */
bill.saveMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderPay',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单退款退货
 * @param {object} params
 */
bill.RefundMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderRefund',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单退货
 * @param {object} params
 */
bill.ReturnMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderReturn',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单提现
 * @param {object} params
 */
bill.CashOutMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderVerify',
		...params
	});
	await common.delay(500);
	return res;
};


