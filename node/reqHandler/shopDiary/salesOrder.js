'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

const salesOrder = module.exports = {};


/**
 * 笑铺新增订单
 */
salesOrder.saveSalesOrderBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-saveBill',
        ...params
    })
};

/**
 * 笑铺中的订单列表
 * @param  {object} params.jsonparam
 * @return {object}
 */
salesOrder.salesOrderBillList = async function (params = {}) {
    params.wrapper = true;
    params.flag = params.flag || 1;
    params.pageNo = params.pageNo || 1;
    params.pageSize = params.pageSize || 20;
    params.jsonParam = Object.assign({}, {
        orderBy: params.orderBy || 'billNo desc',
        proDateGte: params.proDate || common.getCurrentDate(),
        proDateLte: params.proDate || common.getCurrentDate(),
    }, params.jsonParam);
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-findBill',
        ...params
    })
};

/**
 *  笑铺中的订单详情
 * @param {object} params
 */
salesOrder.getSalesOrderBillFull = async function (params) {
    params.wrapper = true;
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-getBillFull',
        isPend: 0,
        ...params
    });
};

/**
 * 作废销售订单
 * @param {Object} params
 * @param {string} params.id
 * @param {Object} params.isPend
 */
salesOrder.deleteSalesOrderBill = async function (params) {
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-deleteBill',
        isPend: 0,
        ...params
    });
};

/**
 * 销售订单终结
 * @param {Object} params
 * @param {string} params.id
 */
salesOrder.terminateSalesOrderBill = async function (params) {
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-terminateOrder',
        ...params
    });
};


/**
 * 笑铺订单设置下次发货日期
 */
salesOrder.setNextExpectDate = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({
        apiKey: 'ec-sspd-salesOrder-setNextExpectDate',
        ...params
    })
};