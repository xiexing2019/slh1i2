'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');

const area = module.exports = {};


/**
 *自定义统计
 * ec-sspd-area-diary
 */
area.areaDiary = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-area-diary',
        startDate: common.getCurrentDate(),
        endDate: common.getCurrentDate(),
        shopId: LOGINDATA.shopId,
        ...params
    });
};