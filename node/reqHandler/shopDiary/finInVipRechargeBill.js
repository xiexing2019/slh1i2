"use strict";
const common = require('../../lib/common');
const format = require('../../data/format');

let bill = module.exports = {};



/**
 * 充值
 *
 * @param  {object} params jsonparam
 * @param  {object} params.jsonparam
 * @return {object}
 */
bill.saveRechargeBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-sspd-finInVipRechargeBill-saveBill', ...params });
};

/**
 * 余额充值列表
 *
 * @param  {Object} params 
 * @param  {Object} params.compId
 * @return {type}        
 */
bill.getRechargeBillList = async function (params) {
    params = Object.assign({
        // flag: 1,//是否显示作废明细 1 代表不显示
        proDateGte: common.getCurrentDate(),
        proDateLte: common.getCurrentDate(),
        shopId: LOGINDATA.shopId,
        orderBy: "proDate desc,id desc"
    }, params);
    return common.apiDo({ apiKey: 'ec-sspd-finInVipRechargeBill-listWithPayWays', wrapper: true, ...params });
};

/**
 * 余额充值详细
 * @param  {Object} params
 * @param  {Object} params.id 
 */
bill.getRechargeBillInfo = async function (params) {
    return common.apiDo({ apiKey: 'ec-sspd-finInVipRechargeBill-getBillFull', wrapper: true, ...params });
};


/**
 * 作废充值单
 * @param  {Object} params
 * @param  {Object} params.id 
 */
bill.deleteRechargeBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-sspd-finInVipRechargeBill-deleteBill', ...params });
};