'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');

let accCheck = module.exports = {};

/**
 * 客户对账单
 * @param {Object} params
 * @param {Object} params.compId 客户ID
 */
accCheck.getSimpleCustAcctCheck = async function (params) {
    let qlParams = Object.assign({
        orderBy: 'proDate desc,createdDate desc',
        orderByDesc: true,
        pageSize: 20,
        pageNo: 1,
        shopId: LOGINDATA.shopId,
        wrapper: true,
        proDateGte: '2019-01-01',
        proDateLte: common.getCurrentDate(),
        jsonParam: {}
    }, params);
    return common.apiDo({
        apiKey: 'ec-sspd-accCheck-simpleCustAccCheck',
        ...qlParams
    });
};
