'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');
const bill = require('./bill');

const salesBill = module.exports = {};


/**
 * 按订单获取销售单数据
 * @param {Object} params
 */
salesBill.getBillFullFromOrder = async function (params) {
    return common.apiDo({
        apiKey: 'ec-sspd-salesBill-getBillFullFromOrder',
        wrapper: true,
        ...params
    });
};

/**
 * 客户分析储值及收款统计接口
 * @param {Object} params 
 */
salesBill.getCustRechargeNum = async function (params = {}) {
    params = Object.assign({
        wrapper: true,
        orderByDesc: true,
        proDateGte: '2019-01-01',
        proDateLte: common.getCurrentDate(),
        orderBy: 'recharge',
    }, params);
    return common.apiDo({
        apiKey: 'ec-sspd-salesBill-getCusRechargeNum',
        ...params
    });
};

/**
 * ec-sspd-saveBill 保存销售单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
salesBill.saveSalesBill = async function (params) {
    if (params.jsonParam.main.hasOwnProperty('srcType') && params.jsonParam.main.srcType != 8)
        params.jsonParam = bill.jsonParamFormat(params.jsonParam);
    const res = await common.apiDo({ apiKey: 'ec-sspd-saveBill', ...params });
    await common.delay(500);
    return res;
};

/**
 *  查询销售单单完整信息
 * @param {object} params
 * @param {number} params.isPend 是否挂单单据(1 是/0 否。指定要作废的单据是否处于挂单状态)*
 * @param {string} params.id 销售单主表ID或挂单ID（isPend=0时，设置为销售单主表ID。isPend=1时，设置为挂单ID）。不传时，返回新增单据时的默认值。
 */
salesBill.getSalesBillFull = async function (params) {
    params.wrapper = true;
    return common.apiDo({ apiKey: 'ec-sspd-getBillFull', ...params });
};


/**
 * 销售单列表查询
 * @description ec-sspd-sales-bill-list
 * @param {Object} params
 * @param {Object} params.jsonParam
 * @return {object} srcTypeIn 单据类型  1为销售单 8为收款单 12为商城订单 13为微商城退单，2为按订货开单,5预付款单
 * {"flag":1,"proDateGte":"2018-10-23","proDateLte":"2018-10-23","shopId":762562,"compId":763178,"srcType":8,"orderBy":"proDate desc,id desc"}
 */
salesBill.getSalesBillList = async function (params = {}) {
    params.wrapper = true;
    params.jsonParam = Object.assign({}, {
        //flag: params.flag || 1,
        proDateGte: params.proDate || common.getCurrentDate(),
        proDateLte: params.proDate || common.getCurrentDate(),
        //compId: params.compId || BASICDATA.ids.dwidXw,
        //srcType: params.srcType || 8,
        srcTypeIn: params.srcType || "1,2,12,13,5",
        orderBy: "proDate desc,billNo desc",
        shopId: LOGINDATA.shopId,
        pageNo: 1,
        pageSize: 15
    }, params.jsonParam);
    return common.apiDo({ apiKey: 'ec-sspd-sales-bill-list', ...params });
};

/**
 * 收款明细
 * @description ec-sspd-sales-bill-list
 * @param {Object} params
 * @param {Object} params.jsonParam
 * @return {object} srcType 单据类型  1为销售单 8为收款单 12为商城订单
 */
salesBill.getSalesBillListDTL = async function (params = {}) {
    params.wrapper = true;
    params.jsonParam = Object.assign({}, {
        //flag: 1,
        proDateGte: params.proDate || common.getCurrentDate(),
        proDateLte: params.proDate || common.getCurrentDate(),
        compId: params.compId || BASICDATA.ids.dwidXw,
        srcType: params.srcType || 8,
        //srcTypeIn: params.srcType || "1,12,13",
        orderBy: "proDate desc,id desc",
        shopId: LOGINDATA.shopId,
        pageNo: 1,
        pageSize: 20
    }, params.jsonParam);
    return common.apiDo({ apiKey: 'ec-sspd-sales-bill-list', ...params });
};

/**
 * ec-trade-purBill-deleteBill 作废销售单
 * @param {Object} params
 * @param {Object} params.id
 * @param {Object} params.isPend
 */
salesBill.deleteSalesBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-sspd-salesBill-deleteBill', ...params });
};

/**
 * 销售单明细
 * @param {Object} params
 * @param {Object} params searchToken
 */
salesBill.getSalesDetList = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        proDateGte: common.getCurrentDate(),
        proDateLte: common.getCurrentDate(),
        shopId: LOGINDATA.shopId,
        orderBy: 'proDate desc,billNo desc',
        wrapper: true,
        pageSize: 0,
    }, params));
    return common.apiDo({ apiKey: 'ec-sspd-salesBill-findDetailDTO', ...params });
};

/**
 * 开单获取上次价
 * @param {Object} params 
 * @param {string} params.tenantSpuIds 多个租户级spu id，以”,”拼接
 */
salesBill.getSalesLastPrice = async function (params) {
    return common.apiDo({ apiKey: 'ec-sspd-salesBillDetail-getLastPrice', ...params });
};

/**
 * 发起支付 销售单/收款单
 * @description 只是为了让服务端判断扫码支付的第三方为支付宝还是微信
 * @description 支付金额限额500
 * @param  {object} params
 * @param {string} params.billId 单据ID
 */
salesBill.createSalesBillPay = async function (params) {
    params.payCode = 26 + common.getRandomNumStr(16);//支付码内容
    params.isStrict = 1;//是否严格模式，笑铺固定传1
    params.check = false;
    return common.apiDo({ apiKey: 'ec-sspd-salesbill-launchMisOnlinePay', ...params });
};

/**
 * 销售单支付回调
 * @param  {object} params
 * @param {string} params.mainId billId
 */
salesBill.misSalesBillPayCallback = async function (params) {
    //  code=xxxx & msg=xxxx & unitId=xxx & shopId=xxx
    params.payId = -common.getRandomNumStr(20);
    params.code = 0;
    params.msg = '支付成功';
    params.unitId = LOGINDATA.unitId;
    params.shopId = LOGINDATA.shopId;
    return common.apiDo({ apiKey: 'ec-sspd-salesbill-misOnlinePayCallback', ...params });
};