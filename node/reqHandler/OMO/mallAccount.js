const name = require('../../data/caps').name;

/**
 *
 * confc.ec_staff
 *
 * seller6 绑定saas
 * seller7 独立app
 */
const account = {
    //对应商城sst
    cs3d2: {
        seller1: { mobile: '18760953333', shopName: '商城5', appId: 'wx4bcc55bd3ce7e912', tenantId: 10034, appKey: 'W7wWVgwkMHx9ukq0pF', appSecret: '1f2e5c19-558a-4865-a383-0d0be861b12e' },
        seller7: { mobile: '12907099151', shopName: '店铺hwF5V', appId: 'wxbf7dc3cc240d8d1e', tenantId: 8586 },
        // client: { mobile: '13221197273', openId: 'oJM315WVczcaeN9j6UAklC_sKE5g' },
        disSeller6: { mobile: '18880000000', appId: 'wx4bcc55bd3ce7e912' },
        // client: { mobile: '13175051890', openId: 'oJM315T183V7yCCzQsuGYKTDyDYg' },
        client: { mobile: '13429157392', openId: 'ooaMf0YMsKi4h5it4kdQWUDy84LA' },

        ecStaff: { code: 'autotest', pass: '000000' },
        sellerPerformance: { mobile: '12907174928', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 10222 }
    },
    //对应商城ss_online
    cs3d1: {
        seller1: { mobile: '12908087593', shopName: '小王', appId: 'wxc09fcf8155edcecd', tenantId: 499325, appKey: 'NSpwu4fWPcj7Ikskf9', appSecret: '35cf8254-b239-4cbd-8f23-7cd7b18a7e2c' },
        seller2: { mobile: '13112345678', shopName: '商城5', appId: 'wx4bcc55bd3ce7e912', tenantId: 1575, appKey: 'HDNdQO4UUikVjRSwvZ', appSecret: '4c16d32b-704d-4bb6-a9b7-9bb1d268b786' },
        seller4: { mobile: '15121212323', shopName: '可惜啦' },//审核环境测试店铺
        seller6: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929, appKey: 'IIaOK7BuKseHJN24b0', appSecret: '281f57de-a3ff-41e7-8191-d896efc157c1' },
        seller7: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929 },
        client: { mobile: '13429157392', openId: 'oWy_j5LrjntUbfUB9XFRho0R7Zfs' },
        ecStaff: { code: 'admin', pass: '000000' },
        sellerPerformance: { mobile: '12907188577', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 6787 },
    },
};



module.exports = account[name];