"use strict";
const caps = require('../../data/caps');
const common = require('../../lib/common');
const format = require('../../data/format');
let reqHandler = module.exports = {};



/**
 * 收支汇总列表
 * @param {object} params
 * @param {object} params.jsonParam
 */
reqHandler.finInExpSumList = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-finInExpBill-findClsSumList',
        pageSize: params.pageSize || 20,
        pageNo: params.pageNo || 1,
        wrapper: true,
        jsonParam: {
            proDateGte: params.proDateGte || '2019-01-01',
            proDateLte: params.proDateLte || common.getCurrentDate(),
            flag: 1,
            inExpType: params.inExpType || 1//1收入2支出
        },
        ...params
    });
};

/**
 * 收支图表
 * @param {object} params
 * @param {object} params.jsonParam
 */
reqHandler.finInExpSumChart = async function (params = {}) {
    return common.apiDo({
        apiKey: 'ec-sspd-finInExpBill-findClsSumChart',
        wrapper: true,
        jsonParam: {
            proDateGte: params.proDateGte || '2019-01-01',
            proDateLte: params.proDateLte || common.getCurrentDate(),
            flag: 1,
            inExpType: params.inExpType || 1//1收入2支出
        },
        ...params
    });
};