"use strict";
const caps = require('../../data/caps');
const common = require('../../lib/common');
const format = require('../../data/format');
let reqHandler = module.exports = {};



/**
 * 新增款号
 * @param {object} params
 * @param {object} params.jsonParam
 */
reqHandler.saveStyle = async function (params, directInv = false) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-dres-spu-save',
		directInv,//建款直接入库传true,建款需要入账传false,不传服务端默认false
		...params
	});
};


/**
 * 款号详情
 * @param {object} params
 * @param {object} params.id 租户spu编号
 */
reqHandler.getStyleInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-dres-spu-getBeanById', wrapper: true, ...params });
};


/**
 * 款号搜索
 */
reqHandler.getStyleInfoList = async function (params) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, {
		//orderByDesc: true,
		needPurPrice: true,
		needStockNum: true,
		wrapper: true,
		shopId: LOGINDATA.shopId,
		flag: 1, //默认为有效款号
		//marketDateStart: common.getCurrentDate(),
		//marketDateEnd: common.getCurrentDate(),
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-dres-spu-list', ...params });
};

/**
 * 款号停用
 * @param {object} params
 * @param {object} params.ids 租户款号编号列表，多个时以”,”分割
 */
reqHandler.disableStyle = async function (params) {
	params = Object.assign({ shopId: LOGINDATA.shopId, }, params);
	return common.apiDo({ apiKey: 'ec-dres-spu-disable', ...params });
};

/**
 * 款号启用
 * @param {object} params
 * @param {object} params.ids 租户款号编号列表，多个时以”,”分割
 */
reqHandler.enableStyle = async function (params) {
	params = Object.assign({ shopId: LOGINDATA.shopId, }, params);
	return common.apiDo({ apiKey: 'ec-dres-spu-enable', ...params });
};

/**
 * 库存流水
 * @param {object} params
 * @param {object} params.tenantSpuId
 * @param {object} params.bizDateStart
 */
reqHandler.getInvFlowList = async function (params) {
	params = Object.assign({
		shopId: LOGINDATA.shopId,
		pageNo: 1,
		pageSize: 0,
		//bizDateStart: common.getCurrentDate(),
		bizDateEnd: common.getCurrentDate()
	}, params);
	return common.apiDo({ apiKey: 'ec-inv-spu-flow-list', ...params });
}


/**
 * 字典类新增
 * @param {object} params
 * @param {object} params.jsonParam
 */
reqHandler.saveDict = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-config-save-dict', ...params, });
};


/**
 * 品牌，材质，风格详情
 * @param {object} params
 * @param {object} params.id
 * @param {object} params.typeId
 */
reqHandler.getDictInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-config-getById-dict', ...params });
};

/**
 * 品牌，材质，风格列表
 * @param {object} params
 * @param {object} params.typeId
 * @param {object} params.unitId
 */
reqHandler.getDictList = async function (params) {
	//显示停用就不传flag
	let qlParams = Object.assign({
		pageSize: 0,
		wrapper: true,
		orderBy: 'updatedDate',
		orderByDesc: true,
	}, format.packJsonParam(params));
	// console.log(`qlParams=${JSON.stringify(qlParams)}`);
	return common.apiDo({ apiKey: 'ec-slh2-conf-dictListWithDresNum', ...qlParams });
};



/**
 * 停用字典
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.disableDict = async function (params) {
	return common.apiDo({
		apiKey: 'ec-config-disable-dict', ...params
	});
};

/**
 * 启用字典
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.enableDict = async function (params) {
	return common.apiDo({
		apiKey: 'ec-config-enable-dict', ...params
	});
};

/**
 * 新增类别
 * @param {object} params
 * @param {object} params.jsonParam
 * @param {object} params.jsonParam.name
 * @param {object} params.jsonParam.parentId 上级分类id
 * @param {object} params.jsonParam.rem
 */
reqHandler.saveClass = async function (params = {}) {
	params = format.packJsonParam(Object.assign({
		name: '类别' + common.getRandomStr(5),
		rem: '类别备注' + common.getRandomStr(10),
	}, params));
	return common.apiDo({
		apiKey: 'ec-dres-class-save', ...params
	});
};



/**
 * 类别详情
 * @param {object} params
 * @param {object} params.id
 *
 */
reqHandler.getClassInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-getById',
		wrapper: true,
		...params
	});
};


/**
 * 类别列表
 * @param {object} params
 * @param {object} params.jsonParam 默认显示启用的类别，显示停用的类别jsonParam 传空
 *
 */
reqHandler.getClassList = async function (params = {}) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-dres-queryClassTree',
		wrapper: true,
		includeSubs: true,
		...params
	});
};


/**
 * 停用类别
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.disableClass = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-disable',
		...params
	})
};

/**
 * 启用类别
 * @param {object} params
 * @param {object} params.id
 */
reqHandler.enableClass = async function (params) {
	return common.apiDo({
		apiKey: 'ec-dres-class-enable',
		...params
	})
};



/**
 * 获取款号自动停用参数
 * 
 */
reqHandler.getStyleAutoStop = async function () {
	return common.apiDo({ apiKey: 'ec-dres-autoStop-get' });
};

/**
 * 保存款号自动停用参数
 * @param {object} params
 * @param {object} params.flag
 * @param {object} params.marketDay 上架天数且库存为0
 * @param {object} params.stockZeroDay 库存无变动
 *  @param {object} params.rem 备注信息
 */
reqHandler.updataStyleAutoStopParam = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-dres-autoStop-save', ...params })
}


/**
 * 获取条码自动生成参数
 * 
 */
reqHandler.getScodeAutoSet = async function () {
	return common.apiDo({ apiKey: 'ec-dres-fetchSimpleRuleSetConfig' });
};

/**
 * 保存条码自动生成参数
 * @param {object} params
 * @param {object} params.styleUseBarcode 1 --是否启用 0关闭 1启用
 */
reqHandler.updataScodeAutoSet = async function (params) {
	params = format.packJsonParam(Object.assign({ unitId: LIGINDATA.unitId }, params));
	return common.apiDo({ apiKey: 'ec-dres-saveRuleSetConfig', ...params });
};


