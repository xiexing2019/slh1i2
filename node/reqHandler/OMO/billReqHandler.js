'use strict';
// const caps = require('../../data/caps');
const common = require('../../lib/common');
const tradeReqHandler = require('../slh2/trade');
const moment = require('moment');
const format = require('../../data/format');
let reqHandler = module.exports = {};

/**
 * 拼接jsonParam
 * @param {object} jsonParam
 */
reqHandler.jsonParamFormat = (jsonParam) => {
	let totalMoney = 0,
		totalNum = 0,
		actualPay = 0;
	//更新hashKey moment().format('YYYY-MM-DD HH:mm:ss:SSS')
	jsonParam.main.hashKey = `ShopDiary-${LOGINDATA.deviceNo}-${Date.now()}-${common.getRandomNum(100, 999)}`;

	//明细
	jsonParam.details.forEach(element => {
		//实际价格 可自行改动
		if (!element.realPrice) element.realPrice = common.mul(element.price, jsonParam.main.mainDiscount);

		element.money = common.mul(element.realPrice, element.num);
		totalMoney += element.money;
		totalNum += element.num;
	});

	//支付  payFlag 支付状态（1 支付完成，0 支付中，-1 支付失败），默认为支付完成
	jsonParam.payways.forEach(element => {
		actualPay += element.money;//if (element.payFlag == 1)
	});

	//main
	jsonParam.main.totalNum = totalNum;
	jsonParam.main.totalMoney = totalMoney - jsonParam.main.favorMoney;

	//fin balance=实付+抹零-总额 （笑铺现在没有核销功能）
	jsonParam.fin.balance = actualPay + jsonParam.main.favorMoney - totalMoney;
	return jsonParam;
};

/**
 * 查询列表接口的params
 */
const qlDefParams = {
	orderBy: 'proDate,billNo',
	orderByDesc: true,
	pageNo: 1,
	pageSize: 15,
	//shopId: LOGINDATA.shopId,
	wrapper: true,
	// flag: 1,
	proDateGte: moment().format('YYYY-MM-DD'),
	proDateLte: moment().format('YYYY-MM-DD'),
};

//销售

/**
 * ec-sspd-saveBill 保存销售单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
reqHandler.saveSalesBill = async function (params) {
	if (params.jsonParam.main.hasOwnProperty('srcType') && params.jsonParam.main.srcType != 8)
		params.jsonParam = reqHandler.jsonParamFormat(params.jsonParam);
	const res = await common.apiDo({
		apiKey: 'ec-sspd-saveBill',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 *  查询销售单单完整信息
 * @param {object} params
 * @param {number} params.isPend 是否挂单单据(1 是/0 否。指定要作废的单据是否处于挂单状态)*
 * @param {string} params.id 销售单主表ID或挂单ID（isPend=0时，设置为销售单主表ID。isPend=1时，设置为挂单ID）。不传时，返回新增单据时的默认值。
 */
reqHandler.getSalesBillFull = async function (params) {
	params.wrapper = true;
	return common.apiDo({
		apiKey: 'ec-sspd-getBillFull',
		...params
	});
};


/**
 * 销售单列表查询
 * @description ec-sspd-sales-bill-list
 * @param {Object} params
 * @param {Object} params.jsonParam
 * @return {object} srcTypeIn 单据类型  1为销售单 8为收款单 12为商城订单
 * {"flag":1,"proDateGte":"2018-10-23","proDateLte":"2018-10-23","shopId":762562,"compId":763178,"srcType":8,"orderBy":"proDate desc,id desc"}
 */
reqHandler.getSalesBillList = async function (params = {}) {
	params.wrapper = true;
	params.jsonParam = Object.assign({}, {
		//flag: 1,
		proDateGte: params.proDate || common.getCurrentDate(),
		proDateLte: params.proDate || common.getCurrentDate(),
		//compId: params.compId || BASICDATA.ids.dwidXw,
		//srcType: params.srcType || 8,
		srcTypeIn: params.srcType || "1,12,13",
		orderBy: "proDate desc,billNo desc",
		shopId: LOGINDATA.shopId,
		pageNo: 1,
		pageSize: 15
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-sspd-sales-bill-list', ...params });
};

/**
 * 收款明细
 * @description ec-sspd-sales-bill-list
 * @param {Object} params
 * @param {Object} params.jsonParam
 * @return {object} srcType 单据类型  1为销售单 8为收款单 12为商城订单
 */
reqHandler.getSalesBillListDTL = async function (params = {}) {
	params.wrapper = true;
	params.jsonParam = Object.assign({}, {
		//flag: 1,
		proDateGte: params.proDate || common.getCurrentDate(),
		proDateLte: params.proDate || common.getCurrentDate(),
		compId: params.compId || BASICDATA.ids.dwidXw,
		srcType: params.srcType || 8,
		//srcTypeIn: params.srcType || "1,12,13",
		orderBy: "proDate desc,id desc",
		shopId: LOGINDATA.shopId,
		pageNo: 1,
		pageSize: 20
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-sspd-sales-bill-list', ...params });
};

/**
 * ec-trade-purBill-deleteBill 作废销售单
 * @param {Object} params
 * @param {Object} params.id
 * @param {Object} params.isPend
 */
reqHandler.deleteSalesBill = async function (params) {
	return common.apiDo({
		apiKey: 'ec-sspd-salesBill-deleteBill',
		...params
	});
};

/**
 * 笑铺销售挂单列表接口不走原来二代的挂单接口
 * ec-trade-pendBill-findPendBill 查询销售单挂单列表
 * @param {Object} params
 * @param {Object} params.jsonParam
 */
reqHandler.getSalesPendingList = async function (params = {}) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, {
		orderBy: 'createdDate',
		flag: 1,
		orderByDesc: true,
		shopId: LOGINDATA.shopId,
	}, params.jsonParam);
	return common.apiDo({
		apiKey: 'ec-trade-pendBill-findPendSimpleBill', wrapper: true, ...params
	});
};

/**
 * ec-trade-pendBill-findPendBill 查询挂单列表
 * @description typeId 销售单：1100 销售订单：1200 采购单：1300 采购订单：1400 销售发货单：1500 销售订单配货单：1600 采购收货单：1700 期初入库单：3100 其他入库单：3200 其他出库单：3300 调整出入库单：3400 盘点单：3500 盘点计划：3600 调拨入库单：3700 要货单：3900
 * @param {Object} params
 * @param {string} params.jsonParam
 * @param {string} params.jsonParam.typeId 单据类型
 */
reqHandler.getPendBillList = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-trade-pendBill-findPendBill',
		orderBy: 'proDate',
		flag: 1,
		orderByDesc: true,
		typeId: 1300,
		...params
	});
};

/**
 * 销售单明细
 * @param {Object} params
 * @param {Object} params searchToken
 */
reqHandler.getSalesDetList = async function (params = {}) {
	params = format.packJsonParam(Object.assign({
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
		shopId: LOGINDATA.shopId,
		orderBy: 'proDate desc,billNo desc',
		wrapper: true,
		pageSize: 0,
	}, params));
	return common.apiDo({ apiKey: 'ec-sspd-salesBill-findDetailDTO', ...params });
};

/**
 * 开单获取上次价
 * @param {Object} params 
 * @param {string} params.tenantSpuIds 多个租户级spu id，以”,”拼接
 */
reqHandler.getSalesLastPrice = async function (params) {
	return common.apiDo({ apiKey: 'ec-sspd-salesBillDetail-getLastPrice', ...params });
};

//进货

/**
 * ec-trade-purBill-saveBill 保存进货单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
reqHandler.savePurchaseBill = async function (params) {
	params.jsonParam = reqHandler.jsonParamFormat(params.jsonParam);
	const res = await common.apiDo({
		apiKey: 'ec-trade-purBill-saveBill',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * ec-trade-purBill-getBillFull 查询采购单完整信息
 * @param {object} params
 * @param {number} params.isPend 是否挂单单据(1 是/0 否。指定要作废的单据是否处于挂单状态)*
 * @param {string} params.id 采购单主表ID或挂单ID（isPend=0时，设置为采购单主表ID。isPend=1时，设置为挂单ID）。不传时，返回新增单据时的默认值。
 */
reqHandler.getPurchaseBillFull = async function (params) {
	if (!params.hasOwnProperty('isPend')) params.isPend = 0;
	params.wrapper = true;
	return common.apiDo({
		apiKey: 'ec-trade-purBill-getBillFull',
		...params
	});
};


/**
 * 进货单列表查询
 *
 * @param  {object} params = {} 
 * @return {object}             
 */
reqHandler.getPurchaseBillList = async function (params = {}) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, qlDefParams, {
		orderBy: 'proDate,billNo'
	}, params.jsonParam);
	return common.apiDo({
		apiKey: 'ec-trade-purBill-findBillWithPayWay',
		...params
	});
};

/**
 * anonymous function - 查询挂单列表
 * @param {Object} params
 * @param {Object} params
 */
reqHandler.getPurchasePendingList = async function (params = {}) {
	params = format.packJsonParam(params);
	params.jsonParam = Object.assign({}, qlDefParams, {
		orderBy: 'proDate',
		typeId: 1300
	}, params.jsonParam);
	return tradeReqHandler.getPendBillList(params);
};


/**
 * ec-trade-purBill-deleteBill 作废采购单
 * @param {Object} params
 */
reqHandler.deletePurchaseBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-purBill-deleteBill', ...params });
};

//盘点

/**
 * 保存盘点计划
 * @param params
 * @return {object}
 */
reqHandler.saveCheckPlan = async function (params) {
	return common.apiDo({ apiKey: 'ec-inv-check-plan-create', ...params });
};

/**
 * 撤销盘点计划
 * @param params
 * @return {object}
 */
reqHandler.deleteCheckPlan = async function (params) {
	return common.apiDo({ apiKey: 'ec-inv-check-plan-delete', ...params });
};

/**
 * 处理盘点计划
 * @param params
 * @return {object}
 */
reqHandler.execCheckPlan = async function (params) {
	params.bizDate = common.getCurrentDate();
	return common.apiDo({ apiKey: 'ec-inv-check-bill-execCheckPlan', ...params });
};

/**
 * 盘点计划详情
 * @param params
 * @return {object}
 */
reqHandler.getCheckPlanDetail = async function (params) {
	params.pageNo = 1;
	params.pageSize = 15;
	return common.apiDo({ apiKey: 'ec-inv-check-plan-detail', ...params });
};

/**
 * 保存盘点单
 * @param params
 * @return {object}
 */
reqHandler.saveCheckBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-inv-check-bill-save', ...params });
};

/**
 * 作废盘点单
* @param params
* @return {object}
*/
reqHandler.deleteCheckBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-inv-check-bill-delete', ...params });
};

/**
 * 盘点计划列表
 *
 * @param  {object} params "searchToken": "关键字 名称、款号、条码、备注",
 * @return {object}              
 */
reqHandler.getCheckList = async function (params = {}) {
	params = format.packJsonParam(Object.assign({
		shopId: LOGINDATA.shopId,
		wrapper: true,
		//flag: 2,//1未处理 2已处理
		//hasDiff: 1,//1有盈亏/2无盈亏
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
		searchKey: ''//输单号、货品名称、款号、备注
	}, params));
	return common.apiDo({
		pageNo: 1,
		pageSize: 15,
		apiKey: 'ec-inv-check-plan-listForSspd',
		...params
	});
};


/**
 * 盘点单详情
 *
 * @param  {object} params
 * @return {object}        
 */
reqHandler.getCheckFull = async function (params) {
	return common.apiDo({
		apiKey: 'ec-inv-check-bill-getBeanById',
		...params
	});
};

// 调拨

/**
 * 保存调出单
 */
reqHandler.saveMoveOutBill = async function (params) {
	//params = format.packJsonParam(params);
	params.jsonParam.hashKey = `ShopDiary-${LOGINDATA.deviceNo}-${Date.now()}-${common.getRandomNum(100, 999)}`;
	const res = await common.apiDo({
		apiKey: 'ec-inv-move-out-bill-save',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 调拨单详情
 *
 * @param  {object} params
 * @param  {object} params.wrapper
 * @param  {object} params.jsonParam
 * @return {object}        
 */
reqHandler.getMoveBillFull = async function (params) {
	params = format.packJsonParam(params);
	params.wrapper = true;
	return common.apiDo({
		apiKey: 'ec-inv-move-out-bill-getBeanById',
		...params
	});
};

/**
 * 接收调入单
 */
reqHandler.saveMoveInBill = async function () {
	return common.apiDo({ apiKey: 'ec-inv-move-in-bill-receive', ...params });
};

/**
 * 调拨单列表
 * @param  {object} params
 * @param  {string} params.inOutType 调入调出类型* 1调拨出库 2调拨入库
 */
reqHandler.getMoveBillList = async function (params = {}) {
	return common.apiDo({ apiKey: 'ec-inv-move-bill-list', ...params });
};


/**
 * 充值
 *
 * @param  {object} params jsonparam
 * @param  {object} params.jsonparam
 * @return {object}
 */
reqHandler.saveRechargeBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-sspd-finInVipRechargeBill-saveBill',
		...params
	})
};

/**
 * 余额充值列表
 *
 * @param  {Object} params 
 * @param  {Object} params.compId
 * @return {type}        
 */
reqHandler.getRechargeBillList = async function (params) {
	params = Object.assign({
		// flag: 1,//是否显示作废明细 1 代表不显示
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
		shopId: LOGINDATA.shopId,
		orderBy: "proDate desc,id desc"
	}, params);
	return common.apiDo({
		apiKey: 'ec-sspd-finInVipRechargeBill-listWithPayWays',
		wrapper: true,
		...params
	});
};

/**
 * 余额充值详细
 * @param  {Object} params
 * @param  {Object} params.id 
 */
reqHandler.getRechargeBillInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-sspd-finInVipRechargeBill-getBillFull',
		wrapper: true,
		...params
	});
};

/**
 * 作废充值单
 * @param  {Object} params
 * @param  {Object} params.id 
 */
reqHandler.deleteRechargeBill = async function (params) {
	return common.apiDo({
		apiKey: 'ec-sspd-finInVipRechargeBill-deleteBill',
		...params
	});
};

/**
 * reqHandler - 付款
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object}
 */
reqHandler.savePurOutBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-save', ...params });
};


/**
 * 付款单明细
 * @param  {object} params
 * @param  {object} params.id 收款单id
 */
reqHandler.getPurOutBillInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-getById', ...params });
};

/**
 *  付款列表
 * @param  {object} params
 * @param  {object} params.compId
 * @return {object}
 */
reqHandler.getPurOutBillList = async function (params) {
	params = format.packJsonParam(Object.assign({
		// flag: 1,
		wrapper: true,
		shopId: LOGINDATA.shopId,
		orderBy: "proDate desc,id desc",
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
	}, params));
	return common.apiDo({
		apiKey: 'ec-trade-finPurOutBill-list',
		...params
	});
};

/**
 * 作废付款单
 * @param  {object} params
 * @param  {object} params.id
 */
reqHandler.deletePurOutBill = async function (params) {
	return common.apiDo({ apiKey: 'ec-trade-finPurOutBill-delete', ...params });
};

//在线支付

/**
 * 检查当前企业在线支付开通状态
 */
reqHandler.checkOnlinePayChannel = async function () {
	return common.apiDo({ apiKey: 'ec-config-ugr-tenant-checkOnlinePayChannel' });
};

/**
 * 发起支付 销售单/收款单
 * @description 只是为了让服务端判断扫码支付的第三方为支付宝还是微信
 * @description 支付金额限额500
 * @param  {object} params
 * @param {string} params.billId 单据ID
 */
reqHandler.createSalesBillPay = async function (params) {
	params.payCode = 26 + common.getRandomNumStr(16);//支付码内容
	params.isStrict = 1;//是否严格模式，笑铺固定传1
	params.check = false;
	return common.apiDo({ apiKey: 'ec-sspd-salesbill-launchMisOnlinePay', ...params });
};
/**
 * 销售单支付回调
 * @param  {object} params
 * @param {string} params.mainId billId
 */
reqHandler.misSalesBillPayCallback = async function (params) {
	//  code=xxxx & msg=xxxx & unitId=xxx & shopId=xxx
	params.payId = -common.getRandomNumStr(20);
	params.code = 0;
	params.msg = '支付成功';
	params.unitId = LOGINDATA.unitId;
	params.shopId = LOGINDATA.shopId;
	return common.apiDo({ apiKey: 'ec-sspd-salesbill-misOnlinePayCallback', ...params });
};

reqHandler.saveSalesBillPayOnline = async function (params) {
	const sfRes = await reqHandler.saveSalesBill(params);
	await reqHandler.createSalesBillPay({ billId: sfRes.result.data.val });
	await reqHandler.misSalesBillPayCallback({ mainId: sfRes.result.data.val });
	return sfRes;
};


/**
 *自定义统计
 * ec-sspd-area-diary
 */
reqHandler.areaDiary = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-sspd-area-diary',
		startDate: common.getCurrentDate(),
		endDate: common.getCurrentDate(),
		shopId: LOGINDATA.shopId,
		...params
	});
};

/**
 *智能引擎
 * ec-sspd-trend-diary
 */
reqHandler.trendDiary = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-sspd-trend-diary',
		timeKind: params.timeKind || 1, //1-按日 2-按周 3-按月 4-按年
		now: common.getCurrentDate(),
		shopId: LOGINDATA.shopId,
	})
};


/**
 * 同步商城订单
 * @param {object} params
 */
reqHandler.saveMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderPay',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单退款退货
 * @param {object} params
 */
reqHandler.RefundMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderRefund',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单退货
 * @param {object} params
 */
reqHandler.ReturnMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderReturn',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * 商城订单提现
 * @param {object} params
 */
reqHandler.CashOutMallBill = async function (params) {
	const res = await common.callBackDo({
		event: 'orderVerify',
		...params
	});
	await common.delay(500);
	return res;
};

//收入单


/**
 *  查询收支单完整信息
 * @param {object} params
 * @param {string} params.id 收支单id
 */
reqHandler.getFinInExpBillFull = async function (params) {
	params.wrapper = true;
	return common.apiDo({
		apiKey: 'ec-sspd-finInExpBIll-getBillFull',
		...params
	});
};

/**
 * 收支列表
 * @description ec-sspd-finInExpBill-findBillWithOneDet
 * @param {Object} params
 * @param {Object} params.jsonParam
 */
reqHandler.getFinInExpBillList = async function (params = {}) {
	params.wrapper = true;
	params.pageNo = 1;
	params.pageSize = 20;
	params.jsonParam = Object.assign({}, {
		flag: params.flag || 1,//1不显示作废
		proDateGte: params.proDate || common.getCurrentDate(),
		proDateLte: params.proDate || common.getCurrentDate(),
	}, params.jsonParam);
	return common.apiDo({ apiKey: 'ec-sspd-finInExpBill-findBillWithOneDet', ...params });
};

/**
 * ec-sspd-finInBill-deleteBill 作废收入单
 * @param {Object} params
 * @param {Object} params.id
 */
reqHandler.deleteFinInBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-sspd-finInBill-deleteBill',
		...params
	});
};

//支出单

/**
 * ec-sspd-finExpBill-saveBill 保存支出单
 * @param {object} params
 * @param {object} params.jsonParam
 * @return {object} {params,result}
 */
reqHandler.saveFinExpBill = async function (params) {
	params = format.packJsonParam(params);
	const res = await common.apiDo({
		apiKey: 'ec-sspd-finExpBill-saveBill',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * ec-sspd-finExpBill-deleteBill 作废支出单
 * @param {Object} params
 * @param {Object} params.id
 */
reqHandler.deleteFinExpBill = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-sspd-finExpBill-deleteBill',
		...params
	});
};

/**
 * 笑铺中的商城订单列表
 * @param  {object} params.jsonparam
 * @return {object}
 */
reqHandler.salesOrderBillList = async function (params = {}) {
	params.wrapper = true;
	params.pageNo = 1;
	params.pageSize = 20;
	params.jsonParam = Object.assign({}, {
		orderBy: params.orderBy || 'billNo desc',
		proDateGte: params.proDate || common.getCurrentDate(),
		proDateLte: params.proDate || common.getCurrentDate(),
	}, params.jsonParam);
	return common.apiDo({
		apiKey: 'ec-sspd-salesOrder-findBill',
		...params
	})
};

/**
 *  笑铺中的商城订单详情
 * @param {object} params
 */
reqHandler.getSalesOrderBillFull = async function (params) {
	params.wrapper = true;
	return common.apiDo({
		apiKey: 'ec-sspd-salesOrder-getBillFull',
		isPend: params.isPend || 0,
		id: params.id,
		...params
	});
};