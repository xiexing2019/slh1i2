const caps = require('../../data/caps');

const spbDefParams = () => {
    return {
        _cid: LOGINDATA.clusterCode || 0,
        _tid: LOGINDATA.tenantId || 0
    }
};

module.exports = {
    url: `${caps.ssurl}`,
    spgUrl: `${caps.ssurl}/spg/api.do`,
    spbUrl: `${caps.ssurl}/spb/api.do`,

    spbDefParams,
};