const common = require('../../lib/common');
const format = require('../../data/format');
const sspdCaps = require('./sspdCaps');
const finReqHandler = require('../slh2/fin');

let reqHandler = module.exports = {};

/**
 * 门店(修改)
 *
 */
reqHandler.saveShop = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-mdm-org-saveFull',
		...params
	});
};


/**
 *  获取能切换门店的门店列表
 *
 * @param  {object} params
 * @return {object} {params,result}
 */
reqHandler.getShopListByDevice = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-mdm-org-shop-list-by-device',
		deviceNo: LOGINDATA.deviceNo, //登录的手机号
		pageSize: 0,
		bdomainCode: 'slh',
		jsonParam: {
			wrapper: true,
			flag: 1
		},
		...params
	});
};


/**
 * 切换门店
 * @param  {object} params
 * @param  {string} params.unitId 帐套
 * @param  {string} params.shopId 门店
 * @return {object} {params,result}
 */
reqHandler.changeUnit = async function (params) {
	const res = await common.apiDo({
		apiKey: 'ec-mdm-sspd-change-unit',
		mobile: LOGINDATA.deviceNo,
		unitId: LOGINDATA.unitId,
		...params
	});
	//切换成功后 更新登录信息
	LOGINDATA = res.result.data;
	return res;
};


/**
 * 员工新增
 */
reqHandler.saveStaff = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-save',
		jsonParam: {
			staff: params,
		},
	});
};

/**
 * 员工详情
 */
reqHandler.getStaffInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-getDtoById',
		id: params.id,
		wrapper: true,
	})
};

/**
 * 员工列表（低于自己角色等级的员工列表 ）
 * @description 接口说明同ec-mdm-staff-list
 * @param {string} flag  是否停用，1：启用，0：停用
 */
reqHandler.getStaffList = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-listCtlLevel',
		pageSize: 0,
		jsonParam: {
			wrapper: true,
			...params
		},
	});
};



/**
 * anonymous function - 停用员工
 *
 * @param  {object} params
 * @return {object}
 */
reqHandler.disableStaff = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-disable',
		id: params.id,
	});
};


/**
 * anonymous function - 启用员工
 *
 * @param  {object} params
 * @return {object}
 */
reqHandler.enableStaff = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-enable',
		id: params.id,
	});
};

/**
 * 对员工授权
 * @param {object} params jsonParam
 * @param {object} params.staffId 授权的员工ID
 * @param {object} params.authPhone 授权手机号
 * @param {object} params.verifyCode 验证码
 */
reqHandler.authorizeStaff = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-mdm-staff-authorizeStaff',
		...params
	})
};

/**
 * 撤销员工授权
 * @param {object} params jsonParam
 * @param {object} params.staffId 需要被撤销授权的员工ID
 */
reqHandler.unauthorizeStaff = async function (params) {
	params = Object.assign({}, sspdCaps.defParams, format.packJsonParam(params));
	return common.apiDo({
		apiKey: 'ec-mdm-staff-unauthorizeStaff',
		shopId: LOGINDATA.shopId,
		...params
	});
};

// 账号  

/**
 * 账户新增
 */
reqHandler.saveAccount = async function (params) {
	// params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-fin-simple-acct-save', ...params });
};

/**
 *  账户流水
 *
 * @param  {object} params 
 * @param  {string} params.id
 * @return {object}         
 */
reqHandler.getAccountFlowList = async function (params) {
	params = Object.assign({
		shopId: LOGINDATA.shopId,
		// acctId: params.id,
		proDateGte: common.getCurrentDate(),
		proDateLte: common.getCurrentDate(),
		traderCap: 1024
	}, params);
	return common.apiDo({
		apiKey: 'ec-fin-acct-flow-list-report',
		jsonParam: params,
		pageSize: 0,
		wrapper: true,
		orderBy: 'proDate desc,id desc'
	});
};

/**
 * 账户详情
 */
reqHandler.getAccountInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-fin-acct-getById',
		id: params.id,
		wrapper: true,
	})
};

/**
 * 账户列表
 * @param {object} jsonParam
 * @param {boolean} flag 0:,1:
 * 
 */
reqHandler.getAccountList = async function (params = {}) {
	params = Object.assign({
		pageSize: 0,
		orderBy: 'showOrder',
		wrapper: true,
		traderCap: 1024,//交易方能力位，固定传1024
	}, sspdCaps.defParams, params);
	return finReqHandler.getAccountListByTenantReal(params);
};

/**
 * 账户停用
 */
reqHandler.disableAccount = async function (params) {
	return common.apiDo({
		apiKey: 'ec-fin-acct-disable',
		id: params.id,
	});
};

/**
 * 账户启用
 */
reqHandler.enableAccount = async function (params) {
	return common.apiDo({
		apiKey: 'ec-fin-acct-enable',
		id: params.id,
	});
};

/**
 * 设置/取消默认账户
 * @param  {object} params
 * @param {string} params.id 账户id
 * @param {string} params.defaultFlag 1:设置为默认；0:取消默认
 */
reqHandler.setDefaultAccount = async function (params) {
	// params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-fin-acct-update-default', ...params });
};


/**
 * 获取所有权限列表
 *
 * @param  {object} params = {}
 * @return {object} {params,result}
 */
reqHandler.getAllFunc = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-func-codes',
		productBits: 128, //产品类型位值集合，此处为128，代表笑铺日记；114-为商陆花(是经典、面料、零售和童装版的位合集)
		productType: 8, //单个产品类型，与productBits二取一即可，其中8代表笑铺日记
		termCap: 0, //终端访问类型，对于笑铺来说此处固定为0
		includeSubs: true, //递归查询出所有功能；false：查询一层功能
		// codeId: null, //查询指定功能的子功能，默认为空代表从顶层查起
		...params
	});
};

/**
 *  新增职位
 *
 * @param  {object} params
 * @return {object}
 */
reqHandler.saveWithPrivs = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-om-role-saveWithPrivs',
		...params,
	});
};


/**
 * 职位详情
 *
 * @param  {object} params
 * @param  {object} params.id
 * @return {object}
 */
reqHandler.getPrivsInfo = async function (params) {
	await common.apiDo({
		apiKey: 'ec-om-role-getWithPrivs', //ec-om-role-getById
		id: params.id,
		wrapper: true,
	});
};



/**
 * 获取职位列表
 *
 * @param  {object} params = {}
 * @return {object}
 */
reqHandler.getPrivsList = async function (params = {}) {
	return common.apiDo({
		apiKey: 'ec-om-role-list',
		pageSize: 0,
		wrapper: true,
		flag: 1,
		showFlag: 1,
		...params
	});
};

/**
 * reqHandler - 停用职位
 *
 * @param  {object} params description
 * @return {object}        description
 */
reqHandler.privsDisenable = async function (params) {
	return common.apiDo({
		apiKey: 'ec-om-role-disable',
		id: params.id,
	})
};

/**
 * reqHandler - 启用职位
 *
 * @param  {object} params description
 * @return {object}        description
 */
reqHandler.privsEnable = async function (params) {
	return common.apiDo({
		apiKey: 'ec-om-role-enable',
		id: params.id,
	})
};

/**
 * reqHandler - 门店列表
 * @param  {object} params
 */
reqHandler.getShopList = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-mdm-org-shop-list', ...params });
};


/**
 * 批量同步货品到商城
 */
reqHandler.mallSpuSync = async function (params) {
	console.log(common.getCurrentTime());
	return common.apiDo({
		apiKey: 'ec-mallSpu-sync',
		ids: params.ids,
	});
};
