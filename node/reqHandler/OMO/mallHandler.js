const slhmallCaps = require('./slhmallCaps');
const common = require('../../lib/common');
const format = require('../../data/format');
const mallAccount = require('./mallAccount');

const mallHandler = module.exports = {
    url: slhmallCaps.spbUrl,
    url2: slhmallCaps.spgUrl,
};

/**
 * 微信登录小程序
 * @param {Object} params
 */
mallHandler.userLoginWithWx = async function (params = {}) {
    delete LOGINDATA.sessionId;
    params = {
        productCode: 'slhMallWxApplet',
        appId: mallAccount.seller1.appId,
        openId: mallAccount.client.openId,
        mobile: mallAccount.client.mobile || '',
        ...params
    };
    //console.log(LOGINDATA);
    const res = await common.post(this.url2, { apiKey: 'ec-ugr-wxApp-loginWithRegister', ...params });
    // console.log(res);
    LOGINDATA = res.result.data;
    // console.log(LOGINDATA);
    await common.delay(500);
};

/**
 * 获取全部商品列表
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
mallHandler.getDresSpuList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), slhmallCaps.spbDefParams());
    // return common.get(this.url, { apiKey: 'ec-spchb-dresSearch-listDresSpu', ...params });
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-listDresSpu', ...params });
};

/**
 * 买家查询商品详情
 * @description 
 * 1. 根据全局商品搜索所得的detailUrl拼接参数 使用卖家的_cid,_tid
 * 2. 2.0.8版本 添加最优活动字段 
 * @param {object} params jsonParam
 * @param {string} params.spuId 租户商品ID
 * @param {string} params.buyerId 买家的租户ID
 */
mallHandler.getFullForBuyer = async function (params) {
    params.productVersion = '2.9.0';
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-getFullForBuyer', ...params });
};

/**
 * 获取客户默认收货地址
 * **/
mallHandler.getUserDefaultRecInfo = async function (params = {}) {
    params = Object.assign(params, slhmallCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spmdm-spUserManage-getUserDefaultRecInfo', ...params });
};

/**
 * 查询租户商品信息
 * @param {object} params 
 * @param {string} params.id 租户商品ID
 */
mallHandler.getFullById = async function (params) {
    params = Object.assign(format.packJsonParam(params), slhmallCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-getFullById', ...params });
};

/**
 * 创建采购订单
 * @param {object} params
 */
mallHandler.savePurBill = async function (params) {
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    params.productVersion = '2.9.0';
    const res = await common.post(this.url, { apiKey: 'ec-sppur-purBill-createFull', ...params });
    if (!params.hasOwnProperty('check')) {
        res.result.data.rows.forEach(row => expect(row, `opTime:${res.opTime}\n\t下单失败:\n\t${JSON.stringify(res.result)}`).to.includes({ isSuccess: 1 }));
    }
    await common.delay(500);
    return res;
};

/**
 * 订单支付
 * @param {object} params jsonParam
 * @param {Array} params.orderIds orderIds
 * @param {string} params.payMoney 此笔支付金额
 */
mallHandler.createPay = async function (params = {}) {
    params = Object.assign({ hashKey: `${Date.now()}${common.getRandomNumStr(5)}` }, params);
    // params.hashKey = `${Date.now()}${common.getRandomNumStr(5)}`;
    params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-sppur-payBill-createPay', ...params });
};

/**
 * 支付回调
 * @description 测试内部调用
 * @param {object} params jsonParam
 * @param {string} params.mainId payDetailId
 * @param {string} params.amount 此笔支付金额(元) 传参时会转化为分
 */
mallHandler.receivePayResult = async function (params) {
    params.amount = common.mul(params.amount, 100);//单位 元转化为分
    params = format.packJsonParam(Object.assign({
        msg: '交易支付成功',
        code: 0,
        payId: Date.now(),
        payer: '',
        payDate: common.getCurrentTime(),
        // 线上入账需要排除自动化数据 必须传负
        // 清分相关需要为正
        payNumber: -common.getRandomNumStr(10),//spCaps.envName.includes('online') ? -common.getRandomNumStr(10) : common.getRandomNumStr(10),
        extra: '',//暂时没用到
        bizType: '11300',//暂时没用到
        payType: '2097152',// 中信微信小程序支付:1048576 支付宝APP支付:4096, 微信APP支付:2097152
        feeRate: 0.6,//平台收取千分之6手续费
    }, params));

    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    params._unitId = LOGINDATA.unitId;
    params._saasId = LOGINDATA.saasId;
    return common.get(this.url, { apiKey: 'ec-sppur-payBill-receivePayResult', ...params });
};

/**
 * 查询单据详情(卖家)
 * @param {object} params
 * @param {string} params.id
 */
mallHandler.salesFindBillFull = async function (params) {
    params._cid = LOGINDATA.clusterCode;
    params._tid = mallAccount.seller1.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBillFull', ...params });
};

/**
 * 查询单据详情(买家)
 * @param {object} params
 * @param {string} params.id
 */
mallHandler.findPurBillFull = async function (params = {}) {
    Object.assign(params, slhmallCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findBillFull', ...params });
};

/**
 * 创建采购订单
 * @description 0.5S轮询一次 等待单据状态更新至3
 */
mallHandler.createPurBill = async function (params) {
    const purRes = await this.savePurBill(params);
    console.log(`创建采购单:${JSON.stringify(purRes.result.data.rows[0])}`);
    let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
    while (checkPurBillFlag.purBillFlag != 3) {
        if (i > 20) throw new Error(`判断订单状态已超过10次`);
        await common.delay(500);
        checkPurBillFlag = await this.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
        console.log(`\n 第${i}次：checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
        i++;
    }
    Object.assign(purRes.result.data.rows[0], checkPurBillFlag);
    return purRes;
}

/**
 * 轮询采购单状态
 * @description 由http同步创建销售单改为mq异步创建, 创建采购单完成时返回, purBillFlag=2(处理中), 销售单创建成功后flag=3, 由客户端轮询
 * @param {object} params
 * @param {string} params.purBillId
 */
mallHandler.checkPurBillFlag = async function (params = {}) {
    Object.assign(params, slhmallCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purBill-checkPurBillFlag', ...params });
};

/**
 * 登录微商城卖家端
 */
mallHandler.smallLogin = async function (params) {
    params.code = mallAccount.seller1.mobile;
    params = format.packJsonParam(params);
    // params.dlProductCode = 'slhMallAssistIOS';
    const res = await common.post(this.url2, { apiKey: 'ec-spugr-spLogin-userLogin', ...params });
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId

        //防止请求间隔过短,单元区会话失效的错误
        //全局区登录，通过mq通知单元区；单元区还没处理好，就请求到了单元区
        //暂时加延迟处理
        await common.delay(500);
    };
    return res;
};

/**
 * 卖家发货
 * @param {object} params jsonParam
 */
mallHandler.deliverSalesBill = async function (params) {
    params = Object.assign(format.packJsonParam(params));
    params._cid = LOGINDATA.clusterCode;
    params._tid = mallAccount.seller1.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-deliverBill-saveFull', ...params });
};

/**
 * 查询销售单
 * @param {object} params
 */
mallHandler.salesFindBills = async function (params = {}) {
    params._cid = LOGINDATA.clusterCode;
    params._tid = mallAccount.seller1.tenantId;
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-findBills', ...params });
};

/**
 * 切换用户门店
 * @param {Object} params jsonParam 
 * @param {string} params.newUnitId 要切换门店的unitId
 * @param {string} params.mobile
 */
mallHandler.changeUserShop = async function (params = {}) {
    params.mobile = mallAccount.seller1.mobile;
    params.newUnitId = mallAccount.seller1.tenantId;
    params = format.packJsonParam(params);
    const res = await common.post(this.url2, { apiKey: 'ec-spugr-spLogin-changeUserShop', ...params });
    // console.log(`res=${JSON.stringify(res)}`);
    //切换成功后 覆盖更新登录信息
    if (res.result.code == 0) {
        Object.assign(LOGINDATA, res.result.data);
        await common.delay(500);
    };
    return res;
};