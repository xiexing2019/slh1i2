'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const getBasicData = require('../../data/getBasicData');
const dresReqHandler = require('../slh2/dres');
const format = require('../../data/format');
const depReqHandler = require('./depReqHandler');
const sspdBasicJson = require('../../shopDiary/help/json/basicJson');
const sspdCaps = require('./sspdCaps');

//接口文档地址 http://106.15.226.60:8080/showdoc/index.php?s=/25&page_id=1655
let sspdMainReq = module.exports = {};
const { defParams, mobiles, defPlatparams } = sspdCaps;


/**
 * syncUnitId - 更新udid
 * @description 根据手机号获取可访问的单元id；对于一米之外来说通常就一个单元
 * @param {Object} deviceNo 对于一米之外而言为手机号
 * @param {Object} unitName 对于一米之外而言为账套名称
 */
sspdMainReq.syncUnitId = async ({
	deviceNo,
	name = mobiles[caps.name]['unitName']
}) => {
	const res = await common.apiDo({
		apiKey: 'ec-config-ugr-unit-findDeviceUnit',
		jsonParam: {
			deviceNo: deviceNo,
			bdomainCode: 'slh'
		},
		...defParams
	});
	try {
		caps.unitId = res.result.data.rows.find(obj => obj.name == name || obj.name == '默认账套').id;
	} catch (error) {
		throw new Error(`更新udid失败:\n${JSON.stringify(res)}\n${error}`);
	};

};

/**
 * sspdLogin - 店员登录
 * @description 一米之外员工根据手机号和验证码登录
 * @param {Object} params
 * @param {string} params.mobile 手机号
 * @param {string} [params.verifyCode='0000'] 验证码
 * @param {boolean} [params.mustVerify=false] 默认不会真正发送短信验证码
 */
sspdMainReq.sspdLogin = async (params) => {
	params = format.packJsonParam(Object.assign({
		unitId: caps.unitId,
		verifyCode: '0000',
		mustVerify: false
	}, defParams, params));
	const res = await common.apiDo({
		apiKey: 'ec-mdm-sspd-login',
		...params
	});
	try {
		LOGINDATA = res.result.data; //更新登录信息
	} catch (error) {
		throw new Error(`更新LOGINDATA失败:\n${JSON.stringify(res)}`);
	};

};

/**
 * mdmUserLogin - 一米之外后台登录
 * @description  一米之外后台登录
 * @param {Object} params
 */
sspdMainReq.mdmUserLogin = async (params) => {
	params = format.packJsonParam(Object.assign({
		//unitId: caps.unitId,
		//code:'slhadmin',
		password: '000000',
		token: ''
	}, defPlatparams, params));
	const res = await common.apiDo({
		apiKey: 'ec-mdm-user-login',
		...params
	});
	await common.delay(500);
	return res;
};

/**
 * sendCode 发送验证码
 * @param {Object} params
 * @param {string} params.mobile 手机号
 */
sspdMainReq.sendCode = async (params) => {
	const res = await common.apiDo({
		apiKey: 'ec-mdm-staff-sendCode',
		jsonParam: {
			unitId: caps.unitId,
			phone: params.mobile,
		},
		...defParams
	});
	// console.log(res);
	return res;
};

/**
 * 一米之外登录+获取常用数据
 * @description 相同手机号时不重新登录
 * @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
 */
sspdMainReq.sspdLoginBySeq = async function ({ seq = '000', name } = {}) {
	if (!mobiles[caps.name]) throw new Error(`本环境没有自动化的一米之外帐套`);
	const mobile = mobiles[caps.name][seq];
	// 更新udid
	await sspdMainReq.syncUnitId({ deviceNo: mobile, name });
	// 相同手机+帐套时 不重新登录
	if (mobile == LOGINDATA.deviceNo && LOGINDATA.unitId == caps.unitId) {
		return;
	};

	await sspdMainReq.sspdLogin({ mobile });
	// 获取基本信息
	// 先获取未隔离的信息(如字典),再切换到每个门店时获取相应的信息(如客户)
	await sspdMainReq.getCommonBasicData();
	await sspdMainReq.getBasicData();
};

/**
 * 一米之外员工登录+获取常用数据
 * @description 相同手机号时不重新登录
 * @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
 */
sspdMainReq.sspdLoginByStaff = async function ({ seq = '004', name } = {}) {
	if (!mobiles[caps.name]) throw new Error(`本环境没有自动化的一米之外帐套`);
	const mobile = mobiles[caps.name][seq];
	// 更新udid
	await sspdMainReq.syncUnitId({ deviceNo: mobile, name });
	// 相同手机+帐套时 不重新登录
	if (mobile == LOGINDATA.deviceNo && LOGINDATA.unitId == caps.unitId) {
		return;
	};

	await sspdMainReq.sspdLogin({ mobile });
	// 获取基本信息
	// 先获取未隔离的信息(如字典),再切换到每个门店时获取相应的信息(如客户)
	await sspdMainReq.getCommonBasicData();
	await sspdMainReq.getBasicData();
};

/**
 * 切换门店
 * @param {Object} params
 * @param {string} params.shopName 门店名称
 */
sspdMainReq.changeShop = async ({ shopName = '常青店' } = {}) => {
	// 获取可切换门店列表
	if (shopName == LOGINDATA.shopName) return;
	const shopInfo = await depReqHandler.getShopListByDevice().then(res => res.result.data.rows.find(data => data.name == shopName));
	//console.log(shopInfo);
	if (!shopInfo) throw new Error(`没有查到要切换的门店:${shopName}`);
	// 切换门店
	await depReqHandler.changeUnit({ shopId: shopInfo.id, unitId: LOGINDATA.unitId });
	// 获取门店基本信息
	await sspdMainReq.getBasicData();
};

/**
 * 切换门店(不获取信息)
 * @param {Object} params
 * @param {string} params.shopName 门店名称
 */
sspdMainReq.changeShopFast = async ({ shopName = '常青店' } = {}) => {
	// 获取可切换门店列表
	if (shopName == LOGINDATA.shopName) return;
	const shopInfo = await depReqHandler.getShopListByDevice().then(res => res.result.data.rows.find(data => data.name == shopName));
	if (!shopInfo) throw new Error(`没有查到要切换的门店:${shopName}`);
	// 切换门店
	await depReqHandler.changeUnit({ shopId: shopInfo.id, unitId: LOGINDATA.unitId });
};



/**
 * 登录加切换门店
 */
sspdMainReq.sspdLoginAndChangShop = async ({ shopName = '常青店' } = {}) => {
	await sspdMainReq.sspdLoginBySeq();
	await sspdMainReq.changeShop({ shopName });
};


//判断一个session是否有效
sspdMainReq.checkSessionId = async (sessionId = LOGINDATA.sessionId) => {
	const res = await common.apiDo({
		apiKey: 'ec-mdm-session-valid',
		sessionId
	});

	try {
		assert.strictEqual(res.data.val, true);
	} catch (error) {
		throw new Error(error);
	};
};

//获取当前登录用户可访问功能
sspdMainReq.getUserFuncs = async (params) => {
	const res = await common.apiDo({
		apiKey: 'ec-config-priv-userFuncs',
		...params
	});
	return res;
};

/**
 * 已授权的员工列表
 * @description 获取当前租户下所有已授权员工列表，按门店(账套)分组
 */
sspdMainReq.getAuthorizedStaffList = async () => {
	return common.apiDo({
		apiKey: 'ec-mdm-staff-authorizedStaffListForSspd'
	});
};

/**
 * staffSave - 新增店员
 * @description 为了方便验证 一个店员只对应一个角色
 * @param {Object} params
 * @param {string} params.roleName 角色名 (老板,店长...)
 * @param {string} params.mobile 手机号
 * @param {string} params.userName 用户名
 */
sspdMainReq.staffSave = async (params) => {
	const roleList = await common.apiDo({
		apiKey: 'ec-om-role-list', //角色列表
		jsonParam: {
			wrapper: true,
			flag: 1
		}
	}).then((res) => common.takeWhile(res.result.rows, (obj) => obj.name === params.roleName));

	if (roleList.length < 1) {
		throw new Error(`新增店员失败,未找到角色:${params.roleName}`);
	};

	await common.apiDo({
		apiKey: 'ec-mdm-staff-save',
		jsonParam: {
			verifyCode: '0000',
			staff: {
				userName: params.userName,
				mobile: params.mobile,
				roleIds: roleList.shift().id,
				rem: params.rem,
				code: params.mobile, //工号001（新增，对于一米之外就是手机号*）
				depId: LOGINDATA.depId
			}
		}
	});
};

const basicDataArr = [{
	apiKey: 'ec-dres-spu-list',
	key: 'styleidAgc001',
	searchToken: 'agc001'
}, {
	apiKey: 'ec-mdm-user-cust-list',
	key: 'dwidXw',
	jsonParam: { searchToken: '小王' }
}, {
	apiKey: 'ec-mdm-org-supp-list',
	key: 'dwidVell',
	nameLike: 'Vell'
}];

/**
 * 获取帐套基本数据
 */
sspdMainReq.getCommonBasicData = async function () {
	if (BASICDATA.colorIds) return;

	BASICDATA.colorIds = await this.syncData({
		type: 'sc_dict_color'
	}).then((res) => res.result.data.rows);

	BASICDATA.sizeIds = await this.syncData({
		type: 'sc_dict_size'
	}).then((res) => res.result.data.rows);

	BASICDATA.brandIds = await this.syncData({
		type: 'sc_dict_brand'
	}).then((res) => res.result.data.rows);

	BASICDATA.classIds = await this.syncData({
		type: 'dres_style_class'
	}).then((res) => res.result.data.rows);

	BASICDATA.season = await this.syncData({
		type: 'sc_dict_season'
	}).then((res) => res.result.data.rows);

	BASICDATA.fabric = await this.getDictList({
		typeId: 637, flag: 1
	}).then((res) => res.result.data.rows);

	BASICDATA.form = await this.getDictList({
		typeId: 835, flag: 1
	}).then((res) => res.result.data.rows);

	BASICDATA.finInCatId = await common.apiDo({
		apiKey: 'ec-config-list-dict',
		jsonParam: { typeId: 1002, flag: 1 }
	}).then((res) => res.result.data.rows.find(obj => obj.codeName == "自动化收入"));

	BASICDATA.finExpCatId = await common.apiDo({
		apiKey: 'ec-config-list-dict',
		jsonParam: { typeId: 1003, flag: 1 }
	}).then((res) => res.result.data.rows.find(obj => obj.codeName == "自动化支出"));
	// 门店信息
	await depReqHandler.getShopListByDevice().then(res => res.result.data.rows.forEach(element => {
		BASICDATA[element.name] = { shopId: element.id };
	}));
};

sspdMainReq.getChainDiaryData = async function () {
	if (BASICDATA.colorIds) return;

	BASICDATA.colorIds = await this.syncData({
		type: 'sc_dict_color'
	}).then((res) => res.result.data.rows);

	BASICDATA.sizeIds = await this.syncData({
		type: 'sc_dict_size'
	}).then((res) => res.result.data.rows);

	BASICDATA.brandIds = await this.syncData({
		type: 'sc_dict_brand'
	}).then((res) => res.result.data.rows);

	BASICDATA.classIds = await this.syncData({
		type: 'dres_style_class'
	}).then((res) => res.result.data.rows);

	BASICDATA[LOGINDATA.shopName] = { shopId: LOGINDATA.shopId };
	basicDataArr[1].apiKey = 'ec-mdm-org-cust-list';
	await this.getBasicData();
};


/**
 * 获取门店基本数据
 * @description accountList:实体账户列表
 */
sspdMainReq.getBasicData = async function () {
	// 相同店铺不再重新获取
	if (BASICDATA.hasOwnProperty(LOGINDATA.shopName) && BASICDATA[LOGINDATA.shopName].ids) {
		BASICDATA.ids = BASICDATA[LOGINDATA.shopName].ids;
		return;
	};
	// 常用id

	BASICDATA[LOGINDATA.shopName].ids = await getBasicData.getDictIds(basicDataArr);

	// 实体账户列表 使用简称作为key
	BASICDATA[LOGINDATA.shopName].accountList = await depReqHandler.getAccountList()
		.then((res) => {
			let json = {};
			res.result.data.rows.forEach(element => {
				json[element.nameAbbr] = element;
				json[element.nameAbbr].typeId = getAccountTypeId(element);
			});
			return json;
		});

	// if (BASICDATA.ids) return;

	BASICDATA.ids = BASICDATA[LOGINDATA.shopName].ids;
	if (!BASICDATA.hasOwnProperty('styleInfo')) BASICDATA.styleInfo = {};
	BASICDATA.styleInfo.agc001 = await dresReqHandler.getStyleInfoById({
		id: BASICDATA.ids.styleidAgc001
	}).then(res => res.result.data);
};

/**
 * 获取银行账户typeId (简单匹配.不一定准确)
 * @param {object} params 
 * @param {string} params.acctType 12:现金,13:银行,14:第三方记账账户,15:第三方在线支付账户,16微商城账户
 * @param {string} params.chanId acctType=14时 1:支付宝 4:微信
 * @return {string} typeId
 */
function getAccountTypeId(params) {
	let typeName = { '12': 'cash', '13': 'bankCard', '14': '', '15': 'other', '16': 'bankCard' }[params.acctType];
	if (params.acctType == 14) typeName = params.chanId == 1 ? 'aliF2F' : 'wxF2F';
	// console.log(`typeName=${typeName},acctType=${params.acctType},chanId=${params.chanId}`);
	return sspdBasicJson.payType[typeName].id;
};

/**
 * ec-inv-sku-realTime-findWithInvKey 获取款号实时库存
 * @description
 * @param {string} styleId 款号id 逗号分隔
 * @return {Object} {params,result:}
 */
sspdMainReq.getInvSkuByStyleId = async function (styleId) {
	return common.apiDo({
		apiKey: 'ec-inv-sku-realTime-findWithInvKey',
		jsonParam: [styleId]
	});
};

/**
 * ec-acct-get-balanceAndScore 获取积分和余额
 * @param {Object} params
 * @param {number} params.traderKind 组织类型，1：组织机构，2：个人
 * @param {number} params.traderCap 能力位* traderKind = 1时 1: 客户, 2: 供应商 traderKind = 1时 2: 顾客(会员)
 * @param {string} params.traderId 交易人*
 * @param {string} params.shopId 门店
 */
sspdMainReq.getBalanceAndScore = async function (params) {
	params.shopId = params.shopId || LOGINDATA.shopId;
	params.traderKind = params.traderKind || 2;
	params.traderCap = params.traderKind == 2 ? 2 : params.traderCap;

	return common.apiDo({
		apiKey: 'ec-acct-get-balanceAndScore',
		...params
	});
};

sspdMainReq.syncData = async function (params) {
	return common.apiDo({
		apiKey: 'ec-syncdata',
		type: params.type,
		pageNo: 1,
		pageSize: 2000,
		// lastSyncTime: common.getCurrentTime(),//不传 获取全部 moment().subtract(5, 'm').format('YYYY-MM-DD HH:mm:ss')
	});
};

sspdMainReq.getDictList = async function (params) {
	//显示停用就不传flag
	let qlParams = Object.assign({
		pageSize: 0,
		wrapper: true,
		orderBy: 'updatedDate',
		orderByDesc: true,
	}, format.packJsonParam(params));
	// console.log(`qlParams=${JSON.stringify(qlParams)}`);
	return common.apiDo({ apiKey: 'ec-slh2-conf-dictListWithDresNum', ...qlParams });
};

/**
 * ec-inv-sku-realTime-getStockNumByOverStore 跨门店查看库存
 * @param {Object} params
 */
sspdMainReq.getStockNumByOverStore = async function (params = {}) {
	params.tenantSpuId = params.tenantSpuId || 277021;
	params.colorId = params.colorId || 2;
	params.sizeId = params.size || 1;
	params.orderByDesc = true;
	params.wrapper = true;

	return common.apiDo({
		apiKey: 'ec-inv-sku-realTime-getStockNumByOverStore',
		...params
	});
};