"use strict";
const caps = require('../../data/caps');
const common = require('../../lib/common');
const format = require('../../data/format');
const moment = require('moment');
let reqHandler = module.exports = {};




/**
 * 新增客户
 *
 * @param  {object} params
 * @param  {object} params.jsonParam
 * @return {object} {params,result}
 */
reqHandler.addCust = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-mdm-user-cust-save',
		...params
	});
};


/**
 * 客户列表
 * @param {object} params
 * @return {object} {params,result}
 */
reqHandler.getCustList = async function (params) {
	params = format.packJsonParam(Object.assign({ wrapper: true, flag: 1, pageSize: 20, pageNo: 1, orderBy: "updatedDate", }, params));
	return common.apiDo({ apiKey: 'ec-mdm-user-cust-list', ...params });
};

/**
 * 客户详情
 *
 * @param  {object} params
 * @param  {string} params.id
 * @return {object} {params,result}
 */

reqHandler.getCustInfo = async function (params) {
	return common.apiDo({ apiKey: 'ec-mdm-user-cust-getFullDtoById', ...params });
};

/**
 * 停用客户
 * @param  {object} params
 * @param  {string} params.id
 */
reqHandler.disableCust = async function (params) {
	return common.apiDo({ apiKey: 'ec-mdm-user-cust-disable', ...params });
};

/**
 * 启用客户
 * @param  {object} params
 * @param  {string} params.id
 */
reqHandler.enableCust = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-user-cust-enable',
		...params
	});
};


/**
 *新增供应商
 **/
reqHandler.addSupplier = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-mdm-org-saveFull',
		...params
	});
};

/**
 * 生日列表
 * @param  {object} params
 * @param  {object} params.jsonParam
 */
reqHandler.listByBirth = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({
		apiKey: 'ec-mdm-user-cust-listByBirth',
		...params
	});
};

/**
 *供应商详情
 **/
reqHandler.getSupplierInfo = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-org-getFull',
		wrapper: true,
		...params
	});
};

/**
 * 供应商列表
 */
reqHandler.getSupplierList = async function (params) {
	return common.apiDo({
		apiKey: 'ec-mdm-org-supp-list',
		wrapper: true,
		...params
	});
};

/**
 *供应商对账单
 **/
reqHandler.getSuppSimpleAcctCheck = async function (params) {
	let qlParams = Object.assign({
		orderBy: 'createdDate',
		orderByDesc: true,
		// pageNo: 1,
		// pageSize: 15,
		// shopId: params.shopId,
		wrapper: true,
		// compId: params.compId,
		proDateGte: common.getDateString([0, 0, -1]),
		proDateLte: common.getCurrentDate(),
		jsonParam: {}
	}, params);
	return common.apiDo({
		apiKey: 'ec-trade-accCheck-findSuppSimpleAcctCheck',
		...qlParams
	});
};

/**
 * 客户对账单
 * @param {Object} params
 * @param {Object} params.compId 客户ID
 */
reqHandler.getSimpleCustAcctCheck = async function (params) {
	let qlParams = Object.assign({
		orderBy: 'proDate desc,createdDate desc',
		orderByDesc: true,
		pageSize: 20,
		pageNo: 1,
		shopId: LOGINDATA.shopId,
		wrapper: true,
		proDateGte: '2019-01-01',
		proDateLte: common.getCurrentDate(),
		jsonParam: {}
	}, params);
	return common.apiDo({
		apiKey: 'ec-sspd-accCheck-simpleCustAccCheck',
		...qlParams
	});
};

/**
 * 积分明细
 * @param {Object} params 
 * @param {Object} params.shopId 门店ID
 * @param {Object} params.traderId 客户ID
 */
reqHandler.getCustScoreDetail = async function (params) {
	params = Object.assign({
		wrapper: true,
		traderKind: 2, //交易方类型, 固定为2
		pageSize: 0,
		shopId: LOGINDATA.shopId,
		flag: 1,
		proDateGte: common.getDateString([0, 0, -1]),
		proDateLte: common.getCurrentDate(),
		orderBy: "proDate desc,createdDate desc,billNo desc"
	}, params);
	return common.apiDo({
		apiKey: 'ec-score-scoreAcctFlow-listByFlowCust',
		...params
	});
};

/**
 * 积分规则保存
 */
reqHandler.saveScoreRule = async function (params) {
	return common.apiDo({
		apiKey: 'ec-score-rule-scoreRule-saveRuleFromMeta',
		jsonParam: {
			domainKind: params.domainKind || 21, //积分规则类型：21(按客户消费)/23(按客户充值)
			money: params.money || 10, //表示M钱产生N积分的M金额；缺省为1
			score: params.score || 1, //表示M钱产生N积分的N积分
		},
	});
};

/**
 * 获取积分规则 无参数
 */
reqHandler.getScoreRule = async function () {
	return common.apiDo({ apiKey: 'ec-score-rule-scoreRule-getRuleToMeta' });
};


/**
 * 获取VIP等级列表 
 */
reqHandler.getRankList = async function () {
	return common.apiDo({
		apiKey: 'ec-mdm-cust-rule-get',
	});
}

/**
 * 等级规则
 * 获取列表
 */
reqHandler.saveRankRule = async function (params) {
	params = format.packJsonParam(params);
	return common.apiDo({ apiKey: 'ec-mdm-cust-rule-save', ...params, });
};


/**
 * 积分调整
 */
reqHandler.saveScoreAdjustBill = async function (params) {
	return common.apiDo({
		apiKey: 'ec-score-scoreAdjustBill-save',
		jsonParam: {
			traderKind: 2, //固定组织结构
			...params
		}
	})
}

/**
 * 客户分析储值及收款统计接口
 * @param {Object} params 
 */
reqHandler.getCustRechargeNum = async function (params = {}) {
	params = Object.assign({
		wrapper: true,
		orderByDesc: true,
		proDateGte: '2019-01-01',
		proDateLte: common.getCurrentDate(),
		orderBy: 'recharge',
	}, params);
	return common.apiDo({
		apiKey: 'ec-sspd-salesBill-getCusRechargeNum',
		...params
	});
};
