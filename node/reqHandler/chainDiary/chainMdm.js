'use strict'
const common = require('../../lib/common');
const format = require('../../data/format');

let chainMdm = module.exports = {};

/**
 * 连锁日记登陆并且更新LOGINDATA
 * @param {Object} params 
 * @param {String} params.mobile 手机号码
 * @param {Boolean} params.mustVerify true或false, 仅开发或测试环境下有用，默认不会真正发送短信验证码",
 * @param {String} params.verifyCode 验证码
 * @param {Number} params.unitid
 * @param {String} params.productCode 客户端产品代码
 * @param {String} params.productVersion 客户端产品版本 
 */
chainMdm.chainLogin = async function (params) {
    const res = await common.apiDo({ apiKey: 'ec-mdm-bcd-login', ...params });
    try {
        LOGINDATA = res.result.data;
    } catch (error) {
        throw new Error('更新LOGINDATA失败');
    };
};

/**
 * 停用供应商
 */
chainMdm.disableSupplier = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-org-disable', ...params });
};

/**
 * 启用供应商
 */
chainMdm.enableSupplier = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-org-enable', ...params });
};

/**
 * 新增客户
 */
chainMdm.saveCustomer = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-mdm-org-saveFull', ...params });
};

/**
 * 客户列表
 */
chainMdm.getCustList = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-org-cust-list', ...params });
};

/**
 * 客户详情
 */
chainMdm.getCustInfo = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-org-getFull', ...params });
};

/**
 * 客户删除
 */
chainMdm.delCust = async function (params) {
    return common.apiDo({ apiKey: 'ec-mdm-org-delete', ...params });
};

/**
 * 退出
 */
chainMdm.logOut = async function (params = {}) {
    common.apiDo({ apiKey: 'ec-mdm-user-logout', ...params });
    LOGINDATA = {};
    BASICDATA = {};
};











