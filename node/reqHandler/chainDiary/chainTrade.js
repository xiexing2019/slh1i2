const common = require('../../lib/common');
const format = require('../../data/format');
const reqHandler = require('../shopDiary/bill');
let chainTrade = module.exports = {};


/**
 * 分销入库列表
 * @param {String} billNo 批次号
 * @param {String} billNoGte 批次号起
 * @param {String} billNoLte 批次号止
 * @param {Date} proDateGte 发生日期起
 * @param {Date} proDateLte 发生日期止
 * @param {Number} compId 供应商Id
 */
chainTrade.purchaseListByAgent = async function (params = {}) {
    return common.apiDo({ apiKey: 'ec-trade-salesBill-findAgentBill', ...params });
};

/**
 *分销入库详情 
 */
chainTrade.purchaseInfoByAgent = async function (params) {
    return common.apiDo({ apiKey: 'ec-trade-purBill-getBillFullFromSalesBill', ...params });
};

/**
 * 分销入库保存
 */
chainTrade.savePurchaseFromAgent = async function (params) {
    params = format.packJsonParam(params);
    params.productCode = 'bcdIOS';
    params.roductVersion = '1.4.5';
    return common.apiDo({ apiKey: 'ec-trade-purBill-save-from-sales-bill', ...params });
};

/**
 * 开销售单
 */
chainTrade.saveSalesBill = async function (params) {
    params = format.packJsonParam(params);
    params.jsonParam = reqHandler.jsonParamFormat(params.jsonParam);
    return common.apiDo({ apiKey: 'ec-trade-salesBill-saveBill', ...params });
};

/**
 * 查看销售单详情 
 */
chainTrade.getSalesInfo = async function (params) {
    return common.apiDo({ apiKey: 'ec-trade-salesBill-getBillFull', ...params });
};

/**
 * 获取销售单列表
 */
chainTrade.getSalesList = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-trade-salesBill-findBill', ...params });
};

/**
 * 作废销售单
 */
chainTrade.deleteBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-trade-salesBill-deleteBill', ...params });
};

/**
 * 挂单列表
 */
chainTrade.pendList = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-trade-pendBill-findPendBill', ...params });
};

/**
 * 查询首页数据
 */
chainTrade.getHomePageDate = async function (params = {}) {
    params = Object.assign({
        beginDate: common.getCurrentDate(),
        endDate: common.getCurrentDate(),
    }, params);
    return common.apiDo({ apiKey: 'ec-trade-diary-getPortalDataByDateArea', ...params });
};

/**
 * 获取多门店库存
 */
chainTrade.getInvOverStore = async function (params) {
    params.productCode = 'bcdIOS';
    params.roductVersion = '1.4.5';
    params.pageSize = 20;
    params.pageNo = 1;
    return common.apiDo({ apiKey: 'ec-inv-queryInvNumByOverStore', ...params });
};

/**
 * 开要货单
 */
chainTrade.saveAskBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-askBill-saveBill', ...params });
};

/**
 * 要货单详情 
 */
chainTrade.getAskBillFull = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-askBill-getBillDetail', ...params });
};

/**
 * 要货单列表
 */
chainTrade.getAskBillList = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-askBill-listBill', ...params });
};

/**
 * 要货单审核
 */
chainTrade.auditAskBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-askBill-auditAskBill', ...params });
};

/**
 * 要货单审核撤销
 */
chainTrade.revokeAskBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-askBill-revokeAskBill', ...params });
};

/**
 * 要货单明细
 */
chainTrade.findAskBillDetail = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-askdetail-findAskDetail', ...params });
};

/**
 * 要货单终结
 */
chainTrade.terminateAskBill = async function (params) {
    return common.apiDo({ apiKey: 'ec-inv-askBill-terminateBill', ...params });
};

/**
 * 要货单转调拨单
 */
chainTrade.saveMoveOutBill = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-out-bill-save', ...params });
};

/**
 * 调拨单详情
 */
chainTrade.getMoveOutBillFull = async function (params) {
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-out-bill-getBeanById', ...params });
};

/**
 * 调拨单列表
 */
chainTrade.getMoveOutBillList = async function (params) {
    params.bizDateS = common.getCurrentDate();
    params.bizDateE = common.getCurrentDate();
    params.shopId = LOGINDATA.shopId;
    params.invId = LOGINDATA.shopId;
    params = format.packJsonParam(params);
    return common.apiDo({ apiKey: 'ec-inv-move-bill-list', ...params });
};
