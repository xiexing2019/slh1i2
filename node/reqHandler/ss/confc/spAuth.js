const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');
let spAuth = module.exports = {
    url: ssCaps.confcUrl
};

/**
 * 衣科员工登录
 * @param {object} params jsonParam
 * @param {string} params.code 工号
 * @param {string} params.pass 密码
 */
spAuth.staffLogin = async function (params = {}) {
    params = format.packJsonParam(Object.assign({}, ssAccount.ecStaff, params));
    const res = await common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};

/**
 * 衣科员工退出登录
 */
spAuth.staffLogout = async function (params) {
    const res = await common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ecLogin-staffLogout', ...params });
    LOGINDATA = {};//登出成功后清除登录信息
    return res;
};

/**
 * 保存衣科员工（管理员）
 * @param {Object} params
 */
spAuth.saveStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-saveStaff', ...params });
};

/**
 * 查看员工列表
 * @param {Object} params jsonParam
 */
spAuth.getStaffList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-list', ...params });
};

/**
 * 查询员工详情
 * @param {Object} params
 * @param {Number} params.id
 * @param {Boolean} params.wrapper true: 需要编码转换； false:不需要
 */
spAuth.getStaffInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-getStaff', ...params });
};


/**
 * 启用员工
 * @param {Object} params
 * @param {Number} params.id 员工id
 */
spAuth.enableStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-enable', ...params });
};

/**
 * 停用员工
 * @param {Object} params
 * @param {Number} params.id 员工id
 */
spAuth.disableStaff = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecStaff-disable', ...params });
};

/**
 * 修改当前用户密码
 * @param {Object} params
 * @param {string} params.oldPwd 旧密码
 * @param {string} params.newPwd 新密码
 */
spAuth.changePwd = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-ecCred-changePwd', ...params });
};

/**
 * 查看角色下的员工列表
 * @param {Object} params
 * @param {string} params.roleCode 角色类型，wmsManager仓管
 */
spAuth.getStaffListByRoleCode = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-staffRole-getAllStaffByRoleCode', ...params });
};

//角色管理

/**
 * 保存角色
 * @param {Object} params
 */
spAuth.saveRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-save', ...params });
};

/**
 * 获取角色详情
 * @param {Object} params
 * @param {Number} params.id
 * @param {string} params.wrapper  true: 需要编码转换； false:不需要
 */
spAuth.getRoleInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-get', ...params });
};


/**
 * 获取角色列表
 * @param {Object} params
 */
spAuth.getRoleList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-list', ...params });
};

/**
 * 角色启用
 * @param {Object} params
 * @param {Number} params.id
 */
spAuth.enableRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-enable', ...params });
};

/**
 * 停用角色 
 * @param {Object} params
 * @param {Number} params.id
 */
spAuth.disableRole = async function (params) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-confc-role-disable', ...params });
};

//权限管理

/**
 * 获取权限列表
 */
spAuth.getPowerList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-func-list', ...params });
};

/**
 * 保存角色权限
 * @param {Object} params
 * @param {Number} params.roleId
 * @param {Array}  params.funcIds 权限列表
 */
spAuth.savePower = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-roleFunc-saveRoleFunc', ...params });
};

/**
 * 获取角色权限
 */
spAuth.getPowerInfo = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-roleFunc-findFuncIdsByRoleId', ...params });
};

/**
 * 获取当前会话所有权限
 */
spAuth.getCurrentPower = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-roleFunc-getCurrentFunc', ...params });
};


/**
 * 白名单-新增
 * @description sessionId使用中心区的会话 
 * @description scopeCap 白名单范围，现在固定1
 * @param {object} params 
 * @param {string} params.mobile 
 */
spAuth.saveWhiteList = async function (params) {
    params.scopeCap = 1;
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-save', ...params });
};

/**
 * 白名单-删除
 * @description sessionId使用中心区的会话 
 * @param {string} id 
 */
spAuth.deleteWhiteList = async function (id) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-delete', id });
};

/**
 * 白名单列表查询
 * @description sessionId使用中心区的会话 
 */
spAuth.getWhiteList = async function () {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spadmin-spWhiteList-list' });
};

/**
 * 保存租户的参数
 * @param {Object} params
 * @param {string} params.tenantId 
 * @param {string}  params.code 参数代码
 */
spAuth.saveUnitParamVal = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-globalUnitParam-saveUnitParamVal', ...params });
};

/**
 * 查看字典树
 * @param {Object} params
 * @param {string} params.hierId 字典编码，如省市编码为850
 * @param {string} params.codeId parent_code
 */
spAuth.getDictTree = async function (params) {
    return common.post(this.url, { apiKey: 'ec-config-dict-tree', ...params });
};

/**
 * 租户员工登录
 * @param {Object} params
 * @param {string} params.code 工号
 * @param {string} params.pass 密码
 * @param {string} params.tenantCode 租户的code，限定卖家
 */
spAuth.spStaffAuthcLogin = async function (params) {
    params = format.packJsonParam(params);
    params._cid = 0;
    const res = await common.get(ssCaps.spbUrl, { apiKey: 'ec-spauth-spStaffAuthc-staffLogin', ...params });
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId
        await common.delay(500);
    };
    return res;
};