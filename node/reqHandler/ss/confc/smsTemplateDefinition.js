const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');


const smsTemplateDefinition = {
    url: ssCaps.confcUrl,

    /**
     * 短信模版添加/修改
     * @param {object} params 
     */
    saveTemplate: async function (params) {
        params = format.packJsonParam(params);
        return common.post(this.url, { apiKey: 'ec-smsTemplateDefinition-saveTemplate', ...params });
    },

    /**
     * 短信模版查询
     * @param {object} params 
     */
    findTemplateList: async function (params) {
        params = format.packJsonParam(params);
        return common.post(this.url, { apiKey: 'ec-smsTemplateDefinition-list', ...params });
    },

    /**
     * 短信模版删除
     * @param {object} params 
     * @param {string} params.id 模版id
     */
    deleteTemplate: async function (params) {
        params = format.packJsonParam(params);
        return common.post(this.url, { apiKey: 'ec-smsTemplateDefinition-delete', ...params });
    },
};

module.exports = smsTemplateDefinition;

