const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const caps = require('../../../data/caps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');
let ecPrint = module.exports = {
    url: 'https://hzdev.hzdlsoft.com/confc'
};

// 打印

/**
 * 查询打印机固件列表
 * @param {object} params
 * @param {string} params.wrapper 是否转码, true则返回ecCaption里的值
 * @param {string} params.orderBy 排序字段 updatedDate
 * @param {string} params.orderByDesc 	是否倒叙 true
 */
ecPrint.getPrinterFirmwareList = async function (params = {}) {
    return common.post(this.url, { apikey: 'ec-print-printerFirmware-list', ...params });
};
