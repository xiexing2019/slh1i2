const caps = require('../../data/caps');

module.exports = caps.name.includes('ss') ? require('./_ssCaps') : require('../sp/spCaps');
