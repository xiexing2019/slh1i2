const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

const spbi = module.exports = {};

/**
 * 销售情况总计
 */
spbi.allSalesStatistics = async function (params) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-dataCenterStatistics-allSalesStatistics', ...params });
};

/**
 * 销售情况当日走势
 */
spbi.todaySalesStatisticsTrend = async function (params) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-dataCenterStatistics-todaySalesStatisticsTrend', ...params });
};

/**
 * 销售情况近期走势
 * @param {object} params
 * @param {string} params.dateIn 起止时间，例子[“2019-05-20”,”2019-05-21”]
 */
spbi.allSalesStatisticsTrend = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-dataCenterStatistics-allSalesStatisticsTrend', ...params });
};

/**
 * 店铺排名接口
 * @param {object} params
 * @param {string} params.dateIn 起止时间，例子[“2019-05-20”,”2019-05-21”]
 * @param {number} params.orderType 排序类型，0访问量 1订单量 2绑定客户
 * @param {boolean} params.orderByDesc 是否倒叙，true
 */
spbi.getShopRankStatistics = async function (params) {
    params = format.packJsonParam(params, ['orderType', 'orderByDesc']);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-dataCenterStatistics-getShopRankStatistics', ...params });
};

/**
 * 获取公众号场景推送统计信息接口
 * @param {object} params
 * @param {string} params.proDate  日期，”2019-6-10”  scene_id: 1开单成功通知 2订单发货通知 3消费成功通知 4退货通知 5反馈结果通知
 */
spbi.getSceneStatsByDate = async function (params) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-spMqSendStats-getSceneStatsByDate', ...params });
};

/**
 * 获取一段时间场景统计占比信息接口
 * @param {object} params
 * @param {string} params.dateIn 起止时间，例子[“2018-05-20”,”2020-05-21”]
 */
spbi.getSceneStatsByDateIn = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-spMqSendStats-getSceneStatsByDateIn', ...params });
};

/**
 * 获取公众号推送详细统计信息接口
 * @param {object} params
 * @param {string} params.dateIn 起止时间，例子[“2018-05-20”,”2020-05-21”]
 */
spbi.getDetailStatsByDate = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spbi-spMqSendStats-getDetailStatsByDate', ...params });
}