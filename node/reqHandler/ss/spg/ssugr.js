const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

const ssugr = module.exports = {
    url: ssCaps.spgUrl,
    captcha: '000000',
    dlProductVersion: '2.9.0',
};

/**
 * Sass 获取绑定二维码
 * @param {object} params
 * @param {number} params.saasType 1.商路花 2.笑铺(必传)
 * @param {number} params.saasShopId Saas店铺id (必传)
 * @param {string} params.saasShopName Sass店铺名称(必传)
 * @param {string} params.saasSn slhSn(必传)
 * @param {string} params.saasUnitId sass帐套id(必传)
 * @param {string} params.timeStamp 时间戳(必传)
 * @returns {object} {code,msg,data:{bindCode}}
 */
ssugr.getBindCodeUrl = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-getBindCodeUrl', ...params });
};

/**
 * Sass开通情况
 * @returns {object} {code,msg,data:{val:‘商陆花客户：1,是，0，不是’}}
 */
ssugr.getDwxxStatusByPhone = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-getDwxxStatusByPhone', ...params });
};

/**
 * 二维码绑定sass
 * @param {object} params
 * @param {number} params.bindId 绑定主键(必传)
 * @param {number} params.timeStamp 生成时间(必传)
 */
ssugr.bindByUrl = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-bindByUrl', ...params });
};

/**
 * 绑定码绑定Sass
 * @param {object} params
 * @param {string} params.bindCode 绑定码(必传)
 */
ssugr.bindByCode = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-bindByCode', ...params });
};

/**
 * 同步sass商品到商城
 * @param {object} params
 * @param {string} params.spuSyncDays
 */
ssugr.synDresForSaas = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-synDresForSaas', ...params });
};

/**
 * 获取绑定saas店铺同账套下的店铺列表接口
 */
ssugr.findSaasShopList = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-findSaasShopList', ...params });
};

/**
 * saas 同账套下店铺改绑 ec-ssugr-ssBindSaas-changeBindShop
 * @param {object} params
 * @param {string} params.slhShopId Saas店铺id   是
 * @param {string} params.slhShopName Saas店铺名称  是
 * @param {string} params.slhSn slhSn  是
 * @param {string} params.slhUnitId saas 账套id  是
 */
ssugr.changeBindShop = async function (params = {}) {
    params = format.packJsonParam(Object.assign({ captcha: common.getRandomNumStr(5) }, params));
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-changeBindShop', ...params });
};

/**
 * saas店铺解绑 ec-ssugr-ssBindSaas-unBindShop
 * @param {object} params
 */
ssugr.unBindShop = async function (params = {}) {
    params = Object.assign({ captcha: common.getRandomNumStr(5) }, params);
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-unBindShop', ...params });
};

/**
 * saas 同账套下店铺改绑时，进行验证码校验
 * @param {object} params
 * @param {string} params.captcha
 */
ssugr.checkCaptcha = async function (params = {}) {
    params = format.packJsonParam(params)
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-checkCaptcha', ...params });
};

/**
 * saas 门店库存同步
 * @param {object} params
 * @param {array} params.slhInvIds Saas店铺id列表 例如：[1,2,3,4,5,6]
 */
ssugr.syncShopInvIds = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.url, { apiKey: 'ec-ssugr-ssBindSaas-syncShopInvIds', ...params })
};

/**
 *
 */