const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');


const confg = module.exports = {};

/**
 * 新增保存图库
 * @param {object} params 
 * @param {string} params.name 
 * @param {string} params.sizeType 
 * @param {string} params.num 
 * @param {string} params.bannerContent  {docId:,typeId:}
 * @param {string} params.actContent  {docId:,typeId:}
 */
confg.createGallery = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-confg-gallery-create', ...params });
};

/**
 * 修改保存单个图库
 * @param {object} params 
 * @param {string} params.name 
 * @param {string} params.sizeType 
 * @param {string} params.num 
 * @param {string} params.bannerContent  {docId:,typeId:}
 * @param {string} params.actContent  {docId:,typeId:}
 */
confg.updateGallery = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-confg-gallery-update', ...params });
};

/**
 * 查询图库列表
 * @param {object} params 
 * @param {string} params.name 
 * @param {string} params.sizeType 
 * @param {string} params.flag 
 */
confg.galleryList = async function (params = {}) {
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-confg-gallery-list', ...params });
};

/**
 * 删除单个图库
 * @param {object} params 
 * @param {string} params.id 
 */
confg.deleteGallery = async function (params = {}) {
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-confg-gallery-delete', ...params });
};

/**
 * 保存分类图库
 * @param {object} params
 * @param {string} params.name
 * @param {string} params.name.name
 * @param {string} params.content.docContent
 */
confg.saveCategoryGallery = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-categoryGallery-save', ...params });
};

/**
 * 分类图库查询
 * @param {object} params
 */
confg.findCategoryGallery = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-categoryGallery-list', ...params });
};

/**
 * 分类图库删除
 * @param {object} params
 * @param {object} params.id
 */
confg.delCategoryGallery = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-categoryGallery-delete', ...params });
};

/**
 * 查询分类图库树形结构
 * @param {object} params
 * @param {object} params.type
 * @param {object} params.parentId
 */
confg.findCategoryGalleryTree = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-confg-categoryGallery-findCategoryGalleryTree', ...params });
};


