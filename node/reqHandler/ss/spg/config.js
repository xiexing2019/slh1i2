const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');


const config = module.exports = {};

/**
 * 获取区域省份
 * @param {object} params 
 */
config.getDistrictRegion = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-config-spRegion-getDistrictRegion', ...params });
};

/**
 * 省市区
 * @param {object} params 
 */
config.exportDistrictRegion = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-config-spRegion-exportDistrictRegion', ...params });
};

/**
 * 获取个性化参数列表
 * @param {object} params {"domainKind":"business","ownerKind":"4","ownerId":925}
 * @param {string} params.tenantId 
 */
config.findDressClass = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spdresb-dresClass-findDressClass', ...params });
};

/**
 * 获取个性化参数列表
 * @param {object} params {"domainKind":"business","ownerKind":"4","ownerId":925}
 * @param {string} params.tenantId 
 * @param {string} params.code 参数编号，例：show_inventory_in_ssmall
 * @param {string} params.domainKind 填business
 * @param {string} params.ownerKind 属主总类，填4
 * @param {string} params.ownerId 填卖家的tenantId
 */
config.findProductParams = async function (params) {
    // params.domainKind = 'business'; //配置域类型，业务用填“business”
    // params.ownerKind = '4'; //属主类型，商城填4
    params = Object.assign(format.packJsonParam(params, ['sellerId']), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-config-param-findProductParams', ...params });
};

/**
 * 单个系统参数获取
 * @param {object} params
 * @param {string} params.code 参数编号，例：show_inventory_in_ssmall
 * @param {string} params.domainKind 填business 'system'
 * @param {string} params.ownerKind 属主总类，填4
 * @param {string} params.ownerId 填卖家的tenantId
 */
config.getParamInfo = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-config-param-getParamInfo', ...params });
};

/**
 * 获取参数
 * @param {object} params
 * @param {string} params.code 参数编号，例：show_inventory_in_ssmall
 * @param {string} params.domainKind 填business 'system'
 * @param {string} params.ownerKind 属主总类，填4
 * @param {string} params.ownerId 填卖家的tenantId
 */
config.getParamVal = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-config-param-getParamVal', ...params });
};

/**
 * 保存更新参数值
 * @param {object} params {"domainKind":"business","ownerKind":"4","ownerId":925}
 * @param {string} params.code 参数代码
 * @param {string} params.ownerId 所属者ID，商城卖家的tenantId
 * @param {string} params.val 参数值
 */
config.saveOwnerVal = async function (params) {
    //{"ownerKind":"4","data":[{"code":"ss_dres_show_price_when_share","domainKind":"business","ownerId":929,"val":"1"}]}
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    // console.log(params);
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-config-param-saveOwnerVal', ...params });
};

/**
 * 获取基础数据字典
 * @description confc.sc_dict
 * @param {object} params 
 * @param {string} params.typeId 字典类别
 */
config.getSpgDictList = async function (params = {}) {
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-config-dict-list', ...params });
};

/**
 * 获取字典类别列表
 * @description spb.sc_dict
 * @param {object} params 
 * @param {string} params.typeId 字典类别
 */
config.getSpbDictList = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-config-list-dict', ...params });
};

/**
 * 保存价格接口
 * @description spb.sc_dict
 * @param {object} params 
 * @param {string} params.id 修改必传，新增不传
 * @param {string} params.typeId 字典类别
 */
config.saveDict = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-config-save-dict', ...params });
};