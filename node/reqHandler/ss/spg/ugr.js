const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let ugr = module.exports = {
    url: ssCaps.spgUrl,
    captcha: '000000'
};

/**
 * 买家小程序登陆(自动注册)
 * @param {Object} params jsonParam
 * @param {string} params.authCode
 * @param {string} params.nickName
 * @param {string} params.avatar
 * @param {string} params.productCode
 * @param {string} params.appId
 * @param {number} params.tenantId
 * @param {string} params.openId
 * @param {string} params.mobile
 * @param {string} params.scene 门店类型（0临时商城，1正式商城，2分销商城）
 */
ugr.wxUserLoginWithRegister = async function (params) {
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-loginWithRegister', ...params });
};

/**
 * 买家小程序登陆
 * @param {Object} params
 * @param {string} params.authCode
 * @param {string} params.nickName
 * @param {string} params.avatar
 * @param {string} params.productCode
 * @param {string} params.appId
 * @param {number} params.tenantId
 * @param {string} params.openId
 * @param {string} params.mobile
 * @param {string} params.scene 门店类型（0临时商城，1正式商城，2分销商城）
 */
ugr.wxAppLogin = async function (params) {
    params.productCode = 'slhMallTest'; // 写死传slhMallH5 德贵确认
    BASICDATA.appId = params.appId; // 创建支付ec-sppur-payBill-createPay 需要传
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-loginV2', ...params });
};

/**
 * 切换门店刷新会话
 * @param {Object} params jsonParam
 * @param {string} params.sellerTenantId 卖家tid
 */
ugr.switchShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-switchShop', ...params });
};

/**
 * 查询是当前openId是否关注公众号
 * @param {Object} params jsonParam
 * @param {string} params.openId 当前用户openId
 * @param {string} params.sellerTenantId 卖家tid
 */
ugr.getSubscribeMpStatus = async function (params) {
    params.productCode = 'slhMallH5';
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-getSubscribeMpStatus', ...params });
};

/**
 * 分享进入门店数据保存
 * @param {Object} params jsonParam
 * @param {string} params.openId 买家openid
 * @param {string} params.srcType 来源类型：2买家分享；3卖家分享
 * @param {string} params.srcId 来源id：买家租户id（srcType=2）/卖家租户id（srcType=3）
 * @param {string} params.shopId 卖家门店id
 */
ugr.shareToUp = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-shareToUp', ...params });
};

/**
 * 小程序买家切换为商城操作员
 * @param {Object} params
 * @param {string} params.sellerTenantId  卖家tid
 */
ugr.wxAppSwitchSeller = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-ugr-wxApp-switchSeller', ...params });
};