const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

const spugr = module.exports = {
    captcha: '000000',
    dlProductVersion: '2.9.0',
};

/**
 * 店铺门头照
 * @description 传入店铺名字获取店铺门头照
 * @param {Object} params
 * @param {number} params.shopName 店铺名称
 */
spugr.getRandomShopOutImg = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-getRandomShopOutImg', ...params });
};

/**
 * 店铺内照片
 * @description 随机获取一张店铺内照片的docId
 * @param {Object} params
 */
spugr.getRandomShopInImg = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-getRandomShopInImg', ...params });
};


/**
 * 身份证照片识别
 * @param {Object} params
 * @param {number} params.frontUrl 身份证正面
 * @param {number} params.backUrl 身份证背面
 */
spugr.idCardDetect = async function (params = {}) {
    // params = Object.assign(format.packJsonParam(params));
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-idCardDetect', ...params });
};

/**
 * 商陆花员工模拟登录（好店迁入）
 */
spugr.slhUserLogin = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spLogin-slhUserLogin', ...params });
};

/**
 * 店铺公众号配置保存
 */
spugr.saveMpConfig = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-saveMpConfig', ...params });
};

/**
 * 门店公众号配置获取
 * @param {number} shopId
 */
spugr.getMpConfig = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params));
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-getMpConfig', ...params });
};

/**
 * 修改店铺基本信息
 */
spugr.updateShop = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-updateShop', ...params });
};

/**
 * 店铺详情
 */
spugr.getShopDetail = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-getDetail', ...params });
};

/**
 * 获取验证码-固定发送验证码到公司老板手机，用于解绑和改绑
 */
spugr.sendCodeToCompany = async function () {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spSmsCaptcha-sendCodeToCompany' });
};

/**
 * 买家绑定手机号
 * @param {Object} params
 * @param {number} params.mobile 手机号
 * @param {string} params.captcha 验证码
 */
spugr.buyerBindMobile = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spUserManage-buyerBindMobile', ...params });
};

/**
 * 更新店铺备注/别名
 * @param {Object} params
 * @param {number} params.shopId
 * @param {number} params.alias 备注、店铺别名
 */
spugr.updateAlias = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['shopId']), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-shop-updateAlias', ...params });
};

/**
 * 用户名称修改接口
 * @param {Object} params
 * @param {number} params.userId
 * @param {number} params.userName 名称
 */
spugr.updateUserName = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['userId']), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spUser-updateUserName', ...params });
};

/**
 * 用户登录
 * @description
 * 1. 登录，返回会话；若未注册，并且验证码正确，则自动注册用户；只支持验证码方式，不再支持密码登录
 * 2. 成功后会更新信息到LOGINDATA
 * @param {Object} params jsonParam
 * @param {string} params.code 登录名：手机号或邮箱等*
 * @param {string} params.pass 密码
 * @param {string} params.captcha 验证码
 * @param {string} param.authcType 验证方式：1:密码 2.验证码
 */
spugr.userLogin = async function (params) {
    params = format.packJsonParam(Object.assign({ dlProductCode: 'slhMallAssistIOS', dlProductVersion: this.dlProductVersion }, params), ['dlProductCode', 'dlProductVersion']);
    const res = await common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spLogin-userLogin', ...params });
    //check=false时,为验证请求失败,不会登录成功
    if (res.result.code == 0) {
        LOGINDATA = Object.assign({}, res.params.jsonParam, res.result.data);//更新sessionId

        //防止请求间隔过短,单元区会话失效的错误
        //全局区登录，通过mq通知单元区；单元区还没处理好，就请求到了单元区
        //暂时加延迟处理
        await common.delay(500);
    };
    return res;
};

/**
 * 登录，返回会话；若未注册，并且验证码正确，则自动注册用户
 * @param {Object} params jsonParam
 * @param {number} params.phone 手机号
 * @param {number} params.captcha 验证码
 */
spugr.userLoginWithRegister = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spLogin-userLoginWithRegister', ...params });
};

/**
 * 获取店铺开通信息
 * @description
 * 1. 如果返回的company为空，则表示没有购买过套餐，跳转到购买套餐页面
 * 2. 如果返回的shop为空，则表示没有开通过店铺，跳转到开通页面
 * 3. 否则根据cred中的信息，进入上一次访问的店铺首页
 */
spugr.fetchShops = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-fetchShops', ...params });
};

/**
 * 用户门店列表
 * @param {Object} params jsonParam
 * @param {number} params.userId 用户id
 */
spugr.userShopList = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssShopManager-findByUser', ...params });
};

/**
 * 切换用户门店
 * @param {Object} params jsonParam
 * @param {string} params.newUnitId 要切换门店的unitId
 * @param {string} params.mobile
 */
spugr.changeUserShop = async function (params) {
    params = format.packJsonParam(params);
    const res = await common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spLogin-changeUserShop', ...params });
    // console.log(`res=${JSON.stringify(res)}`);
    //切换成功后 覆盖更新登录信息
    if (res.result.code == 0) {
        Object.assign(LOGINDATA, res.result.data);
        await common.delay(500);
    };
    return res;
};

/**
 * 店铺开通流更新
 * @description 完成店铺开通流程后 更新状态至99即可
 * @param {Object} params jsonParam
 * @param {string} params.shopId 门店id
 * @param {string} params.openFlowFlag 开通流flag
 */
spugr.updateOpenFlow = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-updateOpenFlow', ...params });
};


/**
 * 保存店铺运营模式
 * @param {object} params
 * @param {string} params.shopId 店铺id
 * @param {string} params.bizModelId 运营模式 1:批发, 2:零售
 */
spugr.updateShopOperateModel = async function (params = {}) {
    // params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-operateModel-update', ...params });
};


/**
 * 卖家首页统计(当天数据)
 * @param {object} params
 */
spugr.getMerchantHomePageStatistics = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-merchantHomePageStatistics-merchantHomePageStatistics', ...params });
};

/**
 * 查询全部套餐
 * @param {object} params
 * @param {string} params.flag 删除标识 1可用，0已删除
 */
spugr.findSetList = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssSet-findSetList', ...params });
};

/**
 * 提交套餐订单
 * @param {object} params
 * @param {string} params.num 数量
 * @param {string} params.code 套餐crmId
 */
spugr.saveValueAddService = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-saveValueAddService', ...params });
};

/**
 * 套餐订单支付回调
 * @param {object} params
 * @param {string} params.billId 订单id
 */
spugr.backCallValueAddService = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-backCallValueAddService', ...params });
};

/**
 * 套餐升级
 * @param {object} params
 * @param {string} params.num 数量
 * @param {string} params.code 套餐crmId
 */
spugr.upgradeValueAddService = async function (params) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-upgradeValueAddService', ...params });
};

/**
 * 当前用户可用套餐列表
 * @param {object} params
 */
spugr.findAvailableSetList = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-findAvailableSetList', ...params });
};

/**
 * 获取套餐到期信息
 * @description ss_set,ss_saas_shop,sp_shop
 * @param {object} params
 */
spugr.getShopSetInfo = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-getShopSetInfo', ...params });
};

/**
 * 套餐订购记录列表
 */
spugr.findVasBillList = async function (params = {}) {
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-findVasBillList', ...params });
};

/**
 * 保存商户信息
 */
spugr.appCreateTenant = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params.name = params.jsonParam.name;
    params.mobile = params.jsonParam.mobile;
    params.provCode = params.jsonParam.provCode;
    params.address = params.jsonParam.address;
    params.typeId = params.jsonParam.typeId;
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-spugr-spTenant-appCreateTenant', ...params });
};

/**
 * 用户申请开店
 * @param {string} name
 * @param {number} phone
 */
spugr.saveUpShopLog = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-upShop-saveUpShopLog', ...params });
}

/**
 * 开店记录
 * @param {string} name
 * @param {number} phone
 * @param {string} startDate
 * @param {string} endDate
 */
spugr.getUpShopLogs = async function (params) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-upShop-getUpShopLogs', ...params });
}

/**
 * 更新店铺额外操作员数量
 * @param {number} shopId
 * @param {number} extraManageNum 额外操作员数量
 */
spugr.updateExtraManageNum = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['shopId']), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-shop-updateExtraManageNum', ...params });
}

/**
 * 额外操作员修改记录
 * @param {number} shopId
 */
spugr.findExtraChangeLogLogs = async function (params) {
    params = Object.assign(format.packJsonParam(params, ['shopId']), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-extraChangeLog-findLogs', ...params });
}

/**
* 店铺查询
* @param {object} params
* @param {string} params.nameLike 名字，支持模糊搜素
*/
spugr.getShopList = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params.wrapper = true;
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-shop-list', ...params });
};

/**
 * 买家首页获取店铺详情
 * @param {object} params
 * @param {string} params.id 店铺id
 */
spugr.getShopFromBuyer = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params.wrapper = true;
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spugr-spShop-getShopFromBuyerV2', ...params });
};

/**
* 买家首页获取店铺详情
* @param {object} params
* @param {string} params.id 店铺id
*/
spugr.getShopForBuyerInSeller = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    // params = format.packJsonParam(params, ['_cid', '_tid']);
    params.wrapper = true;
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spugr-spShop-getShopForBuyerInSeller', ...params });
};

/**
 * 分类获取店铺功能列表(我的微商城权益)
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
spugr.getShopSuiteFuncList = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-listSuiteFuncWithCate', ...params });
};

/**
 * 通过银行卡图片获取银行信息接口
 * @param {object} params
 * @param {string} params.docId 文件上传返回的docId
 */
spugr.getBankInfoByImage = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'bankImageDistinguishp', ...params });
};

/**
 * 修改商户或银行卡信息
 * @param {object} params
 * @param {string} params.docId 文件上传返回的docId
 */
spugr.updateMerchantAndBank = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params._master = 1;
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-updateMerchantAndBank', ...params });
};

/**
 * 换卡的预提交阶段
 * @param {object} params
 * @param {string} params.merchantId 门店商户主键id 必填
 * @param {string} params.bankPhone 银行预留手机号 必填
 * @param {string} params.bankAccountNo 银行账号 必填
 * @param {string} params.bankAccountName 银行账户名 必填
 * @param {string} params.bankName 银行名称 必填
 * @param {string} params.bankCode 银行号 必填
 * @param {string} params.bankType 银行卡类型 1-个人 2-企业
 */
spugr.getPrepareBindingData = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-getPrepareBindingData', ...params });
};

/**
 * 银行卡改绑提交
 * @param {object} params
 * @param {string} params.merchantId 门店商户主键id 必填
 * @param {string} params.merBankId 银行账号id 来源于上一个接口返回 必填
 * @param {string} params.verifyCode 短信验证码
 */
spugr.executeBankCardBinding = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spMerchant-executeBankCardBinding', ...params });
}

/**
 * 批量保存帮助类别
 * @param {object} params
 * @param {string} params.id 主键id，修改的时候必传，新增的时候不传
 * @param {string} params.name 分类名称
 * @param {string} params.showOrder 排序，默认1
 */
spugr.batchSaveHelpCenterCat = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpCenterCat-batchSaveHelpCenterCat', ...params });
};

/**
 * 获取帮助分类（模块）列表
 * @param {object} params
 */
spugr.getCatList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpCenterCat-findCatList', ...params });
};

/**
 * 删除帮助中心分类（模块）
 * @param {object} params
 * @param {string} params.id 主键id
 */
spugr.delHelpCenterCat = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpCenterCat-delHelpCenterCat', ...params });
};

/**
 * 保存文章(视频教程)
 * @param {object} params
 * @param {string} params.id
 * @param {string} params.title
 * @param {string} params.keyWords
 * @param {string} params.showOrder
 * @param {string} params.docContent
 * @param {string} params.docId
 * @param {string} params.flag
 * @param {string} params.catId
 */
spugr.saveHelpDoc = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-saveHelpDoc', ...params });
};

/**
 * 查询文章
 * @param {object} params
 * @param {string} params.titleLike 标题，模糊查询
 * @param {string} params.catId 分类（模块）id
 * @param {string} params.keyWordsLike 关键词，模糊查询
 * @param {string} params.flag 状态，0删除，1正常，2草稿
 */
spugr.findHelpDoc = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-findHelpDoc', ...params });
};

/**
 * 删除文章
 * @param {object} params
 * @param {string} params.id 文章id
 */
spugr.delHelpDoc = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-delHelpDoc', ...params });
};

/**
 * 文章列表
 * @param {object} params
 */
spugr.getHelpDoclist = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-listHelpDoc', ...params });
};

/**
 * 根据id查询文章
 * @param {object} params
 * @param {string} params.id 文章id
 */
spugr.findHelpDocById = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-getHelpDocById', ...params });
};

/**
 * 查询视频列表
 * @param {object} params
 * @param {string} params.titleLike 标题，模糊查询
 * @param {string} params.keyWordsLike 模糊查询
 */
spugr.getVideoList = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-ssHelpDoc-getVideoList', ...params });
};

/**
 * 店铺开通独立公众号
 * @param {object} params
 * @param {string} params.shopId 店铺id(租户id)
 * @param {string} [params.slhSn] 商陆花SN(shopId和Sn不能同时为空)
 * @param {string} params.wxMpAppId 公众号appId
 * @param {string} params.wxAppChannelCode 公众号appId
 * @param {string} params.wxMpChannelCode 公众号渠道编码
 */
spugr.openIndependentWxMp = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-openIndependentWxMp', ...params });
};

// 短信群发

spugr.getEmbGuid = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-sms-getEmbGuid', ...params });
};

/**
 * 发送群发短信
 * @param {object} params
 * @param {string} params.mobiles 发送短信的对象手机号，用,隔开
 * @param {string} params.templateCode 短信代码
 * @param {string} params.param 短信可变参数
 */
spugr.sendGroupSms = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-sms-sendGroupSms', ...params });
};

/**
 * 购买短信包
 * @param {object} params
 * @param {string} params.productId 购买短信包
 */
spugr.chargeSms = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-sms-chargeSms', ...params });
};

/**
 * 查询发送记录
 * @param {object} params
 * @param {string} params.createdDateGte 起始时间
 * @param {string} params.createdDateLte 结束时间
 */
spugr.queryGroupSmsLog = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-sms-queryGroupSmsLog', ...params });
};

/**
 * 查询发送记录（web端）
 * @param {object} params
 * @param {string} params.createdDateGte 起始时间
 * @param {string} params.createdDateLte 结束时间
 * @param {string} params.shopNameLike 商户名称
 * @param {string} params.templateCodes 模版，用,隔开
 */
spugr.queryGroupSmsLogForWeb = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-sms-queryGroupSmsLogForWeb', ...params });
};

/**
 * 解密微信小程序用户信息(含敏感信息)
 * @param {object} params
 * @param {string} params.iv 获取加密算法初始向量
 * @param {string} params.encrytedData 加密数据
 */
spugr.decodeUserInfo = async function (params = {}) {
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-wxapp-decodeUserInfo', ...params });
};

/**
 * 获取可以切换店铺列表
 * @param {object} params
 * @param {string} params.shopId 需要排除的店铺Id
 */
spugr.findOtherShopList = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-findOtherShopList', ...params });
};

/**
 * 修改店铺信息（开通中）
 * @param {object} params
 * @param {string} params.id 要修改的店铺主键id
 * ...
 */
spugr.updateShopWhenOpen = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-spugr-spShop-updateShopWhenOpen', ...params });
};