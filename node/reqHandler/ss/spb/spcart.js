const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spcart = module.exports = {
    url: ssCaps.spbUrl,
};

/**
 * 购物车按spu备注
 * @param {object} params jsonParam
 */
spcart.updateSpuRem = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spcart-shoppingCart-updateSpuRem', ...params });
};
/**
 * 查询购物车
 * @param {object} params
 * @param {string} params.shopId 卖家租户id
 */
spcart.getShoppingCartList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spcart-shoppingCart-list', ...params });
    // 已废弃
    // return common.post(this.url, { apiKey: 'ec-spcart-shoppingCart-findCart', ...params });
};

