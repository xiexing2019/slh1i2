const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spsales = module.exports = {
    url: ssCaps.spbUrl,
};

/**
 * 查询可发货订单明细
 * @param {object} params
 * @param {string} params.salesBillId 销售单id
 */
spsales.getUnfilledDetails = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-getUnfilledDetails', ...params });
};

/**
* 查询账单
* @param {object} params
*/
spsales.WalletBillsList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-salesBill-listWalletBills', ...params });
};

/**
 * 批量发货并打印
 * @param {object} params
 */
spsales.batchSaveDeliverBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-batchSaveFull', ...params });
};


/**
 * 发货
 * @param {object} params
 */
spsales.saveDeliverBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-saveFull', ...params });
};

/**
 * 退货卖家支付回调
 * @description  卖家充值回调
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
spsales.returnReceivePayResult = async function (params = {}) {
    params = format.packJsonParam(Object.assign({
        code: 0,
        // 线上入账需要排除自动化数据 必须传负
        // 清分相关需要为正
        payNumber: -1003333333,//-common.getRandomNumStr(10),//-1003333333,
        payType: '1048576',
        _unitId: LOGINDATA.unitId
    }, params));
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-ssBillReturnRecharge-receivePayResult', ...params });
};

/**
 * 卖家删除退货退款单
 * @param {object} params
 * @param {string} params.returnBillIds 确认删除的退款单id列表。如：”returnBillIds”:[1,2,3]
 */
spsales.hideReturnBills = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-billReturn-hideReturnBills', ...params });
};

/**
 * 线下支付订单审核
 * @param {object} params
 * @param {string} params.id 订单id
 * @param {string} params.accept 是否同意true,false
 * @param {string} params.payWay accept=true时生效；1：现金，2：刷卡，3：汇款，4：代收，5：微信，6：支付宝
 */
spsales.verifyOfflineBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-verifyOfflineBill', ...params });
};

/**
 * 线下支付订单审核
 * @param {object} params
 * @param {string} params.saleBillIds 销售订单id集合 [1,2,3]
 */
spsales.confirmPickedUp = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-confirmPickedUp', ...params });
};

/**
 * 买家删除已完成订单
 * @param {object} params
 * @param {string} params.purBillIds 确认删除的采购订单ID列表。如：”purBillIds”:[1,2,3]
 */
spsales.displayPurBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-displayPurBill', ...params });
};

/**
 * 卖家删除已完成订单
 * @param {object} params
 * @param {string} params.purBillIds 确认删除的采购订单ID列表。如：”purBillIds”:[1,2,3]
 */
spsales.displaySalesBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-displaySalesBill', ...params });
};

/**
 * 卖家首页统计数据(待发货、待退款等)
 * @param {object} params
 * @param {string} params.purBillIds 确认删除的采购订单ID列表。如：”purBillIds”:[1,2,3]
 */
spsales.findStatistic = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBillStatistics-findStatistic', ...params });
};

/**
 * 获取关联客户人数本门店销售统计
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 * @param {long} params.userId 员工id，不传默认当前用户
 * @returns {object} {params, result}
 */
spsales.getSalesForEmployee = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-getSalesForEmployee', ...params });
};


/**
 * 获取关联客户人数本门店销售统计
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 * @param {long} params.userId 员工id，不传默认当前用户
 * @returns {object} {params, result}
 */
spsales.getSalesOverviewForEmployee = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spsales-salesBill-getSalesOverviewForEmployee', ...params });
};


/**
 * 查询订单的发货单列表
 * @param {object} params
 * @param {string} params.salesBillId 销售单id
 */
spsales.getSalesDeliverList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-getSalesDeliverList', ...params });
};

/**
 * 查看发货单详情
 * @param {object} params
 * @param {string} params.id 发货单id
 * @param {string} params.waybillNo 物流单号
 * @param {string} params.logisCompId 快递公司id
 * @param {string} params.newFlag 是否新风格，1是；1.2.1版本后前端务必加上
 */
spsales.getSalesDeliverDetail = async function (params = {}) {
    params = Object.assign({ newFlag: 1 }, params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-getSalesDeliverDetail', ...params });
};

/**
 * 订单的发货单列表
 * @param {object} params
 * @param {string} params.sellerId 卖家id
 * @param {string} params.salesBillId 销售单id
 */
spsales.getPurDeliverList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-getPurDeliverList', ...params });
};

/**
 * 查看发货单详情
 * @param {object} params
 * @param {string} params.sellerId 卖家id
 * @param {string} params.id 发货单id
 * @param {string} params.waybillNo 物流单号
 * @param {string} params.logisCompId 快递公司id
 * @param {string} params.newFlag 是否新风格，1是；1.2.1版本后前端务必加上
 */
spsales.getPurDeliverDetail = async function (params = {}) {
    params = Object.assign({ newFlag: 1 }, params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-deliverBill-getPurDeliverDetail', ...params });
};

/**
 * 卖家web订单明细导出
 * @param {object} params
 * @param {string} params.startTime  是  开始时间, 格式: yyyy-MM-dd
 * @param {string} params.endTime  是  结束时间, 格式: yyyy-MM-dd
 */
spsales.exportBillGroupByDetail = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-salesDetail-exportBillGroupByDetail', ...params });
};

/**
 * 卖家订单部分发货详情
 * @param {object} params
 * @param {string} params.id 订单id
 */
spsales.salesbillSendDetail = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-salesDetail-billSendDetail', ...params });
};

/**
 * 买家部分发货订单详情
 * @param {object} params
 * @param {string} params.id 订单id
 */
spsales.purbillSendDetail = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purDetail-billSendDetail', ...params });
};

/**
 * 线下订单更新定金
 * @param {object} params
 * @param {string} params.id 采购单id
 * @param {string} params.deposit 定金
 */
spsales.offlineBillSetDeposit = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['id']), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spsales-salesBill-extPropsSet', ...params });
};