const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const dis = module.exports = {
    url: ssCaps.spbUrl,
};


/**
 * 分销商 点击分销中心校验
 * @description 申请状态  0 不存在，1 审核通过（启用）,2 申请中 , 3 拒绝
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家租户id
 */
dis.getDisPartnerCustFlag = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-getCustFlag', ...params });
};

/**
 * 申请成为分销商
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} [params.realName] 姓名，新用户申请时必传
 * @param {string} [params.mobile] 电话，新用户申请时必传
 * @param {string} [params.invCode] 新用户申请时不传，通过邀请码申请的必传
 */
dis.applyDistribute = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-applyDistribute', ...params });
};

/**
 * 买家切换分销商
 * @param {object} params
 * @param {string} params.buyerSessionId 买家sessionId
 * @param {string} params.sellerTenantId 卖家tid
 */
dis.switchDistributionSeller = async function (params = {}) {
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-ugr-wxApp-switchDistributionSeller', ...params });
};

/**
 * 分销商 分销中心
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disId 分销商id
 */
dis.getDisStatisticsData = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-getDisStatisticsData', ...params });
};

/**
 * 分销商 提现记录
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disId 分销商id
 * @param {string} params.flag 申请状态，不传默认查全部：0 待审核，1 已打款，2 待打款，3 拒绝
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.getDisWithdrawDetailList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisWithdraw-getDisWithdrawDetailList', ...params });
};

/**
 * 分销商 申请提现
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disId 分销商id
 * @param {string} params.applyMoney 申请提现金额
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.applyWithdraw = async function (params = {}) {
    return common.get(this.url, { apiKey: 'ec-dis-ssDisWithdraw-applyWithdraw', ...params });
};

/**
 * 分销商 查询账户
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disId 分销商id
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.getAccount = async function (params = {}) {
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartnerAccount-getAccount', ...params });
};

/**
 * 分销商 我的客户列表
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disPartnerId 分销商id
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.getMyCustList = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-myCustList', ...params });
};

/**
 * 分销商 查看订单
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} params.disPartnerId 分销商id
 * @param {string} params.billType 付款状态 1待付款, 2已付款, 3已完成 4退货退款 5线下支付 默认不传
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.findDisOrders = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-dis-ssDisSaleBill-findDisOrders', ...params });
};

/**
 * 分销商 订单列表页面获取分销总价
 * @description 需要切换分销商登录
 * @param {object} params
 * @param {string} params.disPartnerId 分销商id
 * @param {string} params._tid 卖家租户id
 * @param {string} params._cid 卖家集群id
 */
dis.getTotalBrokerage = async function (params = {}) {
    return common.post(this.url, { apiKey: 'ec-dis-ssDisSaleBill-getTotalBrokerage', ...params });
};

/**
 * 卖家 生成邀请码
 * @param {object} params
 */
dis.invitationCodeBuilder = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisInvCode-invitationCodeBuilder', ...params });
};

/**
 * 卖家 修改分销商信息
 * @param {object} params
 * @param {object} params.id 分销商id
 * @param {object} params.flag 0：停用，1，正常
 * @param {object} params.realName 真实姓名
 * @param {object} params.mobile 	电话号码
 * @param {object} params.tag  	标签
 * @param {object} params.groupName 	分组名称
 * @param {object} params.groupId 		分组id
 */
dis.updateDisInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-updateDisInfo', ...params });
};

/**
 * 卖家 审核分销申请
 * @param {object} params
 * @param {string} params.disId 分销商id
 * @param {string} params.auditFlag 状态 0：待审核，1：审核通过（启用）3：(审核不通过)拒绝
 */
dis.verifyDisApply = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-verifyDisApply', ...params });
};

/**
 * 卖家 提现申请列表
 * @param {object} params
 * @param {string} params.flag 0：待审核，1：已打款，2：待打款，3：拒绝’
 * @param {string} params.orderBy 排序字段
 * @param {string} params.orderByDesc 是否倒序。(true/false，默认为false)
 */
dis.getWithdrawApplyList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisWithdraw-getWithdrawApplyList', ...params });
};

/**
 * 卖家 提现申请审批
 * @param {object} params
 * @param {string} params.id 提现主键id
 * @param {string} params.flag 申请状态 0：待审核，1：已打款，2：待打款，3：拒绝
                               目前只允许：待审核->拒绝；待审核->待打款；待打款->已打款；三种状态变更方式
 */
dis.apprWithdraw = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisWithdraw-apprWithdraw', ...params });
};

/**
 * 卖家 查询分销商列表
 * @param {object} params
 * @param {string} params.auditFlag 默认 0 审核状态0：待审核，1：审核通过，3：拒绝
 * @param {string} params.orderBy 排序字段
 * @param {string} params.orderByDesc 是否倒序。(true/false，默认为false)
 */
dis.listDisPartner = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-dis-ssDisPartner-listDisPartner', ...params });
};

/**
 * 卖家 查询分销商详情
 * @param {object} params
 * @param {string} params.disId 分销商id
 */
dis.disPartnerDetail = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-disPartnerDetail', ...params });
};

/**
 * 卖家 查询推广商品详情
 * @param {object} params
 * @param {string} params.spuId 商品spuid
 * @param {string} params.disId 分销商id
 */
dis.disPartnerSpuDetail = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-disPartner-spu-getDisPartnerSpuDetail', ...params });
};

/**
 * 卖家 新增/编辑分组信息
 * @param {object} params
 * @param {number} params.groupId
 * @param {string} params.groupName 分组名称
 * @param {list} params.disIds 分销商id列表
 */
dis.saveDisPartnerGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerGroup-saveDisPartnerGroup', ...params });
};

/**
 * 获取分组列表
 * @param {object} params
 * @param {number} params.isRelateLevel 
 */
dis.findDisPartnerGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerGroup-findDisPartnerGroup', ...params });
};

/**
 * 获取分组详情
 * @param {object} params
 * @param {number} params.groupId
 */
dis.disGroupDetail = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerGroup-getdisGroupDetail', ...params });
};


/**
 * 卖家 修改推广员对应商品信息（支持批量）
 * @param {object} params
 * @param {object} params.disSpus
 * @param {object} params.disSpus.disPartnerId	
 * @param {object} params.disSpus.brokerageRate
 * @param {object} params.disSpus.lockFlag
 * @param {object} params.disSpus.spuId
 */
dis.updateDisSpu = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-spu-updateDisSpu', ...params });
};

/**
 * 获取推广商品列表
 * @param {object} params
 */
dis.findDisPartnerSpu = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-disPartner-spu-findDisPartnerSpu', ...params });
};

/**
 * 保存推广等级
 * @param {object} params
 * @param {object} params.disLevel
 * @param {object} params.disLevel.levelName
 * @param {object} params.disLevel.groupName
 * @param {object} params.disLevel.groupId
 * @param {object} params.disLevel.brokerageRate
 */
dis.saveDisPartnerLevel = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerLevel-saveDisPartnerLevel', ...params });
};

/**
 * 删除分组
 * @param {object} params
 * @param {object} params.groupId
 */
dis.delDisPartnerGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerGroup-delDisPartnerGroup', ...params });
};

/**
 * 保存推广自定义库存
 * @param {object} params
 * @param {object} params.spuId
 * @param {object} params.skus
 * @param {object} params.skus.id
 * @param {object} params.skus.skuId
 * @param {object} params.skus.invNum
 */
dis.saveDisSkus = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ss-market-spu-saveDisSkus', ...params });
};

/**
 * 获取推广员等级列表
 * @param {object} params
 */
dis.findDisPartnerLevel = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartnerLevel-findDisPartnerLevel', ...params });
};

/**
 * 获取商品库存信息
 * @param {object} params
 * @param {object} params.spuId
 */
dis.findDisSkus = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ss-market-spu-findDisSkus', ...params });
};

/**
 * 修改商品各等级佣金比例（支持批量）
 * @param {object} params
 * @param {object} params.spuIds
 * @param {object} params.levels
 * @param {object} params.levels.id
 * @param {object} params.levels.brokerageRate
 */
dis.updateSpuLevelRate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-spu-updateSpuLevelRate', ...params });
};

/**
 * 开启推广官等级后调用
 * (無請求參數)
 */
dis.createDisMarket = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-createDisMarket', ...params });
};

/**
 * 删除分销商
 * @param {object} params
 * @param {object} params.id 推广商id
 */
dis.delDisPartner = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-delDisPartner', ...params });
};

/**
 * 关闭等级修改默认佣金比例后调用
 * @param {object} params
 * 无请求参数
 */
dis.delDisPartner = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-dis-ssDisPartner-initDefaultBroRate', ...params });
};

