'use strict';
const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');

let ssbi = module.exports = {
    url: ssCaps.spbUrl,
};

/**
 * 流量趋势统计接口
 * @param {object} params 
 * @param {string} params.dateType 类型：0:7天，1:30天，2：90天 (不传默认7天)
 */
ssbi.getFlowTrendStat = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssbi-ssShopDailyReport-getFlowTrendStat', ...params });
};

/**
 * 每日每月流量统计接口
 * @param {object} params 
 * @param {string} params.dateType 类型：0:每日，1:每月 (不传默认每日)
 */
ssbi.getFlowStatByType = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssbi-ssShopDailyReport-getFlowStatByType', ...params });
};

/**
 * 交易趋势统计接口
 * @param {object} params 
 * @param {string} params.dateType 类型：0:7天，1:30天，2：90天 (不传默认7天)
 */
ssbi.getTradeTrendStat = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssbi-ssShopDailyReport-getTradeTrendStat', ...params });
};

/**
 * 每日每月交易统计接口
 * @param {object} params 
 * @param {string} params.dateType 类型：0:每日，1:每月 (不传默认每日)
 */
ssbi.getTradeStatByType = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssbi-ssShopDailyReport-getTradeStatByType', ...params });
};