const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let ssIM = module.exports = {};


/**
 * 查询指定门店所有客服
 * @param {object} params 
 * @param {string} params.sellerId 卖家租户id
 */
ssIM.findAllAcctsInShop = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spgUrl, { apiKey: 'ec-cust-spNimAccount-findAllAcctsInShop', ...params });
};

/**
 * 转接其他客服聊天
 * @param {object} params 
 * @param {string} params.salesUnitId 客服单元id,从群组扩展信息中获得:csUnitId
 * @param {string} params.teamId 群id
 * @param {string} params.salesCustSvcerIdTo 指定的客服id，卖家端助手重启聊天时，传自己的id，买家端不传
 * @param {string} params.salesCustSvcerIdFrom 指定的客服id，卖家端助手重启聊天时，传自己的id，买家端不传
 */
ssIM.transferConv = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spcsb-nimCustSvcComb-transferConv', ...params });
};

/**
 * 客服主动联系客户IM
 * @description 买家向卖家发起沟通，调用卖家单元接口，需客服登录会话
 * @param {object} params 
 * @param {string} params.buyerUserId 买家用户ID
 * @param {string} params.buyerId 买家租户ID（当buyerUserId获取不到，则传买家租户id,服务端进行查找一个用户联系）
 */
ssIM.startConvSales = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spcsb-nimCustSvcComb-startConvSales', ...params });
};

/**
 * 同步店铺客服
 * @description 
 * 1. 同步店铺客服（内部:开发、测试人员使用）: 重新同步店铺员工，对应客服信息（创建、有效、无效）
 * 2. 请求参数和返回结果数据对应关系：tenantId ->fromAccount,mobilesTo->tMembers
 * @param {object} params 
 * @param {string} params.unitIds 单元id，多个用逗号间隔
 */
ssIM.syncSpServiceUser = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spcust-spServiceUser-syncSpServiceUser', ...params });
};

/**
 * 更新某个机器人的建群数量
 * @description 更新某个机器人的建群数量(解决某个机器人建群数量不对的问题)
 * @param {object} params 
 * @param {string} params.accid 用户IM账户accid
 */
ssIM.updateCreateTeamCount = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-cust-spNimAccount-updateCreateTeamCount', ...params });
};

/**
 * 根据userId或者手机号获取网易云信账户信息
 * @description sp_nim_account
 * @param {object} params 
 * @param {string} params.userIds 用户id
 * @param {string} params.mobiles 手机号，用户id没有时，手机号必传
 */
ssIM.getNimAccountInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spgUrl, { apiKey: 'ec-cust-spNimAccount-getNimAccountInfo', ...params });
};

/**
 * 手动停止服务记录
 * @description 
 * 1. 内部调用 
 * 2. 单元区-卖家 sp_service_record
 * 3. flag 服务状态，0:正在服务，1:服务完成  从1->0
 * @param {object} params 
 * @param {string} params.tenantId 卖家租户id
 * @param {string} params.id 服务记录id
 * @param {string} params.custId 客户id，如果服务记录id不传，custId必传
 */
ssIM.terminateServiceRecord = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spcsb-serviceRecord-terminateServiceRecord', ...params });
};