const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const accounting = module.exports = {
    url: 'http://106.12.21.201:6001/spb/api.do',
};

/**
 * 
 * @description
 * @param {object} params
 * @param {string} params.result 
 * @param {string} params.transCode 
 */
accounting.receiveWithdrawResult = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-accounting-receiveWithdrawResult', ...params });
};

/**
 * 
 * @description
 * @param {object} params
 * @param {string} params.result 
 * @param {string} params.transCode 
 */
accounting.receiveRecordedResult = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-accounting-receiveRecordedResult', ...params });
};