'use strict';
const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const spDresb = require('../../sp/biz_server/spdresb');
const dateHandler = require('../../../lib/dateHandler');
const numUtils = require('number-precision')

let sscoupb = module.exports = {
    url: ssCaps.spbUrl,
};

//优惠券-买家

/**
 * 按门店获取订单最大优惠
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家tid
 * @param {string} params.totalSum 总金额
 * @param {string} params.applicableSpus [{"spuId":39769,"spuPrice":40}]
 */
sscoupb.findFavorableCouponByShop = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purCoupon-findFavorableCouponByShop', ...params });
};

/**
 * 查看我的优惠券详情
 * @param {object} params
 * @param {string} params.couponId 
 * @param {string} params.tenantId 店铺的租户id
 */
sscoupb.getCouponsDetails = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sscoupb-mycoupons-get', ...params });
};

/**
 * 可用优惠券列表
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家tid
 * @param {string} params.totalSum 总金额
 * @param {string} params.applicableSpus [{"spuId":39769,"spuPrice":40}]
 */
sscoupb.findUseableCouponsByShop = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purCoupon-findUseableCouponsByShop', ...params });
};

/**
 * 领券
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 * @param {string} params.coupons [{"couponId":1117,"receiveChannelType":1}] 1 领券中心, 2 卖家推送
 */
sscoupb.receiveCoupon = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-mycoupons-receive', ...params });
};

/**
 * 获取商品可用优惠券列表
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 * @param {string} params.spuId 商品id
 */
sscoupb.findValidCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-findValidCoupons', ...params });
};

/**
 * 领券中心
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
sscoupb.findShopCoupons = async function (params) {
    params = Object.assign(format.packJsonParam(params), Object.assign(ssCaps.spbDefParams(), { pageSize: 0 }));
    return common.get(this.url, { apiKey: 'ec-sscoupb-couponsCenter-findShopCoupons', ...params });
};

/**
 * 我的优惠券列表
 * @param {object} params
 * @param {string} params.sellerTenantId 卖家租户id
 * @param {string} params.cardType 卡券大类： 0-满折券 1-满减券
 * @param {string} params.flags 卡券状态, 以逗号隔开：0-失效(过期) 1-有效 , 2支付中, 3-已消费
 * @param {string} params.orderByDesc 是否倒序 “true”, “false”
 * @param {string} params.orderBy 排序的字段 ,比如receiveDate, endDate, consumeDate 
 */
sscoupb.myCouponList = async function (params) {
    params = Object.assign(format.packJsonParam(params), Object.assign(ssCaps.spbDefParams(), { pageSize: 0 }));
    return common.get(this.url, { apiKey: 'ec-sscoupb-myCoupons-findCoupons', ...params });
};

/**
 * 我的卡券数量
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
sscoupb.myCouponCount = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-mycoupons-count', ...params });
};


//优惠券-卖家

/**
 * 删除
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.deleteCoupon = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-delete', ...params });
};

/**
 * 结束优惠券
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.endCoupon = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-end', ...params });
};

/**
 * 优惠券效果数据
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.getFullEffection = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-effect-getFullEffection', ...params });
};

/**
 * 效果数据-商品销量
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.getFullSpusSales = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-spuSales-getFullSpusSales', ...params });
};

/**
 * 优惠券详情
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.couponDetails = async function (params) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-get', ...params });
};

/**
 * 优惠券详情-货品
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 */
sscoupb.couponDetailsSpus = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-getCheckedSpus', ...params });
};

/**
 * 修改发放总量
 * @param {object} params
 * @param {string} params.couponId 优惠券id
 * @param {string} params.totalQuantity 发放总量 要大于等于发放量
 */
sscoupb.updateCoupon = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-update', ...params });
};

/**
 * 派发
 * @param {object} params
 */
sscoupb.sendToCustomer = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-sendToCustomer', ...params });
};

/**
 * 获取客户列表
 * @param {object} params
 */
sscoupb.getCustomerList = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-ssMallMember-listMember', ...params });
};

/**
 * 获取客户列表部分数据"custs"
 * @param {object} params
 * @param {string} params.number 要发送的客户数量
 */
sscoupb.getCustomerCusts = async function (params) {
    const customerList = await sscoupb.getCustomerList(params);
    // console.log(`customerList=${JSON.stringify(customerList)}`);
    const customerRows = customerList.result.data.rows;
    params.number = params.number > customerRows.length ? customerRows.length : params.number;
    params = Object.assign({
        number: customerRows.length
    }, params);
    let custs = [];
    for (let i = 0; i < params.number; i++) {
        let cust = { "custUserId": customerRows[i].custUserId, "custTenantId": customerRows[i].custTenantId, "name": customerRows[i].name, "nickName": customerRows[i].custUserId, "clusterCode": customerRows[i].clusterCode };
        custs.push(cust);
        cust = [];
    }
    // console.log(custs);
    return custs;
};

/**
 * 查看优惠券列表
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 * @param {string} params.flag 卡券状态 0默认状态, 未开始, 1:进行中 2 已结束
 */
sscoupb.getCouponList = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-list', ...params });
};

/**
 * 创建优惠券
 * @param {object} params
 */
sscoupb.createCoupon = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sscoupb-cardCoupons-save', ...params });
};

/**
 * 构造优惠券数据
 * @param {object} params
 * @param {number} params.cardType 0-满折券 1-满减券
 * @param {number} params.effectPeriodType 用券时间类型, 0: 固定时间段, 1:领取当天起计算天数, 2:领取次日起计算天数
 * @param {number} params.applyDresType 0: 全部商品, 1:部分商品可用, 2:部分商品不可用
 * @param {number} params.getLimit 领取上限,0表示不限次数
 * @param {boolean} params.showInCenter 是否在领券中心显示
 * @param {boolean} params.useTogether 是否与其他优惠同享
 * @param {number} fulfilValue 订单金额  0.01-不限制金额
 * @param {boolean} running 创建进行中的优惠券
 */
sscoupb.setCouponData = async function (params, fulfilValue = 0.01, running = true) {
    const title = common.getRandomChineseStr(4);
    const rem = '测试优惠券-' + title;
    const totalQuantity = common.getRandomNum(1, 50);
    let paramsPart = {
        title: title,
        cardType: 0,
        rem: rem,
        totalQuantity: totalQuantity,
        showInCenter: true, //是否显示在领券中心 前端显示更名为 是否公开优惠券, 如果设置不公开优惠券, 则该优惠券不会出现在领券中心/商品详情页
        // useTogether: false, //20190827去掉是否与其他优惠同享字段，在活动中控制是否可使用优惠券
        effectPeriodType: 0,
        getLimit: 0,
        applyDresType: 0,
        unitId: LOGINDATA.unitId,
        receiverLimitType: 0,
        validPrice: ''
    }
    params = Object.assign(paramsPart, params);

    let rule;
    if (params.cardType == 0) {
        const execNum = common.getRandomNum(1, 9, 1);
        const topValue = common.getRandomNum(1, 1000, 2);
        rule = { execNum: execNum, fulfilValue: fulfilValue, topValue: topValue };
    } else if (params.cardType == 1) {
        if (fulfilValue == 0.01) {
            const execNum = common.getRandomNum(1, 100, 2);
            rule = { execNum: execNum, fulfilValue: fulfilValue };
        } else {
            const execNum = common.getRandomNum(0.01, fulfilValue - 0.01, 2);
            rule = { execNum: execNum, fulfilValue: fulfilValue };
        }

    } else {
        console.log('cardType应为0或1')
    }
    params = Object.assign(paramsPart, { rule: rule });

    //有效期0: 固定时间段, 1:领取当天起计算天数, 2:领取次日起计算天数
    let time;
    if (params.effectPeriodType == 0) {
        if (running == true) {    //当天
            const currentTime = dateHandler.getCurrentDate();
            time = { effectiveDate: currentTime + ' 00:00:00', expiresDate: currentTime + ' 23:59:59' };
        } else {  //明天
            const tomorrow = dateHandler.getDateString([0, 0, +1]);
            time = { effectiveDate: tomorrow + ' 00:00:00', expiresDate: tomorrow + ' 23:59:59' };
        }
    } else if (params.effectPeriodType == 1 || params.effectPeriodType == 2) {
        const receiveEffectPeriod = common.getRandomNum(1, 30);
        time = { receiveEffectPeriod: receiveEffectPeriod, receiveEffectPeriodUnit: 2 };
    }
    params = Object.assign(paramsPart, time);

    //部分商品可用，不可用
    let spus = [];
    if (params.applyDresType == 1 || params.applyDresType == 2) {
        const findSellerSpuList = await spDresb.findSellerSpuList({ flags: 1 });
        const rows = findSellerSpuList.result.data.rows;
        for (let i = 0; i < rows.length; i++) {
            spus.push(rows[i].id);
        }
        // 数组随机取值
        const spusNum = common.getRandomNum(1, spus.length);
        let spusRandom = [];
        for (let i = 0; i < spusNum; i++) {
            let random = common.getRandomNum(0, spus.length - 1);
            spusRandom.push(spus[random]);
            spus.splice(random, 1);
        }
        // console.log(spusRandom);
        params = Object.assign(paramsPart, { spus: spusRandom });
    }
    // console.log(params);
    return params;
}

/**
 * 拼接采购订单参数
 * @description ec-sppur-purBill-createFull
 * @param {object} params 
 * @param {object} params.styleInfo 款号信息
 * @param {object} params.couponInfo 优惠券信息
 * @param {object} params.evalShipFeeInfo 运费信息
 * @param {object} params.addressInfo 地址信息
 */
sscoupb.purJson = function (params, num = 1, payKind = 1) {
    // ({ styleInfo: getFullForBuyer, couponInfo: findFavorableCoupon, evalShipFeeInfo: evalShipFee});
    const [styleInfo, trader] = [params.styleInfo.result.data, params.styleInfo.params];
    let docId;
    if (typeof (styleInfo.spu.ecCaption.docHeader[0]) == "undefined") {
        docId = '';
    } else {
        docId = styleInfo.spu.ecCaption.docHeader[0].docId;
    }
    const detailSpu = {
        spuId: styleInfo.spu.id,
        spuTitle: styleInfo.spu.title,
        spuCode: styleInfo.spu.code,
        spuDocId: docId  //这里要传，不然导致下订单之后订单款号没有图片
    };
    let main = {
        sellerId: trader._tid,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: params.evalShipFeeInfo.result.data.fees[0].fee,//运费。若为到付，则为0
        // shipFeeMoney: 10,
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: payKind,//付款方式。1 预付，2 货到付款，3 线下支付
        couponsIds: params.couponInfo.couponId, //params.couponInfo.result.data[trader._tid].couponId,//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        mineCouponsIds: params.couponInfo.mineCouponId,//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: params.addressInfo.result.data.recInfo.id,//收货地址ID 取当前用户默认地址
    }, details = [];
    common.randomSort(styleInfo.skus).slice(0, 1).forEach((sku) => {
        const detail = Object.assign(new sscoupb.GetDetail(sku, num), detailSpu);
        main.originalMoney += detail.originalPrice;
        main.money += detail.money;
        main.totalNum += detail.num;
        // main.shopCoupsMoney += detail.num * params.couponInfo.result.data[trader._tid].maxDiscountSum;
        details.push(detail);
    });
    main.shopCoupsMoney = params.couponInfo.discountSum;
    main.money = main.money.toFixed(2);
    // main.money = numUtils.strip(main.money, 2);
    main.totalMoney = (main.money - main.shopCoupsMoney + main.shipFeeMoney).toFixed(2);
    return { orders: [{ main, details }] };
};

sscoupb.GetDetail = function (sku, num) {
    this.skuId = sku.id;
    this.spec1 = sku.spec1;
    this.spec1Name = sku.ecCaption.spec1 || '';
    this.spec2 = sku.spec2;
    this.spec2Name = sku.ecCaption.spec2 || '';
    this.spec3 = sku.spec3;
    this.spec3Name = sku.ecCaption.spec3 || '';
    // this.num = common.getRandomNum(1, 10);
    this.num = num;
    this.originalPrice = sku.pubPrice;
    this.price = this.originalPrice;
    this.money = common.mul(this.num, this.price);
    this.rem = '';
};