'use strict';
const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

const spdresup = module.exports = {
    url: ssCaps.spbUrl,
};

//店铺装修

/**
 * 保存店铺装修
 * @param {object} params.content 装修json
 */
spdresup.savePageFragment = async function (params) {
    //params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spdresup-ssPageFragment-saveFull', ...params });
};

/**
 * 装修预览
 * @param {object} params
 */
spdresup.getPageFragment = async function (params = {}) {
    //params = format.packJsonParam(params);
    params._cid = LOGINDATA.clusterCode;
    params._tid = LOGINDATA.tenantId;
    return common.get(this.url, { apiKey: 'ec-spdresup-ssPageFragment-getPageFragment', ...params });
};

/**
 * 默认模板查询
 * @param {object} params
 */
spdresup.getDefPageFragmentList = async function (params = {}) {
    params = Object.assign({ templateVer: 3 }, params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroup-findDefaultPageList', ...params });
};

/**
 * 自定义商品详情查看
 * @param {object} params
 */
spdresup.getGoodsDetail = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresup-ssPageFragment-getGoodsDetail', ...params });
};

/**
 * 保存自定义商品详情
 * @param {object} params
 * @param {string} params.goodsFabricStatus 商品信息详情面料 0 关 1开 下同
 * @param {string} params.goodsBrandStatus 商品信息详情品牌
 * @param {string} params.goodsSeasonStatus 商品信息详情季节
 * @param {string} params.goodsInvStatus 商品信息详情库存
 */
spdresup.saveGoodsDetail = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresup-ssPageFragment-saveGoodsDetail', ...params });
};

/**
 * 获取活动商品列表
 * @param {object} params
 * @param {number} params.id 活动id
 */
spdresup.listSelectedSpus = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresup-ssPageFragment-listSelectedSpus', ...params });
};

/**
 * 获取活动分享页面数据
 * @description 
 * 1. spb.sc_page_fragment
 * 2. 卖家会话
 * @param {object} params
 * @param {string} params.activityId 活动id
 * @param {string} params.shopId 店铺id
 */
spdresup.getSharePageDataForActivity = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresup-ssPageFragment-getSharePageDataForActivity', ...params });
};
