const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spdresb = module.exports = {
    url: ssCaps.spbUrl,
};

//卖家


/**
 * 更新sku售罄状态接口
 * @param {Object} params
 * @param {string} params.validIds 售罄启用的sku的id 例如 “21,22,23”
 * @param {string} params.inValidIds 不启用的sku的id 例如 “21,22,23”
 */
spdresb.updateSkuSoldOutFlag = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSku-updateSkuSoldOutFlag', ...params });
};

/**
 * 卖家分类列表树（app）
 * @param {Object} params
 * @param {string} [params.nameLike] 名字模糊，没有默认查询全部
 * @param {string} [params.parentId] 上次类别id,没有默认0，就是第一级
 * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
 */
spdresb.findDresSpuClassTree = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-findDresSpuClassTree', ...params });
};

/**
 * 新增类目（app）
 * @param {Object} params
 * @param {string} params.name 分类名字
 * @param {string} [params.classDocId] 分类图片
 * @param {string} params.parentId 上次类别id，一级类别传0，二级传上级的(共3级)
 * @param {string} params.level 当前层级，一级类别传1，二级传2
 * @param {string} [params.spuIds] 需要关联的spuIds，多个以逗号隔开，没有就不传
 * @param {string} [params.showOrder] 排序号，预留，新增的分类，默认9999
 */
spdresb.saveFullClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-saveFullClass', ...params });
};

/**
 * 编辑类目（app）
 * @param {Object} params
 * @param {string} params.id 分类id
 * @param {string} params.name 分类名字
 * @param {string} [params.classDocId] 分类图片，不传不修改
 * @param {string} [params.showOrder] 排序号，预留，不传不修改
 */
spdresb.updateFullClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-updateFullClass', ...params });
};

/**
 * 编辑类目顺序 app
 * @param {Object} params
 * @param {Object[]} params.showOrders 分类名字(list 结构)
 * @param {string} params.showOrders[].id 分类id
 * @param {string} params.showOrders[].showOrder 分类顺序值
 */
spdresb.updateClassShowOrder = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-updateClassShowOrder', ...params });
};

/**
 * 删除作废类目 app接口
 * @param {Object} params
 * @param {string} params.id 分类id
 */
spdresb.deleteFullClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-deleteFullClass', ...params });
};

/**
 * 判断类目相关状态 app
 * @description 返回值:0不可以（type为3跳商品列表） 1可以（type为3跳分类列表） 2预留
 * @param {Object} params
 * @param {string} params.id 分类id
 * @param {string} params.typeId 1 代表能否新增， 2 代表能否删除(有子类不能删除，需先删除子类) 3 代表下属跳转 4 能否关联货品
 */
spdresb.judgeFullClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-judgeFullClass', ...params });
};

/**
 * 新增分类到多个商品中（app）
 * @param {Object} params
 * @param {string} params.classId 分类id
 * @param {string} params.dresSpuIds 商品ids，多个以逗号间隔
 */
spdresb.addSpuForClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-addSpuForClass', ...params });
};

/**
 * 新增多个分类到商品中（app）
 * @param {Object} params
 * @param {string} params.spuId 商品id
 * @param {string} params.classIds 分类ids，多个以逗号间隔
 */
spdresb.addClassForSpu = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-addClassForSpu', ...params });
};

/** 
 * 对买家隐藏分类类目
 * @param {Object} params
 * @param {string} params.classId 分类id
 * @param {string} params.hideFlag 隐藏状态位 0 不隐藏 1隐藏
 */
spdresb.hideDresClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-hideDresClass', ...params });
};

/**
 * 更新商品运费模板
 * @param {Object} params
 * @param {string} params.id 主键id
 * @param {string} params.templateId 模板id
 */
spdresb.updateFreeTemplate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-updateFreeTemplate', ...params });
};

/**
 * 批量更新商品运费模板
 * @param {Object} params
 * @param {string} params.spuIds 商品id 多个逗号分隔
 * @param {string} params.templateId 模板id
 */
spdresb.batchUpdateFreeTemplate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-batchUpdateFreeTemplate', ...params });
};

/**
 * 一键更新商品运费模板
 * @param {string} params.templateId 模板id
 */
spdresb.quantityUpdateFreeTemplate = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-quantityUpdateFreeTemplate', ...params });
};

/**
 * 批量设置起批量、起订数
 * @param {object} params
 * @param {string} params.spuIds 需要设置的spuIds，逗号隔开， 如 1,2,3
 * @param {string} params.priceTypes 需要设置的价格类型, 逗号隔开， 如 1,2,3，不传默认清空
 * @param {string} params.typeId 
 * @param {string} params.buyNums
 */
spdresb.batchSetPricesSalesParams = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-batchSetPricesSalesParams', ...params });
};

/**
 * 新增货品（独立app）
 * @param {object} params
 */
spdresb.saveFullByApp = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-saveFullByApp', ...params });
};

/**
 * 商品分组列表接口
 * @param {object} params
 */
spdresb.listGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroup-listGroup', ...params });
};

/**
 * 添加商品分组接口
 * @param {object} params
 * @param {string} params.groupName 名字
 * @param {string} params.dresSpuIds 货品id集合 61740,61744
 * @param {string} params.showPlace 展示位置 1货品详情 2购物车 3我的
 */
spdresb.addGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroup-addGroup', ...params });
};

/**
 * 删除商品分组接口
 * @param {object} params
 * @param {string} params.groupId 分组id
 */
spdresb.deleteGroup = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroup-deleteGroup', ...params });
};

/**
 * 根据商品添加分组
 * @param {object} params
 * @param {string} params.dresSpuId 商品id
 * @param {string} params.dresSpuGroupIds 分组列表集合
 */
spdresb.addDresSpus = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroup-addDresSpus', ...params });
};

/**
 * 分组货品详情接口
 * @param {object} params
 * @param {string} params.id 分组id
 */
spdresb.listDresSpuGroupDetail = async function (params = {}) {
    // params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-listDresSpuGroupDetail', ...params });
};

/**
 * 分组详情接口
 * @param {object} params
 * @param {string} params.id 分组id
 */
spdresb.findDresSpuGroupInfo = async function (params = {}) {
    // params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-findDresSpuGroupInfo', ...params });
};

/**
 * 分组添加商品接口 
 * @description 没有用到，且功能和addGroup()重合
 * @param {object} params
 * @param {string} params.id 分组id
 * @param {string} params.dresSpuIds 货品id[,]分割
 */
spdresb.addDresSpuDetail = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-addDresSpuDetail', ...params });
};

/**
 * 更新分组展示位 
 * @description 没有用到
 * @param {object} params
 * @param {string} params.id 分组id
 * @param {string} params.showPlace 当前页，默认1
 */
spdresb.updateDresSpuGroupShowPlace = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-updateDresSpuGroupShowPlace', ...params });
};

/**
 * 下架的商品删除
 * @param {object} params
 * @param {Array} params.ids SPU ID列表 例如：{“ids”:[1,2,3,4,5,6]}
 */
spdresb.deleteOffMarket = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-deleteOffMarket', ...params });
};


/**
 * 获取价格list（app）
 * @description 获取价格管理list
 * @param {object} params jsonParam
 */
spdresb.getPriceTypeListInApp = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-price-getPriceTypeListInApp', ...params });
};

/**
 * 获取价格下拉选项值list
 * @deprecated 获取价格下拉选项值list
 * @param {object} params jsonParam
 */
spdresb.getPriceTypeSelectItemInApp = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-price-getPriceTypeSelectItemInApp', ...params });
};


//买家

/**
 * 买家分类列表树（小程序）
 * @description  默认按照showOrder升序排序
 * @param {Object} params
 * @param {string} params.sellerId 卖家id
 * @param {string} params.sellerUnitId 卖家单元unitId
 * @param {string} [params.nameLike]  名字模糊，没有默认查询全部
 * @param {string} [params.parentId]  上次类别id,没有默认0，就是第一级
 * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
 */
spdresb.findDresSpuClassTreeByBuyer = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-findDresSpuClassTreeByBuyer', ...params });
};

/**
 * 获取模板运费
 * @param {object} params
 */
spdresb.getOrderFreight = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-spuFree-getOrderFreight', ...params });
};

/**
 * 根据商品分组id获取商品列表
 * @param {object} params
 * @param {string} params.groupId 分组id
 * @param {string} params.buyerId 买家租户id，卖家端调用不传
 * @param {string} params.tenantId 卖家租户id，买家端调用必传
 * 
 */
spdresb.getSpuListByGroupId = async function (params = {}) {
    // params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuGroupDetail-getSpuListByGroupId', ...params });
};

/**
 * 查询商品分类
 * @param {object} params
 * @param {string} params.sellerUnitId 卖家单元id
 * @param {string} params.sellerId 卖家租户id
 */
spdresb.findDressClassByBuyer = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresClass-findDressClassByBuyer', ...params });
};

/**
 * 获取商品小视频
 * @param {object} params
 * @param {string} params.myPraiseFlag 获取我点赞的视频列表时传1，默认不传该字段时查全部
 * @param {string} params.sellerTenantId 卖家租户id
 */
spdresb.findDresFileList = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-findDresFileList', ...params });
};

/**
 * 批量调价 (新增)
 * @param {object} params
 * @param {string} params.spuIds 多个id逗号分割
 * @param {string} params.codeValue 价格类型
 * @param {string} params.preferentialMode 优惠方式 0 折扣价 1 优惠价
 * @param {string} params.preferentialPrice 优惠的价格
 */
spdresb.batchPreferentialPrice = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-batchPreferentialPrice', ...params });
};

/**
 * 获取价格字典值(新增)
 * @param {object} params
 */
spdresb.getPriceDict = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-getPriceDict', ...params });
};

/**
 * 根据skuIds批量查询卖家sku的实时信息
 * @description 卖家接口
 * @param {object} params
 * @param {string} params.buyerId 买家租户id
 */
spdresb.getSkusInfoInBatch = async function (params = {}) {
    params = format.packJsonParam(params, ['_cid', '_tid']);
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-getSkusInfoInBatch', ...params });
};

/**
 * 新增款号权限
 * @param {object} params
 * @param {string} params.authType 权限类型，0全部可见，1部分可见，2部分不可见（保留，暂时没用到）
 * @param {string} params.spuIds 商品编号,多个以逗号隔开（当商品类型为1时，这里是商品分组编号，暂时没用到）
 * @param {string} params.memberIds 客户编号,多个以逗号隔开（当客户类型为1时，这里是客户分组编号，暂时没用到）
 * @param {string} params.memberType 保留字段，客户类型，0单个客户，1客户分组，默认0
 * @param {string} params.startDate 权限生效日期
 * @param {string} params.endDate 权限结束日期
 */
spdresb.addAuth = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuAuth-addAuth', ...params });
};

/**
 * 货品权限详情接口
 * @param {object} params
 * @param {string} params.spuId 商品编号,多个以逗号隔开（当商品类型为1时，这里是商品分组编号，暂时没用到）
 */
spdresb.authInfo = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpuAuth-authInfo', ...params });
};

// 规格属性
// spb.dres_class_unit_prop

/**
 * 货品规格属性新增接口
 * @description 预设的名称含“颜色，面料，尺码，季节，风格”等不能修改，也不能被修改成，类别归属的规格属性不能修改，展示名同一分类下不能重复，重复名称的被禁用规格属性会新增报存会导致启用
 * @param {object} params
 * @param {string} [params.id] 根据id进行更新时传递，规格展示名，其他不允许更新
 * @param {string} [params.propsCode] 不传以caption的拼音值
 * @param {string} params.propCaption 规格名
 * @param {string} params.typeId 类型 1 spu规格 2 属性 3是单独的sku规格, 传id时这个不必传
 */
spdresb.saveSpecsFull = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-specs-saveFull', ...params });
};

/**
 * 货品规格属性列表
 * @description 不分页获取所有的
 * @param {object} params
 * @param {string} [params.propsCode] 规格code
 * @param {string} [params.propCaption] 规格名
 * @param {string} params.typeId 类型 1 spu规格 2 属性 3是单独的sku规格, 传id时这个不必传
 */
spdresb.findSpecsList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-specs-list', ...params });
};

/**
 * 启用（被禁用的）规格属性
 * @param {object} params
 * @param {string} params.id 规格属性id
 */
spdresb.ableSpecsProps = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-specs-ableProps', ...params });
};

/**
 * 禁用规格属性
 * @param {object} params
 * @param {string} params.id 规格属性id
 */
spdresb.disableSpecsProps = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-specs-disableProps', ...params });
};

/**
 * 视频转码优化
 * @param {object} params
 * @param {string} params.spuId 商品id
 * @param {stirng} params.docHeader 商品doc
 */
spdresb.translateCode = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresSpu-translateCode', ...params });
};

/**
 * @param {object} params
 * @param {string} params.classId 分类id
 * @param {string} params.spuIds 商品id，多个以逗号隔开
 * @param {string} params.typeId 0或不传:增量增加，1:增量减少
 */
spdresb.addSpuToClass = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spdresb-dresClass-addSpuForClassIncrement', ...params });
};