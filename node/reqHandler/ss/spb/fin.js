const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let fin = module.exports = {
    url: ssCaps.spbUrl
};

/**
 * 创建收款单
 * @description 卖家创建
 * @param {object} params
 * @param {object} params.payMoney 支付金额
 */
fin.createPay = async function (params = {}) {
    params.hashKey = `${Date.now()}${common.getRandomNumStr(5)}`;
    params = Object.assign(format.packJsonParam(params, ['bizType', 'bizOrderId']), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-ssfin-payBill-createPay', ...params });
};

/**
 * 获取流水总金额
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {string} params.endDate 结束日期
 * @param {string} params.ioFlag 0-收入；1-支出
 */
fin.getTotalFlowMoney = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcctFlowDaily-getTotalFlowMoney', ...params });
};

/**
 * 获取钱包首页数据
 * @param {object} params
 */
fin.getMoneyHomePage = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcct-getHomePageMoney', ...params });
};

/**
 * 按日期获取账户日结总金额
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {string} params.endDate 结束日期
 */
fin.getFinAcctDaily = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcctDaily-findFinAcctDaily', ...params });
};

/**
 * 获取账户流水
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {string} params.endDate 结束日期
 * @param {object} params.orderId bizOrderDetailId
 */
fin.getFinAcctFlow = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcctFlow-findFinAcctFlow', ...params });
};

/**
 * 获取财务流水日结概览
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {object} params.endDate 结束日期
 * @param {object} params.ioFlag 0-收入；1-支出
 * @param {object} params.dateType 1-日，2-月， 3-年
 */
fin.getFinAcctFlowDailyOverview = async function (params = {}) {
    params = Object.assign({ pageNo: 1, pageSize: 20 }, format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcctFlowDaily-findFinAcctFlowDailyOverview', ...params });
};

/**
 * 获取财务流水日结
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {object} params.endDate 结束日期
 * @param {object} params.ioFlag 0-收入；1-支出
 */
fin.getFinAcctFlowDaily = async function (params = {}) {
    params = Object.assign({ pageNo: 1, pageSize: 20 }, format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finAcctFlowDaily-findFinAcctFlowDaily', ...params });
};

/**
 * 获取提现记录
 * @param {object} params
 * @param {object} params.startDate 起始日期
 * @param {string} params.endDate 结束日期
 * @param {object} params.billNo 提现编号
 */
fin.getFinCashBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finCashBill-findFinCashBill', ...params });
};

/**
 * 获取提现详情
 * @param {object} params
 * @param {object} params.id 提现单id
 */
fin.getFinCashBillDetails = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-fin-finCashBill-getById', ...params });
};

/**
 * 提现
 * @param {object} params
 * @param {object} params.hashKey 
 * @param {object} params.acctId 账户ID ,要提现的账户
 * @param {object} params.money 提现金额
 */
fin.createFinCashBill = async function (params = {}) {
    params.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssfin-finCashBill-create', ...params });
};

/**
 * 重新提现
 * @param {object} params
 * @param {object} params.id 提现单id
 */
fin.afreshWithdraw = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-ssfin-finCashBill-afreshWithdraw', ...params });
};


/**
 * 账户额度变更
 * @description 单元区-卖家集群
 * @param {object} params
 * @param {object} params.acctType 账户类型：1301-直播时长，1302-短信余额，1303-快递余额
 */
fin.saveFinAcctFlowInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-fin-finAcct-saveFinAcctFlowInfo', ...params });
};

/**
 * 获取账户直播流水
 * @description 单元区-卖家集群
 * @param {object} params
 * @param {object} params.acctType 账户类型：1301-直播时长，1302-短信余额，1303-快递余额
 */
fin.findFinAcctFlowTypes = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-fin-finAcctFlow-findFinAcctFlowTypes', ...params });
};

/**
 * 获取直播账户信息
 * @description 单元区-卖家集群
 * @param {object} params
 * @param {object} params.acctType 账户类型：1301-直播时长，1302-短信余额，1303-快递余额
 */
fin.getAcctStatistics = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-fin-finAcct-getAcctStatistics', ...params });
};