'use strict';
const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const spDresb = require('../../sp/biz_server/spdresb');
const dateHandler = require('../../../lib/dateHandler');
const numUtils = require('number-precision')

let up = module.exports = {};

/**
 * 查询访客统计
 * @param {object} params
 */
up.getSellerCount = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-up-buyerVisitedLog-getSellerCount', ...params });
};

/**
 * 查询访客记录按天隔离
 * @param {object} params
 * @param {string} params.buyerNameLike 名字
 */
up.findSellerLogsByDay = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-up-buyerVisitedLog-findSellerLogsByDay', ...params });
};

/**
 * 查询访客记录
 * @param {object} params
 * @param {string} params.buyerNameLike 名字
 */
up.findSellerLogs = async function (params) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-up-buyerVisitedLog-findSellerLogs', ...params });
};

/**
 * 卖家查询买家访问商品信息
 */
up.sellerViewQueryByDate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-up-sellerView-queryByDate', ...params });
};

/**
 * 服务端分享商品海报
 * @description 已经不再使用
 * @param {object} params
 * @param {string} params.type 分享类型：2分销分享商品，3分享商品
 * @param {string} [params.disPartnerId] 分销商id；type=2时必填；type=3时不填
 * @param {string} params.shopId 门店id
 * @param {string} params.spuId 货品id
 * @param {string} params.spuPrice 这一版本先传价格
 * @param {string} [params.isHyaline] 默认true，不需要透明传false
 */
up.shareSku = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-up-ssShare-shareSku', ...params });
};