const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');

const spmdm = module.exports = {};

/**
 * 审核客户
 * @param {list} params
 * @param {object} userId  客户的租户id
 * @param {string} flag 状态,1允许访问，0禁止访问
 */
spmdm.auditMemberByUserId = async function (params, custTenantId = {}) {
    let param = Object.assign(format.packJsonParam(params), custTenantId, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-auditMemberByUserId', ...param });
};

/**
 * 保存店铺会员参数配置 (开通余额支付、线下支付)
 * @param {list} params
 * @param {object} custTenantId  客户的租户id
 * @param {string} params.code  参数代码, 余额支付:open_balance_payment, 欠款支付:open_debt_payment
 * @param {string} params.val 参数值, 1:开启, 0:关闭
 */
spmdm.saveMemberParams = async function (params, custTenantId = {}) {
    let param = Object.assign(format.packJsonParam(params), custTenantId, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-saveMemberParams', ...param });
};

/**
 * 申请访问
 * @param {object} params
 * @param {object} params.userId 买家id
 * @param {object} params.tenantId 卖家的租户id
 */
spmdm.applyForVisit = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-SsBuyerVisitApplyRecord-applyForVisit', ...params });
};

/**
 * 保存客户信息
 * @param {object} params
 * @param {object} params.tenantId 卖家的租户id
 */
spmdm.saveMemberInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-saveMemberInfo', ...params });
};

// /**
//  * 营业员邀请客户接口
//  * @description 接口已废弃
//  * @param {object} params
//  * @param {object} params.tenantId 卖家的租户id
//  * @param {object} params.sellerId 店员id
//  */
// spmdm.inviteCustomer = async function (params = {}) {
//     params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
//     return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-inviteCustomer', ...params });
// };

/**
 * 获取客户列表
 * @param {object} params
 */
spmdm.getMemberList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-listMember', ...params });
};

/**
 * 批量更新适用价格和状态
 * @param {object} params
 * @param {object} params.custTenantId 多个,分隔
 * @param {object} params.validPrice 价格类型
 * @param {object} params.flag 状态
 */
spmdm.batchUpdateMemberValidPriceAndFlag = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-batchUpdateMemberValidPriceAndFlag', ...params });
};

/**
 * 更新适用价格
 * @param {object} params
 * @param {object} params.custTenantId 多个,分隔
 * @param {object} params.validPrice 价格类型
 */
spmdm.updateMemberValidPrice = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-updateMemberValidPrice', ...params });
};

/**
 *联合更新客户信息数据
 * @param {object} params
 * @param {object} params.memberId 客户id
 * @param {object} params.validPrice 价格类型
 * @param {object} params.flag 状态
 */
spmdm.updateMemberUnion = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-updateMemberUnion', ...params });
};
/**
 * 统计当前账号下的绑定的客户统计信息
 * @param {object} params
 * @param {object} params.needAuth 是否需要权限控制
 */
spmdm.statisticsRelationMallMemberCount = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-statisticsRelationMallMemberCount', ...params });
};


/**
 * 修改备注名
 * @param {object} params
 * @param {object} params.userId 客户id
 * @param {object} params.rem 姓名备注
 */
spmdm.updateMemberRem = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-updateMemberRem', ...params });
};

/**
 * 根据id查询客户详细信息
 * @param {object} params
 * @param {object} params.userId 客户id
 */
spmdm.getMemberByUserId = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getMemberByUserId', ...params });
};

/**
 * 查询客户分组列表
 * @param {object} params
 */
spmdm.getMemberGroupList = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMemberGroup-listGroup', ...params });
};

/**
 * 添加客户分组
 * @param {object} params
 * @param {object} params.name 分组名称
 * @param {object} params.parentId 父分组id，没有不传
 * @param {object} params.memberIds 客户id，多个以逗号隔开
 */
spmdm.addMemberGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMemberGroup-addGroup', ...params });
};

/**
 * 更新客户分组
 * @param {object} params
 * @param {object} params.id 分组编号
 * @param {object} params.name 分组名称
 * @param {object} params.parentId 父分组id，没有不传
 * @param {object} params.memberIds 客户id，多个以逗号隔开
 */
spmdm.updateMemberGroup = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMemberGroup-updateGroup', ...params });
};

/**
 * 删除分组信息
 * @param {object} params
 * @param {object} params.groupId 分组id
 */
spmdm.deleteMemberGroup = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMemberGroup-deleteGroup', ...params });
};

/**
 * 根据姓名首字母分组显示客户列表
 * @param {object} params
 * @param {object} params.searchToken
 */
spmdm.getMemberListByNamePy = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-listMemberByNamePy', ...params });
};

/**
 * 查询一个分组下的客户列表
 * @param {object} params
 * @param {object} params.groupId 分组id
 */
spmdm.getMembersListFromGroup = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssGroupMember-listMembersFromGroup', ...params });
};

/**
 * 获取二维码
 * @param {object} params
 * @param {string} params._tid 卖家租户id
 * @param {string} params.type 0店员分享店铺；1纯店铺，2分销，默认0
 * @param {string} params.userId type=2时必传，分销商id；type=0时取登录会话用户id
 */
spmdm.getAppCodeForShopSeller = async function (params = {}) {
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getAppCodeForShopSeller', ...params });
};

//员工

/**
 * 员工列表
 * @param {object} params
 */
spmdm.getStaffList = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-listStaff', ...params });
};

/**
 * 新增/编辑员工
 * @param {object} params
 * @param {object} params.userName 姓名
 * @param {object} params.mobile 手机号
 * @param {object} params.roleId 编辑角色id
 * @param {object} params.rem 备注
 * @param {object} params.userId 用户id
 * @param {object} params.oldRoleId 原角色id
 */
spmdm.newOrEditStaff = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-bindOrEditStaff', ...params });
};

/**
 * 员工详情
 * @param {object} params
 * @param {object} params.userId 用户id
 * @param {object} params.roleId 角色id
 */
spmdm.getStaffInfo = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-getStaffInfo', ...params });
};

/**
 * 员工删除
 * @param {object} params
 * @param {object} params.userId 用户id
 * @param {object} params.roleId 角色id
 */
spmdm.deleteStaff = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-unBindStaff', ...params });
};

/**
 * 单个客户移交
 * @param {object} params
 * @param {string} params.userId 员工
 * @param {string} params.custUserId 客户
 */
spmdm.deliverStaffSingle = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-deliverStaffSingle', ...params });
};

/**
 * 员工客户移交
 * @param {object} params
 * @param {object} params.deliverUserId 被转移用户（客户减少者）
 * @param {object} params.userId 转移用户（客户增加者）
 */
spmdm.deliverStaff = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-deliverStaff', ...params });
};

/**员工单个客户移交
 * @param {object} params.userId 员工id
 * @param {object} params.custUserId 客户id
 */
spmdm.deliverStaffSingle = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-deliverStaffSingle', ...params });
};


/**
 * 管理员转让
 * @param {object} params
 * @param {object} params.transferUserId 管理员id
 * @param {object} params.userId 用户id
 */
spmdm.transferStaff = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-spStaffManager-transferStaff', ...params });
};

/**
 * 获取员工关联客户人数
 * @param {object} params
 * @param {string} params.userId 员工id，不传默认当前用户
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 */
spmdm.getClientCountForEmployee = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getClientCountForEmployee', ...params });
};

/**
 * 获取员工关联客户浏览本门店数
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 */
spmdm.getBrowserForEmployee = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getBrowserForEmployee', ...params });
};

/**
 * 获取员工关联客户列表
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 */
spmdm.getClientListForEmployee = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getClientForEmployee', ...params });
};

/**
 * 获取员工关联客户人数本门店销售统计
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 */
spmdm.getSalesForEmployee = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getSalesForEmployee', ...params });
};

/**
 * 获取员工关联客户人数本门店销售统计
 * @param {object} params
 * @param {date} params.startDate 非必传
 * @param {date} params.endDate 非必传
 */
spmdm.getSalesOverviewForEmployee = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getSalesOverviewForEmployee', ...params });
};

/**
 * 批量修改门店访客审核状态
 * @param {object} params
 * @param {string} params.type 类型：0:不更新状态，1:更新状态（默认为0）
 */
spmdm.updateAuditFlagInBatch = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-updateAuditFlagInBatch', ...params });
};

/**
 * 审核客户
 * @param {object} params
 * @param {string} params.userId 客户id
 * @param {string} params.flag 状态,1允许访问，2禁止访问
 */
spmdm.auditMemberByUserId = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-auditMemberByUserId', ...params });
};

/**
 * 批量保存会员参数信息
 * @param {object} params
 * @param {string} params.custTenantId 客户的租户id, 分隔
 * @param {string} params.code 参数代码, 余额支付:open_balance_payment, 欠款支付:open_debt_payment
 * @param {string} params.val 参数值, 1:开启, 0:关闭
 */
spmdm.batchSaveMemberParams = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['custTenantId']), ssCaps.spbDefParams());
    return common.get(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-batchSaveMemberParams', ...params });
};

/**
 * 根据店铺参数批量保存会员参数
 * @param {object} params
 * @param {string} params.code 参数代码, 客户线下支付:open_cust_offline_pay
 * @param {string} params.val 参数值, 1:开启, 0:关闭
 */
spmdm.batchSaveMembersParamWithCode = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-batchSaveMembersParamWithCode', ...params });
};

/**
 * 批量获取商品分享二维码
 * @param {object} params
 * @param {string} params.type  否  分享类型：商品-‘goods’,门店-‘shop’,活动-'specialEvent'
 * @param {string} params.spuIds  是  商品id：多个以逗号隔开
 * @param {bool} params.isAppCode  否  true:生成货品分享的菊花码
 */
spmdm.getBatchShareQrCodeBase64ForWechat = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-ssMallMember-getBatchShareQrCodeBase64ForWechat', ...params });
};

/**
 * 获取对账单
 * @param {object} params
 * @param {string} params.tenantId 是 卖家租户ID
 * @param {string} params.type 是 0按详情 1按批次
 * @param {string} params.orderBy 否 排序，0日期优先 1分店优先 2款号优先 其中明细没有默认排序，按批次默认0 没有款号优先
 * @param {string} proDate1 否 查询开始时间，默认今天
 * @param {string} proDate2 否 查询结束时间，默认今天
 */
spmdm.getSlhPdf = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spmdm-genSlhPdf', ...params });
};
