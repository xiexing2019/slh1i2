const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');

const sppur = module.exports = {
    url: ssCaps.spbUrl
};

/**
 * 再来一单
 * @description
 * @param {object} params
 * @param {string} params.purBillId 采购单id
 */
sppur.addBillItem2Cart = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purBill-addBillItem2Cart', ...params });
};

/**
 * 订单支付状态回退
 * @description
 * @param {object} params
 * @param {string} params.id 买家订单id集合
 */
sppur.returnPayBillStatus = async function (params = {}) {
    Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purBill-returnPayBillStatus', ...params });
};

/**
 * 轮询采购单状态
 * @description 由http同步创建销售单改为mq异步创建, 创建采购单完成时返回, purBillFlag=2(处理中), 销售单创建成功后flag=3, 由客户端轮询
 * @param {object} params
 * @param {string} params.purBillId
 */
sppur.checkPurBillFlag = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purBill-checkPurBillFlag', ...params });
};

/**
 * 一键补货列表
 * @param {object} params
 * @param {string} params.searchToken 查询条件，目前仅支持商品标题，模糊查询
 * @param {string} params.shopId 当前门店id
 */
sppur.findRecentBillSpu = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purBill-findRecentBillSpu', ...params });
};

/**
 * 支付方式字典
 * @param {object} params
 */
sppur.findSellerPayWay = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-purPayWay-findSellerPayWay', ...params });
};

/**
 * 公众号查询电子小票列表
 * @param {object} params
 * @param {string} params.saasShopId 临时门店id，传0表示查询全部
 * @param {string} params.timeType 0 全部 1 周 2 月 3 年, 传入startTime和endTime时此参数不传
 * @param {string} params.startTime 查询开始时间
 * @param {string} params.endTime 查询截至时间
 */
sppur.getPurReceipt = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-getPurReceipt', ...params });
};

/**
 * 退货退款单列表
 * @param {object} params
 */
sppur.findReturnBills = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-findReturnBills', ...params });
};
/**
 * 退货退款单详情
 * @param {object} params
 * @param {string} params.id 退款单id
 */
sppur.getReturnBillDetail = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-sppur-billReturn-getReturnBillDetail', ...params });
};

/**
 * 买家端, 首页提示有待支付的活动订单
 * @param {object} params
 * @param {string} params.sellerId  卖家租户id
 */
sppur.findUnpaidActBillIndex = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-sppur-purBill-findUnpaidActBillIndex', ...params });
};