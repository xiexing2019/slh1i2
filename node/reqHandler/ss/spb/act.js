const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spbUrl = ssCaps.spbUrl;
const act = {
    /** 活动基础 */
    marketBase: {
        /**
         * 订单 - 商品有效性检查
         * @param {object} params
         * @param {string} params.sellerUnitId 卖家租户id
         */
        checkOrder: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-checkOrder', ...params });
        },
        /**
         * 订单 - 下单成功回调
         * @param {object} params
         * @param {string} params.sellerUnitId 卖家租户id
         */
        doWhenOrderOk: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-doWhenOrderOk', ...params });
        },
        /**
         * 查询活动商品信息
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {string} params.sellerUnitId 卖家租户id
         */
        getAllSpuInfoByAct: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-findAllSpuInfoByAct', ...params });
        },
        /**
         * 单个商品参与活动信息
         * @param {object} params
         * @param {string} params.spuId 商品spuId
         * @param {string} params.sellerUnitId 卖家租户id
         */
        getSpuPartActInfo: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-getSpuPartActInfo', ...params });
        },
        /**
         * 单个商品参与活动信息
         * @param {object} params
         * @param {string} params.spuId 商品spuId
         * @param {string} params.sellerUnitId 卖家租户id
         * @param {string} params.spuPrice 会员适用价格
         * @param {string} params.buyerId 买家unitId
         */
        listSpuActInfo: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketDimSpuAction-listSpuActInfo', ...params });
        },
        /**
         * 批量商品参与活动信息
         * @param {object} params
         * @param {string} params.spuKey spu 取值key
         * @param {string} params.sellerUnitId 卖家租户id
         */
        translateSpuPartAct: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-translateSpuPartAct', ...params });
        },
        /**
         * 购物车列表 - 活动信息
         * @param {object} params
         * @param {string} params.actKey actId 取值key
         * @param {string} params.sellerUnitId 卖家租户id
         */
        translateShoppingCartAct: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-translateShoppingCartAct', ...params });
        },
        /**
         * 限时/秒杀活动详情
         * @param {object} params
         * @param {string} params.id 活动id
         */
        getById: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-getById', ...params });
        },
        /**
         * 限时/秒杀批量添加商品
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {Array} params.spuIds 商品id列表, 例: [1111,22222,3333]
         */
        batchAddSpu: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-batchAddSpu', ...params });
        },
        /**
         * 限时/秒杀批量删除商品
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {Array} params.spuIds 商品id列表, 例: [1111,22222,3333]
         */
        batchDeleteSpu: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-batchDeleteSpu', ...params });
        },
        /**
         * 查看单个活动商品信息
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {string} params.spuId 商品id
         */
        findFullMarketSpu: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-spu-findFullMarketSpu', ...params });
        },
        /**
         * 修改活动商品信息
         * @param {object} params
         */
        updateMarketSpu: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-spu-updateMarketSpu', ...params });
        },
        /**
         * 新增活动商品信息
         * @description 直播活动添加商品
         * @param {object} params
         */
        addMarketSpu: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-spu-addMarketSpu', ...params });
        },
        /**
         * 根据活动ID查询活动信息
         * @description
         * 1. 单元区-卖家
         * 2. 服务端内部调用 活动中使用优惠券,查询优惠券列表时使用,根据返回值的coupFlag判断活动中是否可使用该优惠券
         * @param {object} params
         * @param {string} params.sellerUnitId 买家单元ID
         * @param {Array} params.jsonParam 内容为list数据，值为活动id
         */
        getMarketBaseByIds: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketBase-getMarketBaseByIds', ...params });
        },
    },
    /** 拼团 */
    groupBuy: {
        /**
         * 新增拼团接口
         * @param {object} params
         * @param {string} params.id 活动id
         */
        addOrUpdateMarketGroupBuy: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-addOrUpdateMarketGroupBuy', ...params });
        },
        /**
         * 拼团活动详情接口
         * @param {object} params
         * @param {string} params.actId 活动id
         */
        marketGroupBuyDetail: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupBuyDetail', ...params });
        },
        /**
         * 拼团列表接口
         * @param {object} params
         * @param {string} params.flag 拼团状态 0已结束 1 未开始 3进行中 2暂停中
         */
        marketGroupBuyList: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupBuyList', ...params });
        },
        /**
         * 拼团失效接口
         * @param {object} params
         * @param {string} params.actId 活动id
         */
        marketGroupBuyInvalid: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupBuyInvalid', ...params });
        },
        /**
         * 拼团删除接口
         * @param {object} params
         * @param {string} params.actId 活动id
         */
        deleteMarketGroupBuyById: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-deleteMarketGroupBuyById', ...params });
        },
        /**
         * 团购订单列表接口
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {string} params.flag 0 拼团中 1拼团成功 2拼团失败
         */
        marketGroupBuyOrderList: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-findSsMarketGroupBuyOrderList', ...params });
        },
        /**
         * 拼团活动数据展示
         * @param {object} params
         * @param {string} params.actId 活动id
         */
        marketGroupBuyStatistics: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupBuyStatistics', ...params });
        },
        /**
         * 买家查询拼团中订单列表
         * @param {objcet} params
         * @param {number} params.actId 活动id
         * @param {number} params.sellerId 卖家id
         */
        findGroupBuyOrderRunningList: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-findSsMarketGroupBuyOrderRunningList', ...params });
        },
        /**
         * 买家查询拼团中订单详情
         * @param {object} params
         * @param {number} params.actOrderId 拼团团购id
         * @param {number} params.sellerId 卖家id
         */
        findGroupBuyOrderDetail: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-ssMarketGroupBuyOrderDetail', ...params });
        },
        /**
         * 加入或者新建团购下单接口
         * @param {object} params
         */
        groupBuyOrderItem: async function (params = {}) {
            params = Object.assign({ "productVersion": "3.1.0" }, format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-groupBuyOrderItem', ...params });
        }
    },
    /** 限时折扣 */
    timeLimit: {
        /**
         * 新增限时折扣
         * @param {object} params
         */
        saveFull: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-rule-saveFull', ...params });
        },
        /**
         * 限时折扣列表查询
         * @param {object} params
         * @param {string} params.flag 状态 1: 未开始  3: 进行中  0:已结束   -2: 已失效
         */
        getMarketList: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-rule-list', ...params });
        },
        /**
         * 失效活动
         * @param {object} params
         * @param {string} params.id 活动id
         */
        disableRule: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-rule-disable', ...params });
        },
        /**
         * 删除活动
         * @param {object} params
         * @param {string} params.id 活动id
         */
        deleteRule: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-rule-delete', ...params });
        },
        /**
         * 小程序专题活动获取活动列表（实时）
         * @param {object} params
         * @param {string} params.sellerTenantId 卖家tid
         */
        findMarketForActivity: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-rule-findMarketForActivity', ...params });
        },
    },
    /** 秒杀 */
    secKill: {
        /**
         * 新增秒杀活动
         * @param {object} params
         */
        saveFull: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-secKill-saveFull', ...params });
        },
        /**
         * 秒杀列表查询
         * @param {object} params
         * @param {string} params.flag 状态 1: 未开始  2:预热 3: 进行中  0:已结束
         */
        getSecKillList: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-secKill-list', ...params });
        },
        /**
         * 买家端-秒杀活动标题列表
         * @param {object} params
         */
        listForBuyer: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-secKill-listForBuyer', ...params });
        },
        /**
         * 买家端-秒杀活动商品列表查询
         * @param {object} params
         * @param {string} params.actSpuId 活动商品id
         */
        getSpuListForBuyer: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-market-secKill-spu-listForBuyer', ...params });
        }
    },
    groupShare: {
        /**
         * 新增/修改群接龙活动
         * @param {object} params
         */
        saveGroupShareAct: async function (params = {}) {
            params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupShare-saveGroupShareAct', ...params });
        },
        /**
         * 商陆花商品群接龙活动预览页
         * @param {object} params
         * @param {string} params.slhSpuIds 商陆花商品id列表，逗号隔开
         */
        getActPrepareInfo: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupShare-getActPrepareInfo', ...params });
        },
        /**
         * 查询历史群接龙活动 默认分页查第一页
         * @param {object} params
         * @param {string} params.orderBy 排序字段
         * @param {string} params.orderByDesc 是否倒序, ‘true’ 倒序
         * @param {string} params.searchKey 活动查询关键字
         */
        findGroupShareList: async function (params = {}) {
            params = Object.assign({ pageNo: 1, pageSize: 20, ...params }, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupShare-findGroupShareList', ...params });
        },
        /**
         *  查询群接龙活动详情
         * @param {object} params
         * @param {string} params.actId 活动id
         * @param {string} params.sellerUnitId 卖家单元id  买家调用时需要传
         */
        getGroupShareFullInfo: async function (params = {}) {
            Object.assign(params, ssCaps.spbDefParams());
            return common.post(spbUrl, { apiKey: 'ec-ss-marketGroupShare-getGroupShareFullInfo', ...params });
        },
    }
};



module.exports = act;
