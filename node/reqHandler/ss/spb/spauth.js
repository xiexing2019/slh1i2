const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let spAuth = module.exports = {
    url: ssCaps.spbUrl
};

/**
 * 我的界面数据
 * @param {object} params jsonParam
 * @param {string} params.userId 
 */
spAuth.getUserSession = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-spUserRole-getUserSession', ...params });
};
/**
 * 保存角色权限
 * @param {object} params
 * @param {string} params.id 角色id，更新时必传
 * @param {string} params.name 角色名称，添加时必传
 * @param {string} params.rem 备注
 * @param {object} params.funcIds 为空时传[]
 */
spAuth.newRole = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-roleFuncRel-save', ...params });
};

/**
 * 树状获取所有权限
 * @param {object} params
 */
spAuth.getlistByTree = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-roleFuncRel-listByTree', ...params });
};

/**
 * 根据角色id获取权限
 * @param {object} params
 * @param {string} params.roleId 角色id
 */
spAuth.getAuthListByRoleId = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-roleFuncRel-listByRoleId', ...params });
};

/**
 * 删除角色
 * @param {object} params
 * @param {string} params.roleId 角色id
 */
spAuth.deleteRole = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-roleFuncRel-deleteRole', ...params });
};

/**
 * 获取当前门店所有角色
 * @param {object} params
 * @param {string} params.flag 状态
 */
spAuth.getRoleListAtShop = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.get(this.url, { apiKey: 'ec-spauth-spRole-list', ...params });
};