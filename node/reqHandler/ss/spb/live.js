const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let live = module.exports = {
    spbUrl: ssCaps.spbUrl,
    spgUrl: ssCaps.spgUrl
};

/**
 * 查询直播时长首次购买情况
 * @param {Object} params
 */
live.getFirstBuyLiveTimeFlag = async function () {
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-getFirstBuyLiveTimeFlag' });
};

/**
 * 获取直播时长订单列表
 * @param {Object} params
 * @param {number} params.pageNo 否 页
 * @param {number} params.pageSize 否 页大小
 */
live.getLiveTimeVasBills = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-findLiveTimeVasBills', ...params });
};

/**
 * 购买直播时长
 * @param {Object} params
 * @param {string} params.num
 */
live.liveTimeAddService = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-liveTimeAddService', ...params });
};

/**
 * 赠送直播时长
 * @param {Object} params
 * @param {number} params.tenantId
 * @param {double} params.time
 */
live.presentLiveTime = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-presentLiveTime', ...params });
}

/**
 * 获取直播任务结束统计信息
 * @param {Object} params
 * @param {string} params.taskId 直播任务id
 */
live.getLiveTaskStats = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-getLiveTaskStats', ...params });
};

/**
 * 获取直播功能适用情况 1适用 0不适用
 */
live.getLiveShowEnableStatus = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-live-ssLiveRoom-getLiveShowEnableStatus', ...params });
}

/**
 * 保存直播任务
 * @param {Object} params
 * @param {number} params.id  否  直播任务id
 * @param {number} params.liveRoomId 是 直播间id
 * @param {string} params.timGroupId 是 聊天室id
 * @param {string} params.title 是 直播主题
 * @param {date} params.startDate 否 预计开始时间
 * @param {date} params.endDate 否 预计结束时间
 * @param {number} params.robotNum 否 机器人数量 0-1000
 * @param {string} params.spus 否 商品id列表，逗号隔开
 */
live.saveLiveTask = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-saveLiveTask', ...params });
};

/**
 * 操作直播
 * @param {Object} params
 * @param {number} params.id 是-直播任务id
 * @param {string} params.opType 是-操作类型，对应直播任务flag，3 ：开始，4： 暂停 0： 结束， -1： 删除
 */
live.operateLiveTask = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-operateLiveTask', ...params });
};

/**
 * 获取直播任务列表
 * @param {Object} params
 */
live.getLiveTaskList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-market-marketLiveTask-list', ...params });
};

/**
 * 获取直播任务
 * @param {Object} params
 * @param {number} params.sellerUnitId 卖家单元id
 */
live.getLastLiveTask = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-getLastLiveTask', ...params });
};

/**
 * 直播商品列表
 * @param {Object} params
 * @param {number} params.taskId 活动id
 * @param {number} params.sellerUnitId	卖家租户id，买家调用时需要传
 * @param {number} params.showInv 	传1 代表显示库存
 */
live.getLiveTaskSpus = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-findLiveTaskSpus', ...params });
};

/**
 * 新增活动商品信息
 * @param {Object} params
 * @param {number} params.actId 活动id
 * @param {number} params.spuId 活动商品id
 * @param {Object} params.execVal 否，优惠策略json
 * @param {number} params.execVal.execType 优惠类型1=折扣,2=定价(特价),5=免减
 * @param {number} params.execVal.execNum	优惠类型对应的值 1时传（1-9表示对应折扣），2时直接传价钱， 5时传减免额度
 * @param {number} params.execVal.execExtraKind 额外附加的条件类型4=抹零,抹去角和分8=抹零,抹去分
 * @param {Object} params.skuInvList 否，活动商品sku信息
 * @param {number} params.skuInvList.skuId
 * @param {number} params.skuInvList.invNum 库存
 */
live.addLiveTaskSpus = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-saveLiveSpu', ...params });
};

/**
 * 查询店铺关注情况
 * @param {Object} params
 * @param {number} params.shopId 店铺id
 */
live.checkShopFavorState = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-up-favorShop-checkShopFavorState', ...params });
};

/**
 * 直播弹商品
 * @param {Object} params
 * @param {number} params.taskId 直播任务id
 * @param {string} params.spuIds 商品id列表，逗号隔开
 */
live.popSpus = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-popSpus', ...params });
};

/**
 * 获取直播间信息
 * @param {Object} params
 */
live.getLiveRoomInfo = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-live-ssLiveRoom-getLiveRoomInfo', ...params });
};

/**
 * 获取云通信登录账号与签名
 * @param {Object} params
 * @param {number} params.userType 用户，0 买家， 1 卖家
 */
live.getTimUserSig = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-live-ssTimUserSig-getTimUserSig', ...params });
};

/**
 * 加入直播聊天室
 * @param {Object} params
 * @param {number} params.sellerUnitId 卖家单元id 买家进入时需要，模拟卖家登录用
 * @param {number} params.liveRoomId 直播间id
 * @param {number} params.groupId 聊天室id
 */
live.enterGroup = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssLiveGroupMember-enterGroup', ...params });
};

/**
 * 退出直播间聊天室
 * @param {Object} params
 * @param {number} params.sellerUnitId 卖家单元id
 * @param {number} params.liveRoomId 直播间id
 */
live.leaveGroup = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssLiveGroupMember-leaveGroup', ...params });
};

/**
 * 用户发送下单消息
 * @param {Object} params
 * @param {string} params.groupId 群组id
 * @param {string} params.nickName 昵称
 * @param {string} params.spuIds 商品ID列表，逗号隔开
 * @param {number} params.orderType 类型，0 下单 ， 1 加购物车
 */
live.sendUserOrderMsg = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-live-ssMarketLiveTask-sendUserOrderMsg', ...params });
};

/**
 * 点赞直播间
 * @param {Object} params
 * @param {number} params.roomId 直播间id
 * @param {number} params.likeNum 用户点赞数
 * @param {string} params.timUserName 聊天室用户名称
 */
live.likeLiveRoom = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-live-ssLiveRoom-likeLiveRoom', ...params });
};

/**
 * 查找直播间
 * @param {Object} params
 * @param {number} params.id 直播间id
 */
live.findLiveRoom = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-live-ssLiveRoom-getById', ...params });
};
