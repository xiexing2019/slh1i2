const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spchb = module.exports = {
    url: ssCaps.spbUrl,
};


/**
 * 获取全部商品列表
 * @param {object} params
 * @param {string} params.tenantId 卖家租户id
 */
spchb.getDresSpuList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), ssCaps.spbDefParams());
    // return common.get(this.url, { apiKey: 'ec-spchb-dresSearch-listDresSpu', ...params });
    return common.get(this.url, { apiKey: 'ec-spdresb-dresSpu-listDresSpu', ...params });
};
