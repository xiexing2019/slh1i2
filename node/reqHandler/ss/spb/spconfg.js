const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const spconfg = module.exports = {
    url: ssCaps.spbUrl,
};

/**
 * 修改默认模板标识
 * @param {object} params
 * @param {object} params.templateId 模板id
 */
spconfg.updateFeeDefaultTemplate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-updateFeeDefaultTemplate', ...params });
};

/**
 * 卖家模板数据初始化
 * @description 对卖家没有没有模板创建默认模板，针对商品的模板id初始化
 * @param {object} params
 */
spconfg.initFeeRuleData = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-initData', ...params });
};

/**
 * 新运费模板详情
 * @param {object} params
 * @param {string} params.templateId 模板id
 */
spconfg.newTemplateFeeRuleDetails = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-newTemplateFeeRuleDetails', ...params });
};

/**
 * 删除模板
 * @description 软删除操作,并且将所有的商品模板更改成包邮
 * @param {object} params
 * @param {string} params.templateId 模板id
 */
spconfg.deleteTemplate = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-deleteTemplate', ...params });
};

/**
 * 新增模板
 * @description 支持对模板的新增,修改,模板类的规则子集,新增,修改,和删除。示例:假设模板不存在 --则全部新增 假设模板存在。修改了模板名称,新增了运费规则,删除了其中一个运费规则。。修改了包邮规则和新增了包邮规则 和修改了其中一个包邮规则
 * @param {object} params
 * @param {string} params.tempalteName 模板名称
 */
spconfg.saveTemplateFeeRule = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-saveTemplateFeeRule', ...params });
};

/**
 * 运费规则列表
 * @param {object} params
 * @param {string} params.tempalteName 模板名称
 */
spconfg.getTemplateFeeRules = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-spconfg-spFeeRule-getTemplateFeeRules', ...params });
};