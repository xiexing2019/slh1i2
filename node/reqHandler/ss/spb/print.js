const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');

let print = module.exports = {
    url: ssCaps.spbUrl
};

/**
 * 获取开通的物流商网点账户列表
 * @description 单元区print库
 * @param {number} params.expId 物流商id,默认查全部
 */
print.findExpSheetAccount = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-findExpSheetAccount', ...params });
};

/**
 * 新增/编辑电子面单账户
 * @param {object} params jsonParam
 * @param {string} params.expName 快递名称
 * @param {string} params.expId 快递公司id
 * @param {string} params.expAccount 网点账号
 * @param {string} params.expPassword 网点密码
 * @param {string} params.remark 备注
 * @param {string} params.defaultFlag 是否默认账户 1设置为默认，默认0
 */
print.saveExpSheetAccount = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-saveExpSheetAccount', ...params });
};

/**
 * 删除电子面单账户 
 * @param {number} params.id 
 */
print.delExpSheetAccount = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-delExpSheetAccount', ...params });
};

/**
 * 获取网点账户信息 
 * @param {number} params.sheetAccountId 
 */
print.getExpSheetAccountById = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-getExpSheetAccountById', ...params });
};

/**
 * 新增/编辑发货地址 
 * @param {object} params jsonParam
 * @param {string} params.id 新增不传，编辑必传
 * @param {string} params.shipName 发货人
 * @param {string} params.mobile 电话
 * @param {string} params.province 省份
 * @param {string} params.city 城市
 * @param {string} params.district 区域
 * @param {string} params.detail 详细地址
 * @param {string} params.defaultFlag 是否默认地址 1设置为默认，默认0
 */
print.saveShipAddress = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-saveShipAddress', ...params });
};

/**
 * 获取发货地址列表 
 */
print.findShipAddress = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-findShipAddress', ...params });
};

/**
 * 获取发货详情 
 * @param {string} params.shipAddressId 主键
 */
print.getShipAddressById = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-getShipAddressById', ...params });
};

/**
 * 删除发货地址 
 * @param {string} params.id 地址主键
 */
print.delShipAddress = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-delShipAddress', ...params });
};

/**
 * 保存/编辑打印机 
 * @param {object} params jsonParam
 * @param {number} params.id 新增不传，编辑必传
 * @param {number} params.printWay 打印方式 1蓝牙，2打印
 * @param {string} params.printerType 打印类型
 * @param {string} params.printerCode 打印机编码（云打印必传）
 * @param {string} params.printerName 打印机名称
 */
print.savePrinterSetting = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-savePrinterSetting', ...params });
};

/**
 * 获取打印机列表 
 * @param {number} params.printWay 打印方式 1蓝牙，2打印
 */
print.findAvailablePrinters = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-findAvailablePrinters', ...params });
};

/**
 * 打印机详情 
 * @param {string} params.printerId 
 */
print.getPrinterById = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-getPrinterById', ...params });
};

/**
 * 执行打快递面单打印 
 * @param {object} params jsonParam
 * @param {string} params.expName 物流商
 * @param {string} params.expId 物流商id
 * @param {string} params.shipId 发货地址id
 * @param {string} params.printerName 打印机名称
 * @param {string} params.printerId 打印机主键
 * @param {object} params.recData 收件人信息：list数据
 */
print.findAvailablePrinters = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-findAvailablePrinters', ...params });
};

/**
 * 获取打印数据
 */
print.getPrintData = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-getPrintData', ...params });
};

/**
 * 不更换单号打印面单 
 * @param {object} params jsonParam
 * @param {object} params.billIds 订单列表, 例: [1111,22222,3333]
 */
print.executeOriginalPrint = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-executeOriginalPrint', ...params });
};

/**
 * 断开打印机连接 
 * @param {object} params jsonParam
 * @param {object} params.printerId 打印机主键
 */
print.delPrinter = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-delPrinter', ...params });
};

/**
 * 根据快递单号智能匹配物流商信息 
 * @description 单元区-卖家
 * @param {object} params 
 * @param {object} params.expNum 快递单号
 */
print.intelFindMatchedExpress = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    return common.post(this.url, { apiKey: 'ec-print-expressSheet-intelFindMatchedExpress', ...params });
};