const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const order = module.exports = {
    url: ssCaps.opUrl,
};
/**
 * 同步SaaS改单
 * @param {object} params
 * @param {string} params.slhUnitId 商陆花单元id
 * @param {string} params.mallShopTenantId 卖家租户id
 */
order.syncEditedBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['slhUnitId', 'mallShopTenantId']), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-salesBill-syncEditedBill', ...params });
};

/**
 * 终结订单
 * @param {object} params
 * @param {string} params.slhUnitId 商陆花单元id
 * @param {string} params.mallShopTenantId 卖家租户id
 */
order.stockoutReturn = async function (params = {}) {
    Object.assign(params, ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-salesBill-stockoutReturn', ...params });
};

/**
 * 同步商陆花发货接口
 * @param {object} params
 * @param {string} params.sellerId 卖家租户id
 * @param {string} params.salesBillId 线上销售单id
 */
order.syncSaveSalesDeliver = async function (params = {}) {
    Object.assign(params, ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-deliver-syncSaveSalesDeliver', ...params });
};

/**
 * 发起终结缺货单退款
 * @param {object} params
 * @param {string} params.sellerId 卖家租户id
 * @param {string} params.returnId 缺货单id
 */
order.stockoutRefund = async function (params = {}) {
    Object.assign(params, ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-salesBill-stockoutRefund', ...params });
};

/**
 * 创建收款单
 * @param {object} params
 * @param {string} params.bizType 交易类型 1240(传退款单id)
 * @param {string} params.bizOrderId 关联业务单据
 */
order.createReceiptBill = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['bizOrderId']), ssCaps.opDefParams());
    params.bizType = 1240;
    return common.post(this.url, { apiKey: 'ec-op-receiptBill-create', ...params });
};

/**
 * 查询收款单
 * @param {object} params
 * @param {string} params.bizType 交易类型 1240(传退款单id)
 * @param {string} params.bizOrderId 关联业务单据
 */
order.getReceiptBillById = async function (params = {}) {
    Object.assign(params, ssCaps.opDefParams());
    params.bizType = 1240;
    return common.post(this.url, { apiKey: 'ec-op-receiptBill-getById', ...params });
};
