const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const op = module.exports = {
    url: ssCaps.opUrl,
};

/**
 * 获取accessToken
 * @description
 * 1. 每个店铺都有一个固定的appKey和appSecret
 * 2. spg.sp_op_app
 * @param {object} params
 * @param {string} [params._cid] 机房代码，应用申请后平台提供，必须放在url中
 * @param {string} params.appKey 开放应用appKey
 * @param {string} params.appSecret 开放应用appSecret
 */
op.refreshToken = async function (params = {}) {
    Object.assign(params, ssCaps.spbDefParams());
    const res = await common.post(this.url, { apiKey: 'refreshToken', ...params });
    LOGINDATA.accessToken = res.result.data.accessToken;
    return res;
};

/** 保存商品信息
 * @param {object} params
 * @param {number} params.id
 * @param {object} params.spu
 * @param {object} params.sku
 */
op.saveDresSpuFull = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-dresSpu-saveFull', ...params });
};

/**
 * 覆盖更新sku库存
 * @param {object} params
 * @param {Array} params.skus
 * 
 */
op.updateFullDresSkuNum = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-dresSku-updateFullDresSkuNum', ...params });
};

/**
 * 同步作废商城的商品分类
 * @param {object} params
 * @param {Array} params.slhId
 * @param {Array} params.name
 * 
 */
op.syncDeleteDresSpuClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-dresClass-syncDeleteDresSpuClass', ...params });
};


/**
 * 同步商品分类到商城
 * @param {object} params
 * @param {Array} params.slhId
 * @param {Array} params.name
 * 
 */
op.syncDresSpuClass = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-dresClass-syncDresSpuClass', ...params });
};
