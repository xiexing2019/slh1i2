const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../ssCaps');

const order = module.exports = {
    url: ssCaps.opUrl,
};


/**
 * 同步积分、适用价格到商城
 * @param {Object} params
 * @param {string} params.id 商城客户id 必填
 * @param {string} params.slhId 商陆花客户id
 * @param {string} params.slhSellerId 店员
 * @param {string} params.bonus 积分 必填
 * @param {int} params.priceType 价格类型 必填
 * @return {object} {params, result}
 */
order.custLinkBack = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.opDefParams());
    return common.post(this.url, { apiKey: 'ec-op-cust-link-back', ...params });
}