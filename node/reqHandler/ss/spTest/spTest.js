const common = require('../../../lib/common');
const ssCaps = require('../ssCaps');
const format = require('../../../data/format');
const ssAccount = require('../../../ss/data/ssAccount');

let main = module.exports = {

};

main.baseTest = async function (params) {
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-sp-test', ...params });
};

/**
 * 触发定时任务
 * @param {object} params
 * @param {string} params.taskCode 任务code
 * @param {string} params.unitId 单元id
 * @param {string} params._cid 集群id
 */
main.initTask = async function (params) {
    return common.superagent.get(`${ssCaps.taskUrl}?taskCode=${params.taskCode}&unitId=${params.unitId}&_cid=${params._cid}`)
        .then(response => {
            console.log(`response=${JSON.stringify(response)}`);
            return response.text;
        })
        .catch(err => console.log(err));
};
