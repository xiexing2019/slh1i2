const caps = require('../../data/caps');

const ssCaps = {
    ss_dev: {
        spgMysql: {
            host: '192.168.0.13',
            port: '3366'
        },
        spbMysql: {
            host: '192.168.0.2',
            port: '3366'
        },
    },
    ss_test: {
        esUrl: '',
        spgMysql: {
            host: '192.168.0.4',
            port: '3366'
        },
        spbMysql: {
            host: '192.168.0.3',
            port: '3366'
        },
    },
    ss_test2: {
        esUrl: '',
        spgMysql: {
            host: '192.168.0.50',
            port: '3366'
        },
        spbMysql: {
            host: '192.168.0.99',
            port: '3366'
        },
    },
    ss_chk: {
        esUrl: '',
        spgMysql: {
            host: '192.168.0.51',
            port: '3366'
        },
        spbMysql: {
            host: '192.168.0.52',
            port: '3366'
        },
    },
    ss_pre: {
        esUrl: '',
        spgMysql: {
            host: '212.64.95.208',
            port: '3366'
        },
        spbMysql: {
            host: '212.64.87.170',
            port: '3366'
        },
    }
};
const defDB = {
    lqdMysql: {
        host: '172.81.235.150',
        port: '3366'
    },
};

const expSheetAccount = {
    expName: '中通快递',
    expAccount: 'ZTO531524202938513',
    expPassword: '09JD25ZW',
}

const spbDefParams = () => {
    return {
        _cid: LOGINDATA.clusterCode || 0,
        _tid: LOGINDATA.tenantId || 0
    }
};

const opDefParams = () => {
    return {
        _cid: LOGINDATA.clusterCode || 0,
        _tid: LOGINDATA.tenantId || 0,
        accessToken: LOGINDATA.accessToken || ''
    }
};

/**
 * 是否全局会话
 */
const isSpgSessionId = () => LOGINDATA.sessionId.includes('ctr-');

module.exports = {
    url: `${caps.url}`,
    confcUrl: `${caps.url}/confc/api.do`,
    spgUrl: `${caps.url}/spg/api.do`,
    // spgUrl: `http://192.168.0.43:8081/spg/api.do`,
    spbUrl: `${caps.url}/spb/api.do`,
    // spbUrl: `http://152.136.38.172:8081/spb/api.do`,
    opUrl: `${caps.url}/spop/api.do`,
    docUrl: `${caps.url}/doc`,
    // esUrl: spCaps[caps.name].esUrl || '',
    taskUrl: `${caps.url}/spb/task/execute.do`,

    db: Object.assign({}, ssCaps[caps.name], defDB),
    expSheetAccount: expSheetAccount,
    productVersion: '4.0.0',
    rate: 0.003,

    spbDefParams,
    opDefParams,
    isSpgSessionId,

};
