// confc
exports.ecPrint = require('./confc/ecPrint');
exports.smsTemplateDefinition = require('./confc/smsTemplateDefinition');
exports.spAuth = require('./confc/spAuth');

// spg
exports.confg = require('./spg/confg');
exports.config = require('./spg/config');
exports.spbi = require('./spg/spbi');
exports.spugr = require('./spg/spugr');
exports.ugr = require('./spg/ugr');
exports.ssugr = require('./spg/ssugr');

// spb
exports.accounting = require('./spb/accounting');
exports.act = require('./spb/act');
exports.dis = require('./spb/dis');
exports.fin = require('./spb/fin');
exports.live = require('./spb/live');
exports.print = require('./spb/print');
exports.spauth = require('./spb/spauth');
exports.spcart = require('./spb/spcart');
exports.spconfg = require('./spb/spconfg');
exports.spchb = require('./spb/spchb');
exports.spdresb = require('./spb/spdresb');
exports.spdresup = require('./spb/spdresup');
exports.spmdm = require('./spb/spmdm');
exports.sppur = require('./spb/sppur');
exports.spsales = require('./spb/spsales');
exports.ssbi = require('./spb/ssbi');
exports.sscoupb = require('./spb/sscoupb');
exports.ssIM = require('./spb/ssIM');
exports.up = require('./spb/up');

// spop
exports.op = require('./open/op.js');
exports.opOrder = require('./open/order.js');
exports.opClient = require('./open/client');


exports.test = require('./spTest/spTest');

exports.creatSqlPool = require('./mysql');
