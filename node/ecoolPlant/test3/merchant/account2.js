const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');


describe('商户,账户信息', async function () {
    this.timeout(TESTCASE.timeout);
    let merchantAndAccount;
    it('保存账户信息', async function () {
        merchantAndAccount = await ecPlant.merfin.saveMerchantAndAccount({
            // merchantId: 29,
            // productCode:  ,//否	产品号
            slhSn: common.getRandomNumStr(15),//否	客户编号
            callbackUrl: '',//是	进件成功或者失败时回调地址
            subMchName: `店铺${common.getRandomStr(5)}`,//是	商户名称
            subMchShortName: `店铺${common.getRandomStr(5)}`,//否	商户简称，最多10个字符
            subMchType: 3,//是	商户类型： 1 企业商户 2 个体工商户 3 小微商户[不传默认为3]
            subMchAddress: `地址${common.getRandomStr(5)}`,//是	商户地址, 最少10个字符串
            // subMchLicenseNo: '',//否	子商户营业执照号
            // subMchLicenseValid: '',//否	子商户营业执照有效期yyyyMMdd
            ownerUserId: common.getRandomNumStr(5),//	否	老板或归属者用户id
            ownerName: '慈增修',//是	负责人姓名
            ownerPhone: 12909180568,//是	负责人电话
            // ownerEmail: '',//否	responsibleEmail
            ownerIdCard: '330282199111114998',//是	负责人身份证号
            ownerIdCardValid: '20220118',//是	负责人身份证有效期, yyyyMMdd
            ownerIdCardFront: 'docx156894473729771-ssdoc12d.jpg',//是	身份证正面照, docId
            ownerIdCardBack: 'docx156894474029775-ssdoc12d.jpg',//是	身份证背面照, docId
            shopAppearDoc: 'docs15687087375151426-ssdoc10d.jpg',//是	店铺门牌照, 腾讯云地址或者docId
            shopInsideDoc: 'docs15687087375151426-ssdoc10d.jpg',//是	店铺内照片, docId
            // merBankId: 145,//	否	商户账号id(merfin端商户账户id)
            bankAccountNo: '6217730700491713',//	是	银行账号
            bankAccountName: '慈增修',//	是	银行账户名
            bankPhone: 12909180568,//	是	银行预留手机号
            bankName: '中信银行',//	是	银行名称
            bankCode: 302100011000,//	是	联行号
            bankType: 1,//	是	银行卡类型 1 个人 2 企业
            bankOpenName: '中信银行',//	否	银行开户行名称
            bankProvince: '浙江',//	否	开户行所在省
            bankCity: '杭州',//	否	开户行所在城市
            // wxAppId:  ,//	否	微信公众的appId, 不是独立子商户的不用传, 默认使用衣科的
            // miniAppId:  ,//	否	小程序的appId, 不是独立子商户的不用传, 默认使用衣科的
        }).then(res => res.result.data);
        console.log(`\n账户信息=${JSON.stringify(merchantAndAccount)}`);
    });
    it('商户列表', async function () {
        const finMerchants = await ecPlant.merfin.findFinMerchants().then(res => res.result.data.rows);
        console.log(`\nfinMerchants=${JSON.stringify(finMerchants.find(obj => obj.id == merchantAndAccount.merchantId))}`);
    });
    it('认证', async function () {
        let platform = ['CITIC', 'MSFPAY', 'TDEA', 'JL'];
        const res = await ecPlant.merfin.executeMerchantCert({
            platforms: platform.toString(),
            merchantId: merchantAndAccount.merchantId,
            isAfresh: 0,
            merBankId: merchantAndAccount.merBankId
        });
        console.log(`\nres=${JSON.stringify(res)}`);
    });
});
