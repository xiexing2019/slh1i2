const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const posManage = require('../../help/ecPay/csPos');

// 目前使用支付测试环境测试  env使用app1
const account = {
    appKey: 'a1db7c216c354ac69c3add0321b675ef',
    appSecret: '11566522732557',
    merchantNo: 'autotestPos001',
    deviceNo: 'autotestPos001',
    shopId: 107273,
    channelCode: '11566522732557'
};

/**
 * pos支付流程
 * 绑定pos机(仅一次保存至account,后续直接读取account中的值)->创建pos支付单->创建pos退款单
 * 
 * https://lanhuapp.com/web/#/item/project/board?type=share_mark&pid=b20a9e51-9b58-4986-873e-c2e871d3be4b&activeSectionId=&teamId=2bc04050-6482-4698-876b-bedbb8ba0ad6&param=7d6c58ea-2999-48ed-bfc7-6800c298d1a8
 */
describe('创建pos支付单-offline', async function () {

    // 绑定一次即可
    describe.skip('绑定pos机', async function () {
        before(async function () {
            const res = await ecPlant.cspos.bindPos({
                merchantNo: 'autotestPos001',
                deviceNo: 'autotestPos001',
                saasShopId: LOGINDATA.invid,
                slhSn: LOGINDATA.sn,
                saasUnitId: 'autotestPos001',
                ...account
            });
            console.log(`res=${JSON.stringify(res)}`);
        });
        it('', async function () {

        });
    });

    const payBill = posManage.setUpPayBill(account);
    before('创建pos支付单', async function () {
        await common.loginDo({ epid: '17005' });
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        const posJson = posManage.mockOrderJson(account);
        await payBill.creatOrder(posJson);
    });
    it('pos查询支付单列表', async function () {
        const detail = await payBill.getOrderDetail();
        common.isApproximatelyEqualAssert(payBill, detail);
    });
    it('pos获取支付单详情', async function () {
        const detail = await payBill.getOrderDetail();
        common.isApproximatelyEqualAssert(payBill, detail);
    });

    describe('确认支付', async function () {
        before(async function () {
            await payBill.orderPayCb();
        });
        it('pos查询支付单列表', async function () {
            const detail = await payBill.getOrderDetail();
            common.isApproximatelyEqualAssert(payBill, detail);
        });
        it('pos获取支付单详情', async function () {
            const detail = await payBill.getOrderDetail();
            common.isApproximatelyEqualAssert(payBill, detail);
        });
    });

    describe('创建pos退款单', async function () {
        before(async function () {
            await payBill.createRefund();
        });
        it('pos获取支付单详情', async function () {
            const detail = await payBill.getOrderDetail();
            common.isApproximatelyEqualAssert(payBill, detail);
        });
        it('pos查询退款单列表', async function () {
            await payBill.findRefundList();
            // common.isApproximatelyEqualAssert(payBill.getRefundExp(), detail);
        });
        it('pos获取退款单详情', async function () {
            const detail = await payBill.getRefundDetail();
            common.isApproximatelyEqualAssert(payBill.getRefundExp(), detail);
        });
        describe('退款回调', async function () {
            before(async function () {
                await payBill.orderRefundCb();
                await common.delay(1000);
            });
            it('pos获取支付单详情', async function () {
                const detail = await payBill.getOrderDetail();
                common.isApproximatelyEqualAssert(payBill, detail);
            });
            it('pos查询退款单列表', async function () {
                await payBill.findRefundList();
                // common.isApproximatelyEqualAssert(payBill.getRefundExp(), detail);
            });
            it('pos获取退款单详情', async function () {
                const detail = await payBill.getRefundDetail();
                common.isApproximatelyEqualAssert(payBill.getRefundExp(), detail);
            });
        });
    });

});