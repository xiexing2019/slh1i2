const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const walletManage = require('../../help/wallet/walletManage');


describe('钱包', async function () {
    this.timeout(TESTCASE.timeout);
    //传商户id
    const myWallet = walletManage.setupWallet({ merchantId: 133 });
    // const myWallet = walletManage.setupWallet({ merchantId: 2713 });
    before('', async function () {
        await myWallet.getHomePageMoney({ update: true });
        console.log(myWallet);
    });
    //收款单的钱都是待结算状态
    describe('收入', async function () {
        let payMoney = common.getRandomNum(50, 100, 1), finAcctFlowDailymoney, totalFlowMoney;
        before('创建收款单', async function () {
            const res = await myWallet.findFinAcctFlowDailyOverview().then(res => res.data[common.getCurrentDate()].find(ele => ele.tradeType == '200').money);
            finAcctFlowDailymoney = res ? res : 0;
            totalFlowMoney = await myWallet.getTotalFlowMoney();
            console.log(finAcctFlowDailymoney);
            const payBill = await myWallet.createPayBill({ payMoney: payMoney });
            console.log(payBill);
        });
        it('获取钱包首页数据', async function () {
            this.retries(3);
            await common.delay(5000);
            await myWallet.getHomePageMoney({ assert: true, money: payMoney });
        });
        it.skip('获取账户流水', async function () {
            await myWallet.findFinAcctFlow();
        });
        it('按日期获取账户日结总金额', async function () {
            const res = await myWallet.findFinAcctDaily();
            expect(myWallet.totalMoney, `按日期获取账户日结总金额与预期${myWallet.totalMoney}不符`).to.equal(res.find(obj => obj.proDate == `${common.getCurrentDate()} 00:00:00`).endBalMoney);
        });
        it('获取财务流水日结概览', async function () {
            const res = await myWallet.findFinAcctFlowDailyOverview();
            const exp = common.add(finAcctFlowDailymoney, payMoney);
            expect(exp, `财务流水日结概览与预期${exp}不符`).to.equal(res.data[common.getCurrentDate()].find(ele => ele.tradeType == '200').money);
        });
        it.skip('获取财务流水日结', async function () {
            await myWallet.findFinAcctFlowDaily();
        });
        it('获取流水总金额', async function () {
            const res = await myWallet.getTotalFlowMoney();
            const exp = common.add(totalFlowMoney, payMoney);
            expect(exp, `流水总金额与预期${exp}不符`).to.equal(res);
        });
    });
    describe('支出', async function () {
        let id, money = 1;
        before('提现', async function () {
            id = await myWallet.createCashBill({ money: money });
        });
        it('获取提现详情', async function () {
            const res = await myWallet.getfinCashBillById({ id: id });
            common.isApproximatelyEqualAssert({ id: id, money: money, acctId: myWallet.acctId, remainMoney: myWallet.totalMoney }, res);
        });
        it.only('获取钱包首页数据', async function () {
            await myWallet.getHomePageMoney();
        });
        it('获取提现记录', async function () {
            await myWallet.findFinCashBill();
        });
        it('重新提现', async function () {
            const res = await myWallet.afreshWithdrawCashBill({ check: false, id: id });
            expect(res.result, '财务流水日结当天的应为空').to.include({ msg: "提现单不是提现驳回状态，不能重新提现" }, { msgId: "fin_cash_bill_flag_is_not_reject_so_not_afresh_withdraw" });
        });
        it('获取账户流水', async function () {
            await myWallet.findFinAcctFlow();
        });
        it('按日期获取账户日结总金额', async function () {
            await myWallet.findFinAcctDaily();
        });
        it('获取财务流水日结概览', async function () {
            await myWallet.findFinAcctFlowDailyOverview({ ioFlag: 1 });
        });
        it('获取财务流水日结', async function () {
            await myWallet.findFinAcctFlowDaily({ ioFlag: 1 });
        });
        it('获取流水总金额', async function () {
            await myWallet.getTotalFlowMoney({ ioFlag: 1 });
        });
    });
});
