const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');

const posManage = module.exports = {};

class PayOrder {
    constructor() {
        /** id */
        this.id = '';
        /** 开发者appKey */
        this.appKey = '';
        /** 开发者appSecret, */
        this.appSecret = '';
        /** 渠道编码 */
        this.channelCode = '';
        /** 业务id */
        this.bizOrderId = '';
        /** 支付金额，单位分 */
        this.totalAmount = '';
        /** 业务类型 */
        this.bizType = '';
        /** 结果反馈的回调地址 */
        this.callbackUrl = '';
        /** 租户ID */
        this.tenantId = '';
        /** 单元id */
        this.unitId = '';
        /** 门店id */
        this.shopId = '';
        /** 支付方式 */
        this.payType = '';
        /** 描述 */
        this.description = {
            /** 店铺名或导购名 */
            shopper: '',
            /** 客户名称 */
            custName: '',
            /** 客户号码 */
            custMobile: '',
            /** 销售数量 */
            amount: '',
            /** 销售单位(比如: 件） */
            unit: '',
        };

        /** 支付状态(0,-1,1,-99) */
        this.payFlag = 0;
        /** 支付单据号(第三方） */
        this.payId = '';
        /** 退款业务id */
        this.bizRefundId = '';
        /** 退款单id */
        this.refundOrderId = '';
        /** 退款状态：0 未退款，1 已退款 */
        this.refundFlag = '';
    }

    setAuthParams(params) {
        this.appKey = params.appKey;
        this.appSecret = params.appSecret;
        this.merchantNo = params.merchantNo;
        this.deviceNo = params.deviceNo;
        return this;
    }

    /**
     * 创建pos支付单
     * @param {object} params 
     */
    async creatOrder(params) {
        const res = await ecPlant.cspos.createOrder(params);
        console.log(`res=${JSON.stringify(res)}`);

        common.update(this, params);
        this.id = res.result.data.val;
        return this;
    }

    /**
     * pos支付回调
     */
    async orderPayCb() {
        const params = {
            appKey: this.appKey,
            appSecret: this.appSecret,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo,
            payOrderId: this.id,
            money: this.totalAmount,
            endTime: common.getCurrentTime(),
            payId: -common.getRandomNumStr(20),
            posPayType: 1
        };
        const res = await ecPlant.cspos.orderPayCb(params);
        this.payFlag = 1;
        this.payId = params.payId;
        return this;
    }

    /**
     * pos查询支付单列表
     */
    async findOrderList() {
        const dataList = await ecPlant.cspos.findOrderList({
            appKey: this.appKey,
            appSecret: this.appSecret,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo,
            // bizOrderId: this.id
        }).then(res => res.result.data.rows);
        const detail = dataList.find(data => data.id == this.id);
        expect(detail, `pos查询支付单列表 未查询到单据id=${this.id}`).not.to.be.undefined;
        return detail;
    }

    /**
     * 获取支付单详情
     */
    async getOrderDetail() {
        const res = await ecPlant.cspos.getOrderDetail({
            appKey: this.appKey,
            appSecret: this.appSecret,
            payOrderId: this.id,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo
        });
        // console.log(`res=${JSON.stringify(res)}`);
        return res.result.data;
    }

    /**
     * POS退款单创建
     */
    async createRefund() {
        const params = {
            bizOrderId: this.bizOrderId,
            bizRefundId: `autoPos${new Date().getTime()}`,
            bizType: this.bizType,
            refundAmount: this.totalAmount,
            callbackUrl: this.callbackUrl
        };
        const res = await ecPlant.cspos.createRefund(params);
        console.log(`res=${JSON.stringify(res)}`);

        this.refundOrderId = res.result.data.posRefundId;
        this.bizRefundId = params.bizRefundId;
        return this;
    }

    /**
     * pos退款回调
     */
    async orderRefundCb() {
        const params = {
            appKey: this.appKey,
            appSecret: this.appSecret,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo,
            refundOrderId: this.refundOrderId,
            money: this.totalAmount,
            endTime: common.getCurrentTime(),
            refundId: -common.getRandomNumStr(20),
        };
        const res = await ecPlant.cspos.orderRefundCb(params);
        console.log(`res=${JSON.stringify(res)}`);

        // this.refundFlag = 1;//退款不会更新付款单状态 
        return this;
    }

    /**
     * 拼接退款单期望值
     */
    getRefundExp() {
        return {
            id: this.refundOrderId,
            money: this.totalAmount,
            bizRefundId: this.bizRefundId,
            payOrderDes: this.description,
            payOrder: {
                bizOrderId: this.bizOrderId,
                bizType: this.bizType,
                money: this.totalAmount,
                payFlag: this.payFlag,
                payId: this.payId,
                payType: this.payType,
                refundFlag: this.refundFlag,
            },
        };
    }

    /**
     * pos查询退款单列表
     * @description 
     * 1. 内部接口 高胖胖说只要验证返回数据即可
     * 2. 本接口未提供排序筛选功能,无法查询最新生成的数据进行校验
     */
    async findRefundList() {
        const dataList = await ecPlant.cspos.findRefundList({
            appKey: this.appKey,
            appSecret: this.appSecret,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo,
            // bizOrderId: this.id
        }).then(res => res.result.data.rows);
        expect(dataList, `pos查询退款单列表数据为空`).to.have.lengthOf.above(0);
        // console.log(`dataList=${JSON.stringify(dataList)}`);
        // const detail = dataList.find(data => data.id == this.refundOrderId);
        // expect(detail, `pos查询退款单列表 未查询到单据refundOrderId=${this.refundOrderId}`).not.to.be.undefined;
        // return detail;
    }

    /**
     * pos获取退款单详情
     */
    async getRefundDetail() {
        const res = await ecPlant.cspos.getRefundDetail({
            appKey: this.appKey,
            appSecret: this.appSecret,
            refundOrderId: this.refundOrderId,
            merchantNo: this.merchantNo,
            deviceNo: this.deviceNo
        });
        return res.result.data;
    }
};

posManage.setUpPayBill = function (params) {
    const payOrder = new PayOrder();
    payOrder.setAuthParams(params);
    return payOrder;
};

/** 
 * 模拟生成pos支付单参数 
 */
posManage.mockOrderJson = function (params = {}) {
    const json = {
        channelCode: '11566522732557',
        bizOrderId: `autoPos${new Date().getTime()}`,
        totalAmount: common.getRandomNum(100, 500),
        bizType: 2100,
        callbackUrl: 'http://dev.hzdlsoft.com/pay/citic/callback.do',
        tenantId: 925,
        unitId: 921,
        shopId: 107273,//需要绑定pos机的shopId
        payType: 1,
        description: {
            shopper: '啦啦啦',
            custName: 'lqq',
            custMobile: 12908242188,
            amount: common.getRandomNum(1, 5),
            unit: '件'
        }
    };
    return common.update(json, params);
};