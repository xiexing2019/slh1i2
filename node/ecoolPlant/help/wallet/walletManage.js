const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');

class Wallet {
    constructor(params) {
        /** 商户id */
        this.merchantId = params.merchantId;
        this.useAcctId = '';
        /** 处理中 */
        this.bankDealingMoney = 0;
        /** 已提现金额 */
        this.totalWithdrawMenoy = 0;
        this.totalBalMoney = 0;
        /** 总金额 */
        this.totalMoney = 0;
        /** 待结算金额 */
        this.settlementMoney = 0;
        /** 可提现金额 */
        this.withdrawableMoney = 0;
    }

    /**
     * 创建收款单
     * @param {*} params  
     * @param {number} params.payMoney 此笔支付金额
     */
    async createPayBill(params) {
        const res = await ecPlant.fin.createPayBill({ merchantId: this.merchantId, payType: 1, ...params });
        console.log(`\n创建收款单=${JSON.stringify(res)}`);
        const key = res.result.data.qrCodeUrl.split('key=')[1];
        console.log(key);
        //支付
        const getUnionPay = await ecPlant.pay.getUnionPay({
            openId: "o42Bm1gQrfb9AtZpkUz8SRpovrL4",
            key: key,
            payType: 1048576,
        });
        console.log(`\n支付=${JSON.stringify(getUnionPay)}`);
        this.totalBalMoney = common.add(this.totalBalMoney, params.payMoney);
        this.totalMoney = common.add(this.totalMoney, params.payMoney);
        this.settlementMoney = common.add(this.settlementMoney, params.payMoney);
        return res.result.data;
    }

    /**
     * 支付入账解冻，解冻的场景是拼团支付
     */
    async unfreezeBizOrder(params) {
        const res = await ecPlant.fin.unfreezeBizOrder({ jsonParam: [{ merchantId: this.merchantId, payType: params.payType, bizType: 1260, bizOrderId: params.bizOrderId, bizSubOrderId: params.bizSubOrderId }] });
        console.log(`\n创建收款单=${JSON.stringify(res)}`);
    }

    /**
     * 创建提现申请单
     * @param {*} params 
     * @param {object} [params.tenantId] 租户id
     * @param {object} params.money 提现金额
     */
    async createCashBill(params) {
        const res = await ecPlant.fin.createCashBill({ merchantId: this.merchantId, ...params });
        console.log(`\n创建提现申请单=${JSON.stringify(res)}`);

        this.totalBalMoney = common.sub(this.totalBalMoney, params.money);
        this.totalMoney = common.sub(this.totalMoney, params.money);
        this.totalWithdrawMenoy = common.sub(this.settlementMoney, params.money);
        this.withdrawableMoney = common.sub(this.settlementMoney, params.money);
        return res.result.data.val;
    }

    /**
     * 重新提现
     * @param {*} params 
     * @param {object} params.id 提现单id
     */
    async afreshWithdrawCashBill(params) {
        const res = await ecPlant.fin.afreshWithdrawCashBill({ merchantId: this.merchantId, ...params });
        console.log(`\n重新提现=${JSON.stringify(res)}`);
        return res;
    }

    /**
     * 获取提现记录
     * @param {*} params 
     * @param {object} params.merchantId 商户id
     * @param {object} [params.billNo] 提现编号
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     */
    async findFinCashBill(params) {
        const res = await ecPlant.fin.findFinCashBill({ merchantId: this.merchantId, ...params });
        console.log(`\n获取提现记录=${JSON.stringify(res)}`);
    }

    /**
     * 获取提现详情
     * @param {*} params 
     * @param {object} params.merchantId 商户id
     * @param {object} params.id 提现编号
     */
    async getfinCashBillById(params) {
        const res = await ecPlant.fin.getfinCashBillById({ merchantId: this.merchantId, ...params });
        console.log(`\n获取提现详情=${JSON.stringify(res)}`);
        return res.result.data;
    }

    /**
     * 获取钱包首页数据
     * @param {*} opt 
     * @param {*} opt.update 
     */
    async getHomePageMoney(opt = { update: false, assert: false, money: 0 }) {
        const res = await ecPlant.fin.getHomePageMoney({ merchantId: this.merchantId });
        console.log(`\n钱包首页数据=${JSON.stringify(res)}`);
        if (opt.assert) common.isApproximatelyEqualAssert(this, res.result.data);
        if (opt.update) common.update(this, res.result.data);
    }

    /**
     * 获取账户流水
     * @param {object} params
     * @param {object} [params.orderId] bizOrderDetailId
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     * @param {object} [params.pageNo] 分页页码
     * @param {object} [params.pageSize] 分页条数
     */
    async findFinAcctFlow(params = {}) {
        params = Object.assign({
            startDate: common.getCurrentDate(),
            endDate: common.getCurrentDate()
        }, params);
        const res = await ecPlant.fin.findFinAcctFlow({ merchantId: this.merchantId, ...params });
        console.log(`\n获取账户流水=${JSON.stringify(res)}`);
    }

    /**
     * 按日期获取账户日结总金额
     * @param {object} params
     */
    async findFinAcctDaily(params = {}) {
        params = Object.assign({
            startDate: common.getCurrentDate(),
            endDate: common.getCurrentDate()
        }, params);
        const res = await ecPlant.fin.findFinAcctDaily({ merchantId: this.merchantId, ...params });
        console.log(`\n按日期获取账户日结总金额=${JSON.stringify(res)}`);
        return res.result.data.rows;
    }

    /**
     * 获取财务流水日结概览
     * @param {object} params
     * @param {object} [params.ioFlag] 0-收入；1-支出
     * @param {object} [params.dateType] 1-日，2-月， 3-年
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     */
    async findFinAcctFlowDailyOverview(params = {}) {
        params = Object.assign({
            ioFlag: 0,
            dateType: 1,
            startDate: common.getCurrentDate(),
            endDate: common.getCurrentDate()
        }, params);
        const res = await ecPlant.fin.findFinAcctFlowDailyOverview({ merchantId: this.merchantId, ...params });
        console.log(`\n获取财务流水日结概览=${JSON.stringify(res)}`);
        return res.result.data;
    }

    /**
     * 获取财务流水日结
     * @param {object} params
     * @param {object} [params.ioFlag] 0-收入；1-支出
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     */
    async findFinAcctFlowDaily(params = {}) {
        params = Object.assign({
            ioFlag: 0,
            startDate: "2019-09-29",
            endDate: "2019-10-09"
        }, params);
        const res = await ecPlant.fin.findFinAcctFlowDaily({ merchantId: this.merchantId, ...params });
        console.log(`\n获取财务流水日结=${JSON.stringify(res)}`);
    }

    /**
     * 获取流水总金额
     * @param {object} params
     * @param {object} [params.ioFlag] 0-收入；1-支出
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     */
    async getTotalFlowMoney(params = {}) {
        params = Object.assign({
            ioFlag: 0,
            startDate: common.getCurrentDate(),
            endDate: common.getCurrentDate()
        }, params)
        const res = await ecPlant.fin.getTotalFlowMoney({ merchantId: this.merchantId, ...params });
        console.log(`\n获取流水总金额=${JSON.stringify(res)}`);
        return res.result.data.val;
    }

}

const walletManage = module.exports = {};

/**
 * 初始化
 * @param {*} params 
 * @param {*} params.merchantId
 */
walletManage.setupWallet = function (params) {
    return new Wallet(params);
};