const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const plantAccount = require('../../data/plantAccount');


/** 账户类型 */
const acctType = {
    // 资产类
    /** 中信电商管家存管在途账户 */
    transitCITIC: 1101,
    /** 中信电商管家存管应收账户 */
    receiveCITIC: 1102,
    /** 民生中投存管在途账户 */
    transitMSFPAY: 1103,
    /** 民生中投存管应收账户 */
    receiveMSFPAY: 1104,
    /** 中信电商管家存管可用账户 */
    withdrawCITIC: 1200,
    /** 民生中投存管可用账户 */
    withdrawMSFPAY: 1201,
    /** 微信支付存管可用账户 */
    withdrawWEIXIN: 1203,

    // 负债类
    /** 中信存管可提对冲账户(退款入账解冻可提余额不足计入该账户) */
    withdrawHedgeCITIC: 2310,
    /** 民生中投存管可提对冲账户 */
    withdrawHedgeMSFPAY: 2311,
    /** 中信存管应收对冲账户(退款入账解冻应收余额不足计入该账户) */
    receiveHedgeCITIC: 2320,
    /** 民生中投存管应收对冲账户 */
    receiveHedgeMSFPAY: 2321,
};

/**
 * 获取渠道账户类型
 * @param {object} params 
 * @param {string} params.acct 账号类型 receive:应收 receiveHedge:应收对冲 withdraw:可提 withdrawHedge:可提对冲
 * @param {string} params.channel 渠道名称
 */
function getAcctType({ acct, channelCode = 'CITIC' }) {
    return acctType[`${acct}${channelCode}`];
};
function getAllAcctTypeByAcct(acct) {
    if (typeof acct == 'string') acct = [acct];
    const arr = [];
    for (const key in acctType) {
        if (acct.some(ele => key.match(`${ele}(?!Hedge)`))) arr.push(acctType[key]);
    }
    return arr;
};

/**
 * 商户
 */
class Merchant {
    constructor(params) {
        /** 商户id */
        this.merchantId = params.merchantId;
        /** 银行卡id */
        this.merBankId = '';
        /** 财务账户 */
        this.finAcct = {};

        /**  物理主键 */
        this.id = '';
        /**  客户编号 */
        this.slhSn = '';
        /**  平台商户号（sn或自定义） */
        this.mchntNo = '';
        /**  进件成功或失败时回调地址 */
        this.callbackUrl = '';
        /**  已成功进件的第三方支付平台 */
        this.enterPlatform = '';
        /**  必须成功的第三方支付平台 */
        this.mustPlatform = '';
        /**  开通状态 */
        this.enterFlag = 0;
        /**  租户老板的userId */
        this.ownerUserId = '';
        /**  子商户类型 1-企业商户 2-个体工商户 3-小微商户 */
        this.subMchType = 0;
        /**  子商户名称 */
        this.subMchName = '';
        /**  子商户简称 */
        this.subMchShortName = '';
        /**  子商户地址 */
        this.subMchAddress = '';
        /**  子商户营业执照号 */
        this.subMchLicenseNo = '';
        /**  子商户营业执照有效期yyyyMMdd */
        this.subMchLicenseValid = '';
        /**  身份证正面(衣科地址) */
        this.ownerIdCardFront = '';
        /**  身份证背面(衣科地址) */
        this.ownerIdCardBack = '';
        /**  店铺门牌照(衣科地址) */
        this.shopAppearDoc = '';
        /**  店铺内照片(衣科地址) */
        this.shopInsideDoc = '';
        /**  负责人姓名 */
        this.ownerName = '';
        /**  负责人电话 */
        this.ownerPhone = '';
        /**  负责人邮箱 */
        this.ownerEmail = '';
        /**  负责人身份证号 */
        this.ownerIdCard = '';
        /**  负责人身份证有效期yyyyMMdd */
        this.ownerIdCardValid = '';
        /**  产品编号 */
        this.productCode = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  备注说明 */
        this.rem = '';
    }


    /**
     * 初始化商户信息
     */
    async initMerchantInfo() {
        const merchant = await this.findFinMerchantFullsById();
        common.update(this, merchant);
    }

    /**
     * 新增商户
     * @param {*} params 
     */
    async newMerchant(params) {
        const data = await ecPlant.merfin.saveMerchantAndAccount(params).then(res => res.result.data);
        console.log(`\n保存商户和账户信息=${JSON.stringify(data)}`);
        this.merchantId = data.merchantId;
        this.merBankId = data.merBankId;
    }


    /**
     * 商户禁用、启用
     * @param {object} params 
     * @param {object} params.merchantIds 商户Id， 多个以逗号间隔
     * @param {object} params.status 状态变更： 0 禁用，1 启用
     * @param {object} params.msg 说明，解释
     */
    async changeMerchantStatus(params = {}) {
        const res = await ecPlant.merfin.changeMerchantStatus({ merchantIds: this.merchantId, status: 1, msg: 'autotest', ...params });
        console.log(`\n商户禁用、启用=${JSON.stringify(res)}`);
        this.enterFlag = params.status == 0 ? 3 : (this.enterPlatform ? 1 : 0); //fin_merchant 表的enter_flag字段值变动（3 禁用，0、1 非禁用【0 商户无认证渠道，1 商户有认证渠道】
    }

    /**
     * 商户列表查询
     * @param {object} params 
     */
    async findFinMerchantsById() {
        const finMerchants = await ecPlant.merfin.findFinMerchants({ merchantId: this.merchantId }).then(res => res.result.data.rows);
        console.log(`\n商户列表查询=${JSON.stringify(finMerchants)}`);
        return finMerchants.find(obj => obj.id == this.merchantId);
    }

    /**
     * 商户详情列表查询
     * @param {object} params 
     */
    async findFinMerchantFullsById() {
        const finMerchantFulls = await ecPlant.merfin.findFinMerchantFulls({ merchantId: this.merchantId }).then(res => res.result.data.rows);
        console.log(`\n商户详情列表查询=${JSON.stringify(finMerchantFulls)}`);
        return finMerchantFulls.find(obj => obj.id == this.merchantId);
    }

    /**
     * 子商户列表查询
     * @description 查询条件都不传的话，结果为空
     * @param {object} params 
     * @param {object} [params.merchantId] 主商户商户号
     * @param {object} [params.ownerName] 主商户名称（不提供模糊搜索）
     * @param {object} [params.subName] 子成员商户名称
     * @param {object} [params.subId] 业务层标识
     * @param {object} [params.linkStartTime] 关联日期-开始日期,例如：2019-11-11
     * @param {object} [params.linkEndTime] 关联日期-结束日期,例如：2019-11-11
     * @param {object} [params.pageNo] 当前页，默认1
     * @param {object} [params.pageSize] 每页条数，默认20
     */
    async finSubMerchantsByWeb(params) {
        const subMerchants = await ecPlant.submer.finSubMerchantsByWeb({ merchantId: this.merchantId, ...params });
        console.log(`\n子商户列表查询=${JSON.stringify(subMerchants)}`);
        return subMerchants.result.data.rows;
    }

    /**
     * 商户列表 校验
     * @param {*} con 
     */
    async findFinMerchantsAssert() {
        const actual = await this.findFinMerchantsById();
        // console.log('\n校验', JSON.stringify(this));
        common.isApproximatelyEqualAssert(this, actual, [], JSON.stringify(this));
    }

    /**
     * 商户详情列表 校验
     * @param {*} con 
     */
    async findFinMerchantFullsAssert() {
        const actual = await this.findFinMerchantFullsById();
        // console.log('\n校验', this);
        common.isApproximatelyEqualAssert(this, actual, [], JSON.stringify(this));
    }

    /**
     * 查询当前商户财务账户信息
     * @param {*} con 数据库连接
     */
    async selectFinAcct(con) {
        const dbResult = await con.query(`SELECT * FROM merfin001.fin_acct WHERE unit_id = ${this.merchantId}`).then(res => res[0]);
        console.log(`\n当前商户财务账户信息 fin_acct=${JSON.stringify(dbResult)}`);
        return dbResult;
    }

    /**
     * 更新this.finAcct(财务账户)
     * @param {Object[]} accts 
     */
    async updateFinAcct(accts) {
        accts.forEach(acct => {
            const account = this.finAcct[acct.acct_type] || new FinAcct();
            this.finAcct[acct.acct_type] = account.update(acct);
        });
    }

    /**
     * 初始化财务账户
     * @param {*} con 数据库连接
     */
    async initMerchant(con) {
        this.finAcct = {};
        const dbResult = await this.selectFinAcct(con);
        this.updateFinAcct(dbResult);
    }

    /**
     * 数据库更新各财务账户
     * @description 不传则不更新 
     * @param {object} con 数据库连接
     * @param {object} accts 
     * @param {Number} [accts.transit] 在途账户(已废弃)
     * @param {Number} [accts.receive] 应收账户 
     * @param {Number} [accts.receiveHedge] 应收对冲账户
     * @param {Number} [accts.withdraw] 可提账户
     * @param {Number} [accts.withdrawHedge] 可提对冲账户
     */
    async updateFinAcctDB(con, accts) {
        for (const acct of Object.keys(accts)) {
            const acctType = getAllAcctTypeByAcct(acct);
            // console.log('数据库更新 要修改的账户类型', acctType);
            const dbResult = await con.execute(`UPDATE merfin001.fin_acct SET bal_money = ${accts[acct]}, blk_money = ${0} WHERE unit_id = ${this.merchantId} AND acct_type IN (${acctType.toString()})`).then(res => res[0]);
            // console.log(dbResult);
        }
        await this.initMerchant(con);
    }

    /**
     * 账户余额变更
     * @param {Object} acct 
     * @param {Object} acct.acctType 账户类型
     * @param {Object} acct.amount 改变的金额
     */
    async saveFinAcctFlowInfo(acct) {
        if (acct.amount == 0) return;
        const ioFlag = acct.amount > 0 ? 0 : 1;
        const res = await ecPlant.fin.saveFinAcctFlowInfo({
            merchantId: this.merchantId,
            tenantId: 0,
            acctType: acct.acctType,
            name: `autotest更变fin_acct值`,
            amount: Math.abs(acct.amount),  // 元
            bizType: 1241,
            bizOrderId: `${Date.now()}${common.getRandomNum(100, 999)}`,
            ioFlag: ioFlag,    // 收支标记：0-收入；1-支出
            tradeType: ioFlag ? 203 : 202    // 202 转入，203 转出
        });
        console.log(`\n账户余额变更=${JSON.stringify(res)}`);
    }

    /**
     * 修改财务账户的money
     * @param {*} channelCode 
     * @param {*} accts 
     */
    async updateFinAccts(channelCode = 'CITIC', accts = { receive: 0, receiveHedge: 0, withdraw: 0, withdrawHedge: 0 }) {
        for (const acct of Object.keys(accts)) {
            const acctActual = this.getAcctDate({ channelCode: channelCode, acct: acct });
            const acctType = getAcctType({ acct, channelCode });
            const amount = common.sub(accts[acct], acctActual.bal_money);
            await this.saveFinAcctFlowInfo({ acctType: acctType, amount: amount });
        }
    }

    /**
     * fin_acct 数据校验
     * @param {*} con 
     */
    async finAcctAssert(con) {
        const actual = {};
        await this.selectFinAcct(con).then(dbResult => dbResult.forEach(account => actual[account.acct_type] = account));
        console.log('数据校验', this.finAcct);
        common.isApproximatelyEqualAssert(this.finAcct, actual);
    }

    /**
     * 获取指定财务账户数据
     */
    getAcctDate({ acct, channelCode }) {
        const acctType = getAcctType({ acct, channelCode });
        const acctDate = this.finAcct[acctType];
        if (!acctDate) throw new Error(`[查询错误] 未查询到${channelCode}-${acctType}的财务账户信息`);
        return acctDate;
    }

    /**
     * 获取账户余额
     */
    getAcctTotalMoney() {
        const totalMoney = { totalBalMoney: 0, totalBlkMoney: 0 };
        Object.values(this.finAcct).forEach(acct => {
            // acct_type 1开头为资产类 2开头为负债类
            if (acct.acct_type < 2000) {
                totalMoney.totalBalMoney = common.add(totalMoney.totalBalMoney, acct.bal_money);
                totalMoney.totalBlkMoney = common.add(totalMoney.totalBlkMoney, acct.blk_money);
            } else {
                totalMoney.totalBalMoney = common.sub(totalMoney.totalBalMoney, acct.bal_money);
                totalMoney.totalBlkMoney = common.sub(totalMoney.totalBlkMoney, acct.blk_money);
            }
        });
        // totalMoney = { totalBalMoney: totalMoney.totalBalMoney.toFixed(2), totalBlkMoney: totalMoney.totalBlkMoney.toFixed(2) }
        return totalMoney;
    }

    /**
     * 入账 更新应收/应收对冲账户值
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    updateReceiveMoney({ channelCode, money }) {
        /** 应收账户 */
        const receiveAcct = this.getAcctDate({ acct: 'receive', channelCode });
        /** 应收对冲账户 */
        const receiveHedgeAcctType = this.getAcctDate({ acct: 'receiveHedge', channelCode });
        // 应收对冲账户抵扣值
        const _money = common.sub(receiveHedgeAcctType.bal_money, money);
        receiveHedgeAcctType.bal_money = _money >= 0 ? _money : 0;
        receiveAcct.bal_money = _money <= 0 ? common.sub(receiveAcct.bal_money, _money) : 0;
        return this;
    }

    /**
      * 对账 更新应收/应收对冲账户值
      * @param {Object} params 
      * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
      * @param {string} params.money 金额 单位元
      */
    unfreezeReceiveMoney({ channelCode, money }) {
        /** 应收账户 */
        const receiveAcct = this.getAcctDate({ acct: 'receive', channelCode });
        /** 应收对冲账户 */
        const receiveHedgeAcctType = this.getAcctDate({ acct: 'receiveHedge', channelCode });
        // 应收账户抵扣值
        const _money = common.sub(receiveAcct.bal_money, money);
        receiveAcct.bal_money = _money >= 0 ? _money : 0;
        receiveHedgeAcctType.bal_money = _money <= 0 ? common.sub(receiveHedgeAcctType.bal_money, _money) : 0;
        return this;
    }

    /**
      * 对账 更新应收/应收对冲账户值 退款
      * @param {Object} params 
      * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
      * @param {string} params.money 金额 单位元
      */
    unfreezeReceiveMoneyForRefund({ channelCode, money }) {
        /** 应收账户 */
        const receiveAcct = this.getAcctDate({ acct: 'receive', channelCode });
        /** 应收对冲账户 */
        const receiveHedgeAcctType = this.getAcctDate({ acct: 'receiveHedge', channelCode });
        // 应收账户抵扣值
        const _money = common.sub(receiveHedgeAcctType.bal_money, money);
        receiveHedgeAcctType.bal_money = _money >= 0 ? _money : 0;
        receiveAcct.bal_money = _money < 0 ? common.sub(receiveAcct.bal_money, _money) : 0;
        return this;
    }

    /**
     * 对账 更新可提/可提对冲账户值 支付
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    unfreezeWithdrawMoney({ channelCode, money }) {
        /** 可提账户 */
        const withdrawAcct = this.getAcctDate({ acct: 'withdraw', channelCode });
        /** 可提对冲账户 */
        const withdrawHedgeAcctType = this.getAcctDate({ acct: 'withdrawHedge', channelCode });
        console.log(withdrawHedgeAcctType);
        console.log(withdrawHedgeAcctType.bal_money);
        // 应收对冲账户抵扣值
        const _money = common.sub(withdrawHedgeAcctType.bal_money, money);
        withdrawHedgeAcctType.bal_money = _money >= 0 ? _money : 0;
        withdrawAcct.bal_money = _money <= 0 ? common.sub(withdrawAcct.bal_money, _money) : 0;
        return this;
    }

    /**
     * 对账 更新可提/可提对冲账户值 退款
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    unfreezeWithdrawMoneyForRefund({ channelCode, money }) {
        /** 可提账户 */
        const withdrawAcct = this.getAcctDate({ acct: 'withdraw', channelCode });
        /** 可提对冲账户 */
        const withdrawHedgeAcctType = this.getAcctDate({ acct: 'withdrawHedge', channelCode });
        // 应收对冲账户抵扣值
        const _money = common.sub(withdrawAcct.bal_money, money);
        withdrawAcct.bal_money = _money >= 0 ? _money : 0;
        withdrawHedgeAcctType.bal_money = _money < 0 ? common.sub(withdrawHedgeAcctType.bal_money, _money) : 0;
        return this;
    }

    /**
     * 入账 更新可提/可提对冲账户值
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    updateWithdrawMoney({ channelCode, money }) {
        /** 可提账户 */
        const withdrawAcct = this.getAcctDate({ acct: 'withdraw', channelCode });
        /** 可提对冲账户 */
        const withdrawHedgeAcctType = this.getAcctDate({ acct: 'withdrawHedge', channelCode });
        // 应收对冲账户抵扣值
        const _money = common.sub(withdrawHedgeAcctType.bal_money, money);
        withdrawHedgeAcctType.bal_money = _money >= 0 ? _money : 0;
        withdrawAcct.bal_money = _money <= 0 ? common.sub(withdrawAcct.bal_money, _money) : 0;
        return this;
    }

    /**
     * 退款入账 更新应收/可提账户值
     * @description 先扣应收,不足扣可提
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    updateRefundMoney({ channelCode, money }) {
        /** 应收账户 */
        const receiveAcct = this.getAcctDate({ acct: 'receive', channelCode });
        /** 可提账户 */
        const withdrawAcct = this.getAcctDate({ acct: 'withdraw', channelCode });
        // 应收账户抵扣剩余
        const _money = common.sub(receiveAcct.bal_money, money);
        receiveAcct.bal_money = _money >= 0 ? _money : 0;
        console.log(`应收不够，扣可提:${_money}`);
        _money < 0 && (withdrawAcct.bal_money = common.add(withdrawAcct.bal_money, _money));
        return this;
    }

    /**
     * 提现 更新可提账户值
     * @param {Object} params 
     * @param {string} params.channelCode 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA
     * @param {string} params.money 金额 单位元
     */
    updateWithdrawCash({ channelCode, money }) {
        /** 可提账户 */
        const withdrawAcct = this.getAcctDate({ acct: 'withdraw', channelCode });
        if (withdrawAcct.bal_money < money) throw new Error(`可提账户余额=${withdrawAcct.bal_money},提现金额=${money},余额不足请检查\n${JSON.stringify(withdrawAcct)}`);
        withdrawAcct.bal_money = common.sub(withdrawAcct.bal_money, money);
        return this;
    }

}


const merchantManage = module.exports = {};

/**
 * 初始化
 * @param {*} params 
 * @param {*} params.merchantId
 */
merchantManage.setupMerchant = function (params = {}) {
    return new Merchant(params);
};

/**
 * 财务账户
 */
class FinAcct {
    constructor() {
        /**  单元id */
        this.unit_id = '';
        /**  卖家租户或客户id */
        this.tenant_id = 0;
        /**  账户类型 */
        this.acct_type = 0;
        /**  账户名 */
        this.name = '';
        /**  属主类型1-租户自己 2-租户的客户 */
        this.owner_kind = 1;
        /**  账户属主，当owner_kind=1时指向租户老板的userId，=2时指向客户userId */
        this.owner_id = 0;
        /**  余额 */
        this.bal_money = 0;
        /**  其中冻结金额 */
        this.blk_money = 0;
        /**  状态 */
        this.flag = 1;
        /**  加密后的余额支付密码，可空 */
        this.account_password = '';
        // /**  版本 */
        // this.ver = 0;
        /**  扩展属性 */
        this.ext = {};
        /**  账户大类型 默认1 */
        this.acct_group_type = 1;
        /**  saas级别的ID */
        this.saas_id = 1;
    }

    update(params) {
        common.update(this, params);
        return this;
    }
};



