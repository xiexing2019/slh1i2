const common = require('../../../../lib/common');
const ecPlant = require('../../../../reqHandler/ecoolPlant');
const OrderBase = require('../order');

class Order extends OrderBase {
    constructor() {
        super();
        /**  金额，单位为元 */
        this.money = '';
        this.acctId = '';
    }
    /**
     * 提现结果回调反馈
     */
    async receiveWithdrawResult() {
        if (!this.transCode) throw new Error('不存在流水号transCode');
        const res = await ecPlant.account.receiveWithdrawResult({
            transCode: this.transCode,
            failedList: []
        });
        console.log(`\n回调结果反馈=${JSON.stringify(res)}`);
        this.status = 2;
        this.cashFlag = 2;
        this.orderFlag = 2;
    }

    /**
     * 创建提现申请单
     * @param {*} params 
     * @param {object} [params.tenantId] 租户id
     * @param {object} params.money 提现金额
     */
    async createCashBill(params) {
        const withdrawAcct = this.merchant.getAcctDate({ acct: 'withdraw', channelCode: this.channelCode });
        const res = await ecPlant.fin.createCashBill({ merchantId: this.merchant.merchantId, money: common.getRandomNum(10, withdrawAcct.bal_money), ...params });
        console.log(`\n创建提现申请单=${JSON.stringify(res)}`);
        if (!res.result.data) return res;
        const cashBill = res.result.data.rows[0];
        this.bizOrderId = cashBill.id;
        this.bizSubOrderId = cashBill.billNo;
        this.bizType = cashBill.bizType;
        this.acctId = cashBill.acctId;
        this.money = cashBill.money;
        this.orderFlag = 1;
        // this.cashFlag = 0;
        if (!this.bizSubOrderId) { //billNo为异步获取，需要轮询查询
            for (let index = 1; index <= 10; index++) {
                await common.delay(2000);
                const billInfo = await this.getfinCashBillById();
                console.log(`\n第${index}次获取提现详情=${JSON.stringify(billInfo)}`);
                if (billInfo.billNo) {
                    this.bizSubOrderId = billInfo.billNo;
                    break;
                }
            }
            if (!this.bizSubOrderId) throw new Error(`获取提现详情(forBillNo)超过10次:${this.bizOrderId}`);
        }
        this.cashFlag = 1;
        this.status = 1;
        this.merchant.updateWithdrawCash({ channelCode: this.channelCode, money: this.money });
        const totalMoney = this.merchant.getAcctTotalMoney();
        this.totalAcctMoney = { bal_money: totalMoney.totalBalMoney, blk_money: totalMoney.totalBlkMoney };
    }

    /**
     * 获取提现记录
     * @param {*} params 
     * @param {object} params.merchantId 商户id
     * @param {object} [params.billNo] 提现编号
     * @param {object} [params.startDate] 起始日期
     * @param {object} [params.endDate] 结束日期
     */
    async findFinCashBill(params) {
        const res = await ecPlant.fin.findFinCashBill({ merchantId: this.merchant.merchantId, billNo: this.bizSubOrderId, ...params });
        console.log(`\n获取提现记录=${JSON.stringify(res)}`);
        return res.result.data.rows.find(obj => obj.id == this.bizOrderId);
    }

    async finCashBillAssert(params) {
        const actual = await this.findFinCashBill();
        const totalMoney = this.merchant.getAcctTotalMoney();
        const exp = common.update({
            ...this,
            flag: this.cashFlag,
            id: this.bizOrderId,
            billNo: this.bizSubOrderId,
            remainMoney: totalMoney.totalBalMoney,
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * 获取提现详情
     * @param {*} params 
     * @param {object} params.merchantId 商户id
     * @param {object} params.id 提现编号
     */
    async getfinCashBillById(params) {
        const res = await ecPlant.fin.getfinCashBillById({ merchantId: this.merchant.merchantId, id: this.bizOrderId, ...params });
        console.log(`\n获取提现详情=${JSON.stringify(res)}`);
        return res.result.data;
    }

    async finCashBillByIdAssert(params) {
        const actual = await this.getfinCashBillById();
        const totalMoney = this.merchant.getAcctTotalMoney();
        const exp = common.update({
            ...this,
            flag: this.cashFlag,
            id: this.bizOrderId,
            billNo: this.bizSubOrderId,
            remainMoney: totalMoney.totalBalMoney
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * 提现申请单
     * @param {*} con 
     */
    async selectFinCashBill(con) {
        const dbResult = await con.query(`SELECT * FROM merfin001.fin_cash_bill WHERE id = ${this.bizOrderId} `).then(res => res[0]);
        console.log(`\nfin_cash_bill=${JSON.stringify(dbResult)}`);
        return dbResult[0];
    }

    async finCashBillDBAssert(con, params) {
        const actual = await this.selectFinCashBill(con);
        const exp = common.update({
            id: this.bizOrderId,
            unit_id: this.merchant.merchantId,
            flag: this.cashFlag,
            biz_type: this.bizType,
            acct_id: this.acctId,
            bill_no: this.bizSubOrderId,
            money: this.money
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * 提现明细表
     * @param {*} con 
     */
    async selectAccountWithdrawDetail(con) {
        const dbResult = await con.query(`SELECT * FROM merfin.acoount_withdraw_detail WHERE biz_order_id = ${this.bizOrderId} `).then(res => res[0]);
        console.log(`\nacoount_withdraw_detail=${JSON.stringify(dbResult)}`);
        this.transCode = Number(dbResult[0].trans_code);
        return dbResult[0];
    }

    async finAccountWithdrawDetailAssert(con, params) {
        const actual = await this.selectAccountWithdrawDetail(con);
        const exp = common.update({
            biz_order_id: this.bizOrderId,
            biz_type: this.bizType,
            amount: common.mul(this.money, 100),
            trans_code: this.transCode,
            status: this.status,
            unit_id: this.merchant.merchantId,
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * fin_acct_flow 数据校验
     * @param {*} con 
     * @param {*} params 
     */
    async finAcctFlowAssert(con, params) {
        const type201 = await this.finAcctFlowAssertBase(con, { trade_type: 201, ...params });
    }

}


// module.exports = Order;
const orderManage = module.exports = {};

/**
 * 初始化
 * @param {*} params 
 */
orderManage.setupOrder = function (params = {}) {
    return new Order(params);
};