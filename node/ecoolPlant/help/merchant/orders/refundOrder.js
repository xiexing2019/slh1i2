const common = require('../../../../lib/common');
const ecPlant = require('../../../../reqHandler/ecoolPlant');
const PayOrder = require('../orders/payOrder').Order;

class Order extends PayOrder {
    constructor() {
        super();
        this.refundOrder = new Map(); // {bizRefundId:0,refundAmount:0}
    }

    /**
     * 退款余额校验
     * @param {*} params 
     * @param {*} params.bizRefundId 退款业务单号
     * @param {*} params.refundAmount 退款金额，单位为分
     */
    async refundCheckBalance(params) {
        // params.refundAmount = this.leftMoney > params.refundAmount ? params.refundAmount : this.leftMoney;
        const res = await ecPlant.merfin.refundCheckBalance({
            bizRefundId: params.bizRefundId,//string	是	退款业务单号
            bizOrderId: this.bizOrderId,//	string	是	业务订单号
            bizType: this.bizType,//int	是	业务类型
            payType: this.payType,//int	是	支付类型
            callbackUrl: '',//	string	否	支付成功后的回调地址，通知将以Http的形式发送
            merchantId: this.merchant.merchantId,//long	是	商户号
            refundAmount: params.refundAmount,	//long	是	退款金额，单位为分
            ...params
        });
        console.log(`\n退款余额校验=${JSON.stringify(res)}`);
        return res.result;
    }

    /**
     * 余额充值后退款
     * @param {*} params 
     * @param {*} params.bizRefundId 退款业务单号
     * @param {*} params.refundAmount 退款金额，单位为分
     */
    async refund(params) {
        params.refundAmount = this.leftMoney > params.refundAmount ? params.refundAmount : this.leftMoney;
        if (this.isPayCB) {
            await this.receiveRefundResult(params);
        } else {
            const res = await ecPlant.merfin.refund({
                bizRefundId: params.bizRefundId,//string	是	退款业务单号
                bizOrderId: this.bizOrderId,//	string	是	业务订单号
                bizType: this.bizType,//int	是	业务类型
                payType: this.payType,//int	是	支付类型
                callbackUrl: '',//	string	否	支付成功后的回调地址，通知将以Http的形式发送
                merchantId: this.merchant.merchantId,//long	是	商户号
                refundAmount: params.refundAmount	//long	是	退款金额，单位为分
            });
            console.log(`\n退款=${JSON.stringify(res)}`);
        }
        const acctRefundMoney = [...this.refundOrder.values()].map(obj => obj.refundAmount - obj.merFee).reduce((a, b) => common.add(a, b), 0);
        this.status = 0;
        this.transactionType = 2;
        this.orderType = 2;
        this.orderFlag = 1;
        this.leftMoney -= params.refundAmount;
        this.bizSubOrderId = params.bizRefundId;

        this.merFee = common.mul(params.refundAmount, this.getServiceFeeRate('merFeeRate')).toFixed(0);
        const refundOrder = common.update(new RefundOrder(), Object.assign(params, { merFee: this.merFee }));
        this.refundOrder.set(refundOrder.bizRefundId, refundOrder);

        const serviceFeeRate = this.getServiceFeeRate(this.channelCode);
        const feeAmount = common.mul(params.refundAmount, serviceFeeRate).toFixed(0);
        this.serviceFee = { feeAmount: feeAmount, shareAmount: common.sub(this.merFee, feeAmount) };
        let receiveMoney = common.sub(params.refundAmount, this.merFee);
        console.log(`财务账户已退款(包括本次)的钱:${acctRefundMoney}`);
        if (this.receiveMoney <= acctRefundMoney) {
            receiveMoney = this.receiveMoney - acctRefundMoney;
            console.log('退所有钱');
        }

        /** 应收账户 */
        const receiveAcct = this.merchant.getAcctDate({ acct: 'receive', channelCode: this.channelCode });
        /** 可提账户 */
        // const withdrawAcct = this.merchant.getAcctDate({ acct: 'withdraw', channelCode: this.channelCode });
        const _money = common.sub(receiveAcct.bal_money, common.div(receiveMoney, 100));
        this.reconciliationMoney = _money >= 0 ? common.div(receiveMoney, 100) : receiveAcct.bal_money;

        // 更新应收/可提账户值
        this.merchant.updateRefundMoney({ channelCode: this.channelCode, money: common.div(receiveMoney, 100) });
        const totalMoney = this.merchant.getAcctTotalMoney();
        this.totalAcctMoney = { bal_money: totalMoney.totalBalMoney, blk_money: totalMoney.totalBlkMoney };
    }

    /**
     * 退款回调
     * @param {*} params 
     * @param {*} params.bizRefundId 退款业务单号
     * @param {*} params.refundAmount 退款金额，单位为分
     */
    async receiveRefundResult(params) {
        const res = await ecPlant.merfin.receiveRefundResult({
            code: 0,
            amount: params.refundAmount,
            // payNumber: "31209",
            extProp: { merFeeRate: this.getServiceFeeRate('merFeeRate'), callBackBizUrl: '', merchantId: this.merchant.merchantId },
            feeRate: common.mul(this.getServiceFeeRate(this.channelCode), 100),
            // srvCtxUrl__": "https://hzdev.hzdlsoft.com/merfin",
            callbackUrl: '',
            payId: this.payId, //payId要传payOrder表的id，这个要保持一致
            mainId: params.bizRefundId,
            payDate: common.getCurrentDate('YYYY-MM-DD HH:mm:ss'),
        });
        console.log(`\n支付回调=${JSON.stringify(res)}`);
        this.isPayCB = true;
    }

    /**
     * fin_acct_flow 数据校验
     * @param {*} con 数据库连接
     * @param {object} params 
     * @param {object} params.trade_type 
     */
    async finAcctFlowAssert(con, params) {
        await this.finAcctFlowAssertBase(con, { trade_type: 105, ...params });
    }
}

function RefundOrder() {
    /**  退款业务单号 */
    this.bizRefundId = '';
    /**  退款金额，单位为分 */
    this.refundAmount = 0;
    this.merFee = 0;
}

const orderManage = module.exports = { Order };

/**
 * 初始化
 * @param {*} params 
 */
orderManage.setupOrder = function (params = {}) {
    return new Order(params);
};