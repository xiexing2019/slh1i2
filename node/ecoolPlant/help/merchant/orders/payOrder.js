const common = require('../../../../lib/common');
const ecPlant = require('../../../../reqHandler/ecoolPlant');
const caps = require('../../../../../node/data/caps');
const OrderBase = require('../order');

class Order extends OrderBase {
    constructor() {
        super();
        /**  支付总金额，单位为分 */
        this.totalAmount = '';
        /**  支付类型 */
        this.payType = '';
        /**  可退款金额，单位为分 */
        this.leftMoney = 0;
        /**  支付状态(0,-1,1,-99) */
        this.payFlag = '';
        /** 是否支付回调 */
        this.isPayCB = false;
        /** payOrder表的id,退款回调用 */
        this.payId = '';
    }

    /**
     * 支付
     * @param {*} params 
     */
    async createPay(params) {
        const res = await ecPlant.merfin.createPay(params);
        console.log(`\n支付=${JSON.stringify(res)}`);
        if (res.result.code < 0) return res;
        common.update(this, params);
        this.payFlag = 1;   //支付回调，目前是中台处理
        this.transactionType = 1;
        this.orderType = 1;
        this.leftMoney = params.totalAmount;
        this.bizSubOrderId = params.bizOrderId;
        this.bizSubOrderId = params.bizOrderId;
        !this.channelCode && (this.channelCode = 'CITIC');
        params.distPlatformCode && (this.channelCode = params.distPlatformCode);

        // 手续费 单位:分
        this.merFee = common.mul(this.totalAmount, params.merFeeRate).toFixed(0);
        const serviceFeeRate = this.getServiceFeeRate(this.channelCode);
        const feeAmount = common.mul(this.totalAmount, serviceFeeRate).toFixed(0);
        this.serviceFee = { feeAmount: feeAmount, shareAmount: common.sub(this.merFee, feeAmount) };
        this.receiveMoney = common.sub(this.totalAmount, this.merFee);
        console.log(`卖家入账${this.receiveMoney},交易手续费${this.merFee}分`);


        // await this.receivePayResult();
        caps.name.includes('online') && await this.receivePayResult();
        // 更新应收/应收对冲账户值
        this.merchant.updateReceiveMoney({ channelCode: this.channelCode, money: common.div(this.receiveMoney, 100) });
        const totalMoney = this.merchant.getAcctTotalMoney();
        this.totalAcctMoney = { bal_money: totalMoney.totalBalMoney, blk_money: totalMoney.totalBlkMoney };
    }

    /**
     * 支付回调
     * @param {*} params 
     */
    async receivePayResult(params = {}) {
        const res = await ecPlant.merfin.receivePayResult({
            code: 0,
            mainId: this.bizOrderId,
            bizType: this.bizType,
            payNumber: common.getRandomNumStr(7),
            extProp: { rem: '订单入账-autotest', freeze: 0, merFeeRate: this.getServiceFeeRate('merFeeRate'), merchantId: this.merchant.merchantId, platformCode: this.channelCode, callBackBizUrl: '' },
            feeRate: common.mul(this.getServiceFeeRate(this.channelCode), 100),
            payType: this.payType,
            channelCode: "20971521553236802954",
            amount: this.totalAmount,
            // payId: common.getRandomNumStr(25),
            payDate: common.getCurrentDate('YYYYMMDDHHMMSS')
        });
        console.log(`\n支付回调=${JSON.stringify(res)}`);
        this.isPayCB = true;
        this.payId = res.params.payNumber;
        this.bizType == 1261 && (this.receiptFlag = 1);
    }

    /**
     * 支付订单表
     * @param {*} con 
     */
    async selectPayOrder(con) {
        const dbResult = await con.query(`SELECT * FROM pay.pay_order WHERE biz_order_id = ${this.bizOrderId} And biz_type = ${this.bizType}`).then(res => res[0]);
        console.log(`\npay_order=${JSON.stringify(dbResult)}`);
        return dbResult[0];
    }

    /**
     * pay_order 数据校验
     * @param {*} con 
     * @param {*} params 
     */
    async payOrderAssert(con, params = {}) {
        const actual = await this.selectPayOrder(con);
        let refund_flag = 0;
        if (this.transactionType == 1 || this.isPayCB) {
            refund_flag = 0;
        } else if (this.transactionType == 2 && this.leftMoney == 0) {
            refund_flag = 1;
        } else {
            refund_flag = 2;
        };
        const exp = common.update({
            biz_order_id: this.bizOrderId,
            biz_type: this.bizType,
            money: this.totalAmount,
            pay_type: this.payType,
            pay_flag: this.isPayCB ? 0 : this.payFlag,
            refund_flag: refund_flag,
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * 生成支付参数
     * @param {object} params 
     */
    mockPayJson(params = {}) {
        this.bizOrderId = Number(`${Date.now()}${common.getRandomNum(100, 999)}`);
        // this.bizOrderId = Number(`${common.getCurrentDate('MMDDHH')}${common.getRandomNum(100, 999)}`);
        return orderManage.mockPayJson(Object.assign(params, { bizOrderId: this.bizOrderId, merchantId: this.merchant.merchantId }));
    }

    /**
     * fin_acct_flow 数据校验
     * @param {*} con 
     * @param {*} params 
     */
    async finAcctFlowAssert(con, params) {
        const type100 = this.finAcctFlowAssertBase(con, { trade_type: 100, ...params });
        const type103 = this.finAcctFlowAssertBase(con, { trade_type: 103, ...params });
        await type100;
        await type103;
    }
}

const orderManage = module.exports = { Order };

/**
 * 初始化
 * @param {*} params 
 */
orderManage.setupOrder = function (params = {}) {
    return new Order(params);
};

/**
 * 支付订单表
 * @param {object} params 
 * @param {number} params.bizOrderId 业务订单号
 * @param {number} params.merchantId 商户号
 * @param {object[]} params.profitList 分润信息（不用传本merchantId分润，所有方分润后剩下的归该merchantId）；如果是按比例，相对于totalAmount而言
 */
orderManage.mockPayJson = function name(params = {}) {
    const payJson = common.update({
        payMethodType: 5,
        payerOpenId: "o42Bm1gQrfb9AtZpkUz8SRpovrL4",//	string	是	买家openId
        bizOrderId: '',//	string	是	业务订单号
        totalAmount: common.getRandomNum(5000, 10000),//bigdecimal	是	支付总金额，单位为分
        bizType: 1241,//int	是	业务类型
        payType: 1048576,//int	是	支付类型
        description: '商陆花平台交易',//string	是	商品描述
        // callbackUrl: 'http://hzdev.hzdlsoft.com/pay',//	string	否	支付成功后的回调地址，通知将以Http的形式发送
        merchantId: '',//long	是	商户号
        goodsDetail: { productlist: [{ productname: "牙刷", productnum: 2, productamt: 0.5 }, { productname: '电脑', productnum: 2, productamt: 4000 }] },//string	是	商品详情，支付时将展示{“productlist”:[{“productname”:”牙刷”,”productnum”:”2”,”productamt”:”0.5”},{“productname”:”电脑”,”productnum”:”2”,”productamt”:”4000”}]}
        distPlatformCode: '',//清分平台，中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA 微信企业付款-WXSP 若为空中台根据内部规则自动选择一个清分平台；已知清分平台后，尽可能推导出使用哪个支付渠道
        appId: caps.name.includes('online') ? 'wxc09fcf8155edcecd' : 'wxbf7dc3cc240d8d1e',//	string	是	小程序appId
        // channelCode: '',//string	否	应用层可以明确指定一个支付渠道号
        // platformCode: '',//	string	否	即payPlatFormCode 支付平台，中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA 最终要废弃
        sceneKind: 0,//业务场景	是	0-默认，1-推广代收微信付款分佣
        merFeeRate: 0.003,//BigDecimal	否	商户费率，第三方支付渠道费率+平台分成费率；不传默认为千3（0.003）
        // profitList: [
        //     // {//	list	否	分润信息（不用传本merchantId分润，所有方分润后剩下的归该merchantId）；如果是按比例，相对于totalAmount而言
        //     //     subMerchantId: ,//long	是	子成员的商户号（例如：推广员等）
        //     //     profitKind: ,//	Integer	是	0-按比例 1-绝对值
        //     //     profitValue: ,//分润比例	是	profitKind=0时代表分润比例 =1时代表分润金额
        //     // }
        // ],
        extProp: {//	string	否	额外参数
            freeze: 0,//Integer	否	是否冻结：1 是，0或不传，则不冻结, 如冻结，则业务端调用解冻接口
            rem: 'autotest',//Integer	否	资产流水说明
            acctIn: 1//	Integer	否	是否入账：1 是，0或不传，则不入账  暂无用处
        }
    }, params);
    common.update(payJson.extProp, params);
    return payJson;
};