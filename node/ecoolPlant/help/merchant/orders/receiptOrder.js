const common = require('../../../../lib/common');
const ecPlant = require('../../../../reqHandler/ecoolPlant');
const caps = require('../../../../../node/data/caps');
// const OrderBase = require('../order');
const RefundOrder = require('../orders/refundOrder').Order;

class Order extends RefundOrder {
    constructor() {
        super();
        this.payBillId = '';
        /**  金额，单位为元 */
        this.payMoney = '';
    }

    /**
     * 创建收款单
     * @param {*} params  
     * @param {number} params.payMoney 此笔支付金额
     */
    async createReceiptBill(params = { payMoney: common.getRandomNum(50, 100) }) {
        const res = await ecPlant.fin.createPayBill({
            bizOrderId: this.bizOrderId,
            merchantId: this.merchant.merchantId,
            payType: 1,
            payMoney: params.payMoney,
            ...params
        });
        console.log(`\n创建收款单=${JSON.stringify(res)}`);
        const key = res.result.data.qrCodeUrl.split('key=')[1];
        console.log(key);
        this.bizType = 1260;
        this.receiptFlag = 0;
        this.updateReceiptBill({ payBillId: res.result.data.payBillId, payMoney: params.payMoney })
        return key;
    }

    /**
     * 创建收款单(退款充值)
     * @param {*} params  
     * @param {number} params.payBillId 
     * @param {number} params.bizType 
     * @param {number} params.payMoney 此笔支付金额
     */
    updateReceiptBill(params) {
        this.oldBizOrderId = this.bizOrderId;

        this.payMoney = params.payMoney;
        this.payBillId = params.payBillId;
        this.bizOrderId = this.payBillId;
        this.bizSubOrderId = this.payBillId;
        this.bizType = params.bizType;
        this.totalAmount = common.mul(this.payMoney, 100);
    }

    /**
     * 聚合支付
     * @param {object} params
     * @param {string} params.key
     * @param {string} [params.openId]
     * @param {string} [params.payType]
     */
    async createUnionPay(params) {
        const res = await ecPlant.merfin.createPay(common.update({
            payMethodType: 1,
            payerOpenId: "o42Bm1gQrfb9AtZpkUz8SRpovrL4",
            unionPayKey: params.key,
            payType: 1048576,//支付类型，微信：1048576，支付宝：2
        }, params));
        console.log(`\n支付=${JSON.stringify(res)}`);
        this.transactionType = 1;
        this.orderType = 1;
        this.payFlag = 1;
        this.receiptFlag = 0;
        this.bizType = 1261;
        // this.status = 1;
        // this.orderFlag = 0;

        // 手续费 单位:分
        this.merFee = common.mul(this.totalAmount, this.getServiceFeeRate('merFeeRate')).toFixed(0);
        const serviceFeeRate = this.getServiceFeeRate(this.channelCode);
        const feeAmount = common.mul(this.totalAmount, serviceFeeRate).toFixed(0);
        this.serviceFee = { feeAmount: feeAmount, shareAmount: common.sub(this.merFee, feeAmount) };
        this.receiveMoney = common.sub(this.totalAmount, this.merFee);
        console.log(`卖家退款充值入账${this.receiveMoney},交易手续费${this.merFee}分`);

        // await this.receivePayResult();
        caps.name.includes('online') && await this.receivePayResult();
        // 更新应收/应收对冲账户值
        this.merchant.updateReceiveMoney({ channelCode: this.channelCode, money: common.div(this.receiveMoney, 100) });
        const totalMoney = this.merchant.getAcctTotalMoney();
        this.totalAcctMoney = { bal_money: totalMoney.totalBalMoney, blk_money: totalMoney.totalBlkMoney };
        // 服务端自动触发退款
        // this.merchant.updateRefundMoney({ channelCode: this.channelCode, money: common.div(this.receiveMoney, 100) });
    }

    /**
     * 收款单表
     * @param {*} con 
     */
    async selectFinReceiptBill(con) {
        const dbResult = await con.query(`SELECT * FROM merfin001.fin_receipt_bill WHERE id = ${this.payBillId} `).then(res => res[0]);
        console.log(`\nfin_receipt_bill=${JSON.stringify(dbResult)}`);
        return dbResult[0];
    }

    /**
     * fin_acct_flow 数据校验
     * @param {*} con 
     * @param {*} params 
     */
    async finAcctFlowAssert(con, params) {
        const type100 = this.finAcctFlowAssertBase(con, { trade_type: 100, ...params });
        const type103 = this.finAcctFlowAssertBase(con, { trade_type: 103, ...params });
        await type100;
        await type103;
    }

    /**
     * fin_receipt_bill 数据校验
     * @param {*} con 
     * @param {*} params 
     */
    async finReceiptBillAssert(con, params = {}) {
        const actual = await this.selectFinReceiptBill(con);
        const exp = common.update({
            id: this.bizOrderId,
            biz_order_id: this.oldBizOrderId,
            biz_type: this.bizType,
            money: this.payMoney,
            unit_id: this.merchant.merchantId,
            flag: this.receiptFlag,
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }
}


// module.exports = Order;
const orderManage = module.exports = {};

/**
 * 初始化
 * @param {*} params 
 */
orderManage.setupOrder = function (params = {}) {
    return new Order(params);
};