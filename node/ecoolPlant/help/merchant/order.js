const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');

/** 渠道手续费费率 */
const serviceFeeRate = {
    /** 总费率 */
    merFeeRate: 0.003,
    /** 中信手续费费率 */
    CITIC: 0.0023,
    /** 民生(中投)手续费费率 */
    MSFPAY: 0.0022,
}

class Order {
    constructor() {
        /** 业务类型 */
        this.bizType = 1241;
        /** 业务主键 */
        this.bizOrderId = '';
        /** 业务子健 支付后=bizOrderId，退款是bizRefundId，提现是billNo*/
        this.bizSubOrderId = '';
        /** 流水号 */
        this.transCode = '';
        /** 商户信息 */
        this.merchant = '';
        /** 第三方支付平台编号 中信-CITIC 嘉联-JL 中投科信-MSFPAY 腾讯聚鑫-TDEA */
        this.channelCode = 'CITIC';
        /** 支付订单交易类型，1-支付 2-退货 空格-其他 */
        this.transactionType = '';
        /** 订单交易类型，1 -实时交易支付 2 -实时交易退货 3 -预付交易支付 4 -预付交易撤销 5 -预付交易完成 */
        this.orderType = ''

        this.status = 0;
        this.orderFlag = 0;
        this.receiptFlag = 0;
        this.cashFlag = 0;
        /** 下单后财务账户总余额 */
        this.totalAcctMoney = { bal_money: 0, blk_money: 0 };
    }

    /**
     * 提交结算申请
     */
    async commitBizOrderDetail() {
        const today = common.getCurrentDate('YYYYMMDD');
        for (let index = 1; index <= 10; index++) {
            await common.delay(3000);
            const res = await ecPlant.account.commitBizOrderDetail({
                merchantId: this.merchant.merchantId,
                // bizOrderIds: [1571982212673287]
                bizType: this.bizType,
                bizOrderId: this.bizOrderId,
                bizSubOrderId: this.bizSubOrderId
            });
            console.log(`\n第${index}次提交结算申请=${JSON.stringify(res)}`);
            if (res.result.data) {
                // this.transCode = res.result.data[today];
                this.transCode = Object.values(res.result.data)[0];
                break;
            }
        }
        if (!this.transCode) throw new Error(`提交结算申请获取流水号超过10次,bizOrderId=${this.bizOrderId}`);
        console.log('\n提交结算申请获取流水号:', this.transCode);
        this.status = 1;
        this.receiptFlag = 1;
    }

    /**
     * 回调结果反馈
     */
    async receiveRecordedResult() {
        if (!this.transCode) throw new Error('不存在流水号transCode');
        const res = await ecPlant.account.receiveRecordedResult({
            unitId: this.merchant.merchantId,
            transCode: this.transCode,
            result: 0,  //对账结果, 0成功，其他失败
        });
        console.log(`\n回调结果反馈=${JSON.stringify(res)}`);
        this.status = 2;
        this.orderFlag = this.transactionType == 2 ? 4 : 2;
        if (this.transactionType == 1) {
            const money = common.div(this.receiveMoney, 100);
            this.merchant.unfreezeReceiveMoney({ channelCode: this.channelCode, money });
            this.merchant.unfreezeWithdrawMoney({ channelCode: this.channelCode, money });
        }
        if (this.transactionType == 2) {
            // this.merchant.unfreezeReceiveMoney({ channelCode: this.channelCode, money: this.reconciliationMoney });
            // this.merchant.unfreezeWithdrawMoney({ channelCode: this.channelCode, money: this.reconciliationMoney });
            this.merchant.unfreezeReceiveMoneyForRefund({ channelCode: this.channelCode, money: this.reconciliationMoney });
            this.merchant.unfreezeWithdrawMoneyForRefund({ channelCode: this.channelCode, money: this.reconciliationMoney });
        }
    }

    /**
     * 业务订单明细表
     * @param {*} con 
     */
    async selectAccountBizOrderDetail(con) {
        const dbResult = await con.query(`SELECT * FROM merfin001.account_biz_order_detail WHERE biz_order_id = ${this.bizOrderId} And biz_sub_order_id = ${this.bizSubOrderId}`).then(res => res[0]);
        console.log(`\naccount_biz_order_detail=${JSON.stringify(dbResult)}`);
        expect(dbResult[0], `account_biz_order_detail找不到当前单子${this.bizSubOrderId}`).not.to.be.undefined;
        return dbResult[0];
    }

    /**
     * 财务账户流水
     * @param {*} con 
     */
    async selectFinAcctFlow(con) {
        const dbResult = await con.query(`SELECT * FROM merfin001.fin_acct_flow WHERE biz_order_id = ${this.bizOrderId} And biz_order_detail_id = ${this.bizSubOrderId}`).then(res => res[0]);
        console.log(`\nfin_acct_flow=${JSON.stringify(dbResult)}`);
        return dbResult;
    }

    /**
     * account_biz_order_detail 数据校验
     * @param {*} con 
     * @param {*} params.status 状态，0：新建，1：已提交，2:审核通过；3:审核不通过；4:已解冻
     */
    async accountBizOrderDetailAssert(con, params = {}) {
        const actual = await this.selectAccountBizOrderDetail(con);
        let amount = '';
        if (this.transactionType == 1) {
            amount = this.totalAmount;
        } else if (this.transactionType == 2) {
            amount = this.refundOrder.get(this.bizSubOrderId).refundAmount;
        } else {
            console.log(`transactionType出现未知类型:${this.transactionType}`);
        }
        const exp = common.update({
            status: this.status,
            pay_type: this.transactionType,
            order_type: this.orderType,
            biz_type: this.bizType,
            biz_order_id: this.bizOrderId,
            biz_sub_order_id: this.bizSubOrderId,
            order_amount: amount,
            pay_amount: amount,
            share_amount: this.serviceFee.shareAmount,
            fee_amount: this.serviceFee.feeAmount,
            pay_way_type: this.payType,
            unit_id: this.merchant.merchantId,
            platform_code: this.channelCode
        }, params);
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * fin_acct_flow 单条数据校验
     * @param {Object} con 
     * @param {Object} params 
     * @param {Number} params.tradeType 交易类型 100:支付(退款充值),103:交易手续费,105:退款
     */
    async finAcctFlowAssertBase(con, params = { trade_type: 100 }) {
        const actuals = await this.selectFinAcctFlow(con);
        const totalMoney = this.merchant.getAcctTotalMoney();
        console.log(totalMoney);
        const exp = common.update({
            unit_id: this.merchant.merchantId,
            flag: this.orderFlag,
            // "acct_id":24737,
            biz_type: this.bizType,
            biz_order_id: this.bizOrderId,
            biz_order_detail_id: this.bizSubOrderId,
            trade_type: 100
        }, params);
        switch (exp.trade_type) {
            case 100:
                Object.assign(exp, {
                    io_flag: 0, //收支标记：0-收入；1-支出
                    money: common.div(this.totalAmount, 100),
                    bal_money: common.add(this.totalAcctMoney.bal_money, common.div(this.merFee, 100)),
                    blk_money: this.totalAcctMoney.blk_money
                });
                break;

            case 103:
                Object.assign(exp, {
                    io_flag: 1, //收支标记：0-收入；1-支出
                    money: common.div(this.merFee, 100),
                    bal_money: this.totalAcctMoney.bal_money,
                    blk_money: this.totalAcctMoney.blk_money
                });
                break;

            case 105:
                Object.assign(exp, {
                    io_flag: 1, //收支标记：0-收入；1-支出
                    money: common.div(common.sub(this.refundOrder.get(this.bizSubOrderId).refundAmount, this.merFee), 100),
                    bal_money: this.totalAcctMoney.bal_money,
                    blk_money: this.totalAcctMoney.blk_money
                });
                break;

            case 201:
                Object.assign(exp, {
                    io_flag: 1, //收支标记：0-收入；1-支出
                    money: this.money,
                    bal_money: this.totalAcctMoney.bal_money,
                    blk_money: this.totalAcctMoney.blk_money
                });
                break;

            default:
                break;
        }
        const actual = actuals.find(obj => obj.trade_type == exp.trade_type);
        console.log(`\n期望值=${JSON.stringify(exp)}\n实际值=${JSON.stringify(actual)}`);
        if (!exp.money) return;
        common.isApproximatelyEqualAssert(exp, actual);
    }

    /**
     * 获取各渠道手续费
     * @param {*} channelCode 
     */
    getServiceFeeRate(channelCode = 'CITIC') {
        return serviceFeeRate[channelCode];
    }

}


module.exports = Order;