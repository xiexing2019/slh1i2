const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const fs = require('fs');
const path = require('path');

// 获取微商城存储的身份证/银行卡信息
const docInfo = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../ss/data/doc.json'), (err) => {
    if (err) throw new Error(`读取本地图片失败\r\n ${err.message}`);
}))[caps.name.includes('online') ? 'ss_online' : 'ss_pre'];
const idCard = docInfo.idCard,
    bankCard = docInfo.card;

// TODO 修改参数 不同渠道识别身份证
describe('图片服务-图像识别', async function () {
    this.timeout(TESTCASE.timeout);
    let idCardParam, bankCardParam, shopParam;

    describe('身份证图片识别', async function () {
        before('获取身份证识别渠道', async function () {
            idCardParam = await ecPlant.param.getParamInfo({ code: 'merfin_img_idcard_platform' });
            console.log(idCardParam);
        });
        it('图片识别校验', async function () {
            const data = await ecPlant.img.idCardDetect({ frontUrl: idCard.frontImage.docUrl, backUrl: idCard.backImage.docUrl }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(idCard.cardMsg, data, ['address']);
        });
    });

    describe('银行卡图片识别', async function () {
        before('获取银行卡识别渠道', async function () {
            bankCardParam = await ecPlant.param.getParamInfo({ code: 'merfin_img_bankcard_platform' });
            console.log(bankCardParam);
        });
        it('使用bankCardUrl查询', async function () {
            const res = await ecPlant.img.bankCardDetect({ bankCardUrl: bankCard.cardImage.docUrl });
            expect(res.result.data, `图片识别有误\n${JSON.stringify(res)}`).to.includes({ cardNo: bankCard.cardNum });
        });
        it('使用docId查询', async function () {
            const res = await ecPlant.img.bankCardDetect({ docId: bankCard.cardImage.docId });
            expect(res.result.data, `图片识别有误\n${JSON.stringify(res)}`).to.includes({ cardNo: bankCard.cardNum });
        });
    });

    describe('店铺门头照', async function () {
        before('店铺门头照来源渠道', async function () {
            shopParam = await ecPlant.param.getParamInfo({ code: 'merfin_img_shopout_platform' });
            console.log(shopParam);
        });
        it('图片识别校验', async function () {
            const data = await ecPlant.img.getRandomShopOutImg({ shopName: '高胖胖旗舰店' }).then(res => res.result.data);
            const docUrl = data.val;
            console.log(docUrl);
        });
    });

    // 仅线下执行
    // 修改参数时,参数id目前是提前从confc.sc_param中获取并写死 (获取参数接口只返回了val没有id信息)
    // 用例执行完毕后,恢复参数原值
    describe('切换识别渠道-offline', async function () {
        before('修改识别渠道', async function () {
            await ecPlant.auth.staffLogin({ code: '000', pass: '000000', saasId: 4 });
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

            await Promise.all([
                ecPlant.param.updateBaseInfo({ id: 1157, val: idCardParam.result.data.val == 1 ? 2 : 1 }),
                ecPlant.param.updateBaseInfo({ id: 1158, val: bankCardParam.result.data.val == 1 ? 2 : 1 }),
                ecPlant.param.updateBaseInfo({ id: 1160, val: shopParam.result.data.val == 1 ? 0 : 1 })
            ]);
            await ecPlant.param.doGlobalParamChanged();
        });
        after('参数还原', async function () {
            await Promise.all([
                ecPlant.param.updateBaseInfo({ id: 1157, val: idCardParam.result.data.val }),
                ecPlant.param.updateBaseInfo({ id: 1158, val: bankCardParam.result.data.val }),
                ecPlant.param.updateBaseInfo({ id: 1160, val: shopParam.result.data.val })
            ]);
            await ecPlant.param.doGlobalParamChanged();
        });
        describe('身份证图片识别', async function () {
            before('获取身份证识别渠道', async function () {
                const param = await ecPlant.param.getParamInfo({ code: 'merfin_img_idcard_platform' });
                console.log(param);
            });
            it('图片识别校验', async function () {
                const data = await ecPlant.img.idCardDetect({ frontUrl: idCard.frontImage.docUrl, backUrl: idCard.backImage.docUrl }).then(res => {
                    console.log(res);
                    return res.result.data;
                });
                common.isApproximatelyEqualAssert(idCard.cardMsg, data, ['address', 'birth']);
            });
        });
        describe('银行卡图片识别', async function () {
            before('获取银行卡识别渠道', async function () {
                const param = await ecPlant.param.getParamInfo({ code: 'merfin_img_bankcard_platform' });
                console.log(param);
            });
            it('使用bankCardUrl查询', async function () {
                const res = await ecPlant.img.bankCardDetect({ bankCardUrl: bankCard.cardImage.docUrl });
                expect(res.result.data, `图片识别有误\n${JSON.stringify(res)}`).to.includes({ cardNo: bankCard.cardNum });
            });
            it('使用docId查询', async function () {
                const res = await ecPlant.img.bankCardDetect({ docId: bankCard.cardImage.docId });
                expect(res.result.data, `图片识别有误\n${JSON.stringify(res)}`).to.includes({ cardNo: bankCard.cardNum });
            });
        });
        describe('店铺门头照', async function () {
            before('店铺门头照来源渠道', async function () {
                shopParam = await ecPlant.param.getParamInfo({ code: 'merfin_img_shopout_platform' });
                console.log(shopParam);
            });
            it('图片识别校验', async function () {
                const data = await ecPlant.img.getRandomShopOutImg({ shopName: '高胖胖旗舰店' }).then(res => res.result.data);
                const docUrl = data.val;
                console.log(docUrl);
            });
        });
    });

});