const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const plantAccount = require('../../data/plantAccount');
const merchantManage = require('../../help/merchant/merchant');
const orderManage = require('../../help/merchant/orders/payOrder');


// TODO: 只做了单接口校验，商户和子商户完整流程
describe('子商户-offline', async function () {
    this.timeout(TESTCASE.timeout);
    const merchant = merchantManage.setupMerchant({ merchantId: plantAccount.merchant_CITIC_danger.merchantId });
    let con;
    before('初始化商户信息', async function () {
        // await merchant.initMerchantInfo();
        con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
    });
    after('商户启用', async function () {
        await con.end();
    });
    it('子商户查询', async function () {
        await merchant.finSubMerchantsByWeb({ merchantId: 918 });
    });
});