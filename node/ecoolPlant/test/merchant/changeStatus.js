const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const plantAccount = require('../../data/plantAccount');
const merchantManage = require('../../help/merchant/merchant');
const orderManage = require('../../help/merchant/orders/payOrder');


describe('商户禁用、启用-offline', async function () {
    this.timeout(TESTCASE.timeout);
    const merchant = merchantManage.setupMerchant({ merchantId: plantAccount.merchant_CITIC_danger.merchantId });
    const order = orderManage.setupOrder();
    let con;
    before('初始化商户信息', async function () {
        // 关联商户与订单 
        order.merchant = merchant;
        await merchant.initMerchantInfo();

        con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
        await merchant.initMerchant(con);
    });
    after('商户启用', async function () {
        await Promise.all([
            merchant.changeMerchantStatus({ status: 1 }),
            con.end()
        ]);
    });
    describe('商户禁用', async function () {
        before('商户禁用', async function () {
            await merchant.changeMerchantStatus({ status: 0 });
        });
        it('商户列表查询', async function () {
            await merchant.findFinMerchantsAssert();
        });
        it('商户详情列表查询', async function () {
            await merchant.findFinMerchantFullsAssert();
        });
        describe('支付', async function () {
            let payRes;
            before('创建支付', async function () {
                const payJson = order.mockPayJson();
                payJson.check = false;
                payRes = await order.createPay(payJson);
            });
            it('pay_order', async function () {
                expect(payRes.result).to.include({ msg: "商户状态异常，请与管理台联系", msgId: "merchant_pay_disable" });
            });
        });
    });
    describe('商户启用', async function () {
        before('商户启用', async function () {
            await merchant.changeMerchantStatus({ status: 1 });
        });
        it('商户列表查询', async function () {
            await merchant.findFinMerchantsAssert();
        });
        it('商户详情列表查询', async function () {
            await merchant.findFinMerchantFullsAssert();
        });
        describe('支付', async function () {
            before('创建支付', async function () {
                const payJson = order.mockPayJson();
                await order.createPay(payJson);
            });
            it('pay_order', async function () {
                await order.payOrderAssert(con);
            });
        });
    });
});
