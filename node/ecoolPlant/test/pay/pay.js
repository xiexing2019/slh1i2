`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/payOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');

// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    const types = [
        { key: 0, name: '应收账户余额>=money，则直接扣减，应收对冲不变(应该为0);可提对冲账户余额>=money,可提对冲抵消扣减，可提账户不变' },
        { key: 1, name: '应收账户余额>=money，则直接扣减，应收对冲不变(应该为0);可提对冲账户余额<money,可提对冲扣减，剩余可提账户增加' },
        { key: 2, name: '应收账户余额balance<money，则应收账户扣减至0，其余部分应收对冲增加;可提对冲账户余额>=balance,可提对冲抵消扣减balance，可提不变动' },
        { key: 3, name: `应收账户余额balance<money，则应收账户扣减至0，其余部分应收对冲增加;可提对冲账户余额<balance,可提对冲扣减至0，剩余可提账户增加` }
    ];
    for (const type of types) {
        describe(`${channel}下支付入账:${type.name}`, async function () {
            this.timeout(TESTCASE.timeout);
            const order = orderManage.setupOrder();
            let con, conPay, totalAmount = common.getRandomNum(50, 100, 2);//单位为元
            before('绑定商户', async function () {
                con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
                conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
                // 订单绑定对应账户
                order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
                order.channelCode = channel;
                await order.merchant.initMerchant(con);
                console.log(order);
                // 数据库更新各财务账户(初始化数据)
                await order.merchant.updateFinAccts(order.channelCode, { receive: 0, receiveHedge: common.sub(totalAmount, common.getRandomNum(5, 10)), withdraw: 0, withdrawHedge: 0 });
                // 获取当前商户财务账户初始值(保底)
                await order.merchant.initMerchant(con);
                // console.log(order);
                console.log(order.merchant);
            });
            after(async function () {
                await con.end();
                caps.name.includes('online') && await conPay.end();
            });
            describe('支付', async function () {
                before('创建支付', async function () {
                    const payJson = order.mockPayJson({ totalAmount: common.mul(totalAmount, 100), distPlatformCode: channel });
                    await order.createPay(payJson);
                });
                it('pay_order', async function () {
                    await order.payOrderAssert(conPay);
                });
                it('account_biz_order_detail', async function () {
                    this.retries(2);
                    await common.delay(5000);
                    await order.accountBizOrderDetailAssert(con);
                });
                it('fin_acct_flow', async function () {
                    await order.finAcctFlowAssert(con);
                });
                it('财务账户fin_acct校验', async function () {
                    this.retries(3);
                    await common.delay(2000);
                    await order.merchant.finAcctAssert(con);
                });
                describe('提交结算申请', async function () {
                    before('提交结算申请', async function () {
                        switch (type.key) {
                            case 0:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.add(totalAmount, common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.add(totalAmount, common.getRandomNum(5, 10)) });
                                break;
                            case 1:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.add(totalAmount, common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.sub(totalAmount, common.getRandomNum(5, 10)) });
                                break;
                            case 2:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.sub(totalAmount, common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.add(totalAmount, common.getRandomNum(5, 10)) });
                                break;
                            case 3:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.sub(totalAmount, common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.sub(totalAmount, common.getRandomNum(5, 10)) });
                                break;
                            default:
                                console.log(`不修改财务账户数据库`);
                        }
                        await order.merchant.initMerchant(con);
                        await order.commitBizOrderDetail();
                    });
                    it('account_biz_order_detail', async function () {
                        await order.accountBizOrderDetailAssert(con);
                    });
                    it('fin_acct_flow', async function () {
                        await order.finAcctFlowAssert(con);
                    });
                    it('财务账户fin_acct校验', async function () {
                        this.retries(3);
                        await common.delay(2000);
                        await order.merchant.finAcctAssert(con);
                    });
                    describe('回调结果反馈', async function () {
                        before('回调结果反馈', async function () {
                            await order.receiveRecordedResult();
                            console.log(order);
                        });
                        it('fin_acct_flow', async function () {
                            this.retries(3);
                            await common.delay(2000);
                            await order.finAcctFlowAssert(con);
                        });
                        it('财务账户fin_acct校验', async function () {
                            await common.delay(3000);
                            await order.merchant.finAcctAssert(con);
                        });
                    });
                });
            });
        });
    }
}