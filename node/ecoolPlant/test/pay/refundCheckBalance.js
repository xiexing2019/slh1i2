`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/receiptOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');

// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    describe(`${channel}下退款余额校验-offline`, async function () {
        this.timeout(TESTCASE.timeout);
        const order = orderManage.setupOrder();
        const refund = { bizRefundId: Number(`${Date.now()}${common.getRandomNum(100, 999)}`), refundAmount: '' }
        let con, conPay, payflag = false, detailFlag = false;
        before('绑定商户并支付', async function () {
            con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
            conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
            order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
            order.channelCode = channel;
            await order.merchant.initMerchant(con);
            // 修改商户财务账户
            await order.merchant.updateFinAccts(order.channelCode, { receive: 0, receiveHedge: 0, withdraw: 0, withdrawHedge: 0 });
            await order.merchant.initMerchant(con);
            const payJson = order.mockPayJson({ distPlatformCode: channel });
            console.log(payJson);
            await order.createPay(payJson);
            // await common.delay(10000);
        });
        after(async function () {
            await con.end();
            caps.name.includes('online') && await conPay.end();
        });
        it('pay_order', async function () {
            await order.payOrderAssert(conPay);
            payflag = true;
        });
        it('account_biz_order_detail', async function () {
            this.retries(3);
            await common.delay(5000);
            await order.accountBizOrderDetailAssert(con);
            detailFlag = true;
        });
        describe('退款余额校验', async function () {
            before('是否进行退款余额校验', async function () {
                if (!payflag || !detailFlag) this.skip();
            });
            const difAmount = [{ name: '大于', val: 1 }, { name: '等于', val: 0 }, { name: '小于', val: -1 }];
            // const difAmount = [{ name: '大于', val: 1 }];
            for (const amount of difAmount) {
                describe(`退款余额校验:${amount.name}支付总额`, async function () {
                    let money;
                    before('设置退款余额', async function () {
                        money = order.totalAmount + amount.val;
                    });
                    it('退款余额校验', async function () {
                        const refundCheckBalance = await order.refundCheckBalance({ bizRefundId: refund.bizRefundId, refundAmount: money, check: false });
                        if (amount.name == '大于') expect(refundCheckBalance).to.include({ msgId: '退款金额超过支付过的剩余金额！' });
                        else expect(refundCheckBalance.data).to.include({ v1: true });
                    });
                });
            }

            describe('财务账户余额不足', async function () {
                let key;
                before('修改财务账户', async function () {
                    refund.bizRefundId += 1;
                    refund.refundAmount = order.totalAmount;
                    await order.merchant.updateFinAccts(order.channelCode, { receive: 0, receiveHedge: 0, withdraw: 0, withdrawHedge: 0 });
                    await order.merchant.initMerchant(con);
                    // await order.merchant.selectFinAcct(con);
                    //退款余额校验
                    const refundCheckBalance = await order.refundCheckBalance(refund);
                    expect(refundCheckBalance.data).to.include({ v1: false });
                    key = refundCheckBalance.data.v2.split('key=')[1];
                    order.updateReceiptBill({ payBillId: key.split('_')[1], payMoney: common.div(order.totalAmount, 100), bizType: 1261 });
                    console.log(order);
                });
                it('fin_receipt_bill', async function () {
                    await order.finReceiptBillAssert(con);
                });
                describe('退款充值', async function () {
                    before('支付', async function () {
                        await order.createUnionPay({ key: key });
                        console.log(order);
                    });
                    it('pay_order', async function () {
                        this.retries(3);
                        await common.delay(2000);
                        await order.payOrderAssert(conPay);
                    });
                    it('fin_receipt_bill', async function () {
                        await order.finReceiptBillAssert(con);
                    });
                    it('account_biz_order_detail', async function () {
                        this.retries(2);
                        await common.delay(5000);
                        await order.accountBizOrderDetailAssert(con);
                    });
                    it('fin_acct_flow', async function () {
                        this.retries(2);
                        await common.delay(5000);
                        await order.finAcctFlowAssert(con);
                    });
                    it('fin_acct', async function () {
                        // await order.refund2(refund);
                        await order.merchant.finAcctAssert(con);
                    });
                    describe('提交结算申请', async function () {
                        before('提交结算申请', async function () {
                            await order.commitBizOrderDetail();
                        });
                        it('account_biz_order_detail', async function () {
                            await order.accountBizOrderDetailAssert(con);
                        });
                        it('fin_receipt_bill', async function () {
                            await order.finReceiptBillAssert(con);
                        });
                        it('fin_acct_flow', async function () {
                            await order.finAcctFlowAssert(con);
                        });
                        it('fin_acct', async function () {
                            await order.merchant.finAcctAssert(con);
                        });
                        describe('回调结果反馈', async function () {
                            before('回调结果反馈', async function () {
                                await order.receiveRecordedResult();
                            });
                            it('fin_acct_flow', async function () {
                                this.retries(3);
                                await common.delay(2000);
                                await order.finAcctFlowAssert(con);
                            });
                            it('fin_receipt_bill', async function () {
                                await order.finReceiptBillAssert(con);
                            });
                            it('fin_acct', async function () {
                                await order.merchant.finAcctAssert(con);
                            });
                        });
                    });
                });
            });

        });

    });
}