`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/refundOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');


// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    const types = [
        { key: 0, name: '应收对冲账户>=money, 应收对冲账户扣减，应收账户不变,可提账户和可提对冲账户不变' },
        { key: 1, name: '应收对冲账户<money, 应收对冲账户扣减到0，剩余other应收增加,可提账户>=money,可提账户直接扣减，可提对冲不变' },
        { key: 2, name: '应收对冲账户<money, 应收对冲账户扣减到0，剩余other应收增加,可提账户<money,可提账户扣减为0，剩余可提对冲增加' }
    ];

    for (const type of types) {
        describe(`${channel}下退款入账:${type.name}`, async function () {
            this.timeout(TESTCASE.timeout);
            const order = orderManage.setupOrder();
            const refund = { bizRefundId: Number(`${Date.now()}${common.getRandomNum(100, 999)}`), refundAmount: '' };
            let con, conPay, payflag = false, detailFlag = false;
            before('绑定商户并支付', async function () {
                con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
                conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
                order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
                order.channelCode = channel;
                await order.merchant.initMerchant(con);
                const payJson = order.mockPayJson({ distPlatformCode: channel });
                await order.createPay(payJson);
            });
            after(async function () {
                await con.end();
                caps.name.includes('online') && await conPay.end();
            });
            it('支付订单表pay_order数据校验', async function () {
                await order.payOrderAssert(conPay);
                payflag = true;
            });
            it('业务订单明细表account_biz_order_detail数据校验', async function () {
                this.retries(3);
                await common.delay(5000);
                await order.accountBizOrderDetailAssert(con);
                detailFlag = true;
            });
            describe('退款', async function () {
                before('退款', async function () {
                    if (!payflag && !detailFlag) this.skip();
                    refund.refundAmount = order.totalAmount;
                    await order.merchant.updateFinAccts(order.channelCode, { receive: common.div(Math.floor(refund.refundAmount), 200), receiveHedge: 0, withdraw: common.div(refund.refundAmount, 100), withdrawHedge: 0 });
                    await order.merchant.initMerchant(con);
                    console.log(order.merchant);
                    const refundCheckBalance = await order.refundCheckBalance(refund);
                    expect(refundCheckBalance.data).to.include({ v1: true });

                    await order.refund(refund);
                    console.log('商户信息:', order.merchant);

                });
                it('支付订单表pay_order数据校验', async function () {
                    await order.payOrderAssert(conPay);
                });
                it('业务订单明细表account_biz_order_detail数据校验', async function () {
                    this.retries(3);
                    await common.delay(5000);
                    await order.accountBizOrderDetailAssert(con);
                });
                it('财务账户流水fin_acct_flow数据校验', async function () {
                    await order.finAcctFlowAssert(con);
                });
                it('fin_acct', async function () {
                    await order.merchant.finAcctAssert(con);
                });
                describe('提交结算申请', async function () {
                    before(`提交结算申请:${type.name}`, async function () {
                        switch (type.key) {
                            case 0:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.add(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.add(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)) });
                                break;
                            case 1:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.add(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.sub(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)) });
                                break;
                            case 2:
                                await order.merchant.updateFinAccts(order.channelCode, { receive: common.sub(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)), receiveHedge: 0, withdraw: 0, withdrawHedge: common.add(common.div(refund.refundAmount, 100), common.getRandomNum(5, 10)) });
                                break;
                            default:
                                console.log(`不修改财务账户数据库`);
                        };
                        await order.merchant.initMerchant(con);
                        await order.commitBizOrderDetail();
                    });
                    it('业务订单明细表account_biz_order_detail数据校验', async function () {
                        await order.accountBizOrderDetailAssert(con);
                    });
                    it('财务账户流水fin_acct_flow数据校验', async function () {
                        await order.finAcctFlowAssert(con);
                    });
                    it('fin_acct', async function () {
                        await order.merchant.finAcctAssert(con);
                    });
                    describe('回调结果反馈', async function () {
                        before('回调结果反馈', async function () {
                            await order.receiveRecordedResult();
                            console.log(order);
                        });
                        it('财务账户流水fin_acct_flow数据校验', async function () {
                            this.retries(6);
                            common.delay(5000);
                            await order.finAcctFlowAssert(con);
                        });
                        it('fin_acct', async function () {
                            await order.merchant.finAcctAssert(con);
                        });
                    });
                });
            });
        });
    }
}