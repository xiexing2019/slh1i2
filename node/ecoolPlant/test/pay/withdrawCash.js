`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/cashOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');


// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    describe(`${channel}下提现-offline`, async function () {
        this.timeout(TESTCASE.timeout);
        const order = orderManage.setupOrder();
        let con;
        before('绑定商户', async function () {
            con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
            order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
            order.channelCode = channel;
            await order.merchant.initMerchant(con);
            await order.merchant.updateFinAccts(order.channelCode, { withdraw: common.getRandomNum(100, 1000), withdrawHedge: 0 });
            await order.merchant.initMerchant(con);
        });
        after(async function () {
            await con.end();
        });
        describe('提现', async function () {
            before('发起提现', async function () {
                await order.createCashBill();
            });
            it('获取提现详情', async function () {
                await order.finCashBillByIdAssert();
            });
            it('获取提现记录', async function () {
                await order.finCashBillAssert();
            });
            it('fin_cash_bill', async function () {
                await order.finCashBillDBAssert(con);
            });
            it('acoount_withdraw_detail', async function () {
                await order.finAccountWithdrawDetailAssert(con);
            });
            it('fin_acct_flow', async function () {
                await order.finAcctFlowAssert(con);
            });
            it('财务账户fin_acct校验', async function () {
                await order.merchant.finAcctAssert(con);
            });
            describe('回调结果反馈', async function () {
                before('回调结果反馈', async function () {
                    await order.receiveWithdrawResult();
                });
                it('获取提现详情', async function () {
                    await order.finCashBillByIdAssert();
                });
                it('获取提现记录', async function () {
                    await order.finCashBillAssert();
                });
                it('acoount_withdraw_detail', async function () {
                    await order.finAccountWithdrawDetailAssert(con);
                });
                it('财务账户fin_acct校验', async function () {
                    await order.merchant.finAcctAssert(con);
                });
                it('fin_acct_flow', async function () {
                    await order.finAcctFlowAssert(con);
                });
            });
        });
        describe('提现:可提余额不足', async function () {
            const money = common.getRandomNum(0, 9, 2);
            before('更新可提账户', async function () {
                await order.merchant.updateFinAccts(order.channelCode, { withdraw: money, withdrawHedge: 0 });
                await order.merchant.initMerchant(con);
            });
            it('提现', async function () {
                const res = await order.createCashBill({ money: common.add(money, 10), check: false });
                const withdrawAcct = order.merchant.getAcctDate({ acct: 'withdraw', channelCode: order.channelCode });
                expect(res.result).to.include({ msg: `提现金额异常，提现范围0到${Number(withdrawAcct.bal_money)},提现金额异常，提现范围0到${Number(withdrawAcct.bal_money)}`, msgId: `the_use_money_exception_between_0_to_x` })
            });
        });
    });
}