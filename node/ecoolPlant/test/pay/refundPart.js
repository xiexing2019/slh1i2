`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/refundOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');


// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    describe(`${channel}下多次部分退款`, async function () {
        this.timeout(TESTCASE.timeout);
        const order = orderManage.setupOrder();
        const refund = { bizRefundId: Number(`${Date.now()}${common.getRandomNum(100, 999)}`), refundAmount: '' }
        let con, conPay, payflag = false, detailFlag = false;
        before('绑定商户并支付', async function () {
            con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
            conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
            order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
            order.channelCode = channel;
            await order.merchant.initMerchant(con);
            // 修改商户财务账户
            await order.merchant.updateFinAccts(order.channelCode, { receive: 0, receiveHedge: 0, withdraw: 0, withdrawHedge: 0 });
            await order.merchant.initMerchant(con);
            const payJson = order.mockPayJson({ totalAmount: 6151, distPlatformCode: channel });
            console.log(payJson);
            await order.createPay(payJson);
            // await common.delay(10000);
        });
        after(async function () {
            await con.end();
            caps.name.includes('online') && await conPay.end();
        });
        it('pay_order', async function () {
            await order.payOrderAssert(conPay);
            payflag = true;
        });
        it('account_biz_order_detail', async function () {
            this.retries(3);
            await common.delay(5000);
            await order.accountBizOrderDetailAssert(con);
            detailFlag = true;
        });
        for (let index = 1; index <= 2; index++) {
            describe(`第${index}次退款`, async function () {
                before('退款', async function () {
                    refund.bizRefundId += index;
                    console.log(refund);
                    if (!payflag && !detailFlag) this.skip();
                    refund.refundAmount = Math.floor(common.div(order.totalAmount, 2));
                    console.log(order.totalAmount, refund.refundAmount);
                    const refundCheckBalance = await order.refundCheckBalance(refund);
                    expect(refundCheckBalance.data).to.include({ v1: true });
                    await order.refund(refund);
                });
                it('pay_order', async function () {
                    await order.payOrderAssert(conPay);
                });
                it('account_biz_order_detail', async function () {
                    this.retries(3);
                    await common.delay(5000);
                    await order.accountBizOrderDetailAssert(con);
                });
                it('fin_acct_flow', async function () {
                    await order.finAcctFlowAssert(con);
                });
                it('财务账户fin_acct校验', async function () {
                    await common.delay(5000);
                    await order.merchant.finAcctAssert(con);
                });
                describe('提交结算申请', async function () {
                    before('提交结算申请', async function () {
                        await order.commitBizOrderDetail();
                    });
                    it('account_biz_order_detail', async function () {
                        this.retries(3);
                        await common.delay(2000);
                        await order.accountBizOrderDetailAssert(con);
                    });
                    it('fin_acct_flow', async function () {
                        await order.finAcctFlowAssert(con);
                    });
                    it('财务账户fin_acct校验', async function () {
                        await common.delay(5000);
                        await order.merchant.finAcctAssert(con);
                    });
                    describe('回调结果反馈', async function () {
                        before('回调结果反馈', async function () {
                            await order.receiveRecordedResult();
                        });
                        it('财务账户fin_acct校验', async function () {
                            await common.delay(5000);
                            await order.merchant.finAcctAssert(con);
                        });
                        it('fin_acct_flow', async function () {
                            this.retries(3);
                            await common.delay(2000);
                            await order.finAcctFlowAssert(con);
                        });
                    });
                });
            });
        }
    });
}