`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/payOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');

// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    describe(`${channel}下支付入账`, async function () {
        this.timeout(TESTCASE.timeout);
        const order = orderManage.setupOrder();
        let con, conPay, totalAmount = common.getRandomNum(50, 100, 2);//单位为元
        before('绑定商户', async function () {
            con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
            conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
            // 订单绑定对应账户
            order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
            order.channelCode = channel;
            console.log(order);
            // 获取当前商户财务账户初始值(保底)
            await order.merchant.initMerchant(con);
            // console.log(order);
            console.log(order.merchant);
        });
        after(async function () {
            await con.end();
            caps.name.includes('online') && await conPay.end();
        });
        describe('支付', async function () {
            before('创建支付', async function () {
                const payJson = order.mockPayJson({ totalAmount: common.mul(totalAmount, 100), distPlatformCode: channel });
                await order.createPay(payJson);
            });
            it('pay_order', async function () {
                await order.payOrderAssert(conPay);
            });
            it('account_biz_order_detail', async function () {
                this.retries(2);
                await common.delay(5000);
                await order.accountBizOrderDetailAssert(con);
            });
            it('fin_acct_flow', async function () {
                await order.finAcctFlowAssert(con);
            });
            it('财务账户fin_acct校验', async function () {
                this.retries(3);
                await common.delay(2000);
                await order.merchant.finAcctAssert(con);
            });
            describe('提交结算申请', async function () {
                before('提交结算申请', async function () {
                    await order.commitBizOrderDetail();
                });
                it('account_biz_order_detail', async function () {
                    await order.accountBizOrderDetailAssert(con);
                });
                it('fin_acct_flow', async function () {
                    await order.finAcctFlowAssert(con);
                });
                it('财务账户fin_acct校验', async function () {
                    this.retries(3);
                    await common.delay(2000);
                    await order.merchant.finAcctAssert(con);
                });
                describe('回调结果反馈', async function () {
                    before('回调结果反馈', async function () {
                        await order.receiveRecordedResult();
                        console.log(order);
                    });
                    it('fin_acct_flow', async function () {
                        this.retries(3);
                        await common.delay(2000);
                        await order.finAcctFlowAssert(con);
                    });
                    it('财务账户fin_acct校验', async function () {
                        await common.delay(3000);
                        await order.merchant.finAcctAssert(con);
                    });
                });
            });
        });
    });
}