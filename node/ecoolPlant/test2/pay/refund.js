`use strict`
const common = require('../../../lib/common');
const ecPlant = require('../../../reqHandler/ecoolPlant');
const orderManage = require('../../help/merchant/orders/refundOrder');
const merchantManage = require('../../help/merchant/merchant');
const plantAccount = require('../../data/plantAccount');
const caps = require('../../../../node/data/caps');


// const channels = ['CITIC', 'JL', 'MSFPAY', 'TDEA'];
const channels = ['CITIC', 'MSFPAY'];
for (const channel of channels) {
    describe(`${channel}下退款入账`, async function () {
        this.timeout(TESTCASE.timeout);
        const order = orderManage.setupOrder();
        const refund = { bizRefundId: Number(`${Date.now()}${common.getRandomNum(100, 999)}`), refundAmount: '' };
        let con, conPay, payflag = false, detailFlag = false;
        before('绑定商户并支付', async function () {
            con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });
            conPay = caps.name.includes('online') ? await ecPlant.creatSqlPool({ dbName: 'payMySql' }) : con;
            order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount[`merchant_${channel}`].merchantId });
            order.channelCode = channel;
            await order.merchant.initMerchant(con);
            const payJson = order.mockPayJson({ distPlatformCode: channel });
            await order.createPay(payJson);
        });
        after(async function () {
            await con.end();
            caps.name.includes('online') && await conPay.end();
        });
        it('支付订单表pay_order数据校验', async function () {
            await order.payOrderAssert(conPay);
            payflag = true;
        });
        it('业务订单明细表account_biz_order_detail数据校验', async function () {
            this.retries(3);
            await common.delay(5000);
            await order.accountBizOrderDetailAssert(con);
            detailFlag = true;
        });
        describe('退款', async function () {
            before('退款', async function () {
                if (!payflag && !detailFlag) this.skip();
                refund.refundAmount = order.totalAmount;
                const refundCheckBalance = await order.refundCheckBalance(refund);
                expect(refundCheckBalance.data).to.include({ v1: true });

                await order.refund(refund);
                console.log('商户信息:', order.merchant);
            });
            it('支付订单表pay_order数据校验', async function () {
                await order.payOrderAssert(conPay);
            });
            it('业务订单明细表account_biz_order_detail数据校验', async function () {
                this.retries(3);
                await common.delay(5000);
                await order.accountBizOrderDetailAssert(con);
            });
            it('财务账户流水fin_acct_flow数据校验', async function () {
                await order.finAcctFlowAssert(con);
            });
            it('fin_acct', async function () {
                await order.merchant.finAcctAssert(con);
            });
            describe('提交结算申请', async function () {
                before(`提交结算申请`, async function () {
                    await order.commitBizOrderDetail();
                });
                it('业务订单明细表account_biz_order_detail数据校验', async function () {
                    await order.accountBizOrderDetailAssert(con);
                });
                it('财务账户流水fin_acct_flow数据校验', async function () {
                    await order.finAcctFlowAssert(con);
                });
                it('fin_acct', async function () {
                    await order.merchant.finAcctAssert(con);
                });
                describe('回调结果反馈', async function () {
                    before('回调结果反馈', async function () {
                        await order.receiveRecordedResult();
                        console.log(order);
                    });
                    it('财务账户流水fin_acct_flow数据校验', async function () {
                        this.retries(6);
                        common.delay(5000);
                        await order.finAcctFlowAssert(con);
                    });
                    it('fin_acct', async function () {
                        await order.merchant.finAcctAssert(con);
                    });
                });
            });
        });
    });
}