const common = require('../../lib/common');
const superagent = require('superagent');
const fs = require('fs');
const path = require('path');
const config = require('../../data/caps');
const nodemailer = require('nodemailer');

// 校验列表
const tables = [
    { dbName: 'merfin001', tblname: 'account_biz_order_detail' },
    { dbName: 'merfin001', tblname: 'fin_acct' },
    // { dbName: 'merfin001', tblname: 'fin_acct_daily' },
    // { dbName: 'merfin001', tblname: 'fin_acct_flow' },
    // { dbName: 'merfin001', tblname: 'fin_acct_flow_daily' },
    // { dbName: 'merfin001', tblname: 'fin_acct_group_daily' },
    // { dbName: 'merfin001', tblname: 'fin_cash_bill' },
    // { dbName: 'merfin001', tblname: 'fin_merchant_bank' },
    // { dbName: 'merfin001', tblname: 'fin_merchant_bank_settle' },
    // { dbName: 'merfin001', tblname: 'fin_merchant_channel' },
    // { dbName: 'merfin001', tblname: 'fin_receipt_bill' },
    // { dbName: 'merfin001', tblname: 'fin_sub_merchant' },
];

async function checkDB() {
    const userPath = path.join(__dirname, `./checkDB.json`);
    let checkResult = {};
    for (const table of tables) {
        let result = { dataCompare: {}, getCompareDataState: {} };
        let dataCompare = {}, getCompareDataState = {}, dataCompareFlag = false, getCompareDataStateFlag = false;
        const body = { leftConfigId: 'ts_merfin01_edb1', leftDbname: table.dbName, rightConfigId: 'hws2_merfin02_edb1', rightDbname: table.dbName, tblname: table.tblname, keyFields: 'id', viewByTable: 1 };
        await superagent.post('http://212.64.83.224:8081/dbt/tableCompare/dataCompare.do')
            .timeout(0)
            .type('form')
            .send(body)
            .then((response) => {
                dataCompare = JSON.parse(JSON.stringify(response));
            }).catch((err) => {
                console.log(`err=${JSON.stringify(err)}`);
            });
        // console.log(dataCompare.header['set-cookie']);
        // console.log(dataCompare.text);
        result.dataCompare = dataCompare.text;
        if (dataCompare.text.includes('比较通过')) dataCompareFlag = true;

        //leftConfigId=ts_merfin01_edb1&leftDbname=merfin001&rightConfigId=hws2_merfin02_edb1&rightDbname=merfin001&tblname=fin_acct&keyFields=id&excFields=&viewByTable=1
        await superagent.post('http://212.64.83.224:8081/dbt/tableCompare/getCompareDataState.do')
            .timeout(0)
            .set('Cookie', dataCompare.header['set-cookie'])
            // .type('form')
            // .send(body)
            .then((response) => {
                getCompareDataState = JSON.parse(JSON.stringify(response));
            }).catch((err) => {
                console.log(`err=${JSON.stringify(err)}`);
            });
        // console.log(getCompareDataState.text);
        result.getCompareDataState = getCompareDataState.text;
        if (JSON.parse(getCompareDataState.text).TableCompareDiffCount == 0) getCompareDataStateFlag = true;
        if (dataCompareFlag && getCompareDataStateFlag) {
            console.log('比较通过，不记录');
            // console.log(result);
            continue;
        }
        checkResult[table.tblname] = result;
        console.log(checkResult);

        fs.writeFile(userPath, JSON.stringify(checkResult), function (err) {
            if (err) console.log(err);
        });
    }
    return checkResult;
};

(async function () {
    const checkResult = await checkDB();
    console.log(checkResult);
    const mail = {
        subject: `merfin001数据库比对${common.getCurrentTime()}`,
        text: `nmerfin001数据库比对，存在异常数据,详情请下载附件查看`,
    }
    if (Object.keys(checkResult).length == 0) {
        mail.subject = `无异常:merfin001数据库比对${common.getCurrentTime()}`;
        mail.text = `merfin001数据库比对，无异常数据`;
        mail.attachments = [];
    };
    sendmail(mail);
})();

const transport = nodemailer.createTransport({
    service: config.email.service,
    secureConnection: true,
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

//发送邮件
function sendmail(params = {}) {
    return transport.sendMail(common.update({
        from: `"自动化用例报告" <${config.email.user}>`,
        to: ['罗琦<jpw7169@dingtalk.com>'],
        // to: ['陆星欣<luxxhz@dingtalk.com>', '罗琦<jpw7169@dingtalk.com>'],
        subject: `merfin001数据库比对${common.getCurrentTime()}`,
        text: 'merfin001数据库比对,详情请下载附件查看',
        // html: msgs.html || '',
        attachments:
            [{
                filename: 'checkDB.json',   // 附件名
                path: `${__dirname}/checkDB.json`,  // 附件路径
                // cid: 'fn_01'  // _id 可被邮件使用
            }]
    }, params), function (error, response) {
        if (error) {
            console.log("邮件发送失败: " + error);
        } else {
            console.log('邮件发送完成: %j', response);
        }
    });
};


