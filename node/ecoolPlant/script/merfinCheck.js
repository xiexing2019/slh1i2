const common = require('../../lib/common');
const envName = require('../../data/caps').name;
const ecPlant = require('../../reqHandler/ecoolPlant');
const dingtalkRobot = require('../../lib/dingtalkRobot');
const mail = require('../../lib/mail');
const moment = require('moment');

// 排除清分流程使用的自动化商户(由于流程中涉及修改账户数据,对账结果一定不一致)
const plantAccount = require('../data/plantAccount');
const testAccount = Object.values(plantAccount).map(merchant => merchant.merchantId);
console.log('排除测试商户:%j', testAccount);

// 支付中台警告通知群
const dingtalkAccessToken = envName.includes('online') ?
    '26c3fed1c94edd0da0940a52688b15c5add4f07303cea36bc8205d5cf09fbc22' :
    '537a78ea593a05ebf98ce5f034660d26c2a9879a7231cec3b441cd641c76d25c';
// '9c55f9ae730c6e4af95455122b938e07e98328a82e946ca94813025d6da88e24';// 自测用

(async function () {
    console.log('获取所有商户集合');
    const totalUnitIds = await ecPlant.merfin.getAllUnitIds().then(res => {
        const unitIds = res.result.data.rows;
        _.remove(unitIds, (unitId) => testAccount.includes(unitId));
        return unitIds;
    });
    // const totalUnitIds = [5011, 4417, 4345];//自测用 5011为错误数据

    console.log('自检对账开始');

    //统一接口
    await checkAndSendMsg({ totalUnitIds, funcName: 'checkLedgValid', content: '' });

    // // [对账]清分入账状态异常
    // await checkAndSendMsg({ totalUnitIds, funcName: 'ledgOrderFlagCheck', content: `[对账]清分入账状态异常警告` });

    // // [对账]清分提现状态异常
    // await checkAndSendMsg({
    //     totalUnitIds, funcName: 'finCashBillDataCheck', content: `[对账]清分提现状态异常警告`,
    //     dateParams: { startDate: common.getDateString([0, -1, 0]), endDate: common.getDateString([0, 0, -1]) }
    // });

    // // 入账与账户流水状态异常
    // // 周日不入账 跳过校验
    // if (moment().weekday() != 7) {
    //     await checkAndSendMsg({
    //         totalUnitIds, funcName: 'ledgAndFlowDataCheck', content: `[对账]入账与账户流水状态异常警告`,
    //         dateParams: { startDate: common.getDateString([0, 0, -7]), endDate: common.getCurrentDate() }
    //     });
    // }

    // // 账户流水余额异常
    // await checkAndSendMsg({
    //     totalUnitIds, funcName: 'finAcctflowDataCheck', content: `[对账]账户流水余额异常警告`,
    //     dateParams: { startDate: common.getDateString([0, -1, 0]), endDate: common.getCurrentDate() }
    // });

    // // 账户结算金额异常
    // await checkAndSendMsg({ totalUnitIds, funcName: 'finAcctDataCheck', content: `[对账]账户结算金额异常警告` });

    console.log('自检对账完毕');
})();

async function checkAndSendMsg({ totalUnitIds, funcName, content, dateParams = {} }) {
    let msg = '', num = 0;
    const unitIdKEY = funcName == 'checkLedgValid' ? 'unitId' : 'unitIds';
    for (let index = 0; index < totalUnitIds.length; index++) {
        const unitId = totalUnitIds[index];
        await ecPlant.merfin[funcName]({ [unitIdKEY]: unitId, ...dateParams })
            .then(res => {
                // console.log(`res=${JSON.stringify(res)}`);
                const data = res.result.data;
                console.log(`data=${JSON.stringify(data)}`);
                if (data) {
                    for (const key in data) {
                        msg += `\n\nunitId:${unitId}, ${key} : \n${JSON.stringify(data[key])}`;
                        num++;
                    }
                }
            }).catch((error) => {
                console.log(error.message);
            });
    }
    // if (msg) await sendMsg(content + msg);
    if (num) await sendMsg(content + `\n错误总数${num}`, content + msg);
};

async function sendMsg(content, text) {
    // 
    return Promise.all([mail.sendMail('陆星欣<luxxhz@dingtalk.com>,高胖胖<gaomengzhong@dingtalk.com>,刘军<gua5415@dingtalk.com>', { subject: `${common.getCurrentDate()}自检对账结果日志`, text }),
    dingtalkRobot.sendMsg(dingtalkAccessToken, {
        msgtype: 'text',
        text: { content: content },
        at: {
            atMobiles: [
                '18768105597'
            ]
        },
    })]);
};