const common = require('../../lib/common');
const ecPlant = require('../../reqHandler/ecoolPlant');
const orderManage = require('../help/merchant/orders/refundOrder');
const merchantManage = require('../help/merchant/merchant');
const withdrawManage = require('../help/merchant/orders/cashOrder');
const plantAccount = require('../data/plantAccount');

// 总时间 间隔时间 单位S
const time = 60, delayTime = 2;
const order = orderManage.setupOrder(),
    withdrawOrder = withdrawManage.setupOrder();
let con;

(async function () {
    con = await ecPlant.creatSqlPool({ dbName: 'merfinMySql' });

    order.merchant = merchantManage.setupMerchant({ merchantId: plantAccount.merchant_CITIC.merchantId });
    await order.merchant.initMerchant(con);
    console.log(order);
    // 测试数据
    await order.merchant.updateFinAccts(order.channelCode, { receive: 10000, receiveHedge: 0, withdraw: 100000, withdrawHedge: 0 });

    withdrawOrder.merchant = merchantManage.setupMerchant({ merchantId: plantAccount.merchant_CITIC.merchantId });
    await withdrawOrder.merchant.initMerchant(con);

    const count = time / delayTime;
    const billTypeName = ['支付入账', '退款入账', '提现'];
    for (let index = 0; index < count; index++) {
        const num = common.getRandomNum(0, 2);
        console.log(`创建${billTypeName[num]}单 `);
        // 创建退款入账前需要先创建支付入账
        if (num == 0 || num == 1) {
            await creatPay(order);
        }
        if (num == 1) {
            await creatRefund(order);
        }
        if (num == 2) {
            await creatWithdraw(withdrawOrder);
        }
        await common.delay(delayTime * 1000);
    }
    await con.end();
})();
// 支付入账 退款入账 提现 
async function creatPay(order) {
    const payJson = order.mockPayJson();
    await order.createPay(payJson);
};
async function creatRefund(order) {
    const refund = { bizRefundId: Number(`${Date.now()}${common.getRandomNum(100, 999)}`), refundAmount: '' };
    refund.refundAmount = order.totalAmount;
    await order.refund(refund);
};
async function creatWithdraw(order) {
    await order.createCashBill();
};
