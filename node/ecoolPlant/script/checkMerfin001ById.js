const common = require('../../lib/common');
const superagent = require('superagent');
const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');
const config = require('../../data/caps');
const mysql = require('mysql2');

const userPath = path.join(__dirname, `./../temp/checkDB.json`);
const errorPath = path.join(__dirname, `./../temp/errorLog.txt`);
if (fs.existsSync(errorPath)) {
    fs.unlink(errorPath, (err) => {
        if (err) console.log(err);
    });
}
// let checkResult = {};

const dbAcct = {
    // ts_my02i11_merfin01db11
    ts_merfin: {
        user: 'mychk',
        host: '115.159.50.200',
        port: '30211'
    },
    // ts_my02i11_merfin01db12
    ts_merfin2: {
        user: 'mychk',
        host: '115.159.50.200',
        port: '30212'
    },
    // hws2_my02i11_merfin01db11
    hw_merfin: {
        user: 'mychk',
        host: '119.3.92.72',
        port: '30211'
    },
    // hws2_pay02db11
    hw_pay: {
        user: 'mychk',
        host: '119.3.92.72',
        port: '30111'
    },

};

const tables = [
    { dbName: 'merfin001', tblname: 'account_biz_order_detail' },
    // { dbName: 'merfin001', tblname: 'fin_acct' },
    { dbName: 'merfin001', tblname: 'fin_acct_daily' },
    { dbName: 'merfin001', tblname: 'fin_acct_flow' },
    { dbName: 'merfin001', tblname: 'fin_acct_flow_daily' },
    { dbName: 'merfin001', tblname: 'fin_acct_group_daily' },
    { dbName: 'merfin001', tblname: 'fin_cash_bill' },
    { dbName: 'merfin001', tblname: 'fin_merchant_bank' },
    { dbName: 'merfin001', tblname: 'fin_merchant_bank_settle' },
    { dbName: 'merfin001', tblname: 'fin_merchant_channel' },
    { dbName: 'merfin001', tblname: 'fin_receipt_bill' },
    { dbName: 'merfin001', tblname: 'fin_sub_merchant' },
];

async function checkDB(conL, conR, params = { time: ['1970-01-01 00:00:00', '2019-07-16 00:00:00'], num: 1000 }) {
    let checkResult = {}, errorNum = 0, skip = false;
    for (const table of tables) {
        let dbResultCountL, dbResultCountR, selectConditions = '';
        await Promise.all([
            conL.query(`SELECT COUNT(*) AS count FROM ${table.dbName}.${table.tblname} WHERE created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`),
            conR.query(`SELECT COUNT(*) AS count FROM ${table.dbName}.${table.tblname} WHERE created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`)
        ]).then(res => {
            console.log(`查询${table.tblname} count完成`);
            dbResultCountL = res[0][0][0];
            dbResultCountR = res[1][0][0];
        }).catch(err => {
            skip = true;
            console.log(err);
            fs.appendFile(errorPath, err, function (e) {
                if (e) console.log(e);
            });
        });
        if (skip) continue;
        console.log(dbResultCountL, dbResultCountR);
        // 2.dbl查出所有数据      
        let dbResultL = [], dbResultR = [];
        selectConditions = `created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`;
        await Promise.all([
            getAllData(conL, { table: table, dbResultCount: dbResultCountL, selectConditions: selectConditions, num: params.num }),
            getAllData(conR, { table: table, dbResultCount: dbResultCountR, selectConditions: selectConditions, num: params.num })
        ]).then(res => {
            console.log(`查询${table.tblname} 所有数据完成`);
            // console.log(res);
            [dbResultL, dbResultR] = res;
        }).catch(err => {
            skip = true;
            console.log(err);
            fs.appendFile(errorPath, err, function (e) {
                if (e) console.log(e);
            });
        });
        if (skip) continue;
        console.log(dbResultL.length, dbResultR.length);

        // 根据dbL查出的n条数据，查出数据对比
        for (const resultL of dbResultL) {
            selectConditions = `id = ${resultL.id}`;
            // console.log(selectConditions);
            const resultR = dbResultR.find(obj => obj.id == resultL.id);
            const result = { dif: {}, resultL: {}, resultR: {} };
            if (resultR) {
                // console.log(JSON.stringify(resultL));
                // console.log(JSON.stringify(resultR));
                let check = common.difference(resultL, resultR);
                // console.log(check);
                try {
                    check = JSON.parse(JSON.stringify(check));
                } catch (error) {
                    if (check.created_date) check.created_date = '';
                    if (check.updated_date) check.updated_date = '';
                    check = JSON.parse(JSON.stringify(check));
                }
                // console.log(check);
                if (Object.keys(check).length != 0) {
                    result.dif = check;
                    result.resultL = resultL;
                    result.resultR = resultR;
                }
                _.remove(dbResultR, function (n) { return n.id == resultR.id });
            } else {
                result.dif = `查询条件为:${selectConditions};`;
                result.resultL = resultL;
                result.resultR = '未查到数据';
            }
            // console.log(`\nresult=${JSON.stringify(result)}`);
            if (Object.keys(result.dif).length) {
                if (!checkResult[table.tblname]) checkResult[table.tblname] = [];
                // console.log(result);
                checkResult[table.tblname].push(result);
                errorNum++;
            }
        }
        // 剩下的dbResult左表为空
        // console.log(dbResultR);
        dbResultR.forEach(resultR => {
            const result = { dif: {}, resultL: {}, resultR: {} };
            selectConditions = `id = ${resultR.id}`;
            result.dif = `查询条件为:${selectConditions};`;
            result.resultL = '未查到数据';
            result.resultR = resultR;
            if (!checkResult[table.tblname]) checkResult[table.tblname] = [];
            checkResult[table.tblname].push(result);
            errorNum++;
        });
        fs.writeFile(userPath, `${JSON.stringify(checkResult)}`, function (err) {
            if (err) console.log(err);
        });
    };
    console.log(checkResult);
    // console.log(JSON.stringify(checkResult));
    return [checkResult, errorNum];
};

/**
 * 查出查询条件内的所有数据
 * @param {Object} con 数据库连接
 * @param {Object} params.table { dbName: 'merfin001', tblname: 'account_biz_order_detail' }
 * @param {Number} params.dbResultCount 
 * @param {String} params.selectConditions 查询条件
 * @param {Number} params.num limit num
 */
async function getAllData(con, params) {
    let dbResult = [];
    if (params.dbResultCount.count <= params.num) {
        // console.log(0, dbResultCount.count);
        const result = await getResult([0, params.dbResultCount.count]);
        // console.log(`\n${table.tblname}详情=${JSON.stringify(result)}`);
        dbResult = dbResult.concat(result);
    } else {
        let tempNum = 0;
        for (let index = params.num; index < params.dbResultCount.count; index += params.num) {
            // console.log(index - params.num, index);
            const result = await getResult([index - params.num, params.num]);
            // console.log(`\n${table.tblname}详情=${JSON.stringify(result)}`);
            dbResult = dbResult.concat(result);
            tempNum = index;
        }
        if (tempNum != params.dbResultCount.count) {
            // console.log(tempNum);
            const result = await getResult([tempNum, params.num]);
            dbResult = dbResult.concat(result);
        }
    }
    async function getResult(limit = [0, params.num]) {
        return con.query(getSql(params.table, params.selectConditions, limit)).then(res => res[0]);
    }
    return dbResult;
}

function getSql(table, selectConditions, limit = [0, 2]) {
    return `SELECT * FROM ${table.dbName}.${table.tblname} WHERE ${selectConditions} ORDER BY created_date LIMIT ${limit.join()}`;
}

function createHtml(checkResult) {
    let html = '', table;
    Object.keys(checkResult).forEach(tbName => {
        // 每个表一个table
        let details = `<tr><th bgcolor=#C0C0C0 colspan=${Object.keys(checkResult[tbName][0].resultL).length + 1}>${tbName}</th></tr>`;
        checkResult[tbName].forEach(detail => {
            // console.log(detail);
            let keys = typeof detail.resultL != 'string' ? Object.keys(detail.resultL) : Object.keys(detail.resultR);
            // const resultNum = keys.length;
            // const difKey = detail.dif.replace(new RegExp('.*[\=]+(.*)[\,]+.*'), "$1");
            if (typeof detail.dif == 'string') {
                const tdsKey = keys.map(key => {
                    return `<th bgcolor=#C0C0C0><font color=red>${key}</font></th>`;
                });
                let tdsL = '<th bgcolor=#C0C0C0>左表</th>', tdsR = '<th bgcolor=#C0C0C0>右表</th>';
                if (typeof detail.resultL == 'string') {
                    keys.forEach(key => tdsR += `<td><font color=red>${JSON.stringify(detail.resultR[key])}</font></td>`);
                    tdsL += `<td colspan=${keys.length}><font color=red>${detail.resultL},${detail.dif}</font></td>`;
                } else {
                    keys.forEach(key => tdsL += `<td><font color=red>${JSON.stringify(detail.resultL[key])}</font></td>`);
                    tdsR += `<td colspan=${keys.length}><font color=red>${detail.resultR},${detail.dif}</font></td>`;
                }
                details += ` <tr><th bgcolor=#C0C0C0>字段</th>${tdsKey.join('')}</tr>
                             <tr>${tdsL}</tr><tr>${tdsR}</tr>`;
            } else {
                const difKey = Object.keys(detail.dif);
                const tdsKey = keys.map(key => {
                    return difKey.includes(key) ? `<th bgcolor=#C0C0C0><font color=red>${key}</font></th>` : `<th bgcolor=#C0C0C0 >${key}</th>`;
                });
                let tdsL = '<th bgcolor=#C0C0C0>左表</th>', tdsR = '<th bgcolor=#C0C0C0>右表</th>';
                keys.forEach(key => {
                    tdsL += difKey.includes(key) ? `<td><font color=red>${JSON.stringify(detail.resultL[key])}</font></td>` : `<td>${JSON.stringify(detail.resultL[key])}</td>`;
                    tdsR += difKey.includes(key) ? `<td><font color=red>${JSON.stringify(detail.resultR[key])}</font></td>` : `<td>${JSON.stringify(detail.resultR[key])}</td>`;
                });
                details += ` <tr><th bgcolor=#C0C0C0>字段</th>${tdsKey.join('')}</tr>
                             <tr>${tdsL}</tr><tr>${tdsR}</tr>`;
            }
        });
        table = `<table border=1 cellspacing=0>${details}</table>`;
        console.log(table);
        html += table + '<br/>';
    });
    console.log(html);
    return html;
}

const transport = nodemailer.createTransport({
    service: config.email.service,
    secureConnection: true,
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

//发送邮件
function sendmail(params = {}) {
    return transport.sendMail(Object.assign({
        from: `"自动化用例报告" <${config.email.user}>`,
        // to: ['罗琦<jpw7169@dingtalk.com>'],
        to: ['陆星欣<luxxhz@dingtalk.com>', '罗琦<jpw7169@dingtalk.com>,高胖胖<gaomengzhong@dingtalk.com>,刘军<gua5415@dingtalk.com>'],
        subject: `merfin001数据库比对${common.getCurrentTime()}`,
        text: 'merfin001数据库比对,详情请下载附件查看',
        html: '',
        attachments: []
    }, params), function (error, response) {
        if (error) {
            console.log("邮件发送失败: " + error);
        } else {
            console.log('邮件发送完成: %j', response);
        }
    });
}

async function createPool({ dbName }) {
    const cap = dbAcct[dbName];
    // console.log(cap);
    const pool = mysql.createPool({
        user: cap.user || 'myslh',
        password: 'CottonXu',
        host: cap.host,
        port: cap.port,
    });
    return pool.promise();
};


(async function () {
    const db = ['ts_merfin', 'hw_merfin']
    const con = await createPool({ dbName: db[0] });
    const con1 = await createPool({ dbName: db[1] });
    const time = [common.getDateString([0, 0, -7], 'YYYY-MM-DD HH:mm:ss'), common.getCurrentTime()];// '1970-01-01 00:00:00'
    const [checkResult, errorNum] = await checkDB(con, con1, { time: time, num: 1000 });
    await con.end();
    await con1.end();
    const mail = {
        subject: `merfin001数据库(id)比对,${common.getCurrentTime()}`,
    }
    if (Object.keys(checkResult).length == 0) {
        mail.subject = `无异常:merfin001数据库(id)比对${common.getCurrentTime()}`;
        mail.text = `时间范围${time}\nmerfin001数据库(id)比对，无异常数据;\n左库：${dbAcct[db[0]].host}:${dbAcct[db[0]].port},右库：${dbAcct[db[1]].host}:${dbAcct[db[1]].port}`;
    } else {
        if (errorNum > 50) {
            mail.html = `merfin001数据库(id)比对，存在${errorNum}条异常数据<br/>时间范围${time}<br/>左库：${dbAcct[db[0]].host}:${dbAcct[db[0]].port},右库：${dbAcct[db[1]].host}:${dbAcct[db[1]].port}<br/>详情请下载附件查看`;
            const html = mail.html + createHtml(checkResult);
            mail.attachments = [{
                filename: 'merfin001数据库(id)比对.html',   // 附件名
                path: `${__dirname}/../temp/checkDB.html`,  // 附件路径
                // cid: 'fn_01'  // _id 可被邮件使用
            }];
            const htmlPath = path.join(__dirname, `./../temp/checkDB.html`);
            fs.writeFile(htmlPath, html, function (err) {
                if (err) console.log(err);
            });
        } else {
            mail.html = `merfin001数据库(id)比对，存在${errorNum}条异常数据<br/>时间范围${time}<br/>左库：${dbAcct[db[0]].host}:${dbAcct[db[0]].port},右库：${dbAcct[db[1]].host}:${dbAcct[db[1]].port}` + createHtml(checkResult);
        }
    }
    sendmail(mail);
})();