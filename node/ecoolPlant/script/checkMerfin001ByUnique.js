const common = require('../../lib/common');
const fs = require('fs');
const path = require('path');
const nodemailer = require('nodemailer');
const config = require('../../data/caps');
const mysql = require('mysql2');


const dbAcct = {
    // ts_my02i11_merfin01db11
    ts_merfin: {
        user: 'mychk',
        host: '115.159.50.200',
        port: '30211'
    },
    // ts_my02i11_merfin01db12
    ts_merfin2: {
        user: 'mychk',
        host: '115.159.50.200',
        port: '30212'
    },
    // hws2_my02i11_merfin01db11
    hw_merfin: {
        user: 'mychk',
        host: '119.3.92.72',
        port: '30211'
    },
    // hws2_pay02db11
    hw_pay: {
        user: 'mychk',
        host: '119.3.92.72',
        port: '30111'
    },

};

const tables = [
    { dbName: 'merfin001', tblname: 'fin_acct', unique: ['unit_id', 'owner_id', 'owner_kind', 'acct_type'] },
    // { dbName: 'merfin001', tblname: 'fin_acct_daily', unique: ['acct_id', 'unit_id', 'pro_date'] },
    // { dbName: 'merfin001', tblname: 'fin_acct_flow', unique: ['unit_id', 'hash_key'] },
    // { dbName: 'merfin001', tblname: 'fin_acct_flow_daily', unique: ['trade_type', 'owner_id', 'unit_id', 'pro_date'] },
    // { dbName: 'merfin001', tblname: 'fin_acct_group_daily', unique: ['unit_id', 'owner_id', 'acct_group_id', 'pro_date'] },
    // { dbName: 'merfin001', tblname: 'fin_cash_bill', unique: ['unit_id', 'hash_key'] },
    // { dbName: 'merfin001', tblname: 'fin_merchant_bank', unique: ['unit_id', 'bank_account_no'] },
    // { dbName: 'merfin001', tblname: 'fin_merchant_bank_settle', unique: ['unit_id', 'mer_bank_id', 'platform_code'] },
    // { dbName: 'merfin001', tblname: 'fin_merchant_channel', unique: ['unit_id', 'platform_code'] },
    // { dbName: 'merfin001', tblname: 'fin_receipt_bill', unique: ['unit_id', 'hash_key'] },
    // { dbName: 'merfin001', tblname: 'fin_sub_merchant', unique: ['merchant_id', 'unit_id'] },
];

async function checkDB(con, con1, params = { time: ['1970-01-01 00:00:00', '2019-07-16 00:00:00'], num: 1000 }) {
    let checkResult = {};
    for (const table of tables) {
        let selectConditions = '';
        // 查询数据库ts_merfin
        // 1.查询dbl count(*)
        const dbResultCountL = await con.query(`SELECT COUNT(*) AS count FROM ${table.dbName}.${table.tblname} WHERE created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`).then(res => res[0][0]);
        // console.log(`\n${table.tblname}数量L=${JSON.stringify(dbResultCountL)}`);

        // 2.dbl查出所有数据
        selectConditions = `created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`;
        let dbResultL = [];
        if (dbResultCountL.count <= params.num) {
            // console.log(0, dbResultCountL.count);
            const resultL = await getResultL([0, dbResultCountL.count]);
            // console.log(`\n${table.tblname}详情L=${JSON.stringify(resultL)}`);
            dbResultL = dbResultL.concat(resultL);
        } else {
            let tempNum = 0;
            for (let index = params.num; index < dbResultCountL.count; index += params.num) {
                // console.log(index - params.num, index);
                const resultL = await getResultL([index - params.num, index]);
                // console.log(`\n${table.tblname}详情L=${JSON.stringify(resultL)}`);
                dbResultL = dbResultL.concat(resultL);
                tempNum = index;
            }
            if (tempNum != dbResultCountL.count) {
                // console.log(tempNum, dbResultCountL.count);
                const resultL = await getResultL([tempNum, dbResultCountL.count]);
                dbResultL = dbResultL.concat(resultL);
            }
        }
        async function getResultL(limit = [0, params.num]) {
            return con.query(getSql(table, selectConditions, limit)).then(res => res[0]);
        }
        // console.log(dbResultL);
        // console.log(dbResultL.length);

        // 查询数据库ts_merfin2
        // 1.查询dbr count(*)
        // const dbResultCountR = await con1.query(`SELECT COUNT(*) AS count FROM ${table.dbName}.${table.tblname} WHERE created_date > '${params.time[0]}' AND created_date < '${params.time[1]}'`).then(res => res[0][0]);
        // console.log(`\n${table.tblname}数量R=${JSON.stringify(dbResultCountR)}`);

        // if (dbResultCountL.count != dbResultCountR.count) {
        //     result.dif = `左右表总数量不一致:左表${dbResultCountL.count},右表${dbResultCountR.count};`
        // }
        // 根据dbl查出的n条数据，查出数据对比
        for (const resultL of dbResultL) {
            if (resultL.hasOwnProperty('pro_date')) resultL.pro_date = `'${common.utcToLocaleDate(resultL.pro_date)}'`;
            if (resultL.hasOwnProperty('hash_key')) resultL.hash_key = `'${resultL.hash_key}'`;
            if (resultL.hasOwnProperty('platform_code')) resultL.platform_code = `'${resultL.platform_code}'`;
            selectConditions = '';
            table.unique.forEach((uq, index) => {
                let cond = uq + ' = ' + resultL[uq] + ' AND ';
                if (index == table.unique.length - 1) {
                    cond = uq + ' = ' + resultL[uq];
                }
                selectConditions += cond;
            });
            // console.log(selectConditions);

            const dbResultR = await con1.query(getSql(table, selectConditions)).then(res => res[0][0]);
            // console.log(`\n${table.tblname}详情R=${JSON.stringify(dbResultR)}`);
            // dbResultR.unit_id = 1;
            let result = { dif: {}, resultL: {}, resultR: {} };
            if (dbResultR) {
                if (dbResultR.hasOwnProperty('pro_date')) dbResultR.pro_date = `'${common.utcToLocaleDate(dbResultR.pro_date)}'`;
                if (dbResultR.hasOwnProperty('hash_key')) dbResultR.hash_key = `'${dbResultR.hash_key}'`;
                if (dbResultR.hasOwnProperty('platform_code')) dbResultR.platform_code = `'${dbResultR.platform_code}'`;
                console.log(JSON.stringify(resultL));
                console.log(JSON.stringify(dbResultR));
                let check = common.difference(resultL, dbResultR);
                console.log(check);
                try {
                    check = JSON.parse(JSON.stringify(check));
                } catch (error) {
                    if (check.created_date) check.created_date = '';
                    if (check.updated_date) check.updated_date = '';
                    check = JSON.parse(JSON.stringify(check));
                }
                console.log(check);
                if (Object.keys(check).length != 0) {
                    result.dif = check;
                    result.resultL = resultL;
                    result.resultR = dbResultR;
                }
            } else {
                result.dif = `查询条件为:${selectConditions};`;
                result.resultL = resultL;
                result.resultR = '未查到数据';
            }
            console.log(result);
            if (Object.keys(result.dif).length) {
                if (!checkResult[table.tblname]) checkResult[table.tblname] = [];
                // console.log(result);
                checkResult[table.tblname].push(result);
            }
        }
    };
    console.log(checkResult);
    console.log(JSON.stringify(checkResult));
    return checkResult;
};


function getSql(table, selectConditions, limit = [0, 2]) {
    return `SELECT * FROM ${table.dbName}.${table.tblname} WHERE ${selectConditions} LIMIT ${limit.join()}`;
}

function createHtml(checkResult) {
    let html = '', table;
    Object.keys(checkResult).forEach(tbName => {
        // 每个表一个table
        let details = `<tr><th bgcolor="#C0C0C0" colspan=${Object.keys(checkResult[tbName][0].resultL).length + 1}>${tbName}</th></tr>`;
        checkResult[tbName].forEach(detail => {
            // console.log(detail);
            const keys = Object.keys(detail.resultL);
            // const resultNum = keys.length;
            // const difKey = detail.dif.replace(new RegExp('.*[\=]+(.*)[\,]+.*'), "$1");
            if (typeof detail.dif == 'string') {
                const tdsKey = keys.map(key => {
                    return `<th bgcolor="#C0C0C0"><font color="red">${key}</font></th>`;
                });
                let tdsL = '<th bgcolor="#C0C0C0">左表</th>', tdsR = '<th bgcolor="#C0C0C0">右表</th>';
                keys.forEach(key => {
                    tdsL += `<td><font color="red">${JSON.stringify(detail.resultL[key])}</font></td>`;
                    // tdsR += difKey.includes(key) ? `<td><font color="red">${JSON.stringify(detail.resultR[key])}</font></td>` : `<td>${JSON.stringify(detail.resultR[key])}</td>`;
                });
                tdsR += `<td colspan=${keys.length}><font color="red">${detail.resultR},${detail.dif}</font></td>`;
                details += ` <tr><th bgcolor="#C0C0C0">字段</th>${tdsKey.join('')}</tr>
                             <tr>${tdsL}</tr><tr>${tdsR}</tr>`;
            } else {
                const difKey = Object.keys(detail.dif);
                const tdsKey = keys.map(key => {
                    return difKey.includes(key) ? `<th bgcolor="#C0C0C0"><font color="red">${key}</font></th>` : `<th bgcolor="#C0C0C0" >${key}</th>`;
                });
                let tdsL = '<th bgcolor="#C0C0C0">左表</th>', tdsR = '<th bgcolor="#C0C0C0">右表</th>';
                keys.forEach(key => {
                    tdsL += difKey.includes(key) ? `<td><font color="red">${JSON.stringify(detail.resultL[key])}</font></td>` : `<td>${JSON.stringify(detail.resultL[key])}</td>`;
                    tdsR += difKey.includes(key) ? `<td><font color="red">${JSON.stringify(detail.resultR[key])}</font></td>` : `<td>${JSON.stringify(detail.resultR[key])}</td>`;
                });
                details += ` <tr><th bgcolor="#C0C0C0">字段</th>${tdsKey.join('')}</tr>
                             <tr>${tdsL}</tr><tr>${tdsR}</tr>`;
            }
        });
        table = `<table border=1 cellspacing=0>${details}</table>`;
        console.log(table);
        html += table + '<br/>';
    });
    console.log(html);
    return html;
}

const transport = nodemailer.createTransport({
    service: config.email.service,
    secureConnection: true,
    auth: {
        user: config.email.user,
        pass: config.email.pass
    }
});

//发送邮件
function sendmail(params = {}) {
    return transport.sendMail(Object.assign({
        from: `"自动化用例报告" <${config.email.user}>`,
        // to: ['罗琦<jpw7169@dingtalk.com>'],
        to: ['陆星欣<luxxhz@dingtalk.com>', '罗琦<jpw7169@dingtalk.com>'],
        subject: `merfin001数据库比对${common.getCurrentTime()}`,
        text: 'merfin001数据库比对,详情请下载附件查看',
        html: '',
        attachments: []
    }, params), function (error, response) {
        if (error) {
            console.log("邮件发送失败: " + error);
        } else {
            console.log('邮件发送完成: %j', response);
        }
    });
}

async function createPool({ dbName }) {
    const cap = dbAcct[dbName];
    // console.log(cap);
    const pool = mysql.createPool({
        user: cap.user || 'myslh',
        password: 'CottonXu',
        host: cap.host,
        port: cap.port,
    });
    return pool.promise();
};


(async function () {
    const db = ['ts_merfin', 'hw_merfin']
    const con = await createPool({ dbName: db[0] });
    const con1 = await createPool({ dbName: db[1] });
    const time = ['1970-01-01 00:00:00', common.getCurrentTime()];
    const checkResult = await checkDB(con, con1, { time: time, num: 1000 });
    await con.end();
    await con1.end();
    const mail = {
        subject: `时间范围${time},merfin001数据库(UNIQUE)比对${common.getCurrentTime()}`,
    }
    if (Object.keys(checkResult).length == 0) {
        mail.subject = `无异常:时间范围${time},merfin001数据库(UNIQUE)比对${common.getCurrentTime()}`;
        mail.text = `时间范围${time}\nmerfin001数据库(UNIQUE)比对，无异常数据;\n左库：${dbAcct[db[0]].host}:${dbAcct[db[0]].port},右库：${dbAcct[db[1]].host}:${dbAcct[db[1]].port}`;
    } else {
        mail.html = `merfin001数据库(UNIQUE)比对，存在异常数据<br/>时间范围${time}<br/>左库：${dbAcct[db[0]].host}:${dbAcct[db[0]].port},右库：${dbAcct[db[1]].host}:${dbAcct[db[1]].port}
        ` + createHtml(checkResult);
    }
    sendmail(mail);
})();