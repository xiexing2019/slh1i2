const name = require('../../data/caps').name;

/**
 *
 */
const account = {
    plant_test: {
        // merchant_CITIC: { merchantId: 2713, merBankId: 3225 },
        merchant_CITIC: { merchantId: 4417, merBankId: 3225 },
        merchant_MSFPAY: { merchantId: 4345, merBankId: 3129 },

        merchant_CITIC_danger: { merchantId: 133, merBankId: 69 } //危险操作用,比如商户禁用
    },
    plant_online: {
        merchant_CITIC: { merchantId: 3095, merBankId: 2883 },
        merchant_MSFPAY: { merchantId: 5011, merBankId: 4571 },
        merchant_ALL: { merchantId: 5011, merBankId: 4571 }
    }
};



module.exports = account[name];