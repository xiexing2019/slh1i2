const common = require('../../lib/common');

const slhStaff = module.exports = {};

/**
 * loginDo - 登陆login.do
 * @description 获取sessionId
 * @param {object}  [params={}] {logid,pass}
 * @param {boolean} [flag=true] false时,强制登陆
 */
slhStaff.loginDo = async function (params = {}, flag = true) {
    params = Object.assign({
        logid: '000',
        pass: '000000',
        deviceno: caps.deviceNo,
        dlProductCode: caps.dlProductCode,
        dlProductVersion: caps.slhVersion,
        epid: caps.epid,
        language: 'zh-Hans-CN',
        slh_version: caps.slhVersion,
    }, params);
    //同一个帐套+工号时不重新登录
    if (flag && params.logid === LOGINDATA.code && params.epid === LOGINDATA.epid) return;

    await common.post(`${caps.url}/slh/login.do`)
        .send(params)
        .type('form')
        .then((res) => {
            // console.log(`res = ${JSON.stringify(res)}`);
            LOGINDATA = JSON.parse(res.text);
            expect(LOGINDATA, `工号${params.logid}登陆失败\n${JSON.stringify(res)}`).to.not.have.property('error');
            USEECINTERFACE = LOGINDATA.serverCode && LOGINDATA.serverCode == 'G2' ? 2 : 1;
        }).catch((err) => {
            throw new Error(`login.do请求失败:${err.message}`);
        });
    // console.log(`LOGINDATA = ${JSON.stringify(LOGINDATA)};`);
};