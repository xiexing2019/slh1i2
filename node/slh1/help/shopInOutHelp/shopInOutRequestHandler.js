'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const inOutJson = require('../json/inOutJson.js');
const reqHandler = require('../reqHandlerHelp');

/*
 * 封装门店调出/门店调入基础的请求类，如：sf、qf
 */
let requestHandler = module.exports = {};

/*
 * 门店调出
 */

/*
 * 查询调出单详情 qf-1862-1
 * 对应 sf-1862-1
 */
requestHandler.shopOutQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	params.interfaceid = 'qf-1862-1';
	let res = await reqHandler.queryBilling(params);
	return res;
};


/**
 * 门店调出按 批次查
 * @param {object} params 
 */
requestHandler.getShopOutList = async function (params) {
	params = Object.assign({ interfaceid: 'ql-1862' }, params);
	return reqHandler.qlIFCHandler(params);
};


/*
 * 作废调拨出库单
 */
requestHandler.cancelShopOutBill = async function (pk, succeed = true) {
	let res = await common.callInterface('cs-cancel-moveout-bill', {
		'pk': pk,
	});
	succeed && expect(res, `作废单据失败,reason:${JSON.stringify(res)}`).to.includes({
		'val': 'ok',
	});
	return res;
};


/**
 * 在途调拨按 批次查
 * @param {object} params 
 */
requestHandler.getShopInList = async function (params) {
	params = Object.assign({ interfaceid: 'ql-1867' }, params)
	return reqHandler.qlIFCHandler(params);
};

/*
 * 门店调入
 */

/**
 * 调拨入库保存
 * @description 二代生成调出单时就会生成一个调入单 需要使用调入单明细的pk
 * @param onRoadInfo - 调入单明细 qf-1863-3
 * @param {boolean} [check=true] 断言
 * @return {object} {params,result}
 */
requestHandler.shopIn = async function (onRoadInfo, check = true) {
	onRoadInfo = inOutJson.inJson(onRoadInfo);
	let inResult = await common.callInterface('sf-14214-1', {
		'jsonparam': onRoadInfo,
	});
	check && expect(inResult, `sf-14214-1调拨入库失败\njsonparam:${JSON.stringify(onRoadInfo)}\nresult:${JSON.stringify(inResult)}`).to.not.have.property('error');

	return {
		'params': onRoadInfo,
		'result': inResult
	};
};

/**
 * shopInQueryBilling - 获取调入单明细
 * @description  action为add时,返回的prodata默认为当天 为edit时,返回的prodata为调出单的prodata
 * @param {object|string} params
 * @param {string} params.pk 二代需要用调入单的pk ql-1867查询得到的id值
 * @param {string} params.billno 调拨单批次号 不知道调入单pk时传,获取调入单pk
 * @return {object} {params,result}
 */
requestHandler.shopInQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	if (USEECINTERFACE == 2 && params.billno) {
		params.pk = await common.callInterface('ql-1867', {
			id1: params.billno,
			id2: params.billno,
			invidref: LOGINDATA.invid,
		}).then(res => res.dataList[0].id); //获取调入单id
		delete params.billno;
	};
	params.interfaceid = 'qf-1863-3';
	params.actid = 16;
	let res = await reqHandler.queryBilling(params);
	return res;
};

/*
 * 作废调入单
 */
requestHandler.cancelShopInBill = async function (pk, succeed = true) {
	let res = await common.callInterface('cs-cancel-movein-bill', {
		'pk': pk,
	});
	succeed && expect(res, `作废单据失败,reason:${JSON.stringify(res)}`).to.includes({
		'val': 'ok',
	});
	return res;
};


/**
 * shopInAllOnroadBilling - 批量接收调拨单
 * @description
 * 盘点处理前提是相应款号没有未调入单
 * 绑定/解除仓库绑定  相应门店没有调入单
 * 若调拨单过多 可能会超时
 * 受后台功能参数单据可补登天数限制
 * ql-1862门店调入-在途调拨查询受登陆门店限制,只能查询本门店数据
 * @async
 * @param {string} shopid 调入门店的shopid
 */
requestHandler.shopInAllOnroadBilling = async function (shopid) {
	let qlRes = [],
		code,
		params = format.qlParamsFormat({
			'invidref': shopid, //调入门店
			'flag': 0, //状态 0未接收 2已接收
			'invalidflag': 0,
			'prodate1': common.getDateString([0, -3, 0]),
			'prodate2': common.getDateString([0, 0, 0]),
		});
	if (USEECINTERFACE == 1) {
		qlRes = await common.callInterface('ql-1862', params); //门店调出-按批次查
		//console.log(`qlRes : ${JSON.stringify(qlRes)}`);
	} else {
		if (shopid != LOGINDATA.invid) {
			code = LOGINDATA.code;
			let code2 = _.findKey({
				'000': 'shopidCqd',
				'200': 'shopidZzd',
				'400': 'shopidWyd',
				'100': 'shopidCkd',
			}, (value) => BASICDATA[value] == shopid); //根据门店id获取登陆工号
			await common.loginDo({
				logid: code2
			});
		};
		qlRes = await common.callInterface('ql-1867', params); //门店调出-在途调拨
	};
	let ids = qlRes.dataList.map(data => data.id); //调拨单pks
	for (let i = 0, length = ids.length; i < length; i++) {
		let qfRes = await requestHandler.shopInQueryBilling(ids[i]);
		await requestHandler.shopIn(qfRes.result); //调入
		// await common.delay(100);
	};
	if (USEECINTERFACE == 2 && code) {
		await common.loginDo({
			logid: code
		});
	};
};
