"use strict";
const common = require('../../../lib/common.js');

/**
 * 封装打印相关接口
 *
 * @module requestHandler
 */
let requestHandler = module.exports = {};

//销售单打印前更新打印标记，并获取待打印单据的相关信息
requestHandler.getPrint = async function (sfRes, check = true) {
	let param = {
		pk: sfRes.result.pk,
		needRemotePrint: 0, //7.27版本以上该参数必须传，如果不需要远程打印要传needRemotePrint=0，否则无法兼容老版本
		isUpdatePrintFlag: 1, //是否更新打印标记，本接口承担另种用途，1.打印时获取打印信息获取信息，并更新打印标记、打印时间、打印次数。2.修改保存前调用此结构获取信息有什么用？
		opid: LOGINDATA.id,
		isPend: sfRes.params.invalidflag == 9 ? 1 : 0, //是否挂单
	};
	if (USEECINTERFACE == '2') {
		switch (sfRes.params.interfaceid) {
			case 'sf-14212-1': //采购入库
				param.printType = 22;
				break;
			case 'sf-14211-1': //销售开单
				param.printType = 21;
				break;
			case 'sf-1862-1': //调拨出库
				param.printType = 25;
				break;
			default:
				break;
		};
	};
	const printRes = await common.callInterface('cs-getPrint', param);
	check && expect(printRes, `打印错误:${JSON.stringify(printRes)}`).to.not.have.property('error');
	return printRes;
};
