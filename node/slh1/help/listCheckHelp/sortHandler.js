'use strict'

require('../../../lib/global.js');

let sortHandler = module.exports = {};
/*
 * 获取一个对象除modelClass外所有的属性
 */
sortHandler.getPropertys = function (obj) {
	let fields = [];
	for(let i in obj) {
		if(i == 'modelClass') continue;
		fields.push(i);
	}
	return fields;
}

/*
 * 给array数组升序排序
 * isNumber 数组元素是否是数字
 */
sortHandler.arraySort = function (array, isNumber = undefined) {

	if(isNumber == undefined) {
		isNumber = true;
		for(let i = 0; i < array.length; i++) {
			if(isNaN(array[i])) {
				isNumber = false;
				break;
			}
		}
	}

	let newArray = [];
	if(isNumber) {
		newArray = _.cloneDeep(array.sort(function (a, b) {
			if(a > b) {
				return 1;
			} else if(a < b) {
				return -1
			} else {
				return 0;
			}
		}));
		// console.log(`number newArray : ${JSON.stringify(newArray)}`);
	} else {
		newArray = _.cloneDeep(array.sort());
		// console.log(`varchar newArray : ${JSON.stringify(newArray)}`);
	}

	return newArray;
}
