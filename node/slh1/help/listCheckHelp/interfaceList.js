'use strict';
let interfaceList = module.exports = {};

let dedupe = array => Array.from(new Set(array));

//查询列表接口
//用于排序，汇总验证
//盘点管理-盈亏表ql-1926的汇总值在盈亏表的用例中验证(盘点跑完会撤销所有处理记录,会没有数据)
interfaceList.qlListInterfaceId = dedupe(['ql-22301', 'ql-22302'
		, 'ql-1195', 'ql-14424', 'ql-14458', 'ql-1432'
		, 'ql-14451', 'ql-14457', 'ql-14489', 'ql-144891', 'ql-1209', 'ql-1932', 'ql-14485', 'ql-14459', 'ql-14485', 'ql-144851', 'ql-144571', 'ql-142201'
		, 'ql-14490', 'ql-14426', 'ql-14433', 'ql-14425', 'ql-14222', 'ql-1542', 'ql-1432', 'ql-14521', 'ql-14223', 'ql-16235', 'ql-1623', 'ql-1352'
		, 'ql-1442', 'ql-1339', 'ql-14431', 'ql-15005', 'ql-1350', 'qd-15002', 'ql-1427', 'qd-2241', 'ql-14434', 'ql-14439', 'ql-14442', 'ql-14438'
		, 'ql-14436', 'ql-1641', 'ql-144331', 'ql-1862', 'ql-1865', 'ql-1867', 'ql-1863', 'ql-1866', 'ql-1869', 'ql-1868', 'ql-2252', 'ql-2254', 'ql-2253'
		, 'ql-22921', 'ql-2250', 'ql-2251', 'ql-22306', 'ql-1413', 'ql-13471', 'ql-22922', 'ql-14510', 'ql-22103', 'ql-22311', 'ql-22312', 'ql-22102'
		, 'ql-22313', 'ql-1923', 'ql-1924', 'ql-1928', 'ql-1929', 'ql-1352', 'ql-1354', 'ql-1360', 'ql-13601', 'ql-1728', 'ql-1623', 'ql-1447001', 'ql-1345', 'ql-1346'
		, 'ql-16234', 'ql-16232', 'ql-16231', 'ql-16233', 'ql-1728', 'ql-1623', 'ql-1447001', 'ql-1938', 'ql-1939', 'ql-19903'
	]);

interfaceList.qlIFCParams = {
	'ql-144851': 'dwid=BASICDATA.dwidXw',
	'ql-14521': 'shopid=LOGINDATA.invid;logisDwid=BASICDATA.dwidSFKD',
	'ql-13471': 'dwid=BASICDATA.dwidVell',
};

/*
 * 默认排序验证 (desc)
 * 不同接口规则不同
 */
interfaceList.defaultSortordIFC = {
	'ql-142201': ['prodate', 'optime', 'id'],
	'ql-1209': ['prodate', 'billno'],
	'ql-22301': ['prodate', 'billno', 'optime', 'id'],
	'ql-22302': ['mainProdate', 'mainBillno'],
	'ql-22306': ['prodate', 'code'],
	'ql-22102': ['prodate', 'code'],
	'ql-22103': ['prodate', 'code'],
	'ql-1867': ['optime', 'id'],
	'ql-1866': ['prodate', 'billno'],
	'ql-1862': ['prodate', 'billno'],
	'ql-1865': ['mainProdate', 'mainBillno'],
	'ql-14433': ['prodate', 'billno'],
	'ql-14431': ['mainProdate', 'mainOrderno'], //det.id
	// 'ql-14223': ['prodate', 'orderno'],//查询条件有prodate 而结果没有 无法验证
	'ql-1923': ['prodate', 'billno'],
	'ql-1924': ['prodate', 'mainBillno'],
	'ql-1928': ['prodate', 'billno'],
	'ql-15006': ['prodate', 'billno'],
};
