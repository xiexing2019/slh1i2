"use strict";
const common = require('../../lib/common');

let reqHandler = module.exports = {};


/**
 * csIFCHandler - cs接口处理
 * @param {object} params
 * @param {string} params.interfaceid
 * @param {string} params.pk
 * @param {string} params.isPend 是否挂单(二代) 0:否,1:是
 * @param {boolean} params.check 是否需要断言
 * @param {string} params.errorMsg 需要断言时的错误提示
 * @return {object} {params,result}
 */
reqHandler.csIFCHandler = async function (params) {
	const {
		check = true,
		errorMsg = `接口${params.interfaceid}请求失败`
	} = params;
	delete params.check;
	delete params.errorMsg;
	if (!params.action && params.pk) params.action = 'edit';
	if (USEECINTERFACE == '2' && !params.isPend) params.isPend = 0;

	let result = await common.callInterface(params.interfaceid, _.cloneDeep(params));
	//默认断言请求结果是否成功
	//失败在用例中自行验证,有些地方可能只是为了保险做一些请求,对于请求成功失败没有需求
	check && expect(result, `${errorMsg},\nparams:${JSON.stringify(params)}\nreason:${JSON.stringify(result)}`).not.to.have.any.keys('error');
	return {
		params,
		result
	};
};

reqHandler.sfIFCHandler = async function (params) {
	return this.csIFCHandler(params);
};

/**
 * queryBilling - 查询单据详情
 * @param {object} params
 * @param {string} params.interfaceid
 * @param {string} params.pk
 * @param {string} params.isPend 是否挂单 0:否,1:是
 * @return {object} {params,result}
 */
reqHandler.queryBilling = async function (params) {
	params = Object.assign({
		action: 'edit',
		isPend: 0, //二代 是否挂单 0:否,1:是
	}, params);
	return this.csIFCHandler(params);
};

/**
 * ql请求处理
 * @description 一次请求数据太多会损耗性能,需要分成多次请求
 * @param {object} params
 * @param {params} params.interfaceid
 * @return {object} {params,result}
 */
reqHandler.qlIFCHandler = async function (params) {
	const {
		check = true,
	} = params;
	delete params.check;
	const pageSize = params.pagesize || 500;

	//第一次默认请求,只获取汇总值和条目数
	const _params = Object.assign({}, params, {
		search_list: 0, //不获取列表信息
		search_count: 1,
		search_sum: 1,
	});
	let res = await common.callInterface(params.interfaceid, _params);
	if (check) {
		if (res.error) throw new Error(`${params.interfaceid}查询出错,params:${JSON.stringify(_params)},res:${JSON.stringify(res)}`);
		// if(res.count == 0) throw new Error(`${params.interfaceid}查询无结果,params:${JSON.stringify(_params)}`);
	};

	//只获取列表信息
	const pageNum = Math.ceil(res.count / pageSize);
	const qlParams = new Array(pageNum).fill({}).map((obj, index) => {
		return Object.assign({}, params, {
			curpageno: index + 1,
			pagesize: pageSize,
			search_list: 1,
			search_count: 0,
			search_sum: 0
		});
	});

	const dyadicArr = _.chunk(qlParams, 10); //限制并发数量
	for (const arr of dyadicArr) {
		const promises = arr.map((qlParam) => common.callInterface(params.interfaceid, qlParam));
		const listRes = await Promise.all(promises);

		listRes.forEach((element) => {
			if (element.error) {
				console.error('error', element.error);
			} else {
				res.dataList.push(...element.dataList);
			};
		});
	};
	return {
		params,
		result: res,
	};
};

/**
 * editHangBill - 新增修改挂单
 * @param {object} jsonparam
 * @param {string} jsonparam.interfaceid 接口id
 * @param {string} jsonparam.pk 单据pk值(修改时需要传)
 * @return {object} {params,result}
 */
reqHandler.editHangBillHandler = async function (jsonparam, check = true) {
	jsonparam.action = (jsonparam.pk || jsonparam.pendId) ? 'edit' : 'add';
	jsonparam.invalidflag = 9;
	if (USEECINTERFACE == 2) jsonparam.fromPend = 0; //挂单转正式单传1,其他都传0
	let sfRes = await common.editBilling(jsonparam, check);
	return sfRes;
};

/**
 * hangToBillHandler - 挂单转正式单
 * @description 挂单转正式单会生成一个新的pk
 * @param {object}    jsonparam    开单接口对应的qf接口返回值
 * @param {string}    jsonparam.interfaceid  开单接口id
 * @param {string}    jsonparam.pk  单据pk值
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} {params:jsonparam,result:接口返回结果}
 */
reqHandler.hangToBillHandler = async function (jsonparam, check = true) {
	jsonparam.invalidflag = 0;
	jsonparam.action = 'edit';
	if (USEECINTERFACE == 2) {
		jsonparam.fromPend = 1; //转正式单传1，其他都传0
		// jsonparam.pendId = jsonparam.pk; //挂单id
		// delete jsonparam.pk;
	};
	let sfRes = await common.editBilling(jsonparam, false); //需要拼接更新一下jsonparam(flag,hashkey)
	check && expect(sfRes.result, `interfaceid=${jsonparam.interfaceid},挂单转正式单失败:${JSON.stringify(sfRes)}`).to.not.have.property('error');
	return sfRes;
};
