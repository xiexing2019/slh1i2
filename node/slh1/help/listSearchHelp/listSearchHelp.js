'use strict';
const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');

let listSearchHelp = module.exports = {};

const defaultParams = {
  pagesize: 15,
  search_sum: 0
}
/* 
  data = {
	'interfaceid': [
    ['searchKey', 'searchValue', 'verifyKey', 'verifyValue']
	]
}
*/
listSearchHelp.searchVerity = async function (data) {
  const interfaceid = Object.keys(data)[0];
  let searchConditions = data[interfaceid];

  let errMsg = '';
  for (let i = 0; i < searchConditions.length; i++) {

    let result = await singleConditionSearchAndVerity(interfaceid, searchConditions[i]);
    if (result.length > 0) {
      errMsg += result;
    }
  }

  return {
    errMsg,
    verifyResult: errMsg.length === 0
  };
}

/* 
  condition = ['searchKey', 'searchValue', 'verifyKey', 'verifyValue'] 
  or ['searchKey1', 'searchValue1', 'searchKey2', 'searchValue2', 'verifyKey', 'verifyValue']
  verifyKey和verifyValue可以为空
*/
async function singleConditionSearchAndVerity(interfaceid, condition) {
  let params = {};
  let searchKey = condition[0], searchValue = condition[1], verifyKey, verifyValue;
  params[searchKey] = searchValue;

  let isDate = false;
  if (condition.length > 4) {
    isDate = true;
    params[condition[2]] = condition[3];
    verifyKey = condition[4];
  } else {
    verifyKey = condition.length > 2 ? condition[2] : searchKey;
    verifyValue = condition.length > 3 ? condition[3] : searchValue;
  }

  let errMsg = '';
  let result = await common.callInterface(interfaceid, common.mixObject(defaultParams, params));
  if (result.error !== undefined) {
    errMsg += `\n${interfaceid}：条件${searchKey}请求出错,请求返回结果为:${JSON.stringify(result)}`
  } else {
    let dataList = result.dataList;
    if (dataList.length === 0) {
      errMsg += `\n${interfaceid}：条件${searchKey}查询无果,无法进行验证`
    } else {
      for (let data of dataList) {
        if (isDate) {
          let seq = common.subtractionDateString(searchValue, data[verifyKey]);
          if (seq === 1) {
            errMsg += `\n${interfaceid}：条件${searchKey}查询出错`;
            break;
          }
          seq = common.subtractionDateString(condition[3], data[verifyKey]);
          if (seq === -1) {
            errMsg += `\n${interfaceid}：条件${searchKey}查询出错`;
            break;
          }
          break;
        } else {
          if (data[verifyKey] !== verifyValue) {
            errMsg += `\n${interfaceid}：条件${searchKey}查询出错`;
            break;
          }
        }
      }
    }
  }

  return errMsg;
}