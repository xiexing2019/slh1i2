"use strict";

const salesJson = require('./json/salesJson.js');
const inOutJson = require('./json/inOutJson.js');
const purchaseJson = require('./json/purchaseJson.js');
const otherJson = require('./json/otherJson.js');
const orderJson = require('./json/salesOrderJson.js');
const invCheckJson = require('./json/invCheckJson');
const common = require('../../lib/common');
const resportSumJson = require('./json/reportSumJson.js')

module.exports = common.mixObject(salesJson, inOutJson, purchaseJson, otherJson, orderJson, invCheckJson, resportSumJson);
