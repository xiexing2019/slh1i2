'use strict'

let logisVerifyHelp = module.exports = {};

const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../basiceJsonparam.js');

/*
 * 最基础的代收/物流单核销操作
 * 只核销1个代收单，核销时没有特殊货品
 *
 * param   varietyPayment 是否使用多种付款方式
 * 默认只使用现金付款
 *
 * return {verifyJson：*,verifyResult：*}
 * 返回 '提交的核销单信息'、'核销单的返回结果'
 */
logisVerifyHelp.doLogisVerify = async function (remark = '物流商核销', varietyPayment = false) {
	// 开代收单
	let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());
	if (USEECINTERFACE == 2) {
		let verifyRes = await common.callInterface('ql-14521', format.qlParamsFormat({ //代收单列表
			shopid: salesResult.params.shopid,
			logisPayflag: 0,
			dwid: salesResult.params.dwid,
			logisDwid: salesResult.params.logisDwid,
		}, true));
		salesResult.result.verifypk = verifyRes.dataList[0].id; //二代物流核销时，选择单据的pk和销售开单时的pk值不一样
	}
	// 物流商核销
	let verifyJson = {
		'cash': salesResult.params.agency,
		'logisVerifybillids': USEECINTERFACE == 1 ? salesResult.result.pk : salesResult.result.verifypk,
		'logisVerifysum': salesResult.params.agency,
		'remark': remark,
		'dwid': salesResult.params.logisDwid,
	};
	if (varietyPayment) {
		verifyJson.card = 1;
		verifyJson.remit = 1;
		verifyJson.cash = verifyJson.logisVerifysum - verifyJson.card - verifyJson.remit;
	}
	verifyJson = format.verifyJsonFormat(verifyJson);
	let verifyBillRes = await common.callInterface('sf-1452', {
		jsonparam: verifyJson,
	});
	expect(verifyBillRes, `物流商核销失败`).to.not.have.property('error');

	return {
		'verifyJson': verifyJson,
		'verifyResult': verifyBillRes,
		'salesJsonparam': salesResult.params,
		'salesResult': salesResult.result
	};
}

/*
 * 多个代收/物流单核销操作 （核销时加上特殊货品）
 *
 * param   varietyPayment 是否使用多种付款方式
 * 默认只使用现金付款
 *
 * return {verifyJson：*,verifyResult：*}
 * 返回 '提交的核销单信息'、'核销单的返回结果'
 */
logisVerifyHelp.doVarietyBillLogisVerify = async function (remark = '物流商核销', varietyPayment = false) {
	// 开代收单
	let salesResult1 = await common.editBilling(basiceJsonparam.agencySalesJson());
	let salesResult2 = await common.editBilling(basiceJsonparam.agencySalesJson());

	//一代 销售单pk=核销单pk  二代为不同的pk
	let verifyPks = [];
	if (USEECINTERFACE == 2) {
		let verifyRes = await common.callInterface('ql-14521', format.qlParamsFormat({ //代收单列表
			shopid: salesResult1.params.shopid,
			logisPayflag: 0,
			dwid: salesResult1.params.dwid,
			logisDwid: salesResult1.params.logisDwid,
		}, true));
		verifyRes.dataList.forEach((obj) => {
			if (obj.billno == salesResult1.result.billno || obj.billno == salesResult2.result.billno) {
				verifyPks.push(obj.id);
			}
		});
		if (verifyPks.length != 2) throw new Error(`获取核销信息错误 符合条件的核销条目数=${verifyPks.length} != 2`);
	} else {
		verifyPks = [salesResult1.result.pk, salesResult2.result.pk];
	};

	// 物流商核销
	let verifyJson = {
		'card': 1,
		'remit': 1,
		'logisVerifybillids': _.join(verifyPks, ','),
		'logisVerifysum': salesResult1.params.agency + salesResult2.params.agency,
		'remark': remark,
		'dwid': salesResult1.params.logisDwid,
	};
	verifyJson = format.verifyJsonFormat(verifyJson, true); //true表示加上特殊货品,并且设置成多种付款方式
	if (!varietyPayment) {
		verifyJson.cash = verifyJson.logisVerifysum + verifyJson.totalmoney;
		verifyJson.card = 0;
		verifyJson.remit = 0;
	}

	let verifyBillRes = await common.callInterface('sf-1452', {
		jsonparam: verifyJson,
	});
	expect(verifyBillRes, `物流商核销失败`).to.not.have.property('error');

	return {
		'verifyJson': verifyJson,
		'verifyResult': verifyBillRes,
		'salesJsonparam1': salesResult1.params,
		'salesJsonparam2': salesResult2.params,
		'salesResult1': salesResult1.result,
		'salesResult2': salesResult2.result,
	};
}

/*
 * 物流单核销后，验证销售开单--更多--代收收款列表
 *【考虑了核销多张代收单，现金刷卡汇款(微信支付宝除外)的多种付款方式，以及核销时加上特殊货品的情况】
 *
 * verifyJson 提交的核销单信息
 * verifyResult 核销单的返回结果
 */
logisVerifyHelp.collectingPaymentCheck = async function (verifyJson, verifyResult) {
	// 代收收款查询
	let agencyVerifySearch = {
		'dwid': verifyJson.dwid,
		'id1': verifyResult.billno,
		'id2': verifyResult.billno,
		'shopid': LOGINDATA.invid
	};
	let agencyVerifyList = await common.callInterface('ql-1442', format.qlParamsFormat(agencyVerifySearch, true));
	common.isApproximatelyEqualAssert({
		'remit': (verifyJson.remit || 0).toString(),
		'shopname': LOGINDATA.depname,
		'prodate': common.getCurrentDate('YY-MM-DD'),
		'verifysum': verifyJson.logisVerifysum.toString(),
		'totalsum': (Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0)).toString(),
		'totalmoney': (Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0)).toString(),
		'othercost': (Number(verifyJson.totalmoney) || 0).toString(),
		'cash': (verifyJson.cash || 0).toString(),
		'card': (verifyJson.card || 0).toString(),
		'billno': verifyResult.billno
	}, agencyVerifyList.dataList[0]);
}

/*
 * 物流单核销后，验证统计分析--收支流水列表  (这里的收支流水应该是正值，因为代收收款是收入，不是支出)
 *【考虑了核销多张代收单，现金刷卡汇款(微信支付宝除外)的多种付款方式，以及核销时加上特殊货品的情况】
 *
 * verifyJson 提交的核销单信息
 * verifyResult 核销单的返回结果
 */
logisVerifyHelp.inoutRecCheck = async function (verifyJson, verifyResult) {
	// 收支流水查询
	let inoutRecSearch = {
		'accountid': verifyJson.cashaccountid,
		'accountInvid': LOGINDATA.invid
	};
	let inoutRecList = await common.callInterface('ql-1352', format.qlParamsFormat(inoutRecSearch, true));
	// 遍历查询结果，将属于本次物流核销单的收支流水找出来
	let isSearchRight = false,
		inoutRectInfoList = [];
	for (let i = 0; i < inoutRecList.dataList.length; i++) {
		if (inoutRecList.dataList[i].billno == verifyResult.billno && inoutRecList.dataList[i].billtype == '代收收款') {
			isSearchRight = true;
			inoutRectInfoList.push(inoutRecList.dataList[i]);
		}
	}
	expect(isSearchRight, `收支流水有误1`).to.be.true;
	// 验证收支流水中各种付款方式金额是否有误
	let isPaymentRight = true;
	for (let i = 0; i < inoutRectInfoList.length; i++) {
		if (inoutRectInfoList[i].accountid == verifyJson.cashaccountid) {
			isPaymentRight = isPaymentRight && Number(inoutRectInfoList[i].money) == verifyJson.cash;
		}
		if (inoutRectInfoList[i].accountid == verifyJson.cardaccountid) {
			isPaymentRight = isPaymentRight && Number(inoutRectInfoList[i].money) == verifyJson.card;
		}
		if (inoutRectInfoList[i].accountid == verifyJson.remitaccountid) {
			isPaymentRight = isPaymentRight && Number(inoutRectInfoList[i].money) == verifyJson.remit;
		}
	}
	expect(isPaymentRight, `收支流水有误2`).to.be.true;
}

/*
 * 物流单核销后，验证统计分析--综合汇总列表 及明细
 * 现金=销售开单-按批次查界面现金这一列的汇总,此处不再加代收收款的金额,代收收款的金额放到另一列去了
 *【考虑了核销多张代收单，现金刷卡汇款(微信支付宝除外)的多种付款方式，以及核销时加上特殊货品的情况】
 *
 * verifyJson 提交的核销单信息
 * verifyResult 核销单的返回结果
 */
logisVerifyHelp.summaryListCheck = async function (verifyJson, verifyResult) {
	// 综合汇总
	let summaryList = await common.callInterface('ql-1623', format.qlParamsFormat({
		'invid': LOGINDATA.invid
	}, true));
	let batchList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
		'shopid': LOGINDATA.invid
	}, true));
	expect(summaryList.dataList[0], `综合汇总列表现金列有误`).to.includes({
		'cash': batchList.sumrow.cash,
		'remit': batchList.sumrow.remit,
		'card': batchList.sumrow.card
	});

	// 综合汇总明细
	let summaryDetail = await common.callInterface('ql-16235', format.qlParamsFormat({
		'shopid': LOGINDATA.invid,
		'prodate': common.getCurrentDate()
	}));
	let agencySum = 0;
	for (let i = 0; i < summaryDetail.dataList.length; i++) {
		if (summaryDetail.dataList[i].catname == '代收收款') {
			agencySum += Number(summaryDetail.dataList[i].money);
		}
	}
	expect(agencySum.toString(), `综合汇总明细代收项有误`).to.be.equal(summaryList.dataList[0].collection);

	return {
		'summaryList': summaryList,
		'summaryDetail': summaryDetail
	};
}
