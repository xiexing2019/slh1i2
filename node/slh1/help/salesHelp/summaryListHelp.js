'use strict';
let summaryListHelp = module.exports = {};

const common = require('../../../lib/common.js');

/*
 * 验证 实收=现金+微信+刷卡+汇款
 */
summaryListHelp.paysumCheckAssert = function (obj) {
	let sum = (Number(obj.cash) || 0) + (Number(obj.remit) || 0) + (Number(obj.card) || 0) + (Number(obj.weixinpay) || 0) + (Number(obj.alipay) || 0);
	expect(sum, `现金+微信+刷卡+汇款=${sum} 而实收值为:${obj.paysum}`).to.be.equal(Number(obj.paysum));
};

/*
 * 验证 欠款=实销额-(实收+代收)
 * 欠款 balance
 */
summaryListHelp.balanceCheckAssert = function (obj) {
	let sum = ((Number(obj.totalmoney) || 0) - (Number(obj.paysum) || 0) - (Number(obj.daishou) || 0));
	let ret = sum - Number(obj.balance) < 0.001; //浮点数
	expect(ret, `实销额-(实收+代收)=${sum} 而欠款值为:${obj.balance}`).to.be.true;
};

/*
 * 验证 销售数=实销数+退货数
 * 销售数 num
 */
summaryListHelp.numCheckAssert = function (obj) {
	let sum = common.add(obj.actualnum || 0, obj.backnum || 0);
	if (sum != obj.num) {
		throw new Error(`实销数+退货数=${sum} 而销售数为:${obj.num}\n\tjira:SLHSEC-6768`)
	};
};

/*
 * 验证两个对象的现金、刷卡、汇款、微信、支付宝、实收是否相等
 */
summaryListHelp.moneyInfoAssert = function (expected, actual, errorMsg = '') {
	//console.log(`\n preAfter : ${JSON.stringify(expected)} \nactual : ${JSON.stringify(actual)}`);
	//按批次查返回的是actualpay 而不是paysum
	if (expected.paysum) {
		common.isFieldsEqualAssert(expected, actual, 'cash;card;remit;alipay;weixinpay;paysum');
	} else {
		common.isFieldsEqualAssert(expected, actual, 'cash;card;remit;alipay;weixinpay;actualpay=paysum');
	}

}

/*
 * 将两/三个对象的现金、刷卡、汇款、微信、支付宝相加
 */
summaryListHelp.moneySum = function (before, json1, json2 = undefined) {
	before.paysum = (Number(before.paysum) || 0) + (Number(json1.card) || 0) + (Number(json1.cash) || 0) + (Number(json1.remit) || 0);
	before.cash = (Number(before.cash) || 0) + (Number(json1.cash) || 0);
	before.card = (Number(before.card) || 0) + (Number(json1.card) || 0);
	before.remit = (Number(before.remit) || 0) + (Number(json1.remit) || 0);

	if (json2) {
		before.paysum += ((Number(json2.cash) || 0) + (Number(json2.card) || 0) + (Number(json2.remit) || 0));
		before.cash += (Number(json2.cash) || 0);
		before.card += (Number(json2.card) || 0);
		before.remit += (Number(json2.remit) || 0);
	}

	return before;
}
