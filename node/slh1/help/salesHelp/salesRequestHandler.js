const common = require('../../../lib/common.js');
const format = require('../../../data/format');
// const basiceJson = require('../basiceJsonparam.js');
const reqHandler = require('../reqHandlerHelp');

/*
 * 封装销售开单基础的请求类，如：sf、qf
 */
let salesReqHandler = module.exports = {};

/**
 * salesQueryBilling - 查询某个销售单详情 qf-14211-1
 * 对应 sf-14211-1
 * @param {object|string}   params {pk,isPend}|pk
 * @return {object} {params:传入的参数,result:返回结果}
 */
salesReqHandler.salesQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	USEECINTERFACE == 1 ? params.interfaceid = 'qf-1421-1' : params.interfaceid = 'qf-14211-1';
	let res = await reqHandler.queryBilling(params);
	if (USEECINTERFACE == 1 && res.result.actid == 0) res.result.actid = '21';
	return res;
};

salesReqHandler.editSalesBill = async function (sfRes, cb) {
	const qfRes = await salesReqHandler.salesQueryBilling({ pk: sfRes.result.pk });
	let jsonParam = qfRes.result;
	jsonParam.interfaceid = 'sf-14211-1';
	if (typeof cb == 'function') {
		cb(jsonParam);
	};
	return common.editBilling(jsonParam);
};

/*
 * 作废单据
 * succeed=true时 验证作废成功
 */
salesReqHandler.cancelSaleoutBill = async function (pk, succeed = true) {
	let res = await common.callInterface('cs-cancel-saleout-bill', {
		'action': 'edit',
		'pk': pk,
	});
	succeed && expect(res, `作废单据失败,reason:${JSON.stringify(res)}`).to.includes({
		'val': 'ok',
	});
	return res;
};


/**
 * editSalesHangBill - 新增修改销售挂单
 * @description fromPend 转正式单传1,其他都传0
 * @param {object} jsonparam Description
 * @param {string} jsonparam.pk  修改时需要传pk值
 *
 * @return {object} {params,result}
 */
salesReqHandler.editSalesHangBill = async function (jsonparam, check = true) {
	jsonparam.interfaceid = 'sf-14211-1'; //销售开单
	let sfRes = await reqHandler.editHangBillHandler(jsonparam, check);
	return sfRes;
};

/*
 * 挂单转正式单
 * invalidflag=9为挂单
 */
salesReqHandler.hangToSalesBill = async function (jsonparam, check) {
	jsonparam.interfaceid = 'sf-14211-1';
	jsonparam.prodate = common.getCurrentDate(); //挂单转正式单，日期改为当天

	let sfRes = await reqHandler.hangToBillHandler(jsonparam, check);
	return sfRes;
};

/**
 * 按订货开单qf-14401-1
 */
salesReqHandler.salesOrderQueryBilling = async function (pk, isPend = 0) {
	let params = {
		'action': 'edit', //add返回的是空单据的数据
		'pk': pk,
	};
	if (USEECINTERFACE == '2') params.isPend = isPend;
	let orderInfo = await common.callInterface('qf-14401-1', params);
	// expect(orderInfo, `qf-14401-1查询失败:${JSON.stringify(orderInfo)}`).to.not.have.property('error');
	return {
		'params': params,
		'result': orderInfo
	};
};

/**
 * 按订货开单
 * 不修改orderRes的情况下,为全部入库
 * @param {object} orderRes - 销售订货单返回结果 qf-14401-1
 * @param {Boolean} [check=true] - 是否需要断言
 */
salesReqHandler.editSalesBillByOrder = async function (orderRes, check = true) {
	let json = {
		interfaceid: 'sf-14211-1',
		srcType: 2, //按订货开单
		billid: orderRes.pk,
		dwid: orderRes.clientid,
		deliverid: orderRes.sellerid,
		cash: 0,
		card: 0,
		remit: 0,
		remark: '按订货开单'
	};
	delete orderRes.pk; //按订货开单不传pk值
	delete orderRes.clientid;
	delete orderRes.sellerid;
	delete orderRes.actualpay;
	if (USEECINTERFACE == 2) {
		delete orderRes.dwfdid; //客户分店
	};
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(orderRes), json));
	orderRes.details.forEach((obj, index) => {
		jsonparam.details[index].billid = obj.pk;
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});
	let sfData = await common.callInterface('sf-14211-1', {
		'jsonparam': jsonparam,
	});
	check && expect(sfData, `sf-14211-1按订货开单失败\njsonparam:${JSON.stringify(jsonparam)}\nresult:${JSON.stringify(sfData)}`).to.not.have.property('error');
	return {
		'params': jsonparam,
		'result': sfData,
	};
};

/**
 * 新增客户
 */
salesReqHandler.addCust = async function (jsonparam, check = true) {
	// jsonparam.action = jsonparam.pk ? 'edit' : 'add';
	let result = await common.callInterface('sf-1401', {
		'jsonparam': jsonparam
	});
	check && expect(result, `sf-1401新增/修改客户新增失败:\njsonparam:${JSON.stringify(jsonparam)}\nresult:${JSON.stringify(result)}`).not.to.have.property('error');
	return {
		'params': jsonparam,
		'result': result
	};
};

/**
 *	积分兑换
 *	需要传的值 cash:兑换金额 finpayScore:兑换积分 dwid:客户ID值
 *  可选  remark:备注  pk
 */
salesReqHandler.exchangeScore = async function (jsonparam, check = true) {
	let curShop = LOGINDATA.depname == '中洲店' ? 'Zzd' : 'Cqd';
	let params = common.mixObject({
		action: 'add',
		invid: LOGINDATA.invid,
		prodate: common.getCurrentDate(),
		deliver: LOGINDATA.id,
		finpayAccountid: BASICDATA[`cashAccount${LOGINDATA.invid}`], //付款账户ID值
		remark: '积分兑换'
	}, jsonparam);
	let res = await common.callInterface('sf-14520-1', {
		jsonparam: params
	});
	if (check) expect(res, `积分兑换失败:${JSON.stringify(res)}`).to.have.property('pk');
	return {
		'params': params,
		'result': res
	};
};

/*
 * 获取核销信息
 * 同采购单(二代bizType有区别)
 */
salesReqHandler.getVerifyingBills = async function (dwid, invid = LOGINDATA.invid, today = true) {
	let param = format.qlParamsFormat({
		dwid,
		invid,
	}, today);
	if (USEECINTERFACE == '2') param.bizType = 1100; //销售单
	let result = await common.callInterface('ql-15010', param);
	return result;
};

/*
 * 获取物流信息 qf-1432-3
 */
salesReqHandler.getAgencyInfo = async function (pk) {
	let param = {
		pk,
	};
	if (USEECINTERFACE == '2') {
		param.bizType = 1100; //销售单
		param.isPend = 0; //是否挂单 否
	};
	let agencyInfo = await common.callInterface('qf-1432-3', param);
	return agencyInfo;
};
