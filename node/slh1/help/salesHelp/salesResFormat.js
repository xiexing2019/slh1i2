'use strict';
/*
 *  拼接销售开单模块接口返回值
 *  用于与其他接口比较
 *  原则上不删除原有key
 */

let format = module.exports = {};

/*
 *  获取货品库存信息 cs-getinvnum-bycolorsize.dataList
 *  获取款号库存，检查补货，上次价信息 cs-dresSpu-invsale-detailinfo.invnumList
 *  name:colorid_sizeid_invid
 *  返回结果与 当前库存ql-1932 格式相同
 *
 * 	purediffnum	string	采购未到数
 * 	name	string	colorid_sizeid_invid
 *  invnum	string	库存
 *  onroadnum	string	在途数
 *  orderdiffnum	string	订货未到数
 */
format.cs_getinvnumbycolorsizeFormat = function (result) {
	result.forEach((obj) => {
		[obj.colorid, obj.sizeid, obj.invid] = obj.name.split('_');
		// obj.purednum = obj.purediffnum;
		// obj.ordernum = obj.orderdiffnum;
	});
	return result;
};
