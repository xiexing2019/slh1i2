"use strict";

const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const basiceJson = require('../basiceJsonparam.js');
const reqHandlerHelp = require('../reqHandlerHelp');
const getBasicData = require('../../../data/getBasicData.js');
/*
 * 封装销售订货基础的请求类，如：sf、qf
 */
let requestHandler = module.exports = {};

/*
 * 查询某个单据详情 qf-14401-1
 * 对应 sf-14401-1
 * params pk|{pk,isPend}
 * return 返回传入的参数，以及保存后服务端的返回结果
 */
requestHandler.salesOrderQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	params.interfaceid = 'qf-14401-1';
	let res = await reqHandlerHelp.queryBilling(params);
	res.result.pk = params.pk; //qf-14401-1二代挂单未返回
	return res;
};


/*
 * editSalesOrderHangBill - 新增修改销售订单挂单
 * return {jsonparam,result}
 */
requestHandler.editSalesOrderHangBill = async function (jsonparam, check = true) {
	jsonparam.interfaceid = 'sf-14401-1';
	let sfRes = await reqHandlerHelp.editHangBillHandler(jsonparam, check);
	return sfRes;
};


/**
 * hangToSalesOrderBill - 挂单转销售订单
 * @param {object}    jsonparam    qf-14401-1返回结果(需要含有pk值作为pendId)
 * @param {boolean} [check=true] Description
 * @return {object} {params,result}
 */
requestHandler.hangToSalesOrderBill = async function (jsonparam, check = true) {
	jsonparam.interfaceid = 'sf-14401-1';
	jsonparam.prodate = common.getCurrentDate(); //挂单转销售订货单，日期改为当天
	let sfRes = await reqHandlerHelp.hangToBillHandler(jsonparam, check);
	return sfRes;
};

/**
 * saleOrderFinish - 终结订单
 * @param {string|object}    params  pk|{pk,isPend}
 * @return {object} {params,result}
 */
requestHandler.saleOrderFinish = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	let res = await reqHandlerHelp.csIFCHandler(Object.assign({
		interfaceid: 'cs-saleoutOrderFinish',
		check: true, //断言作废成功
		errorMsg: '终结销售订单失败',
	}, params));
	return res;
};

/*
 * 修改订单
 * qfResult 销售订货单qf-14401-1返回结果
 */
requestHandler.editSalesOrderBill = async function (qfResult, check = true) {
	qfResult.interfaceid = 'sf-14401-1';
	qfResult.action = 'edit';
	delete qfResult.shouldpay; //应收
	if (USEECINTERFACE == 2) qfResult.fromPend = 0; //是否挂单 否
	let sfRes = await common.editBilling(qfResult, check);

	return sfRes;
};

/*
 * 作废单据
 * succeed=true时 验证作废成功
 */
requestHandler.cancelSaleorderBill = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	let res = await reqHandlerHelp.csIFCHandler(Object.assign({
		interfaceid: 'cs-saleorder-cancel',
		check: true, //断言作废成功
		errorMsg: '作废销售订单失败',
	}, params));
	return res;
};

/*
 * 销售开单--按订货开单--选中一条明细点击进去的内容为orderInfo
 * 将这里展示的信息转变为调用sf-14211-1需要的jsonparam

requestHandler.changeOrderInfoToSalesInfo = function (orderInfo) {
	orderInfo.action = 'add';
	orderInfo.srcType = '2';
	orderInfo.dwid = orderInfo.clientid;
	orderInfo.deliverid = orderInfo.sellerid;
	orderInfo.billid = orderInfo.pk;
	delete orderInfo.clientid;
	delete orderInfo.sellerid;
	delete orderInfo.pk;
	for(let i = 0; i < orderInfo.details.length; i++) {
		orderInfo.details[i].billid = orderInfo.details[i].pk;
		delete orderInfo.details[i].pk;
	}

	return orderInfo;
}
*/

requestHandler.getSalesOrderMainSearchListExp = function (qfResult) {
	let restnum = 0;
	let endNotPurinNum = 0; //终结未结数
	qfResult.details.map((obj) => {
		restnum += Number(obj.restnum);
	});
	//终结未结数
	if (qfResult.sendflag == 3) {
		endNotPurinNum = restnum;
	};

	return {
		id: qfResult.pk,
		billno: qfResult.billno,
		dwname: qfResult.show_clientid,
		card: qfResult.card,
		inventoryname: qfResult.show_shopid,
		sendflag: qfResult.flag,
		flag: qfResult.showFlag,
		// distname:'',
		num: qfResult.totalnum,
		// diffnum: "5",
		// "printtime": "",
		// deliver: "总经理",
		remark: qfResult.remark,
		endNotPurinNum: endNotPurinNum,
		// "surplusdeposit": "0",
		// "mobilepaystatus": "系统外支付",
		// "paysum": "1010",
		// "payflag": "1",
		// "updatername": "总经理",
		// "totalsum": "1010",
		// "weixinpay": "0",
		// "prodate": "07-06",
		// "restnum": "0",
		// "invalidflag": "0",
		// "delflag": "0",
		// "cash": "300",
		// "endNotdeliverNum": "5",
		// "distflag": "0",
		// "finpayAlipay": "0",
		// "dwfdname": "",
		// "paystat": "0",
		// "alipay": "0",
		// "optime": "07-06 11:03",
		// "modelClass": "SaleOrderBean",
		// "invname": "常青店",
		// "recvnum": "0",
		// "modifyFlag": "0",
		// "invalidFlagName": "否",
		// "distflagname": "否",
		// "remit": "500",
		// "deliverytime": "",
		// "printflag": "0",
		// "totalRoll": "0",
		// "inventoryname": "常青店",
		// "receiptsum": "1010",
		// "card": "210"
	};
};

/**
 * 拼接 销售开单-按明细查结果期望值
 * interfaceid = ql-14431
 * @param {object} qfResult - qf-14401-1 销售订货单据详情
 * @return {array} 期望值
 */
requestHandler.getSalesOrderDetailSearchListExp = function (qfResult) {
	let exp = [];
	for (let i = 0; i < qfResult.details.length; i++) {
		const element = qfResult.details[i];
		const [matCode, matName] = element.show_styleid.split(',');
		let json = {
			'rem': element.rem,
			'mainProdate': qfResult.prodate,
			'mainShopName': qfResult.show_invid,
			'mat_name': matName,
			'mat_code': matCode,
			'mainDwxxName': getBasicData.getNameById(qfResult.clientid), //客户
			'propdresColorid': element.show_colorid,
			'propdresSizeid': element.show_sizeid,
			'price': element.price,
			'num': element.num, //订货数
			'delivernum': element.delivernum, //已发数
			'endNotdeliverNum': 0, //终结未发数
			'flag': qfResult.sendflag,
		};
		json.leftnum = common.sub(json.num, json.delivernum);
		//特殊货品  二代没返回matclsid这个字段，所以判断方式加了一下
		if (element.matclsid == 2 || BASICDATA.specGoodsList.map(val => val.code).indexOf(element.stylecode) > -1) {
			json.num = 0;
			json.delivernum = 0;
		} else {
			if (qfResult.sendflag == 3 && json.leftnum > 0) {
				json.endNotdeliverNum = json.leftnum;
			};
		};
		exp.push(json);
	};
	return exp;
};
