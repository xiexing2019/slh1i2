"use strict";
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const basiceJson = require('../basiceJsonparam.js');

/**
 * 封装盘点管理的请求类，如：sf、qf
 * 有调拨单无法进行盘点处理
 * @module requestHandler
 */
let requestHandler = module.exports = {};

/**
 * invCheckQueryBilling - 查询盘点单据详情 qf-1924-1
 * 对应sf-1924-1
 * @alias module:requestHandler
 * @async
 * @param {string} pk 单据唯一码
 * @return {object} {params,result}
 */
requestHandler.invCheckQueryBilling = async function (pk) {
	const params = {
		'action': 'edit', //add返回的是空单据的数据
		'pk': pk,
	};
	const qfData = await common.callInterface('qf-1924-1', params);

	return {
		'params': params,
		'result': qfData
	};
};


/**
 * getInvCheckMainSearchListExp - 拼接按批次查dataList中一条数据的明细值
 * interfaceid=ql-1923
 * @alias module:requestHandler
 * @param {object} sfRes 开单{params,result}
 * @param {object} qfRes 单据明细{params,result}
 * @return {object}
 */
requestHandler.getInvCheckMainSearchListExp = function (sfRes, qfRes) {
	return {
		billno: sfRes.result.orderno || sfRes.result.billno,
		invName: qfRes.result.show_invid,
		staffName: qfRes.result.show_deliver ? _.last(qfRes.result.show_deliver.split(',')) : '', //经办人
		totalnum: qfRes.result.totalnum,
		opstaffName: qfRes.result.opstaffName || LOGINDATA.name,
		optime: qfRes.result.optime,
		flag: 0,
		processtime: '', //处理时间
		prodate: qfRes.result.prodate,
		rem: '',
		id: qfRes.result.pk
	};
};

/**
 * getInvCheckDetSearchListExp - 拼接按明细查dataList中一条数据的明细值
 * interfaceid=ql-1924
 * @alias module:requestHandler
 * @param {object} sfRes 开单{params,result}
 * @param {object} qfRes 单据明细{params,result}
 * @return {object}
 */
requestHandler.getInvCheckDetSearchListExp = function (styleInfo, sfRes, qfRes) {
	const colorIdx = styleInfo.colorids.split(',').indexOf(qfRes.result.details[0].colorid);
	const sizeIdx = styleInfo.sizeids.split(',').indexOf(qfRes.result.details[0].sizeid);
	return {
		mainBillno: sfRes.result.orderno,
		inventoryName: LOGINDATA.depname,
		propdresStyleCode: styleInfo.code,
		propdresStyleName: styleInfo.name,
		propdresColorid: styleInfo.show_colorids.split(',')[colorIdx],
		propdresSizeid: styleInfo.show_sizeids.split(',')[sizeIdx],
		propdresStyleBrandid: styleInfo.show_brandid,
		propdresStyleClsName: styleInfo.show_classid,
		num: qfRes.result.details[0].num,
		flag: '否', //是否执行
		flagid: '0', //是否执行
		opstaffName: LOGINDATA.name,
		optime: qfRes.result.optime,
	};
};

/**
 * processCheck - 盘点处理(含断言)
 * @description 默认部分盘点
 * 等待服务端添加返回pk值
 * @alias module:requestHandler
 * @async
 * @param {object} [param={}] {invid,checkall}
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} 请求结果
 */
requestHandler.processCheck = async function (param = {}, check = true) {
	// let t1 = Date.now();
	const jsonparam = Object.assign({
		// "pk": "主键，修改的时候需要传",
		action: 'add',
		invid: LOGINDATA.invid,
		checkstartdate: common.getCurrentDate(),
		checkall: 0, //0为部分盘点,1为全部盘点
	}, param);
	let optime = common.getCurrentTime();
	const result = await common.callInterface('sf-1925-1', {
		jsonparam,
	});
	// console.log(`盘点处理 duration=${Date.now() - t1}   optime=${new Date()}`);
	check && expect(result, `盘点处理sf-1925-1失败:\njsonparam:${JSON.stringify(jsonparam)}\nresult:${JSON.stringify(result)}`).not.to.have.property('error');
	return {
		result,
		optime
	};
};
/**
 * checkMainCancel - 撤销盘点处理单
 * 盘点之前的流水不允许修改,为了不影响其他用例，需要撤销
 * @alias module:requestHandler
 * @async
 * @param {string}    pk           单据唯一码
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} 请求结果
 */
requestHandler.checkMainCancel = async function (pk, check = true) {
	const result = await common.callInterface('cs-192531', {
		pk
	});
	check && expect(result, `cs-192531盘点撤销失败:${JSON.stringify(result)}`).not.to.have.property('error');
	return result;
};

/**
 * checkMainCancelByDate - 撤销从date1到今天的所有盘点处理单
 * 防止影响其他模块用例
 * @alias module:requestHandler
 * @async
 * @param {type} date 日期从
 */
requestHandler.checkMainCancelByDate = async function (date) {
	// 防止影响其他模块的用例
	let pks = [];
	let param = format.qlParamsFormat({
		invalidflag: 0,
		prodate1: date || common.getCurrentDate(),
	}, false);
	let qlRes = await common.callInterface('ql-1928', param); //盘点管理-处理记录
	qlRes.dataList.map((data) => pks.push(data.id)); //撤销得按时间从近到远的顺序
	pks = common.dedupe(pks);
	for (let i = 0, length = pks.length; i < length; i++) {
		await requestHandler.checkMainCancel(pks[i]);
	};
};

/**
 * deleteCheckBill - 删除盘点单
 * @alias module:requestHandler
 * @async
 * @param {string}    pk           单据唯一码
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} 请求结果
 */
requestHandler.deleteCheckBill = async function (pk, check = true) {
	const result = await common.callInterface('cs-1923-22', {
		pk
	});
	check && expect(check).not.to.have.property('error');
	return result;
};


/**
 * addCheckPlan - 新增盘点计划
 * @alias module:requestHandler
 * @async
 * @param {object}    param      typeid 1:按类别 2:按品牌 3:按厂商 4:按组合 styleclassids/brandids/providerids/seasons
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} {params,result}
 */
requestHandler.addCheckPlan = async function (param, check = true) {
	const interfaceids = ['', 'sf-1927-StyleClassCheckPlan', 'sf-1927-BrandCheckPlan', 'sf-1927-DwxxCheckPlan', 'sf-1927-MultipleCheckPlan'];
	const params = Object.assign({
		action: 'add',
		invid: LOGINDATA.invid
	}, param);
	let optime = common.getCurrentTime();
	const result = await common.callInterface(interfaceids[param.typeid], {
		jsonparam: params,
	});
	check && expect(result, `${interfaceids[param.typeid]}新增盘点计划失败\nparams:${JSON.stringify(params)}\n${JSON.stringify(result)}`).not.to.have.property('error');
	return {
		params,
		result,
		optime
	};
};

/**
 * deleteCheckPlan - 删除盘点计划单
 * 盘点之前的流水不允许修改,为了不影响其他用例，需要撤销
 * @alias module:requestHandler
 * @async
 * @param {string}    pk           单据唯一码
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} 请求结果
 */
requestHandler.deleteCheckPlan = async function (pk, check = true) {
	if (!pk) throw new Error(`删除盘点计划未传入pk:${pk}`);
	const result = await common.callInterface('cs-1927', {
		pk
	});
	check && expect(check).not.to.have.property('error');
	return result;
};

/**
 * deleteCheckPlanOfUndisposed - 删除所有未处理的盘点计划单
 * 防止影响其他用例
 * @alias module:requestHandler
 * @async
 */
requestHandler.deleteCheckPlanOfUndisposed = async function () {
	let param = format.qlParamsFormat({
		flag: 0
	}, false);
	let qlRes = await common.callInterface('qd-1927', param); //盘点管理-盘点计划表
	let pks = qlRes.dataList.map(data => data.id);
	for (let i = 0, length = pks.length; i < length; i++) {
		await requestHandler.deleteCheckPlan(pks[i]);
	};
};
