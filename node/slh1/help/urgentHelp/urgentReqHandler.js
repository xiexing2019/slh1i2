"use strict";
const common = require('../../../lib/common.js');
// const format = require('../../data/format');
// const basiceJson = require('../basiceJsonparam.js');
const reqHandler = require('../reqHandlerHelp');

/*
 * 封装紧急模式接口
 */
let requestHandler = module.exports = {};


/**
 * 紧急模式上传
 *
 * @param  {object} params
 * @param  {object} params.jsonparam
 * @param  {object} params.jsonparam.urgentBillList 销售单保存参数
 * @param  {string} params.jsonparam.apiName 接口id，默认用sf-1421-1，不是必填
 * @return {object} {params,result}
 */
requestHandler.saveUrgentBill = async function (params) {
	return reqHandler.sfIFCHandler({
		interfaceid: 'cs-saveUrgentBillBatch',
		...params
		// jsonparam: {
		// 	urgentBillList: urgentBillList,
		// 	 apiName: salesJson.interfaceid,//接口id，默认用sf-1421-1
		// }
	});
};


/**
 * 获取紧急模式下单据列表
 * @description
 * dwid:客户，shopId:门店，deliver:经办人，invalidflag:是否作废，flag:是否有效，flagIn:状态类型;
 * @param  {object} qlParams
 * @param  {string} qlParams.dwid 客户
 * @return {object}
 */
requestHandler.getUrgentBillList = async function (qlParams) {
	qlParams = Object.assign({
		prodate1: common.getCurrentDate(),
		prodate2: common.getCurrentDate(),
		flagIn: 1, //状态类型，-1表示保存单据，0表示客户端删除，1表示保存失败，2表示保存中
	}, qlParams);
	console.log(`qlParams : ${JSON.stringify(qlParams)}`);
	return common.callInterface('ql-14210', qlParams);
};


/**
 * 获取紧急模式单据详情（可获取到保存成功的单据id）
 *
 * @param  {object} params
 * @param  {string} params.pk
 * @return {object}
 */
requestHandler.getUrgentBillPreview = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-urgentBillPreview',
		pk: params.pk,
	});
};


/**
 * 上传失败的单据再次保存
 *
 * @param  {object} params
 * @param  {object} params.jsonparam
 * @return {object}
 */
requestHandler.saveUrgentFailedBill = async function (params) {
	return reqHandler.sfIFCHandler({
		interfaceid: 'cs-urgentBillSave',
		...params
	});
};


/**
 * 删除紧急模式异常单据（列表还是可以查询到）
 *
 * @param  {object} params
 * @param  {string} params.pk
 * @return {object}
 */
requestHandler.cancelUrgentBill = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-cancelUrgentBill',
		pk: params.pk,
	})
}
