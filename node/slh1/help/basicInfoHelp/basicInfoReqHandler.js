"use strict";
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const basiceJson = require('../basiceJsonparam.js');
const reqHandler = require('../reqHandlerHelp');
const mainReqHandler = require('../basicInfoHelp/mainDataReqHandler');
let requestHandler = module.exports = {
	sfInterface: '',
	qfInterface: '',
	qlInterface: '',
};

/**
 * editStyle - 新增/修改货品
 * @description
 * 一代 result:{val:pk}
 * 二代款号pk有区别 result:{val:款号在当前用户门店的id,tenantSpuId:租户级的款号ID}
 * 获取货品信息qf-1511需要用款号在当前用户门店的id,其他(如开单)使用租户级的款号ID
 * @param {object}  params
 * @param {object}  params.jsonparam    jsonparam
 * @param {boolean} [params.check=true] 断言
 * @return {object}
 */
requestHandler.editStyle = async function ({
	jsonparam,
	check = true,
} = {}) {
	if (jsonparam && jsonparam.pk) jsonparam.action = 'edit';
	let result = await common.callInterface('sf-1511', {
		'jsonparam': jsonparam,
	});

	if (check) {
		expect(result, `新增/修改货品sf-1511失败:\njsonparam:${JSON.stringify(jsonparam)}\nresult:${JSON.stringify(result)}`).not.to.have.property('error');
		if (USEECINTERFACE == 2) await common.delay(100);
	};

	return {
		'params': jsonparam,
		'result': result,
		'pk': result.tenantSpuId || result.val || '', //开单等操作时需要用到的styleid(一二代兼容)
		'optime': common.getCurrentTime(),
	};
};


/**
 * invNumChange - 库存调整
 * @param {object}  params
 * @param {object}  params.jsonparam jsonparam
 * @param {object}  params.jsonparam jsonparam
 * @param {boolean} [params.check=true]   断言
 * @return {object} Description
 */
requestHandler.invNumChange = async function ({
	jsonparam,
	check = true,
}) {
	let qlParam = format.qlParamsFormat(Object.assign({
		invid: LOGINDATA.invid,
	}, jsonparam));
	let invInfo = await common.callInterface('ql-1932', qlParam).then((res) => res.dataList[0]); //货品管理-当前库存
	// console.log(`invInfo = ${JSON.stringify(invInfo)}`);

	jsonparam = {
		pk: invInfo.id, //必填
		// action: 'edit',
		propdresStyleCode: invInfo.styleid,
		propdresColorid: invInfo.colorid, //sid
		propdresSizeid: invInfo.sizeid, //sid
		invnum: invInfo.invnum, //必填
		recvnum: jsonparam.recvnum, //必填
	};
	let result = await common.callInterface('sf-1932', {
		jsonparam,
	});
	check && expect(result, `库存调整sf-1932失败:${JSON.stringify(result)}`).not.to.have.property('error');
	return {
		params: jsonparam,
		result,
	};
};

/**
 * 新增字典类
 * @param {object} params jsonparam
 * @return {object} {params,result}
 */
requestHandler.saveDict = async function (params) {
	params.action = params.id ? 'edit' : 'add';
	params = format.packJsonParam(params);
	params.jsonparam = params.jsonParam;
	delete params.jsonParam;
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-save-dict-full',
		...params
	});
};

/**
 * 获取字典详情
 * @param {object} params
 * @param {object} params.id
 * @param {object} params.typeid
 * @return {object} {params,result}
 */
requestHandler.getDictInfo = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-get-dict-full',
		pk: params.id,
		typeid: params.typeid,
	});
};

/**cs-syncdata  数据同步
 * 获取字典列表
 * @param {object} params
 * @param {object} params.typeid
 * @return {object} {params,result}
 */
requestHandler.getDictList = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-dict-list',
		pagesize: 0,
		wrapper: true,
		...params
	});
};

/**
 * 停用字典
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.disableDict = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-disable-dict',
		pk: params.pk
	});
};

/**
 * 启用字典
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.enableDict = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-enable-dict',
		pk: params.pk
	});
};


/**
 * 新增类别
 *
 */
requestHandler.saveStyleClass = async function (params) {
	let jsonparam = params;
	jsonparam.action = params.pk ? 'edit' : 'add';
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-1509',
		jsonparam: jsonparam,
	});
};
/**
 * 获取类别详情
 * @param {object} params
 * @param {object} params.id
 * @param {object} params.typeid
 * @return {object} {params,result}
 */
requestHandler.getClassInfo = async function (params) {
	return common.callInterface('qf-1509', {
		pk: params.pk,
	});
};

/**
 * 获取类别列表
 * @param {object} params
 * @param {object} params.id
 * @return {object} {params,result}
 */
requestHandler.getClassList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({}, params, { interfaceid: 'ql-1509' }));
};

/**
 * 停用类别
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.disableClass = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-15091',
		pk: params.pk
	});
};

/**
 * 启用类别
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.enableClass = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-15092',
		pk: params.pk
	})
}


/**
 * 新增门店
 * @param {object} params jsonparam
 * @return {object} {params,result}
 */
requestHandler.saveDepartment = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-1501',
		jsonparam: params,
	});
};

/**
 * 获取门店详情
 * @param {object} params
 * @param {object} params.id
 * @return {object} {params,result}
 */
requestHandler.getDepartmentInfo = async function (params) {
	return common.callInterface('qf-1501', {
		pk: params.pk
	});
};

/**
 * 获取门店列表
 * @param {object} params
 * @param {object} params.name
 * @param {object} params.depid
 * @return {object} {params,result}
 */
requestHandler.getDepList = async function (params = {}) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-1501',
		name: '', //名称
		depid: '', //部门
	}, params));
};

/**
 * 停用门店
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.disableDep = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-1501-2',
		pk: params.pk
	});
};

/**
 * 启用门店
 * @param {object} params
 * @param {object} params.pk
 * @return {object} {params,result}
 */
requestHandler.enableDep = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-1501-3',
		pk: params.pk
	})
};


/**
 * 维护账套数据 新增or修改
 * 任何一个账套先执行这个方法之后，就可以执行自动化脚本了
 * 需要新增品牌，类别，门店价格组，门店，店员，执行标准，安全类别，洗涤说明，
 * 新增货品，厂商，客户，物流商，特殊货品-66666积分抵现
 * @param {object} params
 * @param {object} params
 * @return {object} act {params,result}
 */
requestHandler.maintainData = async function (mainData) {

	for (let key of Object.keys(mainData)) {
		let value = mainData[key];
		switch (key) {
			case 'priceGroup':
				for (let i = 0; i < value.length; i++) {
					let priceGroupList = await requestHandler.getDictList({
						typeid: value[i].typeid,
						check: false
					});
					let act = priceGroupList.result.dataList.find(obj => obj.name == value[i].name)
					if (act != undefined) {
						value[i].id = act.id;
					}
					await requestHandler.saveDict(value[i]);
				};
				break;
			case 'execstd':
			case 'grade':
			case 'safetycat':
			case 'washingrem':
			case 'brand':
				for (let i = 0; i < value.length; i++) {
					let brandList = await requestHandler.getDictList({
						typeid: value[i].typeid,
						check: false
					});
					let act = brandList.result.dataList.find(obj => obj.name == value[i].name);

					if (act === undefined) {
						let sfBrand = await requestHandler.saveDict(value[i]);
						//console.log(`sfBrand : ${JSON.stringify(sfBrand)}`);
						let qfBrand = await requestHandler.getDictInfo({
							id: sfBrand.result.pk,
							typeid: value[i].typeid
						});
						BASICDATA.brand[value[i].name] = qfBrand.result.sid;
					} else {
						value[i].id = act.id;
						await requestHandler.saveDict(value[i])
						BASICDATA.brand[value[i].name] = act.sid;
					};
				};
				break;
			case 'styleClass':
				await common.setGlobalParam('sales_sizehead_ui', 0);
				for (let i = 0; i < value.length; i++) {
					//模糊搜索
					let classList = await requestHandler.getClassList({
						check: false
					});
					let act = classList.result.dataList.find(obj => obj.name == value[i].name);
					if (act == undefined) {
						let sfClass = await requestHandler.saveStyleClass(value[i]);
						BASICDATA.styleClass[value[i].name] = sfClass.result.pk;
					} else {
						BASICDATA.styleClass[value[i].name] = act.id;
					}
				};
				break;
			case 'depGroup':
				let priceGroupList = await requestHandler.getDictList({
					typeid: 764,
					check: false
				});
				await common.setGlobalParam('scm_shop_inv_separate', 1); //绑定仓库要开启异地仓库参数

				for (let i = 0; i < value.length; i++) {
					let depList = await requestHandler.getDepList({
						check: false
					});
					let act = depList.result.dataList.find(obj => obj.name == value[i].name);
					if (act != undefined) {
						value[i].pk = act.id;
					};
					switch (value[i].name) {
						case '常青店':
							value[i].pricegroup = priceGroupList.result.dataList.find(obj => obj.name == 'one').sid;
							break;
						case '文一店':
						case '仓库店':
							value[i].pricegroup = priceGroupList.result.dataList.find(obj => obj.name == 'two').sid;
							if (value[i].name == '文一店')
								value[i].bindinvid = depList.result.dataList.find(obj => obj.name == '仓库店').id;
							break;
						default:
							break;
					}
					let sfDep = await requestHandler.saveDepartment(value[i]);
					BASICDATA.dep[value[i].name] = sfDep.result.pk;

				};
				break;
			case 'customer':
				for (let i = 0; i < value.length; i++) {
					value[i].invid = BASICDATA.dep['常青店']; //常青店
					//手机号查询，如果有获取详情，name查询，如果有，获取详情，
					// let custList = await mainReqHandler.getCustList({
					// 	//check: false,
					// 	mobile: value[i].phone || '',
					// });
					let custList = await mainReqHandler.getCustList({
						dwname: value[i].nameshort,
						check: false
					});
					//console.log(`variable : ${JSON.stringify(custList)}`);
					let act = custList.result.dataList.find(obj => obj.dwname == value[i].nameshort);
					if (act != undefined) {
						let param = await mainReqHandler.getCustInfo({
							pk: act.id
						});
						//console.log(`param : ${JSON.stringify(param)}`);
						value[i] = Object.assign(param, value[i]);
					};
					let sf = await mainReqHandler.saveCust(value[i]);
					BASICDATA.customer[value[i].nameshort] = sf.result.val;
				};

				break;
			case 'supplier':
				for (let i = 0; i < value.length; i++) {
					value[i].invid = BASICDATA.dep['常青店']; //常青店
					let supplierList = await mainReqHandler.getSupplierList({
						check: false
					});
					let act = supplierList.result.dataList.find(obj => obj.name == value[i].name);
					if (act != undefined) {
						value[i].pk = act.id;
					};
					let sf = await mainReqHandler.saveSupplier(value[i]);
					BASICDATA.supplier[value[i].name] = sf.result.val;
				}
				break;
			case 'logistics':
				for (let i = 0; i < value.length; i++) {
					value[i].invid = BASICDATA.dep['常青店']; //常青店
					let logisticsList = await mainReqHandler.getLogisticsList({
						check: false
					});
					let act = logisticsList.result.dataList.find(obj => obj.nameshort == value[i].nameshort);
					if (act != undefined) {
						value[i].pk = act.id;
					};
					let sf = await mainReqHandler.saveLogistics(value[i]);
					BASICDATA.logistics[value[i].nameshort] = sf.result.val;
				}
				break;
			case 'style':
				for (let i = 0; i < value.length; i++) {
					value[i].shopid = BASICDATA.dep['常青店'];
					value[i].brandid = BASICDATA.brand['Adidas'];
					value[i].classid = value[i].code == 'agc002' ? BASICDATA.styleClass['鞋'] : BASICDATA.styleClass['登山服'];
					value[i].season = BASICDATA.season['春季'];
					value[i].dwid = value[i].code == 'agc002' ? BASICDATA.supplier['Rt'] : BASICDATA.supplier['Vell'];
					let styleList = await mainReqHandler.getStyleList({
						stylename: value[i].code,
						check: false
					});
					//console.log(`styleList : ${JSON.stringify(styleList)}`);
					let act = styleList.result.dataList.find(obj => obj.code == value[i].code);
					if (act != undefined) {
						let param = await common.fetchMatInfo(act.id);
						//console.log(`param : ${JSON.stringify(param)}`);
						value[i].colorids = common.dedupe(param.colorids.split(',').concat(value[i].colorids.split(','))).toString();
						value[i].sizeids = common.dedupe(param.sizeids.split(',').concat(value[i].sizeids.split(','))).toString();
						value[i] = Object.assign(param, value[i]);
					};
					let sf = await requestHandler.editStyle({
						jsonparam: value[i]
					});
					BASICDATA.logistics[value.code] = sf.result.val;
				}
				break;
			case 'specialStyle':
				for (let i = 0; i < value.length; i++) {
					let specialList = await mainReqHandler.getSpecialStyleList({
						check: false
					});
					let act = specialList.result.dataList.find(obj => obj.code == value[i].code);
					if (act != undefined) {
						value[i].pk = act.id;
					};
					await mainReqHandler.saveSpecialStyle(value[i]);
				}
				break;
			default:
				break;
		};
	};
	//新增员工
	let roleList = await mainReqHandler.getRoleList(LOGINDATA.epid);
	let depidList = await common.callInterface('cs-curdetname', {
		epid: LOGINDATA.epid
	});

	let account = ['现金账户', '银行账户', '代收账户', '微信账户', '支付宝账户'];
	for (let key in BASICDATA.dep) {
		//修改账户

		let accountParams = {
			invid: BASICDATA.dep[key],
			name: key + '-代收账户',
			nameshort: '代',
			typeid: 2, //账户类型 代收
			banktypeid: 0, //通用，刷卡，汇款
			busitypeid: 0, //通用，销售，采购
		};
		//if(key == '常青店') accountParams.name = '东灵测试-代收账户';
		let accountList = await mainReqHandler.getAccountList({
			invid: BASICDATA.dep[key],
			check: false,
		});
		let act = accountList.result.dataList.find(obj => obj.name == accountParams.name);
		if (act == undefined) {
			let rs = await mainReqHandler.saveAccount(accountParams);
			//console.log(`rs : ${JSON.stringify(rs)}`);
		};
		accountList = await mainReqHandler.getAccountList({
			invid: BASICDATA.dep[key],
			check: false,
		});
		for (let k = 0; k < accountList.result.dataList.length; k++) {
			accountParams.pk = accountList.result.dataList[k].id;
			accountParams.name = accountList.result.dataList[k].name;
			accountParams.nameshort = accountList.result.dataList[k].nameshort;
			for (let key of Object.keys(account)) {
				if (accountList.result.dataList[k].name.includes(account[key])) {
					accountParams.typeid = key;
					break;
				};
			}
			//console.log(`accountParams : ${JSON.stringify(accountParams)}`);
			await mainReqHandler.saveAccount(accountParams);
		}
		//新增员工
		let params = {};
		for (let i = 0; i < roleList.result.dataList.length; i++) {
			delete params.pk;
			params.name = roleList.result.dataList[i].name;
			if (key != '常青店') params.name = key + roleList.result.dataList[i].name;
			params.roleids = roleList.result.dataList[i].id;
			params.depid = depidList.dataList.find(obj => obj.name == key).id; //新增员工时的depid不是新增门店时返回的pk
			switch (key) {
				case '常青店':
					params.code = '0' + _.padStart(i, 2, '0');
					break;
				case '中洲店':
					params.code = '2' + _.padStart(i, 2, '0');
					break;
				case '文一店':
					params.code = '4' + _.padStart(i, 2, '0');
					break;
				case '仓库店':
					params.code = '1' + _.padStart(i, 2, '0');
					break;
				default:
					break;
			};
			let staffList = await mainReqHandler.getStaffList();
			let act = staffList.result.dataList.find(obj => obj.code == params.code);
			if (act != undefined) {
				params.pk = act.id;
			}
			await mainReqHandler.saveStaff(params);
		}
	}
};



/**
 * 维护门店数据 新增or修改
 * 绑定仓库？设置价格组
 * @param {object} params
 * @param {object} params
 * @return {object} {params,result}
 */
requestHandler.maintainDepData = async function (params) {
	let DepList = await this.getDepList();
	//如果有就直接返回，没有就新增
	let act = DepList.result.dataList.find(obj => obj.name = params.name);
	if (act == undefined) {
		return this.saveDepartment(params);
	} else {
		return act;
	};
};
