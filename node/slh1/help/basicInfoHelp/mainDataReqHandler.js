"use strict";
const common = require('../../../lib/common');
const format = require('../../../data/format');
const reqHandler = require('../reqHandlerHelp');



let mainReqHandler = module.exports = {};


/**
 *  货品详情---httpRequest.fetchMatInfo
 *
 * /


/**
 * 货品列表
 */
mainReqHandler.getStyleList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-15110',
	}, params));
};


/**
 * 特殊货品新增
 */
mainReqHandler.saveSpecialStyle = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-1518-1',
		jsonparam: params,
	})
}

/**
 * 特殊货品详情
 */
mainReqHandler.getSpecialStyleInfo = async function (params) {
	return common.callInterface('qf-1518-1', {
		pk: params.pk
	});
};


/**
 * 特殊货品列表
 *
 */
mainReqHandler.getSpecialStyleList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-1518',
	}, params));
};





//------客户、厂商、物流商

/**
 * 新增/修改客户
 *
 */
mainReqHandler.saveCust = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	params = format.packJsonParam(params);
	params.jsonparam = params.jsonParam;
	delete params.jsonParam;
	return reqHandler.sfIFCHandler({ interfaceid: 'sf-1401', ...params });
};


/**
 * 客户详情
 *
 */
mainReqHandler.getCustInfo = async function (params) {
	return common.callInterface('qf-1401', {
		pk: params.pk
	});
};


/**
 * 客户列表
 *
 */
mainReqHandler.getCustList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-1339',
	}, params));
};


/**
 * 新增厂商
 */
mainReqHandler.saveSupplier = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	params = format.packJsonParam(params);
	params.jsonparam = params.jsonParam;
	delete params.jsonParam;
	return reqHandler.sfIFCHandler({ interfaceid: 'sf-2241', ...params });
};


/**
 * 厂商详情
 *
 */
mainReqHandler.getSupplierInfo = async function (params) {
	return common.callInterface('qf-2241', {
		pk: params.pk
	});
};


/**
 * 厂商列表
 *
 */
mainReqHandler.getSupplierList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'qd-2241',
	}, params));
};


/**
 *新增物流商
 */
mainReqHandler.saveLogistics = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-2310',
		jsonparam: params,
	});
};


/**
 * 物流商详情
 *
 */
mainReqHandler.getLogisticsInfo = async function (params) {
	return common.callInterface('qf-2310', {
		pk: params.pk
	});
};


/**
 * 物流商列表
 *
 */
mainReqHandler.getLogisticsList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'qd-2310',
	}, params));
};


/**
 * 获取岗位
 */
mainReqHandler.getRoleList = async function (params) {
	return reqHandler.csIFCHandler({
		interfaceid: 'cs-getPostBaseData',
		epid: params.epid
	});
}

/**
 * 新增员工
 */
mainReqHandler.saveStaff = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	params = format.packJsonParam(params);
	params.jsonparam = params.jsonParam;
	delete params.jsonParam;
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-1110-2',
		...params,
	});
};
/**
 * 员工详情
 * @param {Object} params
 * @param {Object} params.pk
 */
mainReqHandler.getStaffInfo = async function (params) {
	return common.callInterface('qf-1110-2', params);
};


/**
 * 员工列表
 *
 */
mainReqHandler.getStaffList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-1110',
	}, params));
};

/**
 * 停用员工
 * @param {object} params
 * @param {object} params.pk
 */
mainReqHandler.disableStaff = async function (params) {
	return reqHandler.csIFCHandler(Object.assign({}, params, { interfaceid: 'cs-1110-2' }));
};

/**
 * 启用员工
 * @param {object} params
 * @param {object} params.pk
 */
mainReqHandler.enableStaff = async function (params) {
	return reqHandler.csIFCHandler(Object.assign({}, params, { interfaceid: 'cs-1110-3' }));
};



/**
 * 新增账户
 * 客户端调用，没有提供过 账户保存的别的接口
 */
mainReqHandler.saveAccount = async function (params) {
	params.action = params.pk ? 'edit' : 'add';
	return reqHandler.sfIFCHandler({
		interfaceid: 'sf-1531-4',
		jsonparam: params,
	});
};
/**
 * 账户详情
 *
 */
mainReqHandler.getAccountInfo = async function (params) {
	return common.callInterface('qf-1531-4', {
		pk: params.pk
	});
};


/**
 * 账户列表
 *
 */
mainReqHandler.getAccountList = async function (params) {
	return reqHandler.qlIFCHandler(Object.assign({
		interfaceid: 'ql-1531',
	}, params));
};
