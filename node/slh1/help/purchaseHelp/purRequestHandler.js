'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
// const basiceJson = require('../basiceJsonparam.js');
const reqHandler = require('../reqHandlerHelp');

/*
 * 封装采购入库/采购订货基础的请求类，如：sf、qf
 */
let requestHandler = module.exports = {
	//发货状态
	deliveryFlagArr: ['未入库', '部分入库', '全部入库', '结束'],
};

/**
 * 查询采购入库单据详情 qf-14212-1
 * 对应sf-14212-1
 * @param pk - 单据唯一码
 * @param {number} [isPend=0] 是否挂单 默认否 (二代必填参数)
 * @return 返回传入的参数，以及保存后服务端的返回结果
 */
requestHandler.purQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	params.interfaceid = 'qf-14212-1';
	return reqHandler.queryBilling(params);
};


/**
 * cancelPurchaseBill - 作废入库单
 * @param {string|object}    params    pk|{pk,isPend}
 * @return {object} {params,result:{val:'ok'}}
 */
requestHandler.cancelPurchaseBill = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	return reqHandler.csIFCHandler(Object.assign({
		interfaceid: 'cs-cancel-pured-bill',
		check: true, //断言作废成功
		errorMsg: '作废入库单失败',
	}, params));
};

/**
 * editSalesHangBill - 新增修改采购挂单
 * @param {object} jsonparam
 * @param {string} jsonparam.pk  修改时需要传pk值
 * @return {object} {params,result}
 */
requestHandler.editPurHangBill = async function (jsonparam, check = true) {
	jsonparam.interfaceid = 'sf-14212-1';
	return reqHandler.editHangBillHandler(jsonparam, check);
};

/*
 * 挂单转采购入库单
 * invalidflag=9为挂单
 */
requestHandler.hangToPurBill = async function (jsonparam, check = true) {
	jsonparam.interfaceid = 'sf-14212-1';
	jsonparam.prodate = common.getCurrentDate(); //挂单转正式单，日期改为当天
	return reqHandler.hangToBillHandler(jsonparam, check);
};

/*
 * 获取核销信息
 * 同销售单(二代bizType有区别)
 */
requestHandler.getVerifyingBills = async function (dwid, invid = LOGINDATA.invid) {
	let param = format.qlParamsFormat({
		dwid,
		invid,
	}, true);
	if (USEECINTERFACE == '2') param.bizType = 1300; //采购单
	return common.callInterface('ql-15010', param);
};

requestHandler.getPurBillDetails = async function (pk, isPend = 0) {
	let param = {
		pk: pk
	};
	if (USEECINTERFACE == '2') param.isPend = isPend; //默认非挂单单据，设置为0
	return common.callInterface('qf-14212-3', param);
};

/*
 * 按订货入库
 * orderRes 采购订货单返回结果 qf-14212-1
 * 默认全部入库
 */
requestHandler.invinByOrder = async function (orderRes, check = true) {
	let json = {
		interfaceid: 'sf-14212-1',
		billid: orderRes.pk,
		deliver: orderRes.respopid,
		remark: '按订货入库'
	};
	orderRes.srcType = 2; // 按订货开单
	delete orderRes.pk;
	delete orderRes.respopid;
	delete orderRes.shouldpay;
	delete orderRes.actualpay; //开单后会更新
	let jsonparam = common.mixObject(_.cloneDeep(orderRes), json);
	orderRes.details.forEach((obj, index) => {
		jsonparam.details[index].billid = obj.pk || '';
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});
	let sfRes = await common.editBilling(jsonparam, check);
	return sfRes;
};

/**
 * 拼接 采购入库-按批次查查询界面结果期望值
 * interfaceid = ql-22301
 * @param {object} qfResult - qf-14212-1 采购入库单据详情
 * @return {object} 期望值
 */
requestHandler.getPurMainSearchListExp = function (qfResult) {
	let exp = {
		optime: qfResult.optime,
		remark: qfResult.remark,
		remit: qfResult.remit || 0,
		shopname: qfResult.show_shopid,
		verifysum: qfResult.finpayVerifysum,
		id: qfResult.pk,
		finpayAlipay: qfResult.finpayAlipay,
		balance: qfResult.balance,
		finpayWeixinpay: qfResult.finpayWeixinpay || 0,
		modelClass: qfResult.modelClass,
		purmoney: qfResult.totalmoney,
		card: qfResult.card || 0,
		totalmoney: qfResult.totalmoney,
		dwname: qfResult.show_dwid,
		agency: qfResult.agency || 0,
		totalnum: qfResult.totalnum,
		seller: _.last(qfResult.show_deliver.split(',')), //e.g. 000,总经理
		prodate: qfResult.prodate,
		opname: USEECINTERFACE == 1 ? qfResult.opname : _.last(qfResult.opname.split(',')),
		invalidflag: qfResult.invalidflag,
		weixinpay: qfResult.finpayWeixinpay || 0,
		alipay: qfResult.finpayAlipay || 0,
		// totalRoll: '',//总匹数
		cash: qfResult.cash || 0,
		puredmaincode: '0', //订货号 没有默认为0
		paysum: qfResult.actualpay, //付款
		billno: qfResult.billno,
		outsum: 0, //进货额
		backsum: 0, //退货额
	};
	qfResult.details.map((obj) => {
		obj.num < 0 ? exp.backsum -= Number(obj.total) : exp.outsum += Number(obj.total);
	});
	return exp;
};

// 按分销入库-查询 ql-142202
requestHandler.getPurByDistributionListExp = function (salesRes, purRes) {
	let exp = {
		id: purRes.partnerBillId,
		invname: purRes.show_invid,
		'dwxx.name': purRes.show_dwid, //客户
		'seller.name': salesRes.show_deliver, //店员
		rem: purRes.rem || '',
		prodate: purRes.prodate,
		orderno: salesRes.billno,
		totalnum: salesRes.totalnum,
		totalmoney: salesRes.totalmoney,
		diffnum: purRes.totalnum, //差异数
		partnerNum: common.sub(salesRes.totalnum, purRes.totalnum), //发货数
	};
	exp.flag = exp.partnerNum == 0 ? '未入库' : '部分入库'; //发货状态 全部入库的不显示
	return exp;
};

requestHandler.getPurByDistributionBillExp = function (jsonParam) {
	let mapfield = 'dwid;show_dwid;prodate;srcType;partnerBillId;invid;show_invid;shopid;respopid;show_respopid;rem';
	let exp = format.dataFormat(jsonParam, mapfield);
	exp.details = [], exp.totalmoney = 0, exp.totalnum = 0;
	mapfield = 'stylename;sizeid;stylecode;colorid;fileid;billid;show_sizeid;show_styleid;show_colorid;price;styleid;rem;recvnum';
	jsonParam.details.forEach((detail) => {
		let num = common.sub(detail.recvnum, detail.num);
		if (num > 0) {
			let detailExp = format.dataFormat(detail, mapfield);
			detailExp.num = num;
			exp.totalnum = common.add(exp.totalnum, num);
			detailExp.total = common.mul(num, detail.price);
			exp.totalmoney = common.add(exp.totalmoney, detailExp.total);
			exp.details.push(detailExp);
		};
	});

	return exp;
};

/**
 * 拼接 按订货入库-查询界面结果期望值
 * interfaceid = ql-22306
 * @param {object} orderResult - qf-22101-1 采购订货单据详情
 * @return {object} 期望值
 */
requestHandler.getInvinByOrderListExp = function (orderResult) {
	let deliveryflagArr = ['未入库', '部分入库', '全部入库'];
	let exp = {
		optime: orderResult.optime,
		invname: orderResult.show_invid,
		deliveryflag: deliveryflagArr[orderResult.flag],
		provideridname: orderResult.show_dwid,
		totalnum: orderResult.totalnum,
		prodate: orderResult.prodate,
		opname: LOGINDATA.name,
		id: orderResult.pk,
		flag: orderResult.flag,
		respstaffname: orderResult.show_respopid.split(',')[1], //e.g. 000,总经理
		invalidflag: '0',
		modelClass: 'PuredMainBean',
		rem: orderResult.rem,
		purcost: 0,
		billno: orderResult.billno,
	};
	orderResult.details.map((obj) => {
		exp.purcost = common.add(exp.purcost, obj.recvnum); //入库数
	});
	exp.diffnum = common.sub(exp.totalnum, exp.purcost); //差异数
	return exp;
};


//
//---------------采购订货---------------------
//

/**
 * 查询采购订货单据详情 qf-22101-1
 * 对应sf-22101-1
 * @param {string|object} params - pk|{pk,isPend,shopid}
 * @return {object} {params,result}
 */
requestHandler.purOrderQueryBilling = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	params.interfaceid = 'qf-22101-1';
	// if(!params.shopid) params.shopid = LOGINDATA.invid;
	let res = await reqHandler.queryBilling(params);
	return res;
};

/**
 * cancelPurOrderBill - 作废订货单
 * @param {string|object} params pk|{pk,check}
 * @return {object} {params,result}
 */
requestHandler.cancelPurOrderBill = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	let res = await reqHandler.csIFCHandler(Object.assign({
		interfaceid: 'cs-22102',
		check: true, //断言作废成功
		errorMsg: '作废订货单失败',
	}, params));
	return res;
};


/**
 * endPurOrderBill - 终结采购订单
 * @param {object|string} params
 * @param {string} params.pk
 * @param {string} params.check 是否需要断言
 * @param {string} params.errorMsg 失败提示
 * @return {object} {params,result}
 */
requestHandler.endPurOrderBill = async function (params) {
	if (typeof (params) == 'string') {
		params = {
			pk: params,
		};
	};
	let res = await reqHandler.csIFCHandler(Object.assign({
		interfaceid: 'cs-teminalpured',
		check: true, //断言作废成功
		errorMsg: '终结订单失败',
	}, params));
	return res;
};


/**
 * 拼接 采购订货-按批次查结果期望值
 * interfaceid = ql-22102
 * @param {object} qfResult - qf-22101-1 采购订货单据详情
 * @return {object} 期望值
 */
requestHandler.getPurOrderMainSearchListExp = function (qfResult) {
	let recvnum = 0,
		endNotPurinNum = 0; //终结未结数
	qfResult.details.map((obj) => {
		recvnum += Number(obj.recvnum);
	});
	//终结未结数
	if (qfResult.flag == 3 && qfResult.totalnum > recvnum) {
		endNotPurinNum = common.sub(qfResult.totalnum, recvnum);
	};

	return {
		id: qfResult.pk,
		optime: qfResult.createoptime,
		remit: qfResult.remit,
		deliveryflag: this.deliveryFlagArr[qfResult.flag],
		endNotPurinNum: endNotPurinNum,
		provideridname: qfResult.show_dwid,
		invdate: '',
		totalsum: qfResult.totalsum,
		modelClass: 'PuredMainBean',
		card: qfResult.card,
		purcost: recvnum,
		diffnum: Number(qfResult.totalnum) - recvnum,
		isonline: '0',
		invname: qfResult.show_invid,
		totalnum: qfResult.totalnum,
		prodate: qfResult.prodate,
		opname: LOGINDATA.name,
		flag: qfResult.flag,
		weixinpay: qfResult.finpayWeixinpay || 0,
		alipay: qfResult.finpayAlipay || 0,
		invalidflag: '0',
		cash: qfResult.cash,
		respstaffName: qfResult.show_respopid.split(',')[1],
		rem: qfResult.rem,
		pricedec: qfResult.actualpay,
		billno: qfResult.billno,
	};
};

/**
 * 拼接 采购订货-按明细查结果期望值
 * interfaceid = ql-22103
 * @param {object} qfResult - qf-22101-1 采购订货单据详情
 * @return {array} 期望值
 */
requestHandler.getPurOrderDetailSearchListExp = function (qfResult) {
	let exp = [];
	for (let index = 0; index < qfResult.details.length; index++) {
		const element = qfResult.details[index];
		const [matCode, matName] = element.show_styleid.split(',');
		let json = {
			'total': element.total,
			'rem': element.rem,
			'mainProdate': qfResult.prodate,
			'invname': qfResult.show_invid,
			'mat_name': matName,
			'mat_code': matCode,
			'provideridname': qfResult.show_dwid,
			'propdresColorid': element.show_colorid,
			'propdresSizeid': element.show_sizeid,
			'price': element.price,
			'num': element.num,
			'recvnum': element.recvnum, //已入库
			// 'mainid': element.pk,
			'endNotPurinNum': 0,
			// 'billno': purOrderResult.result.billno,
		};
		json.leftnum = common.sub(json.num, json.recvnum); //未入库
		json.deliveryflag = this.deliveryFlagArr[qfResult.flag];
		if (qfResult.flag == 3 && json.leftnum > 0) {
			json.endNotPurinNum = json.leftnum; //终结未入库
		};
		exp.push(json);
	};
	return exp;
};
