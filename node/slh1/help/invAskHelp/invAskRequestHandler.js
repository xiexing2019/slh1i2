const common = require('../../../lib/common.js');
const reqHandler = require('../reqHandlerHelp');
const format = require('../../../data/format');

let invAskReqHandler = module.exports = {};
let qlParams = {
    prodate1: common.getCurrentDate(),
    prodate2: common.getCurrentDate(),
    // invid: BASICDATA.shopidCqd,//申请门店
    // targetinvid: BASICDATA.shopidZzd,//目标仓库
}
/**
 * 新增修改要货单
 * @param {object} params 
 */
invAskReqHandler.saveInvAskBill = async function (params) {
    params.action = params.pk ? 'edit' : 'add';
    let sfBill = await common.editBilling(params, params.check);
    return {
        params: sfBill.params,
        result: sfBill.result,
        optime: common.getCurrentTime(),
    };
};

/**
 * 批量新增要货单
 * @param {object} params 
 */
invAskReqHandler.saveBulkInvAskBill = async function (params) {
    let res = await common.editBilling(params, params.check);
    return {
        params: res.params,
        result: res.result,
        optime: common.getCurrentTime(),
    };
};

/**
 * 获取要货单详情
 * @param {object} params 
 * @param {object} params.pk 批次id
 */
invAskReqHandler.getInvAskBillInfo = async function (params) {
    params.interfaceid = 'qf-2317-1';
    return reqHandler.queryBilling(params);

};

/**
 * 要货单按批次查询
 * @description invalidflag:终结状态 0 否，1 是 终结 deliver:营业员
 * @param {object} params 
 */
invAskReqHandler.getInvAskBillList = async function (params) {
    params = Object.assign({ interfaceid: 'ql-2321' }, qlParams, params);
    return reqHandler.qlIFCHandler(params);
};

/**
 * 要货单按明细查询
 * @description propdresStyleid:款号id deliver:营业员 flag:状态 rem:备注 id:唯一id
 * @param {object} params 
 */
invAskReqHandler.getInvAskBillDetList = async function (params) {
    params = Object.assign({ interfaceid: 'ql-2322' }, qlParams, params);
    return reqHandler.qlIFCHandler(params);
};

/**
 * 终结要货单
 * @param {object} params 
 * @param {object} params.pk 主键，要货单id
 */
invAskReqHandler.endInvAskBill = async function (params) {
    params.interfaceid = 'cs-invAskDoEnd';
    // console.log(`终结params=${JSON.stringify(params)}`);
    return reqHandler.csIFCHandler(params);
};


/**
 * 要货单调拨发货保存
 * @param {object} params 
 * @param {object} params
 */
invAskReqHandler.saveShopOutForInvAskBill = async function (params) {
    params.interfaceid = 'sf-2324-1';
    params.action = params.pk ? 'edit' : 'add';
    return common.editBilling(params, params.check);
};



/**
 * 获取要货单调拨发货详情 要货单转调拨单页面
 * @param {object} params 
 * @param {object} params.pk 要货单id
 */
invAskReqHandler.getShopOutForInvAskInfo = async function (params) {
    params.interfaceid = 'qf-2324-1';
    //qf-2324-1这个接口action要传add wky说
    params.action = 'add';
    return reqHandler.queryBilling(params);

};

/**
 * 获取要货调拨列表
 * @description deliver:经办人
 * @param {object} params 
 */
invAskReqHandler.getShopOutForInvAskList = async function (params) {
    params = Object.assign({ interfaceid: 'ql-2324' }, qlParams, params);
    return reqHandler.qlIFCHandler(params);
};



/**
 * 按要货报单列表
 * @param {object} params
 */
invAskReqHandler.getFormListByAsk = async function (params) {
    params = Object.assign({ interfaceid: 'ql-22108' }, qlParams, params);
    return reqHandler.qlIFCHandler(params);
};

/**
 * 按要货报单详情
 * @param {object} params 
 * @param {object} params.pk 门店id
 * @param {object} params.dwid 厂商id 没有厂商传0（客户端这样处理的）
 */
invAskReqHandler.getFormInfoByAsk = async function (params) {
    params.interfaceid = 'qf-22108-1';
    params.action = 'add';
    return reqHandler.queryBilling(params);
};

/**
 * 保存按要货报单
 * @param {object} params 
 */
invAskReqHandler.saveFormByAsk = async function (params) {
    params.interfaceid = 'sf-22108-1';
    params.action = params.pk ? 'edit' : 'add';
    return common.editBilling(params, params.check);
};





