"use strict";
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../basiceJsonparam.js');
let resportSumType = module.exports = {};

/**
 *新增收入支出类别，inouttype为1是收入类别，为2是支出类别，
 *action为add是新增，edit是修改，调用同一个接口sf-1660
 *action, inouttype, pk
 */
resportSumType.addOrEditType = async function (param = {}) {

	let name = param.name || common.getRandomStr(6);
	param.action = param.pk ? 'edit' : 'add';
	if(name != param.name) {
		if(param.inouttype === 1) {
			param.name = `收入${name}`;
		} else if(param.inouttype === 2) {
			param.name = `支出${name}`;
		}
	}
	let catid = await common.callInterface('sf-1660', {
		jsonparam: param,
	}); //callInterface请求结果是{vai：id}  二代要加jsonparam包起来
	param.pk = catid.val;
	return {
		'params': param,
		'result': catid,
	}
}

/**
 * 查询收入表详情
 * inComeCheckQuery -  qf-1355-1
 * 对应sf-1355-1
 * @alias module:requestHandler
 * @async
 * @param {string} pk 单据唯一码
 * @return {object} {params,result}
 */
resportSumType.inComeCheckQuery = async function (pk) {
	const params = {
		'action': 'edit', //add返回的是空单据的数据
		'pk': pk,
	};
	const qfData = await common.callInterface('qf-1355-1', params);

	return {
		'params': params,
		'result': qfData,
	};
};


/**
 *作废收支单
 */
resportSumType.cancelFinMain = async function (pk, succeed = true) {
	let res = await common.callInterface('cs-cancelFinMain', {
		'action': 'edit',
		'pk': pk,
	});
	//console.log(`res : ${JSON.stringify(res)}`);
	succeed && expect(res, `作废单据失败,reason:${JSON.stringify(res)}`).to.includes({
		'val': 'ok',
	});
	return res;
};

//-------获取门店账户信息---------
resportSumType.getAccountInfo = async function (invid) {
	let accountInfo = await common.callInterface('cs-getAccountData', format.qlParamsFormat({
		invid: invid,
	}));
	expect(accountInfo, `查询门店账户信息错误\n${JSON.stringify(accountInfo)}`).to.not.have.property('error');
	return accountInfo;
}
