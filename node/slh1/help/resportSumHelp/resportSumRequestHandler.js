'use strict';
const common = require('../../../lib/common.js');

let requestHandler = module.exports = {};


/**
 * getInOutRecExp
 *  - 根据单据详细信息拼接统计分析-收支流水的期望值
 * 订单需要用相应的预付款单的结果
 * @param {object} res qf接口返回值(需要有qf接口的interfaceid)
 * @return {array} [data1,data2...]
 */
requestHandler.getInOutRecExp = function (res) {
	let exp = {};
	let data = {
		optime: res.optime,
		prodate: res.prodate,
		opstaffName: USEECINTERFACE == 1 ? res.opname : _.last(res.opname.split(',')), //000,总经理
		billno: res.billno,
		rem: `单位[${res.show_dwid}]`,
		accountInvName: res.show_shopid,
		staffName: res.show_deliver.split(',')[1], //000,总经理
		// id:账户流水id,
	};
	// ['代收收款', '采购单', '销售单']
	let plusMinus = 1; //销售为正 采购为负
	switch (res.interfaceid) {
		case 'qf-14211-1':
			data.billtype = '销售单';
			break;
		case 'qf-14212-1':
			data.billtype = USEECINTERFACE == 1 ? '采购单' : '采购进货';
			plusMinus = -1;
			break;
		default:
			break;
	};

	['cash', 'card', 'remit', 'agency'].forEach((key) => {
		if (res[key] && res[key] != 0) {
			let accountid = res[`${key}accountid`];
			if (exp[accountid]) {
				exp[accountid].money += Number(res[key]) * plusMinus;
			} else {
				let data1 = _.cloneDeep(data);
				data1.accountid = res[`${key}accountid`];
				//二代先不判断accountName
				if (USEECINTERFACE == 1) data1.accountName = res[`show_${key}accountid`];
				data1.money = Number(res[key]) * plusMinus;
				exp[accountid] = data1;
			};
		};
	});

	return Object.values(exp);
};


/**
 * editReceivableBill - 新增修改应收调整单
 * 针对客户
 * @param {object}  params              、
 * @param {object}    params.jsonparam    jsonparam
 * @param {boolean} [params.check=true] 是否需要断言
 * @return {object} {params,result}
 */
requestHandler.editReceivableBill = async function ({
	jsonparam,
	check = true
} = {}) {
	jsonparam = Object.assign({
		deliver: LOGINDATA.id,
		dwid: BASICDATA.dwidXw,
		invid: LOGINDATA.invid,
		totalmoney: common.getRandomNumStr(4),
		prodate: common.getCurrentDate(),
		remark: '应收调整单',
		type: 22, //单据类型 服务端有默认值
	}, jsonparam);
	if (jsonparam.pk) jsonparam.action = 'edit';
	let result = await common.callInterface('sf-1345', {
		jsonparam: jsonparam,
	});
	let optime = common.getCurrentTime();
	check && expect(result, `保存应收调整单sf-1345失败:jsonparam=${JSON.stringify(jsonparam)}\nresult=${JSON.stringify(result)}`)
		.to.not.have.property('error');
	return {
		params: jsonparam,
		result,
		optime,
	};
};

/**
 * editReceivableBill - 新增修改应付调整单
 * 针对厂商
 * @param {object}  params              、
 * @param {object}    params.jsonparam    jsonparam
 * @param {boolean} [params.check=true] 是否需要断言
 * @return {object} {params,result}
 */
requestHandler.editPaybleBill = async function ({
	jsonparam,
	check = true
} = {}) {
	jsonparam = Object.assign({
		deliver: LOGINDATA.id,
		dwid: BASICDATA.dwidVell,
		invid: LOGINDATA.invid,
		//shopId: LOGINDATA.invid,
		totalmoney: common.getRandomNumStr(4),
		prodate: common.getCurrentDate(),
		remark: '应付调整单',
		type: 13, //单据类型 服务端有默认值
	}, jsonparam);
	if (jsonparam.pk) jsonparam.action = 'edit';
	//console.log(`jsonparam : ${JSON.stringify(jsonparam)}`);
	let result = await common.callInterface('sf-1346', {
		jsonparam: jsonparam,
	});
	let optime = common.getCurrentTime();
	check && expect(result, `保存应付调整单sf-1346失败:jsonparam=${JSON.stringify(jsonparam)}\nresult=${JSON.stringify(result)}`)
		.to.not.have.property('error');
	return {
		params: jsonparam,
		result,
		optime,
	};
};
