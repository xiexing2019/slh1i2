'use strict';
const caps = require('../../../data/caps.js');
const moment = require('moment');
const common = require('../../../lib/common');

let invAskJson = module.exports = {};


/**
 * 要货单json
 * @param {object} params
 * @param {object} params.styleInfo
 */
invAskJson.invAskBillJson = (params) => {
    let json = {
        interfaceid: 'sf-2320-1',
        invid: LOGINDATA.invid,//申请门店ID值
        targetinvid: BASICDATA.shopidZzd,//目标仓库
        deliver: LOGINDATA.id,//营业员
        prodate: common.getCurrentDate(),
        totalnum: 0,//申请数量
    };
    let colorIds = params.styleInfo.colorids.split(','),
        sizeIds = params.styleInfo.sizeids.split(',');
    let barCodes = [];
    for (let index = 0; index < colorIds.length; index++) {
        let colorId = colorIds[index];
        sizeIds.forEach(sizeId => {
            barCodes.push({ colorId, sizeId })
        });
    };
    barCodes = common.randomSort(barCodes);//随机颜色尺码
    json.details = Array(Number(params.count || 2)).fill({}).map((detail, index) => {
        detail = {
            num: common.getRandomNum(10, 100),//要货数量
            styleid: params.styleInfo.pk,//款号ID值
            colorid: barCodes[index].colorId,//颜色ID值
            sizeid: barCodes[index].sizeId,//尺码id
            rem: `明细${index + 1}`,//备注
        };
        json.totalnum = common.add(detail.num, json.totalnum);
        return detail;
    });
    return json;
};

/**
 * 批量要货
 * @param {object} params
 * @param {object} params.styleInfo
 */
invAskJson.bulkInvAskBillJson = (params) => {
    let json = invAskJson.invAskBillJson({ styleInfo: params.styleInfo, count: params.count || 3 });
    json.interfaceid = 'sf-2319-1';
    delete json.targetinvid;
    //设置默认值
    if (json.details.length == 3) {
        json.details[0].targetinvid = BASICDATA.shopidZzd;
        json.details[1].targetinvid = BASICDATA.shopidWyd;
        json.details[2].targetinvid = BASICDATA.shopidZzd;
    }
    return json;
};

