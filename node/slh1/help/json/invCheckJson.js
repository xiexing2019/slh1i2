'use strict';
const common = require('../../../lib/common.js');

let json = module.exports = {};

json.invCheckJson = function () {
	return {
		interfaceid: 'sf-1924-1',
		details: [{
			sizeid: 'M',
			colorid: 'BaiSe',
			matCode: 'Agc001',
			price: '0',
			rem: '明细1',
			num: '1000',
    	}, {
			sizeid: 'L',
			colorid: 'BaiSe',
			matCode: 'Agc001',
			price: '0',
			rem: '明细2',
			num: '2000',
    	}],
	};
};
