'use strict';
const common = require('../../../lib/common.js');
let salesJson = module.exports = {};

//销售正常开单 无欠余
// 这里details数组必须至少有2个元素
// classSummaryList.js等类里面将默认这里至少有两个元素，没有做数组越界的判断

/**
 * 上次价JSON
 * @param {object} params.code
 */
salesJson.lastPriceJson = (params = {}) => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"styleid": params.id,
			// "matCode": params.code || "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": common.getRandomNum(1, 50),
			"discount": 1,
		}],
	};
};



salesJson.rechargeJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 10000,
		"cash": 0,
		"remit": 0,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": []
	};
};

salesJson.salesJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

/**
 * 退货json
 */
salesJson.returnBackJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 0,
		"cash": 0,
		"remit": 0,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": -2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": -3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

//代收
salesJson.agencySalesJson = () => {
	var rem = common.getRandomStr(6);
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"remark": "代收",
		"agency": 600,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"logisBillno": new Date().getTime(),
		"logisRem": `logisRem${rem}`,
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
		}],
	};
};

//快速标记代收模式jsonparam
salesJson.quickAgencySalesJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"remark": "快速标记代收",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"balancetype": 2, //只有快速标记代收模式下，代收设置为是的情况下balancetype=2,其他情况balancetype=0， 服务端会根据其他业务参数自动判定结余类型，结余类型:0为未付,2代收,3退货、4.预付,5为找零",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

//开单的同时订货
salesJson.orderWhenSalesJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "开单的同时订货",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"recvnum": "5",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"recvnum": "5",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

// 特殊货品
salesJson.specialSalesJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 210,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"rem": "Spec1",
			"matCode": "00000",
			"price": 20,
			"num": -1,
			"colorid": "0",
			"sizeid": "0"
		}, {
			"rem": "Spec2",
			"matCode": "00001",
			"price": 30,
			"num": 1,
			"colorid": "0",
			"sizeid": "0"
		}],
	};
};

//简化的jsonparam 只有必填项
salesJson.simpleSalesJson = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"cash": 400,
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

// 现金刷卡汇款加产品折扣
salesJson.salesJsonPaymenthod5 = (goodInfo = undefined) => {
	let json = {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "现金刷卡汇款加客户折扣",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
	if (goodInfo) {
		let details = [];
		for (let i = 0; i < json.details.length; i++) {
			details.push({
				"num": json.details[i].num,
				"sizeid": goodInfo.sizeids.split(',')[i],
				"styleid": goodInfo.pk,
				"rem": json.details[i].rem,
				"colorid": goodInfo.colorids.split(',')[0],
				"price": goodInfo.stdprice1,
				"discount": (Number(goodInfo.discount) || 1).toString(),
			});
		}
		json.details = details;
	}
	return json;
};


// 现金刷卡汇款加客户折扣
salesJson.salesJsonPaymenthod6 = (customer = undefined) => {
	let json = {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "现金刷卡汇款加客户折扣",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
	if (customer) {
		json.dwid = customer.pk;
		json.maindiscount = (Number(customer.discount) || 1).toString();
	}
	return json;
};

// 现金+刷卡+汇款+配货员
salesJson.salesJsonPaymethod20 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		'invdisopid': LOGINDATA.id,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	}
};

// 现金+刷卡+汇款+配货员  即退货又拿货
salesJson.salesJsonPaymethod20_1 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": BASICDATA.dwidXw,
		'invdisopid': LOGINDATA.id,
		"card": 20,
		"cash": 20,
		"remit": 40,
		"remark": "备注",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": -1,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 1,
			"sizeid": "L",
			"matCode": "Agc002",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 280,
			"discount": 1,
		}],
	}
};

//开单模式19 产品折扣+代收
salesJson.salesJsonPaymethod19 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"remark": "开单模式19",
		"agency": 480,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "0.8"
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "0.8"
		}],
	};
};

//整单折扣+代收
salesJson.salesJsonPaymenthod18 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"remark": "开单模式18",
		"agency": 480,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"maindiscount": "0.8",
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
		}],
	};
};

//异地  这里details数组必须是至少有两个元素
//开单门店 常青店  发货门店 中洲店
salesJson.salesJsonPaymenthod15 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"remark": "异地发货",
		"remit": 100,
		"card": 200,
		"cash": 300,
		"shopid": BASICDATA.shopidCqd,
		"invid": BASICDATA.shopidZzd,
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
		}],
	};
};

//异地+代收
//开单门店 常青店  发货门店 中洲店
salesJson.salesJsonPaymenthod21 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"remark": "开单模式21",
		"agency": 600,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"shopid": BASICDATA.shopidCqd,
		"invid": BASICDATA.shopidZzd,
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
		}],
	};
};

//异地+代收 + 开单同时订货
//开单门店 常青店  发货门店 中洲店
salesJson.salesJsonPaymenthod21_1 = () => {
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1", //一代原来是2，适配二代报错，改1，一代也是可以跑的
		"remark": "异地发货+代收+开单同时订货",
		"agency": 600,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"shopid": BASICDATA.shopidCqd,
		"invid": BASICDATA.shopidZzd,
		"details": [{
			"num": 1,
			"recvnum": "5",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 2,
			"recvnum": "5",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

// 按组开单 童装模式+代收
salesJson.salesJsonPaymenthod10 = () => {
	var rem = common.getRandomStr(6);
	return {
		"interfaceid": "sf-14211-1",
		"srcType": "1",
		"agency": 600,
		"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"logisBillno": new Date().getTime(),
		"logisRem": `logisRem${rem}`,
		"remark": "备注",
		"details": [{
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "M",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "1",
			"rowid": "0"
		}, {
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "1",
			"rowid": "0"
		}, {
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "XL",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "1",
			"rowid": "0"
		}, {
			"num": "1",
			"groupflag": "0",
			"sizeid": "M",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "1",
			"rowid": "0"
		}, {
			"num": "3",
			"groupflag": "0",
			"sizeid": "L",
			"matCode": "Agc001",
			"colorid": "BaiSe",
			"price": "200",
			"discount": "1",
			"rowid": "0"
		}, {
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "M",
			"matCode": "Agc001",
			"colorid": "HongSe",
			"price": "200",
			"discount": "1",
			"rowid": "1"
		}, {
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"colorid": "HongSe",
			"price": "200",
			"discount": "1",
			"rowid": "1"
		}, {
			"num": "2",
			"groupflag": "1",
			"groupnum": "2",
			"sizeid": "XL",
			"matCode": "Agc001",
			"colorid": "HongSe",
			"price": "200",
			"discount": "1",
			"rowid": "1"
		}, {
			"num": "3",
			"groupflag": "0",
			"sizeid": "L",
			"matCode": "Agc001",
			"colorid": "TuoSe",
			"price": "200",
			"discount": "1",
			"rowid": "2"
		}, {
			"num": "2",
			"groupflag": "0",
			"sizeid": "XL",
			"matCode": "Agc001",
			"colorid": "TuoSe",
			"price": "200",
			"discount": "1",
			"rowid": "2"
		}],
		"type": "21",
		"typeid": "21",
		"actid": "21"
	};
};
