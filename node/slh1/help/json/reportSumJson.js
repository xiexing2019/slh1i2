"use strict";
const common = require('../../../lib/common.js');
let resportSumJson = module.exports = {};



//收入单
resportSumJson.incomeJson = function () {
	return {
		interfaceid: "sf-1355-1",
		inouttype: 1,
		rem: "备注123",
		details: [{
			money: 123,
			price: '0',
			rem: "明细1",
		}, {
			money: 145,
			price: '0',
			rem: "明细2",
		}]
	};
}

//支出单
resportSumJson.outcomeJson = function () {
	return {
		interfaceid: "sf-1356-1",
		inouttype: 2,
		rem: "备注123",
		details: [{
			money: 123,
			price: '0',
			rem: "明细1",
		}, {
			money: 145,
			price: '0',
			rem: "明细2",
		}]
	};
}
