'use strict';
const common = require('../../../lib/common.js');
let orderJson = module.exports = {};

//details里面的数值最好不要改动
orderJson.salesOrderJson = () => {
	return {
		"interfaceid": "sf-14401-1",
		"clientid": BASICDATA.dwidXw,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

//销售订货+特殊货品 details里面的数值最好不要改动
orderJson.specialSalesOrderJson = () => {
	return {
		"interfaceid": "sf-14401-1",
		"card": 210,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"rem": "Spec1",
			"matCode": "00000",
			"price": 20,
			"num": -1,
			"colorid": "0",
			"sizeid": "0"
		}, {
			"rem": "Spec2",
			"matCode": "00001",
			"price": 30,
			"num": 1,
			"colorid": "0",
			"sizeid": "0"
		}],
	};
};

// 现金刷卡汇款+产品折扣
orderJson.salesOrderJson5 = (goodInfo = undefined) => {
	let json = {
		"interfaceid": "sf-14401-1",
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "现金刷卡汇款+客户折扣",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
	if (goodInfo) {
		let details = [];
		for (let i = 0; i < json.details.length; i++) {
			details.push({
				"num": json.details[i].num,
				"sizeid": goodInfo.sizeids.split(',')[i],
				"styleid": goodInfo.pk,
				"rem": json.details[i].rem,
				"colorid": goodInfo.colorids.split(',')[0],
				"price": goodInfo.stdprice1,
				"discount": goodInfo.discount,
			});
		}
		json.details = details;
	}
	return json;
};

// 现金刷卡汇款+客户折扣
orderJson.salesOrderJson6 = (customer = undefined) => {
	let json = {
		"interfaceid": "sf-14401-1",
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "现金刷卡汇款+客户折扣",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
	if (customer) {
		json.clientid = customer.pk;
		json.maindiscount = customer.discount;
	}
	return json;
};

// 现金+刷卡+汇款+配货员
// 验证数据时默认配货员是当前登录人员，因此最好不要改动invdisopid
orderJson.salesOrderJson20 = () => {
	return {
		"interfaceid": "sf-14401-1",
		'invdisopid': LOGINDATA.id,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "备注",
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": 3,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}]
	};
};

//异地+代收
//开单门店 常青店  发货门店 中洲店
//订货没有代收 在按订货开单再进行代收操作
orderJson.salesOrderJson21 = () => {
	return {
		"interfaceid": "sf-14401-1",
		"remark": "开单模式21",
		// "agency": 600,
		// "logisDwid": BASICDATA.dwidSFKD, //物流商ID值
		"shopid": BASICDATA.shopidCqd,
		"invid": BASICDATA.shopidZzd,
		"details": [{
			"num": "1",
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": "200",
		}, {
			"num": "2",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
			"price": "200",
		}],
	};
};


orderJson.salesOrderDistributeJson = (orderDet) => {
	let jsonparam = {
		mainid: orderDet.pk,
		details: [],
	};
	orderDet.details.forEach(detail => {
		jsonparam.details.push({
			detid: detail.pk,
			disnum: Number(detail.num)
		});
	});
	return {
		interfaceid: 'cs-saveOrderDetDistribute',
		jsonparam
	};
};