'use strict';
const caps = require('../../../data/caps.js');
const moment = require('moment');

let inOutJson = module.exports = {};

//--------门店调出-------

// 中洲店是盘点门店，也做盘点门店用，
// 所以在做调拨的时候，不要从其他门店调货到中洲店，
// 如果不得已调了，请立即将在途调拨的所有单据做一遍调入操作
inOutJson.outJson = () => {
	return {
		'interfaceid': 'sf-1862-1',
		'actid': '25',
		'action': 'add',
		'shopid': BASICDATA.shopidZzd, //调出门店
		'invidref': BASICDATA.shopidCqd, //调入门店
		'details': [{
			'colorid': 'BaiSe',
			'num': '3',
			'price': '200',
			'sizeid': 'M',
			"matCode": "Agc001",
			'total': '600',
			'rem': '明细1'
		}],
		'inoutflag': '2',
		'invalidflag': '0',
		'remark': '批量调出',
		'type': '25',
		'typeid': '25'
	};
};


//---------门店调入--------


inOutJson.inJson = (orginJson) => {

	let time = moment().format('YYYY-MM-DD HH:mm:ss:SSS');
	let json = {
		'actid': '16',
		'type': '16',
		'typeid': '16',
		'action': 'add',
		'deliver': orginJson.deliver,
		'inoutflag': '1',
		'invalidflag': '0',
		'invid': orginJson.invidref,
		'shopid': orginJson.invidref,
		'remark': orginJson.remark,
		'totalnum': orginJson.totalnum,
		'prodate': orginJson.prodate,
		'invidref': orginJson.shopid,
		'hashkey': `${time}`,
		pk: orginJson.pk,
	};
	if (USEECINTERFACE == 2) {
		json.shopid = orginJson.shopid;
		json.invid = orginJson.shopid;
		json.invidref = orginJson.invidref;
	}

	if (USEECINTERFACE == 1) {
		json.billid = orginJson.pk;
	} else {
		json.billid = orginJson.billid;
	};

	let details = [];
	for (let i = 0; i < orginJson.details.length; i++) {
		let item = orginJson.details[i];
		details.push({
			'billid': item.pk || item.billid,
			'pk': item.pk,
			'rowid': item.rowid,
			'styleid': item.styleid,
			'colorid': item.colorid,
			'sizeid': item.sizeid,
			'rem': item.rem,
			'num': item.num,
		});
	}
	json.details = details;
	return json;
}
