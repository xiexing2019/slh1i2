'use strict';
const common = require('../../../lib/common.js');
let json = module.exports = {};


//--------货品管理---------

//新增货品
json.addGoodJson = (params = {}) => {
	let str = common.getRandomStr(8);
	return Object.assign({
		action: 'add',
		code: `g${str}`,
		name: `name${str}`,
		invid: LOGINDATA.invid,
		shopid: LOGINDATA.invid,
		marketdate: common.getCurrentDate(),
		colorids: `${BASICDATA.coloridBaiSe}`,
		sizeids: `${BASICDATA.sizeidM},${BASICDATA.sizeidL},${BASICDATA.sizeidXL}`,
		purprice: 100, //采购价
		stdprice1: 200, //销售价1
		stdprice2: 180,
		stdprice3: 160,
		stdprice4: 140,
		isallowreturn: 1, //是否允许退货 是
		discount: 1,
	}, params);
};

json.addGoodJsonFull = () => {
	let str = common.getRandomStr(5);
	return {
		action: 'add',
		code: `g${str}`,
		name: `name${str}`,
		invid: LOGINDATA.invid,
		shopid: LOGINDATA.invid,
		marketdate: common.getCurrentDate(),
		colorids: `${BASICDATA.coloridBaiSe}`,
		sizeids: `${BASICDATA.sizeidM},${BASICDATA.sizeidL},${BASICDATA.sizeidXL}`,
		purprice: 100, //采购价
		stdprice1: 200, //销售价1
		stdprice2: 180,
		stdprice3: 160,
		stdprice4: 140,
		isallowreturn: 1, //是否允许退货 是
		discount: 1,
		brandid: 1,
		season: 1, //代表春季,
		classid: BASICDATA.classIds['鞋'],
		dwid: BASICDATA.dwidVell,
	}
}

// 批量调价--价格
json.changePriceJson = (params = {}) => {
	return Object.assign({
		action: 'add', // add edit
		adjustType: '0', // 0-价格 1-折扣
		stdprice1: '10',
		stdprice2: '20',
		stdprice3: '30',
		stdprice4: '40',
		stdprice5: '50',
		pk: '', // 选中款号id，多个款号id逗号隔开。如果pk为空，根据下面的查询条件筛选商品
	})
}
// 批量调价价格-断言
json.changePriceJsonExp = () => {
	return {
		newstdprice1: '10',
		newstdprice2: '20',
		newstdprice3: '30',
		newstdprice4: '40',
		newstdprice5: '50',
	}
}

// 批量调价-折扣
json.changeDiscountJson = (params = {}) => {
	return Object.assign({
		action: 'add', // add edit
		adjustType: '1',
		batchDiscount: '0.5',
		pk: '', // 选中款号id，多个款号id逗号隔开。如果pk为空，根据下面的查询条件筛选商品
	}, params)
}

// 品牌折扣
json.brandDiscountJson = () => {
	return {
		'action': 'add',
		'discount0': '1',
		'discount1': '0.5',
		'discount2': '0.6',
		'discount3': '0.7',
		'discount4': '0.8',
		'discount5': '0.9',
	}
}
// 修改品牌折扣
json.editBrandDiscountJson = () => {
	return {
		'action': 'edit',
		'discount0': '1.1',
		'discount1': '1.2',
		'discount2': '1.3',
		'discount3': '1.4',
		'discount4': '1.5',
		'discount5': '1.6',
	}
}

// 修改品牌折扣断言
json.editBrandDiscountExpJson = () => {
	return {
		'purprice': '1.1',
		'stdprice1': '1.2',
		'stdprice2': '1.3',
		'stdprice3': '1.4',
		'stdprice4': '1.5',
		'stdprice5': '1.6',
	}
}


json.brandDiscountExpJson = () => {
	return {
		'purprice': '1',
		'stdprice1': '0.5',
		'stdprice2': '0.6',
		'stdprice3': '0.7',
		'stdprice4': '0.8',
		'stdprice5': '0.9',
	}
}


//-------------------往来管理------------------

//新增客户
json.addCustJson = () => {
	var name = common.getRandomStr(6);
	return {
		"action": "add",
		"nameshort": `cust${name}`,
		"creditbalance": "0",
		"discount": "1",
		invid: LOGINDATA.invid,
	};
};

//三位小数客户折扣
json.threeDecimalCustDiscountJson = () => {
	var name = common.getRandomStr(6);
	return {
		"action": "add",
		"nameshort": `cust${name}`,
		"creditbalance": "0",
		"discount": "0.855",
	};
};

//新增厂商
json.addManufacturerJson = () => {
	let masName = 'mas' + common.getRandomStr(6);

	return {
		name: masName,
		nameshort: masName,
		discount: '1',
		creditbalance: '1',
		action: 'add',
	}
};

//客户区域
json.addCustomerDistrict = (params = {}) => {
	return Object.assign({
		'action': 'add',
		'name': `客户区域${common.getRandomStr(5)}`,
		'py': `py${common.getRandomStr(5)}`,
		'parentid': '',
		'type': 'area'
	}, params)
}


//-------------------盘点管理--------------------

// 新增盘点
json.addInventoryJson = (mat) => {
	let json = {
		"interfaceid": "sf-1924-1",
		"details": [{
			"num": "100",
			"sizeid": "M",
			"matCode": "agc001",
			"rem": "盘点明细备注",
			"colorid": "BaiSe"
		}],
		"action": "add"
	};

	if (mat) {
		json.details[0].styleid = mat.pk;
		json.details[0].sizeid = mat.sizeids.split(',')[0];
		json.details[0].colorid = mat.colorids.split(',')[0];
	}
	return json;
};



//------------------门店管理------------------
//新增员工
json.saveStaffJson = (params) => {
	return Object.assign({
		name: '员工' + common.getRandomStr(5),
		code: 'code' + common.getRandomStr(6),
		birthday: common.getCurrentDate(),
		sex: 1, //0 空  1 男  2 女
		addr: (params.pk ? '修改地址' : '新增地址') + common.getRandomChineseStr(5) + common.getRandomStr(6),
		mobile: common.getRandomNumStr(11),
		qq: common.getRandomNumStr(9),
		// assistantFlag:,//店员助手标志 0：否，1：是
		// spAssistantFlag:,//好店助手标志 0：否，1：是
	}, params);
};


/**
 *	apiKey: ec-mdm-org-saveFull
 *	保存组织机构
 *
 *
 *  capability 能力位
 *  二进制: 1-客户 10-供应商 100-加盟商
 */


/**
 * addRetailTraderJson - 新增分销商
 * @param {object}  jsonParam
 * @param {string}  jsonParam.name               名称
 * @param {string}  jsonParam.branchCode         小租户代码
 * @param {string} [jsonParam.parentId=当前登录门店] 父组织id
 * @return {object}
 */
json.addRetailTraderJson = ({
	name,
	branchCode,
	parentId,
}) => {
	let json = {
		'1': {
			'areaId': '',
			'shopId': '',
			'staffId': '',
			'typeId': '',
			'priceType': '',
			'discount': 1,
			'rank': '',
			'noRetFlag': 0, //是否不允许退货
			'isDebt': 0, //是否欠款
			'linkFlag': 0,
			'authCode': '',
			'labelCode': '', //客户标签；多个时，隔开
			'extraSrc': '',
			'creditBalance': '',
			'alarmBalance': '',
			'slbFlag': 0,
			'clientSign': '',
			'ecCaption': {
				'shopId': '',
				'staffId': '',
				'typeId': '',
				'priceType': '',
				'areaId': ''
			}
		},
		'4': {},
		'1024': {
			'branchCode': branchCode, //小租户代码 *
		},
		'address': {
			'provinceCode': '',
			'cityCode': '',
			'countyCode': '',
			'detailAddr': ''
		},
		'owner': {
			'mobile': '',
			'userName': '',
			'birthday': '',
			'gender': 0, //性别：1男，2女，0未知
		},
		'org': {
			// 'id':'',//组织机构id（新增时为空，修改时必须有值）
			'name': name,
			'code': '', //编码
			'organType': 1, //组织类型 *
			'capability': 1028, //能力位 *
			'compId': '',
			'parentId': parentId, //父组织id
			'telephone': '',
			'addrId': '',
			'areaAccode': '',
			'contacts': {
				'fax': '',
				'weixin': ''
			},
			'fax': '',
			'weixin': '',
			'showOrder': '', //显示顺序
			'tenantId': '',
			'remark': '',
			'accode': '',
			'createdDate': '',
			'updatedDate': '',
			'ver': '',
			'ecCaption': {
				'parentId': ''
			}
		}
	};
	return json;
};

//新增门店
json.addShopJson = ({
	name
}) => {
	return {
		'64': {
			'typeId': '',
			'printHead': ' ',
			'printRem': '',
			'bindInvId': '',
			'priceGroup': '',
			'tallyAccountId': '',
			'accountName': '',
			'accountNo': '',
			'accountName2': '',
			'accountNo2': '',
			'accountName3': '',
			'accountNo3': '',
			'accountName4': '',
			'accountNo4': '',
			'accountName5': '',
			'accountNo5': '',
			'accountName6': '',
			'accountNo6': '',
			'alipay': '',
			'weixinpay': '',
			'marketId': '',
			'logoFileId': '',
			'shopArea': '',
			'openid': '',
			'busiAreaUserIds': '',
			'updatedDate': '',
			'invFlag': 0,
			'ecCaption': {
				'bindInvId': '',
				'priceGroup': ''
			}
		},
		'256': {
			'typeId': ''
		},
		'address': {
			'provinceCode': '',
			'cityCode': '',
			'countyCode': '',
			'townCode': '',
			'detailAddr': ''
		},
		'org': {
			'name': name,
			'namePy': '',
			'code': '',
			'organType': '2',
			'capability': 320,
			'ownerId': '',
			'compId': '',
			'telephone': '',
			'addrId': '',
			'areaAccode': '',
			'contacts': '',
			'showOrder': '',
			'tenantId': '',
			'remark': '',
			'accode': '',
			'createdDate': '',
			'updatedDate': '',
			'ver': '',
			'ecCaption': {
				'ownerId': ''
			},
		},
	};
};

json.addStaffJson = ({
	code,
	name,
	shopId,
	roleIds
}) => {
	return {
		'staff': {
			'code': code,
			'mobile': '',
			'userName': name,
			'birthday': '',
			'nickName': '',
			'gender': 0,
			'depId': shopId,
			'password': '',
			'roleIds': roleIds,
			'staffType': '',
			'assistantFlag': 0,
			'ecCaption': {
				'gender': '',
				'depId': '',
				'staffType': ''
			}
		},
		'address': {
			'detailAddr': ''
		},
	};
};

json.addCustMdmJson = ({
	name,
	relFranId
}) => {
	return {
		'1': {
			'areaId': '',
			'shopId': '',
			'staffId': '',
			'typeId': 0,
			// 'priceType': '',
			'discount': 1,
			'rank': '',
			'noRetFlag': 0,
			'isDebt': 0,
			'linkFlag': 0,
			'relFranId': relFranId, //关联加盟商
			'authCode': '',
			'labelCode': '',
			'extraSrc': '',
			'creditBalance': '',
			'alarmBalance': '',
			'slbFlag': 0,
			'clientSign': '',
			'ecCaption': {
				'shopId': '',
				'staffId': '',
				'typeId': '',
				// 'priceType': '',
				'areaId': '',
				'relFranId': ''
			}
		},
		'address': {
			'provinceCode': '',
			'cityCode': '',
			'countyCode': '',
			'detailAddr': ''
		},
		'owner': {
			'mobile': '',
			'userName': '',
			'birthday': '',
			'gender': 0
		},
		'org': {
			'name': name,
			'code': '',
			'organType': 1,
			'capability': 1,
			'compId': '',
			'parentId': '',
			'telephone': '',
			'addrId': '',
			'areaAccode': '',
			'contacts': {
				'fax': '',
				'weixin': ''
			},
			'fax': '',
			'weixin': '',
			'showOrder': '',
			'tenantId': '',
			'remark': '',
			'accode': '',
			'createdDate': '',
			'updatedDate': '',
			'ver': '',
			'ecCaption': {
				'parentId': ''
			}
		}
	};
};

json.addCompanyJson = ({
	name,
	relFranId
}) => {
	return {
		'2': {
			'staffId': '',
			'shopId': '',
			'linkFlag': 0,
			'accountNo': '',
			'areaId': '',
			'priceType': '',
			'relFranId': relFranId,
			'ecCaption': {
				'shopId': '',
				'staffId': '',
				'areaId': '',
				'relFranId': ''
			}
		},
		'address': {
			'provinceCode': '',
			'cityCode': '',
			'countyCode': '',
			'townCode': '',
			'detailAddr': ''
		},
		'owner': {
			'mobile': '',
			'userName': '',
			'birthday': '',
			'gender': ''
		},
		'org': {
			'name': name,
			'code': '',
			'organType': 1,
			'capability': 2,
			'ownerId': '',
			'parentId': 0,
			'compId': '',
			'telephone': '',
			'addrId': '',
			'areaAccode': '',
			'contacts': '',
			'showOrder': '',
			'tenantId': '',
			'remark': '',
			'accode': '',
			'createdDate': '',
			'updatedDate': '',
			'ver': '',
			'ecCaption': {
				'ownerId': ''
			},
		},
	};
};