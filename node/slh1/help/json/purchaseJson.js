'use strict';
let purJson = module.exports = {};


//-----------采购订货--------------
purJson.purchaseOrderJson = () => {
	return {
		"interfaceid": "sf-22101-1",
		"card": "400",
		"cash": "2000",
		"remit": "600",
		"rem": "备注", //二代采购订货整单备注字段和一代不同，二代是remark，一代是rem，
		"details": [{
			"num": "10",
			"sizeid": "M",
			"matCode": "Agc001", //用于在jsonparam中拼出styleid
			"rem": "明细1",
			"colorid": "BaiSe"
		}, {
			"num": "20",
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe"
		}]
	};
};

//-----------采购入库--------------

//采购入库 无欠余
purJson.purchaseJson = () => {
	return {
		"interfaceid": "sf-14212-1",
		"card": 1000,
		"cash": 500,
		"remit": 1000,
		"remark": "备注",
		"details": [{
			"num": 10,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
		}, {
			"num": 15,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
		}]
	};
};

// 均色均码采购入库无欠余
purJson.junPurchaseJson = () => {
	return {
		"interfaceid": "sf-14212-1",
		"card": 1000,
		"cash": 500,
		"remit": 1000,
		"remark": "备注",
		"details": [{
			"num": 10,
			"sizeid": "JunMa",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "JunSe",
		}, {
			"num": 15,
			"sizeid": "JunMa",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "JunSe",
		}]
	};
};

//采购员进行采购入库，不要通过format进行格式化，
//使用的时候自己补充deliver、invid、shopid信息即可
purJson.purBypurchaserJson = () => {
	return {
		'actid': '10',
		'action': 'add',
		// 'deliver' = ;
		'details': [{
			'colorid': BASICDATA.coloridBaiSe,
			'num': '10',
			'rowid': '0',
			'sizeid': BASICDATA.sizeidM,
			'styleid': BASICDATA.styleidAgc001,
		}],
		'dwid': BASICDATA.dwidVell,
		'inoutflag': '1',
		'invalidflag': '0',
		// invid = 1224;
		'optime': new Date('YYYY-MM-DD'),
		'prodate': new Date('YYYY-MM-DD'),
		'remark': "采购员采购入库",
		// shopid = 1224;
		'totalnum': '10',
		'type': '10',
		'typeid': '10',
	}
};

// 均色均码模式下批量入库
purJson.bulkPurchaseIgnoreCS = () => {
	return {
		'interfaceid': 'sf-22401-1',
		'inoutflag': '1',
		'type': '10',
		'details': [{
			'colorid': 'JunSe',
			'sizeid': 'JunMa',
			'mainDwid': BASICDATA.dwidVell,
			'discount': '1',
			'price': 100,
			'num': 10,
			'rem': '批量入库明细1',
			'matCode': 'Agc001',
		}, {
			'colorid': 'JunSe',
			'sizeid': 'JunMa',
			'mainDwid': BASICDATA.dwidRt,
			'discount': '1',
			'price': 50,
			'num': 15,
			'rem': '批量入库明细2',
			'matCode': 'Agc002',
		}],
	};
};

// 颜色尺码模式下批量入库(相同款号)
purJson.bulkPurchaseSameMatCS = () => {
	return {
		'interfaceid': 'sf-22401-1',
		'inoutflag': '1',
		'type': '10',
		'remark': '颜色尺码模式下相同款号批量入库',
		'details': [{
			'colorid': 'BaiSe',
			'sizeid': 'M',
			'mainDwid': BASICDATA.dwidVell,
			'discount': '1',
			'price': 100,
			'num': 10,
			'rem': '批量入库明细1',
			'matCode': 'Agc001',
		}, {
			'colorid': 'BaiSe',
			'sizeid': 'L',
			'mainDwid': BASICDATA.dwidVell,
			'discount': '1',
			'price': 100,
			'num': 15,
			'rem': '批量入库明细2',
			'matCode': 'Agc001',
		}],
	};
};

// 颜色尺码模式下批量入库(不同款号)
purJson.bulkPurchaseDiferenceMatCS = () => {
	return {
		'interfaceid': 'sf-22401-1',
		'inoutflag': '1',
		'type': '10',
		'remark': '颜色尺码模式下不同款号批量入库',
		'details': [{
			'colorid': 'BaiSe',
			'sizeid': 'M',
			'mainDwid': BASICDATA.dwidVell,
			'discount': '1',
			'price': 100,
			'num': 10,
			'rem': '批量入库明细1',
			'matCode': 'Agc001',
		}, {
			'colorid': 'BaiSe',
			'sizeid': 'M',
			'mainDwid': BASICDATA.dwidRt,
			'discount': '1',
			'price': 50,
			'num': 15,
			'rem': '批量入库明细2',
			'matCode': 'Agc002',
		}],
	};
};

purJson.specialPurchaseJson = () => {
	return {
		"interfaceid": "sf-14212-1",
		"card": 1010,
		"cash": 500,
		"remit": 1000,
		"remark": "备注",
		"details": [{
			"num": 10,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
		}, {
			"num": 15,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细2",
			"colorid": "BaiSe",
		}, {
			"rem": "Spec1",
			"matCode": "00000",
			"price": 20,
			"num": -1,
			"colorid": "0",
			"sizeid": "0"
		}, {
			"rem": "Spec2",
			"matCode": "00001",
			"price": 30,
			"num": 1,
			"colorid": "0",
			"sizeid": "0"
		}],
	};
};
