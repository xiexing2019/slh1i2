'use strict';
const common = require('../../../lib/common');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const reqHandler = require('../../help/reqHandlerHelp');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const moment = require('moment');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
/**
 * 首页罗盘功能
 * 暂时只有1代
 * 获取罗盘（代办事项）汇总 cs-attention-backlog
 * http://jira.hzdlsoft.com:7082/browse/SLH-22530
 */
describe('首页罗盘', function () {
	this.timeout(300000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		await common.setGlobalParam('mat_useSaleDays', 1); //是否启用款号返厂期限 启用
		await common.setGlobalParam('sales_days_warning', 3); //款号返厂期限即将到期，提前几天提醒 3天
		await common.setGlobalParam('sales_order_delivery_days', 3); //销售订单，订单即将到期提前提醒天数 提前3天到期订单
		//非总经理验证用
		await common.setGlobalParam('sc_show_onlymyshop_bynogm', 1); //非总经理岗位是否只显示自己所在门店(仅跨门店查询用)  所有查询列表只出现自己门店
	});

	after(async () => {
		await common.setGlobalParam('sc_show_onlymyshop_bynogm', 0); //默认显示所有门店
	});

	it('260002.近期生日', async () => {
		let res = await getBacklog(1);

		let birthday = res.birthday;
		let qlRes = await common.callInterface('ql-1339', {
			birthStart: birthday.dateStart,
			birthEnd: birthday.dateEnd,
			delflag: 0
		});

		expect(qlRes, '客户列表查询出错').not.to.have.property('error');
		expect(parseInt(birthday.birthdayCount), '近期生日客户数量与客户列表查询所得数量不符').to.be.equal(qlRes.dataList.length);
	});

	it('260015.调拨在途', async () => {
		let res = await getBacklog('100');

		let onroad = res.onroad;
		let qlRes = await common.callInterface('ql-1867', {
			invidref: onroad.invid,
			pagesize: 0,
		});
		expect(qlRes, '调拨在途查询出错').not.to.have.property('error');
		expect(parseInt(onroad.onroadNum), '调拨在途数量与列表查询所得数量不符').to.be.equal(parseInt(qlRes.dataList.length));
	});

	describe('权限验证', function () {
		it('1.无权限', async () => {
			let res = await getBacklog('000000');
			expect(res).not.to.have.any.keys('birthday', 'onroad', 'orderDiff', 'prePurin', 'orderWarning', 'styleOverSale');
		});
		it('2.有权限', async () => {
			let res = await getBacklog('11111111');
			expect(res).to.have.all.keys('modelClass', 'birthday', 'onroad', 'orderDiff', 'prePurin', 'orderWarning', 'styleOverSale', 'allowDelivery', 'pending');
		});
	});

	// 待入库,订货未发
	let keys = ['prePurin', 'orderDiff'];
	for (let i = 0; i < 2; i++) {
		describe(`待入库,订货未发验证`, function () {
			let info = {},
				curInfo = {},
				key = keys[i];
			before(async () => {
				curInfo = await getBacklog().then(obj => obj[key]);

			});
			it(`${260009 + i * 9}.全部入库`, async () => {
				await dataPreparation({
					key
				}); //新增全部入库数据
				info = _.cloneDeep(curInfo);
				curInfo = await getBacklog().then(obj => obj[key]); //获取当前数据
				common.isApproximatelyEqualAssert(info, curInfo, [], `全部入库后 ${key}数据错误`);
			});
			it(`${260011 + i * 9}.全部入库.超出订货数`, async () => {
				await dataPreparation({
					key,
					editJson: (obj) => {
						obj.cash = Number(obj.cash) + 100;
						obj.details[0].num = Number(obj.details[0].num) + 1;
					}
				});
				info = _.cloneDeep(curInfo);
				curInfo = await getBacklog().then(obj => obj[key]); //获取当前数据
				common.isApproximatelyEqualAssert(info, curInfo, [], `全部入库后 ${key}数据错误`);
			});
			it(`${260010 + i * 9}.部分入库`, async () => {
				let num = 0; //待入库数
				await dataPreparation({
					key,
					editJson: (obj) => {
						obj.details.forEach((detail) => {
							detail.num--;
							num++;
						});
						obj.cash = Number(obj.cash) - num * obj.details[0].price;
					}
				});
				info = _.cloneDeep(curInfo);
				info[`${key}Num`] = common.add(info[`${key}Num`], 1);
				//info[`${key}Num`] = Number(info[`${key}Num`]) + num;
				curInfo = await getBacklog().then(obj => obj[key]); //获取当前数据
				common.isApproximatelyEqualAssert(info, curInfo, ['dateEnd'], `部分入库后 ${key}数据错误`);
			});
			it(`${260012 + i * 9}.部分入库.一条明细不入库,一条明细多入库`, async () => {
				let num = 0;
				await dataPreparation({
					key,
					editJson: (obj) => {
						[obj.details[0].num, num] = [num, obj.details[0].num]; //一条明细不入库
						obj.details[1].num++;
						obj.cash = Number(obj.cash) - obj.details[0].price * (num - 1);
					}
				});
				info = _.cloneDeep(curInfo);
				info[`${key}Num`] = common.add(info[`${key}Num`], 1);
				//	info[`${key}Num`] = common.add(info[`${key}Num`], num);
				curInfo = await getBacklog().then(obj => obj[key]); //获取当前数据
				common.isApproximatelyEqualAssert(info, curInfo, [], `部分入库后,${key}数据错误`);
			});
			it(`${260013 + i * 9}.部分入库后终结订单`, async () => {
				let num = 0;
				await dataPreparation({
					key,
					editJson: (obj) => {
						obj.details.forEach((detail) => {
							detail.num--;
							num++;
						});
						obj.cash = Number(obj.cash) - num * obj.details[0].price;
					},
					endBill: true
				});
				info = _.cloneDeep(curInfo);
				curInfo = await getBacklog().then(obj => obj[key]); //获取当前数据
				common.isApproximatelyEqualAssert(info, curInfo, [], `部分入库后终结订单 ${key}数据错误`);
			});
		});
	};

	describe('260027.即将到期商品', function () {
		before(async () => {
			// 数据准备 防止查询无数据
			// 新增销售周期为1~10的货品, 前置条件设置sales_days_warning为3
			let json = basiceJson.addGoodJson();
			json.saleDays = common.getRandomNum(1, 10); //销售周期
			await basicReqHandler.editStyle({
				jsonparam: json,
			});
		});
		it('1.即将到期-是', async () => {
			//罗盘汇总 货品查询
			let [info, qlRes] = await Promise.all([getBacklog(), common.callInterface('ql-15110', {
				overSaleDays: 1, //即将过期 是
				flag: 0, //是否停用 否
			})]);
			expect(qlRes.count, `即将到期商品统计数据错误`).to.equal(info.styleOverSale.styleOverSaleNum);
			qlRes.dataList.forEach((data) => {
				if (data.saleDaysRemain == '') throw new Error(`货品查询 即将过期-是 显示未设置销售周期的货品\ndata:${JSON.stringify(data)}`);
				if (data.saleDaysRemain < 0 || data.saleDaysRemain > 3) throw new Error(`货品查询 即将过期-是 显示未过期商品\ndata:${JSON.stringify(data)}`);
			});
		});
		it('2.即将到期-否', async () => {
			let qlRes = await common.callInterface('ql-15110', {
				overSaleDays: 0, //即将过期 否
				flag: 0, //是否停用 否
			});
			qlRes.dataList.forEach((data) => {
				if (data.saleDaysRemain == '') throw new Error(`货品查询 即将过期-否 显示未设置销售周期的货品\ndata:${JSON.stringify(data)}`);
				if (data.saleDaysRemain >= 0 && data.saleDaysRemain <= 3) throw new Error(`货品查询 即将过期-否 显示过期商品\ndata:${JSON.stringify(data)}`);
			});
		});
	});

	describe('即将到期订单验证', () => {
		it('260032.即将到期-是', async () => {
			let res = await getBacklog('100000');
			let orderWarning = res.orderWarning;

			let qlRes = await common.callInterface('ql-14433', {
				pagesize: 0,
				isDeliverytime: 1,
			});
			expect(qlRes, '销售订货列表查询出错').not.to.have.property('error');

			expect(parseInt(orderWarning.orderWarningCount), '即将到期订货单数与销售订货列表查询结果数不符合').to.be.equal(qlRes.dataList.length);

			for (let data of qlRes.dataList) {
				let result = data.deliverytime !== undefined && data.deliverytime.length > 0;
				expect(result, '销售订货列表数据无发货日期').to.be.equal(true);

				let diff = getDatesDifference(data.deliverytime);

				result = diff <= 3 && diff >= 0;
				expect(result, '销售订货列表数据发货日期不在提醒范围').to.be.equal(true);
			}
		});

		it('260032.即将到期-否', async () => {
			let qlRes = await common.callInterface('ql-14433', {
				pagesize: 0,
				isDeliverytime: 0,
			});
			expect(qlRes, '销售订货列表查询出错').not.to.have.property('error');

			for (let data of qlRes.dataList) {
				let result = data.deliverytime !== undefined && data.deliverytime.length > 0;
				expect(result, '销售订货列表数据无发货日期').to.be.equal(true);

				let diff = getDatesDifference(data.deliverytime);
				result = diff > 3 || diff < 0;
				expect(result, '销售订货列表数据发货日期在提醒范围').to.be.equal(true);
			}
		});

		it('260033.验证订单类型-未发货的', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData();

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount) + 1, '验证订单类型-未发货出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});

		it('260035.验证订单类型-部分发货的', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData((obj) => {
				obj.details[0].num = 1;
				obj.details[1].num = 1;
			});

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount) + 1, '验证订单类型-部分发货的出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});

		it('260038.验证订单类型-部分多发货的', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData((obj) => {
				obj.details[0].num = 5;
				obj.details[1].num = 0;
			});

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount) + 1, '验证订单类型-部分多发货的出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});

		it('260034.验证订单类型-全部发货的', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData(() => { });

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount), '验证订单类型-全部发货的出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});

		it('260036.验证订单类型-作废的', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData(null, 2);

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount), '验证订单类型-作废的出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});

		it('260037.验证订单类型-结束', async () => {
			let oldRes = await getBacklog('100000');

			await prepareSalesOrderData(null, 1);

			let newRes = await getBacklog('100000');

			expect(parseInt(oldRes.orderWarning.orderWarningCount), '验证订单类型-结束的出错').to.be.equal(parseInt(newRes.orderWarning.orderWarningCount));
		});
	});

	describe('满足发货条件', function () {
		let allowDeliveryNum, info;
		before('获取满足发货数总数', async function () {
			await common.loginDo();
			await common.setGlobalParam('sales_order_deliver_mode', 5);    //发货模式改为按客户未发
			await common.setGlobalParam('sales_orderDeliveryAlert', 0);
			const res = await getBacklog();
			allowDeliveryNum = res.allowDelivery.dwxxCount;
			await common.loginDo({ logid: '200' });
			info = await getBacklog().then(res => res.allowDelivery.dwxxCount);   //获取常青店的满足发货条件数
		});

		after('参数还原', async function () {
			await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单
			await common.setGlobalParam('sales_orderDeliveryAlert', 1);
		});

		//保证满足发货条件 外面显示的数量和列表的总数一致
		it('发货总数验证', async function () {
			await common.loginDo();
			const qlRes = await common.callInterface('ql-142271',
				format.qlParamsFormat({ sortField: 'orderNum', sortfieldOrder: 1 })
			);
			expect(qlRes.dataList.length, `罗盘中满足发货条件数和列表中的总数不一致`).to.be.equal(Number(allowDeliveryNum));
		});

		describe('验证满足发货列表', function () {
			let addCustRes, goodRes, purchaseJson, purchaseRes, salesOrderJson;
			const orderNum = 10;//
			before('新增一个客户和商品', async function () {
				let json = await basiceJson.addCustJson();
				json.sellerid = BASICDATA.staffid000;
				addCustRes = await salesReqHandler.addCust(json);
				//新增商品
				goodRes = await basicReqHandler.editStyle({
					jsonparam: basiceJsonparam.addGoodJson()
				});

				salesOrderJson = basiceJsonparam.salesOrderJson();
				salesOrderJson.details[0].styleid = goodRes.pk;
				salesOrderJson.details[0].num = orderNum;
				salesOrderJson.details.splice(1, 2);
				salesOrderJson.clientid = addCustRes.result.val;
				await common.editBilling(salesOrderJson);
			});

			describe('库存大于订货数', function () {
				before('入库', async function () {
					//入库数大于订货数，保证此款的库存大于订货数
					purchaseJson = basiceJsonparam.purchaseJson();
					purchaseJson.details[0].styleid = goodRes.pk;
					purchaseJson.details[0].num = common.add(orderNum, 1);
					purchaseRes = await common.editBilling(purchaseJson);
				});


				it('库存数大于订货数', async function () {
					let exp = getAllowDeliveryExp(addCustRes, salesOrderJson, purchaseJson);
					let allowDeliveryList = await common.callInterface('ql-142271', format.qlParamsFormat({}));

					let info = allowDeliveryList.dataList.find(val => val.dwid == addCustRes.result.val);

					if (info == undefined) {
						throw new Error('满足发货条件列表中没有显示符合的店员');
					} else {
						common.isApproximatelyEqualAssert(exp, info);
					};
				});

				it('验证此数据是按门店隔离的', async function () {
					await common.loginDo({ logid: '200' });
					let allowDelveryNum = await getBacklog().then(res => res.allowDelivery.dwxxCount);
					expect(allowDelveryNum, `常青店新加了满足发货条件的单据，中洲店数量也增加了`).to.be.equal(info);
				});
			});

			describe('库存小于订货数', function () {
				before('入库小于订货数', async function () {
					await common.loginDo();
					purchaseJson.details[0].num = common.sub(orderNum, 1);
					purchaseJson.action = 'edit';
					purchaseJson.pk = purchaseRes.result.pk;
					await common.editBilling(purchaseJson);
				});

				it('库存数小于订货数', async function () {
					let allowDeliveryList = await common.callInterface('ql-142271', format.qlParamsFormat({}));
					let info = allowDeliveryList.dataList.find(val => val.dwid == addCustRes.result.val);
					let exp = getAllowDeliveryExp(addCustRes, salesOrderJson, purchaseJson);
					common.isApproximatelyEqualAssert(exp, info);
				});
			});

			describe('没有库存', function () {
				before('作废采购入库单', async function () {
					await purReqHandler.cancelPurchaseBill(purchaseRes.result.pk);  //作废采购单
				});

				it('没有库存时', async function () {
					let allowDeliveryList = await common.callInterface('ql-142271', format.qlParamsFormat({}));
					let info = allowDeliveryList.dataList.find(val => val.dwid == addCustRes.result.val);
					expect(info, `客户不满足发货条件时，也显示在了列表中`).to.be.undefined;
				});
			});

		});
	});

	describe('非总经理', function () {
		let info1 = {},
			info2 = {};
		before(async () => {
			await common.loginDo({
				logid: '004', //店长
			});
			info1 = await getBacklog();

			await common.loginDo({
				logid: '200', //中洲店总经理
			});

			let json = basiceJson.addCustJson();
			json.birthday = common.getCurrentDate();
			await salesReqHandler.addCust(json); //新增客户

			await common.editBilling(basiceJson.purchaseOrderJson()); //新增采购订单

			json = basiceJson.salesOrderJson();
			json.deliverytime = moment().add(1, 'days').format('YYYY-MM-DD');
			await common.editBilling(json); //新增销售订单

			await common.loginDo({
				logid: '004', //店长
			});
			info2 = await getBacklog();
		});
		after(async () => {
			await common.loginDo();
		});
		it('260005.近期生日数据验证', async () => {
			expect(parseInt(info1.birthday.birthdayCount) + 1, `非总经理登陆,近期生日客户数量与客户列表查询所得数量不符`).to.equal(parseInt(info2.birthday.birthdayCount));
		});
		it('260007.待入库数据验证', async () => {
			expect(info1.prePurin.prePurinNum, `非总经理登陆,待入库统计了其他门店的数据`).to.equal(info2.prePurin.prePurinNum);
		});
		it('260017.订货未发数据验证', async () => {
			expect(info1.orderDiff.orderDiffNum, `非总经理登陆,订货未发统计了其他门店的数据`).to.equal(info2.orderDiff.orderDiffNum);
		});
		it('260031.即将到期订单数据验证', async () => {
			expect(info1.orderWarning.orderWarningCount, `非总经理登陆,即将到期订单统计了其他门店的数据`).to.equal(info2.orderWarning.orderWarningCount);
		});
	});

});


/**
 * getBacklog - 获取罗盘（代办事项）汇总
 * @param {string} [needsMark=111111] 位运算，默认返回全部，客户端根据权限判断需要返回哪些统计
 *                                    从右往左分别为:近期生日人数,订货待入库件数,调拨在途数,客户订货未发数,即将到期商品,即将超出返厂期限的数量
 * @return {object} 请求结果
 */
async function getBacklog(needsMark = '11111111') {
	let res = await common.callInterface('cs-attention-backlog', {
		needsMark: parseInt(needsMark, '2'),
	});
	if (res.error) throw new Error(`cs-attention-backlog请求失败${JSON.stringify(res)}`);
	return res;
};


/**
 * dataPreparation - 待入库数据准备
 * @param {object}  params
 * @param {string}  [params.key=prePurin]   prePurin:采购,orderDiff:销售
 * @param {object}  [params.editJson={}]   修改订单的jsonparam用于做入库单
 * @param {boolean} [params.endBill=false] 是否需要终结订单
 */
async function dataPreparation({
	key = 'prePurin',
	editJson = {},
	endBill = false
} = {}) {
	let orderRes = await common.editBilling(key == 'prePurin' ? basiceJson.purchaseOrderJson() : basiceJson.salesOrderJson()); //新增订货
	let qfRes = await reqHandler.queryBilling({
		interfaceid: key == 'prePurin' ? 'qf-22101-1' : 'qf-14401-1',
		pk: orderRes.result.pk,
	});
	if (typeof editJson == 'function') {
		editJson(qfRes.result);
	};
	if (key == 'prePurin') {
		await purReqHandler.invinByOrder(qfRes.result); //按订货入库
	} else {
		await salesReqHandler.editSalesBillByOrder(qfRes.result); //按订货开单
	};
	if (endBill) await reqHandler.csIFCHandler({
		interfaceid: key == 'prePurin' ? 'cs-teminalpured' : 'cs-saleoutOrderFinish',
		pk: orderRes.result.pk,
		check: true, //断言
		errorMsg: '终结订单失败',
	}); //终结订单
};

async function prepareSalesOrderData(editJson = null, editFlag = 0) {
	let salesOrderParams = basiceJson.salesOrderJson();
	salesOrderParams.deliverytime = moment().add(1, 'days').format('YYYY-MM-DD');
	let sfRes = await common.editBilling(salesOrderParams);
	if (editJson && typeof (editJson) == 'function') {
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		editJson(qfRes.result);
		await salesReqHandler.editSalesBillByOrder(qfRes.result);
	}

	if (editFlag === 1) {
		await salesOrderRequest.saleOrderFinish(sfRes.result.pk); //终结订单
	} else if (editFlag === 2) {
		await salesOrderRequest.cancelSaleorderBill({
			pk: sfRes.result.pk,
			check: false,
		});
	}
}


function getDatesDifference(dateStr1, dateStr2 = common.getCurrentDate()) {
	let momentObj1 = moment(dateStr1 > 8 ? dateStr1 : '20' + dateStr1);
	let momentObj2 = moment(dateStr2);

	return momentObj1.diff(momentObj2, 'days');
}


function getAllowDeliveryExp(custRes, orderJson, purchaseJson) {
	let exp = {
		dwid: custRes.result.val,
		deliveryNum: Math.min(purchaseJson.details[0].num, orderJson.details[0].num),
		stockNum: purchaseJson.details[0].num,
		orderNum: orderJson.details[0].num,
		staffName: '总经理'
	};
	return exp;
};