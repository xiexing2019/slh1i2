'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
// const getBasicData = require('../../../data/getBasicData.js');

describe('pdf', function () {
    before(async function () {
        await common.loginDo();
        // BASICDATA = await getBasicData.getBasicID();
    });

    // http://jira.hzdlsoft.com:7082/browse/SLH-23448
    const targets = ['16401', '14493', '14494', '14495', '14496', '16402', '10124', '10125', '10126', '10128'];
    // const targets = ['16401'];
    for (const target of targets) {
        it(`SLH-23448.pdf打印纸张配置${target}`, async function () {
            const params = {
                target: target,
                paperType: common.getRandomNum(1, 4),
                printType: 3,//打印方式，1 针打，2 蓝牙打印，3 云打印
                check: true
            };;
            await common.callInterface('cs-savePdfPaperByTarget', params);

            const qlRes = await common.callInterface('cs-getPdfPaperByTarget', { target: target });
            common.isApproximatelyEqualAssert(params, qlRes);
        });

        // http://jira.hzdlsoft.com:7082/browse/SLH-23312
        // 自动化冒泡一遍没有服务端错误即可
        // 登录需要带mac地址 测试环境98:ca:33:1f:a4:8a 线上未配置
        it.skip(`SLH-23312.pdf云打印${target}`, async function () {
            const params = format.qlParamsFormat({ shopid: LOGINDATA.invid }, true);
            await common.callInterface('cs-cloudPdfPrint', { target, check: true, ...params });
        });
    };

});