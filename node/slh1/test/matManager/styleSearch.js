'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');

describe('货品查询', function () {
	this.timeout('30000');
	let styleInfo = {}; //客户信息
	const showStyleBarCodeField = 'stylecode=code;stylename=name';

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		// console.log(`styleInfo = ${JSON.stringify(styleInfo)}`);
	});

	//需要开启参数dres_auto_create_sku和style_barcode_colorsize
	//条码生成规则根据参数:style_barcode_colorsize_rule
	it('100058.显示条码功能.rankA', async function () {
		let [colorsInfo, sizesInfo, expArr] = [[], [], []];
		//获取颜色信息
		const colorNames = styleInfo.show_colorids.split(','); //名称模糊查询
		for (let i = 0, length = colorNames.length; i < length; i++) {
			let data = await getBasicData.getDictListInfo({
				typeName: 'color',
				name: colorNames[i],
			}).then((res) => common.takeWhile(res.result.dataList, (obj) => obj.name == colorNames[i])); //名称模糊查询,排除无关内容
			colorsInfo.push(...data);
		};
		//获取尺码信息
		const sizeNames = styleInfo.show_sizeids.split(',');
		for (let i = 0, length = sizeNames.length; i < length; i++) {
			let data = await getBasicData.getDictListInfo({
				typeName: 'size',
				name: sizeNames[i],
			}).then((res) => common.takeWhile(res.result.dataList, (obj) => obj.name == sizeNames[i])); //名称模糊查询,排除无关内容
			sizesInfo.push(...data);
		};

		//条码生成规则
		// let barcodeRule = await common.callInterface('ql-11601', format.qlParamsFormat({
		// 	code: 'style_barcode_colorsize_rule',
		// })).then((res) => res.dataList[0].val_name);

		let qlRes = await common.callInterface('ql-1528', format.qlParamsFormat({
			styleid: styleInfo.pk,
		}, false)); //条码查询
		let expObj = format.dataFormat(styleInfo, showStyleBarCodeField);
		colorsInfo.map((color) => {
			sizesInfo.map((size) => {
				let exp1 = Object.assign(expObj, {
					color: color.name,
					size: size.name,
					barcode: `${styleInfo.code}${color.code}${size.code}`,
				});
				expArr.push(_.cloneDeep(exp1));
			});
		});

		common.isApproximatelyArrayAssert(expArr, qlRes.dataList);
	});

});
