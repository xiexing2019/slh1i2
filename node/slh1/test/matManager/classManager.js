'use strict';

const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const inOutHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const metManager = require('../../../reqHandler/slh1/index');

describe('款号类别管理-mainLine', function () {
    this.timeout(30000);
    let parendId, sfRes;
    before('新增款号类别', async function () {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        // 查询父类别
        parendId = await metManager.slh1MetManager.queryGoodClassList({ name: '鞋' }).then(
            res => res.dataList.find(data => data.name == '鞋'));
        // 新增类别
        sfRes = await metManager.slh1MetManager.saveGoodClassFull({ 'parentid': parendId.id || '' });
    });
    after('停用款号类别', async () => {
        await metManager.slh1MetManager.disableGoodClass({ pk: sfRes.result.pk })
    });

    describe('新增款号类别', function () {
        it('查询款号类别列表', async function () {
            let qlRes = await metManager.slh1MetManager.queryGoodClassList({ name: sfRes.params.jsonparam.name }).then(
                res => res.dataList.find(data => data.id == sfRes.result.pk));
            common.isApproximatelyEqual(sfRes.params.jsonparam, qlRes);
        });
        it('查询款号类别详情', async function () {
            let qfRes = await metManager.slh1MetManager.queryGoodClassFull({ pk: sfRes.result.pk });
            common.isApproximatelyEqual(sfRes.params.jsonparam, qfRes);
        });
    });

    describe('修改款号类别', function () {
        let editGoodClass;
        before('修改款号类别', async () => {
            editGoodClass = await metManager.slh1MetManager.saveGoodClassFull({ pk: sfRes.result.pk });
        });
        it('查询款号类别列表', async function () {
            let qlRes = await metManager.slh1MetManager.queryGoodClassList({ name: editGoodClass.params.jsonparam.name }).then(
                res => res.dataList.find(data => data.id == editGoodClass.result.pk));
            common.isApproximatelyEqual(editGoodClass.params.jsonparam, qlRes);
        });
        it('查询款号类别详情', async function () {
            let qfRes = await metManager.slh1MetManager.queryGoodClassFull({ pk: sfRes.result.pk });
            common.isApproximatelyEqual(editGoodClass.params.jsonparam, qfRes);
        });
    });

});