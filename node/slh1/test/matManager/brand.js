'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const reqHandlerHelp = require('../../help/reqHandlerHelp');
const metManager = require('../../../reqHandler/slh1/index');
const basicJsonparam = require('../../help/basiceJsonparam')

describe('品牌管理-mainLine', function () {
    this.timeout('30000');
    before(async () => {
        await common.loginDo();
    });
    describe('品牌', function () {
        let newBrand;
        before(async () => {
            // 新增品牌
            newBrand = await metManager.slh1MetManager.saveBrandFull();
        });
        after(async () => {
            // 停用品牌
            await metManager.slh1MetManager.disableBrand({ pk: newBrand.result.pk })
        })
        describe('新增品牌', function () {
            it('查询品牌列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryBrandList({ name: newBrand.params.jsonparam.name }).then(
                    res => res.dataList.find(data => data.id == newBrand.result.pk));
                common.isApproximatelyEqual(newBrand.params.jsonparam, qlRes);
            });
            it('查询品牌详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryBrandFull({ pk: newBrand.result.pk });
                common.isApproximatelyEqual(newBrand.params.jsonparam, qfRes);
            });
        });

        describe('修改品牌', function () {
            let eidtBrandRes;
            before(async () => {
                // 修改品牌信息
                eidtBrandRes = await metManager.slh1MetManager.saveBrandFull({ id: newBrand.result.pk });
            });
            it('查询品牌列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryBrandList({ name: eidtBrandRes.params.jsonparam.name }).then(
                    res => res.dataList.find(data => data.id == eidtBrandRes.result.pk));
                common.isApproximatelyEqual(eidtBrandRes.params.jsonparam, qlRes);
            });
            it('查询品牌详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryBrandFull({ pk: newBrand.result.pk });
                common.isApproximatelyEqual(eidtBrandRes.params.jsonparam, qfRes);
            })
        });
    });

    describe('品牌折扣', function () {
        let discountParams, newBrand, brandData, newBrandDiscount;
        before(async function () {
            // 新增品牌
            newBrand = await metManager.slh1MetManager.saveBrandFull();
            // 查询新增的品牌的sid
            brandData = await metManager.slh1MetManager.queryBrandFull({ pk: newBrand.result.pk });
            // 品牌折扣参数
            discountParams = basicJsonparam.brandDiscountJson();
            discountParams.brandsid = brandData.sid;
            // 新增品牌折扣
            newBrandDiscount = await metManager.slh1MetManager.saveBrandDiscountFull({ jsonparam: discountParams });

        });
        describe('新增品牌折扣', function () {
            it('查询品牌折扣列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryBrandDiscountList({ brandid: brandData.sid, }).then(
                    res => res.result.dataList.find(data => data.brandsid == brandData.sid));
                expect(qlRes.brandsid).to.be.equal(brandData.sid);
            });
            it('查询品牌折扣详情', async function () {
                let qullRes = await metManager.slh1MetManager.queryBrandDiscountFull({ pk: newBrandDiscount.result.val });
                common.isApproximatelyEqual(basicJsonparam.brandDiscountExpJson(), qullRes);
            });
        });

        describe('修改品牌折扣', function () {
            let editRes;
            before(async () => {
                // 修改json
                let editDiscountParams = basicJsonparam.editBrandDiscountJson();
                editDiscountParams.brandsid = brandData.sid;
                editDiscountParams.pk = newBrandDiscount.result.val
                // 修改折扣
                editRes = await metManager.slh1MetManager.saveBrandDiscountFull({ jsonparam: editDiscountParams });
            })
            it('查询品牌折扣列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryBrandDiscountList({ brandid: brandData.sid, }).then(
                    res => res.result.dataList.find(data => data.brandsid == brandData.sid));
                expect(qlRes.brandsid).to.be.equal(brandData.sid);
            });
            it('查询品牌折扣详情', async function () {
                let qullRes = await metManager.slh1MetManager.queryBrandDiscountFull({ pk: editRes.result.val });
                common.isApproximatelyEqual(qullRes, basicJsonparam.editBrandDiscountExpJson());
            });
        });
    });
});