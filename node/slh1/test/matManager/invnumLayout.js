'use strict';
const common = require('../../../lib/common');
const basicInfoHelp = require('../../help/basicInfoHelp/basicInfoReqHandler');
const basicJson = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');

describe('库存分布', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('价值验证', function () {
		let styleRes = {};
		before(async () => {
			await common.setGlobalParam('dres_style_pricemode', 1); //价格模式 不同门店不同的价格体系
			await common.setGlobalParam('sc_dres_style_usetagprice', 0); //货品建款的价格模式 默认价格模式

			styleRes = await basicInfoHelp.editStyle({
				jsonparam: basicJson.addGoodJson()
			}); //新增货品

			let json = basicJson.purchaseJson();
			json.details[0].styleid = styleRes.pk;
			await common.editBilling(json); //新增入库单

			await common.loginDo({
				logid: '200',
			});
			styleRes.params.pk = styleRes.pk;
			styleRes.params.invid = LOGINDATA.invid;
			styleRes.params.shopid = LOGINDATA.invid;
			await basicInfoHelp.editStyle({
				jsonparam: styleRes.params
			});

			await common.editBilling(json);

		});
		after(async () => {
			// await common.setGlobalParam('dres_style_pricemode', 0); //价格模式 统一的价格体系
			// await common.setGlobalParam('invsumprice', 0); //库存核算价格 库存按进货价核算
		});
		it('100290.不同门店不同价格，相同进货价,价值按销价1核算', async function () {
			await common.setGlobalParam('dres_style_pricemode', 2); //价格模式 不同门店不同价格，相同进货价
			await common.setGlobalParam('invsumprice', 1); //库存核算价格 价值按销价1核算

			await test100286(styleRes, 'stdprice1', 2);
		});
		it('100286.不同门店不同价格,库存按进货价核算', async function () {
			await common.setGlobalParam('dres_style_pricemode', 1); //价格模式 不同门店不同的价格体系
			await common.setGlobalParam('invsumprice', 0); //库存核算价格 库存按进货价核算

			await test100286(styleRes, 'purprice', 1);
		});
		it('100287.不同门店不同价格,价值按销价1核算', async function () {
			await common.setGlobalParam('dres_style_pricemode', 1); //价格模式 不同门店不同的价格体系
			await common.setGlobalParam('invsumprice', 1); //库存核算价格 价值按销价1核算

			await test100286(styleRes, 'stdprice1', 1);
		});
		it('100288.统一的价格体系，库存按进货价核算', async function () {
			await common.setGlobalParam('dres_style_pricemode', 0); //价格模式 统一的价格体系
			await common.setGlobalParam('invsumprice', 0); //库存核算价格 库存按进货价核算

			await test100286(styleRes, 'purprice', 0);
		});
		it('100289.统一的价格体系，价值按销价1核算', async function () {
			await common.setGlobalParam('dres_style_pricemode', 0); //价格模式 统一的价格体系
			await common.setGlobalParam('invsumprice', 1); //库存核算价格 价值按销价1核算

			await test100286(styleRes, 'stdprice1', 0);
		});
	});

});

async function test100286(styleRes, priceKey, priceMode) {
	await common.loginDo();
	let qlParams = {
		classid: 0,
		styleid: styleRes.pk,
	};
	let json = _.cloneDeep(styleRes.params);
	json.pk = styleRes.pk;
	json.invid = LOGINDATA.invid;
	json.shopid = LOGINDATA.invid;
	let price = json[priceKey]; //原价
	let price1 = json[priceKey] = common.add(json[priceKey], 10); //修改价格
	await basicInfoHelp.editStyle({
		jsonparam: json
	});
	let dynamicPrice = {};
	if (priceMode == 0 || (priceKey == 'purprice' && priceMode == 2)) {
		dynamicPrice.other = price1;
	} else {
		dynamicPrice.other = price;
		dynamicPrice[LOGINDATA.invname] = price1;
	};
	// console.log(dynamicPrice);
	await checkInvMoney(qlParams, dynamicPrice, `A门店修改${priceKey}价格`);

	await common.loginDo({
		logid: '200',
	});
	let price2 = json[priceKey] = common.add(json[priceKey], 10);
	json.invid = LOGINDATA.invid;
	json.shopid = LOGINDATA.invid;
	await basicInfoHelp.editStyle({
		jsonparam: json
	});
	if (priceMode == 0 || (priceKey == 'purprice' && priceMode == 2)) {
		dynamicPrice.other = price2;
	} else {
		dynamicPrice[LOGINDATA.invname] = price2;
	};
	// console.log(dynamicPrice);
	await checkInvMoney(qlParams, dynamicPrice, `B门店修改${priceKey}价格`);

};

async function checkInvMoney(qlParams, dynamicPrice, msg) {
	qlParams.styleflag = 0;
	qlParams.pagesize = 15;
	let qlRes = await common.callInterface('ql-19904', qlParams);
	expect(qlRes.dataList.length, `库存分布明细未查询到期望数据 styleid:${qlParams.styleid}`).to.equal(1);

	let exp = 0;
	qlRes.dynamicTitle.forEach((shopName, index) => {
		let price = dynamicPrice[shopName] || dynamicPrice.other;
		exp += common.mul(price, qlRes.dataList[0].dynamicData[index]);
	});
	expect(exp, `${msg},ql-19904库存分布明细-价值错误`).to.equal(Number(qlRes.dataList[0].invmoney));
};
