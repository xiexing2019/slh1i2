'use strict';
const common = require('../../../lib/common');
const getBasicData = require('../../../data/getBasicData');

//接口文档 http://106.15.226.60:8080/showdoc/index.php?s=/2&page_id=2023 
//jira http://jira.hzdlsoft.com:7082/browse/SLH-23354
describe('图片-slh2', function () {
    this.timeout('30000');

    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
    });

    it('上传图片', async function () {
        let str = common.getRandomNumStr(8),
            docIds = '';
        for (let index = 0; index < 9; index++) {
            docIds += `${str + index},`;
        };

        await common.callInterface('sf-15118', {
            jsonparam: {
                docIds: docIds,
                pk: BASICDATA.styleidAgc001,
            },
            check: true,
        });
        const qfRes = await common.fetchMatInfo(BASICDATA.styleidAgc001);
        expect(qfRes.fileid, `qf-1511.fileid值 != sf-15118 params.docIds`).to.equal(docIds);
    });



});