'use strict';

const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const inOutHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const metManager = require('../../../reqHandler/slh1/index');

describe('货品管理-mainLine', function () {
    before(async function () {
        this.timeout(30000);
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
    })

    describe('批量启用停用货品', function () {
        let goodList;
        before(async function () {
            goodList = await metManager.slh1MetManager.queryGoodList({ flag: 0, pagesize: 2 }).then(
                res => res.dataList.map(function (data) { if (data.code) { return data } }));
            // 停用货品
            await metManager.slh1MetManager.disableGoods({ pk: `${goodList[0].id},${goodList[1].id}` });
        });
        describe('批量停用货品', function () {
            it('查询货品列表', async function () {
                for (let i of goodList) {
                    let qlRes = await metManager.slh1MetManager.queryGoodList({ stylename: i.code }).then(
                        res => res.dataList.find(data => data.id));
                    expect(Number(qlRes.delflag), `停用失败，停用后，款号delflag 任然为0`).to.equal(1);
                };
            });
            it('查询货品详情', async function () {
                for (let i of goodList) {
                    let qfRes = await metManager.slh1MetManager.queryGoodFull({ pk: i.id });
                    expect(Number(qfRes.delflag), `停用失败，停用后，款号delflag 任然为0`).to.equal(1);
                };
            });
        });
        describe('批量启用货品', function () {
            before(async () => {
                // 批量启用多个货品
                await metManager.slh1MetManager.enableGoods({ pk: `${goodList[0].id},${goodList[1].id}` });
            })
            it('查询货品列表', async function () {
                for (let i of goodList) {
                    let qlRes = await metManager.slh1MetManager.queryGoodList({ stylename: i.code }).then(
                        res => res.dataList.find(data => data.id));
                    expect(Number(qlRes.delflag), `启用用失败，启用后，款号delflag 任然为1`).to.equal(0);
                };
            });
            it('查询货品详情', async function () {
                for (let i of goodList) {
                    let qfRes = await metManager.slh1MetManager.queryGoodFull({ pk: i.id });
                    expect(Number(qfRes.delflag), `启用失败，启用后，款号delflag 任然为1`).to.equal(0);
                };
            });
        });
    });

    describe('单个启用停用货品', function () {
        this.timeout(30000);
        let goodList;
        before(async function () {
            // 查询款号列表
            goodList = await metManager.slh1MetManager.queryGoodList({ flag: 0, pagesize: 1 }).then(
                res => res.dataList.find(data => data.id));
        });
        describe('停用单个货品', function () {
            let disableGood
            before(async () => {
                // 停用款号
                disableGood = await metManager.slh1MetManager.disableGood({ pk: goodList.id });
            });
            it('查询货品列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryGoodList({ name: goodList.name }).then(
                    res => res.dataList.find(data => data.name == goodList.name));
                expect(qlRes.delflag, `停用失败${qlRes}`).to.equal('1');
            });
            it('查询货品详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryGoodFull({ pk: goodList.id });
                expect(qfRes.delflag, `停用失败${qfRes}`).to.equal('1');
            });
        });
        describe('启用单个货品', function () {
            let enableGood;
            before(async () => {
                // 启用款号
                enableGood = await metManager.slh1MetManager.enableGood({ pk: goodList.id });
            })
            it('查询货品列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryGoodList({ name: goodList.name }).then(
                    res => res.dataList.find(data => data.name == goodList.name)
                );
                expect(qlRes.delflag, `停用失败${qlRes}`).to.equal('0');
            });
            it('查询货品详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryGoodFull({ pk: goodList.id });
                expect(qfRes.delflag, `停用失败${qfRes}`).to.equal('0');
            });
        });
    });
});