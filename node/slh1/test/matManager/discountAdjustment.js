const common = require('../../../lib/common');
const getBasicData = require('../../../data/getBasicData');
const format = require('../../../data/format');
const reqHandler = require('../../help/reqHandlerHelp');
const basicJson = require('../../help/basiceJsonparam');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

//http://jira.hzdlsoft.com:7082/browse/SLH-23914
//支持款号折扣批量调整，可以按某个品牌，某个上架时间范围，按季节，特定折扣批量设置成某个折扣
describe('折扣批量调整', function () {
    this.timeout('30000');
    //待调整商品列表,款号id(),调整结果
    let dresList, styleIds, adjustRes, adjustFullExp;

    before('新增调价', async () => {
        await common.loginDo({});
        BASICDATA = await getBasicData.getBasicID();

        // let json = basicJson.addGoodJson();
        // json.special = 1; //特价商品
        // let goodRes = await basicReqHandler.editStyle({jsonparam: json});
        // console.log(`goodRes=${JSON.stringify(goodRes)}`);

        //货品查询 获取调价商品列表
        dresList = await common.callInterface('ql-15110', format.qlParamsFormat({ marketdate1: common.getDateString([0, 0, -1]), marketdate2: common.getCurrentDate(), flag: 0 }))
            .then((res) => res.dataList.slice(0, 3));
        styleIds = dresList.map((data) => data.id);
        //批量调整折扣
        adjustRes = await diccountAdjust({ pk: styleIds.join(','), batchDiscount: common.getRandomNum(0.1, 1, 1), rem: 'test' });
        // console.log(`\nadjustRes=${JSON.stringify(adjustRes)}`);
    });

    it('调价详情查询', async function () {
        adjustFullExp = getAdjustFullExp({ dresList, curDiscount: adjustRes.params.batchDiscount });
        const qfRes = await common.callInterface('ql-15121', format.qlParamsFormat({ id: adjustRes.result.val }));
        common.isApproximatelyArrayAssert(adjustFullExp, qfRes.dataList);
    });

    it('调价记录查询', async function () {
        const qlData = await common.callInterface('ql-15120', format.qlParamsFormat({ optime1: common.getCurrentDate(), optime2: common.getCurrentDate() }))
            .then((res) => { return res.dataList.find((data) => data.id = adjustRes.result.val) });
        common.isApproximatelyEqualAssert(getAdjustListExp({ dresList, adjustRes }), qlData);
    });

    it('商品折扣验证', async function () {
        const exp = getDresFullExp({ dresList, curDiscount: adjustRes.params.batchDiscount });
        for (let index = 0; index < styleIds.length; index++) {
            const styleInfo = await common.fetchMatInfo(styleIds[index]);
            common.isApproximatelyEqualAssert(exp[index], styleInfo);
        };
    });

    describe('撤销调价', function () {
        before(async function () {
            await reqHandler.csIFCHandler({ interfaceid: 'cs-revokeChangePrice', pk: adjustRes.result.val });
        });
        it('商品折扣验证', async function () {
            let seasonList = await common.callInterface('ql-1526', {});
            for (let index = 0; index < styleIds.length; index++) {
                const styleInfo = await common.fetchMatInfo(styleIds[index]);
                let exp = dresList.find((data) => data.id == styleIds[index]);
                let season = seasonList.dataList.find(data => data.name === exp.season);
                //季节为空
                exp = Object.assign(exp, { season: season === undefined ? 0 : season.sid, show_isprocess: exp.isprocess, pk: exp.id, show_dwid: exp.provideridname, show_locationid: exp.locationid });
                //season=春季，season=1，show_season
                common.isApproximatelyEqualAssert(exp, styleInfo, ['isprocess', 'optime', 'locationid']);
            };
        });

        it('调价记录查询', async function () {
            const qlRes = await common.callInterface('ql-15120', format.qlParamsFormat({ optime1: common.getCurrentDate(), optime2: common.getCurrentDate() }));
            expect(qlRes.dataList.find((data) => data.id == adjustRes.result.val).delflag).to.equal('1');
        });

        it('查询调价记录详情', async function () {
            const qfRes = await common.callInterface('ql-15121', format.qlParamsFormat({ id: adjustRes.result.val }));
            common.isApproximatelyArrayAssert(adjustFullExp, qfRes.dataList);
        });
    });

    describe('异常验证', function () {
        it('折扣大于1', async function () {
            const res = await diccountAdjust({ pk: styleIds.join(','), batchDiscount: 1.1, check: false });
            expect(res.result).to.includes({ msgId: '折扣必须大于0且小于等于1' });
        });
        it('折扣小于1', async function () {
            const res = await diccountAdjust({ pk: styleIds.join(','), batchDiscount: 0, check: false });
            expect(res.result).to.includes({ msgId: '折扣必须大于0且小于等于1' });
        });
        it('折扣非数字', async function () {
            const res = await diccountAdjust({ pk: styleIds.join(','), batchDiscount: 'a', check: false });
            expect(res.result).to.includes({ msgId: 'error_field_numberic_format' });
        });
        it('折扣为空', async function () {
            const res = await diccountAdjust({ pk: styleIds.join(','), batchDiscount: '', check: false });
            expect(res.result).to.includes({ msgId: '折扣必须大于0且小于等于1' });
        });
    });

    describe('根据查询条件调折扣', function () {
        let styleAgc002Bef, styleList, adjustRes;
        before(async () => {
            let qlParams = {
                brandid: 1,
                dwid: BASICDATA.dwidVell,
                season: 1,//代表春季,
                discount: 1,
                classid: BASICDATA.classIds['鞋'],
                marketdate1: common.getDateString([0, 0, -1]),
                marketdate2: common.getCurrentDate(),
                flag: 0,//是否停用 0为否
            };
            styleList = await common.callInterface('ql-15110', format.qlParamsFormat(qlParams)).then((res) => res.dataList);;
            if (styleList.length === 0) {
                await basicReqHandler.editStyle({ jsonparam: basicJson.addGoodJsonFull() });//新增款号
                styleList = await common.callInterface('ql-15110', format.qlParamsFormat(qlParams)).then((res) => res.dataList);;
            };
            // console.log(`styleList=${JSON.stringify(styleList.dataList)}`);
            styleAgc002Bef = await common.fetchMatInfo(BASICDATA.styleidAgc002);
            adjustRes = await diccountAdjust({ batchDiscount: common.getRandomNum(0.1, 1, 2), ...qlParams });
        });
        it('符合查询条件的款号折扣验证', async function () {
            const exp = getDresFullExp({ dresList: styleList, curDiscount: adjustRes.params.batchDiscount });
            for (let index = 0; index < styleList.length; index++) {
                const styleInfo = await common.fetchMatInfo(styleList[index].id);
                common.isApproximatelyEqualAssert(exp[index], styleInfo);
            };
        });
        it('不符合查询条件的款号验证', async function () {
            let styleAgc002Aft = await common.fetchMatInfo(BASICDATA.styleidAgc002);
            common.isApproximatelyEqualAssert(styleAgc002Bef, styleAgc002Aft);
        });
    });
});

/**
 * 新增批量调整折扣
 * @param {object} params 
 * @param {string} params.pk styleIds
 * @param {string} params.batchDiscount 折扣
 */
async function diccountAdjust(params) {
    if (!params.hasOwnProperty('check')) params.check = true;
    return common.callInterface2({ interfaceid: 'sf-15120-1', adjustType: 1, action: 'add', ...params });
};

/**
 * 拼接调价后,商品详情期望值
 * @param {object} dresList 
 * @param {string} curDiscount
 */
function getDresFullExp({ dresList, curDiscount }) {
    const mapField = 'id;code;name;stdprice1;stdprice2;discount';
    let exp = dresList.map((data) => {
        let _exp = format.dataFormat(data, mapField);
        if (curDiscount != undefined) _exp.discount = curDiscount;
        return _exp;
    });
    return exp;
};

/**
 * 拼接调价后,调价记录款号明细 期望值
 * @param {object} dresList 
 * @param {object} adjustRes 
 */
function getAdjustFullExp({ dresList, curDiscount }) {
    const mapField = 'code;name;discount';
    const priceKeys = ['stdprice1', 'stdprice2', 'stdprice3', 'stdprice4', 'stdprice5'];
    let exp = dresList.map((data) => {
        let _exp = format.dataFormat(data, mapField);
        _exp.newdiscount = curDiscount;
        priceKeys.forEach(key => {
            _exp[key] = -1;
            _exp[`new${key}`] = -1;
        });
        return _exp;
    });
    return exp;
};

/**
 * 拼接调价后,款号调价记录-查询列表 期望值
 * @param {object} dresList 
 * @param {object} adjustRes 
 */
function getAdjustListExp({ dresList, adjustRes }) {
    const styleNames = dresList.map((data) => `${data.code}-${data.name}`).join(',');
    let exp = { optime: common.getCurrentTime(), id: adjustRes.result.val, batchmul: 0, stylenames: styleNames, adjustType: '折扣', dynamicData: ["0", "0"], delflag: 0, batchadd: 0, batchDiscount: adjustRes.params.batchDiscount };
    return exp;
};