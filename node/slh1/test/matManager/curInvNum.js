'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

//ql-1932
describe('当前库存', function () {
	this.timeout('30000');
	let [styleInfo, colorInfo, sizeInfo] = [{}, {}, {}]; //款号信息
	const mapDataMain = 'propdresStyleBrandName=show_brandid;locationName=show_locationid;propdresStyleDwxxName=show_dwid;inventoryName:show_shopid;classname:show_classid;propdresStyleMarketdate=marketdate;invid=shopid;styleid=pk;propdresStyleName=name;propdresStyleCode=code';

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		// console.log(`styleInfo = ${JSON.stringify(styleInfo)}`);
		colorInfo = await getBasicData.getDictListInfo({
			typeName: 'color',
			name: '白色',
		}).then((res) => common.takeWhile(res.result.dataList, (obj) => obj.name == '白色')[0]); //名称模糊查询,排除无关内容

		sizeInfo = await getBasicData.getDictListInfo({
			typeName: 'size',
			name: 'L',
		}).then((res) => common.takeWhile(res.result.dataList, (obj) => obj.name == 'L')[0]); //名称模糊查询,排除无关内容
	});

	it('100001.查询.rankA', async function () {
		// let json = format.jsonparamFormat(basiceJsonparam.salesJson());
		let param = format.qlParamsFormat({
			concatPropdresStyleCodePropdresStyleName: styleInfo.name,
			invid: LOGINDATA.invid,
			marketdate1: styleInfo.marketdate,
			marketdate2: styleInfo.marketdate,
			// propdresColorid: json.details[0],
			// propdresSizeid: styleInfo.sizeids.split(',')[0],
			propdresStyleBrandid: styleInfo.brandid,
			propdresStyleClassid: styleInfo.classid,
			propdresStyleDwid: styleInfo.dwid,
			propdresStyleFlag: 0, //是否停用
			propdresStyleSeason: styleInfo.season,
			propdresStyleid: styleInfo.pk,
		});
		let qlRes = await common.callInterface('ql-1932', param); //货品管理-当前库存
		let exp = format.dataFormat(styleInfo, mapDataMain);
		qlRes.dataList.map((obj) => {
			common.isApproximatelyEqualAssert(exp, obj);
		});
	});

	it.skip('100167.季节查询-做完单独查询再来看', async function () {
		let param = format.qlParamsFormat({
			propdresStyleSeason: 0, //空季节
			pagesize: 30,
		});
		let qlRes = await common.callInterface('ql-1932', param);
		qlRes.dataList.map((obj) => {
			//
		});
	});

	it('100253.颜色查询', async function () {
		let param = format.qlParamsFormat({
			propdresColorid: colorInfo.sid,
			pagesize: 30,
		});
		let qlRes = await common.callInterface('ql-1932', param);
		qlRes.dataList.map((obj) => {
			common.isFieldsEqualAssert(obj, colorInfo, 'colorid=sid;propdresColorName=name;propdresColorCode=code');
		});
	});

	//可用库存=当前库存+在途+采购未到-订货未发，若为负数则为0
	//查询条件增加一项：仅可用库存，仅显示可用库存大于0的记录
	//onlyAvailStockNum 只有传或者不传2种情况
	describe('SLH-24013.库存查询增加可用库存', function () {
		it('可用库存数检查', async function () {
			const qlRes = await common.callInterface('ql-1932', format.qlParamsFormat({ curpageno: 1, pagesize: 100 }, true));
			qlRes.dataList.forEach(data => {
				const availStockNum = Math.max(Number(data.invnum) + Number(data.onroadnum) + Number(data.purednum) - Number(data.ordernum), 0);
				expect(Number(data.availStockNum), `${JSON.stringify(data)}\n可用库存不正确:availStockNum(${data.availStockNum}) != invnum(${data.invnum})+onroadnum(${data.onroadnum})+purednum(${data.purednum})-ordernum(${data.ordernum})`).to.equal(availStockNum);
			});
		});
		it('仅可用库存查询-是', async function () {
			const qlRes = await common.callInterface('ql-1932', format.qlParamsFormat({ curpageno: 1, pagesize: 100, onlyAvailStockNum: 1 }, true));
			qlRes.dataList.forEach(data => {
				expect(Number(data.availStockNum), `${JSON.stringify(data)}\n查询结果错误`).to.above(0);
			});
		});
	});

	describe.skip('库存调整-要补', function () {
		before(async () => {

		});
		it('100090.rankA', async function () {
			let sfRes = await basicReqHandler.invNumChange({
				jsonparam: {
					propdresStyleid: styleInfo.pk,
					propdresColorid: colorInfo.sid,
					propdresSizeid: sizeInfo.sid,
					recvnum: common.getRandomNum(100, 1000),
				},
				check: false,
			});
			console.log(`sfRes = ${JSON.stringify(sfRes)}`);
		});
	});

	describe('权限检查', function () {
		before(async () => {

		});
		after(async () => {
			await common.loginDo();
			await common.setGlobalParam('inv_allow_search_invnum', 1);
		});
		it('100100.1', async function () {
			await common.setGlobalParam('inv_allow_search_invnum', 0); //门店库存是否允许跨门店查询 不允许门店相互查询库存

			await common.loginDo({
				logid: '004',
				pass: '000000',
			});
			let param = format.qlParamsFormat({
				search_list: 0,
				invid: BASICDATA.shopidZzd,
			});
			let qlRes = await common.callInterface('ql-1932', param);
			expect(qlRes.count).to.equal('0');

			await common.loginDo({
				logid: '005',
				pass: '000000',
			});
			qlRes = await common.callInterface('ql-1932', param);
			expect(qlRes.count).to.equal('0');
		});

	});

	//8.52 当前库存-入库时间 http://jira.hzdlsoft.com:7082/browse/SLH-23122
	//最后一次入库日期计算规则：
	// 1.比较款号sku入库的单据日期,取最大的单据日期作为最后入库日期
	// 2.款号sku在某一天拿货退货之和大于0,则认为这一天有入库,作为入库日期比较，
	// 例如:款号sku在2018-06-07 拿货10件退货11件,入库数量之和为-1,2018-06-07 无入库
	describe('SLH-23122.当前库存-入库时间验证', function () {
		let styleId, purJson, qlParams, sfRes, origLastPureTimes = {},
			lastPureTimes = {};
		before(async () => {
			//当前库存-入库时间由参数inv_showSpuLastPrueTime控制
			await common.setGlobalParam('inv_showSpuLastPrueTime', 1);

			//新增货品
			const styleRes = await basicInfoReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			});
			styleId = styleRes.pk;

			purJson = basiceJsonparam.purchaseJson();
			purJson.details.forEach((detail) => detail.styleid = styleId);

			qlParams = format.qlParamsFormat({
				propdresStyleid: styleId,
				check: true
			});
		});
		// after(async () => {
		// 	await common.setGlobalParam('inv_showSpuLastPrueTime', 0);
		// });
		it('1.未入库-当前库存无记录', async function () {
			const qlRes = await common.callInterface('ql-1932', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});
		it('2.新增采购入库--mainLine', async function () {
			sfRes = await common.editBilling(purJson);
			const exp = getLastPureTimeByBill(sfRes.params);

			await common.delay(200); //入库时间异步更新 需要等待
			const qlRes = await common.callInterface('ql-1932', qlParams);
			lastPureTimes = getLastPureTimeByList(qlRes.dataList); //入库时间
			common.isApproximatelyEqualAssert(exp, lastPureTimes);
		});
		it('3.修改入库单日期', async function () {
			sfRes.params.pk = sfRes.result.pk;
			sfRes.params.action = 'edit';
			sfRes.params.prodate = common.getDateString([0, 0, -1]); //改为昨天
			sfRes = await common.editBilling(sfRes.params); //修改单据
			const exp = Object.assign({}, lastPureTimes, getLastPureTimeByBill(sfRes.params));

			await common.delay(2000); //入库时间异步更新 需要等待
			const qlRes = await common.callInterface('ql-1932', qlParams);
			origLastPureTimes = getLastPureTimeByList(qlRes.dataList);
			common.isApproximatelyEqualAssert(exp, origLastPureTimes);
		});
		it('4.退货', async function () {
			let backJson = _.cloneDeep(purJson);
			backJson.details.forEach((detail) => detail.num = -10);
			backJson.details[0].sizeid = 'XL'; //修改尺码
			sfRes = await common.editBilling(backJson);

			const qlRes = await common.callInterface('ql-1932', qlParams);
			lastPureTimes = getLastPureTimeByList(qlRes.dataList);
			common.isApproximatelyEqualAssert(origLastPureTimes, lastPureTimes); //负库存不更新
		});
		it('5.进货-使拿货退货和大于0', async function () {
			let backJson = _.cloneDeep(purJson);
			backJson.details.forEach((detail) => detail.num = 20);
			backJson.details[0].sizeid = 'XL'; //修改尺码
			sfRes = await common.editBilling(backJson);
			await common.delay(1000); //当前库存可能还未更新，加延迟
			const exp = Object.assign(lastPureTimes, getLastPureTimeByBill(sfRes.params));
			const qlRes = await common.callInterface('ql-1932', qlParams);
			lastPureTimes = getLastPureTimeByList(qlRes.dataList);
			common.isApproximatelyEqualAssert(exp, lastPureTimes);
		});
		it('6.作废入库单', async function () {
			await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancel-pured-bill',
				pk: sfRes.result.pk,
				check: true, //断言作废成功
				errorMsg: '作废入库单失败',
			});
			await common.delay(1000); //当前库存可能还未更新，加延迟
			const qlRes = await common.callInterface('ql-1932', qlParams);
			lastPureTimes = getLastPureTimeByList(qlRes.dataList);
			common.isApproximatelyEqualAssert(origLastPureTimes, lastPureTimes);
		});
	});
});

function getLastPureTimeByBill(billInfo) {
	let exp = {};
	billInfo.details.forEach(element => {
		//款号id-颜色id-尺码id-门店id
		let key = `${element.styleid}-${element.colorid}-${element.sizeid}-${billInfo.invid}`;
		exp[key] = billInfo.prodate;
	});
	return exp;
};

function getLastPureTimeByList(curInvList) {
	let exp = {};
	curInvList.forEach(element => {
		//款号id-颜色id-尺码id-门店id
		let key = `${element.styleid}-${element.colorid}-${element.sizeid}-${element.invid}`;
		exp[key] = element.lastPureTime;
	});
	return exp;
};
