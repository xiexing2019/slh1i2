'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/**
 * 超储统计/缺货统计
 *
 * 基本思路为新增货品-入库100件-修改最小/最大库存验证
 * 验证最小库存=0时,修改库存为负数验证
 **/
describe('超储统计/缺货统计', function () {
	this.timeout('30000');
	let [styleInfo, qlParams, styleSfRes] = [{}, {}, {}]; //款号信息,查询参数

	//根据款号信息qf-1511结果拼接
	const searchMapField = 'styleid=pk;stylecodename=name;marketdate1=marketdate;marketdate2=marketdate';
	const mapFieldMain = 'id=pk;style_code=code;style_name=name;style_marketdate=marketdate;style_maxstock=maxstock;style_minstock=minstock';

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		styleSfRes = await editStyleStockInfo(basiceJsonparam.addGoodJson(), [0, 0]); //新增货品
		styleInfo = await common.fetchMatInfo(styleSfRes.pk); //获取货品信息
		styleInfo.pk = styleSfRes.pk; //修改货品信息时,需要用款号的门店id

		qlParams = format.qlParamsFormat(format.dataFormat(styleInfo, searchMapField));

		let jsonparam = basiceJsonparam.purchaseJson();
		jsonparam.details[0].styleid = styleSfRes.pk;
		jsonparam.details[0].num = 100;
		await common.editBilling(jsonparam); //采购入库 库存100
	});

	describe('超储统计', function () {
		it('100079.最大库存为0不计入超储统计', async function () {
			await editStyleStockInfo(styleInfo, [0, 0]);
			let qlRes = await common.callInterface('ql-1938', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});

		it('100080.库存>最大库存', async function () {
			await editStyleStockInfo(styleInfo, [0, 30]);

			let params = {
				'ql-1938': qlParams, //货品管理-更多-超储统计
				'ql-1932': {
					propdresStyleid: styleInfo.pk,
					search_list: 0, //取汇总值
				}, //货品管理-当前库存
			};
			let qlRes = await common.getResults(params);
			let exp = format.dataFormat(styleInfo, mapFieldMain);
			exp.invnum = qlRes['ql-1932'].sumrow.invnum; //库存
			exp.largernum = common.sub(exp.invnum, exp.style_maxstock); //超储数
			common.isApproximatelyEqualAssert(exp, qlRes['ql-1938'].dataList[0]);
		});

		it('100081.库存=最大库存', async function () {
			await editStyleStockInfo(styleInfo, [0, 100]);
			let qlRes = await common.callInterface('ql-1938', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});
	});

	describe('缺货统计', function () {
		it('100087.库存<最小库存', async function () {
			await editStyleStockInfo(styleInfo, [120, 200]);

			let params = {
				'ql-1939': qlParams, //货品管理-更多-缺货统计
				'ql-1932': {
					propdresStyleid: styleInfo.pk,
					search_list: 0, //取汇总值
				}, //货品管理-当前库存
			};
			let qlRes = await common.getResults(params);
			let exp = format.dataFormat(styleInfo, mapFieldMain);
			exp.invnum = qlRes['ql-1932'].sumrow.invnum; //库存
			exp.lessnum = common.sub(exp.style_minstock, exp.invnum); //缺货数
			common.isApproximatelyEqualAssert(exp, qlRes['ql-1939'].dataList[0]);
		});

		it('100088.库存=最小库存', async function () {
			await editStyleStockInfo(styleInfo, [100, 200]);
			let qlRes = await common.callInterface('ql-1939', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});

		it('100089.最小库存<库存<最大库存', async function () {
			await editStyleStockInfo(styleInfo, [90, 120]);
			let qlRes = await common.callInterface('ql-1938', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});

		it('100086.最小库存为0不计入缺货统计', async function () {
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.details[0].styleid = styleSfRes.pk;
			jsonparam.details[0].num = 110;
			await common.editBilling(jsonparam); //销售开单 库存-10

			await editStyleStockInfo(styleInfo, [0, 200]);
			let qlRes = await common.callInterface('ql-1939', qlParams);
			expect(qlRes.dataList.length).to.equal(0);
		});
	});

	describe('', function () {
		it('100040.最大库存<最小库存', async function () {

		});
	});

});

async function editStyleStockInfo(styleInfo, [minStock, maxStock]) {
	[styleInfo.minstock, styleInfo.maxstock] = [minStock, maxStock];
	let sfRes = await basicReqHandler.editStyle({
		jsonparam: styleInfo,
	});
	return sfRes;
};
