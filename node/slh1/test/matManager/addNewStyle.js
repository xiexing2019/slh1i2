'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJsonparam = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const inOutHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const metManager = require('../../../reqHandler/slh1/index');

describe('新增货品', function () {
	this.timeout('60000');

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-19480
	//款号中显示的颜色尺码为sid
	//经wky确认,根据parenttypeid排序,parenttypeid相同时没有排序
	describe('SLH-19480.款号颜色尺码排序验证', function () {
		let styleInfo;
		const getDcitParenttypeid = (dataList) => {
			const info = {};
			dataList.forEach(data => info[data.sid] = data.parenttypeid);
			return info;
		},
			dictSortCheck = (dictExp, dictActual, errMsg) => {
				const actual = dictActual.map((data) => {
					if (dictExp.hasOwnProperty(data)) {
						return dictExp[data];
					} else {
						throw new Error(`字典${Object.keys(dictExp)}中未找到${data}`);
					};
				});
				expect(actual, `${errMsg}\nexp:${JSON.stringify(dictExp)}\nactual:${dictActual}\n`).to.eql(_.cloneDeep(actual).sort());
			};
		before(async function () {
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		});
		it('颜色', async function () {
			const dictRes = await basicInfoReqHandler.getDictList({
				typeid: 601
			});
			const dictData = getDcitParenttypeid(dictRes.result.dataList);
			dictSortCheck(dictData, styleInfo.colorids.split(','), '款号颜色排序错误');
		});
		it('尺码', async function () {
			const dictRes = await basicInfoReqHandler.getDictList({
				typeid: 605
			});
			const dictData = getDcitParenttypeid(dictRes.result.dataList);
			dictSortCheck(dictData, styleInfo.sizeids.split(','), '款号尺码排序错误');
		});
	});

	describe('新增货品-mainLine', function () {
		//this.retries(2);
		let basicGood, basicData, sfRes, qfRes, qlRes;
		before(async () => {
			basicGood = newStyleCode(basiceJsonparam.addGoodJson());
		})
		after(async () => {
			await common.setGlobalParam('style_auto_gencode', 0); //自动生成款号关闭
		})
		//名称，颜色，尺码必填字段由客户端控制
		it('100023.1.必填信息验证', async function () {
			TESTCASE = {
				describe: "新增货品款号字段验证，受参数控制",
				expect: "不传必填字段，保存一代报服务端错误二代报款号不能为空",
			}
			await common.setGlobalParam('style_auto_gencode', 0); //自动生成款号关闭
			let basicStyle = newStyleCode(basiceJsonparam.addGoodJson());
			delete basicStyle.code;
			let sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicStyle,
				check: false,
			});
			expect(sfRes.result.error, `新增款号成功${JSON.stringify(sfRes.params)}`).to.includes(USEECINTERFACE == 1 ? '服务端错误' : '款号不能为空');
		});

		it('100024.新增货品-全部信息', async function () {
			basicData = await common.callInterface('cs-getBaseDataByLogin', {});
			//console.log(`basicData.sql_gradeid_list : ${JSON.stringify(basicData.sql_gradeid_list[1].id)}`);
			await common.setGlobalParam("dres_style_print_label", 1); //扩展信息（安全类别，执行标准，洗涤说明）
			basicGood.brandid = basicData.dict_brand[0].id; //品牌的id
			basicGood.season = basicData.sql_matseason[1].id; //第0个为空，取第一个
			basicGood.classid = basicData.matcls[0].id; //类别
			basicGood.uselastprice = 1; //是否启用上次价  是
			basicGood.special = 1; //是否特价  是
			basicGood.dwid = BASICDATA.dwidVell;
			basicGood.maxstock = 5; //最大库存
			basicGood.minstock = 1; //最小库存
			basicGood.respopid = LOGINDATA.id;
			// basicGood.purpricedetals.push({
			// 	dwid: BASICDATA.dwidRt,
			// 	price: 110,
			// })
			//	basicGood.unit = ; //计量单位
			basicGood.packnum = "10"; //装箱数
			basicGood.lining = "棉麻"; //面料

			basicGood.execstdid = basicData.sql_execstdid_list[1].id; //执行标准
			basicGood.safetycatid = basicData.sql_safetycatid_list[1].id;
			basicGood.gradeid = basicData.sql_gradeid_list[1].id; //等级sql_gradeid_list
			basicGood.washingrem = basicData.sql_washingrem_list[1].id; //洗涤说明
			basicGood.rem = "新增货品备注";
			sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicGood,
			});
			qfRes = await common.fetchMatInfo(sfRes.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes);

		});
		//依赖上条用例
		it.skip('100024.2.货品查询界面字段验证ql-15110', async function () {
			qlRes = await common.callInterface('ql-15110', format.qlParamsFormat({}));
			let exp = {
				provideridname: "Vell",
				code: qfRes.code,
				name: qfRes.name,
				brandid: qfRes.brandid,
				delflag: '0',
				classid: basicData.matcls[0].id, //类别id
				purprice: qfRes.purprice,
				stdprice1: qfRes.stdprice1,
				stdprice2: qfRes.stdprice2,
				stdprice3: qfRes.stdprice3,
				invnum: '0',
				rem: qfRes.rem,
				optime: qfRes.optime || sfRes.optime, //二代qf没有返回optime
				opname: LOGINDATA.name,
				//delopname: //厂商源款
				marketdate: qfRes.marketdate,
			};
			//console.log(`qlRes.dataList[0] : ${JSON.stringify(qlRes.dataList[0])}`);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		})
		//依赖上条用例
		it('查询条件验证', async function () {
			let params = format.qlParamsFormat({
				dwid: BASICDATA.dwidVell,
				stylename: basicGood.code || basicGood.name,
				marketdate1: qfRes.marketdate,
				marketdate2: qfRes.marketdate,
				colorid: BASICDATA.coloridBaiSe,
				sizeid: BASICDATA.sizeidM,
				respopid: basicGood.respopid,
				brandid: basicGood.brandid,
				flag: 0,
				classid: basicData.matcls[0].id,
				season: basicData.sql_matseason[1].id,
			});
			qlRes = await common.callInterface('ql-15110', params);
			expect(qlRes.count, `查询结果出错 count=${qlRes.count}!=1`).to.equal('1');
			common.isApproximatelyEqualAssert({
				code: basicGood.code,
				name: basicGood.name,
			}, qlRes.dataList[0]);
		})
		// it('停用货品', async function () {
		// 	let params = format.qlParamsFormat({
		// 		stylename: 'newname',
		// 		flag: 0, //是否作废
		// 	});
		// 	qlRes = await common.callInterface('ql-15110', params);
		// 	let csStyleCode = qlRes.dataList[0].code; //
		// 	let csParams = format.qlParamsFormat({
		// 		interfaceid: 'cs-1511-33',
		// 		pk: qlRes.dataList[0].id,
		// 	})
		// 	let csStyleRes = await reqHandler.csIFCHandler(csParams); //款号停用
		// 	csStyleCode = csStyleCode + '_' + common.getCurrentDate(); //款号停用后追加当前日期，防止有相同款号的情况，导致下面是否作废查询出问题
		// 	let qlParams = format.qlParamsFormat({
		// 		stylename: csStyleCode,
		// 	});
		// 	qlRes = await common.callInterface('ql-15110', qlParams);
		// 	expect(Number(qlRes.dataList[0].delflag), `停用后，款号delflag仍为0`).to.equal(1);
		// 	qlParams.flag = 0; //是否作废 否
		// 	qlRes = await common.callInterface('ql-15110', qlParams);
		// 	expect(Number(qlRes.count), `货品查询是否作废查询失效`).to.equal(0);
		// })
		// it('批量停用-启用货品-new', async function () {
		// 	// 1、查询启用的货品
		// 	qlRes = await metManager.metManager.qlStyles({
		// 		stylename: 'newname',
		// 		flag: 0 //是否作废
		// 	}); // 货品查询
		// 	let csStyleCodeList = [], ids = [], i = 0;
		// 	while (i < 5) {
		// 		csStyleCodeList.push(qlRes.dataList[i].code)
		// 		ids.push(qlRes.dataList[i].id)
		// 		i++
		// 	};
		it('修改款号', async function () {
			basicGood.code = basicGood.code + "edit";
			basicGood.name = basicGood.name + "edit";
			basicGood.marketdate = common.getDateString([0, 0, -1]);
			basicGood.colorids = `${BASICDATA.coloridTuoSe}`;
			basicGood.sizeids = `${BASICDATA.sizeidM},${BASICDATA.sizeidXL}`;
			basicGood.purprice = 105; //采购价
			basicGood.stdprice1 = 205; //销售价1
			basicGood.stdprice2 = 185;
			basicGood.stdprice3 = 165;
			basicGood.stdprice4 = 145;
			basicGood.isallowreturn = 0; //是否允许退货 否
			basicGood.discount = 0.9;
			basicGood.brandid = basicData.dict_brand[1].id; //品牌的id
			basicGood.season = basicData.sql_matseason[2].id; //第0个为空，取第一个
			basicGood.classid = basicData.matcls[1].id; //类别
			basicGood.uselastprice = 0; //是否启用上次价  否
			basicGood.special = 0; //是否特价  否
			basicGood.dwid = BASICDATA.dwidRt;
			basicGood.maxstock = 4; //最大库存
			basicGood.minstock = 0; //最小库存
			basicGood.respopid = LOGINDATA.id;
			// basicGood.purpricedetals.push({
			// 	dwid: BASICDATA.dwidRt,
			// 	price: 110,
			// })
			basicGood.unit = "2"; //计量单位
			basicGood.packnum = "5"; //装箱数
			basicGood.lining = "棉麻" + "edit"; //面料 标准
			basicGood.execstdid = basicData.sql_execstdid_list[0].id; //执行标准
			basicGood.safetycatid = basicData.sql_safetycatid_list[0].id; //安全类别
			basicGood.gradeid = basicData.sql_gradeid_list[0].id; //等级
			basicGood.washingrem = basicData.sql_washingrem_list[0].id; //洗涤说明
			basicGood.rem = "修改货品备注";
			basicGood.pk = sfRes.pk;
			sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicGood,
			});
			qfRes = await common.fetchMatInfo(sfRes.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes)
		});
		it('修改货品-相同款号', async function () {
			const dresRes = await basicInfoReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			});
			dresRes.params.code = sfRes.params.code;
			dresRes.params.pk = dresRes.result.val;
			const editRes = await basicInfoReqHandler.editStyle({
				jsonparam: dresRes.params,
				check: false
			});
			expect(editRes.result).to.includes({
				msgId: 'stylecode_already_exist',
				error: `设置或生成的款号${sfRes.params.code}，已有相同款号存在,请检查`
			});
		});
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-24041
	//出现一致性提示后,必须将相同的款号改为其他的。否则会一直提示 -> after中数据维护
	//自动生成款号，款号为空的话款号设置成原来款号
	describe('SLH-24041.自动生成款号', function () {
		let styleRes, dresDetail;
		before(async function () {
			await common.setGlobalParam('style_auto_gencode', 1); //自动生成款号  为启用
		});
		after(async function () {
			styleRes.params.pk = styleRes.result.val;
			styleRes.params.code = styleRes.params.name + 'new';
			await basicInfoReqHandler.editStyle({
				jsonparam: styleRes.params,
			});

			await common.setGlobalParam('style_auto_gencode', 0); //自动生成款号关闭
		});
		it('100027.新增款号', async function () {
			let styleJson = basiceJsonparam.addGoodJson()
			delete styleJson.code;
			styleRes = await basicInfoReqHandler.editStyle({
				jsonparam: styleJson,
			});

			dresDetail = await common.fetchMatInfo(styleRes.result.val);
			common.isApproximatelyEqualAssert(styleRes.params, dresDetail, ['code']);
		});
		it('修改商品-清空款号-保存', async function () {
			delete dresDetail.code;
			await basicInfoReqHandler.editStyle({
				jsonparam: dresDetail,
			});

			dresDetail = await common.fetchMatInfo(styleRes.result.val);
			common.isApproximatelyEqualAssert(styleRes.params, dresDetail, ['code']);
		});
		it('款号自动生成-款号唯一性校验', async function () {
			let styleJson = basiceJsonparam.addGoodJson()
			styleJson.code = Number(dresDetail.code) + 1;
			styleRes = await basicInfoReqHandler.editStyle({
				jsonparam: styleJson,
			});

			styleJson.code = '';
			const _styleRes = await basicInfoReqHandler.editStyle({
				jsonparam: styleJson,
				check: false
			});
			expect(_styleRes.result).to.include({
				msgId: 'stylecode_already_exist'
			});
		});
		it('款号自动生成-修改款号为空-款号设置成原来款号', async function () {
			const oldCode = dresDetail.code;
			delete dresDetail.code;
			const editRes = await basicInfoReqHandler.editStyle({
				jsonparam: dresDetail,
			});
			const _dresDetail = await common.fetchMatInfo(editRes.result.val);
			expect(_dresDetail.code).to.equal(oldCode);
		});
	});

	describe('最大库存与最小库存验证', function () {
		it('100038.最大库存等于最小库存', async function () {
			let basicGood = newStyleCode(basiceJsonparam.addGoodJson());
			basicGood.maxstock = 1; //最大库存
			basicGood.minstock = 1;
			let sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicGood,
				check: false,
			});
			expect(sfRes.result, `保存出错${JSON.stringify(sfRes.params)}`).not.to.have.property('error');
		})
		it('100039.最大库存大于于最小库存', async function () {
			let basicGood = newStyleCode(basiceJsonparam.addGoodJson());
			basicGood.maxstock = 5; //最大库存
			basicGood.minstock = 1;
			let sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicGood,
				check: false,
			});
			expect(sfRes.result, `保存出错${JSON.stringify(sfRes.params)}`).not.to.have.property('error');
		})
		it('100040.最大库存小于最小库存', async function () {
			let basicGood = newStyleCode(basiceJsonparam.addGoodJson());
			basicGood.maxstock = 1; //最大库存
			basicGood.minstock = 5;
			let sfRes = await basicInfoReqHandler.editStyle({
				jsonparam: basicGood,
				check: false,
			});
			expect(sfRes.result.error, ``).to.includes("最小库存数不能大于最大库存数");

		})
	});

	describe('流水明细-备注检查', function () {
		let outRes;
		after(async () => {
			await invCheckReqHandler.checkMainCancelByDate();
		})
		it('100199.1.类型为销售单', async function () {
			let json = basiceJsonparam.salesJson();
			await stockInOut(json);
		});
		it('100199.2.既拿货又退货', async function () {
			let json = basiceJsonparam.salesJson();
			json.details[0].num = -13;
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await stockInOut(json);
		});
		it('100200.销售退货', async function () {
			let json = basiceJsonparam.salesJson();
			json.details[0].num = -13;
			json.details.splice(1, 1);
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await stockInOut(json);
		});
		it('100202.调拨出库', async function () {
			await common.loginDo({
				logid: 200,
			});
			let json = basiceJsonparam.outJson();
			outRes = await stockInOut(json); //调出单
		})
		//100203依赖100202
		it('100203.调拨入库', async function () {
			await common.loginDo();
			let qfShopOut = await inOutHandler.shopInQueryBilling({
				pk: outRes.result.pk,
				billno: outRes.result.billno,
			}); //获取调入单明细
			let inRes = await inOutHandler.shopIn(qfShopOut.result); //门店调入

			//流水明细
			let params = format.qlParamsFormat({
				styleid: qfShopOut.result.details[0].styleid,
				pagesize: 15,
			});
			let qlRes = await common.callInterface('ql-15122', params);
			expect(qlRes, `ql-15122流水明细查询出错${params}`).to.not.have.property('error');
			let exp = {
				optime: inRes.params.hashkey,
				id: inRes.result.pk,
				maintype: USEECINTERFACE == 1 ? '调拨入库单' : '调拨入库',
				invnum: LOGINDATA.invnum,
				subnum: 0,
				addnum: inRes.params.totalnum,
				billno: inRes.result.billno,
				prodate: inRes.params.prodate,
				abstractMsg: `调出门店:${getBasicData.getNameById(outRes.params.shopid)},接收门店:${getBasicData.getNameById(outRes.params.invidref)}`,
			};
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0])
		})
		it.skip('100204.盘点处理', async function () {
			await common.loginDo({
				logid: 200,
			});
			await invCheckReqHandler.processCheck({}, false); //先进行部分盘点；
			let json = basiceJsonparam.invCheckJson();
			await stockInOut(json);
		})
		//需要后台增加字段
		it('100205.库存调整单', async function () {
			await common.loginDo({
				logid: 200,
			});
			await inOutHandler.shopInAllOnroadBilling(LOGINDATA.invid); //有在途数不允许库存调整
			let jsonparam = {
				propdresStyleid: BASICDATA.styleidAgc001, //款号agc001
				propdresColorid: BASICDATA.coloridBaiSe, //白色
				propdresSizeid: BASICDATA.sizeidM, //M码
				recvnum: 10,
			};
			let invSfRes = await basicInfoReqHandler.invNumChange({
				jsonparam: jsonparam,
			})
			// console.log(`invSfRes : ${JSON.stringify(invSfRes)}`);
			//流水明细
			let params = format.qlParamsFormat({
				styleid: qfShopOut.result.details[0].styleid,
				pagesize: 15,
			});
			let qlRes = await common.callInterface('ql-15122', params);
			expect(qlRes, `ql-15122流水明细查询出错${params}`).to.not.have.property('error');
			// let exp = {
			// 	optime: inRes.params.hashkey,
			// 	id: invSfRes.result.val,
			// 	maintype: USEECINTERFACE == 1 ? '调拨入库单' : '调拨入库',
			// 	invnum: LOGINDATA.invnum,
			// 	subnum: common.sub(jsonparam.recvnum, invSfRes.params.invnum),
			// 	addnum: 0,
			// 	billno: inRes.result.billno,
			// 	prodate: inRes.params.prodate,
			// 	abstractMsg:'库存调整单',
			// }
		})
		it('100206.1.入库单', async function () {
			await common.delay(500); //请求过快，可能和盘点有冲突，延迟一下
			let json = basiceJsonparam.purchaseJson();
			await stockInOut(json);
		})
		it('100206.2.入库单为负数', async function () {
			let json = basiceJsonparam.purchaseJson();
			json.details[1].num = -3;
			await stockInOut(json);
		})
	});

	describe('批量调价-main', function () {
		let newStyle1, newStyle2, changePrice, changeDiscount;
		before(async () => {
			newStyle1 = await basicInfoReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
				check: true,
			});
			// newStyle2 = await basicInfoReqHandler.editStyle({
			// 	jsonparam: basiceJsonparam.addGoodJson(),
			// 	check: true,
			// });
		});

		it('批量调价-价格', async function () {
			// 新增调价
			changePrice = await metManager.slh1MetManager.saveChangePriceFull({
				// pk: `${newStyle1.result.val},${newStyle2.result.val}`
				pk: newStyle1.result.val
			});
			// 查询调价列表
			let changePriceList = await metManager.slh1MetManager.queryChangePriceList({
				stylename: newStyle1.params.name
			}).then((res) => {
				return res.result.dataList.find((data) => data.id == newStyle1.result.val);
			});
			// 查询调节详情
			let changePriceFull = await metManager.slh1MetManager.queryChangePriceFull({ id: changePrice.result.val })
			common.isApproximatelyEqualAssert(changePriceList, changePrice.params.jsonparam);
			common.isApproximatelyEqualAssert(changePriceFull, basiceJsonparam.changePriceJsonExp());
		});

		it('撤销调整价格', async function () {
			// 撤销调价--调整价格
			let revokeChangePrice = await metManager.slh1MetManager.revokeChangePrice({ pk: changePrice.result.val, check: true });
			// 查询调价列表
			let changePriceList = await metManager.slh1MetManager.queryChangePriceList({ stylename: newStyle1.params.name }).then(
				(res) => { return res.result.dataList.find((data) => data.id == newStyle1.result.val); });
			// 查询调节详情
			let changePriceFull = await metManager.slh1MetManager.queryChangePriceFull({ id: changePrice.result.val });
			expect(revokeChangePrice, `撤销调价-价格失败`).to.hasOwnProperty('val');
			common.isApproximatelyEqualAssert(changePriceList, newStyle1.params);
			common.isApproximatelyEqualAssert(changePriceFull, newStyle1.params);
		})

		it('批量调价-折扣', async function () {
			// 新增调价-折扣
			changeDiscount = await metManager.slh1MetManager.saveChangeDiscountFull({
				// pk: `${newStyle1.result.val},${newStyle2.result.val}`
				pk: newStyle1.result.val
			});
			// 查询调价列表
			let changeDiscountList = await metManager.slh1MetManager.queryChangePriceList({
				stylename: newStyle1.params.name
			}).then((res) => {
				return res.result.dataList.find((data) => data.id == newStyle1.result.val);
			});
			// 查询调节详情
			let changeDiscountFull = await metManager.slh1MetManager.queryChangePriceFull({
				id: changeDiscount.result.val
			}).then((res) => {
				return res.dataList.find((data) => data.name == newStyle1.params.name);
			});
			expect(changeDiscountList.discount, `批量调价失败`).to.equal(changeDiscount.params.jsonparam.batchDiscount);
			expect(changeDiscountFull.newdiscount, `批量调价失败`).to.equal(changeDiscount.params.jsonparam.batchDiscount);

		});

		it('撤销调整折扣', async function () {
			let revokeChangeDiscount = await metManager.slh1MetManager.revokeChangePrice({
				pk: changeDiscount.result.val,
				check: true
			});
			// 查询调价列表
			let changeDiscountList = await metManager.slh1MetManager.queryChangePriceList({
				stylename: newStyle1.params.name
			}).then((res) => {
				return res.result.dataList.find((data) => data.id == newStyle1.result.val);
			});
			// 查询调节详情
			let changeDiscountFull = await metManager.slh1MetManager.queryChangePriceFull({
				id: changeDiscount.result.val
			}).then((res) => {
				return res.dataList.find((data) => data.name == newStyle1.params.name);
			});
			expect(changeDiscountList.discount, `批量调价失败`).to.equal(String(newStyle1.params.discount));
			expect(changeDiscountFull.discount, `批量调价失败`).to.equal(String(newStyle1.params.discount));
		});

		it('只允许撤销最后一次调价', async function () {
			// 新增调价
			let changePrice1 = await metManager.slh1MetManager.saveChangePriceFull({ pk: newStyle1.result.val });
			// 新增调价
			let changePrice2 = await metManager.slh1MetManager.saveChangePriceFull({ pk: newStyle1.result.val });
			// 撤销调价--调整价格
			let revokeChangePrice = await metManager.slh1MetManager.revokeChangePrice({ pk: changePrice1.result.val, });
			expect(revokeChangePrice.error, `只能撤销最后一次调价失败`).to.equal("只能撤销最后一次调价");
		});




	});

	// describe('库存调整功能-new', function () {

	// });

});

//新增货品的款号区分一下
function newStyleCode(goodjson) {

	let str = common.getRandomStr(6);
	goodjson.code = `new${str}`;
	goodjson.name = `newname${str}`;
	return goodjson;
};
//流水明细-不同单据
async function stockInOut(json) {
	let qfShopOut;
	json = format.jsonparamFormat(json);
	let sfRes = await common.editBilling(json);
	let params = format.qlParamsFormat({
		styleid: json.details[0].styleid,
		pagesize: 15,
	});
	//流水明细
	let qlRes = await common.callInterface('ql-15122', params);
	expect(qlRes, `ql-15122流水明细查询出错${params}`).to.not.have.property('error');
	let type, abstractMsg, exp = {};
	switch (sfRes.params.type) {
		case 21:
			type = '销售单', abstractMsg = `客户:${getBasicData.getNameById(sfRes.params.dwid)}`,
				exp.subnum = sfRes.params.totalnum, exp.addnum = 0;
			break;
		case 10:
			type = USEECINTERFACE == 1 ? '入库单' : '采购进货', abstractMsg = `供应商:${getBasicData.getNameById(sfRes.params.dwid)}`,
				exp.subnum = 0, exp.addnum = sfRes.params.totalnum;
			break;
		case 25: {
			type = USEECINTERFACE == 1 ? '调拨出库单' : '调拨出库',
				abstractMsg = `调出门店:${getBasicData.getNameById(sfRes.params.shopid)},接收门店:${getBasicData.getNameById(sfRes.params.invidref)}`,
				exp.subnum = sfRes.params.totalnum,
				exp.addnum = 0;
			return sfRes; //调出单
		}
			break;
		case 17: {
			let invParams = format.qlParamsFormat({
				invid: LOGINDATA.invid,
				styleid: json.details[0].styleid,
			})
			let invNum = await common.callInterface('ql-1932', invParams);
			//console.log(`invNum : ${JSON.stringify(invNum)}`);
			let checkResult = await invCheckReqHandler.processCheck(); //部分盘点，修改下盘点处理的pk

			qlRes = await common.callInterface('ql-15122', params);
			expect(qlRes, `ql-15122流水明细查询出错${params}`).to.not.have.property('error');
			sfRes.result.pk = checkResult.result.pk;
			sfRes.result.billno = checkResult.result.billno;
			type = USEECINTERFACE == 1 ? '盘点单' : '盘点库存';
			abstractMsg = '盘点处理:部分盘点';
			exp.subnum = 0,
				exp.addnum = common.sub(sfRes.params.totalnum, invNum.sumrow.invnum);
		}
			break;
		default:
			break;
	};
	//	let optime = .substr(18, 19);
	//console.log(`optime : ${JSON.stringify(optime)}`);
	exp = {
		optime: sfRes.params.hashkey,
		id: sfRes.result.pk,
		maintype: type,
		invnum: LOGINDATA.invnum,
		billno: sfRes.result.billno,
		prodate: sfRes.params.prodate,
		abstractMsg: abstractMsg,
	};
	common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);

};