'use strict';

const common = require('../../../lib/common');
const format = require('../../../data/format');
const reqHandlerHelp = require('../../help/reqHandlerHelp');
const metManager = require('../../../reqHandler/slh1/index');
const basicJsonparam = require('../../help/basiceJsonparam')

describe('基本设置-mainLine', function () {
    this.timeout(30000);

    before(async function () {
        await common.loginDo();
    });

    describe('颜色组管理', function () {
        let newColorGroup;
        before(async () => {
            // 新增颜色组
            newColorGroup = await metManager.slh1MetManager.saveColorGroupFull();
        });
        after(async () => {
            // 停用颜色组
            await metManager.slh1MetManager.disableDict({ pk: newColorGroup.result.pk });
        });

        describe('新增颜色组', function () {
            it('查询颜色组列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryColorGroupList({ name: newColorGroup.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newColorGroup.result.pk));
                common.isApproximatelyEqual(newColorGroup.params.jsonparam, qlRes);
            });
            it('查询颜色组详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryColorGroupFull({ pk: newColorGroup.result.pk });
                common.isApproximatelyEqual(newColorGroup.params.jsonparam, qfRes);
            });
        });

        describe('修改颜色组', function () {
            let editColorGroup;
            before(async () => {
                // 修改颜色组
                editColorGroup = await metManager.slh1MetManager.saveColorGroupFull({ id: newColorGroup.result.pk });
            })
            it('查询颜色组列表', async function () {
                let groupList = await metManager.slh1MetManager.queryColorGroupList({ name: editColorGroup.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newColorGroup.result.pk));
                common.isApproximatelyEqual(editColorGroup.params.jsonparam, groupList);
            });
            it('查询颜色组详情', async function () {
                let groupFull = await metManager.slh1MetManager.queryColorGroupFull({ pk: newColorGroup.result.pk });
                common.isApproximatelyEqual(editColorGroup.params.jsonparam, groupFull);
            });
        });
    });

    describe('颜色管理', function () {
        let newColor;
        before(async () => {
            //查询颜色组列表
            let parentColor = await metManager.slh1MetManager.queryColorGroupList({ name: '中' }).then(
                res => res.result.dataList.find(data => data.id));
            let colorFull = await metManager.slh1MetManager.queryColorFull({ pk: parentColor.pk });
            // 新增颜色
            newColor = await metManager.slh1MetManager.saveColorFull({ parentid: colorFull.sid || '' });
        });
        after(async () => {
            // 停用颜色
            await metManager.slh1MetManager.disableDict({ pk: newColor.result.pk });
        });
        describe('新增颜色', function () {
            it('查询颜色列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryColorList({ name: newColor.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newColor.result.pk));
                common.isApproximatelyEqual(newColor.params.jsonparam, qlRes);
            });
            it('查询颜色详情', async function () {
                let qfRes = await metManager.slh1MetManager.queryColorFull({ pk: newColor.result.pk });
                common.isApproximatelyEqual(newColor.params.jsonparam, qfRes);
            });
        });
        describe('修改颜色', function () {
            let editColor;
            before(async () => {
                // 修改颜色
                editColor = await metManager.slh1MetManager.saveColorFull({ id: newColor.result.pk });
            });
            it('查询颜色列表', async function () {
                let qlRes = await metManager.slh1MetManager.queryColorList({ name: editColor.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == editColor.result.pk));
                common.isApproximatelyEqual(editColor.params.jsonparam, qlRes);
            });
            it('查询颜色详情', async function () {
                let colorFull = await metManager.slh1MetManager.queryColorFull({ pk: editColor.result.pk });
                common.isApproximatelyEqual(editColor.params.jsonparam, colorFull);
            });
        });

    });

    describe('尺码组管理', function () {
        let newSizeGroup;
        before(async () => {
            // 新增尺码组
            newSizeGroup = await metManager.slh1MetManager.saveSizeGroupFull();
        });
        after(async () => {
            // 停用尺码组
            await metManager.slh1MetManager.disableDict({ pk: newSizeGroup.result.pk })
        })
        describe('新增尺码组', function () {
            it('查询尺码组列表', async function () {
                // 查询尺码组列表
                let qlRes = await metManager.slh1MetManager.querySizeGroupList({ name: newSizeGroup.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newSizeGroup.result.pk)
                );
                common.isApproximatelyEqual(newSizeGroup.params.jsonparam, qlRes);
            });
            it('查询尺码组详情', async function () {
                let qfRes = await metManager.slh1MetManager.querySizeGroupFull({ pk: newSizeGroup.result.pk });
                common.isApproximatelyEqual(newSizeGroup.params.jsonparam, qfRes);
            });
        });
        describe('修改尺码组', function () {
            let editSizeGroup;
            before(async () => {
                // 修改尺码组
                editSizeGroup = await metManager.slh1MetManager.saveSizeGroupFull({ id: newSizeGroup.result.pk });
            })
            it('查询尺码组列表', async function () {
                let qlRes = await metManager.slh1MetManager.querySizeGroupList({ name: editSizeGroup.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == editSizeGroup.result.pk));
                common.isApproximatelyEqual(editSizeGroup.params.jsonparam, qlRes);
            });
            it('查询尺码组详情', async function () {
                let qfRes = await metManager.slh1MetManager.querySizeGroupFull({ pk: editSizeGroup.result.pk });
                common.isApproximatelyEqual(editSizeGroup.params.jsonparam, qfRes);
            });
        });
    });

    describe('尺码管理', function () {
        let newSize, newSizeGroup;
        before(async () => {
            // 新增尺码组
            newSizeGroup = await metManager.slh1MetManager.saveSizeGroupFull();
            //查询尺码组详情
            let sizegroupFull = await metManager.slh1MetManager.querySizeGroupFull({ pk: newSizeGroup.result.pk });
            // 新增尺码
            newSize = await metManager.slh1MetManager.saveSizeFull({ parentid: sizegroupFull.sid || '' });
        });
        after(async () => {
            // 停用尺码组
            await metManager.slh1MetManager.disableDict({ pk: newSizeGroup.result.pk });
            // 停用尺码
            await metManager.slh1MetManager.disableDict({ pk: newSize.result.pk });
        });
        describe('新增尺码', function () {
            it('查询尺码列表', async function () {
                let qlRes = await metManager.slh1MetManager.querySizeList({ name: newSize.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newSize.result.pk));
                common.isApproximatelyEqual(newSize.params.jsonparam, qlRes);
            });
            it('查询尺码详情', async function () {
                let qfRes = await metManager.slh1MetManager.querySizeFull({ pk: newSize.result.pk });
                common.isApproximatelyEqual(newSize.params.jsonparam, qfRes);
            });
        });
        describe('修改尺码', function () {
            let editSize;
            before(async () => {
                // 修改尺码
                editSize = await metManager.slh1MetManager.saveSizeFull({ id: newSize.result.pk });
            })
            it('查询尺码列表', async function () {
                // 查询尺码列表
                let qlRes = await metManager.slh1MetManager.querySizeList({ name: editSize.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == editSize.result.pk));
                common.isApproximatelyEqual(editSize.params.jsonparam, qlRes);
            });
            it('查询尺码详情', async function () {
                let qfRes = await metManager.slh1MetManager.querySizeFull({ pk: editSize.result.pk });
                common.isApproximatelyEqual(editSize.params.jsonparam, qfRes);
            });
        });
    });

    describe('价格名称', function () {
        let newPriceName, pricesid, newClient;
        before(async () => {
            // 新增价格名称
            newPriceName = await metManager.slh1MetManager.savePriceNameFull();
            // 查询价格名称详情
            pricesid = await metManager.slh1MetManager.queryPriceNameFull({ pk: newPriceName.result.pk })
            // 新增客户
            let params = basicJsonparam.addCustJson();
            params.pricetype = pricesid.sid
            newClient = await metManager.slh1CustomerManager.saveCustomerFull(params);
        });
        after(async () => {
            // 修改客户使用价格
            let params = basicJsonparam.addCustJson();
            params.pk = newClient.result.pk
            await metManager.slh1CustomerManager.saveCustomerFull(params);
            // 停用价格名称
            await metManager.slh1MetManager.disableDict({ pk: newPriceName.result.pk });
        })
        it('使用中的价格不能停用', async function () {
            let disablePrice = await metManager.slh1MetManager.savePriceNameFull({ id: newPriceName.result.pk, parenttypeid: 0 });
            expect(disablePrice.result.error, `使用中的价格不能停用,失败${disablePrice}`).to.be.equal('该价格名称正在被使用，无法停用');
        });


    });






});