'use strict';
const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');
const sortHandler = require('../../help/listCheckHelp/sortHandler.js');
const interListHandler = require('../../help/listCheckHelp/interfaceList.js');

//查询列表接口数组
const interfaceidArr = require('../../help/listCheckHelp/interfaceList').qlListInterfaceId;
//默认排序验证 不同接口规则不同
const defaultSortord = require('../../help/listCheckHelp/interfaceList').defaultSortordIFC;

let params = format.qlParamsFormat({
	prodate1: common.getDateString([0, 0, -5]), //根据帐套数据自行调节
	prodate2: common.getDateString([0, 0, 0]),
	optime1: common.getDateString([0, 0, -5]),
	optime2: common.getDateString([0, 0, 0]),
	// search_count: 0,
	search_sum: 0,
	// pagesize: 5
});

describe("列表排序验证", function () {
	this.timeout(900000);

	before(async () => {
		await common.loginDo();
	});

	it.skip('验证排序', async function () {
		let ret = true,
			errorMsg = '';
		let interfaceData = _.chunk(interfaceidArr, 10);
		// console.log(`interfaceData : ${JSON.stringify(interfaceData)}`);
		for (let i = 0; i < interfaceData.length; i++) {
			let result = await asyncPostSearchRequest(interfaceData[i]);
			ret = ret && result.isRightOrder;
			errorMsg += result.errorMsg;

			await common.delay(100);
			// console.log(`result : ${JSON.stringify(result)}`);

		}
		expect(ret, errorMsg).to.be.true;
	});

	it.skip('170741.默认排序验证', async function () {
		const interfaceids = Object.keys(defaultSortord);
		const promises = interfaceids.map((interfaceid) => common.callInterface(interfaceid, params));
		const datas = await Promise.all(promises); //获取结果
		let isOk = true;
		let errorMsg = ''; //收集错误日志

		for (let i = 0, dataLength = datas.length; i < dataLength; i++) {
			let interfaceid = interfaceids[i];
			if (!datas[i].dataList || datas[i].dataList.length < 2) {
				errorMsg += `\ninterfaceid=${interfaceids[i]} 数组长度小于2，无法验证排序结果`;
				isOk = false;
				continue;
			};
			let actualArr = [];
			datas[i].dataList.map((data) => {
				let str = '';
				defaultSortord[interfaceid].map((field) => {
					if (field.includes('no') && data[field]) {
						data[field] = data[field].padStart(6, '0'); //批次号长度补全
					}
					str += `${data[field]},`;
				});
				actualArr.push(str); //mysql不区分大小写 .toLowerCase()
			});
			let expArr = _.cloneDeep(actualArr).sort().reverse(); //倒叙排序

			let ret = true;
			for (let idx = 0, length = actualArr.length; idx < length; idx++) {
				if (actualArr[idx] != expArr[idx]) {
					errorMsg += `\ninterfaceid=${interfaceids[i]} 默认排序错误 idx=${idx} actual=${actualArr[idx]} exp=${expArr[idx]}`; //
					ret = false;
					break;
				};
			};
			// if(ret) console.log(`interfaceid = ${interfaceid} 默认排序验证通过`);
			isOk = isOk && ret;
		};
		expect(isOk, `默认排序验证错误:${errorMsg}\n`).to.be.true;
	});

});

/*
 * 10个接口做一次并发请求
 */
async function asyncPostSearchRequest(interfaceList) {
	let promises = interfaceList.map((interfaceid) => common.callInterface(interfaceid, params));
	let datas = await Promise.all(promises);

	let ret = true,
		errorMsg = '';
	for (let i = 0; i < datas.length; i++) {
		let value = datas[i];
		if (JSON.stringify(value).includes('error')) {
			ret = false;
			errorMsg += `\n${interfaceList[i]}请求出错,请求返回结果为:${JSON.stringify(value)}`;
		} else if (value.count == 0) {
			ret = false;
			errorMsg += `\n${interfaceList[i]}查询无果,无法进行排序验证`;
		} else {
			//验证接口各个字段排序
			let result = await interfaceAllFieldSortCheck(interfaceList[i], value.dataList);
			ret = ret && result.isRightOrder;
			errorMsg += result.errorMsg;
		}
	}

	return {
		'isRightOrder': ret,
		'errorMsg': errorMsg
	}
}


/*
 * 针对接口interfaceid的返回结果，对所有的返回字段遍历做排序操作
 */
async function interfaceAllFieldSortCheck(interfaceid, resultList) {

	let fields = sortHandler.getPropertys(resultList[0]);

	let isRightOrder = true;
	let errorMsg = '';
	for (let i in fields) {
		let ascSortResult = await singleSortCheck(interfaceid, '0', fields[i]);
		let desSortResult = await singleSortCheck(interfaceid, '1', fields[i]);
		if (!ascSortResult.isRightOrder) {
			errorMsg += ascSortResult.errorMsg;
			isRightOrder = isRightOrder && ascSortResult.isRightOrder;
		}
		if (!desSortResult.isRightOrder) {
			errorMsg += desSortResult.errorMsg;
			isRightOrder = isRightOrder && desSortResult.isRightOrder;
		}
	}

	return {
		'isRightOrder': isRightOrder,
		'errorMsg': errorMsg
	};
}

/*
 * 单个发送排序请求，并验证排序是否正确
 *
 * interfaceid 接口号
 * sortOrder  0:升序 1:降序
 * sortField  排序的字段
 *
 * @return 排序是否正确
 */
async function singleSortCheck(interfaceid, sortOrder, sortField) {
	/* 发送排序请求 */
	let sortParams = common.mixObject(params, {
		pagesize: 15
	});
	let result = await common.callInterface(interfaceid, common.mixObject(sortParams, {
		'sortfieldOrder': sortOrder,
		'sortField': sortField
	}));
	if (JSON.stringify(result).includes('本列不支持排序')) {
		return {
			'isRightOrder': true,
			'errorMsg': `\n接口:${interfaceid} 排序字段:${sortField} 暂不支持排序`
		};
	} else if (result.hasOwnProperty('error')) {
		return {
			'isRightOrder': false,
			'errorMsg': `\n接口:${interfaceid} 排序字段:${sortField} 排序类型:${sortOrder} 请求出错,请求返回结果为:${JSON.stringify(result)}}`
		};
	} else if (result.hasOwnProperty('count') && result.count == '0') {
		return {
			'isRightOrder': false,
			'errorMsg': `\n接口:${interfaceid} 排序字段:${sortField} 排序类型:${sortOrder} 查询无果，无法进行排序验证`
		};
	}

	/* 将要排序的字段放到singleFieldList数组中，并将所有元素转换为小写 */
	let singleFieldList = [];
	for (let i = 0; i < result.dataList.length; i++) {
		singleFieldList.push(result.dataList[i][sortField].toLowerCase());
	}

	/* 对singleFieldList进行排序 */
	singleFieldList = sortHandler.arraySort(singleFieldList); //升序
	if (sortOrder == 1 || sortOrder == '1') singleFieldList = singleFieldList.reverse(); //降序

	/* 验证 将singleFieldList结果与result中的结果对比 */
	let isSortRight = true;
	for (let i = 0; i < result.dataList.length; i++) {
		if (result.dataList[i][sortField].toLowerCase() != singleFieldList[i]) {
			isSortRight = false;
			break;
		}
	}

	return {
		'isRightOrder': isSortRight,
		'errorMsg': !isSortRight ? `\n接口:${interfaceid} 排序字段:${sortField} 排序类型:${sortOrder} 排序错误` : '',
	};
}
