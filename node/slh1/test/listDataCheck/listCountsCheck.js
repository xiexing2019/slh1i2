'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
//查询列表接口数组
const interfaceidArr = require('../../help/listCheckHelp/interfaceList').qlListInterfaceId;
// const interfaceidArr = ['ql-142201']; //测试用

/*
 *	忽略标题
 *	这些标题不计算汇总值,sumrow中显示为0
 */
const ignoreTitles = {
	'ql-14451': ['backrate', 'avgprice'], //服务端已去除
	'ql-1209': ['roll'],
};

const processTitles = {
	'ql-1354': 'inouttype=2;totalmoney;minus',
	'ql-1360': 'inouttype=2;money;minus',
};

/*
 *  ql接口的汇总值验证
 *  其他用例不再单独验证
 */
describe.skip("列表汇总验证", function () {
	this.timeout(300000); //接口过多

	let params = format.qlParamsFormat({
		prodate1: common.getDateString([0, 0, -5]), //根据帐套数据自行调节
		prodate2: common.getDateString([0, 0, 0]),
		optime1: common.getDateString([0, 0, -5]),
		optime2: common.getDateString([0, 0, 0]),
	});
	before(async () => {
		await common.loginDo();
	});

	it('汇总验证', async () => {
		let dyadicArr = _.chunk(interfaceidArr, 10); //拆分数组 防止并发太多出现问题
		let ret = true,
			errorMsg = '';
		for (let arr of dyadicArr) {
			let promises = arr.map((interfaceid) => common.callInterface(interfaceid, params));
			let datas = await Promise.all(promises);
			datas.map((data, index) => {
				if (data.error) {
					ret = false;
					errorMsg += `\n${arr[index]}请求出错,请求返回结果为:${JSON.stringify(data)}`;
				} else if (data.count == 0) {
					ret = false;
					errorMsg += `\n${arr[index]}查询无果，无法进行汇总验证`;
				} else {
					let result = countsCheckAssert(data, arr[index]);
					ret = ret && result.flag;
					errorMsg += result.errorMsg;
				};
			});
		};
		expect(ret, errorMsg).to.be.true;
	});

});

//汇总值验证
//返回{flag,errorMsg}
function countsCheckAssert(data, interfaceid) {
	// console.log(`data = ${JSON.stringify(data)}`);
	let expected = getCounts(dataProcessing(interfaceid, data.dataList));

	let ignoreArr = []; //忽略字段
	if (ignoreTitles[interfaceid]) ignoreArr = ignoreTitles[interfaceid];
	let ret = common.isApproximatelyEqual(expected, data.sumrow, ignoreArr);
	if (ret.errorMsg != '') ret.errorMsg = `\n${interfaceid}汇总值错误  ${ret.errorMsg}`;
	return ret;
};

//计算dataList的汇总值
function getCounts(dataList) {
	let count = {};
	for (let i = 0, length = dataList.length; i < length; i++) {
		count = common.addObject(count, dataList[i]);
	};
	return count;
};

//dataList数据处理
//删除作废数据 数值取反等
function dataProcessing(interfaceid, arr) {
	//删除作废数据 汇总值中不应该统计
	if (arr[0].invalidflag) {
		for (let i = 0, length = arr.length; i < length; i++) {
			if (arr[i].invalidflag == 1) {
				arr.splice(i, 1);
				i--;
				length--;
			};
		};
	};

	//特殊字段处理 (e.g. 支出为负,dataList中显示为正值,汇总时需要取负值)
	if (processTitles[interfaceid]) {
		let [cond, keys, processType] = processTitles[interfaceid].split(';');
		keys = keys.split(',');
		cond = cond.split('=');
		for (let i = 0, length = arr.length; i < length; i++) {
			if (arr[i][cond[0]] == cond[1]) {
				keys.map((key) => {
					arr[i][key] = processFunc(processType, arr[i][key]);
				});
			};
		};
	};

	return arr;
};

function processFunc(type, str) {
	switch (type) {
		case 'plus':
			str = Math.abs(str);
			break;
		case 'minus':
			str = -Math.abs(str);
			break;
		default:
			break
	};
	return str;
};
