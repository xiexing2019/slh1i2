'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');

/*
 * 采购订货-按汇总
 */
describe('采购订货-按汇总', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('按款号', function () {
		let styleInfo;
		before(async () => {
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		});
		it('130004.1.查询--待入库数按明细查和按汇总查不一致已提bug', async function () {
			let params = {
				'ql-22103': { //采购订货-按明细查
					prodate1: common.getDateString([0, 0, -7]),
					mainInvid: LOGINDATA.invid,
					propdresStyleid: BASICDATA.styleidAgc001,
					search_list: 0, //只需要汇总值
				},
				'ql-22311': { //采购订货-按汇总-按汇总
					prodate1: common.getDateString([0, 0, -7]),
					styleid: BASICDATA.styleidAgc001,
					stylename: styleInfo.name,
					shopid: LOGINDATA.invid,
					classid: styleInfo.classid,
				},
			};
			let qlRes = await common.getResults(params);
			expect(qlRes['ql-22311'].count, '查询结果不唯一').to.equal('1');
			let exp = qlRes['ql-22103'].sumrow;
			exp.id = BASICDATA.styleidAgc001;
			exp.mat_code = styleInfo.code;
			exp.mat_name = styleInfo.name;
			exp.mat_money = exp.total;
			exp.diffnum = exp.leftnum;
			common.isApproximatelyEqualAssert(exp, qlRes['ql-22311'].dataList[0]);
			//查询结果唯一,汇总值等于datalist[0]的值
			common.isApproximatelyEqualAssert(qlRes['ql-22311'].dataList[0], qlRes['ql-22311'].sumrow)
		});
		it('130004.4总经理查询其他门店数据', async function () {
			let params = {
				'ql-22103': { //采购订货-按明细查
					prodate1: common.getDateString([0, 0, -7]),
					mainInvid: BASICDATA.shopidZzd,
					search_list: 0, //只需要汇总值
				},
				'ql-22311': { //采购订货-按汇总-按汇总
					prodate1: common.getDateString([0, 0, -7]),
					shopid: BASICDATA.shopidZzd,
				},
			};
			let qlRes = await common.getResults(params);
			expect(Number(qlRes['ql-22311'].count), '查询无结果').to.be.above(0);
			common.isApproximatelyEqualAssert(qlRes['ql-22103'].sumrow, qlRes['ql-22311'].sumrow);
		});
		it('130004.5.非总经理角色查询', async function () {
			let param = format.qlParamsFormat({
				prodate1: common.getDateString([-1, 0, 0]),
				search_list: 0,
			}, false);
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});
			let qlRes = await common.callInterface('ql-22311', param);

			param.shopid = LOGINDATA.invid;
			let qlRes2 = await common.callInterface('ql-22311', param);

			param.shopid = BASICDATA.shopidZzd;
			let qlRes3 = await common.callInterface('ql-22311', param);

			await common.loginDo();
			expect(qlRes.sumrow, '非总经理角色查询所有门店数据与查询本门店数据不一致').to.eql(qlRes2.sumrow);
			expect(qlRes3.count, '常青店非总经理角色查询其他门店数据出错').to.eql('0');
		});
	});

	describe('按厂商', function () {
		let dwInfo;
		before(async () => {
			dwInfo = await common.callInterface('qf-2241', {
				pk: BASICDATA.dwidVell,
			});
		});
		it('130005.1.查询', async function () {
			let params = {
				'ql-22103': { //采购订货-按明细查
					prodate1: common.getDateString([0, 0, -7]),
					mainInvid: LOGINDATA.invid,
					mainDwid: BASICDATA.dwidVell,
					search_list: 0, //只需要汇总值
				},
				'ql-22312': { //采购订货-按汇总-按厂商
					prodate1: common.getDateString([0, 0, -7]),
					dwid: BASICDATA.dwidVell,
					shopid: LOGINDATA.invid,
				},
			};
			let qlRes = await common.getResults(params);
			expect(qlRes['ql-22312'].count, '查询结果不唯一').to.equal('1');

			let exp = qlRes['ql-22103'].sumrow;
			exp.id = BASICDATA.dwidVell;
			exp.dwname = dwInfo.nameshort;
			exp.mat_money = exp.total;
			exp.diffnum = exp.leftnum;
			common.isApproximatelyEqualAssert(exp, qlRes['ql-22312'].dataList[0]);
			//查询结果唯一,汇总值等于datalist[0]的值
			common.isApproximatelyEqualAssert(qlRes['ql-22312'].dataList[0], qlRes['ql-22312'].sumrow)
		});
		it('130005.4.总经理查询其他门店数据', async function () {
			let params = {
				'ql-22103': { //采购订货-按明细查
					prodate1: common.getDateString([0, 0, -7]),
					mainInvid: BASICDATA.shopidZzd,
					search_list: 0, //只需要汇总值
				},
				'ql-22312': { //采购订货-按汇总-按厂商
					prodate1: common.getDateString([0, 0, -7]),
					shopid: BASICDATA.shopidZzd,
				},
			};
			let qlRes = await common.getResults(params);
			expect(Number(qlRes['ql-22312'].count), '查询无结果').to.be.above(0);
			common.isApproximatelyEqualAssert(qlRes['ql-22103'].sumrow, qlRes['ql-22312'].sumrow);
		});
		it('130005.5.非总经理角色查询', async function () {
			let param = format.qlParamsFormat({
				prodate1: common.getDateString([-1, 0, 0]),
				search_list: 0,
			}, false);
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});
			let qlRes = await common.callInterface('ql-22312', param);

			param.shopid = LOGINDATA.invid;
			let qlRes2 = await common.callInterface('ql-22312', param);

			param.shopid = BASICDATA.shopidZzd;
			let qlRes3 = await common.callInterface('ql-22312', param);

			await common.loginDo();
			expect(qlRes.sumrow, '非总经理角色查询所有门店数据与查询本门店数据不一致').to.eql(qlRes2.sumrow);
			expect(qlRes3.count, '常青店非总经理角色查询其他门店数据出错').to.eql('0');
		});
	});

	describe('按门店', function () {
		before(async () => {
			await common.setDataPrivilege('aa', '-2');
			await common.setDataPrivilege('shopmanager', '-2');
		});

		it('130006,按门店汇总,验证数据的正确性.rankA', async function () {
			let summaryByShop = await common.callInterface('ql-22313', format.qlParamsFormat({
				'shopid': LOGINDATA.invid
			}, true));
			let orderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			let summaryByShopAft = await common.callInterface('ql-22313', format.qlParamsFormat({
				'shopid': LOGINDATA.invid
			}, true));
			common.isApproximatelyEqualAssert({
				'id': summaryByShop.dataList[0].id,
				'num': common.add(summaryByShop.dataList[0].num, orderResult.params.totalnum),
				'recvnum': summaryByShop.dataList[0].recvnum,
				'shopname': summaryByShop.dataList[0].shopname,
				'mat_money': common.add(summaryByShop.dataList[0].mat_money, orderResult.params.totalsum),
				'diffnum': common.add(summaryByShop.dataList[0].diffnum, orderResult.params.totalnum),
			}, summaryByShopAft.dataList[0]);
		});

		it('130006,按门店汇总,总经理可以看到所有门店数据.rankA', async function () {
			// 中洲店登录做一个采购订货单，先在其他门店有按门店汇总数据
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});
			let orderJson = basiceJsonparam.purchaseOrderJson();
			let orderResult = await common.editBilling(orderJson);

			// 常青店登录
			await common.loginDo();
			orderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			let summaryByShop = await common.callInterface('ql-22313', format.qlParamsFormat({}, true));
			expect(Number(summaryByShop.count) > 1, `总经理可查看范围有误`).to.be.true;
		});

		it('130006,按门店汇总,店长, 采购员角色只能看本门店的数据.rankA', async function () {
			await common.loginDo({
				'logid': '004',
				'pass': '000000'
			});
			let summaryByShop = await common.callInterface('ql-22313', format.qlParamsFormat({}, true));
			expect(Number(summaryByShop.count) == 1, `店长可查看范围有误1`).to.be.true;
			expect(summaryByShop.dataList[0], `店长可查看范围有误2`).to.includes({
				'shopname': LOGINDATA.depname,
			});

			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});
			summaryByShop = await common.callInterface('ql-22313', format.qlParamsFormat({}, true));
			expect(Number(summaryByShop.count) == 1, `采购员可查看范围有误1`).to.be.true;
			expect(summaryByShop.dataList[0], `采购员查看范围有误2`).to.includes({
				'shopname': LOGINDATA.depname,
			});
		});
	});

});
