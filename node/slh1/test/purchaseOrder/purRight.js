'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');

/*
 * 采购入库模块，权限相关
 */
describe('采购入库模块，权限相关', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('130038,采购员能看到的订单范围检查.rankA', function () {

		let otherShopid;
		before(async () => {
			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});
			let orderJson = basiceJsonparam.purchaseOrderJson();

			// 常青店订货
			orderJson.invid = orderJson.shopid = LOGINDATA.invid;
			let orderResult = await common.editBilling(orderJson);
			// 中洲店订货
			otherShopid = orderJson.invid = orderJson.shopid = BASICDATA.shopidZzd;
			if (orderJson.cash) orderJson.cashaccountid = BASICDATA[`cashAccount${otherShopid}`];
			if (orderJson.card) orderJson.cardaccountid = BASICDATA[`bankAccount${otherShopid}`];
			if (orderJson.remit) orderJson.remitaccountid = BASICDATA[`agencyAccount${otherShopid}`];
			orderResult = await common.editBilling(orderJson);
		});

		after(async () => {
			await common.loginDo();
			await common.setDataPrivilege('aa', '-2'); //修改采购员 "记录权限"
			await common.loginDo();
		});

		// 注意：修改某个角色 "记录权限" 后，要重新登录才能生效
		it('所有部门', async function () {
			await common.loginDo();
			await common.setDataPrivilege('aa', '-1'); //修改采购员 "记录权限"
			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});

			// 验证按批次查询范围
			let batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'invid': LOGINDATA.invid
			}, true));
			expect(Number(batchList.dataList.length) > 0, `采购员记录权限设置的是所有部门，但是无法查询到本门店的按批次查订货信息`).to.be.true;

			batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'invid': otherShopid
			}, true));
			expect(Number(batchList.dataList.length) > 0, `采购员记录权限设置的是所有部门，但是无法查询到其他门店的按批次查订货信息`).to.be.true;

			// 验证按明细查查询范围
			let detailList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'mainInvid': LOGINDATA.invid
			}, true));
			expect(Number(detailList.dataList.length) > 0, `采购员记录权限设置的是所有部门，但是无法查询到本门店的按明细查订货信息`).to.be.true;

			detailList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'mainInvid': otherShopid
			}, true));
			expect(Number(detailList.dataList.length) > 0, `采购员记录权限设置的是所有部门，但是无法查询到其他门店的按明细查订货信息`).to.be.true;
		});

		it('本部门及下级所有部门', async function () {
			await common.loginDo();
			await common.setDataPrivilege('aa', '-2'); //修改采购员 "记录权限"
			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});

			// 验证按批次查询范围
			let batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'invid': LOGINDATA.invid
			}, true));
			expect(Number(batchList.dataList.length) > 0, `采购员记录权限设置的是本部门及下级所有部门，但是无法查询到本门店的按批次查订货信息`).to.be.true;

			batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'invid': otherShopid
			}, true));
			expect(Number(batchList.dataList.length) == 0, `采购员记录权限设置的是本部门及下级所有部门，但是查询到了其他门店的按批次查订货信息`).to.be.true;

			// 验证按明细查查询范围
			let detailList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'mainInvid': LOGINDATA.invid
			}, true));
			expect(Number(detailList.dataList.length) > 0, `采购员记录权限设置的是本部门及下级所有部门，但是无法查询到本门店的按明细查订货信息`).to.be.true;

			detailList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'mainInvid': otherShopid
			}, true));
			expect(Number(detailList.dataList.length) == 0, `采购员记录权限设置的是本部门及下级所有部门，但是查询到了其他门店的按明细查订货信息`).to.be.true;
		});
	});

	describe('130045,采购员给别的门店做采购订货.rankA', function () {
		before(async () => {
			await common.setGlobalParam('shopmanager_right', 0);
			await common.setDataPrivilege('aa', '-1'); //修改采购员 "记录权限" -- 所有门店
			await common.setDataPrivilege('shopmanager', '-2'); //修改店长 "记录权限" -- 本部门及下级所有门店
		});

		after(async () => {
			await common.loginDo();
			await common.setDataPrivilege('aa', '-2'); //修改采购员 "记录权限"
			await common.loginDo();
		});

		it('采购员给别的门店做采购订货', async function () {
			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});

			let otherShopid;
			// 中洲店订货
			let orderJson = basiceJsonparam.purchaseOrderJson();
			otherShopid = orderJson.invid = orderJson.shopid = BASICDATA.shopidZzd;
			if (orderJson.cash) orderJson.cashaccountid = BASICDATA[`cashAccount${otherShopid}`];
			if (orderJson.card) orderJson.cardaccountid = BASICDATA[`bankAccount${otherShopid}`];
			if (orderJson.remit) orderJson.remitaccountid = BASICDATA[`agencyAccount${otherShopid}`];
			let orderResult = await common.editBilling(orderJson);

			// 验证 '采购订货-按批次查界面采购员能看到其它门店的采购单,不需要通过门店条件查询一道'
			let batchList = await common.callInterface('ql-22102', format.qlParamsFormat({}, true));
			let isRight = false;
			for (let i = 0; i < batchList.dataList.length; i++) {
				if (batchList.dataList[i].id == orderResult.result.pk) {
					isRight = true;
					break;
				}
			}
			expect(isRight, `'采购订货-按批次查界面采购员能看到其它门店的采购单,不需要通过门店条件查询一道'(用例原文);但是这里看不到其他门店的采购单`).to.be.true;

			// 验证 '重新进入该订单,订货门店还是新增时输入的门店值'
			let orderInfo = await purRequestHandler.purOrderQueryBilling(orderResult.result.pk);
			expect(orderInfo.result, `重新进入采购订货单界面时，订货门店不是新增时输入的门店值`).to.includes({
				'invid': orderResult.params.invid,
				'show_invid': '中洲店'
			});

			// 验证 '其它门店的人员在按订货入库界面能看到采购员为该门店做的采购订单'
			await common.loginDo({
				'logid': '204',
				'pass': '000000'
			});
			let purByOrderList = await common.callInterface('ql-22306', format.qlParamsFormat({
				'dwid': orderResult.params.dwid,
			}, true));
			isRight = false;
			for (let i = 0; i < purByOrderList.dataList.length; i++) {
				if (purByOrderList.dataList[i].id == orderResult.result.pk) {
					isRight = true;
					break;
				}
			}
			expect(isRight, `'其它门店的人员在按订货入库界面能看到采购员为该门店做的采购订单'(用例原文);但是这里看不到其他门店的采购单`).to.be.true;

			//
		});
	});

});
