'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const billExp = require('../../getExp/billExp');
/*
 * 采购订货-按批次查
 */
describe('采购订货--按批次查', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('130022.按批次查-查询', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
		let param = format.qlParamsFormat({
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			// invdate1: common.getCurrentDate(),
			// invdate2: common.getCurrentDate(),
			dwid: sfRes.params.dwid,
			invid: qfRes.result.invid,
			respopid: qfRes.result.respopid,
			flag: 0,
			invalidflag: 0,
			styleid: qfRes.result.details[0].styleid,
			rem: qfRes.result.rem,
			shopid: qfRes.result.shopid,
		}, true);
		let qlRes = await common.callInterface('ql-22102', param);
		let exp = purRequestHandler.getPurOrderMainSearchListExp(qfRes.result);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('130014.2.采购订货-按批次查,整单备注检查.rankA', async function () {
		let json = format.jsonparamFormat(basiceJsonparam.purchaseOrderJson());
		let qlRes = await common.callInterface('ql-22102', format.qlParamsFormat({
			rem: json.rem,
		}, true));
		expect(qlRes.dataList.length, '查询无结果').to.above(0);
		qlRes.dataList.map((data) => {
			expect(data.rem).to.includes(json.rem);
		});
	});

	describe('130001.发货状态查询', function () {
		let orderRes, param;
		before(async () => {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			param = format.qlParamsFormat({
				id1: orderRes.result.billno,
				id2: orderRes.result.billno,
				flag: 0,
			}, true);
		});
		it('1.未发货', async function () {
			await listCheckFor130001(param, orderRes.result.pk);
		});
		it('2.部分发货', async function () {
			let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			qfRes.result.details[0].num = 4;
			qfRes.result.details[1].num = 6;
			await purRequestHandler.invinByOrder(qfRes.result); //按订货入库

			param.flag = 1;
			await listCheckFor130001(param, orderRes.result.pk);
		});
		it('3.全部发货', async function () {
			let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			qfRes.result.details[0].num = 6;
			qfRes.result.details[1].num = 14;
			await purRequestHandler.invinByOrder(qfRes.result);

			param.flag = 2;
			await listCheckFor130001(param, orderRes.result.pk);
		});
		it('4.多发', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			qfRes.result.details[0].num = 20;
			qfRes.result.details[1].num = 30;
			let sfRes = await purRequestHandler.invinByOrder(qfRes.result);

			param = format.qlParamsFormat({
				id1: orderRes.result.billno,
				id2: orderRes.result.billno,
				flag: 2,
			}, true);
			await listCheckFor130001(param, orderRes.result.pk);
		});
		async function listCheckFor130001(param, pk) {
			let qfRes = await purRequestHandler.purOrderQueryBilling(pk);
			let qlRes = await common.callInterface('ql-22102', param);
			let exp = purRequestHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		};
	});

	describe('130041.采购退货-仅退货', function () {
		let sfRes, qfRes;
		it('1.单据明细检查', async function () {
			let json = basiceJsonparam.purchaseOrderJson();
			[json.cash, json.card, json.remit] = [0, 0, 0];
			json.details.map((obj) => obj.num = -10);
			sfRes = await common.editBilling(json);
			qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.采购订货-按批次查', async function () {
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				dwid: sfRes.params.dwid,
				invid: qfRes.result.invid,
				respopid: qfRes.result.respopid,
				flag: 0,
				invalidflag: 0,
				rem: qfRes.result.rem,
				shopid: qfRes.result.shopid,
			}, true);
			let qlRes = await common.callInterface('ql-22102', param);
			let exp = purRequestHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('3.采购入库-按订货入库', async function () {
			let param = await format.qlParamsFormat({
				dwid: sfRes.params.dwid,
				styleid: sfRes.params.details[0].styleid,
				rem: sfRes.params.remark,
				invid: LOGINDATA.invid
			}, true);
			let qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库
			let exp = purRequestHandler.getInvinByOrderListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('4.采购入库-按批次查', async function () {
			let param = format.qlParamsFormat({
				puredmaincode: sfRes.result.billno,
				shopid: qfRes.result.invid,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			expect(qlRes.count, `采购入库-按批次查不显示仅退货单据`).to.equal('0');
		});
	});

	describe('130042.采购退货-退货并退款', function () {
		let sfRes, qfRes;
		it('1.单据明细检查', async function () {
			let json = basiceJsonparam.purchaseOrderJson();
			[json.cash, json.card, json.remit] = [-1000, -400, -600];
			json.details.map((obj) => obj.num = -10);
			sfRes = await common.editBilling(json);
			qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.采购订货-按批次查', async function () {
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				dwid: sfRes.params.dwid,
				invid: qfRes.result.invid,
				respopid: qfRes.result.respopid,
				flag: 0,
				invalidflag: 0,
				rem: qfRes.result.rem,
				shopid: qfRes.result.shopid,
			}, true);
			let qlRes = await common.callInterface('ql-22102', param);
			let exp = purRequestHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('3.采购入库-按订货入库', async function () {
			let param = await format.qlParamsFormat({
				dwid: sfRes.params.dwid,
				styleid: sfRes.params.details[0].styleid,
				rem: sfRes.params.remark,
				invid: LOGINDATA.invid
			}, true);
			let qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库
			let exp = purRequestHandler.getInvinByOrderListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('4.采购入库-按批次查', async function () {
			let param = format.qlParamsFormat({
				puredmaincode: sfRes.result.billno,
				shopid: qfRes.result.invid,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			expect(qlRes.count, `未找到订单号为${sfRes.result.billno}的预付款单`).to.equal('1');
		});
		it('5.核销信息', async function () {
			let prepayRes = await purRequestHandler.purQueryBilling(sfRes.result.prepayid); //获取预付款单的批次号
			let verifyResult = await purRequestHandler.getVerifyingBills(sfRes.params.dwid); //获取核销界面信息
			let exp = {
				cashaccountid: prepayRes.result.cashaccountid,
				totalmoney: prepayRes.result.totalmoney,
				optime: prepayRes.result.optime,
				remitaccountid: prepayRes.result.remitaccountid,
				daishou: '0',
				invname: qfRes.result.show_invid,
				dwname: qfRes.result.show_dwid,
				remark: prepayRes.result.remark,
				remit: sfRes.params.remit,
				dwfdname: '',
				agencyaccountid: '0',
				totalnum: prepayRes.result.totalnum,
				cardaccountid: prepayRes.result.cardaccountid,
				verifysum: prepayRes.result.finpayVerifysum,
				seller: prepayRes.result.show_deliver.split(',')[1],
				id: prepayRes.result.pk,
				balance: prepayRes.result.balance,
				modelClass: 'Map',
				cash: sfRes.params.cash,
				card: sfRes.params.card,
				// "card3": "0",
				// "card2": "0",
				balancetype: '1',
				balancetypename: '未付',
				billno: prepayRes.result.billno, //预付款批次号
			};
			common.isApproximatelyEqualAssert(exp, verifyResult.dataList[0]);
		});
	});

	describe('130043.对采购退货的单据进行按订货入库', async function () {
		let stockParam, stockRes, orderRes, sfRes;
		before(async () => {
			let json = basiceJsonparam.purchaseOrderJson();
			[json.cash, json.card, json.remit] = [0, 0, 0];
			json.details.map((obj) => obj.num = -10);
			json = format.jsonparamFormat(json);

			stockParam = format.qlParamsFormat({
				propdresStyleid: json.details[0].styleid,
				propdresColorid: json.details[0].colorid,
				propdresSizeid: json.details[0].sizeid,
				invid: LOGINDATA.invid
			}, false);
			stockRes = await common.callInterface('ql-1932', stockParam);

			orderRes = await common.editBilling(json);
		});
		it('1.按订货入库操作', async function () {
			let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(qfRes.result);
		});
		it('2.采购订货-按批次查', async function () {
			let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			let param = format.qlParamsFormat({
				id1: orderRes.result.billno,
				id2: orderRes.result.billno,
				flag: 2, //全部发货
				invalidflag: 0,
				shopid: LOGINDATA.invid,
			}, true);
			let qlRes = await common.callInterface('ql-22102', param);
			let exp = purRequestHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('3.采购入库-按批次查', async function () {
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				shopid: LOGINDATA.invid,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			let exp = purRequestHandler.getPurMainSearchListExp(qfRes.result);
			exp.puredmaincode = orderRes.result.billno;
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('4.当前库存', async function () {
			let stockRes2 = await common.callInterface('ql-1932', stockParam);
			let exp = common.addObject(stockRes.dataList[0], billExp.getCurInvListExp(sfRes)[`${sfRes.params.details[0].styleid}-${sfRes.params.details[0].colorid}-${sfRes.params.details[0].sizeid}`]);
			// console.log(`exp=${JSON.stringify(exp)}`);
			// 已经全部入库，待入库前后值不会变化
			common.isApproximatelyEqualAssert(exp, stockRes2.dataList[0]);
		});
	});

});
