'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const reqHandlerHelp = require('../../help/reqHandlerHelp');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

describe('采购订货新增修改--mainLine', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('130017.单据修改界面检查.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('130009.部分发货的单子不允许作废', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);

		let json = _.cloneDeep(qfRes.result);
		[json.cash, json.card, json.remit] = [0, 0, 0];
		json.details[0].num = 4;
		json.details[1].num = 6;
		await purRequestHandler.invinByOrder(json); //按订货入库--部分入库

		let cancelRes = await purRequestHandler.cancelPurOrderBill({
			pk: sfRes.result.pk,
			check: false,
		});
		expect(cancelRes.result).to.includes({
			error: '订单已入库，不允许作废'
		});
	});

	//130007均色均码与13008接口上同理,接口跳过
	it('130008.颜色尺码+新增订货.rankA', async function () {
		//新增厂商
		let dwRes = await common.callInterface('sf-2241', {
			jsonparam: basiceJsonparam.addManufacturerJson()
		});
		expect(dwRes, '新增厂商出错').to.have.property('val');

		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		});

		let json = basiceJsonparam.purchaseOrderJson();
		json.dwid = dwRes.val;
		json.details[0].styleid = goodRes.pk;
		let sfRes = await common.editBilling(json);
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('130010.不输入店员时在单据修改界面检查店员显示', async function () {
		let json = basiceJsonparam.purchaseOrderJson();
		json.respopid = ''; //不输入店员
		let sfRes = await common.editBilling(json);
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
		expect(qfRes.result, '采购订单明细错误').to.includes({
			respopid: LOGINDATA.id,
			show_respopid: `${LOGINDATA.code},${LOGINDATA.name}`,
		});

		let param = await format.qlParamsFormat({
			interfaceid: 'ql-22102',
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			invid: LOGINDATA.invid,
		}, true);

		let qlRes = await reqHandlerHelp.qlIFCHandler(param);
		//let qlRes = await common.callInterface('ql-22102', param); //采购订货-按批次查
		expect(qlRes.result.dataList[0].respstaffName, '采购订货-按批次查 店员显示错误').to.equal(LOGINDATA.name);
		param.interfaceid = 'ql-22306';
		qlRes = await reqHandlerHelp.qlIFCHandler(param);
		//qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库
		expect(qlRes.result.dataList[0].respstaffname, '采购入库-按订货入库 店员显示错误').to.equal(LOGINDATA.name);
	});

	it('130025.查看修改日志(修改记录)', async function () {
		//this.retries(2);
		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);

		//角色A修改
		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		await common.editBilling(qfRes.result);

		//角色B修改
		await common.loginDo();
		qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk); //需要重新获取单据信息
		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		let sfResEdit = await common.editBilling(qfRes.result);

		qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
		let editLog = await common.callInterface('qf-22101-5', {
			pk: sfRes.result.pk
		});
		let exp = {
			hashkey: sfRes.params.hashkey,
			optime: qfRes.result.optime,
			delopstaffName: '', //作废人
			createoptime: qfRes.result.createoptime,
			modelClass: 'PuredMain',
			createrName: qfRes.result.show_respopid.split(',')[1],
			fileid: '',
			opname: LOGINDATA.name,
			pk: sfRes.result.pk,
		};
		common.isApproximatelyEqualAssert(exp, editLog);
	});

	describe('作废', function () {
		let sfRes
		it('订单作废', async function () {
			sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());

			await purRequestHandler.cancelPurOrderBill(sfRes.result.pk); //作废订货单

			let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('130003.2.作废后,按批次查界面检查', async function () {
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				invalidflag: 1
			}, true);
			let qlRes = await common.callInterface('ql-22102', param);
			expect(qlRes.dataList[0]).to.includes({
				billno: sfRes.result.billno,
				invalidflag: '1',
				flag: '0'
			});
		});
		it('130003.1.作废后,按明细查界面检查', async function () {
			let param = format.qlParamsFormat({
				billno: sfRes.result.billno,
			}, true);
			let qlRes = await common.callInterface('ql-22103', param);
			expect(qlRes.count).to.equal('0');
		});
	});

	describe('130011.客户或供应商信息不允许修改.rankA', function () {
		let sfRes;
		before(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 0); //单据是否允许修改客户或厂商 不允许

			sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		});
		it('1.将厂商名称改为别的名称', async function () {
			let qfRes = await purRequestHandler.purOrderQueryBilling(sfRes.result.pk);
			qfRes.result.action = 'edit';
			qfRes.result.interfaceid = sfRes.params.interfaceid;
			qfRes.result.dwid = BASICDATA.dwidRt;
			let editRes = await common.editBilling(qfRes.result, false);
			expect(editRes.result).to.includes({
				error: '客户或供应商信息不允许修改'
			});
		});
		//将厂商名称从有改为无 前端控制
		it.skip('2.将厂商名称从有改为无-客户端控制', async function () { });
	});

});
