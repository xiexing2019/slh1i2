'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');

/*
 * 采购订货按明细查
 */
describe('采购订货--按明细查', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('130002,全局变量:颜色尺码+按明细查', async function () {
		let purOrderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let orderInfo = (await purRequestHandler.purOrderQueryBilling(purOrderResult.result.pk)).result;
		let detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
			'billno': purOrderResult.result.billno,
			'mainInvid': purOrderResult.params.invid,
			'mainDwid': purOrderResult.params.dwid,
		}, true));

		expect(detailsList.dataList[0], `采购订货按明细查数据有误1`).to.includes({
			'total': orderInfo.details[0].total,
			'rem': orderInfo.details[0].rem,
			'mainProdate': common.getCurrentDate('YY-MM-DD'),
			'invname': orderInfo.show_invid,
			'mat_name': (orderInfo.details[0].show_styleid).split(',')[1],
			'mat_code': (orderInfo.details[0].show_styleid).split(',')[0],
			'deliveryflag': '未入库',
			'provideridname': orderInfo.show_dwid,
			'propdresColorid': orderInfo.details[0].show_colorid,
			'num': orderInfo.details[0].num,
			'propdresSizeid': orderInfo.details[0].show_sizeid,
			'price': orderInfo.details[0].price,
			'billno': purOrderResult.result.billno,
		});
		expect(detailsList.dataList[1], `采购订货按明细查数据有误1`).to.includes({
			'total': orderInfo.details[1].total,
			'rem': orderInfo.details[1].rem,
			'mainProdate': common.getCurrentDate('YY-MM-DD'),
			'invname': orderInfo.show_invid,
			'mat_name': (orderInfo.details[1].show_styleid).split(',')[1],
			'mat_code': (orderInfo.details[1].show_styleid).split(',')[0],
			'deliveryflag': '未入库',
			'provideridname': orderInfo.show_dwid,
			'propdresColorid': orderInfo.details[1].show_colorid,
			'num': orderInfo.details[1].num,
			'propdresSizeid': orderInfo.details[1].show_sizeid,
			'price': orderInfo.details[1].price,
			'billno': purOrderResult.result.billno,
		});
	});

	it('130014.3.在采购订货-按明细查,明细备注检查.rankA', async function () {
		let json = basiceJsonparam.purchaseOrderJson();
		let qlRes = await common.callInterface('ql-22103', format.qlParamsFormat({
			rem: json.details[0].rem
		}, true));
		expect(qlRes.dataList.length, '查询无结果').to.above(0);
		qlRes.dataList.map((data) => {
			expect(data.rem).to.includes(json.details[0].rem);
		});
	});

	describe('130019,按明细查-添加差异数列', function () {
		it('1.少发', async function () {
			let purOperate = await purchaseByOrder('-1');
			let detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': purOperate.orderResult.billno,
				'mainInvid': purOperate.orderParams.invid,
				'mainDwid': purOperate.orderParams.dwid,
			}, true));
			expect(detailsList.dataList[0], `采购订货按明细查，差异字段有误`).to.includes({
				"leftnum": (Number(purOperate.orderParams.details[0].num) - Number(purOperate.params.details[0].num)).toString(),
				'deliveryflag': '部分入库'
			});
		});
		it('2.全部发货', async function () {
			let purOperate = await purchaseByOrder('0');
			let detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': purOperate.orderResult.billno,
				'mainInvid': purOperate.orderParams.invid,
				'mainDwid': purOperate.orderParams.dwid,
			}, true));
			expect(detailsList.dataList[0], `采购订货按明细查，差异字段有误`).to.includes({
				"leftnum": (Number(purOperate.orderParams.details[0].num) - Number(purOperate.params.details[0].num)).toString(),
				'deliveryflag': '全部入库'
			});
		});
		it('3.多发', async function () {
			let purOperate = await purchaseByOrder('1');
			let detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': purOperate.orderResult.billno,
				'mainInvid': purOperate.orderParams.invid,
				'mainDwid': purOperate.orderParams.dwid,
			}, true));
			expect(detailsList.dataList[0], `采购订货按明细查，差异字段有误`).to.includes({
				"leftnum": (Number(purOperate.orderParams.details[0].num) - Number(purOperate.params.details[0].num)).toString(),
				'deliveryflag': '全部入库'
			});
		});
		it('4.终结订单', async function () {
			let orderJson = basiceJsonparam.purchaseOrderJson();
			orderJson.details = [_.cloneDeep(orderJson.details[0])];
			let orderRes = await common.editBilling(orderJson);
			await purRequestHandler.endPurOrderBill(orderRes.result.pk); //终结订单

			let detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': orderRes.result.billno,
				'mainInvid': orderRes.params.invid,
				'mainDwid': orderRes.params.dwid,
			}, true));
			common.isApproximatelyEqualAssert({
				'num': orderRes.params.totalnum,
				'leftnum': orderRes.params.totalnum,
				'recvnum': '0',
				'total': orderRes.params.totalsum,
				'deliveryflag': '结束'
			}, detailsList.dataList[0]);
		});
	});

});

/*
 * 采购订货--按订货入库列表--订货明细--按订货入库
 */
async function purchaseByOrder(deliverType) {
	let orderJson = basiceJsonparam.purchaseOrderJson();
	orderJson.details = [_.cloneDeep(orderJson.details[0])];
	let purOrderResult = await common.editBilling(orderJson);

	let orderList = await common.callInterface('ql-22306', format.qlParamsFormat({
		'dwid': purOrderResult.params.dwid,
		'invid': purOrderResult.params.invid,
	}, true));
	let purOrder;
	for (let i = 0; i < orderList.dataList.length; i++) {
		if (orderList.dataList[i].billno == purOrderResult.result.billno) {
			purOrder = orderList.dataList[i];
			break;
		}
	}
	expect(purOrder != undefined, `按订货入库列表没有查询到数据`).to.be.true;

	let purOrderInfo = await purRequestHandler.purOrderQueryBilling(purOrder.id);
	expect(purOrderInfo.result, `按订货入库明细查询有误:${JSON.stringify(purOrderInfo)}`).to.not.have.property('error');
	let purchaseJson = format.jsonparamFormat(purchaseJsonByOrder(purOrderInfo.result, deliverType));
	let purchaseResult = await common.callInterface('sf-14212-1', {
		'jsonparam': purchaseJson,
	});
	expect(purchaseResult, `采购入库失败:${JSON.stringify(purchaseResult)}`).to.not.have.property('error');

	return {
		'orderResult': purOrderResult,
		'orderParams': purOrderResult.params,
		'result': purchaseResult,
		'params': purchaseJson
	}
}

/*
 * 根据按订货入库的信息组装采购入库jsonparam
 *
 * @params
 * orderInfo 按订货入库数据
 * deliverType 发货类型 -1:少发 0:全发 1:多发
 */
function purchaseJsonByOrder(orderInfo, deliverType) {
	let json = {
		interfaceid: 'sf-14212-1',
		billid: orderInfo.pk,
		deliver: orderInfo.respopid,
		remark: '按订货入库'
	};
	delete orderInfo.pk;
	delete orderInfo.respopid;
	delete orderInfo.shouldpay;
	delete orderInfo.actualpay; //开单后会更新
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(orderInfo), json));
	orderInfo.details.map((obj, index) => {
		jsonparam.details[index].billid = obj.pk || '';
		jsonparam.details[index].num = (Number(jsonparam.details[index].num) + Number(deliverType)).toString();
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});

	return jsonparam;
}
