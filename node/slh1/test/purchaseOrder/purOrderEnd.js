'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler.js');

//18-07-05 订单增加终结统计 http://jira.hzdlsoft.com:7082/browse/SLH-23124
describe('采购订货--订单终结--mianLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('130015.订单终结-未入库', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			await purReqHandler.endPurOrderBill(orderRes.result.pk); //终结订单

			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk); //获取单据详情
			// console.log(qfRes.result);

		});
		it('1.采购订货-按批次查', async function () {
			const batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `采购入库按批次查单据状态有误`).to.includes({
				"flag": "3",
				"deliveryflag": "结束"
			});

			const exp = purReqHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.采购订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': orderRes.result.billno,
				'mainInvid': orderRes.params.invid,
				'mainDwid': orderRes.params.dwid,
			}, true));
			const exp = purReqHandler.getPurOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
		it('3.采购入库-按订货入库', async function () {
			let purByOrderList = await common.callInterface('ql-22306', format.qlParamsFormat({
				'styleid': orderRes.params.details[0].styleid,
				'invid': orderRes.params.details[0].invid,
			}, true));
			let isRight = true;
			for (let i = 0; i < purByOrderList.dataList.length; i++) {
				if (purByOrderList.dataList[i].billno == orderRes.result.billno) {
					isRight = false;
					break;
				}
			}
			expect(isRight, `订单终结后，按订货入库列表还是有终结后的订单，错误`).to.be.true;
		});
		it.skip('4.厂商账款--超时先跳过', async function () {
			let prepaidList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'puredmaincode': orderRes.result.billno,
				'shopid': orderRes.params.invid,
			}, true));
			expect(prepaidList, `生成采购订单之后，在采购入库--按批次查列表没有查询到唯一的预付款单，错误`).to.includes({
				'count': '1'
			});
			let balanceList = await common.callInterface('ql-13471', format.qlParamsFormat({
				'dwid': orderRes.params.dwid,
				'shopid': orderRes.params.invid,
			}));
			let balanceInfo;
			for (let i = 0; i < balanceList.dataList.length; i++) {
				if (balanceList.dataList[i].billno == prepaidList.dataList[0].billno) {
					balanceInfo = balanceList.dataList[i];
				}
			}
			expect(balanceInfo, `厂商门店帐预付款单没有显示已终结的采购订单，错误`).to.includes({
				"totalmoney": "0",
				"remark": "预付款",
				"typename": (USEECINTERFACE == 1) ? "进货单" : "采购进货",
				"finpayPaysum": orderRes.params.totalsum.toString(),
				"shopname": LOGINDATA.depname,
				"billno": prepaidList.dataList[0].billno,
				"prodate": common.getCurrentDate('YY-MM-DD'),
			});
		});
		it('130016.1.重复终结订单', async function () {
			let stopOrder = await purReqHandler.endPurOrderBill({
				pk: orderRes.result.pk,
				check: false,
			}); //重复终结订单
			expect(stopOrder.result, `订单已结束，应该是不能重复结束的，这里可以重复结束`).to.includes({
				"error": "采购订单已终结"
			});
		});
	});

	describe('130016.订单终结-部分入库', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk);

			//按订货入库-部分入库
			const orderInfo = purchaseJsonByOrder(qfRes.result, '-1');
			await common.editBilling(orderInfo); //部分入库

			await purReqHandler.endPurOrderBill({
				pk: orderRes.result.pk,
				errorMsg: '终结订单失败(部分入库)',
			});

			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk); //获取单据详情
			// console.log(qfRes.result);

		});
		it('1.采购订货-按批次查', async function () {
			const batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `采购入库按批次查单据状态有误`).to.includes({
				"flag": "3",
				"deliveryflag": "结束"
			});

			const exp = purReqHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.采购订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': orderRes.result.billno,
				'mainInvid': orderRes.params.invid,
				'mainDwid': orderRes.params.dwid,
			}, true));
			const exp = purReqHandler.getPurOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	describe('130017.订单终结-全部入库', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk);

			//按订货入库-全部入库
			const orderInfo = purchaseJsonByOrder(qfRes.result, '0');
			await common.editBilling(orderInfo); //全部入库

			await purReqHandler.endPurOrderBill({
				pk: orderRes.result.pk,
				errorMsg: '终结订单失败(全部入库)',
			});

			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk); //获取单据详情
			// console.log(qfRes.result);

		});
		it('1.采购订货-按批次查', async function () {
			const batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `采购入库按批次查单据状态有误`).to.includes({
				"flag": "3",
				"deliveryflag": "结束"
			});

			const exp = purReqHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.采购订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': orderRes.result.billno,
				'mainInvid': orderRes.params.invid,
				'mainDwid': orderRes.params.dwid,
			}, true));
			const exp = purReqHandler.getPurOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	describe('130018.订单终结-多发', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk);

			//按订货入库-多发
			const orderInfo = purchaseJsonByOrder(qfRes.result, '1');
			await common.editBilling(orderInfo); //全部入库

			await purReqHandler.endPurOrderBill({
				pk: orderRes.result.pk,
				errorMsg: '终结订单失败(多发)',
			});

			qfRes = await purReqHandler.purOrderQueryBilling(orderRes.result.pk); //获取单据详情
			// console.log(qfRes.result);

		});
		it('1.采购订货-按批次查', async function () {
			const batchList = await common.callInterface('ql-22102', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `采购入库按批次查单据状态有误`).to.includes({
				"flag": "3",
				"deliveryflag": "结束"
			});

			const exp = purReqHandler.getPurOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.采购订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-22103', format.qlParamsFormat({
				'billno': orderRes.result.billno,
				'mainInvid': orderRes.params.invid,
				'mainDwid': orderRes.params.dwid,
			}, true));
			const exp = purReqHandler.getPurOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	it('130065,终结的订单不允许再作废', async function () {
		// 终结未入库订单，并作废
		let orderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		await purReqHandler.endPurOrderBill(orderResult.result.pk); //终结订单

		await disableBillAndCheckPurednum(orderResult);

		// 终结部分入库订单，并作废
		orderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let orderInfo = await purReqHandler.purOrderQueryBilling(orderResult.result.pk);
		orderInfo = purchaseJsonByOrder(orderInfo.result, '-1');
		let purchaseResult = await common.editBilling(orderInfo);
		await purReqHandler.endPurOrderBill({
			pk: orderResult.result.pk,
			errorMsg: '2.终结订单失败(部分入库)'
		}); //终结订单

		await disableBillAndCheckPurednum(orderResult);

		// 终结全部入库订单，并作废
		orderResult = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		orderInfo = await purReqHandler.purOrderQueryBilling(orderResult.result.pk);
		orderInfo = purchaseJsonByOrder(orderInfo.result, '0');
		purchaseResult = await common.editBilling(orderInfo);
		await purReqHandler.endPurOrderBill({
			pk: orderResult.result.pk,
			errorMsg: '3.终结订单失败(全部发货)'
		}); //终结订单

		await disableBillAndCheckPurednum(orderResult);

	});

	it('130047.订单终结后检查修改日志', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		await purReqHandler.endPurOrderBill(sfRes.result.pk); //终结订单
		let qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk);

		let editLog = await common.callInterface('qf-22101-5', {
			pk: sfRes.result.pk
		});
		let exp = {
			hashkey: sfRes.params.hashkey,
			optime: qfRes.result.optime,
			delopstaffName: '', //作废人
			createoptime: qfRes.result.createoptime,
			modelClass: 'PuredMain',
			createrName: qfRes.result.show_respopid.split(',')[1],
			fileid: '',
			opname: LOGINDATA.name,
			pk: sfRes.result.pk,
		};
		common.isApproximatelyEqualAssert(exp, editLog);
	});
});


/*
 * 根据按订货入库的信息组装采购入库jsonparam
 *
 * @params
 * orderInfo 按订货入库数据
 * deliverType 发货类型 -1:少发 0:全发 1:多发
 */
function purchaseJsonByOrder(orderInfo, deliverType) {
	let json = {
		interfaceid: 'sf-14212-1',
		billid: orderInfo.pk,
		deliver: orderInfo.respopid,
		remark: '按订货入库'
	};
	delete orderInfo.pk;
	delete orderInfo.respopid;
	delete orderInfo.shouldpay;
	delete orderInfo.actualpay; //开单后会更新
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(orderInfo), json));
	orderInfo.details.map((obj, index) => {
		jsonparam.details[index].billid = obj.pk || '';
		jsonparam.details[index].num = (Number(jsonparam.details[index].num) + Number(deliverType)).toString();
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});

	return jsonparam;
}

/*
 * 作废已入库、部分入库、未入库的采购订货单
 * 并比较作废前后当前库存列表中待入库字段
 * 期望：1.作废不成功  2.待入库字段不变
 *
 * @params
 * orderResult 采购订货相关信息
 */
async function disableBillAndCheckPurednum(orderResult) {
	let searchCondition = {
		'invid': orderResult.params.invid,
		'propdresColorid': orderResult.params.details[0].colorid,
		'propdresSizeid': orderResult.params.details[0].sizeid,
		'propdresStyleid': orderResult.params.details[0].styleid,
	};
	let currInvList = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition));
	expect(currInvList, `查询当前库存失败:${JSON.stringify(currInvList)}`).to.not.have.property('error');

	let disableBill = await common.callInterface('cs-22102', {
		'pk': orderResult.result.pk
	});
	expect(disableBill, `已终结订单应该是不允许作废的，这里作废成功了`).to.includes({
		"error": "订单已终结，不允许作废"
	});

	let currInvListAft = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition));
	expect(currInvListAft.dataList[0].purednum, `已终结的订单，作废操作后，当前库存中的待入库数量变了，错误`).to.be.equal(currInvList.dataList[0].purednum);
}