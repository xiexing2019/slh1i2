'use strict';

const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const inOutHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler.js');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
describe('门店调入-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	// 关于qf-1863-3的调用，限制比较多，接口又没有详细说明
	// 只传pk得到一个结果，传pk、action('add'/'edit')又是一个结果，传pk、action、actid又是另一个结果
	// 为防止移动端开发人员调用失误，这个用例UI也需要验证一遍
	if (USEECINTERFACE == 1) {
		//二代qf返回为开单日期,客户端会在进入详细时将prodata改成当天
		it('140006,门店调拨-在途调拨，默认日期检查', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});

			// 门店调出
			let outJson = basiceJson.outJson();
			outJson.prodate = common.getDateString([0, 0, -1]);
			let outRes = await common.editBilling(outJson);

			// 常青店登录
			await common.loginDo();
			let onRoadDetail = await inOutHandler.shopInQueryBilling({
				pk: outRes.result.pk,
				billno: outRes.result.billno, //调出单批次号
			});
			expect(onRoadDetail.result, `在途调拨，默认日期应该是今天，而不是调出日期`).to.includes({
				'prodate': common.getCurrentDate()
			});
		});
	};

	it.skip('140023.门店调入-按款号汇总 查询.rankA', async function () {
		await common.loginDo({
			'logid': '200',
		});
		let outRes = await common.editBilling(basiceJson.outJson());

		await common.loginDo();
		let styleInfo = await common.fetchMatInfo(outRes.params.details[0].styleid); //获取货品信息

		let param = format.qlParamsFormat({
			stylecode: outRes.params.details[0].styleid,
			stylename: styleInfo.name,
			brandid: styleInfo.brandid,
			invidref: outRes.params.shopid,
			shopid: outRes.params.invidref,
		}, true);
		//门店调入-按款号汇总,调入单信息
		let [qlRes1, qfRes] = await Promise.all([common.callInterface('ql-1869', param),
		inOutHandler.shopInQueryBilling({
			pk: outRes.result.pk,
			billno: outRes.result.billno, //调出单批次号
		})]);
		let isNull = qlRes1.dataList.length <= 0; //一天的第一次查询可能没数据

		let inRes = await inOutHandler.shopIn(qfRes.result); //调入

		let qlRes2 = await common.callInterface('ql-1869', param);
		let exp = {
			shop: '常青店',
			stylename: styleInfo.name,
			nums: common.add(isNull ? 0 : qlRes1.dataList[0].nums, outRes.params.details[0].num),
			stylecode: styleInfo.code,
			brandid: styleInfo.show_brandid,
			totals: common.add(isNull ? 0 : qlRes1.dataList[0].totals, outRes.params.details[0].total),
			invidref: '中洲店'
		};
		common.isApproximatelyEqualAssert(exp, qlRes2.dataList[0]);
		expect(qlRes2.count).to.equal('1'); //同一款号相同调出门店和相同调入门店的合并
	});

	it('140028.非总经理登录-只能查看调入门店为本门店的汇总值.rankA', async function () {
		await common.loginDo({ //常青店 店长登陆
			'logid': '004',
			'pass': '000000'
		});
		let param = format.qlParamsFormat({
			shopid: BASICDATA.shopidZzd, //调入门店
			search_list: 0
		}, false);
		let qlRes = await common.callInterface('ql-1869', param); //门店调入-按款号汇总
		expect(qlRes.count, '查询调入门店为其他门店结果出错').to.equal('0');

		param.shopid = BASICDATA.shopidCqd;
		qlRes = await common.callInterface('ql-1869', param);
		expect(Number(qlRes.count), '查询调入门店为本门店结果出错').to.above(0);
	});

	it('140031.可以补录以前的单据', async function () {
		await common.loginDo({
			'logid': '200',
			'pass': '000000'
		});
		let outJson = format.jsonparamFormat(basiceJson.outJson());
		outJson.prodate = common.getDateString([0, 0, -1]); // 调出日期为昨天
		let outRes = await common.editBilling(outJson);

		await common.loginDo();
		let qfRes = await inOutHandler.shopInQueryBilling({
			pk: outRes.result.pk,
			billno: outRes.result.billno, //调出单批次号
		}); //获取调入单信息
		let inRes = await inOutHandler.shopIn(qfRes.result); //调入
		qfRes = await common.callInterface('qf-14214-1', { //获取调拨入库单据信息
			pk: inRes.result.pk
		});
		common.isAqualOptimeAssert(outJson.prodate, qfRes.prodate);
	});

	// 这个describe中每一个块都是相互关联的，不要随意变动其顺序
	describe('140001.门店调入.rankA', function () {
		let outInfo, onRoadList, onRoadDetail, inInfo, currInvSC = {
			'propdresStyleFlag': '0',
		};

		it('1.新增调出', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});

			let outJson = format.jsonparamFormat(basiceJson.outJson());
			currInvSC.invid = LOGINDATA.invid;
			currInvSC.propdresSizeid = outJson.details[0].sizeid;
			currInvSC.propdresColorid = outJson.details[0].colorid;
			currInvSC.propdresStyleid = outJson.details[0].styleid;

			// 货品查询-当前库存 查询中洲店agc001、白色、M码
			let currInvNum = await common.callInterface('ql-1932', format.qlParamsFormat(currInvSC, false));
			expect(currInvNum, `当前库存列表查询出错:${JSON.stringify(currInvNum)}`).to.not.have.property('error');

			// 门店调出
			outInfo = await common.editBilling(outJson);

			// 调出后验证库存变化
			let currInvNumAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvSC, false));
			if (currInvNum.count == '0') {
				expect(Number(currInvNumAft.dataList[0].invnum), `当前库存列表统计库存信息有误1`).to.equal(Number(-outJson.details[0].num));
			} else {
				expect(Number(currInvNumAft.dataList[0].invnum), `当前库存列表统计库存信息有误2`).to.equal(common.sub(currInvNum.dataList[0].invnum, outJson.details[0].num));
			};
		});

		it('2.在途调拨列表', async function () {
			await common.loginDo();
			onRoadList = await common.callInterface('ql-1867', format.qlParamsFormat({
				'id1': outInfo.result.billno,
				'id2': outInfo.result.billno,
				'shopid': outInfo.params.shopid, //调出地
			}));
			common.isApproximatelyEqualAssert({
				'totalmoney': outInfo.params.totalmoney,
				'invname': '中洲店',
				'invalidflag': 0,
				'remark': outInfo.params.remark,
				'inventoryrefName': '常青店',
				'totalnum': outInfo.params.totalnum,
				'billno': outInfo.result.billno,
				'prodate': common.getCurrentDate('YY-MM-DD')
			}, onRoadList.dataList[0]);
		});

		it('3.在途调拨明细', async function () {
			onRoadDetail = await inOutHandler.shopInQueryBilling(onRoadList.dataList[0].id).then(res => res.result); //门店调入-在途调拨详细页面
			if (USEECINTERFACE == 2) {
				[outInfo.params.shopid, outInfo.params.invidref] = [outInfo.params.invidref, outInfo.params.shopid];
				outInfo.params.type = 16;
				outInfo.params.invid = LOGINDATA.invid;
			};
			common.isApproximatelyEqualAssert(outInfo.params, onRoadDetail, ['hashkey']);
		});

		it('4.调入', async function () {
			// 先查询常青店agc001、白色、M码当前库存
			currInvSC.invid = LOGINDATA.invid;
			let currInvNum = await common.callInterface('ql-1932', format.qlParamsFormat(currInvSC, false));
			expect(currInvNum, `当前库存列表查询出错:${JSON.stringify(currInvNum)}`).to.not.have.property('error');

			// -----------------------调入操作开始---------------------------
			inInfo = await inOutHandler.shopIn(onRoadDetail);
			// -----------------------调入操作结束---------------------------

			//验证调入后当前库存变化
			let currInvNumAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvSC, false));
			common.isApproximatelyEqualAssert(currInvNumAft.dataList[0], {
				'invnum': common.add(currInvNum.dataList[0].invnum, onRoadDetail.details[0].num),
				'onroadnum': common.sub(currInvNum.dataList[0].onroadnum, onRoadDetail.details[0].num),
			});
		});

		it('5.在途调拨列应该查询不到已调入的单据', async function () {
			let onRoadList = await common.callInterface('ql-1867', format.qlParamsFormat({
				'id1': outInfo.result.billno,
				'id2': outInfo.result.billno,
				'shopid': outInfo.params.shopid,
			}));
			expect(onRoadList, `在途调拨列表查询到了已调入的单据，错误`).to.includes({
				'count': '0'
			});
		});
	});

	describe('140002.门店调入-按批次查.rankA', function () {
		let outRes;
		it('中洲店调出', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});

			// 门店调出
			let outJson = format.jsonparamFormat(basiceJson.outJson());
			outRes = await common.editBilling(outJson);
		});

		it('常青店未调入的在途调拨单,在按批次查处不应该查询到相应数据', async function () {
			await common.loginDo();

			let batchList = await common.callInterface('ql-1863', format.qlParamsFormat({
				'billno1': outRes.result.billno,
				'billno2': outRes.result.billno,
				'invidref': BASICDATA.shopidZzd,
				'shopid': BASICDATA.shopidCqd,
			}, true));
			expect(batchList, `门店调入-按批次查列表查询到了未调入的在途调拨信息，错误`).to.includes({
				'count': '0'
			});
		});

		it('调入,并验证按批次信息', async function () {
			// 调入
			let onRoadDetail = await inOutHandler.shopInQueryBilling({
				pk: outRes.result.pk,
				billno: outRes.result.billno, //调出单批次号
			});
			let inInfo = await inOutHandler.shopIn(onRoadDetail.result);

			// 按批次查
			let batchList = await common.callInterface('ql-1863', format.qlParamsFormat({
				'billno1': outRes.result.billno,
				'billno2': outRes.result.billno,
				'id1': inInfo.result.billno,
				'id2': inInfo.result.billno,
				'invidref': BASICDATA.shopidZzd,
				'shopid': BASICDATA.shopidCqd,
			}, true));
			common.isApproximatelyEqualAssert({
				'invname': '常青店',
				'invnameref': '中洲店',
				'remark': inInfo.params.remark,
				'type': outRes.result.billno,
				'totalnum': inInfo.params.totalnum.toString(),
				'prodate': common.getCurrentDate('YY-MM-DD'),
				'purmoney': outRes.params.totalmoney.toString(),
				'billno': inInfo.result.billno
			}, batchList.dataList[0]);
		});
	});

	describe('140003.门店调入-按明细查.rankA', function () {
		let outInfo;
		it('1.中洲店调出', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});

			// 门店调出
			let outJson = format.jsonparamFormat(basiceJson.outJson());
			outInfo = await common.editBilling(outJson);
		});
		//二代ql-1866接口 调出单批次查询条件失效
		it('2.常青店未调入的在途调拨单，在按明细查处不应该查询到相应的数据', async function () {
			await common.loginDo();
			let detailList = await common.callInterface('ql-1866', format.qlParamsFormat({
				'outbillno': outInfo.result.billno,
				'invidref': BASICDATA.shopidZzd,
				'shopid': BASICDATA.shopidCqd,
				'stylename': 'auto001',
			}, true));
			expect(detailList, `门店调入-按明细查列表查询到了未调入的在途调拨信息，错误`).to.includes({
				'count': '0'
			});
		});

		it('3.调入,并验证按明细查信息', async function () {
			// 调入
			let onRoadDetail = await inOutHandler.shopInQueryBilling({
				pk: outInfo.result.pk,
				billno: outInfo.result.billno,
			});
			let inInfo = await inOutHandler.shopIn(onRoadDetail.result);

			// 按明细查
			let detailList = await common.callInterface('ql-1866', format.qlParamsFormat({
				'outbillno': outInfo.result.billno,
				'billno': inInfo.result.billno,
				'invidref': BASICDATA.shopidZzd,
				'shopid': BASICDATA.shopidCqd,
				'stylename': 'auto001',
			}, true));
			common.isApproximatelyEqualAssert({
				'total': outInfo.params.totalmoney,
				'stylename': 'auto001',
				'stylecolor': '白色',
				'shopname': '常青店',
				'invidrefname': '中洲店',
				'prodate': common.getCurrentDate('YY-MM-DD'),
				'stylesize': 'M',
				'num': inInfo.params.totalnum,
				'billno': inInfo.result.billno,
				'outbillno': outInfo.result.billno
			}, detailList.dataList[0]);
		});
	});

	describe('140030.调入单作废后检查.rankA', function () {
		let outRes, inRes, invParam, invInfo1; //调出单,调入单,当前库存参数,当前库存结果
		//获取当前库存信息 {}
		async function getCurInvnumInfo(param) {
			let invInfo = {};
			let result = await common.callInterface('ql-1932', param);
			result.dataList.map((obj) => {
				invInfo[`${obj.inventoryName}-invnum`] = obj.invnum; //库存
				invInfo[`${obj.inventoryName}-onroadnum`] = obj.onroadnum; //在途数
			});
			return invInfo;
		};
		before(async () => {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});
			outRes = await common.editBilling(basiceJson.outJson()); //调出

			invParam = format.qlParamsFormat({
				'propdresStyleid': outRes.params.details[0].styleid,
				'propdresColorid': outRes.params.details[0].colorid,
				'propdresSizeid': outRes.params.details[0].sizeid,
			}, false);
			invInfo1 = await getCurInvnumInfo(invParam);

			await common.loginDo();
			let qfRes = await inOutHandler.shopInQueryBilling({
				pk: outRes.result.pk,
				billno: outRes.result.billno,
			}); //获取调入单信息
			inRes = await inOutHandler.shopIn(qfRes.result); //调入
		});
		it('1.作废', async function () {
			await inOutHandler.cancelShopInBill(inRes.result.pk);
		});
		it('2.作废后检查在途单', async function () {
			let qlRes = await common.callInterface('ql-1867', format.qlParamsFormat({ //门店调入-在途调拨
				id1: outRes.result.billno,
				id2: outRes.result.billno,
				invidref: outRes.params.invidref,
				shopid: outRes.params.shopid,
			}));
			expect(qlRes.count, `作废调入单后,在途单未还原`).to.equal('1');
		});
		it('3.作废后检查调出单', async function () {
			let param = format.qlParamsFormat({
				id1: outRes.result.billno,
				id2: outRes.result.billno,
				shopid: outRes.params.shopid,
				invidref: outRes.params.invidref,
				flag: 0
			}, true);
			let qlRes = await common.callInterface('ql-1862', param); //门店调出-按批次查
			expect(qlRes.dataList[0]).to.includes({
				billno: outRes.result.billno,
				flag: '未接收'
			});
		});
		it('4.作废后检查库存和在途数', async function () {
			let invInfo2 = await getCurInvnumInfo(invParam);
			common.isApproximatelyEqualAssert(invInfo1, invInfo2);
		});
	});

	describe('140029,修改调出单的接收门店', function () {
		let outInfo;
		before(async () => {
			await common.loginDo({
				logid: '200'
			});
			// 门店调出 中洲店调出到仓库店
			let outJson = basiceJson.outJson();
			outJson.invidref = BASICDATA.shopidCkd;
			outInfo = await common.editBilling(outJson);
		});
		//有bug
		// 前端调入界面没有选择调入门店选择框，不存在这种场景，先跳过
		it.skip('1.仓库店登录，并调入，且调入时修改调入门店为常青店-前端界面没有调入门店选择框', async function () {
			TESTCASE = {
				describe: "在途调拨时修改调入门店",
				expect: "提示和调出单门店不一致，请退出查询后重新接收"
			}
			await common.loginDo({
				'logid': '100',
			});

			// 调入
			let onRoadDetail = await inOutHandler.shopInQueryBilling({
				pk: outInfo.result.pk,
				billno: outInfo.result.billno,
			});
			onRoadDetail.result.invidref = BASICDATA.shopidCqd;

			let inResult = await inOutHandler.shopIn(onRoadDetail.result, false);
			expect(inResult.result, `门店调入成功:${JSON.stringify(inResult)}`).to.includes({
				"error": "和调出单门店不一致，请退出查询后重新接收"
			});
		});

		it('2.修改调出单，将调入门店改为常青店，再去仓库店查看在途调拨那里应该没有对应的在途调拨单', async function () {
			TESTCASE = {
				describe: "修改调出单的调入门店从仓库店到常青店，仓库店店登录是否有调拨单",
				expect: "仓库店在途调拨没有对应的在途调拨单"
			}
			// 修改调出单接收门店
			await common.loginDo({
				'logid': '200',
			});
			let outJson = await inOutHandler.shopOutQueryBilling(outInfo.result.pk);
			outJson.result.interfaceid = 'sf-1862-1';
			outJson.result.invidref = BASICDATA.shopidCqd;
			outJson.result.action = 'edit';
			let outResult = await common.editBilling(outJson.result);
			// 仓库店登录，验证在途调拨列表无法查询到之前的相应的调拨信息
			await common.loginDo({
				'logid': '100',
				'pass': '000000'
			});
			let onRoadList = await common.callInterface('ql-1867', format.qlParamsFormat({
				'id1': outInfo.result.billno,
				'id2': outInfo.result.billno,
				'shopid': outInfo.params.shopid,
			}));
			expect(onRoadList.count, `${LOGINDATA.name}在途调拨列表错误`).to.equal('0');

			// 常青店登录，验证在途调拨列表有调拨信息
			await common.loginDo();
			onRoadList = await common.callInterface('ql-1867', format.qlParamsFormat({
				'id1': outInfo.result.billno,
				'id2': outInfo.result.billno,
				'shopid': outInfo.params.shopid,
			}));
			expect(onRoadList.count, `${LOGINDATA.name}在途调拨列表错误`).to.equal('1');
		});
	});

	describe.skip('关于停用款号以及颜色尺码的调入验证--补', function () {
		it('140036,停用的款号也要能调入', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});

			// 新增款号
			let goodResult = await basicReqHandler.editStyle({
				jsonparam: basiceJson.addGoodJson(),
			});
			expect(goodResult, `新增货品失败:${JSON.stringify(goodResult)}`).to.not.have.property('error');

			// 中洲店门店调出 并将相应货品停用
			let outJson = format.jsonparamFormat(basiceJson.outJson());
			outJson.details[0].styleid = goodResult.pk;
			let outResult = await common.callInterface('sf-1862-1', {
				'jsonparam': outJson
			});
			expect(outResult, `中洲店门店调出错误:${JSON.stringify(outResult)}`).to.not.have.property('error');
			let disableGood = await common.callInterface('cs-1511-33', { //停用货品
				pk: goodResult.pk
			});
			expect(disableGood, `停用货品失败:${JSON.stringify(disableGood)}`).to.not.have.property('error');

			// 验证常青店登录，做调入，且调入要成功
			await common.loginDo();
			let onRoadDetail = await common.callInterface('qf-1863-3', {
				'pk': outResult.pk
			});
			expect(onRoadDetail, `在途调拨列表点进去列表有误:${JSON.stringify(onRoadDetail)}`).to.not.have.property('error');
			await inOutHandler.shopIn(onRoadDetail);

		});

		it.skip('140037,款号的颜色尺码停用了，也要能调入', async function () {

		});
	});
});
