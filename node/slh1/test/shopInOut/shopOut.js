'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const shopInOutRequest = require('../../help/shopInOutHelp/shopInOutRequestHandler');
const storesBinding = require('../../../data/warehouseStoresBinding');
const printRequestHandler = require('../../help/printHelp/printRequestHandler');
const reqHandler = require('../../help/reqHandlerHelp');

/*
 *  门店调出
 *  中洲店调往常青店
 */
describe('门店调出-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo({
			'logid': '200',
			'pass': '000000',
		});
		BASICDATA = await getBasicData.getBasicID();
	});

	it('150003.批量调出', async function () {
		let json = format.jsonparamFormat(basiceJson.outJson());

		let param = format.qlParamsFormat({
			'propdresStyleid': json.details[0].styleid,
			'propdresColorid': json.details[0].colorid,
			'propdresSizeid': json.details[0].sizeid,
		}, false);
		let invInfo1 = await getCurInvnumInfo(param); //货品管理-当前库存

		let sfRes = await common.editBilling(json);
		let qfRes = await shopInOutRequest.shopOutQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);

		let invInfo2 = await getCurInvnumInfo(param);
		invInfo1['常青店-onroadnum'] = common.add(invInfo1['常青店-onroadnum'], json.details[0].num);
		invInfo1['中洲店-invnum'] = common.sub(invInfo1['中洲店-invnum'], json.details[0].num);
		common.isApproximatelyEqualAssert(invInfo1, invInfo2);
	});

	it('150001.按批次查', async function () {
		let sfRes = await common.editBilling(basiceJson.outJson());
		let param = format.qlParamsFormat({
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			shopid: sfRes.params.shopid,
			invidref: sfRes.params.invidref,
			remark: sfRes.params.remark,
			invalidflag: 0,
			flag: 0
		}, true);
		let qlRes = await common.callInterface('ql-1862', param);
		let exp = getMainSearchExp(sfRes);
		common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);
	});

	it('150010.按明细查.rankA', async function () {
		await common.setGlobalParam('movesumprice', 1); //调拨核算价格 调拨按销价1核算
		let sfRes = await common.editBilling(basiceJson.outJson());
		let qfRes = await shopInOutRequest.shopOutQueryBilling(sfRes.result.pk); //获取调出单信息
		let styleInfo = await common.fetchMatInfo(sfRes.params.details[0].styleid);

		let param = format.qlParamsFormat({
			propdresStyleid: sfRes.params.details[0].styleid,
			concatPropdresStyleCodePropdresStyleName: styleInfo.name,
			propdresColorid: sfRes.params.details[0].colorid,
			propdresSizeid: sfRes.params.details[0].sizeid,
			mainShopid: sfRes.params.shopid,
			mainInvidref: sfRes.params.invidref,
			mainFlag: 0,
			propdresStyleClassid: styleInfo.classid,
		}, true);
		let qlRes = await common.callInterface('ql-1865', param);
		let exp = {
			optime: sfRes.params.hashkey, //includes
			total: qfRes.result.details[0].total,
			propdresStyleStdprice1: styleInfo.stdprice1,
			mainProdate: common.getCurrentDate('YY-MM-DD'),
			mainOpstaffName: LOGINDATA.name,
			propdresColorid: qfRes.result.details[0].show_colorid,
			mainInventoryrefName: '常青店',
			mainid: sfRes.result.pk,
			mainShopName: '中洲店',
			num: qfRes.result.details[0].num,
			price: styleInfo.stdprice1,
			propdresSizeid: qfRes.result.details[0].show_sizeid,
			unitName: styleInfo.show_unit,
			rem: qfRes.result.details[0].rem,
			propdresStyleBrandid: styleInfo.show_brandid,
			recvnum: '0',
			checkprenum: `-${qfRes.result.details[0].num}`,
			mainBillno: sfRes.result.billno,
			propdresStyleName: styleInfo.name,
			propdresStyleCode: styleInfo.code
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('150013.修改其他门店的未调入的调拨单', async function () {
		let sfRes = await common.editBilling(basiceJson.outJson());
		let qfRes = await shopInOutRequest.shopOutQueryBilling(sfRes.result.pk);

		await common.loginDo();
		qfRes.result.interfaceid = 'sf-1862-1';
		qfRes.result.action = 'edit';
		sfRes = await common.editBilling(qfRes.result, false);

		await common.loginDo({ //防止影响其他用例
			'logid': '200',
			'pass': '000000',
		});
		expect(sfRes.result).to.includes({
			error: '暂不允许修改其他门店调拨单'
		}, `修改其他门店调拨单错误 ${JSON.stringify(sfRes.result)}`);
	});

	it('150017.门店调出-按款号汇总 查询', async function () {
		let json = format.jsonparamFormat(basiceJson.outJson());
		let styleInfo = await common.fetchMatInfo(json.details[0].styleid);

		let param = format.qlParamsFormat({
			stylecode: json.details[0].styleid,
			stylename: styleInfo.name,
			brandid: styleInfo.brandid,
			invidref: json.invidref,
			shopid: json.shopid,
		}, true);
		let qlRes1 = await common.callInterface('ql-1868', param); //门店调出-按款号汇总

		let outRes = await common.editBilling(json);

		let qlRes2 = await common.callInterface('ql-1868', param);
		let exp = {
			shop: '中洲店',
			stylename: styleInfo.name,
			nums: common.add(qlRes1.dataList[0].nums || 0, outRes.params.details[0].num),
			stylecode: styleInfo.code,
			brandid: styleInfo.show_brandid,
			totals: common.add(qlRes1.dataList[0].totals || 0, outRes.params.details[0].total),
			invidref: '常青店'
		};
		common.isApproximatelyEqualAssert(exp, qlRes2.dataList[0]);
		expect(qlRes2.count).to.equal('1'); //同一款号相同调出门店和相同调入门店的合并
	});

	it('150022.非总经理登录-只能查看调出门店为本门店的汇总值', async function () {
		await common.loginDo({ //中洲店 店长登陆
			'logid': '204',
			'pass': '000000'
		});
		let param = {
			interfaceid: 'ql-1868',
			prodate1: common.getDateString([0, -1, 0]),
			prodate2: common.getCurrentDate(),
			shopid: BASICDATA.shopidCqd,
		};
		let qlRes = await reqHandler.qlIFCHandler(param);
		expect(qlRes.result.count, `查询调出门店为其他门店结果出错${JSON.stringify(qlRes.result.count)}`).to.equal('0');
		param.shopid = BASICDATA.shopidZzd;
		qlRes = await reqHandler.qlIFCHandler(param);
		expect(Number(qlRes.result.count), `查询调出门店为本门店结果出错${JSON.stringify(qlRes.result.count)}`).to.above(0);
	});

	it('150023.不输入店员时,店员默认为当前登录店员工号', async function () {
		let json = format.jsonparamFormat(basiceJson.outJson());
		json.deliver = '';
		let outRes = await common.editBilling(json);

		let param = format.qlParamsFormat({
			id1: outRes.result.billno,
			id2: outRes.result.billno,
			shopid: outRes.params.shopid,
			invidref: outRes.params.invidref,
		}, true);
		let qlRes = await common.callInterface('ql-1862', param);
		expect(qlRes.dataList[0]).to.includes({
			opname: LOGINDATA.name,
			//staffName: LOGINDATA.name,二代没有返回这个字段，先备注掉，验证的是seller字段，不重要
			seller: LOGINDATA.name,
		}, `按批次查结果错误 ${JSON.stringify(qlRes.dataList[0])}`);

		let qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
		assert.equal(LOGINDATA.id, qfRes.result.deliver, `单据明细店员错误 ${qfRes.result.deliver}!=${LOGINDATA.id}`);
	});

	it('150033,【门店调出-按批次查】增加调出日期列', async function () {
		await common.loginDo({
			'logid': '200',
			'pass': '000000'
		});

		let outJson = format.jsonparamFormat(basiceJson.outJson());
		outJson.prodate = common.getDateString([0, 0, -1]);
		let outResult = await common.editBilling(outJson);
		// 验证日期查询条件
		let batchList = await common.callInterface('ql-1862', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, -1]),
		}, false));
		let isSearchResultRight = true;
		let shortDate = common.getDateString([0, 0, -1]).substr(2)
		for (let i = 0; i < batchList.dataList.length; i++) {
			if (batchList.dataList[i].prodate != shortDate) isSearchResultRight = false;
		}
		expect(isSearchResultRight, `日期查询条件查询出来的结果有误`).to.be.true;

		// 验证日期、操作时间两个字段
		batchList = await common.callInterface('ql-1862', format.qlParamsFormat({
			'invidref': outJson.invidref,
			'shopid': outJson.shopid,
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, -1]),
			'id1': outResult.result.billno,
			'id2': outResult.result.billno
		}, false));
		let isTimeRight = true;
		if (batchList.dataList[0].prodate != shortDate) isTimeRight = false;
		if (!batchList.dataList[0].optime.includes(common.getCurrentDate('MM-DD'))) isTimeRight = false;
		expect(isTimeRight, `门店调出--按批次查列表日期、操作时间字段有误`).to.be.true;
	});

	it.skip('150036.修改单价为0保存', async function () {
		await common.setGlobalParam('fin_move_price', 1); //门店调拨是否可以填写价格 调拨有价格选项
		let outRes = await common.editBilling(basiceJson.outJson());

		let qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
		qfRes.result.interfaceid = 'sf-1862-1';
		qfRes.result.action = 'edit';
		qfRes.result.details[0].price = 0;
		outRes = await common.editBilling(qfRes.result);
		qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
		assert.equal(0, qfRes.result.details[0].price);
		let param = format.qlParamsFormat({
			id1: outRes.result.billno,
			id2: outRes.result.billno,
			shopid: outRes.params.shopid,
			invidref: outRes.params.invidref,
		}, true);
		let qlRes = await common.callInterface('ql-1862', param); //门店调出-按批次查
		let exp = getMainSearchExp(outRes);
		common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);
	})

	describe('作废', function () {
		after(async () => {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
		});
		it('150002.1.作废未接收的调拨单', async function () {
			let sfRes = await common.editBilling(basiceJson.outJson());
			await shopInOutRequest.cancelShopOutBill(sfRes.result.pk);
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				shopid: sfRes.params.shopid,
				invidref: sfRes.params.invidref,
				invalidflag: 1, //是否作废
				flag: 0 //一代 0:未接收 1:已接收 二代 1未接收，2 已接收，-1是已作废
			}, true);
			if (USEECINTERFACE == 2) param.flag = -1;
			let qlRes = await common.callInterface('ql-1862', param);
			expect(qlRes.dataList.length, `门店调出-按批次查ql-1862查询作废调拨单结果有误`).to.equal(1);
			let exp = getMainSearchExp(sfRes);
			exp.invalidflag = '是';
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('150002.2.作废全部接收的调拨单', async function () {
			let sfRes = await common.editBilling(basiceJson.outJson());
			await common.loginDo();
			let qfRes = await shopInOutRequest.shopInQueryBilling({
				pk: sfRes.result.pk,
				billno: sfRes.result.billno,
			});
			let res = await shopInOutRequest.shopIn(qfRes.result); //调入

			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			let cancelRes = await shopInOutRequest.cancelShopOutBill(sfRes.result.pk, false);
			expect(cancelRes).to.includes({
				error: '调入方已确认接收，不允许操作'
			});
		});
		it('150042.作废其他门店的调拨单', async function () {
			let outRes = await common.editBilling(basiceJson.outJson());
			await common.loginDo();
			let cancelRes = await shopInOutRequest.cancelShopOutBill(outRes.result.pk, false);
			expect(cancelRes).to.includes({
				error: '暂不允许修改其他门店调拨单'
			});
		});
	});

	describe('150031.删除所有款号明细', function () {
		it('1.新增调出单,不输款号,直接点保存', async function () {
			let json = format.jsonparamFormat(basiceJson.outJson());
			json.details = [];
			let outRes = await common.editBilling(json, false);
			expect(outRes.result).to.includes({
				error: '空单不允许保存'
			});
		});
		it('2.修改调出单,删除所有的款号,点保存', async function () {
			let outRes = await common.editBilling(basiceJson.outJson());
			let qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
			qfRes.result.interfaceid = 'sf-1862-1';
			qfRes.result.action = 'edit';
			qfRes.result.details = []; //删除明细
			outRes = await common.editBilling(qfRes.result, false);
			expect(outRes.result).to.includes({
				error: '空单不允许保存'
			});
		});
	});

	describe('门店绑定仓库，门店向仓库做调拨', function () {
		it('150034.1 门店向绑定仓库调拨.rankA', async function () {
			await common.loginDo({ //文一店总经理登陆
				'logid': '400',
				'pass': '000000',
			});

			let json = format.jsonparamFormat(basiceJson.outJson());
			json.shopid = BASICDATA.shopidWyd;
			json.invidref = BASICDATA.shopidCkd;

			let param = format.qlParamsFormat({
				'propdresStyleid': json.details[0].styleid,
				'propdresColorid': json.details[0].colorid,
				'propdresSizeid': json.details[0].sizeid,
			}, false);
			let invInfo1 = await getCurInvnumInfo(param); //获取库存
			let outRes = await common.editBilling(json);
			let invInfo2 = await getCurInvnumInfo(param);
			invInfo1['仓库店-onroadnum'] = common.add(invInfo1['仓库店-onroadnum'], json.details[0].num);
			invInfo1['仓库店-invnum'] = common.sub(invInfo1['仓库店-invnum'], json.details[0].num);
			common.isApproximatelyEqualAssert(invInfo1, invInfo2);
		});
		it('150034.2 绑定仓库向门店调拨.rankA', async function () {
			await common.loginDo({ //仓库店总经理登陆
				'logid': '100',
				'pass': '000000',
			});

			let json = format.jsonparamFormat(basiceJson.outJson());
			json.shopid = BASICDATA.shopidCkd;
			json.invidref = BASICDATA.shopidWyd;

			let param = format.qlParamsFormat({
				'propdresStyleid': json.details[0].styleid,
				'propdresColorid': json.details[0].colorid,
				'propdresSizeid': json.details[0].sizeid,
			}, false);
			let invInfo1 = await getCurInvnumInfo(param);

			let outRes = await common.editBilling(json);

			let invInfo2 = await getCurInvnumInfo(param);
			invInfo1['仓库店-onroadnum'] = common.add(invInfo1['仓库店-onroadnum'], json.details[0].num);
			invInfo1['仓库店-invnum'] = common.sub(invInfo1['仓库店-invnum'], json.details[0].num);
			common.isApproximatelyEqualAssert(invInfo1, invInfo2);
		});
		it('150035.门店绑定仓库调出门店为当前门店.rankA', async function () {
			await common.loginDo({ //文一店总经理登陆
				'logid': '400',
				'pass': '000000',
			});
			let json = basiceJson.outJson();
			json.shopid = BASICDATA.shopidWyd;
			json.invidref = BASICDATA.shopidCqd;
			let outRes = await common.editBilling(json);

			let qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
			expect(qfRes.result).to.includes({
				shopid: BASICDATA.shopidWyd,
				show_shopid: '文一店'
			});

			qfRes.result.interfaceid = 'sf-1862-1';
			qfRes.result.action = 'edit';
			outRes = await common.editBilling(qfRes.result);
			qfRes = await shopInOutRequest.shopOutQueryBilling(outRes.result.pk);
			expect(qfRes.result).to.includes({
				shopid: BASICDATA.shopidWyd,
				show_shopid: '文一店'
			});

			let param = format.qlParamsFormat({
				id1: outRes.result.billno,
				id2: outRes.result.billno,
				shopid: outRes.params.shopid,
				invidref: outRes.params.invidref,
			}, true);
			let qlRes = await common.callInterface('ql-1862', param); //门店调出-按批次查
			let exp = getMainSearchExp(outRes); //拼接门店调出-按批次查的期望值
			exp.shopname = '文一店';
			common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);
		});
	});

	describe('150044.打印后的单据再打印', function () {
		before(async () => {
			await common.setGlobalParam('cannotmodifyafterprint', 2); //单据打印后不允许修改 都不允许修改
		});
		after(async () => {
			await common.setGlobalParam('cannotmodifyafterprint', 0); //单据打印后不允许修改 不限制
		});
		it('1.调拨单', async function () {
			let sfRes = await common.editBilling(basiceJson.outJson());
			await printRequestHandler.getPrint(sfRes);
			let printRes = await printRequestHandler.getPrint(sfRes); //重新打印
			expect(printRes, `重复打印错误:${JSON.stringify(printRes)}`).to.not.have.property('error');
		});
		it('2.采购入库', async function () {
			let sfRes = await common.editBilling(basiceJson.purchaseJson());
			await common.delay(500);
			await printRequestHandler.getPrint(sfRes);
			let printRes = await printRequestHandler.getPrint(sfRes); //重新打印
			expect(printRes, `重复打印错误:${JSON.stringify(printRes)}`).to.not.have.property('error');
		});
		it('3.销售订货', async function () {
			let sfRes = await common.editBilling(basiceJson.salesOrderJson());
			let param = {
				pk: sfRes.result.pk,
				isUpdatePrintFlag: 1
			};
			await common.callInterface('cs-getOrderPrint', param); //打印
			let printRes = await common.callInterface('cs-getOrderPrint', param); //重新打印
			expect(printRes, `重复打印错误:${JSON.stringify(printRes)}`).to.not.have.property('error');
		});
		it('4.销售开单', async function () {
			let sfRes = await common.editBilling(basiceJson.salesJson());
			await common.delay(500);
			await printRequestHandler.getPrint(sfRes);
			let printRes = await printRequestHandler.getPrint(sfRes); //重新打印
			expect(printRes, `重复打印错误:${JSON.stringify(printRes)}`).to.not.have.property('error');
		});
	});

	//文一店已经绑定仓库店
	if (USEECINTERFACE == 1) {
		describe('绑定仓库', function () {
			before(async () => {
				// await storesBinding.unboundWarehouse('中洲店'); //解除绑定
				await storesBinding.bindingWarehouse('中洲店'); //中洲店绑定仓库店
				await common.loginDo({ //中洲店总经理登陆
					'logid': '200',
					'pass': '000000',
				});
			});
			after(async () => {
				await storesBinding.unboundWarehouse('中洲店'); //解除绑定
			});
			it('150051.调出门店和接收门店绑定同一个门店.rankA', async function () {
				let json = format.jsonparamFormat(basiceJson.outJson());
				json.shopid = BASICDATA.shopidZzd;
				json.invidref = BASICDATA.shopidWyd;

				let param = format.qlParamsFormat({
					'propdresStyleid': json.details[0].styleid,
					'propdresColorid': json.details[0].colorid,
					'propdresSizeid': json.details[0].sizeid,
				}, false);
				let invInfo1 = await getCurInvnumInfo(param);

				let outRes = await common.editBilling(json);

				let invInfo2 = await getCurInvnumInfo(param);
				invInfo1['仓库店-onroadnum'] = common.add(invInfo1['仓库店-onroadnum'], json.details[0].num);
				invInfo1['仓库店-invnum'] = common.sub(invInfo1['仓库店-invnum'], json.details[0].num);
				common.isApproximatelyEqualAssert(invInfo1, invInfo2);
			});
		});
	}
	//接口有问题 等待服务端解决
	describe.skip('打印条码', function () {
		// before(async() => {
		//
		// });
		it('150014.打印条码功能', async function () {
			let outRes = await common.editBilling(basiceJson.outJson());
			let qlRes = await common.callInterface('cs-getPrintContentByPurbill', {
				pk: '10014057969' //outRes.result.pk
			});
			console.log(`qlRes = ${JSON.stringify(qlRes)}`);
		});
	});

});

//获取货品当前库存信息
async function getCurInvnumInfo(param) {
	let invInfo = {};
	let result = await common.callInterface('ql-1932', param);
	result.dataList.forEach((obj) => {
		invInfo[`${obj.inventoryName}-invnum`] = obj.invnum; //库存
		invInfo[`${obj.inventoryName}-onroadnum`] = obj.onroadnum; //在途数
	});
	return invInfo;
};

//拼接门店调出-按批次查期望结果
//未调入 未作废
function getMainSearchExp(sfRes) {
	let exp = {
		// optime: sfRes.params.optime, //includes
		totalmoney: sfRes.params.totalmoney,
		remark: '批量调出',
		shopname: '中洲店',
		inventoryrefName: '常青店',
		totalnum: sfRes.params.totalnum,
		prodate: common.getCurrentDate('YY-MM-DD'),
		seller: LOGINDATA.name,
		opname: LOGINDATA.name,
		id: sfRes.result.pk,
		flag: '未接收',
		invalidflag: '否',
		staffName: LOGINDATA.name,
		billno: sfRes.result.billno,
	};
	return exp;
};
