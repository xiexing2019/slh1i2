'use strict';

const common = require('../../../lib/common.js');
// const caps = require('../../../data/caps');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
// const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');

/*
 * 根据岗位限制店员补登天数限制
 */

describe('补登天数限制-slh2', function () {
	this.timeout(60000); //补登时间越久的单据花费时间越久 目前最慢需要30S

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('invinout_checknum', 0); //设置允许负库存
		await common.setGlobalParam('sc_fixprodate', 0); //不限制修改单据日期
	});
	after(async () => {
		await setInsertHisParam(0); //修改为不限制补登天数
	});

	const testData = [0, 1, 2]; //0, , 3, 7, 15, 30, 60, 90, 300 补登时间太长的花费时间过多
	for (let i = 0, length = testData.length; i < length; i++) {
		describe(`170455.补登${testData[i]}天`, function () {
			before(async () => {
				await setInsertHisParam(testData[i]); //修改角色权限
			});
			it('销售开单', async function () {
				await insertHisBilling(testData[i], salesInterfInfo());
			});
			it('销售订单', async function () {
				await insertHisBilling(testData[i], salesOrderInterfInfo());
			});
			it('采购入库', async function () {
				await insertHisBilling(testData[i], purchaseInterfInfo());
			});
			it('采购订货', async function () {
				await insertHisBilling(testData[i], purchaseOrderInterfInfo());
			});
			it('门店调出', async function () {
				await common.loginDo({
					'logid': '204',
					'pass': '000000'
				});
				await insertHisBilling(testData[i], outInterInfo());
			});
		});
	};

});

function salesInterfInfo() {
	let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
	let salesInterfaceInfo = {
		'save': {
			'interfaceid': 'sf-14211-1',
			'param': jsonparam
		},
		'queryForm': {
			'interfaceid': 'qf-14211-1',
			'param': ''
		},
		'queryList': {
			'interfaceid': 'ql-142201',
			'param': {
				'dwid': jsonparam.dwid,
				'shopid': LOGINDATA.invid,
				'billnokey1': 'billno1',
				'billnokey2': 'billno2'
			}
		}
	};

	return salesInterfaceInfo;
}

function salesOrderInterfInfo() {
	let jsonparam = format.jsonparamFormat(basiceJsonparam.salesOrderJson());
	let salesOrderInterfaceInfo = {
		'save': {
			'interfaceid': 'sf-14401-1',
			'param': jsonparam
		},
		'queryForm': {
			'interfaceid': 'qf-14401-1',
			'param': ''
		},
		'queryList': {
			'interfaceid': 'ql-14433',
			'param': {
				'clientid': jsonparam.clientid,
				'shopid': LOGINDATA.invid,
				'billnokey1': 'id1',
				'billnokey2': 'id2'
			}
		}
	};

	return salesOrderInterfaceInfo;
}

function purchaseInterfInfo() {
	let jsonparam = format.jsonparamFormat(basiceJsonparam.purchaseJson());
	let purchaseInterfaceInfo = {
		'save': {
			'interfaceid': 'sf-14212-1',
			'param': jsonparam
		},
		'queryForm': {
			'interfaceid': 'qf-14212-1',
			'param': ''
		},
		'queryList': {
			'interfaceid': 'ql-22301',
			'param': {
				'dwid': jsonparam.dwid,
				'shopid': LOGINDATA.invid,
				'billnokey1': 'id1',
				'billnokey2': 'id2'
			}
		}
	};

	return purchaseInterfaceInfo;
}

function purchaseOrderInterfInfo() {
	let jsonparam = format.jsonparamFormat(basiceJsonparam.purchaseOrderJson());
	let purchaseOrderInterfaceInfo = {
		'save': {
			'interfaceid': 'sf-22101-1',
			'param': jsonparam
		},
		'queryForm': {
			'interfaceid': 'qf-22101-1',
			'param': ''
		},
		'queryList': {
			'interfaceid': 'ql-22102',
			'param': {
				'dwid': jsonparam.dwid,
				'invid': LOGINDATA.invid,
				'billnokey1': 'id1',
				'billnokey2': 'id2'
			}
		}
	};

	return purchaseOrderInterfaceInfo;
}

function outInterInfo() {
	let jsonparam = format.jsonparamFormat(basiceJsonparam.outJson());
	let outInterfaceInfo = {
		'save': {
			'interfaceid': 'sf-1862-1',
			'param': jsonparam
		},
		'queryForm': {
			'interfaceid': 'qf-1862-1',
			'param': ''
		},
		'queryList': {
			'interfaceid': 'ql-1862',
			'param': {
				// 'deliver': jsonparam.deliver,
				'shopid': jsonparam.shopid,
				'billnokey1': 'id1',
				'billnokey2': 'id2'
			}
		}
	};

	return outInterfaceInfo;
}

/*
 * 修改店长的 补登天数 功能参数,并重新登录
 */
async function setInsertHisParam(val) {
	await common.loginDo(); //使用非店长角色登陆修改参数(重新登录判断)
	//修改角色功能权限
	let roleParam = {
		'ipadcode': 'shopmanager', //店长
		'code': 'insert_his',
		'val': val
	};
	await common.setRoleParam(roleParam);

	// 修改角色功能权限后要重新登录才生效
	await common.loginDo({
		'logid': '004',
		'pass': '000000'
	});
};


/*
val 补登天数值
0表示没有限制，可补登任意天数单据
1表示不可补登,即:只可补登当天单据，也可以理解为可补登1天单据
2表示只可补登昨天，即:只可补登2天单据
n表示只可补登n天单据
*/

/*
interfaceInfo  开单、查询表单、查询列表等一系列业务需要的接口及参数信息
{
	'save': {
		'interfaceid': ,
		'param':
	},
	'queryForm': {
		'interfaceid': ,
		'param':
	},
	'queryList': {
		'interfaceid': ,
		'param':
	}
};
*/

/*
 * 验证补登销售单、销售订货单、采购单、采购订货单、门店调出单
 */
async function insertHisBilling(val, interfaceInfo) {

	let saveInterfaceid = interfaceInfo.save.interfaceid;
	let saveParam = interfaceInfo.save.param;
	let formInterfaceid = interfaceInfo.queryForm.interfaceid;
	let listInterfaceid = interfaceInfo.queryList.interfaceid;
	let listParam = interfaceInfo.queryList.param;

	//-------------------在补登范围内---------------------------
	// 开单
	let jsonparam = saveParam;
	jsonparam.optime = common.getCurrentDate();
	if (val == 0) {
		jsonparam.prodate = common.getDateString([0, 0, -3]); //301
		jsonparam.remark = `31`;
	} else {
		jsonparam.prodate = common.getDateString([0, 0, -(val - 1)]);
		jsonparam.remark = `补登${val}天`;
	}
	// let time = Date.now();
	let salesResult = await common.callInterface(saveInterfaceid, {
		'jsonparam': jsonparam,
	});
	// let duration = Date.now() - time;
	// console.log(`${saveInterfaceid}花费时间${duration}ms`);
	expect(salesResult, `${saveInterfaceid}失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
	// 查询单据明细信息
	let billInfoParams = {
		'pk': salesResult.pk,
		'action': 'edit'
	};
	if (USEECINTERFACE == '2') billInfoParams.isPend = 0; //是否挂单 否
	let billInfo = await common.callInterface(formInterfaceid, billInfoParams);
	expect(billInfo, `${formInterfaceid} 查看单据明细出错:${JSON.stringify(billInfo)}`).to.not.have.property('error');
	// 按批次查
	listParam.prodate1 = jsonparam.prodate;
	listParam.prodate2 = jsonparam.prodate;
	listParam[listParam.billnokey1] = salesResult.billno;
	listParam[listParam.billnokey2] = salesResult.billno;
	delete listParam.billnokey1;
	delete listParam.billnokey2;
	let searchCondition = format.qlParamsFormat(listParam);
	let batchInfo = await common.callInterface(listInterfaceid, searchCondition);

	// 断言
	expect(batchInfo, `${listInterfaceid}没有查询到补登入库单据:${JSON.stringify(batchInfo)}`).to.includes({
		'count': '1'
	});
	common.isApproximatelyEqual(jsonparam, billInfo);
	common.isApproximatelyEqual(billInfo, batchInfo.dataList[0]);

	//-------------------在补登范围以外---------------------------
	if (val == 0) return; //不限制补登天数

	let alertString = `只允许补登${val}天内的单据`;
	jsonparam.prodate = common.getDateString([0, 0, -val]);
	format.updateHashkey(jsonparam);
	salesResult = await common.callInterface(saveInterfaceid, {
		'jsonparam': jsonparam,
	});
	// console.log(`超出补登天数提示 : ${JSON.stringify(salesResult)}`);
	expect(JSON.stringify(salesResult), `(超出补登天验证)${saveInterfaceid}返回结果:${JSON.stringify(salesResult)} \njsonparam:${JSON.stringify(jsonparam)}`).to.includes(alertString);
}
