'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const resportSumRequestHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
describe("销售开单-按批次查-slh2", function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('170003.查询条件组合查询-slh2', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesJson());
		//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		let param = format.qlParamsFormat({
			billno1: qfRes.result.billno,
			billno2: qfRes.result.billno,
			deliver: qfRes.result.deliver,
			dwid: qfRes.result.dwid,
			dwtypeid: 0, //客户类别
			epid: qfRes.result.epid,
			flag: 1, //核销 无欠余为1
			invalidflag: 0, //状态
			invdisflag: 0, //是否配货
			rem: qfRes.result.remark,
			shopid: qfRes.result.shopid,
		}, true);
		let qlRes = await common.callInterface('ql-142201', param);
		let exp = qfRes.result;
		exp.dwname = exp.show_dwid;
		exp.maininvname = exp.show_shopid;
		exp.seller = exp.show_deliver.split(',')[1];
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	//flag=1表示否，flag=0表示是  与核销相反
	it('170005.是否未结-slh2', async function () {
		let param = format.qlParamsFormat({
			flag: 0,
		}, true);
		let qlRes = await common.callInterface('ql-142201', param);
		// console.log(`qlRes = ${JSON.stringify(qlRes)}`);
		expect(qlRes.dataList).to.satisfy(function (arr) {
			if (arr.length == 0) return false;
			let ret = true;
			arr.map((obj) => {
				ret = ret && obj.flag == 0 && obj.balance != '0';
			});
			return ret;
		}, '查询结果出错');
	});

	it('170019.退货并退款情况下实收金额检查.rankA-slh2', async function () {
		let res1 = await getResQl_16235(); //统计分析-综合收支表 获取起始值

		let json = basiceJsonparam.simpleSalesJson();
		[json.cash, json.card, json.remit] = [-100, -200, -300];
		json.details[0].num = -3;
		json.details[0].rem = '退货';
		let sfRes = await common.editBilling(json); //退货+退款
		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk); //获取单据详情
		if (USEECINTERFACE == 2) await common.delay(1500);
		let params = {
			'ql-142201': { //销售开单-按批次查
				billno1: sfRes.result.billno,
				billno2: sfRes.result.billno,
				today: true,
			},
			'ql-1352': { //统计分析-收支流水
				accountInvid: LOGINDATA.invid,
				today: true,
			},
		};
		let qlRes = await common.getResults(params);
		TESTCASE.describe = '退货+退款时,验证销售开单按批次查数据';
		common.isApproximatelyEqualAssert(sfRes.params, qlRes['ql-142201'].dataList[0]);
		expect(Number(qlRes['ql-142201'].dataList[0].actualpay), '实收值错误').to.equal(-600); //实收
		if (USEECINTERFACE == 2) await common.delay(8000);
		TESTCASE.describe = '退货+退款时,验证统计分析-收支流水数据';
		qfRes.result.interfaceid = 'qf-14211-1';
		let expDatalist = resportSumRequestHandler.getInOutRecExp(qfRes.result);

		common.isApproximatelyArrayAssert(expDatalist, qlRes['ql-1352'].dataList.slice(0, expDatalist.length));

		TESTCASE.describe = '退货+退款时,验证统计分析-综合收支表数据';
		let res2 = await getResQl_16235();
		common.isApproximatelyEqualAssert(common.addObject(res1, {
			cash: 100,
			bank: 500,
		}), res2);
	});

	//正常保存,不能出现错误提示
	it('170608.单据修改界面修改退货数后保存.rankA-slh2', async function () {
		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].num = 10;
		let sfRes = await common.editBilling(json);

		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		sfRes.params.action = 'edit';
		sfRes.params.cash = 0;
		sfRes.params.pk = sfRes.result.pk;
		sfRes.params.details[0].num = -6;
		sfRes.params.details[0].pk = qfRes.result.details[0].pk;
		await common.editBilling(sfRes.params);

		sfRes.params.details[0].num = -9;
		await common.editBilling(sfRes.params);
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-23167
	//订货号字段->orderno
	it('170639/170640.订货号检查', async function () {
		if (USEECINTERFACE == 2) this.skip();
		let orderJson = basiceJsonparam.salesOrderJson();
		let orderSfRes = await common.editBilling(orderJson); //新增订货单
		let orderQfData = await salesRequestHandler.salesOrderQueryBilling(orderSfRes.result.pk);

		let sfRes = await salesRequestHandler.editSalesBillByOrder(orderQfData.result); //按订货入库-全部入库
		let param = format.qlParamsFormat({
			orderno: orderSfRes.result.billno,
		}, true);
		let qlRes = await common.callInterface('ql-142201', param); //销售开单-按批次查
		let exp = [{
			billno: sfRes.result.billno - 1,
			billid: orderSfRes.result.billno,
			remark: '预付款',
		}, {
			billno: sfRes.result.billno,
			billid: orderSfRes.result.billno,
		}];
		common.isApproximatelyArrayAssert(exp, qlRes.dataList);
	});

	it('170783.销售订货的挂单,不能生成预付款单.rankA', async function () {
		let orderSfRes = await salesOrderReqHandler.editSalesOrderHangBill(basiceJsonparam.salesOrderJson()); //生成销售订货挂单

		if (USEECINTERFACE == 1) {
			let res = await salesRequestHandler.salesQueryBilling(orderSfRes.result.prepayid); //
			expect(res.result).to.includes({
				billno: '0',
				invalidflag: '10', //订单挂单的预付款单
			});

			let param = format.qlParamsFormat({
				billno1: 0,
				billno2: 0,
			}, true);
			let qlResult = await common.callInterface('ql-142201', param); //按批次查
			expect(qlResult.dataList.length).to.equal(0);
		} else {
			expect(orderSfRes.result).not.to.have.property('prepayid'); //不生成预付款单
		};

	});

	describe('配货', function () {
		let [invdisRes, sfRes] = [{}, {}];
		before(async function () {
			sfRes = await common.editBilling(basiceJsonparam.salesJson());
		});
		it('170008.是否配货', async function () {
			let param = format.qlParamsFormat({
				billno1: sfRes.result.billno,
				billno2: sfRes.result.billno,
				invdisflag: 0, //是否配货 否
			}, true);
			let qlRes = await common.callInterface('ql-142201', param);
			expect(qlRes.dataList[0], `error:${JSON.stringify(qlRes)}`).to.includes({
				billno: sfRes.result.billno,
				invdisflag: '0',
			});

			invdisRes = await setInvdis(sfRes.result.pk);
			expect(invdisRes, `设置配货结果:${JSON.stringify(invdisRes)}`).to.have.property('val');

			param.invdisflag = 1; //是否配货 是
			qlRes = await common.callInterface('ql-142201', param);
			expect(qlRes.dataList[0], `error:${JSON.stringify(qlRes)}`).to.includes({
				billno: sfRes.result.billno,
				invdisflag: '1',
			});
		});

		it('170638.重复配货', async function () {
			invdisRes = await setInvdis(sfRes.result.pk);
			expect(invdisRes, `error:${JSON.stringify(invdisRes)}`).to.includes({
				error: '不允许重复设置已配货',
			});
		});
		async function setInvdis(pk) {
			let res = await common.callInterface('cs-setInvdisflag', {
				pk,
				isPend: 0,
			});
			return res;
		};
	});

	describe('已配货的销售单据修改', function () {

		after(async () => {
			await common.setGlobalParam('sales_cannot_modify_distributed', 0);
		});

		it('170558,销售单已配货的单子只允许修改付款方式--不限制.rankA', async function () {
			await common.setGlobalParam('sales_cannot_modify_distributed', 0);
			await common.setGlobalParam('dwxx_not_allow_edit', 1); //单据是否允许修改客户或厂商  设置为允许
			// cs-setInvdisflag
			let salesResult = await common.editBilling(format.jsonparamFormat(basiceJsonparam.salesJson()));
			// let salesResult = await common.callInterface('sf-14211-1', {
			// 	'jsonparam': format.jsonparamFormat(basiceJsonparam.salesJson())
			// });
			// expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			let setDistribution = await common.callInterface('cs-setInvdisflag', {
				'pk': salesResult.result.pk,
				isPend: 0
			});
			expect(setDistribution, `设置已配货失败:${JSON.stringify(setDistribution)}`).to.not.have.property('error');
			let salesForm = await salesRequestHandler.salesQueryBilling(salesResult.result.pk);
			// let salesForm = await common.callInterface('qf-14211-1', {
			// 	'pk': salesResult.result.pk
			// });

			// 验证支付方式修改是否成功
			salesForm.result.remit = (Number(salesForm.result.remit) || 0) + 1;
			salesForm.result.cash = Number(salesForm.result.totalmoney) - Number(salesForm.result.remit) - Number(salesForm.result.card);
			salesForm.result.action = 'edit';
			salesForm.result.interfaceid = 'sf-14211-1';
			salesResult = await common.editBilling(salesForm.result, false);
			expect(salesResult.result, `已配货的单据可以修改支付方式，实际结果修改失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

			//验证修改 客户名称/明细/明细备注和整单备注/店员 信息后，保存成功

			salesForm = await salesRequestHandler.salesQueryBilling(salesResult.result.pk);
			salesForm.result.action = 'edit';
			salesForm.result.dwid = BASICDATA.dwidLs;
			salesForm.result.details[0].rem = `detailRemark${common.getRandomStr(4)}`;
			salesForm.result.details[0].num = Number(salesForm.result.details[0].num) + 1;
			salesForm.result.remark = `mainRemark${common.getRandomStr(4)}`;
			salesForm.result.deliver = BASICDATA.staffid004;
			salesForm.result.interfaceid = 'sf-14211-1';
			salesResult = await common.editBilling(salesForm.result, false);
			expect(salesResult.result, `已配货单据可以修改客户名称/明细/明细备注和整单备注/店员信息，实际结果修改失败:${salesResult}`).to.not.have.property('error');
		});

		it('170559,销售单已配货的单子只允许修改付款方式--只允许修改付款方式', async function () {
			await common.setGlobalParam('sales_cannot_modify_distributed', 1);
			await common.setGlobalParam('dwxx_not_allow_edit', 0); //单据是否允许修改客户或厂商 设置为不允许

			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': format.jsonparamFormat(basiceJsonparam.salesJson())
			});
			expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			let setDistribution = await common.callInterface('cs-setInvdisflag', {
				'pk': salesResult.pk,
				isPend: 0
			});
			expect(setDistribution, `设置已配货失败:${JSON.stringify(setDistribution)}`).to.not.have.property('error');
			let salesForm = await common.callInterface('qf-14211-1', {
				'pk': salesResult.pk,
				isPend: 0
			});

			let salesPK = salesResult.pk;

			// 验证支付方式修改是否成功
			salesForm.remit = (Number(salesForm.remit) || 0) + 1;
			salesForm.cash = Number(salesForm.totalmoney) - Number(salesForm.remit) - Number(salesForm.card);
			salesForm.action = 'edit';

			salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesForm
			});
			expect(salesResult, `已配货的单据可以修改支付方式，实际结果修改失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

			//验证修改 客户名称信息后，保存失败
			salesForm = await common.callInterface('qf-14211-1', {
				'pk': salesPK,
				isPend: 0
			});
			salesForm.action = 'edit';
			salesForm.dwid = BASICDATA.dwidLs;
			salesForm = format.updateHashkey(salesForm);
			salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesForm
			});
			expect(salesResult, `已配货单据不可以修改客户信息，实际结果修改成功:${JSON.stringify(salesResult)}`).to.includes({
				'error': '客户或供应商信息不允许修改'
			});

			// 验证修改 明细/明细备注 信息后，保存失败
			salesForm = await common.callInterface('qf-14211-1', {
				'pk': salesPK,
				isPend: 0
			});
			salesForm.action = 'edit';
			salesForm.detchgflag = '1';
			salesForm.details[0].rem = `detailRemark${common.getRandomStr(4)}`;
			salesForm.details[0].num = Number(salesForm.details[0].num) + 1;
			salesForm.interfaceid = 'sf-14211-1';
			salesForm = format.jsonparamFormat(salesForm);
			salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesForm
			});
			expect(salesResult, `已配货单据不可以修改明细信息，实际结果修改成功:${JSON.stringify(salesResult)}`).to.includes({
				'error': '已经配货的单据，只能修改支付方式'
			});

			//验证修改 整单备注信息后，保存失败
			salesForm = await common.callInterface('qf-14211-1', {
				'pk': salesPK,
				isPend: 0
			});
			salesForm.action = 'edit';
			salesForm.detchgflag = '1';
			salesForm.remark = `mainRemark${common.getRandomStr(4)}`;
			salesForm = format.updateHashkey(salesForm);
			salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesForm
			});
			expect(salesResult, `已配货单据不可以修改整单备注信息，实际结果修改成功:${JSON.stringify(salesResult)}`).to.includes({
				'error': '已经配货的单据，只能修改支付方式'
			});

			// 验证修改 店员信息后，保存失败
			salesForm = await common.callInterface('qf-14211-1', {
				'pk': salesPK,
				isPend: 0
			});
			salesForm.action = 'edit';
			salesForm.detchgflag = '1';
			salesForm.deliver = salesForm.deliver == BASICDATA.staffid000 ? BASICDATA.staffid004 : BASICDATA.staffid000;
			salesForm = format.updateHashkey(salesForm);
			salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesForm
			});
			expect(salesResult, `已配货单据不可以修改店员信息，实际结果修改成功:${JSON.stringify(salesResult)}`).to.includes({
				'error': '已经配货的单据，只能修改支付方式'
			});
		});
	});
});

//统计分析-综合收支表
async function getResQl_16235() {
	let param = format.qlParamsFormat({
		prodate: common.getCurrentDate(),
		shopid: LOGINDATA.invid,
	}, false);
	let res = await common.callInterface('ql-16235', param);
	let ret = {
		cash: 0,
		bank: 0,
		// card: 0,
		// remit: 0
	};
	res.dataList.forEach(function (obj) {
		if (obj.catname == '销售退款') ret.cash = obj.money;
		if (obj.accountname == '银' && obj.catname.includes('销售退款')) ret.bank = obj.money;
		// if(obj.catname == '销售退款(刷卡)') ret.card = obj.money;
		// if(obj.catname == '销售退款(汇款)') ret.remit = obj.money;
	});
	return ret;
}
