'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--按配货员汇总
 */

describe('销售开单--按汇总--按配货员汇总-slh2', function () {

	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 20);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	it('170633,按配货员汇总', async function () {
		await zzdLogin();
		let res = await common.editBilling(basiceJsonparam.salesJsonPaymethod20()); //新增销售单

		await common.loginDo();
		res = await common.editBilling(basiceJsonparam.salesJsonPaymethod20()); //新增销售单

		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({
			'shopid': LOGINDATA.invid
		}, true));
		let isRight = true;
		for (let i = 0; i < summaryList.dataList.length; i++) {
			if (summaryList.dataList[i].invname != LOGINDATA.depname) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `按配货员汇总门店查询条件有误`).to.be.true;
	});

	it('170634,即退货又拿货', async function () {
		// 按配货员汇总
		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));

		// 开单
		let res = await common.editBilling(basiceJsonparam.salesJsonPaymethod20_1()); //新增销售单

		// 按配货员汇总
		let summaryListAft = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));
		common.isApproximatelyEqualAssert(sumObjects(summaryList, res.params), summaryListAft.dataList[0]);
	});

	it('170635,按店员汇总包含预付款单的钱款', async function () {

		// 按配货员汇总
		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));

		let res = await common.editBilling(basiceJsonparam.salesOrderJson20()); //新增订货单
		let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({
			'orderno': res.result.billno
		}, true));
		let expectJson = {};
		//配货员字段，二代返回 invdisopid  一代返回 distristaffname
		if (USEECINTERFACE == 2) {
			expectJson.invdisopid = LOGINDATA.name;
		} else {
			expectJson.distristaffname = LOGINDATA.name;
		};
		expect(batchList.dataList[0], `销售开单按批次查页面的预付款单中配货员字段有误`).to.includes(expectJson);

		// 按配货员汇总
		let summaryListAft = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));
		for (let key in summaryListAft.dataList[0]) {
			summaryListAft.dataList[0][key] = Number(summaryListAft.dataList[0][key]).toString();
		};
		expect(summaryListAft.dataList[0], `按配货员汇总错误`).to.includes(orderSumObjects(summaryList, res.params));
	});

	it('170637,按配货员汇总各角色查看范围', async function () {

		// 中洲店总经理登录并开单
		await zzdLogin();
		let res = await common.editBilling(basiceJsonparam.salesJsonPaymethod20()); //新增销售单

		// 常青店店长登录 应该只能查询到常青店数据
		await cqdDzLogin();
		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({}, true));
		let isRight = true;
		for (let i = 0; i < summaryList.dataList.length; i++) {
			if (summaryList.dataList[i].invname != LOGINDATA.depname) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `店长登录，按配货员汇总列表应该只能查询到本门店的数据`).to.be.true;

		// 常青店总经理登录 应该能查询到所有门店的数据
		await common.loginDo();
		summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({}, true));
		isRight = false;
		for (let i = 0; i < summaryList.dataList.length; i++) {
			if (summaryList.dataList[i].invname != LOGINDATA.depname) {
				isRight = true;
				break;
			}
		}
		expect(isRight, `总经理登录，按配货员汇总列表应该能查询到所有门店的数据`).to.be.true;
	});
});


/*
 * 中洲店总经理登录
 */
async function zzdLogin() {
	let loginParams = {
		'logid': '200',
		'pass': '000000'
	};
	await common.loginDo(loginParams);
}

/*
 * 常青店店长登录
 */
async function cqdDzLogin() {
	let loginParams = {
		'logid': '004',
		'pass': '000000'
	};
	await common.loginDo(loginParams);
}


function sumObjects(obj, json) {
	if (obj.dataList.length == 0) {
		return {
			'alipay': (json.alipay || '0'),
			'weixinpay': (json.alipay || '0'),
			'remit': json.remit,
			'cash': json.cash,
			'card': json.card,
			'totalnum': json.totalnum,
			'totalsum': (json.totalnum || json.totalmoney)
		};
	} else {
		return {
			'alipay': ((Number(obj.dataList[0].alipay) || 0) + (Number(json.alipay) || 0)).toString(),
			'weixinpay': ((Number(obj.dataList[0].weixinpay) || 0) + (Number(json.weixinpay) || 0)).toString(),
			'remit': ((Number(obj.dataList[0].remit) || 0) + (Number(json.remit) || 0)).toString(),
			'cash': ((Number(obj.dataList[0].cash) || 0) + (Number(json.cash) || 0)).toString(),
			'card': ((Number(obj.dataList[0].card) || 0) + (Number(json.card) || 0)).toString(),
			'totalnum': ((Number(obj.dataList[0].totalnum) || 0) + (Number(json.totalnum) || 0)).toString(),
			'totalsum': ((Number(obj.dataList[0].totalsum) || 0) + (Number(json.totalsum) || Number(json.totalmoney) || 0)).toString()
		}
	}
}


function orderSumObjects(obj, json) {
	if (obj.dataList.length == 0) {
		return {
			'alipay': (json.alipay || '0'),
			'weixinpay': (json.alipay || '0'),
			'remit': json.remit,
			'cash': json.cash,
			'card': json.card
		};
	} else {
		return {
			'alipay': ((Number(obj.dataList[0].alipay) || 0) + (Number(json.alipay) || 0)).toString(),
			'weixinpay': ((Number(obj.dataList[0].weixinpay) || 0) + (Number(json.weixinpay) || 0)).toString(),
			'remit': ((Number(obj.dataList[0].remit) || 0) + (Number(json.remit) || 0)).toString(),
			'cash': ((Number(obj.dataList[0].cash) || 0) + (Number(json.cash) || 0)).toString(),
			'card': ((Number(obj.dataList[0].card) || 0) + (Number(json.card) || 0)).toString()
		}
	}
}
