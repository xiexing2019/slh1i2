'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');

/*
 * 销售开单--按汇总--按客户上货
 */
describe('销售开单--按汇总--按客户上货-slh2', function () {
	this.timeout(30000);
	let styleAgc001 = {}; //agc001的详细信息
	let clientXw = {};
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		clientXw = await common.callInterface('qf-1401', {
			action: 'edit',
			pk: BASICDATA.dwidXw,
		});
	});

	it('171016.增加客户名称查询条件.rankA', async function () {
		let param = format.qlParamsFormat({
			mainDwxxName: 'cust',
		}, true);
		let result = await common.callInterface('ql-14459', param);
		expect(result.dataList.length, '查询无结果').to.be.above(0);
		result.dataList.map((obj, index) => {
			expect(obj.dw_name.includes('cust'), `查询结果出错:${obj.dw_name}不包含'cust'`).to.be.true;
		});
	});

	it('170684.店员权限只能看本门店数据', async function () {
		await common.loginDo({ //中洲店店长登陆
			logid: '204',
			pass: '000000',
		});
		let params = { //只获取汇总数据
			'ql-1209': { //销售开单-按明细查
				invname: LOGINDATA.invid,
				search_list: 0,
				search_count: 0,
				today: true, //一代查询条件，规定了必须有起始日期，后期可能会不要这个条件
			},
			'ql-14459': { //销售开单-按客户上货
				search_list: 0,
				search_count: 0,
				today: true, //一代查询条件，规定了必须有起始日期，后期可能会不要这个条件
			},
		};
		let qlRes = await common.getResults(params);
		let qlRes1209 = await common.callInterface('ql-1209', format.qlParamsFormat({
			invname: LOGINDATA.invid,
		}, true)); //一代查询条件，规定了必须有起始日期，后期可能会不要这个条件
		let sum = 0,
			sumMoney = 0;
		for (let i = 0; i < qlRes1209.dataList.length; i++) {
			if (qlRes1209.dataList[i].dw_name == "") {
				sum = common.add(sum, qlRes1209.dataList[i].num);
				sumMoney = common.add(sumMoney, qlRes1209.dataList[i].mat_money);
			}
		};
		let exp = {
			num: -sum,
			mat_money: -sumMoney,
		}
		exp = common.addObject(qlRes['ql-1209'].sumrow, exp); //按客户上货不统计客户为空的单据，按明细查需要减掉客户为空时数量
		await common.loginDo(); //重新登陆，防止影响别的用例
		common.isApproximatelyEqualAssert(exp, qlRes['ql-14459'].sumrow);
	});

	describe('170338.按客户上货数据验证', function () {
		let [params, qfRes, qlRes, detailRes] = [{}, {}, {}, {}];
		before(async function () {
			let json = basiceJsonparam.salesJson();
			let sfRes = await common.editBilling(json); //小王 agc001开单
			qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			params = {
				'ql-1209': { //销售开单-按明细查
					dwid: BASICDATA.dwidXw,
					styleid: styleAgc001.pk,
					today: true,
					// search_list: 1,
					// search_count: 1,
				},
				'ql-14459': { //销售开单-按客户上货
					mainDwid: BASICDATA.dwidXw,
					mainDwxxTypeid: clientXw.typeid,
					mainDwxxName: clientXw.nameshort,
					marketdate1: styleAgc001.marketdate,
					marketdate2: styleAgc001.marketdate,
					propdresStyleid: styleAgc001.pk,
					propdresStyleBrandid: styleAgc001.brandid,
					propdresStyleSeason: styleAgc001.season,
					propdresStyleClassid: styleAgc001.classid,
					today: true,
				},
			};
			qlRes = await common.getResults(params);
			// console.log(`qlRes = ${JSON.stringify(qlRes)}`);
			expect(qlRes['ql-14459'], `ql-14459查询出错:${JSON.stringify(qlRes['ql-14459'])}`).not.to.have.property('error');
			expect(qlRes['ql-1209'], `ql-1209查询出错:${JSON.stringify(qlRes['ql-1209'])}`).not.to.have.property('error');
		});
		it('1.dataList验证', function () {
			let exp = {
				// 'id': 'id',
				'mat_code': styleAgc001.code,
				'mat_name': styleAgc001.name,
				'brand': styleAgc001.show_brandid,
				'dw_name': clientXw.nameshort,
				// 'salenum': '销售数',
				// 'backnum': '退货数',
				// 'num': '实销数',//第二点
				// 'total': '实销额 ',//第二点
				// 'paysum': '实收',
				'main_optime': common.getCurrentDate('YY-MM-DD'),
				// 'buytimes': '拿货次数',
				'dwid': BASICDATA.dwidXw,
				'styleid': styleAgc001.pk,
				'propdresStyleCode': styleAgc001.code,
				'propdresStyleName': styleAgc001.name,
				'mainDwxxNameshort': clientXw.nameshort,
				// 'lastoptime': qfRes.result.optime,
				// 'countnum': '拿货次数'//第三点验证
			};
			common.isApproximatelyEqualAssert(exp, qlRes['ql-14459'].dataList[0]);
		});
		it('2.实销数、实销额与销售开单-按明细查-输入日期、客户、款号 汇总值一致', function () {
			expect(qlRes['ql-1209'].sumrow.num).to.equal(qlRes['ql-14459'].sumrow.num);
			expect(qlRes['ql-1209'].sumrow.mat_money).to.equal(qlRes['ql-14459'].sumrow.total);
		});
		it('3.拿货次数验证', function () {
			TESTCASE = {
				describe: "按客户上货拿货次数验证",
				expect: "按客户上货拿货次数=按明细查中除了只有退货的单据数之和"

			}
			let billnoArr = []; //统计agc001的批次号
			qlRes['ql-1209'].dataList.map((obj) => {
				if (Number(obj.num) > 0) billnoArr.push(obj.billno); //只退货的不计算在拿货次数中
			});
			billnoArr = common.dedupe(billnoArr);
			expect(String(billnoArr.length), `按明细查拿货次数为${billnoArr.length},按客户上货拿货次数为${qlRes['ql-14459'].dataList[0].countnum}`)
				.to.equal(qlRes['ql-14459'].dataList[0].countnum);
		});
		it('4.详细页面数据验证', async function () {
			let param = _.cloneDeep(params['ql-14459']);
			// param.id = qlRes['ql-14459'].dataList[0].id;
			detailRes = await common.callInterface('ql-144591', format.qlParamsFormat(param, true));
			let exp = {
				'billno': qfRes.result.billno,
				'mat_code': styleAgc001.code,
				'mat_name': styleAgc001.name,
				'main_optime': qfRes.result.optime,
				'num': qfRes.result.totalnum,
				'price': qfRes.result.details[0].realprice,
			};
			expect(detailRes.dataList).to.satisfy(function (arr) {
				return common.isArrayContainObject(arr, exp);
			}, `详细页面未匹配到批次号为${exp.billno}的销售单`);
		});
		// 详细界面拿货次数只要这个款在客户的一张销售单出现就算一次，不管是不是仅退货
		it('170345.详细界面拿货次数检查', function () {
			TESTCASE = {
				describe: "按客户上货详细界面拿货次数验证",
				expect: "按客户上货详细界面底下记录条数=按明细查中除了只有退货的单据数之和"

			}
			let billnoArr = []; //统计agc001的批次号
			qlRes['ql-1209'].dataList.map((obj) => billnoArr.push(obj.billno));
			billnoArr = common.dedupe(billnoArr);
			expect(String(billnoArr.length), `按明细查拿货次数为${billnoArr.length},按客户上货详细界面拿货次数为${detailRes.count}`)
				.to.equal(detailRes.count);
		});
	});

});
