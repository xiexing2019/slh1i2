'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--金额汇总 (顺带覆盖了一点按客户、按店员汇总关于几种付款方式金额的验证)
 */

describe('销售开单--按汇总-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	it('170588,实收栏', async function () {

		let moneySearchCond = {
			'shopid': LOGINDATA.invid
		};
		let staffSearchCond = {
			'staffid': BASICDATA.staffid000,
			'shopid': LOGINDATA.invid
		};
		let custSearchCond = {
			'dwid': BASICDATA.dwidXw
		}

		// 记录开单前 按金额/店员/客户汇总 的查询结果
		let summaryByMoney = await common.callInterface('ql-14424', format.qlParamsFormat(moneySearchCond, true));
		let summaryByStaff = await common.callInterface('ql-14451', format.qlParamsFormat(staffSearchCond, true));
		let summaryByCust = await common.callInterface('ql-14457', format.qlParamsFormat(custSearchCond, true));

		summaryListHelp.paysumCheckAssert(summaryByMoney.dataList[0]);
		summaryListHelp.paysumCheckAssert(summaryByStaff.dataList[0]);
		summaryListHelp.paysumCheckAssert(summaryByCust.dataList[0]);

		// 用小王 李四开单
		let jsonparamXW = format.jsonparamFormat(basiceJsonparam.salesJson());
		jsonparamXW.deliver = BASICDATA.staffid000;
		await common.editBilling(jsonparamXW);
		let jsonparamLS = format.jsonparamFormat(basiceJsonparam.salesJson());
		jsonparamLS.deliver = BASICDATA.staffid000;
		jsonparamLS.dwid = BASICDATA.dwidLs;
		await common.editBilling(jsonparamLS);

		//开单后 按金额/店员/客户汇总 的查询结果
		let summaryByMoneyAft = await common.callInterface('ql-14424', format.qlParamsFormat(moneySearchCond, true));
		let summaryByStaffAft = await common.callInterface('ql-14451', format.qlParamsFormat(staffSearchCond, true));
		let summaryByCustAft = await common.callInterface('ql-14457', format.qlParamsFormat(custSearchCond, true));

		// 开单前+开单 == 开单后
		summaryListHelp.moneyInfoAssert(summaryListHelp.moneySum(summaryByMoney.dataList[0], jsonparamXW, jsonparamLS), summaryByMoneyAft.dataList[0], '按金额汇总-前后');
		summaryListHelp.moneyInfoAssert(summaryListHelp.moneySum(summaryByStaff.dataList[0], jsonparamXW, jsonparamLS), summaryByStaffAft.dataList[0], '按店员汇总-前后');
		summaryListHelp.moneyInfoAssert(summaryListHelp.moneySum(summaryByCust.dataList[0], jsonparamXW), summaryByCustAft.dataList[0], '按客户汇总-前后');

		// 开单后 == 按批次查
		let moneyList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'shopid': LOGINDATA.invid
		}, true));
		let staffList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'deliver': BASICDATA.staffid000,
			'shopid': LOGINDATA.invid
		}, true));
		let custList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		summaryListHelp.moneyInfoAssert(moneyList.sumrow, summaryByMoneyAft.sumrow, '按批次-按金额汇总');
		summaryListHelp.moneyInfoAssert(staffList.sumrow, summaryByStaffAft.sumrow, '按批次-按店员汇总');
		summaryListHelp.moneyInfoAssert(custList.sumrow, summaryByCustAft.sumrow, '按批次-按客户汇总');
	});

	it('170306,按金额汇总', async function () {
		//---------------------------------开单模式2--------------------------------------
		let moneySearchCond = {
			'shopid': LOGINDATA.invid
		};
		let summary = await common.callInterface('ql-14424', format.qlParamsFormat(moneySearchCond, true));
		// 开单模式2下，开代收单
		let jsonparam = format.jsonparamFormat(basiceJsonparam.agencySalesJson());
		let result = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(result, `开单模式2下，开单失败`).to.not.have.property('error');
		// 开单前+开单 == 开单后
		let summaryAft = await common.callInterface('ql-14424', format.qlParamsFormat(moneySearchCond, true));
		common.isApproximatelyEqualAssert({
			'daishou': common.add(summary.dataList[0].daishou, jsonparam.agency),
		}, summaryAft.dataList[0]);
		// expect(summaryAft.dataList[0], `开单模式2下，代收金额计算错误`).to.includes({
		// 	'daishou': common.add(summary.dataList[0].daishou,jsonparam.agency),
		// });
		//按金额汇总和按批次页面汇总栏的金额比较
		let batchList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'shopid': LOGINDATA.invid
		}, true));
		common.isFieldsEqualAssert(summaryAft.dataList[0], batchList.sumrow, 'daishou;alipay;weixinpay;remit;cash;card;paysum=actualpay');

		//---------------------------------快速标记代模式--------------------------------------
		summary = _.cloneDeep(summaryAft);
		//快速标记代收开单
		jsonparam = format.jsonparamFormat(basiceJsonparam.quickAgencySalesJson());
		result = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(result, `快速标记代收，开单失败`).to.not.have.property('error');
		//开单前==开单后【快速标记代收本质上是欠款单】
		summaryAft = await common.callInterface('ql-14424', format.qlParamsFormat(moneySearchCond, true));
		expect(summaryAft.dataList[0], `快速标记代收模式下，代收金额计算错误`).to.includes({
			'daishou': summary.dataList[0].daishou
		});
		//按金额汇总和按批次页面汇总栏的金额比较
		batchList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'shopid': LOGINDATA.invid
		}, true));
		common.isFieldsEqualAssert(summaryAft.dataList[0], batchList.sumrow, 'daishou;alipay;weixinpay;remit;cash;card;paysum=actualpay');
	});

	it('170726.非总经理其他门店查询', async function () {
		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		let param = format.qlParamsFormat({
			'shopid': BASICDATA.shopidZzd
		}, true);
		let result = await common.callInterface('ql-14424', param);
		expect(result.dataList.length, '店长查询其他门店数据错误').to.equal(0);

		param.shopid = BASICDATA.shopidCqd;
		result = await common.callInterface('ql-14424', param);

		await common.loginDo();
		let result2 = await common.callInterface('ql-14424', param);
		expect(result.dataList[0], '店长查询本门店数据错误').to.eql(result2.dataList[0]);
	});
});
