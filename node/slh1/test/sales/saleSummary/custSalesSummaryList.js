'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');
const basicReqHandler = require('../../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 销售开单--按汇总--按客户销售
 */

describe('销售开单--按汇总--按客户销售-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it.skip('170587,按客户销售不能受积分兑换的影响-要补', async function () { });

	// 现金，刷卡，汇款，实收、代收、实销数、实销额与“销售开单-按批次查-输入客户”查询的现金、刷卡、汇款、实收、代收、数量、金额  底部数据汇总一致；
	// 退货数 与“销售开单-按汇总-按退货汇总-输入客户”的数量一致；
	// 销售数=实销数+退货数
	it('170325,按客户销售列表数据验证', async function () {
		let param = format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true);
		//按汇总-按客户销售,按批次查,按汇总-按退货汇总
		let [summaryByCust, batchList, summaryByBack] = await Promise.all([common.callInterface('ql-14457', param),
		common.callInterface('ql-142201', param), common.callInterface('ql-14489', param)]);

		// 现金，刷卡，汇款，实收、代收、实销数、实销额
		TESTCASE.expect = `ql-14457按客户销售,现金,刷卡,汇款,实收,代收,实销数,实销额与'销售开单-按批次查-输入客户'查询的现金,刷卡,汇款,实收,代收,数量,金额的底部数据汇总一致`;
		let compareField = 'totalmoney;daishou;remit;actualnum=totalnum;alipay;weixinpay;cash;card;paysum=actualpay';
		common.isFieldsEqualAssert(summaryByCust.dataList[0], batchList.sumrow, compareField);

		// 退货数 backnum
		TESTCASE.expect = `按客户销售的退货数 等于 按退货汇总的数量`;
		if (summaryByCust.dataList[0].backnum == '0') {
			expect(summaryByBack).to.includes({
				"count": "0"
			});
		} else {
			assert.equal(summaryByBack.sumrow.num, summaryByCust.sumrow.backnum);
		};

		//销售数=实销数+退货数
		TESTCASE.expect = `按客户销售 实销数+退货数=销售数`;
		assert.equal(common.add(summaryByCust.dataList[0].actualnum, summaryByCust.dataList[0].backnum), summaryByCust.dataList[0].num, `按客户销售汇总'销售数'统计错误`);
	});

	it('170718.客户销售按品牌', async function () {
		let params = {
			'ql-14457': { //销售汇总--按客户销售
				'dwid': BASICDATA.dwidXw,
				'prodate1': common.getCurrentDate(),
				'prodate1': common.getCurrentDate(),
			},
			'ql-144572': { //客户销售按品牌
				'dwid': BASICDATA.dwidXw,
				'prodate1': common.getCurrentDate(),
				'prodate1': common.getCurrentDate(),
			},
		};
		let originQlRes = await common.getResults(params);

		await common.editBilling(basiceJsonparam.salesOrderJson()); //新增订单 不计算进拿货次数

		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		}); //新增货品 没有品牌
		let json = basiceJsonparam.specialSalesJson();
		json.dwid = BASICDATA.dwidXw;
		json.details[1].styleid = goodRes.pk;
		json.details[1].num = -1;
		await common.editBilling(json); //有品牌与没有品牌+特殊货品

		let qlRes = await common.getResults(params);
		common.isApproximatelyEqualAssert(qlRes['ql-14457'].dataList[0], qlRes['ql-144572'].sumrow); //客户销售按品牌汇总验证

		//退货率=(退货数/销售数)
		let returnrate = (common.div(qlRes['ql-144572'].dataList[0].backnum, qlRes['ql-144572'].dataList[0].totalnum) * 100).toFixed(2);
		expect(qlRes['ql-144572'].dataList[0].returnrate, '退货率错误').to.equal(`${returnrate}%`);

		qlRes['ql-144572'].dataList.map((obj, index) => {
			if (obj.brandname == 'Adidas') { //agc001的品牌
				common.isApproximatelyEqualAssert({
					backnum: originQlRes['ql-144572'].dataList[index].backnum,
					totalnum: common.add(originQlRes['ql-144572'].dataList[index].totalnum, json.details[0].num),
				}, obj);
			};
			if (obj.brandname == '') {
				common.isApproximatelyEqualAssert({
					backnum: common.sub(originQlRes['ql-144572'].dataList[index].backnum, json.details[1].num),
					totalnum: originQlRes['ql-144572'].dataList[index].totalnum
				}, obj);
			};
		});

		//按客户销售的拿货次数是按客户统计，按客户上货的拿货次数按货品统计
		assert.equal(qlRes['ql-14457'].dataList[0].askcount, Number(originQlRes['ql-14457'].dataList[0].askcount) + 1, '拿货次数错误');
	});

	describe('170331,按客户销售明细验证', function () {
		//销售开单-按批次查,按客户销售明细,销售单明细,按客户销售明细的明细
		let batchResult, summaryList, billingInfo, summaryInfo;
		before(async () => {
			// 销售开单
			let salesResult = await common.editBilling(basiceJsonparam.salesJson());

			[batchResult, summaryList, billingInfo, summaryInfo] = await Promise.all([
				common.callInterface('ql-142201', format.qlParamsFormat({
					'dwid': salesResult.params.dwid,
					'invalidflag': 0
				}, true)),
				common.callInterface('ql-144571', format.qlParamsFormat({
					'dwid': salesResult.params.dwid
				}, true)),
				salesReqHandler.salesQueryBilling(salesResult.result.pk),
				common.callInterface('cs-get-bill-detail', {
					'pk': salesResult.result.pk,
					'bizType': '1100' // bizType: 1100：销售单，1200：销售订单，1300：采购单，1400：采购订单
				})]);
		});
		it(`1.判断'按客户销售明细列表' 跟 '按批次查' 数据是否一致`, async function () {
			TESTCASE = {
				describe: "按客户销售-明细列表和按批次查数据是否一致",
				expect: `按客户销售-明细列表与按批次查数据一致`,
				jira: 'SLHSEC-6767,作废的单据也显示了。已关闭'
			};
			isBatchListEqulaSummaryList(batchResult.dataList, summaryList.dataList);
		});
		it(`2.判断 '按客户销售明细的明细' 跟 '按批次查的明细' 数据是否一致`, async function () {
			isBatchInfoEqualSummaryInfo(billingInfo.result, summaryInfo);
		});
	});

});

/*
 * 判断'按客户销售明细列表' 跟 '按批次查' 数据是否一致
 */
function isBatchListEqulaSummaryList(batchList, summaryList) {
	if (batchList.length != summaryList.length) {
		throw new Error(`销售开单-按批次查ql-142201与按客户销售明细列表ql-144571返回的数据条目数不同`);
	};

	for (let i = 0; i < summaryList.length; i++) {
		for (let j = batchList.length; j > 0; j--) {
			if (summaryList[i]['billno'] == batchList[j - 1]['billno'] && summaryList[i]['id'] == batchList[j - 1]['id']) {
				let fields = USEECINTERFACE == 1 ? 'optime;totalmoney;daishou;invname;dwname;remark;remit;dwfdname;totalnum;seller;balance;alipay;weixinpay;cash;card;billno' :
					'optime;totalmoney;dwname;dwfdname;seller;balance;billno';
				let result = common.isFieldsEqual(summaryList[i], batchList[j - 1], fields);
				if (!result.flag) {
					throw new Error(`billno:${summaryList[i].billno} 销售开单-按批次查ql-142201与按客户销售明细列表ql-144571数据不匹配${result.errorMsg}`);
					break;
				} else {
					batchList.splice(j - 1, 1);
				};
			};
		};
	};
};

/*
 * 判断 '按客户销售明细的明细' 跟 '按批次查的明细' 数据是否是一致的
 */
function isBatchInfoEqualSummaryInfo(batchInfo, summaryInfo) {
	// 比较mainfield部分相同
	let compareField = 'totalnum=totalNum;totalmoney=totalSum;finpayAlipay|0=alipay;finpayWeixinpay|0=weixinpay;billno=billNo;remit;cash;card;agency|0;prodate';
	common.isFieldsEqualAssert(batchInfo, summaryInfo, compareField, `销售单明细与按客户销售明细的明细mainfield部分数据不匹配`);

	// 比较details.count相同
	if (batchInfo.details.count != summaryInfo.details.count) {
		throw new Error(`销售单明细(${batchInfo.details.count})与按客户销售明细的明细(${summaryInfo.details.count})数据条目数不同`);
	};

	// 比较detailsfield部分是否相同
	for (let i = 0; i < summaryInfo.details.length; i++) {
		for (let j = batchInfo.details.length; j > 0; j--) {
			let summaryItem = summaryInfo.details[i];
			let batchItem = batchInfo.details[j - 1];
			if (summaryItem.pk == batchItem.pk) {
				compareField = 'stylename=styleName;total;show_colorid=colorName;num;stylecode=styleCode;price;show_sizeid=sizeName;discount';
				common.isFieldsEqualAssert(batchItem, summaryItem, compareField, `销售单明细与按客户销售明细的明细details部分数据不匹配`);
				batchInfo.details.splice(j - 1, 1);
			};
		};
	};
};
