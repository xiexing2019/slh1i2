'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--按类别汇总
 */


describe('销售开单--按汇总--按类别汇总-slh2', function () {
	this.timeout(30000);
	let styleAgc001;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
	});

	it('170356,按类别汇总', async function () {
		// 按类别汇总查询
		let param = format.qlParamsFormat({
			'propdresStyleClassid': styleAgc001.classid
		}, true);
		let classSummary = await common.callInterface('ql-14490', param);
		expect(classSummary, `按类别汇总查询失败:${JSON.stringify(classSummary)}`).to.not.have.property('error');

		// 销售开单
		let salenum = 8,
			backnum = -2;
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.details[0].num = salenum;
		jsonparam.details[1].num = backnum; //固定第二个元素做退货
		let sfRes = await common.editBilling(jsonparam);

		// 按类别汇总查询
		let classSummaryAft = await common.callInterface('ql-14490', param);

		common.isApproximatelyEqualAssert({
			'num': Number(classSummary.dataList[0].num) + salenum + backnum,
			'prenum': Number(classSummary.dataList[0].prenum) + Math.abs(backnum),
			'recvnum': Number(classSummary.dataList[0].recvnum) + salenum,
			'accountmoney': Number(classSummary.dataList[0].accountmoney) + Number(sfRes.params.totalmoney)
		}, classSummaryAft.dataList[0])
	});

	// it('170356,按类别汇总(特殊货品不计入实销额)', async function () {
	//
	// });
});
