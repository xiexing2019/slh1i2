'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const reqHandlerHelp = require('../../../help/reqHandlerHelp');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--按店员汇总
 * ql-14451
 */

describe('销售开单--按汇总--按店员汇总', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('170319.按店员汇总数据验证-slh2', function () {
		let params = {},
			qlRes = {};
		before(async function () {
			params = { //只获取汇总数据
				'ql-142201': { //销售开单-按批次查
					shopid: LOGINDATA.invid, //使用登陆门店查询
					deliver: LOGINDATA.id,
					search_list: 0,
					search_count: 0,
					prodate1: common.getDateString([0, 0, -7]),
					prodate2: common.getCurrentDate(),
				},
				'ql-1209': { //销售开单-按明细查
					shopid: LOGINDATA.invid,
					deliver: LOGINDATA.id,
					typeid: 1, //退货有bug，类型查询条件底下汇总值有误
					search_list: 0,
					search_count: 0,
					prodate1: common.getDateString([0, 0, -7]),
					prodate2: common.getCurrentDate(),
				},
				'ql-14451': { //销售开单-按店员汇总
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					search_list: 0,
					search_count: 0,
					prodate1: common.getDateString([0, 0, -7]),
					prodate2: common.getCurrentDate(),
				},
			};
			qlRes = await common.getResults(params);
			delete qlRes['ql-142201'].sumrow.outsum; //？销额
			// console.log(`qlRes = ${JSON.stringify(qlRes)}`);
		});
		it('1.返回结果汇总值 与 销售开单-按批次查 同时间段内指定店员结果一致', function () {
			if (USEECINTERFACE == 2) {
				assert.equal(Math.abs(qlRes['ql-142201'].sumrow.payback), Math.abs(qlRes['ql-14451'].sumrow.balance));
			} //一代 销售开单-按批次查 未结和还款/抵扣 列 没有底部汇总值
			common.isApproximatelyEqualAssert(qlRes['ql-142201'].sumrow, qlRes['ql-14451'].sumrow, ['balance']);
		});
		it('2.退货数 与 销售开单-按明细查-类型-输入客户 一致', async function () {
			//二代赠品改版
			let backNum;
			if (USEECINTERFACE == 2) {
				backNum = Math.abs(qlRes['ql-1209'].sumrow.num)
			} else {
				let param2 = _.cloneDeep(params['ql-1209']);
				param2.typeid = 2;
				param2.interfaceid = 'ql-1209';
				let res = await reqHandlerHelp.qlIFCHandler(param2);
				backNum = Math.abs(common.add(qlRes['ql-1209'].sumrow.num, res.result.sumrow.num || 0));
			}
			expect(backNum).to.equal(Math.abs(qlRes['ql-14451'].sumrow.backnum), `按明细查退货数为${backNum} 与按店员汇总退货数为-${qlRes['ql-14451'].sumrow.backnum} 不一致`);
		});
		it('3.实收=现金+微信+刷卡+汇款', function () {
			summaryListHelp.paysumCheckAssert(qlRes['ql-14451'].sumrow);
		});
		it('4.欠款=实销额-(现金+微信+刷卡+汇款+代收)', function () {
			summaryListHelp.balanceCheckAssert(qlRes['ql-14451'].sumrow);
		});
		it('5.销售数=实销数+退货数', function () {
			summaryListHelp.numCheckAssert(qlRes['ql-14451'].sumrow); //二代有bug 实销数汇总值错误
		});
		it('6.实销额2=实销额-其他费用', function () {
			let obj = qlRes['ql-14451'].sumrow;
			let sum = (Number(obj.totalmoney) || 0) - (Number(obj.othercost) || 0);
			expect(sum, `实销额-其他费用=${sum} 而实销额2为:${obj.actualsalesmoney_noothercost}`).to.be.equal(Number(obj.actualsalesmoney_noothercost));
		});
		it('7.抹零、其他费用 与销售开单-按明细查-输入款号--特殊货品（除抹零）一致', async function () {
			let param1 = {
				'ql-1209': { //销售开单-按明细查
					shopid: LOGINDATA.invid,
					deliver: LOGINDATA.id,
					stylename: '000', //名称模糊查询 除了积分抵现外的所有特殊货品
					prodate1: common.getDateString([0, 0, -1]),
					today: false,
				},
				'ql-14451': { //销售开单-按店员汇总
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					prodate1: common.getDateString([0, 0, -1]),
					today: false,
					search_list: 0,
					search_count: 0,
				},
			};
			let res = await common.getResults(param1);
			let [favorsum, othercost] = [0, 0]; //抹零 其他费用
			res['ql-1209'].dataList.forEach((obj) => {
				obj.mat_code == '00000' ? favorsum = common.add(favorsum, obj.mat_money) : othercost = common.add(othercost, obj.mat_money);
			}); //按明细查中，用特殊货品的款号查询不到结果，是二代的bug
			assert.equal(-favorsum, res['ql-14451'].sumrow.favorsum, `特殊货品汇总值不一致 按明细查为-${favorsum} 按店员汇总为${res['ql-14451'].sumrow.favorsum}`)

			param1['ql-1209'].stylename = '66666'; //积分抵现
			param1['ql-1209'].search_list = 0;
			let res2 = await common.callInterface('ql-1209', format.qlParamsFormat(param1['ql-1209'], false));
			othercost = common.add(othercost, res2.sumrow.mat_money || 0);
			assert.equal(othercost, res['ql-14451'].sumrow.othercost, `其他费用汇总值不一致 按明细查为-${othercost} 按店员汇总为${res['ql-14451'].sumrow.othercost}`);
		});
		it('8.进入详细 与销售开单-按明细查、输入店员、门店查询核对', async function () {
			let param1 = {
				'ql-1209': { //销售开单按明细查
					shopid: LOGINDATA.invid,
					deliver: LOGINDATA.id,
					search_list: 0,
					search_count: 0,
					prodate1: common.getDateString([0, 0, -7]),
				},
				'ql-144512': { //按营业员明细
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					prodate1: common.getDateString([0, 0, -7]),
				},
			};
			let res = await common.getResults(param1);
			let num = 0,
				specCode = BASICDATA.specGoodsList.map(obj => obj.code);
			let actual = res['ql-144512'].dataList.map((obj) => {
				if (!specCode.includes(obj.mat_code)) num += Number(obj.num); //排除特殊货品
			});
			//二代赠品计入店员汇总-明细界面的数量中
			assert.equal(num, res['ql-1209'].sumrow.num, `按明细查汇总结果为${res['ql-1209'].sumrow.num} 进入详细结果为${num}`);

		});
	});

	describe('每日业绩-slh2', function () {
		it('170708.每日业绩', async function () {
			TESTCASE = {
				describe: "按店员汇总-每日业绩数据是否正确",
				expect: `按店员汇总-每日业绩和按店员汇总中的汇总值一致`,
				jira: 'SLHSEC-6810 ql-14451中outsum计算有误（已关闭）',
			}
			let param1 = {
				'ql-14451': { //销售开单-按店员汇总
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					search_list: 0,
					search_count: 0,
					today: true,
				},
				'ql-144511': { //按店员汇总-店员每天汇总
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					search_list: 0,
					search_count: 0,
					today: true,
				},
			};
			let res = await common.getResults(param1);
			common.isApproximatelyEqualAssert(res['ql-14451'].sumrow, res['ql-144511'].sumrow);
		});

		it('170709.每日业绩-同一个店员同一天在不同门店销售', async function () {
			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			let sfRes = await common.editBilling(basiceJsonparam.simpleSalesJson());

			await common.loginDo({ //常青店店长登陆
				'logid': '004',
				'pass': '000000',
				'search_list': 0,
				'search_count': 1,
				'search_sum': 0,
			});
			let param = {
				shopid: BASICDATA.shopidZzd,
				staffid: LOGINDATA.id,
			};
			let result = await common.callInterface('ql-14451', param);
			expect(result.count, '店员只能看到本门店数据').to.equal('0');

			param.shopid = LOGINDATA.invid;
			result = await common.callInterface('ql-14451', param);
			expect(result.count, '店员看不到本门店数据').to.equal('1');

			await common.loginDo();
			param.shopid = BASICDATA.shopidZzd;
			delete param.staffid;
			result = await common.callInterface('ql-14451', param);
			expect(Number(result.count), '总经理可以看到其他门店数据').to.above(1);
		});

		it.skip('170324.店员在门店间调动-要补', async function () {
			//新增一个店员（先查询是否有店员，没有就新增
			// 开单，然后换门店，然后再开单，然后再验这条用例
			//
			let staffRes = await common.callInterface('qf-1110-2', {
				pk: BASICDATA.staffid004,
			});
			staffRes.action = 'edit';
			//部门id？
		});
	});

	describe('查看员工销售额、销售数，近期销售额，销售数', function () {
		describe('总经理查看自己的销售额,销售数', function () {
			let res, curDayQlRes, curWeekQlRes, curMouthQlRes, qlJson;
			before('获取数据', async function () {
				qlJson = getQlParams(LOGINDATA.id);
				curDayQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curDay));
				curWeekQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curWeek));
				curMouthQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curMonth));
			})

			it('总经理当天销售额，销售数排名汇总', async function () {
				//cs-findSaleSumWithRankByRange 该接口，如果指定时间段内该店员未销售，那么rank都返回-1的
				res = await common.callInterface('cs-findSaleSumWithRankByRange', {
					sellerId: LOGINDATA.id
				});
				let exp = await getExpectExp(qlJson.curDay, curDayQlRes, LOGINDATA.id);
				common.isApproximatelyEqualAssert(exp, res.curDay);
			});

			it('总经理本周销售额，销售数排名汇总', async function () {
				let exp = await getExpectExp(qlJson.curWeek, curWeekQlRes, LOGINDATA.id);
				common.isApproximatelyEqualAssert(exp, res.curWeek, [], '本周销售排名获取错误，查询条件是' + JSON.stringify(qlJson));
			});

			it('总经理本月销售额，销售数排名汇总', async function () {
				let exp = await getExpectExp(qlJson.curMonth, curMouthQlRes, LOGINDATA.id);
				common.isApproximatelyEqualAssert(exp, res.curMonth);
			});

		});

		describe('总经理查看员工的销售额，销售数', async function () {
			let res, curDayQlRes, curWeekQlRes, curMouthQlRes, qlJson;
			before('获取数据', async function () {
				qlJson = getQlParams(BASICDATA.staffid004);
				curDayQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curDay));
				curWeekQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curWeek));
				curMouthQlRes = await common.callInterface('ql-14451', format.qlParamsFormat(qlJson.curMonth));
			})

			it('店员当天销售额，销售数排名汇总', async function () {
				res = await common.callInterface('cs-findSaleSumWithRankByRange', {
					sellerId: BASICDATA.staffid004
				});
				let exp = await getExpectExp(qlJson.curDay, curDayQlRes, BASICDATA.staffid004);
				common.isApproximatelyEqualAssert(exp, res.curDay);
			});

			it('店员本周销售额，销售数排名汇总', async function () {
				let exp = await getExpectExp(qlJson.curWeek, curWeekQlRes, BASICDATA.staffid004);
				common.isApproximatelyEqualAssert(exp, res.curWeek, [], '本周销售排名获取错误，查询条件是' + JSON.stringify(qlJson));
			});

			it('店员本月销售额，销售数排名汇总', async function () {
				let exp = await getExpectExp(qlJson.curMonth, curMouthQlRes, BASICDATA.staffid004);
				common.isApproximatelyEqualAssert(exp, res.curMonth);
			});
		});
	});

	it('获取30天内销售情况汇总', async function () {
		let arr = [];
		let res = await common.callInterface('cs-findNearestMonthSaleSum', {
			sellerId: LOGINDATA.id
		});
		for (let i = 29; i >= 0; i--) {
			let json = {
				staffid: LOGINDATA.id,
				prodate1: common.getDateString([0, 0, -i]),
				prodate2: common.getDateString([0, 0, -i])
			};
			let qlRes = await common.callInterface('ql-14451', format.qlParamsFormat(json));
			if (qlRes.dataList.length != 0) {
				arr.push({
					actualSum: qlRes.dataList[0].totalmoney,
					actualNum: qlRes.dataList[0].actualnum,
					saleDate: common.getDateString([0, 0, -i])
				});
			}
			await common.delay(30);
		};
		common.isApproximatelyEqualAssert(arr, res.dataList);
	});

});

/**
 * @param {Object} params 当天，本周，当月的查询条件对象
 * @param {String} sortField 排序字段
 * @param {String} staffid  需要排序后所在位置的员工号
 */
async function getRank(params, sortField, staffid) {
	let rank = 0;
	let sortJson = _.cloneDeep(params);
	sortJson.sortField = sortField;
	sortJson.sortfieldOrder = '1';
	delete sortJson['staffid']; //不要员工查询条件
	let qlRes = await common.callInterface('ql-14451', format.qlParamsFormat(sortJson));
	for (let i = 0; i < qlRes.dataList.length; i++) {
		if (qlRes.dataList[i].id == staffid) {
			rank = i + 1;
			break;
		}
	};
	return rank;
};


/**
 * @param {Object} param 当天，本周，当月的查询条件
 * @param {Object} qlResult  查询条件为当天的ql查询结果列表
 * @param {Integer} id     需要查询的员工id
 */
async function getExpectExp(param, qlResult, id) {
	//如果该店员当天没有进行过销售，rank都返回-1的，表示没排名
	if (qlResult.dataList.length == 0) {
		return {
			actualSumRank: -1,
			actualSum: 0,
			actualNum: 0,
			actualNumRank: -1,
		};
	} else {
		return {
			actualSumRank: await getRank(param, 'totalmoney', id),
			actualSum: qlResult.dataList[0].totalmoney,
			actualNum: qlResult.dataList[0].actualnum,
			actualNumRank: await getRank(param, 'actualnum', id),
		};
	};
};

function getQlParams(id) {
	let qlJson = {
		curDay: {
			staffid: id,
			prodate1: common.getCurrentDate(),
			prodate2: common.getCurrentDate(),
		},
		curMonth: {
			staffid: id,
			prodate1: common.getCurrentMonth().firstDate,
			prodate2: common.getCurrentMonth().lastDate,
		},
		curWeek: {
			staffid: id,
			prodate1: common.getCurrentWeek().firstDate,
			prodate2: common.getCurrentDate(),
		}
	};
	return qlJson;
}