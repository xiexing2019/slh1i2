
const common = require('../../../../lib/common');
const getBasicData = require('../../../../data/getBasicData.js');
const format = require('../../../../data/format');
const basiceJsonparam = require('../../../help/basiceJsonparam');
//由于按特殊货品汇总，这里的特殊货品是包括正常销售单里面的特殊货品，还包括物流核销里面的特殊货品，由于物流核销里面的
//特殊货品返回的是总值，所以无法进行数据对比验证，只能通过数据变化量进行验证

describe('销售开单-按汇总-按特殊货品汇总', function () {
    this.timeout(30000);
    let searchCondition, specialSumyRes, sfJson;

    before('初始汇总值获取', async function () {
        await common.loginDo();
        await getBasicData.getBasicID();
        await common.setGlobalParam('paymethod', 2);   //代收模式开启
        searchCondition = format.qlParamsFormat({
            shopid: LOGINDATA.invid
        }, true);

        specialSumyRes = await common.callInterface('ql-144201', searchCondition);   //查询初始的特殊货品汇总值
    });

    after('参数还原', async function () {
        await common.setGlobalParam('paymethod', 2);
    });

    it('新增特殊货品', async function () {
        //查询条件
        sfJson = basiceJsonparam.specialSalesJson();
        await common.editBilling(sfJson);   //开包括特殊货品的销售单
        let exp = getSpecialExp(specialSumyRes.dataList, sfJson.details.slice(2, 4));

        let res = await common.callInterface('ql-144201', searchCondition);
        //查询初始的特殊货品汇总值
        common.isApproximatelyArrayAssert(exp, res.dataList);
        specialSumyRes = _.cloneDeep(res);   //更新初始值
    });

    it('物流核销特殊货品', async function () {
        const sfRes = await common.editBilling(basiceJsonparam.agencySalesJson());   //开代收单
        const logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
            'logisDwid': sfRes.params.logisDwid,
            'shopid': LOGINDATA.invid,
            'dwid': sfRes.params.dwid
        }, true));

        let verifyJson = {
            'billno': logisChooseList.dataList[0].billno,
            'cash': 600,
            'logisVerifybillids': logisChooseList.dataList[0].id,
            'logisVerifysum': Number(logisChooseList.dataList[0].finpayagency),
            'remark': '特殊货品验证',
            'dwid': logisChooseList.dataList[0].logisdwxxid
        };
        verifyJson = format.verifyJsonFormat(verifyJson, true);
        await common.callInterface('sf-1452', {
            jsonparam: verifyJson,
        });      //核销物流单，里面包含特殊货品的

        verifyJson.details.forEach(val => {
            val.matCode = BASICDATA.specGoodsList.find(ele => ele.id == val.styleid).code;
        });   //根据特殊货品id找到对应的code

        const exp = getSpecialExp(specialSumyRes.dataList, verifyJson.details);
        const res = await common.callInterface('ql-144201', searchCondition);   //查询初始的特殊货品汇总值

        common.isApproximatelyArrayAssert(exp, res.dataList);
    });
});

function getSpecialExp(specialSumy, sfParams) {

    sfParams.forEach(val => {

        let info = specialSumy.find(style => style.styleCode == val.matCode);
        if (info == undefined) {
            specialSumy.push({
                totalSum: common.mul(val.num, val.price),
                styleCode: val.matCode
            });
        } else {
            common.addObject(info, { totalSum: common.mul(val.num, val.price) });
        };
    });
    return specialSumy;
};