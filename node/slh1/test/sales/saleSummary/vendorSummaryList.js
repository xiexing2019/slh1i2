'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--按厂商汇总
 */

describe('销售开单--按汇总--按厂商汇总-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('170361,按厂商汇总', async function () {
		this.retries(2);
		// 按厂商汇总查询
		let vendorSummary = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));

		// 销售开单
		let salenum = 8,
			backnum = -2;
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.details[0].num = salenum;
		jsonparam.details[1].num = backnum; //固定第二个元素做退货
		jsonparam = format.jsonparamFormat(jsonparam);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

		// 按厂商汇总查询
		let vendorSummaryAft = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));
		common.isApproximatelyEqualAssert({
			'salenum': common.add(vendorSummary.dataList[0].salenum, salenum),
			'backnum': common.add(vendorSummary.dataList[0].backnum, Math.abs(backnum)),
			'totalnum': common.add(vendorSummary.dataList[0].totalnum, jsonparam.totalnum),
			'totalmoney': common.add(vendorSummary.dataList[0].totalmoney, jsonparam.totalmoney)
		}, vendorSummaryAft.dataList[0]);
	});

	it('170361,按厂商汇总明细查', async function () {
		let summaryDetailBef = await common.callInterface('ql-144251', format.qlParamsFormat({
			'styledwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));

		// 销售开单
		let salenum = 8,
			backnum = -2;
		let jsonparam = basiceJsonparam.specialSalesJson();
		jsonparam.details[0].num = salenum;
		jsonparam.details[1].num = backnum; //固定第二个元素做退货
		jsonparam = format.jsonparamFormat(jsonparam);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');
		let salesInfo = (await salesReqHandler.salesQueryBilling(salesResult.pk)).result;

		let summaryDetail = await common.callInterface('ql-144251', format.qlParamsFormat({
			'styledwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));
		expect(summaryDetail.count, `按厂商汇总明细有误count`).to.be.equal((Number(summaryDetailBef.count) + jsonparam.details.length - 2).toString()); //不包括特殊货品

		let summaryExpectedList = [];
		for (let i = 0; i < 2; i++) { //不包括特殊货品
			let item = {
				'mat_color': salesInfo.details[i].show_colorid,
				// 'main_optime': ,
				'mat_code': salesInfo.details[i].stylecode,
				'mat_size': salesInfo.details[i].show_sizeid,
				'seller': LOGINDATA.name,
				'discount': salesInfo.details[i].discount,
				'num': salesInfo.details[i].num,
				'price': salesInfo.details[i].price,
				'billno': salesInfo.billno,
				'mat_money': salesInfo.details[i].total,
				'id': `${salesInfo.details[0].stylecode}_${salesInfo.details[i].show_colorid}_${salesInfo.details[i].show_sizeid}`
			};
			summaryExpectedList.push(item);
		}
		let summaryList = [];
		for (let i = 0; i < 2; i++) { //不包括特殊货品
			let item = _.cloneDeep(summaryDetail.dataList[i]);
			item.id = `${summaryDetail.dataList[i].mat_code}_${summaryDetail.dataList[i].mat_color}_${summaryDetail.dataList[i].mat_size}`;
			summaryList.push(item);
		}
		common.isApproximatelyArrayAssert(summaryExpectedList, summaryList);
	});

	it('170361,总经理对应角色能显示所有门店信息', async function () {
		// 中洲店登录
		await zzdLogin();
		// 销售开单
		let salenum = 8,
			backnum = -2;
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.details[0].num = salenum;
		jsonparam.details[1].num = backnum; //固定第二个元素做退货
		delete jsonparam.cash;
		delete jsonparam.card;
		delete jsonparam.remit;
		jsonparam = format.jsonparamFormat(jsonparam);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

		// 常青店登录
		await common.loginDo();
		let vendorSummary = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.shopidZzd
		}, true));

		let moreThenZero = vendorSummary.dataList.length > 0;
		expect(moreThenZero, `总经理对应的角色应该能看到所有门店的'按厂商汇总'信息`).to.be.true;
	});

	it('170648,按厂商汇总门店查询', async function () {
		let vendorSummaryCqd = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));

		let vendorSummaryZzd = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.shopidZzd
		}, true));

		// 中洲店登录
		await zzdLogin();
		// 销售开单
		let salenum = 8,
			backnum = -2;
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.details[0].num = salenum;
		jsonparam.details[1].num = backnum; //固定第二个元素做退货
		delete jsonparam.cash;
		delete jsonparam.card;
		delete jsonparam.remit;
		jsonparam = format.jsonparamFormat(jsonparam);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');
		let salesInfo = (await salesReqHandler.salesQueryBilling(salesResult.pk)).result;

		// 常青店
		await common.loginDo();
		let vendorSummaryCqdAft = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.invid
		}, true));

		let vendorSummaryZzdAft = await common.callInterface('ql-14425', format.qlParamsFormat({
			'dwid': BASICDATA.dwidVell,
			'shopid': LOGINDATA.shopidZzd
		}, true));

		common.isApproximatelyEqualAssert(vendorSummaryCqd.dataList[0], vendorSummaryCqdAft.dataList[0]);
		common.isApproximatelyEqualAssert({
			'salenum': common.add(vendorSummaryZzd.dataList[0].salenum, salenum),
			'backnum': common.add(vendorSummaryZzd.dataList[0].backnum, Math.abs(backnum)),
			'totalnum': common.add(vendorSummaryZzd.dataList[0].totalnum, jsonparam.totalnum),
			'totalmoney': common.add(vendorSummaryZzd.dataList[0].totalmoney, jsonparam.totalmoney)
		}, vendorSummaryZzdAft.dataList[0]);
		// expect(vendorSummaryZzdAft.dataList[0], `按厂商汇总列表有误`).to.includes({
		// 	'salenum': (Number(vendorSummaryZzd.dataList[0].salenum) + salenum).toString(),
		// 	'backnum': (Number(vendorSummaryZzd.dataList[0].backnum) + Math.abs(backnum)).toString(),
		// 	'totalnum': (Number(vendorSummaryZzd.dataList[0].totalnum) + Number(jsonparam.totalnum)).toString(),
		// 	'totalmoney': (Number(vendorSummaryZzd.dataList[0].totalmoney) + Number(jsonparam.totalmoney)).toString()
		// });
	});
});


/*
 * 中洲店总经理登录
 */
async function zzdLogin() {
	let loginParams = {
		'logid': '200',
		'pass': '000000'
	};
	await common.loginDo(loginParams);
}
