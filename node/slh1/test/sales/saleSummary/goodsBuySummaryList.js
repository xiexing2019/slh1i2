'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
// const summaryListHelp = require('../../../../help/salesHelp/summaryListHelp');

/*
 * 销售开单--按汇总--按款号上货
 */
describe('销售开单--按汇总--按款号上货-slh2', function () {
	this.timeout(30000);
	let styleAgc001 = {}; //agc001的详细信息
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
	});

	it('170459.详细-检查一个客户对某款号是否上货', async function () {
		let custRes = await salesReqHandler.addCust(basiceJsonparam.addCustJson()); //新增客户
		if (USEECINTERFACE == 2) await common.delay(500);
		let param = format.qlParamsFormat({
			dwid: custRes.result.val,
			styleid: styleAgc001.pk,
			delflag: 0, //是否上货 否
		}, false);
		let res = await common.callInterface('ql-144861', param); //销售汇总-按款号上货明细
		let exp = {
			id: custRes.result.val,
			name: custRes.params.nameshort,
			delflag: '否',
			// 'ordernum': 'ordernum'
		};
		common.isApproximatelyEqualAssert(exp, res.dataList[0]);

		let json = basiceJsonparam.simpleSalesJson();
		json.dwid = custRes.result.val;
		let sfRes = await common.editBilling(json);

		param.delflag = 1; //是否上货 是
		exp.delflag = '是';
		res = await common.callInterface('ql-144861', param);
		common.isApproximatelyEqualAssert(exp, res.dataList[0]);

		//销售单作废
		await salesReqHandler.cancelSaleoutBill(sfRes.result.pk); //作废
		param.delflag = 0; //是否上货 否
		exp.delflag = '否';
		res = await common.callInterface('ql-144861', param);
		common.isApproximatelyEqualAssert(exp, res.dataList[0]);
	});

	it('170458.按款号上货详细数据检查', async function () {
		let params = {
			'ql-14486': { //销售汇总-按款号上货
				styleid: styleAgc001.pk,
				brandid: styleAgc001.brandid,
				season: styleAgc001.season,
				classid: styleAgc001.classid,
			},
			'ql-144861': { //销售汇总-按款号上货明细
				styleid: styleAgc001.pk,
				delflag: 1,
				search_list: 0,
			},
		};
		let qlRes = await common.getResults(params);
		let detailRes2 = await common.callInterface('ql-144861', format.qlParamsFormat({
			styleid: styleAgc001.pk,
			delflag: 0,
			search_list: 0,
		}, false));
		let exp = {
			id: styleAgc001.pk,
			ordernum: qlRes['ql-144861'].count, //上货客户
			name: styleAgc001.name,
			noordernum: detailRes2.count, //未上货客户
			code: styleAgc001.code,
		};
		common.isApproximatelyArrayAssert(qlRes['ql-14486'].dataList[0], exp);
	});
});
