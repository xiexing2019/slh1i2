'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const basicReqHandler = require('../../../help/basicInfoHelp/basicInfoReqHandler');

// const summaryListHelp = require('../../../help/salesHelp/summaryListHelp');

/*
 * 销售开单--按汇总--按款号汇总
 */
describe('销售开单--按汇总--按款号汇总', function () {
	this.timeout(30000);
	let styleAgc001 = {}; //agc001的详细信息
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		// console.log(`styleAgc001 = ${JSON.stringify(styleAgc001)}`);
	});

	//查询界面列出与查询条件匹配的款号、名称、上架日期、颜色、尺码、实销数、库存
	//不稳定，偶尔qlRes2返回的num没有改变 invnum已经正确变化 有时间去问开发
	it('开单验证数据', async function () {
		let jsonparam = basiceJsonparam.salesJson();
		let param = format.qlParamsFormat({
			shopid: LOGINDATA.invid, //使用登陆门店查询
			styleid: styleAgc001.pk,
			stylename: styleAgc001.name,
			respopid: styleAgc001.respopid,
			colorid: BASICDATA[`colorid${jsonparam.details[0].colorid}`] || jsonparam.details[i].colorid,
			sizeid: BASICDATA[`sizeid${jsonparam.details[0].sizeid}`] || jsonparam.details[i].sizeid,
			marketdate1: styleAgc001.marketdate,
			marketdate2: styleAgc001.marketdate,
			dwid: styleAgc001.dwid,
			brandid: styleAgc001.brandid,
		}, true);
		let qlRes1 = await common.callInterface('ql-14458', param); //销售开单-按款号汇总
		// console.log(`qlRes1 = ${JSON.stringify(qlRes1)}`);
		let sfRes = await common.editBilling(jsonparam);
		// console.log(`sfRes = ${JSON.stringify(sfRes)}`);
		await common.delay(100); //等待ql-14458 num更新
		let qlRes2 = await common.callInterface('ql-14458', param);
		// console.log(`qlRes2 = ${JSON.stringify(qlRes2)}`);
		let exp = {
			marketdate: styleAgc001.marketdate,
			mat_color: '白色', //与colorids对应
			num: +qlRes1.dataList[0].num + sfRes.params.details[0].num, //
			mat_code: styleAgc001.code,
			providerid: styleAgc001.show_dwid,
			main_optime: common.getCurrentDate('YY-MM-DD'),
			mat_size: 'M', //
			mat_name: styleAgc001.name,
			invnum: +qlRes1.dataList[0].invnum - sfRes.params.details[0].num, //库存
			brandid: styleAgc001.show_brandid,
			mat_money: +qlRes1.dataList[0].mat_money + qlRes2.dataList[0].stdprice1 * sfRes.params.details[0].num + '',
		};
		common.isApproximatelyEqualAssert(exp, qlRes2.dataList[0]);
	});

	// 实销数与“销售开单-按明细查-输入时间、门店、款号”查询的底部数据汇总数量、“统计分析-利润表-时间、门店、款号”查询的底部数据汇总数量一致
	it('170308.2 按款号汇总实销数与按明细查,统计分析-利润表底部数据一致', async function () {
		let params = { //只获取底部汇总数据
			'ql-1209': { //销售开单-按明细查
				shopid: LOGINDATA.invid, //使用登陆门店查询
				styleid: styleAgc001.pk,
				today: true,
				search_list: 0,
				search_count: 0,
			},
			'ql-1447001': { //统计分析-日利润表
				invid: LOGINDATA.invid,
				styleid: styleAgc001.pk,
				today: true,
				search_list: 0,
				search_count: 0,
			},
			'ql-14458': { //销售开单-按款号汇总
				shopid: LOGINDATA.invid,
				styleid: styleAgc001.pk,
				today: true,
				search_list: 0,
				search_count: 0,
			},
		};
		let qlRes = await common.getResults(params);
		let ret1 = common.isApproximatelyEqual(qlRes['ql-14458'].sumrow, qlRes['ql-1209'].sumrow);
		let ret2 = common.isApproximatelyEqual(qlRes['ql-14458'].sumrow, {
			num: qlRes['ql-1447001'].sumrow.totalnum,
			// mat_money: qlRes['ql-1447001'].sumrow.totalmoney,
		});
		expect(ret1.flag, `按款号汇总与销售开单-按明细查 底部实销数不一致\n${ret1.errorMsg}`).to.be.true;
		expect(ret2.flag, `按款号汇总与统计分析-日利润表 底部实销数不一致\n${ret2.errorMsg}`).to.be.true;
	});

	// 库存数与“货品管理-当前库存-输入款号、门店”查询的库存  一致
	it('170308.3 按款号汇总实销数与货品管理-当前库存 库存一致', async function () {

		/**
		 * 查询条件中的颜色尺码要传，否则当前库存、按款和汇总数据可能对不上
		 * 原因：按款号汇总只汇总销售过的数据，但是有些颜色尺码有库存，但并未销售过，
		 * 所以：汇总的数据就可能缺少有库存但未销售过的库存，导致跟当前库存的汇总对不上
		 */
		let params = { //只获取底部汇总数据
			'ql-1932': { //货品管理-当前库存
				invid: LOGINDATA.invid, //使用登陆门店查询
				propdresStyleid: styleAgc001.pk,
				search_list: 0,
				search_count: 0,
				propdresColorid: BASICDATA.coloridBaiSe,
				propdresSizeid: BASICDATA.sizeidM,
			},
			'ql-14458': { //销售开单-按款号汇总
				shopid: LOGINDATA.invid,
				styleid: styleAgc001.pk,
				today: false,
				search_list: 0,
				search_count: 0,
				colorid: BASICDATA.coloridBaiSe,
				sizeid: BASICDATA.sizeidM,
			},
		};
		let qlRes = await common.getResults(params);
		assert.equal(Number(qlRes['ql-1932'].sumrow.invnum), qlRes['ql-14458'].sumrow.invnum, `货品管理-当前库存 库存值:${qlRes['ql-1932'].sumrow.invnum}
		与销售开单-按款号汇总 库存值:${qlRes['ql-14458'].sumrow.invnum}不同`);
		// expect(qlRes['ql-1932'].sumrow.invnum, `货品管理-当前库存 库存值:${qlRes['ql-1932'].sumrow.invnum}
		// 与销售开单-按款号汇总 库存值:${qlRes['ql-14458'].sumrow.invnum}不同`).to.equal(qlRes['ql-14458'].sumrow.invnum);
	});

	//款号停用后,还是应显示当前实际库存,而不应显示为0
	it('170503.款号停用后的库存数检查', async () => {
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		}); //新增货品

		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].styleid = goodRes.pk;
		let sfRes = await common.editBilling(json); //使用新增货品开单

		let cancelRes = await common.callInterface('cs-1511-33', { //停用货品
			pk: goodRes.pk,
		});
		expect(cancelRes, `停用货品失败 ${JSON.stringify(cancelRes)}`).to.includes({
			val: 'ok',
		});

		let param = format.qlParamsFormat({
			styleid: goodRes.pk,
		}, true);
		let res = await common.callInterface('ql-14458', param);
		common.isApproximatelyEqualAssert({
			num: sfRes.params.details[0].num, // 实销数
			invnum: '-' + sfRes.params.details[0].num, //库存
		}, res.dataList[0]);
	});

	describe('170437.门店查询', function () {
		let shopAssert = async (right, errorMsg) => {
			let params = format.qlParamsFormat({
				shopid: BASICDATA.shopidZzd,
			}, false);
			let res = await common.callInterface('ql-14458', params);
			expect(res.dataList.length).to.satisfy((length) => {
				return right ? length > 0 : length == 0;
			}, `${errorMsg},实际查询结果datalist.length=${res.dataList.length}`);
		};
		after(async function () {
			await common.loginDo();
		});
		it('总经理-显示全部门店的款号销售数、库存', async function () {
			await shopAssert(true, '期望查询结果大于0');
		});
		it('其他店员只能查询到本门店的款号销售数、库存', async function () {
			await common.loginDo({
				logid: '004',
				pass: '000000',
			});
			await shopAssert(false, '期望查询结果等于0');
		});
	});
});
