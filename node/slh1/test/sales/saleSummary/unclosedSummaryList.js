'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');

/*
 * 销售开单--按汇总--按客户未结统计
 */

describe('销售开单--按汇总--客户未结统计-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('170332,客户未结列表数据验证.rankA', async function () {

		let unclosedSummary = await common.callInterface('ql-14485', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));

		/*
		 *  验证 实收=现金+微信+刷卡+汇款+支付宝
		 */
		summaryListHelp.paysumCheckAssert(unclosedSummary.dataList[0]);

		/*
		 *  验证'实付'>、=、<'应付的时候' 未结统计列表显示值是否正常
		 */
		let unclosed = 0; //未结
		//第一次 实付 > 应付
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.cash = 1000;
		jsonparam = format.jsonparamFormat(jsonparam);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `第一次销售单开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		let billingInfo = await salesReqHandler.salesQueryBilling(salesResult.pk);
		unclosed = billingInfo.result.balance;

		let unclosedSummaryAfter = await common.callInterface('ql-14485', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		summaryListHelp.moneyInfoAssert(summaryListHelp.moneySum(unclosedSummary.dataList[0], jsonparam), unclosedSummaryAfter.dataList[0], '客户未结汇总1-前后');

		assert(Number(unclosedSummaryAfter.dataList[0].specmoney), common.add(unclosedSummary.dataList[0].specmoney, unclosed), `按客户未结'未结'字段统计错误`);

		//common.isApproximatelyArrayAssert({unclosedSummaryAfter.dataList[0].specmoney},);
		// 第二次 实付 = 未结   这一次不是未结单 因此unclosedSummary==unclosedSummaryAfter
		jsonparam.cash = 300;
		jsonparam = format.jsonparamFormat(jsonparam);
		unclosedSummary = _.cloneDeep(unclosedSummaryAfter);
		salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `第二次销售单开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		billingInfo = await salesReqHandler.salesQueryBilling(salesResult.pk);
		unclosed = billingInfo.result.balance;

		unclosedSummaryAfter = await common.callInterface('ql-14485', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		common.isApproximatelyEqualAssert(unclosedSummary, unclosedSummaryAfter);

		//第三次 实付 < 应付
		jsonparam.cash = 0;
		jsonparam = format.jsonparamFormat(jsonparam);
		unclosedSummary = _.cloneDeep(unclosedSummaryAfter);
		salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `第三次销售单开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		billingInfo = await salesReqHandler.salesQueryBilling(salesResult.pk);
		unclosed = billingInfo.result.balance;

		unclosedSummaryAfter = await common.callInterface('ql-14485', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		summaryListHelp.moneyInfoAssert(summaryListHelp.moneySum(unclosedSummary.dataList[0], jsonparam), unclosedSummaryAfter.dataList[0], '客户未结汇总-前后');
		expect(Number(unclosedSummaryAfter.dataList[0].specmoney), `按客户未结'未结'字段统计错误`).to.be.equal(common.add(unclosedSummary.dataList[0].specmoney, unclosed));

	});

	/*
	 *  验证 '未结统计' 与 '按批次查汇总栏' 是否一致
	 */
	it('170332,"客户未结"与"按批次查汇总栏".rankA', async function () {
		let unclosedSummary = await common.callInterface('ql-14485', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		let batchList = await common.callInterface('ql-142201', format.qlOnlySumFormat({
			'dwid': BASICDATA.dwidXw,
			'flag': 0
		}, true));
		let compareField = 'cash;card;remit;paysum=actualpay;num=totalnum;totalmoney'; //daishou
		common.isFieldsEqualAssert(unclosedSummary.dataList[0], batchList.sumrow, compareField);
		expect(unclosedSummary.dataList[0].salenum, `客户未结'退货数'统计错误`).to.be.equal((Number(unclosedSummary.dataList[0].num) + Number(unclosedSummary.dataList[0].backnum)).toString());
	});

	it('170332,客户未结明细数据验证.rankA', async function () {
		// 销售开单
		let sfRes = await common.editBilling(basiceJsonparam.salesJson());

		// 批次比较
		let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({ //销售开单-按批次查
			'dwid': BASICDATA.dwidXw,
			'flag': 0,
			'invalidflag': 0,
			'shopid': LOGINDATA.invid,
		}, true));
		let unclosedSummary = await common.callInterface('ql-144851', format.qlParamsFormat({ //按客户未结-详细
			'dwid': BASICDATA.dwidXw,
			'shopid': LOGINDATA.invid,
		}, true));
		let compareResult = isBatchListEqulaSummaryList(batchList.dataList, unclosedSummary.dataList);
		expect(compareResult.isEqual, compareResult.errorMsg).to.be.true;

		// 明细比较
		let billingInfo = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		let param = {
			'pk': sfRes.result.pk,
		};
		if (USEECINTERFACE == '2') param.bizType = 1100; //销售单
		let summaryInfo = await common.callInterface('qf-1934', param);
		compareResult = isBatchInfoEqualSummaryInfo(billingInfo.result, summaryInfo);
		expect(compareResult.isEqual, `${compareResult.errorMsg}`).to.be.true;

	});
	//有bug，店长可以看到所有门店的数据
	it('170829.店员查看按客户未结的权限', async function () {
		TESTCASE = {
			describe: "按店员汇总-店长不能看到其他门店数据",
			expect: `res['ql-14485'].dataList[0]=res['ql-142201'].sumrow`,
			jira: 'SLHSEC-6769店长显示所有门店数据'
		}
		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		let params = {
			'ql-14485': {
				'dwid': BASICDATA.dwidXw,
				'today': true
			}, //按客户未结
			'ql-142201': {
				'dwid': BASICDATA.dwidXw,
				'flag': 0,
				'today': true
			} //销售开单-按批次查
		};
		let res = await common.getResults(params);
		let compareField = 'cash;card;remit;paysum=actualpay;daishou;num=totalnum;totalmoney';
		let compareResult = common.isFieldsEqual(res['ql-14485'].dataList[0], res['ql-142201'].sumrow, compareField);

		await common.loginDo();
		if (!compareResult.flag) throw new Error(`销售开单-按客户未结结果!=按批次查结果${compareResult.errorMsg}`);
	});
});


/*
 * 判断'客户未结明细列表' 跟 '按批次查' 数据是否一致
 * 客户未结明细列表中包含应收调整单的数据,因此dataList.length不同
 */
function isBatchListEqulaSummaryList(batchList, summaryList) {
	let errorMsg = '';

	const fields = 'optime=prodate;totalmoney;daishou;remit;seller;id;balance;weixinpay;alipay;cash;card;billno'; //
	summaryList.forEach(obj => {
		const exp = common.takeWhile(batchList, (data) => data.id == obj.id);
		if (exp[0]) {
			let result = common.isFieldsEqual(obj, exp[0], fields); //验证相同批次号的数据是否相同
			if (!result.flag) {
				errorMsg += `\nbillno:${obj.billno} 数据不对${result.errorMsg}`;
			};
		} else {
			//未匹配到的是应收调整单 190193中验证
			// console.log(`未找到billno:${obj.billno}`);
		};
	});
	return {
		'isEqual': errorMsg == '',
		'errorMsg': errorMsg
	};
};


/*
 * 判断 '客户未结明细的明细' 跟 '按批次查的明细' 数据是否是一致的
 */

function isBatchInfoEqualSummaryInfo(batchInfo, summaryInfo) {
	// 比较mainfield部分相同
	let compareField = 'totalmoney;deliver;remark;show_deliver;totalnum;billno;prodate;pk';
	let mainFieldCompare = common.isFieldsEqual(batchInfo, summaryInfo, compareField);
	if (!mainFieldCompare.flag) {
		return mainFieldCompare;
	}

	// 比较details.count相同
	if (batchInfo.details.count != summaryInfo.details.count) {
		return {
			'isEqual': false,
			'errorMsg': 'details.count不同'
		};
	}

	// 比较detailsfield部分是否相同
	for (let i = 0; i < summaryInfo.details.length; i++) {
		for (let j = batchInfo.details.length; j > 0; j--) {
			let summaryItem = summaryInfo.details[i];
			let batchItem = batchInfo.details[j - 1];
			if (summaryItem.pk == batchItem.pk) {
				compareField = 'total;show_colorid;stylecode=show_styleid;num;price;stylename=matPropdresStyleName;sizeid;styleid;colorid;show_sizeid;pk';
				let detailFieldCompare = common.isFieldsEqual(batchItem, summaryItem, compareField);
				if (!detailFieldCompare.flag) {
					return detailFieldCompare;
				} else {
					batchInfo.details.splice(j - 1, 1);
				}
			}
		}
	}

	return {
		'isEqual': true
	};
}
