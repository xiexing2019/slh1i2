'use strict';

const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');
const summaryListHelp = require('../../../help/salesHelp/summaryListHelp.js');
const basicReqHandler = require('../../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 销售开单--按汇总--按退货汇总
 */

describe('销售开单--按退货汇总-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	// after(async() => {});

	it('170313,验证按退货汇总列表', async function () {
		//销售开单
		await common.editBilling(basiceJsonparam.salesJson());

		// 退货
		let tempjson = basiceJsonparam.salesJson();
		for (let i = 0; i < tempjson.details.length; i++) {
			tempjson.details[i].num = '-1';
		}
		await common.editBilling(tempjson);

		//查询按退货汇总、按明细查
		let searchCondition = format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw,
			'invid': LOGINDATA.invid,
			'shopid': LOGINDATA.invid,
			'typeid': '1'
		}, true);
		let returnSummary = await common.callInterface('ql-14489', searchCondition);
		let detailList = await common.callInterface('ql-1209', searchCondition);

		//找出所有的抹零金额
		let matMoney = 0,
			num = Math.abs(Number(detailList.sumrow.num));
		for (let i = 0; i < detailList.dataList.length; i++) {
			if (!['抹零', '积分抵现'].includes(detailList.dataList[i].mat_name)) { //排除特殊货品
				matMoney = common.add(matMoney, detailList.dataList[i].mat_money);
			};
		};

		//sum(det.num * det.price * det.discount)/sum(det.num) AS avgreturnprice
		let avgreturnprice = common.div(Math.abs(matMoney), num);
		assert.equal(num, returnSummary.dataList[0].num, `退货数量错误`);
		assert.equal(avgreturnprice, returnSummary.dataList[0].avgreturnprice, `平均退货价错误`);
		assert.equal(returnSummary.sumrow.num, returnSummary.dataList[0].num, `汇总栏退货数量错误`);
		assert.equal(returnSummary.sumrow.totalreturn, returnSummary.dataList[0].totalreturn, `汇总栏退货金额错误`);
	});

	it('170394,验证退货汇总明细中不包含采购退货', async function () {
		// 入库
		let jsonparam = format.jsonparamFormat(basiceJsonparam.purchaseJson());
		let purchaseResult = await common.callInterface('sf-14212-1', {
			'jsonparam': jsonparam
		});
		expect(purchaseResult, `入库单失败`).to.not.have.property('error');
		// 退货
		let tempjson = basiceJsonparam.purchaseJson();
		for (let i = 0; i < tempjson.details.length; i++) {
			tempjson.details[i].num = -1;
		}
		let returnJson = format.jsonparamFormat(tempjson);
		let returnMats = await common.callInterface('sf-14212-1', {
			'jsonparam': returnJson
		});
		expect(returnMats, `退货失败`).to.not.have.property('error');

		// 验证退货汇总明细中没有采购退货单
		let searchCondition = {
			'dwid': returnJson.dwid,
			'invid': returnJson.shopid,
			'typeid': 1
		}
		let detailList = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));
		expect(detailList, `退货汇总明细中包含了采购退货信息 : ${JSON.stringify(detailList)}`).to.includes({
			'count': '0'
		});
	});

	it('170644,退货汇总不能包含调出单退货', async function () {

		//退货前查询按退货汇总、按退货汇总明细
		let searchCondition = {
			'typeid': '1'
		};
		let returnSummary = await common.callInterface('ql-14489', format.qlParamsFormat(searchCondition, true));
		let detailList = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));

		// 门店调出退货
		let outJson = format.jsonparamFormat(basiceJsonparam.outJson());
		outJson.invidref = BASICDATA.shopidCkd;
		let outResult = await common.callInterface('sf-1862-1', {
			'jsonparam': outJson
		});
		expect(outResult, `门店调出退货失败${JSON.stringify(outResult)}`).to.not.have.property('error');

		// 退货后再次查询按退货汇总、按退货汇总明细
		let returnSummaryAft = await common.callInterface('ql-14489', format.qlParamsFormat(searchCondition, true));
		let detailListAft = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));

		// 验证按退货汇总列表是否受门店调出退货影响
		common.isApproximatelyEqualAssert(returnSummary, returnSummaryAft);
		common.isApproximatelyEqualAssert(detailList, detailListAft);
	});

	it('170707,退货汇总明细--退货时间/退货明细不合并', async function () {
		TESTCASE = {
			describe: "退货汇总明细验证",
			jira: "二代连续开单报服务端错误，已处理，等发布"
		}
		let custResult = await salesReqHandler.addCust(basiceJsonparam.addCustJson()); //新增客户
		// console.log(`custResult : ${JSON.stringify(custResult)}`);

		// 开单
		let jsonparam = basiceJsonparam.salesJson();
		jsonparam.details[0].num = '15';
		jsonparam = format.jsonparamFormat(jsonparam);
		jsonparam.dwid = custResult.result.val;
		let salesResult = await common.editBilling(jsonparam);
		// 退货
		let tempjson = basiceJsonparam.salesJson();
		tempjson.details[0].num = '-1';
		tempjson.details.splice(1, 1);
		let returnJson = format.jsonparamFormat(tempjson);
		returnJson.dwid = custResult.result.val;
		let returnMats = await common.editBilling(returnJson);
		expect(returnMats, `第一次退货失败:${JSON.stringify(returnMats)}`).to.not.have.property('error');
		// 查询退货汇总明细
		let searchCondition = {
			'dwid': custResult.result.val,
			'typeid': '1'
		};
		let detailList = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));

		//验证退货明细数据 17-11-21
		detailList.dataList[0].prodate = `20${detailList.dataList[0].prodate}`;
		common.isApproximatelyEqualAssert({
			'totalreturn': returnJson.details[0].total,
			'num': returnJson.details[0].num,
			'mat_code': 'agc001',
			'price': returnJson.details[0].price,
			'dw_name': custResult.params.nameshort,
			'seller': '总经理',
			'prodate': returnJson.prodate
		}, detailList.dataList[0]);


		//第二次退货：验证相同的款号颜色尺码数量客户的时候，退货明细不合并
		returnJson = format.updateHashkey(returnJson);
		returnMats = await common.editBilling(returnJson);
		expect(returnMats, `第二次退货失败:${JSON.stringify(returnMats)}`).to.not.have.property('error');
		let detailList2 = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));
		expect(detailList.count, `信息完全相同，退货明细合并,正常情况下不应该合并`).to.not.equal(detailList2.count);

		// 第三次退货：验证汇总明细列表默认按照批次号排序 【即:最新的数据排列在最上面】
		returnJson.details[0].num = '-3';
		returnJson = format.jsonparamFormat(returnJson);
		returnMats = await common.editBilling(returnJson);

		expect(returnMats, `第三次退货失败:${JSON.stringify(returnMats)}`).to.not.have.property('error');
		let detailList3 = await common.callInterface('ql-144891', format.qlParamsFormat(searchCondition, true));
		common.isApproximatelyEqualAssert({
			'num': '-3'
		}, detailList3.dataList[0]);
	});

	it('170725,【销售开单-按汇总-按退货汇总】变动款号季节', async function () {
		//新增货品 库存为0 无季节
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		});

		let goodInfo = await common.fetchMatInfo(goodRes.pk); //获取货品信息

		// 开单
		let jsonparam = basiceJsonparam.simpleSalesJson();
		jsonparam.details[0].styleid = goodRes.pk; //明细1改成特殊货品
		await common.editBilling(jsonparam); //新增销售单

		// 退货前查询“按退货汇总列表”
		let searchCondition = format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw,
			'invid': LOGINDATA.invid,
			'season': '0', //空季节
		}, true);
		let blankSummary = await common.callInterface('ql-14489', searchCondition);
		searchCondition.season = BASICDATA.seasonidSpring;
		let springSummary = await common.callInterface('ql-14489', searchCondition);

		// 退货
		jsonparam.details[0].num = -1; //使用新增货品退货
		await common.editBilling(jsonparam);

		// 修改货品季节信息
		goodInfo.season = BASICDATA.seasonidSpring;
		goodInfo.action = 'edit';
		goodRes = await basicReqHandler.editStyle({
			jsonparam: goodInfo,
		});

		// 退货后查询“按退货汇总列表”
		searchCondition.season = '0';
		let blankSummaryAft = await common.callInterface('ql-14489', searchCondition);
		searchCondition.season = BASICDATA.seasonidSpring;
		let springSummaryAft = await common.callInterface('ql-14489', searchCondition);

		common.isApproximatelyEqualAssert(blankSummaryAft.dataList[0], blankSummary.dataList[0]);
		common.isApproximatelyEqualAssert({
			'num': common.add(springSummary.dataList[0].num, Math.abs(Number(jsonparam.details[0].num))),
		}, springSummaryAft.dataList[0]);

	});

});
