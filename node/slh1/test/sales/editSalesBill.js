const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const salesResFmt = require('../../help/salesHelp/salesResFormat');
const reqHandler = require('../../help/reqHandlerHelp');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const printReqHandler = require('../../help/printHelp/printRequestHandler');

let jsonparam;
describe('单据验证', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		// console.log(`BASICDATA=${JSON.stringify(BASICDATA)}`);
	});

	it('170063.修改客户-slh2', async function () {
		//单据是否允许修改客户或厂商
		//0不允许 1允许
		await common.setGlobalParam('dwxx_not_allow_edit', 0);

		jsonparam = basiceJsonparam.salesJson();
		jsonparam.cash = 1000, jsonparam.card = jsonparam.remit = 0;
		let sfRes = await common.editBilling(jsonparam);

		jsonparam.action = 'edit';
		jsonparam.dwid = BASICDATA.dwidLs;
		jsonparam.pk = sfRes.result.pk;
		sfRes = await common.editBilling(jsonparam, false);
		expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
			'error': '客户或供应商信息不允许修改',
		});

		//汇款必须填写客户信息
		jsonparam.dwid = '';
		sfRes = await common.editBilling(jsonparam, false);
		expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
			'error': '客户或供应商信息不允许修改',
		});
	});

	it('170095.整单备注和明细备注.rankA-slh2', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesJson());
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('170112.开单时显示当前库存-slh2', async function () {
		let json = {
			'cs-getinvnum-bycolorsize': { //获取货品库存信息
				stylecode: 'agc001',
				isCurShop: 1, //是否只显示当前门店信息 是
			},
			'ql-1932': { //当前库存
				propdresStyleid: BASICDATA.styleidAgc001,
				invid: LOGINDATA.invid,
			},
		};
		let results = await common.getResults(json);
		// console.log(results['cs-getinvnum-bycolorsize']);

		common.isApproximatelyArrayAssert(results['ql-1932'].dataList,
			salesResFmt.cs_getinvnumbycolorsizeFormat(results['cs-getinvnum-bycolorsize'].dataList), [], `cs-getinvnum-bycolorsize库存信息数据与当前库存信息不匹配`);
	});

	//有bug totalmoney只能2位？
	it.skip('170526.特殊货品为小数-slh2-调', async function () {
		let params = [{
			code: 'pricedec', //单价小数位
			val: 3, //0元 1角 2分 3厘
		}, {
			code: 'sales_need_roundoff', //总计是否需要四舍五入
			val: 0, //0不需要 1需要
		}, {
			code: 'sales_discounted_price_mode', //折扣后单价的核算模式
			val: 0, //默认不作处理，保留精确小数
		}];
		await common.setGlobalParams(params);

		jsonparam = basiceJsonparam.specialSalesJson();
		jsonparam.details[2].price = 0.985; //0.985
		jsonparam.details[3].price = 1.23;
		let sfRes = await common.editBilling(jsonparam);
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('170072.款号价格为负数时检查.rankA-slh2', async function () {
		jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
		jsonparam.details[0].price = -jsonparam.details[0].price;
		let res = await common.editBilling(jsonparam, false);
		expect(res.result).to.includes({
			'error': '[第1行]单价或金额不能为负数',
		}, `error: ${JSON.stringify(res.result)}`);
	});

	it('170906.作废天数-先判断全局参数,再功能参数.rankA-slh2', async function () {
		await common.setGlobalParam('sales_invalidate_days', 5); //修改作废5天内的单据
		await common.setRoleParam({
			'ipadcode': 'shopmanager', //店长
			'code': 'cancel_his', //单据可作废天数
			'val': '3' //可作废3天内单据
		});
		let json = basiceJsonparam.simpleSalesJson();
		json.prodate = common.getDateString([0, 0, -10]);
		let sfRes = await common.editBilling(json); //生成一个10天前的销售单
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		qfRes.result.interfaceid = 'sf-14211-1';
		qfRes.result.action = 'edit';
		qfRes.result.remark += 'a';
		let sfResEdit = await common.editBilling(qfRes.result, false); //修改单据

		let cancelResult = await salesReqHandler.cancelSaleoutBill(sfRes.result.pk, false); //作废单据

		await common.loginDo(); //常青店登陆 防止影响后续用例

		expect(sfResEdit.result, '修改单据验证错误').to.includes({
			error: USEECINTERFACE == 1 ? '只允许作废或修改5天内的单据' : '只允许修改5天内的单据'
		});
		expect(cancelResult, '作废单据验证错误').to.includes({
			error: USEECINTERFACE == 1 ? '只允许作废或修改5天内的单据' : '只允许作废5天内的单据'
		});
	});

	it('171059.款号包含and,检查上次成交记录.rankA-slh2', async function () {
		let goodJson = basiceJsonparam.addGoodJson();
		goodJson.code = `goodand${common.getRandomStr(5)}`;
		goodJson.name = goodJson.code;
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: goodJson,
		});

		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].styleid = goodRes.pk;
		await common.editBilling(json);

		let param = format.qlParamsFormat({
			dwid: BASICDATA.dwidXw,
			styleid: goodRes.pk,
		})
		let result = await common.callInterface('ql-144612', param);
		result.dataList.forEach((obj) => {
			expect(obj.mat_code).to.equal(goodRes.params.code);
		});
	});

	// 款号中带@可以正常新增货品和开单
	// 直接用@作为款号,则无法新增货品(前端控制,UI验证)
	it('171125.@符号控制-slh2', async function () {
		let goodJson = basiceJsonparam.addGoodJson();
		goodJson.code += '@';
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: goodJson
		});

		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].styleid = goodRes.pk;
		await common.editBilling(json);
	});

	it('170166.查看修改日志(修改记录)-slh2', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.simpleSalesJson());
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = 'sf-14211-1';

		let sfResEdit = await common.editBilling(qfRes.result); //修改单据
		if (USEECINTERFACE == 2) await common.delay(1000);   //加点延迟  不然会报财务处理未完成 等小邱优化完不需要加延迟
		let printRes = await printReqHandler.getPrint(sfResEdit);
		// let printRes = await common.callInterface('cs-getPrint', { //打印
		// 	pk: sfResEdit.result.pk,
		// });
		let printTime = common.getCurrentTime();
		let qfResEdit = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		let editLog = await common.callInterface('qf-14211-3', { //查看修改日志
			pk: sfResEdit.result.pk
		});
		let exp = {
			hashkey: USEECINTERFACE == 1 ? sfRes.params.hashkey : sfResEdit.params.hashkey,
			optime: qfRes.result.optime,
			printtime: printTime,
			verifybillBillno: '0',
			delopstaffName: '',
			// modelClass: 'DLInvMain',
			updateoptime: qfResEdit.result.optime,
			// fileid: '',
			updateopstaffName: sfResEdit.params.opname,
			opname: LOGINDATA.name,
			pk: sfRes.result.pk,
		};
		common.isApproximatelyEqualAssert(exp, editLog); //
	});

	describe('汇款是否需要填写客户-slh2', function () {
		let json = basiceJsonparam.salesJson();
		json.dwid = ''; //不输入客户
		after(async () => {
			await common.setGlobalParam('sales_remit_require_dw', 1); //0可以不填写 1默认要填写
		});
		it('170583.汇款无需填写客户', async function () {
			await common.setGlobalParam('sales_remit_require_dw', 0);
			await common.editBilling(json);
		});
		it('170582.汇款需填写客户.rankA', async function () {
			await common.setGlobalParam('sales_remit_require_dw', 1);
			let res = await common.editBilling(json, false);
			expect(res.result).to.includes({
				'error': '汇款必须填写客户信息',
			}, `error: ${JSON.stringify(res.result)}`);
		});
	});

	describe('负库存开单检查-slh2', function () {
		//新增货品返回结果,使用新增货品开单的jsonparam,销售开单sf接口返回结果
		let [goodRes, json, sfRes] = [{}, {}, {}];
		before(async () => {
			goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}) //新增货品 库存为0
			// console.log(`goodRes = ${JSON.stringify(goodRes)}`);
			json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].colorid = goodRes.params.colorids.split(',')[0];
			json.details[0].sizeid = goodRes.params.sizeids.split(',')[0];
		});
		after(async () => {
			await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查，允许负库存
		});
		afterEach(async () => {
			if (sfRes.result && sfRes.result.pk) {
				await salesReqHandler.cancelSaleoutBill(sfRes.result.pk); //作废成功的单据 保持货品没有库存
				sfRes = {};
			}
		});

		it('170116.开单时不允许负库存.rankA', async function () {
			TESTCASE = {
				describe: 'invinout_checknum=1,,开单数量大于库存,验证错误提示',
				jira: 'SLHSEC-5775',
			};
			await common.setGlobalParam('invinout_checknum', 1); //是否允许负库存 检查，必须先入库再出库
			sfRes = await common.editBilling(json, false); //使用新增货品开单
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name},白色,M] 库存不足，差${sfRes.params.details[0].num}`);
		});

		it('170118.库存不足时开单修改界面', async function () {
			TESTCASE = {
				describe: 'invinout_checknum=1,,开单数量大于库存,验证错误提示',
				jira: 'SLHSEC-5775',
			};
			//先关闭验证。防止库存不足时，数据准备失败
			await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查，允许负库存
			sfRes = await common.editBilling(basiceJsonparam.simpleSalesJson()); //使用有库存的货品正常开单
			let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			// console.log(`sfRes = ${JSON.stringify(sfRes)}`);

			await common.setGlobalParam('invinout_checknum', 1); //是否允许负库存 检查，必须先入库再出库
			qfRes.result.action = 'edit';
			qfRes.result.interfaceid = 'sf-14211-1';
			qfRes.result.details[0] = json.details[0]; //
			sfRes = await common.editBilling(qfRes.result, false); //修改刚才的单据 把款号修改为库存为0或负数的款号 qfRes.result
			// console.log(`\nsfRes = ${JSON.stringify(sfRes)}`);
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name},白色,M] 库存不足，差${sfRes.params.details[0].num}`);
		});

		it('有库存记录,开单时不允许负库存', async function () {
			//新增入库 使之有库存记录
			let purJson = _.cloneDeep(json);
			purJson.interfaceid = 'sf-14212-1';
			delete purJson.dwid;
			await common.editBilling(purJson);

			json.details[0].num += 5;
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name},白色,M] 库存不足，差5`);
		});

		it('170117.开单时允许负库存', async function () {
			await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查，允许负库存
			sfRes = await common.editBilling(json); //使用新增货品开单

			let param = format.qlParamsFormat({
				propdresStyleid: goodRes.pk,
			}, false);
			let qlResult = await common.callInterface('ql-1932', param); //验证库存
			// console.log(`qlResult=${JSON.stringify(qlResult)}`);
			assert.equal(-5, qlResult.sumrow.invnum, `开单时允许负库存时，开单，当前库存${goodRes.params.code}的库存数错误`);
		});
	});

	//用例需要先做数据准备 再修改参数
	describe('窜码发货', function () {
		//新增货品返回结果,使用新增货品开单的jsonparam,销售开单sf接口返回结果
		let [goodRes, json, sfRes] = [{}, {}, {}];
		before(async () => {
			await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查，允许负库存
			await common.setGlobalParam('sales_checkbacknum', 0); //开单保存开启退货数和上次购买数的比对验证 默认不开启

			goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			}); //新增货品 库存为0

			json = basiceJsonparam.salesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[1].styleid = goodRes.pk;
			await common.editBilling(json);
		});
		after(async () => {
			await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查，允许负库存
			await common.setGlobalParam('fin_price_base', 0); //财务中货品成本价的核算方法 按最新进货价
		});

		it('170651.库存总数为正/170652.修改时超过总库存(一正一负)', async function () {
			json = await dataPrepareFor170651(goodRes, [-10, 20]); //使库存变为[-10,20]

			json.details[0].num = 1;
			sfRes = await common.editBilling(json); //使用新增货品开单

			let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			qfRes.result.action = 'edit';
			qfRes.result.interfaceid = 'sf-14211-1';
			qfRes.result.details[0].num = 11; //超过库存总数
			let editSfRes = await common.editBilling(qfRes.result, false); //修改刚才的单据 把款号修改为库存为0或负数的款号
			expect(editSfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);
		});

		it('170653.开单时超过总库存(一正一负)', async function () {
			json = await dataPrepareFor170651(goodRes, [-10, 20]); //使库存变为[-10,20]
			json.details[0].num = 11; //开单11件 超过总库存
			sfRes = await common.editBilling(json, false); //使用新增货品开单,超过总库存
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);
		});

		it('170654.库存总数为正(2个都为正)', async function () {
			json = await dataPrepareFor170651(goodRes, [5, 10]); //使库存变为[5,10]
			json.details[0].num = 6;
			await common.editBilling(json); //使用新增货品开单
		});

		it('170655.开单时超过总库存(一正一负)', async function () {
			json = await dataPrepareFor170651(goodRes, [5, 10]); //使库存变为[5,10]
			json.details[0].num = 16; //开单11件 超过总库存
			sfRes = await common.editBilling(json, false); //使用新增货品开单,超过总库存
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);
		});

		it('170656.库存总数为0', async function () {
			json = await dataPrepareFor170651(goodRes, [-5, 5]); //使库存变为[-5,5]
			json.details[0].num = 1;
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);
		});

		it('170657.库存总数为负', async function () {
			json = await dataPrepareFor170651(goodRes, [-10, 5]); //使库存变为[-10,5]
			json.details[0].num = 1;
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差6`);
		});

		it.skip('170658.不计算其他门店的数据/170659.不计算在途数', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			json.details[0].num = -10;
			sfRes = await common.editBilling(json); //中洲店 尺码A 10件

			let jsonparam = format.jsonparamFormat({
				interfaceid: 'sf-1862-1',
				details: [{
					colorid: goodRes.params.colorids.split(',')[0],
					num: 8,
					price: 200,
					sizeid: goodRes.params.sizeids.split(',')[0],
					styleid: goodRes.pk,
				}],
			});
			let sfResOut = await common.editBilling(jsonparam); //中洲店向常青店做调出单

			await common.loginDo();
			json = await dataPrepareFor170651(goodRes, [0, 0]); //使库存变为[0,0]
			json.details[0].num = 1;
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);
		});

		it('170660.允许修改采购进货单的数量', async function () {
			json = await dataPrepareFor170651(goodRes, [0, 0]);

			let jsonPurchase = format.jsonparamFormat({
				interfaceid: 'sf-14212-1',
				cash: 900,
				details: [{
					num: 9,
					styleid: goodRes.pk,
					sizeid: goodRes.params.sizeids.split(',')[0],
					colorid: goodRes.params.colorids.split(',')[0],
				}],
			});
			let sfResPurchase = await common.editBilling(jsonPurchase); //采购入库9件
			let qfResult = await reqHandler.queryBilling({
				interfaceid: 'qf-14212-1',
				pk: sfResPurchase.result.pk
			}); //获取采购入库单明细

			json.details[0].num = 9;
			await common.editBilling(json); //销售开单9件

			await common.setGlobalParam('fin_price_base', 2); //财务中货品成本价的核算方法 按移动加权平均价

			qfResult.result.action = 'edit';
			qfResult.result.interfaceid = 'sf-14212-1';
			qfResult.result.details[0].num = 8; //库存变为-1
			qfResult.result.totalmoney = 800;
			sfRes = await common.editBilling(format.jsonparamFormat(qfResult.result), false);
			//用例更新 不允许负库存
			//http://jira.hzdlsoft.com:7082/browse/SLH-20998?filter=-2
			expect(sfRes.result.error).to.includes(`[常青店]中[${goodRes.params.code},${goodRes.params.name}] 库存不足，差1`);

			qfResult = await reqHandler.queryBilling({
				interfaceid: 'qf-14212-1',
				pk: sfResPurchase.result.pk
			}); //获取采购入库单明细
			qfResult.result.action = 'edit';
			qfResult.result.interfaceid = 'sf-14212-1';
			qfResult.result.details[0].num = 11; //库存变为2
			qfResult.result.totalmoney = 1100;
			sfRes = await common.editBilling(format.jsonparamFormat(qfResult.result), false);
			expect(sfRes.result, `2.修改入库单失败:\n${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
	});

	describe('销售开单，是否允许修改单据日期-slh2', function () {
		before(async () => {
			await common.setGlobalParam('sales_invalidate_days', 5); //修改作废5天内的单据
		});
		after(async () => {
			await common.setGlobalParam('sc_fixprodate', 0); //是否允许修改单据日期 不限制修改单据日期
		});
		it('170468,【销售开单+开单】是否允许修改单据日期--不限制.rankA', async function () {
			await common.setGlobalParam('sc_fixprodate', 0); //是否允许修改单据日期 不限制修改单据日期
			let salesRes = await common.editBilling(basiceJsonparam.salesJson());

			let salesForm = await salesReqHandler.salesQueryBilling(salesRes.result.pk);
			salesForm.result.interfaceid = salesRes.params.interfaceid;
			salesForm.result.prodate = common.getDateString([0, 0, -1]);
			salesRes = await common.editBilling(salesForm.result, false);
			if (USEECINTERFACE == 2) {
				//二代不允许修改单据了
				expect(salesRes.result, `不予许修改单据但是修改单据成功:${JSON.stringify(salesRes)}`).to.includes({ error: '涉及账款的单据暂不支持修改日期' });

			} else {
				expect(salesRes.result, `参数设置的不限制修改单据的日期，但现在修改日期失败:${JSON.stringify(salesRes)}`).to.not.have.property('error');
			};
		});
		it('170469,【销售开单+开单】是否允许修改单据日期--限制修改销售单日期.rankA', async function () {
			await common.setGlobalParam('sc_fixprodate', 1); //是否允许修改单据日期 限制修改销售单日期
			let salesRes = await common.editBilling(basiceJsonparam.salesJson());

			let salesForm = await salesReqHandler.salesQueryBilling(salesRes.result.pk);
			salesForm.result.interfaceid = salesRes.params.interfaceid;
			salesForm.result.prodate = common.getDateString([0, 0, -1]);
			salesRes = await common.editBilling(salesForm.result, false);
			expect(salesRes.result, `参数设置的限制修改单据的日期，但现修改单据日期未出现预期提示语:${JSON.stringify(salesRes)}`).to.includes({
				'error': '系统设定不允许修改开单日期'
			});
		});
		describe('170470,【销售开单+开单】是否允许修改单据日期--限制修改所有单据日期.rankA', function () {
			async function test170470({
				jsonParam,
				qfIFC
			}) {
				let sfRes = await common.editBilling(jsonParam);
				let qfRes = await reqHandler.queryBilling({
					interfaceid: qfIFC,
					pk: sfRes.result.pk,
				});
				qfRes.result.interfaceid = sfRes.params.interfaceid;
				qfRes.result.prodate = common.getDateString([0, 0, -1]);
				sfRes = await common.editBilling(qfRes.result, false);
				expect(sfRes.result, `参数设置的限制修改单据的日期，但现修改采购入库单日期未出现预期提示语:${JSON.stringify(sfRes)}`).to.includes({
					'error': '系统设定不允许修改开单日期'
				});
			};
			before(async () => {
				await common.setGlobalParam('sc_fixprodate', 2); //是否允许修改单据日期 限制修改所有单据日期
			});
			after(async () => {
				await common.loginDo();
			});
			it('1.销售开单', async function () {
				await test170470({
					jsonParam: basiceJsonparam.salesJson(),
					qfIFC: 'qf-14211-1'
				});
			});
			it('2.销售订货', async function () {
				await test170470({
					jsonParam: basiceJsonparam.salesOrderJson(),
					qfIFC: 'qf-14401-1'
				});
			});
			it('3.采购入库', async function () {
				await test170470({
					jsonParam: basiceJsonparam.purchaseJson(),
					qfIFC: 'qf-14212-1'
				});
			});
			it('4.采购订货', async function () {
				await test170470({
					jsonParam: basiceJsonparam.purchaseOrderJson(),
					qfIFC: 'qf-22101-1'
				});
			});
			it('5.门店调出', async function () {
				await common.loginDo({
					'logid': '200',
				});
				await test170470({
					jsonParam: basiceJsonparam.outJson(),
					qfIFC: 'qf-1862-1'
				});
			});
		});

	});

	describe('单据打印后修改-slh2', function () {
		let qfRes, qfResPrint;
		before(async () => {
			let roleParam = {
				'ipadcode': 'shopmanager', //店长
				'code': 'modifyafterprint', //销售单修改权限
				'val': -1 //保存后不允许修改
			};
			await common.setRoleParam(roleParam);
			await common.loginDo({
				'logid': '004',
			});
			let json = basiceJsonparam.simpleSalesJson();
			//json.printflag = 0;
			let sfRes = await common.editBilling(json);
			qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);

			qfRes.result.interfaceid = 'sf-14211-1';
			qfRes.result.action = 'edit';
			//qfRes.result.actid = '21';
			//json.printflag = 1;
			sfRes = await common.editBilling(json);
			if (USEECINTERFACE == 2) await common.delay(1500);  //这里二代稍微加延迟 因为财务计算问题 要等邱处理了去掉
			await printReqHandler.getPrint(sfRes);
			qfResPrint = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			qfResPrint.result.interfaceid = 'sf-14211-1';
			qfResPrint.result.action = 'edit';
		});
		after(async () => {
			await common.loginDo();
			await common.setGlobalParam('cannotmodifyafterprint', 0); //单据打印后不允许修改 不限制
		});

		it('170773.单据打印后不允许修改-都不允许修改.rankA', async function () {
			await common.setGlobalParam('cannotmodifyafterprint', 2); //单据打印后不允许修改 都不允许修改
			let roleParam = {
				'ipadcode': 'shopmanager', //店长
				'code': 'modifyafterprint', //销售单修改权限
				'val': -1 //保存后不允许修改
			};
			await common.setRoleParam(roleParam);
			await common.loginDo({
				'logid': '004',
			});
			await editBillAfterPrintCheck(qfRes, qfResPrint);
		});

		it('170774.单据打印后不允许修改-不限制.rankA', async function () {
			await common.setGlobalParam('cannotmodifyafterprint', 0); //单据打印后不允许修改 不限制
			let roleParam = {
				'ipadcode': 'shopmanager', //店长
				'code': 'modifyafterprint', //销售单修改权限
				'val': -1 //保存后不允许修改
			};
			await common.setRoleParam(roleParam);
			await common.loginDo({
				'logid': '004',
			});
			await editBillAfterPrintCheck(qfRes, qfResPrint);
		});

		it('170775.单据打印后不允许修改-明细不允许修改', async function () {
			await common.setGlobalParam('cannotmodifyafterprint', 1); //单据打印后不允许修改 明细不允许修改
			let roleParam = {
				'ipadcode': 'shopmanager', //店长
				'code': 'modifyafterprint', //销售单修改权限
				'val': -1 //保存后不允许修改
			};
			await common.setRoleParam(roleParam);
			await common.loginDo({
				'logid': '004',
			});
			await editBillAfterPrintCheck(qfRes, qfResPrint);

			qfRes.result.details.push({
				num: 3,
				sizeid: 'L',
				matCode: 'Agc001',
				colorid: 'BaiSe',
				price: 200,
			});
			let sfRes = await common.editBilling(qfRes.result, false);
			expect(sfRes.result).to.includes({
				error: '单据不允许修改，因为设置了角色参数“销售单据修改权限”'
			});

		});
	});

	describe('170746,跨门店修改单据-slh2', function () {
		let loginInfoGM = {
			'staffInfoA': { //中洲店
				'logid': '200',
				'pass': '000000'
			},
			'staffInfoB': { //常青店
				'logid': '000',
				'pass': '000000'
			}
		};
		let loginInfoShopManager = {
			'staffInfoA': { //中洲店
				'logid': '204',
				'pass': '000000'
			},
			'staffInfoB': { //常青店
				'logid': '004',
				'pass': '000000'
			}
		};
		before(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 1); //单据允许修改客户和厂商
			await common.setToggleRoleColumn({
				rolecode: 'shopmanager',
				columncode: 'styleproviderid',
				off: 0,
			}); //设置店长敏感字段厂商勾选，二代不勾选厂商，供应商id在qf-22101-1接口中就不返回了
		})
		it('170746,跨门店修改其他门店的销售单', async function () {
			let jsonparam = basiceJsonparam.salesJson();
			let qfInterfaceid = 'qf-14211-1';

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改销售单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `不允许跨门店修改单据，但实际结果是店长跨门店修改销售单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `允许跨门店修改单据，但实际结果是总经理跨门店修改销售单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `允许跨门店修改单据，但实际结果是店长跨门店修改销售单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
		});

		it('170746,跨门店修改其他门店的销售订单', async function () {
			let jsonparam = basiceJsonparam.salesOrderJson();
			let qfInterfaceid = 'qf-14401-1';

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改销售订单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `不允许跨门店修改单据，但实际结果是店长跨门店修改销售订单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `允许跨门店修改单据，但实际结果是总经理跨门店修改销售订单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `允许跨门店修改单据，但实际结果是店长跨门店修改销售订单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
		});

		it('170746,跨门店修改其他门店的采购入库单', async function () {
			let jsonparam = basiceJsonparam.purchaseJson();
			let qfInterfaceid = 'qf-14212-1';

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改采购入库单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `不允许跨门店修改单据，但实际结果是店长跨门店修改采购入库单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `允许跨门店修改单据，但实际结果是总经理跨门店修改采购入库单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `允许跨门店修改单据，但实际结果是店长跨门店修改采购入库单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
		});

		it('170746,跨门店修改其他门店的采购订货单', async function () {
			TESTCASE = {
				// describe: "没有修改单据日期但是提示--涉及账款的单据暂不支持修改日期",
				jira: 'SLHSEC-7520',
			};
			let jsonparam = basiceJsonparam.purchaseOrderJson();
			let qfInterfaceid = 'qf-22101-1';

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改采购订货单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `不允许跨门店修改单据，但实际结果是店长跨门店修改采购订货单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的单据"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `允许跨门店修改单据，但实际结果是总经理跨门店修改采购订货单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoShopManager);
			expect(editResult, `允许跨门店修改单据，但实际结果是店长跨门店修改采购订货单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');
		});

		// 非总经理看不到其他门店的‘调出数据’，因此这里只判断总经理的跨门店修改就可以了
		// 接口有问题，等待服务端修复
		it.skip('170746,跨门店修改其他门店的门店调出单-调', async function () {
			let jsonparam = basiceJsonparam.outJson();
			let qfInterfaceid = 'qf-1862-1';

			// 因为门店调出，约定好了要从中洲店调货给常青店
			let specialLoginInfoGM = {
				'staffInfoA': loginInfoGM.staffInfoB, //常青店
				'staffInfoB': loginInfoGM.staffInfoA
			};

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, specialLoginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改门店调出单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "暂不允许修改其他门店调拨单"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, specialLoginInfoGM);
			expect(editResult, `允许跨门店修改单据(调出单除外,无论参数怎么设置,都不能跨门店修改调出单)，但实际结果是总经理跨门店修改门店调出单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "暂不允许修改其他门店调拨单"
			});
		});

		// 非总经理看不到其他门店的‘盘点数据’，因此这里只判断总经理的跨门店修改就可以了
		it('170746,跨门店修改其他门店的盘点单', async function () {
			// 先新增货品，用于做盘点单
			let addMatResult = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			});
			let matInfo = await common.fetchMatInfo(addMatResult.pk);
			expect(matInfo, `获取货品详情失败:${JSON.stringify(matInfo)}`).to.not.have.property('error');


			let jsonparam = basiceJsonparam.addInventoryJson(matInfo);
			let qfInterfaceid = 'qf-1924-1';

			//不允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 0);
			let editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `不允许跨门店修改单据，但实际结果是总经理跨门店修改盘点单成功:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的盘点单"
			});

			//允许跨门店修改
			await common.setGlobalParam('scm_modify_bill_over_shop', 1);
			editResult = await editAcrossShop(jsonparam, qfInterfaceid, loginInfoGM);
			expect(editResult, `允许跨门店修改单据，但实际结果是总经理跨门店修改盘点单失败:${JSON.stringify(editResult)}`).to.includes({
				"error": "不允许修改其它门店的盘点单"
			});
		});
	});

	describe('170890.单据修改时不提示款号已停用.rankA-slh2', function () {
		after(async () => {
			await common.loginDo();
		});
		it('1.销售单', async function () {
			let json = basiceJsonparam.simpleSalesJson();
			await editBillCheckFor170890('sf-14211-1', 'qf-14211-1', json);
		});
		it('2.销售订货单', async function () {
			let json = basiceJsonparam.salesOrderJson();
			await editBillCheckFor170890('sf-14401-1', 'qf-14401-1', json);
		});
		it('3.采购入库单', async function () {
			let json = basiceJsonparam.purchaseJson();
			await editBillCheckFor170890('sf-14212-1', 'qf-14212-1', json);
		});
		it('4.采购订货单', async function () {
			let json = basiceJsonparam.purchaseOrderJson();
			await editBillCheckFor170890('sf-22101-1', 'qf-22101-1', json);
		});
		it('5.门店调出单', async function () {
			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			let json = basiceJsonparam.outJson();
			await editBillCheckFor170890('sf-1862-1', 'qf-1862-1', json);
		});
	});

	// sales_bill_sync_to_ecqc 是否同步销售单到查询中心 ->1 同步 
	//http://119.3.46.172:6082/zentao/bug-view-1613.html  等汪解决
	describe.skip('SLH-23686.单据修改日志', function () {
		let sfRes;
		before(async function () {
			sfRes = await common.editBilling(basiceJsonparam.salesJson());
			// console.log(`sfRes=${JSON.stringify(sfRes)}`);
			await common.setGlobalParam('dwxx_not_allow_edit', 1);    //允许修改厂商和客户参数开启
		});

		after('参数还原', async function () {
			await common.setGlobalParam('dwxx_not_allow_edit', 0);
		});

		it('新增单据,查看修改日志', async function () {
			await common.delay(200);
			const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
			common.isApproximatelyEqualAssert({ dataList: [], count: 0 }, qlRes);
		});

		it('不修改内容保存', async function () {
			await salesReqHandler.editSalesBill(sfRes, jsonparam => {
				jsonparam.deliver2 = 0;
			});
			await common.delay(200);
			const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });

			common.isApproximatelyEqualAssert({ dataList: [], count: 0 }, qlRes);
		});

		describe('修改主数据', async function () {

			it('修改支付金额', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);

				const editRes = await salesReqHandler.editSalesBill(sfRes, (jsonParam) => {
					jsonParam.cash = common.add(jsonParam.cash, 100);
					jsonParam.card = common.add(jsonParam.card, 200);
					jsonParam.remit = common.add(jsonParam.remit, 200);
					jsonParam.deliver2 = 0;
				});
				await common.delay(1000);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				console.log(`qlRes=${JSON.stringify(qlRes)}`);

				let exp = getExpByField(qfRes.result, { main: ['cash', 'card', 'remit'] }, editRes.params, 1);

				qlRes.dataList.forEach((res, index) => {
					common.isApproximatelyArrayAssert(JSON.parse(res.mainDatas), exp[index].mainDatas);
				});
			});

			it('修改客户', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				const qlResBefore = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				qlResBefore.dataList[0].mainDatas = JSON.parse(qlResBefore.dataList[0].mainDatas);
				//修改客户
				await salesReqHandler.editSalesBill(sfRes, (jsonparam) => {
					jsonparam.dwid = BASICDATA.dwidLs;
					jsonparam.deliver2 = 0;
				});
				//查询单据详情
				const qfResAfter = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				await common.delay(1000);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });

				let exp = getExpByField(qfRes.result, { main: ['show_dwid'] }, qfResAfter.result, 1);
				exp.push(qlResBefore.dataList[0]);

				qlRes.dataList.forEach((res, index) => {
					common.isApproximatelyArrayAssert(exp[index].mainDatas, JSON.parse(res.mainDatas));
				});
			});

			it('修改店员', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				await salesReqHandler.editSalesBill(sfRes, (jsonparam) => {
					jsonparam.deliver = BASICDATA.staffid004;
					jsonparam.deliver2 = 0;
				});
				const qfResAfter = await salesReqHandler.salesQueryBilling(sfRes.result.pk);

				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });

				let exp = getExpByField(qfRes.result, { main: ['show_deliver'] }, qfResAfter.result, 1);
				common.isApproximatelyArrayAssert(JSON.parse(qlRes.dataList[0].mainDatas), exp[0].mainDatas);
			});

			it('修改整单备注', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);

				const editRes = await salesReqHandler.editSalesBill(sfRes, (jsonparam) => {
					jsonparam.remark = '整单备注';
					jsonparam.deliver2 = 0;
				});
				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				let exp = getExpByField(qfRes.result, { main: ['remark'] }, editRes.params, 1);

				common.isApproximatelyArrayAssert(JSON.parse(qlRes.dataList[0].mainDatas), exp[0].mainDatas);
			});
		});

		describe('修改明细', async function () {

			it('修改明细', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				const editRes = await salesReqHandler.editSalesBill(sfRes, (jsonParam) => {
					jsonParam.details[0].num = 20;
					jsonParam.details[1].num = common.sub(jsonParam.details[1].num, 1);
				});
				// console.log(`res=${JSON.stringify(res)}`);
				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				let exp = getExpByField(qfRes.result, { details: ['num'] }, editRes.params, 2);
				common.isApproximatelyArrayAssert(exp.detDatas[0].colorSizeDets, JSON.parse(qlRes.dataList[0].detDatas)[0].colorSizeDets);
			});

			it('修改单价', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				const editRes = await salesReqHandler.editSalesBill(sfRes, (jsonparam) => {
					jsonparam.details[0].price = jsonparam.details[0].realprice = common.add(jsonparam.details[0].price, 100);
					jsonparam.details[0].total = common.add(jsonparam.details[0].total, common.mul(jsonparam.details[0].num, 100));
					jsonparam.details[1].price = jsonparam.details[1].realprice = common.add(jsonparam.details[1].price, 100);
					jsonparam.details[1].total = common.add(jsonparam.details[1].total, common.mul(jsonparam.details[1].num, 100));
				});
				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				let exp = getExpByField(qfRes.result, { details: ['price'] }, editRes.params, 2);
				common.isApproximatelyArrayAssert(exp.detDatas[0].colorSizeDets, JSON.parse(qlRes.dataList[0].detDatas)[0].colorSizeDets);
			});

			it('添加商品', async function () {
				await salesReqHandler.editSalesBill(sfRes, (jsonparam) => {
					jsonparam.details.push({
						"num": 3,
						"sizeid": "L",
						"matCode": "Agc002",
						"rem": "明细2",
						"colorid": "BaiSe",
						"price": 200,
						"discount": 1,
					});
				});
				let exp = [{ oldVal: 0, sizeId: 3, color: '白色', size: 'L', colorId: 3, opType: 'add', newVal: 3 }];
				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: sfRes.result.pk, billType: 21 });
				common.isApproximatelyArrayAssert(exp, JSON.parse(qlRes.dataList[0].detDatas)[0].colorSizeDets);
			});
		});

		describe('同时修改主数据和明细', async function () {
			let saveRes;
			before('新增一个单据', async function () {
				saveRes = await common.editBilling(basiceJsonparam.simpleSalesJson());
			});

			it('同时修改明细备注和支付方式', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(saveRes.result.pk);
				const editRes = await salesReqHandler.editSalesBill(saveRes, (jsonParam) => {
					jsonParam.details[0].num = 10;
					jsonParam.cash = 500;
					jsonParam.deliver2 = 0;
				});
				await common.delay(500);
				const qlRes = await common.callInterface('ql-1159', { billid: saveRes.result.pk, billType: 21 });

				let exp = getExpByField(qfRes.result, { main: ['cash'], details: ['num'] }, editRes.params, 0);

				common.isApproximatelyArrayAssert(JSON.parse(qlRes.dataList[0].mainDatas), exp.mainDatas);
				common.isApproximatelyArrayAssert(JSON.parse(qlRes.dataList[0].detDatas)[0].colorSizeDets, exp.detDatas[0].colorSizeDets);
			});

			it('同时修改两个明细', async function () {
				const qfRes = await salesReqHandler.salesQueryBilling(saveRes.result.pk);

				const editRes = await salesReqHandler.editSalesBill(saveRes, (jsonParam) => {
					jsonParam.details[0].num = 20;
					jsonParam.details[0].price = jsonParam.details[0].realprice = common.add(jsonParam.details[0].price, 100);
					jsonParam.details[0].total = common.add(jsonParam.details[0].total, common.mul(jsonParam.details[0].num, 100));
				});
				await common.delay(300);
				const qlRes = await common.callInterface('ql-1159', { billid: saveRes.result.pk, billType: 21 });

				let exp = getExpByField(qfRes.result, { details: ['num', 'price'] }, editRes.params, 2);
				common.isApproximatelyArrayAssert(exp.detDatas[0].colorSizeDets, JSON.parse(qlRes.dataList[0].detDatas)[0].colorSizeDets);
			});
		});
	});

	//
	describe('验证小计是否正确', async function () {
		before(async function () {
			// 验证小计是否正确 默认开启
			await common.setGlobalParam('check_total', 1);
		});
		it('小计计算错误时保存', async function () {
			let salesJson = format.jsonparamFormat(basiceJsonparam.salesJson());
			salesJson.details[0].total += 20;
			salesJson.totalmoney += 20;
			const sfRes = await common.callInterface2({ interfaceid: salesJson.interfaceid, jsonparam: salesJson, check: false });

			const exp = USEECINTERFACE == 1 ? { msgId: 'totalsum_error', error: `整单金额和明细金额合计不一致(${salesJson.totalmoney}<>${salesJson.totalmoney - 20})` } : { error: `第1行小计金额计算有误(${salesJson.details[0].total}<>${salesJson.details[0].total - 20})` };
			expect(sfRes.result).to.includes(exp)
		});
	});

	describe('支持多店员开单', async function () {
		const staff000Name = '总经理';
		const staff004Name = '店长';

		describe('正常销售单', async function () {
			let sfRes, qfRes;
			before('多店员开单', async function () {
				const salesJson = basiceJsonparam.simpleSalesJson();
				salesJson.deliver = BASICDATA.staffid000;
				salesJson.deliver2 = BASICDATA.staffid004;
				sfRes = await common.editBilling(salesJson);   //开单操作
			});

			it('销售开单修改界面数据验证', async function () {
				qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				//这里actid忽略是因为查询详情接口用了 qf-1421-1 返回的actid为0 ，和汪确认，修改时这个字段没有意义，
				common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['actid']);
			});

			it('销售开单-按批次查', async function () {
				const exp = _.cloneDeep(qfRes.result);
				exp.sellerId = sfRes.params.deliver;
				exp.seller2Id = sfRes.params.deliver2;
				exp.seller = staff000Name;
				exp.seller2 = staff004Name;

				let qlRes = await common.callInterface('ql-142201', format.qlParamsFormat({ billno1: sfRes.result.billno, billno2: sfRes.result.billno }), true);
				let billInfo = qlRes.dataList.find(val => val.billno == sfRes.result.billno);
				if (billInfo == undefined) {
					throw new Error(`销售开单按批次差没有找到订单号为: ${sfRes.result.billno}的销售单`)
				} else {
					common.isApproximatelyEqualAssert(exp, billInfo);
				};
			});
		});

		describe('按订货开单', async function () {
			let sfRes, qfRes;
			before('新增销售订单，按订货开单', async function () {
				const orderJson = basiceJsonparam.salesOrderJson();
				const sfOrderRes = await common.editBilling(orderJson);
				const orderInfo = await salesReqHandler.salesOrderQueryBilling(sfOrderRes.result.pk).then(res => res.result);
				orderInfo.deliver = BASICDATA.staffid000;
				orderInfo.deliver2 = BASICDATA.staffid004;
				sfRes = await salesReqHandler.editSalesBillByOrder(orderInfo, true);  //按订货开单
			});

			it('qf查询', async function () {
				qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['actid', 'orderversion', 'billno']);
			});

			it('销售开单-按批次查', async function () {
				const exp = _.cloneDeep(qfRes.result);
				exp.sellerId = sfRes.params.deliver;
				exp.seller2Id = sfRes.params.deliver2;
				exp.seller = staff000Name;
				exp.seller2 = staff004Name;

				let qlRes = await common.callInterface('ql-142201', format.qlParamsFormat({ billno1: sfRes.result.billno, billno2: sfRes.result.billno }), true);
				let billInfo = qlRes.dataList.find(val => val.billno == sfRes.result.billno);
				if (billInfo == undefined) {
					throw new Error(`销售开单按批次差没有找到订单号为: ${sfRes.result.billno}的销售单`)
				} else {
					common.isApproximatelyEqualAssert(exp, billInfo, ['billid']);
				};
			});
		});

		describe('挂单转正式单', async function () {
			let sfRes, qfRes;

			before('开挂单，并且转为正式单', async function () {
				let hangJson = basiceJsonparam.simpleSalesJson();
				hangJson.invalidflag = 9; //挂单
				const hangRes = await salesReqHandler.editSalesHangBill(hangJson);
				hangRes.params.pk = hangRes.result.pk;
				hangRes.params.deliver = BASICDATA.staffid000;
				hangRes.params.deliver2 = BASICDATA.staffid004;
				sfRes = await salesReqHandler.hangToSalesBill(hangRes.params);
			});

			it('销售开单修改界面数据验证', async function () {
				qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
				common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['actid']);
			});

			it('销售开单-按批次查', async function () {
				const exp = _.cloneDeep(qfRes.result);
				exp.sellerId = sfRes.params.deliver;
				exp.seller2Id = sfRes.params.deliver2;
				exp.seller = staff000Name;
				exp.seller2 = staff004Name;

				let qlRes = await common.callInterface('ql-142201', format.qlParamsFormat({ billno1: sfRes.result.billno, billno2: sfRes.result.billno }), true);
				let billInfo = qlRes.dataList.find(val => val.billno == sfRes.result.billno);
				if (billInfo == undefined) {
					throw new Error(`销售开单按批次差没有找到订单号为: ${sfRes.result.billno}的销售单`)
				} else {
					common.isApproximatelyEqualAssert(exp, billInfo);
				};
			});
		});
	});

});




//串码发货 数据准备
//changeArr 调整后库存
//返回用于开单的json
async function dataPrepareFor170651(goodRes, changeArr) {
	let sizeids = goodRes.params.sizeids.split(',');
	let colorids = goodRes.params.colorids.split(',');

	//数据准备
	await common.setGlobalParam('invinout_checknum', 0); //是否允许负库存 不检查,允许负库存

	let param = format.qlParamsFormat({
		propdresStyleid: goodRes.pk,
		propdresColorid: colorids[0],
		propdresSizeid: sizeids[0],
		invid: LOGINDATA.invid,
	}, false);
	let qlRes = await common.callInterface('ql-1932', param); //获取尺码a当前库存
	let jsonparam = {
		pk: qlRes.dataList[0].id,
		// action: 'edit',
		propdresStyleCode: param.propdresStyleid,
		propdresColorid: param.propdresColorid,
		propdresSizeid: param.propdresSizeid,
		invnum: qlRes.dataList[0].invnum,
		recvnum: changeArr[0]
	};
	let adjRes = await common.callInterface('sf-1932', { //库存调整 尺码a
		jsonparam
	});

	param.propdresSizeid = sizeids[1];
	qlRes = await common.callInterface('ql-1932', param); ////获取尺码b当前库存
	jsonparam.pk = qlRes.dataList[0].id;
	jsonparam.propdresSizeid = sizeids[1];
	jsonparam.invnum = qlRes.dataList[0].invnum;
	jsonparam.recvnum = changeArr[1];
	adjRes = await common.callInterface('sf-1932', { //库存调整 尺码b
		jsonparam
	});

	await common.setGlobalParam('invinout_checknum', 2); //是否允许负库存 不允许款号库存为负,适用窜码销售情况

	//拼接用例需要的jsonparam 明细的数量根据用例 自行设置
	let json = basiceJsonparam.simpleSalesJson();
	json.details[0].styleid = goodRes.pk;
	json.details[0].colorid = colorids[0];
	json.details[0].sizeid = sizeids[0]; //使用尺码a开单 验证串码

	return json;
};


/*
 * 跨门店修改单据
 *
 * @param
 * jsonparam 保存单据时提交的jsonparam
 * qfInterfaceid 查询单据详情时调用的接口
 * loginInfo 涉及到的登录工号及id ，具体结构如下
 *【注意staffInfoA、staffInfoB必须分属不同门店】
 * {
 * 	'staffInfoA': {
 * 		'logid': '***',
 * 		'pass': '******'
 * 	},
 * 	'staffInfoB': {
 * 		'logid': '***',
 * 		'pass': '******'
 * 	}
 * }
 *
 * @return 返回跨门店修改结果
 */
async function editAcrossShop(jsonparam, qfInterfaceid, loginInfo) {
	// A门店登录并开单
	await common.loginDo(loginInfo.staffInfoA);
	let billResult = await common.editBilling(jsonparam);
	let params = {
		'pk': billResult.result.pk,
		interfaceid: qfInterfaceid,
	};
	if (USEECINTERFACE == '2') params.isPend = 0; //是否挂单 否
	let billInfo = await reqHandler.queryBilling(params)


	// B门店登录并修改A门店的单据
	await common.loginDo(loginInfo.staffInfoB);
	billInfo.result.action = 'edit';
	billInfo.result.pk = billResult.result.pk;
	billInfo.result.remark = `${loginInfo.staffInfoB.logid}修改${loginInfo.staffInfoA.logid}的${jsonparam.interfaceid}单据`;
	billInfo.result.interfaceid = jsonparam.interfaceid;
	billResult = await common.editBilling(billInfo.result, false);

	return billResult.result;
};

//单据打印后修改验证
async function editBillAfterPrintCheck(qfRes, qfResPrint) {
	qfRes.result.cash = 2000;
	let sfRes = await common.editBilling(qfRes.result, false);
	expect(sfRes.result).to.includes({
		error: '单据不允许修改，因为设置了角色参数“销售单据修改权限”'
	});

	qfResPrint.result.cash = 2000;
	sfRes = await common.editBilling(qfResPrint.result, false);
	expect(sfRes.result).to.includes({
		error: '单据不允许修改，因为设置了角色参数“销售单据修改权限”',
	});
};

//170890 单据修改时不提示款号已停用验证
async function editBillCheckFor170890(sfIFC, qfIfc, json) {
	let goodRes = await basicReqHandler.editStyle({
		jsonparam: basiceJsonparam.addGoodJson(),
	}); //新增货品

	json.details[0].styleid = goodRes.pk; //使用新增货品开单
	let sfRes = await common.editBilling(json);
	let params = {
		action: 'edit',
		pk: sfRes.result.pk
	};
	if (USEECINTERFACE == '2') params.isPend = 0; //是否挂单
	let qfResult = await common.callInterface(qfIfc, params);

	let cancelRes = await common.callInterface('cs-1511-33', { //停用货品
		pk: goodRes.pk
	});

	qfResult.interfaceid = sfIFC;
	qfResult.action = 'edit';
	qfResult.remark += 'a';
	await common.editBilling(qfResult); //修改单据，正常保存
};

/**
 * 
 * @param {Object} qfRes 执行此操作前，单据详情结果 
 * @param {Object} fileids 修改字段  例如  {main:['cash'],details:['num','price']};
 * @param {Object} editParams 修改单据时传的参数 
 * @param {Number} editType  修改类型，0-都修改   1-修改头部   2-修改明细 
 */

function getExpByField(qfRes, fileids, editParams, editType) {

	switch (editType) {
		case 1:
			return [{ mainDatas: getMainExp(), detDatas: '', opName: LOGINDATA.rolename, updatedDate: common.getCurrentTime() }];     //主数据有修改，详情没有修改
		case 2:
			return { mainDatas: [], opName: LOGINDATA.rolename, updatedDate: common.getCurrentTime(), detDatas: [{ colorSizeDets: getDetailExp(), styleName: editParams.show_styleid }] };
		case 0:
			return { mainDatas: getMainExp(), detDatas: [{ colorSizeDets: getDetailExp() }] };
		default:
			break;
	};

	function getMainExp() {
		let exp = [];
		fileids.main.forEach(val => {
			exp.push({
				oldVal: val == 'show_deliver' ? qfRes[val].split(',')[1] : qfRes[val],
				field: val,
				newVal: val == 'show_deliver' ? editParams[val].split(',')[1] : editParams[val]
			});
		});
		return exp;
	};

	function getDetailExp() {
		let info = qfRes.details.map(val => {
			return { color: val.show_colorid, size: val.show_sizeid, sizeid: val.sizeid, colorid: val.colorid };
		});
		fileids.details.forEach(val => {
			info.forEach((res, index) => {
				switch (val) {
					case 'num':
						res.oldVal = qfRes.details[index].num;
						res.newVal = editParams.details[index].num;
						break;
					case 'price':
						res.priceOldVal = qfRes.details[index].price;
						res.priceNewVal = editParams.details[index].price;
					case 'rem':
						res.oldRem = qfRes.details[index].rem;
						res.newRem = editParams.details[index].rem;
						break;
					default:
						break;
				};
			});

		});

		return info;
	};
};
