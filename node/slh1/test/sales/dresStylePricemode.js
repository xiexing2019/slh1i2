"use strict";
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
/*
 *  价格模式
 *  需要维护好价格组 - 相关接口sf-1501 qf-1501 ql-1501
 *  常青店 价格组one,中洲店 无价格组,文一店 仓库店 价格组two     
 * 	价格组的功能  二代是不用的。所以此处也不必进行适配
 */
describe('价格模式', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sc_dres_style_usetagprice', 0); //货品建款的价格模式 默认价格模式
	});

	after(async () => {
		await common.setGlobalParam('dres_style_pricemode', 0); //价格模式 统一的价格体系
	});

	it('170663.省代模式下,不能开启参数-不同门店不同的价格体系', async function () {
		await common.setGlobalParam('sc_dres_style_usetagprice', 1); //货品建款的价格模式 省代模式
		let result = await common.setGlobalParam('dres_style_pricemode', 1, false); //价格模式 不同门店不同的价格体系
		await common.setGlobalParam('sc_dres_style_usetagprice', 0); //货品建款的价格模式 默认价格模式
		expect(result, `修改参数成功`).to.includes({
			error: '【不同门店不同的价格体系】的价格模式与【省代价格模式】的货品建款的价格模式，不能同时存在'
		});
	});

	it('170664.省代模式下,不能开启参数-不同门店不同销售价,相同进货价', async function () {
		await common.setGlobalParam('sc_dres_style_usetagprice', 1); //货品建款的价格模式 省代模式
		let result = await common.setGlobalParam('dres_style_pricemode', 2, false); //价格模式 不同门店不同销售价，相同进货价
		await common.setGlobalParam('sc_dres_style_usetagprice', 0); //货品建款的价格模式 默认价格模式
		expect(result, `修改参数成功`).to.includes({
			error: '【不同门店不同的价格体系】的价格模式与【省代价格模式】的货品建款的价格模式，不能同时存在'
		});
	});

	describe('不同门店不同销售价，相同进货价', function () {
		before(async function () {
			await common.setGlobalParam('dres_style_pricemode', 2); //价格模式 不同门店不同销售价，相同进货价
		});
		after(async function () {
			await common.setGlobalParam('sales_pricecheck', 0); //销售价格允许改高不允许改低 不检查
			await common.setGlobalParam('sales_pricecheck_pricetype', -1); //销售开单价不能低于指定的价格类型 不限制
		});
		// 3.文一店新增一个款进货价100，中洲店、常青店登录查看---所有门店进货价100
		// 4.文一店修改进货价为120 零批价等，常青店、中洲店登录查看----进货价全部为120，其余价格:同一价格组的价格变化，不同价格组的不变化，无价格组的不变化

		it('170662.价格组验证', async function () {
			await common.loginDo({ //文一店总经理登陆
				'logid': '400',
				'pass': '000000',
			});
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			}); //新增货品 库存为0
			let goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			pricedetailsAssert(goodResult, 2, 'two'); //价格组明细验证 必须使用创建门店验证

			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			priceAssert(goodResult, 'two', 100); //货品价格验证

			//文一店总经理登陆 修改货品价格
			await common.loginDo({
				'logid': '400',
				'pass': '000000',
			});
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			goodResult.action = 'edit';
			goodResult.pk = goodRes.pk;
			goodResult.purprice = 120;
			goodResult.stdprice1 = 220;
			goodResult.stdprice2 = 210;
			goodResult.stdprice3 = 200;
			goodRes = await basicReqHandler.editStyle({
				jsonparam: goodResult
			});

			//仓库店总经理登陆 相同价格组
			await common.loginDo({
				'logid': '100',
				'pass': '000000',
			});
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			priceAssert(goodResult, 'two', 120); //货品价格验证

			//中洲店总经理登陆 无价格组
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			priceAssert(goodResult, '无价格组', 120); //货品价格验证

			//常青店总经理登陆 不同价格组
			await common.loginDo();
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			priceAssert(goodResult, 'one', 120); //货品价格验证
		});

		it('170665.低于指定价格类型 ', async function () {
			await common.setGlobalParam('default_stdprice', 1); //默认显示价格类型 1零批价
			await common.setGlobalParam('sales_pricecheck', 1); //销售价格允许改高不允许改低 1销售价不能低于零批价
			await common.setGlobalParam('sales_pricecheck_pricetype', 1); //销售开单价不能低于指定的价格类型 价格1

			await common.loginDo();
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品 库存为0
			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			let goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			goodResult.action = 'edit';
			goodResult.pk = goodRes.pk;
			goodResult.stdprice1 = 190;
			goodRes = await basicReqHandler.editStyle({
				jsonparam: goodResult
			});

			await common.loginDo({ //常青店店长登陆
				'logid': '004',
				'pass': '000000',
			});
			let json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].price = 195;
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: `[第1行][${goodResult.code}]价格输入错误，因为启用了价格验证价格类型[1]`,
			}, `error: ${JSON.stringify(sfRes)}`);

			await common.setGlobalParam('sales_pricecheck_pricetype', -1); //销售开单价不能低于指定的价格类型 不限制
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: `[第1行][${goodResult.code}]价格输入错误，因为启用了价格验证价格类型[1]`,
			}, `error: ${JSON.stringify(sfRes)}`);
		});

		it('170792,【销售开单-开单】不同门店不同价格，营业员开单检查 价格改高不允许改低.rankA', async function () {
			await common.setGlobalParam('sales_pricecheck', 0); //销售价格允许改高不允许改低 0不检查
			await common.setGlobalParam('sales_pricecheck_pricetype', 1); //销售开单价不能低于指定的价格类型 价格1
			await common.loginDo();
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品 库存为0
			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			let goodResult = await common.fetchMatInfo(goodRes.result.pk); //获取货品信息
			goodResult.action = 'edit';
			goodResult.pk = goodRes.pk;
			goodResult.stdprice1 = 240;
			goodRes = await basicReqHandler.editStyle({
				jsonparam: goodResult
			});

			await common.loginDo({ //常青店店长登陆
				'logid': '005',
				'pass': '000000',
			});
			let json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].price = 200;
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.not.have.property('error');
		});

	});

	//需要维护后台价格组 详见doc/autotest.md
	describe('不同门店不同的价格体系', function () {
		let [goodRes, goodResult] = [{}, {}];

		before(async function () {
			await common.setGlobalParam('dres_style_pricemode', 1); //价格模式 不同门店不同的价格体系
		});

		after(async () => {
			await common.setGlobalParam('sales_use_lastsaleprice', 0); //颜色尺码下，开单是否显示上次单价 不显示
			await common.setGlobalParam('sales_show_lastprice', 0); //是否启用上次成交价作为本次开单单价 不启用
		});

		it('170242.价格组验证', async function () {
			await common.loginDo();
			goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品 库存为0
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			pricedetailsAssert(goodResult, 1, 'one'); //价格组明细验证 必须使用创建门店验证
			priceAssert(goodResult, 'one', 100); //货品价格验证

			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			goodResult = await common.fetchMatInfo(goodRes.pk); //获取货品信息
			// console.log(`goodResult = ${JSON.stringify(goodResult)}`);
			priceAssert(goodResult, 'one', 0); //货品价格验证
		});
		//依赖170242
		it('170244.按明细查界面检查差额值', async function () {
			//修改货品价格
			goodResult.action = 'edit';
			goodResult.pk = goodRes.pk;
			goodResult.purprice = 120;
			goodResult.stdprice1 = 220;
			goodResult.stdprice2 = 210;
			goodResult.stdprice3 = 200;
			goodRes = await basicReqHandler.editStyle({
				jsonparam: goodResult
			});

			let json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].price = goodResult.stdprice1;
			let sfRes = await common.editBilling(json);

			let param = format.qlParamsFormat({
				billno: sfRes.result.billno,
				shopid: LOGINDATA.invid,
			}, true);
			let qlRes = await common.callInterface('ql-1209', param);
			assert.equal(qlRes.dataList[0].diffsum, json.details[0].price * json.details[0].num);
		});

		it('170494.取上次价', async function () {
			await common.setGlobalParam('sales_show_lastprice', 1); //颜色尺码下，开单是否显示上次单价 显示
			await common.setGlobalParam('sales_use_lastsaleprice', 1); //是否启用上次成交价作为本次开单单价 启用
			await common.setGlobalParam('sales_getlastprice_byshop', 0); //是否按门店取客户上次价 按门店

			await common.loginDo(); //常青店总经理登陆
			goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品 库存为0
			let json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].price = 200;
			let sfRes = await common.editBilling(json); //销售价200

			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			json.details[0].price = 180; //销售价180
			sfRes = await common.editBilling(json);

			await common.loginDo(); //常青店总经理登陆
			// 常青店查询上次价
			let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
				'shopid': LOGINDATA.invid,
				'dwid': json.dwid,
				'styleid': goodRes.pk
			});
			expect(lastPriceInfo, `取客户上次价 按门店获取上次价接口有误`).to.includes({ //按门店 取本门店的上次价
				'price': '200'
			});

			await common.setGlobalParam('sales_getlastprice_byshop', 1); //是否按门店取客户上次价 全局
			lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
				'shopid': LOGINDATA.invid,
				'dwid': json.dwid,
				'styleid': goodRes.pk,
			});
			expect(lastPriceInfo, `取客户上次价 按全局获取上次价接口有误`).to.includes({ //按门店 取全局的上次价
				'price': '180'
			});
		});
	});

});

/**
 *  验证价格组明细的价格是否正确
 *  @param {String} styleid - 款号id
 *  @param {String} pricemode - 价格模式 0统一的价格体系 1不同门店不同销售价 2不同门店不同销售价,相同进货价
 *  @param {String} pricegroup - 价格组 [无价格组,one,two]
 *  @returns {Object} 货品信息
 */
function pricedetailsAssert(goodResult, pricemode, pricegroup) {
	goodResult.pricedetails.map((pricedetail) => {
		expect(pricedetail).to.satisfy((obj) => {
			let isEqualPrice = obj.pricegroup == pricegroup || pricemode == 0; //价格组相同或统一价格体系时取相同价格
			let ret = isEqualPrice ? obj.purprice == goodResult.purprice : obj.purprice == 0; //验证进货价
			for (let i = 1; i <= 5; i++) { //适用价格验证
				let exp = isEqualPrice ? goodResult[`stdprice${i}`] : 0;
				ret = ret && obj[`stdprice${i}`] == exp;
				if (!ret) {
					console.log(`价格验证错误pricedetails = ${JSON.stringify(obj)} \n错误key=stdprice${i} exp=${exp}`);
					break;
				};
			};
			return ret;
		});
	});
};

function priceAssert(goodResult, pricegroup, purprice) {
	for (let j = 0; j < goodResult.pricedetails.length; j++) {
		let expObj = goodResult.pricedetails[j];
		if (expObj.invid == LOGINDATA.invid || expObj.pricegroup == pricegroup) {
			//2不同门店不同销售价,相同进货价 不是取pricedetails中的价格
			assert.equal(goodResult.purprice, purprice, `进货价错误 actual=${goodResult.purprice},expected=${purprice}`);

			for (let i = 1; i <= 5; i++) { //适用价格验证
				assert.equal(goodResult[`stdprice${i}`], expObj[`stdprice${i}`], `价格验证错误 stdprice${i} actual=${expObj[`stdprice${i}`]},expected=${expObj[`stdprice${i}`]}`);
			};
			break;
		};
	};
};
