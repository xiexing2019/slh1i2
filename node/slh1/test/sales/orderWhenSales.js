'use strict';

const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 销售开单的同时订货 相关用例验证
 */
describe('销售开单的同时订货--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sales_orderwhensales', 1); //开启开单的同时订货功能
		await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单
	});

	after(async () => {
		await common.setGlobalParam('sales_orderwhensales', 0); //开单的同时订货功能 默认不启用
	});

	it('170125,开单的同时订货', async function () {
		//销售开单
		let salesReturn = await common.editBilling(basiceJsonparam.orderWhenSalesJson());
		//查看生成的销售单
		let billingInfo = await salesReqHandler.salesQueryBilling(salesReturn.result.pk)
			.then((obj) => obj.result);

		//按批次查[销售开单]
		let searchCondition = {
			'billno1': salesReturn.result.billno,
			'billno2': salesReturn.result.billno,
			'shopid': salesReturn.params.invid,
			'dwid': salesReturn.params.dwid
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchCondition, true));

		// 按配货开单qf页面 订货单
		let orderBillingInfo = await salesOrderReqHandler.salesOrderQueryBilling(billingInfo.billid2);

		//订货按批次查
		searchCondition = {
			'clientid': orderBillingInfo.clientid,
			'sellerid': orderBillingInfo.sellerid,
			'shopid': orderBillingInfo.shopid,
			'invid': orderBillingInfo.invid,
			'id1': orderBillingInfo.billno,
			'id2': orderBillingInfo.billno
		};
		let orderBatchInfo = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));

		//验证开单数据和生成的数据是否一致
		common.isApproximatelyEqualAssert(salesReturn.params, billingInfo);
		common.isApproximatelyEqualAssert(billingInfo, batchInfo.dataList[0]);
		common.isApproximatelyEqualAssert(getExpectedOrderInfo(salesReturn.params), orderBillingInfo.result);
		// common.isApproximatelyArrayAssert(orderBillingInfo.result, orderBatchInfo.dataList[0]);
	});

	it('170607.客户为空时进行开单同时订货操作.rankA', async function () {
		let json = _.cloneDeep(basiceJsonparam.orderWhenSalesJson());
		json.dwid = ''; //不输入客户
		json.cash += json.remit; // 往来单位为空不允许有欠款或余额
		delete json.remit; // 汇款必须填写客户信息 默认打开 170582.170583中涉及
		let res = await common.editBilling(json, false);
		let errorMsg;
		USEECINTERFACE == 1 ? errorMsg = '客户不能为空' : errorMsg = '客户为空，无法保存';
		expect(res.result).to.includes({
			error: errorMsg,
		}, `error: ${JSON.stringify(res.result)}`);
	});
});

/*
 * 根据开单数据 生成销售订货的期望数据  (销售订货的客户、门店、店员、款号等数据跟销售开单数据一致)
 */
function getExpectedOrderInfo(salesJsonparam) {
	let salesOrderInfo = _.cloneDeep(salesJsonparam);
	salesOrderInfo.interfaceid = "sf-14401-1";
	salesOrderInfo.cash = "0";
	salesOrderInfo.cashaccountid = '';
	salesOrderInfo.card = "0";
	salesOrderInfo.cardaccountid = '';
	salesOrderInfo.remit = "0";
	salesOrderInfo.remitaccountid = '';
	salesOrderInfo.srcType = "2";
	delete salesOrderInfo.balance;
	delete salesOrderInfo.totalmoney;
	salesOrderInfo.sellerid = salesOrderInfo.deliver;
	salesOrderInfo.clientid = salesOrderInfo.dwid;

	for (let i = 0; i < salesOrderInfo.details.length; i++) {
		salesOrderInfo.details[i].num = salesOrderInfo.details[i].recvnum;
		delete salesOrderInfo.details[i].recvnum;
	}

	return format.jsonparamFormat(salesOrderInfo);
}
