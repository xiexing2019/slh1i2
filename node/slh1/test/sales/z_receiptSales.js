'use strict';

const common = require('../../../lib/common.js');
const caps = require('../../../data/caps');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 收款单相关用例验证
 *
 * 收款单的相关用例脚本 要在最后执行，否则会导致作废等操作无法继续
 * 会影响价格改高改低验证，原因未找到 处理方式先放到销售最后跑
 * 暂时使用改变文件名称的方式，使之排在最后一个运行
 */

describe("销售开单收款操作-slh2", function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('fin_enable_tally', 1); //启用财务结账收款功能
	});

	it('170164,销售开单--收款.rankA', async function () {
		await common.setGlobalParam('sales_use_wxpay', 1); //开启微信支付
		await common.setGlobalParam('sales_use_alipay', 1); //开启支付宝支付

		//先去收款单列表看一下今日当前门店有没有收款操作，如有收款，撤销;没有收款单，去做收款操作
		let searchReceiptList = {
			'invid': LOGINDATA.invid,
			'optime1': common.getDateString([-2, 0, 0]),
			'optime2': common.getCurrentDate()
		};
		let receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));
		if (receiptList.count > 0) {
			let deletePk = receiptList.dataList[0]['id'];
			let deleteResult = await common.callInterface('cs-1195-delete', {
				'pk': deletePk
			});
			expect(deleteResult, `\n定位:170164,销售开单--收款 \n撤销收款单失败:${JSON.stringify(deleteResult)}`).to.not.have.property('error');
		}

		//销售开单按批次查列表,获取第一条销售单的PK,并进行收款操作
		let searchBatchList = {
			'shopid': LOGINDATA.invid,
			'prodate1': common.getDateString([0, 0, 0]),
			'prodate2': common.getCurrentDate(),
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, false));
		if (batchInfo.count > 0) {
			let receiptPk = batchInfo.dataList[0]['id'];
			let receiptResult = await common.callInterface('cs-sales-receivables', {
				'lastbillid': receiptPk
			});
			expect(receiptResult, `收款失败:${JSON.stringify(receiptResult)}`).to.not.have.property('error');
		} else {
			expect(true, `新增收款单失败，因为近三十天内，未查询到销售单据【170164,销售开单--收款】`).to.be.false;
		}

		//验证收款单的金额，与按批次查列表的金额是否相同
		let billno1, billno2;
		let actualReceipt = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));
		if (actualReceipt.count > 0) {
			billno1 = actualReceipt.dataList[0]['firstbillid'];
			billno2 = actualReceipt.dataList[0]['lastbillid'];
		}
		searchBatchList.prodate1 = common.getDateString([-2, 0, 0]);
		searchBatchList.search_list = 0;
		if (billno1 != undefined) searchBatchList.billno1 = billno1;
		if (billno2 != undefined) searchBatchList.billno2 = billno2;
		let expectedReceipt = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, false));
		// console.log(`\nactualReceipt : ${JSON.stringify(actualReceipt.dataList[0])} \nexpectedReceipt : ${JSON.stringify(expectedReceipt.sumrow)}`);
		//断言
		let isEqual = isPaymentAmountEqual(expectedReceipt, actualReceipt);
		expect(isEqual, `收款单金额有误:${isEqual}`).to.be.true;
	});

	//排序汇总数据有单独的脚本统一验证  【必须接着上一条170164】
	it('170275,验证翻页排序汇总数据.rankA', async function () {
		let searchReceiptList = {
			'invid': LOGINDATA.invid,
			'optime1': common.getDateString([-2, 0, 0]),
			'optime2': common.getCurrentDate()
		};
		let receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));

		let flag = true,
			errorMsg = '';
		if (!receiptList.sumrow.hasOwnProperty('cash') || receiptList.sumrow['cash'] == '') {
			errorMsg += `cash字段没有汇总;`;
			flag = false;
		}
		if (!receiptList.sumrow.hasOwnProperty('card') || receiptList.sumrow['card'] == '') {
			errorMsg += `card字段没有汇总;`;
			flag = false;
		}
		if (!receiptList.sumrow.hasOwnProperty('remit') || receiptList.sumrow['remit'] == '') {
			errorMsg += `remit字段没有汇总;`;
			flag = false;
		}
		expect(flag, errorMsg).to.be.true;
	});

	// 必须接着上一条170275
	it('170277,撤销收款单', async function () {
		let searchReceiptList = {
			'invid': LOGINDATA.invid,
			'optime1': common.getDateString([-2, 0, 0]),
			'optime2': common.getCurrentDate()
		};
		let receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));
		if (receiptList.count > 0) {
			let deletePk = receiptList.dataList[0]['id'];
			let deleteResult = await common.callInterface('cs-1195-delete', {
				'pk': deletePk
			});
			expect(deleteResult, `\n定位:170277,撤销收款单 \n撤销收款单失败:${JSON.stringify(deleteResult)}`).to.includes({
				val: 'ok',
			});
		}
	});

	it('170738,跨门店收款--总经理', async function () {
		//仓库店开单
		let sfData = await salesFromCkd();
		expect(sfData, `\n定位:170738,跨门店收款--总经理 \n销售开单失败:${JSON.stringify(sfData)}`).to.not.have.property('error');

		//今日仓库店如有收款操作，撤销
		let searchReceiptList = {
			'invid': BASICDATA.shopidCkd,
			'optime1': common.getDateString([-2, 0, 0]),
			'optime2': common.getCurrentDate()
		};
		let receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));

		if (receiptList.count > 0) {
			let deletePk = receiptList.dataList[0]['id'];
			let deleteResult = await common.callInterface('cs-1195-delete', {
				'pk': deletePk
			});
			expect(deleteResult, `\n定位:170738,跨门店收款--总经理 \n撤销收款单失败:${JSON.stringify(deleteResult)}`).to.not.have.property('error');
		}

		// 销售开单按批次查列表, 获取第一条销售单的PK, 并进行收款操作
		let searchBatchList = {
			'shopid': BASICDATA.shopidCkd,
			'prodate1': common.getDateString([0, -1, 0]),
			'prodate2': common.getCurrentDate(),
		};

		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, false));
		if (batchInfo.count > 0) {
			//收款
			let receiptPk = batchInfo.dataList[0]['id'];
			let receiptResult = await common.callInterface('cs-sales-receivables', {
				'lastbillid': receiptPk
			});
			// 查询收款结果
			searchReceiptList.optime1 = common.getCurrentDate();
			receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));

			// 验证收款成功
			expect(receiptResult, `\n定位:170738,跨门店收款--总经理 \n收款失败:${JSON.stringify(receiptResult)}`).to.includes({
				val: 'ok',
			});
			// 验证收款门店是仓库店 ，而非常青店
			expect(receiptList.dataList[0], `\n定位:170738,跨门店收款--总经理 \n收款门店错误`).to.includes({
				"invName": "仓库店"
			});
		}
	});

	it('170738,跨门店收款--财务员', async function () {
		await common.setGlobalParam('sc_show_onlymyshop_bynogm', 0); //非总经理岗位显示所有门店
		await treasurerLogin();

		//仓库店开单
		let sfData = await salesFromCkd();
		expect(sfData, `\n定位:170738,跨门店收款--财务员 \n销售开单失败:${JSON.stringify(sfData)}`).to.not.have.property('error');

		//今日仓库店如有收款操作，撤销
		let searchReceiptList = {
			'invid': BASICDATA.shopidCkd,
			'optime1': common.getDateString([-2, 0, 0]),
			'optime2': common.getCurrentDate()
		};
		let receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));
		if (receiptList.count > 0) {
			let deletePk = receiptList.dataList[0]['id'];
			let deleteResult = await common.callInterface('cs-1195-delete', {
				'pk': deletePk
			});
			expect(deleteResult, `\n定位:170738,跨门店收款--财务员 \n撤销收款单失败:${JSON.stringify(deleteResult)}`).to.not.have.property('error');
		}

		//销售开单按批次查列表,获取第一条销售单的PK,并进行收款操作
		let searchBatchList = {
			'shopid': BASICDATA.shopidCkd,
			'prodate1': common.getDateString([0, -1, 0]),
			'prodate2': common.getCurrentDate(),
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, false));
		if (batchInfo.count > 0) {
			//收款
			let receiptPk = batchInfo.dataList[0]['id'];
			let receiptResult = await common.callInterface('cs-sales-receivables', {
				'lastbillid': receiptPk
			});
			// 查询收款结果
			searchReceiptList.optime1 = common.getCurrentDate();
			receiptList = await common.callInterface('ql-1195', format.qlParamsFormat(searchReceiptList, false));

			// 验证收款成功
			expect(receiptResult, `\n定位:170738,跨门店收款--财务员 \n收款失败:${JSON.stringify(receiptResult)}`).to.includes({
				val: 'ok',
			});
			// 验证收款门店是仓库店 ，而非常青店
			expect(receiptList.dataList[0], `\n定位:170738,跨门店收款--财务员 \n收款门店错误`).to.includes({
				"invName": "仓库店"
			});
		}
	});
});

/*
 * 判断收款的各种收款方式下 收款金额是否正确
 */
function isPaymentAmountEqual(expected, actual) {
	if (actual.dataList[0]['cash'] != expected.sumrow['cash']) {
		// 现金
		return `cash不同 expect:${expected.sumrow['cash']} actual:${actual.dataList[0]['cash']}`;
	}
	if (actual.dataList[0]['card'] != expected.sumrow['card']) {
		// 刷卡
		return `card不同 expect:${expected.sumrow['card']} actual:${actual.dataList[0]['card']}`;
	}
	if (Number(actual.dataList[0]['remit']) != expected.sumrow['remit']) {
		// 汇款
		return `remit不同 expect:${expected.sumrow['remit']} actual:${actual.dataList[0]['remit']}`;
	}
	if (actual.dataList[0]['alipay'] != expected.sumrow['alipay']) {
		// 支付宝
		return `alipay不同 expect:${expected.sumrow['alipay']} actual:${actual.dataList[0]['alipay']}`;
	}
	if (actual.dataList[0]['weixinpay'] != expected.sumrow['weixinpay']) {
		// 微信
		return `weixinpay不同 expect:${expected.sumrow['weixinpay']} actual:${actual.dataList[0]['weixinpay']}`;
	}

	return true;
}

/*
 * 常青店财务员登录
 */
async function treasurerLogin() {

	let loginParams = {
		'logid': '001',
		'pass': '000000',
		'deviceno': caps.deviceNo,
		'dlProductCode': 'slh',
		'epid': caps.epid,
		'language': 'zh-Hans-CN',
		'slh_version': caps.slhVersion,
	};
	await common.loginDo(loginParams);
	BASICDATA = await getBasicData.getBasicID();

}

/*
 * 仓库店开单 [销售开单]
 */
async function salesFromCkd() {
	let jsonparam = basiceJsonparam.salesJson();
	jsonparam.card = jsonparam.cash = jsonparam.remit = 0;
	jsonparam.invid = jsonparam.shopid = BASICDATA.shopidCkd;
	jsonparam = format.jsonparamFormat(jsonparam);
	let sfData = await common.callInterface('sf-14211-1', {
		'jsonparam': jsonparam,
	});

	return sfData;
}
