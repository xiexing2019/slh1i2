'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const printReqHandler = require('../../help/printHelp/printRequestHandler');
const reqHandler = require('../../help/reqHandlerHelp');

//开单模式
describe('挂单-slh2--mainLine', async function () {
	this.timeout(30000);
	//挂单的jsonparam,挂单的sfRes,是否挂单(二代qf接口需要)
	let [hangJson, hangRes, isPend] = [{}, {}, 1];

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('ignorecolorsize', 0); //显示颜色尺码表
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款

		hangJson = basiceJsonparam.specialSalesJson();
		hangJson.invalidflag = 9; //挂单
	});

	it('170303.按挂单-正常功能检查.rankA', async function () {
		hangRes = await salesReqHandler.editSalesHangBill(hangJson); //新增挂单
		let qfRes = await salesReqHandler.salesQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1,
		}); //查询挂单详细信息
		common.isApproximatelyEqualAssert(hangRes.params, qfRes.result, ['invalidflag']); //挂单单据验证

		let param = format.qlParamsFormat({
			shopid: hangRes.params.shopid,
			dwid: hangRes.params.dwid,
			invalidflag: 9, //挂单9 作废1
			remark: hangRes.params.remark,
			pagesize: 15,
		}, true);
		let qlRes = await common.callInterface('ql-14222', param); //销售开单-按挂单 查询
		//agency 在异地+代收 170674中验证
		//weixinpay alipay storedValueCost dwfdname payback未涉及
		let exp = getQLResExpect(hangRes, qfRes);
		expect(qlRes.dataList).to.satisfy((arr) => common.isArrayContainObject(arr, exp),
			`dataList:${JSON.stringify(qlRes.dataList)}\n未找到期望值exp:${JSON.stringify(exp)}`);

		if (USEECINTERFACE == 2) await common.delay(2000);
	});

	//依赖170303
	it('170642.挂单不允许设置为已配货', async function () {
		TESTCASE = {
			describe: "挂单不允许设置为已配货",
			expect: `提示作废，待作废，挂单的单子不允许设置已配货`,
			jira: 'SLHSEC-6795'
		}
		// hangRes = await common.editBilling(hangJson);
		let res = await common.callInterface('cs-setInvdisflag', { //配货
			pk: hangRes.result.pk,
			isPend: 1
		});
		expect(res).to.includes({
			error: '作废，待作废，挂单的单子不允许设置已配货',
		});
	});

	//依赖170303
	it('170171.挂单转销售单.rankA', async function () {
		hangRes.params.pk = hangRes.result.pk;
		let sfRes = await salesReqHandler.hangToSalesBill(hangRes.params); //挂单转销售单
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['pk']); //挂单转销售单后，会生成新的pk

		let param = format.qlParamsFormat({
			billno1: qfRes.result.billno,
			billno2: qfRes.result.billno,
			invalidflag: 0, //状态
		}, true);
		let qlRes = await common.callInterface('ql-142201', param); //按批次查 检查销售单
		common.isApproximatelyEqualAssert(qfRes.result, qlRes.dataList[0]);
	});

	//包含170170.挂单修改界面新增删除操作, 170176.挂单修改界面修改客户和付款方式
	it('170173.二次挂单.rankA', async function () {
		hangRes = await salesReqHandler.editSalesHangBill(hangJson); //新增挂单
		let qfRes = await salesReqHandler.salesQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1,
		});

		let json = hangRes.params;
		json.pk = qfRes.params.pk;
		json.dwid = BASICDATA.dwidLs; //修改客户
		json.details[1] = { //修改明细 等同界面上删除新增操作
			"num": "5",
			"sizeid": "XL",
			"matCode": "Agc001",
			"rem": "修改明细",
			"colorid": "BaiSe",
			"price": "200",
		};
		json.cash += 200;
		json.card += 100;
		json.remit += 100;
		hangRes = await salesReqHandler.editSalesHangBill(json); //二次挂单
		qfRes = await salesReqHandler.salesQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1,
		});
		common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);

		let param = format.qlParamsFormat({
			shopid: hangRes.params.shopid,
			dwid: hangRes.params.dwid,
			invalidflag: 9, //挂单9 作废1
			remark: hangRes.params.remark,
			pagesize: 15,
		}, true);
		let qlRes = await common.callInterface('ql-14222', param); //销售开单-按挂单 查询
		let exp = getQLResExpect(hangRes, qfRes);
		expect(qlRes.dataList).to.satisfy((arr) => common.isArrayContainObject(arr, exp),
			`查询结果中未找到pk为${exp.id}的挂单\ndataList:${JSON.stringify(qlRes.dataList)}`);
	});

	it('170304.按挂单-挂单作废.rankA', async function () {
		hangRes = await salesReqHandler.editSalesHangBill(hangJson); //新增挂单 用例独立性
		let qfRes = await salesReqHandler.salesQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1
		});
		let cancelHangRes = await common.callInterface('cs-14222-2', { //作废挂单
			pk: hangRes.result.pk,
		});
		expect(cancelHangRes).to.includes({
			val: 'ok',
		}, `作废挂单失败 error:${JSON.stringify(cancelHangRes)}`);

		let param = format.qlParamsFormat({
			shopid: hangRes.params.shopid,
			dwid: hangRes.params.dwid,
			invalidflag: 1, //挂单9 作废1
			pagesize: 15,
		}, true);
		let qlRes = await common.callInterface('ql-14222', param); //销售开单-按挂单 查询
		let exp = getQLResExpect(hangRes, qfRes);
		exp.invalidflag = 1;
		expect(qlRes.dataList).to.satisfy((arr) => common.isArrayContainObject(arr, exp),
			`查询结果中未找到pk为${exp.id}的挂单\ndataList:${JSON.stringify(qlRes.dataList)}`);
	});

	it('171029.挂单作废后检查库存和帐款.rankA', async function () {
		let json = _.cloneDeep(hangJson);
		[json.cash, json.card, json.remit] = [0, 0, 0]; //未付-欠款
		let sfRes = await salesReqHandler.editSalesHangBill(json);

		let params = {
			'ql-1932': { //当前库存
				propdresStyleid: BASICDATA.styleidAgc001,
				search_list: 0,
				search_sum: 1,
			},
			'ql-1350': { //往来管理-客户账款-客户总账
				dwid: BASICDATA.dwidXw
			},
		};
		let qlResBef = await common.getResults(params); //作废前

		await reqHandler.csIFCHandler({
			interfaceid: 'cs-14222-2', //作废挂单
			pk: sfRes.result.pk,
			isPend: 1
		});

		let qlRes = await common.getResults(params);
		common.isApproximatelyEqualAssert(qlResBef, qlRes);
	});

	it('170175.对正常销售单执行挂单操作', async function () {
		if (USEECINTERFACE == 2) this.skip();
		TESTCASE = {
			describe: '对正常销售单执行挂单操作,没有错误提示',
			jira: 'SLHSEC-7534'
		}
		let sfRes = await common.editBilling(basiceJsonparam.simpleSalesJson()); //新增销售单
		sfRes.params.pk = sfRes.result.pk;
		sfRes = await salesReqHandler.editSalesHangBill(sfRes.params, false);
		expect(sfRes.result, `对正常的销售单执行挂单操作的错误提示语错误:\n${JSON.stringify(sfRes)}`).to.includes({ msgId: "bill_not_allow_savetemp" });
	});

	it('170758.检查还款/抵扣', async function () {
		let json = _.cloneDeep(hangJson);
		[json.cash, json.card, json.remit] = [0, 0, 0];
		let sfRes = await salesReqHandler.editSalesHangBill(json);
		let qfRes = await salesReqHandler.salesQueryBilling({
			pk: sfRes.result.pk,
			isPend: 1
		});
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('170724.前几天的挂单保存之后日期应变为当天.rankA', async function () {
		let json = _.cloneDeep(hangJson);
		json.prodate = common.getDateString([0, 0, -1]);
		let hangRes = await salesReqHandler.editSalesHangBill(json); //生成昨天的挂单
		let qfRes = await salesReqHandler.salesQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1,
		});
		qfRes.result.interfaceid = 'sf-14211-1';
		qfRes.result.action = 'edit';
		qfRes.result.invalidflag = 0;
		qfRes.result.fromPend = 1;
		let sfRes = await common.editBilling(qfRes.result); //挂单转销售单 因为需要验证prodate,所以不能用hangToSalesBill
		qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert({
			prodate: common.getCurrentDate(),
		}, qfRes.result, [], '单据日期错误');

		let param = format.qlParamsFormat({
			billno1: qfRes.result.billno,
			billno2: qfRes.result.billno,
			invalidflag: 0, //状态
		}, true);
		let qlResult = await common.callInterface('ql-142201', param); //按批次查 检查销售单
		common.isApproximatelyEqualAssert({
			prodate: common.getCurrentDate(),
		}, qlResult.dataList[0], [], '单据明细日期错误');
	});
	//170724补充 前几天的挂单保存之后日期应变为当天 挂单后，qf接口返回日期为当天
	it('170724.销售订货/采购入库检查.rankA', async function () {
		let json = _.cloneDeep(hangJson);
		json.prodate = common.getDateString([0, 0, -1]);

		//销售订货
		let hangRes = await salesOrderReqHandler.editSalesOrderHangBill(json); //生成昨天的挂单
		let qfRes = await salesOrderReqHandler.salesOrderQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1
		});
		expect(qfRes.result.prodate).to.equal(common.getCurrentDate());

		//采购入库
		json.dwid = BASICDATA.dwidVell;
		hangRes = await purReqHandler.editPurHangBill(json); //生成昨天的挂单
		qfRes = await purReqHandler.purQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1
		});
		expect(qfRes.result.prodate).to.equal(common.getCurrentDate());
	});

	if (USEECINTERFACE == 1) {
		//二代为客户端控制 调用cs-share提示单据已不存在 不能作为判断依据
		it('171008.分享挂单时检查提示', async function () {
			let sfResHang = await salesReqHandler.editSalesHangBill(hangJson);
			let result = await common.callInterface('cs-share', {
				typeid: 1, //1表示销售单，3表示销售订单
				pk: sfResHang.result.pk,
			});
			expect(result).to.includes({
				error: '仅允许分享正常单据、作废、待作废、挂单均不允许分享!'
			});
		});
	};

	describe('170710.挂单允许销售价格为0，转正式单不允许销售价格为0', function () {
		before(async function () {
			//销售允许单价为0的退货和开单--0,默认退货和销售价格不能为零
			await common.setGlobalParam('sales_allownoprice', 0);
		});

		it('1.销售开单-开单，输入款号，修改价格为0，点保存', async function () {
			TESTCASE = {
				describe: '销售允许单价为0的退货和开单--0,默认退货和销售价格不能为零\n\t销售开单-开单，输入款号，修改价格为0，点保存',
				expect: 'sales_allownoprice:0时,修改价格为0,不能正常开单'
			};
			let json = basiceJsonparam.simpleSalesJson();
			json.details[0].price = 0;
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '销售价不能为零，请核对款号[agc001]价格是否维护' : `[第1行]销售价不能为零，请核对款号[agc001]价格是否维护`,
			});
		});

		it('2.销售开单-挂单 + 挂单转正式单', async function () {
			TESTCASE = {
				describe: '销售允许单价为0的退货和开单--0,默认退货和销售价格不能为零\n\t销售开单-挂单转正式单，修改价格为0，点保存',
				expect: 'sales_allownoprice:0时,修改价格为0,不能正常开单'
			};
			let json = _.cloneDeep(hangJson);
			json.details[0].price = 0;
			hangRes = await salesReqHandler.editSalesHangBill(json);

			hangRes.params.pk = hangRes.result.pk;
			let sfRes = await salesReqHandler.hangToSalesBill(hangRes.params, false); //挂单转销售单
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '销售价不能为零，请核对款号[agc001]价格是否维护' : `[第1行]销售价不能为零，请核对款号[agc001]价格是否维护`,
			});
		});

		it('3.销售订货-开单 + 销售开单-按订货开单', async function () {
			TESTCASE = {
				describe: '销售允许单价为0的退货和开单--0,默认退货和销售价格不能为零\n\t销售开单-按订货开单，修改价格为0，点保存',
				expect: 'sales_allownoprice:0时,修改价格为0,不能正常开单',
				jira: 'SLHSEC-7263',
			};
			let json = basiceJsonparam.salesOrderJson();
			json.details[0].price = 0;
			let sfRes = await common.editBilling(json); //生成订货单
			let orderRes = await salesOrderReqHandler.salesOrderQueryBilling(sfRes.result.pk);

			sfRes = await salesReqHandler.editSalesBillByOrder(orderRes.result, false);
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '销售价不能为零，请核对款号[agc001]价格是否维护' : `[第1行]销售价不能为零，请核对款号[agc001]价格是否维护`,
			});
		});

		it('4.销售订货-挂单 + 挂单转正式单', async function () {
			let json = basiceJsonparam.salesOrderJson();
			json.details[0].price = 0; //销售价改为0
			hangRes = await salesOrderReqHandler.editSalesOrderHangBill(json);

			let qfRes = await salesOrderReqHandler.salesOrderQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1
			}); //获取挂单信息
			if (!qfRes.result.pk) qfRes.result.pk = qfRes.params.pk;
			//挂单转销售订单 应该正常保存
			await salesOrderReqHandler.hangToSalesOrderBill(qfRes.result);
		});

	});

	describe('销售开单允许作废和修改天数', function () {
		before(async () => {
			await common.setGlobalParam('sales_invalidate_days', 0); //不能作废
		});
		after(async () => {
			//销售开单允许作废和修改天数
			//参数0,1,3,5,7,30,90代表可以允许天数
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 5);
		});

		it('170953.2 采购入库挂单', async function () {
			let json = _.cloneDeep(hangJson);
			json.interfaceid = 'sf-14212-1'; //采购入库
			json.dwid = BASICDATA.dwidVell;
			json.prodate = common.getDateString([0, 0, -1]);
			let sfResHang = await purReqHandler.editPurHangBill(json); //采购入库 生成以前的挂单
			let qfResultHang = await purReqHandler.purQueryBilling({
				pk: sfResHang.result.pk,
				isPend: 1,
			});

			qfResultHang.result.interfaceid = sfResHang.params.interfaceid; //采购入库
			qfResultHang.result.action = 'edit';
			qfResultHang.result.remark += 'a';
			sfResHang = await purReqHandler.editPurHangBill(qfResultHang.result); //修改挂单-二次挂单

			let cancelHangRes = await purReqHandler.cancelPurchaseBill({
				pk: sfResHang.result.pk,
				isPend: 1
			});
			expect(cancelHangRes.result).to.includes({
				val: 'ok',
			}, `作废挂单失败 error:${JSON.stringify(cancelHangRes)}`);
		});

		it('170953.3 销售订货挂单', async function () {
			let json = _.cloneDeep(hangJson);
			json.prodate = common.getDateString([0, 0, -1]);
			let sfResHang = await salesOrderReqHandler.editSalesOrderHangBill(json); //生成以前的挂单
			let qfResultHang = await salesOrderReqHandler.salesOrderQueryBilling({
				pk: sfResHang.result.pk,
				isPend: 1
			});

			qfResultHang.result.interfaceid = sfResHang.params.interfaceid; //销售订货
			qfResultHang.result.action = 'edit';
			qfResultHang.result.remark += 'a';
			sfResHang = await salesOrderReqHandler.editSalesOrderHangBill(qfResultHang.result); //修改挂单-二次挂单

			//作废挂单
			await salesOrderReqHandler.cancelSaleorderBill({
				pk: sfResHang.result.pk,
				isPend,
			});
		});

		it('170645.挂单转销售单不限制修改天数', async function () {
			let json = _.cloneDeep(hangJson);
			json.prodate = common.getDateString([0, 0, -1]); //生成一个昨天的挂单
			let sfResHang = await salesReqHandler.editSalesHangBill(json);

			sfResHang.params.pk = sfResHang.result.pk;
			await salesReqHandler.hangToSalesBill(sfResHang.params); //挂单转销售单
		});

		it('170905.前几天的挂单转为正式单/作废.rankA', async function () {
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});
			let json = _.cloneDeep(hangJson);
			json.prodate = common.getDateString([0, 0, -1]);

			let sfResHang = await salesReqHandler.editSalesHangBill(json); //生成以前的挂单
			sfResHang.params.pk = sfResHang.result.pk;
			let sfRes = await salesReqHandler.hangToSalesBill(sfResHang.params); //挂单转销售单

			sfResHang = await salesReqHandler.editSalesHangBill(json); //生成以前的挂单
			let cancelResult = await common.callInterface('cs-14222-2', { //作废挂单
				pk: sfResHang.result.pk,
			});
			expect(cancelResult).to.includes({
				val: 'ok',
			}, `作废挂单失败 error:\n${JSON.stringify(cancelResult)}`);
		});
	});

	describe('权限检查', function () {
		before(async function () {
			await common.loginDo({ //中洲店店长登陆
				'logid': '204',
				'pass': '000000',
			});
		});

		after(async function () {
			await common.loginDo();
		});

		//只应显示本门店的挂单,不显示其它门店的挂单 (不要用总经理工号去测)
		it('170177.其他角色权限检查', async function () {
			let param = format.qlParamsFormat({
				prodate1: common.getDateString([0, -1, 0]),
				// pagesize:15
			}, false);
			let qlRes = await common.callInterface('ql-14222', param); //销售开单-按挂单 查询
			expect(qlRes.dataList).to.satisfy((arr) => {
				let ret = true;
				for (let i = 0; i < arr.length; i++) {
					if (arr[i].shopname == '常青店') {
						ret = false;
						break;
					};
				};
				return ret;
			});
		});
	});

	describe('非总经理验证', function () {
		before(async () => {
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});
		});
		after(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_print_bypendbill', 0); //挂单是否打印 默认不打印
			await common.setGlobalParam('cannotmodifyafterprint', 0); //单据打印后不允许修改 不限制
		});

		it('170944.非总经理按挂单只显示本门店数据', async function () {
			let param = format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				clientid: BASICDATA.dwidXw,
				prodate1: common.getDateString([0, 0, -10])
			}, false);
			let salesResult = await common.callInterface('ql-14222', param); //销售开单-按挂单
			let salesOrderResult = await common.callInterface('ql-144331', param); //销售订货-按挂单

			salesResult.dataList.map((obj) => {
				expect(obj.dwname).to.equal('小王');
				expect(obj.shopname).to.equal('常青店');
			});
			salesOrderResult.dataList.map((obj) => {
				expect(obj.dwname).to.equal('小王');
				expect(obj.invname).to.equal('常青店');
			});
		});

		it('170975.挂单打印后修改单据', async () => {
			await common.setGlobalParam('sales_print_bypendbill', 1); //挂单是否打印 打印
			await common.setGlobalParam('cannotmodifyafterprint', 1); //单据打印后不允许修改 明细不允许修改

			let json = _.cloneDeep(hangJson);
			let sfResHang = await common.editBilling(json);
			await printReqHandler.getPrint(sfResHang); //更新打印标记
			let qfResHang = await salesReqHandler.salesQueryBilling({
				pk: sfResHang.result.pk,
				isPend: 1,
			});

			qfResHang.result.interfaceid = sfResHang.params.interfaceid;
			qfResHang.result.action = 'edit';
			qfResHang.result.details[0].num = 10;
			await common.editBilling(qfResHang.result); //二次挂单 正常保存
		});
	});

});

function getQLResExpect(hangRes, qfRes) {
	return {
		billno: 0,
		shopname: '常青店', //异地+代收 170674中也有涉及
		invname: '常青店', //异地+代收 170674中也有涉及
		id: hangRes.result.pk,
		invalidflag: 9,
		finpayPaysum: qfRes.result.actualpay || qfRes.result.totalmoney,
		cash: hangRes.params.cash || 0,
		card: hangRes.params.card || 0,
		remit: hangRes.params.remit || 0,
		remark: qfRes.result.remark,
		balance: qfRes.result.balance,
		dwname: qfRes.result.show_dwid,
		prodate: common.getCurrentDate('YY-MM-DD'),
		opname: qfRes.result.opname,
		totalnum: qfRes.result.totalnum,
		totalmoney: qfRes.result.totalmoney,
		finpayVerifysum: qfRes.result.finpayVerifysum || 0,
		optime: qfRes.result.optime,
		staffName: qfRes.result.opname,
	};
};
