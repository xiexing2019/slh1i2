'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 销售开单--直接开单,自动核销订货数
 */
describe('直接开单,自动核销订货数-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sales_order_deliver_mode', 3);
		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('paymethod', 2);
	});

	after(async () => {
		await common.setGlobalParam('sales_order_deliver_mode', 1);
	});

	it('170847.终结的订单不能再变为普通单.rankA', async function () {
		let custRes = await salesRequest.addCust(basiceJsonparam.addCustJson());
		let json = basiceJsonparam.simpleSalesJson();
		json.interfaceid = 'sf-14401-1';
		json.clientid = custRes.result.val; //销售订货
		json.details[0].num = 5;
		let sfResOrder = await common.editBilling(json);

		json.interfaceid = 'sf-14211-1';
		json.dwid = custRes.result.val; //销售开单
		json.details[0].num = 3; //部分发货
		await common.editBilling(json);

		await salesOrderRequest.saleOrderFinish(sfResOrder.result.pk); //终结订单
		await common.editBilling(json); //继续销售开单

		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息
		common.isApproximatelyEqualAssert({
			showFlag: '结束',
			sendflag: '3',
			recvnum: '3', //已发数
		}, qfResOrder.result);
	});

	it('170851.1 自动核销发货数.rankA', async function () {
		let custRes = await salesRequest.addCust(basiceJsonparam.addCustJson());
		expect(custRes.result, `新增客户失败:${JSON.stringify(custRes)}`).to.have.property('val');

		let json = basiceJsonparam.simpleSalesJson();
		json.interfaceid = 'sf-14401-1';
		json.clientid = custRes.result.val; //销售订货 客户
		json.details[0].num = 5;

		let sfResOrder = await common.editBilling(json); //新增订单
		await common.delay(1000);
		let sfResOrder2 = await common.editBilling(json); //新增订单
		await common.delay(1000);
		let sfResOrder3 = await common.editBilling(json); //新增订单

		json.interfaceid = 'sf-14211-1';
		json.dwid = custRes.result.val; //销售开单
		json.details[0].num = 8;
		await common.editBilling(json); //新增销售单

		//如果客户有多次订货未发， 发货优先级是按照订货日期， 越早的越优先发货
		//在发完货后， 销售订货里的已发数， 订单状态会自动跟着变
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //第一条订单
		common.isApproximatelyEqualAssert({
			showFlag: '全部发货',
			sendflag: '2',
			recvnum: '5', //已发数
		}, qfResOrder.result);
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder2.result.pk); //第二条订单
		common.isApproximatelyEqualAssert({
			showFlag: '部分发货',
			sendflag: '1',
			recvnum: '3', //已发数
		}, qfResOrder.result);
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder3.result.pk); //第三条订单
		common.isApproximatelyEqualAssert({
			showFlag: '未发货',
			sendflag: '0',
			recvnum: '0', //已发数
		}, qfResOrder.result);
	});

	it('170851.2 作废.rankA', async function () {
		let custRes = await salesRequest.addCust(basiceJsonparam.addCustJson());
		expect(custRes.result, `新增客户失败:${JSON.stringify(custRes)}`).to.have.property('val');

		let json = basiceJsonparam.simpleSalesJson();
		json.interfaceid = 'sf-14401-1';
		json.clientid = custRes.result.val; //销售订货 客户
		json.details[0].num = 5;
		let sfResOrder = await common.editBilling(json); //新增订单

		json.interfaceid = 'sf-14211-1';
		json.dwid = custRes.result.val; //销售开单
		json.details[0].num = 5;
		let sfRes = await common.editBilling(json); //新增销售单
		await salesRequest.cancelSaleoutBill(sfRes.result.pk); //作废单据

		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息
		common.isApproximatelyEqualAssert({
			showFlag: '未发货',
			sendflag: '0',
			recvnum: '0', //已发数
		}, qfResOrder.result);
	});

	// http://jira.hzdlsoft.com:7082/browse/SLH-19578
	// cs-getsalebill-lastprice:获取销售开单上次价
	// @param biztype 默认21，返回销售单上次价，150表示销售订单上次价
	describe('170851.3 上次价.rankA', function () {
		let lastPriceParam, salesJson, salesOrderJson;
		//销售订单价格200 销售单价格210
		before(async () => {
			await common.setGlobalParam('sales_show_lastprice', 1); //开单显示上次单价
			await common.setGlobalParam('sales_use_lastsaleprice', 1); //启用上次成交价作为本次开单单价

			salesOrderJson = basiceJsonparam.simpleSalesJson();
			salesOrderJson.interfaceid = 'sf-14401-1';
			salesOrderJson.clientid = BASICDATA.dwidLs; //这里客户从小王--李四  小王由于采购订货太多，导致直接开单，自动核销订货数的时候二代经常超时
			salesOrderJson.details[0].price = 200;

			salesJson = basiceJsonparam.simpleSalesJson();
			salesJson.dwid = BASICDATA.dwidLs;
			salesJson.details[0].price = 210;

			lastPriceParam = {
				'shopid': LOGINDATA.invid,
				'dwid': BASICDATA.dwidLs,
				'clientid': BASICDATA.dwidLs,
				'styleid': BASICDATA.styleidAgc001,
			};
		});
		after(async () => {
			await common.setGlobalParam('sales_use_lastsaleprice', 0); //不启用上次成交价作为本次开单单价
			await common.setGlobalParam('sales_show_lastprice', 0); //开单不显示上次单价
			await common.setGlobalParam('saleout_lastprice_strategy', 0); //销售开单获取上次价策略 只获取上次开单价
			await common.setGlobalParam('saleorder_lastprice_strategy', 0); //销售订货获取上次价策略 只获取上次订货价
		});
		beforeEach(async () => {
			lastPriceParam.biztype = 21;
		});

		it('1.销售开单和销售订货各自取各自的成交价', async function () {
			await common.setGlobalParam('saleout_lastprice_strategy', 0); //销售开单获取上次价策略 只获取上次开单价
			await common.setGlobalParam('saleorder_lastprice_strategy', 0); //销售订货获取上次价策略 只获取上次订货价

			let sfResOrder = await common.editBilling(salesOrderJson); //新增订单
			let sfRes = await common.editBilling(salesJson); //新增销售单

			let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售开单上次价错误').to.equal('210');

			lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售订货上次价错误').to.equal('200');
		});

		it('2.销售开单获取上次价策略-上次开单价和上次订货价取最近的', async function () {
			await common.setGlobalParam('saleout_lastprice_strategy', 1); //上次开单价和上次订货价取最近的

			let sfRes = await common.editBilling(salesJson); //新增销售单
			let sfResOrder = await common.editBilling(salesOrderJson); //新增订单
			lastPriceParam.biztype = 150; //默认21，返回销售单上次价，150表示销售订单上次价
			let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售开单上次价错误').to.equal('200');

			sfRes = await common.editBilling(salesJson); //新增销售单
			lastPriceParam.biztype = 21;
			lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售开单上次价错误').to.equal('210');
		});

		it('3.优先获取上次订货价,不存在则取上次开单价', async function () {
			await common.setGlobalParam('saleout_lastprice_strategy', 2); //优先获取上次订货价，不存在则取上次开单价

			//先开单,再订货,再销售开单中,则取订货的成交价
			let sfRes = await common.editBilling(salesJson); //新增销售单
			let sfResOrder = await common.editBilling(salesOrderJson); //新增订单
			let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售开单上次价错误').to.equal('200');

			//先开单,再订货,再开单,在销售开单中,还是取订货的订货价
			sfRes = await common.editBilling(salesJson); //新增销售单
			lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售开单上次价错误').to.equal('200');
		});

		it('4.销售订货获取上次价策略,上次开单价和上次订货价取最近的', async function () {
			await common.setGlobalParam('saleorder_lastprice_strategy', 1); //上次开单价和上次订货价取最近的

			//先开单,不订货,在销售订货中,一直取开单的成交价
			let sfRes = await common.editBilling(salesJson); //新增销售单
			let lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售订单上次价错误1').to.equal('210');

			//先开单,再订货,在销售订货中,则取订货的订货价
			await common.delay(1000);
			let sfResOrder = await common.editBilling(salesOrderJson); //新增订单
			lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售订单上次价错误2').to.equal('200');

			//先开单,再订货,再开单,在销售订货中,是取开单的成交价
			sfRes = await common.editBilling(salesJson); //新增销售单
			lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', lastPriceParam);
			expect(lastPriceInfo.price, '销售订单上次价错误3').to.equal('210');
		});
	});
});
