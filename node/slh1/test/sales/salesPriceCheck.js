'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

//开单的jsonparam
const jsonparamSource = _.cloneDeep(basiceJson.simpleSalesJson());

//全局参数
let params = [{
	code: 'sales_pricecheck', //销售价格允许改高不允许改低
	val: 0, //0不检查 1销售价不能低于零批价 2不检查(店长权限)
}, {
	code: 'sales_pricecheck_pricetype', //销售开单价不能低于指定的价格类型
	val: 0, //-1不限制 0采购价 1~5:价格1~5
}, {
	code: 'sales_pricecheck_continue', //当销售开单价低于指定的价格类型时，是否允许继续保存
	val: 0, //0没有权限可以挂单，有权限可以记账 1第一个人必须挂单，第二个人有权限可以记账
}];
// 后台角色权限
// 修改后需要重新登录
let roleParams = [{
	ipadcode: 'shopmanager', //店长
	code: 'sales_pricecheck_continue', //销售开单低于指定价格时是否允许继续保存
	val: 0, //0只允许挂单 1允许记账
}, {
	ipadcode: 'boss', //开单员
	code: 'sales_pricecheck_continue', //销售开单低于指定价格时是否允许继续保存
	val: 0, //0只允许挂单 1允许记账
}];

//修改参数
//e.g. [0,0,0]
let setParams = async function (arr, arr2) {
	params.map((value, index) => value.val = arr[index]);
	await common.setGlobalParams(params);

	if (arr2) {
		roleParams.map((value, index) => value.val = arr2[index]);
		await common.setRoleParam(roleParams[0]);
		await common.setRoleParam(roleParams[1]);
	};
};

let login004 = {
	logid: '004',
	pass: '000000',
};
let login005 = {
	logid: '005',
	pass: '000000',
};

// 3.店长/开单员输入低于采购价和适用价格,分别进行保存和挂单
// 4.店长/开单员输入高于采购价和适用价格，分别进行保存和挂单
// 5.店长/开单员输入高于采购价，低于适用价格，分别进行保存和挂单
// 6.销售开单、销售订货、按订货入库都需执行上述步骤3/4/5
describe("销售价格允许改高不允许改低", function () {
	this.timeout(30000);
	let styleAgc001;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		await common.setGlobalParam('default_stdprice', 1); //默认显示价格类型 1零批价 2打包价
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2);
		await common.setGlobalParam('sales_pricecheck', 0); //销售价格允许改高不允许改低 0不检查
		await common.setGlobalParam('sales_pricecheck_pricetype', -1); //销售开单价不能低于指定的价格类型 -1不限制
	});

	it('总经理不受控制', async function () {
		await priceCheck(+styleAgc001.purprice - 10, 'pass', 'pass');
	});

	describe('170728.rankB', function () {
		before(async () => {
			await setParams([0, 0, 0], [0, 0]);
			await common.loginDo(login004, false);
		});
		it(`店长/开单员 低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'onlyHang', 'pass');
		});
		it(`店长/开单员 高于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
		});
		it(`店长/开单员 高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
		});
	});
	describe('170729.rankB', function () {
		before(async () => {
			await setParams([0, 0, 0], [1, 1]);
			await common.loginDo(login004, false);
		});
		it(`店长/开单员 低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'pass', 'pass');
		});
		it(`店长/开单员 高于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
		});
		it(`店长/开单员 高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
		});
	});
	describe('170730.rankB', function () {
		let result = {};
		before(async () => {
			await setParams([0, 0, 1], [1, 0]);
			await common.loginDo(login004, false);
		});
		it(`3 店长 低于采购价和适用价格`, async function () {
			result = await priceCheck(+styleAgc001.purprice - 10, 'onlyHang', 'pass');
			await hangToSalesCheck(result.hangRes, 'onlyHang');
		});
		it(`5 店长 高于采购价和适用价格`, async function () {
			result = await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
			await hangToSalesCheck(result.hangRes, 'pass');
		});
		it(`7 店长 高于采购价，低于适用价格`, async function () {
			result = await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
			await hangToSalesCheck(result.hangRes, 'pass');
		});
	});
	describe('170730.rankB', function () {
		let result = {};
		before(async () => {
			await setParams([0, 0, 1], [1, 0]);
		});
		it(`4 开单员 低于采购价和适用价格`, async function () {
			await common.loginDo(login005, false);
			result = await priceCheck(+styleAgc001.purprice - 10, 'onlyHang', 'pass');
			await common.loginDo(login004);
			await hangToSalesCheck(result.hangRes, 'pass');
		});
		it(`6 开单员 高于采购价和适用价格`, async function () {
			await common.loginDo(login005);
			result = await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
			await common.loginDo(login004);
			await hangToSalesCheck(result.hangRes, 'pass');
		});
		it(`8 开单员 高于采购价，低于适用价格`, async function () {
			await common.loginDo(login005);
			result = await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
			await common.loginDo(login004);
			await hangToSalesCheck(result.hangRes, 'pass');
		});
	});
	describe('170731.rankB', function () {
		before(async () => {
			await setParams([1, -1, 0]);
			await common.setGlobalParam('default_stdprice', 2);
			await common.loginDo(login004, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`店长/开单员 输入低于适用价格(打包价)`, async function () {
			let res = await priceCheck(+styleAgc001.stdprice2 - 10, 'priceErr', 'priceErr');
		});
		it(`店长/开单员 高于适用价格和采购价`, async function () {
			let res = await priceCheck(+styleAgc001.stdprice2 + 100, 'pass', 'pass');
		});
	});
	describe('170732.rankB', function () {
		before(async () => {
			await setParams([1, 0, 0]);
			await common.setGlobalParam('default_stdprice', 2);
			await common.loginDo(login004, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`店长/开单员 输入高于采购价，低于适用价格（打包价）`, async function () {
			await priceCheck(+styleAgc001.stdprice2 - 10, 'priceErr', 'priceErr');
		});
		it(`店长/开单员 低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'priceErr', 'priceErr');
		});
		it(`店长/开单员 高于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice2 + 100, 'pass', 'pass');
		});
	});
	describe('170733.rankB', function () {
		before(async () => {
			await setParams([1, 2, 0]);
			await common.setGlobalParam('default_stdprice', 2);
			await common.loginDo(login004, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`店长/开单员 输入高于采购价，低于适用价格（打包价）`, async function () {
			await priceCheck(+styleAgc001.stdprice2 - 10, 'priceErr', 'priceErr');
		});
		it(`店长/开单员 低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'priceErr', 'priceErr');
		});
		it(`店长/开单员 高于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice2 + 100, 'pass', 'pass');
		});
	});
	describe('170734.rankB', function () {
		before(async () => {
			await setParams([2, 0, 0], [0, 0]);
			await common.setGlobalParam('default_stdprice', 2);
			await common.loginDo(login004, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`4 店长 输入低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'onlyHang', 'pass');
		});
		it(`5 店长 输入高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice2 - 10, 'pass', 'pass');
		});
	});
	describe('170734.rankB', function () {
		before(async () => {
			await setParams([2, 0, 0], [0, 0]);
			await common.setGlobalParam('default_stdprice', 2);
			await common.loginDo(login005, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`4 开单员 输入低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'priceErr', 'priceErr');
		});
		it(`5 开单员 输入高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice2 - 10, 'priceErr', 'priceErr');
		});
	});
	describe('170735.rankB', function () {
		before(async () => {
			await setParams([2, 0, 0], [1, 1]);
			await common.loginDo(login004, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`店长 输入高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
		});
		it(`店长 输入低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'pass', 'pass');
		});
	});
	describe('170735.rankB', function () {
		before(async () => {
			await setParams([2, 0, 0], [1, 1]);
			await common.loginDo(login005, false);
		});
		after(async () => {
			await common.setGlobalParam('default_stdprice', 1); //防止影响其他用例
		});
		it(`开单员 输入高于采购价，低于适用价格`, async function () {
			await priceCheck(+styleAgc001.stdprice1 - 10, 'priceErr', 'priceErr');
		});
		it(`开单员 输入低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'priceErr', 'priceErr');
		});
	});
	describe('170736.rankB', function () {
		let result = {};
		before(async () => {
			await setParams([2, 0, 1], [1, 0]);
			await common.loginDo(login004, false);
		});
		it(`3 店长 低于采购价和适用价格`, async function () {
			result = await priceCheck(+styleAgc001.purprice - 10, 'onlyHang', 'pass');
			await hangToSalesCheck(result.hangRes, 'onlyHang');
		});
		it(`5 店长 高于采购价和适用价格`, async function () {
			result = await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
			await hangToSalesCheck(result.hangRes, 'pass');
		});
		it(`7 店长 高于采购价，低于适用价格`, async function () {
			result = await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
			await hangToSalesCheck(result.hangRes, 'pass');
		});
	});
	describe('170736.rankB', function () {
		let result = {};
		before(async () => {
			await setParams([2, 0, 1], [1, 0]);
			await common.loginDo(login005, false);
		});
		it(`4 开单员 低于采购价和适用价格`, async function () {
			result = await priceCheck(+styleAgc001.purprice - 10, 'priceErr', 'priceErr');
		});
		it(`6 开单员 高于采购价和适用价格`, async function () {
			await common.loginDo(login005);
			result = await priceCheck(+styleAgc001.stdprice1 + 10, 'pass', 'pass');
			await common.loginDo(login004);
			await hangToSalesCheck(result.hangRes, 'pass');
		});
		it(`7 开单员 高于采购价，低于适用价格`, async function () {
			await common.loginDo(login005);
			result = await priceCheck(+styleAgc001.stdprice1 - 10, 'priceErr', 'priceErr');
		});
	});
	describe('170737', function () {
		before(async () => {
			await setParams([0, -1, 0]);
			await common.loginDo(login004);
		});
		it(`店长/开单员 输入高于采购价，低于适用价格（打包价）`, async function () {
			await priceCheck(+styleAgc001.stdprice1 - 10, 'pass', 'pass');
		});
		it(`店长/开单员 低于采购价和适用价格`, async function () {
			await priceCheck(+styleAgc001.purprice - 10, 'pass', 'pass');
		});
	});
	describe('170822/170823.rankB', function () {

		it('170822,【销售开单-开单】客户折扣模式下“销售价格允许改高不允许改低”是与折前价比较的.rankA', async function () {

			await common.setGlobalParam('sales_pricecheck', 1); //销售价格允许改高不允许改低 1销售价不能低于零批价
			await common.setGlobalParam('sales_pricecheck_pricetype', -1); //销售开单价不能低于指定的价格类型 -1不限制
			await common.setGlobalParam('paymethod', 6);

			// 新增客户
			let custDiscount = '0.8';
			let custJson = basiceJson.addCustJson();
			custJson.discount = custDiscount;
			let custResult = await common.callInterface('sf-1401', {
				'jsonparam': custJson
			});
			expect(custResult, `新增客户失败:${JSON.stringify(custResult)}`).to.not.have.property('error');
			await common.loginDo({
				'logid': '004',
				'pass': '000000'
			});
			let json = basiceJson.simpleSalesJson();
			json.dwid = custResult.val;
			json.maindiscount = custDiscount;
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.not.have.property('error');
		});

		it('170822,【销售开单-开单】产品折扣模式下“销售价格允许改高不允许改低”是与折前价比较的', async function () {
			await common.setGlobalParam('sales_pricecheck', 1); //销售价格允许改高不允许改低 1销售价不能低于零批价
			await common.setGlobalParam('sales_pricecheck_pricetype', -1); //销售开单价不能低于指定的价格类型 -1不限制
			await common.setGlobalParam('paymethod', 5);

			//新增货品
			let matDiscount = 0.1;
			let goodJson = basiceJson.addGoodJson();
			goodJson.discount = matDiscount;
			let goodResult = await basicReqHandler.editStyle({
				jsonparam: goodJson,
			});
			expect(goodResult, `新增货品失败:${JSON.stringify(goodResult)}`).to.not.have.property('error');

			await common.loginDo({
				'logid': '004',
				'pass': '000000'
			});
			let json = basiceJson.simpleSalesJson();
			for (let i = 0; i < json.details.length; i++) {
				json.details[i].discount = goodJson.discount;
				json.details[i].styleid = goodResult.pk;
			}
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result, `销售开单失败:${JSON.stringify(sfRes.result)}`).to.not.have.property('error');
		});

		it('170823,【销售开单-开单】客户折扣模式下“销售开单价不能低于指定的价格类型”是与折后价比较的', async function () {
			await common.setGlobalParam('sales_pricecheck', 0); //销售价格允许改高不允许改低 0不检查
			await common.setGlobalParam('sales_pricecheck_pricetype', 0); //销售开单价不能低于指定的价格类型 采购价
			await common.setGlobalParam('paymethod', 6);
			await common.setRoleParam({
				ipadcode: 'shopmanager', //店长
				code: 'sales_pricecheck_continue', //销售开单低于指定价格时是否允许继续保存
				val: 0, //0只允许挂单 1允许记账
			});

			await common.loginDo();
			// 新增客户
			let custDiscount = '0.1';
			let custJson = basiceJson.addCustJson();
			custJson.discount = custDiscount;
			let custResult = await common.callInterface('sf-1401', {
				'jsonparam': custJson
			});
			expect(custResult, `新增客户失败:${JSON.stringify(custResult)}`).to.not.have.property('error');
			await common.loginDo({
				'logid': '004',
				'pass': '000000'
			});
			let json = basiceJson.simpleSalesJson();
			json.dwid = custResult.val;
			json.maindiscount = custDiscount;
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				"error": "款号[agc001]销售价已低于指定底价,此单您只允许挂单,不允许直接保存记账"
			});
		});

		it('170823,【销售开单-开单】产品折扣模式下“销售开单价不能低于指定的价格类型”是与折后价比较的', async function () {
			await common.setGlobalParam('sales_pricecheck', 0); //销售价格允许改高不允许改低 0不检查
			await common.setGlobalParam('sales_pricecheck_pricetype', 0); //销售开单价不能低于指定的价格类型 采购价
			await common.setGlobalParam('paymethod', 5);
			await common.setRoleParam({
				ipadcode: 'shopmanager', //店长
				code: 'sales_pricecheck_continue', //销售开单低于指定价格时是否允许继续保存
				val: 0, //0只允许挂单 1允许记账
			});

			await common.loginDo();
			//新增货品
			let matDiscount = 0.1;
			let goodJson = basiceJson.addGoodJson();
			goodJson.discount = matDiscount;
			let goodResult = await basicReqHandler.editStyle({
				jsonparam: goodJson
			});
			expect(goodResult, `新增货品失败:${JSON.stringify(goodResult)}`).to.not.have.property('error');

			await common.loginDo({
				'logid': '004',
				'pass': '000000'
			});
			let json = basiceJson.simpleSalesJson();
			for (let i = 0; i < json.details.length; i++) {
				json.details[i].discount = goodJson.discount;
				json.details[i].styleid = goodResult.pk;
			}
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				"error": `款号[${goodJson.code}]销售价已低于指定底价,此单您只允许挂单,不允许直接保存记账`
			});
		});
	});
});

/*
 *	调价验证
 *	price 价格
 *	state1 正常单状态
 *  state2 挂单状态
 *  res:{params,result}
 */
async function priceCheck(price, state1, state2) {
	let json = _.cloneDeep(jsonparamSource);
	json.details[0].price = price;
	let res = await common.editBilling(json, false);
	priceAssert(res, state1, '开单');

	json.invalidflag = 9;
	let hangRes = await common.editBilling(json, false);
	priceAssert(hangRes, state2, '挂单');

	return {
		res,
		hangRes
	};
};

async function hangToSalesCheck(hangRes, state) {
	hangRes.params.pk = hangRes.result.pk;
	let res = await salesRequestHandler.hangToSalesBill(hangRes.params, false);
	priceAssert(res, state, '挂单转销售单');
};


/**
 * priceAssert - 断言
 * @param {object}   res    开单请求 {params,result}
 * @param {string}   state  onlyHang:只允许挂单,priceErr:价格错误,default:开单错误
 * @param {string} [msg=''] Description
 *
 * @return {type} Description
 */
function priceAssert(res, state, msg = '') {
	switch (state) {
		case 'onlyHang':
			expect(res.result).to.includes({
				"error": "款号[agc001]销售价已低于指定底价,此单您只允许挂单,不允许直接保存记账"
			}, `error:${msg} \n${JSON.stringify(res.result)}`);
			break;
		case 'priceErr':
			expect(res.result).to.includes({
				"error": "[第1行][agc001]价格输入错误，因为启用了价格验证价格类型[1]"
			}, `error:${msg} \n${JSON.stringify(res.result)}`);
			break;
		default:
			expect(res.result, `error:${msg} \n${JSON.stringify(res.result)}`).to.have.property('pk');
			break;
	};
};
