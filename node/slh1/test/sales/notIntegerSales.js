'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const shopInOutRequestHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler');

/*
 * 小数开单 [开单时num输入小数]
 */

describe('170756,小数开单-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2);
	});

	it('小数开销售单', async function () {
		let jsonparam = processJson(basiceJsonparam.salesJson());
		let sfRes = await common.editBilling(jsonparam);
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, qfRes.result);
	});

	it('小数开销售订货单', async function () {
		let jsonparam = processJson(basiceJsonparam.salesOrderJson());
		let sfRes = await common.editBilling(jsonparam);
		let qfRes = await salesReqHandler.salesOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, qfRes.result);
	});

	it('小数开采购入库单', async function () {
		let jsonparam = processJson(basiceJsonparam.purchaseJson());
		let sfRes = await common.editBilling(jsonparam);
		let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, qfRes.result);
	});

	it('小数开采购订货单', async function () {
		let jsonparam = processJson(basiceJsonparam.purchaseOrderJson());
		let sfRes = await common.editBilling(jsonparam);
		let qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, qfRes.result);
	});

	it('小数开门店调出单', async function () {
		await common.loginDo({
			'logid': '200',
			'pass': '000000'
		});
		let jsonparam = processJson(basiceJsonparam.outJson());
		let outResult = await common.editBilling(jsonparam);
		let outQfResult = await shopInOutRequestHandler.shopOutQueryBilling(outResult.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, outQfResult.result);
		// 调入
		await common.loginDo({
			'logid': '000',
			'pass': '000000'
		});
		let inQfResult = await shopInOutRequestHandler.shopInQueryBilling({
			pk: outResult.result.pk,
			billno: outResult.result.billno,
		});
		let inResult = await shopInOutRequestHandler.shopIn(inQfResult.result);
	});
});

/*
 * 将jsonparam.details数组各个元素中的num改为小数
 */
function processJson(jsonparam) {
	for (let i = 0; i < jsonparam.details.length; i++) {
		jsonparam.details[i].num = Number(jsonparam.details[i].num) + 0.5;
	}

	jsonparam = format.jsonparamFormat(jsonparam);
	return jsonparam;
}
