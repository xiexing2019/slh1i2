'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const reqHandler = require('../../help/reqHandlerHelp');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler');

describe("销售开单-按明细查-slh2", function () {
	this.timeout(30000);
	let styleAgc001;
	before(async () => {
		await common.loginDo();
		// await common.setGlobalParam('search_designate_range', 1);
		BASICDATA = await getBasicData.getBasicID();
		styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);
	});

	it('170029.查询条件组合查询', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesJson());
		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		let param = format.qlParamsFormat({
			billno: qfRes.result.billno,
			shopid: qfRes.result.shopid,
			dwid: qfRes.result.dwid,
			deliver: qfRes.result.deliver,
			dwfdid: qfRes.result.dwfdid, //客户分店
			styleclassid: styleAgc001.classid,
			styleid: qfRes.result.details[0].styleid,
			stylename: qfRes.result.details[0].stylename,
			colorid: qfRes.result.details[0].colorid,
			sizeid: qfRes.result.details[0].sizeid,
			brandid: styleAgc001.brandid,
			marketdate1: styleAgc001.marketdate,
			marketdate2: styleAgc001.marketdate,
			stdpricetype: qfRes.result.details[0].stdpricetype,
			typeid: qfRes.result.details[0].typeid, //类型
			season: styleAgc001.season,
			rem: qfRes.result.details[0].rem,
			styledwid: styleAgc001.dwid,
		}, true);
		let qlRes = await common.callInterface('ql-1209', param);
		let exp = common.mixObject(qfRes.result.details[0], param);
		delete exp.stdpricetype; //结果显示为零批价不是id
		exp.typeid = '销售';
		exp.brandid = styleAgc001.show_brandid;
		exp.season = styleAgc001.show_season;
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('170643.参数验证  进货入库自动更新款号厂商-更新', async function () {
		//进货入库自动更新款号厂商  0:不更新 1:更新
		await common.setGlobalParam('updatedwidwhenpurin', 1);
		let json = basiceJsonparam.specialPurchaseJson();
		let sfRes = await common.editBilling(json); //采购入库+特殊货品

		json = basiceJsonparam.specialSalesJson();
		sfRes = await common.editBilling(json); //销售开单+特殊货品
		let param = format.qlParamsFormat({
			billno: sfRes.result.billno,
			styleid: BASICDATA.styleid00000,
		}, true);
		let qlRes = await common.callInterface('ql-1209', param);
		if (qlRes.dataList.length != 1) {
			TESTCASE.expect = ``;
			throw new Error(`'ql-1209'销售开单-按明细查特殊货品查询错误`);
		};
		await common.setGlobalParam('updatedwidwhenpurin', 0); //关闭 防止影响其他用例
		TESTCASE = {
			describe: "检查采购入库后特殊货品是否会产生厂商",
			expect: `一代qlRes.dataList[0].provideridname为空，二代不返回字段provideridname`,
			jira: 'SLHSEC-3701销售开单-按明细查款号查询输入特殊货品款号，查询为空'
		};
		//console.log(`qlRes.dataList[0] : ${JSON.stringify(qlRes.dataList[0])}`);
		if (USEECINTERFACE == 1) {
			common.isApproximatelyEqualAssert({
				provideridname: '',
			}, qlRes.dataList[0]);
		} else {
			expect(qlRes.dataList[0]).not.to.have.property('provideridname');
		}

	});

	//需要补充ui验证
	//ui操作 同一批次所有款号的价格类型，都以输入最后一个款号使用的价格类型为标准
	it('170859.价格类型', async function () {
		let json = basiceJsonparam.salesJson(); //第一个款号为零批价
		json.details[1].styleid = BASICDATA.styleidAgc002;
		json.details[1].stdpricetype = 2; //另一款号，价格类型选择打包价
		json.details[1].price = 180;
		let sfRes = await common.editBilling(json);
		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);

		let param = format.qlParamsFormat({
			billno: sfRes.result.billno,
			shopid: LOGINDATA.invid
		}, true);
		let qlResult = await common.callInterface('ql-1209', param);
		qlResult.dataList.map((obj) => {
			if (obj.mat_code == 'agc001') assert.equal(obj.stdpricetype, '零批价');
			if (obj.mat_code == 'agc002') assert.equal(obj.stdpricetype, '打包价');
		});
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-23981
	it('SLH-23981', async function () {
		//新增货品
		let saveStyle = await basicInfoReqHandler.editStyle({ jsonparam: basiceJsonparam.addGoodJson() });
		let qlRes = await reqHandler.qlIFCHandler({ interfaceid: 'ql-1209', styleid: saveStyle.result.val });
		expect(qlRes.result.dataList.length, `销售开单-按明细查询新增的款号，查询有误，\n结果：${qlRes.result.sumrow}`).to.equal(0);
	});

	describe('170037.类型输入条件检查.rankA', async function () {
		let [json, param] = [{}, {}];
		before(function () {
			json = basiceJsonparam.simpleSalesJson();
			json.cash = -400;
			json.details[0].num = -2;
			param = format.qlParamsFormat({}, true);;
		});
		//退货时，typeid默认为1
		it('1.退货+备注不输入内容', async function () {
			let sfRes = await common.editBilling(json); //退货+退款
			param.billno = sfRes.result.billno; //更新billno
			param.typeid = 1;
			let qlRes = await common.callInterface('ql-1209', param);
			common.isApproximatelyEqualAssert({
				billno: sfRes.result.billno,
				typeid: '退货',
				rem: '',
			}, qlRes.dataList[0]);
		});
		//二代赠品改版，先不跑
		if (USEECINTERFACE == 1) {
			it('2.退货+设置typeid验证', async function () {
				TESTCASE = {
					describe: '销售开单-按明细查类型查询条件验证',
					jira: '二代赠品改版'
				}
				let json1 = _.cloneDeep(json);
				json1.details[0].typeid = 2;
				let sfRes = await common.editBilling(json1);
				param.billno = sfRes.result.billno; //更新billno
				param.typeid = 2;
				let qlRes = await common.callInterface('ql-1209', param);
				expect(qlRes.dataList[0], `退货+typeid=${param.typeid}出错\n结果为:${JSON.stringify(qlRes)}`).to.includes({
					billno: sfRes.result.billno,
				});
			});
		};
		it('3.170038.特殊货品检查', async function () {
			param = format.qlParamsFormat({
				styleid: BASICDATA.styleid00000,
				prodate1: common.getDateString([0, -1, 0]),
				typeid: 1, //特殊货品类型为退货
				search_list: 0,
				search_count: 1,
				search_sum: 0,
			});
			let qlRes = await common.callInterface('ql-1209', param);
			if (USEECINTERFACE == 1) {
				expect(Number(qlRes.count)).to.be.above(0);
			} else {
				expect(Number(qlRes.count)).to.be.equal(0);
			}
		});
	});


});
