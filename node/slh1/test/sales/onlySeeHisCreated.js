'use strict';

const common = require('../../../lib/common.js');
const caps = require('../../../data/caps');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 仅本人创建
 */

// 店长
let login004 = {
	logid: '004',
	pass: '000000',
};
// 开单员
let login005 = {
	logid: '005',
	pass: '000000',
};

describe('仅本人创建', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setDataPrivilege('boss', '-3');
	});
	after(async () => {
		await common.setDataPrivilege('boss', '-2');
	});

	it('170516,仅本人创建的客户', async function () {
		await common.loginDo(login004);
		let custRes1 = await salesReqHandler.addCust(basiceJsonparam.addCustJson()); //新增客户
		await common.loginDo(login005);
		let custRes2 = await salesReqHandler.addCust(basiceJsonparam.addCustJson()); //新增客户
		let custList = await common.callInterface('ql-1339', format.qlParamsFormat({
			'delflag': '0',
			'dwname': custRes1.params.nameshort,
		}, true));
		expect(custList, `仅本人创建的客户 错误`).to.includes({
			'count': '0'
		});
	});

	it('170517,只能查看本人创建的订货信息(按批次查、按明细查、销售开单-按订货开单)', async function () {
		// 店长登录 并开订货单
		await common.loginDo(login004);
		let orderResult1 = await common.editBilling(basiceJsonparam.salesOrderJson(), false); //新增销售订单
		expect(orderResult1.result, `仅本人创建，店长销售开单失败:${orderResult1.result}`).to.not.have.property('error');
		// 开单员登录并开订货单
		await common.loginDo(login005);
		let orderResult2 = await common.editBilling(basiceJsonparam.salesOrderJson(), false);
		expect(orderResult2.result, `仅本人创建，开单员销售开单失败:${orderResult2.result}`).to.not.have.property('error');

		// 开单员查询订单相关信息

		// 验证 销售订货按批次查
		let batchList1 = await common.callInterface('ql-14433', format.qlParamsFormat({
			'id1': orderResult1.result.billno,
			'id2': orderResult1.result.billno,
			'clientid': orderResult1.params.clientid,
			'shopid': orderResult1.params.shopid
		}, true));
		expect(batchList1, `ql-14433仅本人创建，开单员看到了店长开的订货单信息，错误`).to.includes({
			'count': '0'
		});
		let batchList2 = await common.callInterface('ql-14433', format.qlParamsFormat({
			'id1': orderResult2.result.billno,
			'id2': orderResult2.result.billno,
			'clientid': orderResult2.params.clientid,
			'shopid': orderResult2.params.shopid
		}, true));
		expect(batchList2, `ql-14433仅本人创建，开单员看到不到自己的订货单信息，错误`).to.includes({
			'count': '1'
		});
		// 按批次点进去不能有错
		let orderInfo = await common.callInterface('qf-14401-1', {
			'pk': batchList2.dataList[0].id
		});
		expect(orderInfo, `qf-14401-1仅本人创建，开单员查看自己开的订单信息出错`).to.not.have.property('error');

		// 验证销售订货按明细查
		let detailList1 = await common.callInterface('ql-14431', format.qlParamsFormat({
			'id1': orderResult1.result.billno,
			'id2': orderResult1.result.billno,
			'mainShopid': orderResult1.params.shopid,
			'mainClientid': orderResult1.params.clientid
		}, true));
		expect(detailList1, `ql-14431仅本人创建，开单员看到了店长开的订货单信息，错误`).to.includes({
			'count': '0'
		});
		let detailList2 = await common.callInterface('ql-14431', format.qlParamsFormat({
			'id1': orderResult2.result.billno,
			'id2': orderResult2.result.billno,
			'mainShopid': orderResult2.params.shopid,
			'mainClientid': orderResult2.params.clientid
		}, true));
		expect(Number(detailList2.count) > 0, `ql-14431仅本人创建，开单员看到不到自己的订货单信息，错误`).to.be.true;

		// 验证 按订货开单
		let salesByOrderList1 = await common.callInterface('ql-14223', format.qlParamsFormat({
			'clientid': orderResult1.params.clientid,
			'shopid': orderResult1.params.shopid,
			'orderno1': orderResult1.result.billno,
			'orderno2': orderResult1.result.billno,
		}, true));
		expect(salesByOrderList1, `ql-14223仅本人创建，开单员看到了店长开的订货单信息，错误`).to.includes({
			'count': '0'
		});
		let salesByOrderList2 = await common.callInterface('ql-14223', format.qlParamsFormat({
			'clientid': orderResult2.params.clientid,
			'shopid': orderResult2.params.shopid,
			'orderno1': orderResult2.result.billno,
			'orderno2': orderResult2.result.billno,
		}, true));
		expect(salesByOrderList2, `ql-14223仅本人创建，开单员看到不到自己的订货单信息，错误`).to.includes({
			'count': '1'
		});

	});

	it('170518,只能查看本人创建的开单信息(按批次查、按明细查)', async function () {
		// 店长登录并开单
		await common.loginDo(login004);
		let salesResult1 = await common.editBilling(basiceJsonparam.agencySalesJson(), false);
		expect(salesResult1.result, `仅本人创建，店长销售开单失败:${JSON.stringify(salesResult1.result)}`).to.not.have.property('error');

		// 开单员登录并开单
		await common.loginDo(login005);
		let salesResult2 = await common.editBilling(basiceJsonparam.agencySalesJson(), false);
		expect(salesResult2.result, `仅本人创建，开单员销售开单失败:${JSON.stringify(salesResult2.result)}`).to.not.have.property('error');

		// 开单员查询销售单相关信息

		// 验证 销售开单按批次查
		let batchList1 = await common.callInterface('ql-142201', format.qlParamsFormat({
			'billno1': salesResult1.result.billno,
			'billno2': salesResult1.result.billno,
			'dwid': salesResult1.params.dwid,
			'invid': salesResult1.params.shopid
		}, true));
		expect(batchList1, `ql-142201仅本人创建，开单员看到了店长的销售单信息，错误`).to.includes({
			'count': '0'
		});
		let batchList2 = await common.callInterface('ql-142201', format.qlParamsFormat({
			'billno1': salesResult2.result.billno,
			'billno2': salesResult2.result.billno,
			'dwid': salesResult2.params.dwid,
			'invid': salesResult2.params.shopid
		}, true));
		expect(batchList2, `ql-142201仅本人创建，开单员看不到自己的销售单信息，错误`).to.includes({
			'count': '1'
		});
		// 验证点进去明细信息
		let billInfo = await common.callInterface('qf-14211-1', {
			'pk': batchList2.dataList[0].id
		});
		expect(billInfo, `qf-14211-1仅本人创建，开单员看不到自己的销售单信息，错误`).to.not.have.property('error');

		// 验证 销售开单明细
		let detailList1 = await common.callInterface('ql-1209', format.qlParamsFormat({
			'billno': salesResult1.result.billno,
			'shopid': salesResult1.params.shopid,
			'dwid': salesResult1.params.dwid
		}, true));
		expect(detailList1, `ql-1209仅本人创建，开单员看到了店长的销售单信息，错误`).to.includes({
			'count': '0'
		});
		let detailList2 = await common.callInterface('ql-1209', format.qlParamsFormat({
			'billno': salesResult2.result.billno,
			'shopid': salesResult2.params.shopid,
			'dwid': salesResult2.params.dwid
		}, true));
		expect(Number(detailList2.count) > 0, `ql-1209仅本人创建，开单员看不到自己的销售单信息，错误`).to.be.true;
	});

});
