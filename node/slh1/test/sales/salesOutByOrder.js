'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

//按订货开单相关用例
//按订货开单 查看单据明细 接口为qf-14401-1 即销售订单查询
describe('销售开单-按订货开单-slh2', async function () {
	this.timeout(30000);
	let [orderSfRes, orderQfRes, orderQlParam] = [{}, {}, {}]; //销售订单sf,qf返回结果 按订货开单查询参数

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('ignorecolorsize', 0); //显示颜色尺码表
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
		await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单
	});

	it('170252.单据查询.rankA--mainLine', async function () {
		let json = basiceJsonparam.salesOrderJson();
		orderSfRes = await common.editBilling(json); //新增销售订单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQlParam = format.qlParamsFormat({
			styleid: orderSfRes.params.details[0].styleid,
			clientid: orderSfRes.params.clientid,
			orderno1: orderSfRes.result.billno,
			orderno2: orderSfRes.result.billno,
			shopid: orderSfRes.params.shopid,
			flag: 0, //发货状态 未发货
		}, true);
		await salesByOrderResAssert(orderQlParam, orderQfRes.result); //销售开单-按订货开单结果验证
	});

	//依赖170252
	// prepayid 预付款单据的pk值 没有预付款则为0
	it('170266.预付款单据检查', async function () {
		let res = await salesRequest.salesQueryBilling(orderSfRes.result.prepayid);
		let exp = _.cloneDeep(orderSfRes.params);
		exp.remark = '预付款';
		exp.totalnum = 0;
		exp.totalmoney = 0;
		delete exp.details;
		common.isApproximatelyEqualAssert(exp, res.result, ['actid']);
	});

	//依赖170252
	it('170261.部分发货.rankA', async function () {
		let json = _.cloneDeep(orderQfRes.result);
		json.details[0].num = 1;
		json.details[1].num = 2;
		let sfRes = await salesRequest.editSalesBillByOrder(json); //按订货开单 部分入库
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQlParam.flag = 1; //发货状态 部分发货
		await salesByOrderResAssert(orderQlParam, orderQfRes.result); //销售开单-按订货开单结果验证
	});

	//依赖170252 && 170261
	it('160172,【销售订单-按批次查】修改部分发货的单子', async function () {
		let param = {
			'pk': orderSfRes.result.pk
		};
		if (USEECINTERFACE == 2) param.isPend = 0;
		let orderInfo = await common.callInterface('qf-14401-1', param);

		orderInfo.action = 'edit';
		orderInfo.details[0].num = 1;
		delete orderInfo.details[0].total;
		delete orderInfo.totalnum;
		delete orderInfo.shouldpay;
		delete orderInfo.totalsum;
		orderInfo = format.jsonparamFormat(orderInfo);
		let result = await common.callInterface('sf-14401-1', {
			'jsonparam': orderInfo
		});
		expect(result, `修改部分发货的销售订单成功，但是按照用例应该修改失败`).to.includes({
			"error": "系统设定不许修改部分发货"
		});
	});

	//依赖170252
	it('170260.全部发货', async function () {
		orderQfRes.result.details[0].num = orderQfRes.result.details[0].restnum; //未发数
		orderQfRes.result.details[1].num = orderQfRes.result.details[1].restnum; //未发数
		let sfRes = await salesRequest.editSalesBillByOrder(orderQfRes.result); //按订货开单 全部入库
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQlParam.flag = 2; //发货状态 全部发货
		await salesByOrderResAssert(orderQlParam, orderQfRes.result); //销售开单-按订货开单结果验证

		let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
		let param = {
			billno1: qfRes.result.billno,
			billno2: qfRes.result.billno,
		};
		let qlRes = await common.callInterface('ql-142201', param); //销售开单-按批次查
		let exp = qfRes.result;
		exp.dwname = exp.show_dwid;
		exp.maininvname = exp.show_shopid;
		exp.seller = exp.show_deliver.split(',')[1];
		exp.billid = orderSfRes.result.billno; //ql-142201中的billid为订货号
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('170262.已终结的订单检查.rankA', async function () {
		orderSfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQfRes.result.details[0].num = 1;
		orderQfRes.result.details[1].num = 2;
		let sfRes = await salesRequest.editSalesBillByOrder(orderQfRes.result); //按订货开单 部分入库

		let finishRes = await salesOrderRequest.saleOrderFinish(orderSfRes.result.pk);

		let param = format.qlParamsFormat({
			orderno1: orderSfRes.result.billno,
			orderno2: orderSfRes.result.billno,
		}, true);
		let res = await common.callInterface('ql-14223', param); //销售开单-按订货开单查询
		//已终结的订单不能再显示在按订货开单界面
		expect(res.count, `按订货开单 查询到已终结订单 orderBillno=${orderSfRes.result.billno}`).to.equal('0');
	});

	//ui也需要验证
	it('170264.特殊货品金额计算', async function () {
		orderSfRes = await common.editBilling(basiceJsonparam.specialSalesOrderJson()); //新增销售订单 带特殊货品
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQfRes.result.details[0].num = 1;
		orderQfRes.result.details[1].num = 2;
		let sfRes = await salesRequest.editSalesBillByOrder(orderQfRes.result); //按订货开单 部分入库

		let qfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息
		qfRes.result.details.forEach(val => val.restnum = Number(val.restnum).toString());
		expect(qfRes.result).to.satisfy((obj) => {
			return obj.details[2].restnum == '0' && obj.details[3].restnum == '0'; //未发数
		}, `pk值为${orderSfRes.result.pk}的销售订货单，部分入库后，特殊货品的已发数为不等于0`);
	});
	//销售订货的flag没有更新，已报
	it('170265.核销预付款/170453.订货额,已付,未付检查.rankA', async function () {
		let json = basiceJsonparam.salesOrderJson();
		json.clientid = BASICDATA.dwidLs; //需要做核销操作 客户改为李四，小王的核销数据太多了
		orderSfRes = await common.editBilling(json); //新增销售订单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息
		let qfRes = await salesRequest.salesQueryBilling(orderSfRes.result.prepayid); //获取预付款单的批次号

		let verifyRes = await salesRequest.getVerifyingBills(orderSfRes.params.clientid, orderSfRes.params.shopid, false); //获取核销界面信息
		common.isApproximatelyEqualAssert({
			dwname: orderQfRes.result.show_clientid,
			remark: '预付款',
			cash: orderSfRes.params.cash,
			card: orderSfRes.params.card,
			remit: orderSfRes.params.remit,
			billno: qfRes.result.billno, //预付款批次号
		}, verifyRes.dataList[0]);

		orderQfRes.result.finpayVerifybillids = verifyRes.dataList[0].id;
		orderQfRes.result.finpayVerifysum = verifyRes.dataList[0].balance;
		let sfRes = await salesRequest.editSalesBillByOrder(orderQfRes.result, false); //全部入库+核销
		expect(sfRes.result, `核销预付款单失败 \nerror:${JSON.stringify(sfRes)}`).to.have.property('pk');

		//170453 核销后，预付款还是算在已付里面的
		//订货额=总计，已付=预付款，未付=订货额-已付
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //重新获取订单信息
		let param = format.qlParamsFormat({
			orderno1: orderQfRes.result.billno,
			orderno2: orderQfRes.result.billno,
			flag: 2, //发货状态 全部发货
		}, true);
		await salesByOrderResAssert(param, orderQfRes.result); //销售开单-按订货开单结果验证
	});

	it('170432.修改订货单数量-订货额,已付,未付检查', async function () {
		let json = basiceJsonparam.salesOrderJson();
		orderSfRes = await common.editBilling(json); //新增销售订货单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQfRes.result.details[0].num = Number(orderSfRes.params.details[0].num) + 2; //修改订货数
		orderQfRes.result.cash = Number(orderSfRes.params.cash) + 400;
		orderSfRes = await salesOrderRequest.editSalesOrderBill(orderQfRes.result);
		expect(orderSfRes.result, `修改订单出错 \nerror:${JSON.stringify(orderSfRes)}`).to.have.property('pk');

		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息
		orderQlParam.flag = 0; //发货状态 未发货
		orderQlParam.orderno1 = orderQfRes.result.billno;
		orderQlParam.orderno2 = orderQfRes.result.billno;
		await salesByOrderResAssert(orderQlParam, orderQfRes.result); //销售开单-按订货开单结果验证
	});
	//flag状态没有改变，已报
	it('170435.部分发货/全部发货后作废--订货额,已付,未付检查.rankA', async function () {
		orderSfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订货单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息
		let sfRes = await salesRequest.editSalesBillByOrder(orderQfRes.result); //全部入库
		await salesRequest.cancelSaleoutBill(sfRes.result.pk); //作废销售单
		orderQfRes = await salesOrderRequest.salesOrderQueryBilling(orderSfRes.result.pk); //获取订单信息

		orderQlParam.flag = 0; //作废按订货开单后 发货状态应该为未发货
		orderQlParam.orderno1 = orderQfRes.result.billno;
		orderQlParam.orderno2 = orderQfRes.result.billno;
		await salesByOrderResAssert(orderQlParam, orderQfRes.result); //销售开单-按订货开单结果验证
	});

	//这里最后把款号启用了，防止影响二代其他地方的用例
	it('170479.当日上架的款号昨天订货/170409.停用货品查询', async function () {
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson()
		}); //新增货品

		let json = basiceJsonparam.salesOrderJson();
		json.details[0].styleid = goodRes.pk;
		let sfRes = await common.editBilling(json); //新增销售订单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		sfRes = await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单

		let cancelRes = await common.callInterface('cs-1511-33', { //停用货品
			pk: goodRes.pk, //这里可能有错
		});
		expect(cancelRes, `停用货品失败 ${JSON.stringify(cancelRes)}`).to.includes({
			val: 'ok',
		});

		let param = format.qlParamsFormat({
			styleid: goodRes.pk,
		}, true);
		let res = await common.callInterface('ql-14223', param);
		expect(res.count, `按订货开单查询不到停用款号,pk值为${goodRes.pk}`).not.to.equal('0');
		await common.callInterface('cs-1511-32', { //启用货品
			pk: goodRes.pk,
		});
	});

	it('170482.按订货开单界面修改日期后再次检查开单日期', async function () {
		let json = basiceJsonparam.salesOrderJson();
		let sfRes = await common.editBilling(json); //新增销售订单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		qfRes.result.prodate = common.getDateString([0, 0, -1]); //开单日期改为昨天
		sfRes = await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单 全部入库

		qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
		expect(qfRes.result.prodate).to.equal(common.getDateString([0, 0, -1])); //验证销售单的日期
	});

	it('170272.开单日期检查', async function () {
		let json = basiceJsonparam.salesOrderJson();
		json.prodate = common.getDateString([0, 0, -1]);
		let sfResOrder = await common.editBilling(json); //新增昨天的销售订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);

		qfResOrder.result.prodate = common.getDateString([0, 0, 0]);
		let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单 全部入库
		let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
		// console.log(`qfResult = ${JSON.stringify(qfResult)}`);
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);
		expect(qfRes.result.prodate, `销售单日期为${qfRes.result.prodate}`).to.equal(common.getDateString([0, 0, 0])); //验证销售单的日期
		expect(qfResOrder.result.prodate, `销售订货单日期为${qfResOrder.result.prodate}`).to.equal(common.getDateString([0, 0, -1])); //验证销售订单的日期
	});

	it('170669.多发', async function () {
		if (USEECINTERFACE == 2) this.skip();   //二代这个不改  就这样设计的 和开发确认过
		let sfResOrder = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

		let delivernum = Number(qfResOrder.result.details[0].num) + 10;
		qfResOrder.result.details[0].num = delivernum; //多发10件
		let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单 全部入库
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息
		let param = format.qlParamsFormat({
			orderno1: qfResOrder.result.billno,
			orderno2: qfResOrder.result.billno,
			flag: 2, //发货状态 全部发货
		}, true);

		await salesByOrderResAssert(param, qfResOrder.result); //销售开单-按订货开单结果验证
		expect(qfResOrder.result.details[0].restnum, `按订货开单未发数错误`).to.equal('0'); //未发数
		expect(qfResOrder.result.details[0].delivernum, `按订货开单已发数错误`).to.equal(String(delivernum)); //已发数
	});

	it('170836.作废天数影响', async function () {
		await common.setGlobalParam('sales_invalidate_days', 0); //销售开单允许作废和修改天数 不能作废
		await common.setGlobalParam('sc_fixprodate', 0); //是否允许修改单据日期 不限制
		let json = basiceJsonparam.salesOrderJson();
		json.prodate = common.getDateString([0, 0, -1]);
		let sfRes = await common.editBilling(json); //新增昨天的销售订单

		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		qfRes.result.prodate = common.getCurrentDate();
		sfRes = await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单 全部入库 正常开单，不能报错
		qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
		expect(qfRes.result.prodate).to.equal(common.getCurrentDate()); //验证销售单的日期
		await common.setGlobalParam('sales_invalidate_days', 5); //销售开单允许作废和修改天数 只能作废5天
	});

	it('171027.删除一行后在该行位置上添加特殊货品并保存,差异数检查.rankA', async function () {
		if (USEECINTERFACE == 1) {
			let specResult = await common.callInterface('qf-1518-1', {
				pk: BASICDATA.styleid00000, //抹零
			});
			specResult.ratio = 0; //按订货开单 删除明细添加特殊货品  需要将最高比例改为0(不限制)
			delete specResult.flag;
			specResult.action = 'edit';
			let editSpec = await common.callInterface('sf-1518-1', {
				jsonparam: specResult,
				pk: specResult.pk
			});
			expect(editSpec, '修改特殊货品失败').to.have.property('val');
		};

		let json = basiceJsonparam.salesOrderJson();
		json.details.splice(1); //单明细
		let sfResOrder = await common.editBilling(json);
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);

		qfResOrder.result.details = [{
			"rem": "Spec1",
			"matCode": "00000",
			"price": 20,
			"num": -1,
			"colorid": "0",
			"sizeid": "0"
		}];
		delete qfResOrder.result.balance;
		let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result);

		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);
		qfResOrder.result.details[0].restnum = Number(qfResOrder.result.details[0].restnum).toString();
		expect(qfResOrder.result.details[0].restnum, '销售订单差异数错误').to.equal(json.details[0].num.toString());
	});

	//销售订单的订货数为[2,3]
	describe('170713.部分发货/全部发货单据 修改销售单', function () {
		it('1.选择部分发货的单子,输入超过订货数/与订货数相同保存', async function () {
			let sfRes = await getResFor170713([1, 2], [1, 1]);
			expect(sfRes.result, `按订货开单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		it('2.选择部分发货的单子,一个款全部发货,另外一个款部分发货,再输入发货数保存', async function () {
			let sfRes = await getResFor170713([2, 2], [0, 1]);
			expect(sfRes.result, `按订货开单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		it('3.选择全部发货的单子(已发数与订货数相等),输入数量点保存', async function () {
			let sfRes = await getResFor170713([2, 3], [1, 1]);
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货或已终结，不允许再发货',
			});
		});
		it('4.选择全部发货的单子(已发数大于订货数),输入数量点保存', async function () {
			let sfRes = await getResFor170713([4, 5], [1, 1]);
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货或已终结，不允许再发货',
			});
		});
	});

	//销售订单的订货数为[2,3]
	describe('170596.参数:允许修改已发货的订单-修改订货单验证.rankA', function () {
		before(async function () {
			await common.setGlobalParam('order_allow_edit_fully_deliver', 1); //是否允许修改已发货的订单 允许修改已发货的订单
		});
		after(async function () {
			await common.setGlobalParam('order_allow_edit_fully_deliver', 0); //是否允许修改已发货的订单 默认不允许
		});

		it('1.选择部分发货的订单，在修改界面增加新的款号和数量/将已发货款号的数量改得比发数大/随意修改未发货款号的数量,然后点保存', async function () {
			let sfRes = await getResFor170596([1, 0], (obj) => {
				obj.details[0].num = 3; //将已发货款号的数量改得比发数大
				obj.details[1].num = 2; //修改未发货款号的数量
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `修改销售订单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		//有bug，下面这条
		it('2.选择部分发货的订单,将已发货款号的数量改得比已发数小,再保存', async function () {
			let sfRes = await getResFor170596([1, 2], (obj) => {
				obj.details[0].num = 1;
				obj.details[1].num = 1;
			});
			//console.log(`sfRes.result : ${JSON.stringify(sfRes.result)}`);
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '[第2行][agc001]已发货，订货数不允许修改成比已发数小',
			});
		});
		it('160074.6.选择部分发货的订单,删除已发货的款号,点保存', async function () {
			let sfRes = await getResFor170596([1, 2], (obj) => {
				obj.details = []; //删除明细
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '已发货的明细不允许删除',
			});
		});
		it('160114.4.选择部分发货的订单,删除未发货的款号,点保存', async function () {
			let sfRes = await getResFor170596([2, 0], (obj) => {
				obj.details.splice(1); //删除未发货明细
			});
			expect(sfRes.result, `修改销售订单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		it('160074.7.选择部分发货的订单,将所有款号的订货数修改成和已发数一样,点保存', async function () {
			let sfRes = await getResFor170596([1, 2], (obj) => {
				obj.details[0].num = 1;
				obj.details[1].num = 2;
			});
			expect(sfRes.result, `修改销售订单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		it('160074.9.选择部分发货但预付款被部分或全部核销的订单,增加或修改款号订货数量后保存', async function () {
			let sfRes = await getResFor170596(async (pk) => {
				let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(pk); //获取订单信息
				let verifyRes = await salesRequest.getVerifyingBills(qfResOrder.result.clientid); //获取核销信息

				qfResOrder.result.finpayVerifybillids = verifyRes.dataList[0].id;
				qfResOrder.result.finpayVerifysum = verifyRes.dataList[0].balance;
				qfResOrder.result.details[0].num = 1;
				qfResOrder.result.details[1].num = 2;
				await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单 部分发货 核销预付款
			}, (obj) => {
				obj.details[0].num = 5; //修改订货数量
				obj.details[1].num = 5; //修改订货数量
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单预付款被核销，不允许修改',
			});
		});

		it('3.选择全部发货的订单,将数量改得比已发数大,然后点保存', async function () {
			let sfRes = await getResFor170596([2, 3], (obj) => {
				obj.details[0].num = 5;
				obj.details[1].num = 6;
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货，不允许修改',
			});
		});
		it('4.选择全部发货的订单,将数量改得比已发数小,然后点保存', async function () {
			let sfRes = await getResFor170596([2, 3], (obj) => {
				obj.details[0].num = 1;
				obj.details[1].num = 2;
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货，不允许修改',
			});
		});
		it('160074.3.选择一条全部发货的款号,删除款号/增加款号,点保存', async function () {
			let sfRes = await getResFor170596([2, 3], (obj) => {
				obj.details = [{ //删除已有款号/添加款号
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				}];
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货，不允许修改',
			});
		});

		it('160074.2.选择一条已结束的订单,修改数量/增加款号,点保存', async function () {
			let sfRes = await getResFor170596(async (pk) => {
				await salesOrderRequest.saleOrderFinish(pk); //终结订单
			}, (obj) => {
				obj.details[0].num = 5; //修改数量
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '已结束的订单不允许修改',
			});
		});
		it('160074.10.选择一条已作废的订单,增加款号点保存', async function () {
			let sfRes = await getResFor170596(async (pk) => {
				await salesOrderRequest.cancelSaleorderBill(pk); //作废订单
			}, (obj) => {
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已作废，不允许修改',
			});
		});
	});

	//销售订单的订货数为[2,3]
	describe('160088.颜色尺码+不允许修改已发货的订单', function () {
		before(async () => {
			await common.setGlobalParam('order_allow_edit_fully_deliver', 0); //是否允许修改已发货的订单 默认不允许
		});

		it('2.选择部分发货的订单,修改订货数/增加款号,点保存', async function () {
			let sfRes = await getResFor170596([1, 2], (obj) => {
				obj.details[0].num = 4; //修改订货数
				obj.details[1].num = 5; //修改订货数
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '系统设定不许修改部分发货',
			});
		});
		it('3.选择未发货的订单,修改订货数/增加款号,点保存', async function () {
			let sfRes = await getResFor170596(undefined, (obj) => {
				obj.details[0].num = 4; //修改订货数
				obj.details[1].num = 5; //修改订货数
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `修改销售订单失败 error:${JSON.stringify(sfRes)}`).to.have.property('pk');
		});
		it('4.修改全部发货的订单,修改订货数/增加款号,点保存', async function () {
			let sfRes = await getResFor170596([2, 3], (obj) => {
				obj.details[0].num = 4; //修改订货数
				obj.details[1].num = 5; //修改订货数
				obj.details.push({ //增加新的款号和数量
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				});
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '订单已全部发货，不允许修改',
			});
		});

		it('160114.2.选择终结的订单,删除/增加款号,点保存', async function () {
			let sfRes = await getResFor170596(async (pk) => {
				await salesOrderRequest.saleOrderFinish(pk); //终结订单
			}, (obj) => {
				obj.details = [{
					"num": 4,
					"sizeid": "XL",
					"matCode": "Agc001",
					"rem": "明细3",
					"colorid": "BaiSe",
					"price": 200,
					"discount": 1,
				}];
			});
			expect(sfRes.result, `error:${JSON.stringify(sfRes)}`).to.includes({
				error: '已结束的订单不允许修改',
			});
		});
	});

	//销售订单订货数2
	describe('170908.按订货开单的销售单允许修改，同时更新订单状态.rankA', function () {
		before(async () => {
			await common.setGlobalParam('modify_salebill_by_order', 0); //允许修改,同时更新订单状态
		});
		it('3.全部发货,按批次查界面修改数量小于订货数', async function () {
			await checkFor170908(2, async (json) => {
				json.details[0].num = 1;
				let sfRes = await common.editBilling(json, false); //修改按订货开单生成的销售单
				return sfRes;
			});
		});
		it('4.部分发货,在销售开单-按批次查界面修改数量大于/等于订货数', async function () {
			await checkFor170908(1, async (json) => {
				json.details[0].num = 3;
				let sfRes = await common.editBilling(json, false); //修改按订货开单生成的销售单
				return sfRes;
			});
		});
		//系统参数：	sales_allowZeroNum 控制当数量为0时能否保存，二代还没有这个参数，这是客户端参数
		// ipad上，二代保存数量为0的单据会提示第[1]行数值型错误
		//接口开数量为0的单据会报空单不允许保存，二代先不跑用例
		if (USEECINTERFACE == 1) {
			it('5.全部发货,在销售开单-按批次查界面修改数量全部为0', async function () {
				await checkFor170908(2, async (json) => {
					json.details[0].num = 0;
					let sfRes = await common.editBilling(json, false); //修改按订货开单生成的销售单
					return sfRes;
				});
			});
		};
		it('6.全部发货,在销售开单-按批次查界面作废该单据', async function () {
			await checkFor170908(2, async (json) => {
				let cancelRes = await salesRequest.cancelSaleoutBill(json.pk); //作废单据
				json.details[0].num = 0; //作废后发货数变成0
				return {
					result: cancelRes
				};
			});
		});
	});

	//adev1提示语为订单已全部发货或已终结，不允许再发货 等待服务端解决
	describe('170909.按订货开单的销售单不允许修改,必须作废后重新按订货开单.rankA', function () {
		before(async () => {
			await common.setGlobalParam('modify_salebill_by_order', 1); //不允许修改,必须作废后重新按订货开单
		});
		after(async () => {
			await common.setGlobalParam('modify_salebill_by_order', 0); //允许修改,同时更新订单状态
		});
		it('3.全部发货,再在销售开单-按批次查界面修改数量小于订货数', async function () {
			await checkFor170908(2, async (json) => {
				json.details[0].num = 1;
				let sfRes = await common.editBilling(json, false); //修改按订货开单生成的销售单
				return sfRes;
			}, `参数:"按订货开单的销售单是否允许修改"为"不允许"`);
		});
		it('4.部分发货,然后再销售订货-按批次查终结该订单,在销售开单-按批次查界面修改该数量', async function () {
			await checkFor170908(1, async (json, sfResOrder) => {
				let finishRes = await salesOrderRequest.saleOrderFinish(sfResOrder.result.pk); //终结订单

				json.details[0].num = 2;
				let sfRes = await common.editBilling(json, false); //修改按订货开单生成的销售单
				return sfRes;
			}, `参数:"按订货开单的销售单是否允许修改"为"不允许"`);
		});
	});

	describe('其他角色验证', function () {
		after(async function () {
			await common.loginDo();
		});
		//中洲店角色开单 常青店配货员按订货开单验证
		it('170568.配货员按订货开单将别的门店的订货单进行开单验证', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			let sfResOrder = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
			let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

			await common.loginDo({ //常青店配货员登陆
				'logid': '007',
				'pass': '000000',
			});
			let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单
			qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

			let param = format.qlParamsFormat({
				orderno1: qfResOrder.result.billno,
				orderno2: qfResOrder.result.billno,
				shopid: BASICDATA.shopidZzd, //中洲店
				flag: 2, //发货状态 全部发货
			}, true);
			//若查询出错,需要检查后台角色是否设置正确
			await salesByOrderResAssert(param, qfResOrder.result); //销售开单-按订货开单结果验证

			let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
			param = format.qlParamsFormat({
				billno1: qfRes.result.billno,
				billno2: qfRes.result.billno,
				shopid: BASICDATA.shopidZzd, //中洲店
			}, true);
			let qlResult = await common.callInterface('ql-142201', param); //销售开单-按批次查
			expect(qlResult.dataList[0]).to.includes({
				billno: qfRes.result.billno,
				maininvname: '中洲店',
				billid: qfResOrder.result.billno,
			});
		});
	});

	//按库存 为否时,显示全部订货单,包括全部发货,部分发货和未发货.
	//按库存 为是时,只显示订单里部分款号或全部款号存在库存大于0的订单.订单里全部款号对应的库存都小于0的订单不会在按订货开单界面显示
	describe('查询条件-按库存 验证', function () {
		let qlParam;
		before(async function () {
			qlParam = format.qlParamsFormat({
				byinvnum: 1, //按库存 0否 1是
			}, true);
		});
		it('170763.1 没有正库存.rankA', async function () {
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			}); //新增货品 库存为0
			let json = basiceJsonparam.salesOrderJson();
			json.details[0].styleid = goodRes.pk;
			json.details.splice(1); //只用新增的货品开单
			let sfRes = await common.editBilling(json); //新增销售订单

			qlParam.byinvnum = 1; //按库存 是
			qlParam.orderno1 = sfRes.result.billno;
			qlParam.orderno2 = sfRes.result.billno;
			let result = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单
			expect(result.count, `按库存为是时,显示库存不大于的订单`).to.equal('0');

			qlParam.byinvnum = 0; //按库存 否
			result = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单
			expect(result.count, `按库存为否时,找不到库存为0的订单`).to.equal('1');
		});
		it('170763.2 正库存.rankA', async function () {
			//agc001需要保证正库存
			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());

			qlParam.byinvnum = 0; //按库存 否
			qlParam.orderno1 = sfRes.result.billno;
			qlParam.orderno2 = sfRes.result.billno;
			let result = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单
			expect(result.count, `按库存为否时,找不到批次号为${sfRes.result.billno}的订单`).to.equal('1');

			qlParam.byinvnum = 1; //按库存 是
			result = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单
			expect(result.count, `按库存为是时,找不到批次号为${sfRes.result.billno}的订单`).to.equal('1');

			let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
			sfRes = await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单 全部入库

			result = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单

			expect(result.count, `按库存为是时,不显示全部入库的订单`).to.equal('0');
		});
	});

});

// 根据销售订货qf接口返回值，验证销售开单-按订货开单结果
// qlParam 销售开单-按订货开单ql-14223 查询参数
// 未涉及 dwfdname:客户分店
async function salesByOrderResAssert(qlParam, qfResult) {
	let res = await common.callInterface('ql-14223', qlParam); //销售开单-按订货开单
	expect(res.dataList.length, `按订货开单ql-14223,查询无结果`).to.above(0);

	//根据销售订货qf接口返回值，拼接按订货开单查询结果的期望值
	let [delivernum, restnum, diffnum] = [0, 0, 0];
	let sendflagValue = ['未发货', '部分发货', '全部发货', '结束'];
	qfResult.details.map((obj) => {
		delivernum += Number(obj.delivernum || 0);
		restnum += Number(obj.restnum || 0);
	});
	//console.log(`qfResult : ${JSON.stringify(qfResult)}`);
	let exp = {
		id: qfResult.pk,
		invalidflag: qfResult.invalidflag,
		shopname: qfResult.show_invid,
		orderno: qfResult.billno,
		dwxxname: qfResult.show_clientid,
		sellername: USEECINTERFACE == 1 ? qfResult.show_sellerid : _.last(qfResult.show_sellerid.split(',')),
		totalnum: qfResult.totalnum, //订货数
		totalsum: qfResult.totalsum, //订货额
		optime: qfResult.optime,
		receiptsum: USEECINTERFACE == 1 ? qfResult.receiptsum : qfResult.prepay, //已付额
		delivernum: delivernum, //已发数
		restnum: restnum, //未发数
		diffnum: qfResult.totalnum - delivernum, //差异数
		sendflagid: qfResult.flag,
		sendflag: sendflagValue[qfResult.flag],
	};
	// console.log(`exp : ${JSON.stringify(exp)}`);
	// console.log(`res.dataList[0] : ${JSON.stringify(res.dataList[0])}`);
	if (USEECINTERFACE == 2) delete exp.optime;
	common.isApproximatelyEqualAssert(exp, res.dataList[0]);
};

//用例170713验证 修改按订货开单的单据 返回第二次按订货开单的销售单结果
//销售订单为basiceJsonparam.salesOrderJson 订货数为[2,3]
//arr1 第一次按订货开单
//arr2 第二次按订货开单
async function getResFor170713(arr1, arr2) {
	let sfResOrder = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
	let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息
	qfResOrder.result.details[0].num = arr1[0];
	qfResOrder.result.details[1].num = arr1[1];
	let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单
	qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

	qfResOrder.result.details[0].num = arr2[0];
	qfResOrder.result.details[1].num = arr2[1];
	sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result, false); //按订货开单
	return sfRes;
};

/*
 * 用例170596验证 修改订货单 返回修改订货单的结果
 * 销售订单为basiceJsonparam.salesOrderJson 订货数为[2,3]
 * operate1 array:按订货开单入库数 / function:修改订货单(终结/作废等)
 * operate2 修改订单明细
 */
async function getResFor170596(operate1, operate2) {
	let sfResOrder = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单

	let qfResOrder;
	if (typeof (operate1) == 'function') {
		await operate1(sfResOrder.result.pk); //作废,终结订单等操作
	} else if (typeof (operate1) == 'object') {
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息
		qfResOrder.result.details[0].num = operate1[0];
		qfResOrder.result.details[1].num = operate1[1];
		await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单
	};

	qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //重新获取订单信息
	operate2(qfResOrder.result); //修改销售订单明细
	sfResOrder = await salesOrderRequest.editSalesOrderBill(qfResOrder.result, false); //修改订单
	// console.log(`sfResOrder = ${JSON.stringify(sfResOrder)}`);
	return sfResOrder;
};

//用例170908/170909验证 销售订单全部入库后 修改生成的销售单 返回修改结果
async function checkFor170908(num, func, errorMsg = '') {
	let json = basiceJsonparam.salesOrderJson();
	json.details.splice(1); //只需要一条明细
	let sfResOrder = await common.editBilling(json); //新增销售订单
	let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

	let orderNum = Number(qfResOrder.result.details[0].num);
	qfResOrder.result.details[0].num = num; //按订货入库数
	let sfRes = await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单

	let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
	let jsonparam = qfRes.result;
	jsonparam.action = 'edit';
	jsonparam.interfaceid = 'sf-14211-1';
	delete jsonparam.details[0].invnum;

	sfRes = await func(jsonparam, sfResOrder); //修改按订货开单生成的销售单
	if (errorMsg != '') {
		expect(sfRes.result).to.includes({
			error: errorMsg
		});
	} else {
		qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //重新获取订单信息
		let showFlagValue = ['未发货', '部分发货', '全部发货']; //, '结束'
		let sendflag = jsonparam.details[0].num == 0 ? '0' : jsonparam.details[0].num < orderNum ? '1' : '2';
		expect(qfResOrder.result).to.includes({
			flag: sendflag,
			showFlag: showFlagValue[sendflag],
		});
		let param = format.qlParamsFormat({
			orderno1: sfResOrder.result.billno,
			orderno2: sfResOrder.result.billno,
			shopid: sfResOrder.result.invid
		}, true);
		await salesByOrderResAssert(param, qfResOrder.result); //销售开单-按订货开单结果验证
	};
};
