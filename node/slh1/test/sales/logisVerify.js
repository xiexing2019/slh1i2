'use strict';

const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const logisVerifyHelp = require('../../help/salesHelp/logisVerifyHelp.js');

/*
 * 物流核销
 */

describe('物流核销--mainLine', function () {
	this.timeout(30000);

	let logisBillno = '000000';

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2);
	});

	it.skip('170287,物流商核销基础验证.rankA', async function () {
		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doLogisVerify(); //开代收单，并物流核销
		let verifyJson = agencyVerify.verifyJson;
		let jsonparam = agencyVerify.salesJsonparam;

		// 验证物流单详情
		let verifyInfo = await common.callInterface('qf-1432-4', {
			'pk': verifyJson.logisVerifybillids
		});

		common.isApproximatelyEqualAssert({
			'agency': verifyJson.logisVerifysum,
			'logisDwid': verifyJson.dwid,
			'show_shopid': LOGINDATA.depname,
			'logisBillno': jsonparam.logisBillno,
			'prodate': common.getCurrentDate(),
			'finpayAgency': verifyJson.logisVerifysum,
			'shopid': LOGINDATA.invid,
			'dwid': jsonparam.dwid,
			'logisRem': jsonparam.logisRem
		}, verifyInfo);

		// 验证物流单列表
		let searchVerifyList = {
			'dwid': jsonparam.dwid,
			'logisDwid': verifyJson.dwid,
			'logisBillno': jsonparam.logisBillno,
			'id1': verifyJson.billno,
			'id2': verifyJson.billno
		};
		let verifyList = await common.callInterface('ql-1432', format.qlParamsFormat(searchVerifyList, true));
		common.isApproximatelyEqualAssert({
			'agencyflag': '是',
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': verifyJson.dwid,
			'logisbillno': jsonparam.logisBillno,
			'invalidflag': '0',
			'dwxxid': jsonparam.dwid,
			'finpayagency': verifyJson.logisVerifysum,
			'logispayflagid': '1', //货款收讫  覆盖用例170290
			'logispayflag': '是' //货款收讫
		}, verifyList.dataList[0])

	});

	it('170287,物流商核销.rankA', async function () {
		// 核销前 查询综合汇总、综合收支表
		let summaryList = await common.callInterface('ql-1623', format.qlParamsFormat({
			'invid': LOGINDATA.invid
		}, true));

		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doLogisVerify('物流商核销170287');
		let verifyJson = agencyVerify.verifyJson;
		let verifyResult = agencyVerify.verifyResult;

		// 验证 ‘代收收款’ 列表
		await logisVerifyHelp.collectingPaymentCheck(verifyJson, verifyResult);

		// 验证 ‘收支流水’ 列表
		await logisVerifyHelp.inoutRecCheck(verifyJson, verifyResult);

		// 验证 ‘综合汇总’、‘综合收支表’ 列表
		let summaryListAft = await logisVerifyHelp.summaryListCheck(verifyJson, verifyResult);
		let expectedCollection = Number(summaryList.dataList[0].collection) + Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0);
		common.isApproximatelyEqualAssert({
			'collection': expectedCollection,
		}, summaryListAft.summaryList.dataList[0]);
	});

	it('170288,物流商核销多种支付方式.rankA', async function () {
		// 核销前 查询综合汇总、综合收支表
		let summaryList = await common.callInterface('ql-1623', format.qlParamsFormat({
			'invid': LOGINDATA.invid
		}, true));

		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doLogisVerify('核销加多种支付方式', true);
		let verifyJson = agencyVerify.verifyJson;
		let verifyResult = agencyVerify.verifyResult;

		// 验证 ‘代收收款’ 列表
		await logisVerifyHelp.collectingPaymentCheck(verifyJson, verifyResult);

		// 验证 ‘收支流水’ 列表
		await logisVerifyHelp.inoutRecCheck(verifyJson, verifyResult);

		// 验证 ‘综合汇总’、‘综合收支表’ 列表
		let summaryListAft = await logisVerifyHelp.summaryListCheck(verifyJson, verifyResult);
		let expectedCollection = Number(summaryList.dataList[0].collection) + Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0);
		common.isApproximatelyEqualAssert({
			'collection': expectedCollection,
		}, summaryListAft.summaryList.dataList[0]);
	});

	it('170569,物流商核销+多种支付方式+特殊货品.rankA', async function () {
		// 核销前 查询综合汇总、综合收支表
		let summaryList = await common.callInterface('ql-1623', format.qlParamsFormat({
			'invid': LOGINDATA.invid
		}, true));

		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doVarietyBillLogisVerify('核销加特殊货品加多种支付方式', true);
		let verifyJson = agencyVerify.verifyJson;
		let verifyResult = agencyVerify.verifyResult;

		// 验证 ‘代收收款’ 列表
		await logisVerifyHelp.collectingPaymentCheck(verifyJson, verifyResult);

		// 验证 ‘收支流水’ 列表
		await logisVerifyHelp.inoutRecCheck(verifyJson, verifyResult);

		// 验证 ‘综合汇总’、‘综合收支表’ 列表
		let summaryListAft = await logisVerifyHelp.summaryListCheck(verifyJson, verifyResult);
		let expectedCollection = Number(summaryList.dataList[0].collection) + Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0);
		expect(summaryListAft.summaryList.dataList[0], `综合汇总列表代收收款列有误`).to.includes({
			'collection': expectedCollection.toString()
		});
	});

	it('170293,作废代收收款单/物流核销单', async function () {
		// 查询条件
		let inoutRecSearch = {
			'accountid': BASICDATA[`cashAccount${LOGINDATA.invid}`],
			'accountInvid': LOGINDATA.invid,
			'billtype': '80'
		};
		let summaryListSearch = {
			'invid': LOGINDATA.invid
		};
		let summaryDetSearch = {
			'shopid': LOGINDATA.invid,
			'prodate': common.getCurrentDate()
		};

		// 开单-->核销-->作废前 查询 收支流水、综合汇总、综合汇总明细
		let inoutRecList = await common.callInterface('ql-1352', format.qlParamsFormat(inoutRecSearch, true));
		let summaryList = await common.callInterface('ql-1623', format.qlParamsFormat(summaryListSearch, true));
		let summaryDetail = await common.callInterface('ql-16235', format.qlParamsFormat(summaryDetSearch));

		//执行 开单-->核销-->作废
		let agencyVerify = await logisVerifyHelp.doVarietyBillLogisVerify('核销加特殊货品加多种支付方式', true);
		let verifyJson = agencyVerify.verifyJson;
		let verifyResult = agencyVerify.verifyResult;
		let deleteResult = await common.callInterface('cs-cancel-1442', {
			'pk': verifyResult.pk
		})
		expect(deleteResult, `作废代收收款单失败`).to.not.have.property('error');

		// 开单-->核销-->作废后 开始验证以下6项目

		// 1、验证代收收款查询的状态应该为作废
		let agencyVerifySearch = {
			'dwid': verifyJson.dwid,
			'id1': verifyResult.billno,
			'id2': verifyResult.billno,
			'shopid': LOGINDATA.invid,
			'invalidflag': '1'
		};
		let agencyVerifyList = await common.callInterface('ql-1442', format.qlParamsFormat(agencyVerifySearch, true));
		common.isApproximatelyEqualAssert({
			'invalidflag': '1',
			'remit': (verifyJson.remit || 0).toString(),
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'verifysum': verifyJson.logisVerifysum.toString(),
			'totalsum': (Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0)).toString(),
			'totalmoney': (Number(verifyJson.logisVerifysum) + (Number(verifyJson.totalmoney) || 0)).toString(),
			'othercost': (Number(verifyJson.totalmoney) || 0).toString(),
			'cash': (verifyJson.cash || 0).toString(),
			'card': (verifyJson.card || 0).toString(),
			'billno': verifyResult.billno
		}, agencyVerifyList.dataList[0]);

		// 2、验证 代收单选择列表 被核销的代收单又重新显示
		let logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': agencyVerify.salesJsonparam1.logisDwid,
			'shopid': LOGINDATA.invid,
			'dwid': agencyVerify.salesJsonparam1.dwid
		}, true));
		let isHaveBillno1 = false,
			isHaveBillno2 = false;
		for (let i = 0; i < logisChooseList.dataList.length; i++) {
			if (logisChooseList.dataList[i].billno == agencyVerify.salesResult1.billno) {
				isHaveBillno1 = true;
			}
			if (logisChooseList.dataList[i].billno == agencyVerify.salesResult2.billno) {
				isHaveBillno2 = true;
			}
			if (isHaveBillno1 && isHaveBillno2) {
				break;
			}
		}
		expect(isHaveBillno1, `作废代收收款单后，代收单选择列表中 被核销的代收单没有重新显示`).to.be.true;
		expect(isHaveBillno2, `作废代收收款单后，代收单选择列表中 被核销的代收单没有重新显示`).to.be.true;

		// 3、验证物流单 货款收讫应该为否(即未核销物流单)
		let searchVerifyList = {
			'dwid': agencyVerify.salesJsonparam1.dwid,
			'logisDwid': verifyJson.dwid,
			'id1': agencyVerify.salesResult1.billno,
			'id2': agencyVerify.salesResult2.billno
		};
		let verifyList = await common.callInterface('ql-1432', format.qlParamsFormat(searchVerifyList, true));
		expect(verifyList.dataList[0], `作废代收收款单后，物流单货款收讫状态有误`).to.includes({
			'logispayflagid': '0',
			'logispayflag': '否'
		});
		expect(verifyList.dataList[1], `作废代收收款单后，物流单货款收讫状态有误`).to.includes({
			'logispayflagid': '0',
			'logispayflag': '否'
		});

		// 4、验证收支流水列表去掉物流核销时对应的流水单据
		let inoutRecListAft = await common.callInterface('ql-1352', format.qlParamsFormat(inoutRecSearch, true));
		common.isApproximatelyEqualAssert(inoutRecListAft, inoutRecList);

		// 5、验证综合汇总 代收收款列去掉作废的代收款项
		let summaryListAft = await common.callInterface('ql-1623', format.qlParamsFormat(summaryListSearch, true));
		expect(summaryListAft.dataList[0].collection, `作废代收收款单后，综合汇总列表代收收款值有误`).to.be.equal(summaryList.dataList[0].collection);

		// 6、验证综合汇总明细 代收收款条目去掉作废的代收数据
		let summaryDetailAft = await common.callInterface('ql-16235', format.qlParamsFormat(summaryDetSearch));
		let agencyMoneyList = [],
			isRight = true;
		for (let i = 0; i < summaryDetail.dataList.length; i++) {
			if (summaryDetail.dataList[i].catname == '代收收款') {
				agencyMoneyList.push(summaryDetail.dataList[i]);
			}
		}
		for (let i = 0; i < agencyMoneyList.length; i++) {
			isRight = isRight && common.isArrayContainObject(summaryDetailAft.dataList, agencyMoneyList[i]);
			if (!isRight) break;
		}
		expect(isRight, `作废代收收款后，综合汇总明细有误`).to.be.true;
	});

	it('170472,物流商代收应收款验证.rankA', async function () {
		let agencyInfo = await common.callInterface('cs-getlogisangecysum', {
			'shopid': LOGINDATA.invid,
			'dwid': BASICDATA.dwidSFKD
		});

		let agencyList = await common.callInterface('ql-14521', {
			'logisDwid': BASICDATA.dwidSFKD,
			'shopid': LOGINDATA.invid,
			'pagesize': '0',
			'logisPayflag': '0', //二代必传
		});
		let sumAgency = 0;
		for (let i = 0; i < agencyList.dataList.length; i++) {
			sumAgency += Number(agencyList.dataList[i].finpayagency);
		}
		expect(Number(agencyInfo.agency), `物流商代收应收金额有误`).to.be.equal(sumAgency);
	});

	it('170441/170425,童装模式-待核销的物流单/物流单列表', async function () {
		if (USEECINTERFACE == 2) this.skip();
		await common.setGlobalParam('paymethod', 10);

		let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod10());
		let salesResult = await common.editBilling(jsonparam);

		// 代收单选择列表
		let logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': jsonparam.logisDwid,
			'shopid': LOGINDATA.invid,
			'dwid': jsonparam.dwid
		}, true));
		let isRight = false,
			choosed;
		for (let i = 0; i < logisChooseList.dataList.length; i++) {
			if (logisChooseList.dataList[i].billno == salesResult.result.billno) {
				choosed = logisChooseList.dataList[i];
				isRight = true;
				break;
			}
		}
		expect(isRight, `待核销物流单列表有误`).to.be.true;
		expect(choosed, `待核销物流单列表有误:${JSON.stringify(choosed)}`).to.includes({
			'agencyflag': '是',
			'logispayflagid': '0',
			'logispayflag': '否',
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': jsonparam.logisDwid,
			'finpayagency': jsonparam.agency.toString(),
			'dwxxid': jsonparam.dwid,
			'billno': salesResult.result.billno
		});

		// 验证物流单列表
		let searchVerifyList = {
			'dwid': jsonparam.dwid,
			'logisDwid': jsonparam.logisDwid,
			'logisBillno': jsonparam.logisBillno,
			'id1': salesResult.result.billno,
			'id2': salesResult.result.billno
		};
		let verifyList = await common.callInterface('ql-1432', format.qlParamsFormat(searchVerifyList, true));
		expect(verifyList.dataList[0], `物流单列表有误`).to.includes({
			'agencyflag': '是',
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': jsonparam.logisDwid,
			'logisbillno': jsonparam.logisBillno.toString(),
			'invalidflag': '0',
			'dwxxid': jsonparam.dwid,
			'finpayagency': jsonparam.agency.toString(),
			'billno': salesResult.result.billno,
			'logispayflagid': '0',
			'logispayflag': '否',
			'logisrem': jsonparam.logisRem
		});

		await common.setGlobalParam('paymethod', 2);
	});

	it('170296,代收收款单组合查询条件验证', async function () {
		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doLogisVerify();
		let verifyJson = agencyVerify.verifyJson;
		let verifyResult = agencyVerify.verifyResult;

		// 代收收款查询
		let agencyVerifySearch = {
			'dwid': verifyJson.dwid,
			'id1': verifyResult.billno,
			'id2': verifyResult.billno,
			'shopid': LOGINDATA.invid,
			'invalidflag': '0'
		};
		let agencyVerifyList = await common.callInterface('ql-1442', format.qlParamsFormat(agencyVerifySearch, true));
		common.isApproximatelyEqualAssert({
			'remit': (verifyJson.remit || 0).toString(),
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'verifysum': verifyJson.logisVerifysum.toString(),
			'totalsum': verifyJson.logisVerifysum.toString(),
			'cash': (verifyJson.cash || 0).toString(),
			'card': (verifyJson.card || 0).toString(),
			'billno': verifyResult.billno
		}, agencyVerifyList.dataList[0]);
		agencyVerifySearch.invalidflag = '1';
		agencyVerifyList = await common.callInterface('ql-1442', format.qlParamsFormat(agencyVerifySearch, true));
		expect(agencyVerifyList, `代收收款列表有误`).to.includes({
			'count': '0'
		});

	});

	// 核销时加了特殊货品
	it('170300/170410,代收收款列表及其详细信息验证', async function () {
		// 开代收单
		let salesResult1 = await common.editBilling(basiceJsonparam.agencySalesJson());
		let salesResult2 = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 代收单选择列表
		let logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': salesResult1.params.logisDwid,
			'shopid': LOGINDATA.invid,
			'dwid': salesResult1.params.dwid
		}, true));
		if (USEECINTERFACE == 1) {
			expect(logisChooseList.dataList[0].id, `代收单选择列表默认排序有问题,最新的代收单应该默认排在最前面`).to.be.equal(salesResult2.result.pk);
			expect(logisChooseList.dataList[1].id, `代收单选择列表默认排序有问题,最新的代收单应该默认排在最前面`).to.be.equal(salesResult1.result.pk);
		} else {
			expect(logisChooseList.dataList[0].billno, `代收单选择列表默认排序有问题,最新的代收单应该默认排在最前面`).to.be.equal(salesResult2.result.billno);
			expect(logisChooseList.dataList[1].billno, `代收单选择列表默认排序有问题,最新的代收单应该默认排在最前面`).to.be.equal(salesResult1.result.billno);
		};


		// 物流商核销
		let verifyJson = {
			'card': 1,
			'remit': 1,
			'logisVerifybillids': `${logisChooseList.dataList[0].id},${logisChooseList.dataList[1].id}`,
			'logisVerifysum': Number(logisChooseList.dataList[0].finpayagency) + Number(logisChooseList.dataList[1].finpayagency),
			'remark': '物流商核销',
			'dwid': logisChooseList.dataList[0].logisdwxxid
		};
		verifyJson = format.verifyJsonFormat(verifyJson, true);
		let verifyResult = await common.callInterface('sf-1452', {
			jsonparam: verifyJson,
		});
		expect(verifyResult, `物流商核销失败`).to.not.have.property('error');

		// 代收收款查询
		let agencyVerifySearch = {
			'dwid': verifyJson.dwid,
			'id1': verifyResult.billno,
			'id2': verifyResult.billno,
			'shopid': LOGINDATA.invid
		};
		let agencyVerifyList = await common.callInterface('ql-1442', format.qlParamsFormat(agencyVerifySearch, true));
		for (let key in agencyVerifyList.dataList[0]) {
			if (key != 'shopname' && key != 'prodate') {
				agencyVerifyList.dataList[0][key] = Number(agencyVerifyList.dataList[0][key]).toString();
			};
		};

		expect(agencyVerifyList.dataList[0], `代收收款列表有误`).to.includes({
			'remit': (verifyJson.remit || 0).toString(),
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'totalmoney': (Number(verifyJson.totalmoney) + Number(verifyJson.logisVerifysum)).toString(),
			'verifysum': verifyJson.logisVerifysum.toString(),
			'totalsum': (Number(verifyJson.totalmoney) + Number(verifyJson.logisVerifysum)).toString(),
			'cash': (verifyJson.cash || 0).toString(),
			'card': (verifyJson.card || 0).toString(),
			'billno': verifyResult.billno
		});

		// 代收收款明细验证
		let agencyVerifyInfo = await common.callInterface('qf-1442', {
			'pk': agencyVerifyList.dataList[0].id
		});

		if (USEECINTERFACE == 1) {
			expect(agencyVerifyInfo, `代收收款单明细有误:${JSON.stringify(agencyVerifyInfo)}`).to.includes({
				'totalmoney': (Number(verifyJson.logisVerifysum) + Number(verifyJson.totalmoney)).toString(),
				'totalsum': (Number(verifyJson.logisVerifysum) + Number(verifyJson.totalmoney)).toString(),
				'logisVerifysum': verifyJson.logisVerifysum.toString(),
				'remark': verifyJson.remark,
				'remit': verifyJson.remit.toString(),
				'card': verifyJson.card.toString(),
				'cash': verifyJson.cash.toString(),
				'shopname': LOGINDATA.depname,
				'prodate': common.getCurrentDate(),
				'othercost': verifyJson.totalmoney.toString()
			});
		} else {
			expect(agencyVerifyInfo, `代收收款单明细有误:${JSON.stringify(agencyVerifyInfo)}`).to.includes({
				'totalmoney': (Number(verifyJson.logisVerifysum) + Number(verifyJson.totalmoney)).toString(),
				'totalsum': (Number(verifyJson.logisVerifysum) + Number(verifyJson.totalmoney)).toString(),
				'shopname': LOGINDATA.depname,
				'prodate': common.getCurrentDate().slice(2),
				'othercost': verifyJson.totalmoney.toString()
			});
		};
	});

	/*
	 * ql-14521关于shopid逻辑
	 *
	 * 客户端: 都传shopid(且当前门店)
	 * 服务端: 如果不允许跨门店核销shopid生效，否则忽略这个条件
	 */
	it.skip('170291,不允许跨门店核销时，不能显示其他门店的物流代收单-客户端控制', async function () {
		// 不允许跨门店核销
		await common.setGlobalParam('sales_verify_overshop', 0);

		// 中洲店登录并开代收单
		await gmLoginZzd();
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 常青店登录并开代收单
		await common.loginDo();
		salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 代收单选择列表
		let logisChooseListCqd = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': salesResult.params.logisDwid,
			'shopid': LOGINDATA.invid,
			'dwid': salesResult.params.dwid
		}, true));
		let isAllCqdData = true;
		for (let i = 0; i < logisChooseListCqd.dataList.length; i++) {
			if (logisChooseListCqd.dataList[i].shopname != LOGINDATA.depname) {
				isAllCqdData = false;
				break;
			}
		}
		expect(isAllCqdData, `不允许跨门店核销时，显示了其他门店的物流代收单，错误`).to.be.true;

		// 允许跨门店核销
		await common.setGlobalParam('sales_verify_overshop', 1);
	});

	it('170575,核销时可以输入全部的物流商,但店长在物流商查询列表中只能查到本门店的物流商', async function () {
		await shopManagerLoginZzd();
		let logisDwxxList = await common.callInterface('qd-2310', format.qlParamsFormat({}));
		let isHaveCqd = false;
		for (let i = 0; i < logisDwxxList.dataList.length; i++) {
			if (logisDwxxList.dataList[i].invName != LOGINDATA.depname) {
				isHaveCqd = true;
			}
		}
		expect(isHaveCqd, `核销时可以输入全部的物流商,但店长在物流商查询列表中只能查到本门店的物流商,错误`).to.be.false;
		await common.loginDo();
	});

	it('170577,物流核销时待核销物流单选择界面--日期和客户查询条件查询', async function () {
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 代收单选择列表
		let logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': salesResult.params.logisDwid,
			'dwid': salesResult.params.dwid,
			'prodate1': common.getCurrentDate(),
			'prodate2': common.getCurrentDate(),
			'shopid': salesResult.params.invid,
		}));

		let isRight = true;
		for (let i = 0; i < logisChooseList.dataList.length; i++) {
			if (logisChooseList.dataList[i].prodate != common.getCurrentDate('YY-MM-DD') || logisChooseList.dataList[i].dwxxid != salesResult.params.dwid) {
				isRight = false;
			}
		}
		expect(isRight, `物流核销时待核销物流单选择界面--日期和客户查询条件查询有误`).to.be.true;

		logisChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': salesResult.params.logisDwid,
			'dwid': salesResult.params.dwid,
			'prodate1': common.getDateString([0, 0, 1]),
			'prodate2': common.getDateString([0, 0, 1]),
			shopid: salesResult.params.invid
		}));

		expect(logisChooseList, `物流核销时待核销物流单选择界面--日期和客户查询条件查询有误`).to.includes({
			'count': '0'
		});
	});

	it('170641/170612,非总经理修改物流单', async function () {
		// 修改未核销的物流单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());
		let logisInfo = await common.callInterface('qf-1432-4', {
			'pk': salesResult.result.pk
		});
		let jsonparam = {
			'action': 'edit',
			'pk': logisInfo.pk,
			'agency': logisInfo.agency,
			'billno': logisInfo.billno,
			'dwid': logisInfo.dwid,
			'logisBillno': logisBillno,
			'logisDwid': logisInfo.logisDwid,
			'logisRem': '',
			'prodate': logisInfo.prodate,
			'shopid': logisInfo.shopid
		};
		let editResult = await common.callInterface('sf-1432-4', {
			'jsonparam': jsonparam
		});
		expect(editResult, `非总经理修改未核销物流单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');

		let logisInfoAft = await common.callInterface('qf-1432-4', {
			'pk': salesResult.result.pk
		});
		logisInfo.logisBillno = logisBillno;
		logisInfo.logisRem = '';
		common.isApproximatelyEqualAssert(logisInfo, logisInfoAft);

		let agencyInfo = await salesReqHandler.getAgencyInfo(salesResult.result.pk); //获取物流信息
		expect(agencyInfo, `非总经理修改未核销物流单后，代收单页面代收信息有误`).to.includes({
			'logisBillno': logisBillno,
			'logisRem': ''
		});


		// 修改已核销的物流单
		let verifyResult = await logisVerifyHelp.doLogisVerify();
		logisInfo = await common.callInterface('qf-1432-4', {
			'pk': verifyResult.salesResult.pk
		});
		jsonparam = {
			'action': 'edit',
			'pk': logisInfo.pk,
			'agency': logisInfo.agency,
			'billno': logisInfo.billno,
			'dwid': logisInfo.dwid,
			'logisBillno': logisBillno,
			'logisDwid': logisInfo.logisDwid,
			'logisRem': '',
			'prodate': logisInfo.prodate,
			'shopid': logisInfo.shopid
		};
		editResult = await common.callInterface('sf-1432-4', {
			'jsonparam': jsonparam
		});
		expect(editResult, `非总经理修改已核销物流单失败:${JSON.stringify(editResult)}`).to.not.have.property('error');

		logisInfoAft = await common.callInterface('qf-1432-4', {
			'pk': verifyResult.salesResult.pk
		});
		logisInfo.logisBillno = logisBillno;
		logisInfo.logisRem = '';
		common.isApproximatelyEqualAssert(logisInfo, logisInfoAft);

		agencyInfo = await salesReqHandler.getAgencyInfo(verifyResult.salesResult.pk); //获取物流信息
		expect(agencyInfo, `非总经理修改未核销物流单后，代收单页面代收信息有误`).to.includes({
			'logisBillno': logisBillno,
			'logisRem': ''
		});
	});

	it('170686,开启-允许跨门店核销时，显示全部门店的余款', async function () {
		// 允许跨门店核销
		await common.setGlobalParam('sales_verify_overshop', 1);

		// 中洲店总经理登录 并开代收单
		await gmLoginZzd();
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 销售开单-->物流核销+-->点核销
		let verifyChooseList = await common.callInterface('ql-14521', format.qlParamsFormat({
			'logisDwid': salesResult.params.logisDwid,
			'pagesize': '0'
		}));
		let verifysum = 0;
		for (let i = 0; i < verifyChooseList.dataList.length; i++) {
			verifysum += Number(verifyChooseList.dataList[i].finpayagency);
		}

		// 销售开单-->物流查询
		let logisList = await common.callInterface('ql-1432', format.qlOnlySumFormat({
			'prodate1': common.getDateString([-10, 0, 0]),
			'prodate2': common.getCurrentDate(),
			'logisDwid': salesResult.params.logisDwid,
			'logisPayflag': '0',
			'invalidflag': '0'
		}));

		let agencyInfo = await common.callInterface('cs-getlogisangecysum', {
			'dwid': BASICDATA.dwidSFKD
		});
		expect(Number(logisList.sumrow.finpayagency), `170686,开启-允许跨门店核销时，显示全部门店的余款 错误`).to.be.equal(verifysum);
		expect(logisList.sumrow.finpayagency, `170686,开启-允许跨门店核销时，显示全部门店的余款 错误`).to.be.equal(agencyInfo.agency);
		await common.loginDo();
	});

	it.skip('170688,不开启允许跨门店核销时，显示本门店的余款-客户端控制', async function () {
		// 不允许跨门店核销
		await common.setGlobalParam('sales_verify_overshop', 0);

		await gmLoginZzd();
		let agencyInfo = await common.callInterface('cs-getlogisangecysum', {
			'dwid': BASICDATA.dwidSFKD,
			'shopid': LOGINDATA.invid
		});

		let logisList = await common.callInterface('ql-1432', format.qlOnlySumFormat({
			'prodate1': common.getDateString([-10, 0, 0]),
			'prodate2': common.getCurrentDate(),
			'logisDwid': BASICDATA.dwidSFKD,
			'logisPayflag': '0',
			'shopid': LOGINDATA.invid
		}));
		expect(logisList.sumrow.finpayagency, `170688,不开启允许跨门店核销时，显示本门店的余款 错误`).to.be.equal(agencyInfo.agency);
		await common.loginDo();
	});

	it('170251,物流单核销修改日志里不显示核销时间和核销批次', async function () {
		// 开代收单 -->物流核销单
		let agencyVerify = await logisVerifyHelp.doLogisVerify();
		let salesResult = agencyVerify.salesResult;

		let editHistory = await common.callInterface('qf-1421-3', {
			'pk': salesResult.pk
		});
		expect(editHistory, `物流单核销修改日志里显示了核销时间，错误`).to.not.have.property('verifytime');
		expect(editHistory, `物流单核销修改日志里显示了核销批次，错误`).to.includes({
			'verifybillBillno': '0'
		});
	});

	it('170421,【销售开单-按订货开单】通过代收方式收钱', async function () {
		await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单

		let orderSfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单

		let orderInfo = await salesReqHandler.salesOrderQueryBilling(orderSfRes.result.pk);
		expect(orderInfo.result, `没有查询到订单信息:${JSON.stringify(orderInfo)}`).to.not.have.property('error');

		let jsonparam = getSalesJson(orderInfo.result);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `按订货开代收单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'id1': salesResult.billno,
			'id2': salesResult.billno,
			'shopid': jsonparam.shopid,
		}, true));
		expect(logisList.dataList[0], ``).to.includes({
			'agencyflag': '是',
			'logispayflagid': '0',
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': jsonparam.logisDwid,
			'invalidflag': '0',
			'finpayagency': (jsonparam.agency).toString(),
			'dwxxid': jsonparam.dwid,
			'billno': salesResult.billno,
		});

	});

	it('170687,开启跨门店核销核销不同门店的物流单', async function () {
		// 允许跨门店核销
		await common.setGlobalParam('sales_verify_overshop', 1);
		await common.setGlobalParam('invinout_checknum', 0);

		// 常青店开代收单
		let salesResult1 = await common.editBilling(basiceJsonparam.agencySalesJson(), false);
		expect(salesResult1.result, `常青店开销售代收单失败:${JSON.stringify(salesResult1.result)}`).to.not.have.property('error');

		// 中洲店登录开代收单
		await gmLoginZzd();
		let salesResult2 = await common.editBilling(basiceJsonparam.agencySalesJson(), false);
		expect(salesResult2.result, `中洲店开销售代收单失败:${JSON.stringify(salesResult2.result)}`).to.not.have.property('error');

		// 中洲店核销 常青店物流单+中洲店物流单+特殊货品
		let verifyJson = {
			'card': 1,
			'remit': 1,
			'logisVerifybillids': `${salesResult1.result.pk},${salesResult2.result.pk}`,
			'logisVerifysum': salesResult1.params.agency + salesResult2.params.agency,
			'remark': '开启跨门店核销核销不同门店的物流单',
			'dwid': salesResult1.params.logisDwid,
		};
		verifyJson = format.verifyJsonFormat(verifyJson, true); //true表示加上特殊货品,并且设置成多种付款方式
		verifyJson.cash = verifyJson.logisVerifysum + verifyJson.totalmoney;
		verifyJson.card = 0;
		verifyJson.remit = 0;
		let verifyBillRes = await common.callInterface('sf-1452', {
			jsonparam: verifyJson,
		});
		expect(verifyBillRes, `物流商核销失败`).to.not.have.property('error');

		// 检查物流核销列表两个物流单状态
		let verifyList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'dwid': salesResult1.params.dwid,
			'shopid': salesResult1.params.shopid,
			'id1': salesResult1.result.billno,
			'id2': salesResult1.result.billno
		}, true));
		expect(verifyList.dataList[0], `物流单列表有误`).to.includes({
			'logispayflagid': '1', //货款收讫  覆盖用例170290
			'logispayflag': '是' //货款收讫
		});
		verifyList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'dwid': salesResult2.params.dwid,
			'shopid': salesResult2.params.shopid,
			'id1': salesResult2.result.billno,
			'id2': salesResult2.result.billno
		}, true));
		expect(verifyList.dataList[0], `物流单列表有误`).to.includes({
			'logispayflagid': '1', //货款收讫  覆盖用例170290
			'logispayflag': '是' //货款收讫
		});
		// 检查收支流水列表操作人
		let inoutRec = await common.callInterface('ql-1352', format.qlParamsFormat({
			'accountInvid': LOGINDATA.invid,
			'billtype': '80'
		}, true));
		let isRight = false;
		for (let i = 0; i < inoutRec.dataList.length; i++) {
			if (inoutRec.dataList[i].billno == verifyBillRes.billno && inoutRec.dataList[i].staffName == LOGINDATA.name) {
				isRight = true;
				break;
			}
		}
		expect(isRight, `收支流水表操作人字段有误`).to.be.true;


		await common.loginDo();
	});
});

function getSalesJson(orderInfo) {
	let json = {
		'interfaceid': 'sf-14211-1',
		'deliver': orderInfo.sellerid,
		'dwid': orderInfo.clientid,
		'shopid': orderInfo.shopid,
		'maindiscount': (orderInfo.maindiscount || 1).toString(),
		'srcType': '2',
		'billno': orderInfo.billno,
		// 'billid': orderInfo.pk,
		'pk': orderInfo.pk,
		'action': 'add',
		'agency': 1000,
		'card': 0,
		'cash': 0,
		'remit': 0,
		'logisDwid': BASICDATA.dwidSFKD,
		'logisRem': '销售开单-按订货开单通过代收的方式收款'
	};
	let details = [];
	for (let i = 0; i < orderInfo.details.length; i++) {
		let item = orderInfo.details[i];
		details.push({
			'styleid': item.styleid,
			'rowid': item.rowid,
			'colorid': item.colorid,
			'sizeid': item.sizeid,
			'num': item.restnum,
			'price': item.price,
			'discount': (item.discount || 1).toString(),
			'pk': item.pk,
		});
	}
	json.details = details;
	json = format.jsonparamFormat(json);

	return json;
}

async function gmLoginZzd() {
	let param = {
		'logid': '200',
		'pass': '000000'
	};
	await common.loginDo(param);
}

async function shopManagerLoginZzd() {
	let param = {
		'logid': '204',
		'pass': '000000'
	};
	await common.loginDo(param);
}