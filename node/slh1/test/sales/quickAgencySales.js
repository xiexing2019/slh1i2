'use strict';

const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 快速标记代收模式 相关用例验证
 */

describe("快速标记代收模式-slh2", function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		await common.setGlobalParam('paymethod', 9); //快速标记代收模式
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2
	});

	it('170070.快速标记代收-标记代收.rankA', async function () {
		//快速标记代收模式开单
		let sfRes = await common.editBilling(basiceJsonparam.quickAgencySalesJson());

		//查看生成的快速标记代收单
		let billingInfo = await salesReqHandler.salesQueryBilling(sfRes.result.pk).then(res => res.result);

		//按批次查
		let searchCondition = {
			'billno1': sfRes.result.billno,
			'billno2': sfRes.result.billno,
			'shopid': sfRes.params.invid,
			'dwid': sfRes.params.dwid
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchCondition, true));

		//验证开单数据和生成单据数据是否一致
		expect(batchInfo.dataList[0], `\n生成的单据没有标记快速代收`).to.includes({
			"balancetypename": "是"
		});
		common.isApproximatelyEqualAssert(sfRes.params, billingInfo);
		common.isApproximatelyEqualAssert(billingInfo, batchInfo.dataList[0]);
	});

	it('170071.快速标记代收-不标记代收.rankA', async function () {
		//快速标记代收模式开单
		let processJson = basiceJsonparam.quickAgencySalesJson();
		processJson.balancetype = 1;
		let sfRes = await common.editBilling(processJson);

		//查看生成的快速标记代收单
		let billingInfo = (await salesReqHandler.salesQueryBilling(sfRes.result.pk)).result;

		//按批次查
		let searchCondition = {
			'billno1': sfRes.result.billno,
			'billno2': sfRes.result.billno,
			'shopid': sfRes.params.invid,
			'dwid': sfRes.params.dwid
		}
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchCondition, true));

		//验证开单数据和生成单据数据是否一致
		expect(batchInfo.dataList[0], `\n生成的单据标记了快速代收`).to.includes({
			"balancetypename": "否"
		});
		common.isApproximatelyEqualAssert(sfRes.params, billingInfo);
		common.isApproximatelyEqualAssert(billingInfo, batchInfo.dataList[0]);
	});
})
