'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesResHandler = require('../../help/salesHelp/salesRequestHandler');
/*
 * 退货验证
 */

describe('退货验证-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	after(async () => {
		await common.setGlobalParam('sales_checkbacknum', 0);
	});

	it('170181,客户不允许退货.rankA', async function () {
		//新增客户并开销售单 (新增客户设置为不允许退货)
		let preOperat = await addCustAndSaleGoods('1');

		// 对前面的开单退货
		let backJson = _.cloneDeep(preOperat.salesJson);
		for (let i = 0; i < backJson.details.length; i++) {
			backJson.details[i].num = -backJson.details[i].num;
		}
		backJson = clearMoney(backJson);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': backJson
		});
		expect(salesResult, `客户不允许退货控制有误`).to.includes({
			'error': '该客户不允许退货，请检查'
		});
	});

	it('170074,开启退货数验证时提示具体哪个款号的退货数超出.rankA', async function () {
		await common.setGlobalParam('sales_checkbacknum', 1);

		//新增客户并开销售单 (新增客户设置为允许退货)
		let preOperat = await addCustAndSaleGoods();
		//console.log(`preOperat : ${JSON.stringify(preOperat)}`);
		let salesDetails = preOperat.salesInfo.result.details[0];
		let errorInfo = `${salesDetails.show_styleid},${salesDetails.show_colorid},${salesDetails.show_sizeid}`;
		// 对前面的开单退货
		let backJson = _.cloneDeep(preOperat.salesJson);
		for (let i = 0; i < backJson.details.length; i++) {
			backJson.details[i].num = -(Number(backJson.details[i].num) * 2);
		}
		backJson = clearMoney(backJson);
		let salesResult = await common.editBilling(backJson, false);
		expect(salesResult.result, `客户不允许退货控制有误`).to.includes({
			'error': `款号【${errorInfo}】退货数量高于拿货总数量，请核对`
		});
	});
	it('170075,客户退货数量－不填客户', async function () {
		await common.setGlobalParam('sales_checkbacknum', 1);

		let salesJson = format.jsonparamFormat(salesAlsoBackJson());
		salesJson.dwid = '';
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `不填写客户时，只要退货数小于拿货数，允许开单成功。但这里开单失败，请检查:${JSON.stringify(salesResult)}`).to.not.have.property('error');
	});

	it.skip('170211,均色均码退货不输入客户验证提示信息', async function () {
		await common.setGlobalParam('ignorecolorsize', 1);

		let salesJson = format.jsonparamFormat({
			'interfaceid': 'sf-14211-1',
		});
		salesJson.dwid = '';
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});

		await common.setGlobalParam('ignorecolorsize', 0);
	});
	/*
	 * 服务端逻辑:只受sales_verify_overshop的控制，关于退货验证的系统参数控制逻辑在客户端
	 *
	 * 这里验证的时候只在常青店开单，因此不受sales_verify_overshop的影响
	 */
	describe('验证get-customer-sku-sum', function () {
		let dwid = '',
			jsonparam, skuInfo;

		before(function () {
			jsonparam = {
				'shopid': LOGINDATA.invid,
				'skus': [{
					'colorid': BASICDATA['coloridBaiSe'],
					'num': '-1',
					'sizeid': BASICDATA['sizeidM'],
					'styleid': BASICDATA['styleidAgc001'],
				}],
			};
			if (USEECINTERFACE == '2') jsonparam.bizType = 1100; //销售单
		});

		it('1、未拿货', async function () {
			let custJson = basiceJsonparam.addCustJson();
			let custResult = await common.callInterface('sf-1401', {
				'jsonparam': custJson
			});
			expect(custResult, `新增客户失败:${JSON.stringify(custResult)}`).to.not.have.property('error');
			dwid = custResult.val;

			jsonparam.dwid = dwid;
			skuInfo = await common.callInterface('get-customer-sku-sum', {
				'jsonparam': jsonparam
			});
			if (USEECINTERFACE == 1) {
				expect(skuInfo.dataList.length == 0, `未拿货情况下，查询sku结果有误`).to.be.true;
			} else {
				common.isApproximatelyEqualAssert({ totalBuyNum: '', isSecondSale: '0', firstBuyDate: '' }, skuInfo.dataList[0])
			};
		});

		it('2、第一次拿货', async function () {
			expect(dwid, `获取客户id失败`).to.not.equal('');

			// 销售开单
			let salesJson = format.jsonparamFormat(getSalesJsonparam(dwid));
			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesJson
			});
			expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

			// 验证sku
			skuInfo = await common.callInterface('get-customer-sku-sum', {
				'jsonparam': jsonparam
			});
			skuInfo.dataList[0].totalBuyNum = Number(skuInfo.dataList[0].totalBuyNum).toString();
			expect(skuInfo.dataList[0], `未拿货情况下，查询sku结果有误`).to.includes({
				'isSecondSale': '0',
				'sizeid': jsonparam.skus[0].sizeid,
				'styleid': jsonparam.skus[0].styleid,
				'colorid': jsonparam.skus[0].colorid,
				'totalBuyNum': salesJson.details[0].num.toString(),
				'firstBuyDate': `${common.getCurrentDate('YYYY-MM-DD')} 00:00:00`
			});
		});

		it('3、退货', async function () {
			expect(dwid, `获取客户id失败`).to.not.equal('');

			// 销售开单 (退货)
			let salesJson = getSalesJsonparam(dwid);
			salesJson.details[0].num = -1;
			salesJson = format.jsonparamFormat(salesJson);
			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesJson
			});
			expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

			// 验证sku
			let totalBuyNum = Number(skuInfo.dataList[0].totalBuyNum) + salesJson.details[0].num;
			skuInfo = await common.callInterface('get-customer-sku-sum', {
				'jsonparam': jsonparam
			});
			skuInfo.dataList[0].totalBuyNum = Number(skuInfo.dataList[0].totalBuyNum).toString();
			expect(skuInfo.dataList[0], `未拿货情况下，查询sku结果有误`).to.includes({
				'isSecondSale': '0',
				'sizeid': jsonparam.skus[0].sizeid,
				'styleid': jsonparam.skus[0].styleid,
				'colorid': jsonparam.skus[0].colorid,
				'totalBuyNum': totalBuyNum.toString(),
				'firstBuyDate': `${common.getCurrentDate('YYYY-MM-DD')} 00:00:00`
			});
		});

		it('4、补货', async function () {
			expect(dwid, `获取客户id失败`).to.not.equal('');

			// 销售开单
			let salesJson = format.jsonparamFormat(getSalesJsonparam(dwid));
			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesJson
			});
			expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

			// 验证sku
			let totalBuyNum = Number(skuInfo.dataList[0].totalBuyNum) + salesJson.details[0].num;
			skuInfo = await common.callInterface('get-customer-sku-sum', {
				'jsonparam': jsonparam
			});
			skuInfo.dataList[0].totalBuyNum = Number(skuInfo.dataList[0].totalBuyNum).toString();
			expect(skuInfo.dataList[0], `未拿货情况下，查询sku结果有误`).to.includes({
				'isSecondSale': '1',
				'sizeid': jsonparam.skus[0].sizeid,
				'styleid': jsonparam.skus[0].styleid,
				'colorid': jsonparam.skus[0].colorid,
				'totalBuyNum': totalBuyNum.toString(),
				'firstBuyDate': `${common.getCurrentDate('YYYY-MM-DD')} 00:00:00`
			});
		});

	});

	it('170227,单据修改状态下退货验证问题', async function () {
		let custJson = basiceJsonparam.addCustJson();
		let custResult = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		expect(custResult, `新增客户失败:${JSON.stringify(custResult)}`).to.not.have.property('error');

		// 销售开单
		let salesJson = getSalesJsonparam(custResult.val);
		salesJson.details[0].num = 10;
		salesJson = format.jsonparamFormat(salesJson);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `销售开单失败:${salesResult}`).to.not.have.property('error');

		// 退货
		let backJson = _.cloneDeep(salesJson);
		backJson.details[0].num = -5;
		backJson = clearMoney(backJson);
		let backSales = await common.callInterface('sf-14211-1', {
			'jsonparam': backJson
		});
		expect(backSales, `销售退货失败:${salesResult}`).to.not.have.property('error');

		// 修改销售退货
		let backInfo = await common.callInterface('qf-14211-1', {
			'pk': backSales.pk,
			isPend: 0,
		});
		backJson = _.cloneDeep(backInfo);
		backInfo.details[0].num = -6;
		backInfo = clearMoney(backInfo);
		backInfo.action = 'edit';
		delete backInfo.shouldpay;
		delete backInfo.totalmoney;
		delete backInfo.balance;
		backSales = await common.callInterface('sf-14211-1', {
			'jsonparam': backInfo
		});
		expect(backSales, `修改销售退货失败:${JSON.stringify(backSales)}`).to.not.have.property('error');
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-23793
	describe('退货时间', function () {
		let json;
		before(async () => {
			if (USEECINTERFACE == 2) this.pending = true;
			await common.setGlobalParam('sales_checkbacknum', 0);//开单保存开启退货数和上次购买数的比对验证 0为不开启
			await common.setGlobalParam('sales_backmat_expiredays_allowinput', 0);//销售开单退货时验证时，是否允许继续输入 0为不允许输入
			await common.setGlobalParam('sales_backmat_expiredays', 2);//退货期限(天数),销售开单退货时验证是否已经超出期限	
			//新增客户
			let cust = await salesResHandler.addCust(basiceJsonparam.addCustJson());
			json = basiceJsonparam.salesJson();
			json.dwid = cust.result.val;
			json.prodate = common.getDateString([0, 0, -3]);
			//销售开单-三天前的的单据
			await common.editBilling(json);
			//设置退货
			json.details.forEach(ele => {
				ele.num = -3;
			});
		});
		after(async () => {
			await common.setGlobalParam('sales_checkbacknum', 1);//开单保存开启退货数和上次购买数的比对验证  0为不开启
			await common.setGlobalParam('sales_backmat_expiredays_allowinput', 1);//销售开单退货时验证时，是否允许继续输入
			await common.setGlobalParam('sales_backmat_expiredays', 10);//退货期限(天数),销售开单退货时验证是否已经超出期限	
		});
		it('开退货期限内的退货单', async function () {
			let backSalesJson = _.cloneDeep(json);
			backSalesJson.prodate = common.getDateString([0, 0, -2]);
			let sfRes = await common.editBilling(backSalesJson, false);
			expect(sfRes.result, '退货期限内开退货单失败').to.have.property('pk');
		});
		it('开退货期限外退货单', async function () {
			let backSalesJson = _.cloneDeep(json);
			backSalesJson.prodate = common.getCurrentDate();
			let sfRes = await common.editBilling(backSalesJson, false);
			expect(sfRes.result, '设置退货期限两天，退三天后的单据退货成功了').to.includes({ error: '该款[agc001，auto001，白色，M]已超出退货期限，不允许退货' });
		});


	});

});

/*
 * 清空所有付款方式的钱款
 */
function clearMoney(jsonparam) {
	jsonparam.remit = 0;
	jsonparam.cash = 0;
	jsonparam.card = 0;

	return format.jsonparamFormat(jsonparam);
}

/*
 * 开单jsonparam  details只有一个元素
 *
 * 注意：这个json的detail中的颜色尺码不能改动
 */
function getSalesJsonparam(dwid) {
	return {
		"interfaceid": "sf-14211-1",
		"dwid": dwid,
		"card": 200,
		"cash": 300,
		"remit": 500,
		"remark": "退货验证",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

/*
 * 开单jsonparam 不输入客户时，即拿货又退货，单拿货>退货
 */
function salesAlsoBackJson() {
	return {
		"interfaceid": "sf-14211-1",
		"cash": 200,
		"remark": "不输入客户时即拿货又退货",
		"srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		"details": [{
			"num": 4,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": -2,
			"sizeid": "M",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}, {
			"num": -1,
			"sizeid": "L",
			"matCode": "Agc001",
			"rem": "明细1",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		}],
	};
};

/*
 * 新增客户并返回客户id
 *
 * noretflag 是否允许退货
 */
async function addCustAndSaleGoods(noretflag = '0') {
	let custJson = basiceJsonparam.addCustJson();
	custJson.noretflag = noretflag;
	let custResult = await salesResHandler.addCust(custJson);
	// 用新增的客户开单
	let salesJson = getSalesJsonparam(custResult.result.val);
	salesJson = format.jsonparamFormat(salesJson);
	let salesResult = await common.editBilling(salesJson);
	//查询销售单想想
	let salesInfo = await salesResHandler.salesQueryBilling(salesResult.result.pk);
	return {
		'dwid': custResult.result.val,
		'salesJson': salesJson,
		'salesInfo': salesInfo,
	};
};
