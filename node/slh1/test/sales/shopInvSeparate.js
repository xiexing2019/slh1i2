'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const storesBinding = require('../../../data/warehouseStoresBinding.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/**
 * 异地发货
 * 二代不支持异地发货模式 暂且不适配
 */
describe('异地发货', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('scm_shop_inv_separate', '1'); //开启异地仓库模式
		await common.setGlobalParam('paymethod', 15);

	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2);
	});

	/*
	 * 常青店登陆验证
	 */

	it('170119,发货门店配货员可查看销售单', async function () {
		// 常青店开销售单 【发货门店是中洲店】
		let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod15());
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `异地发货模式下开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		let salesForm = await salesRequest.salesQueryBilling(salesResult.pk);

		// 中洲店登录，检查前面一步的销售单
		await common.loginDo({
			'logid': '207',
			'pass': '000000'
		});
		let searchBatchList = {
			'shopid': jsonparam.shopid,
			'dwid': jsonparam.dwid,
			'billno1': salesResult.billno,
			'billno2': salesResult.billno,
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, true));

		await common.loginDo();

		expect(batchInfo.dataList[0], `中洲店配货员看不到门店的销售单,错误`).to.includes({
			"maininvname": "常青店",
			"invname": "中洲店"
		});
		common.isApproximatelyEqualAssert(salesForm.result, batchInfo.dataList[0]);
	});

	it('170124,开单门店开单员可查看销售单，且不能看到其他门店的销售数据', async function () {
		await common.setDataPrivilege('boss', '-2');

		// 常青店开销售单 【发货门店是中洲店】
		let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod15());
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparam
		});
		expect(salesResult, `异地发货模式下开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		let salesForm = await salesRequest.salesQueryBilling(salesResult.pk);

		// 常青店开单员登录，检查前面一步的销售单
		await common.loginDo({
			'logid': '005',
			'pass': '000000'
		});
		let searchBatchList = {
			'dwid': jsonparam.dwid,
			'billno1': salesResult.billno,
			'billno2': salesResult.billno,
		};
		let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, true));
		expect(batchInfo.dataList[0], `开单门店开单员看不到本门店开的异地发货单据，错误`).to.includes({
			"maininvname": "常青店",
			"invname": "中洲店"
		});
		common.isApproximatelyEqualAssert(salesForm.result, batchInfo.dataList[0]);

		//验证开单员只能看到本门店单据
		searchBatchList = {
			'dwid': jsonparam.dwid
		};
		batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, true));
		let isRight = true;
		for (let i = 0; i < batchInfo.dataList.length; i++) {
			if (batchInfo.dataList[i].maininvname != LOGINDATA.depname) isRight = false;
		}
		expect(isRight, `开单员看到了其他门店的销售单据，错误`).to.be.true;

		await common.loginDo();
	});

	it('170121,后台不绑仓库，开单时选择发货仓库', async function () {
		// 开单用到的json
		let salesJson = basiceJsonparam.salesJsonPaymenthod15();
		// details第一个元素num>0,第二个元素<0
		salesJson.details[0].num = 5;
		salesJson.details[1].num = -2;
		salesJson = format.jsonparamFormat(salesJson);

		let searchCondition1 = {
			'propdresColorid': salesJson.details[0].colorid,
			'propdresSizeid': salesJson.details[0].sizeid,
			'propdresStyleFlag': '0',
			'propdresStyleid': salesJson.details[0].styleid
		};
		let searchCondition2 = {
			'propdresColorid': salesJson.details[1].colorid,
			'propdresSizeid': salesJson.details[1].sizeid,
			'propdresStyleFlag': '0',
			'propdresStyleid': salesJson.details[1].styleid
		};

		let inventoryListBef1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
		let inventoryListBef2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		if (USEECINTERFACE == 2) await common.delay(500);
		let inventoryListAft1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
		let inventoryListAft2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));

		// --------------------验证开单时从哪个门店扣减库存--------------------
		let sortOutResult = sortOutShopInvInfo(inventoryListBef1, salesJson.invid);
		let selfInvtoryBef = sortOutResult.selfInvtory;
		let otherInvtoryBef = sortOutResult.otherInvtory;
		sortOutResult = sortOutShopInvInfo(inventoryListAft1, salesJson.invid);
		let selfInvtoryAft = sortOutResult.selfInvtory;
		let otherInvtoryAft = sortOutResult.otherInvtory;
		selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
		expect(selfInvtoryAft[0], `(开单)后台不绑仓库，开单时选择发货仓库，则从开单门店操作库存，这里与期望结果不符，错误`).to.includes({
			'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[0].num)).toString()
		});
		common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);

		// --------------------验证退货时给哪个门店增加库存--------------------
		sortOutResult = sortOutShopInvInfo(inventoryListBef2, salesJson.invid);
		selfInvtoryBef = sortOutResult.selfInvtory;
		otherInvtoryBef = sortOutResult.otherInvtory;
		sortOutResult = sortOutShopInvInfo(inventoryListAft2, salesJson.invid);
		selfInvtoryAft = sortOutResult.selfInvtory;
		otherInvtoryAft = sortOutResult.otherInvtory;
		selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
		expect(selfInvtoryAft[0], `(退货)后台不绑仓库，开单时选择发货仓库，则从开单门店操作库存，这里与期望结果不符，错误`).to.includes({
			'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[1].num)).toString()
		});
		common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);
	});

	it('170120,后台不绑仓库，开单时不选择发货仓库', async function () {
		// 开单用到的json
		let salesJson = basiceJsonparam.salesJsonPaymenthod15();
		// details第一个元素num>0,第二个元素<0
		salesJson.details[0].num = 5;
		salesJson.details[1].num = -2;
		salesJson = format.jsonparamFormat(salesJson);
		// 不设定发货门店
		delete salesJson.invid;

		let searchCondition1 = {
			'propdresColorid': salesJson.details[0].colorid,
			'propdresSizeid': salesJson.details[0].sizeid,
			'propdresStyleFlag': '0',
			'propdresStyleid': salesJson.details[0].styleid
		};
		let searchCondition2 = {
			'propdresColorid': salesJson.details[1].colorid,
			'propdresSizeid': salesJson.details[1].sizeid,
			'propdresStyleFlag': '0',
			'propdresStyleid': salesJson.details[1].styleid
		};

		let inventoryListBef1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
		let inventoryListBef2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		if (USEECINTERFACE == 2) await common.delay(500);
		let inventoryListAft1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
		let inventoryListAft2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));

		// --------------------验证开单时从哪个门店扣减库存--------------------
		let sortOutResult = sortOutShopInvInfo(inventoryListBef1, salesJson.shopid);
		let selfInvtoryBef = sortOutResult.selfInvtory;
		let otherInvtoryBef = sortOutResult.otherInvtory;
		sortOutResult = sortOutShopInvInfo(inventoryListAft1, salesJson.shopid);
		let selfInvtoryAft = sortOutResult.selfInvtory;
		let otherInvtoryAft = sortOutResult.otherInvtory;
		selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
		expect(selfInvtoryAft[0], `(开单)后台不绑仓库，开单时不选择发货仓库，默认从开单门店操作库存，这里与期望结果不符，错误`).to.includes({
			'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[0].num)).toString()
		});
		common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);

		// --------------------验证退货时给哪个门店增加库存--------------------
		sortOutResult = sortOutShopInvInfo(inventoryListBef2, salesJson.shopid);
		selfInvtoryBef = sortOutResult.selfInvtory;
		otherInvtoryBef = sortOutResult.otherInvtory;
		sortOutResult = sortOutShopInvInfo(inventoryListAft2, salesJson.shopid);
		selfInvtoryAft = sortOutResult.selfInvtory;
		otherInvtoryAft = sortOutResult.otherInvtory;
		selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
		expect(selfInvtoryAft[0], `(退货)后台不绑仓库，开单时不选择发货仓库，默认从开单门店操作库存，这里与期望结果不符，错误`).to.includes({
			'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[1].num)).toString()
		});
		common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);
	});

	it('171138,【销售开单-按款号汇总】异地发货+门店查询', async function () {
		// 开单用到的json
		let salesJson = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod15());
		// 查询条件
		let searchCondition = {
			'styleid': salesJson.details[0].styleid,
			'colorid': salesJson.details[0].colorid,
			'sizeid': salesJson.details[0].sizeid,
		};

		// 开单之前查询常青店、中洲店按款号汇总的数据
		searchCondition.shopid = salesJson.shopid;
		let summaryByMatCqdBef = await common.callInterface('ql-14458', format.qlParamsFormat(searchCondition, true));
		searchCondition.shopid = salesJson.invid;
		let summaryByMatZzdBef = await common.callInterface('ql-14458', format.qlParamsFormat(searchCondition, true));

		// 开单
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		await common.delay(100);
		if (USEECINTERFACE == 2) await common.delay(500);
		// 开单之后查询常青店、中洲店按款号汇总数据
		searchCondition.shopid = salesJson.shopid;
		let summaryByMatCqdAft = await common.callInterface('ql-14458', format.qlParamsFormat(searchCondition, true));
		searchCondition.shopid = salesJson.invid;
		let summaryByMatZzdAft = await common.callInterface('ql-14458', format.qlParamsFormat(searchCondition, true));

		// 验证开单门店--常青店汇总数据实销数变了，库存不变；而中洲店实销数不变，库存变了
		summaryByMatCqdAft.dataList[0].num = Number(summaryByMatCqdAft.dataList[0].num).toString();

		expect(summaryByMatCqdAft.dataList[0], `异地发货模式，开单门店的汇总数据应该变化，与期望结果不符`).to.includes({
			'num': (Number(summaryByMatCqdBef.dataList[0].num) + Number(salesJson.details[0].num)).toString(),
			'invnum': summaryByMatCqdBef.dataList[0].invnum
		});

		if (summaryByMatZzdBef.count == '0') {
			common.isApproximatelyEqualAssert(summaryByMatZzdBef, summaryByMatZzdAft);
		} else {
			expect(summaryByMatZzdAft.dataList[0], `异地发货模式，仓库门店的汇总数据不应该变化，与期望结果不符`).to.includes({
				'num': summaryByMatZzdBef.dataList[0].num,
				'invnum': (Number(summaryByMatZzdBef.dataList[0].invnum) - Number(salesJson.details[0].num)).toString()
			});
		}
	});

	describe('门店绑定仓库(文一店绑仓库店)', function () {
		before(async () => {
			await common.setGlobalParam('paymethod', 2);
			await common.setGlobalParam('scm_shop_inv_separate', 1); //支持异地仓库
			await storesBinding.bindingWarehouse(); //绑定仓库店

			// 文一店总经理登录
			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
		});
		after(async () => {
			await common.loginDo();
		});

		it('170447,后台门店绑仓库', async function () {
			// 开单用到的json
			let salesJson = basiceJsonparam.salesJson();
			// details第一个元素num>0,第二个元素<0
			salesJson.details[0].num = 5;
			salesJson.details[1].num = -2;
			salesJson = format.jsonparamFormat(salesJson);
			delete salesJson.invid;

			let searchCondition1 = {
				'propdresColorid': salesJson.details[0].colorid,
				'propdresSizeid': salesJson.details[0].sizeid,
				'propdresStyleFlag': '0',
				'propdresStyleid': salesJson.details[0].styleid
			};
			let searchCondition2 = {
				'propdresColorid': salesJson.details[1].colorid,
				'propdresSizeid': salesJson.details[1].sizeid,
				'propdresStyleFlag': '0',
				'propdresStyleid': salesJson.details[1].styleid
			};

			let inventoryListBef1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
			let inventoryListBef2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));
			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': salesJson
			});
			expect(salesResult, `销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			if (USEECINTERFACE == 2) await common.delay(500);
			let inventoryListAft1 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition1));
			let inventoryListAft2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition2));

			// --------------------验证开单时从哪个门店扣减库存--------------------
			let sortOutResult = sortOutShopInvInfo(inventoryListBef1, BASICDATA.shopidCkd);
			let selfInvtoryBef = sortOutResult.selfInvtory;
			let otherInvtoryBef = sortOutResult.otherInvtory;
			sortOutResult = sortOutShopInvInfo(inventoryListAft1, BASICDATA.shopidCkd);
			let selfInvtoryAft = sortOutResult.selfInvtory;
			let otherInvtoryAft = sortOutResult.otherInvtory;
			selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
			expect(selfInvtoryAft[0], `(开单)后台绑定仓库，则从仓库门店操作库存，这里与期望结果不符，错误`).to.includes({
				'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[0].num)).toString()
			});
			common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);

			// --------------------验证退货时给哪个门店增加库存--------------------
			sortOutResult = sortOutShopInvInfo(inventoryListBef2, BASICDATA.shopidCkd);
			selfInvtoryBef = sortOutResult.selfInvtory;
			otherInvtoryBef = sortOutResult.otherInvtory;
			sortOutResult = sortOutShopInvInfo(inventoryListAft2, BASICDATA.shopidCkd);
			selfInvtoryAft = sortOutResult.selfInvtory;
			otherInvtoryAft = sortOutResult.otherInvtory;
			selfInvtoryAft[0].invnum = Number(selfInvtoryAft[0].invnum).toString();
			expect(selfInvtoryAft[0], `(退货)后台绑定仓库，则从仓库门店操作库存，这里与期望结果不符，错误`).to.includes({
				'invnum': (Number(selfInvtoryBef[0].invnum) - Number(salesJson.details[1].num)).toString()
			});
			common.isApproximatelyEqualAssert(otherInvtoryBef, otherInvtoryAft);
		});

		it('170955,[销售开单-开单]异地仓库+门店绑定仓库，配货员验证', async function () {
			// 开单用到的json
			let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
			delete jsonparam.invid;
			let salesResult = await common.callInterface('sf-14211-1', {
				'jsonparam': jsonparam
			});
			expect(salesResult, `异地发货模式下开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

			// 仓库店配货员角色登录
			await common.loginDo({
				'logid': '107',
				'pass': '000000'
			});
			let searchBatchList = {
				'shopid': jsonparam.shopid,
				'dwid': jsonparam.dwid,
				'billno1': salesResult.billno,
				'billno2': salesResult.billno,
			};
			let batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat(searchBatchList, true));

			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
			expect(batchInfo.dataList[0], `仓库店配货员看不到门店的销售单，错误`).to.includes({
				"maininvname": "文一店",
				"invname": "仓库店"
			});
		});

		it('171122.客户未结详细界面检查数据显示-门店绑定仓库.rankA', async function () {
			let json = basiceJsonparam.salesJson();
			json = format.jsonparamFormat(json);
			delete json.invid;
			[json.cash, json.card, json.remit] = [0, 0, 0];
			let sfRes = await common.editBilling(json);
			let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);

			let result = await common.callInterface('ql-144851', format.qlParamsFormat({
				'dwid': BASICDATA.dwidXw,
				'shopid': LOGINDATA.invid,
				'pagesize': 15
			}, true)); //按客户未结-详细
			common.isApproximatelyEqualAssert(qfRes.result, result.dataList[0]);
		});

		it('171137,[销售开单-按款号汇总]门店绑定仓库+门店查询', async function () {
			// 开单用到的json
			let salesJson = basiceJsonparam.salesJson();
			salesJson = format.jsonparamFormat(salesJson);
			delete salesJson.invid;
			// 查询条件
			let searchCondition = format.qlParamsFormat({
				'styleid': salesJson.details[0].styleid,
				'colorid': salesJson.details[0].colorid,
				'sizeid': salesJson.details[0].sizeid,
				'prodate1': common.getDateString([0, -1, 0]), //若当天未开过单,销售开单-按汇总-按款号汇总则不会显示数据
				'prodate2': common.getDateString([0, 0, 0]),
			});

			// 开单之前查询文一店、仓库店按款号汇总的数据
			searchCondition.shopid = BASICDATA.shopidWyd;
			let summaryByMatWydBef = await common.callInterface('ql-14458', searchCondition); //销售开单-按汇总-按款号汇总
			searchCondition.shopid = BASICDATA.shopidCkd;
			let summaryByMatCkdBef = await common.callInterface('ql-14458', searchCondition);

			// 开单
			await common.editBilling(salesJson);
			await common.delay(100);

			// 开单之后查询文一店、仓库店按款号汇总数据
			searchCondition.shopid = BASICDATA.shopidWyd;
			let summaryByMatWydAft = await common.callInterface('ql-14458', searchCondition);
			searchCondition.shopid = BASICDATA.shopidCkd;
			let summaryByMatCkdAft = await common.callInterface('ql-14458', searchCondition);
			expect(summaryByMatCkdAft.dataList.length, '开单后汇总值查询为空').to.equal(1);
			// 验证开单门店--文一店汇总数据发生了变化，而仓库店汇总数据不变  (这里的数据不变指的是销售数、销售额)
			summaryByMatWydAft.dataList[0].num = Number(summaryByMatWydAft.dataList[0].num).toString();

			expect(summaryByMatWydAft.dataList[0], `仓库绑定门店，开单门店的汇总数据应该变化，与期望结果不符`).to.includes({
				'num': (Number(summaryByMatWydBef.dataList[0].num) + Number(salesJson.details[0].num)).toString(),
				'invnum': summaryByMatWydBef.dataList[0].invnum
			});
			expect(summaryByMatCkdAft.dataList[0], `仓库绑定门店，仓库门店的汇总数据不应该变化，与期望结果不符`).to.includes({
				'num': summaryByMatCkdBef.dataList[0] ? summaryByMatCkdBef.dataList[0].num : '0',
				'invnum': common.sub(summaryByMatCkdBef.dataList[0] ? summaryByMatCkdBef.dataList[0].invnum : 0, salesJson.details[0].num).toString(),
			});
		});
	});

	describe('非总经理验证', function () {
		before(async () => {
			await common.loginDo({ //店长登陆
				'logid': '004',
				'pass': '000000',
			});
			await common.setGlobalParam('paymethod', 15);
		});
		after(async () => {
			await common.loginDo();
		});

		it('170766.店长 按订货开单界面显示开单门店的数据.rankA', async function () {
			let json = basiceJsonparam.salesJsonPaymenthod15();
			json.interfaceid = 'sf-14401-1';
			json.remark = '开单模式15';
			let sfRes = await common.editBilling(json);
			let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);

			let param = format.qlParamsFormat({
				orderno1: qfRes.result.billno,
				orderno2: qfRes.result.billno,
			}, true);
			let result = await common.callInterface('ql-14223', param); //销售开单-按订货开单

			expect(result.dataList[0], `按订货开单 门店错误\n${result.dataList[0]}`).to.includes({
				shopname: LOGINDATA.invname //门店是开单门店
			});
		});

		it('171123.客户未结详细界面检查数据显示-异地发货.rankA', async function () {
			let json = basiceJsonparam.salesJsonPaymenthod15();
			[json.cash, json.card, json.remit] = [0, 0, 0];
			let sfRes = await common.editBilling(json);
			let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);

			let result = await common.callInterface('ql-144851', format.qlParamsFormat({
				'dwid': BASICDATA.dwidXw,
				'shopid': LOGINDATA.invid,
				'pagesize': 15
			}, true));
			common.isApproximatelyEqualAssert(qfRes.result, result.dataList[0]);
		});
	});

	describe('销售开单-按订货开单+配货员', function () {
		let orderJson, custShopAccountSC, currInvnumSC, shopName;
		before(async () => {
			if (USEECINTERFACE == 2) {
				this.pending = true;
			}
			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
			await common.setGlobalParam('sales_order_deliver_mode', 1); //销售订单发货模式 按订货开单

			shopName = LOGINDATA.depname; //部门名称
			// 订货jsonparam
			orderJson = basiceJsonparam.salesOrderJson();
			orderJson.card = 0;
			orderJson.remit = 0;
			orderJson.cash = 0;
			orderJson = format.jsonparamFormat(orderJson);

			// 客户门店帐查询条件
			custShopAccountSC = {
				'dwxxid': orderJson.clientid
			};

			// 当前库存查询条件
			currInvnumSC = {
				'propdresColorid': orderJson.details[0].colorid,
				'propdresSizeid': orderJson.details[0].sizeid,
				'propdresStyleid': orderJson.details[0].styleid,
				'propdresStyleFlag': 0
			};

		});

		it('171055.关闭异地仓库,检查账款库存发生在开单门店还是仓库店', async function () {
			await storesBinding.unboundWarehouse(); //解除绑定仓库
			await common.setGlobalParam('paymethod', 2); //切换开单模式为2
			//await common.setGlobalParam('scm_shop_inv_separate', 0); //支持异地仓库 不支持

			// ------------------开单前，文一店查询客户门店帐、当前库存-------------------------
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// ------------------文一店销售订货,仓库店按订货开单-------------------------
			let salesResult = await common.callInterface('sf-14401-1', {
				'jsonparam': orderJson
			});
			expect(salesResult, `文一店销售订货失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			await common.loginDo({ //仓库店配货员登陆
				'logid': '107',
				'pass': '000000'
			});
			let warehouseName = LOGINDATA.depname;
			let salesByOrderInfo = await salesOrderRequest.salesOrderQueryBilling(salesResult.pk);
			let salesByOrder = await salesRequest.editSalesBillByOrder(salesByOrderInfo.result);
			expect(salesByOrder.result, `仓库店配货员按订货开单失败:${JSON.stringify(salesByOrder)}`).to.not.have.property('error');

			// ------------------开单后，文一店查询客户门店帐、当前库存-------------------------
			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// -------------------验证按批次查列表单据所属门店、库存扣减门店、客户账款记录在哪个门店下面-----------------
			let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({
				'billno1': salesByOrder.result.billno,
				'billno2': salesByOrder.result.billno,
				'dwid': salesByOrder.params.dwid,
			}, true));
			common.isApproximatelyEqualAssert({
				'maininvname': shopName,
				'invname': shopName,
			}, batchList.dataList[0], [], `门店不绑定仓库时，销售开单客户门店帐有误(CKD)`);
			common.isApproximatelyEqualAssert({
				'balance': custShopAccountCKD.dataList[0].balance
			}, custShopAccountCKDAft.dataList[0], [], `门店不绑定仓库时，销售开单客户门店帐有误(CKD)`);
			let theBalance = Number(orderJson.card) + Number(orderJson.cash) + Number(orderJson.remit) - Number(orderJson.totalsum);
			common.isApproximatelyEqualAssert({
				'balance': common.add(custShopAccountWYD.dataList[0].balance, theBalance)
			}, custShopAccountWYDAft.dataList[0], [], `门店不绑定仓库时，销售开单客户门店帐有误(WYD)`);
			common.isApproximatelyEqualAssert({
				'invnum': currInvnumCKD.dataList[0].invnum
			}, currInvnumCKDAft.dataList[0], [], `门店不绑定仓库时，销售开单后当前门店库存有误(CKD)`);
			common.isApproximatelyEqualAssert({
				'invnum': common.sub(currInvnumWYD.dataList[0].invnum, orderJson.details[0].num),
			}, currInvnumWYDAft.dataList[0], [], `门店不绑定仓库时，销售开单后当前门店库存有误(WYD)`)
		});

		it('171057.开启异地仓库+不绑定仓库,检查账款库存发生在开单门店还是仓库店.rankA', async function () {

			await common.setGlobalParam('scm_shop_inv_separate', 1); //支持异地仓库 启用
			await storesBinding.unboundWarehouse(); //门店不绑定仓库
			await common.setGlobalParam('paymethod', 15);

			// ------------------开单前，文一店、仓库店查询客户门店帐、当前库存-------------------------
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// ------------------文一店销售订货，仓库店按订货开单-------------------------
			let salesResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.updateHashkey(orderJson)
			});
			expect(salesResult, `文一店销售订货失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			await common.loginDo({
				'logid': '107',
				'pass': '000000'
			});
			let warehouseName = LOGINDATA.depname;
			let salesByOrderInfo = await salesRequest.salesOrderQueryBilling(salesResult.pk);
			salesByOrderInfo.result.invid = LOGINDATA.invid;
			let salesByOrder = await salesRequest.editSalesBillByOrder(salesByOrderInfo.result);
			expect(salesByOrder.result, `仓库店配货员按订货开单失败:${JSON.stringify(salesByOrder.result)}`).to.not.have.property('error');
			if (USEECINTERFACE == 2) await common.delay(1000);
			// ------------------开单后，文一店查询客户门店帐、当前库存-------------------------
			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// -------------------验证按批次查列表单据所属门店、库存扣减门店、客户账款记录在哪个门店下面-----------------
			let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({
				'billno1': salesByOrder.result.billno,
				'billno2': salesByOrder.result.billno,
				'dwid': salesByOrder.params.dwid,
			}, true));
			expect(batchList.dataList[0], `门店不绑定仓库时，销售开单按批次查页面开单门店字段有误`).to.includes({
				'maininvname': shopName,
				'invname': warehouseName,
			});
			let theBalance = Number(orderJson.card) + Number(orderJson.cash) + Number(orderJson.remit) - Number(orderJson.totalsum);
			// expect(custShopAccountCKDAft.dataList[0], `门店不绑定仓库时，销售开单客户门店帐有误(CKD)`).to.includes({
			// 	'balance': custShopAccountCKD.dataList[0].balance
			// });
			common.isApproximatelyEqualAssert({
				'balance': custShopAccountCKD.dataList[0].balance
			}, custShopAccountCKDAft.dataList[0]);
			// expect(custShopAccountWYDAft.dataList[0], `门店不绑定仓库时，销售开单客户门店帐有误(WYD)`).to.includes({
			// 	'balance': common.add(custShopAccountWYD.dataList[0].balance,theBalance)
			// });
			common.isApproximatelyEqualAssert({
				'balance': common.add(custShopAccountWYD.dataList[0].balance, theBalance)
			}, custShopAccountWYDAft.dataList[0]);
			common.isApproximatelyEqualAssert({
				'invnum': common.sub(currInvnumCKD.dataList[0].invnum, orderJson.details[0].num),
			}, currInvnumCKDAft.dataList[0]);

			common.isApproximatelyEqualAssert({
				'invnum': currInvnumWYD.dataList[0].invnum
			}, currInvnumWYDAft.dataList[0]);
			await common.setGlobalParam('paymethod', 2);
		});

		it('171056.开启异地仓库+绑定仓库,检查账款库存发生在开单门店还是仓库店', async function () {
			await common.setGlobalParam('scm_shop_inv_separate', 1); //支持异地仓库 启用
			await storesBinding.bindingWarehouse(); //绑定仓库店

			// ------------------开单前，文一店查询客户门店帐、当前库存-------------------------
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKD = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKD = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// ------------------文一店销售订货，仓库店按订货开单-------------------------
			let salesResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.updateHashkey(orderJson)
			});
			expect(salesResult, `文一店销售订货失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
			await common.loginDo({
				'logid': '107',
				'pass': '000000'
			});
			let warehouseName = LOGINDATA.depname;
			let salesByOrderInfo = await salesRequest.salesOrderQueryBilling(salesResult.pk);
			let salesByOrder = await salesRequest.editSalesBillByOrder(salesByOrderInfo.result);
			expect(salesByOrder.result, `仓库店配货员按订货开单失败:${JSON.stringify(salesByOrder.result)}`).to.not.have.property('error');

			// ------------------开单后，文一店查询客户门店帐、当前库存-------------------------
			await common.loginDo({
				'logid': '400',
				'pass': '000000'
			});
			// 客户门店帐
			custShopAccountSC.invid = BASICDATA.shopidWyd;
			let custShopAccountWYDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));
			custShopAccountSC.invid = BASICDATA.shopidCkd;
			let custShopAccountCKDAft = await common.callInterface('ql-15001', format.qlParamsFormat(custShopAccountSC, false));

			// 当前库存
			currInvnumSC.invid = BASICDATA.shopidWyd;
			let currInvnumWYDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));
			currInvnumSC.invid = BASICDATA.shopidCkd;
			let currInvnumCKDAft = await common.callInterface('ql-1932', format.qlParamsFormat(currInvnumSC, false));

			// -------------------验证按批次查列表单据所属门店、库存扣减门店、客户账款记录在哪个门店下面-----------------
			let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({
				'billno1': salesByOrder.result.billno,
				'billno2': salesByOrder.result.billno,
				'dwid': salesByOrder.params.dwid,
			}, true));
			expect(batchList.dataList[0], `门店绑定仓库时，销售开单按批次查页面开单门店字段有误`).to.includes({
				'maininvname': shopName,
				'invname': warehouseName,
			});
			let theBalance = Number(orderJson.card) + Number(orderJson.cash) + Number(orderJson.remit) - Number(orderJson.totalsum);
			expect(custShopAccountCKDAft.dataList[0], `门店绑定仓库时，销售开单客户门店帐有误(CKD)`).to.includes({
				'balance': custShopAccountCKD.dataList[0].balance
			});
			expect(custShopAccountWYDAft.dataList[0], `门店绑定仓库时，销售开单客户门店帐有误(WYD)`).to.includes({
				'balance': (Number(custShopAccountWYD.dataList[0].balance) + theBalance).toString()
			});
			expect(currInvnumCKDAft.dataList[0], `门店绑定仓库时，销售开单后当前门店库存有误(CKD)`).to.includes({
				'invnum': (Number(currInvnumCKD.dataList[0].invnum) - Number(orderJson.details[0].num)).toString(),
			});
			expect(currInvnumWYDAft.dataList[0], `门店绑定仓库时，销售开单后当前门店库存有误(WYD)`).to.includes({
				'invnum': currInvnumWYD.dataList[0].invnum
			});
		});
	});

});

/*
 * 从当前库存列表中分拣出当前门店库存、其他门店出库
 *
 * invtoryList 当前库存列表
 * shopid 分拣标准（按门店）
 */
function sortOutShopInvInfo(invtoryList, shopid) {
	let selfInvtory = [],
		otherInvtory = [];
	for (let i = 0; i < invtoryList.dataList.length; i++) {
		let item = invtoryList.dataList[i];
		if (item.invid == shopid) {
			selfInvtory.push(item);
		} else {
			otherInvtory.push(item);
		}
	}

	return {
		'selfInvtory': selfInvtory,
		'otherInvtory': otherInvtory
	};
}
