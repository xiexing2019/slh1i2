'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
/*
 * 单据修改
 */
describe('170136,单据修改.rankA-slh2-天数7需要80多秒', function () {
	this.timeout(60000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		// 设置这三种角色可以补登任意天数之前的单据
		await setInsertHisParam('gm', 0);
		await setInsertHisParam('shopmanager', 0);
		await setInsertHisParam('boss', 0);
		// 设置功能参数，单据可作废天数为：不限制
		await common.setRoleParam({
			'ipadcode': 'gm', //总经理
			'code': 'cancel_his',
			'val': '0'
		});
		await common.setRoleParam({
			'ipadcode': 'shopmanager', //店长
			'code': 'cancel_his',
			'val': '0'
		});
		await common.setRoleParam({
			'ipadcode': 'boss', //开单员
			'code': 'cancel_his',
			'val': '0'
		});
		// 单据可补登天数
		await common.setRoleParam({
			'ipadcode': 'gm',
			'code': 'edit_his',
			'val': '0',
		});
		await common.setRoleParam({
			'ipadcode': 'shopmanager',
			'code': 'edit_his',
			'val': '0',
		});
		await common.setRoleParam({
			'ipadcode': 'boss',
			'code': 'edit_his',
			'val': '0',
		});
	});

	describe('销售开单允许修改和作废的天数0,不允许作废', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 0);
		});
		it('店长受控制', async function () {
			await notGMDisableOEditSales('004', 0);
		});
		it('开单员受控制', async function () {
			await notGMDisableOEditSales('005', 0);
		});
		it('总经理不受控制，可以作废', async function () {
			await GMDisableOEditSales('000', 0);
		});
	});

	describe('销售开单允许修改和作废的天数1,当天', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 1);
		});
		it('店长受控制', async function () {
			await notGMDisableOEditSales('004', 1);
		});
		it('开单员受控制', async function () {
			await notGMDisableOEditSales('005', 1);
		});
		it('总经理不受控制，可以作废', async function () {
			await GMDisableOEditSales('000', 1);
		});
	});

	describe.skip('销售开单允许修改和作废的天数3', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 3);
		});
		it('店长受控制', async function () {
			await notGMDisableOEditSales('004', 3);
		});
		it('开单员受控制', async function () {
			await notGMDisableOEditSales('005', 3);
		});
		it('总经理不受控制，可以作废', async function () {
			await GMDisableOEditSales('000', 3);
		});
	});

	describe.skip('销售开单允许修改和作废的天数5', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 5);
		});
		it('店长受控制', async function () {
			await notGMDisableOEditSales('004', 5);
		});
		it('开单员受控制', async function () {
			await notGMDisableOEditSales('005', 5);
		});
		it('总经理不受控制，可以作废', async function () {
			await GMDisableOEditSales('000', 5);
		});
	});

	describe.skip('销售开单允许修改和作废的天数7', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('sales_invalidate_days', 7);
		});
		it('店长受控制', async function () {
			await notGMDisableOEditSales('004', 7);
		});
		it('开单员受控制', async function () {
			await notGMDisableOEditSales('005', 7);
		});
		it('总经理不受控制，可以作废', async function () {
			await GMDisableOEditSales('000', 7);
		});
	});
	if (USEECINTERFACE == 1) {
		describe('170951,【销售开单-按批次查】已作废的单据不能进行商圈推送', function () {
			it('已作废的单据不能进行商圈推送', async function () {
				await common.loginDo();
				let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
				let salesResult = await common.editBilling(jsonparam);
				let disableSales = await common.callInterface('cs-cancel-saleout-bill', {
					'pk': salesResult.result.pk
				});
				expect(disableSales, `作废销售单失败:${JSON.stringify(disableSales)}`).to.not.have.property('error');
				let makeToOnline = await common.callInterface('cs-makeToOnline', {
					'pk': salesResult.result.pk
				});
				expect(makeToOnline, `已作废的单据不能进行商圈推送，但这里推送成功了`).to.includes({
					"error": "单据已作废，不允许该操作"
				});
			});
		});
	}
});

/*
 * 非总经理作废和修改销售单
 *
 * workCode 工号  (总经理除外)
 * val 作废几天内的单据 (0是不允许作废和修改，1只允许作废和修改当天，3只允许作废和修改三天内...)
 */
async function notGMDisableOEditSales(workCode, val) {
	await common.loginDo({
		'logid': workCode,
		'pass': '000000'
	});

	//val == 0 不允许修改作废 特殊处理
	if (val == 0) {
		let salesResult = await common.editBilling(format.jsonparamFormat(basiceJsonparam.salesJson()), false);
		expect(salesResult, `补登${val}天内的数据有误`).to.not.have.property('error');
		let salesForm = await salesReqHandler.salesQueryBilling(salesResult.result.pk);
		salesForm.result.action = 'edit';
		salesForm.result.remark = '修改销售单据';
		salesForm.result.interfaceid = 'sf-14211-1';
		salesResult = await common.editBilling(salesForm.result, false);
		//console.log(`salesResult.result : ${JSON.stringify(salesResult.result)}`);
		expect(salesResult.result, `修改${val}天内的数据有误`).to.includes({
			'error': USEECINTERFACE == 1 ? '不允许修改或作废销售单，请确认!' : '不允许[修改]销售单，请确认!'
		});
		let disableSales = await common.callInterface('cs-cancel-saleout-bill', {
			'pk': salesForm.result.pk
		});
		expect(disableSales, `作废${val}天内的数据有误`).to.includes({
			'error': USEECINTERFACE == 1 ? '不允许修改或作废销售单，请确认!' : '不允许[作废]销售单，请确认!'
		});


	} else {
		// ---------------------允许作废和修改范围内的---------------------
		let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
		jsonparam.prodate = common.getDateString([0, 0, -(val - 1)]);
		jsonparam.remark = `${workCode}补登${val}天内的数据有误`;
		let salesResult = await common.editBilling(jsonparam, false);
		expect(salesResult.result, `补登${val}天内的数据有误`).to.not.have.property('error');
		let salesForm = await salesReqHandler.salesQueryBilling(salesResult.result.pk);
		salesForm.result.action = 'edit';
		salesForm.result.remark = '修改销售单据';
		salesForm.result.interfaceid = 'sf-14211-1';
		salesResult = await common.editBilling(salesForm.result, false);
		expect(salesResult.result, `修改${val}天内的数据有误:${JSON.stringify(salesResult.result)}`).to.not.have.property('error');
		let disableSales = await common.callInterface('cs-cancel-saleout-bill', {
			'pk': salesForm.result.pk
		});
		expect(disableSales, `作废${val}天内的数据有误:${JSON.stringify(disableSales)}`).to.not.have.property('error');

		// ---------------------允许作废和修改范围之外的---------------------
		jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
		jsonparam.prodate = common.getDateString([0, 0, -val]);
		salesResult = await common.editBilling(jsonparam, false);
		expect(salesResult.result, `补登${val}天之外的数据有误`).to.not.have.property('error');
		salesForm = await salesReqHandler.salesQueryBilling(salesResult.result.pk);
		salesForm.result.action = 'edit';
		salesForm.result.remark = '修改销售单据';
		salesForm.result.interfaceid = 'sf-14211-1';
		salesResult = await common.editBilling(salesForm.result, false);
		expect(salesResult.result, `修改${val}天之外的数据有误:${JSON.stringify(salesResult.result)}`).to.includes({
			'error': USEECINTERFACE == 1 ? `只允许作废或修改${val}天内的单据` : `只允许修改${val}天内的单据`,
		});
		disableSales = await common.callInterface('cs-cancel-saleout-bill', {
			'pk': salesForm.result.pk
		});
		expect(disableSales, `作废${val}天之外的数据有误:${JSON.stringify(disableSales)}`).to.includes({
			'error': USEECINTERFACE == 1 ? `只允许作废或修改${val}天内的单据` : `只允许作废${val}天内的单据`,
		});
	}
}

/*
 * 总经理作废和修改销售单
 *
 * workCode 工号
 * val 作废几天内的单据 (0是不允许作废和修改，1只允许作废和修改当天，3只允许作废和修改三天内...)
 */
async function GMDisableOEditSales(workCode, val) {
	await common.loginDo({
		'logid': workCode,
		'pass': '000000'
	});

	// ---------------------允许作废和修改范围之外的---------------------
	let jsonparam = format.jsonparamFormat(basiceJsonparam.salesJson());
	jsonparam.prodate = common.getDateString([0, 0, -val]);
	let salesResult = await common.editBilling(jsonparam, false);
	expect(salesResult.result, `总经理补登${val}天内的数据有误`).to.not.have.property('error');
	let salesForm = await salesReqHandler.salesQueryBilling(salesResult.result.pk);
	salesForm.result.action = 'edit';
	salesForm.result.remark = '修改销售单据';
	salesForm.result.interfaceid = 'sf-14211-1';
	salesResult = await common.editBilling(salesForm.result, false);
	expect(salesResult.result, `总经理应该可以修改${val}天之外的数据，但实际情况是修改失败，错误`).to.not.have.property('error');
	let disableSales = await common.callInterface('cs-cancel-saleout-bill', {
		'pk': salesResult.result.pk
	});
	expect(disableSales, `总经理应该可以作废${val}天之外的数据，但实际情况是作废失败，错误`).to.not.have.property('error');
}

/*
 * 修改角色的 补登天数 功能参数
 *
 * role 角色 [gm=总经理,shopmanager=店长,boss=开单员，tally=财务员，inventory=仓管，aa=采购员，seller=营业员，invdistributor=配货员]
 * val 天数 （0比较特殊表示不限制补登）
 */
async function setInsertHisParam(role, val) {
	//修改角色功能权限
	let roleParam = {
		'ipadcode': role, //店长
		'code': 'insert_his',
		'val': val
	};
	await common.setRoleParam(roleParam);
}