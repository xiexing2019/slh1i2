"use strict";
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

//二代积分规则改变 用例需要重新设计
describe('积分--mainLine', function () {
	this.timeout(30000);

	before(async function () {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('scoretomoney', 100); //多少积分兑换一元 100
	});

	after(async function () {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	it('170184.积分是否跨门店共享-不开启.rankA', async function () {
		await common.setGlobalParam('sales_client_score_share', 0); //积分是否跨门店共享 不共享

		await common.loginDo(); //常青店总经理登陆
		let res = await getCurrentScore(BASICDATA.dwidXw);
		expect(res['ql-1339'].totalscore, `客户查询 总经理显示所有门店积分`).to.equal(res.totalscore);
		expect(res['cs-findOtherBalanceAndScore'].score, `核销界面只显示本门店的积分`).to.equal(res.curShopScore);

		await common.loginDo({ //常青店 店长登陆
			'logid': '004',
			'pass': '000000',
		});
		let res2 = await getCurrentScore(BASICDATA.dwidXw);
		expect(res2['ql-1339'].totalscore, `客户查询 非总经理只显示本门店的积分`).to.equal(res.curShopScore);
		expect(res2['cs-findOtherBalanceAndScore'].score, `核销界面只显示本门店的积分`).to.equal(res.curShopScore);
	});

	it('170185.积分是否跨门店共享-开启.rankA', async function () {
		await common.setGlobalParam('sales_client_score_share', 1); //积分是否跨门店共享 共享

		await common.loginDo(); //常青店总经理登陆
		let res = await getCurrentScore(BASICDATA.dwidXw);
		expect(res['ql-1339'].totalscore, `客户查询 总经理显示所有门店积分`).to.equal(res.totalscore);
		expect(res['cs-findOtherBalanceAndScore'].score, `核销界面显示本门店的积分`).to.equal(res.totalscore);

		await common.loginDo({ //常青店 店长登陆
			'logid': '004',
			'pass': '000000',
		});
		let res2 = await getCurrentScore(BASICDATA.dwidXw);
		expect(res2['ql-1339'].totalscore, `客户查询 非总经理显示所有门店积分`).to.equal(res.totalscore);
		expect(res2.totalscore, `积分查询 非总经理只显示本门店的积分`).to.equal(res.curShopScore);
		expect(res2['cs-findOtherBalanceAndScore'].score, `核销界面显示本门店的积分`).to.equal(res.totalscore);
	});

	it('170183.积分跨门店共享-开单验证.rankA', async function () {
		await common.setGlobalParam('sales_client_score_share', 1); //积分是否跨门店共享 共享

		await common.loginDo();
		let origScore = await getCurrentScore(BASICDATA.dwidXw);

		await common.loginDo({ //中洲店总经理登陆
			'logid': '200',
			'pass': '000000',
		});
		let sfRes = await common.editBilling(basiceJsonparam.simpleSalesJson());

		await common.loginDo();
		let curScore = await getCurrentScore(BASICDATA.dwidXw);
		assert.equal(curScore['ql-1339'].totalscore, Number(origScore['ql-1339'].totalscore) + Number(sfRes.params.totalmoney));
	});

	it('170186.兑换积分大于可兑换积分总额', async function () {
		let curScore = await getBalanceAndScore(BASICDATA.dwidLs);
		// console.log(`curScore = ${JSON.stringify(curScore)}`);
		let res = await salesRequestHandler.exchangeScore({
			cash: 1000,
			finpayScore: Number(curScore.score) + 1000, //兑换积分大于可兑换积分总额
			dwid: BASICDATA.dwidLs
		}, false);
		expect(res.result).to.includes({
			error: '客户当前积分不足'
		});
	});

	it('170187.积分兑换后检查剩余积分/170189.兑换记录.rankA', async function () {
		let origScore = await getBalanceAndScore(BASICDATA.dwidXw);
		let exchangeRes = await salesRequestHandler.exchangeScore({
			cash: 1000,
			finpayScore: 1000,
			dwid: BASICDATA.dwidXw
		});
		let curScore = await getBalanceAndScore(BASICDATA.dwidXw);
		assert.equal(curScore.score, Number(origScore.score) - 1000);

		let param = format.qlParamsFormat({
			dwid: BASICDATA.dwidXw,
			invid: LOGINDATA.invid,
		}, true);
		let qlResult = await common.callInterface('ql-14520', param);
		expect(qlResult.dataList[0], `兑换记录验证错误\nerror:${JSON.stringify(qlResult.dataList[0])}`).to.includes({
			finpayScore: exchangeRes.params.finpayScore.toString(),
			id: exchangeRes.result.pk,
			typename: '兑换单',
			cash: exchangeRes.params.cash.toString(),
			staffName: LOGINDATA.name,
			opname: LOGINDATA.name,
			invname: LOGINDATA.invname,
			dwname: '小王',
			remark: exchangeRes.params.remark,
			prodate: common.getCurrentDate('YY-MM-DD')
		});
	});

	it.skip('170690.积分抵现(积分不跨门店共享)', async function () {
		await common.setGlobalParam('sales_client_score_share', 0); //积分是否跨门店共享 不共享
		let origScore = await getBalanceAndScore(BASICDATA.dwidXw);

		let json = basiceJsonparam.simpleSalesJson();
		json.dwid = BASICDATA.dwidXw;
		json.details[0].num = 5;
		jsonAddSpecial(json, '66666', common.div(origScore.score, 100) + 100);
		let sfRes = await common.editBilling(json, false); //超出本门店积分开单
		expect(sfRes.result, `积分抵现(超过本门店积分),开单成功`).to.includes({
			error: '客户当前积分不足'
		});

		json.details[json.details.length - 1].price = 1;
		sfRes = await common.editBilling(json);

		let curScore = await getBalanceAndScore(BASICDATA.dwidXw);
		//积分抵扣 100分换1元
		//积分变化 = totalmoney + 积分抵扣的金额(1) - 积分抵扣(1*100)
		let changeScore = Number(sfRes.params.totalmoney) - 99;
		assert.equal(curScore.score, Number(origScore.score) + changeScore,
			`积分抵扣后，当前积分错误 curScore=${curScore.score} exp=${Number(origScore.score) + changeScore}`);

		json = basiceJsonparam.salesOrderJson();
		jsonAddSpecial(json, '66666', 10);
		sfRes = await common.editBilling(json, false);
		expect(sfRes.result, `销售订货使用积分抵现开单成功`).to.includes({
			error: '特殊货品[积分抵现]不能用于订单'
		});
	});

	it.skip('170691.积分抵现(积分跨门店共享)', async function () {
		await common.setGlobalParam('sales_client_score_share', 1); //积分是否跨门店共享 共享
		let origScore = await getBalanceAndScore(BASICDATA.dwidXw); //返回所有门店积分

		let json = basiceJsonparam.simpleSalesJson();
		json.dwid = BASICDATA.dwidXw;
		json.details[0].num = 5;
		jsonAddSpecial(json, '66666', common.div(origScore.score, 100) + 100);
		let sfRes = await common.editBilling(json, false); //超出所有门店积分开单
		expect(sfRes.result, `积分抵现(超过本门店积分),开单成功`).to.includes({
			error: '客户当前积分不足'
		});


		let deduct = Math.floor(common.div(origScore.score, 100)) - 1; //超过本门店积分，低于总积分
		json.details[json.details.length - 1].price = deduct;
		sfRes = await common.editBilling(json);

		let curScore = await getBalanceAndScore(BASICDATA.dwidXw);
		//积分抵扣 100分换1元
		//积分变化 = totalmoney + 积分抵扣的金额(deduct) - 积分抵扣(deduct*100)
		let changeScore = Number(sfRes.params.totalmoney) - deduct * 99;

		//作废单据
		//超出本门店，低于总积分的积分抵现后，本门店的积分会变成负数，会影响不跨门店的积分抵现验证，因此作废
		await salesRequestHandler.cancelSaleoutBill(sfRes.result.pk);

		assert.equal(curScore.score, Number(origScore.score) + changeScore,
			`积分抵扣后，当前积分错误,curScore=${curScore.score} exp=${Number(origScore.score) + changeScore}`);
	});

	it('170692.积分抵现(异地发货模式)', async function () {
		await common.setGlobalParam('sales_client_score_share', 1); //积分是否跨门店共享 共享
		await common.setGlobalParam('paymethod', 21); //开单模式21 异地+代收
		let origScore = await getCurrentScore(BASICDATA.dwidXw); //获取起始积分

		let json = basiceJsonparam.salesJsonPaymenthod21();
		json.dwid = BASICDATA.dwidXw;
		jsonAddSpecial(json, '66666', 1); //积分抵现 1
		let sfRes = await common.editBilling(json); //常青店开单,中洲店发货

		let curScore = await getCurrentScore(BASICDATA.dwidXw); //获取开单后积分
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款

		//积分抵扣 100分换1元
		//积分变化 = totalmoney + 积分抵扣的金额(1) - 积分抵扣(1*100)
		let changeScore = Number(sfRes.params.totalmoney) - 99;
		assert.equal(curScore.curShopScore, Number(origScore.curShopScore) + changeScore,
			`积分抵扣后，当前积分错误,curScore=${curScore.curShopScore} exp=${Number(origScore.curShopScore) + changeScore}`);
		assert.equal(curScore['中洲店'], origScore['中洲店'],
			`发货门店积分错误 actual:${curScore['中洲店']} != exp:${origScore['中洲店']}`);
	});

	it('170695.特价商品不计算积分.rankA', async function () {
		let origScore = await getBalanceAndScore(BASICDATA.dwidXw); //获取起始积分
		let json = basiceJsonparam.addGoodJson();
		json.special = 1; //特价商品
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: json,
		}); //新增货品


		json = basiceJsonparam.simpleSalesJson();
		json.dwid = BASICDATA.dwidXw;
		json.details[0].styleid = goodRes.pk;
		let sfRes = await common.editBilling(json); //使用特价商品开单

		let curScore = await getBalanceAndScore(BASICDATA.dwidXw); //获取开单后积分
		expect(origScore.score).to.equal(curScore.score);
	});

	it.skip('170693.已作废的抵现单不能再显示', async function () {
		let json = basiceJsonparam.simpleSalesJson();
		json.dwid = BASICDATA.dwidLs;
		jsonAddSpecial(json, '66666', 1); //积分抵现 1
		let sfRes = await common.editBilling(json);
		expect(sfRes.result, `开单失败:${JSON.stringify(sfRes)}`).to.have.property('pk');

		let param = format.qlParamsFormat({
			dwid: BASICDATA.dwidLs,
			invid: LOGINDATA.invid,
			invalidflag: 0, //是否作废 否
		}, true);
		let qlResult = await common.callInterface('ql-14520', param); //积分兑换记录
		let exp = {
			finpayScore: '100',
			typename: '兑换单',
			cash: '1',
		};
		expect(qlResult.dataList[0], `兑换记录验证错误\nerror:${JSON.stringify(qlResult.dataList[0])}`).to.includes(exp);
		exp = qlResult.dataList[0];

		await salesRequestHandler.cancelSaleoutBill(sfRes.result.pk); //作废单据
		qlResult = await common.callInterface('ql-14520', param); //积分兑换记录

		expect(qlResult.dataList).to.satisfy((arr) => {
			return !common.isArrayContainObject(arr, exp);
		});
	});

	describe('特殊货品是否计算到积分', function () {
		let specInfo, json;
		before(async () => {
			specInfo = await getSpecInfo(BASICDATA.styleid00000); //抹零

			json = basiceJsonparam.simpleSalesJson();
			json.dwid = BASICDATA.dwidXw;
			json.details[0].num = 5;
			json.details[0].price = 200;
			jsonAddSpecial(json, '00000', 100);
		});
		after(async () => {
			specInfo.special = 1; //核算到积分ID值(0否 1是 默认为0)
			await editSpecInfo(specInfo);
		});
		it('170560.计算到积分.rankA', async function () {
			specInfo.special = 1; //核算到积分ID值(0否 1是 默认为0)
			await editSpecInfo(specInfo);

			let origScore = await getBalanceAndScore(BASICDATA.dwidXw);

			let sfRes = await common.editBilling(json);

			let curScore = await getBalanceAndScore(BASICDATA.dwidXw);
			assert.equal(curScore.score, Number(origScore.score) + 900);
		});

		it('170561.不计算到积分.rankA', async function () {
			specInfo.special = 0; //核算到积分ID值(0否 1是 默认为0)
			await editSpecInfo(specInfo);

			let origScore = await getBalanceAndScore(BASICDATA.dwidXw);

			let sfRes = await common.editBilling(json);

			let curScore = await getBalanceAndScore(BASICDATA.dwidXw);
			assert.equal(curScore.score, Number(origScore.score) + 1000);
		});
	});

	describe('170533.特殊货品金额不能超出最高比例', function () {
		let specInfo, jsonparam;
		before(async () => {
			specInfo = await getSpecInfo(BASICDATA.styleid00000); //抹零

			jsonparam = basiceJsonparam.simpleSalesJson();
			jsonparam.details[0].num = 1;
			jsonparam.details[0].price = 200;
			jsonparam.cash = 0;
		});
		after(async () => {
			specInfo.ratio = 10; //最高比例(%)
			await editSpecInfo(specInfo);
		});

		it('1.比例为0时,销售开单-开单,输入总额200,抹零200', async function () {
			specInfo.ratio = 0; //最高比例(%)
			await editSpecInfo(specInfo);

			let json = _.cloneDeep(jsonparam);
			jsonAddSpecial(json, '00000', 200);
			await common.editBilling(json);
		});

		it('2.比例为10时,销售开单-开单,还款200,抹零20', async function () {
			specInfo.ratio = 10; //最高比例(%)
			await editSpecInfo(specInfo);

			let sfRes = await common.editBilling(jsonparam); //生成欠款单

			let verifyRes = await salesRequestHandler.getVerifyingBills(sfRes.params.dwid); //获取核销信息
			let json = _.cloneDeep(jsonparam);
			json.details = [];
			json.finpayVerifybillids = verifyRes.dataList[0].id;
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			json.cash = 180;
			jsonAddSpecial(json, '00000', 20);
			await common.editBilling(json);
		});

		it('3.比例为10时,销售开单-开单,还款200,抹零21', async function () {
			let sfRes = await common.editBilling(jsonparam); //生成欠款单

			let verifyRes = await salesRequestHandler.getVerifyingBills(sfRes.params.dwid); //获取核销信息
			let json = _.cloneDeep(jsonparam);
			json.details = [];
			json.finpayVerifybillids = verifyRes.dataList[0].id;
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			json.cash = 179;
			jsonAddSpecial(json, '00000', 21);
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: '特殊货品[00000,抹零]金额超出最高比例',
			});
		});

		it('4.比例为10时,销售开单-开单,货品200,抹零20', async function () {
			let json = _.cloneDeep(jsonparam);
			jsonAddSpecial(json, '00000', 20);
			await common.editBilling(json);
		});

		it('5.比例为10时,销售开单-开单,货品200,抹零21', async function () {
			let json = _.cloneDeep(jsonparam);
			jsonAddSpecial(json, '00000', 21);
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: '特殊货品[00000,抹零]金额超出最高比例',
			});
		});

		it('6.比例为10时,销售开单-开单,货品200,还款200,抹零40', async function () {
			let sfRes = await common.editBilling(jsonparam); //生成欠款单

			let verifyRes = await salesRequestHandler.getVerifyingBills(sfRes.params.dwid); //获取核销信息
			let json = _.cloneDeep(jsonparam);
			json.finpayVerifybillids = verifyRes.dataList[0].id;
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			json.cash = 360;
			jsonAddSpecial(json, '00000', 40);
			await common.editBilling(json);
		});

		it('7.比例为10时,销售开单-开单,货品200,还款200,抹零41', async function () {
			let sfRes = await common.editBilling(jsonparam); //生成欠款单

			let verifyRes = await salesRequestHandler.getVerifyingBills(sfRes.params.dwid); //获取核销信息
			let json = _.cloneDeep(jsonparam);
			json.finpayVerifybillids = verifyRes.dataList[0].id;
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			json.cash = 359;
			jsonAddSpecial(json, '00000', 41);
			sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: '特殊货品[00000,抹零]金额超出最高比例',
			});
		});

		it('8.比例为10时,销售开单-开单,货品200,打包费200,抹零21', async function () {
			let json = _.cloneDeep(jsonparam);
			jsonAddSpecial(json, '00000', 21);
			jsonAddSpecial(json, '00001', 200);
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: '特殊货品[00000,抹零]金额超出最高比例',
			});
		});
	});

});



/**
 *  获取当前积分
 *  @param {String} dwid - 客户id
 */
async function getCurrentScore(dwid) {
	let params = {
		//获取非本门店余额与本门店积分
		'cs-findOtherBalanceAndScore': {
			invid: LOGINDATA.invid,
			dwid: dwid,
		},
		//往来管理-客户查询
		'ql-1339': {
			dwid: dwid,
		},
		//积分查询
		'ql-14510': {
			dwid: dwid,
		}
	};
	let res = await common.getResults(params);

	res['ql-1339'].totalscore = res['ql-1339'].dataList[0].totalscore;
	delete res['ql-1339'].dataList;

	res['ql-14510'].dataList.map((obj) => {
		if (obj.invid == LOGINDATA.invid) res.curShopScore = obj.score;
		res[obj.invName] = obj.score;
	});
	res.totalscore = res['ql-14510'].sumrow.score;
	delete res['ql-14510'];
	// console.log(`res = ${JSON.stringify(res)}`);
	return res;
};

/**
 * 获取非本门店余额与本门店积分
 * @param {String} dwid - 客户id
 */
async function getBalanceAndScore(dwid) {
	let param = {
		invid: LOGINDATA.invid,
		dwid: dwid,
	};
	let result = await common.callInterface('cs-findOtherBalanceAndScore', param);
	return result;
};

/**
 *  在开单的jsonparam中添加特殊货品
 *  @param {String} specCode - specCode 00000:抹零  00001:打包费  66666:积分抵现
 *  @param {String} specNum - 特殊货品数量
 */
function jsonAddSpecial(json, specCode, specNum) {
	let num = specCode == '00001' ? 1 : -1;
	json.details.push({
		matCode: specCode,
		price: specNum,
		num: num,
		colorid: "0",
		sizeid: "0"
	});
};

/**
 * 获取特殊货品信息
 * @param {String} specStyleid - 特殊货品的styleid
 */
async function getSpecInfo(specStyleid) {
	let specInfo = await common.callInterface('qf-1518-1', { //特殊货品详情
		pk: BASICDATA.styleid00000, //抹零
	});
	return specInfo;
};

/**
 * 修改特殊货品信息
 * @param {object} specInfo - 特殊货品jsonparam
 */
async function editSpecInfo(specInfo) {
	delete specInfo.flag;
	specInfo.action = 'edit';

	let res = await common.callInterface('sf-1518-1', { //特殊货品保存
		jsonparam: specInfo,
		pk: specInfo.pk
	});
	expect(res, '修改特殊货品失败').to.have.property('val');
};
