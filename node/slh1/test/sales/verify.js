'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 核销
 */
describe('核销-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2);
	});

	it('170785.核销批次检查', async function () {
		let dwid = BASICDATA.dwidXw;
		let sfRes = await addDebtSalesBill(dwid); //生成欠款单

		let json = await addSalesJsonWithVerify(dwid);
		let sfResVerify = await common.editBilling(json); //核销欠款单

		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = 'sf-14211-1';
		qfRes.result.remark = '修改已核销单据';
		json = format.updateHashkey(qfRes.result);
		sfRes = await common.editBilling(json, false);
		let billno = USEECINTERFACE == 1 ? sfResVerify.result.billno : format.numberFormat(sfResVerify.result.billno);
		expect(sfRes.result).to.includes({
			error: `本单据已被核销，不允许操作，核销本单的批次为${billno}`
		});
	});

	describe('单据核销', function () {
		//核销客户,欠款单,核销界面,核销信息,开单+核销
		let dwid, debtRes, verifyRes, verifyInfo, sfResVerify;
		before(async function () {
			dwid = BASICDATA.dwidLs;
		});

		it('核销界面验证', async function () {
			debtRes = await addDebtSalesBill(dwid); //生成欠款单
			verifyRes = await salesRequestHandler.getVerifyingBills(dwid); //核销界面
			verifyInfo = verifyRes.dataList[0];

			let qfRes = await salesRequestHandler.salesQueryBilling(debtRes.result.pk);
			if (qfRes.result.cashaccountid == '') qfRes.result.cashaccountid = '0';
			if (qfRes.result.cardaccountid == '') qfRes.result.cardaccountid = '0';
			if (qfRes.result.remitaccountid == '') qfRes.result.remitaccountid = '0';
			if (qfRes.result.agencyaccountid == '') qfRes.result.agencyaccountid = '0';
			common.isApproximatelyEqualAssert(qfRes.result, verifyRes.dataList[0]);
		});

		it('核销单据', async function () {
			let json = basiceJsonparam.simpleSalesJson();
			json.dwid = dwid;
			json.finpayVerifybillids = verifyRes.dataList[0].id;
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			sfResVerify = await common.editBilling(json); //开单+核销

			//核销界面 不显示已核销的数据
			verifyRes = await salesRequestHandler.getVerifyingBills(dwid); //获取核销后,核销界面数据
			expect(verifyRes.dataList, `核销界面显示已核销的数据 核销数据为:\n${JSON.stringify(verifyInfo)}`).to.satisfy((arr) => {
				return !common.isArrayContainObject(arr, verifyInfo);
			});
		});
		//	依赖上一条
		it('170058.核销后检查本单已核销', async function () {
			let result = await getSelfVerifiedBills(dwid, sfResVerify.result.pk);
			common.isApproximatelyEqualAssert(result.dataList[0], verifyInfo);
		});

		it('170059.核销后检查所有已核销', async function () {
			let result = await getAllVerifiedBills(dwid);
			expect(result.dataList, `所有已核销未找到核销信息 核销信息为:\n${JSON.stringify(verifyInfo)}`).to.satisfy((arr) => {
				return common.isArrayContainObject(arr, verifyInfo);
			});
		});

		it('170167.查看修改日志(核销记录)', async function () {
			let result = await common.callInterface('qf-14211-3', {
				pk: debtRes.result.pk
			});
			let qlRes = await salesRequestHandler.salesQueryBilling(sfResVerify.result.pk);
			let exp = {
				hashkey: debtRes.params.hashkey,
				opname: verifyInfo.seller,
				pk: debtRes.result.pk,
				verifybillBillno: qlRes.result.billno, //核销批次
				verifytime: qlRes.result.optime, //
			};
			common.isApproximatelyEqualAssert(exp, result);
		});
	});

	describe('170065.检查核销.rankA', function () {
		let dwid;
		before(async function () {
			dwid = BASICDATA.dwidXw;
			await common.setGlobalParam('verifylimit', 0); //收款金额必须不小于核销金额 不限制
		});

		it('1.核销欠款单,不输入任何款号,然后点未付,保存', async function () {
			await addDebtSalesBill(dwid); //生成欠款单
			let json = await addSalesJsonWithVerify(dwid);
			json.details = [];
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await common.editBilling(json);
		});
		it('2.核销欠款单,输入款号,然后点未付,保存', async function () {
			await addDebtSalesBill(dwid); //生成欠款单
			let json = await addSalesJsonWithVerify(dwid);
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await common.editBilling(json);
		});
		it('3.核销余款,但让余款小于货品金额,然后点未付,保存', async function () {
			await addSpareSalesBill(dwid); //生成余款单 1000
			let json = await addSalesJsonWithVerify(dwid);
			json.details.num = 10;
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await common.editBilling(json);
		});
		it('4.核销余款,但让余款大于货品金额,然后保存', async function () {
			await addSpareSalesBill(dwid); //生成余款单 1000
			let json = await addSalesJsonWithVerify(dwid);
			await common.editBilling(json);
		});
		it('5.核销欠款,输入抹零,点未付,保存', async function () {
			await addDebtSalesBill(dwid); //生成欠款单
			let json = await addSalesJsonWithVerify(dwid);
			json.details.push({
				matCode: '00000',
				price: 20,
				num: -1,
				colorid: "0",
				sizeid: "0"
			});
			[json.cash, json.card, json.remit] = [0, 0, 0];
			await common.editBilling(json);
		});
	});

	describe('跨门店核销', function () {
		//客户id,欠款单sfRes,客户门店账参数,客户门店账起始值
		let dwid, sfResDebt, clientShopParam, origResult;
		before(async () => {
			await common.setGlobalParam('sales_verify_overshop', 1); //允许跨门店核销
			dwid = BASICDATA.dwidLs;

			await common.loginDo({ //中洲店总经理登陆
				'logid': '200',
				'pass': '000000',
			});
			sfResDebt = await addDebtSalesBill(dwid); //生成欠款单

			await common.loginDo();
			clientShopParam = format.qlParamsFormat({
				dwxxid: dwid,
			}, false);
			origResult = await common.callInterface('ql-15001', clientShopParam); //客户门店账
		});
		it('170467.跨门店核销后检查本单已核销和所有已核销', async function () {
			let verifyRes = await salesRequestHandler.getVerifyingBills(dwid, BASICDATA.shopidZzd); //获取核销界面信息
			let verifyInfo = getVerifyInfo(verifyRes, '中洲店', sfResDebt.result.billno);
			expect(verifyInfo, '未找到其他门店的核销数据').to.have.property('billno');

			let json = basiceJsonparam.simpleSalesJson();
			json.dwid = dwid;
			json.finpayVerifybillids = verifyInfo.id;
			json.finpayVerifysum = verifyInfo.balance;
			json.cash -= Number(verifyInfo.balance); //生成不欠不余的单据
			let sfRes = await common.editBilling(json);

			let result = await getSelfVerifiedBills(dwid, sfRes.result.pk); //本单已核销
			common.isApproximatelyEqualAssert(result.dataList[0], verifyInfo);

			result = await getAllVerifiedBills(dwid); //所有已核销
			expect(result.dataList, `所有已核销未找到核销信息 核销信息为:\n${JSON.stringify(verifyInfo)}`).to.satisfy((arr) => {
				return common.isArrayContainObject(arr, verifyInfo);
			});
		});
		//依赖170467
		it('170466.跨门店核销后客户帐款检查', async function () {
			let curResult = await common.callInterface('ql-15001', clientShopParam);
			origResult.dataList.map((obj) => {
				// if(obj.invname == '常青店')//常青店不变
				if (obj.invname == '中洲店') obj.balance = Number(obj.balance) - sfResDebt.params.balance; //中洲店欠款单被核销
			});
			let param = format.qlParamsFormat({
				dwid: dwid
			}, false);
			let result = await common.callInterface('ql-1350', param); //往来管理-客户总账
			common.isArrayContainArrayAssert(curResult.dataList, origResult.dataList); //isApproximatelyArrayAssert
			assert.equal(Number(origResult.sumrow.balance) - sfResDebt.params.balance, curResult.sumrow.balance, '客户门店账汇总值错误');
			assert.equal(result.sumrow.dwbalance, curResult.sumrow.balance, '客户总账错误');
		});
	});

	//对已发生跨门店核销的账套，不允许关闭跨门店核销参数--不跨门店核销需要重新弄个账套
	describe('170762.是否允许跨门店核销 参数控制信用额度', function () {
		let jsonparam, sfRes;
		before(async function () {
			let param = basiceJsonparam.addCustJson();
			param.creditbalance = 4000; //信用额度
			let custRes = await salesRequestHandler.addCust(param);
			// console.log(`custRes = ${JSON.stringify(custRes)}`);
			jsonparam = basiceJsonparam.simpleSalesJson();
			jsonparam.dwid = custRes.result.val;
			jsonparam.details[0].num = 10; //欠款2000
			[jsonparam.cash, jsonparam.card, jsonparam.remit] = [0, 0, 0]; //未付

			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			await common.editBilling(jsonparam); //中洲店开单 欠款2000

			await common.loginDo(); //常青店登陆
		});
		afterEach(async () => {
			if (sfRes.result && sfRes.result.pk) {
				await salesRequestHandler.cancelSaleoutBill(sfRes.result.pk); //作废 保持欠款2000
				sfRes = {};
			};
		});

		// 信用额度4000+不开启跨门店，客户在门店A欠款2000，然后再去B门店欠款2100，点保存--保存成功
		it.skip('1.不开启信用额度+不开启跨门店--另一个账套', async function () {
			await common.setGlobalParam('sales_verify_overshop', 0); //不允许跨门店核销
			await common.setGlobalParam('sales_creditbalance_check', 0); //开单是否启用客户信用额度控制 不启用

			let json = _.cloneDeep(jsonparam);
			json.details[0].num = 11;
			sfRes = await common.editBilling(json);
		});

		//开启信用额度+信用额度4000+不开启跨门店，客户在门店A欠款2000，然后再去B门店欠款4001，点保存--保存不成功
		it.skip('2.开启信用额度+不开启跨门店--另一个账套', async function () {
			await common.setGlobalParam('sales_verify_overshop', 0); //不允许跨门店核销
			await common.setGlobalParam('sales_creditbalance_check', 1); //开单是否启用客户信用额度控制 启用

			let json = _.cloneDeep(jsonparam);
			json.details[0].num = 21;
			sfRes = await common.editBilling(json, false);

			expect(sfRes.result).to.includes({
				error: `当前客户欠款累计金额（包含本次销售）为4,200,大于信用额度金额4,000`
			});
		});

		// 信用额度4000+允许跨门店，客户在门店A欠款2000，然后再去B门店欠款2200，点保存--保存不成功，
		it('3.开启信用额度+允许跨门店', async function () {
			TESTCASE = {
				describe: "信用额度跨门店共享",
				expect: "开单失败，提示当前客户欠款累计金额（包含本次销售）为4,200,大于信用额度金额4,000",
				jira: 'SLHSEC-7286',
			};
			await common.setGlobalParam('sales_verify_overshop', 1); //允许跨门店核销
			await common.setGlobalParam('sales_creditbalance_check', 1); //开单是否启用客户信用额度控制 启用

			let json = _.cloneDeep(jsonparam);
			json.details[0].num = 11;
			sfRes = await common.editBilling(json, false); //

			expect(sfRes.result, `开单成功：${JSON.stringify(sfRes)}`).to.includes({
				error: `当前客户欠款累计金额（包含本次销售）为4,200,大于信用额度金额4,000`
			});
		});

	});

});

/*
 *	获取所有已核销结果
 *  dataList 倒序。。。
 */
async function getAllVerifiedBills(dwid) {
	let param = format.qlParamsFormat({
		dwid: dwid,
	}, false);
	if (USEECINTERFACE == '2') param.bizType = 1100; //销售单
	let result = await common.callInterface('ql-15009', param);
	return result;
};

/*
 *	获取本单已核销结果
 */
async function getSelfVerifiedBills(dwid, billid) {
	let param = format.qlParamsFormat({
		dwid,
		billid
	}, false);
	if (USEECINTERFACE == '2') param.bizType = 1100; //销售单
	let result = await common.callInterface('ql-15008', param);
	return result;
};

/*
 *	生成欠款单
 *  return  {params,result}
 */
async function addDebtSalesBill(dwid) {
	let json = basiceJsonparam.simpleSalesJson();
	json.dwid = dwid;
	[json.cash, json.card, json.remit] = [0, 0, 0]; //生成一个欠款单
	let sfRes = await common.editBilling(json);
	return sfRes;
};

/*
 *	生成余款单
 *  余款1000 尽量别变
 *  return {params,result}
 */
async function addSpareSalesBill(dwid) {
	let json = basiceJsonparam.simpleSalesJson();
	json.dwid = dwid;
	json.cash += 1000;
	let sfRes = await common.editBilling(json);
	return sfRes;
};

//170065获取开单jsonparam 带核销信息
async function addSalesJsonWithVerify(dwid) {
	let verifyRes = await salesRequestHandler.getVerifyingBills(dwid); //获取核销界面数据

	let json = basiceJsonparam.simpleSalesJson();
	json.dwid = dwid;
	json.finpayVerifybillids = verifyRes.dataList[0].id;
	json.finpayVerifysum = verifyRes.dataList[0].balance;
	return json;
};

//获取核销界面 指定核销信息
function getVerifyInfo(verifyResult, invname, billno) {
	let json = {};
	for (let i = 0; i < verifyResult.dataList.length; i++) {
		if (billno && verifyResult.dataList[i].billno != billno) continue;
		if (verifyResult.dataList[i].invname == invname) {
			json = verifyResult.dataList[i];
			break;
		};
	};
	return json;
};
