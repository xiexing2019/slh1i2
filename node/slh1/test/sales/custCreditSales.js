'use strict';

const common = require('../../../lib/common.js');
const caps = require('../../../data/caps');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 客户信用额度
 */

describe('客户信用额度-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sales_creditbalance_check', 1);
	});

	after(async () => {
		await common.setGlobalParam('sales_creditbalance_check', 0);
		await common.setGlobalParam('sales_default_creditmoney', 0);
	});

	it('170556+170610/170385,检查默认信用额度0，以及开单不限制欠款额度.rankA', async function () {
		await common.setGlobalParam('sales_default_creditmoney', 0);
		let custJson = getFastAddCustJson();
		let addCust = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		expect(addCust, `170556快速新增客户失败:${JSON.stringify(addCust)}`).to.not.have.property('error');
		let custInfo = await common.callInterface('qf-1401', {
			'pk': addCust.val
		});
		common.isApproximatelyEqualAssert({
			'creditbalance': '0'
		}, custInfo);

		let salesJson = basiceJsonparam.salesJson();
		salesJson.remit = 0;
		salesJson.cash = 0;
		salesJson.card = 0;
		salesJson.dwid = addCust.val;
		salesJson = format.jsonparamFormat(salesJson);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesJson, `170556信用额度设置为0时，开单限制有误:${JSON.stringify(salesResult)}`).to.not.have.property('error');
	});
	//检查默认信用额度，二代是前端控制的
	if (USEECINTERFACE == 1) {
		it('170555.检查默认信用额度1.rankA', async function () {
			await common.setGlobalParam('sales_default_creditmoney', 1);
			let custJson = getFastAddCustJson();
			let addCust = await salesReqHandler.addCust(custJson);
			let custInfo = await common.callInterface('qf-1401', {
				'pk': addCust.result.val
			});
			common.isApproximatelyEqualAssert({
				'creditbalance': '1'
			}, custInfo);
		});
	}
	it('170611.开单超过信息额度时的提示.rankA', async function () {
		let custJson = getFastAddCustJson();
		custJson.creditbalance = 1;
		let addCust = await salesReqHandler.addCust(custJson); //新增客户
		let salesJson = basiceJsonparam.salesJson();
		salesJson.remit = 0;
		salesJson.cash = 0;
		salesJson.card = 0;
		salesJson.dwid = addCust.result.val;
		salesJson = format.jsonparamFormat(salesJson);
		let salesResult = await common.editBilling(salesJson, false);
		expect(salesResult.result, `信用额度设置为1时，开单限制有误1:${JSON.stringify(salesResult)}`).to.have.property('error');
		let isRight = false;
		if (salesResult.result.error.includes('大于信用额度金额')) isRight = true;
		expect(isRight, `信用额度设置为1时，开单限制有误2:${JSON.stringify(salesResult)}`).to.be.true;
	});

	it('170384,后台开启信用额度控制+客户信用额度值大于0.rankA', async function () {
		await common.setGlobalParam('sales_default_creditmoney', 1);

		let custJson = getFastAddCustJson();
		custJson.creditbalance = '100';
		let addCust = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		expect(addCust, `170384快速新增客户失败:${JSON.stringify(addCust)}`).to.not.have.property('error');
		let custInfo = await common.callInterface('qf-1401', {
			'pk': addCust.val
		});
		common.isApproximatelyEqualAssert({
			'creditbalance': '100'
		}, custInfo);

		let salesJson = basiceJsonparam.salesJson();
		salesJson.remit = 0;
		salesJson.cash = 0;
		salesJson.card = 0;
		salesJson.dwid = addCust.val;
		salesJson = format.jsonparamFormat(salesJson);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `170384信用额度设置为100时，开单限制有误1:${JSON.stringify(salesResult)}`).to.have.property('error');
		let isRight = false;
		if (salesResult.error.includes('大于信用额度金额')) isRight = true;
		expect(isRight, `170384信用额度设置为100时，开单限制有误2:${JSON.stringify(salesResult)}`).to.be.true;
	});

	it('170386,后台关闭信用额度控制', async function () {
		await common.setGlobalParam('sales_creditbalance_check', 0);

		let custJson = getFastAddCustJson();
		custJson.creditbalance = '100';
		let addCust = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		expect(addCust, `170386快速新增客户失败:${JSON.stringify(addCust)}`).to.not.have.property('error');
		let custInfo = await common.callInterface('qf-1401', {
			'pk': addCust.val
		});
		common.isApproximatelyEqualAssert({
			'creditbalance': '100'
		}, custInfo);

		let salesJson = basiceJsonparam.salesJson();
		salesJson.remit = 0;
		salesJson.cash = 0;
		salesJson.card = 0;
		salesJson.dwid = addCust.val;
		salesJson = format.jsonparamFormat(salesJson);
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(salesResult, `170386后台关闭信用额度控制，开单限制有误:${JSON.stringify(salesResult)}`).to.not.have.property('error');
	});
});


function getFastAddCustJson() {
	return {
		'action': 'add',
		'addr': common.getRandomStr(12),
		'nameshort': `credit${common.getRandomStr(5)}`,
		'sellerid': LOGINDATA.id,
	};
}
