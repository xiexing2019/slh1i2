'use strict';

const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const reqHandler = require('../../help/reqHandlerHelp');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 上次价
 * 新增了一条参数 需要更新
 * http://jira.hzdlsoft.com:7082/browse/SLH-19578
 */
describe('上次价-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sales_show_lastprice', 1);
		await common.setGlobalParam('sales_use_lastsaleprice', 1);
	});

	after(async () => {
		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('sales_show_lastprice', 0);
	});

	it('170105,查看上次成交价.rankA', async function () {
		await common.setGlobalParam('saleout_lastprice_strategy', 0);
		await common.setGlobalParam('saleorder_lastprice_strategy', 0);
		let jsonparam = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparam, {
			'price': 180
		});
		jsonparam.details = [_.cloneDeep(jsonparam.details[0])];
		jsonparam = format.jsonparamFormat(jsonparam);
		let firstSales = await common.editBilling(jsonparam);
		let lastPriceInfo = await reqHandler.csIFCHandler({
			interfaceid: 'cs-getsalebill-lastprice',
			shopid: jsonparam.shopid,
			dwid: jsonparam.dwid,
			styleid: jsonparam.details[0].styleid
		});
		expect(lastPriceInfo.result, `获取上次价接口有误`).to.includes({
			'price': '180'
		});
		let salesHistory = await reqHandler.qlIFCHandler({
			interfaceid: 'ql-144612',
			dwid: jsonparam.dwid,
			styleid: jsonparam.details[0].styleid,
			prodate1: common.getCurrentDate(),
			prodate2: common.getCurrentDate(),
		});
		// console.log(`salesHistory : ${JSON.stringify(salesHistory.result.dataList.length)}`);
		common.isApproximatelyEqualAssert({
			'invname': LOGINDATA.depname,
			'billno': firstSales.result.billno,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'seller': LOGINDATA.name,
			'styleid': jsonparam.details[0].styleid,
			'sizeid': jsonparam.details[0].sizeid,
			'colorid': jsonparam.details[0].colorid,
			'num': jsonparam.details[0].num,
			'price': jsonparam.details[0].price,
			'discount': jsonparam.details[0].discount,
		}, salesHistory.result.dataList[0]);

	});

	it('170106,看到上次成交记录.rankA', async function () {
		TESTCASE = {
			describe: '开单修改价格,验证上次成交记录',
		};
		await common.setGlobalParam('saleout_lastprice_strategy', 0);
		await common.setGlobalParam('saleorder_lastprice_strategy', 0);
		let jsonparam = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparam, {
			'price': 180
		}); //修改价格
		jsonparam = format.jsonparamFormat(jsonparam);
		let firstSales = await common.editBilling(jsonparam); //新增销售开单
		let historyRecords = await common.callInterface('ql-150061', {
			'dwid': jsonparam.dwid,
			'styleid': jsonparam.details[0].styleid
		}); //客户款号历史销售明细
		let actualList = [];
		historyRecords.dataList.forEach((obj) => {
			if (obj.billno == firstSales.result.billno) actualList.push(obj);
		});

		let mapfield = 'colorid=matColorId;sizeid=matSizeId;num;rem;price;discount;total=mat_money';
		let compare = compareLists(jsonparam.details, actualList, mapfield);
		expect(compare, `上次成交记录有误`).to.be.true;
	});

	it('170522,(aclist感叹号-更多)显示全部门店成交记录.rankA', async function () {
		// 中洲店登录，并开单
		await common.loginDo({
			'logid': '204',
			'pass': '000000',
		});
		let jsonparamZzd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamZzd, {
			'price': 170
		});
		jsonparamZzd.details = [_.cloneDeep(jsonparamZzd.details[0])];
		jsonparamZzd = format.jsonparamFormat(jsonparamZzd);
		let salesInZzd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamZzd
		});
		expect(salesInZzd, `中洲店销售开单失败:${JSON.stringify(salesInZzd)}`).to.not.have.property('error');

		// 常青店登录 ，查询并验证中洲店销售数据
		await common.loginDo();
		let salesHistory = await common.callInterface('ql-144612', format.qlParamsFormat({
			'dwid': jsonparamZzd.dwid,
			'styleid': jsonparamZzd.details[0].styleid,
			'pagesize': '15',
			'sortField': 'main_optime',
			'sortfieldOrder': '1'
		}));
		common.isApproximatelyEqualAssert({
			'invname': '中洲店',
			'billno': salesInZzd.billno,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'styleid': salesHistory.dataList[0].styleid,
			'sizeid': jsonparamZzd.details[0].sizeid,
			'colorid': jsonparamZzd.details[0].colorid,
			'num': jsonparamZzd.details[0].num,
			'price': jsonparamZzd.details[0].price,
			'discount': jsonparamZzd.details[0].discount
		}, salesHistory.dataList[0]);

		// 常青店开单
		let jsonparamCqd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamCqd, {
			'price': 170
		});
		jsonparamCqd.details = [_.cloneDeep(jsonparamCqd.details[0])];
		jsonparamCqd = format.jsonparamFormat(jsonparamCqd);
		let salesInCqd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamCqd
		});
		expect(salesInCqd, `常青店销售开单失败:${JSON.stringify(salesInCqd)}`).to.not.have.property('error');
		salesHistory = await common.callInterface('ql-144612', format.qlParamsFormat({
			'dwid': jsonparamCqd.dwid,
			'styleid': jsonparamCqd.details[0].styleid,
			'pagesize': '15'
		}));
		common.isApproximatelyEqualAssert({
			'invname': LOGINDATA.depname,
			'billno': salesInCqd.billno,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'seller': LOGINDATA.name,
			'styleid': salesHistory.dataList[0].styleid,
			'sizeid': jsonparamCqd.details[0].sizeid,
			'colorid': jsonparamCqd.details[0].colorid,
			'num': jsonparamCqd.details[0].num,
			'price': jsonparamCqd.details[0].price,
			'discount': jsonparamCqd.details[0].discount
		}, salesHistory.dataList[0]);
	});

	it('170529,按门店获取上次价.rankA', async function () {
		await common.setGlobalParam('sales_getlastprice_byshop', 0);
		// 常青店登录并开单
		await common.loginDo();
		let jsonparamCqd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamCqd, {
			'price': 180
		});
		jsonparamCqd = format.jsonparamFormat(jsonparamCqd);
		let salesInCqd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamCqd
		});
		expect(salesInCqd, `常青店销售开单失败:${JSON.stringify(salesInCqd)}`).to.not.have.property('error');

		// 中洲店登录，并开单
		await common.loginDo({
			'logid': '204',
			'pass': '000000',
		});
		let jsonparamZzd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamZzd, {
			'price': 170
		});
		jsonparamZzd = format.jsonparamFormat(jsonparamZzd);
		let salesInZzd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamZzd
		});
		expect(salesInZzd, `中洲店销售开单失败:${JSON.stringify(salesInZzd)}`).to.not.have.property('error');

		// 中洲店查询上次价
		let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': jsonparamZzd.shopid,
			'dwid': jsonparamZzd.dwid,
			'styleid': jsonparamZzd.details[0].styleid
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'price': '170'
		});

		// 常青店查询上次价
		await common.loginDo();
		lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': jsonparamCqd.shopid,
			'dwid': jsonparamCqd.dwid,
			'styleid': jsonparamCqd.details[0].styleid
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'price': '180'
		});
	});

	it('170530,按全局获取上次价.rankA', async function () {
		await common.setGlobalParam('sales_getlastprice_byshop', 1);
		// 常青店登录并开单
		await common.loginDo();
		let jsonparamCqd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamCqd, {
			'price': 180
		});
		jsonparamCqd = format.jsonparamFormat(jsonparamCqd);
		let salesInCqd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamCqd
		});
		expect(salesInCqd, `常青店销售开单失败:${JSON.stringify(salesInCqd)}`).to.not.have.property('error');

		// 中洲店登录，并开单
		await common.loginDo({
			'logid': '204',
			'pass': '000000',
		});
		let jsonparamZzd = basiceJsonparam.salesJson();
		changeDetailsInfo(jsonparamZzd, {
			'price': 170
		});
		jsonparamZzd = format.jsonparamFormat(jsonparamZzd);
		let salesInZzd = await common.callInterface('sf-14211-1', {
			'jsonparam': jsonparamZzd
		});
		expect(salesInZzd, `中洲店销售开单失败:${JSON.stringify(salesInZzd)}`).to.not.have.property('error');

		// 中洲店查询上次价
		let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': jsonparamZzd.shopid,
			'dwid': jsonparamZzd.dwid,
			'styleid': jsonparamZzd.details[0].styleid
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'price': '170'
		});

		// 常青店查询上次价
		await common.loginDo();
		lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': jsonparamCqd.shopid,
			'dwid': jsonparamCqd.dwid,
			'styleid': jsonparamCqd.details[0].styleid
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'price': '170'
		});
	});

	describe('销售订货', function () {

		after(async () => {
			await common.setGlobalParam('paymethod', 2);
		});

		it('160065,销售订货时可以自动获取上次订货价(不取上次销售价).rankA', async function () {
			await common.setGlobalParam('saleorder_lastprice_strategy', 0); //销售订货获取上次价策略	--	只获取上次订货价，默认
			await common.setGlobalParam('sales_getlastprice_byshop', 1); //按全局获取上次价

			await common.loginDo();
			let orderJson = basiceJsonparam.salesOrderJson();
			for (let i = 0; i < orderJson.details.length; i++) {
				orderJson.details[i].price = 170;
			}
			orderJson = format.jsonparamFormat(orderJson);
			await common.editBilling(orderJson); //新增销售订货

			let salesJson = basiceJsonparam.salesJson();
			for (let i = 0; i < salesJson.details.length; i++) {
				salesJson.details[i].price = 180;
			}
			salesJson = format.jsonparamFormat(salesJson);
			await common.editBilling(salesJson); //新增销售开单

			// 验证，希望获取到的是上次订货价，而不是上次销售价cs-getsaleorder-lastprice
			let lastPriceInfo;
			if (USEECINTERFACE == 1) {
				lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
					'shopid': LOGINDATA.invid,
					'dwid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
					'biztype': 150 //表示销售订货上次价
				});
			} else {
				lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', {
					'shopid': LOGINDATA.invid,
					'clientid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
				});
			}
			expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
				'price': '170'
			});
		});

		it('160078,产品折扣模式下自动取上次折扣值', async function () {
			await common.setGlobalParam('paymethod', 5);

			let orderJson = basiceJsonparam.salesOrderJson();
			for (let i = 0; i < orderJson.details.length; i++) {
				orderJson.details[i].discount = 0.855;
			}
			orderJson = format.jsonparamFormat(orderJson);
			let orderResult = await common.editBilling(orderJson);
			// 验证，希望获取到的是上次折扣
			let lastPriceInfo;
			if (USEECINTERFACE == 1) {
				lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
					'shopid': LOGINDATA.invid,
					'dwid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
					'biztype': 150 //表示销售订货上次价
				});
			} else {
				lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', {
					'shopid': LOGINDATA.invid,
					'clientid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
				});
			}
			expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
				'discount': '0.855'
			});
		});

		it('160079,客户折扣模式下自动取上次折扣值', async function () {
			await common.setGlobalParam('paymethod', 6);
			await common.setGlobalParam('discount_verify', 2);

			let orderJson = basiceJsonparam.salesOrderJson();
			orderJson.maindiscount = 0.955;
			orderJson = format.jsonparamFormat(orderJson);
			let orderResult = await common.editBilling(orderJson);
			// 验证，希望获取到的是上次折扣
			let lastPriceInfo;
			if (USEECINTERFACE == 1) {
				lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
					'shopid': LOGINDATA.invid,
					'dwid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
					'biztype': 150 //表示销售订货上次价
				});
			} else {
				lastPriceInfo = await common.callInterface('cs-getsaleorder-lastprice', {
					'shopid': LOGINDATA.invid,
					'clientid': BASICDATA.dwidXw,
					'styleid': orderJson.details[0].styleid,
				});
			}
			expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
				'discount': '0.955'
			});
		});
	});
});

// 修改details中的某些属性
function changeDetailsInfo(jsonparam, newInfo) {
	for (let i = 0; i < jsonparam.details.length; i++) {
		let item = jsonparam.details[i];
		for (let key in newInfo) {
			item[key] = newInfo[key];
		}
	}
}

/*
 * 比较两个数组是否相等
 */
function compareLists(expectList, actualList, mapfield) {
	let isRight = true;
	for (let i = 0; i < expectList.length; i++) {
		let isContine = false;
		for (let j = 0; j < actualList.length; j++) {
			if (actualList[j].mat_code == 'agc001') actualList[j].styleid = BASICDATA['styleidAgc001'];
			if (expectList[i].styleid == actualList[j].styleid && expectList[i].colorid == actualList[j].matColorId && expectList[i].sizeid == actualList[j].matSizeId) {
				isContine = common.isFieldsEqual(expectList[i], actualList[j], mapfield).flag;
				if (isContine) break;
			};
		};
		isRight = isRight && isContine;
	};

	return isRight;
}
