'use strict';
const common = require('../../../lib/common.js');
const caps = require('../../../data/caps');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 销售开单模块与在途调拨相关业务
 */

describe('在途调拨-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('170387,销售开单统计在途数', async function () {
		await common.setGlobalParam('sales_allinvnum', 1);

		// 调拨前查询agc001库存
		let beforeInvNumData = await common.callInterface('cs-getinvnum-bycolorsize', {
			'stylecode': 'agc001',
			'slh_version': caps.slhVersion
		});
		expect(beforeInvNumData, `调拨之前查询agc001库存失败:${JSON.stringify(beforeInvNumData)}`).to.not.have.property('error');

		//登录中洲店总经理登录，并做调拨操作
		await common.loginDo({
			'logid': '200',
			'pass': '000000',
		});
		let jsonparam = format.jsonparamFormat(basiceJsonparam.outJson());
		let outResult = await common.callInterface('sf-1862-1', {
			'jsonparam': jsonparam
		});
		expect(outResult, `调拨操作失败:${JSON.stringify(outResult)}`).to.not.have.property('error');

		// 常青店店长登录，并查询agc001库存
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		let afterInvNumData = await common.callInterface('cs-getinvnum-bycolorsize', {
			'stylecode': 'agc001',
			'slh_version': caps.slhVersion
		});
		expect(afterInvNumData, `调拨后查询agc001库存失败:${JSON.stringify(afterInvNumData)}`).to.not.have.property('error');

		// 断言
		// console.log(`\nbeforeInvNumData : ${JSON.stringify(beforeInvNumData)} \n afterInvNumData : ${JSON.stringify(afterInvNumData)}`);
		for (let i = 0; i < jsonparam.details.length; i++) {
			let name = jsonparam.details[i]['colorid'] + '_' + jsonparam.details[i]['sizeid'] + "_" + jsonparam.invidref;
			for (let j = 0; j < beforeInvNumData.dataList.length; j++) {
				let item = beforeInvNumData.dataList[j];
				if (item.name == name) {
					item.onroadnum = Number(jsonparam.details[i].num) + Number(item.onroadnum);
				}
			}
		}
		common.isApproximatelyEqual(beforeInvNumData, afterInvNumData);
	});
});
