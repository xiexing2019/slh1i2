'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
/*
 *  均色均码模式
 */
describe('均色均码-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('ignorecolorsize', 1); //均色均码
	});

	after(async () => {
		await common.setGlobalParam('ignorecolorsize', 0); //颜色尺码
	});

	it('170723.均色均码模式,库存数检查', async function () {
		let origInvnum = await getInvnumData(BASICDATA.styleid3035);
		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].styleid = BASICDATA.styleid3035;
		json.details[0].colorid = BASICDATA.coloridJunSe;
		json.details[0].sizeid = BASICDATA.sizeidJunMa;
		let sfRes = await common.editBilling(json);
		let curInvnum = await getInvnumData(BASICDATA.styleid3035);

		let name = `${BASICDATA.coloridJunSe}_${BASICDATA.sizeidJunMa}_${LOGINDATA.invid}`;
		for (let i = 0; i < curInvnum.dataList.length; i++) {
			if (curInvnum.dataList[i].name == name) {
				curInvnum.dataList[i].invnum = Number(curInvnum.dataList[i].invnum) + Number(json.details[0].num);
				break;
			};
		};
		common.isApproximatelyEqual(origInvnum, curInvnum);

		let qfRes = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		qfRes.result.action = 'edit';
		qfRes.result.details[0].num = '10';
		await common.editBilling(qfRes.result);
		curInvnum = await getInvnumData(BASICDATA.styleid3035);

		for (let i = 0; i < origInvnum.dataList.length; i++) {
			if (origInvnum.dataList[i].name == name) {
				origInvnum.dataList[i].invnum = Number(origInvnum.dataList[i].invnum) - 10;
				break;
			};
		};
		common.isApproximatelyEqual(origInvnum, curInvnum);
	});

});

//开单界面 获取货品库存信息
async function getInvnumData(styleid) {
	let param = {
		styleid: styleid,
	};
	let data = await common.callInterface('cs-getinvnum-bycolorsize', param);
	return data;
};
