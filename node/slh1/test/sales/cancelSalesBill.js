'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler');
const reqHandler = require('../../help/reqHandlerHelp');

let jsonparam;

describe("销售开单-作废-slh2", function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2); //开单模式2
	});

	it('170137.作废.rankA', async function () {
		//开单+作废
		let sfRes = await common.editBilling(basiceJsonparam.salesJson());
		await salesRequestHandler.cancelSaleoutBill(sfRes.result.pk);
		jsonparam = sfRes.params;
		jsonparam.invalidflag = 1;

		//单据验证
		let qfData = await salesRequestHandler.salesQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(jsonparam, qfData.result);

		let params = {
			'ql-142201': { //销售开单-按批次查
				billno1: sfRes.result.billno,
				billno2: sfRes.result.billno,
				invalidflag: 1,
			},
			'ql-1209': { //销售开单-按明细查
				billno: sfRes.result.billno,
			},
		};
		let res = await common.getResults(params);
		common.isApproximatelyEqualAssert(qfData.result, res['ql-142201'].dataList[0]);
		expect(res['ql-1209'].count, `按明细查可以查到作废单据，批次号为${sfRes.result.billno}。`).to.equal('0');
	});

	it('170023.销售单作废,物流商核销记录检查.rankA', async function () {
		TESTCASE = {
			describe: '检查物流商核销记录里,已作废的物流单是否还显示在待核销明细里',
			expect: '作废的物流单不会在物流商待核销明细里',
		};
		let sfRes = await common.editBilling(basiceJsonparam.agencySalesJson());
		await salesRequestHandler.cancelSaleoutBill(sfRes.result.pk);

		//销售开单-物流商核销-代收单选择
		let qlRes = await common.callInterface('ql-14521', format.qlParamsFormat({
			shopid: sfRes.params.shopid,
			logisPayflag: 0,
			dwid: sfRes.params.dwid,
			logisDwid: sfRes.params.logisDwid,
		}, true));
		for (let i = 0, length = qlRes.dataList.length; i < length; i++) {
			if (qlRes.dataList[i].id == sfRes.result.pk) {
				throw new Error(`ql-14521代收单查询结果中找到pk值为${sfRes.result.pk}的作废单据`);
				break;
			};
		};
	});

	it('170006.按批次查-作废条件检查+汇总值', async function () {
		let param = format.qlParamsFormat({
			invalidflag: 0,
		}, true);
		let qlRes = await common.callInterface('ql-142201', param);
		invalidflagCheck(qlRes, 0);

		param.invalidflag = 1;
		qlRes = await common.callInterface('ql-142201', param);
		invalidflagCheck(qlRes, 1);
	});

	describe('170776/170777.修改日志(作废记录)', function () {
		it('1.销售开单', async function () {
			await editInfoCheck('sf-14211-1', 'cs-cancel-saleout-bill', 'qf-14211-3');
		});
		it('2.销售订货', async function () {
			await editInfoCheck('sf-14401-1', 'cs-saleorder-cancel', 'qf-14401-2');
		});
		it('3.采购入库', async function () {
			await editInfoCheck('sf-14212-1', 'cs-cancel-pured-bill', 'qf-14212-3');
		});
		it('4.采购订货', async function () {
			await editInfoCheck('sf-22101-1', 'cs-22102', 'qf-22101-5');
		});
	});
});

describe('SLH-24374.开启退货验证时，作废单据需要验证退货数', function () {
	this.timeout(30000);
	let addCustRes, salesBillRes;
	before('参数设置、新增客户', async function () {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sc_cancelCheckNum', 1);   //作废单据时是否验证退货数超过拿货数	是
		await common.setGlobalParam('sales_checkbacknum', 1);  //开单保存开启退货数和上次购买数的比对验证 是

		let saveCustJson = basiceJsonparam.addCustJson();
		addCustRes = await salesRequestHandler.addCust(saveCustJson);   //添加一个客户 

		let salesJson = basiceJsonparam.simpleSalesJson();
		salesJson.dwid = addCustRes.result.val;   //用刚新增的客户开单
		salesBillRes = await common.editBilling(salesJson);

		//退货
		let backJson = _.cloneDeep(salesBillRes.params);
		backJson.details.forEach(val => val.num = -val.num); //数量变为负数,模拟退货
		await common.editBilling(backJson);
	});

	it('作废之前的单子', async function () {
		let salesInfo = await salesRequestHandler.salesQueryBilling(salesBillRes.result.pk);
		let errorMsg = `款号【${salesInfo.result.details[0].show_styleid},${salesInfo.result.details[0].show_colorid},${salesInfo.result.details[0].show_sizeid}】退货数量高于拿货总数量，请核对`;
		let cancelRes = await salesRequestHandler.cancelSaleoutBill(salesBillRes.result.pk, false);
		expect(cancelRes, `作废单据时是否验证退货数超过拿货数开启，但是作废时没有验证`).to.includes({ 'error': `${errorMsg}` });
	});

	it('再新增一个包含此款号的单子,然后作废单子', async function () {
		let salesJson = basiceJsonparam.simpleSalesJson();
		salesJson.dwid = addCustRes.result.val;   //用刚新增的客户开单
		await common.editBilling(salesJson);
		await salesRequestHandler.cancelSaleoutBill(salesBillRes.result.pk);   //此方法自带断言，所以不用再次进行断言判断了
	});

	after('参数还原', async function () {
		await common.setGlobalParam('sc_cancelCheckNum', 0);
		await common.setGlobalParam('sales_checkbacknum', 0);
	});
});

//作废条件检查
function invalidflagCheck(res, invalidflag) {
	expect(res.dataList).to.satisfy(function (arr) {
		if (arr.length == 0) return false;
		let ret = true;
		arr.map((obj) => {
			ret = ret && obj.invalidflag == invalidflag;
		});
		return ret;
	}, `invalidflag=${invalidflag}时，查询结果出错`);
};

/**
 * 修改日志(作废记录)检查
 * @param {string} sfIFC 开单接口
 * @param {string} cancelIFC 作废接口
 * @param {string} qfIFC 查看修改日志接口
 */
async function editInfoCheck(sfIFC, cancelIFC, qfIFC) {
	let json = basiceJsonparam.simpleSalesJson();
	json.interfaceid = sfIFC;
	delete json.dwid;
	let sfRes = await common.editBilling(json);

	let editLog = await common.callInterface(qfIFC, {
		pk: sfRes.result.pk
	});
	let exp = {
		deloptime: '',
		delopstaffName: '',
	};
	if (USEECINTERFACE == 1) {
		common.isApproximatelyEqualAssert(exp, editLog, [], `作废前,查看修改日志${qfIFC} deloptime,delopstaffName字段有误`);
	} else {
		expect(editLog, `作废前,查看修改日志${qfIFC} deloptime,delopstaffName字段有误`).not.to.have.keys(Object.keys(exp));
	};

	await reqHandler.csIFCHandler({
		'interfaceid': cancelIFC,
		'pk': sfRes.result.pk,
	});
	if (USEECINTERFACE == 2) common.delay(2000);

	let optime = common.getCurrentDate('YYYY-MM-DD HH:mm:ss');
	editLog = await common.callInterface(qfIFC, {
		pk: sfRes.result.pk
	});
	common.isApproximatelyEqualAssert({
		deloptime: optime,
		delopstaffName: LOGINDATA.name,
	}, editLog, [], `作废后,查看修改日志${qfIFC} deloptime,delopstaffName字段有误`);
};