'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 销售开单--按订货配货后开单
 */

describe('按订货配货后开单--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('sales_order_deliver_mode', 2);
		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('paymethod', 2);
	});

	after(async () => {
		await common.setGlobalParam('sales_order_deliver_mode', 1);
		await common.setGlobalParam('paymethod', 2);
	});

	it('170844,【销售开单-按订货配货】按配货开单时检查单价和折扣值.rankA', async function () {
		await common.setGlobalParam('sales_allow_discount_morethan1', 1); //允许折扣大于1
		let orderData = await getPrepareDataForDistribute();
		let orderJson = orderData.jsonparam;
		orderJson.details = [_.cloneDeep(orderJson.details[0])];

		// 第一次订货 price:100 discount:1.1
		orderJson.details[0].price = 100;
		orderJson.details[0].discount = 1.1;
		let orderResult = await common.editBilling(format.jsonparamFormat(_.cloneDeep(orderJson)));

		// 第二次订货 price:200 discount:1.2
		orderJson.details[0].price = 200;
		orderJson.details[0].discount = 1.2;
		orderResult = await common.editBilling(format.jsonparamFormat(_.cloneDeep(orderJson)));

		await common.setGlobalParam('sales_allow_discount_morethan1', 0); //把参数值修改回去，放在影响其他用例
		//汇总展现该款号所有客户的订货数据
		let orderInfoSummary = await prepareOrderDistribute(orderData.styleid);
		// 根据汇总的订货数据给各个客户配货
		await saveOrderDistribute(orderData.styleid, orderInfoSummary);

		// 查看配货结果
		let qfDistribution = await common.callInterface('qf-1427-saleout_dres', {
			action: 'add',
			invid: LOGINDATA.invid,
			pk: orderData.clientid
		});
		common.isApproximatelyEqualAssert({
			"price": "200",
			"discount": "1.2"
		}, qfDistribution.details[0]);

	});

	it('170845,按配货开单后检查界面上订单的刷新情况.rankA', async function () {
		let data = await getPrepareDataForDistribute();

		let sfResOrder = await common.editBilling(data.jsonparam); //新增订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);

		let prepareData = await prepareOrderDistribute(data.styleid); //获取按款号配货界面数据
		let saveResult = await saveOrderDistribute(data.styleid, prepareData); //保存订单配货数据
		let distributenum = 0;
		saveResult.params.listData.map((obj) => {
			distributenum += Number(obj.num);
		});

		let param = format.qlParamsFormat({
			clientid: data.clientid
		}, false);
		let result = await common.callInterface('ql-1427', param); //配货汇总信息
		common.isApproximatelyEqualAssert({
			id: data.clientid,
			invid: LOGINDATA.invid,
			invname: LOGINDATA.invname,
			dwname: qfResOrder.result.show_clientid,
			prodate: common.getCurrentDate('YY-MM-DD'),
			distributenum: distributenum.toString(),
			opname: LOGINDATA.name
		}, result.dataList[0]);

		let sfRes = await addBillByDistribute(data.clientid); //按配货开单
		result = await common.callInterface('ql-1427', param); //配货开单后 查询界面重新查询
		expect(result.dataList.length, '已发完货的订单在按配货开单界面依然显示').to.equal(0);
	});

	it('170846,【销售开单-按订货配货】整单折扣模式下保存时检查结果', async function () {
		await common.setGlobalParam('paymethod', 7);
		let orderData = await getPrepareDataForDistribute();
		orderData.jsonparam.maindiscount = '1';

		// 订货
		let orderRes = await common.editBilling(orderData.jsonparam);

		//汇总展现该款号所有客户的订货数据
		let orderInfoSummary = await prepareOrderDistribute(orderData.styleid);
		// 根据汇总的订货数据给各个客户配货
		await saveOrderDistribute(orderData.styleid, orderInfoSummary);

		// 查看配货结果
		let qfDistribution = await common.callInterface('qf-1427-saleout_dres', {
			action: 'add',
			invid: LOGINDATA.invid,
			pk: orderData.clientid
		});
		expect(qfDistribution, `qf-1427-saleout_dres查看配货结果错误:${JSON.stringify(qfDistribution)}`).to.not.have.property('error');

		qfDistribution.maindiscount = '0.8';
		qfDistribution.interfaceid = 'sf-14211-1';
		qfDistribution.srcType = '3';
		for (let i = 0; i < qfDistribution.details.length; i++) {
			delete qfDistribution.details[i].realprice;
		}
		let jsonparam = format.jsonparamFormat(qfDistribution);
		await common.editBilling(qfDistribution); //按配货开单
		await common.setGlobalParam('paymethod', 2);
	});

	it('170848,更新配货单.rankA', async function () {
		let data = await getPrepareDataForDistribute();
		data.jsonparam.details[0].num = 10;
		data.jsonparam.details[1].num = 15;

		let sfResOrder = await common.editBilling(data.jsonparam); //新增订单
		let prepareData = await prepareOrderDistribute(data.styleid); //获取按款号配货界面数据
		await saveOrderDistribute(data.styleid, prepareData); //保存订单配货数据

		prepareData = await prepareOrderDistribute(data.styleid); //重新获取按款号配货界面数据
		let curSaveResult = await saveOrderDistribute(data.styleid, prepareData); //更新配货单
		let distributenum = 0;
		curSaveResult.params.listData.map((obj) => {
			distributenum += Number(obj.num);
		});

		let param = format.qlParamsFormat({
			clientid: data.clientid
		}, false);
		let result = await common.callInterface('ql-1427', param); //配货汇总信息
		common.isApproximatelyEqualAssert({
			id: data.clientid,
			invid: LOGINDATA.invid,
			invname: LOGINDATA.invname,
			distributenum: distributenum,
		}, result.dataList[0]);
	});

	it('170861.核销.rankA', async function () {
		let data = await getPrepareDataForDistribute();
		let sfResOrder = await common.editBilling(data.jsonparam); //新增订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk);

		let prepareData = await prepareOrderDistribute(data.styleid); //获取按款号配货界面数据
		let saveResult = await saveOrderDistribute(data.styleid, prepareData); //保存订单配货数据

		let qfResult = await common.callInterface('qf-1427-saleout_dres', { //获取配货汇总数据转开单数据
			action: 'add',
			invid: LOGINDATA.invid,
			pk: data.clientid
		});

		let verifyResult = await salesRequest.getVerifyingBills(data.clientid); //获取核销界面信息

		qfResult.interfaceid = 'sf-14211-1';
		qfResult.action = 'add';
		qfResult.pk = data.clientid;
		qfResult.srcType = '3';
		qfResult.finpayVerifybillids = verifyResult.dataList[0].id; //核销订单-预付款
		qfResult.finpayVerifysum = verifyResult.dataList[0].balance;
		await common.editBilling(qfResult); //保存成功，正常核销
	});

	it('170977.总经理按配货开单界面仅显示本门店数据.rankA', async function () {
		let distributList = await common.callInterface('ql-1427', format.qlParamsFormat({}, false));
		distributList.dataList.map((obj) => {
			expect(obj.invname).to.equal(LOGINDATA.invname);
		});
	});

	describe('170849,【销售开单-按订货配货-按配货开单】颜色尺码模式.rankA', function () {

		it('同一款号，给不同客户配货', async function () {
			// 新增第一个客户以及款号,并开订货单
			let orderData = await getPrepareDataForDistribute();
			let orderJson = orderData.jsonparam;
			orderJson.details = [_.cloneDeep(orderJson.details[0])];
			let orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson))
			});
			expect(orderResult, `销售订货失败1:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			// 新增第二个客户,并开订货单
			let cust2 = await salesRequest.addCust(basiceJsonparam.addCustJson());
			expect(cust2.result, `新增客户失败:${JSON.stringify(cust2)}`).to.have.property('val');
			orderJson.clientid = cust2.result.val;
			orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson))
			});
			expect(orderResult, `销售订货失败2:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			// 配货开单前查询按配货开单列表
			let distributListBef = await common.callInterface('ql-1427', format.qlParamsFormat({}, false));
			expect(distributListBef, `按配货开单列表查询有误:${JSON.stringify(distributListBef)}`).to.not.have.property('error');

			//汇总展现该款号所有客户的订货数据
			let orderInfoSummary = await prepareOrderDistribute(orderData.styleid);
			// 根据汇总的订货数据给各个客户配货
			await saveOrderDistribute(orderData.styleid, orderInfoSummary);

			// 配货开单后查询按配货开单列表
			let distributList = await common.callInterface('ql-1427', format.qlParamsFormat({}, false));

			// 验证按配货列表 是否将不同客户相同款号的配货数据分开统计
			expect(distributList, `按配货开单列表查询有误:${JSON.stringify(distributList)}`).to.not.have.property('error');
			expect(distributList, `同一款号，给不同客户配货后，按配货开单列表没有新增响应客户的按配货开单列表数据`).to.includes({
				'count': (Number(distributListBef.count) + 2).toString()
			});
		});

		it('不同款号，给同一客户配货', async function () {
			// 新增第一个款号、客户，并开订货单
			let orderData = await getPrepareDataForDistribute();
			let orderJson = orderData.jsonparam;
			orderJson.details = [_.cloneDeep(orderJson.details[0])];
			let orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(orderJson)
			});
			expect(orderResult, `销售订货失败1:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			// 新增第二个款号，并开订货单
			let orderJson2 = _.cloneDeep(orderJson);
			let good2 = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson()
			}); //新增货品 无订货记录
			orderJson2.details[0].styleid = good2.pk;
			orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson2))
			});
			expect(orderResult, `销售订货失败2:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			// 配货开单前查询按配货开单列表
			let distributListBef = await common.callInterface('ql-1427', format.qlParamsFormat({}, false));
			expect(distributListBef, `按配货开单列表查询有误:${JSON.stringify(distributListBef)}`).to.not.have.property('error');

			//第一个款号 ：汇总展现该款号所有客户的订货数据 并 根据汇总的订货数据给各个客户配货
			let orderInfoSummary1 = await prepareOrderDistribute(orderData.styleid);
			let distributResult1 = await saveOrderDistribute(orderData.styleid, orderInfoSummary1);

			//第二个款号 ：汇总展现该款号所有客户的订货数据 并 根据汇总的订货数据给各个客户配货
			let orderInfoSummary2 = await prepareOrderDistribute(good2.pk);
			let distributResult2 = await saveOrderDistribute(good2.pk, orderInfoSummary2);

			// 配货开单后查询按配货开单列表
			let distributList = await common.callInterface('ql-1427', format.qlParamsFormat({}, false));

			// 1、验证按配货列表 是否将相同客户不同款号的配货数据汇总统计
			expect(distributList, `按配货开单列表查询有误:${JSON.stringify(distributList)}`).to.not.have.property('error');
			expect(distributList, `同一款号，给不同客户配货后，按配货开单列表没有新增响应客户的按配货开单列表数据`).to.includes({
				'count': (Number(distributListBef.count) + 1).toString()
			});

			//2、验证按配货列表各字段的正确性
			distributList = await common.callInterface('ql-1427', format.qlParamsFormat({
				'clientid': orderData.clientid
			}, false));
			common.isApproximatelyEqualAssert({
				'id': orderData.clientid,
				'invid': LOGINDATA.invid,
				'invname': LOGINDATA.depname,
				'distributenum': common.add(distributResult1.params.listData[0].num, distributResult2.params.listData[0].num),
				'prodate': common.getCurrentDate('YY-MM-DD'),
				'opname': LOGINDATA.name
			}, distributList.dataList[0]);

			// 3、验证按配货列表点进去的明细数据主要字段的正确性
			let distributInfo = await common.callInterface('qf-1427-saleout_dres', {
				action: 'add',
				invid: LOGINDATA.invid,
				pk: orderData.clientid
			});
			let expectDistributInfo = getDistributeInfo(distributResult1.params, distributResult2.params);
			common.isApproximatelyEqualAssert(expectDistributInfo, distributInfo);
		});

		let ordersInfo, salesInfo, clientid;
		it('发货优先级是按照先订先发的规则，且单据状态根据是否完全发货显示‘部分发货’、‘全部发货’、‘未发货’', async function () {
			// 新增客户以及款号,并开订货单
			let orderData = await getPrepareDataForDistribute();
			clientid = orderData.clientid;
			let orderJson = orderData.jsonparam;
			orderJson.details = [_.cloneDeep(orderJson.details[0])];
			orderJson.details[0].num = 3;
			let orderResult1 = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson))
			});
			expect(orderResult1, `销售订货失败1:${JSON.stringify(orderResult1)}`).to.not.have.property('error');
			await common.delay(1000);
			// 第二次销售订货
			let orderResult2 = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson))
			});
			expect(orderResult2, `销售订货失败2:${JSON.stringify(orderResult2)}`).to.not.have.property('error');
			await common.delay(1000);
			// 第三次销售订货
			let orderResult3 = await common.callInterface('sf-14401-1', {
				'jsonparam': format.jsonparamFormat(_.cloneDeep(orderJson))
			});
			expect(orderResult3, `销售订货失败3:${JSON.stringify(orderResult3)}`).to.not.have.property('error');

			// 展示该款号所有客户的订货数据
			let distributData = await prepareOrderDistribute(orderData.styleid);
			await saveOrderDistribute(orderData.styleid, distributData, 5);
			let distributeResult = await addBillByDistribute(clientid);
			salesInfo = distributeResult.result;
			ordersInfo = {
				'orderResult1': orderResult1,
				'orderResult2': orderResult2,
				'orderResult3': orderResult3,
			};

			let searchCondition = {
				'id1': orderResult1.billno,
				'id2': orderResult1.billno,
				'clientid': clientid,
				'shopid': LOGINDATA.invid,
			};
			let orderList1 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			searchCondition.id1 = orderResult2.billno;
			searchCondition.id2 = orderResult2.billno;
			let orderList2 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			searchCondition.id1 = orderResult3.billno;
			searchCondition.id2 = orderResult3.billno;
			let orderList3 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			// 验证发货顺序以及发货状态
			expect(orderList1.dataList[0], `发货优先级是按照先订先发的规则，且单据状态根据是否完全发货显示'部分发货'、'全部发货'、'未发货',验证结果错误`).to.includes({
				"flag": "全部发货"
			});
			expect(orderList2.dataList[0], `发货优先级是按照先订先发的规则，且单据状态根据是否完全发货显示'部分发货'、'全部发货'、'未发货',验证结果错误`).to.includes({
				"flag": "部分发货"
			});
			expect(orderList3.dataList[0], `发货优先级是按照先订先发的规则，且单据状态根据是否完全发货显示'部分发货'、'全部发货'、'未发货',验证结果错误`).to.includes({
				"flag": "未发货"
			});
		});

		// 紧接着上一条用例 “发货优先级是按照先订先发的规则，且单据状态根据是否完全发货显示‘部分发货’、‘全部发货’、‘未发货’”
		it('销售开单-按批次查，作废-按配货开单生成的销售单', async function () {
			if (!ordersInfo || !salesInfo || !clientid) {
				expect(true, `本条用例是基于上一条订货发货优先级别结果的，但是上一条用例失败了，这条也就无法验证了`).to.be.false;
			}

			// 作废按配货开的销售单
			let disableResult = await common.callInterface('cs-cancel-saleout-bill', {
				'pk': salesInfo.pk
			});
			expect(disableResult, `作废按配货开的销售单失败:${JSON.stringify(disableResult)}`).to.not.have.property('error');

			let searchCondition = {
				'id1': ordersInfo.orderResult1.billno,
				'id2': ordersInfo.orderResult1.billno,
				'clientid': clientid,
				'shopid': LOGINDATA.invid,
			};
			let orderList1 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			searchCondition.id1 = ordersInfo.orderResult2.billno;
			searchCondition.id2 = ordersInfo.orderResult2.billno;
			let orderList2 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			searchCondition.id1 = ordersInfo.orderResult3.billno;
			searchCondition.id2 = ordersInfo.orderResult3.billno;
			let orderList3 = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
			// 验证发货发货状态
			expect(orderList1.dataList[0], `按配货开的销售单已作废，但是对应的销售订单发货状态没有恢复,验证结果错误`).to.includes({
				"flag": "未发货"
			});
			expect(orderList2.dataList[0], `按配货开的销售单已作废，但是对应的销售订单发货状态没有恢复,验证结果错误`).to.includes({
				"flag": "未发货"
			});
			expect(orderList3.dataList[0], `按配货开的销售单已作废，但是对应的销售订单发货状态没有恢复,验证结果错误`).to.includes({
				"flag": "未发货"
			});

			// 验证按配货开的列表响应客户数据是否恢复
			let distributList = await common.callInterface('ql-1427', format.qlParamsFormat({
				'clientid': clientid
			}, false));
			expect(Number(distributList.count) == 1, `验证按配货开的列表响应客户数据是否恢复`).to.be.true;

			// 验证恢复的按配货开单数据可以接着开单
			let distributResult = await addBillByDistribute(clientid);
			expect(distributResult.result, `恢复的按配货开单数据接着开单失败:${JSON.stringify(distributResult)}`).to.not.have.property('error');
		});

	});

});

//获取按订货配货的准备数据
//新增客户id,新增款号id,销售订单的jsonparam
async function getPrepareDataForDistribute() {
	let custRes = await salesRequest.addCust(basiceJsonparam.addCustJson());
	let goodRes = await basicReqHandler.editStyle({
		jsonparam: basiceJsonparam.addGoodJson()
	}); //新增货品 无订货记录

	let json = basiceJsonparam.salesOrderJson();
	json.clientid = custRes.result.val;
	json.details[0].styleid = goodRes.pk;
	json.details[1].styleid = goodRes.pk;
	return {
		clientid: custRes.result.val,
		styleid: goodRes.pk,
		jsonparam: json,
	};
};

//准备订单配货页面数据
async function prepareOrderDistribute(styleid) {
	let param = {
		styleid: styleid,
		invid: LOGINDATA.invid
	};
	let result = await common.callInterface('cs-prepareOrderDistribute', param);

	expect(result, `cs-prepareOrderDistribute错误:${JSON.stringify(result)}`).to.not.have.property('error');

	return result;
};

//保存订单配货数据
async function saveOrderDistribute(styleid, prepareData, distributNum = 0) {
	let jsonparam = {
		invid: LOGINDATA.invid,
		styleid: styleid
	};

	let listData = [];
	for (let key in prepareData.mapData) {
		let arr = prepareData.mapData[key];
		let [colorid, sizeid] = key.split('-');
		arr.map((obj) => { //拼接不同客户的配货信息
			obj.colorid = colorid;
			obj.sizeid = sizeid;
			obj.num = distributNum ? distributNum : Math.floor(Math.random() * Number(obj.ordernum) + 1);
			listData.push(obj);
		});
	};
	jsonparam.listData = listData;
	let result = await common.callInterface('cs-saveOrderDistribute', {
		jsonparam: jsonparam
	});
	expect(result, `cs-saveOrderDistribute错误:${JSON.stringify(result)}`).to.includes({
		"val": "ok"
	});

	return {
		params: jsonparam,
		result
	};
};

//按配货开单
async function addBillByDistribute(clientid) {
	let qfResult = await common.callInterface('qf-1427-saleout_dres', {
		action: 'add',
		invid: LOGINDATA.invid,
		pk: clientid
	});

	qfResult.interfaceid = 'sf-14211-1';
	qfResult.action = 'add';
	qfResult.pk = clientid;
	qfResult.srcType = '3';
	qfResult.typeid = '21';
	//console.log(`qfResult : ${JSON.stringify(qfResult)}`);
	let sfRes = await common.editBilling(qfResult);
	return sfRes;
};

/*
 * 不同货品同一个客户，根据各个货品的配货信息，组装成一个配货明细
 * 即qf-1427-saleout_dres返回的内容
 *
 * 注意这不是一个通用方法，参数要求
 * distribut1，distribut2的listData都只有一个元素
 * 且listData中的clientid相同
 */
function getDistributeInfo(distribut1, distribut2) {
	if (distribut1.listData[0].clientid != distribut2.listData[0].clientid) {
		return undefined;
	}

	let distributeInfo = {
		"show_shopid": LOGINDATA.depname,
		"shopid": LOGINDATA.invid,
		"details": [{
			"sizeid": distribut1.listData[0].sizeid,
			"colorid": distribut1.listData[0].colorid,
			"num": distribut1.listData[0].num,
			"styleid": distribut1.styleid,
		}, {
			"sizeid": distribut2.listData[0].sizeid,
			"colorid": distribut2.listData[0].colorid,
			"num": distribut2.listData[0].num,
			"styleid": distribut2.styleid,
		}],
		"deliver": LOGINDATA.id,
		"totalnum": (Number(distribut1.listData[0].num) + Number(distribut2.listData[0].num)).toString(),
		"prodate": common.getCurrentDate(),
		"dwid": distribut1.listData[0].clientid
	};

	return distributeInfo;
};
