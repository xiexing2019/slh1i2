'use strict';

const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 销售订货--新增、修改、作废等业务验证
 */

describe('销售订货-slh2--mianLine', function () {
	this.timeout(30000);

	let agc002Info;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		agc002Info = await common.fetchMatInfo(BASICDATA.styleidAgc002);
	});

	it('160047.新增订货单.rankA', async function () {
		let orderRes = await common.editBilling(basiceJson.salesOrderJson());

		let batchInfo = await common.callInterface('ql-14433', format.qlParamsFormat({
			'clientid': orderRes.params.clientid,
			'id1': orderRes.result.billno,
			'id2': orderRes.result.billno,
			'shopid': orderRes.params.shopid,
		}, true));
		expect(batchInfo, `销售订货按批次查有误:${JSON.stringify(batchInfo)}`).to.not.have.property('error');
		common.isApproximatelyEqualAssert(orderRes.params, batchInfo.dataList[0]);
		// common.isApproximatelyEqualAssert(orderRes.result, batchInfo.dataList[0]);
	});

	it('160049.订单修改界面内容检查.rankA', async function () {
		let orderRes = await common.editBilling(basiceJson.salesOrderJson());

		let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);
		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);
	});

	it('160052,【销售订货－新增订货】开单日期检查', async function () {
		let orderJson = basiceJson.salesOrderJson();
		orderJson.prodate = common.getDateString([0, 0, -2]);
		let orderRes = await common.editBilling(orderJson);

		let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);
		expect(orderInfo.result, `销售订货-按批次查-点击明细有误:${JSON.stringify(orderInfo)}`).to.not.have.property('error');
		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);
	});

	it('160020,【销售订货-按明细查】作废订单后内容检查', async function () {
		TESTCASE = {
			describe: "作废的订货单据，在按批次查中显示为作废",
			expect: "ql-14433接口返回的invalidflag='1'",
		}
		let orderRes = await common.editBilling(basiceJson.salesOrderJson());
		await salesOrderReqHandler.cancelSaleorderBill(orderRes.result.pk); //作废单据

		//销售订货-按批次查
		let searchCondition = {
			'clientid': orderRes.params.clientid,
			'id1': orderRes.result.billno,
			'id2': orderRes.result.billno,
			'shopid': orderRes.params.shopid
		};
		let bacthList = await common.callInterface('ql-14433', format.qlParamsFormat(searchCondition, true));
		expect(bacthList.dataList[0], `作废后按批次查的单据作废状态有误`).to.includes({
			invalidflag: '1'
		});
		//销售订货-按明细查
		TESTCASE.describe = "作废的订货单据，在按明细查查不到";
		TESTCASE.expect = "ql-14431接口返回的count为0";
		TESTCASE.jira = "SLH-23005 批次号查询条件失效，已关闭";
		searchCondition = {
			'id1': orderRes.result.billno,
			'id2': orderRes.result.billno,
			'mainClientid': orderRes.params.clientid,
			'mainShopid': orderRes.params.shopid,
			'propdresStyleid': orderRes.params.details[0].styleid
		};
		let detailList = await common.callInterface('ql-14431', format.qlParamsFormat(searchCondition, true));
		expect(detailList.count, `销售订货按明细查有误`).to.be.equal('0');

	});

	it('160109,【销售订货-新增订货】跨门店修改销售订货单.rankA', async function () {
		await common.setGlobalParam('scm_modify_bill_over_shop', 0); //不允许跨门店修改

		// 中洲店登录新增销售订货
		await common.loginDo({
			'logid': '200',
			'pass': '000000'
		});
		let orderRes = await common.editBilling(basiceJson.salesOrderJson());

		// 常青店登录修改销售订单
		await common.loginDo();
		let qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);
		qfRes.result.remark = '跨门店修改销售订货单';
		orderRes = await salesOrderReqHandler.editSalesOrderBill(qfRes.result, false); //修改销售订单
		expect(orderRes.result, `跨门店修改销售订货成功，但是按参数设置应该不允许跨门店修改`).to.includes({
			error: '不允许修改其它门店的单据'
		});

		//允许跨门店修改
		await common.setGlobalParam('scm_modify_bill_over_shop', 1);
		orderRes = await salesOrderReqHandler.editSalesOrderBill(qfRes.result, false); //修改销售订单
		expect(orderRes.result, `跨门店修改销售订货失败，但是按参数设置应该允许跨门店修改`).to.not.have.property('error');
	});

	it('160096,销售订货-按明细查页面已发数汇总与销售订货-按批次查页面已发数对比', async function () {
		await common.setGlobalParam('sales_order_deliver_mode', 1);

		let orderJson = format.jsonparamFormat(basiceJson.specialSalesOrderJson());
		let orderResult = await common.callInterface('sf-14401-1', {
			'jsonparam': orderJson
		});
		expect(orderResult, `销售订货失败:${JSON.stringify(orderResult)}`).to.not.have.property('error');
		// 按订货开单qf页面
		let orderBillingInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderResult.pk);
		expect(orderBillingInfo.result, `按订货开单详情查询失败:${JSON.stringify(orderBillingInfo)}`).to.not.have.property('error');
		// 按订货开单
		let salesByOrder = await salesReqHandler.editSalesBillByOrder(orderBillingInfo.result);

		let searchCondition = {
			'clientid': orderJson.clientid,
			'shopid': orderJson.shopid
		};
		let bacthList = await common.callInterface('ql-14433', format.qlOnlySumFormat(searchCondition, true));

		searchCondition = {
			'mainClientid': orderJson.clientid,
			'mainShopid': orderJson.shopid
		};
		let detailList = await common.callInterface('ql-14431', format.qlOnlySumFormat(searchCondition, true));
		expect(Number(bacthList.sumrow.recvnum), `错误`).to.be.equal(Number(detailList.sumrow.delivernum));
	});

	it('160077,【销售订货－新增订货】加工款按订货开单', async function () {
		if (agc002Info.hasOwnProperty('error')) {
			expect(true, `获取加工款agc002失败，无法进行160077用例的验证`).to.be.false;
		}
		await common.setGlobalParam('sales_order_deliver_mode', 1);
		let orderJson = basiceJson.salesOrderJson();
		for (let i = 0; i < orderJson.details.length; i++) {
			orderJson.details[i].styleid = agc002Info.pk;
			orderJson.details[i].price = agc002Info.stdprice1;
		}
		orderJson = format.jsonparamFormat(orderJson);
		let orderResult = await common.callInterface('sf-14401-1', {
			'jsonparam': orderJson
		});
		expect(orderResult, `销售订货失败:${JSON.stringify(orderResult)}`).to.not.have.property('error');

		// 按订货开单qf页面
		let orderBillingInfo = await salesReqHandler.salesOrderQueryBilling(orderResult.pk);
		expect(orderBillingInfo.result, `按订货开单详情查询失败:${JSON.stringify(orderBillingInfo)}`).to.not.have.property('error');
		// 按订货开单
		await salesReqHandler.editSalesBillByOrder(orderBillingInfo.result);
	});

	it('160064,【销售订货-新增订货】检查历史订货时间', async function () {
		await common.setGlobalParam('sales_order_deliver_mode', 1);

		let orderJson = format.jsonparamFormat(basiceJson.salesOrderJson());
		orderJson.prodate = common.getDateString([0, 0, -2]);
		let orderResult = await common.callInterface('sf-14401-1', {
			'jsonparam': orderJson
		});
		expect(orderResult, `销售订货失败:${JSON.stringify(orderResult)}`).to.not.have.property('error');

		// 按订货开单qf页面
		let orderBillingInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderResult.pk);
		orderBillingInfo.result.prodate = common.getDateString([0, 0, 0]);
		expect(orderBillingInfo.result, `按订货开单详情查询失败:${JSON.stringify(orderBillingInfo)}`).to.not.have.property('error');
		// 按订货开单
		await salesReqHandler.editSalesBillByOrder(orderBillingInfo.result);

		// 验证，希望开单后订货日期还是两天前，不能因为今天做的按订货开单，就将订货日期改为今天了
		let orderList = await common.callInterface('ql-14433', format.qlParamsFormat({
			'clientid': orderJson.clientid,
			'id1': orderResult.billno,
			'id2': orderResult.billno,
			'shopid': orderJson.shopid,
		}, false));
		expect(orderList.dataList[0], `按订货开单后，销售订货--按批次列表订货日期错误`).to.includes({
			'prodate': orderJson.prodate.substr(5)
		});
	});

	it('160080,【销售订货-新增订货】检查订单数值-整单折扣模式.rankA', async function () {
		await common.setGlobalParam('paymethod', 7);

		let orderJson = basiceJson.salesOrderJson();
		orderJson.maindiscount = '0.855';
		let orderRes = await common.editBilling(orderJson);

		let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);

		await common.setGlobalParam('paymethod', 2);

		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);
	});

	it('160076,【销售订货-新增订货】折扣模式下输入过大数量', async function () {
		await common.setGlobalParam('paymethod', 7);

		let orderJson = basiceJson.salesOrderJson();
		orderJson.maindiscount = '0.855';
		for (let i = 0; i < orderJson.details.length; i++) {
			orderJson.details[i].num = 30000;
		}
		let orderRes = await common.editBilling(orderJson);

		let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);

		// 订单作废
		await salesOrderReqHandler.cancelSaleorderBill(orderRes.result.pk);
		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);

		await common.setGlobalParam('paymethod', 2);
	});

	it('160174.订单无法使用积分抵现', async function () {
		let json = basiceJson.salesOrderJson();
		json.details.push({
			matCode: '66666', //积分抵现
			price: '10',
			num: '-1',
			colorid: '0',
			sizeid: '0'
		});
		let sfRes = await common.editBilling(json, false);
		expect(sfRes.result).to.includes({
			error: '特殊货品[积分抵现]不能用于订单'
		});
	});

	describe('160069.销售订货-新增订货,客户或供应商信息不允许修改', function () {
		let qfRes = {};
		let errorMsg;
		before(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 0); //单据是否允许修改客户或厂商 不允许
			USEECINTERFACE == 2 ? errorMsg = '客户为空，无法保存' : errorMsg = '客户不能为空';
			// 新增销售订单
			let orderRes = await common.editBilling(basiceJson.salesOrderJson());

			//获取订单信息
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);
			qfRes.result.interfaceid = orderRes.params.interfaceid;
			qfRes.result.action = 'edit';
		});
		it('1.将客户名称从A改到B', async function () {
			// 验证：将客户名称从A改到B应该是不允许的
			let json = _.cloneDeep(qfRes.result);
			json.clientid = BASICDATA.dwidLs;
			let orderRes = await common.editBilling(json, false);
			expect(orderRes.result, `销售订货将客户从A改到B成功，但是按照参数设置应该是不允许修改客户的`).to.includes({
				'error': '客户或供应商信息不允许修改'
			});
		});
		it('2.将客户名称从有改到无', async function () {
			// 验证：将客户名称从有改到无应该是不允许的
			let json = _.cloneDeep(qfRes.result);
			json.clientid = '';
			let orderRes = await common.editBilling(json, false);
			expect(orderRes.result, `销售订货将客户从有改到无成功，但是按照参数设置应该是不允许修改客户的`).to.includes({
				'error': errorMsg
			});
		});
	});

	describe('配货员', function () {
		before(async () => {
			await common.loginDo({ //中洲店配货员登陆
				'logid': '207',
				'pass': '000000',
			});
		});
		after(async () => {
			await common.loginDo();
		});
		it('160117.检查配货员可以查看到同一帐套内不同门店的销售订货单', async function () {
			let param = format.qlParamsFormat({
				shopid: BASICDATA.shopidCqd,
				search_list: 0
			}, false);
			let qlRes = await common.callInterface('ql-14433', param); //销售订货-按批次查
			expect(Number(qlRes.count), '查询无结果,配货员看不到其他门店的销售订货单').to.above(0);
		});
		it('160118.配货员按订货开单后,单据属性门店内容检查.rankA', async function () {
			await common.loginDo();
			let orderRes = await common.editBilling(basiceJson.salesOrderJson()); //新增常青店未发货的销售订单
			let qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);

			await common.loginDo({ //中洲店配货员登陆
				'logid': '207',
				'pass': '000000',
			});
			let sfRes = await salesReqHandler.editSalesBillByOrder(qfRes.result);
			qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			expect(qfRes.result).to.includes({
				shopid: BASICDATA.shopidCqd,
				show_shopid: '常青店'
			});
		});
	});

	describe('`异地发货+代收`/`异地发货`模式下，验证发货门店', function () {
		before(async () => {
			if (USEECINTERFACE == 2) {
				this.pending = true;
			}
			await common.loginDo();
			await common.setGlobalParam('paymethod', 21);
		});

		after(async () => {
			await common.setGlobalParam('paymethod', 2);
		});

		it('160160,【销售订货-按批次查】修改页面应该显示发货门店', async function () {
			let orderJson = format.jsonparamFormat(basiceJson.salesOrderJson());
			orderJson.invid = BASICDATA.shopidCkd;
			let orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': orderJson
			});
			expect(orderResult, `异地发货模式下销售订货失败:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			// 检查 销售订货--按批次查列表，发货门店是否为仓库店
			let batchInfo = await common.callInterface('ql-14433', format.qlParamsFormat({
				'clientid': orderJson.clientid,
				'id1': orderResult.billno,
				'id2': orderResult.billno,
				'shopid': orderJson.shopid,
			}, true));
			expect(batchInfo.dataList[0], `销售订货按批次查发货仓库字段有误`).to.includes({
				"inventoryname": "仓库店",
				"invname": "常青店"
			})

			// 检查 销售开单--按订货开单，修改页面发货门店是否为仓库店
			let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderResult.pk);
			expect(orderInfo.result, `销售开单--按订货开单修改页面，发货门店有误`).to.includes({
				"invid": BASICDATA.shopidCkd,
				"show_invid": "仓库店",
				"shopid": BASICDATA.shopidCqd,
				"show_shopid": "常青店",
			});
		});

		it.skip('160163,异地+代收,挂单,修改页面应该显示发货门店', async function () {
			let orderJson = basiceJson.salesOrderJson();
			orderJson.invid = BASICDATA.shopidCkd;
			orderJson.invalidflag = '9';
			if (USEECINTERFACE == 2) orderJson.fromPend = 1; //是否挂单 是
			let orderRes = await common.editBilling(orderJson);

			// 验证 销售订货--按挂单发货门店为仓库店
			let batchInfo = await common.callInterface('ql-144331', format.qlParamsFormat({
				'clientid': orderJson.clientid,
				'shopid': orderJson.shopid,
				'remark': orderJson.remark
			}, true));
			expect(batchInfo.dataList[0], `销售订货按挂单发货仓库字段有误`).to.includes({
				"inventoryName": "仓库店",
				"invname": "常青店"
			});

			// 修改挂单并转成销售单
			let hangingInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk, 1); //挂单
			expect(hangingInfo.result, `销售开单--按挂单修改页面，发货门店有误`).to.includes({
				"invid": BASICDATA.shopidCkd,
				"show_invid": "仓库店",
				"shopid": BASICDATA.shopidCqd,
				"show_shopid": "常青店",
			});
		});

		it('160160,【销售开单-按批次查】异地发货模式，修改页面应该显示发货门店', async function () {
			let salesJson = basiceJson.salesJson();
			salesJson.invid = BASICDATA.shopidCkd;
			let sfRes = await common.editBilling(salesJson, false);
			expect(sfRes.result, `异地发货模式下销售开单失败:${JSON.stringify(sfRes.result)}`).to.not.have.property('error');

			// 检查 销售开单--按批次查列表，发货门店是否为仓库店
			let batchList = await common.callInterface('ql-142201', format.qlParamsFormat({
				'dwid': sfRes.params.dwid,
				'billno1': sfRes.result.billno,
				'billno2': sfRes.result.billno,
				'shopid': LOGINDATA.invid
			}, true));
			expect(batchList.dataList[0], `销售开单按批次查列表发货门店字段有误`).to.includes({
				"invname": "仓库店",
				"maininvname": "常青店",
			});

			// 检查 销售开单修改页面，发货门店是否为仓库店
			let salesInfo = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			expect(salesInfo.result, `销售开单详情界面发货门店字段有误`).to.includes({
				"show_shopid": "常青店",
				"show_invid": "仓库店",
				"invid": BASICDATA.shopidCkd,
				"shopid": BASICDATA.shopidCqd,
			});
		});
	});

	describe('销售订单修改限制', function () {

		before('管理员登陆', async function () {
			await common.loginDo();
		});
		after(async () => {
			await common.loginDo();
		});
		it('160182.不允许修改', async function () {
			await checkRoleParamOfOrderEdit(0, {
				error: '不允许修改'
			}, {
					error: '不允许修改'
				});
		});
		//后台-角色权限-功能参数-销售订货修改限制-不允许 参数设置在160182中修改
		it('160173.挂单转正式单不受限制', async function () {
			let hangRes = await salesOrderReqHandler.editSalesOrderHangBill(basiceJson.salesOrderJson());
			let qfRes = await salesOrderReqHandler.salesOrderQueryBilling({ pk: hangRes.result.pk, isPend: 1 }); //获取挂单信息
			await salesOrderReqHandler.hangToSalesOrderBill(qfRes.result); //挂单转销售订单
		});
		it('160183.打印后不允许修改.rankA', async function () {
			await checkRoleParamOfOrderEdit(10, undefined, {
				error: '打印后不允许修改'
			});
		});
		it('160184.不限制', async function () {
			await checkRoleParamOfOrderEdit(20);
		});
	});

});

/*
 * 验证角色参数-销售订单修改限制
 * result1 修改未打印订单结果
 * result2 修改已打印订单结果
 */
async function checkRoleParamOfOrderEdit(roleParamVal, result1, result2) {
	await common.setRoleParam({
		'ipadcode': 'shopmanager', //店长
		'code': 'order_edit',
		'val': roleParamVal
	});
	await common.loginDo({
		'logid': '004',
		'pass': '000000',
	}, false);
	let json = basiceJson.salesOrderJson();
	let sfRes = await common.editBilling(json); //新增未打印订单
	let qfRes = await salesOrderReqHandler.salesOrderQueryBilling(sfRes.result.pk);
	let editRes = await salesOrderReqHandler.editSalesOrderBill(qfRes.result, false); //修改订单
	//二代没有返回modelClass: 'DLDynamicModel'，
	if (!result1) {
		expect(editRes.result).to.have.property('pk');
	} else {
		expect(editRes.result).to.includes(result1, `修改未打印订单验证错误 ${JSON.stringify(editRes.result)}`);
	};


	sfRes = await common.editBilling(json); //新增已打印订单
	let param = {
		pk: sfRes.result.pk,
		isUpdatePrintFlag: 1
	};
	await common.callInterface('cs-getOrderPrint', param); //销售订单打印前更新打印标记
	qfRes = await salesOrderReqHandler.salesOrderQueryBilling(sfRes.result.pk);
	editRes = await salesOrderReqHandler.editSalesOrderBill(qfRes.result, false); //修改订单
	if (!result2) {
		expect(editRes.result).to.have.property('pk');
	} else {
		expect(editRes.result).to.includes(result2, `修改已打印订单验证错误 ${JSON.stringify(editRes.result)}`);
	}
};
