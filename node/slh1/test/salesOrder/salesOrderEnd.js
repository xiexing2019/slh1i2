'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

//18-07-05 订单增加终结统计 http://jira.hzdlsoft.com:7082/browse/SLH-23124
describe('销售订货--订单终结', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('130015.订单终结-未开单', function () {
		let orderRes, qfRes;
		before(async () => {
			await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单
			orderRes = await common.editBilling(basiceJson.specialSalesOrderJson());

			await salesOrderReqHandler.saleOrderFinish(orderRes.result.pk); //终结订单

			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
		});
		it('1.销售订货-按批次查', async function () {
			const batchList = await common.callInterface('ql-14433', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `销售订货按批次查单据状态有误`).to.includes({
				flag: '结束',
				sendflag: '3',
			});

			const exp = salesOrderReqHandler.getSalesOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.销售订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-14431', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'mainShopid': orderRes.params.invid,
				'mainClientid': orderRes.params.dwid,
			}, true));
			const exp = salesOrderReqHandler.getSalesOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	describe('订单终结-部分发货', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJson.specialSalesOrderJson());
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
			const orderInfo = salesJsonByOrder(qfRes.result, '-1');
			await common.editBilling(orderInfo); //按订货开单-部分发货
			await salesOrderReqHandler.saleOrderFinish(orderRes.result.pk); //终结订单
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
		});
		it('1.销售开单-按批次查', async function () {
			const batchList = await common.callInterface('ql-14433', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `销售订货按批次查单据状态有误`).to.includes({
				flag: '结束',
				sendflag: '3',
			});
			const exp = salesOrderReqHandler.getSalesOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.销售订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-14431', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'mainShopid': orderRes.params.invid,
				'mainClientid': orderRes.params.dwid,
			}, true));
			const exp = salesOrderReqHandler.getSalesOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	describe('订单终结-全部发货', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJson.specialSalesOrderJson());
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
			const orderInfo = salesJsonByOrder(qfRes.result, '0');
			await common.editBilling(orderInfo); //按订货开单-全部发货
			await salesOrderReqHandler.saleOrderFinish(orderRes.result.pk); //终结订单
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
		});
		it('1.销售开单-按批次查', async function () {
			const batchList = await common.callInterface('ql-14433', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `销售订货按批次查单据状态有误`).to.includes({
				flag: '结束',
				sendflag: '3',
			});
			const exp = salesOrderReqHandler.getSalesOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.销售订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-14431', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'mainShopid': orderRes.params.invid,
				'mainClientid': orderRes.params.dwid,
			}, true));
			const exp = salesOrderReqHandler.getSalesOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

	describe('订单终结-多发', function () {
		let orderRes, qfRes;
		before(async () => {
			orderRes = await common.editBilling(basiceJson.specialSalesOrderJson());
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
			const orderInfo = salesJsonByOrder(qfRes.result, '1');
			await common.editBilling(orderInfo); //按订货开单-多发
			await salesOrderReqHandler.saleOrderFinish(orderRes.result.pk); //终结订单
			qfRes = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取单据详情
		});
		it('1.销售开单-按批次查', async function () {
			const batchList = await common.callInterface('ql-14433', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'invid': orderRes.params.invid,
			}, true));
			expect(batchList.dataList[0], `销售订货按批次查单据状态有误`).to.includes({
				flag: '结束',
				sendflag: '3',
			});
			const exp = salesOrderReqHandler.getSalesOrderMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, batchList.dataList[0]);
		});
		it('2.销售订货-按明细查', async function () {
			const detailsList = await common.callInterface('ql-14431', format.qlParamsFormat({
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'mainShopid': orderRes.params.invid,
				'mainClientid': orderRes.params.dwid,
			}, true));
			const exp = salesOrderReqHandler.getSalesOrderDetailSearchListExp(qfRes.result);
			common.isApproximatelyArrayAssert(exp, detailsList.dataList);
		});
	});

});

/*
 * 根据按订货开单的信息组装销售开单jsonparam
 *
 * @params
 * orderInfo 按订货开单数据
 * deliverType 发货类型 -1:少发 0:全发 1:多发
 */
function salesJsonByOrder(orderInfo, deliverType) {
	let json = {
		interfaceid: 'sf-14211-1',
		srcType: 2, //按订货开单
		billid: orderInfo.pk,
		deliver: orderInfo.sellerid, //经办人id
		dwid: orderInfo.clientid,
		remark: '按订货开单',
	};
	delete orderInfo.pk; //按订货开单不传pk值
	delete orderInfo.sellerid;
	delete orderInfo.clientid;
	//delete orderInfo.shouldpay;
	delete orderInfo.actualpay;
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(orderInfo), json));
	orderInfo.details.map((obj, index) => {
		jsonparam.details[index].billid = obj.pk || '';
		let _num = obj.matclsid == 2 ? 0 : deliverType;
		jsonparam.details[index].num = common.add(jsonparam.details[index].num, _num);
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});
	return jsonparam;
}
