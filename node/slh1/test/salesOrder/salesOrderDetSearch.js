'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 销售订货--按明细查
 */

describe('销售订货--按明细查-slh2', function () {

	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('160020,【销售订货—按明细查】', function () {
		it('明细查列表每个字段验证', async function () {
			let orderRes = await common.editBilling(basiceJson.salesOrderJson());
			let orderForm = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk);

			let searchCondition = {
				'id1': orderRes.result.billno,
				'id2': orderRes.result.billno,
				'mainClientid': orderRes.params.clientid,
				'mainShopid': orderRes.params.shopid,
				'propdresStyleid': orderRes.params.details[0].styleid
			};
			let qlRes = await common.callInterface('ql-14431', format.qlParamsFormat(searchCondition, true));
			common.isArrayContainArrayAssert(orderForm.result.details, qlRes.dataList);
		});

		it('查询条件组合查询', async function () {
			TESTCASE = {
				describe: "销售订货-按明细查，查询条件检查",
			}
			let orderJson = format.jsonparamFormat(basiceJson.salesOrderJson());
			let orderResult = await common.callInterface('sf-14401-1', {
				'jsonparam': orderJson
			});
			expect(orderResult, `销售订货失败:${JSON.stringify(orderResult)}`).to.not.have.property('error');

			let searchCondition = {
				'id1': orderResult.billno,
				'id2': orderResult.billno,
				'mainClientid': orderJson.clientid,
				'mainShopid': orderJson.shopid,
				'propdresStyleid': orderJson.details[0].styleid
			};
			let detailList = await common.callInterface('ql-14431', format.qlParamsFormat(searchCondition, true));
			expect(Number(detailList.count), `销售订货按明细查有误`).to.be.equal(orderJson.details.length);
			searchCondition.propdresStyleid = '0';
			detailList = await common.callInterface('ql-14431', format.qlParamsFormat(searchCondition, true));
			expect(detailList.count, `销售订货按明细查有误`).to.be.equal('0');
		});
	});


});
