'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesRequest = require('../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 按厂商报单
 *
 */
describe('按厂商报单', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160070.查询', async function () {
		let param = format.qlParamsFormat({
			clientid: BASICDATA.dwidXw,
			offerid: BASICDATA.dwidVell,
		}, true);
		let orignResult = await common.callInterface('ql-14436', param);

		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());

		let result = await common.callInterface('ql-14436', param);
		let exp = {
			id: BASICDATA.dwidVell,
			dwid: BASICDATA.dwidVell,
			num: common.add(orignResult.dataList[0] && orignResult.dataList[0].num || 0, sfRes.params.totalnum),
			dwname: 'Vell',
			prodate1: common.getCurrentDate(),
			prodate2: common.getCurrentDate(),
			sellerid: '',
		};
		expect(result.dataList.length, '查询结果有误').to.equal(1);
		common.isApproximatelyEqualAssert(exp, result.dataList[0]);
	});

	it('160093.检查明细.rankA', async function () {
		let goodRes = await basicInfoReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		});
		let json = basiceJsonparam.salesOrderJson();
		json.details[0].styleid = goodRes.pk; //租户级ID
		let sfRes = await common.editBilling(json);

		let param = {
			pk: '0', //新增货品 无厂商
			sellerid: BASICDATA.staffid000,
			action: 'add',
			prodate1: common.getCurrentDate(),
		};
		let qlRes = await common.callInterface('qf-22101-3', param); //按厂商报单明细界面
		let exp = {
			sizeid: sfRes.params.details[0].sizeid,
			matclsid: '0', //货品状态
			invnum: '0',
			colorid: sfRes.params.details[0].colorid,
			num: sfRes.params.details[0].num,
			styleid: sfRes.params.details[0].styleid,
			rem: sfRes.params.details[0].rem,
			recvnum: '0',
			tagprice: '0',
			pk: '0'
		};
		common.isApproximatelyEqualAssert(exp,
			common.takeWhile(qlRes.details, (detail) => detail.styleid == sfRes.params.details[0].styleid)[0]);
	});

	it('160149.转采购单.rankA', async function () {
		await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单

		let sfRes = await saveOrderDetails(BASICDATA.dwidVell, (obj) => {
			obj.rem = '备注';
			obj.respopid = LOGINDATA.id;
		}); //转采购单

		let qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk);
		// sfRes.params.respopid = LOGINDATA.id;
		let exp = getSupplierFormExp(sfRes); //按厂商报单期望值
		common.isApproximatelyEqualAssert(exp, qfRes.result);
	});

	it('160150.修改/作废已转成采购单的订货单.rankA', async function () {
		await common.setGlobalParam('check_sales_order_submit_maker', 0); //销售订单按厂商报单后是否允许修改 默认不允许修改

		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		await saveOrderDetails(BASICDATA.dwidVell); //转采购单

		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		qfRes.result.interfaceid = 'sf-14401-1';
		qfRes.result.action = 'edit';
		qfRes.result.details[0].num = 10;
		delete qfRes.result.shouldpay;

		let res = await common.editBilling(qfRes.result, false);
		expect(res.result).to.includes({
			error: '已报单的明细不允许修改'
		});

		res = await salesOrderRequest.cancelSaleorderBill({
			pk: sfRes.result.pk,
			check: false,
		});
		expect(res.result).to.includes({
			error: '本订单中明细被转化为采购订单不允许作废',
		});
	});

	it('160151.已报单的订货单作废.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		let sfResPur = await saveOrderDetails(BASICDATA.dwidVell); //转采购单

		let cancelRes = await common.callInterface('cs-22102', { //作废生成的采购订单
			pk: sfResPur.result.pk
		});
		expect(cancelRes, `采购订单作废失败:\n${JSON.stringify(cancelRes)}`).to.have.property('val');

		let param = format.qlParamsFormat({
			pk: BASICDATA.dwidVell,
			action: 'add',
		}, true);
		let qlRes = await common.callInterface('qf-22101-3', param);
		common.isApproximatelyEqualAssert(sfResPur.params, qlRes); //作废后 报单数据恢复至未报单
	});

	it('160152.已终结的订单不能显示在报单里面', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		await salesOrderRequest.saleOrderFinish(sfRes.result.pk); //终结订单

		let param = format.qlParamsFormat({
			pk: BASICDATA.dwidVell,
			action: 'add',
		}, true);
		let qlRes = await common.callInterface('qf-22101-3', param);
		expect(qlRes.details).to.satisfy((arr) => {
			let ret = true;
			for (let i = 0; i < arr.length; i++) {
				if (arr[i].billid == qfRes.result.details[0].pk) {
					ret = false;
					break;
				};
			};
			return ret;
		}, `报单中显示已终结的订单 billno为${sfRes.result.billno}`);
	});

	it('160162.没有厂商的款号转采购单', async function () {
		let sfRes = await saveOrderDetails('0', undefined, false); //没有厂商的dwid=0

		expect(sfRes.result, `开单成功:${JSON.stringify(sfRes.result)}`).to.have.property('error');
		expect(sfRes.result.error).to.includes('厂商为空');
	});

	it('160164.总经理可以转化其他门店的单子', async function () {
		await common.loginDo({ //中洲店总经理
			'logid': '200',
			'pass': '000000',
		});
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		await common.loginDo();
		sfRes = await saveOrderDetails(BASICDATA.dwidVell, async (obj) => {
			expect(obj.details).to.satisfy((details) => {
				let ret = false;
				for (let i = 0; i < details.length; i++) {
					if (details[i].billid == qfRes.result.details[0].pk) {
						ret = true;
						break;
					}
				}
				return ret;
			}, `报单中未找到其他门店的订单 billno为${sfRes.result.billno}`);
		});
		qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk);
		common.isFieldsEqualAssert(LOGINDATA, qfRes.result, 'invid;invname=show_invid');
	});

	it('160166.店长只显示本门店数据', async function () {
		await common.loginDo({
			'logid': '204',
			'pass': '000000',
		});
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		let param = format.qlParamsFormat({
			pk: BASICDATA.dwidVell,
			action: 'add',
		}, true);
		let qfResult = await common.callInterface('qf-22101-3', param);

		await common.loginDo();
		expect(qfResult.details).to.satisfy((details) => {
			let ret = true;
			for (let i = 0; i < details.length; i++) {
				if (details[i].billid == qfRes.result.details[0].pk) {
					ret = false;
					break;
				}
			}
			return ret;
		}, `店长显示了其他门店的订单 billno为${sfRes.result.billno}`);
	});

	it('160178.默认店长权限,按厂商报单转采购订单,总经理去查看该采购订货单.rankA', async function () {
		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		await common.setGlobalParam('shopmanager_right', 0); //默认店长权限
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		sfRes = await saveOrderDetails(BASICDATA.dwidVell); //转采购单

		await common.loginDo();
		qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk); //获取采购订货-修改页面数据
		common.isApproximatelyEqualAssert(sfRes.result, qfRes.result);
		if (USEECINTERFACE == 1) {
			expect(qfRes.result).to.includes({
				cash: '0',
				card: '0',
				remit: '0',
			}, `支付方式错误`);
		} else {
			expect(qfRes.result, '支付方式错误').not.to.have.all.keys(['cash', 'card', 'remit']); //二代支付为0时,就不返回相应的字段
		};
	});

	describe('开单员', function () {
		before(async () => {
			await common.loginDo({ //常青店开单员
				'logid': '005',
				'pass': '000000',
			});
		});
		after(async () => {
			await common.loginDo();
		});
		it('160156.允许修改报单数量.rankA', async function () {
			await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单
			let sfRes = await saveOrderDetails(BASICDATA.dwidVell, (obj) => {
				obj.details[0].num = common.add(obj.details[0].num, 5);
			}); //转采购单

			let qfRes = await purReqHandler.purOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params.details, qfRes.result.details, ['pk']); //sfRes.params.details中的pk为0
		});
		it('160157.按厂商报单界面店员输入框检查', async function () {
			let param = format.qlParamsFormat({
				sellerid: LOGINDATA.id,
				offerid: BASICDATA.dwidVell,
			}, true);
			let qlRes = await common.callInterface('ql-14436', param);

			let param2 = _.cloneDeep(param);
			param2.sellerid = BASICDATA.staffid000;
			let qlRes_000 = await common.callInterface('ql-14436', param2);

			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单

			let qlRes2 = await common.callInterface('ql-14436', param);
			assert(common.add(qlRes.dataList[0] && qlRes.dataList[0].num || 0, sfRes.params.totalnum), qlRes2.dataList[0].num);

			let qlRes2_000 = await common.callInterface('ql-14436', param2);
			common.isApproximatelyEqualAssert(qlRes_000, qlRes2_000); //其他店员数据不变
		});
		it('160159.报单可以限制营业员只能报自己的单', async function () {
			await common.setDataPrivilege('boss', '-3'); //开单员记录权限 仅本人创建
			let param = format.qlParamsFormat({
				offerid: BASICDATA.dwidVell,
			}, true);
			let qlRes = await common.callInterface('ql-14436', param);

			await common.loginDo();
			await common.editBilling(basiceJsonparam.salesOrderJson()); //新增销售订单

			await common.loginDo({ //常青店开单员
				'logid': '005',
				'pass': '000000',
			});
			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
			let qlRes2 = await common.callInterface('ql-14436', param);
			assert(common.add(qlRes.dataList[0] && qlRes.dataList[0].num || 0, sfRes.params.totalnum), qlRes2.dataList[0].num);

			param = format.qlParamsFormat({
				pk: BASICDATA.dwidVell,
				action: 'add',
			}, true);
			let qfRes = await common.callInterface('qf-22101-3', param);
			//按厂商报单界面点击记录进入修改界面,只会展示该店员创建的订货内容
			expect(qlRes2.dataList[0].num, '报单数据预览页面数据错误').to.equal(qfRes.totalnum);
		});
	});

});

/*
 * 按厂商报单转采购单
 *
 */
async function saveOrderDetails(dwid, editJson, check = true) {
	let param = format.qlParamsFormat({
		pk: dwid,
		action: 'add',
	}, true);
	let qfRes = await common.callInterface('qf-22101-3', param);

	if (typeof (editJson) == 'function') await editJson(qfRes); //修改按厂商报单明细
	if (USEECINTERFACE == 2) {
		qfRes.totalsum = 0; //初始化
		qfRes.details.forEach(val => {
			val.total = common.mul(val.num, val.price); //重新计算下小计。保持金额的正确性,二代这里他是检测小计的
			qfRes.totalsum += val.total;
		});
	};
	let sfRes = await common.callInterface('sf-22101-3', {
		jsonparam: qfRes,
	});

	check && expect(sfRes, `按厂商报单转采购单失败:\n${JSON.stringify(sfRes)}`).to.have.property('pk');

	return {
		params: qfRes,
		result: sfRes
	};
};


/**
 * getSupplierFormExp - 按厂商报单期望值
 *
 * @param  {object} sfRes saveOrderDetails的返回值
 * @param  {object} sfRes.params
 * @param  {object} sfRes.result
 * @return {object}      description
 */
function getSupplierFormExp(sfRes) {
	let exp = {
		show_cardaccountid: '',
		show_cashaccountid: '',
		show_remitaccountid: '',
		cash: '',
		remit: '',
		pk: sfRes.result.pk,
		billno: sfRes.result.billno,
		respopid: LOGINDATA.id,
		// show_respopid:,
	};
	const mapfield = 'show_invid;dwid;flag;purcost;totalsum;totalnum;prodate;details;rem;show_dwid;invid;';
	exp = Object.assign(exp, format.dataFormat(sfRes.params, mapfield));
	exp.details.forEach(obj => delete obj.pk);
	return exp;
}
