'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const reqHandler = require('../../help/reqHandlerHelp');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');

//
describe('按订单配货后开单', function () {
	let distributeNum = 0; //计数器功能-配货数
	this.timeout('30000');

	//销售订单，配货单详情，配货单，销售单
	let clientId, orderRes, preQfRes, distributeRes, salesRes, qlData, getDistribute;

	before(async function () {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		//新增客户
		const custRes = await salesReqHandler.addCust(basiceJsonparam.addCustJson());
		clientId = custRes.result.val;

		await common.setGlobalParam('sales_order_deliver_mode', 2);
	});
	after(async function () {
		await common.setGlobalParam('sales_order_deliver_mode', 1);
	});

	it('1.获取销售订货明细', async function () {
		orderRes = await common.editBilling(getOrderJson({
			clientId
		}));
		// console.log(`orderRes=${JSON.stringify(orderRes)}`);
		preQfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-14428-saleorder_dres',
			pk: orderRes.result.pk
		});
		common.isApproximatelyEqualAssert(orderRes.params, preQfRes.result);
	});
	it('2.按订单配货-查询列表', async function () {
		const qlRes = await common.callInterface('ql-14428', format.qlParamsFormat({
			flag: 0,
			hasDisnum: 0, //未配货
			orderno1: orderRes.result.billno,
			orderno2: orderRes.result.billno,
			check: true,
		}));
		const exp = getDistributeListExp(preQfRes.result);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});
	it('3.配货', async function () {
		//默认全部配货
		distributeRes = await reqHandler.sfIFCHandler(basiceJsonparam.salesOrderDistributeJson(preQfRes.result));
		distributeNum = common.add(distributeNum, preQfRes.result.totalnum);
		// console.log(`distributeRes: ${JSON.stringify(distributeRes)}`);
	});
	it('1.获取销售订货明细', async function () {
		preQfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-14428-saleorder_dres',
			pk: orderRes.result.pk
		});
		//验证disnum
		common.isApproximatelyEqualAssert(distributeRes.params.jsonparam.details, preQfRes.result.details);
	});
	it('2.按订单配货-查询列表', async function () {
		const qlRes = await common.callInterface('ql-14428', format.qlParamsFormat({
			flag: 0,
			hasDisnum: 1, //已配货
			orderno1: orderRes.result.billno,
			orderno2: orderRes.result.billno,
			check: true,
		}));
		const exp = getDistributeListExp(preQfRes.result);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});
	it('4.按配货开单-查询列表', async function () {
		const qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
			clientid: preQfRes.result.clientid
		}));
		qlData = qlRes.dataList[0];
		// console.log(`qlData=${JSON.stringify(qlData)}`);
	});
	//配货界面
	it('5.获取配货单明细', async function () {
		//获取配货界明细
		getDistribute = await reqHandler.queryBilling({
			interfaceid: 'qf-1427-saleout_dres',
			pk: qlData.id,
			invid: qlData.invid,
			action: 'add'
		});
		//	console.log(`preQfRes=${JSON.stringify(preQfRes.result)}`);
	});
	it('6.按配货开单-开单保存', async function () {
		let json = _.cloneDeep(getDistribute.result);
		json.interfaceid = 'sf-1427-saleout_dres';
		json.action = 'add';
		salesRes = await common.editBilling(json);
		//按配货开单后配货数要减掉数量

		distributeNum = common.sub(distributeNum, json.totalnum);
		// console.log(`salesRes=${JSON.stringify(salesRes)}`);
	});
	it('2.按订单配货-查询列表', async function () {
		const qlRes = await common.callInterface('ql-14428', format.qlParamsFormat({
			check: true,
			flag: 2,
			hasDisnum: 0,
			orderno1: orderRes.result.billno,
			orderno2: orderRes.result.billno,
		}));
		// console.log(`qlRes.dataList[0] : ${JSON.stringify(qlRes.dataList[0])}`);
		preQfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-14428-saleorder_dres',
			pk: orderRes.result.pk
		});
		const exp = getDistributeListExp(preQfRes.result);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});
	it('3.按订单配货界面', async function () {
		//console.log(`salesRes : ${JSON.stringify(salesRes)}`);
		let qfRes = await salesReqHandler.salesQueryBilling(salesRes.result.pk);
		//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
		common.isApproximatelyEqualAssert(salesRes.params.jsonparam, qfRes.result);
	});
	describe('配货单', function () {
		let preQfRes, sfResSales, orderRes, qfDistribute;
		before(async function () {
			orderRes = await common.editBilling(getOrderJson({
				clientId
			}));
			//销售订货明细
			preQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14428-saleorder_dres',
				pk: orderRes.result.pk
			});

		});
		it('1.配货数小于订货数,生成配货单', async function () {
			let params = basiceJsonparam.salesOrderDistributeJson(preQfRes.result);
			params.jsonparam.details[0].disnum--;
			// params.check = false;
			// 按订单配货
			await reqHandler.sfIFCHandler(params);
			//这里配货数定死了
			distributeNum = common.add(distributeNum, 19);
		});
		it('2.修改配货中的销售订单', async function () {
			const qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14401-1',
				pk: orderRes.result.pk
			});
			let editJson = _.cloneDeep(qfRes.result);
			editJson.interfaceid = orderRes.params.interfaceid;
			const editRes = await common.editBilling(editJson, false);
			expect(editRes.result).to.includes({
				msgId: 'orderdet_disnum_not_allow_edit',
				error: '已配的货的销售订单不允许修改'
			});
		});
		it('按配货开单', async function () {
			//按配货开单列表查询
			const qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
				clientid: preQfRes.result.clientid
			}));

			qfDistribute = await reqHandler.queryBilling({
				interfaceid: 'qf-1427-saleout_dres',
				pk: qlRes.dataList[0].id,
				invid: qlRes.dataList[0].invid,
				action: 'add'
			});
			//按配货开单
			let json = _.cloneDeep(qfDistribute.result);
			json.interfaceid = 'sf-1427-saleout_dres';
			json.action = 'add';
			sfResSales = await common.editBilling(json);
			//按配货开单

			distributeNum = common.sub(distributeNum, json.totalnum);
		});
		it('销售订货按批次查订单状态', async function () {
			preQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14428-saleorder_dres',
				pk: orderRes.result.pk
			});
			const qlRes2 = await common.callInterface('ql-14428', format.qlParamsFormat({
				flag: 1, //部分发货
				hasDisnum: 0, //未配货
				orderno1: orderRes.result.billno,
				orderno2: orderRes.result.billno,
				check: true,
			}));
			const exp = getDistributeListExp(preQfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes2.dataList[0]);
		});
		it('修改按配货开单的销售单', async function () {
			const qfResSales = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfResSales.result.pk,
			});
			let editJson = _.cloneDeep(qfResSales.result);
			editJson.interfaceid = 'sf-14211-1';
			const editRes = await common.editBilling(editJson, false);
			expect(editRes.result).to.includes({
				"msgId": "saleout_by_distribute_order_cannot_modify",
				"error": "按配货开单生成的销售单不允许修改"
			});
		});
		it('作废按配货开单的销售单', async function () {
			let rs = await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancel-saleout-bill',
				pk: sfResSales.result.pk,
				check: false,
			});
			expect(rs.result).to.includes({
				val: 'ok'
			});
			distributeNum = common.add(distributeNum, sfResSales.params.totalnum);
		})
		describe('3.作废配货中的销售订单', function () {
			let orderSales, orderSalesQf;
			before(async () => {
				//新增订单
				orderSales = await common.editBilling(getOrderJson({
					clientId
				}));
				//销售订货明细
				orderSalesQf = await reqHandler.queryBilling({
					interfaceid: 'qf-14428-saleorder_dres',
					pk: orderSales.result.pk
				});
				//配货
				await reqHandler.sfIFCHandler(basiceJsonparam.salesOrderDistributeJson(orderSalesQf.result));
				distributeNum = common.add(distributeNum, orderSalesQf.result.totalnum);
			});
			it('1、作废订单', async function () {
				await reqHandler.csIFCHandler({
					interfaceid: 'cs-saleorder-cancel',
					pk: orderSales.result.pk,
				});
				distributeNum = common.sub(distributeNum, orderSalesQf.result.totalnum);
			});
			it('2、配货列表没有这张配货单', async function () {
				let qlRes = await common.callInterface('ql-14428', format.qlParamsFormat({
					orderno1: orderSales.result.billno,
					orderno2: orderSales.result.billno,
				}));
				expect(Number(qlRes.count), '作废订单后按订单配货列表中仍有记录').to.equal(0);
			});
			it('3.已经配货的信息仍存在', async function () {
				let qfResSalesOrder = await reqHandler.queryBilling({
					interfaceid: 'qf-14428-saleorder_dres',
					pk: orderSales.result.pk
				});
				//按订单配货默认全部配货
				qfResSalesOrder.result.details.forEach((detail, index) => {
					expect(detail.disnum).to.equal(orderSalesQf.result.details[index].num);
				});
			});

		});
		describe('4.终结配货中的销售订单', function () {
			let orderSales, orderSalesQf;
			before(async () => {
				orderSales = await common.editBilling(getOrderJson({
					clientId
				}));
				orderSalesQf = await reqHandler.queryBilling({
					interfaceid: 'qf-14428-saleorder_dres',
					pk: orderSales.result.pk
				});
				//配货
				await reqHandler.sfIFCHandler(basiceJsonparam.salesOrderDistributeJson(orderSalesQf.result));
				distributeNum = common.add(distributeNum, orderSalesQf.result.totalnum);
			});
			it('终结订单', async function () {
				let rs = await reqHandler.csIFCHandler({
					interfaceid: 'cs-saleoutOrderFinish',
					pk: orderSales.result.pk,
					check: false,
				});
				distributeNum = common.sub(distributeNum, orderSalesQf.result.totalnum);
				expect(rs.result).to.includes({
					val: 'ok',
				});

			});
			it('2、配货列表没有这张配货单', async function () {
				let qlRes = await common.callInterface('ql-14428', format.qlParamsFormat({
					orderno1: orderSales.result.billno,
					orderno2: orderSales.result.billno,
				}));
				expect(Number(qlRes.count), '终结订单后按订单配货列表中仍有记录').to.equal(0);
			});
			it('3.已经配货的信息仍存在', async function () {
				let qfResSalesOrder = await reqHandler.queryBilling({
					interfaceid: 'qf-14428-saleorder_dres',
					pk: orderSales.result.pk
				});
				//按订单配货默认全部配货
				qfResSalesOrder.result.details.forEach((detail, index) => {
					expect(detail.disnum).to.equal(orderSalesQf.result.details[index].num);
				});
			});

		});
		describe('特殊的配货数', function () {
			it('配货数为0', async function () {
				//新增客户
				let cust = await salesReqHandler.addCust(basiceJsonparam.addCustJson());
				let distributeInfo = await orderDistribute(cust.result.val);
				distributeInfo.params.jsonparam.details.forEach(detail => {
					detail.disnum = 0;
				});
				//按订单配货
				await reqHandler.sfIFCHandler(distributeInfo.params);
				let qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: cust.result.val,
				}));
				expect(qlRes.dataList.length, '配货数为0的配货单，按配货开单界面可以查到').to.equal(0);
			});
			it('配货数为负数', async function () {
				let distributeInfo = await orderDistribute(clientId);
				distributeInfo.params.jsonparam.details.forEach(detail => {
					detail.disnum = -1;
				});
				distributeInfo.params.check = false;
				//按订单配货
				let resMessage = await reqHandler.sfIFCHandler(distributeInfo.params);
				expect(resMessage.result, '配货数为负数配货成功').to.includes({
					error: '订单明细的配货数量不能小于0'
				});
			});
			it('配货数+已发数大于订货数', async function () {
				let distributeInfo = await orderDistribute(clientId);
				distributeInfo.params.jsonparam.details.forEach(detail => {
					detail.disnum = 8;
				});
				//按订单配货
				await reqHandler.sfIFCHandler(distributeInfo.params);
				distributeNum = common.add(distributeNum, 16);
				//按配货开单列表查询
				let qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: clientId
				}));

				qfDistribute = await reqHandler.queryBilling({
					interfaceid: 'qf-1427-saleout_dres',
					pk: qlRes.dataList[0].id,
					invid: qlRes.dataList[0].invid,
					action: 'add'
				});
				//按配货开单
				let json = _.cloneDeep(qfDistribute.result);
				json.interfaceid = 'sf-1427-saleout_dres';
				json.action = 'add';
				sfResSales = await common.editBilling(json);
				distributeNum = common.sub(distributeNum, json.totalnum);
				//按订单配货--配货数+已发数大于订货数
				distributeInfo.params.jsonparam.details.forEach(detail => {
					detail.disnum = 20;
				});
				distributeInfo.params.check = false;
				let distributeBill = await reqHandler.sfIFCHandler(distributeInfo.params);
				expect(distributeBill.result, '配货数+已发数大于订货数配货成功').to.includes({
					error: '第1行明细的配货数+订货已发数不能大于订货数'
				});
			});
			it('配货成功后，修改配货数为0', async function () {
				let distributeInfo = await orderDistribute(clientId);
				//按订单配货
				await reqHandler.sfIFCHandler(distributeInfo.params);
				//按配货开单列表查询
				let qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: clientId,
				}));
				expect(qlRes.dataList.length).to.equal(1);
				//修改按订单配货中的配货数为0
				distributeInfo.params.jsonparam.details.forEach(detail => {
					detail.disnum = 0;
				});
				await reqHandler.sfIFCHandler(distributeInfo.params);
				//按配货开单列表查询结果为空
				qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: clientId,
				}));
				expect(qlRes.dataList.length, '配货成功后修改配货数为0，按配货开单列表仍有这张配货单').to.equal(0);

			});
			it('配货日期不同，同门店同客户按配货开单界面只有一条记录', async function () {
				let distributeInfo = await orderDistribute(clientId);
				//按订单配货
				await reqHandler.sfIFCHandler(distributeInfo.params);
				//默认全部配货20
				distributeNum = common.add(distributeNum, 20);
				//按配货开单列表查询
				let qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: clientId,
				}));
				//新增昨天的销售订货
				let json = basiceJsonparam.salesOrderJson();
				json.clientid = clientId;
				json.prodate = common.getDateString([0, 0, -1]);
				let salesOrder = await common.editBilling(json);
				//销售订货明细
				let salesOrderQf = await reqHandler.queryBilling({
					interfaceid: 'qf-14428-saleorder_dres',
					pk: salesOrder.result.pk
				});
				//console.log(`salesOrderQf : ${JSON.stringify(salesOrderQf)}`);
				// 按订单配货
				let params = basiceJsonparam.salesOrderDistributeJson(salesOrderQf.result);
				await reqHandler.sfIFCHandler(params);

				distributeNum = common.add(distributeNum, salesOrderQf.result.totalnum);
				//按配货开单列表查询
				qlRes = await common.callInterface('ql-1427', format.qlParamsFormat({
					clientid: clientId,
				}));
				expect(qlRes.dataList.length, '不同订单日期的订单，配货后，按配货开单界面不止有一条数据').to.equal(1);
				expect(Number(qlRes.dataList[0].distributenum), '不同订货日期的订单，配货后的配货数有问题').to.equal(25);

			})
			it('特殊货品--不支持配货', async function () { });

		});

	});

});


function getOrderJson(params) {
	let json = basiceJsonparam.salesOrderJson();
	json.clientid = params.clientId;
	[json.cash, json.card, json.remit] = [0, 0, 0];
	json.details.forEach((detail, index) => {
		detail.num = 10;
	});
	return json;
};

function getSalesJson(qfDet) {
	//version inoutflag type balance
	let json = format.dataFormat(qfDet, 'remitaccountid;remit;remark;dwfdid;agencyaccountid;cardaccountid;finpayAlipay;shopid;finpayWeixinpay');
	return json;
};

function getDistributeListExp(qfDet) {
	let exp = format.dataFormat(qfDet, 'orderno=billno;optime;totalsum;dwxxname=show_clientid;totalnum;shopname=show_shopid;sellername=show_sellerid;id=pk;invalidflag');
	// let { paddiffnum, diffnum, diffsum, sendflagid, receiptsum };
	exp.disnum = 0, exp.restnum = 0;
	qfDet.details.forEach(detail => {
		exp.disnum += Number(detail.disnum);
		exp.restnum += Number(detail.restnum); //未发数
	});
	exp.delivernum = exp.totalnum - exp.restnum; //已发数
	exp.sendflag = exp.restnum == 0 ? '全部发货' : exp.restnum == exp.totalnum ? '未发货' : '部分发货';
	return exp;
};

//按订单配货-按订单配货之前的流程
async function orderDistribute(clientId) {
	//销售订货
	let salesOrder = await common.editBilling(getOrderJson({
		clientId
	}));

	// 销售订货明细
	let salesOrderQf = await reqHandler.queryBilling({
		interfaceid: 'qf-14428-saleorder_dres',
		pk: salesOrder.result.pk
	});

	// 按订单配货
	let params = basiceJsonparam.salesOrderDistributeJson(salesOrderQf.result);

	return {
		salesOrderQf,
		params,
		// salesOrderQf: salesOrderQf,
		// params: params,
	};
}
