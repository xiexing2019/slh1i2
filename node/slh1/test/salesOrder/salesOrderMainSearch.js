'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const salesRequest = require('../../help/salesHelp/salesRequestHandler');

describe('销售订货-按批次查-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160002.查询条件组合查询', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		let param = format.qlParamsFormat({
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
			clientid: qfRes.result.clientid,
			sellerid: qfRes.result.sellerid,
			shopid: qfRes.result.invid,
			invalidflag: 0,
			remark: qfRes.result.remark,
			sendflag: 0, //发货状态 未发货
		}, true);
		let qlRes = await common.callInterface('ql-14433', param); //销售订货-按批次查

		//使用订货单信息拼接按批次查期望值
		let exp = qfRes.result;
		exp.flag = '未发货';
		exp.id = exp.pk;
		exp.invname = exp.show_shopid;
		exp.dwname = exp.show_clientid;
		exp.num = exp.totalnum;
		common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);
	});
	//qfRes中的details中两条数据，返回的顺序反了
	it('160006.订单修改界面新增或删除款号.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		qfRes.result.details.slice(0, 1);
		qfRes.result.details.push({
			"num": 5,
			"sizeid": "XL",
			"matCode": "Agc002",
			"rem": "修改明细",
			"colorid": "BaiSe",
			"price": 200,
			"discount": 1,
		});
		sfRes = await salesOrderRequest.editSalesOrderBill(qfRes.result);
		qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['orderversion', 'balance']);
	});

	it('160009.作废功能', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		await salesOrderRequest.cancelSaleorderBill(sfRes.result.pk); //作废订单

		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		//二代不返回sendflag字段，都是用flag字段表示的
		if (USEECINTERFACE == 1) {
			expect(qfRes.result, '作废订单状态错误').to.includes({
				sendflag: '0',
				invalidflag: '1'
			});
		} else {
			expect(qfRes.result, '作废订单状态错误').to.includes({
				flag: '0',
				invalidflag: '1'
			});
		};

		let param = format.qlParamsFormat({
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
			invalidflag: 1
		}, true);
		let qlRes = await common.callInterface('ql-14433', param); //销售订货-按批次查

		//使用订货单信息拼接按批次查期望值
		let exp = qfRes.result;
		exp.flag = '未发货';
		exp.id = exp.pk;
		exp.invname = exp.show_shopid;
		exp.dwname = exp.show_clientid;
		exp.num = exp.totalnum;
		common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);

		param = format.qlParamsFormat({
			orderno1: qfRes.result.billno,
			orderno2: qfRes.result.billno,
		}, true);
		qlRes = await common.callInterface('ql-14223', param); //销售开单-按订货开单
		expect(qlRes.dataList.length, '销售开单-按订货开单 不应该显示作废的订单').to.equal(0);
	});

	it('160012.终结订单', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		await salesOrderRequest.saleOrderFinish(sfRes.result.pk); //终结订单

		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		if (USEECINTERFACE == 1) {
			expect(qfRes.result, '终结订单状态错误').to.includes({
				sendflag: '3',
				invalidflag: '0'
			});
		} else {
			expect(qfRes.result, '终结订单状态错误').to.includes({
				flag: '3',
				invalidflag: '0'
			});
		};
		let param = format.qlParamsFormat({
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
			invalidflag: 0,
			sendflag: 3 //发货状态 结束
		}, true);
		let qlRes = await common.callInterface('ql-14433', param); //销售订货-按批次查

		//使用订货单信息拼接按批次查期望值
		let exp = qfRes.result;
		exp.flag = '结束';
		exp.id = exp.pk;
		exp.invname = exp.show_shopid;
		exp.dwname = exp.show_clientid;
		exp.num = exp.totalnum;
		common.isApproximatelyEqualAssert(qlRes.dataList[0], exp);

		param = format.qlParamsFormat({
			orderno1: qfRes.result.billno,
			orderno2: qfRes.result.billno,
		}, true);
		qlRes = await common.callInterface('ql-14223', param); //销售开单-按订货开单
		expect(qlRes.dataList.length, '销售开单-按订货开单 不应该显示终结的订单').to.equal(0);
	});

	it('160011.查看修改日志', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);

		qfRes.result.remark += 'a';
		let sfResEdit = await salesOrderRequest.editSalesOrderBill(qfRes.result);
		qfRes = await salesOrderRequest.salesOrderQueryBilling(sfResEdit.result.pk);

		let editInfo = await common.callInterface('qf-14401-2', {
			pk: sfRes.result.pk
		});
		let exp = {
			pk: sfRes.result.pk,
			delopstaffName: '', //作废人
			deloptime: '', //作废时间
			createrName: qfRes.result.createrName,
			createoptime: qfRes.result.createoptime,
			opstaffName: qfRes.result.updaterName,
			optime: qfRes.result.optime,
			hashkey: sfRes.params.hashkey,
		};
		common.isApproximatelyEqualAssert(exp, editInfo);

		await common.loginDo({
			'logid': '004',
			'pass': '000000',
		});
		qfRes.result.remark += 'a';
		sfResEdit = await salesOrderRequest.editSalesOrderBill(qfRes.result);
		qfRes = await salesOrderRequest.salesOrderQueryBilling(sfResEdit.result.pk);
		editInfo = await common.callInterface('qf-14401-2', {
			pk: sfRes.result.pk
		});

		await common.loginDo();
		exp.opstaffName = qfRes.result.updaterName;
		exp.optime = qfRes.result.optime;
		common.isApproximatelyEqualAssert(exp, editInfo);
	});

	it('160194.单据未发数检查', async function () {
		if (USEECINTERFACE == 1) {
			let specResult = await common.callInterface('qf-1518-1', {
				pk: BASICDATA.styleid00000, //抹零
			});
			specResult.ratio = 0; //按订货开单 删除明细添加特殊货品  需要将最高比例改为0(不限制)
			delete specResult.flag;
			specResult.action = 'edit';
			let editSpec = await common.callInterface('sf-1518-1', {
				jsonparam: specResult,
				pk: specResult.pk
			}); //特殊货品保存
			expect(editSpec, `修改特殊货品失败:\n${JSON.stringify(editSpec)}`).to.have.property('val');
		};

		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
		qfRes.result.details = [{ //删除全部订货记录-添加一个特殊货品抹零
			'matCode': '00000',
			'price': 2,
			'num': -1,
			'colorid': '0',
			'sizeid': '0'
		}];
		await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单

		let param = format.qlParamsFormat({
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
		}, true);
		let qlRes = await common.callInterface('ql-14433', param);
		qlRes.dataList[0].diffnum = Number(qlRes.dataList[0].diffnum).toString();
		expect(qlRes.dataList[0]).to.includes({
			recvnum: '0',
			diffnum: sfRes.params.totalnum.toString(),
		}, `查询结果出错 ${JSON.stringify(qlRes.dataList[0])}`);
	});

	describe('160197.终结的订单不允许再作废', function () {
		it('1.未发货', async function () {
			await checkFor160197();
		});
		it('2.部分发货', async function () {
			await checkFor160197((obj) => {
				obj.details[0].num = 1;
				obj.details[1].num = 1;
			});
		});
		it('3.全部发货', async function () {
			await checkFor160197(() => { }); //默认为全部发货
		});
		async function checkFor160197(editJson) {
			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());

			if (typeof (editJson) == 'function') {
				let qfRes = await salesOrderRequest.salesOrderQueryBilling(sfRes.result.pk);
				editJson(qfRes.result);
				await salesRequest.editSalesBillByOrder(qfRes.result);
			};

			await salesOrderRequest.saleOrderFinish(sfRes.result.pk); //终结订单
			let cancelRes = await salesOrderRequest.cancelSaleorderBill({
				pk: sfRes.result.pk,
				check: false,
			});
			//这里一二代验证顺序不一样，二代是先验证明细的发货状态的，再验证终结状态的。一代都是先判断终结状态的
			if (USEECINTERFACE == 1 || (USEECINTERFACE == 2 && editJson == undefined)) {
				expect(cancelRes.result).to.includes({
					error: '订单已终结，不允许作废'
				}, '作废单据验证错误');
			} else {
				expect(cancelRes.result).to.includes({
					error: '已发货的明细不允许删除'
				}, '作废单据验证错误');
			};

			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
			}, true);
			let qlRes = await common.callInterface('ql-14433', param);
			expect(qlRes.dataList[0].restnum, `待发数错误`).to.equal('0');
		};
	});

	describe('店长', async function () {
		before(async () => {
			await common.loginDo({
				'logid': '204',
				'pass': '000000',
			});
		});
		after(async () => {
			await common.loginDo();
		});
		it('160168.门店查询条件检查', async function () {
			let param = format.qlParamsFormat({
				shopid: BASICDATA.shopidCqd,
			}, true);
			let qlRes = await common.callInterface('ql-14433', param); //销售订货-按批次查
			expect(qlRes.count).to.equal('0');
		});
	});
});
