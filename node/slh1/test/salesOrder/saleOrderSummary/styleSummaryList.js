'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesRequest = require('../../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../../help/salesOrderHelp/salesOrderRequestHandler');
const basicReqHandler = require('../../../help/basicInfoHelp/basicInfoReqHandler');
/*
 * 销售订货-按汇总-按款号
 * ql-14434
 * showcolorsize 是否需要明细  都以款号id 降序排序
 * 1:是 [显示颜色 尺码 以货品分组]  0:否 [不显示颜色 尺码 以款号分组(默认)]
 *
 * ql-144342 按款号-明细
 */
describe('销售订货-按款号-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160031.按款号-查询', async function () {
		let json = basiceJsonparam.salesOrderJson();

		let param = format.qlParamsFormat({
			styleid: BASICDATA.styleidAgc001,
			dwid: json.clientid,
			providerid: BASICDATA.dwidVell,
			invid: LOGINDATA.invid,
			showcolorsize: 1, //显示颜色尺码
		}, true);
		let orignQlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号

		let sfRes = await common.editBilling(json); //新增订单

		//显示颜色尺码 dataList验证
		let curQlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		curQlRes.dataList.map((data, index) => {
			sfRes.params.details.map((detail) => {
				if (detail.styleid == data.id && detail.sizeid == data.stylesize && detail.colorid == data.stylecolor) {
					let exp = orignQlRes.dataList[index]; //起始值
					exp.totalsum = common.add(exp.totalsum, detail.total);
					exp.restnumsum = common.add(exp.restnumsum, detail.num);
					exp.alltotalsum = common.add(exp.alltotalsum, detail.num);
					exp.totalnumsum = common.add(exp.totalnumsum, detail.num);
					common.isApproximatelyEqualAssert(exp, data);
				};
			});
		});

		//不显示颜色尺码 dataList验证
		param.showcolorsize = 0; //ipad不显示颜色尺码
		let curQlRes2 = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		let exp = {
			id: BASICDATA.styleidAgc001,
			stylename: 'auto001',
			stylecode: 'agc001',
			providername: 'Vell',
			stylesize: '',
			stylecolor: '',
			totalsum: common.add(orignQlRes.sumrow.totalsum, sfRes.params.totalsum),
			restnumsum: common.add(orignQlRes.sumrow.restnumsum, sfRes.params.totalnum), //未发数
			alltotalsum: common.add(orignQlRes.sumrow.alltotalsum, sfRes.params.totalnum), //差异数
			delivernumsum: orignQlRes.sumrow.delivernumsum, //已发数
			totalnumsum: common.add(orignQlRes.sumrow.totalnumsum, sfRes.params.totalnum),
		};
		common.isApproximatelyEqualAssert(exp, curQlRes2.dataList[0]);
	});

	it('160035.未发数/已发数/差异数 数值检查', async function () {
		let json = basiceJsonparam.salesOrderJson();
		let sfResOrder = await common.editBilling(json); //新增订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

		let param = format.qlParamsFormat({
			styleid: BASICDATA.styleidAgc001,
			invid: LOGINDATA.invid,
			showcolorsize: 0, //不显示颜色尺码
		}, true);
		let orignQlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号

		qfResOrder.result.details[0].num = 1;
		qfResOrder.result.details[1].num = 2;
		await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单 部分入库
		let qlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		numCheckFor160035(orignQlRes, qlRes, [3, 3, 3]); //[未发数,差异数,已发数]

		await salesOrderRequest.saleOrderFinish(sfResOrder.result.pk); //终结订单
		qlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		numCheckFor160035(orignQlRes, qlRes, [5, 3, 3]); //[未发数,差异数,已发数]
	});

	it('160036.未发数数值检查-多发', async function () {
		let json = basiceJsonparam.salesOrderJson(); //一共订货5件
		let sfResOrder = await common.editBilling(json); //新增订单
		let qfResOrder = await salesOrderRequest.salesOrderQueryBilling(sfResOrder.result.pk); //获取订单信息

		let param = format.qlParamsFormat({
			styleid: BASICDATA.styleidAgc001,
			invid: LOGINDATA.invid,
			showcolorsize: 0, //不显示颜色尺码
		}, true);
		let orignQlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号

		qfResOrder.result.details[0].num = 5;
		qfResOrder.result.details[1].num = 5;
		await salesRequest.editSalesBillByOrder(qfResOrder.result); //按订货开单 部分入库
		let qlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		numCheckFor160035(orignQlRes, qlRes, [5, 10, 10]); //[未发数,差异数,已发数]
	});

	it('160090.未发数检查-款号停用', async function () {
		let goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		}); //新增货品
		let json = basiceJsonparam.salesOrderJson();
		json.details[0].styleid = goodRes.pk;
		let sfResOrder = await common.editBilling(json); //新增订单

		let cancelRes = await common.callInterface('cs-1511-33', { //停用货品
			pk: goodRes.pk
		});
		expect(cancelRes, `停用货品失败 ${JSON.stringify(cancelRes)}`).to.includes({
			val: 'ok',
		});

		let param = format.qlParamsFormat({
			styleid: goodRes.pk,
			invid: LOGINDATA.invid,
			showcolorsize: 0, //不显示颜色尺码
		}, true);
		if (USEECINTERFACE == 2) await common.delay(500);   //二代这里加点延迟
		let qlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		let exp = {
			id: goodRes.pk,
			stylename: goodRes.params.name,
			stylecode: `${goodRes.params.code}_${common.getCurrentDate()}`,
			totalsum: sfResOrder.params.details[0].total,
			restnumsum: json.details[0].num, //未发数
			alltotalsum: json.details[0].num, //差异数
			delivernumsum: '0', //已发数
			totalnumsum: json.details[0].num,
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		await common.callInterface('cs-1511-32', { //启用货品
			pk: goodRes.pk,
		});
	});

	it('160037.厂商查询', async function () {
		let param = format.qlParamsFormat({
			providerid: BASICDATA.dwidVell,
		}, true);
		let qlRes = await common.callInterface('ql-14434', param); //销售订货-按汇总-按款号
		qlRes.dataList.map((obj) => {
			expect(obj.providername).to.equal('Vell');
		});
	});

	it('160111.按款号详细界面验证', async function () {
		let json = basiceJsonparam.salesOrderJson();
		let params = {
			'ql-14434': { //销售订货-按汇总-按款号
				styleid: BASICDATA.styleidAgc001,
				dwid: json.clientid,
				providerid: BASICDATA.dwidVell,
				invid: LOGINDATA.invid,
				showcolorsize: 1,
				today: true,
			},
			'ql-144342': { //销售订货-按款号-明细界面
				providerid: BASICDATA.dwidVell,
				dwid: json.clientid,
				styleid: BASICDATA.styleidAgc001,
				shopid: LOGINDATA.invid,
				today: true
			},
		};
		let qlRes = await common.getResults(params);
		//ql-14434的id为styleid  ql-144342的id为matid 因此忽略比较
		common.isApproximatelyArrayAssert(qlRes['ql-14434'].dataList, qlRes['ql-144342'].dataList, ['id']);
	});

	it('按款号-按客户数据验证', async function () {
		let json = basiceJsonparam.salesOrderJson();
		let params = {
			'ql-14434': { //销售订货-按汇总-按款号
				styleid: BASICDATA.styleidAgc001,
				dwid: json.clientid,
				invid: LOGINDATA.invid,
				showcolorsize: 1,
				today: true,
			},
			'ql-144341': { //销售订货-按款号-按客户
				id: BASICDATA.styleidAgc001,
				dwxxid: json.clientid,
				shopid: LOGINDATA.invid,
				today: true
			},
		};
		let qlRes = await common.getResults(params);
		//ql-14434的id为styleid  ql-144341的id为matid 因此忽略比较
		common.isApproximatelyArrayAssert(qlRes['ql-14434'].dataList, qlRes['ql-144341'].dataList, ['id']);
	});

	it('160155.门店查询条件验证', async function () {
		let params = {
			'ql-14434': { //销售订货-按汇总-按款号
				search_list: 0,
				today: true
			},
			'ql-14431': { //销售订货-按明细查
				search_list: 0,
				today: true
			}
		};
		let qlRes = await common.getResults(params);
		let exp = {
			delivernumsum: qlRes['ql-14431'].sumrow.delivernum,
			totalnumsum: qlRes['ql-14431'].sumrow.num,
			restnumsum: qlRes['ql-14431'].sumrow.restnum,
		};
		expect(qlRes['ql-14434'].sumrow).to.include(exp);

		params['ql-14434'].invid = LOGINDATA.invid;
		params['ql-14431'].mainShopid = LOGINDATA.invid;
		qlRes = await common.getResults(params);
		exp = {
			delivernumsum: qlRes['ql-14431'].sumrow.delivernum,
			totalnumsum: qlRes['ql-14431'].sumrow.num,
			restnumsum: qlRes['ql-14431'].sumrow.restnum,
		};
		expect(qlRes['ql-14434'].sumrow).to.include(exp);
	});
});

/*
 * 160035 未发数 差异数 已发数 验证
 * changeArr 已起始值为基础 [未发数,差异数,已发数]
 */
function numCheckFor160035(orignResult, curResult, changeArr) {
	assert.equal(common.sub(orignResult.dataList[0].restnumsum, changeArr[0]), curResult.dataList[0].restnumsum, '未发数错误');
	assert.equal(common.sub(orignResult.dataList[0].alltotalsum, changeArr[1]), curResult.dataList[0].alltotalsum, '差异数错误');
	assert.equal(common.add(orignResult.dataList[0].delivernumsum, changeArr[2]), curResult.dataList[0].delivernumsum, '已发数错误');
};
