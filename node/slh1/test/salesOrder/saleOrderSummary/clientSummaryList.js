'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesRequest = require('../../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../../help/salesOrderHelp/salesOrderRequestHandler');
/*
 * 销售订货-按汇总-按客户
 * ql-14442
 */
describe('销售订货-按客户', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160044.按客户-查询', async function () {
		//销售订货-按批次查和按明细查的待发数restnumsum字段计算方式不一样，
		// 有种特殊情况，两条明细，一条发货数8大于订货数5，另一条发货数为0订货数为5，导致按批次查待发货数为2，
		// 而按明细查中，两条明细的待发数分别为0，5，换成按明细查中数据
		let json = basiceJsonparam.salesOrderJson();
		json.clientid = BASICDATA.dwidXw;

		let param = format.qlParamsFormat({
			clientid: BASICDATA.dwidXw,
		}, true);
		let orignResult = await common.callInterface('ql-14442', param); //销售订货-按汇总-按客户

		let sfRes = await common.editBilling(json);
		json.clientid = BASICDATA.dwidLs;
		await common.editBilling(json); //使用其他客户开单
		let params = {
			'ql-14442': param,
			'ql-14431': { //销售订货-按明细查
				mainClientid: BASICDATA.dwidXw,
				today: true
			}

		};
		let qlRes = await common.getResults(params);

		let exp = {
			id: BASICDATA.dwidXw,
			mat_name: '小王',
			num: common.add(orignResult.dataList[0].num, sfRes.params.totalnum),
			recvnum: orignResult.dataList[0].recvnum,
			diffnum: common.add(orignResult.dataList[0].diffnum, sfRes.params.totalnum),
			restnumsum: common.add(orignResult.dataList[0].restnumsum, sfRes.params.totalnum),
			mat_money: common.add(orignResult.dataList[0].mat_money, sfRes.params.totalsum)
		};
		//新增单据 数据验证
		common.isApproximatelyEqualAssert(exp, qlRes['ql-14442'].dataList[0]);
		//与销售订货-按明细查汇总值的已发数,未发数,差异数,数量对比
		qlRes['ql-14431'].sumrow.restnumsum = qlRes['ql-14431'].sumrow.restnum;
		common.isApproximatelyEqualAssert(qlRes['ql-14431'].sumrow, qlRes['ql-14442'].dataList[0]);
	});
});
