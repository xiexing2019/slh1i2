'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesRequest = require('../../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../../help/salesOrderHelp/salesOrderRequestHandler');
/*
 * 销售订货-按汇总-按门店
 * ql-14438
 */
describe('销售订货-按门店', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160046.按门店-查询', async function () {
		//按门店查询中的待发数计算方式与按批次查中的待发数不一样，换成按明细查中数据
		let json = basiceJsonparam.salesOrderJson();

		let param = format.qlParamsFormat({
			shopid: LOGINDATA.invid,
		}, true);
		let orignResult = await common.callInterface('ql-14438', param); //销售订货-按汇总-按门店

		await common.loginDo({
			'logid': '200',
			'pass': '000000',
		});
		await common.editBilling(json); //其他门店开单

		await common.loginDo();
		let sfRes = await common.editBilling(json);

		let params = {
			'ql-14438': param,
			'ql-14431': { //销售订货-按明细查
				mainShopid: LOGINDATA.invid,
				today: true
			}
		};
		let qlRes = await common.getResults(params);

		let exp = {
			id: LOGINDATA.invid,
			mat_name: LOGINDATA.invname,
			num: common.add(orignResult.dataList[0].num, sfRes.params.totalnum),
			recvnum: orignResult.dataList[0].recvnum,
			diffnum: common.add(orignResult.dataList[0].diffnum, sfRes.params.totalnum),
			restnumsum: common.add(orignResult.dataList[0].restnumsum, sfRes.params.totalnum),
			mat_money: common.add(orignResult.dataList[0].mat_money, sfRes.params.totalsum)
		};
		//新增单据 数据验证
		common.isApproximatelyEqualAssert(exp, qlRes['ql-14438'].dataList[0]);
		//与销售订货-按明细查汇总值的已发数,未发数,差异数,数量对比
		qlRes['ql-14431'].sumrow.restnumsum = qlRes['ql-14431'].sumrow.restnum;
		common.isApproximatelyEqualAssert(qlRes['ql-14431'].sumrow, qlRes['ql-14438'].dataList[0]);
	});
});
