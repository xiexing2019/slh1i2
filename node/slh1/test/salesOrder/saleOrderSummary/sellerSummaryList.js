'use strict';
const common = require('../../../../lib/common.js');
const format = require('../../../../data/format');
const getBasicData = require('../../../../data/getBasicData.js');
const basiceJsonparam = require('../../../help/basiceJsonparam.js');
const salesRequest = require('../../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../../help/salesOrderHelp/salesOrderRequestHandler');
/*
 * 销售订货-按汇总-按店员
 * ql-14439
 */
describe('销售订货-按店员', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160042.按店员-查询', async function () {
		//按店员的待发数计算方式与按批次查计算方式不一致，换成按明细查中数据
		let json = basiceJsonparam.salesOrderJson();
		json.sellerid = BASICDATA.staffid000;

		let param = format.qlParamsFormat({
			sellerid: BASICDATA.staffid000,
		}, true);
		let orignResult = await common.callInterface('ql-14439', param); //销售订货-按汇总-按店员

		let sfRes = await common.editBilling(json);
		json.sellerid = BASICDATA.staffid004;
		await common.editBilling(json); //使用其他店员开单

		let params = {
			'ql-14439': param,
			'ql-14431': { //销售订货-按明细查
				mainSellerid: BASICDATA.staffid000,
				today: true
			}
		};
		let qlRes = await common.getResults(params);

		let exp = {
			id: BASICDATA.staffid000,
			mat_name: LOGINDATA.name,
			num: common.add(orignResult.dataList[0].num, sfRes.params.totalnum),
			recvnum: orignResult.dataList[0].recvnum,
			diffnum: common.add(orignResult.dataList[0].diffnum, sfRes.params.totalnum),
			restnumsum: common.add(orignResult.dataList[0].restnumsum, sfRes.params.totalnum),
			mat_money: common.add(orignResult.dataList[0].mat_money, sfRes.params.totalsum)
		};
		//新增单据 数据验证
		common.isApproximatelyEqualAssert(exp, qlRes['ql-14439'].dataList[0]);
		//与销售订货-按明细查汇总值的已发数,未发数,差异数,数量对比
		qlRes['ql-14431'].sumrow.restnumsum = qlRes['ql-14431'].sumrow.restnum;
		common.isApproximatelyEqualAssert(qlRes['ql-14431'].sumrow, qlRes['ql-14439'].dataList[0]);

	});
});
