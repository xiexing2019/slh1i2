'use strict';

const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHander = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 销售订货--挂单相关
 */

describe('销售订货--挂单相关', function () {

	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160121,【销售订货-新增订货】挂单', async function () {
		let orderJson = format.jsonparamFormat(basiceJson.salesOrderJson());
		orderJson.invalidflag = '9';
		orderJson.remark = `hanging${common.getRandomStr(4)}`;
		let orderRes = await common.editBilling(orderJson);

		// 验证 销售订货--按挂单能查到挂单数据，且验证数据的正确性
		let batchInfo = await common.callInterface('ql-144331', format.qlParamsFormat({
			'clientid': orderJson.clientid,
			'shopid': orderJson.shopid,
			'remark': orderJson.remark
		}, true));
		expect(batchInfo, `销售订货按挂单有误:${JSON.stringify(batchInfo)}`).to.not.have.property('error');
		common.isApproximatelyEqualAssert(orderJson, batchInfo.dataList[0]);
		common.isApproximatelyEqualAssert(orderRes.params, batchInfo.dataList[0]);

		// 验证 销售订货--按批次查不能查到这条挂单数据
		batchInfo = await common.callInterface('ql-14433', format.qlParamsFormat({
			'clientid': orderJson.clientid,
			'id1': orderRes.result.billno,
			'id2': orderRes.result.billno,
			'shopid': orderJson.shopid,
			'remark': orderJson.remark
		}, true));
		expect(batchInfo.count, `销售订货按批次查显示了挂单的数据，错误`).to.be.equal('0');

		// 验证 销售开单--按批次查不能查到这条挂单数据
		batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat({
			'invid': orderJson.shopid,
			'billno1': orderRes.result.billno,
			'billno2': orderRes.result.billno,
			'dwid': orderJson.clientid,
			'rem': orderJson.remark
		}, true));
		expect(batchInfo.count, `销售开单按批次查显示了挂单的数据，错误`).to.be.equal('0');

		// 验证 销售开单--按挂单不能查到这条挂单数据
		batchInfo = await common.callInterface('ql-14222', format.qlParamsFormat({
			'shopid': orderJson.shopid,
			'dwid': orderJson.clientid,
			'remark': orderJson.remark
		}, true));
		expect(batchInfo.count, `销售开单按挂单显示了挂单的数据，错误`).to.be.equal('0');

	});

	it('160124,【销售订货-按挂单】查询、排序', async function () {
		let orderJson = format.jsonparamFormat(basiceJson.salesOrderJson());
		orderJson.invalidflag = '9';
		orderJson.remark = `hanging${common.getRandomStr(4)}`;
		let orderResult = await common.callInterface('sf-14401-1', {
			'jsonparam': orderJson
		});
		let orderRes = await common.editBilling(orderJson);

		// 验证 销售订货--按挂单查询条件的正确性,排序有统一脚本验证
		let batchInfo = await common.callInterface('ql-144331', format.qlParamsFormat({
			'clientid': orderJson.clientid,
			'shopid': orderJson.shopid,
			'remark': orderJson.remark,
			'sellerid': orderJson.sellerid,
		}, true));
		expect(batchInfo, `销售订货按挂单有误:${JSON.stringify(batchInfo)}`).to.not.have.property('error');
		expect(batchInfo.dataList[0], `销售订货按挂单批次号有误`).to.includes({
			'prepaybillBillno': '0'
		});
		common.isApproximatelyEqualAssert(orderJson, batchInfo.dataList[0]);
		common.isApproximatelyEqualAssert(orderRes.params, batchInfo.dataList[0]);
	});

	it('160126.挂单转正式单,以及订货成功后验证销售开单按批次查列表中对应生成的预付款单.rankA', async function () {
		// 挂单
		let orderJson = basiceJson.salesOrderJson();
		orderJson.remark = `hanging${common.getRandomStr(4)}`;
		let orderRes = await salesOrderReqHander.editSalesOrderHangBill(orderJson); //新增挂单

		// 修改挂单并转成销售订单
		let hangInfo = await salesOrderReqHander.salesOrderQueryBilling({
			pk: orderRes.result.pk,
			isPend: 1,
		}); //获取挂单信息
		hangInfo.result.clientid = BASICDATA.dwidLs;
		hangInfo.result.sellerid = BASICDATA.staffid004;
		for (let i = 0, length = hangInfo.result.details.length; i < length; i++) {
			hangInfo.result.details[i].styleid = BASICDATA.styleidAgc002;
		};
		orderRes = await salesOrderReqHander.hangToSalesOrderBill(hangInfo.result); //挂单转销售订单
		hangInfo = await salesOrderReqHander.salesOrderQueryBilling(orderRes.result.pk); //获取单据信息

		// 销售订货-按挂单 挂单转为正式单后,按挂单界面没有该条数据
		let batchInfo = await common.callInterface('ql-144331', format.qlParamsFormat({
			'clientid': orderRes.params.clientid,
			'shopid': orderRes.result.shopid,
			'remark': orderRes.params.remark,
			'sellerid': orderRes.params.sellerid,
		}, true));
		expect(batchInfo.count, `挂单转正式单，按挂单界面应该没有该条数据，但这里查到了，有误`).to.be.equal('0');

		// 销售订货-按批次查 有该条数据，数据全部正确，批次号显示为正常批次号
		let orderQlRes = await common.callInterface('ql-14433', format.qlParamsFormat({
			'clientid': orderRes.params.clientid,
			'id1': orderRes.result.billno,
			'id2': orderRes.result.billno,
			'shopid': orderRes.params.shopid,
			'remark': orderRes.params.remark
		}, true));
		expect(orderQlRes, `销售订货按批次查有误`).to.not.have.property('error');
		hangInfo.result.show_sellerid = _.last(hangInfo.result.show_sellerid.split(','));
		hangInfo.result.prodate = hangInfo.result.prodate.substr(5);
		let mapField = 'billno;show_shopid=invname;show_clientid=dwname;showFlag=flag;show_sellerid=deliver;cash;remit;card;totalsum;totalnum=num;flag=sendflag;remark;pk=id;prodate;';
		if (USEECINTERFACE == 1) mapField = `${mapField};optime`;   //二代这个接口不返回optime的
		common.isFieldsEqualAssert(hangInfo.result, orderQlRes.dataList[0], mapField);

		// 销售开单-按批次查 有该条数据，数据全部正确，批次号显示为正常批次号
		batchInfo = await common.callInterface('ql-142201', format.qlParamsFormat({
			'invid': orderRes.params.shopid,
			'orderno': orderRes.result.billno,
			'dwid': orderRes.params.clientid,
			'remark': orderRes.params.remark,
		}, true));

		common.isApproximatelyEqualAssert(batchInfo.dataList[0], {
			'remark': '预付款',
			'remit': hangInfo.result.remit || 0,
			'card': hangInfo.result.card || 0,
			'invname': LOGINDATA.depname,
			'prodate': hangInfo.result.prodate.substr(2),
			'cash': hangInfo.result.cash || 0,
			'actualpay': Number(hangInfo.result.remit || 0) + Number(hangInfo.result.cash || 0) + Number(hangInfo.result.card || 0),
			'billid': orderRes.result.billno,
			'orderid': orderRes.result.pk,
		});
	});

	it('160127.二次挂单', async function () {
		let json = basiceJson.salesOrderJson();
		// 挂单
		let orderRes = await salesOrderReqHander.editSalesOrderHangBill(json);

		// 二次挂单
		//修改款号和支付方式
		json.details.forEach((detail) => {
			detail.styleid = BASICDATA.styleidAgc002;
		});
		[json.cash, json.remit, json.card] = [json.card, json.cash, json.remit];
		json.pk = orderRes.result.pk;
		let editRes = await salesOrderReqHander.editSalesOrderHangBill(json);

		let hangInfo = await salesOrderReqHander.salesOrderQueryBilling({
			pk: editRes.result.pk,
			isPend: 1,
		}); //获取挂单信息
		common.isApproximatelyEqualAssert(editRes.params, hangInfo.result, ['orderversion']);
	});

	it('160129.挂单作废', async function () {
		// 挂单
		let orderJson = basiceJson.salesOrderJson();
		orderJson.remark = `${common.getCurrentDate()}hanging${common.getRandomStr(4)}`;
		let orderRes = await salesOrderReqHander.editSalesOrderHangBill(orderJson);

		// 作废挂单
		await salesOrderReqHander.cancelSaleorderBill({
			pk: orderRes.result.pk,
			isPend: 1,
		});

		// 验证挂单作废后,销售订货--按挂单界面查询不到刚才作废的挂单
		let batchInfo = await common.callInterface('ql-144331', format.qlParamsFormat({
			'clientid': orderRes.params.clientid,
			'shopid': orderRes.params.shopid,
			'remark': orderRes.params.remark,
			'sellerid': orderRes.params.sellerid,
		}, true));
		expect(batchInfo.count, `作废挂单，按挂单界面应该没有该条数据，但这里查到了，有误`).to.be.equal('0');

	});

	it('160130,【销售订货-按挂单】特殊货品', async function () {
		// 挂单
		let orderJson = format.jsonparamFormat(basiceJson.specialSalesOrderJson()); //带特殊货品
		orderJson.remark = `hanging${common.getRandomStr(4)}`;

		let hangRes = await salesOrderReqHander.editSalesOrderHangBill(orderJson);

		// 修改挂单并转成销售单
		let hangInfo = await salesOrderReqHander.salesOrderQueryBilling({
			pk: hangRes.result.pk,
			isPend: 1,
		}); //获取挂单信息

		common.isApproximatelyEqualAssert(hangRes.params, hangInfo.result);
	});

	it('160131,【销售订货-按挂单】终结订单', async function () {
		// 挂单
		let orderRes = await salesOrderReqHander.editSalesOrderHangBill(basiceJson.salesOrderJson());

		// 终结订单
		let finishRes = await salesOrderReqHander.saleOrderFinish({
			pk: orderRes.result.pk,
			isPend: 1, //是否挂单 是
			check: false,
		});
		expect(finishRes.result, `终结销售订单成功，但挂单是不允许终结订单的，错误`).to.includes({
			error: "不能终结挂起的订单"
		});
	});

	it('160171,总经理加载挂单时显示所有门店的挂单', async function () {
		await common.loginDo({
			'logid': '200',
			'pass': '000000'
		});
		let orderRes = await salesOrderReqHander.editSalesOrderHangBill(basiceJson.salesOrderJson());

		await common.loginDo();
		let hangingList = await common.callInterface('ql-144331', format.qlParamsFormat({}, false));
		expect(hangingList.dataList[0], `总经理可以看到其他门店挂单，这里没有查询到，错误`).to.includes({
			"invname": "中洲店"
		});
	});

	it('160192.未付挂单', async function () {
		let json = basiceJson.salesOrderJson();
		[json.cash, json.card, json.remit] = [0, 0, 0];
		let hangRes = await salesOrderReqHander.editSalesOrderHangBill(json);

		let param = format.qlParamsFormat({
			shopid: LOGINDATA.invid,
			clientid: hangRes.params.clientid,
		}, true);
		let qlRes = await common.callInterface('ql-144331', param);
		common.isApproximatelyEqualAssert(qlRes.dataList[0], Object.assign({
			'prepaybillBillno': '0'
		}, json));
	});
});
