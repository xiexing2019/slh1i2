'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesRequest = require('../../help/salesHelp/salesRequestHandler');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 按缺货查
 * ql-1641
 *
 * http://jira.hzdlsoft.com:7082/browse/SLH-20282
 * http://jira.hzdlsoft.com:7082/browse/SLH-4329
 */
describe('按缺货查', async function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('160081.查询', async function () {
		let styleAgc001 = await common.fetchMatInfo(BASICDATA.styleidAgc001);

		let param = format.qlParamsFormat({
			styleid: BASICDATA.styleidAgc001,
			stylename: styleAgc001.name,
			dwid: styleAgc001.dwid,
			brandid: styleAgc001.brandid,
			season: styleAgc001.season,
			marketdate1: styleAgc001.marketdate,
			marketdate2: styleAgc001.marketdate,
			invid: LOGINDATA.invid,
			// outofstock
		}, true);
		let qlRes = await common.callInterface('ql-1641', param);

		qlRes.dataList.map((obj) => {
			expect(obj).to.includes({
				stylecode: styleAgc001.code,
				stylename: styleAgc001.name
			}, '查询结果不匹配');
			expect(styleAgc001.show_colorids.includes(obj.colorname), `颜色显示错误 ${styleAgc001.show_colorids}不包含${obj.colorname}`).to.be.true;
			expect(styleAgc001.show_sizeids.includes(obj.sizename), `尺码显示错误 ${styleAgc001.show_sizeids}不包含${obj.sizename}`).to.be.true;
		});
	});

	// 新建(flag=0)||部分发货(flag=1) && 明细未发数(num-delivernum)>0
	it('160083.检查订货未发数、采购未到数、库存(含在途)', async function () {
		let params = {
			'ql-14431': { //销售订货-按明细查
				propdresStyleid: BASICDATA.styleidAgc001,
				today: true,
				mainShopid: LOGINDATA.invid,
				search_list: 0,
			},
			'ql-1641': { //销售订货-按缺货查
				styleid: BASICDATA.styleidAgc001,
				invid: LOGINDATA.invid,
				today: true
			},
			'ql-1932': { //货品管理-当前库存
				propdresStyleid: BASICDATA.styleidAgc001,
				invid: LOGINDATA.invid,
			},
		};
		let qlRes = await common.getResults(params);

		// 销售订货-按缺货查 订货未发数 = 销售订货-按明细查 待发数
		expect(qlRes['ql-1641'].sumrow.ordernotsend, '订货未发数错误').to.equal(qlRes['ql-14431'].sumrow.restnum);

		let [expInstock, actInstock, expPurednotin, actPurednotin] = [{}, {}, {}, {}];
		qlRes['ql-1641'].dataList.map((obj) => {
			expInstock[`${obj.colorname}-${obj.sizename}`] = obj.instock;
			expPurednotin[`${obj.colorname}-${obj.sizename}`] = obj.purednotin;
		});
		qlRes['ql-1932'].dataList.map((obj) => {
			actInstock[`${obj.propdresColorName}-${obj.propdresSizeName}`] = common.add(obj.invnum, obj.onroadnum).toString();
			actPurednotin[`${obj.propdresColorName}-${obj.propdresSizeName}`] = obj.purednum;
		});
		// 销售订货-按缺货查 库存(含在途) = 货品管理-当前库存 库存+在途数
		if (USEECINTERFACE == 2) {
			for (let key in actInstock) {
				actInstock[key] = Number(actInstock[key]);
			}
		};

		expect(actInstock).to.includes(expInstock, '库存(含在途)错误');
	});

	it('160086.检查销售订货单的日期对订货未发数的影响', async function () {
		let params = {
			'ql-14431': { //销售订货-按明细查
				propdresStyleid: BASICDATA.styleidAgc001,
				mainShopid: LOGINDATA.invid,
				search_list: 0,
				prodate1: common.getDateString([0, 0, -2]),
			},
			'ql-1641': { //销售订货-按缺货查
				styleid: BASICDATA.styleidAgc001,
				invid: LOGINDATA.invid,
				prodate1: common.getDateString([0, 0, -2]),
			}
		};
		let qlRes = await common.getResults(params);

		// 销售订货-按缺货查 订货未发数 = 销售订货-按明细查 待发数
		expect(qlRes['ql-1641'].sumrow.ordernotsend, '订货未发数错误').to.equal(qlRes['ql-14431'].sumrow.restnum);
	});

	// 需要按顺序执行
	// 缺货数 = 订货未发数-库存(含在途)-采购未到数
	describe('160119.缺货数检查', async function () {
		let goodRes, json, param;
		before(async () => {
			goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品

			json = {
				interfaceid: 'sf-14401-1',
				details: [{
					styleid: goodRes.pk,
					sizeid: 'M',
					colorid: 'BaiSe',
					price: '200',
					num: 5
				}],
			};

			param = format.qlParamsFormat({
				styleid: goodRes.pk,
				invid: BASICDATA.shopidCqd
			}, true);
		});
		//生成调拨单,让库存(含在途)>订货未发数,采购未到数为0,检查销售订货-按缺货查界面 缺数显示
		//库存(含在途) 10  订货未发数 5
		it('1.调拨单,库存(含在途)>订货未发数,采购未到数为0', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			let outJson = basiceJsonparam.outJson();
			outJson.details[0].styleid = goodRes.pk;
			outJson.details[0].num = 10;
			await common.editBilling(outJson); //中洲店向常青店 调拨出库

			await common.loginDo();
			await common.editBilling(json); //销售订货

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '5', //订货未发数
				instock: '10', //库存（含在途）
				purednotin: '0', //采购未到数
				outofstock: '0' //缺货数
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');
		});
		//生成采购订货单,让采购未到数>订货未发数,库存(含在途)为0,检查销售订货-按缺货查界面 缺数显示
		//采购未到数 10
		it('2.采购订货单,采购未到数>订货未发数,库存(含在途)为0', async function () {
			let purOrderJson = _.cloneDeep(json);
			purOrderJson.interfaceid = 'sf-22101-1';
			purOrderJson.details[0].num = 10;
			await common.editBilling(purOrderJson); //新增采购订单

			let salesJson = _.cloneDeep(json);
			salesJson.interfaceid = 'sf-14211-1';
			salesJson.details[0].num = 10;
			await common.editBilling(salesJson); //销售开单10件 使库存+在途=0

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '5', //订货未发数
				instock: '0', //库存（含在途）
				purednotin: '10', //采购未到数
				outofstock: '0' //缺货数
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');
		});
		//产生单据,让(库存(含在途)+采购未到数)>订货未发数,检查销售订货-按缺货查界面 缺数显示
		it('3.(库存(含在途)+采购未到数)>订货未发数', async function () {
			let purJson = _.cloneDeep(json);
			purJson.interfaceid = 'sf-14212-1';
			await common.editBilling(purJson); //新增采购入库单

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '5', //订货未发数
				instock: '5', //库存（含在途）
				purednotin: '10', //采购未到数
				outofstock: '0' //缺货数
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');
		});

		it('160084.1.当缺货数>0,检查该款号是否显示', async function () {
			json.details[0].num = 20;
			await common.editBilling(json);

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '25', //订货未发数
				instock: '5', //库存（含在途）
				purednotin: '10', //采购未到数
				outofstock: '10' //缺货数
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');
		});
	});

	//本用例为了方便凑数据,需要按顺序执行
	describe('两个门店对同一款号缺货,查看是否会相互影响', function () {
		let orderJson, param, paramCKD, paramZZD;
		before(async () => {
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品
			// console.log(`goodRes = ${JSON.stringify(goodRes)}`);
			orderJson = {
				interfaceid: 'sf-14401-1',
				details: [{
					styleid: goodRes.pk,
					sizeid: 'M',
					colorid: 'BaiSe',
					price: '200',
					num: 5
				}],
			};

			param = format.qlParamsFormat({
				styleid: goodRes.pk
			}, true);

			paramCKD = _.cloneDeep(param);
			paramCKD.invid = BASICDATA.shopidCqd;

			paramZZD = _.cloneDeep(param);
			paramZZD.invid = BASICDATA.shopidZzd;
		});
		it('160100.A门店缺货,B门店不缺货,总的不缺货', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			await common.editBilling(orderJson); //中洲店新增订货单 缺货

			await common.loginDo();
			let purJson = _.cloneDeep(orderJson);
			purJson.interfaceid = 'sf-14212-1';
			await common.editBilling(purJson); //常青店新增入库单

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '5',
				instock: '5',
				purednotin: '0',
				outofstock: '0'
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');

			qlRes = await common.callInterface('ql-1641', paramCKD);
			expect(qlRes.dataList.length, '常青店查询出错').to.equal(0);

			qlRes = await common.callInterface('ql-1641', paramZZD);
			expect(qlRes.dataList[0]).to.includes({
				ordernotsend: '5', //订货未发数
				instock: '0', //库存（含在途）
				purednotin: '0', //采购未到数
				outofstock: '5' //缺货数
			}, '中洲店查询出错');
		});

		it('160099.A门店缺货,B门店不缺货,总的缺货', async function () {
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			await common.editBilling(orderJson); //中洲店新增订货单 缺货

			await common.loginDo();
			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '10',
				instock: '5',
				purednotin: '0',
				outofstock: '5'
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock')
			qlRes = await common.callInterface('ql-1641', paramCKD);
			expect(qlRes.dataList.length, '常青店查询出错').to.equal(0);

			qlRes = await common.callInterface('ql-1641', paramZZD);
			expect(qlRes.dataList[0]).to.includes({
				ordernotsend: '10',
				instock: '0',
				purednotin: '0',
				outofstock: '10'
			}, '中洲店查询出错');
		});

		it('160085.A门店缺货,B门店缺货', async function () {
			await common.loginDo();
			orderJson.details[0].num = 10;
			await common.editBilling(orderJson); //常青店新增订货单 缺货

			let qlRes = await common.callInterface('ql-1641', param);
			let exp = {
				ordernotsend: '20',
				instock: '5',
				purednotin: '0',
				outofstock: '15'
			};
			common.isFieldsEqualAssert(exp, qlRes.dataList[0], 'ordernotsend;instock;purednotin;outofstock');
			qlRes = await common.callInterface('ql-1641', paramCKD);
			expect(qlRes.dataList[0]).to.includes({
				ordernotsend: '10',
				instock: '5',
				purednotin: '0',
				outofstock: '5'
			}, '常青店查询出错');

			qlRes = await common.callInterface('ql-1641', paramZZD);
			expect(qlRes.dataList[0]).to.includes({
				ordernotsend: '10',
				instock: '0',
				purednotin: '0',
				outofstock: '10'
			}, '中洲店查询出错');
		});
	});

});
