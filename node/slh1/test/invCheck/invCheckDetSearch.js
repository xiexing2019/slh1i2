'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');

describe('盘点管理-按明细查-slh2', function () {
	this.timeout(30000);
	let styleInfo = {};

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
	});

	it('180014.组合查询', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

		let param = format.qlParamsFormat({
			mainBillno: sfRes.result.billno,
			invid: sfRes.params.invid,
			propdresStyleid: sfRes.params.details[0].styleid,
			propdresColorid: sfRes.params.details[0].colorid,
			propdresSizeid: sfRes.params.details[0].sizeid,
			propdresStyleBrandid: styleInfo.brandid,
			propdresStyleClassid: styleInfo.classid,
			// opstaffName 操作人
			optime1: common.getCurrentDate(),
			optime2: common.getCurrentDate(),
			flag: 0, //是否盘点处理
			prostylename: styleInfo.name,
		}, false);
		let qlRes = await common.callInterface('ql-1924', param); //盘点管理-按明细查
		let exp = invCheckReqHandler.getInvCheckDetSearchListExp(styleInfo, sfRes, qfRes);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('180015.1.品牌查询条件', async function () {
		let param = format.qlParamsFormat({
			propdresStyleBrandid: styleInfo.brandid,
			optime1: common.getDateString([0, 0, -7]),
		}, false);
		let qlRes = await common.callInterface('ql-1924', param); //盘点管理-按明细查
		qlRes.dataList.map((data) => {
			expect(data.propdresStyleBrandid).to.equal(styleInfo.show_brandid);
		});
	});

	it('180015.2.类别查询条件', async function () {
		let param = format.qlParamsFormat({
			propdresStyleClassid: styleInfo.classid,
			optime1: common.getDateString([0, 0, -7]),
		}, false);
		let qlRes = await common.callInterface('ql-1924', param); //盘点管理-按明细查
		qlRes.dataList.map((data) => {
			expect(data.propdresStyleClsName).to.equal(styleInfo.show_classid);
		});
	});

});
