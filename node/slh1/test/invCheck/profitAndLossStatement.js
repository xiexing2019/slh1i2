'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');

/*
 * 盈亏表
 * 涉及盈亏表数据验证的用例尽量全放到这里
 * 只在最后进行盘点撤销,保证执行本模块时，盈亏表有多条数据
 */
describe.skip('盘点管理-盈亏表-slh2', function () {
	this.timeout(30000);
	let styleInfo;

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();

		await common.setGlobalParam('checksumprice', 1); //盘点核算价格 销价1

		// 先尝试进行一次盘点处理 方便后面凑数据
		await invCheckReqHandler.processCheck();

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		// console.log(`styleInfo = ${JSON.stringify(styleInfo)}`);
	});

	after(async () => {
		//撤销盘点处理单 防止影响其他模块的用例
		await invCheckReqHandler.checkMainCancelByDate();
	});

	it('180049.盈亏金额的正确性', async function () {
		let startInvStock = {};
		let result = await common.callInterface('ql-1932', format.qlParamsFormat({
			propdresStyleid: BASICDATA.styleidAgc001,
			invid: LOGINDATA.invid
		})); //货品管理-当前库存 获取库存起始值
		result.dataList.map((obj) => {
			startInvStock[`${obj.propdresColorName}-${obj.propdresSizeName}`] = obj.invnum; //库存
		});
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson()); //新增盘点单
		let processRes = await invCheckReqHandler.processCheck(); //盘点处理
		let processtime = common.getCurrentTime();
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk); //获取盘点单信息
		let param = format.qlParamsFormat({
			propdresStyleid: BASICDATA.styleidAgc001,
			mainInvid: LOGINDATA.invid,
			billno1: processRes.result.billno,
			billno2: processRes.result.billno
		}, true);
		let qlRes = await common.callInterface('ql-1926', param); //盘点管理-盈亏表
		let exp = [];
		qlRes.dataList.map((data) => {
			let key = `${data.propdresColorid}-${data.propdresSizeid}`;
			expect(startInvStock, `未盘点到${key}`).to.have.property(key); //部分盘点会盘点该款号所有有库存的颜色尺码

			let json = {
				key: key,
				optime: processtime,
				mainProdate: common.getCurrentDate(),
				// "id": "0",
				price: styleInfo.stdprice1, //销价1 在before中设置
				rem: '',
				mainBillno: processRes.result.billno,
				checkprenum: startInvStock[key], //盘前
				checkpresum: common.mul(startInvStock[key], styleInfo.stdprice1), //盘前金额 按销价1
			};
			for (let i = 0, length = qfRes.result.details.length; i < length; i++) {
				if (qfRes.result.details[i].show_colorid == data.propdresColorid && qfRes.result.details[i].show_sizeid == data.propdresSizeid) {
					data.isMatch = true;
					json.recvnum = qfRes.result.details[i].num; //盘后
					json.recvsum = common.mul(qfRes.result.details[i].num, styleInfo.stdprice1); //盘后金额 按销价1
				};
			};
			if (!data.isMatch) {
				json.rem = '盘点清零';
				json.recvnum = 0; //盘后
				json.recvsum = 0; //盘后金额
			};
			json.num = common.sub(json.recvnum, json.checkprenum);
			json.total = common.sub(json.recvsum, json.checkpresum);
			exp.push(json);
		});
		common.isApproximatelyArrayAssert(exp, qlRes.dataList);
	});

	//数据在180049中验证
	it('180034.查询', async function () {
		let param = format.qlParamsFormat({
			mainInvid: LOGINDATA.invid,
			propdresStyleid: BASICDATA.styleidAgc001,
			propdresStyleYear1: styleInfo.year1, //年份
			propdresStyleBrandid: styleInfo.brandid,
			propdresStyleClassid: styleInfo.classid,
			propdresStyleDwid: styleInfo.dwid,
			mainInvid: LOGINDATA.invid,
		}, true);
		let qlRes = await common.callInterface('ql-1926', param); //盘点管理-盈亏表
		let exp = {
			mainProdate: common.getCurrentDate(),
			mainInventoryName: LOGINDATA.depname,
			propdresStyleDwxxName: styleInfo.show_dwid,
			propdresStyleBrandid: styleInfo.show_brandid, //brandid
			propdresStyleSeason: styleInfo.show_season,
			propdresStyleName: styleInfo.name,
			propdresStyleCode: styleInfo.code
		};
		expect(qlRes.dataList.length, '查询无结果').to.above(0);
		qlRes.dataList.forEach((data) => common.isApproximatelyEqualAssert(exp, data));
	});

	//汇总值验证
	//因为会一直撤销盘点处理单，所以盈亏表会没有数据,汇总值只能在这验证
	it('180037.底部盈亏总数数值检查', async function () {
		let param = format.qlParamsFormat({
			prodate1: common.getDateString([0, -1, 0]),
		}, false);
		let qlRes = await common.callInterface('ql-1926', param); //盘点管理-盈亏表

		expect(qlRes, `查询出错:${JSON.stringify(qlRes)}`).not.to.have.property('error');
		expect(qlRes.dataList.length, `查询无果,无法进行汇总验证`).to.above(0);
		let sum = {};
		for (let i = 0, length = qlRes.dataList.length; i < length; i++) {
			sum = common.addObject(sum, qlRes.dataList[i]);
		};
		common.isApproximatelyEqualAssert(qlRes.sumrow, sum);
	});

});
