'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');
const reqHandlerHelp = require('../../help/reqHandlerHelp');

/*
 * 新增修改盘点单
 */
describe('盘点管理-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();
	});

	after(async () => {
		//撤销盘点处理单 防止影响其他模块的用例
		await invCheckReqHandler.checkMainCancelByDate();
	});

	it('180019.新增盘点.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('180007.修改未处理盘点单', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		qfRes.result.details[1] = { //模拟删除一条明细，新增一条明细
			sizeid: 'XL',
			colorid: 'BaiSe',
			matCode: 'Agc001',
			price: '0',
			rem: '修改明细',
			num: '3000',
		};
		sfRes = await common.editBilling(qfRes.result);
		qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
	});

	it('180008.修改已处理盘点单', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		await invCheckReqHandler.processCheck(); //盘点处理
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		qfRes.result.details[1] = { //模拟删除一条明细，新增一条明细
			sizeid: 'XL',
			colorid: 'BaiSe',
			matCode: 'Agc001',
			price: '0',
			rem: '修改明细',
			num: '3000',
		};
		sfRes = await common.editBilling(qfRes.result, false);
		expect(sfRes.result).to.includes({
			error: USEECINTERFACE == 1 ? '盘点单已处理，不允许更改' : '该盘点单已处理'
		});
	});

	it('180009.删除未处理盘点单', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		await invCheckReqHandler.deleteCheckBill(sfRes.result.pk);

		let param = format.qlParamsFormat({
			billno_gte: sfRes.result.orderno,
			billno_lte: sfRes.result.orderno,
			invid: sfRes.params.invid,
		}, false);
		let qlRes = await common.callInterface('ql-1923', param);
		expect(qlRes.dataList.length).to.equal(0); //按批次查中无该盘点单
	});

	it('180010.删除已处理盘点单', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		await invCheckReqHandler.processCheck(); //盘点处理
		let result = await invCheckReqHandler.deleteCheckBill(sfRes.result.pk, false);
		expect(result).to.includes({
			error: USEECINTERFACE == 1 ? '盘点单已处理，不允许删除' : '该盘点单已处理'
		});
	});

	describe('盘点时是否允许输入负数', function () {
		let json = basiceJsonparam.invCheckJson();
		json.details[0].num = -1; //输入负数

		after(async () => {
			await common.setGlobalParam('inv_check_allownegative', 0); //盘点时是否允许输入负数 默认不开启
		});
		it('180105.盘点时是否允许输入负数-允许', async function () {
			await common.setGlobalParam('inv_check_allownegative', 1); //盘点时是否允许输入负数 开启
			await common.editBilling(json);
		});
		it('180106.盘点时是否允许输入负数-不允许', async function () {
			await common.setGlobalParam('inv_check_allownegative', 0); //盘点时是否允许输入负数 默认不开启
			let sfRes = await common.editBilling(json, false);
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '受参数“盘点时是否允许输入负数”控制，当前盘点数量不允许输入负数' : '请检查盘点单数量'
			});
		});
	});

	describe('未盘点款号', function () {
		let results = {};
		after(async () => {
			await common.loginDo({
				logid: '200',
			});
		});
		it('180094.检查数据正确性', async function () {
			let params = {
				'ql-1924': { //盘点管理-按明细查
					optime1: common.getCurrentDate(),
					optime2: common.getCurrentDate(),
					invid: LOGINDATA.invid,
				},
				'ql-1929': { //盘点管理-未盘点款号
					optime1: common.getCurrentDate(), //开单时间 必填
					optime2: common.getCurrentDate(),
					invid: LOGINDATA.invid,
				},
				'ql-1932': { //货品管理-当前库存
					invid: LOGINDATA.invid,
				},
			};
			results = await common.getResults(params);
			let checkStyleCode = common.dedupe(results['ql-1924'].dataList.map((obj) => obj.propdresStyleCode)); //获取已盘点的款号 code

			let invInfo = {}; //未盘点款号信息
			results['ql-1929'].dataList.forEach((obj) => invInfo[obj.stylecode] = obj.invnum);
			expect(Object.keys(invInfo), `未盘点款号中包含盘点款号信息`).not.to.includes(checkStyleCode);

			let curInv = {}; //当前库存信息
			results['ql-1932'].dataList.forEach((obj) => curInv[obj.propdresStyleCode] = common.add(obj.invnum, curInv[obj.propdresStyleCode] || 0));
			common.isApproximatelyEqualAssert(invInfo, curInv); //未盘点款号的库存验证
		});
		//取180094结果里未盘点款号的一条信息作为查询目标
		it('180092.查询条件检查', async function () {
			TESTCASE = {
				describe: "未盘点款号查询条件查询条件检查",
				expect: "查询结果和查询条件一致"

			}
			let styleId = await common.callInterface('cs-getpk-style', {
				code: results['ql-1929'].dataList[0].stylecode
			}).then((obj) => obj.id); //根据款号获取货品id
			let styleInfo = await common.fetchMatInfo(styleId); //获取货品详细信息

			let param = {
				invid: LOGINDATA.invid,
				optime1: common.getCurrentDate(), //开单时间 必填
				optime2: common.getCurrentDate(),
				billno1: results['ql-1924'].dataList[results['ql-1924'].dataList.length - 1].mainBillno,
				billno2: results['ql-1924'].dataList[0].mainBillno,
				styleid: styleId,
				stylename: styleInfo.name,
				classid: styleInfo.classid,
				brandid: styleInfo.brandid,
				dwid: styleInfo.dwid,
			};
			let qlRes = await common.callInterface('ql-1929', param); //盘点管理-未盘点款号
			common.isApproximatelyEqualAssert({
				invname: LOGINDATA.invname,
				stylename: styleInfo.name,
				stylecode: styleInfo.code,
				onroadnum: '0'
			}, qlRes.dataList[0]);
		});
		//取180094结果里未盘点款号的一条信息停用
		it('180089.已停用的款号不显示', async function () {
			TESTCASE = {
				describe: "未盘点款号-已停用的款号不应显示",
				expect: "未盘点款号-搜索停用的款号，qlRes.count=0",
			}
			let styleId;
			// 盘点管理-未盘点款号
			for (let i = 0, length = results['ql-1929'].dataList.length; i < length; i++) {
				if (results['ql-1929'].dataList[i].stylename.includes('name')) { //找一个自动生成的款号
					styleId = await common.callInterface('cs-getpk-style', {
						code: results['ql-1929'].dataList[i].stylecode
					}).then((obj) => obj.id);
					break;
				};
			};
			await reqHandlerHelp.csIFCHandler({
				interfaceid: 'cs-1511-33',
				pk: styleId,
				errorMsg: '停用错误',
			});
			if (USEECINTERFACE == 2) await common.delay(1000);
			let param = {
				optime1: common.getCurrentDate(), //开单时间 必填
				optime2: common.getCurrentDate(),
				styleid: styleId,
			};
			let qlRes = await common.callInterface('ql-1929', param); //盘点管理-未盘点款号
			expect(qlRes.count, `盘点管理-未盘点款号中依然显示停用款号`).to.equal('0');
		});

		//常青店角色验证查询权限
		it('180095.1.店长和开单员只能看自己门店的数据', async function () {
			await common.loginDo({
				logid: '004',
				pass: '000000',
			}); //常青店店长登陆
			let param = {
				invid: BASICDATA.shopidZzd,
				optime1: common.getCurrentDate(), //开单时间 必填
				optime2: common.getCurrentDate(),
			};
			let qlRes = await common.callInterface('ql-1929', param); //盘点管理-未盘点款号
			expect(qlRes.count, `盘点管理-未盘点款号 店长可以看到其他门店数据`).to.equal('0');
		});
		it('180095.2.总经理可以看到其他门店的数据', async function () {
			await common.loginDo(); //常青店总经理
			let param = {
				invid: BASICDATA.shopidZzd,
				optime1: common.getCurrentDate(), //开单时间 必填
				optime2: common.getCurrentDate(),
			};
			let qlRes = await common.callInterface('ql-1929', param); //盘点管理-未盘点款号
			expect(Number(qlRes.count), `盘点管理-未盘点款号 总经理看不到其他门店数据`).to.above(0);
		});
	});

});
