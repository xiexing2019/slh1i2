'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');

describe('盘点管理-按批次查-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();
	});

	it('180001.组合查询', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

		let param = format.qlParamsFormat({
			billno_gte: sfRes.result.orderno,
			billno_lte: sfRes.result.orderno,
			invid: qfRes.result.invid,
			flag: 0,
			deliver: qfRes.result.deliver,
			optime_gte: common.getCurrentDate(),
			optime_lte: common.getCurrentDate(),
			// billid billid
			id1: sfRes.result.orderno,
			id2: sfRes.result.orderno,
			optime1: common.getCurrentDate(),
			optime2: common.getCurrentDate(),
		}, false);
		let qlRes = await common.callInterface('ql-1923', param); //盘点管理-按批次查
		let exp = invCheckReqHandler.getInvCheckMainSearchListExp(sfRes, qfRes);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('180002.批次从/批次到验证', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let param = format.qlParamsFormat({
			billno_gte: Number(sfRes.result.orderno) - 10,
			billno_lte: Number(sfRes.result.orderno),
		}, false);
		let qlRes = await common.callInterface('ql-1923', param);
		qlRes.dataList.map((data) => {
			expect(Number(data.billno)).to.within(param.billno_gte, param.billno_lte);
		});
	});

	it('180048.处理人检查', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
		let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

		qfRes.result.action = 'edit';
		qfRes.result.interfaceid = sfRes.params.interfaceid;
		qfRes.result.details[0].num = 3000;
		sfRes = await common.editBilling(qfRes.result); //修改未处理盘点单
		qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk); //重新获取盘点单信息

		let param = format.qlParamsFormat({
			billno_gte: sfRes.result.orderno,
			billno_lte: sfRes.result.orderno,
			invid: sfRes.params.invid,
		}, false);
		let qlRes = await common.callInterface('ql-1923', param); //盘点管理-按批次查
		let exp = invCheckReqHandler.getInvCheckMainSearchListExp(sfRes, qfRes);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	describe('非总经理', function () {
		after(async () => {
			await common.loginDo({
				logid: '200',
			});
		});
		it('180047.单据检查', async function () {
			await common.loginDo({
				logid: '004',
				pass: '000000'
			});
			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
				optime_gte: common.getDateString([0, 0, -7]),
				optime1: common.getDateString([0, 0, -7]),
			}, false);
			let qlRes = await common.callInterface('ql-1923', param); //盘点管理-按批次查
			expect(qlRes.count).to.equal('0');
		});
		it('18058.修改其他门店的未处理盘点单', async function () {
			await common.loginDo({
				logid: '200',
			});
			let sfRes = await common.editBilling(basiceJsonparam.invCheckJson());
			let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk);

			await common.loginDo();
			qfRes.result.interfaceid = sfRes.params.interfaceid;
			qfRes.result.action = 'edit';
			qfRes.result.details[0].num = 4000;
			sfRes = await common.editBilling(qfRes.result, false);
			expect(sfRes.result).to.includes({
				error: '不允许修改其它门店的盘点单',
			});
		});
	});

});
