'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');

/*
 * 盘点处理+处理记录 相关用例
 * 跑完后需要撤销盘点处理单 否则会影响其他模块的用例
 */
describe('盘点管理-盘点处理-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();

		//先进行一次盘点处理 将之前的盘点单先处理掉
		// await invCheckReqHandler.processCheck();
	});

	after(async () => {
		await invCheckReqHandler.checkMainCancelByDate();
	});

	//调拨用例在常青店验证,因此常青店含有在途数
	it('180028.存在在途数的门店进行盘点处理', async function () {
		TESTCASE = {
			describe: 'inv_check_allowinout=0时,存在在途数的门店进行盘点处理',
		};
		await common.setGlobalParam('inv_check_allowinout', 0); //盘点处理是否允许有未处理出入库流水 不允许
		let checkResult = await invCheckReqHandler.processCheck({
			invid: BASICDATA.shopidCqd,
		}, false);
		//二代是先获取要执行的盘点单，如果没有，后边就不执行了
		expect(checkResult.result).to.includes({
			error: USEECINTERFACE == 1 ? '本仓库(店铺)还有调拨单没有接收入库，请全部接收之后再做盘点处理' : '本仓库(店铺)没有新录入的盘点单，请核对',
		});
	});

	//数据有点难凑先放着。。
	//匹配dataList中的处理时间来获取盘点数量
	it('180056.盘点处理后,库存检查.rankA', async function () {
		let startDate = common.getDateString([0, 0, -2]);
		//盘点处理
		await invCheckReqHandler.processCheck({
			checkstartdate: common.getDateString([0, 0, -3]),
		});

		let json = format.jsonparamFormat(basiceJsonparam.invCheckJson());
		let params = {
			'ql-1932': { //货品管理-当前库存
				invid: LOGINDATA.invid,
				propdresStyleid: json.details[0].styleid,
				propdresColorid: json.details[0].colorid,
				propdresSizeid: json.details[0].sizeid,
				search_list: 0,
			},
			'ql-1924': { //盘点管理-按明细查
				invid: LOGINDATA.invid,
				propdresStyleid: json.details[0].styleid,
				propdresColorid: json.details[0].colorid,
				propdresSizeid: json.details[0].sizeid,
				optime1: common.getDateString([-2, 0, 0]),
				optime2: startDate,
				flag: 1, //是否盘点处理
				search_list: 0,
			},
			'ql-22302': { //采购入库-按明细查
				mainShopid: LOGINDATA.invid,
				propdresStyleid: json.details[0].styleid,
				propdresColorid: json.details[0].colorid,
				propdresSizeid: json.details[0].sizeid,
				prodate1: startDate,
				search_list: 0,
			},
			'ql-1209': { //销售开单按明细查
				shopid: LOGINDATA.invid,
				styleid: json.details[0].styleid,
				colorid: json.details[0].colorid,
				sizeid: json.details[0].sizeid,
				prodate1: startDate,
				search_list: 0,
			},
			'ql-1865': { //门店调出-按明细查
				propdresStyleid: json.details[0].styleid,
				propdresColorid: json.details[0].colorid,
				propdresSizeid: json.details[0].sizeid,
				mainShopid: LOGINDATA.invid,
				mainFlag: 0, //接收状态
				prodate1: startDate,
				search_list: 0,
			},
			'ql-1866': { //门店调入-按明细查
				styleid: json.details[0].styleid,
				colorid: json.details[0].colorid,
				sizeid: json.details[0].sizeid,
				shopid: LOGINDATA.invid,
				prodate1: startDate,
				search_list: 0,
			}
		};
		let qlRes = await common.getResults(params);
		console.log(`qlRes = ${JSON.stringify(qlRes)}`);
		console.log(`当前库存=${qlRes['ql-1932'].sumrow.invnum}`);
		let sum = Number(qlRes['ql-1924'].sumrow.num) - Number(qlRes['ql-1209'].sumrow.num) -
			Number(qlRes['ql-1865'].sumrow.num) + Number(qlRes['ql-22302'].sumrow.realnum) + Number(qlRes['ql-1866'].sumrow.num);
		console.log(`sum=${sum}`);
	});

	it.skip('180096.5.处理记录-盘点处理数量检查', async function () {
		let processRes = await invCheckReqHandler.processCheck(); //盘点处理
		let qlRes1 = await common.callInterface('ql-1923', format.qlParamsFormat({
			processtime_gte: common.getCurrentDate(),
			processtime1: common.getCurrentDate(),
			search_list: 0,
		}, false)); //盘点管理-按批次查
		let qlRes2 = await common.callInterface('ql-19251', {
			billid: processRes.result.pk
		}); //处理记录-详细界面
		expect(qlRes1.count, `盘点处理条目数=${qlRes1.count},处理记录-详细界面条目数=${qlRes2.count}`).to.equal(qlRes2.count);
	});

	describe('180027.处理日期设置', function () {
		before(async () => {
			await common.editBilling(basiceJsonparam.invCheckJson()); //生成盘点单
		});
		afterEach(async () => {
			await invCheckReqHandler.checkMainCancelByDate();
		});
		it('1.将日期设置为后天,然后点部分处理', async function () {
			TESTCASE = {
				describe: "盘点日期选择后天，进行部分盘点",
				expect: "提示--盘点处理日期最多选到第二天，请重新选择",
				jira: 'SLHSEC-7488',
			}
			let checkResult = await invCheckReqHandler.processCheck({
				checkstartdate: common.getDateString([0, 0, 2])
			}, false);
			expect(checkResult.result).to.includes({
				error: '盘点处理日期最多选到第二天，请重新选择'
			});
		});
		it('2.将日期设置为明天,然后点部分处理', async function () {
			await invCheckReqHandler.processCheck({
				checkstartdate: common.getDateString([0, 0, 1])
			});
		});
		it('3.将日期设置为今天,然后点部分处理', async function () {
			await invCheckReqHandler.processCheck();
		});
	});

	describe('180033.盘点撤销.rankA', function () {
		let sfRes, processRes, processtime;
		before(async () => {
			await invCheckReqHandler.processCheck({}, false); //盘点处理
		});
		after(async () => {
			await invCheckReqHandler.checkMainCancelByDate();
		});
		it('1.盘点撤销', async function () {
			sfRes = await common.editBilling(basiceJsonparam.invCheckJson()); //生成盘点单

			if (USEECINTERFACE == 2) await common.delay(1500);

			processRes = await invCheckReqHandler.processCheck(); //盘点处理
			processtime = common.getCurrentTime();
			await invCheckReqHandler.checkMainCancel(processRes.result.pk); //盘点撤销
			if (USEECINTERFACE == 2) await common.delay(1000);
		});
		it('2.按批次查', async function () {
			let qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk || sfRes.result.val);

			let param = format.qlParamsFormat({
				billno_gte: sfRes.result.orderno,
				billno_lte: sfRes.result.orderno,
				invid: qfRes.result.invid,
				flag: 0,
				id1: sfRes.result.orderno,
				id2: sfRes.result.orderno,
				optime1: common.getCurrentDate(),
				optime2: common.getCurrentDate(),
			}, false);
			let qlRes = await common.callInterface('ql-1923', param); //盘点管理-按批次查
			let exp = invCheckReqHandler.getInvCheckMainSearchListExp(sfRes, qfRes);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('3.盘点记录', async function () {
			let param = format.qlParamsFormat({
				id1: processRes.result.billno,
				id2: processRes.result.billno,
				invid: LOGINDATA.invid,
				invalidflag: 1,
			}, true);
			let qlRes = await common.callInterface('ql-1928', param); //盘点管理-处理记录
			expect(qlRes.count).to.equal('1');
			let exp = {
				id: processRes.result.pk,
				optime: processtime,
				invname: LOGINDATA.depname,
				invalidflag: 1,
				remark: '部分盘点',
				totalnum: sfRes.params.totalnum,
				billno: processRes.result.billno,
				prodate: common.getCurrentDate(),
				opname: LOGINDATA.name,
			};
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0], ['optime']);
		});
		it('180100.4.盘点处理-详细界面', async function () {
			TESTCASE = {
				describe: '盘点撤销后,进入盘点记录详细界面,内容显示为空',
				expect: `ql-19251返回count为0`,
			};
			let qlRes = await common.callInterface('ql-19251', {
				billid: processRes.result.pk
			}); //盘点撤销后,点进详细界面,显示为空
			expect(qlRes.count, `billid=${processRes.result.pk}`).to.equal('0');
		});
		it('4.盈亏表', async function () {
			let param = format.qlParamsFormat({
				mainInvid: LOGINDATA.invid,
				billno1: processRes.result.billno,
				billno2: processRes.result.billno,
			}, true);
			let qlRes = await common.callInterface('ql-1926', param); //盘点管理-盈亏表
			expect(qlRes.count).to.equal('0');
		});
		it('5.撤销时间点靠前的批次', async function () {
			processRes = await invCheckReqHandler.processCheck();

			await common.editBilling(basiceJsonparam.invCheckJson()); //生成盘点单
			if (USEECINTERFACE == 2) await common.delay(1000);
			await invCheckReqHandler.processCheck();

			let cancelRes = await invCheckReqHandler.checkMainCancel(processRes.result.pk, false);
			let errorMsg = USEECINTERFACE == 1 ? '只能撤销最近一次盘点处理' : '只能撤销最后一条盘点记录';
			expect(cancelRes).to.includes({
				error: errorMsg
			});
		});
	});

	//checkall 0:部分盘点,1:全部盘点
	//after中会撤销当天所有盘点处理单
	for (let checkall = 1; checkall >= 0; checkall--) {
		let title = ['部分盘点', '全部盘点'];
		describe('180042.盘点处理的单据修改', function () {
			let [saleRes, purRes, outRes] = [{}, {}, {}]; //销售单,采购单,调拨单
			before(async () => {
				await common.setGlobalParam('inv_check_modifylimit', 1); //不允许修改盘点之前出入库流水 盘点后不允许修改

				purRes = await common.editBilling(basiceJsonparam.purchaseJson());
				saleRes = await common.editBilling(basiceJsonparam.salesJson());
				outRes = await common.editBilling(basiceJsonparam.outJson());
				// console.log(`outRes : ${JSON.stringify(outRes)}`);
				await invCheckReqHandler.processCheck({
					checkall
				}); //盘点处理
				await common.delay(5000);
			});
			after(async () => {
				await invCheckReqHandler.checkMainCancelByDate();
			});
			it(`${title[checkall]}.1.修改/作废盘点前采购入库单据`, async function () {
				await testFor180042(checkall, purRes, 'qf-14212-1', 'cs-cancel-pured-bill');
			});
			it(`${title[checkall]}.2.修改/作废盘点后采购入库单据`, async function () {
				await testFor180042(checkall, basiceJsonparam.purchaseJson(), 'qf-14212-1', 'cs-cancel-pured-bill');
			});
			it(`${title[checkall]}.3.修改/作废盘点前销售单`, async function () {
				await testFor180042(checkall, saleRes, 'qf-14211-1', 'cs-cancel-saleout-bill');
			});
			it(`${title[checkall]}.4.修改/作废盘点后销售单`, async function () {
				await testFor180042(checkall, basiceJsonparam.salesJson(), 'qf-14211-1', 'cs-cancel-saleout-bill');
			});
			it.skip(`${title[checkall]}.5.修改/作废盘点前调出单`, async function () {
				await testFor180042(checkall, outRes, 'qf-1862-1', 'cs-cancel-moveout-bill');
			});
			it(`${title[checkall]}.6.修改/作废盘点后调出单`, async function () {
				await testFor180042(checkall, basiceJsonparam.outJson(), 'qf-1862-1', 'cs-cancel-moveout-bill');
			});
		});
		//180025+180026
		describe(`${180025 + checkall}.${title[checkall]}.rankA`, function () {
			let [sfRes, qfRes, processRes, processtime, invInfo] = [{}, {}, {}, '', {}];
			before(async () => {
				//先进行一次盘点处理 部分盘点
				await invCheckReqHandler.processCheck();

				sfRes = await common.editBilling(basiceJsonparam.invCheckJson()); //新增盘点单

				let result = await common.callInterface('ql-1932', format.qlParamsFormat({
					propdresStyleid: sfRes.params.details[0].styleid
				})); //货品管理-当前库存
				result.dataList.map((obj) => {
					invInfo[`${obj.inventoryName}-${obj.colorid}-${obj.sizeid}`] = obj.invnum; //库存
				});
			});
			after(async () => {
				await invCheckReqHandler.checkMainCancelByDate();
			});
			it(`1.${title[checkall]}`, async function () {
				processRes = await invCheckReqHandler.processCheck({
					checkall
				}); //盘点处理
				processtime = processRes.optime;
			});
			it('2.按批次查', async function () {
				qfRes = await invCheckReqHandler.invCheckQueryBilling(sfRes.result.pk); //盘点单据详情
				let param = format.qlParamsFormat({
					billno_gte: sfRes.result.orderno,
					billno_lte: sfRes.result.orderno,
					invid: sfRes.params.invid,
					flag: 1,
					optime_gte: common.getCurrentDate(),
					optime_lte: common.getCurrentDate(),
					// billid billid
					id1: sfRes.result.orderno,
					id2: sfRes.result.orderno,
					optime1: common.getCurrentDate(),
					optime2: common.getCurrentDate(),
				}, false);
				let qlRes = await common.callInterface('ql-1923', param); //盘点管理-按批次查
				let exp = invCheckReqHandler.getInvCheckMainSearchListExp(sfRes, qfRes);
				exp.flag = 1; //盘点单已处理
				exp.processtime = processtime; //处理时间
				common.isApproximatelyEqualAssert(exp, qlRes.dataList[0], ['processtime', 'optime']);
			});
			it('180029.处理记录-查询', async function () {
				let param = format.qlParamsFormat({
					id1: processRes.result.billno,
					id2: processRes.result.billno,
					invid: LOGINDATA.invid,
					invalidflag: 0,
				}, true);
				let qlRes = await common.callInterface('ql-1928', param);
				expect(qlRes.count).to.equal('1'); //查询结果唯一
				let exp = {
					id: processRes.result.pk,
					optime: processtime,
					invname: LOGINDATA.depname,
					invalidflag: 0,
					remark: title[checkall],
					totalnum: sfRes.params.totalnum,
					billno: processRes.result.billno,
					prodate: common.getCurrentDate(),
					opname: LOGINDATA.name,
				};
				common.isApproximatelyEqualAssert(exp, qlRes.dataList[0], ['optime']);
			});
			it('180100.处理记录-详细界面.rankA', async function () {
				let qlRes = await common.callInterface('ql-19251', {
					billid: processRes.result.pk
				}); //处理记录-详细界面
				common.isApproximatelyEqualAssert({
					id: sfRes.result.pk,
					optime: qfRes.result.optime,
					flag: USEECINTERFACE == 1 ? 1 : 9, //一代1代表已处理，二代9代表已处理
					processtime: processtime,
					staffName: USEECINTERFACE == 1 ? qfRes.result.opstaffName : qfRes.result.show_deliver, //show_deliver
					rem: '',
					invName: qfRes.result.show_invid,
					totalnum: qfRes.result.totalnum,
					billno: sfRes.result.orderno,
					prodate: qfRes.result.prodate, //接口没有返回prodate 已让后台加
					opstaffName: USEECINTERFACE == 1 ? qfRes.result.opstaffName : qfRes.result.show_deliver,
				}, qlRes.dataList[0], ['processtime', 'optime']);
			});
			it('4.库存检查', async function () {
				let invInfo2 = {};
				let result = await common.callInterface('ql-1932', format.qlParamsFormat({
					propdresStyleid: sfRes.params.details[0].styleid
				}));
				result.dataList.forEach((obj) => invInfo2[`${obj.inventoryName}-${obj.colorid}-${obj.sizeid}`] = obj.invnum);
				for (let key in invInfo) {
					if (key.includes('中洲店')) invInfo[key] = '0'; //同款号,未盘点到的颜色尺码数量清零
				};
				sfRes.params.details.forEach((obj) => invInfo[`中洲店-${obj.colorid}-${obj.sizeid}`] = obj.num);
				common.isApproximatelyEqualAssert(invInfo, invInfo2);
			});
			it(`5.再次${title[checkall]}`, async function () {
				let checkResult = await invCheckReqHandler.processCheck({
					checkall
				}, false);
				expect(checkResult.result).to.includes({
					error: USEECINTERFACE == 1 ? '本仓库(店铺)没有新录入的盘点流水，请核对' : '本仓库(店铺)没有新录入的盘点单，请核对',
				});
			});
		});
	};

});

async function testFor180042(checkall, sfRes, qfIFC, cancelIFC) {
	let errorMsgs = ['盘点之前的流水不允许修改', '全盘之前出入库流水不允许修改'];
	let isEdit = sfRes.params ? true : false;
	if (!isEdit) sfRes = await common.editBilling(sfRes);

	let param = {
		'action': 'edit', //add返回的是空单据的数据
		'pk': sfRes.result.pk,
	};
	if (USEECINTERFACE == '2') param.isPend = 0; //qfIFC,cancelIFC 是否挂单 否
	let qfResult = await common.callInterface(qfIFC, param); //获取单据信息
	qfResult.interfaceid = sfRes.params.interfaceid;
	qfResult.action = 'edit';
	qfResult.details[0].num = common.add(qfResult.details[0].num, 2); //修改单据货品数量
	sfRes = await common.editBilling(qfResult, false);
	if (isEdit) {
		expect(sfRes.result, `${errorMsgs[checkall]},修改单据${sfRes.params.interfaceid}成功:\n${JSON.stringify(sfRes.result)}`).to.have.property('error');
		expect(sfRes.result.error, `修改盘点之前的单据错误:${JSON.stringify(sfRes)}`).to.include(errorMsgs[checkall]);
	} else {
		expect(sfRes.result, `开单失败:${JSON.stringify(sfRes.result)}`).not.to.have.property('error');
	};

	let cancelResult = await common.callInterface(cancelIFC, param);
	isEdit ? expect(cancelResult.error, '作废盘点之前的单据错误').to.include(errorMsgs[checkall]) :
		expect(cancelResult, `作废单据失败,reason:${JSON.stringify(cancelResult)}`).to.includes({
			'val': 'ok',
		});
};
