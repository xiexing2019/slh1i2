'use strict';
const basiceJsonparam = require('../../help/basiceJsonparam');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const invCheckReqHandler = require('../../help/invCheckHelp/invCheckHelp');
const shopInOutReqHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler');
// const salesRequestHandler = require('../../../help/salesHelp/salesRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
// 前提 门店不存在未处理的盘点单和盘点计划
// 全盘处理要求本门店没有未调入的调拨单
describe('盘点管理-盘点计划', function () {
	this.timeout(30000);
	let styleInfo, goodRes;

	before(async () => {
		await common.loginDo({
			logid: '200',
		});
		BASICDATA = await getBasicData.getBasicID();

		await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理盘点计划单
		await shopInOutReqHandler.shopInAllOnroadBilling(LOGINDATA.invid); //批量调入登陆门店的所有未调入单
		await invCheckReqHandler.processCheck({}, false); //盘点处理 若没有则跳过

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);

		//新增货品 无品牌,类别,厂商 当做不属于组合盘点计划的款号
		goodRes = await basicReqHandler.editStyle({
			jsonparam: basiceJsonparam.addGoodJson(),
		});
		// console.log(`goodRes = ${JSON.stringify(goodRes)}`);
	});

	after(async () => {
		await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理盘点计划单
		await invCheckReqHandler.checkMainCancelByDate(); //撤销盘点处理
	});

	//新增盘点计划 180061~180078
	//按组合的用例编号为180090,180108,180110,180111,180112,
	//typeid 1:按类别 2:按品牌 3:按厂商 4:按组合
	//describe需要按顺序执行
	const typeName = ['', '按类别', '按品牌', '按厂商', '按组合'];
	for (let typeid = 1; typeid < 5; typeid++) {
		describe(`新增盘点计划-${typeName[typeid]}.rankA--mainLine`, function () {
			//当前库存参数,起始库存信息,盘点计划参数,盘点计划期望值,新增盘点单,盘点处理结果
			let [invStockParam, startInvStock, checkPlanParam, checkPlanExp, invCheckRes, processRes] = [{}, {}, {}, {}, {}, {}];
			before(`${180055 + typeid * 6}.新增盘点计划`, async () => {
				await invCheckReqHandler.processCheck({}, false); //盘点处理 若没有则跳过
				checkPlanParam.typeid = typeid;
				checkPlanExp = {
					inventoryName: LOGINDATA.depname,
					opstaffName: LOGINDATA.name,
					flag: 0, //是否处理
					typeid: typeName[typeid],
					year: '',
					invMainBillno: '',
					brandids: '',
					styleclassids: '',
					providerids: '',
					seasons: '',
				};
				invStockParam = format.qlParamsFormat({
					invid: LOGINDATA.invid,
					// search_list: 0,
				}, false);
				if (typeid == 1 || typeid == 4) {
					invStockParam.propdresStyleClassid = styleInfo.classid;
					checkPlanParam.styleclassids = `${styleInfo.classid},`;
					checkPlanExp.styleclassids = styleInfo.show_classid;
				};
				if (typeid == 2 || typeid == 4) {
					invStockParam.propdresStyleBrandid = styleInfo.brandid;
					checkPlanParam.brandids = `${styleInfo.brandid},`;
					checkPlanExp.brandids = styleInfo.show_brandid;
				};
				if (typeid == 3 || typeid == 4) {
					invStockParam.propdresStyleDwid = styleInfo.dwid;
					checkPlanParam.providerids = `${styleInfo.dwid},`;
					checkPlanExp.providerids = styleInfo.show_dwid;
				};
				startInvStock = await common.callInterface('ql-1932', invStockParam); //货品管理-当前库存
				let sfRes = await invCheckReqHandler.addCheckPlan(checkPlanParam); //新增盘点计划
				checkPlanExp.optime = sfRes.optime;
				checkPlanExp.id = sfRes.result.pk;
				// checkPlanExp.billno = sfRes.result.billno;
			});
			after(async () => {
				await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理的盘点计划
				await invCheckReqHandler.checkMainCancelByDate(); //撤销盘点处理
			});
			//新增盘点计划
			it(`${180055 + typeid * 6}.1.查询盘点计划`, async function () {
				let qlRes = await common.callInterface('qd-1927', format.qlParamsFormat({
					invid: LOGINDATA.invid,
					flag: 0, //是否处理
				}, false)); //盘点管理-盘点计划表
				expect(qlRes.dataList.length).to.above(0);
				common.isApproximatelyEqualAssert(checkPlanExp, qlRes.dataList[0], ['processtime', 'optime']);
			});
			it(`${180056 + typeid * 6}.2.门店只存在未处理的盘点计划,再新增一条该门店的盘点计划点保存`, async function () {
				let sfRes = await invCheckReqHandler.addCheckPlan(checkPlanParam, false); //新增盘点计划
				expect(sfRes.result).to.includes({
					error: '盘点计划正在执行中，请等盘点处理完毕后新增计划'
				});
			});
			//根据上面的盘点计划新增盘点单
			it(`${180058 + typeid * 6}.新增盘点计划成功后-新增盘点单`, async function () {
				let json = basiceJsonparam.invCheckJson();
				json.details[0].styleid = goodRes.pk;
				let sfRes = await common.editBilling(json, false); //输入不符合条件的款号A和符合条件的款号B,点保存
				expect(sfRes.result).to.includes({
					error: `款号:${goodRes.params.code},不属于本次盘点计划`
				});

				sfRes.params.details = [sfRes.params.details[1]];
				invCheckRes = await common.editBilling(sfRes.params); //删除款号A,新增盘点单保存成功

				let qfRes = await invCheckReqHandler.invCheckQueryBilling(invCheckRes.result.pk); //获取盘点单详情
				qfRes.result.action = 'edit';
				qfRes.result.interfaceid = invCheckRes.params.interfaceid;
				qfRes.result.details.push(json.details[0]);
				sfRes = await common.editBilling(qfRes.result, false); //选择刚新增的盘点单进入,输入不符合条件的款号A,点保存
				expect(sfRes.result).to.includes({
					error: `款号:${goodRes.params.code},不属于本次盘点计划`
				});
			});
			it(`${180056 + typeid * 6}.4.门店存在未处理的盘点计划和盘点单,再新增一条该门店的盘点计划点保存`, async function () {
				let sfRes = await invCheckReqHandler.addCheckPlan(checkPlanParam, false); //新增盘点计划
				expect(sfRes.result).to.includes({
					error: '盘点计划正在执行中，请等盘点处理完毕后新增计划',
				});
			});
			//需要保证常青店没有未处理的盘点单
			it.skip(`${180056 + typeid * 6}.5.门店存在未处理的盘点计划和盘点单,再新增一条其他门店的盘点计划点保存`, async function () {
				let qlRes = await common.callInterface('ql-1923', format.qlParamsFormat({
					optime1: common.getDateString([0, -1, 0]),
					invid: BASICDATA.shopidCqd,
				}, false)); //盘点管理-按批次查
				for (let i = 0, length = qlRes.dataList.length; i < length; i++) {
					if (qlRes.dataList[i].flag == 0) await invCheckReqHandler.deleteCheckBill(qlRes.dataList[i].id); //删除未处理盘点单
				};

				let param = _.cloneDeep(checkPlanParam);
				param.invid = BASICDATA.shopidCqd;
				await invCheckReqHandler.addCheckPlan(param); //新增盘点计划
			});
			//盘点处理 部分处理验证错误提示
			it(`${180059 + typeid * 6}.1.新增盘点单成功后-进行部分盘点`, async function () {
				await common.loginDo();
				let json = basiceJsonparam.outJson();
				json.shopid = BASICDATA.shopidCqd; //调出门店
				json.invidref = BASICDATA.shopidZzd; //调入门店
				let outRes = await common.editBilling(json); //新增调出单

				await common.loginDo({
					logid: '200',
				});
				let checkResult = await invCheckReqHandler.processCheck({}, false); //门店还存在盘点调拨单,进行部分处理
				let errorMsg = USEECINTERFACE == 1 ? '本仓库(店铺)还有调拨单没有接收入库，请全部接收之后再做盘点处理' : '存在未执行的盘点计划，不能做部分盘点';
				expect(checkResult.result).to.includes({
					error: errorMsg,
				});

				let qfRes = await shopInOutReqHandler.shopInQueryBilling({
					pk: outRes.result.pk, //一代
					billno: outRes.result.billno, //二代根据批次号获取调入单pk
				}); //获取调出单信息
				let rs = await shopInOutReqHandler.shopIn(qfRes.result); //调入调拨单
				// console.log(`rs=${JSON.stringify(rs)}`);
				//做了调入单，当前库存中的相应字段需要发生改变
				for (let i = 0; i < rs.params.details.length; i++) {
					for (let j = 0; j < startInvStock.dataList.length; j++) {
						if (rs.params.details[i].colorid == startInvStock.dataList[j].colorid && rs.params.details[i].sizeid == startInvStock.dataList[j].sizeid && rs.params.details[i].styleid == startInvStock.dataList[j].styleid) {
							startInvStock.dataList[j].invnum = common.add(startInvStock.dataList[j].invnum, rs.params.details[i].num);
							startInvStock.dataList[j].total = common.mul(startInvStock.dataList[j].invnum, startInvStock.dataList[j].price);
							startInvStock.dataList[j].availStockNum = common.add(startInvStock.dataList[j].availStockNum, rs.params.details[i].num);
							startInvStock.sumrow.invnum = common.add(startInvStock.sumrow.invnum, rs.params.details[i].num);
							startInvStock.sumrow.total = common.add(startInvStock.sumrow.total, startInvStock.dataList[j].total);
							startInvStock.sumrow.availStockNum = common.add(startInvStock.sumrow.availStockNum, rs.params.details[i].num);
							break;
						};
					};
				};
				checkResult = await invCheckReqHandler.processCheck({}, false); //门店无调拨单,进行部分处理
				expect(checkResult.result).to.includes({
					error: '存在未执行的盘点计划，不能做部分盘点',
				});
			});
			//盘点处理 全盘处理 + 盘点管理-按批次查/盘点计划表 + 货品管理-当前库存
			it(`${180059 + typeid * 6}.2.新增盘点单成功后-进行盘点处理`, async function () {
				processRes = await invCheckReqHandler.processCheck({
					checkall: 1
				}); //全盘处理
				//let optime = common.getCurrentTime();
				let qfRes = await invCheckReqHandler.invCheckQueryBilling(invCheckRes.result.pk); //获取盘点单信息

				//盘点管理-按批次查
				let qlRes = await common.callInterface('ql-1923', format.qlParamsFormat({
					invid: qfRes.result.invid,
					flag: 1,
					optime1: common.getCurrentDate(),
					optime2: common.getCurrentDate(),
					optime_gte: common.getCurrentDate(), //处理时间从
					optime_lte: common.getCurrentDate(), //处理时间到
					// processtime_gte: common.getCurrentDate(), //处理时间从
					// processtime_lte: common.getCurrentDate(), //处理时间到
				}, false));
				let exp = invCheckReqHandler.getInvCheckMainSearchListExp(invCheckRes, qfRes);
				exp.flag = 1;
				exp.processtime = processRes.optime;
				common.isApproximatelyEqualAssert(exp, qlRes.dataList[0], ['processtime', 'optime']);

				//盘点管理-盘点计划表
				let qlRes2 = await common.callInterface('qd-1927', format.qlParamsFormat({
					invid: LOGINDATA.invid,
					flag: 1, //是否处理
				}, false));
				expect(qlRes2.dataList.length).to.above(0);
				checkPlanExp.flag = 1;
				checkPlanExp.invMainBillno = processRes.result.billno;
				common.isApproximatelyEqualAssert(checkPlanExp, qlRes2.dataList[0]);

				//货品管理-当前库存
				let qlRes3 = await common.callInterface('ql-1932', invStockParam);
				expect(qfRes.result.totalnum, `当前库存汇总值错误`).to.equal(qlRes3.sumrow.invnum);
				qlRes3.dataList.map((data) => {
					if (data.styleid == qfRes.result.details[0].styleid && data.colorid == qfRes.result.details[0].colorid && data.sizeid == qfRes.result.details[0].sizeid) {
						expect(Number(data.invnum), '盘点款号数量错误').to.equal(Number(qfRes.result.details[0].num));
					} else {
						expect(Number(data.invnum), '未盘点款号数量错误').to.equal(0);
					};
				});
			});
			//
			it('180083.删除已处理的计划表', async function () {
				let result = await invCheckReqHandler.deleteCheckPlan(checkPlanExp.id, false);
				let errorMsg = USEECINTERFACE == 1 ? '不能删除已执行的盘点计划' : '该盘点计划已执行';
				expect(result).to.includes({
					error: errorMsg
				});
			});
			it(`${180060 + typeid * 6}.盘点处理完毕后-进行盘点撤销`, async function () {
				let sfRes = await invCheckReqHandler.addCheckPlan(checkPlanParam); //新增盘点计划
				let cancelResult = await invCheckReqHandler.checkMainCancel(processRes.result.pk, false);
				expect(cancelResult).to.includes({
					error: '盘点计划正在执行中，盘点处理无法撤销'
				});

				await invCheckReqHandler.deleteCheckPlan(sfRes.result.pk); //删除盘点计划
				await invCheckReqHandler.checkMainCancel(processRes.result.pk); //撤销盘点处理单
				let qfRes = await invCheckReqHandler.invCheckQueryBilling(invCheckRes.result.pk); //获取盘点单信息

				//盘点管理-按批次查
				let qlRes = await common.callInterface('ql-1923', format.qlParamsFormat({
					billno_gte: invCheckRes.result.billno || invCheckRes.result.orderno,
					billno_lte: invCheckRes.result.billno || invCheckRes.result.orderno,
					invid: LOGINDATA.invid,
					flag: 0,
				}, false));
				let exp = invCheckReqHandler.getInvCheckMainSearchListExp(invCheckRes, qfRes);
				common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);

				//盘点管理-盘点计划表
				let qlRes2 = await common.callInterface('qd-1927', format.qlParamsFormat({
					invid: LOGINDATA.invid,
					flag: 0, //是否处理
				}, false));
				expect(qlRes2.dataList.length).to.above(0);
				checkPlanExp.flag = 0;
				//错误调试日志
				if (checkPlanExp.invMainBillno == '') {
					console.log(`全盘处理结果,invMainBillno输出为空:\n${JSON.stringify(processRes)}`);
					console.log(`\ncheckPlanExp=${JSON.stringify(checkPlanExp)}`);
					console.log(`\nactual=${JSON.stringify(qlRes2.dataList[0])}`);
				};
				common.isApproximatelyEqualAssert(checkPlanExp, qlRes2.dataList[0]);

				//货品管理-当前库存
				let qlRes3 = await common.callInterface('ql-1932', invStockParam);
				common.isApproximatelyEqualAssert(startInvStock.dataList, qlRes3.dataList);
			});
			//撤销后 门店只存在未处理的盘点单
			it(`${180056 + typeid * 6}.3.门店只存在未处理的盘点单，再新增一条该门店A的盘点计划点保存`, async function () {
				let sfRes = await invCheckReqHandler.addCheckPlan(checkPlanParam, false); //新增盘点计划
				expect(sfRes.result).to.includes({
					error: '盘点计划正在执行中，请等盘点处理完毕后新增计划'
				});
			});
			//
			if (typeid == 4) {
				it('180107.4.选择盘点类型,不选择门店,点保存', async function () {
					let res = await invCheckReqHandler.addCheckPlan(Object.assign(checkPlanParam, {
						invid: undefined, //不输入门店
						typeid
					}), false);
					expect(res.result).to.includes({
						error: 'invid参数不正确!'
					});
				});
			};
		});

		//checkall 0:部分盘点 1;全部盘点
		for (let checkall = 0; checkall < 2; checkall++) {
			describe(`180113.停用组合类型-${typeName[typeid]}`, function () {
				let checkPlanParam = {};
				before(async () => {
					await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理的盘点计划
					await invCheckReqHandler.processCheck({}, false); //盘点处理 若没有则跳过
				});
				after(async () => {
					await test180113Help(checkPlanParam, 'start'); // 启用品牌/类别/厂商
					await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理的盘点计划
					await invCheckReqHandler.checkMainCancelByDate(); //撤销盘点处理
				});
				it(`${checkall == 0 ? '部分盘点' : '全部盘点'}`, async function () {
					checkPlanParam.typeid = typeid;
					//类别
					if (typeid == 1 || typeid == 4) {
						checkPlanParam.styleclassids = `${styleInfo.classid},`;
					};
					//品牌
					if (typeid == 2 || typeid == 4) {
						checkPlanParam.brandids = `${styleInfo.brandid},`;
					};
					//厂商
					if (typeid == 3 || typeid == 4) {
						checkPlanParam.providerids = `${styleInfo.dwid},`;
					};

					let checkPlanRes = await invCheckReqHandler.addCheckPlan(checkPlanParam); //新增盘点计划
					let invCheckRes = await common.editBilling(basiceJsonparam.invCheckJson()); //新增盘点单
					await test180113Help(checkPlanParam, 'stop'); // 停用品牌/类别/厂商

					let processRes = await invCheckReqHandler.processCheck({
						checkall
					}, false);
					if (checkall == 0) {
						expect(processRes.result).to.includes({
							error: '存在未执行的盘点计划，不能做部分盘点',
						});
					} else {
						expect(processRes.result, `盘点处理失败:\n${JSON.stringify(processRes.result)}`).not.to.have.property('error');
					};
				});
			});

		};

	};

	describe('180079.按品牌/按类别/按厂商三个不能同时新增', function () {
		let errorMsg;
		before(async () => {
			await invCheckReqHandler.addCheckPlan({
				typeid: 2,
				brandids: styleInfo.brandid
			}, false); //若没有盘点计划,则新增

			errorMsg = USEECINTERFACE == 1 ? '门店/仓库中洲店有未处理的盘点单，请处理完毕后新增计划' : '盘点计划正在执行中，请等盘点处理完毕后新增计划';
		});
		after(async () => {
			await invCheckReqHandler.deleteCheckPlanOfUndisposed(); //删除未处理的盘点计划
		});
		it('1.新增类别盘点计划,点保存', async function () {
			let res = await invCheckReqHandler.addCheckPlan({
				typeid: 1,
				styleclassids: styleInfo.classid
			}, false);
			expect(res.result).to.includes({
				error: errorMsg
			});
		});
		it('2.新增厂商盘点计划,点保存', async function () {
			let res = await invCheckReqHandler.addCheckPlan({
				typeid: 3,
				providerids: styleInfo.dwid
			}, false);
			expect(res.result).to.includes({
				error: errorMsg
			});
		});
	});

	describe('180081.总经理能新增全部门店的盘点计划', function () {
		before(async () => {
			await invCheckReqHandler.deleteCheckPlanOfUndisposed();
		});
		after(async () => {
			await invCheckReqHandler.deleteCheckPlanOfUndisposed();
		});
		it('2.新增其他门店盘点计划+查询', async function () {
			let exp = {
				inventoryName: '常青店',
				opstaffName: LOGINDATA.name,
				flag: 0, //是否处理
				typeid: '按品牌',
				year: '',
				invMainBillno: '',
				brandids: styleInfo.show_brandid,
				styleclassids: '',
				providerids: '',
				seasons: '',
			};
			let sfRes = await invCheckReqHandler.addCheckPlan({
				invid: BASICDATA.shopidCqd,
				typeid: 2,
				brandids: styleInfo.brandid
			}); //新增盘点计划-按品牌
			exp.optime = common.getCurrentTime();
			exp.id = sfRes.result.pk;
			exp.billno = sfRes.result.billno;;

			let qlRes = await common.callInterface('qd-1927', format.qlParamsFormat({
				invid: BASICDATA.shopidCqd,
				flag: 0, //是否处理
			}, false)); //盘点管理-盘点计划表
			expect(qlRes.dataList.length).to.above(0);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
	});

});

// 类别/品牌/厂商-停用/启用
async function test180113Help(checkPlanParam, action) {
	let res = {},
		msg = action == 'stop' ? '停用' : '启用';
	//	类别,品牌,厂商
	let interfaceidArr = action == 'stop' ? ['cs-15091', 'cs-disable-dict', 'cs-2241-2'] : ['cs-15092', 'cs-enable-dict', 'cs-2241-3'];
	if (checkPlanParam.styleclassids) {
		res = await common.callInterface(interfaceidArr[0], {
			pk: checkPlanParam.styleclassids.split(',')[0],
		});
		expect(res, `${msg}类别失败:\n${JSON.stringify(res)}`).to.includes({
			val: 'ok'
		});
	};
	if (checkPlanParam.brandids) {
		res = await common.callInterface(interfaceidArr[1], {
			pk: checkPlanParam.brandids.split(',')[0],
		});
		expect(res, `${msg}品牌失败:\n${JSON.stringify(res)}`).to.includes({
			val: 'ok'
		});
	};
	if (checkPlanParam.providerids) {
		res = await common.callInterface(interfaceidArr[2], {
			pk: checkPlanParam.providerids.split(',')[0],
		});
		expect(res, `${msg}厂商失败:\n${JSON.stringify(res)}`).to.includes({
			val: 'ok'
		});
	};

};
