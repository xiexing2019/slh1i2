'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
/**
 * 开采购入库单后，当前库存、库存分布、款号库存以及收支流水的变化
 * @description 二代环境不稳定 当前库存ql-1932 需要1S延迟才能刷新数据
 */

describe('采购入库-slh2--mainLine', function () {

	this.timeout(30000);

	let goodRes, lastInvData = {};
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		// 获取'登山服'的id
		let classidInfo = await common.callInterface('ql-1509', {
			'name': '登山服'
		});
		expect(classidInfo, `获取登山服信息失败:${JSON.stringify(classidInfo)}`).to.includes({
			'count': '1'
		});

		let goodJson = basiceJson.addGoodJson();
		goodJson.classid = classidInfo.dataList[0].id;
		goodJson.season = BASICDATA.seasonidSpring;
		goodRes = await basicReqHandler.editStyle({
			jsonparam: goodJson,
		}) //新增货品 库存为0
	});
	describe('120019,新增入库+付款.rankA', function () {
		let invnumLayout1, purResult, purQfRes, currInvnum, matInvnum, invnumLayout2;
		before(async () => {
			await common.setGlobalParam('invsumprice', 0); //保证库存核算按进货价核算
			// 开入库单之前先查询库存分布
			invnumLayout1 = await common.callInterface('ql-19902', format.qlParamsFormat({
				'classid': goodRes.params.classid,
				'season': goodRes.params.season,
				'styleflag': '0'
			}));
			expect(invnumLayout1, `查询库存分布出错:${JSON.stringify(invnumLayout1)}`).to.not.have.property('error');

			// 新增入库(付款)
			let purJson = basiceJson.purchaseJson();
			for (let i = 0; i < purJson.details.length; i++) {
				purJson.details[i].styleid = goodRes.pk;
			}
			purResult = await common.editBilling(purJson);
			purQfRes = await purReqHandler.purQueryBilling(purResult.result.pk);
		});
		after(async () => {
			lastInvData.currInvnum = currInvnum;
			lastInvData.matInvnum = matInvnum;
			lastInvData.invnumLayout = invnumLayout2;
		});

		it('验证按批次查', async function () {
			// 验证按批次查
			let batchList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': purResult.result.billno,
				'id2': purResult.result.billno,
				'dwid': purResult.params.dwid,
				'shopid': purResult.params.shopid
			}));
			common.isApproximatelyEqualAssert(purResult.params, batchList.dataList[0]);
		});

		it('验证当前库存', async function () {
			if (USEECINTERFACE == 2) await common.delay(200); //二代需要加延迟
			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat({
				'invid': LOGINDATA.invid,
				'propdresColorid': purResult.params.details[0].colorid,
				'propdresSizeid': purResult.params.details[0].sizeid,
				'propdresStyleid': goodRes.pk,
			})); //货品管理-当前库存
			common.isApproximatelyEqualAssert({
				'total': purResult.params.details[0].total,
				'invnum': purResult.params.details[0].num,
				'colorid': purResult.params.details[0].colorid,
				'sizeid': purResult.params.details[0].sizeid,
				'price': purResult.params.details[0].price,
				'styleid': purResult.params.details[0].styleid,
				'propdresStyleName': goodRes.params.name,
				'propdresStyleCode': goodRes.params.code,
			}, currInvnum.dataList[0]);
		});

		it('验证款号库存', async function () {
			this.retries(2);
			if (USEECINTERFACE == 2) await common.delay(800);
			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'styleid': goodRes.pk,
			}));
			common.isApproximatelyEqualAssert({
				'invnum': purResult.params.totalnum,
				'style_code': goodRes.params.code,
				'invid': purResult.params.shopid,
				'styleid': purResult.params.details[0].styleid,
				'toltalEntry': purResult.params.totalnum, //累计进
				'style_marketdate': common.getCurrentDate('YY-MM-DD'),
			}, matInvnum.dataList[0], [], JSON.stringify(matInvnum.dataList));
		});

		it('验证库存分布', async function () {
			invnumLayout2 = await common.callInterface('ql-19902', format.qlParamsFormat({
				'classid': goodRes.params.classid,
				'season': goodRes.params.season,
				'styleflag': '0'
			}));
			let index = invnumLayout2.dynamicTitle.indexOf(LOGINDATA.depname);
			common.isApproximatelyEqualAssert({
				'invmoney': common.add(invnumLayout1.dataList[0].invmoney, purResult.params.totalmoney),
				'invnum': common.add(invnumLayout1.dataList[0].invnum, purResult.params.totalnum),
			}, invnumLayout2.dataList[0]);
			let expectInvnum = common.add(invnumLayout1.dataList[0].dynamicData[index], purResult.params.totalnum);
			expect(Number(invnumLayout2.dataList[0].dynamicData[index]), `库存分布有误2`).to.be.equal(expectInvnum);
		});

		// 验证统计分析-->收支流水 (现金、刷卡、汇款)
		it.skip('验证收支流水', async function () {
			this.retries(2); //不同表之间更新时间
			await common.delay(1000); //这里的延迟不能删掉，入库操作跟收支流水之间有一定的时间间隔
			let qlRes = await common.callInterface('ql-1352', format.qlParamsFormat({
				accountInvid: LOGINDATA.invid,
			}, true));

			purQfRes.result.interfaceid = purQfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(purQfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
	});

	// 依赖120019,新增入库+付款
	describe('120020,退货+退款.rankA', function () {
		let invnumLayout1, purResult, purQfRes, currInvnum, matInvnum, invnumLayout2;
		before(async () => {
			await common.setGlobalParam('sales_backmat_alert', 0); //关闭 “逐条进行退货数大于拿货数验证”

			// 开入库单之前先查询库存分布
			invnumLayout1 = lastInvData.invnumLayout;

			// 采购退货+退钱
			let purJson = basiceJson.purchaseJson();
			let totalmoney = 0;
			for (let i = 0; i < purJson.details.length; i++) {
				purJson.details[i].styleid = goodRes.pk;
				purJson.details[i].num = -2;
				purJson.details[i].price = goodRes.params.purprice;
				purJson.details[i].total = Number(purJson.details[i].num) * Number(purJson.details[i].price);
				totalmoney += Number(purJson.details[i].total);
			}
			purJson.totalmoney = totalmoney;
			purJson.card = -10;
			purJson.cash = -20;
			purJson.remit = Number(purJson.totalmoney) - Number(purJson.card) - Number(purJson.cash);
			purResult = await common.editBilling(purJson);
			purQfRes = await purReqHandler.purQueryBilling(purResult.result.pk);
		});
		after(async () => {
			lastInvData.currInvnum = currInvnum;
			lastInvData.matInvnum = matInvnum;
			lastInvData.invnumLayout = invnumLayout2;
		});
		it('1.验证按批次查', async function () {
			// 验证按批次查
			let batchList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': purResult.result.billno,
				'id2': purResult.result.billno,
				'dwid': purResult.params.dwid,
				'shopid': purResult.params.shopid
			}));
			common.isApproximatelyEqualAssert(purResult.params, batchList.dataList[0]);
		});

		it.skip('2.验证当前库存', async function () {
			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'propdresColorid': purResult.params.details[0].colorid,
				'propdresSizeid': purResult.params.details[0].sizeid,
				'propdresStyleid': purResult.params.details[0].styleid
			}));
			common.isApproximatelyEqualAssert({
				'total': common.add(lastInvData.currInvnum.dataList[0].total, purResult.params.details[0].total),
				'invnum': common.add(lastInvData.currInvnum.dataList[0].invnum, purResult.params.details[0].num),
				'colorid': purResult.params.details[0].colorid,
				'sizeid': purResult.params.details[0].sizeid,
				'price': purResult.params.details[0].price.toString(),
				'styleid': purResult.params.details[0].styleid,
				'propdresStyleName': goodRes.params.name,
				'propdresStyleCode': goodRes.params.code,
			}, currInvnum.dataList[0])

		});

		it('3.验证款号库存', async function () {
			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'styleid': goodRes.pk,
			}));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(lastInvData.matInvnum.dataList[0].invnum, purResult.params.totalnum),
				'style_code': goodRes.params.code,
				'invid': purResult.params.shopid,
				'toltalEntry': common.add(lastInvData.matInvnum.dataList[0].toltalEntry, purResult.params.totalnum),
				'styleid': purResult.params.details[0].styleid,
				'style_marketdate': common.getCurrentDate('YY-MM-DD'),
			}, matInvnum.dataList[0])
		});

		it('4.验证库存分布', async function () {
			invnumLayout2 = await common.callInterface('ql-19902', format.qlParamsFormat({
				'classid': goodRes.params.classid,
				'season': goodRes.params.season,
				'styleflag': '0'
			}));
			let index = invnumLayout2.dynamicTitle.indexOf(LOGINDATA.depname);
			common.isApproximatelyEqualAssert({
				'invmoney': common.add(invnumLayout1.dataList[0].invmoney, purResult.params.totalmoney),
				'invnum': common.add(invnumLayout1.dataList[0].invnum, purResult.params.totalnum),
			}, invnumLayout2.dataList[0]);

			let expectInvnum = common.add(invnumLayout1.dataList[0].dynamicData[index], purResult.params.totalnum);
			expect(Number(invnumLayout2.dataList[0].dynamicData[index]), `库存分布有误2`).to.be.equal(expectInvnum);
		});

		// 验证统计分析-->收支流水 (现金、刷卡、汇款)
		it.skip('5.验证收支流水', async function () {
			await common.delay(2000); //这里的延迟不能删掉，入库操作跟收支流水之间有一定的时间间隔
			let qlRes = await common.callInterface('ql-1352', format.qlParamsFormat({
				accountInvid: LOGINDATA.invid,
			}, true));

			purQfRes.result.interfaceid = purQfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(purQfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
	});

	// 依赖120020,退货+退款
	describe('120021,退货+不退款.rankA', function () {
		let invnumLayout1, purResult, lastShopCredit, lastCredit, lastMoneyFlowing, currInvnum, matInvnum, invnumLayout2;
		before(async () => {
			// 拼接purJson
			let purJson = basiceJson.purchaseJson();
			for (let i = 0; i < purJson.details.length; i++) {
				purJson.details[i].styleid = goodRes.pk;
				purJson.details[i].num = -2;
			}
			[purJson.cash, purJson.card, purJson.remit] = [0, 0, 0];
			purJson = format.jsonparamFormat(purJson);

			// 开入库单之前先查询库存分布
			invnumLayout1 = lastInvData.invnumLayout;

			//开入库单之前先查询厂商账款
			lastShopCredit = await common.callInterface('ql-15003', {
				'dwxxid': purJson.dwid,
				'invid': purJson.shopid
			});
			expect(lastShopCredit, `按门店查询厂商账款出错:${JSON.stringify(lastShopCredit)}`).to.not.have.property('error');
			lastCredit = await common.callInterface('ql-15007', {
				'dwid': purJson.dwid
			});
			expect(lastCredit, `厂商总账查询出错`).to.not.have.property('error');

			// 开入库单之前先查询收支流水
			lastMoneyFlowing = await common.callInterface('ql-1352', format.qlParamsFormat({}, true));
			expect(lastMoneyFlowing, `收支流水查询出错`).to.not.have.property('error');

			// 采购退货+不退款
			purResult = await common.editBilling(purJson);
		});
		after(async () => {
			lastInvData.currInvnum = currInvnum;
			lastInvData.matInvnum = matInvnum;
			lastInvData.invnumLayout = invnumLayout2;
		});
		it('验证按批次查', async function () {
			// 验证按批次查
			let batchList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': purResult.result.billno,
				'id2': purResult.result.billno,
				'dwid': purResult.params.dwid,
				'shopid': purResult.params.shopid
			}));
			common.isApproximatelyEqualAssert(purResult.params, batchList.dataList[0]);
		});

		it.skip('验证当前库存', async function () {
			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'propdresColorid': purResult.params.details[0].colorid,
				'propdresSizeid': purResult.params.details[0].sizeid,
				'propdresStyleid': purResult.params.details[0].styleid
			}));
			common.isApproximatelyEqualAssert({
				'total': common.add(lastInvData.currInvnum.dataList[0].total, purResult.params.details[0].total),
				'invnum': common.add(lastInvData.currInvnum.dataList[0].invnum, purResult.params.details[0].num),
				'colorid': purResult.params.details[0].colorid,
				'sizeid': purResult.params.details[0].sizeid,
				'price': purResult.params.details[0].price.toString(),
				'styleid': purResult.params.details[0].styleid,
				'propdresStyleName': goodRes.params.name,
				'propdresStyleCode': goodRes.params.code,
			}, currInvnum.dataList[0]);
		});

		it('验证款号库存', async function () {
			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'styleid': goodRes.pk,
			}));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(lastInvData.matInvnum.dataList[0].invnum, purResult.params.totalnum),
				'style_code': goodRes.params.code,
				'invid': purResult.params.shopid,
				'toltalEntry': common.add(lastInvData.matInvnum.dataList[0].toltalEntry, purResult.params.totalnum),
				'styleid': purResult.params.details[0].styleid,
				'style_marketdate': common.getCurrentDate('YY-MM-DD'),
			}, matInvnum.dataList[0]);
		});

		it('验证库存分布', async function () {
			invnumLayout2 = await common.callInterface('ql-19902', format.qlParamsFormat({
				'classid': goodRes.params.classid,
				'season': goodRes.params.season,
				'styleflag': '0'
			}));
			let index = invnumLayout2.dynamicTitle.indexOf(LOGINDATA.depname);
			common.isApproximatelyEqualAssert({
				'invmoney': common.add(invnumLayout1.dataList[0].invmoney, purResult.params.totalmoney),
				'invnum': common.add(invnumLayout1.dataList[0].invnum, purResult.params.totalnum),
			}, invnumLayout2.dataList[0]);
			let expectInvnum = common.add(invnumLayout1.dataList[0].dynamicData[index], purResult.params.totalnum);
			expect(Number(invnumLayout2.dataList[0].dynamicData[index]), `库存分布有误2`).to.be.equal(expectInvnum);
		});
		// 验证统计分析-->收支流水 (现金、刷卡、汇款)
		it('验证收支流水', async function () {
			await common.delay(1000); //这里的延迟不能删掉，入库操作跟收支流水之间有一定的时间间隔
			let moneyFlowing = await common.callInterface('ql-1352', format.qlParamsFormat({}, true));
			common.isApproximatelyEqualAssert({
				'count': lastMoneyFlowing.count,
			}, moneyFlowing);
		});

		it.skip('验证厂商账款', async function () {
			//开入库单之前先查询厂商账款
			let shopCredit = await common.callInterface('ql-15003', {
				'dwxxid': purResult.params.dwid,
				'invid': purResult.params.shopid
			});
			common.isApproximatelyEqualAssert({
				'balance': common.add(lastShopCredit.dataList[0].balance, purResult.params.balance),
			}, shopCredit.dataList[0]);

			let credit = await common.callInterface('ql-15007', {
				'dwid': purResult.params.dwid
			});
			common.isApproximatelyEqualAssert({
				'balance': common.add(lastCredit.dataList[0].balance, purResult.params.balance)
			}, credit.dataList[0]);
		});
	});

	// 依赖120021,退货+不退款
	describe('120023,新增入库+不付款.rankA', function () {
		let invnumLayout1, purResult, lastShopCredit, lastCredit, lastMoneyFlowing, currInvnum, matInvnum, invnumLayout2;
		before(async () => {
			// 拼接purJson
			let purJson = basiceJson.purchaseJson();
			for (let i = 0; i < purJson.details.length; i++) {
				purJson.details[i].styleid = goodRes.pk;
			}
			purJson.card = 0;
			purJson.cash = 0;
			purJson.remit = 0;
			purJson = format.jsonparamFormat(purJson);

			// 开入库单之前先查询库存分布
			invnumLayout1 = lastInvData.invnumLayout;

			//开入库单之前先查询厂商账款
			lastShopCredit = await common.callInterface('ql-15003', {
				'dwxxid': purJson.dwid,
				'invid': purJson.shopid
			});
			expect(lastShopCredit, `按门店查询厂商账款出错:${JSON.stringify(lastShopCredit)}`).to.not.have.property('error');
			lastCredit = await common.callInterface('ql-15007', {
				'dwid': purJson.dwid
			});
			expect(lastCredit, `厂商总账查询出错`).to.not.have.property('error');
			// 开入库单之前先查询收支流水
			lastMoneyFlowing = await common.callInterface('ql-1352', format.qlParamsFormat({}, true));
			expect(lastMoneyFlowing, `收支流水查询出错`).to.not.have.property('error');

			// 采购退货+不退款
			purResult = await common.editBilling(purJson);
		});
		after(async () => {
			lastInvData.currInvnum = currInvnum;
			lastInvData.matInvnum = matInvnum;
			lastInvData.invnumLayout = invnumLayout2;
		});
		it('验证按批次查', async function () {
			// 验证按批次查
			let batchList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': purResult.result.billno,
				'id2': purResult.result.billno,
				'dwid': purResult.params.dwid,
				'shopid': purResult.params.shopid
			}));
			common.isApproximatelyEqualAssert(purResult.params, batchList.dataList[0]);
		});

		it.skip('验证当前库存', async function () {
			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'propdresColorid': purResult.params.details[0].colorid,
				'propdresSizeid': purResult.params.details[0].sizeid,
				'propdresStyleid': purResult.params.details[0].styleid
			}));
			common.isApproximatelyEqualAssert({
				'total': common.add(lastInvData.currInvnum.dataList[0].total, purResult.params.details[0].total),
				'invnum': common.add(lastInvData.currInvnum.dataList[0].invnum, purResult.params.details[0].num),
				'colorid': purResult.params.details[0].colorid,
				'sizeid': purResult.params.details[0].sizeid,
				'price': purResult.params.details[0].price.toString(),
				'styleid': purResult.params.details[0].styleid,
				'propdresStyleName': goodRes.params.name,
				'propdresStyleCode': goodRes.params.code,
			}, currInvnum.dataList[0]);
		});

		it('验证款号库存', async function () {
			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat({
				'invid': purResult.params.shopid,
				'styleid': goodRes.pk,
			}));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(lastInvData.matInvnum.dataList[0].invnum, purResult.params.totalnum),
				'style_code': goodRes.params.code,
				'invid': purResult.params.shopid,
				'toltalEntry': common.add(lastInvData.matInvnum.dataList[0].toltalEntry, purResult.params.totalnum),
				'styleid': purResult.params.details[0].styleid,
				'style_marketdate': common.getCurrentDate('YY-MM-DD'),
			}, matInvnum.dataList[0]);
		});

		it('验证库存分布', async function () {
			invnumLayout2 = await common.callInterface('ql-19902', format.qlParamsFormat({
				'classid': goodRes.params.classid,
				'season': goodRes.params.season,
				'styleflag': '0'
			}));
			let index = invnumLayout2.dynamicTitle.indexOf(LOGINDATA.depname);
			common.isApproximatelyEqualAssert({
				'invmoney': common.add(invnumLayout1.dataList[0].invmoney, purResult.params.totalmoney),
				'invnum': common.add(invnumLayout1.dataList[0].invnum, purResult.params.totalnum),
			}, invnumLayout2.dataList[0]);

			let expectInvnum = common.add(invnumLayout1.dataList[0].dynamicData[index], purResult.params.totalnum);
			expect(Number(invnumLayout2.dataList[0].dynamicData[index]), `库存分布有误2`).to.be.equal(expectInvnum);
		});

		// 验证统计分析-->收支流水 (现金、刷卡、汇款)
		it('验证收支流水', async function () {
			await common.delay(1000); //这里的延迟不能删掉，入库操作跟收支流水之间有一定的时间间隔
			let moneyFlowing = await common.callInterface('ql-1352', format.qlParamsFormat({}, true));
			common.isApproximatelyEqualAssert({
				'count': lastMoneyFlowing.count,
			}, moneyFlowing);
		});
		it.skip('验证厂商账款', async function () {
			//开入库单之前先查询厂商账款
			let shopCredit = await common.callInterface('ql-15003', {
				'dwxxid': purResult.params.dwid,
				'invid': purResult.params.shopid
			});
			common.isApproximatelyEqualAssert({
				'balance': common.add(lastShopCredit.dataList[0].balance, purResult.params.balance),
			}, shopCredit.dataList[0]);
			let credit = await common.callInterface('ql-15007', {
				'dwid': purResult.params.dwid
			});
			common.isApproximatelyEqualAssert({
				'balance': common.add(lastCredit.dataList[0].balance, purResult.params.balance)
			}, credit.dataList[0]);
		});
	});
});
