'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const billExp = require('../../getExp/billExp');

describe('按订货入库', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	//app1与adev1提示略有不同 问题不大，暂时按app1结果验证,等后续合并结果
	it('120026.不支持按订货开单的跨门店操作', async function () {
		await common.loginDo({
			'logid': '200',
			'pass': '000000',
		});
		let orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson()); //新增订货
		let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);

		await common.loginDo();
		let sfRes = await purRequestHandler.invinByOrder(qfRes.result, false);
		expect(sfRes.result).to.includes({
			error: '不允许按订货开单入库的跨门店操作，请按指定门店的工号登录操作',
			// error: '目前不支持按订货开单入库的跨门店操作，请按指定门店的工号登录操作'
		});
	});

	it('120027.2.新增款号', async function () {
		let orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
		qfRes.result.details.push({
			num: 30,
			sizeid: 'XL',
			matCode: 'Agc001',
			colorid: 'BaiSe',
			rem: '明细3'
		});
		let sfRes = await purRequestHandler.invinByOrder(qfRes.result); //按订货入库
		qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['orderversion', 'billno', 'billid']);
	});

	it('120062.按订货入库界面付款后,检查入库单里结余的值.rankA', async function () {
		let orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let qfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
		//付款方式里某个值等于该单总额
		qfRes.result.cash = qfRes.result.totalsum;
		qfRes.result.card = 0;
		qfRes.result.remit = 0;
		let sfRes = await purRequestHandler.invinByOrder(qfRes.result);

		qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
		expect(Number(qfRes.result.balance)).to.equal(0);
	});

	it('120106.多发检查入库数', async function () {
		let orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
		let orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);

		orderQfRes.result.details[0].num = common.add(orderQfRes.result.details[0].num, 10);
		let sfRes = await purRequestHandler.invinByOrder(orderQfRes.result); //按订货入库-多发

		orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk); //重新获取订货单信息
		common.isApproximatelyEqualAssert(orderQfRes.result.details[0], {
			num: orderRes.params.details[0].num,
			recvnum: sfRes.params.details[0].num,
		});

		let param = await format.qlParamsFormat({
			dwid: orderRes.params.dwid,
			styleid: orderRes.params.styleid,
			rem: orderRes.params.remark,
			invid: LOGINDATA.invid
		}, true);
		let qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库

		let exp = getInvinByOrderQLExp(orderQfRes.result);
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	//后续it都依赖1
	describe('120025.按订货入库 查询+库存--mainLine', function () {
		this.retries(2);
		let orderRes, orderQfRes, sfRes, qfRes, qlParams, invInfo1, invInfo2;
		before(async () => {
			await common.setGlobalParam('invsumprice', 0); //保证库存核算是按进货价核算的
			let purJson = format.jsonparamFormat(basiceJsonparam.purchaseOrderJson());
			let styleInfo = await common.fetchMatInfo(purJson.details[0].styleid); //获取货品信息
			qlParams = {
				'ql-1932': { //货品管理-当前库存
					'invid': purJson.invid,
					'propdresColorid': purJson.details[0].colorid,
					'propdresSizeid': purJson.details[0].sizeid,
					'propdresStyleid': purJson.details[0].styleid,
				},
				'ql-1934': { //货品管理-款号库存
					'invid': purJson.invid,
					'styleid': purJson.details[0].styleid,
					'propdresStyleFlag': 0,
				},
				'ql-19902': { //货品管理-库存分布
					'classid': styleInfo.classid,
					'season': styleInfo.season,
					'styleflag': '0',
				},
			};
			invInfo1 = await common.getResults(qlParams); //获取库存起始值
		});
		it('1.按订货入库', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson()); //新增采购订单
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk); //获取采购订单明细

			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result); //按订货入库-全部入库

			qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk); //获取入库单信息
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['orderversion', 'billno', 'optime']);
		});

		it('2.按订货入库-查询', async function () {
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk); //获取采购订单明细

			let param = await format.qlParamsFormat({
				dwid: orderRes.params.dwid,
				styleid: orderRes.params.details[0].styleid,
				rem: orderRes.params.remark,
				invid: LOGINDATA.invid
			}, true);
			let qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库
			let exp = getInvinByOrderQLExp(orderQfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});

		it('120096.入库单-按批次查检查', async function () {
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				shopid: LOGINDATA.invid,
				invalidflag: 0,
				// flag: 2, //全部入库
				puredmaincode: orderQfRes.result.billno,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			let exp = getMainSearchListExp(qfRes.result);
			exp.puredmaincode = orderRes.result.billno; //订货号
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});

		it.skip('3.当前库存验证', async function () {
			if (USEECINTERFACE == 2) await common.delay(8000); //purednum刷新慢
			invInfo2 = await common.getResults(qlParams);
			let exp = common.addObject(invInfo1['ql-1932'].dataList[0], billExp.getCurInvListExp(sfRes)[`${sfRes.params.details[0].styleid}-${sfRes.params.details[0].colorid}-${sfRes.params.details[0].sizeid}`]);
			common.isApproximatelyEqualAssert(exp, invInfo2['ql-1932'].dataList[0], ['version'], `当前库存数据错误:\nsfRes:${JSON.stringify(sfRes)}`);
		});

		it.skip('4.款号库存验证', async function () {
			let exp = Object.assign(invInfo1['ql-1934'].dataList[0], {
				'invnum': common.add(invInfo1['ql-1934'].dataList[0].invnum || 0, sfRes.params.totalnum),
				'invid': orderRes.params.invid,
				'toltalEntry': common.add(invInfo1['ql-1934'].dataList[0].toltalEntry || 0, sfRes.params.totalnum), //累计进
				'styleid': orderRes.params.details[0].styleid,
			});
			common.isApproximatelyEqualAssert(exp, invInfo2['ql-1934'].dataList[0]);
		});

		it.skip('5.库存分布验证', async function () {
			let index = invInfo1['ql-19902'].dynamicTitle.indexOf('常青店');
			let exp = Object.assign(invInfo1['ql-19902'].dataList[0], {
				'invnum': common.add(invInfo1['ql-19902'].dataList[0].invnum || 0, sfRes.params.totalnum),
				'invmoney': common.add(invInfo1['ql-19902'].dataList[0].invmoney || 0, sfRes.params.totalmoney),
			});
			exp.dynamicData[index] = common.add(exp.dynamicData[index], sfRes.params.totalnum);
			common.isApproximatelyEqualAssert(exp, invInfo2['ql-19902'].dataList[0]);
		});

	});

	describe('120064.部分入库 查询+厂商账款.rankA', function () {
		let orderRes, orderQfRes, sfRes, qfRes;
		it('1.部分入库', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);

			let json = _.cloneDeep(orderQfRes.result);
			let verifyRes = await purRequestHandler.getVerifyingBills(json.dwid); //获取核销信息
			json.finpayVerifybillids = verifyRes.dataList[0].id; //一代verifyRes返回值中的id为相应单据的id,二代是收付款单的id
			json.finpayVerifysum = verifyRes.dataList[0].balance;
			[json.cash, json.card, json.remit] = [0, 0, 0];
			json.details[0].num = 4;
			json.details[1].num = 6;
			sfRes = await purRequestHandler.invinByOrder(json); //按订货入库--部分入库+核销预付款

			qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['orderversion', 'billno', 'optime']);
		});
		it('2.按订货入库-查询', async function () {
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			let param = await format.qlParamsFormat({
				dwid: orderRes.params.dwid,
				styleid: orderRes.params.styleid,
				rem: orderRes.params.remark,
				invid: LOGINDATA.invid
			}, true);
			let qlRes = await common.callInterface('ql-22306', param); //采购入库-按订货入库
			let exp = getInvinByOrderQLExp(orderQfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('3.采购订货-按批次查-查询', async function () {
			let param = await format.qlParamsFormat({
				id1: orderRes.result.billno,
				id2: orderRes.result.billno,
				invid: LOGINDATA.invid
			}, true);
			let qlRes = await common.callInterface('ql-22102', param); //采购订货-按批次查
			//console.log(`orderQfRes : ${JSON.stringify(orderQfRes)}`);
			orderQfRes.result.purcost = sfRes.params.totalnum;
			orderQfRes.result.diffnum = common.sub(orderRes.params.totalnum, sfRes.params.totalnum);
			orderQfRes.result.deliveryflag = '部分入库';

			common.isApproximatelyEqualAssert(orderQfRes.result, qlRes.dataList[0]);
		});
		it('4.待核销明细', async function () {
			let qlRes = await purRequestHandler.getVerifyingBills(orderRes.params.dwid); //获取核销信息
			let exp = Object.assign(qfRes.result, {
				invname: qfRes.result.show_invid || qfRes.result.show_shopid,
				dwname: qfRes.result.show_dwid,
				agencyaccountid: '0',
				verifysum: qfRes.result.finpayVerifysum,
				balance: 2000,
				balancetype: '3',
				balancetypename: '退货',
			});
			if (USEECINTERFACE == 1) exp.id = sfRes.result.pk; //二代的id为收付款单id,无法验证
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
	});

	describe('120092.全部发货状态不允许继续发货', function () {
		let orderRes, orderQfRes, sfRes, msgStr;
		before(() => {
			msgStr = USEECINTERFACE == 1 ? '入库' : '发货';
		});
		it('1.选择部分发货的单子,输入超过订货数/与订货数相同保存', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			orderQfRes.result.details[0].num = 4;
			orderQfRes.result.details[1].num = 6;
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);

			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);
		});
		it('2.选择部分发货的单子,一个款全部发货,另外一个款部分发货,再输入发货数保存', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			orderQfRes.result.details[1].num = 6;
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);

			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);
		});
		it('3.选择全部发货的单子(已发数与订货数相等),输入数量点保存', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);

			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result, false);
			expect(sfRes.result).to.includes({
				error: `订单已全部${msgStr}或已终结，不允许再${msgStr}`,
			});
		});
		it('4.选择全部发货的单子(已发数大于订货数),输入数量点保存', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);

			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			orderQfRes.result.details[0].num = common.add(orderQfRes.result.details[0].num, 10);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result, false);
			expect(sfRes.result).to.includes({
				error: `订单已全部${msgStr}或已终结，不允许再${msgStr}`,
			});
		});
		it('5.全部发货,作废采购订货单', async function () {
			orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			sfRes = await purRequestHandler.invinByOrder(orderQfRes.result);
			let cancelRes = await purRequestHandler.cancelPurOrderBill({
				pk: orderRes.result.pk,
				check: false,
			});
			expect(cancelRes.result).to.includes({
				error: '订单已入库，不允许作废'
			});
		});
	});

	describe('异地仓库', function () {
		before(async () => {
			await common.loginDo({
				'logid': '400',
				'pass': '000000',
			});
		});
		after(async () => {
			await common.loginDo();
		});
		it('120153.异地仓库 总经理为自己门店做按订货入库操作-颜色尺码.rankA', async function () {
			let orderRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			let orderQfRes = await purRequestHandler.purOrderQueryBilling(orderRes.result.pk);
			let sfRes = await purRequestHandler.invinByOrder(orderQfRes.result); //按订货入库-全部入库
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
			sfRes.params.invid = BASICDATA.shopidCkd;
			sfRes.params.show_invid = '仓库店';
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['orderversion', 'billno', 'optime']);
		});
	});

});

/*
 * 拼接 按订货入库-查询界面结果期望值
 */
function getInvinByOrderQLExp(orderResult) {
	let deliveryflagArr = ['未入库', '部分入库', '全部入库'];
	let exp = {
		optime: orderResult.optime,
		invname: orderResult.show_invid,
		deliveryflag: deliveryflagArr[orderResult.flag],
		provideridname: orderResult.show_dwid,
		totalnum: orderResult.totalnum,
		prodate: orderResult.prodate,
		opname: LOGINDATA.name,
		id: orderResult.pk,
		flag: orderResult.flag,
		respstaffname: orderResult.show_respopid.split(',')[1], //e.g. 000,总经理
		invalidflag: '0',
		modelClass: 'PuredMainBean',
		rem: orderResult.rem,
		purcost: 0,
		billno: orderResult.billno,
	};
	orderResult.details.forEach((obj) => {
		exp.purcost = common.add(exp.purcost, obj.recvnum); //入库数
	});
	exp.diffnum = common.sub(exp.totalnum, exp.purcost); //差异数
	return exp;
};

/*
 * 拼接按批次查结果的期望值
 * qfResult - qf-14212-1返回结果
 */
function getMainSearchListExp(qfResult) {
	let exp = {
		optime: qfResult.optime,
		remark: qfResult.remark,
		remit: qfResult.remit,
		shopname: qfResult.show_shopid,
		verifysum: qfResult.finpayVerifysum,
		id: qfResult.pk,
		finpayAlipay: qfResult.finpayAlipay,
		balance: qfResult.balance,
		finpayWeixinpay: qfResult.finpayWeixinpay,
		modelClass: qfResult.modelClass,
		purmoney: qfResult.totalmoney,
		card: qfResult.card,
		totalmoney: qfResult.totalmoney,
		dwname: qfResult.show_dwid,
		agency: qfResult.agency || 0,
		totalnum: qfResult.totalnum,
		seller: qfResult.show_deliver.split(',')[1], //e.g. 000,总经理
		prodate: qfResult.prodate,
		opname: qfResult.opname || LOGINDATA.name,
		invalidflag: qfResult.invalidflag,
		weixinpay: qfResult.finpayWeixinpay || 0,
		alipay: qfResult.finpayAlipay || 0,
		// totalRoll: '',//总匹数
		cash: qfResult.cash,
		// puredmaincode: '0', //订货号 没有默认为0
		paysum: qfResult.actualpay, //付款
		billno: qfResult.billno,
		outsum: 0, //进货额
		backsum: 0, //退货额
	};
	qfResult.details.map((obj) => {
		obj.num < 0 ? exp.backsum += Number(obj.total) : exp.outsum += Number(obj.total);
	});
	return exp;
};
