'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
// const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 批量入库
 */

describe('批量入库--mianLine', function () {
	this.timeout(30000);

	let agc001Info, agc002Info;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		agc001Info = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		agc002Info = await common.fetchMatInfo(BASICDATA.styleidAgc002);
	});

	// 除了验证批量入库后按批次、按明细、款号库存情况外
	// 同时验证了：1、不同厂商的货品做批量入库单生成多个按批次单据,
	// 2、同时每个按批次单据都是欠款单
	// 3、不同厂商的货品做批量入库，qf结果中只能显示某一个厂商的款号内容,不能同时显示多个厂商的款号内容
	describe('120024,均色均码+批量入库(这个用例中还穿插了120040、120042需要验证的内容).rankA', function () {
		let purInfo, invInfo;
		before(async () => {
			await common.setGlobalParam('ignorecolorsize', 1); //设置均色均码

			// 批量入库前，先查询当前库存、款号库存、库存分布情况
			let purJson = format.jsonparamFormat(basiceJson.bulkPurchaseIgnoreCS());
			invInfo = {
				'currInvSC1': {
					'invid': purJson.invid,
					'propdresColorid': purJson.details[0].colorid,
					'propdresSizeid': purJson.details[0].sizeid,
					'propdresStyleid': purJson.details[0].styleid,
				},
				'currInvSC2': {
					'invid': purJson.invid,
					'propdresColorid': purJson.details[1].colorid,
					'propdresSizeid': purJson.details[1].sizeid,
					'propdresStyleid': purJson.details[1].styleid,
				},
				'matInvSC1': {
					'invid': purJson.invid,
					'styleid': purJson.details[0].styleid,
				},
				'matInvSC2': {
					'invid': purJson.invid,
					'styleid': purJson.details[1].styleid,
				},
				'invLayoutSC1': {
					'classid': agc001Info.classid,
					'season': agc001Info.season,
					'styleflag': '0'
				},
				'invLayoutSC2': {
					'classid': agc002Info.classid,
					'season': agc002Info.season,
					'styleflag': '0'
				}
			};
			let currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat(invInfo.currInvSC1));
			expect(currInvnum, `当前库存查询失败:${JSON.stringify(currInvnum)}`).to.not.have.property('error');
			invInfo.currInvnum1 = currInvnum;
			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat(invInfo.currInvSC2));
			expect(currInvnum, `当前库存查询失败:${JSON.stringify(currInvnum)}`).to.not.have.property('error');
			invInfo.currInvnum2 = currInvnum;

			let matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat(invInfo.matInvSC1));
			expect(matInvnum, `款号库存查询失败:${JSON.stringify(matInvnum)}`).to.not.have.property('error');
			invInfo.matInvnum1 = matInvnum;
			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat(invInfo.matInvSC2));
			expect(matInvnum, `款号库存查询失败:${JSON.stringify(matInvnum)}`).to.not.have.property('error');
			invInfo.matInvnum2 = matInvnum;

			let invnumLayout = await common.callInterface('ql-19902', format.qlParamsFormat(invInfo.invLayoutSC1));
			expect(invnumLayout, `库存分布查询失败:${JSON.stringify(invnumLayout)}`).to.not.have.property('error');
			invInfo.invnumLayout1 = invnumLayout;
			invnumLayout = await common.callInterface('ql-19902', format.qlParamsFormat(invInfo.invLayoutSC2));
			expect(invnumLayout, `库存分布查询失败:${JSON.stringify(invnumLayout)}`).to.not.have.property('error');
			invInfo.invnumLayout2 = invnumLayout;

			// 批量入库
			purInfo = await common.editBilling(purJson);
			if (USEECINTERFACE == 1) await common.delay(500); //
		});

		after(async () => {
			await common.setGlobalParam('ignorecolorsize', 0); //设置为颜色尺码
		});

		let purInfo1, purInfo2;
		it('验证采购入库明细,不同厂商的货品做批量入库单生成多个按批次单据,同时每个按批次单据都是欠款单', async function () {
			let billIds = purInfo.result.billIds.split(',');
			expect(billIds.length == 2, `不同厂商的货品一起做批量入库单，不同厂商要生成不同的按批次单据`).to.be.true;
			purInfo1 = await purReqHandler.purQueryBilling(billIds[0]);
			purInfo2 = await purReqHandler.purQueryBilling(billIds[1]);
			expect(Number(purInfo1.result.balance) < 0, `批量入库生成的采购入库单应该是欠款单，但这里不是，数据错误`).to.be.true;
			common.isApproximatelyEqualAssert({
				'balance': -purInfo.params.details[0].total,
				'totalmoney': purInfo.params.details[0].total,
				'shouldpay': purInfo.params.details[0].total,
				'totalnum': purInfo.params.details[0].num,
				'prodate': purInfo.params.prodate,
				'cashchange': '0',
				'agency': '0',
				'remit': '0',
				'card': '0',
				'cash': '0',
				'actualpay': '0',
				'finpayAlipay': '0',
				'invid': purInfo.params.invid,
				'deliver': purInfo.params.deliver,
				'remark': '',
			}, purInfo1.result);
			common.isApproximatelyEqualAssert({
				'total': purInfo.params.details[0].total,
				'styleid': purInfo.params.details[0].styleid,
				'sizeid': purInfo.params.details[0].sizeid,
				'colorid': purInfo.params.details[0].colorid,
				'num': purInfo.params.details[0].num,
				'rem': purInfo.params.details[0].rem,
			}, purInfo1.result.details[0]);
			expect(purInfo1.result.details.length == 1, `只能显示某一个厂商的款号内容,不能同时显示多个厂商的款号内容，这里不是这样的错误`).to.be.true;
			expect(Number(purInfo2.result.balance) < 0, `批量入库生成的采购入库单应该是欠款单，但这里不是，数据错误`).to.be.true;
			common.isApproximatelyEqualAssert({
				'balance': -purInfo.params.details[1].total,
				'totalmoney': purInfo.params.details[1].total,
				'shouldpay': purInfo.params.details[1].total,
				'totalnum': purInfo.params.details[1].num,
				'prodate': purInfo.params.prodate,
				'cashchange': '0',
				'agency': '0',
				'remit': '0',
				'card': '0',
				'cash': '0',
				'actualpay': '0',
				'finpayAlipay': '0',
				'invid': purInfo.params.invid,
				'deliver': purInfo.params.deliver,
				'remark': '',
			}, purInfo2.result);
			common.isApproximatelyEqualAssert({
				'total': purInfo.params.details[1].total,
				'styleid': purInfo.params.details[1].styleid,
				'sizeid': purInfo.params.details[1].sizeid,
				'colorid': purInfo.params.details[1].colorid,
				'num': purInfo.params.details[1].num,
				'rem': purInfo.params.details[1].rem,
			}, purInfo2.result.details[0]);
			expect(purInfo2.result.details.length == 1, `只能显示某一个厂商的款号内容,不能同时显示多个厂商的款号内容，这里不是这样的错误`).to.be.true;
		});

		it('验证按批次查', async function () {
			let billNos = purInfo.result.billno.split(',');
			let batchList1 = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': billNos[0],
				'id2': billNos[0],
				'dwid': purInfo.params.details[0].mainDwid,
				'shopid': purInfo.params.invid,
			}));
			let batchList2 = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': billNos[1],
				'id2': billNos[1],
				'dwid': purInfo.params.details[1].mainDwid,
				'shopid': purInfo.params.invid,
			}));
			common.isApproximatelyEqualAssert({
				'remit': '0',
				'shopname': purInfo1.result.show_shopid,
				'balance': purInfo1.result.balance,
				'finpayWeixinpay': '0',
				'totalmoney': purInfo1.result.totalmoney,
				'dwname': purInfo1.result.show_dwid,
				'agency': '0',
				'totalnum': purInfo1.result.totalnum,
				'opname': purInfo1.result.opname,
				'prodate': common.getCurrentDate('YY-MM-DD'),
				'weixinpay': '0',
				'alipay': '0',
				'cash': '0',
				'billno': billNos[0],
			}, batchList1.dataList[0]);

			common.isApproximatelyEqualAssert({
				'shopname': purInfo2.result.show_shopid,
				'balance': purInfo2.result.balance,
				'totalmoney': purInfo2.result.totalmoney,
				'dwname': purInfo2.result.show_dwid,
				'agency': '0',
				'weixinpay': '0',
				'finpayWeixinpay': '0',
				'alipay': '0',
				'cash': '0',
				'remit': '0',
				'card': '0',
				'totalnum': purInfo2.result.totalnum,
				'opname': purInfo2.result.opname,
				'prodate': common.getCurrentDate('YY-MM-DD'),
				'billno': billNos[1],
			}, batchList2.dataList[0]);
		});

		it('验证当前库存', async function () {
			let currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat(invInfo.currInvSC1));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfo.currInvnum1.dataList[0].invnum, purInfo.params.details[0].num),
				'colorid': purInfo.params.details[0].colorid,
				'sizeid': purInfo.params.details[0].sizeid,
				'styleid': purInfo.params.details[0].styleid,
			}, currInvnum.dataList[0]);

			currInvnum = await common.callInterface('ql-1932', format.qlParamsFormat(invInfo.currInvSC2));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfo.currInvnum2.dataList[0].invnum, purInfo.params.details[1].num),
				'colorid': purInfo.params.details[1].colorid,
				'sizeid': purInfo.params.details[1].sizeid,
				'styleid': purInfo.params.details[1].styleid,
			}, currInvnum.dataList[0]);
		});

		it('验证款号库存', async function () {
			let matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat(invInfo.matInvSC1));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfo.matInvnum1.dataList[0].invnum, purInfo.params.details[0].num),
				'invid': purInfo.params.invid,
				'toltalEntry': common.add(invInfo.matInvnum1.dataList[0].toltalEntry, purInfo.params.details[0].num),
				'styleid': purInfo.params.details[0].styleid,
				'style_marketdate': invInfo.matInvnum1.dataList[0].style_marketdate,
			}, matInvnum.dataList[0]);

			matInvnum = await common.callInterface('ql-1934', format.qlParamsFormat(invInfo.matInvSC2));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfo.matInvnum2.dataList[0].invnum, purInfo.params.details[1].num),
				'invid': purInfo.params.invid,
				'toltalEntry': common.add(invInfo.matInvnum2.dataList[0].toltalEntry, purInfo.params.details[1].num),
				'styleid': purInfo.params.details[1].styleid,
				'style_marketdate': invInfo.matInvnum2.dataList[0].style_marketdate,
			}, matInvnum.dataList[0]);
		});

		it('验证库存分布', async function () {
			//数据更新慢。
			let invnumLayout = await common.callInterface('ql-19902', format.qlParamsFormat(invInfo.invLayoutSC1));
			let index = invnumLayout.dynamicTitle.indexOf(LOGINDATA.depname);
			assert.equal(common.add(invInfo.invnumLayout1.dataList[0].invnum, purInfo.params.details[0].num), invnumLayout.dataList[0].invnum, `库存分布有误1`);

			let expectInvnum = common.add(invInfo.invnumLayout1.dataList[0].dynamicData[index], purInfo.params.details[0].num);
			assert.equal(expectInvnum, invnumLayout.dataList[0].dynamicData[index], `库存分布有误2`);

			invnumLayout = await common.callInterface('ql-19902', format.qlParamsFormat(invInfo.invLayoutSC2));
			index = invnumLayout.dynamicTitle.indexOf(LOGINDATA.depname);
			assert.equal(common.add(invInfo.invnumLayout2.dataList[0].invnum, purInfo.params.details[1].num), invnumLayout.dataList[0].invnum, `库存分布有误1`);

			expectInvnum = common.add(invInfo.invnumLayout2.dataList[0].dynamicData[index], purInfo.params.details[1].num);
			assert.equal(expectInvnum, invnumLayout.dataList[0].dynamicData[index], `库存分布有误2`);
		});
	});

	describe('120048,批量入库后店员检查', function () {
		let [purInfo, purInfo1] = [{}, {}];
		before(async () => {
			await common.setGlobalParam('ignorecolorsize', 1);

			// 批量入库前，先查询当前库存、款号库存、库存分布情况
			let purJson = basiceJson.bulkPurchaseIgnoreCS();
			purJson.details = [_.cloneDeep(purJson.details[0])];
			purJson = format.jsonparamFormat(purJson);
			purJson.opid = purJson.deliver;
			delete purJson.deliver;
			let purResult = await common.callInterface(purJson.interfaceid, {
				'jsonparam': purJson
			});
			expect(purResult, `批量入库失败:${JSON.stringify(purResult)}`).to.not.have.property('error');
			purInfo = {
				'params': purJson,
				'result': purResult
			};
		});

		after(async () => {
			await common.setGlobalParam('ignorecolorsize', 0); //设置为颜色尺码
		});

		it('检查按明细查店员名称', async function () {
			let billIds = purInfo.result.billIds.split(',');
			purInfo1 = await purReqHandler.purQueryBilling(billIds[0]);
			expect(purInfo1.result, `按明细查默认店员有误`).to.includes({
				'deliver': LOGINDATA.id
			});
			common.isApproximatelyEqualAssert(purInfo.params, purInfo1.result, ['rowid']);
		});

		it('检查按批次查店员名称', async function () {
			let billNos = purInfo.result.billno.split(',');
			let batchList1 = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': billNos[0],
				'id2': billNos[0],
				'dwid': purInfo.params.details[0].mainDwid,
				'shopid': purInfo.params.invid,
			}));
			expect(batchList1.dataList[0], `按批次查默认店员有误`).to.includes({
				'seller': LOGINDATA.name
			});
			common.isApproximatelyEqualAssert(purInfo1.result, batchList1.dataList[0]);
		});
	});

	describe('异地仓库', function () {
		before(async () => {
			await common.loginDo({
				'logid': '400', //文一店总经理，文一店绑仓库店
				'pass': '000000',
			});
			await common.setGlobalParam('ignorecolorsize', 1); //均色均码
		});
		after(async () => {
			await common.loginDo();
			await common.setGlobalParam('ignorecolorsize', 0); //颜色尺码
		});
		it('120114.门店绑定仓库,批量入库的入库门店为开单门店.rankA', async function () {
			let sfRes = await common.editBilling(basiceJson.bulkPurchaseIgnoreCS());
			let pkArr = sfRes.result.billIds.split(',');
			let billnoArr = sfRes.result.billno.split(',');

			let qfRes = await purReqHandler.purQueryBilling(pkArr[0]);
			common.isApproximatelyEqualAssert({
				shopid: BASICDATA.shopidWyd,
				show_shopid: '文一店',
				invid: BASICDATA.shopidCkd,
				show_invid: '仓库店'
			}, qfRes.result);

			let qlRes = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': billnoArr[0],
				'id2': billnoArr[0],
				'dwid': sfRes.params.details[0].mainDwid,
				'shopid': BASICDATA.shopidWyd,
			}));
			expect(qlRes.dataList[0], `按批次查门店有误`).to.includes({
				'shopname': '文一店',
			});
		});
	});

});
