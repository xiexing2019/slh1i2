'use strict';
const common = require('../../../lib/common.js');
const reqHandler = require('../../help/reqHandlerHelp');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');

/*
 * 开采购入库单后，当前库存、库存分布、款号库存以及收支流水的变化
 */
describe('采购入库--上次价-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		await common.setGlobalParam('sales_show_lastprice', 1);
		await common.setGlobalParam('sales_use_lastsaleprice', 1);
	});

	after(async () => {
		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('sales_show_lastprice', 0);
	});

	it('120088,【采购入库-新增入库】取上次价.rankA', async function () {
		// 常青店
		await common.loginDo();
		let purJson = basiceJson.purchaseJson();
		for (let i = 0; i < purJson.details.length; i++) {
			purJson.details[i].price = 150;
		};
		await common.editBilling(purJson);

		// 中洲店
		await common.loginDo({
			'logid': '200',
		});
		purJson = basiceJson.purchaseJson();
		for (let i = 0; i < purJson.details.length; i++) {
			purJson.details[i].price = 155;
		};
		const sfRes = await common.editBilling(purJson);

		// 常青店查询上次价
		await common.loginDo();
		let params = {
			interfaceid: 'cs-getlistpurprice',
			dwid: sfRes.params.dwid,
			styleid: sfRes.params.details[0].styleid,
		};
		if (USEECINTERFACE == '2') params.bizType = 1300;
		const lastPrice = await reqHandler.csIFCHandler(params);
		expect(lastPrice.result, `采购入库获取上次价错误`).to.includes({
			'price': '155'
		});
	});
});
