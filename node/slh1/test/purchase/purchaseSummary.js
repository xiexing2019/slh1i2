'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
describe('采购汇总-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('按金额汇总', function () {
		it('120007,采购汇总->按金额汇总', async function () {
			//按金额汇总列表 按批次查列表
			let [moneySummary, batchSumrow] = await Promise.all([common.callInterface('ql-2252', {
				'prodate1': common.getCurrentDate(),
				'prodate2': common.getCurrentDate()
			}), common.callInterface('ql-22301', format.qlOnlySumFormat({}, true))])

			common.isApproximatelyEqualAssert({
				'weixinpay': batchSumrow.sumrow.weixinpay,
				'alipay': batchSumrow.sumrow.alipay,
				'remit': batchSumrow.sumrow.remit,
				'cash': batchSumrow.sumrow.cash,
				'card': batchSumrow.sumrow.card,
			}, moneySummary.dataList[0]);
		});
	});

	describe('按款号汇总', function () {
		it('120008.1.查询.rankA', async function () {
			let json = format.jsonparamFormat(basiceJson.purchaseJson());
			let styleInfo = await common.fetchMatInfo(json.details[0].styleid);
			let param = format.qlParamsFormat({
				styleid: json.details[0].styleid,
				stylename: styleInfo.name,
				maindwid: json.dwid,
				marketdate1: styleInfo.marketdate,
				marketdate2: styleInfo.marketdate,
				shopid: json.shopid,
				classid: styleInfo.classid,
				brandid: styleInfo.brandid,
				season: styleInfo.season,
			}, true);
			let qlRes = await common.callInterface('ql-22921', param); //采购入库--按汇总--按款号汇总
			let exp = {
				marketdate: styleInfo.marketdate,
				mat_name: styleInfo.name,
				mat_class: styleInfo.show_classid,
				mat_code: styleInfo.code,
				mat_brand: styleInfo.show_brandid,
				dw_name: styleInfo.show_dwid,
				mat_season: styleInfo.show_season,
			};
			qlRes.dataList.forEach((obj) => {
				common.isApproximatelyEqualAssert(exp, obj);
			});
		});

		it('120008.2.按款号汇总不统计特殊货品', async function () {
			await common.editBilling(basiceJson.specialPurchaseJson()); //新增入库单+特殊货品
			let param = format.qlParamsFormat({}, true);
			let qlRes = await common.callInterface('ql-22921', param); //采购入库--按汇总--按款号汇总
			qlRes.dataList.map((obj) => {
				expect(['00000', '00001']).not.to.includes(obj.mat_code);
			});
		});

		it('120008.3.查询数据验证', async function () {
			let json = format.jsonparamFormat(basiceJson.purchaseJson());
			let param = format.qlParamsFormat({
				maindwid: json.dwid,
				styleid: json.details[0].styleid,
			}, true);
			let qlRes1 = await common.callInterface('ql-22921', param); //采购入库--按汇总--按款号汇总

			let sfRes = await common.editBilling(json);
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);

			let qlRes2 = await common.callInterface('ql-22921', param);
			for (let i = 0, length = qlRes1.dataList.length; i < length; i++) {
				let data = qlRes1.dataList[i];
				qfRes.result.details.map((obj) => {
					if (obj.styleid == json.details[0].styleid && obj.show_colorid == data.mat_color && obj.show_sizeid == data.mat_size) {
						data.total = common.add(data.total, obj.total);
						data.num = common.add(data.num, obj.num);
						data.asknum = common.add(data.asknum, obj.num);
						data.askcountnum++;
					};
				});
			};
			common.isApproximatelyArrayAssert(qlRes1.dataList, qlRes2.dataList);
		});

		it('120045.厂商验证', async function () {
			let param = format.qlParamsFormat({
				maindwid: BASICDATA.dwidRt,
				search_list: 0
			}, true);
			let qlRes1 = await common.callInterface('ql-22921', param); //采购入库--按汇总--按款号汇总

			let json = basiceJson.purchaseJson();
			json.dwid = BASICDATA.dwidVell;
			await common.editBilling(json); //厂商Vell入库单
			json.dwid = BASICDATA.dwidRt;
			let sfRes = await common.editBilling(json); //厂商Rt入库单

			let qlRes2 = await common.callInterface('ql-22921', param);
			qlRes1.sumrow.num = common.add(qlRes1.sumrow.num, sfRes.params.totalnum);
			qlRes1.sumrow.asknum = common.add(qlRes1.sumrow.asknum, sfRes.params.totalnum);
			qlRes1.sumrow.askcountnum++;
			qlRes1.sumrow.total = common.add(qlRes1.sumrow.total, sfRes.params.totalmoney);
			common.isApproximatelyEqualAssert(qlRes1.sumrow, qlRes2.sumrow);
		});

		it('120121.拿货次数验证', async function () {
			let params = {
				'ql-22302': { //采购入库-按明细查
					propdresStyleid: BASICDATA.styleidAgc001,
					today: true
				},
				'ql-22921': { //采购入库--按汇总--按款号汇总
					styleid: BASICDATA.styleidAgc001,
					search_list: 0,
					today: true,
				},
			};
			let qlRes = await common.getResults(params);
			let billnoArr = []; //统计agc001的批次号
			qlRes['ql-22302'].dataList.map((obj) => {
				if (Number(obj.realnum) > 0) billnoArr.push(obj.mainBillno); //只退货的不计算在拿货次数中
			});
			billnoArr = common.dedupe(billnoArr); //批次号去重
			expect(String(billnoArr.length), `按明细查拿货次数为${billnoArr.length},按款号汇总拿货次数为${qlRes['ql-22921'].sumrow.askcountnum}`)
				.to.equal(qlRes['ql-22921'].sumrow.askcountnum);
		});

		describe('120080.店员权限查询', function () {
			let param = format.qlParamsFormat({
				search_list: 0
			}, true);
			before(async () => {
				await common.loginDo({
					'logid': '004',
					'pass': '000000',
				});
			});
			after(async () => {
				await common.loginDo();
			});
			it('1.本门店数据查询', async function () {
				let qlResAll = await common.callInterface('ql-22921', param);
				param.shopid = BASICDATA.shopidCqd;
				let qlResCqd = await common.callInterface('ql-22921', param);
				expect(qlResAll.sumrow).to.eql(qlResCqd.sumrow);
			});
			it('2.其他门店数据查询', async function () {
				param.shopid = BASICDATA.shopidZzd;
				let qlResZzd = await common.callInterface('ql-22921', param);
				expect(qlResZzd.count, `常青店店员查询其他门店结果错误 ${qlResZzd.count}!=0\n查询结果为:${JSON.stringify(qlResZzd)}`).to.equal('0');
			});
		});
	});

	describe('按厂商返货', function () {
		before(async () => {
			await common.setGlobalParam('sales_backmat_alert', 0); //关闭 “逐条进行退货数大于拿货数验证”
		});

		it('120009,采购汇总->按厂商返货', async function () {
			// 新增货品
			let addGoodRes = await basicInfoReqHandler.editStyle({
				jsonparam: basiceJson.addGoodJson()
			});

			// 用新增的货品开退货单
			let purchaseJson = basiceJson.purchaseJson();
			purchaseJson.details[0].styleid = addGoodRes.pk;
			purchaseJson.details[0].num = -1;
			let purResult = await common.editBilling(purchaseJson);
			let qfResult = await purRequestHandler.purQueryBilling(purResult.result.pk);

			// 验证按厂商返货汇总数据
			let backmatSummary = await common.callInterface('ql-2254', format.qlParamsFormat({
				'dwid': BASICDATA.dwidVell,
				'styleid': addGoodRes.pk,
			}, true));
			common.isApproximatelyEqualAssert({
				'num': purResult.params.details[0].num,
				'mat_color': qfResult.result.details[0].show_colorid,
				'mat_size': qfResult.result.details[0].show_sizeid,
				'marketdate': common.getCurrentDate('YY-MM-DD'),
				'mat_name': addGoodRes.params.name,
				'mat_code': addGoodRes.params.code,
				'mat_money': purResult.params.details[0].total
			}, backmatSummary.dataList[0]);
		});
	});

	describe('按厂商汇总', function () {
		it('120010,采购汇总->按厂商汇总', async function () {
			TESTCASE = {
				describe: '采购汇总-按厂商汇总值与采购入库-按批次查中值一致',
				jira: 'SLHSEC-7502'
			};
			let sfRes = await common.editBilling(basiceJson.purchaseJson()); //为了使按厂商汇总列表有数据
			// 按厂商汇总列表 按批次查列表
			let [vendorSummary, batchSumrow] = await Promise.all([common.callInterface('ql-2253', format.qlParamsFormat({
				'dwid': sfRes.params.dwid,
			}, true)), common.callInterface('ql-22301', format.qlOnlySumFormat({
				'dwid': sfRes.params.dwid
			}, true))]);

			// 验证按厂商汇总数据的正确性
			common.isApproximatelyEqualAssert({
				'num': common.add(batchSumrow.sumrow.totalnum, vendorSummary.dataList[0].backnum),
				'weixinpay': batchSumrow.sumrow.weixinpay,
				'alipay': batchSumrow.sumrow.alipay,
				'actualmoney': batchSumrow.sumrow.totalmoney,
				'remit': batchSumrow.sumrow.remit,
				'cash': batchSumrow.sumrow.cash,
				'card': batchSumrow.sumrow.card,
				'actualnum': batchSumrow.sumrow.totalnum
			}, vendorSummary.dataList[0]);
		});

		it('120133,按厂商汇总 界面检查退货数', async function () {
			let backJson = basiceJson.purchaseJson();
			for (let i = 0; i < backJson.details.length; i++) {
				backJson.details[i].num = -Number(backJson.details[i].num);
			}
			backJson.card = 0;
			backJson.cash = 0;
			backJson.remit = 0;
			let sfRes = await common.editBilling(backJson);
			// 按厂商汇总列表
			let vendorSummary = await common.callInterface('ql-2253', format.qlParamsFormat({
				'dwid': sfRes.params.dwid,
			}, true));
			expect(Number(vendorSummary.dataList[0].backnum) > 0, `界面检查退货数小于0,错误`).to.be.true;
		});
	})

	// 出入库汇总只汇总 '采购入库'、'门店调入'、'门店调出'三种单据，销售数据不统计在内
	describe('按出入库汇总', function () {
		// 采购入库，入库数大于0
		it('120011,采购汇总->出入库汇总->入库', async function () {
			let purResult = await common.editBilling(basiceJson.purchaseJson());
			let invnumSummary = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));
			expect(invnumSummary, `采购入库，出入库汇总列表查询出错:${JSON.stringify(invnumSummary)}`).to.not.have.property('error');
			common.isApproximatelyEqualAssert({
				'totalmoney': purResult.params.totalmoney,
				'invname': LOGINDATA.depname,
				'dictname': '采购进货',
				'totalnum': purResult.params.totalnum,
				'billno': purResult.result.billno,
				'prodate': common.getCurrentDate('MM-DD'),
			}, invnumSummary.dataList[0]);
		});
		// 采购入库，入库数小于0（退货）
		it('120011,采购汇总->出入库汇总->出库', async function () {
			let purJson = basiceJson.purchaseJson();
			purJson.details = _.cloneDeep([purJson.details[0]]);
			purJson.details[0].num = -3;
			let purResult = await common.editBilling(purJson);
			let invnumSummary = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));
			expect(invnumSummary, `采购入库，出入库汇总列表查询出错:${JSON.stringify(invnumSummary)}`).to.not.have.property('error');
			common.isApproximatelyEqualAssert({
				'totalmoney': purResult.params.totalmoney,
				'invname': LOGINDATA.depname,
				'dictname': '采购进货',
				'totalnum': purResult.params.totalnum,
				'billno': purResult.result.billno,
				'prodate': common.getCurrentDate('MM-DD'),
			}, invnumSummary.dataList[0]);
		});
		// 门店调入
		it.skip('120011,采购汇总->出入库汇总->调入-调一下', async function () {
			// 中洲店调出
			await common.loginDo({
				'logid': '200',
				'pass': '000000'
			});
			let outResult = await common.editBilling(basiceJson.outJson());

			// 常青店调入
			await common.loginDo();
			let onRoadInfo = await common.callInterface('qf-1863-3', {
				'pk': outResult.result.pk,
			});
			let inJson = format.jsonparamFormat(basiceJson.inJson(onRoadInfo));
			let inResult = await common.callInterface('sf-14214-1', {
				'jsonparam': inJson
			});
			expect(inResult, `门店调入失败:${JSON.stringify(inResult)}`).to.not.have.property('error');

			// 按出入库汇总
			let invnumSummary = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));

			expect(invnumSummary.dataList[0], `采购入库，出入库汇总数据有误`).to.includes({
				'totalmoney': inJson.totalsum.toString(),
				'invname': LOGINDATA.depname,
				'dictname': '调拨入库',
				'totalnum': inJson.totalnum.toString(),
				'billno': inResult.billno,
				'prodate': common.getCurrentDate('MM-DD'),
			});
		});
		// 门店调出
		it.skip('120011,采购汇总->出入库汇总->调出-调入需要调一下', async function () {
			// 常青店调出
			await common.loginDo();
			let outJson = basiceJson.outJson();
			outJson.shopid = BASICDATA.shopidCqd; //调出门店--常青店
			outJson.invidref = BASICDATA.shopidCkd; //调入门店--仓库店
			let outResult = await common.editBilling();

			// 仓库店调入
			await common.loginDo({
				'logid': '100',
				'pass': '000000'
			});
			let onRoadInfo = await common.callInterface('qf-1863-3', {
				'pk': outResult.result.pk,
			});
			let inJson = format.jsonparamFormat(basiceJson.inJson(onRoadInfo));
			let inResult = await common.callInterface('sf-14214-1', {
				'jsonparam': inJson
			});
			expect(inResult, `门店调入失败:${JSON.stringify(inResult)}`).to.not.have.property('error');

			//常青店登录 按出入库汇总
			await common.loginDo();
			let invnumSummary = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));
			expect(invnumSummary.dataList[0], `采购入库，出入库汇总数据有误`).to.includes({
				'totalmoney': inJson.totalsum.toString(),
				'invname': LOGINDATA.depname,
				'dictname': '调拨入库',
				'totalnum': inJson.totalnum.toString(),
				'billno': outResult.billno,
				'prodate': common.getCurrentDate('MM-DD'),
			});
		});
		// 销售数据不统计在内
		it('120011,销售数据不统计在内', async function () {
			let invnumSummary1 = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));
			let salesResult = await common.editBilling(basiceJson.salesJson());
			let invnumSummary2 = await common.callInterface('ql-2251', format.qlParamsFormat({}, true));
			expect(invnumSummary2, `采购入库-按出入库汇总统计了销售数据，错误`).to.includes({
				'count': invnumSummary1.count
			});
		});
		it('120118,挂单的数据不应该显示', async function () {
			let param = format.qlParamsFormat({
				search_list: 0,
				search_count: 1,
			}, true);
			let qlRes = await common.callInterface('ql-2251', param);

			await purRequestHandler.editPurHangBill(basiceJson.purchaseJson()); //新增挂单

			let qlRes2 = await common.callInterface('ql-2251', param);
			expect(qlRes.count, `采购入库-按出入库汇总统计了挂单数据,错误`).to.equal(qlRes2.count);
		});
	});

	describe('按类别汇总', function () {
		let styleInfo = {};
		before(async () => {
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		});

		it('120013.查询/数据验证', async function () {
			let param = format.qlParamsFormat({
				propdresStyleClassid: styleInfo.classid,
			}, true);
			let qlRes1 = await common.callInterface('ql-2250', param);

			let sfRes = await common.editBilling(basiceJson.purchaseJson());
			let qlRes2 = await common.callInterface('ql-2250', param);
			let [num, accountmoney] = [0, 0];
			sfRes.params.details.map((obj) => {
				if (obj.styleid == BASICDATA.styleidAgc001) {
					num = common.add(num, obj.num);
					accountmoney = common.add(accountmoney, obj.total);
				};
			});
			qlRes1.dataList.map((obj) => {
				expect(obj.main_type, `查询结果类别不匹配 ${obj.main_type}!=${styleInfo.show_classid}`).to.equal(styleInfo.show_classid);
				if (obj.mat_code == styleInfo.code && obj.mat_name == styleInfo.name) {
					obj.num = common.add(obj.num, num);
					obj.accountmoney = common.add(obj.accountmoney, accountmoney);
				};
			});
			common.isApproximatelyArrayAssert(qlRes1.dataList, qlRes2.dataList);
		});

		it('120031.按类别汇总功能检查', async function () {
			let params = {
				'ql-2250': {
					search_list: 0,
					prodate1: common.getDateString([0, 0, -7])
				},
				'ql-22302': {
					search_list: 0,
					prodate1: common.getDateString([0, 0, -7])
				}
			};
			let qlRes = await common.getResults(params);
			expect(qlRes['ql-2250'].sumrow.num).to.equal(qlRes['ql-22302'].sumrow.realnum);
		});

		it('120032.按类别汇总不统计特殊货品', async function () {
			await common.editBilling(basiceJson.specialPurchaseJson()); //新增入库单+特殊货品
			let param = format.qlParamsFormat({}, true);
			let qlRes = await common.callInterface('ql-2250', param); //采购入库--按汇总--按类别汇总
			qlRes.dataList.map((obj) => {
				expect(['00000', '00001']).not.to.includes(obj.mat_code);
			});
		});

	});

	describe('按品牌汇总', function () {
		let styleInfo;
		before(async () => {
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
			// console.log(`styleInfo = ${JSON.stringify(styleInfo)}`);
		});
		it('120119.组合查询', async function () {
			let params = {
				'ql-22922': { //采购入库-按品牌汇总
					shopid: LOGINDATA.invid,
					brandid: styleInfo.brandid,
					prodate1: common.getDateString([0, 0, -5]),
				},
				'ql-22302': { //采购入库-按明细查
					shopid: LOGINDATA.invid,
					propdresStyleBrandid: styleInfo.brandid,
					prodate1: common.getDateString([0, 0, -5]),
				}
			};
			let qlRes = await common.getResults(params);
			let backnum = 0;
			qlRes['ql-22302'].dataList.map((data) => {
				if (data.realnum < 0) backnum = common.sub(backnum, data.realnum);
			});
			let exp = {
				id: styleInfo.brandid,
				total: qlRes['ql-22302'].sumrow.total,
				num: qlRes['ql-22302'].sumrow.realnum, //数量
				brandname: styleInfo.show_brandid,
				asknum: common.add(qlRes['ql-22302'].sumrow.realnum, backnum), //进货数
				brandid: styleInfo.brandid,
				backnum: backnum
			};
			common.isApproximatelyEqualAssert(exp, qlRes['ql-22922'].dataList[0]);
		});
		it('120128.详细页面数据验证', async function () {
			let params = {
				'ql-22922': { //采购入库-按品牌汇总
					brandid: styleInfo.brandid,
					prodate1: common.getDateString([0, 0, -5]),
				},
				'ql-229221': { //采购入库-按品牌汇总 详细页面
					brandid: styleInfo.brandid,
					prodate1: common.getDateString([0, 0, -5]),
				},
				'ql-22302': { //采购入库-按明细查
					propdresStyleBrandid: styleInfo.brandid,
					prodate1: common.getDateString([0, 0, -5]),
				}
			};
			let qlRes = await common.getResults(params);
			let detDate = {};
			qlRes['ql-22302'].dataList.forEach((obj) => {
				let matCode = `${obj.propdresStyleCode}-${obj.propdresColorid}-${obj.propdresSizeid}`;
				if (!detDate[matCode]) {
					detDate[matCode] = {
						mat_color: obj.propdresColorid,
						mat_name: obj.propdresStyleName,
						mat_size: obj.propdresSizeid,
						mat_code: obj.propdresStyleCode,
						mat_brand: obj.propdresStyleBrandid,
						mat_season: obj.season,
						// dw_name: obj.mainDwxxNameshort,//按明细查的厂商为开单厂商 与款号本身的厂商不同
						billnoArr: [], //统计批次号
					};
				};
				detDate[matCode].total = common.add(detDate[matCode].total || 0, obj.total);
				if (obj.realnum < 0) {
					detDate[matCode].diffnum = common.sub(detDate[matCode].diffnum || 0, obj.realnum); //退货数
				} else if (obj.realnum > 0) {
					detDate[matCode].asknum = common.add(detDate[matCode].asknum || 0, obj.realnum);
					detDate[matCode].billnoArr.push(obj.mainBillno);
				}
				detDate[matCode].num = common.sub(detDate[matCode].asknum, detDate[matCode].diffnum || 0);
			});
			let exp = Object.values(detDate);
			exp.forEach((obj) => {
				obj.askcountnum = common.dedupe(obj.billnoArr).length; //拿货次数
				delete obj.billnoArr;
			});


			common.isApproximatelyArrayAssert(exp, qlRes['ql-229221'].dataList);
			//详细界面数据汇总值等于按品牌汇总明细值
			common.isApproximatelyEqualAssert(qlRes['ql-22922'].dataList[0], qlRes['ql-229221'].sumrow);
		});
		it('120126.详细页面默认按数量排序', async function () {
			let qlRes = await common.callInterface('ql-229221', format.qlParamsFormat({
				brandid: styleInfo.brandid,
				prodate1: common.getDateString([0, 0, -7]),
			}));
			expect(qlRes.dataList.length, '按品牌汇总明细页面数据不足,无法进行默认排序验证').to.above(1);

			let actualArr = [];
			qlRes.dataList.map((data) => {
				actualArr.push(data.num);
			});
			let expArr = _.cloneDeep(actualArr).sort((a, b) => b - a); //倒叙排序
			expect(expArr).to.eql(actualArr);
		});
	});

});
