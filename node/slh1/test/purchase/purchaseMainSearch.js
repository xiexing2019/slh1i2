'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const billExp = require('../../getExp/billExp');
/**
 * 采购入库-按批次查
 */
describe('采购入库-按批次查-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('120001.按批次查-查询.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());
		//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
		let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
		//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
		let param = format.qlParamsFormat({
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			shopid: qfRes.result.invid,
			invalidflag: 0,
			flag: 1,
			dwid: qfRes.result.dwid,
			deliver: qfRes.result.deliver,
			remark: qfRes.result.remark,
		}, true);
		let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
		expect(qlRes.count, `查询结果出错 count=${qlRes.count}!=1`).to.equal('1');
		let exp = purRequestHandler.getPurMainSearchListExp(qfRes.result);
		//qfRes接口少返回了个optime，已叫开发加了，
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('120219,验证备注栏查询条件.rankA', async function () {
		//if(USEECINTERFACE == 1) await common.setGlobalParam('enable_search_date_range', 0); //查询日期范围限制,为不启用，以后可能会去掉
		let purchaseJson = format.jsonparamFormat(basiceJsonparam.purchaseJson());
		purchaseJson.remark = '验证备注';
		let purResult = await common.callInterface('sf-14212-1', {
			'jsonparam': purchaseJson
		});
		expect(purResult, `采购入库失败,${JSON.stringify(purResult)}`).to.not.have.property('error');

		// 满足条件查询
		let searchCondition = {
			'remark': '备注',
			prodate1: common.getDateString([0, -1, 0]),
			prodate2: common.getDateString([0, 0, 0]),
		};
		let qlResult = await common.callInterface('ql-22301', format.qlParamsFormat(searchCondition));
		let isRight = false; //一开始就设置为false，为了防止查询到0条数据
		for (let i = 0; i < qlResult.dataList.length; i++) {
			isRight = qlResult.dataList[i].remark.includes('备注');
		}
		expect(isRight, `备注查询条件正确 `).to.be.true;

		// 不满足条件查询
		searchCondition = Object.assign({}, searchCondition, { remark: '不存在备注' });
		qlResult = await common.callInterface('ql-22301', format.qlParamsFormat(searchCondition));
		expect(qlResult, `不满足备注查询条件的查询出来了结果，错误${JSON.stringify(purResult)}`).to.includes({
			'count': '0'
		});
		searchCondition = Object.assign({}, searchCondition, {
			'sortField': 'remark',
			'sortfieldOrder': '1',
			'remark': '备注'
		})
		qlResult = await common.callInterface('ql-22301', format.qlParamsFormat(searchCondition));
		expect(qlResult, `备注列排序报服务端错误${JSON.stringify(purResult)}`).to.not.have.property('error');
	});

	it('120097.采购订货生成的预付款对应的订货号检查-slh1', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson()); //新增采购订单
		let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.prepayid); //获取预付款单信息

		let param = format.qlParamsFormat({
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
			shopid: qfRes.result.invid,
			puredmaincode: sfRes.result.billno,
		}, true);
		let qlRes = await common.callInterface('ql-22301', param);
		let exp = purRequestHandler.getPurMainSearchListExp(qfRes.result);
		exp.puredmaincode = sfRes.result.billno;
		//问题应该是同120001
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	describe('120003.单据作废,验证厂商账款与货品库存.rankA', function () {
		let json = basiceJsonparam.purchaseJson();
		let billObj = {
			'不欠不余': {},
			'欠款': {
				cash: json.cash - 100,
			},
			'余款': {
				cash: json.cash + 100,
			},
		};
		before(async () => {
			await common.setGlobalParam('invsumprice', 0); //库存核算价格 进货价
		});
		for (let type in billObj) {
			it.skip(`120003.${type}入库单作废`, async function () {
				json.cash = billObj[type].cash || json.cash;
				let sfRes = await common.editBilling(json); //新增采购入库单
				await common.delay(2000);
				let qlParams = {
					'ql-15007': { //厂商总账
						dwid: sfRes.params.dwid,
					},
					'ql-1932': { //当前库存
						propdresStyleid: sfRes.params.details[0].styleid,
						invid: sfRes.params.invid,
						propdresColorid: sfRes.params.details[0].colorid,
						propdresSizeid: sfRes.params.details[0].sizeid,
					},
				};
				if (USEECINTERFACE == 2) await common.delay(500);
				let qlRes1 = await common.getResults(qlParams); //取初始值
				//作废入库单
				await purRequestHandler.cancelPurchaseBill(sfRes.result.pk);

				if (USEECINTERFACE == 2) await common.delay(1000);
				let qlRes2 = await common.getResults(qlParams); //作废后取值
				qlRes1['ql-15007'].dataList[0].balance = common.sub(qlRes1['ql-15007'].dataList[0].balance, sfRes.params.balance);
				qlRes1['ql-15007'].sumrow.balance = common.sub(qlRes1['ql-15007'].sumrow.balance, sfRes.params.balance);
				common.isApproximatelyEqualAssert(qlRes1['ql-15007'], qlRes2['ql-15007']);
				let exp = common.subObject(qlRes1['ql-1932'].sumrow, billExp.getCurInvListExp(sfRes)[`${sfRes.params.details[0].styleid}-${sfRes.params.details[0].colorid}-${sfRes.params.details[0].sizeid}`]);
				common.isApproximatelyEqualAssert(exp, qlRes2['ql-1932'].sumrow);
			});
		};
	});

	describe('120044.按批次查-作废操作和查询.rankA', function () {
		it('1.作废预付款单据-slh1', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson()); //新增采购订单
			let cancelRes = await purRequestHandler.cancelPurchaseBill({
				pk: sfRes.result.prepayid,
				check: false,
			}); //作废预付款单
			let name = USEECINTERFACE == 1 ? '采购订货' : '采购订单';
			let billNo = USEECINTERFACE == 1 ? sfRes.result.billno : format.numberFormat(sfRes.result.billno);
			expect(cancelRes.result).to.includes({
				error: `本批次是${name}预付款单，不允许操作，请从${name}中修改，批次号为${billNo}`
			});
		});
		it('2.入库单作废查询', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());
			await purRequestHandler.cancelPurchaseBill(sfRes.result.pk); //作废入库单
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);
			//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				shopid: qfRes.result.invid,
				invalidflag: 1,
				//flag: 1,
				dwid: qfRes.result.dwid,
				deliver: qfRes.result.deliver,
				remark: qfRes.result.remark,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param);
			let exp = purRequestHandler.getPurMainSearchListExp(qfRes.result);
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);

			param.invalidflag = 0;
			qlRes = await common.callInterface('ql-22301', param);
			expect(qlRes.count).to.equal('0');
		});
	});

	describe.skip('开单员', function () {
		after(async () => {
			await common.loginDo();
		});
		it('120190.开单员修改单据后,款号的进货价验证.rankA', async function () {
			let goodJson = basiceJsonparam.addGoodJson();
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: goodJson,
			}); //新增款号
			let sfRes = await common.editBilling(basiceJsonparam.purchaseJson()); //采购入库
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);

			goodJson.pk = goodRes.pk;
			goodJson.purprice = 200;
			goodRes = await basicReqHandler.editStyle({
				jsonparam: goodJson,
			});

			await common.loginDo({ //开单员登陆
				'logid': '005',
			});
			qfRes.result.interfaceid = 'sf-14212-1';
			qfRes.result.details[0] = {
				num: 20,
				sizeid: 'L',
				styleid: goodRes.pk,
				colorid: 'BaiSe',
				price: goodJson.purprice,
			};
			await common.editBilling(qfRes.result);
			await common.loginDo();
			let goodQfRes = await common.fetchMatInfo(goodRes.pk);
			expect(Number(goodQfRes.purprice)).to.equal(200);
		});
	});

});
