'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');

/**
 * 与采购相关的系统参数,这个文件要一起跑
 */

describe('参数关联', function () {
	this.timeout(30000);
	let changeResult;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('purchaser_always_purprice_asaclist', 0);
		await common.setGlobalParam('purchase_type', 1); //采购入库简单模式
	});

	/*
		1. 全局参数 - 采购入库模式--设置成 1, 默认简单模式， 再设置参数 - 采购员是否总是使用采购价做选款aclist的价格显示--开启， 提示不能同时开启
		2. 全局参数： 采购入库模式 - 复杂模式， 再设置参数 - 采购员是否总是使用采购价做选款aclist的价格显示--开启， 成功
		3. 全局参数 - 采购员是否总是使用采购价做选款aclist的价格显示--开启， 再设置参数 - 采购入库模式– 1, 默认简单模式

		期望结果：
		1. 提示【 采购入库模式简单模式， 不能与采购员是否总是使用采购价做选款aclist的价格显示同时开启】
		2. 保存成功
		3. 提示【 采购入库模式简单模式， 不能与采购员是否总是使用采购价做选款aclist的价格显示同时开启】
	  */

	it('120189.1,采购入库参数关联', async function () {
		changeResult = await common.setGlobalParam('purchaser_always_purprice_asaclist', 1, false);
		expect(changeResult, `参数关联失败1`).to.includes({
			"error": "采购入库模式简单模式，不能与采购员是否总是使用采购价做选款aclist的价格显示同时开启"
		});
	});
	it('120189.2', async function () {
		await common.setGlobalParam('purchase_type', 2); //采购入库复杂模式
		changeResult = await common.setGlobalParam('purchaser_always_purprice_asaclist', 1, false);
		expect(changeResult, `修改系统参数[采购员是否总是使用采购价做选款aclist的价格]失败`).to.includes({
			val: 'ok'
		});
	})
	it('120189.3', async function () {
		changeResult = await common.setGlobalParam('purchase_type', 1, false);
		expect(changeResult, `参数关联失败3`).to.includes({
			"error": "采购入库模式简单模式，不能与采购员是否总是使用采购价做选款aclist的价格显示同时开启"
		});
	});
});
