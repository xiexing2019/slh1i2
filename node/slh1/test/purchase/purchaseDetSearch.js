'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
/**
 * 采购入库-按明细查
 */
describe('采购入库-按明细查-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('120047.按明细查-查询.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());

		let styleInfo = await common.fetchMatInfo(sfRes.params.details[0].styleid);
		let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);

		let param = format.qlParamsFormat({
			mainBillno: qfRes.result.billno,
			mainShopid: qfRes.result.invid,
			mainDwid: qfRes.result.dwid,
			mainDeliver: qfRes.result.deliver,
			concatPropdresStyleCodePropdresStyleName: styleInfo.name,
			propdresStyleid: sfRes.params.details[0].styleid,
			propdresColorid: sfRes.params.details[0].colorid,
			propdresSizeid: sfRes.params.details[0].sizeid,
			propdresStyleClassid: styleInfo.classid,
			propdresStyleBrandid: styleInfo.brandid,
			propdresStyleSeason: styleInfo.season,
		}, true);
		let qlRes = await common.callInterface('ql-22302', param); //采购入库-按明细查
		expect(qlRes.count, `查询结果出错 count=${qlRes.count}!=1`).to.equal('1');
		let exp = {
			propdresStyleStdprice1: styleInfo.stdprice1,
			total: qfRes.result.details[0].total,
			realnum: qfRes.result.details[0].num,
			mainDwxxNameshort: qfRes.result.show_dwid,
			recvnum: '0',
			mainProdate: qfRes.result.prodate,
			mainOpstaffName: qfRes.result.opname,
			mainInventoryName: qfRes.result.show_invid,
			propdresColorid: qfRes.result.details[0].show_colorid,
			mainShopName: qfRes.result.show_shopid,
			propdresSizeid: qfRes.result.details[0].show_sizeid,
			price: styleInfo.purprice,
			season: styleInfo.show_season,
			unitName: styleInfo.show_unit,
			propdresStyleBrandid: styleInfo.show_brandid,
			rem: qfRes.result.details[0].rem,
			propdresStyleName: styleInfo.name,
			mainStaffName: qfRes.result.show_deliver.split(',')[1], //e.g. 000,总经理
			mainBillno: sfRes.result.billno,
			// checkprenum: ,//差异数
			propdresStyleCode: styleInfo.code,
			mainOptime: qfRes.result.optime
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});


});
