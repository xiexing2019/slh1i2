'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam');
const printReqHandler = require('../../help/printHelp/printRequestHandler');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const wareHouseBind = require('../../../data/warehouseStoresBinding.js');
const mainDateReqHandler = require('../../help/basicInfoHelp/mainDataReqHandler');
describe('采购入库新增修改', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('120005,输入不存在的款号提示信息-slh2', async function () {
		// 不存在的款号保存
		let purchaseJson = basiceJsonparam.purchaseJson();
		purchaseJson.details[0].styleid = '0';
		let sfRes = await common.editBilling(purchaseJson, false);
		expect(sfRes.result, `输入不存在的款号开单成功`).to.have.property('error');
		expect(sfRes.result.error, `错误提示不符`).to.includes('款号在服务端不存在');
	});

	it('120033.新增入库单修改保存.rankA-slh2', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());
		sfRes.params.pk = sfRes.result.pk;
		sfRes.params.action = 'edit';
		sfRes.params.card += 100;
		sfRes.params.details[0] = {
			num: 3,
			sizeid: 'XL',
			matCode: 'Agc001',
			rem: '明细3',
			colorid: 'BaiSe',
		}; //删除/添加款号
		let sfResEdit = await common.editBilling(sfRes.params); //修改采购入库单
		let qfRes = await purReqHandler.purQueryBilling(sfResEdit.result.pk);
		common.isApproximatelyEqualAssert(sfResEdit.params, qfRes.result);
	});

	describe('是否允许修改客户或供应商-slh2', function () {
		let purInfo;
		before(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 0); //不允许修改客户或厂商
		})
		it('120046,将供应商修改从无到有.rankA', async function () {
			// 没有厂商开入库单
			let purchaseJson = format.jsonparamFormat(basiceJsonparam.purchaseJson());
			delete purchaseJson.dwid;
			let purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purchaseJson
			});
			expect(purResult, `采购入库失败,${JSON.stringify(purResult)}`).to.not.have.property('error');

			// 修改前面开的入库单--增加一个厂商
			let purBillInfo = await purReqHandler.purQueryBilling(purResult.pk);
			purBillInfo.result.action = 'edit';
			purBillInfo.result.dwid = BASICDATA.dwidVell;
			purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purBillInfo.result
			});
			expect(purResult, `采购入库失败,${JSON.stringify(purResult)}`).to.not.have.property('error');
			purInfo = {
				'params': purBillInfo.result,
				'result': purResult
			};
		});

		// 依赖120046
		it('120060,将供应商修改从有到无 和从A改到B.rankA', async function () {
			// 从A改到B
			let purBillInfo = await purReqHandler.purQueryBilling(purInfo.result.pk);
			purBillInfo.result.action = 'edit';
			purBillInfo.result.dwid = BASICDATA.dwidRt;
			let purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purBillInfo.result
			});
			expect(purResult, `采购入库修改成功,但是按照系统参数设定修改了供应商应该修改失败,${JSON.stringify(purResult)}`).to.includes({
				"error": "客户或供应商信息不允许修改"
			});

			// 从有改到无
			delete purBillInfo.result.dwid;
			purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purBillInfo.result
			});
			expect(purResult, `采购入库修改成功,但是按照系统参数设定修改了供应商应该修改失败,${JSON.stringify(purResult)}`).to.includes({
				"error": "客户或供应商信息不允许修改"
			});
		});
	});

	describe('挂单-slh2--mainLine', function () {
		let [json, isPend] = [{}, 1];
		before(async () => {
			json = basiceJsonparam.purchaseJson();
			json.invalidflag = 9; //挂单
		});
		it('120017.1.挂单明细/按挂单查询验证.rankA', async function () {
			let hangRes = await purReqHandler.editPurHangBill(json); //新增挂单
			let qfRes = await purReqHandler.purQueryBilling({
				pk: hangRes.result.pk,
				isPend
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);
			let param = await format.qlParamsFormat({
				shopid: hangRes.params.invid,
				dwid: hangRes.params.dwid,
			}, true);
			let qlRes = await common.callInterface('ql-14226', param); //采购入库-按挂单
			let exp = getHangListExp(qfRes.result);
			if (USEECINTERFACE == 2) exp.opname = _.last(exp.opname.split(','));
			//qfRes没有返回actualpay
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('120017.4.挂单作废功能验证.rankA', async function () {
			let hangRes = await purReqHandler.editPurHangBill(json); //新增挂单
			await purReqHandler.cancelPurchaseBill({
				pk: hangRes.result.pk,
				isPend: 1, //是否挂单 是
			}); //作废挂单

			let param = await format.qlParamsFormat({
				shopid: hangRes.params.invid,
				dwid: hangRes.params.dwid,
			}, true);
			let qlRes = await common.callInterface('ql-14226', param);
			qlRes.dataList.forEach((obj) => {
				expect(obj.id).not.to.equal(hangRes.result.pk);
			});
		});
		it('120017.5.重复挂单功能验证.rankA', async function () {
			let hangRes = await purReqHandler.editPurHangBill(json); //新增挂单
			hangRes.params.pk = hangRes.result.pk;
			hangRes.params.remark += 'a';
			hangRes = await purReqHandler.editPurHangBill(hangRes.params); //修改挂单

			let qfRes = await purReqHandler.purQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);
		});
		it('120058.挂单转正式采购入库单后打印.rankA', async function () {
			let hangRes = await purReqHandler.editPurHangBill(json); //新增挂单
			hangRes.params.details.push({
				num: 3,
				sizeid: 'XL',
				matCode: 'Agc001',
				rem: '明细3',
				colorid: 'BaiSe',
			}); //添加款号
			hangRes.params.pk = hangRes.result.pk;
			let sfRes = await purReqHandler.hangToPurBill(hangRes.params); //挂单转正式采购入库单
			if (USEECINTERFACE == 2) await common.delay(2500);//财务异步操作还没完成的时候可能会报 财务处理检查失败，请稍后重试 先增加延迟试试  小邱说先加延迟，后续会优化
			let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['pk']);

			let printRes = await printReqHandler.getPrint(sfRes); //打印
			expect(printRes, `打印出错`).to.includes({
				billno: sfRes.result.billno
			});
		});
		it('120018.采购入库的挂单加载后能正常修改保存.rankA', async function () {
			let hangRes = await purReqHandler.editPurHangBill(json); //新增挂单
			hangRes.params.dwid = BASICDATA.dwidRt;
			hangRes.params.card += 100;
			hangRes.params.details[0] = {
				num: 3,
				sizeid: 'XL',
				matCode: 'Agc001',
				rem: '明细3',
				colorid: 'BaiSe',
			}; //删除/添加款号
			hangRes.params.pk = hangRes.result.pk;
			let sfRes = await purReqHandler.hangToPurBill(hangRes.params); //挂单转正式采购入库单
			let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['pk']);
		});
		it('120188.按批次查不显示挂单数据', async function () {
			let param = format.qlParamsFormat({
				id1: '0',
				id2: '0',
				prodate1: common.getDateString([-1, 0, 0]),
				search_list: 0,
			}, false);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			expect(qlRes.count).to.equal('0');
		});
	});

	describe('120022.检查核销-收款金额必须不小于核销金额.rankA-slh2', async function () {
		let dwid;
		before(async () => {
			dwid = BASICDATA.dwidRt;
			await common.setGlobalParam('verifylimit', 1); //收款金额必须不小于核销金额 限制
		});
		after(async () => {
			await common.setGlobalParam('verifylimit', 0); //收款金额必须不小于核销金额 不限制
		});
		it('1.核销欠款单,不输入任何款号,然后点未付,保存', async function () {
			let sfRes = await addPurBill(dwid, 'debt'); //先做一张欠款单
			let json = {
				interfaceid: 'sf-14212-1',
				cash: 0,
				card: 0,
				remit: 0,
				details: [],
			};
			let jsonparam = await addPurJsonWithVerify({
				dwid: dwid,
				billno: sfRes.result.billno
			}, json);
			//console.log(`sfRes.result : ${JSON.stringify(sfRes.result)}`);
			sfRes = await common.editBilling(jsonparam, false);
			expect(sfRes.result).to.includes({
				error: '收款金额至少要大于等于核销金额!'
			});
		});
		it('2.核销欠款单,输入款号,然后点未付,保存', async function () {
			let sfRes = await addPurBill(dwid, 'debt');
			let json = {
				interfaceid: 'sf-14212-1',
				cash: 0,
				card: 0,
				remit: 0,
				details: [{
					num: 10,
					sizeid: 'M',
					matCode: 'Agc001',
					colorid: 'BaiSe',
				}],
			};
			let jsonparam = await addPurJsonWithVerify({
				dwid: dwid,
				billno: sfRes.result.billno
			}, json);
			sfRes = await common.editBilling(jsonparam, false);
			expect(sfRes.result).to.includes({
				error: '收款金额至少要大于等于核销金额!'
			});
		});
		it('3.核销余款,但让余款小于货品金额,然后点未付,保存', async function () {
			let sfRes = await addPurBill(dwid, 'spare');
			let json = {
				interfaceid: 'sf-14212-1',
				cash: 0,
				card: 0,
				remit: 0,
				details: [{
					num: sfRes.params.totalnum + 10,
					sizeid: 'M',
					matCode: 'Agc001',
					colorid: 'BaiSe',
				}],
			};
			let jsonparam = await addPurJsonWithVerify({
				dwid: dwid,
				billno: sfRes.result.billno
			}, json);
			await common.editBilling(jsonparam); //应该正常保存
		});
		it('4.核销余款,但让余款大于货品金额,然后保存,,保存', async function () {
			let sfRes = await addPurBill(dwid, 'spare');
			let json = {
				interfaceid: 'sf-14212-1',
				cash: 0,
				card: 0,
				remit: 0,
				details: [{
					num: sfRes.params.totalnum - 5,
					sizeid: 'M',
					matCode: 'Agc001',
					colorid: 'BaiSe',
				}],
			};
			let jsonparam = await addPurJsonWithVerify({
				dwid: dwid,
				billno: sfRes.result.billno
			}, json);
			await common.editBilling(jsonparam); //应该正常保存
		});
		it('5.核销欠款,输入抹零,点未付,保存', async function () {
			let sfRes = await addPurBill(dwid, 'debt');
			let json = {
				interfaceid: 'sf-14212-1',
				cash: 0,
				card: 0,
				remit: 0,
				details: [{
					num: -1,
					sizeid: '0',
					matCode: '00000', //抹零
					colorid: '0',
					price: 20
				}],
			};
			let jsonparam = await addPurJsonWithVerify({
				dwid: dwid,
				billno: sfRes.result.billno
			}, json);
			sfRes = await common.editBilling(jsonparam, false);
			expect(sfRes.result).to.includes({
				error: '收款金额至少要大于等于核销金额!'
			});
		});
	});

	describe('120132.跨门店核销-采购.rankA-slh2', function () {
		let dwid, qlParam, accountRes1, accountRes2;
		let cqdDebtRes, zzdDebtRes; //欠款单结果
		before(async () => {
			// await common.setGlobalParam('sales_verify_overshop', 1); //允许跨门店核销
			dwid = BASICDATA.dwidRt;
			qlParam = format.qlParamsFormat({
				dwxxid: dwid
			}, false);

			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});
			zzdDebtRes = await addPurBill(dwid, 'debt');
			await addPurBill(dwid, 'spare'); //余款单

			await common.loginDo();
			cqdDebtRes = await addPurBill(dwid, 'debt');
			await addPurBill(dwid, 'spare'); //余款单

			accountRes1 = await common.callInterface('ql-15003', qlParam);
		});
		it('6.核销其他门店数据,厂商门店账验证', async function () {
			let json = basiceJsonparam.purchaseJson();
			let jsonparam = await addPurJsonWithVerify({
				dwid: BASICDATA.dwidRt,
				invid: BASICDATA.shopidZzd,
				billno: zzdDebtRes.result.billno
			}, json);
			jsonparam.cash -= Number(json.finpayVerifysum); //核销欠款
			await common.editBilling(jsonparam);
			if (USEECINTERFACE == 2) await common.delay(1200);
			accountRes2 = await common.callInterface('ql-15003', qlParam);
			accountRes1.dataList.forEach((obj) => {
				if (obj.invname == '中洲店') obj.balance = common.sub(obj.balance, json.finpayVerifysum);
			});
			common.isApproximatelyArrayAssert(accountRes1.dataList, accountRes2.dataList);
		});
		//
		it('5.供应商的最终结余验证', async function () {
			TESTCASE = {
				describe: 'cs-getlastbalance与ql-15003的总账值需要一致',
				jira: 'SLHSEC-7522',
			};
			let param = {
				invid: BASICDATA.shopidCqd,
				dwid: dwid,
				billtype: 1, //1 是新增入库界面查看厂商结余，0是新增开单界面查看客户结余
			};
			let balanceRes = await common.callInterface('cs-getlastbalance', param);
			//厂商Rt 采购入库界面的结余值和厂商账款界面的汇总值不一致
			// console.log(`balanceRes.balance=${JSON.stringify(balanceRes.balance)}`);
			// console.log(`accountRes2.sumrow.balance=${JSON.stringify(accountRes2.sumrow.balance)}`);
			expect(balanceRes.balance, `常青店-供应商的最终结余错误`).to.equal(accountRes2.sumrow.balance);
			param.invid = BASICDATA.shopidZzd;
			balanceRes = await common.callInterface('cs-getlastbalance', param);
			expect(balanceRes.balance, `中洲店-供应商的最终结余错误`).to.equal(accountRes2.sumrow.balance);
		});
		it('8.厂商总账验证', async function () {
			let qlRes = await common.callInterface('ql-15007', format.qlParamsFormat({
				dwid
			}, false));
			expect(qlRes.sumrow.balance).to.equal(accountRes2.sumrow.balance);
		});
	});

	describe('修改日志-slh2', function () {
		it('120101.查看修改日志(修改记录)', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());
			let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
			qfRes.result.action = 'edit';
			qfRes.result.interfaceid = 'sf-14212-1';

			let sfResEdit = await common.editBilling(qfRes.result);
			let qfResEdit = await purReqHandler.purQueryBilling(sfRes.result.pk);
			let editLog = await purReqHandler.getPurBillDetails(sfRes.result.pk);
			let exp = {
				hashkey: USEECINTERFACE == 1 ? sfRes.params.hashkey : sfResEdit.params.hashkey,
				optime: qfRes.result.optime,
				verifybillBillno: '0',
				delopstaffName: '', //作废操作人
				// modelClass: 'DLInvMain',
				updateoptime: qfResEdit.result.optime,
				updateopstaffName: qfResEdit.result.opname,
				opname: qfRes.result.opname,
				pk: sfRes.result.pk
			};
			common.isApproximatelyEqualAssert(exp, editLog);
		});
		it('120102.查看修改日志(核销记录)', async function () {
			let sfRes = await addPurBill(BASICDATA.dwidVell, 'spare'); //
			let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
			let json = basiceJsonparam.purchaseJson();
			let jsonparam = await addPurJsonWithVerify({
				dwid: sfRes.params.dwid,
				billno: sfRes.result.billno
			}, json);

			let verifyRes = await common.editBilling(jsonparam);
			let qfResVerify = await purReqHandler.purQueryBilling(verifyRes.result.pk);
			let editLog = await purReqHandler.getPurBillDetails(sfRes.result.pk);

			let exp = {
				hashkey: sfRes.params.hashkey,
				optime: qfRes.result.optime,
				verifybillBillno: verifyRes.result.billno,
				verifytime: qfResVerify.result.optime,
				delopstaffName: '', //作废操作人
				// modelClass: 'DLInvMain',
				updateoptime: '',
				updateopstaffName: '',
				opname: qfRes.result.opname,
				pk: sfRes.result.pk
			};
			common.isApproximatelyEqualAssert(exp, editLog);
		});
	});

	describe('简单入库模式', function () {
		before(async () => {
			await common.loginDo();
			await common.setGlobalParam('purchaser_always_purprice_asaclist', 0); //采购员是否总是用采购价做选款aclist的价格显示  0默认不开启
			await common.setGlobalParam('purchase_type', 1); //采购入库模式，1简单入库模式
		});

		after(async () => {
			await common.setGlobalParam('purchase_type', 2); //采购入库模式，2复杂入库模式
			await common.setGlobalParam('purchaser_always_purprice_asaclist', 1);
		});

		it('120125,采购员登录A门店给B门店做入库单.rankA', async function () {

			let purJson = basiceJsonparam.purBypurchaserJson();
			let searchCondition = {
				'propdresColorid': purJson.details[0].colorid,
				'propdresSizeid': purJson.details[0].sizeid,
				'propdresStyleFlag': '0',
				'propdresStyleid': purJson.details[0].styleid,
			};

			searchCondition.invid = BASICDATA.shopidCqd;
			let invInfoCqd = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			expect(invInfoCqd, `查询当前库存出错:${JSON.stringify(invInfoCqd)}`).to.not.have.property('error');

			searchCondition.invid = BASICDATA.shopidZzd;
			let invInfoZzd = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			expect(invInfoZzd, `查询当前库存出错:${JSON.stringify(invInfoZzd)}`).to.not.have.property('error');

			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});
			purJson.deliver = LOGINDATA.id;
			purJson.shopid = purJson.invid = BASICDATA.shopidZzd;
			let purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purJson
			});
			expect(purResult, `简单入库模式下，采购员做入库失败了:${JSON.stringify(purResult)}`).to.not.have.property('error');

			await common.loginDo();
			searchCondition.invid = BASICDATA.shopidCqd;
			let invInfoCqd2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			expect(invInfoCqd2.dataList[0], `常青店当前库存数据错误`).to.includes({
				'invnum': invInfoCqd.dataList[0].invnum,
			});

			searchCondition.invid = BASICDATA.shopidZzd;
			let invInfoZzd2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfoZzd.dataList[0].invnum, purJson.details[0].num),
			}, invInfoZzd2.dataList[0]);


		});

		it('120156,异地仓库+门店绑定仓库.rankA', async function () {
			await common.setGlobalParam('sc_show_onlymyshop_bynogm', 0); //非总经理岗位是否显示自己所在门店：默认显示全部门店
			await common.setGlobalParam('pur_in_by_cross_store', 1); //采购入库->简单模式，是否支持跨门店：支持
			await common.setDataPrivilege('aa', '-1'); //后台记录权限设置为所有门店
			await common.setGlobalParam('scm_shop_inv_separate', 1); //支持异地仓库
			await wareHouseBind.bindingWarehouse(); //文一店绑定仓库店

			let purJson = basiceJsonparam.purBypurchaserJson();
			purJson.deliver = LOGINDATA.id;
			purJson.shopid = purJson.invid = BASICDATA.shopidZzd;
			let searchCondition = {
				'propdresColorid': purJson.details[0].colorid,
				'propdresSizeid': purJson.details[0].sizeid,
				'propdresStyleFlag': '0',
				'propdresStyleid': purJson.details[0].styleid,
			};

			// 入库前查询当前库存、厂商账款
			searchCondition.invid = BASICDATA.shopidCkd;
			let invInfoCkd = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			expect(invInfoCkd, `查询当前库存出错:${JSON.stringify(invInfoCkd)}`).to.not.have.property('error');
			let accountInfo = await common.callInterface('ql-15003', {
				'dwxxid': purJson.dwid,
				'invid': BASICDATA.shopidWyd,
			});
			expect

			//常青店采购员登录，给文一店做采购入库
			await common.loginDo({
				'logid': '003',
				'pass': '000000'
			});
			purJson.deliver = LOGINDATA.id;
			purJson.shopid = purJson.invid = BASICDATA.shopidWyd;
			let purResult = await common.callInterface('sf-14212-1', {
				'jsonparam': purJson
			});
			expect(purResult, `简单入库模式下，采购员做入库失败了:${JSON.stringify(purResult)}`).to.not.have.property('error');

			//入库后验证数据
			let batchList = await common.callInterface('ql-22301', format.qlParamsFormat({
				'id1': purResult.billno,
				'id2': purResult.billno,
				'shopid': BASICDATA.shopidWyd,
			}, true));
			expect(batchList.dataList[0], `入库单所属门店有误`).to.includes({
				'shopname': '文一店'
			});
			searchCondition.invid = BASICDATA.shopidCkd;
			let invInfoCkd2 = await common.callInterface('ql-1932', format.qlParamsFormat(searchCondition, false));
			common.isApproximatelyEqualAssert({
				'invnum': common.add(invInfoCkd.dataList[0].invnum, purJson.details[0].num),
			}, invInfoCkd2.dataList[0]);
			let accountInfo2 = await common.callInterface('ql-15003', {
				'dwxxid': purJson.dwid,
				'invid': BASICDATA.shopidWyd,
			});
			common.isApproximatelyEqualAssert({
				'balance': common.add(accountInfo.dataList[0].balance, batchList.dataList[0].balance),
			}, accountInfo2.dataList[0]);

		});
	});

	describe('历史拿货记录', function () {
		let sfRes, qfRes;
		before('新增采购单', async function () {
			await common.loginDo();
			const purchaseJson = basiceJsonparam.purchaseJson();
			purchaseJson.dwid = BASICDATA.dwidVell;    //用厂商  Vell做采购单  采购单两条明细款号是一样的。
			sfRes = await common.editBilling(purchaseJson);
			qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
		});

		it('查看历史记录', async function () {
			const qlRes = await common.callInterface('ql-150071', { dwid: sfRes.params.dwid, styleid: qfRes.result.details[0].styleid });
			//历史记录的前两条应该就是刚刚开采购单的这两条明细。
			common.isApproximatelyArrayAssert(getHistoryRecordExp(qfRes.result), qlRes.dataList.slice(0, 2));
		});

		it('没权限的角色查看', async function () {
			await common.loginDo({ logid: '004' });
			const qlRes = await common.callInterface('ql-150071', { dwid: sfRes.params.dwid, styleid: qfRes.result.details[0].styleid });
			//历史记录的前两条应该就是刚刚开采购单的这两条明细。
			common.isApproximatelyArrayAssert(getHistoryRecordExp(qfRes.result), qlRes.dataList.slice(0, 2));
			qlRes.dataList.forEach(val => {
				expect(val.hasOwnProperty('mat_money')).to.be.false;   //没进货价权限的查看是不返回这个字段的
			});
		});
	});

	describe('保存单据时验证退货数大于拿货数', function () {
		let saveCustRes, qfRes;
		before('参数准备,保存采购入库单', async function () {
			await common.loginDo();
			await common.setGlobalParam('pur_checkBackNum', 1);
			saveCustRes = await mainDateReqHandler.saveSupplier({ nameshort: `manu${common.getRandomStr(6)}` });
			let sfJson = basiceJsonparam.purchaseJson();
			sfJson.dwid = saveCustRes.result.val;
			const sfRes = await common.editBilling(sfJson);   //开采购入库单]
			qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);
		});

		after('系统参数还原', async function () {
			await common.setGlobalParam('pur_checkBackNum', 0);
		});

		it('退货数大于拿货数', async function () {
			let sfJson = basiceJsonparam.purchaseJson();
			sfJson.dwid = saveCustRes.result.val;
			sfJson.details.forEach(val => {
				val.num = -common.add(val.num, 1);
			});
			const sfRes = await common.editBilling(sfJson, false);   //false 内部不进行断言
			const errorMsg = `款号[${qfRes.result.details[0].show_styleid},${qfRes.result.details[0].show_colorid},${qfRes.result.details[0].show_sizeid}]采购总数${qfRes.result.details[0].num}件，退货总数${-sfRes.params.details[0].num}件，退货数大于拿货数，请重新修改数量`;
			expect(sfRes.result).to.includes({ "error": errorMsg });
		});

		it('退货数小于拿货数', async function () {
			let sfJson = basiceJsonparam.purchaseJson();
			sfJson.dwid = saveCustRes.result.val;
			sfJson.details.forEach(val => {
				val.num = -common.sub(val.num, 1);
			});
			await common.editBilling(sfJson);   //方法里面自带断言
		});
	});
});

/*
 * 拼接按挂单-查询结果期望值
 * ql-14226
 */
function getHangListExp(qfResult) {
	let mapfield = 'totalmoney;dwname=show_dwid;remark;remit;optime;opname;invname=show_invid;shopname=show_shopid;totalnum;prodate;id=pk;balance;finpayVerifysum;cash;card';
	let exp = format.dataFormat(qfResult, mapfield); //;
	exp.finpayPaysum = Number(qfResult.cash || 0) + Number(qfResult.card || 0) + Number(qfResult.remit || 0) + Number(qfResult.agency || 0);;
	exp.invalidflag = 9;
	exp.staffName = _.last(qfResult.show_deliver.split(',')); //e.g. 000,总经理
	exp.alipay = qfResult.finpayAlipay || 0;
	exp.agency = qfResult.agency || 0;
	exp.weixinpay = qfResult.finpayWeixinpay || 0;
	exp.billno = 0;
	return exp;
};

/**
 *	生成采购入库单
 *  @param dwid - 厂商id
 *  @param type - debt欠款 spare余款
 *  @return  {params,result}
 */
async function addPurBill(dwid, type) {
	let json = basiceJsonparam.purchaseJson();
	json.dwid = dwid;
	if (type == 'debt') [json.cash, json.card, json.remit] = [0, 0, 0]; //生成一个欠款单
	if (type == 'spare') json.cash += 1000; //生成一个余款单
	let sfRes = await common.editBilling(json);
	return sfRes;
};


/**
 * getVerifyInfo - 获取核销界面 指定核销信息
 * @description 二代单据pk != 核销单pk 得用billno进行筛选
 * @param {object} verifyResult 核销界面结果
 * @param {string} billno       单据批次号
 * @return {object} 核销信息
 */
function getVerifyInfo(verifyResult, billno) {
	let json = {};
	for (let i = 0, length = verifyResult.dataList.length; i < length; i++) {
		if (verifyResult.dataList[i].billno == billno) {
			json = verifyResult.dataList[i];
			break;
		};
	};
	return json;
};

//拼接带核销信息的jsonparam
async function addPurJsonWithVerify(verifyParam, json) {
	let verifyRes = await purReqHandler.getVerifyingBills(verifyParam.dwid, verifyParam.invid); //获取核销界面数据
	let verifyData = getVerifyInfo(verifyRes, verifyParam.billno);
	json.dwid = verifyParam.dwid;
	json.finpayVerifybillids = verifyData.id;
	json.finpayVerifysum = verifyData.balance;
	return json;
};

function getHistoryRecordExp(qfRes) {
	let exp = [];
	let mapfield = 'num;discount;mat_color=show_colorid;mat_size=show_sizeid;mat_code=stylecode;mat_name=stylename;price;mat_money=total;rem';
	qfRes.details.forEach(val => {
		let info = format.dataFormat(val, mapfield);
		info.seller = qfRes.opname;
		exp.push(info);
	});
	return exp;
};
