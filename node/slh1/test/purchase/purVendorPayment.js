'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');

/*
 * 开采购入库单后，当前库存、库存分布、款号库存以及收支流水的变化
 */
describe('采购入库--新增入库--厂商账款-slh2', function () {
	this.timeout(60000);
	this.retries(2);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('120029,厂商总账明细', function () {
		// 关于厂商总账列表账款数据的正确性120021、120023已覆盖
		// it('厂商总账查询', async function () {
		//
		// });
		let purJson;
		it('1.欠款', async function () {
			// 欠款
			purJson = basiceJson.purchaseJson();
			purJson.card = 0;
			purJson.cash = 0;
			purJson.remit = 0;
			await paymentDetailCheck(purJson);
		});
		it('2.不欠不余', async function () {
			// 不欠不余
			purJson = basiceJson.purchaseJson();
			await paymentDetailCheck(purJson);
		});
		it('3.余款', async function () {
			// 余款
			purJson = basiceJson.purchaseJson();
			purJson.card = Number(purJson.card) + 1000;
			await paymentDetailCheck(purJson);
		});
	});
	describe('120030,厂商门店帐', function () {
		// 关于厂商门店帐列表账款数据的正确性120021、120023已覆盖
		// it('厂商门店帐查询', async function () {
		//
		// });
		let purJson;
		it('欠款', async function () {
			// 欠款
			purJson = basiceJson.purchaseJson();
			purJson.card = 0;
			purJson.cash = 0;
			purJson.remit = 0;
			await paymentDetailCheck(purJson, LOGINDATA.invid);
		});
		it('不欠不余', async function () {
			// 不欠不余
			purJson = basiceJson.purchaseJson();
			await paymentDetailCheck(purJson, LOGINDATA.invid);
		});
		it('余款', async function () {
			// 余款
			purJson = basiceJson.purchaseJson();
			purJson.card = Number(purJson.card) + 1000;
			await paymentDetailCheck(purJson, LOGINDATA.invid);
		});
	});
});
/**
 * 提交采购入库单后验证厂商总账明细
 */
async function paymentDetailCheck(purJson, shopid = undefined) {
	let purResult = await common.editBilling(purJson);
	purJson = purResult.params;
	if (USEECINTERFACE == 2) await common.delay(5000); //finpayLastbalance没有更新
	// 采购入库后查询厂商总账明细
	let paymentDetail;
	if (shopid == undefined) {
		paymentDetail = await common.callInterface('ql-13471', format.qlParamsFormat({
			'dwid': purJson.dwid,
		}));
	} else {
		paymentDetail = await common.callInterface('ql-13471', format.qlParamsFormat({
			'dwid': purJson.dwid,
			'shopid': shopid,
		}));
	}
	let actualpay = Number(purJson.card || 0) + Number(purJson.remit || 0) + Number(purJson.cash || 0) + Number(purJson.weixinpay || 0) + Number(purJson.alipay || 0);

	expect(paymentDetail.dataList[0], `厂商总账明细结果数据错误`).to.includes({
		'totalmoney': purJson.totalmoney.toString(),
		'remark': purJson.remark,
		'typename': USEECINTERFACE == 1 ? '进货单' : '采购进货',
		'finpayPaysum': actualpay.toString(),
		'billno': purResult.result.billno,
		'finpayLastbalance': ((actualpay - Number(purJson.totalmoney)) + Number(paymentDetail.dataList[1].finpayLastbalance)).toString(),
	});
}
