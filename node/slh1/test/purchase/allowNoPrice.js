'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesRequestHandler = require('../../help/salesHelp/salesRequestHandler.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/**
 * 销售允许单价为0的退货和开单
 * 0，默认退货和销售价格不能为0
 * 1，退货和销售价格允许为0
 */

describe('销售允许单价为0的退货和开单-slh2', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('120110,允许价格为0(货品本来价格不为零)', function () {

		before(async () => {
			await common.setGlobalParam('sales_allownoprice', 1);
		});

		after(async () => {
			await common.setGlobalParam('sales_allownoprice', 0);
		});

		it('验证价格为零进行各种类型开单', async function () {
			// 采购
			let purJson = noPriceSales(basiceJson.purchaseJson()); //采购入库
			await common.editBilling(purJson);

			let purOrderJson = noPriceSales(basiceJson.purchaseOrderJson()); //采购订货
			let purOrderResult = await common.editBilling(purOrderJson);

			let purOrderInfo = await purRequestHandler.purOrderQueryBilling(purOrderResult.result.pk);
			let purByOrderJson = getPurJsonByOrderInfo(purOrderInfo.result); //按订货入库
			await common.editBilling(purByOrderJson);

			let bulkJson = noPriceSales(basiceJson.bulkPurchaseSameMatCS()); //批量入库
			await common.editBilling(bulkJson);

			// 销售
			let salesJson = noPriceSales(basiceJson.salesJson()); //销售开单
			await common.editBilling(salesJson);

			let salesOrderJson = noPriceSales(basiceJson.salesOrderJson()); //销售订货
			let salesOrderResult = await common.editBilling(salesOrderJson);

			let orderInfo = await salesRequestHandler.salesOrderQueryBilling(salesOrderResult.result.pk); //按订货入库
			let salesByOrderJson = getSalesJsonBySalesOrderInfo(orderInfo.result); //按订货开单
			await common.editBilling(salesByOrderJson);

			// 调拨
			let outJson = noPriceSales(basiceJson.outJson());
			await common.editBilling(outJson);
		});

	});

	describe('120109,允许价格为0(货品本来价格为零)', function () {
		let styleid;
		before(async () => {
			await common.setGlobalParam('sales_allownoprice', 1);

			let goodJson = basiceJson.addGoodJson();
			goodJson.purprice = goodJson.stdprice1 = goodJson.stdprice2 = goodJson.stdprice3 = goodJson.stdprice4 = 0;
			// let addGoodResult = await common.callInterface('sf-1511', {
			// 	'jsonparam': goodJson
			// });
			let addGoodRes = await basicReqHandler.editStyle({
				jsonparam: goodJson
			})
			styleid = addGoodRes.pk;
		});

		after(async () => {
			await common.setGlobalParam('sales_allownoprice', 0);
		});

		it('验证价格为零进行各种类型开单', async function () {
			// 采购
			let purJson = noPriceSales(basiceJson.purchaseJson(), styleid); //采购入库
			await common.editBilling(purJson);

			let purOrderJson = noPriceSales(basiceJson.purchaseOrderJson(), styleid); //采购订货
			let purOrderResult = await common.editBilling(purOrderJson);

			let purOrderInfo = await purRequestHandler.purOrderQueryBilling(purOrderResult.result.pk);
			let purByOrderJson = getPurJsonByOrderInfo(purOrderInfo.result); //按订货入库
			await common.editBilling(purByOrderJson);

			let bulkJson = noPriceSales(basiceJson.bulkPurchaseSameMatCS(), styleid); //批量入库
			await common.editBilling(bulkJson);

			// 销售
			let salesJson = noPriceSales(basiceJson.salesJson(), styleid); //销售开单
			await common.editBilling(salesJson);

			let salesOrderJson = noPriceSales(basiceJson.salesOrderJson(), styleid); //销售订货
			let salesOrderResult = await common.editBilling(salesOrderJson);

			let orderInfo = await salesRequestHandler.salesOrderQueryBilling(salesOrderResult.result.pk); //按订货入库
			let salesByOrderJson = getSalesJsonBySalesOrderInfo(orderInfo.result); //按订货开单
			await common.editBilling(salesByOrderJson);

			// 调拨
			let outJson = noPriceSales(basiceJson.outJson(), styleid);
			await common.editBilling(outJson);
		});
	});
});


/**
 * 去掉jsonparam中的价格,如果传入了新的styleid则将styleid也替换掉
 */
function noPriceSales(jsonparam, styleid = undefined) {
	for (let i = 0; i < jsonparam.details.length; i++) {
		jsonparam.details[i].price = 0;
		if (styleid != undefined) jsonparam.details[i].styleid = styleid;
	}
	jsonparam.card = 0;
	jsonparam.cash = 0;
	jsonparam.remit = 0;

	return jsonparam;
}

/**
 * 按订货入库jsonparam
 */
function getPurJsonByOrderInfo(purOrderInfo) {
	let json = {
		interfaceid: 'sf-14212-1',
		billid: purOrderInfo.pk,
		deliver: purOrderInfo.respopid,
		remark: '按订货入库'
	};
	delete purOrderInfo.pk;
	delete purOrderInfo.respopid;
	delete purOrderInfo.shouldpay;
	delete purOrderInfo.actualpay; //开单后会更新
	purOrderInfo = noPriceSales(purOrderInfo); //去掉价格
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(purOrderInfo), json));
	purOrderInfo.details.map((obj, index) => {
		jsonparam.details[index].billid = obj.pk || '';
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});

	return jsonparam;
}

/**
 * 按订货开单jsonparam
 */
function getSalesJsonBySalesOrderInfo(salesOrderInfo) {
	let json = {
		interfaceid: 'sf-14211-1',
		srcType: 2,
		billid: salesOrderInfo.pk,
		dwid: salesOrderInfo.clientid,
		deliverid: salesOrderInfo.sellerid,
		cash: 0,
		card: 0,
		remit: 0,
		remark: '按订货开单'
	};
	delete salesOrderInfo.pk;
	delete salesOrderInfo.clientid;
	delete salesOrderInfo.sellerid;
	salesOrderInfo = noPriceSales(salesOrderInfo); //去掉价格
	let jsonparam = format.jsonparamFormat(common.mixObject(_.cloneDeep(salesOrderInfo), json));
	salesOrderInfo.details.map((obj, index) => {
		jsonparam.details[index].billid = obj.pk;
		delete jsonparam.details[index].pk;
		delete jsonparam.details[index].show_styleid;
		delete jsonparam.details[index].invnum;
	});

	return jsonparam;
}
