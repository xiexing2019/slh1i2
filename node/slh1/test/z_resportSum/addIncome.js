'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const resportSumType = require('../../help/resportSumHelp/resportSumType.js');
const resportSumRequestHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
const reqHandler = require('../../help/reqHandlerHelp');
//const resSumReqHandler = require('../../../help/resportSumHelp/resportSumRequestHandler');
const basiceJsonparam = require('../../help/basiceJsonparam.js');

//收支表底部汇总值需要特殊处理一下，逻辑是收入-支出，不算作废
//统计分析-收支表相关
// 3、第1步完成以后后，需要每条it再重新log下，确认写接口返回的数据和期望值是否一样
// 如有必要，重新拼接期望值，可以参考星欣写的拼接期望值的方法
// 4、拼接期望值可以用sf的params，不能拘泥于qf的result
//店长权限和总经理登录，底部汇总也没搞
//
// 要把关于收支的用例全都转化掉

describe('统计分析-收支表-slh2', function () {
	this.timeout(30000);
	let typeCatid, typePk, accountInTypeId, accountOutTypeId, accountInfo;

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		// 获取门店账户信息
		accountInfo = await resportSumType.getAccountInfo(LOGINDATA.invid);
		// 收支类别
		let accountType = await common.callInterface('ql-1660', format.qlParamsFormat({}));
		//console.log(`accountType.dataList[0] : ${JSON.stringify(accountType)}`);
		for (let i = 0; i < accountType.dataList.length; i++) {
			if (accountInTypeId && accountOutTypeId) break;
			if ((!accountInTypeId) && accountType.dataList[i].inouttype === "1") {
				accountInTypeId = USEECINTERFACE == 1 ? accountType.dataList[i].id : accountType.dataList[i].sid;
				continue;
			} else if ((!accountOutTypeId) && accountType.dataList[i].inouttype === "2") {
				accountOutTypeId = USEECINTERFACE == 1 ? accountType.dataList[i].id : accountType.dataList[i].sid;
				continue;
			};
		};
	});

	// 1:收入 2:支出
	for (let i = 1; i < 3; i++) {
		describe('收入和支出.rankA', function () {
			it('190012.3新增收入支出类别', async function () {
				let addResult = await resportSumType.addOrEditType({
					action: 'add',
					inouttype: i
				});
				typeCatid = USEECINTERFACE == 1 ? addResult.result.val : addResult.result.sid;
				typePk = addResult.result.val;
				let qfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-1660',
					pk: typePk,
				});
				TESTCASE.expect = `qf-1660收支类别信息应与新增时相同`;
				common.isApproximatelyEqualAssert(addResult.params, qfRes.result);
				//收入支出类别查询：
				let qlRes = await common.callInterface('ql-1660', format.qlParamsFormat({})); //  收入支出类别没有查询条件

				// 新增qfRes两个属性id和inouttypename
				qfRes.id = qfRes.result.pk;
				qfRes.inouttypename = qfRes.result.inouttype == '1' ? '收入' : '支出';
				let exp = common.isArrayContainObject(qlRes.dataList, qfRes.result); //判断接口返回的数组中是否包含
				//console.log(`exp : ${JSON.stringify(exp)}`);
				TESTCASE.expect = `ql-1660收入支出类别列表中应能找到新增类别`;
				expect(exp, `ql-1660收入支出类别列表中未找到新增类别${JSON.stringify(addResult.result)}`).to.equal(true);

			});
			it('190012.2类别名称重复', async function () {
				let qfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-1660',
					pk: typePk,
					// inouttype: i,
				});
				//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
				let sameResult = await resportSumType.addOrEditType({
					name: qfRes.result.name,
					inouttype: i,
				})
				//console.log(`sameResult : ${JSON.stringify(sameResult)}`);
				if (USEECINTERFACE == 1) {
					expect(sameResult.result).to.includes({
						error: '相同记录已存在',
					})
				} else {
					expect(sameResult.result).to.includes({
						error: `${qfRes.result.name}已存在`,
					})
				}

			});
			it('190028.修改收入支出类别', async function () {
				let qfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-1660',
					pk: typePk,
				}); //查询要修改的记录
				//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
				let afterEdit = await resportSumType.addOrEditType({
					inouttype: qfRes.result.inouttype,
					pk: typePk,
					name: qfRes.result.name,
				}); //修改名称 params type pk name
				//console.log(`afterEdit.params : ${JSON.stringify(afterEdit.params)}`);
				let qfResAfter = await reqHandler.queryBilling({
					interfaceid: 'qf-1660',
					pk: typePk,
				}); //param1
				//console.log(`qfResAfter : ${JSON.stringify(qfResAfter)}`);
				common.isApproximatelyEqual(afterEdit.params, qfResAfter.result); //result type pk不变 name变了
			});

			it('190013.新增收支表', async function () {
				let param;
				if (i === 1) {
					param = basiceJsonparam.incomeJson();
					//console.log(`param : ${JSON.stringify(param)}`);
					param.details[1].catid = accountInTypeId;
				} else {
					param = basiceJsonparam.outcomeJson();
					param.details[1].catid = accountOutTypeId;
				}
				param.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param); //返回的params是传参，result是接口返回pk；
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk); //返回的params值是action和pk，result是接口返回的数据
				common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
			});

			it('190002.收支表日期+类别查询', async function () {
				let param1;
				if (i === 1) {
					param1 = basiceJsonparam.incomeJson();
					param1.details[1].catid = accountInTypeId;
				} else {
					param1 = basiceJsonparam.outcomeJson();
					param1.details[1].catid = accountOutTypeId;
				}
				param1.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param1); //新增单据
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk); //查询详情

				//await common.delay(500);
				let param = format.qlParamsFormat({
					prodate1: qfRes.result.prodate,
					prodate2: qfRes.result.prodate,
					inouttype: qfRes.result.inouttype,
					billid: USEECINTERFACE == 1 ? qfRes.result.billid : sfRes.result.billno,
					invid: LOGINDATA.invid, //门店id
					rem: qfRes.result.rem,
				});
				let qlRes = await common.callInterface('ql-1354', param); //查询ql接口的结果

				//以下都是为了拼接期望值
				let accountname, accountnameshort; //查账户名称
				for (let i = 0; i < accountInfo.dataList.length; i++) {
					if (accountInfo.dataList[i].id == qfRes.result.accountid) {
						accountname = accountInfo.dataList[i].fullname;
						accountnameshort = accountInfo.dataList[i].name;
						break;
					}
				};
				let inouttypename = i == 1 ? "收入" : "支出";
				let exp = {
					optime: sfRes.params.hashkey, //当前时间
					totalmoney: qfRes.result.totalsum,
					accountname: accountname, //账户名称
					invname: LOGINDATA.invname, //门店名称
					remark: qfRes.result.rem,
					inouttype: qfRes.result.inouttype,
					prodate: qfRes.result.prodate,
					opname: LOGINDATA.name,
					id: qfRes.result.pk,
					accountnameshort: accountnameshort,
					invalidflag: 0,
					inouttypename: inouttypename,
					billno: USEECINTERFACE == 1 ? qfRes.result.billid : sfRes.result.billno,
					staffname: LOGINDATA.name,
				};
				// console.log(`exp : ${JSON.stringify(exp)}`);
				// console.log(`qlRes.dataList[0] : ${JSON.stringify(qlRes.dataList[0])}`);
				common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]); //error

			});

			it('190005.收入单作废', async function () {
				let param1;
				if (i === 1) {
					param1 = basiceJsonparam.incomeJson();
					param1.details[1].catid = accountInTypeId;
				} else {
					param1 = basiceJsonparam.outcomeJson();
					param1.details[1].catid = accountOutTypeId;
				}
				param1.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param1); //新增单据
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk); //qf没有返回invalidflag
				let res = await resportSumType.cancelFinMain(pk); //作废刚刚新增的单据
				await common.delay(500);
				let qlRes = await common.callInterface('ql-1354', format.qlParamsFormat({
					billid: USEECINTERFACE == 1 ? qfRes.result.billid : sfRes.result.billno,
				}));
				//qf没有返回invalidflag
				//判断qlRes中的invalidflag是否变为1，
				expect(qlRes.dataList[0].invalidflag).to.be.equal('1');
			})

			it('190009.收支汇总查询', async function () {
				//开单前查一下，然后开单，开单后再查一下，检查开单后金额=开单前金额+开单金额
				let qlParam = format.qlParamsFormat({
					inouttype: i,
					invid: LOGINDATA.invid,
				}, true);
				let beforeAdd = await common.callInterface('ql-1360', qlParam);
				//console.log(`beforeAdd : ${JSON.stringify(beforeAdd)}`);

				let param;
				if (i === 1) {
					param = basiceJsonparam.incomeJson();
					param.details[1].catid = accountInTypeId;
				} else {
					param = basiceJsonparam.outcomeJson();
					param.details[1].catid = accountOutTypeId;
				}
				param.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param);
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk);
				//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
				let exp = _.cloneDeep(beforeAdd.dataList); //不能用let exp=beforeAdd,这是值传递，exp修改了，beforeAdd的值也会被修改
				for (let j = 0; j < qfRes.result.details.length; j++) {
					for (let k = 0; k < beforeAdd.dataList.length; k++) {
						if (qfRes.result.details[j].catid == beforeAdd.dataList[k].id) {
							exp[k].money = common.add(qfRes.result.details[j].money, beforeAdd.dataList[k].money);
							break;
						};
						if (k == (beforeAdd.dataList.length - 1)) {
							let param1 = {
								accountcate: qfRes.result.details[j].show_catid,
								id: qfRes.result.details[j].catid,
								money: qfRes.result.details[j].money,
								inouttype: qfRes.result.inouttype,
							};
							exp.push(param1);
						}
					}
				}
				//console.log(`exp : ${JSON.stringify(exp)}`);
				let qlResSum = await common.callInterface('ql-1360', qlParam);

				//console.log(`qlR : ${JSON.stringify(qlResSum.dataList)}`);
				common.isApproximatelyArrayAssert(exp, qlResSum.dataList);
			})

			it('190010.收支汇总详细', async function () {
				let param;
				if (i === 1) {
					param = basiceJsonparam.incomeJson();
					param.details[1].catid = accountInTypeId;
				} else {
					param = basiceJsonparam.outcomeJson();
					param.details[1].catid = accountOutTypeId;
				}
				param.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param);
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk);
				// 获取门店账户信息
				let accountname; //查账户名称
				for (let i = 0; i < accountInfo.dataList.length; i++) {
					if (accountInfo.dataList[i].id == qfRes.result.accountid) {
						accountname = accountInfo.dataList[i].fullname;
						break;
					}
				}
				//	console.log(`accountname : ${JSON.stringify(accountname)}`);
				let qlSumParam = format.qlParamsFormat({
					inouttype: i,
					invid: LOGINDATA.invid,
				}, true);
				let qlResSum = await common.callInterface('ql-1360', qlSumParam); //查询收支汇总页面
				for (let i = 0; i < qfRes.result.details.length; i++) {
					let qlParam = format.qlParamsFormat({
						prodate1: qfRes.result.prodate,
						prodate2: qfRes.result.prodate,
						inouttype: qfRes.result.inouttype,
						catid: qfRes.result.details[i].catid,
						invid: qfRes.result.invid,
					});
					let qlRes = await common.callInterface('ql-13601', qlParam); //查询该类别在收支汇总-详细页面的数据
					//	console.log(`qlRes : ${JSON.stringify(qlRes)}`);
					let exp = {
						staff: LOGINDATA.name,
						accountname: accountname,
						money: qfRes.result.details[i].money,
						prodate: qfRes.result.prodate,
					};
					common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
					//console.log(`exp : ${JSON.stringify(exp)}`);
					//console.log(`qlResSum : ${JSON.stringify(qlRes.dataList[0])}`);
					let sum = 0;
					for (let j = 0; j < qlResSum.dataList.length; j++) {
						if (qlResSum.dataList[j].id == qfRes.result.details[i].catid) {
							//console.log(`qlResSum.dataList[j].id : ${JSON.stringify(qlResSum.dataList[j].id)}`);
							//console.log(`qfRes.result.details[i].catid : ${JSON.stringify(qfRes.result.details[i].catid)}`);
							sum = Number(qlResSum.dataList[j].money);
						}
					}
					//console.log(`sum : ${JSON.stringify(sum)}`);
					let expSum = 0;
					for (let k = 0; k < qlRes.dataList.length; k++) {
						expSum = common.add(expSum, qlRes.dataList[k].money);
					}
					//console.log(`expSum : ${JSON.stringify(expSum)}`);
					expect(expSum, '收支汇总页面金额和收支汇总详细页面总额不一致').to.be.equal(sum);
				}
			})
			it('190022.收支流水', async function () {
				let param;
				if (i === 1) {
					param = basiceJsonparam.incomeJson();
					param.details[1].catid = accountInTypeId;
				} else {
					param = basiceJsonparam.outcomeJson();
					param.details[1].catid = accountOutTypeId;
				}
				param.details[0].catid = typeCatid;
				let sfRes = await common.editBilling(param);
				let pk = USEECINTERFACE == 1 ? sfRes.result.val : sfRes.result.pk;
				let qfRes = await resportSumType.inComeCheckQuery(pk);
				//console.log(`sfRes : ${JSON.stringify(qfRes)}`);
				qfRes.result.interfaceid = 'qf-1355-1';
				let qlParams = format.qlParamsFormat({
					accountInvid: LOGINDATA.invid,
				}, true);
				//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
				//console.log(`qfRes : ${JSON.stringify(qfRes)}`);
				qfRes.result.optime = sfRes.params.hashkey;
				let qlRes = await common.callInterface('ql-1352', qlParams);

				//console.log(`qlRes.dataList[0] : ${JSON.stringify(qlRes.dataList[0])}`);
				//	console.log(`LOGINDATA : ${JSON.stringify(LOGINDATA)}`);
				let accountname; //查账户名称
				for (let i = 0; i < accountInfo.dataList.length; i++) {
					if (accountInfo.dataList[i].id == qfRes.result.accountid) {
						accountname = accountInfo.dataList[i].fullname;
						break;
					}
				};
				let dataListExp = {
					optime: qfRes.result.optime,
					prodate: qfRes.result.prodate,
					opstaffName: qfRes.result.opname,
					billno: sfRes.result.billid,
					accountInvName: LOGINDATA.invname,
					staffName: qfRes.result.show_deliver.split(',')[1],
					rem: qfRes.result.rem,
					opstaffName: LOGINDATA.name,
					accountid: qfRes.result.accountid,
					accountName: accountname,
				};
				let plusMinus = 1;
				if (qfRes.result.inouttype == 1) {
					dataListExp.billtype = "收入单";
				} else {
					dataListExp.billtype = "支出单";
					plusMinus = -1;
				}
				dataListExp.money = Number(qfRes.result.totalsum) * plusMinus;
				// console.log(`dataListExp : ${JSON.stringify(dataListExp)}`);
				// console.log(`qlRes : ${JSON.stringify(qlRes.dataList[0])}`);
				common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList[0]);
			})
		})
	}

	//常青店店长登录，验证是否可以查到其他门店数据
	// 收支汇总页面查不到其他门店数据
	// 1、用门店查询条件去查，查不到
	// 2、没有输入门店查询条件，也查不到
	// 收支汇总详细页面中查出的数据必须是本门店的数据 门店id
	//
	describe('权限检查', function () {
		before(async () => {
			await common.loginDo({
				logid: '004',
				pass: '000000',
			});
		});
		after(async () => {
			await common.loginDo();
		});

		it('190140.收支表权限检查', async function () {
			//收支表页面店长查不到其他门店的数据
			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd, //中洲门店id，
			});
			let qlRes = await common.callInterface('ql-1354', param); //收支表页面
			//console.log(`qlRes : ${JSON.stringify(qlRes)}`);
			expect(qlRes.count, `店长权限,收支表显示其他门店数据`).to.equal('0');

			param.invid = ''; //没有选择门店，
			qlRes = await common.callInterface('ql-1354', param);
			qlRes.dataList.map((obj) => { //循环判断是否属于当前门店
				expect(obj.invname, `店长权限,收支表显示其他门店数据`).to.equal(LOGINDATA.invname);
			})
		})
		it('190142.收支汇总和详细页面-权限检查', async function () {
			//收支汇总页面，店长登录，查询条件只有本门店
			//查询条件为空  查询条件为当前门店，查出来的数据都是本门店的数据
			//
			let param = format.qlParamsFormat({}, true);
			let qlResSum1 = await common.callInterface('ql-1360', param); //门店查询条件为空
			param.invid = LOGINDATA.invid;
			let qlResTable = await common.callInterface('ql-1354', param);
			assert.equal(Number(qlResSum1.sumrow.money), qlResTable.sumrow.totalmoney, `店长权限，收支汇总显示其他门店数据`);
			let qlResSum2 = await common.callInterface('ql-1360', param); //门店查询条件为当前登录门店
			assert.equal(Number(qlResSum2.sumrow.money), qlResTable.sumrow.totalmoney, `店长权限，收支汇总显示其他门店数据`);
			let qlResDetal = await common.callInterface('ql-13601', param); //收支汇总详细
			let accountname = [];
			for (let i = 0; i < accountInfo.dataList.length; i++) {
				accountname.push(accountInfo.dataList[i].fullname);
			}; //获取本门店账户名称
			for (let i = 0; i < qlResDetal.dataList.length; i++) {
				expect(accountname, `收支汇总明细显示其他门店数据`).to.includes(qlResDetal.dataList[i].accountname);
			} //断言收支汇总详细页面中的账号名称是否包含在本门店账户名称中
		});
	})
})
