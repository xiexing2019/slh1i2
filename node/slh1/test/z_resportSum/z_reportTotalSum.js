'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const basiceJsonparam = require('../../help/basiceJsonparam');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');

//本用例集基本不做开单操作,统计前面跑的用例的数据进行汇总验证,因此需要最后跑
describe('综合汇总-slh2', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	//数值准确性在后面的用例里验证
	it('190030.查询', async function () {
		let qlRes = await common.callInterface('ql-1623', format.qlParamsFormat({
			invid: LOGINDATA.invid
		}, true));
		expect(qlRes.count).to.equal('1'); //查询结果唯一
		expect(qlRes.dataList[0]).to.includes({
			invname: LOGINDATA.invname,
		});
	});

	//欠款,还款,余款,抵扣在190047~190067新综合汇总接口中已验证
	describe('190035.检查汇总各项数值正确性', function () {
		let defParam, qlRes = {};
		before(async () => {
			defParam = {
				invid: LOGINDATA.invid,
				shopid: LOGINDATA.invid,
				today: true,
				search_list: 0, //只取汇总值
			};
			let params = {
				'ql-1623': defParam, //统计分析-综合汇总
				'ql-22301': defParam, //采购入库-按批次查
				'ql-142201': defParam, //销售开单-按批次查
				'ql-14489': defParam, //销售开单-按退货汇总
				'ql-14451': defParam, //销售开单-按店员汇总
				'ql-1442': defParam, //销售开单-代收收款单
			};
			qlRes = await common.getResults(params);
		});
		// 1.进数：=采购入库数+采购退货数(退货数为负值). 对比值:采购入库-按批次查-总数汇总
		it('1.进数', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-22301'].sumrow, 'purinnum=totalnum');
		});
		// 2.销数: =只考虑销售开单卖出去的数 对比:销售开单-按汇总-按店员汇总-销售数这一列的汇总值
		it('2.销数', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-14451'].sumrow, 'num', `由该问题导致jira:SLHSEC-6768`);
		});
		// 3.销额: = 实销额+退额-特殊货品(这里只能反着算,没有地方可以直接比对.特殊货品的值为各种特殊货品累加在一起,退额表示退货的金额,是正值)
		it('3.销额', async function () {
			expect(Number(qlRes['ql-1623'].sumrow.totalmoney))
				.to.equal(Number(qlRes['ql-1623'].sumrow.actualsalesmoney) + Number(qlRes['ql-1623'].sumrow.backmoney) - Number(qlRes['ql-1623'].sumrow.specmoney));
		});
		// 4.退数:销售开单退货数,对比值:销售开单-按汇总-按退货汇总-数量这一列的汇总值
		it('4.退数', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-14489'].sumrow, 'backnum=num');
		});
		// 5.退额:销售开单退货额,对比值:销售开单-按汇总-按退货汇总-小计这一列的汇总值.综合汇总里进行了四舍五入处理
		it('5.退额', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-14489'].sumrow, 'backmoney=totalreturn');
		});
		// 6.特殊货品: 各种类型的特殊货品值的累加. 对比值:销售开单-按明细查-款号名称用0000查询--小计这一栏的汇总值
		it('6.特殊货品', async function () {
			TESTCASE = {
				describe: '验证 销售开单-按明细查,各种特殊货品值的累加 等于 统计分析-综合汇总的特殊货品的值',
				expect: 'ql-1209特殊货品的mat_money的累加值 = ql-1623的specmoney',
				jira: "SLHSEC-3701,销售开单-按明细查-款号查询，输入特殊货品款号，查询失效导致",
			};
			let specList = BASICDATA.specGoodsList; //特殊货品列表
			let param = format.qlParamsFormat(defParam, true);
			let promises = specList.map((obj) => {
				if (USEECINTERFACE == 1 || obj.flag == 1) { //flag 1代 1:停用,2:启用,  二代 1:启用 待后续再确认
					param.styleid = obj.id;
					return common.callInterface('ql-1209', param); //销售开单-按明细查
				};
			}); //销售开单-按明细查 获取特殊货品汇总值
			let res = await Promise.all(promises);
			let specMoney = 0;
			for (let i = 0; i < res.length; i++) {
				if (Object.keys(res[i].sumrow).length != 0)
					specMoney = common.add(specMoney, res[i].sumrow.mat_money);
			};
			assert.equal(specMoney, qlRes['ql-1623'].sumrow.specmoney);
		});
		// 7.实销数:同时考虑销售开单卖出的数和退回来的数.实销数=销售数-退货数(这里退货数按正值算),即实际卖了10件,退了2件,实销数=10-2.
		// 对比值:销售开单--按汇总-按店员汇总-实销数这一列的汇总值
		it('7.实销数', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-14451'].sumrow, 'actualnum', `由该问题导致jira:SLHSEC-6768`);
		});
		// 8.实销额:  销售开单-按批次查-金额这一列的汇总值.它等于实际销售出去的钱减去退货的钱再加上特殊货品(这里的特殊货品=抹零+打包费+免单+打折+....,反正是各种特殊货品数值的累加,像抹零为负值,打包为正值,就直接-100+200这样加)
		// 对比值:销售开单-按汇总-按店员汇总-实销额这一列的汇总值
		// 对比值:销售开单-按汇总-按客户销售-实销额这一列的汇总值
		it('8.实销额', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-14451'].sumrow, 'actualsalesmoney=totalmoney', `由该问题导致jira:SLHSEC-6768`);
		});
		// 9.现金:销售开单收到的现金的总和,对比值:销售开单-按批次查-现金这一列的汇总值
		it('9.现金', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-142201'].sumrow, 'cash');
		});
		// 10.刷卡:销售开单收到的刷卡的总和,对比值:销售开单-按批次查-刷卡这一列的汇总值
		it('10.刷卡', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-142201'].sumrow, 'card');
		});
		// 11.汇款:销售开单收到的汇款的总和,对比值:销售开单-按批次查-汇款这一列的汇总值
		it('11.汇款', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-142201'].sumrow, 'remit');
		});
		// 12.代收: 通过物流商代收方式产生的金额,对比值:销售开单-按批次查-代收这一列的汇总值
		it('12.代收', async function () {
			common.isFieldsEqualAssert(qlRes['ql-1623'].sumrow, qlRes['ql-142201'].sumrow, 'daishou;daishou=distribretsum');
		});
		// 17.代收收款:物流代收核销时现金+刷卡+汇款,对比值:销售开单-更多-代收收款查询 页面金额这一列的汇总值
		it('17.代收收款', async function () {
			TESTCASE = {
				describe: '验证 销售开单-更多-代收收款查询,金额的汇总值 等于 统计分析-综合汇总的代收收款的值',
				expect: 'ql-1442.sumrow.totalmoney = ql-1623.sumrow.collection',
			};
			if (qlRes['ql-1442'].count == 0) qlRes['ql-1442'].sumrow.totalmoney = 0;
			common.isFieldsEqualAssert(qlRes['ql-1442'].sumrow, qlRes['ql-1623'].sumrow, 'totalmoney=collection');
		});
	});

	describe('综合汇总详细界面', function () {
		let qlRes = {};
		before(async () => {
			let params = {
				'ql-16235': {
					shopid: LOGINDATA.invid,
					prodate: common.getCurrentDate(),
				}, //统计分析-综合汇总详细见界面统计
				'cs-getAccountData': {
					invid: LOGINDATA.invid,
				}, //账户信息
				'ql-1352': {
					accountInvid: LOGINDATA.invid,
					today: true,
				}, //统计分析-收支流水
				'ql-1360': {
					invid: LOGINDATA.invid,
					today: true,
				}, //统计分析-收支汇总
			};
			qlRes = await common.getResults(params);
		});
		it('190037.数据验证', async function () {
			TESTCASE = {
				describe: "综合汇总详细界面数据验证",
				expect: "综合汇总详细界面数据和收支流水界面要数据一致",
				jira: '由SLHSEC-7377这个问题引起，实际值少了几条',
			}
			let accountName = {}; //{id:name}
			qlRes['cs-getAccountData'].dataList.forEach((obj) => accountName[obj.id] = obj.name);

			//根据收支流水数据,拼接期望值
			let exp = {};
			for (let i = 0, length = qlRes['ql-1352'].dataList.length; i < length; i++) {
				let obj = qlRes['ql-1352'].dataList[i];
				if (['收入单', '支出单'].includes(obj.billtype)) continue; //排除收支单数据

				//1收入 2支出
				if (obj.money >= 0) {
					obj.inouttype = 1;
				} else {
					obj.inouttype = 2;
					obj.money = -obj.money;
				};

				// key = ‘账户id-单据类型-收支类型’
				let key = `${obj.accountid}-${obj.billtype}-${obj.inouttype}`;
				if (exp[key]) {
					exp[key].money = common.add(exp[key].money, obj.money);
				} else {
					exp[key] = {
						accountname: accountName[obj.accountid], //e.g. '现','银','代'
						money: obj.money,
						accountid: obj.accountid,
						inouttype: obj.inouttype,
						catname: obj.billtype,
					};
					//
					if (USEECINTERFACE == 1) {
						if (obj.billtype == '销售单' && obj.inouttype == 2) exp[key].catname = '销售退款';
						if (obj.billtype == '采购单' && obj.inouttype == 1) exp[key].catname = '采购退款';
					};
				};
			};
			// common.isApproximatelyEqualAssert(exp, qlRes['ql-16235'].dataList, ['catname']);
			// console.log(`exp= ${JSON.stringify(Object.values(exp))}`);
			// console.log(`dataList = ${JSON.stringify(qlRes['ql-16235'].dataList)}`);;
			common.isArrayContainArrayAssert(Object.values(exp), qlRes['ql-16235'].dataList, ['catname'], '统计分析-综合汇总详细见界面统计数据有误');
		});

	});

	//inv_inout_main main.type=21(销售单)
	//pagesize=30 取最近的30条数据验证
	describe('还款,欠款,抵扣,余款子页面数据检查', function () {

		let qlRes = {},
			[accbalanceArr, debtArr, paybackArr, deductArr] = [[], [], [], []]; //余款,欠款,还款,抵扣
		before(async () => {
			let defParam = {
				shopid: LOGINDATA.invid,
				prodate: common.getCurrentDate(),
				today: true,
				sortField: 'billno',
				sortfieldOrder: 1, //按批次号倒序排序
				pagesize: 30, //取部分数据验证
			};
			let params = {
				'ql-142201': defParam, //销售开单-按批次查
				'ql-16234': defParam, //余款明细
				'ql-16232': defParam, //欠款明细
				'ql-16231': defParam, //还款明细
				'ql-16233': defParam, //抵扣明细
				'ql-1623': {
					invid: LOGINDATA.invid,
					today: true,
				}, //统计分析-综合汇总
			};
			qlRes = await common.getResults(params);

			//销售开单-按批次查
			qlRes['ql-142201'].dataList.forEach((obj) => {
				if (obj.invalidflag == 0) {
					for (let key in obj) {
						obj[key] = isNaN(obj[key]) ? obj[key] : Number(obj[key]);
					};
					const totalsum = obj.totalmoney; //单子总金额
					// obj.actualpay = pay.cash + pay.card + pay.remit + pay.agency + pay.weixinpay + pay.alipay

					//为了方便后续调试,数据分开归类
					//余款 ql-16234
					if (obj.balance > 0 && obj.verifysum < 0) {
						obj.accbalance = obj.balance;
						obj.totalmoney = totalsum + obj.backsum - obj.othercost + obj.favor;
						accbalanceArr.push(obj);
					};
					if (obj.verifysum >= 0 && obj.actualpay - totalsum > 0) {
						obj.accbalance = obj.actualpay - totalsum;
						obj.totalmoney = totalsum + obj.backsum - obj.othercost + obj.favor;
						accbalanceArr.push(obj);
					};

					//欠款 ql-16232
					if (obj.balance < 0 && obj.verifysum >= 0) {
						obj.debt = Math.abs(obj.balance);
						obj.totalmoney = totalsum + obj.backsum;
						debtArr.push(obj);
					};
					if (obj.verifysum < 0 && obj.actualpay - totalsum < 0) {
						obj.debt = Math.abs(obj.actualpay - totalsum);
						obj.totalmoney = totalsum + obj.backsum;
						debtArr.push(obj);
					};

					//还款 ql-16231
					if (obj.verifysum < 0 && obj.actualpay - totalsum > 0) {
						if (obj.actualpay - totalsum < Math.abs(obj.verifysum)) {
							obj.payback = obj.actualpay - totalsum;
						} else {
							obj.payback = Math.abs(obj.verifysum);
						};
						obj.totalmoney = totalsum + obj.backsum;
						paybackArr.push(obj);
					};

					//抵扣 ql-16233
					if (obj.verifysum > 0 && totalsum - obj.actualpay > 0) {
						if (obj.verifysum >= totalsum - obj.actualpay) {
							obj.deduct = totalsum - obj.actualpay;
						} else {
							obj.deduct = obj.verifysum;
						};
						obj.totalmoney = totalsum + obj.backsum;
						deductArr.push(obj);
					};
				};
			});

		});
		// let i = 3;
		// common.isApproximatelyEqualAssert(qlRes['ql-16234'].dataList[i], accbalance[i]);
		it('190038.余款', async function () {
			common.isArrayContainArrayAssert(accbalanceArr, qlRes['ql-16234'].dataList);
		});
		it('190039.欠款', async function () {
			common.isArrayContainArrayAssert(debtArr, qlRes['ql-16232'].dataList);
		});
		it('190040.还款', async function () {
			TESTCASE = {
				describe: "综合汇总详细界面-还款界面数据验证",
				expect: "综合汇总详细界面数据和按批次查-还款/抵扣列汇总值一致",
			}
			common.isArrayContainArrayAssert(paybackArr, qlRes['ql-16231'].dataList);
		});
		it('190041.抵扣', async function () {
			common.isArrayContainArrayAssert(deductArr, qlRes['ql-16233'].dataList);
		});
		//验证子界面的汇总值等于综合汇总的值 累加验证在汇总脚本验证
		it('190049.检查合计', async function () {
			TESTCASE = {
				describe: "综合汇总详细界面-右上角四个按钮每个界面数据汇总值和综合汇总界面数据验证",
				expect: "综合汇总详细界面-右上角四个按钮每个界面数据汇总值和综合汇总界面响应列数据一致",
				jira: "SLHSEC-6792 综合汇总-详细界面-还款界面，还款列没有值",
			}

			common.isApproximatelyEqualAssert({
				accbalance: qlRes['ql-16234'].sumrow.accbalance, //余款
				debtmoney: qlRes['ql-16232'].sumrow.debt, //欠款
				paybackmoney: qlRes['ql-16231'].sumrow.payback, //还款
				deduct: qlRes['ql-16233'].sumrow.deduct, //抵扣
			}, qlRes['ql-1623'].dataList[0]);
		});
	});

	describe('权限检查', function () {
		after(async () => {
			await common.loginDo();
		});
		it('190100.1.总经理', async function () {
			let qlRes = await common.callInterface('ql-1623', format.qlParamsFormat({}, true));
			let invNames = qlRes.dataList.map((obj) => obj.invname);
			expect(invNames.length).to.above(1);
		});
		it('190100.2.店长', async function () {
			await common.loginDo({
				logid: '004',
				pass: '000000',
			});
			let qlRes = await common.callInterface('ql-1623', format.qlParamsFormat({}, true));
			let invNames = qlRes.dataList.map((obj) => obj.invname);
			expect(invNames).to.eql([LOGINDATA.invname]);
		});
		it('190140.店长查询其他门店数据', async function () {
			await common.loginDo({
				logid: '004',
				pass: '000000',
			});
			let qlRes = await common.callInterface('ql-1623', format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
			}, true));
			expect(qlRes.count).to.equal('0');
		});
	});

});
