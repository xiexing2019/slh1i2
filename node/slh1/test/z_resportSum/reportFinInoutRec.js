'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const basiceJsonparam = require('../../help/basiceJsonparam');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler');

//190090一代跑不通，
/*
 * 收支流水相关用例
 */
describe('收支流水-slh2', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	//验证prodate,accountInvName,accountid,balance 其他字段在190025中验证
	it('190022.查询', async function () {
		TESTCASE = {
			describe: "收支流水-金额、余额检查",
			expect: `余额=金额+上一条余额`,
			jira: 'SLHSEC-6801'
		}
		let qlRes = await common.callInterface('ql-1352', format.qlParamsFormat({
			accountid: BASICDATA[`cashAccount${LOGINDATA.invid}`],
			accountInvid: LOGINDATA.invid,
		}, true)); //统计分析-收支流水

		qlRes.dataList.map((obj, index) => {
			common.isApproximatelyEqualAssert(obj, {
				prodate: common.getCurrentDate(),
				accountInvName: LOGINDATA.invname,
				accountid: BASICDATA[`cashAccount${LOGINDATA.invid}`]
			});
			if (index < qlRes.count - 1) {
				expect(common.add(obj.money, qlRes.dataList[index + 1].balance), `统计分析-收支流水 余额错误`).to.equal(Number(obj.balance));

			};
		});
	});
	//要使账户余额为负，开单时现金得输入负数
	it('190090.帐户余额允许为负', async function () {
		await common.setGlobalParam('fin_account_balance_negative', 0); //账户余额允许为负 默认允许

		let param = format.qlParamsFormat({
			accountid: BASICDATA[`cashAccount${LOGINDATA.invid}`],
			accountInvid: LOGINDATA.invid,
			pagesize: 15,
		});
		let qlRes = await common.callInterface('ql-1352', param);
		//若账户余额不为负，则生成一个单据，使之为负
		let json = basiceJsonparam.salesJson();
		[json.cash, json.card, json.remit] = [-common.add(qlRes.dataList[0].balance, 1), 0, 0];
		await common.editBilling(json);
		if (USEECINTERFACE == 2) await common.delay(500);
		qlRes = await common.callInterface('ql-1352', param);
		// console.log(`qlRes : ${JSON.stringify(qlRes.dataList[0])}`);
		let negative = qlRes.dataList[0].balance < 0;


		expect(negative).to.be.true;
	});

	//代收核销单在170287中验证
	//收入+支出单放到收支流水中验证--已在addIncome.js中验证
	// 二代没有返回银行账户：accountName qf接口未返回show_${key}accountid
	describe('190025.金额检查', function () {
		let qlParams = format.qlParamsFormat({
			accountInvid: LOGINDATA.invid,
		}, true);
		it('1.销售开单', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.salesJson());
			if (USEECINTERFACE == 2) await common.delay(5000);
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk
			});
			let qlRes = await common.callInterface('ql-1352', qlParams); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
		it('2.销售订货', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson());
			if (USEECINTERFACE == 2) await common.delay(5000);
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.prepayid, //预付款单pk
			}); //获取生成的预付款单信息

			let qlRes = await common.callInterface('ql-1352', qlParams); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
		it('3.采购入库', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.purchaseJson());
			if (USEECINTERFACE == 2) await common.delay(3000);
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14212-1',
				pk: sfRes.result.pk
			});
			let qlRes = await common.callInterface('ql-1352', qlParams); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
		it('4.采购订货', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.purchaseOrderJson());
			if (USEECINTERFACE == 2) await common.delay(800);
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14212-1',
				pk: sfRes.result.prepayid, //预付款单pk
			}); //获取生成的预付款单信息

			let qlRes = await common.callInterface('ql-1352', qlParams); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
	});

	describe('190104.退货并退款.rankA', function () {
		let qlParams = format.qlParamsFormat({
			accountInvid: LOGINDATA.invid,
		}, true);
		it('1.销售开单', async function () {
			let json = basiceJsonparam.salesJson();
			[json.cash, json.card, json.remit] = [-100, -200, -300];
			[json.details[0].num, json.details[1].num] = [-1, -2];
			let sfRes = await common.editBilling(json);
			if (USEECINTERFACE == 2) await common.delay(500);
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk
			});
			// console.log(`time=${sfRes.duration}`);
			//统计分析-收支流水
			let qlRes = await common.callInterface('ql-1352', qlParams);
			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
		it('2.采购入库', async function () {
			let json = basiceJsonparam.purchaseJson();
			[json.cash, json.card, json.remit] = [-100, -200, -300];
			[json.details[0].num, json.details[1].num] = [-1, -2];
			let sfRes = await common.editBilling(json);
			if (USEECINTERFACE == 2) await common.delay(3500);
			let qfRes = await purRequestHandler.purQueryBilling(sfRes.result.pk);

			let qlRes = await common.callInterface('ql-1352', qlParams); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));

		});
	});

	describe('190138.备注', function () {
		before(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 1); //单据是否允许修改客户或厂商  设置为允许
		});
		after(async () => {
			await common.setGlobalParam('dwxx_not_allow_edit', 0); //单据是否允许修改客户或厂商  不允许
		});
		it('4.选一个客户的单据修改为另外一个客户保存', async function () {
			TESTCASE = {
				describe: "修改单据，收支流水-备注也修改",
				expect: `rem:单位[小王]变成[李四]`,
				jira: 'SLHSEC-6742'
			}
			let sfRes = await common.editBilling(basiceJsonparam.salesJson());
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk
			});

			qfRes.result.interfaceid = sfRes.params.interfaceid;
			qfRes.result.action = 'edit';
			qfRes.result.dwid = BASICDATA.dwidLs;
			sfRes = await common.editBilling(qfRes.result);
			if (USEECINTERFACE == 2) await common.delay(5000);
			qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk
			});
			let qlRes = await common.callInterface('ql-1352', format.qlParamsFormat({
				accountInvid: LOGINDATA.invid,
			}, true)); //统计分析-收支流水

			qfRes.result.interfaceid = qfRes.params.interfaceid;
			let dataListExp = resSumReqHandler.getInOutRecExp(qfRes.result);
			common.isApproximatelyArrayAssert(dataListExp, qlRes.dataList.slice(0, dataListExp.length));
		});
		it.skip('5.后台-财务管理-转款单', async function () {
			let sfRes = await common.callInterface('sf-1357', {
				jsonparam: {
					action: 'add',
					prodate: common.getCurrentDate(),
					deliver: LOGINDATA.id,
					accountidfrom: BASICDATA[`cashAccount${LOGINDATA.invid}`],
					accountidto: BASICDATA[`cardAccount${LOGINDATA.invid}`],
					money: common.getRandomNum(100, 1000),
				},
			});
			console.log(`sfRes = ${JSON.stringify(sfRes)}`);
			// let qfRes = await common.callInterface('qf-1357',{pk});
		});
	});

	describe('店长权限', function () {
		before(async () => {
			await common.loginDo({
				logid: '004',
				pass: '000000'
			});
		});
		after(async () => {
			await common.loginDo();
		});
		it('190110.店长查看收支流水的权限', async function () {
			//查询其他门店数据
			let param = format.qlParamsFormat({
				accountInvid: BASICDATA.shopidZzd,
			}, true);
			let qlRes = await common.callInterface('ql-1352', param);
			expect(qlRes.count).to.equal('0');

			//查询所有,应该都是本门店数据
			param.accountInvid = '';
			qlRes = await common.callInterface('ql-1352', param);
			qlRes.dataList.map((obj) => {
				expect(obj.accountInvName, `统计分析-收支流水,店长权限-显示其他门店数据`).to.equal(LOGINDATA.invname);
			});
		});
	});

});
