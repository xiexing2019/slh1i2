'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const basiceJsonparam = require('../../help/basiceJsonparam');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
let errorKeys = [];
//新综合汇总接口
//用例190047~190067
//http://jira.hzdlsoft.com:7082/browse/SLH-2850
describe('新综合汇总接口-slh2', function () {
	this.timeout(30000);

	let salesJson = {};

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		salesJson = basiceJsonparam.simpleSalesJson();
		[salesJson.cash, salesJson.card, salesJson.remit] = [0, 0, 0];
	});

	after(() => {
		if (errorKeys.length != 0) console.log(`errorKeys = ${JSON.stringify(Array.from(new Set(errorKeys)))}`);
	});

	it('190047.核销欠款1500,退货1000,付款300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 7.5; //生成欠款单 1500
		});
		json.details[0].num = -5;
		json.cash = 300;

		await testField(json, {
			actualnum: -5,
			backnum: 5,
			backmoney: 1000,
			cash: 300,
			paybackmoney: 1300,
			actualsalesmoney: -1000,
			actualpay: 300
		});
	});
	it('190048.核销欠款200,退货1000,付款300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1; //生成欠款单 200
		});
		json.details[0].num = -5;
		json.cash = 300;

		await testField(json, {
			actualnum: -5,
			backnum: 5,
			backmoney: 1000,
			cash: 300,
			paybackmoney: 200,
			actualsalesmoney: -1000,
			actualpay: 300,
			accbalance: 1100,
		});
	});
	it('190049.核销欠款200,购买1000,付款300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1; //生成欠款单 200
		});
		json.details[0].num = 5;
		json.cash = 300;

		await testField(json, {
			actualnum: 5,
			totalmoney: 1000,
			debtmoney: 700,
			actualsalesmoney: 1000,
			num: 5,
			cash: 300,
			actualpay: 300,
		});
	});
	it('190050.核销欠款1000,付款500,购买300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details[0].num = 1.5;
		json.cash = 500;

		await testField(json, {
			paybackmoney: 200,
			actualnum: 1.5,
			totalmoney: 300,
			actualsalesmoney: 300,
			num: 1.5,
			cash: 500,
			actualpay: 500,
		});
	});
	it('190051.核销欠款200,付款40,购买400,退货300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1; //生成欠款单 200
		});
		json.details[0].num = 2;
		json.details.push({
			num: -1.5,
			sizeid: 'L',
			matCode: 'Agc001',
			rem: '明细2',
			colorid: 'BaiSe',
			price: 200,
			discount: 1,
		});
		json.cash = 40;

		await testField(json, {
			actualnum: 0.5,
			backmoney: 300,
			totalmoney: 400,
			debtmoney: 60,
			actualsalesmoney: 100,
			num: 2,
			cash: 40,
			actualpay: 40,
			backnum: 1.5
		});
	});
	it('190052.核销欠款1000,退2000,购买3600,付1000', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details[0].num = 18; //购买3600
		json.details.push({
			num: -10,
			sizeid: 'L',
			matCode: 'Agc001',
			rem: '明细2',
			colorid: 'BaiSe',
			price: 200,
			discount: 1,
		}); //退货2000
		json.cash = 1000;

		await testField(json, {
			actualnum: 8,
			backmoney: 2000,
			totalmoney: 3600,
			debtmoney: 600,
			actualsalesmoney: 1600,
			num: 18,
			cash: 1000,
			actualpay: 1000,
			backnum: 10,
		});
	});
	it('190053.核销欠款1000,退9000,购买3600,付1000', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details[0].num = 18; //购买3600
		json.details.push({
			num: -45,
			sizeid: 'L',
			matCode: 'Agc001',
			rem: '明细2',
			colorid: 'BaiSe',
			price: 200,
			discount: 1,
		}); //退货9000
		json.cash = 1000;

		await testField(json, {
			paybackmoney: 1000,
			actualnum: -27,
			backmoney: 9000,
			totalmoney: 3600,
			actualsalesmoney: -5400,
			num: 18,
			accbalance: 5400,
			cash: 1000,
			actualpay: 1000,
			backnum: 45,
		});
	});
	it('190054.核销欠款1000,特殊货品抹零100,付900', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details = [{
			rem: 'Spec1',
			matCode: '00000',
			price: 100,
			num: -1,
			colorid: '0',
			sizeid: '0'
		}];
		json.cash = 900;

		await testField(json, {
			"paybackmoney": 1000,
			"actualsalesmoney": -100,
			"cash": 900,
			"actualpay": 900,
			"specmoney": -100
		});
	});
	it('190055.核销欠款1000,购买2000,付3300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details[0].num = 10;
		json.cash = 3300;

		await testField(json, {
			paybackmoney: 1000,
			actualnum: 10,
			totalmoney: 2000,
			actualsalesmoney: 2000,
			num: 10,
			accbalance: 300,
			cash: 3300,
			actualpay: 3300
		});
	});
	it('190056.核销欠款1000,购买2000,付2800', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details[0].num = 10;
		json.cash = 2800;

		await testField(json, {
			paybackmoney: 800,
			actualnum: 10,
			totalmoney: 2000,
			actualsalesmoney: 2000,
			num: 10,
			cash: 2800,
			actualpay: 2800,
		});
	});
	it('190057.核销欠款1000,付2000', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details = [];
		json.cash = 2000;

		await testField(json, {
			paybackmoney: 1000,
			accbalance: 1000,
			cash: 2000,
			actualpay: 2000,
		});
	});
	it('190058.核销欠款1000,付200', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 5; //生成欠款单 1000
		});
		json.details = [];
		json.cash = 200;

		await testField(json, {
			paybackmoney: 200,
			cash: 200,
			actualpay: 200,
		});
	});
	it('190059.核销余款200,退货1000,付款100', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1;
			obj.cash = 400; //生成余款单 200
		});
		json.details[0].num = -5;
		json.cash = 100;

		await testField(json, {
			actualnum: -5,
			backmoney: 1000,
			actualsalesmoney: -1000,
			accbalance: 1100,
			cash: 100,
			actualpay: 100,
			backnum: 5
		});
	});
	it('190060.核销余款200,购买1000,付款300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1;
			obj.cash = 400; //生成余款单 200
		});
		json.details[0].num = 5;
		json.cash = 300;

		await testField(json, {
			deduct: 200,
			actualnum: 5,
			totalmoney: 1000,
			debtmoney: 500,
			actualsalesmoney: 1000,
			num: 5,
			cash: 300,
			actualpay: 300,
		});
	});
	it('190061.核销余款300,购买800,付款600', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1;
			obj.cash = 500; //生成余款单 300
		});
		json.details[0].num = 4;
		json.cash = 600;

		await testField(json, {
			deduct: 200,
			actualnum: 4,
			totalmoney: 800,
			actualsalesmoney: 800,
			num: 4,
			cash: 600,
			actualpay: 600,
		});
	});
	it('190062.核销余款200,付款500,购买300', async function () {
		let json = await getVerifyJson(salesJson, (obj) => {
			obj.details[0].num = 1;
			obj.cash = 400; //生成余款单 200
		});
		json.details[0].num = 1.5;
		json.cash = 500;

		await testField(json, {
			actualnum: 1.5,
			totalmoney: 300,
			actualsalesmoney: 300,
			num: 1.5,
			accbalance: 200,
			cash: 500,
			actualpay: 500,
		});
	});
	it('190063.纯退货300,不做其它操作', async function () {
		let json = _.cloneDeep(salesJson);
		json.details[0].num = -1.5;

		await testField(json, {
			actualnum: -1.5,
			backmoney: 300,
			actualsalesmoney: -300,
			accbalance: 300,
			backnum: 1.5
		});
	});
	it('190064.购买200,付80', async function () {
		let json = _.cloneDeep(salesJson);
		json.details[0].num = 1;
		json.cash = 80;

		await testField(json, {
			actualnum: 1,
			totalmoney: 200,
			debtmoney: 120,
			actualsalesmoney: 200,
			num: 1,
			cash: 80,
			actualpay: 80,
		});
	});
	it('190065.购买200,不付钱', async function () {
		let json = _.cloneDeep(salesJson);
		json.details[0].num = 1;

		await testField(json, {
			actualnum: 1,
			totalmoney: 200,
			debtmoney: 200,
			actualsalesmoney: 200,
			num: 1,
		});
	});
	it('190066.订货1600', async function () {
		let json = basiceJsonparam.salesOrderJson();
		[json.cash, json.card, json.remit] = [1600, 0, 0];
		json.details[0].num = 3;
		json.details[1].num = 5;

		await testField(json, {
			accbalance: 1600,
			cash: 1600,
			actualpay: 1600,
		});
	});
	it('190067.购买200,付220', async function () {
		let json = _.cloneDeep(salesJson);
		json.details[0].num = 1;
		json.cash = 220;

		await testField(json, {
			actualnum: 1,
			totalmoney: 200,
			actualsalesmoney: 200,
			num: 1,
			accbalance: 20,
			cash: 220,
			actualpay: 220,
		});
	});
});

//获取综合汇总本门店的当天数据
async function getReportTotalSumData() {
	let qlRes = await common.callInterface('ql-1623', format.qlParamsFormat({
		invid: LOGINDATA.invid,
	}, true));
	return qlRes.dataList[0];
};

//先生成核销单A,然后根据json拼接核销单据A的jsonparam
async function getVerifyJson(json, jsonHandler) {
	let json1 = _.cloneDeep(json);
	jsonHandler(json1);
	await common.editBilling(json1);

	let verifyInfo = await salesReqHandler.getVerifyingBills(json1.dwid)
		.then((obj) => obj.dataList[0]); //获取核销信息

	let json2 = _.cloneDeep(json);
	json2.finpayVerifybillids = verifyInfo.id;
	json2.finpayVerifysum = verifyInfo.balance;
	return json2;
};

//验证综合汇总结果
async function testField(json, exp) {
	let errorMsg = '';
	let data1 = await getReportTotalSumData(); //获取起始值

	let sfRes = await common.editBilling(json);
	// console.log(`sfRes = ${JSON.stringify(sfRes)}`);

	let data2 = await getReportTotalSumData();

	let actual = common.subObject(data2, data1);
	exp = Object.assign({
		daishou: 0,
		remit: 0,
		deduct: 0,
		collection: 0,
		// storedValueCost: 0,
		purinnum: 0,
		paybackmoney: 0,
		card: 0,
		actualnum: 0,
		backmoney: 0,
		totalmoney: 0,
		profit: 0,
		debtmoney: 0,
		actualsalesmoney: 0,
		num: 0,
		alipay: 0,
		weixinpay: 0,
		accbalance: 0,
		cash: 0,
		actualpay: 0,
		agency_notpaid: 0,
		backnum: 0,
		specmoney: 0
	}, exp);
	// console.log(`actual = ${JSON.stringify(actual)}`);
	// expect(actual, `actual=${JSON.stringify(actual)}\nexp=${JSON.stringify(exp)}`).to.includes(exp);
	for (let key in actual) {
		if (exp[key] && actual[key] != exp[key]) {
			errorKeys.push(key);
			errorMsg += `key=${key} exp=${exp[key]} actual=${actual[key]}\n`;
		};
	};

	expect(errorMsg).to.equal('');
};
