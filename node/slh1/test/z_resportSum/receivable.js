'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');


describe('应收调整单-slh2', function () {
	this.timeout('30000');
	let clientInfo = {}; //客户信息

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		clientInfo = await common.callInterface('qf-1401', {
			'pk': BASICDATA.dwidXw,
		}); //获取客户信息
	});

	it('190175.新增', async function () {
		let sfRes = await resSumReqHandler.editReceivableBill(); //新增应收调整单
		let qfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-1345',
			pk: sfRes.result.pk
		}); //获取应收调整单明细值
		common.isApproximatelyEqualAssert(Object.assign({
			show_dwid: clientInfo.nameshort,
			show_invid: LOGINDATA.invname,
			show_deliver: `${LOGINDATA.code},${LOGINDATA.name}` // USEECINTERFACE == 1 ? `${LOGINDATA.code},${LOGINDATA.name}` : LOGINDATA.name,
		}, sfRes.params), qfRes.result);
	});

	it('190177.客户和营业员,输入不存在的内容', async function () {
		let sfRes = await resSumReqHandler.editReceivableBill({
			jsonparam: {
				dwid: 'undefined',
			},
			check: false,
		}); //新增应收调整单
		expect(sfRes.result.error).to.includes('服务端错误');

		sfRes = await resSumReqHandler.editReceivableBill({
			jsonparam: {
				deliver: 'undefined',
			},
			check: false,
		}); //新增应收调整单
		expect(sfRes.result.error).to.includes('服务端错误');
	});

	//可以正常保存,由客户端控制
	it.skip('190178.客户栏输入厂商-客户端控制', async function () { });

	it('190183.金额从正数改为负数', async function () {
		TESTCASE = {
			describe: '应收调整单金额正负，经营历程结余类型体现为未付和退货',
			expect: '应收调整单金额为正，经营历程结余类型为未付，金额为负，结余类型为退货',
			jira: 'SLHSEC-5653,经营历程单据排序有问题',
		}
		let sfRes = await resSumReqHandler.editReceivableBill();
		if (USEECINTERFACE == 2) {
			await common.delay(500);
		};
		let param = format.qlParamsFormat({
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			shopid: LOGINDATA.invid,
			type: sfRes.params.type,
		}, true);
		let qlRes = await common.callInterface('ql-1906', param);
		expect(qlRes.dataList[0], `单据信息:${JSON.stringify(sfRes)},统计分析-经营历程查询错误：${JSON.stringify(qlRes.dataList[0])}`).to.includes({
			finpayBalancetype: '未付'
		});

		sfRes = await resSumReqHandler.editReceivableBill({
			jsonparam: {
				pk: sfRes.result.pk,
				totalmoney: -common.getRandomNumStr(4),
			},
		});
		qlRes = await common.callInterface('ql-1906', param);
		expect(qlRes.dataList[0]).to.includes({
			finpayBalancetype: '退货'
		});
	});

	it('190190.修改门店', async function () {
		let sfRes = await resSumReqHandler.editReceivableBill();
		sfRes = await resSumReqHandler.editReceivableBill({
			jsonparam: {
				pk: sfRes.result.pk,
				invid: BASICDATA.shopidZzd,
			},
			check: false,
		});
		expect(sfRes.result).to.includes({
			error: USEECINTERFACE == 1 ? '不允许修改单据所属门店' : '不允许修改门店',
		});
	});

	it('190185.组合查询', async function () {
		let sfRes = await resSumReqHandler.editReceivableBill();

		let param = format.qlParamsFormat({
			invid: LOGINDATA.invid,
			billno: sfRes.result.billno,
			billno1: sfRes.result.billno,
			billno2: sfRes.result.billno,
			dwid: sfRes.params.dwid,
			dwxxTypeid: clientInfo.typeid,
			remark: sfRes.params.remark,
			invalidflag: 0,
		}, true);
		let qlRes = await common.callInterface('ql-1345', param);
		let exp = {
			optime: sfRes.optime,
			totalmoney: sfRes.params.totalmoney,
			id: sfRes.result.pk,
			invname: LOGINDATA.invname,
			dwname: clientInfo.nameshort,
			remark: sfRes.params.remark,
			staffName: LOGINDATA.name,
			invalidflag: '0',
			invalidflagname: "否",
			billno: sfRes.result.billno,
			prodate: sfRes.params.prodate,
			opname: LOGINDATA.name,
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('190189.作废', async function () {
		let sfRes = await resSumReqHandler.editReceivableBill();
		await reqHandler.csIFCHandler({
			interfaceid: 'cs-1345',
			pk: sfRes.result.pk
		});
		let param = format.qlParamsFormat({
			invid: LOGINDATA.invid,
			billno: sfRes.result.billno,
			invalidflag: 1,
		}, true);
		let qlRes = await common.callInterface('ql-1345', param);
		let exp = {
			optime: sfRes.optime,
			totalmoney: sfRes.params.totalmoney,
			id: sfRes.result.pk,
			invname: LOGINDATA.invname,
			remark: sfRes.params.remark,
			staffName: LOGINDATA.name,
			invalidflag: '1',
			invalidflagname: '是',
			billno: sfRes.result.billno,
			prodate: sfRes.params.prodate,
			opname: LOGINDATA.name,
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	//一代 整数:9位,小数:3位; 二代 整数:16位,小数:3位
	//每天的账款设置了上限值，因此需要对刚刚做的边界值测试的单据要进行作废，否则会报错
	describe.skip('190181.金额输入较大数字', function () {
		let maximum;
		before(async () => {
			maximum = USEECINTERFACE == 1 ? 9 : 13; //不确定后续是否会更改
		});
		it('1.边界值', async function () {
			let sfRes = await resSumReqHandler.editReceivableBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(maximum),
				},
			}); //新增应收调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1345',
				pk: sfRes.result.pk
			}); //获取应收调整单明细值

			await reqHandler.csIFCHandler({
				interfaceid: 'cs-1345',
				pk: sfRes.result.pk
			}); //作废

			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.超出边界值', async function () {
			let sfRes = await resSumReqHandler.editReceivableBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(maximum + 1), //超出边界
				},
				check: false,
			}); //新增应收调整单
			if (sfRes.result.pk) {
				await reqHandler.csIFCHandler({
					interfaceid: 'cs-1345',
					pk: sfRes.result.pk,
				}); //作废
			};
			expect(sfRes.result).to.have.property('error');
		});
	});

	//应收调整单totalmoney必填项判断，二代是receiveMoney，需要确认一下一代
	describe('190176.检查必填项', function () {
		const errorMsgs = () => {
			return USEECINTERFACE == '1' ? {
				// deliver: '', //客户端判断
				dwid: `服务端错误`,
				invid: '门店获取出错',
				totalmoney: '空单不允许保存',
				// prodate: '', //客户端判断
			} : {
					dwid: `参数[compId]必填`,
					invid: '参数[shopId]必填',
					totalmoney: '参数[receiveMoney]必填', //在这里  payMoney
				};
		};
		const keys = ['dwid', 'invid', 'totalmoney'];
		for (let key of keys) {
			it(`不填写${key},检查开单结果`, async function () {
				let jsonparam = {};
				jsonparam[key] = '';
				let sfRes = await resSumReqHandler.editReceivableBill({
					jsonparam: jsonparam,
					check: false
				}); //新增应收调整单
				expect(sfRes.result.error).to.includes(errorMsgs()[key]);
			});
		};
	});

	describe('190182.金额输入0,正数,负数', function () {
		it('1.正数', async function () {
			let sfRes = await resSumReqHandler.editReceivableBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(4),
				},
			}); //新增应收调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1345',
				pk: sfRes.result.pk
			}); //获取应收调整单明细值
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.负数', async function () {
			let sfRes = await resSumReqHandler.editReceivableBill({
				jsonparam: {
					totalmoney: -common.getRandomNumStr(4),
				},
			}); //新增应收调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1345',
				pk: sfRes.result.pk
			}); //获取应收调整单明细值
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('3.输入0', async function () {
			let sfRes = await resSumReqHandler.editReceivableBill({
				jsonparam: {
					totalmoney: 0,
				},
				check: false,
			}); //新增应收调整单
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '空单不允许保存' : '空单据不允许保存!',
			});
		});
	});

	//需要常青店001财务员 004店长
	describe('权限检查', function () {
		before(async () => {
			await resSumReqHandler.editReceivableBill({
				jsonparam: {
					invid: BASICDATA.shopidZzd,
				},
			}); //新增其他门店的应收调整单
		});
		after(async () => {
			await common.loginDo();
		});
		it('190179.店长', async function () {
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});

			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
			}, true);
			let qlRes = await common.callInterface('ql-1345', param);
			expect(qlRes.count, '店长可以看到其他门店数据错误').to.equal('0');

			param.invid = '';
			qlRes = await common.callInterface('ql-1345', param);
			qlRes.dataList.map((obj) => {
				expect(obj.invname, `店长权限,应收调整单显示其他门店数据`).to.equal(LOGINDATA.invname);
			});
		});
		it('190191.财务员查看数据', async function () {
			await common.loginDo({
				'logid': '001',
				'pass': '000000',
			});

			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
			}, true);
			let qlRes = await common.callInterface('ql-1345', param);
			expect(Number(qlRes.count), '财务员无法看到其他门店数据').to.above(0);
		});
	});

	describe('数据校验', function () {
		let sfRes = {};
		before(async () => {
			sfRes = await resSumReqHandler.editReceivableBill();
		});
		it('190192.1.往来管理-客户门店账明细查询', async function () {
			this.retries(2);
			let param = format.qlParamsFormat({
				pagesize: 1, //只获取需要验证的那条数据
				dwid: sfRes.params.dwid,
				shopid: LOGINDATA.invid,
			}, false);
			let qlRes = await common.callInterface('ql-150011', param);
			let exp = {
				optime: sfRes.optime,
				totalmoney: sfRes.params.totalmoney,
				daishou: 0,
				invname: LOGINDATA.invname,
				remit: 0,
				seller: LOGINDATA.name,
				id: sfRes.result.pk,
				balance: -sfRes.params.totalmoney,
				storedValueCost: 0,
				weixinpay: 0,
				alipay: 0,
				cash: 0,
				card: 0,
				billno: sfRes.result.billno,
				givesum: 0
			};
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		//往来管理-客户门店账明细查询/销售开单-按客户未结明细查询点击进去的界面都是qf-1934
		it('190192.2.invmain信息', async function () {
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1934',
				pk: sfRes.result.pk,
				bizType: 5500, //应收调整单
			});
			let qfRes2 = await reqHandler.queryBilling({
				interfaceid: 'qf-1345',
				pk: sfRes.result.pk
			}); //获取应收调整单明细值
			common.isApproximatelyEqualAssert(qfRes.result, Object.assign(qfRes2.result, {
				billno: sfRes.result.billno,
				typename: '应收调整单',
			}), ['type']);
		});
		it('190193.销售开单-按客户未结明细查询', async function () {
			let param = format.qlParamsFormat({
				dwid: sfRes.params.dwid,
				shopid: LOGINDATA.invid,
			}, true);
			let qlRes = await common.callInterface('ql-144851', param);
			let exp = {
				id: sfRes.result.pk,
				totalmoney: sfRes.params.totalmoney,
				balance: -sfRes.params.totalmoney,
				billno: sfRes.result.billno,
			};
			expect(common.isArrayContainObject(qlRes.dataList, exp)).to.be.true;
		});
		it('190194.销售开单-按客户销售', async function () {
			let param = format.qlParamsFormat({
				pagesize: 15, //取最近生成的15条数据
				dwid: sfRes.params.dwid,
				shopid: LOGINDATA.invid,
			}, false);
			let qlRes = await common.callInterface('ql-144571', param);
			qlRes.dataList.map((obj) => {
				expect(obj.id).not.to.equal(sfRes.result.pk);
			});
		});
	});
});
