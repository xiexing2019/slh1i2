'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const basiceJsonparam = require('../../help/basiceJsonparam');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
/**
 * 利润表
 * ql-1447001 ql-1447002 ql-1447003
 *
 * ql-1447002 prodate必填 查询一天的明细 参数prodate1，prodate2无效 (二代)
 *
 * 特殊货品需要考虑是否核算到利润  profitcount:'否'
 **/
describe('日利润表-slh2', function () {
	this.timeout(30000);
	let [styleInfo, clientInfo, specNoProfit] = [{}, {}, []];
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		clientInfo = await common.callInterface('qf-1401', {
			'pk': BASICDATA.dwidXw
		}); //获取客户信息

		BASICDATA.specGoodsList.forEach((obj) => {
			if (obj.profitcount == '否') {
				specNoProfit.push(_.get(obj, 'code'));
			};
		}); //不核算到利润的特殊货品
		// console.log(`BASICDATA.=${JSON.stringify(BASICDATA.specGoodsList)}`);
		// console.log(`specNoProfit=${JSON.stringify(specNoProfit)}`);
	});
	//验证成本价,成本额是否正确 利润值在190083,190084中验证
	it('190089.检查加工类商品的利润值', async function () {
		TESTCASE = {
			jira: 'SLHSEC-7482',
		};
		let info = await common.fetchMatInfo(BASICDATA.styleidAgc002); //加工类商品 purprice
		let param = format.qlParamsFormat({
			styleid: info.pk,
			prodate: common.getCurrentDate(),
		}, true);
		let qlRes = await common.getResults({
			'ql-1447002': param,
			'ql-1447003': param,
		});
		//利润表详细
		qlRes['ql-1447002'].dataList.forEach((obj) => {
			common.isApproximatelyEqualAssert({
				mat_code: info.code,
				mat_name: info.name,
				purprice: info.purprice, //进货价
				purmoney: common.mul(obj.num, info.purprice), //成本额
			}, obj);
		});
		//按单利润表
		qlRes['ql-1447003'].dataList.forEach((obj) => {
			common.isApproximatelyEqualAssert({
				purmoney: common.mul(obj.num, info.purprice), //成本额
			}, obj);
		});
	});

	//未验证成本额 后续用例验证
	it('190083.查看详细', async function () {
		TESTCASE = {
			describe: "日利润表详细界面的各项值是否正确",
			expect: "日利润表详细界面的值和销售开单-按明细查中的值一致",
		}
		let params = {
			'ql-1447002': {
				// today: true,//参数prodate1和prodate2是无效的
				invid: LOGINDATA.invid,
				prodate: common.getCurrentDate(),
			}, //利润明细
			'ql-1209': {
				today: true,
				shopid: LOGINDATA.invid,
			}, //销售开单-按明细查
		};
		let qlRes = await common.getResults(params);
		let exp = {};
		for (let i = 0, length = qlRes['ql-1209'].dataList.length; i < length; i++) {
			let obj = qlRes['ql-1209'].dataList[i];
			if (specNoProfit.includes(obj.mat_code)) continue; //排除不核算到利润的货品

			let key = `${obj.mat_code}-${obj.price}-${obj.discount}`; //相同款号-价格-折扣生成一条数据
			if (!exp[key]) {
				// mat_money 销售额->金额
				exp[key] = format.dataFormat(obj, 'mat_code;mat_name;num;price;discount;mat_money');
			} else {
				//二代特殊货品显示了数量，一代为0
				// if (USEECINTERFACE == 2)) { exp[key].num = common.add(exp[key].num, 1) };
				exp[key].num = common.add(exp[key].num, obj.num);
				exp[key].mat_money = common.add(exp[key].mat_money, obj.mat_money); //销售额
			};
		};
		common.isApproximatelyArrayAssert(Object.values(exp), qlRes['ql-1447002'].dataList);
		qlRes['ql-1447002'].dataList.forEach((obj) => {
			expect(Number(obj.alltotal)).to.equal(common.sub(obj.mat_money, obj.purmoney)); //利润额验证
		});
	});
	//未验证成本额 后续用例验证
	it('190084.按单利润表', async function () {
		TESTCASE = {
			describe: "日利润表详细界面-按单利润表中数据是否正确",
			expect: "日利润表详细界面-按单利润表中数据和销售开单-按明细查中的值一致",
			jira: 'SLHSEC-6838和SLHSEC-7484',
		}
		this.timeout(300000); //销售开单-按明细查有时数据过多
		let params = {
			'ql-1447003': {
				today: true,
				invid: LOGINDATA.invid,
				shopid: LOGINDATA.invid, //二代传字段
			}, //日利润表-详细界面-按单利润表
			'ql-1209': {
				today: true,
				invid: LOGINDATA.invid,
				shopid: LOGINDATA.invid, //二代传字段
			}, //销售开单-按明细查
		};
		let qlRes = await common.getResults(params);

		let exp = {};
		for (let i = 0, length = qlRes['ql-1209'].dataList.length; i < length; i++) {
			let obj = qlRes['ql-1209'].dataList[i];
			if (specNoProfit.includes(obj.mat_code)) continue; //排除不核算到利润的货品

			let key = `${obj.billno}`;
			if (!exp[key]) {
				// purmoney: '成本额',profit: '利润额'
				exp[key] = format.dataFormat(obj, 'billno;prodate;dw_name;num;mat_money');
			} else {
				exp[key].num = common.add(exp[key].num, obj.num);
				exp[key].mat_money = common.add(exp[key].mat_money, obj.mat_money); //销售额
			};
		};
		common.isApproximatelyArrayAssert(Object.values(exp), qlRes['ql-1447003'].dataList);
		qlRes['ql-1447003'].dataList.map((obj) => {
			expect(Number(obj.profit)).to.equal(common.sub(obj.mat_money, obj.purmoney)); //利润额验证
		});
	});

	//以相同条件查询,ql-1447001结果唯一 = ql-1447002汇总总和 = ql-1447003 汇总总和
	describe('查询', function () {
		let qlRes = {};
		before(async () => {
			let param = format.qlParamsFormat({
				invid: LOGINDATA.invid,
				styleid: BASICDATA.styleidAgc001, //二代 需要用租户级的id
				stylename: styleInfo.name,
				dwid: BASICDATA.dwidXw,
				dwtypeid: clientInfo.typeid,
				deliver: LOGINDATA.id,
				providerid: styleInfo.dwid,
				brandid: styleInfo.brandid,
				season: styleInfo.season,
				marketdate1: styleInfo.marketdate,
				marketdate2: styleInfo.marketdate,
				prodate: common.getCurrentDate(),
			}, true);
			//console.log(`BASICDATA : ${JSON.stringify(BASICDATA)}`);
			let params = {
				'ql-1447001': param, //利润表
				'ql-1447002': param, //利润明细
				'ql-1447003': param, //按单利润表
			};
			qlRes = await common.getResults(params);
			// console.log(`qlRes = ${JSON.stringify(qlRes)}`);
		});
		it('190087.利润表查询', async function () {
			expect(qlRes['ql-1447001'].count).to.equal('1');
			common.isApproximatelyEqualAssert({
				invid: LOGINDATA.invid,
				invname: LOGINDATA.invname,
				prodate: common.getCurrentDate(),
			}, qlRes['ql-1447001'].dataList[0]);
		});
		it('190139.利润明细查询', async function () {
			expect(Number(qlRes['ql-1447002'].count), `利润明细查询无结果`).to.above(0);
			qlRes['ql-1447002'].dataList.map((obj) => {
				expect(obj, `利润表明细查询结果与查询条件不匹配`).to.includes({
					mat_code: styleInfo.code,
					mat_name: styleInfo.name
				});
			});
		});
		// obj1[totalmoney]:800.000 不等于 obj2[mat_money]:400
		// obj1[profit]:600.00000 不等于 obj2[alltotal]:200: expected false to be true
		it('190085.日利润表和详细界面利润额总和一致检查', async function () {
			TESTCASE = {
				describe: '日利润表和详细界面的汇总值一致',
				expect: 'ql-1447001查询出来的值=ql-1447002的汇总值',
				jira: 'SLHSEC-6806',
			}
			let [sum1] = [{}];
			qlRes['ql-1447002'].dataList.map((obj) => common.addObject(sum1, obj));
			//利润表和利润明细一致性检查
			common.isFieldsEqualAssert(qlRes['ql-1447001'].dataList[0], sum1, 'totalnum=num;totalmoney=mat_money;pursum=purmoney;profit=alltotal');
		});
		it('190085.明细利润额和按单利润总和一致检查', async function () {
			TESTCASE = {
				describe: '日利润表详细界面的值和按单利润表中汇总值一致',
				expect: 'ql-1447002的汇总值=ql-1447003的汇总值',

			}
			let [sum1, sum2] = [{}, {}];
			qlRes['ql-1447002'].dataList.map((obj) => common.addObject(sum1, obj));
			qlRes['ql-1447003'].dataList.map((obj) => common.addObject(sum2, obj));

			//利润明细和按单利润总和一致检查
			common.isFieldsEqualAssert(sum1, sum2, 'num;alltotal=profit;mat_money;purmoney');
		})
	});

	describe('成本额检查', function () {
		before(async () => {
			//	await common.setGlobalParam('updatepricewhenpurin', 1); //采购入库更新款号进货价开启
		})

		after(async () => {
			await common.setGlobalParam('fin_price_base', 0); //财务中货品成本价的核算方法 按最新进货价
			//await common.setGlobalParam('updatepricewhenpurin', 0);
		});
		it('190044.最新进货价', async function () {
			await common.setGlobalParam('fin_price_base', 0); //财务中货品成本价的核算方法 按最新进货价
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品

			let json = basiceJsonparam.purchaseJson();
			json.details[0].styleid = goodRes.pk;
			await common.editBilling(json); //采购入库—新增入库-进货价100

			json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].num = 1;
			json.details[0].price = 200;
			await common.editBilling(json); //销售开单-开单-销售价(1个)200,利润=1*(200-100)

			//修改款号进货价110
			let info = await common.fetchMatInfo(goodRes.pk);
			//console.log(`info : ${JSON.stringify(info)}`);
			info.pk = goodRes.pk;
			info.purprice = 110;
			info.action = 'edit';
			goodRes = await basicReqHandler.editStyle({
				jsonparam: info,
			});
			//console.log(`goodRes : ${JSON.stringify(goodRes)}`);
			expect(goodRes, `修改货品失败`).not.to.have.property('error');
			await common.editBilling(json); //销售开单-开单-销售价(1)200
			await common.delay(1000);
			let qlRes = await common.callInterface('ql-1447001', format.qlParamsFormat({
				styleid: goodRes.pk,
				invid: LOGINDATA.invid,
			}, true));
			common.isApproximatelyEqualAssert({
				purprice: info.purprice, //进货价
				totalmoney: '400', //销售额
				invid: LOGINDATA.invid,
				profit: '180', //利润额
				invname: LOGINDATA.invname,
				pursum: '220', //成本额
				totalnum: '2',
			}, qlRes.dataList[0]);
		});
		it('190045.按当时进货价', async function () {
			TESTCASE = {
				describe: "日利润表-成本额检查-按当时进货价",
				expect: `purprice=100;totalmoney=400;invid=${LOGINDATA.invid};profit=190;invname=${LOGINDATA.invname};pursum=210;totalnum=2`,
				jira: 'SLHSEC-6746,profit值计算有误'
			}
			await common.setGlobalParam('fin_price_base', 3); //财务中货品成本价的核算方法 按当时进货价
			let goodRes = await basicReqHandler.editStyle({
				jsonparam: basiceJsonparam.addGoodJson(),
			}); //新增货品采购价100
			let json = basiceJsonparam.purchaseJson();
			json.details[0].styleid = goodRes.pk;
			await common.editBilling(json); //采购入库—新增入库-进货价100

			let param = format.qlParamsFormat({
				styleid: goodRes.pk,
				invid: LOGINDATA.invid,
			}, true);
			//let qlRes = await common.callInterface('ql-1447001', param);
			json = basiceJsonparam.simpleSalesJson();
			json.details[0].styleid = goodRes.pk;
			json.details[0].num = 1;
			json.details[0].price = 200;
			await common.editBilling(json); //销售开单-开单-销售价(1个)200,利润=1*(200-100)

			//修改款号进货价110
			let info = await common.fetchMatInfo(goodRes.pk);
			info.pk = goodRes.pk;
			info.purprice = 110;
			info.action = 'edit';
			goodRes = await basicReqHandler.editStyle({
				jsonparam: info,
			});
			await common.editBilling(json); //销售开单-开单-销售价(1)200

			let qlRes = await common.callInterface('ql-1447001', param);
			common.isApproximatelyEqualAssert({
				purprice: '100',
				totalmoney: '400',
				invid: LOGINDATA.invid,
				profit: '190',
				invname: LOGINDATA.invname,
				pursum: '210',
				totalnum: '2',
			}, qlRes.dataList[0]);
		});
	});
});
