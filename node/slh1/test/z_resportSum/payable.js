'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');


describe('应付调整单-slh2', function () {
	this.timeout('30000');
	let dwInfo = {}; //供应商信息

	before(async () => {
		await common.loginDo({
			//	'pass': '000000',
		});
		BASICDATA = await getBasicData.getBasicID();

		dwInfo = await common.callInterface('qf-2241', {
			'pk': BASICDATA.dwidVell,
		}); //获取供应商信息
	});

	it('190196.新增', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill(); //新增应付调整单
		//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
		let qfRes = await reqHandler.queryBilling({
			interfaceid: 'qf-1346',
			pk: sfRes.result.pk
		}); //获取应付调整单明细值
		common.isApproximatelyEqualAssert(Object.assign({
			show_dwid: dwInfo.nameshort,
			show_invid: LOGINDATA.invname,
			show_deliver: `${LOGINDATA.code},${LOGINDATA.name}`, //USEECINTERFACE == 1 ? `${LOGINDATA.code},${LOGINDATA.name}` : LOGINDATA.name,
		}, sfRes.params), qfRes.result);
	});

	it('190198.供应商和经办人,输入不存在的内容', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill({
			jsonparam: {
				dwid: 'undefined',
			},
			check: false,
		}); //新增应付调整单
		expect(sfRes.result.error).to.includes('服务端错误');

		sfRes = await resSumReqHandler.editPaybleBill({
			jsonparam: {
				deliver: 'undefined',
			},
			check: false,
		}); //新增应付调整单
		expect(sfRes.result.error).to.includes('服务端错误');
	});

	//可以正常保存,由客户端控制
	it.skip('190199.供应商栏输入厂商-客户端控制', async function () { });

	it('190204.金额从正数改为负数', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill();
		if (USEECINTERFACE == 2) {
			await common.delay(1800);
		};
		let param = format.qlParamsFormat({
			id1: sfRes.result.billno,
			id2: sfRes.result.billno,
			shopid: LOGINDATA.invid,
			type: sfRes.params.type,
		}, true);
		let qlRes = await common.callInterface('ql-1906', param); //统计分析-汇总表-经营历程
		expect(qlRes.dataList[0], `单据信息:${JSON.stringify(sfRes)},\n统计分析-汇总表-经营历程查询结果错误:${JSON.stringify(qlRes.dataList[0])}\n`).to.includes({
			finpayBalancetype: '未付'
		});

		sfRes = await resSumReqHandler.editPaybleBill({
			jsonparam: {
				pk: sfRes.result.pk,
				totalmoney: -common.getRandomNumStr(4),
			},
		});
		if (USEECINTERFACE == 2) {
			await common.delay(500);
		};
		qlRes = await common.callInterface('ql-1906', param);
		expect(qlRes.dataList[0], `单据信息:${JSON.stringify(sfRes)},\n统计分析-汇总表-经营历程查询结果错误:${JSON.stringify(qlRes.dataList[0])}`).to.includes({
			finpayBalancetype: '退货'
		});
	});

	it('190211.修改门店', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill();
		sfRes = await resSumReqHandler.editPaybleBill({
			jsonparam: {
				pk: sfRes.result.pk,
				invid: BASICDATA.shopidZzd,
			},
			check: false,
		});
		expect(sfRes.result).to.includes({
			error: USEECINTERFACE == 1 ? '不允许修改单据所属门店' : '不允许修改门店',
		});
	});

	it('190206.组合查询', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill();

		let param = format.qlParamsFormat({
			invid: LOGINDATA.invid,
			billno: sfRes.result.billno,
			billno1: sfRes.result.billno,
			billno2: sfRes.result.billno,
			dwid: sfRes.params.dwid,
			remark: sfRes.params.remark,
			invalidflag: 0,
		}, true);
		let qlRes = await common.callInterface('ql-1346', param);
		let exp = {
			optime: sfRes.optime,
			totalmoney: sfRes.params.totalmoney,
			id: sfRes.result.pk,
			invname: LOGINDATA.invname,
			dwname: dwInfo.nameshort,
			remark: sfRes.params.remark,
			staffName: LOGINDATA.name,
			invalidflag: '0',
			invalidflagname: "否",
			billno: sfRes.result.billno,
			prodate: sfRes.params.prodate,
			opname: LOGINDATA.name,
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	it('190210.作废', async function () {
		let sfRes = await resSumReqHandler.editPaybleBill();
		await reqHandler.csIFCHandler({
			interfaceid: 'cs-1346',
			pk: sfRes.result.pk
		});
		let param = format.qlParamsFormat({
			invid: LOGINDATA.invid,
			billno: sfRes.result.billno,
			invalidflag: 1,
		}, true);
		let qlRes = await common.callInterface('ql-1346', param);
		let exp = {
			optime: sfRes.optime,
			totalmoney: sfRes.params.totalmoney,
			id: sfRes.result.pk,
			invname: LOGINDATA.invname,
			remark: sfRes.params.remark,
			staffName: LOGINDATA.name,
			invalidflag: '1',
			invalidflagname: '是',
			billno: sfRes.result.billno,
			prodate: sfRes.params.prodate,
			opname: LOGINDATA.name,
		};
		common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
	});

	//一代 整数:9位,小数:3位; 二代 整数:16位,小数:3位
	//期末金额有限制，因此需要对刚刚做的边界值测试的单据要进行作废，否则会报错
	describe.skip('190202.金额输入较大数字', function () {
		let maximum;
		before(async () => {
			maximum = USEECINTERFACE == 1 ? 9 : 13; //不确定后续是否会更改
		});
		it('1.边界值', async function () {

			let sfRes = await resSumReqHandler.editPaybleBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(maximum),
				},
			}); //新增应付调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1346',
				pk: sfRes.result.pk
			}); //获取应付调整单明细值

			await reqHandler.csIFCHandler({
				interfaceid: 'cs-1346',
				pk: sfRes.result.pk
			}); //作废

			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.超出边界值', async function () {
			TESTCASE = {
				describe: '检查应收调整单边界值',
				expect: '二代输入超过13位整数，一代输入9位整数，不允许保存成功'
			}
			let sfRes = await resSumReqHandler.editPaybleBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(maximum + 1), //超出边界
				},
				check: false,
			}); //新增应付调整单
			if (sfRes.result.pk) {
				await reqHandler.csIFCHandler({
					interfaceid: 'cs-1346',
					pk: sfRes.result.pk,
				}); //作废
			};
			expect(sfRes.result).to.have.property('error');
		});
	});

	describe('190197.检查必填项', function () {
		const errorMsgs = () => {
			return USEECINTERFACE == '1' ? {
				// deliver: '', //客户端判断
				dwid: `服务端错误`,
				invid: '门店获取出错',
				totalmoney: '空单不允许保存',
				// prodate: '', //客户端判断
			} : {
					dwid: `参数[compId]必填`,
					invid: '参数[shopId]必填',
					totalmoney: '参数[payMoney]必填',
				};
		};
		const keys = ['dwid', 'invid', 'totalmoney'];
		for (let key of keys) {
			it(`不填写${key},检查开单结果`, async function () {
				let jsonparam = {};
				jsonparam[key] = '';
				let sfRes = await resSumReqHandler.editPaybleBill({
					jsonparam: jsonparam,
					check: false,
				}); //新增应付调整单
				expect(sfRes.result.error).to.includes(errorMsgs()[key]);
			});
		};
	});

	describe('190203.金额输入0,正数,负数', function () {
		it('1.正数', async function () {
			let sfRes = await resSumReqHandler.editPaybleBill({
				jsonparam: {
					totalmoney: common.getRandomNumStr(4),
				},
			}); //新增应付调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1346',
				pk: sfRes.result.pk
			}); //获取应付调整单明细值
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('2.负数', async function () {
			let sfRes = await resSumReqHandler.editPaybleBill({
				jsonparam: {
					totalmoney: -common.getRandomNumStr(4),
				},
			}); //新增应付调整单
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1346',
				pk: sfRes.result.pk
			}); //获取应付调整单明细值
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('3.输入0', async function () {
			let sfRes = await resSumReqHandler.editPaybleBill({
				jsonparam: {
					totalmoney: 0,
				},
				check: false,
			}); //新增应付调整单
			expect(sfRes.result).to.includes({
				error: USEECINTERFACE == 1 ? '空单不允许保存' : '空单据不允许保存!',
			});
		});
	});

	//需要常青店001财务员 004店长
	describe('权限检查', function () {
		before(async () => {
			await resSumReqHandler.editPaybleBill({
				jsonparam: {
					invid: BASICDATA.shopidZzd,
				},
			}); //新增其他门店的应付调整单
		});
		after(async () => {
			await common.loginDo();
		});
		it('190200.店长', async function () {
			await common.loginDo({
				'logid': '004',
				'pass': '000000',
			});

			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
			}, true);
			//log(`param : ${JSON.stringify(param)}`);
			let qlRes = await common.callInterface('ql-1346', param);
			//console.log(`qlRes : ${JSON.stringify(qlRes)}`);
			expect(qlRes.count, '店长可以看到其他门店数据错误').to.equal('0');

			param.invid = '';
			qlRes = await common.callInterface('ql-1346', param);
			//console.log(`qlRes : ${JSON.stringify(qlRes)}`);
			qlRes.dataList.map((obj) => {
				expect(obj.invname, `店长权限,应付调整单显示其他门店数据`).to.equal(LOGINDATA.invname);
			});
		});
		it('190212.财务员查看数据', async function () {
			await common.loginDo({
				'logid': '001',
				'pass': '000000',
			});

			let param = format.qlParamsFormat({
				invid: BASICDATA.shopidZzd,
			}, true);
			let qlRes = await common.callInterface('ql-1346', param);
			expect(Number(qlRes.count), '财务员无法看到其他门店数据').to.above(0);
		});
	});

	describe('数据校验', function () {
		let sfRes = {};
		before(async () => {
			sfRes = await resSumReqHandler.editPaybleBill();
		});
		it('190213.1.往来管理-厂商门店帐-明细查询', async function () {
			let param = format.qlParamsFormat({
				pagesize: 1, //只获取需要验证的那条数据
				dwid: sfRes.params.dwid,
				shopid: LOGINDATA.invid,
			}, false);
			let qlRes = await common.callInterface('ql-13471', param); //厂商门店帐-明细
			let exp = {
				totalmoney: sfRes.params.totalmoney,
				id: sfRes.result.pk,
				remark: sfRes.params.remark,
				finpayVerifyothersum: 0,
				typename: "应付调整单",
				finpayPaysum: 0,
				shopname: LOGINDATA.invname,
				billno: sfRes.result.billno,
				prodate: sfRes.params.prodate,
			};
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('190213.2.invmain信息', async function () {
			let qfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-1935',
				pk: sfRes.result.pk,
				bizType: 5600, //应付调整单
			});
			let qfRes2 = await reqHandler.queryBilling({
				interfaceid: 'qf-1346',
				pk: sfRes.result.pk
			}); //获取应付调整单明细值
			// Object.assign(target,object),把object复制到target，并返回新的obj
			common.isApproximatelyEqualAssert(qfRes.result, Object.assign(qfRes2.result, {
				billno: sfRes.result.billno,
				typename: '应付调整单',
			}), ['type']); //先忽略type，二代有个bizType指向了type
		});
	});
});
