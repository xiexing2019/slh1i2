'use strict';
const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const resSumReqHandler = require('../../help/resportSumHelp/resportSumRequestHandler');
const basiceJsonparam = require('../../help/basiceJsonparam');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

describe('按汇总', function () {
	this.timeout(30000);
	let styleInfo = {};
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		// //console.log(BASICDATA);
		styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
		// //console.log(styleInfo);
	});

	describe('退货表', function () {
		it('190068.查询', async function () {
			let qlRes = await common.callInterface('ql-1728', format.qlParamsFormat({
				styleid: BASICDATA.styleidAgc001,
				styleName: styleInfo.name,
				invid: LOGINDATA.invid,
			}, true)); //这里有个true，
			// //console.log(`qlRes : ${JSON.stringify(qlRes)}`);
			expect(qlRes.count).to.equal('1'); //查询结果唯一
			expect(qlRes.dataList[0]).to.includes({
				// id: '',//这个id只返回了其中一个单据的qk值，有问题，跳过验证
				// num: '', //190071验证
				invid: LOGINDATA.invid,
				stylename: styleInfo.name,
				invname: LOGINDATA.invname,
				stylecode: styleInfo.code,
			});
		});
		it('190071.款号退货数检查', async function () {
			let params = {
				'ql-1728': {
					today: true,
				},
				'ql-14489': {
					today: true,
				}, //销售开单-按退货汇总
			};
			let qlRes = await common.getResults(params);

			//销售开单-按退货汇总没有具体款号信息,需要进入详细界面获取
			let promises = qlRes['ql-14489'].dataList.map((obj) => {
				let param = format.qlParamsFormat({
					typeid: obj.typeid,
					invid: obj.invid
				}, true);
				return common.callInterface('ql-144891', param); //按退货汇总明细
			});
			let results = await Promise.all(promises); //获取所有退货信息

			let exp = {};
			for (let i = 0, length = results.length; i < length; i++) {
				let invid = qlRes['ql-14489'].dataList[i].invid;
				results[i].dataList.map((obj) => {
					let key = `${invid}-${obj.mat_code}`;
					if (exp[key]) {
						exp[key].num = common.add(exp[key].num, Math.abs(obj.num));
					} else {
						exp[key] = {
							invid: invid,
							stylecode: obj.mat_code,
							num: Math.abs(obj.num),
						};
					};
				});
			};
			common.isApproximatelyArrayAssert(Object.values(exp), qlRes['ql-1728'].dataList);
		});
	});
});
