const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const basiceJson = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData.js');

const mainReqHandler = require('../../help/basicInfoHelp/mainDataReqHandler');

describe('员工管理', function () {
    this.timeout(30000);
    //随机排序店员类别
    let depidList, roleList,
        clerkTypes = common.randomSort([{ 0: '普通店员' }, { 1: '店长' }, { 2: '单日店助' }, { 3: '双日店助' }, { 9: '后勤人员' }]);
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        //随机排序门店
        depidList = await common.callInterface('cs-curdetname', {
            epid: LOGINDATA.epid
        }).then(res => common.randomSort(res.dataList));
        // console.log(`depidList=${JSON.stringify(depidList)}`);
        roleList = await mainReqHandler.getRoleList(LOGINDATA.epid).then(res => common.randomSort(res.result.dataList));
        // console.log(`\nroleList=${JSON.stringify(roleList)}`);
    });
    it('停用的员工登录', async function () {


    });
    describe('员工管理主流程', function () {
        let saveStaffRes, staffInfo, editStaffRes;
        before(async () => {
            //新增员工
            saveStaffRes = await mainReqHandler.saveStaff(basiceJson.saveStaffJson({
                roleids: roleList[0].id,
                depid: depidList[0].id,
                clerktype: Object.keys(clerkTypes[0]).toString(),
            }));
            // console.log(`saveStaffRes=${JSON.stringify(saveStaffRes)}`);
            staffInfo = await mainReqHandler.getStaffInfo({ pk: saveStaffRes.result.pk });
            // console.log(`staffInfo=${JSON.stringify(staffInfo)}`);
        });
        after(async () => {
            //停用员工
            await mainReqHandler.disableStaff({ pk: saveStaffRes.result.pk })
        });
        it('员工详情验证', async function () {
            common.isApproximatelyEqualAssert(Object.assign({}, saveStaffRes.params.jsonparam, { pk: saveStaffRes.result.pk, delflag: "0", assistantFlag: 0, spAssistantFlag: 0 }), staffInfo);
        });
        it('员工列表验证', async function () {
            let staffList = await mainReqHandler.getStaffList({ delflag: 0, code: saveStaffRes.params.jsonparam.code, roleid1: saveStaffRes.params.jsonparam.roleids }).then(res => res.result.dataList.find(obj => obj.id == saveStaffRes.result.pk));
            // console.log(`staffList=${JSON.stringify(staffList)}`);
            expect(staffList).to.not.be.undefined;
            let exp = getStaffListExp(staffInfo);
            exp.qq = saveStaffRes.params.jsonparam.qq;
            // console.log(`exp=${JSON.stringify(exp)}`);
            common.isApproximatelyEqualAssert(exp, staffList);
        });
        it('新增的员工登录', async function () {
            await common.loginDo({ logid: saveStaffRes.params.jsonparam.code })
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            common.isApproximatelyEqualAssert(saveStaffRes.params.jsonparam, LOGINDATA);
        });
        it('相同员工工号验证', async function () {
            await common.loginDo();
            let sfRes = await mainReqHandler.saveStaff(basiceJson.saveStaffJson({
                code: saveStaffRes.params.jsonparam.code,
                roleids: roleList[0].id,
                depid: depidList[0].id,
                clerktype: Object.keys(clerkTypes[0]).toString(),
                check: false,
            }));
            // console.log(`sfRes=${JSON.stringify(sfRes)}`);
            expect(sfRes.result, `新增相同工号的员工成功:${sfRes}`).to.include({ msgId: 'staff_code_exists', error: '员工工号重复' });
        });
        //新增相同手机号员工，一代不限制，二代限制
        describe('修改员工信息', function () {
            let editStaffInfo;
            before(async () => {
                await common.loginDo();
                editStaffRes = await mainReqHandler.saveStaff(basiceJson.saveStaffJson({
                    pk: saveStaffRes.result.pk,
                    birthday: common.getDateString([0, 0, -1]),
                    sex: 2,
                    roleids: roleList[1].id,
                    depid: depidList[0].id,
                    clerktype: Object.keys(clerkTypes[1]).toString(),
                    check: false
                }));
                // console.log(`editStaffRes=${JSON.stringify(editStaffRes)}`);
                if (editStaffRes.result.error) {
                    [saveStaffRes, editStaffRes] = [editStaffRes, saveStaffRes];
                    throw new Error(`修改员工信息出错:${saveStaffRes}`);
                };
                // console.log(`修改editStaffRes=${JSON.stringify(editStaffRes)}`);
                editStaffInfo = await mainReqHandler.getStaffInfo({ pk: editStaffRes.result.pk });
                // console.log(`修改editStaffInfo=${JSON.stringify(editStaffInfo)}`);
            });
            it('修改后员工详情验证', async function () {
                common.isApproximatelyEqualAssert(Object.assign({}, editStaffRes.params.jsonparam, { delflag: "0", assistantFlag: 0, spAssistantFlag: 0 }), editStaffInfo);
            });
            it('修改后员工列表验证', async function () {
                let staffList = await mainReqHandler.getStaffList({ delflag: 0, code: editStaffRes.params.jsonparam.code, roleid1: editStaffRes.params.jsonparam.roleids }).then(res => res.result.dataList.find(obj => obj.id == editStaffRes.result.pk));
                // console.log(`修改staffList=${JSON.stringify(staffList)}`);
                expect(staffList).to.not.be.undefined;
                let exp = getStaffListExp(editStaffInfo);
                // console.log(`修改exp=${JSON.stringify(exp)}`);
                exp.qq = editStaffRes.params.jsonparam.qq;
                common.isApproximatelyEqualAssert(exp, staffList);
            });
            it('修改信息后员工登录', async function () {
                await common.loginDo({ logid: editStaffInfo.code })
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                common.isApproximatelyEqualAssert(editStaffRes.params.jsonparam, LOGINDATA);
            });
        });
        describe('停用员工', function () {
            before(async () => {
                //停用员工
                await common.loginDo();
                await mainReqHandler.disableStaff({ pk: editStaffRes.result.pk });
                staffInfo = await mainReqHandler.getStaffInfo({ pk: editStaffRes.result.pk });
                // console.log(`staffInfo=${JSON.stringify(staffInfo)}`);
            });
            it('停用员工后员工详情验证', async function () {
                common.isApproximatelyEqualAssert(Object.assign({}, editStaffRes.params.jsonparam, { pk: editStaffRes.result.pk, delflag: "1", assistantFlag: 0, spAssistantFlag: 0 }), staffInfo);
            });
            it('停用员工后员工列表验证', async function () {
                let staffList = await mainReqHandler.getStaffList({ delflag: 1, clerktype: editStaffRes.params.jsonparam.clerktype, code: editStaffRes.params.jsonparam.code, roleid1: editStaffRes.params.jsonparam.roleids }).then(res => res.result.dataList.find(obj => obj.id == editStaffRes.result.pk));
                // console.log(`停用staffList=${JSON.stringify(staffList)}`);
                expect(staffList).to.not.be.undefined;
                let exp = getStaffListExp(staffInfo);
                // console.log(`停用exp=${JSON.stringify(exp)}`);
                exp.qq = editStaffRes.params.jsonparam.qq;
                common.isApproximatelyEqualAssert(exp, staffList);
            });
            it('停用的员工登录', async function () {
                try {
                    await common.loginDo({ logid: staffInfo.code });
                } catch (err) {
                    expect(err.message).to.include('登录工号不存在');
                };
            });
        });
        describe('启用员工', function () {
            before(async () => {
                //启用员工
                await common.loginDo();
                await mainReqHandler.enableStaff({ pk: editStaffRes.result.pk });
                staffInfo = await mainReqHandler.getStaffInfo({ pk: editStaffRes.result.pk });
                // console.log(`staffInfo=${JSON.stringify(staffInfo)}`);
            });
            it('启用员工后员工详情验证', async function () {
                common.isApproximatelyEqualAssert(Object.assign({}, editStaffRes.params.jsonparam, { pk: editStaffRes.result.pk, delflag: "0", assistantFlag: 0, spAssistantFlag: 0 }), staffInfo);
            });
            it('启用员工后员工列表验证', async function () {
                let staffList = await mainReqHandler.getStaffList({ delflag: 0, code: editStaffRes.params.jsonparam.code, roleid1: editStaffRes.params.jsonparam.roleids }).then(res => res.result.dataList.find(obj => obj.id == editStaffRes.result.pk));
                // console.log(`启用staffList=${JSON.stringify(staffList)}`);
                expect(staffList).to.not.be.undefined;
                let exp = getStaffListExp(staffInfo);
                // console.log(`启用exp=${JSON.stringify(exp)}`);
                exp.qq = editStaffRes.params.jsonparam.qq;
                common.isApproximatelyEqualAssert(exp, staffList);
            });
            it('启用的员工登录', async function () {
                await common.loginDo({ logid: staffInfo.code })
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                common.isApproximatelyEqualAssert(editStaffRes.params.jsonparam, LOGINDATA);
            });
        });
    });

    describe('权限检查', function () {
        before(async () => {
            await common.loginDo({ logid: '005' });
        });
        it('查询条件输入其他门店店长登录查询列表', async () => {
            let staffList = await mainReqHandler.getStaffList({ depid: BASICDATA.shopidZzd });
            expect(staffList.result.dataList.length, `店长登录查询到了其他门店员工:${staffList}`).to.equal(0);

        });
        it('门店查询条件不输入其他门店店长登录查询列表', async () => {
            let staffList = await mainReqHandler.getStaffList().then(res => res.result.dataList);
            expect(staffList.length, `不输入查询条件查询员工结果为空`).to.above(0);
            staffList.forEach(ele => {
                expect(ele.depname, `店长登录查询到了其他门店的员工`).to.equal(LOGINDATA.depname);
            });
        });
    });

});

function getStaffListExp(staffInfo) {
    return format.dataFormat(staffInfo, 'id=pk;code;name;depname=show_depid;delflag;spAssistantFlag=show_spAssistantFlag;mobile;rolename=show_roleids;clerktype=show_clerktype;assistantFlag=show_assistantFlag;addr');
};
