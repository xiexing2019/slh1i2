const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const basiceJsonparam = require('../../help/basiceJsonparam');
const invAskJson = require('../../help/json/invAskJson');
const reqHandler = require('../../help/reqHandlerHelp');
const mainReqHandler = require('../../help/basicInfoHelp/mainDataReqHandler');
const basicReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');
const invAskReqHandler = require('../../help/invAskHelp/invAskRequestHandler');
const purReqHandler = require('../../help/purchaseHelp/purRequestHandler');
const inOutHandler = require('../../help/shopInOutHelp/shopInOutRequestHandler.js');
const billExp = require('../../getExp/billExp');

//http://jira.hzdlsoft.com:7082/browse/SLH-24092

/**
 * recvnum 已发数 checkprenum 发货差异数 moverecvnum 发货验货数（后台）
 * onroadnum 在途数 purednum待入库 ordernum报单数
 */
describe('要货补货管理', function () {
    this.timeout(90000);
    let styleInfo, supplier, loginArr = [{ logidA: '000', logidB: '200' }, { logidA: '004', logidB: '204' }];//
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        await common.setGlobalParam('fin_move_price', 1);//	门店调拨是否可以填写价格 为门店调拨是否可以填写价格
        await common.setGlobalParam('movesumprice', 1);//调拨核算价格 调拨按销价1核算
        await common.setGlobalParam('sc_invAskSendAutoEnd', 0);
        await common.setGlobalParam('sc_invAskToPureInv', 1);//	要货报单采购订单所属门店 为报单门店（仓库店）
        await common.setGlobalParam('inv_allow_search_invnum', 1); //默认门店可自由查询各自库存
        styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
        // console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
    });

    for (const value of Object.values(loginArr)) {
        describe(`${value.logidA}要货单`, function () {
            let sfInvAskBill, qfInvAskBill, formListBef;
            before('新增要货单', async () => {
                //获取按要要货报单列表起始值
                await common.loginDo({ logid: value.logidB });
                formListBef = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getDateString([0, 0, -7]), prodate2: common.getCurrentDate() });
                await common.loginDo({ logid: value.logidA });
                //新增要货单
                sfInvAskBill = await invAskReqHandler.saveInvAskBill(invAskJson.invAskBillJson({ styleInfo }));
                // console.log(`新增sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);
                //获取要货单详情
                qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                // console.log(`qfInvAskBill=${JSON.stringify(qfInvAskBill)}`);
            });
            describe('常青门店单据验证', function () {
                it('要货单详情验证', async function () {
                    // console.log(`新增要货详情qfInvAskBill=${JSON.stringify(qfInvAskBill)}`);
                    // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);
                    let exp = _.cloneDeep(sfInvAskBill.params);
                    exp = Object(exp, getAskBillInfoFlagExp(sfInvAskBill));
                    exp.flag = 0;//状态
                    common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                });
                it('要货单按批次查', async function () {
                    //要货按批次查
                    let invAskBillList = await invAskReqHandler.getInvAskBillList({ invalidflag: 0, sendFlag: 0, orderFlag: 0 });
                    // console.log(`新增要货单按批次查invAskBillList=${JSON.stringify(invAskBillList.result.dataList[0])}`);
                    let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                    // console.log(`新增要货单按批次查exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                });
                it('要货单按明细查', async function () {
                    //要货按明细查
                    let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                    // console.log(`invAskBillDetList=${JSON.stringify(invAskBillDetList.result.dataList.slice(0, 2))}`);
                    let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                    // console.log(`新增要货单按明细查exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                });
            });
            describe('中洲店验证', function () {
                before(async () => {
                    //中洲店登录
                    await common.loginDo({ logid: value.logidB });
                    // await common.loginDo({ logid: 200 });
                });
                it('仓库店-要货调拨列表', async function () {
                    let shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList({ sendFlag: 0, invalidflag: 0 });
                    // console.log(`要货调拨列表shopOutListForAsk=${JSON.stringify(shopOutListForAsk.result.dataList[0])}`);
                    let exp = getShopOutListForAskExp({ qfInvAskBill, sfInvAskBill });
                    // console.log(`要货调拨列表exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, shopOutListForAsk.result.dataList[0]);
                });
                it('仓库店-要货调拨页面详情验证', async function () {
                    let shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                    // console.log(`要货调拨详情shopOutInfoForAsk=${JSON.stringify(shopOutInfoForAsk)}`);
                    let exp = getShopOutInfoForAskExp(qfInvAskBill);
                    // console.log(`\n要货调拨详情exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);

                });
                it('要货报单列表验证', async function () {
                    let exp = getAskFormListExp({ qfInvAskBill });
                    let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getDateString([0, 0, -7]), prodate2: common.getCurrentDate() });
                    // console.log(`formList.result=${JSON.stringify(formList)}`);
                    // console.log(`formListBef.result.dataList.length=${JSON.stringify(formListBef)}`);

                    formListBef.result.dataList.length == 0 ? common.isApproximatelyEqualAssert(exp, formList.result.dataList[0]) : common.isApproximatelyEqualAssert(common.addObject(_.cloneDeep(formListBef.result.dataList[0]), { restnum: exp.restnum }), formList.result.dataList[0]);
                    common.isApproximatelyEqualAssert(common.add(formListBef.result.sumrow.restnum, exp.restnum), formList.result.sumrow.restnum);
                });
                it('要货报单详情验证', async function () {
                    let formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });

                    let exp = getAskFormInfoExp(qfInvAskBill);
                    // console.log(`exp=${JSON.stringify(exp)}`);
                    // console.log(`formInfo.result.details[0]=${JSON.stringify(formInfo.result.details[0])}`);
                    common.isArrayContainArrayAssert(exp, formInfo.result.details);
                    common.isApproximatelyEqualAssert({
                        dwid: BASICDATA.dwidVell,
                        invid: BASICDATA.shopidZzd,
                        prodate: common.getCurrentDate(),
                    }, formInfo.result);
                });

            });
            describe('修改要货单', function () {
                before(async () => {
                    //常青店登录
                    await common.loginDo({ logid: value.logidA });
                    // await common.loginDo();
                    let json = invAskJson.invAskBillJson({ styleInfo });
                    json.pk = sfInvAskBill.result.pk;
                    json.details.forEach((detail, index) => {
                        detail.rem = `修改明细${index + 1}`;
                    });
                    //修改要货单
                    sfInvAskBill = await invAskReqHandler.saveInvAskBill(json);
                    // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill.result)}`);
                    qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                });
                describe('常青门店单据验证', function () {
                    it('要货单详情验证', async function () {
                        let exp = _.cloneDeep(sfInvAskBill.params);
                        exp = Object(exp, getAskBillInfoFlagExp(sfInvAskBill));
                        exp.flag = 0;//状态
                        // console.log(`qfInvAskBill.result=${JSON.stringify(qfInvAskBill.result)}`);
                        // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);
                        common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);

                    });
                    it('要货单按批次查', async function () {
                        //要货按批次查
                        let invAskBillList = await invAskReqHandler.getInvAskBillList({ sendFlag: 0, orderFlag: 0, invalidflag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                    });
                    it('要货单按明细查', async function () {
                        //要货按明细查
                        invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                    });
                });
                describe('中洲店验证', function () {
                    before(async () => {
                        //中洲店登录
                        await common.loginDo({ logid: value.logidB });
                        // await common.loginDo({ logid: 200 });
                    });
                    it('仓库店-要货调拨列表', async function () {
                        let shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList({ sendFlag: 0, invalidflag: 0 });
                        let exp = getShopOutListForAskExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyEqualAssert(exp, shopOutListForAsk.result.dataList[0]);
                    });
                    it('仓库店-要货调拨页面详情验证', async function () {
                        let shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                        let exp = getShopOutInfoForAskExp(qfInvAskBill);
                        common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);
                    });
                    it('要货报单列表验证', async function () {
                        let exp = getAskFormListExp({ qfInvAskBill });
                        let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getDateString([0, 0, -7]), prodate2: common.getCurrentDate() });
                        // console.log(`formList.result=${JSON.stringify(formList.result)}`);
                        formListBef.result.dataList.length == 0 ? common.isApproximatelyEqualAssert(exp, formList.result.dataList[0]) : common.isApproximatelyEqualAssert(common.addObject(_.cloneDeep(formListBef.result.dataList[0]), { restnum: exp.restnum }), formList.result.dataList[0]);
                        common.isApproximatelyEqualAssert(common.add(formListBef.result.sumrow.restnum, exp.restnum), formList.result.sumrow.restnum);
                    });
                    it('要货报单详情验证', async function () {
                        let formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                        let exp = getAskFormInfoExp(qfInvAskBill);
                        common.isArrayContainArrayAssert(exp, formInfo.result.details);
                        common.isApproximatelyEqualAssert({
                            dwid: BASICDATA.dwidVell,
                            invid: BASICDATA.shopidZzd,
                            prodate: common.getCurrentDate(),
                        }, formInfo.result);
                    });
                });
            });
            describe('终结要货单', function () {
                before(async () => {
                    //常青店登录
                    await common.loginDo({ logid: value.logidA });
                    // await common.loginDo();
                    //终结要货单
                    await invAskReqHandler.endInvAskBill({ pk: sfInvAskBill.result.val || sfInvAskBill.result.pk });
                    qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.val || sfInvAskBill.result.pk });
                });
                describe('常青门店单据验证', function () {
                    it('要货单详情验证', async function () {
                        let exp = _.cloneDeep(sfInvAskBill);
                        exp = Object(exp, getAskBillInfoFlagExp(sfInvAskBill));
                        exp.flag = 0;//状态
                        exp.params.invalidflag = 1;//是否终结
                        common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                        // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);
                    });
                    it('要货单按批次查', async function () {
                        invAskBillList = await invAskReqHandler.getInvAskBillList({ sendFlag: 0, orderFlag: 0, invalidflag: 1, });
                        let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                    });
                    it('要货单按明细查', async function () {
                        invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList.slice(0, exp.length));
                    });
                });
                describe('中洲店验证', function () {
                    let shopOutInfoForAsk;
                    before(async () => {
                        //中洲店登录
                        await common.loginDo({ logid: value.logidB });
                        shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.val || sfInvAskBill.result.pk });
                    });
                    it('终结要货单后中洲店-要货调拨列表', async function () {
                        let shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList({ billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });//sendFlag: 0, orderFlag: 0, invalidflag: 1 wky已去掉
                        expect(shopOutListForAsk.result.dataList.length).to.equal(0);
                    });
                    it('终结要货单后中洲店-要货调拨页面详情验证', async function () {
                        let exp = getShopOutInfoForAskExp(qfInvAskBill);
                        common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);
                    });
                    it('终结要货单后按要货报单数据验证', async function () {
                        let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getDateString([0, 0, -7]), prodate2: common.getCurrentDate() });
                        // console.log(`formList.result=${JSON.stringify(formList)}`);
                        common.isApproximatelyEqualAssert(formListBef.result, formList.result);
                    });
                    it('仓库店-保存已终结的要货单', async function () {
                        shopOutInfoForAsk.result.check = false;
                        let sfShopOut = await invAskReqHandler.saveShopOutForInvAskBill(shopOutInfoForAsk.result);
                        // let sfShopOut = await invAskReqHandler.saveShopOutForInvAskBill({ jsonparam: shopOutInfoForAsk.result, check: false });
                        expect(sfShopOut.result).to.includes({ msgId: 'order_already_end_not_allow_send', error: '要货单已全部发货或已终结，不允许再发货' });
                    })
                });
            });
        });
        //部分发货，全部发货，多发货
        for (let i = 1; i < 4; i++) {
            describe(`${value.logidA}按要货调拨--主流程`, function () {
                let sfInvAskBill, formListBef, qfInvAskBill, sfShopOutForAsk, shopOutInfoForAsk, styleInvBef;
                before(async () => {
                    //开启参数“要货调拨后自动终结要货单”，调拨保存后自动终结要货单
                    //常青店登录，新增要货单
                    await common.loginDo({ logid: value.logidB });
                    //新增要货单
                    formListBef = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });

                    await common.loginDo({ logid: value.logidA });
                    sfInvAskBill = await invAskReqHandler.saveInvAskBill(invAskJson.invAskBillJson({ styleInfo }));
                    // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);
                    //获取要货门店初始库存

                    styleInvBef = await reqHandler.qlIFCHandler({
                        interfaceid: 'ql-1932',
                        propdresStyleid: styleInfo.pk,
                    });
                    //中洲店登录
                    await common.loginDo({ logid: value.logidB });
                    // await common.loginDo({ logid: 200 });
                    //要货调拨详情
                    shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                    // console.log(`shopOutInfo=${JSON.stringify(shopOutInfoForAsk)}`);
                    //要货调拨保存--发货
                    if (i == 1) {
                        shopOutInfoForAsk.result.details[0].num = common.sub(shopOutInfoForAsk.result.details[0].num, 5);
                    } else if (i == 3) {
                        shopOutInfoForAsk.result.details[0].num = common.add(shopOutInfoForAsk.result.details[0].num, 5);
                    };
                    // console.log(`shopOutInfoForAsk.result=${JSON.stringify(shopOutInfoForAsk.result)}`);
                    sfShopOutForAsk = await invAskReqHandler.saveShopOutForInvAskBill(shopOutInfoForAsk.result);
                    // console.log(`sfShopOutForAsk=${JSON.stringify(sfShopOutForAsk)}`);
                });
                describe('常青店--门店', function () {
                    before(async () => {
                        //常青店登录，
                        await common.loginDo({ logid: value.logidA });
                        qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                    });
                    it('门店要货单详情验证', async function () {
                        let exp = getAskBillInfoExp({ sfInvAskBill, sfRes: sfShopOutForAsk });
                        // console.log(`sfShopOutForAsk=${JSON.stringify(sfShopOutForAsk)}`);
                        // console.log(`常青店要货单详情exp=${JSON.stringify(exp)}`);
                        // console.log(`常青店要货单详情result=${JSON.stringify(qfInvAskBill.result)}`);
                        exp.invalidflag = 0;//终结
                        exp.sendFlag = i == 3 ? 2 : i;
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                    });
                    it('门店要货单按批次查验证', async function () {
                        let invAskBillList = await invAskReqHandler.getInvAskBillList({ sendFlag: i == 3 ? 2 : i, orderFlag: 0, invalidflag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                        // console.log(`常青店要货按批次查exp=${JSON.stringify(exp)}`);
                        // console.log(`常青店要货按批次查actual=${JSON.stringify(invAskBillList.result.dataList[0])}`);
                        common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                        expect(invAskBillList.result.dataList[0].invalidflag, '是否自动终结').to.equal('0');
                    });
                    it('要货单按明细查', async function () {
                        let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });//flag: 0,
                        let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                        common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                    })
                    it('门店在途调拨按批次查验证', async function () {
                        let shopInList = await inOutHandler.getShopInList();
                        // console.log(`在途调拨按批次查shopInList=${JSON.stringify(shopInList.result.dataList[0])}`);
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        common.isApproximatelyEqualAssert({
                            id: sfShopOutForAsk.result.pk,
                            billno: sfShopOutForAsk.result.billno,
                            // seller": "中洲店总经理",
                            inventoryrefName: sfShopOutForAsk.params.show_invidref,
                            // "optime": "09-29 11:56",
                            invname: sfShopOutForAsk.params.show_shopid,
                            remark: sfShopOutForAsk.params.remark,
                            totalmoney: sfShopOutForAsk.params.totalmoney,
                            opname: LOGINDATA.name,
                            modifyFlag: 0,
                            totalnum: sfShopOutForAsk.params.totalnum,
                            prodate: sfShopOutForAsk.params.prodate,
                            invalidflag: sfShopOutForAsk.params.invalidflag
                        }, shopInList.result.dataList[0]);
                    });
                    //用调拨出库详情验证
                    it('门店调拨入库单详情验证', async function () {
                        let shopInInfo = await inOutHandler.shopInQueryBilling(sfShopOutForAsk.result.pk);
                        // console.log(`常青门店调入详情=${JSON.stringify(shopInInfo.result)}`);
                        // console.log(`仓库店要货调拨保存param=${JSON.stringify(sfShopOutForAsk.params)}`);
                        common.isApproximatelyEqualAssert(sfShopOutForAsk.params, shopInInfo.result, ['checkprenum', 'pk', 'rowid']);
                    });

                    it('库存验证', async function () {
                        let styleInvAft = await reqHandler.qlIFCHandler({
                            interfaceid: 'ql-1932',
                            propdresStyleid: styleInfo.pk,
                        });
                        let exp = getInvExp({ sfRes: _.cloneDeep(sfShopOutForAsk), styleInfo });
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        // console.log(`styleInvBef.result.dataList.length=${JSON.stringify(styleInvBef.result.dataList.length)}`);
                        let invDataList = _.cloneDeep(styleInvBef.result.dataList);
                        for (let j = 0; j < invDataList.length; j++) {
                            let key = `${invDataList[j].invid}-${invDataList[j].colorid}-${invDataList[j].sizeid}`;
                            invDataList[j] = common.addObject(_.cloneDeep(invDataList[j]), exp[key]);
                            invDataList[j].availStockNum = invDataList[j].availStockNum > 0 ? invDataList[j].availStockNum : 0;
                        };
                        // console.log(`invDataList=${JSON.stringify(invDataList)}`);
                        common.isApproximatelyArrayAssert(invDataList, styleInvAft.result.dataList);

                    });
                });
                describe('中洲店', function () {
                    let shopOutListForAsk, shopOutInfoForAsk;
                    before(async () => {
                        await common.loginDo({ logid: value.logidB });
                        sendShopInv1 = await common.callInterface('ql-1932', {
                            invid: LOGINDATA.invid,
                            propdresStyleid: styleInfo.pk,
                        });
                        shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList();
                        shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                    });
                    it('要货调拨单列表验证', async function () {
                        //全部发货 要货调拨列表查不到
                        if (i != 1) {
                            let act = shopOutListForAsk.result.dataList.find(obj => obj.id == sfInvAskBill.result.pk);
                            expect(act).to.be.undefined;
                        } else {
                            let exp = getShopOutListForAskExp({ qfInvAskBill, sfInvAskBill });
                            // console.log(`中洲店要货调拨列表exp=${JSON.stringify(exp)}`);
                            // console.log(`发货后要货调拨列表act=${JSON.stringify(shopOutListForAsk.result.dataList[0])}`);
                            common.isApproximatelyEqualAssert(exp, shopOutListForAsk.result.dataList[0]);
                        };

                    });
                    it('要货调拨单详情验证', async function () {
                        let exp = getShopOutInfoForAskExp(qfInvAskBill);
                        // console.log(`要货调拨单详情exp=${JSON.stringify(exp)}`);
                        // console.log(`要货调拨单详情result=${JSON.stringify(shopOutInfoForAsk.result)}`);
                        common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);
                    });
                    it('中洲店调拨出库单列表验证', async function () {
                        let shopOutList = await inOutHandler.getShopOutList().then(res => res.result.dataList.find(data => data.id == sfShopOutForAsk.result.pk));
                        // console.log(`仓库店门店调出列表shopOutList=${JSON.stringify(shopOutList)}`);
                        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                        common.isApproximatelyEqualAssert({
                            seller: LOGINDATA.name,
                            inventoryrefName: sfShopOutForAsk.params.show_invidref,
                            "flag": "未接收",
                            remark: sfShopOutForAsk.params.remark,
                            totalmoney: sfShopOutForAsk.params.totalmoney,
                            opname: LOGINDATA.name,
                            totalnum: sfShopOutForAsk.params.totalnum,
                            prodate: sfShopOutForAsk.params.prodate,
                            shopname: sfShopOutForAsk.params.show_shopid,
                            staffName: LOGINDATA.name,
                            id: sfShopOutForAsk.result.pk,
                            billno: sfShopOutForAsk.result.billno,
                            "invalidflag": "否",
                        }, shopOutList);
                        // console.log(`仓库店要货调拨列表shopOutListForAsk=${JSON.stringify(shopOutListForAsk.result.dataList[0])}`);

                    });
                    it('中洲店调拨出库单详情验证', async function () {
                        let shopOutInfo = await inOutHandler.shopOutQueryBilling(sfShopOutForAsk.result.pk);
                        // console.log(`仓库店门店调出详情shopOutInfo=${JSON.stringify(shopOutInfo)}`);
                        // console.log(`要货调拨发货保存sfShopOutForAsk=${JSON.stringify(sfShopOutForAsk)}`);
                        common.isApproximatelyEqualAssert(sfShopOutForAsk.params, shopOutInfo.result, ['pk', 'rowid']);
                        expect(shopOutInfo.result.pk).to.equal(sfShopOutForAsk.result.pk);
                    });
                    it('调拨发货后报单列表验证', async function () {
                        let exp = getAskFormListExp({ qfInvAskBill });
                        // console.log(`formListBef=${JSON.stringify(formListBef)}`);
                        let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                        // console.log(`formList.result=${JSON.stringify(formList.result)}`);
                        formListBef.result.dataList.length == 0 ? common.isApproximatelyEqualAssert(exp, formList.result.dataList[0]) : common.isApproximatelyEqualAssert(common.addObject(_.cloneDeep(formListBef.result.dataList[0]), { restnum: exp.restnum }), formList.result.dataList[0]);
                        common.isApproximatelyEqualAssert(common.add(formListBef.result.sumrow.restnum, exp.restnum), formList.result.sumrow.restnum);
                    });
                    it('调拨发货后报单详情验证', async function () {
                        let formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                        let exp = getAskFormInfoExp(qfInvAskBill);
                        if (i != 1) {
                            expect(exp.length).to.equal(0);
                        } else {
                            common.isArrayContainArrayAssert(exp, formInfo.result.details);
                            common.isApproximatelyEqualAssert({
                                dwid: BASICDATA.dwidVell,
                                invid: LOGINDATA.invid,
                                prodate: common.getCurrentDate(),
                            }, formInfo.result);
                        };
                    });
                });

                describe('门店调入', function () {
                    let inInfo;
                    before(async () => {
                        await common.loginDo({ logid: value.logidA });
                        let qfRes = await inOutHandler.shopInQueryBilling({
                            pk: sfShopOutForAsk.result.pk,
                        });
                        inInfo = await inOutHandler.shopIn(qfRes.result);
                    });
                    after('作废调入单', async () => {
                        await inOutHandler.cancelShopInBill(inInfo.result.pk);
                    });
                    it('在途调拨列表没有该数据', async function () {
                        let onRoadList = await reqHandler.qlIFCHandler({
                            interfaceid: 'ql-1867',
                            id1: sfShopOutForAsk.result.billno,
                            id2: sfShopOutForAsk.result.billno,
                        });
                        expect(onRoadList.result.dataList.length).to.equal(0);
                    });
                    it('调入后，按批次查验证', async function () {
                        let shopInList = await reqHandler.qlIFCHandler({
                            interfaceid: 'ql-1863',
                            billno1: sfShopOutForAsk.result.billno,
                            billno2: sfShopOutForAsk.result.billno,
                        });
                        common.isApproximatelyEqualAssert({
                            invname: '常青店',
                            invnameref: '中洲店',
                            billno: inInfo.result.billno,
                            type: sfShopOutForAsk.result.billno,
                            totalnum: inInfo.params.totalnum,
                            // purmoney: sfShopOutForAsk.params.totalmoney,
                        }, shopInList.result.dataList[0]);
                    });

                });
                describe('异常流', function () {
                    it('发货后常青店修改要货单', async function () {
                        await common.loginDo({ logid: value.logidA });
                        let updateJson = _.cloneDeep(sfInvAskBill.params);
                        updateJson.details = qfInvAskBill.result.details;
                        updateJson.details[0].num = common.sub(updateJson.details[0].num, 5);
                        updateJson = Object.assign({
                            interfaceid: 'sf-2320-1',
                            pk: sfInvAskBill.result.pk,
                            check: false,
                            // totalnum: common.sub(updateJson.totalnum, 5),
                        }, updateJson);
                        let updateInvAskBill = await invAskReqHandler.saveInvAskBill(updateJson);
                        // console.log(`updateInvAskBill=${JSON.stringify(updateInvAskBill)}`);
                        i == 1 ? expect(updateInvAskBill.result).includes.keys('pk') : expect(updateInvAskBill.result).to.includes({ msgId: 'order_not_allow_edit_fully_deliver', error: '订单已全部发货，不允许修改' });
                        if (i == 1) sfInvAskBill = updateInvAskBill;
                    });
                });
                describe('作废调拨单', function () {
                    before(async () => {
                        //中洲店登录
                        await common.loginDo({ logid: value.logidB });
                        //作废调拨出库单
                        await inOutHandler.cancelShopOutBill(sfShopOutForAsk.result.pk);
                    });
                    describe('常青店--门店', function () {
                        before(async () => {
                            //常青店登录，
                            await common.loginDo({ logid: value.logidA });
                            qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                            // console.log(`qfInvAskBill=${JSON.stringify(qfInvAskBill)}`);
                        });
                        it('门店要货单详情验证', async function () {
                            let exp = getAskBillInfoBySave(sfInvAskBill);
                            // console.log(`exp=${JSON.stringify(exp)}`);
                            // console.log(`sfInvAskBill.params=${JSON.stringify(sfInvAskBill.params)}`);
                            common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                        });
                        it('门店要货单按批次查验证', async function () {
                            let invAskBillList = await invAskReqHandler.getInvAskBillList({ invalidflag: 0, sendFlag: 0, orderFlag: 0 });
                            let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                            common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                            expect(Number(invAskBillList.result.dataList[0].invalidflag), '是否自动终结').to.equal(0);
                        });
                        it('要货单按明细查验证', async function () {
                            //要货按明细查
                            let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                            // console.log(`作废调拨单后要货单按明细查invAskBillDetList=${JSON.stringify(invAskBillDetList.result.dataList.slice(0, 2))}`);
                            let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                            // console.log(`作废调拨单后要货单按明细查exp=${JSON.stringify(exp)}`);
                            common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                        });
                        it('门店在途调拨数据验证', async function () {
                            let shopInList = await inOutHandler.getShopInList({
                                id1: sfShopOutForAsk.result.billno,
                                id2: sfShopOutForAsk.result.billno,
                            });
                            // console.log(`在途调拨shopInList.params=${JSON.stringify(shopInList.params)}`);
                            expect(shopInList.result.dataList.length, '中洲店作废调拨出库单，常州店在途调拨仍然可以查到单据').to.equal(0);
                        });

                        it('库存验证', async function () {
                            let styleInvAft = await reqHandler.qlIFCHandler({
                                interfaceid: 'ql-1932',
                                propdresStyleid: styleInfo.pk,
                            });
                            common.isApproximatelyEqualAssert(styleInvBef.result, styleInvAft.result);

                        });
                    });
                    describe('中洲店--仓库店', function () {
                        let shopOutListForAsk, shopOutInfoForAsk;
                        before(async () => {
                            await common.loginDo({ logid: value.logidB });
                            sendShopInv1 = await common.callInterface('ql-1932', {
                                invid: LOGINDATA.invid,
                                propdresStyleid: styleInfo.pk,
                            });
                            shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList();
                            shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                        });
                        it('要货调拨单列表验证', async function () {
                            let exp = getShopOutListForAskExp({ qfInvAskBill, sfInvAskBill });
                            // console.log(`作废调拨发货单的后要货调拨列表=${JSON.stringify(shopOutListForAsk.result.dataList[0])}`);
                            common.isApproximatelyEqualAssert(exp, shopOutListForAsk.result.dataList[0]);
                            expect(shopOutListForAsk.result.dataList[0].sendFlag).to.equal('未发货');
                        });
                        it('要货调拨单详情验证', async function () {
                            let exp = getShopOutInfoForAskExp(qfInvAskBill);
                            common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);
                        });
                        it('中洲店调出单按批次查验证', async function () {
                            let shopOutList = await inOutHandler.getShopOutList({
                                id1: sfShopOutForAsk.result.billno,
                                id2: sfShopOutForAsk.result.billno,
                            });
                            common.isApproximatelyEqualAssert({
                                seller: LOGINDATA.name,
                                inventoryrefName: sfShopOutForAsk.params.show_invidref,
                                flag: "未接收",
                                // "optime": "09-29 14:48",
                                remark: sfShopOutForAsk.params.remark,
                                // "totalmoney": "11400",
                                opname: LOGINDATA.name,
                                totalnum: sfShopOutForAsk.params.totalnum,
                                prodate: sfShopOutForAsk.params.prodate,
                                shopname: sfShopOutForAsk.params.show_shopid,
                                // "totalRoll": "",
                                staffName: LOGINDATA.name,
                                id: sfShopOutForAsk.result.pk,
                                billno: sfShopOutForAsk.result.billno,
                                invalidflag: "是",
                            }, shopOutList.result.dataList[0]);
                            // console.log(`作废后仓库店门店调出列表shopOutList=${JSON.stringify(shopOutList)}`);
                            // console.log(`作废后仓库店要货调拨列表shopOutListForAsk=${JSON.stringify(shopOutListForAsk.result.dataList[0])}`);
                        });
                        it('中洲店调出单详情验证', async function () {
                            let shopOutInfo = await inOutHandler.shopOutQueryBilling(sfShopOutForAsk.result.pk);
                            // console.log(`作废后仓库店门店调出详情shopOutInfo=${JSON.stringify(shopOutInfo)}`);
                            // console.log(`作废后要货调拨发货保存sfShopOutForAsk=${JSON.stringify(sfShopOutForAsk)}`);
                            common.isApproximatelyEqualAssert(sfShopOutForAsk.params, shopOutInfo.result, ['pk']);
                        });
                        it('作废调拨单后要货报单列表验证', async function () {
                            let exp = getAskFormListExp({ qfInvAskBill });
                            let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                            // console.log(`formList.result=${JSON.stringify(formList.result)}`);
                            formListBef.result.dataList.length == 0 ? common.isApproximatelyEqualAssert(exp, formList.result.dataList[0]) : common.isApproximatelyEqualAssert(common.addObject(_.cloneDeep(formListBef.result.dataList[0]), { restnum: exp.restnum }), formList.result.dataList[0]);
                            common.isApproximatelyEqualAssert(common.add(formListBef.result.sumrow.restnum, exp.restnum), formList.result.sumrow.restnum);
                        });
                        it('作废调拨单后要货报单详情验证', async function () {
                            let formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                            let exp = getAskFormInfoExp(qfInvAskBill);
                            // console.log(`exp=${JSON.stringify(exp)}`);
                            // console.log(`formInfo.result.=${JSON.stringify(formInfo.result)}`);
                            common.isArrayContainArrayAssert(exp, formInfo.result.details);
                            common.isApproximatelyEqualAssert({
                                dwid: BASICDATA.dwidVell,
                                invid: LOGINDATA.invid,
                                prodate: common.getCurrentDate(),
                            }, formInfo.result);
                        });
                    });
                });
            });
        };

        //部分报单，全部报单，多报单
        for (let i = 1; i < 4; i++) {
            describe(`${value.logidA}按要货报单--采购订货`, function () {
                let newStyleInfo, sfInvAskBill, formListBef, formInfo, sfPurOrder, qfPurOrder;
                before(async () => {
                    //常青店登录，新增要货单
                    await common.loginDo({ logid: value.logidA });
                    //新增厂商
                    supplier = await mainReqHandler.saveSupplier({ name: `报单${common.getRandomStr(5)}` });
                    // //新增货品
                    let jsonparam = basiceJsonparam.addGoodJson();
                    jsonparam.dwid = supplier.result.val;
                    let style = await basicReqHandler.editStyle({ jsonparam });
                    // // console.log(`style=${JSON.stringify(style)}`);
                    newStyleInfo = await common.fetchMatInfo(style.pk);
                    // // console.log(`newStyleInfo=${JSON.stringify(newStyleInfo)}`);
                    // //新增要货单
                    sfInvAskBill = await invAskReqHandler.saveInvAskBill(invAskJson.invAskBillJson({ styleInfo: newStyleInfo }));
                    // console.log(`sfInvAskBill=${JSON.stringify(sfInvAskBill)}`);

                    //中洲店登录
                    await common.loginDo({ logid: value.logidB });
                    //获取报单详情
                    formListBef = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: supplier.result.val, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                    formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: supplier.result.val });
                    // console.log(`formInfo=${JSON.stringify(formInfo)}`);
                    let json = _.cloneDeep(formInfo.result);
                    json.billid = sfInvAskBill.result.pk;
                    //转采购订单
                    //部分报单
                    if (i == 1) json.details[0].num = common.sub(json.details[0].num, 5)
                    else if (i == 3) json.details[0].num = common.add(json.details[0].num, 5);
                    sfPurOrder = await invAskReqHandler.saveFormByAsk(json);
                    // console.log(`sfPurOrder=${JSON.stringify(sfPurOrder)}`);
                    qfPurOrder = await purReqHandler.purOrderQueryBilling(sfPurOrder.result.pk);

                });
                describe('报单门店', function () {
                    it('按要货报单列表验证', async function () {
                        let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: supplier.result.val, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                        expect(formList.result.dataList.length).to.equal(0);
                    });
                    it('采购订货详情验证', async function () {
                        let exp = _.cloneDeep(sfPurOrder.params);
                        exp = Object.assign(exp, format.dataFormat(sfPurOrder.result, 'id=pk;orderid=pk'));
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        // console.log(`qfPurOrder.result=${JSON.stringify(qfPurOrder.result)}`);
                        common.isApproximatelyEqualAssert(exp, qfPurOrder.result, ['pk', 'show_cardaccountid', 'show_remitaccountid', 'show_cashaccountid', 'cardaccountid', 'remitaccountid', 'cashaccountid']);
                    });
                    it('采购订货按批次查', async function () {
                        let exp = purReqHandler.getPurOrderMainSearchListExp(qfPurOrder.result);
                        let purOrderList = await reqHandler.qlIFCHandler({ interfaceid: 'ql-22102', id1: sfPurOrder.result.billno, id2: sfPurOrder.result.billno });
                        expect(purOrderList.result.dataList.length).to.above(0);
                        // console.log(`purOrderList.result.dataList[0]=${JSON.stringify(purOrderList.result.dataList[0])}`);
                        common.isApproximatelyEqualAssert(exp, purOrderList.result.dataList[0]);
                    });
                    it('采购订货按明细查', async function () {
                        let exp = purReqHandler.getPurOrderDetailSearchListExp(qfPurOrder.result);
                        let purOrderDetList = await reqHandler.qlIFCHandler({ interfaceid: 'ql-22103', billno: sfPurOrder.result.billno });
                        common.isArrayContainArrayAssert(exp, purOrderDetList.result.dataList.slice(0, exp.length));
                    });
                    it('当前库存变化', async function () {
                        let styleInvAft = await reqHandler.qlIFCHandler({
                            interfaceid: 'ql-1932',
                            propdresStyleid: newStyleInfo.pk,
                        });
                        // console.log(`styleInvAft=${JSON.stringify(styleInvAft)}`);
                        let exp = [];

                        sfPurOrder.params.details.forEach(detail => {
                            let data = {
                                invid: sfPurOrder.params.invid,
                                invnum: 0,
                            };
                            exp.push(Object.assign(data, format.dataFormat(detail, 'styleid;colorid;sizeid;purednum=num;availStockNum=num')));

                        });
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        common.isApproximatelyArrayAssert(exp, styleInvAft.result.dataList);
                    });
                })
                describe('常青店要货单验证', function () {
                    let qfInvAskBill;
                    before(async () => {
                        await common.loginDo({ logid: value.logidA });
                        // console.log(`sfInvAskBill.result=${JSON.stringify(sfInvAskBill.result)}`);
                        qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                        // console.log(`qfInvAskBill=${JSON.stringify(qfInvAskBill)}`);
                    });
                    it('报单后常青店要货明细验证', async function () {
                        let exp = getAskBillInfoExp({ sfInvAskBill, sfRes: sfPurOrder });
                        exp.orderFlag = i == 3 ? 2 : i;
                        common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                    });
                    it('报单后常青店要货按批次查', async function () {
                        let invAskBillList = await invAskReqHandler.getInvAskBillList({ billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        // console.log(`invAskBillList.result.dataList[0]=${JSON.stringify(invAskBillList.result.dataList[0])}`);
                        common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                    });
                    it('报单后常青店要货按明细查验证', async function () {
                        let flagArr = [0, 15, 20];//状态 0 新建，10 缺货，15 部分下单，20 全部下单，50 已取消
                        let flag = i == 3 ? flagArr[2] : flagArr[i];
                        let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                        // console.log(`invAskBillDetList=${JSON.stringify(invAskBillDetList)}`);
                        let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                        for (let j = 0; j < exp.length; j++) {
                            if (Number(exp[j].flagid) != flag) exp.splice(j, 1);
                        };
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                    });
                });
                describe('按订货入库', function () {
                    let sfPurchase, qfPurchase;
                    before(async () => {
                        //按订货入库
                        await common.loginDo({ logid: value.logidB });
                        sfPurchase = await purReqHandler.invinByOrder(qfPurOrder.result);
                        qfPurchase = await purReqHandler.purQueryBilling(sfPurchase.result.pk);
                    });
                    after('作废采购单', async () => {
                        await purReqHandler.cancelPurchaseBill(sfPurchase.result.pk);
                    });
                    it('采购单详情', async function () {
                        common.isApproximatelyEqualAssert(sfPurchase.params, qfPurchase.result, ['orderversion', 'billno', 'billid']);
                    });
                    it('采购入库按批次查', async function () {
                        let purList = await reqHandler.qlIFCHandler({ interfaceid: 'ql-22301', puredmaincode: sfPurOrder.result.billno });
                        let exp = purReqHandler.getPurMainSearchListExp(qfPurchase.result);
                        exp.puredmaincode = sfPurOrder.result.billno;
                        common.isApproximatelyEqualAssert(exp, purList.result.dataList[0]);
                    });
                });
                describe('作废采购订单', function () {
                    before(async () => {
                        //作废采购订单
                        await common.loginDo({ logid: value.logidB });
                        await purReqHandler.cancelPurOrderBill(sfPurOrder.result.pk);
                    });
                    describe('要货报单验证', function () {
                        it('要货报单列表验证', async function () {
                            let formList = await invAskReqHandler.getFormListByAsk({ invid: BASICDATA.shopidCqd, dwid: supplier.result.val, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                            common.isApproximatelyEqualAssert(formListBef.result, formList.result);
                        });
                        it('要货报单详情验证', async function () {
                            let formInfoAft = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: supplier.result.val, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate() });
                            common.isApproximatelyEqualAssert(formInfo.result, formInfoAft.result);
                        });
                    });
                    describe('常青店要货单验证', function () {
                        before(async () => {
                            qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                        });
                        it('要货单详情验证', async function () {
                            let exp = _.cloneDeep(sfInvAskBill.params);
                            exp = Object(exp, getAskBillInfoFlagExp(sfInvAskBill));
                            exp.flag = 0;//状态
                            exp.orderFlag = 0;
                            common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                            // expect(Number(qfInvAskBill.result.orderFlag)).to.equal(0);
                        });
                        it('要货单按批次查验证', async function () {
                            let invAskBillList = await invAskReqHandler.getInvAskBillList({ orderFlag: 0 });
                            expect(Number(invAskBillList.result.dataList[0].orderFlag)).to.equal(0);
                        });
                        it('要货单按明细验证', async function () {
                            let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag: 0, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                            let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                            common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                        });
                    });
                });
            });
        };

        describe(`${value.logidA}批量新增`, function () {
            let sfRes = {}, billIds, billNos, sfBulkInvAskBill, qfInvAskBill = {},
                expAskList = [], expAskDetList = [], expAskShopOutList = [];
            before(async () => {
                //常青店登录
                await common.loginDo({ logid: value.logidA });
                let json = invAskJson.bulkInvAskBillJson({ styleInfo });
                // console.log(`json=${JSON.stringify(json)}`);
                // 批量新增要货单
                sfBulkInvAskBill = await invAskReqHandler.saveBulkInvAskBill(json);
                // console.log(`sfBulkInvAskBill=${JSON.stringify(sfBulkInvAskBill)}`);
                billIds = sfBulkInvAskBill.result.billIds.split(',');
                billNos = sfBulkInvAskBill.result.billno.split(',');
                billIds.forEach((billid, index) => {
                    sfRes[billid] = { result: {}, optime: sfBulkInvAskBill.optime };
                    sfRes[billid].result = { billid, billno: billNos[index], };
                });
                // console.log(`sfRes=${JSON.stringify(sfRes)}`);
            });
            describe('常青店', function () {
                it('要货单详情验证', async function () {
                    //拼接要货单详情期望值
                    let qfExp = {};
                    const details = sfBulkInvAskBill.params.details,
                        mainVal = format.dataFormat(sfBulkInvAskBill.params, 'invid;deliver;prodate;invalidflag');
                    for (let i = 0; i < details.length; i++) {
                        let exp = Object.assign({
                            targetinvid: details[i].targetinvid,
                            totalnum: details[i].num,
                            sendFlag: 0,//发货状态
                            flag: 0,
                            orderFlag: 0,
                            details: []
                        }, mainVal);
                        exp.details.push(details[i]);
                        if (qfExp[details[i].targetinvid]) {
                            qfExp[details[i].targetinvid].details.push(details[i]);
                            qfExp[details[i].targetinvid].totalnum = common.add(qfExp[details[i].targetinvid].totalnum, details[i].num);
                        } else {
                            qfExp[details[i].targetinvid] = exp;
                        };
                    };
                    let index = 0;
                    let qfInvAskBillExp = {};
                    for (let value of Object.values(qfExp)) {
                        qfInvAskBillExp[billIds[index]] = Object.assign(value, { pk: billIds[index] });
                        index++;
                    };
                    for (let i = 0; i < billIds.length; i++) {
                        await invAskReqHandler.getInvAskBillInfo({ pk: billIds[i] }).then(res => qfInvAskBill[billIds[i]] = res);
                        common.isApproximatelyEqualAssert(qfInvAskBillExp[billIds[i]], qfInvAskBill[billIds[i]].result);
                    };
                    for (let key of Object.keys(qfInvAskBill)) {
                        let exp = getInvAskListExp({ qfInvAskBill: qfInvAskBill[key], sfInvAskBill: sfRes[key] });
                        expAskList.push(exp);
                        exp = getInvAskDetListExp({ qfInvAskBill: qfInvAskBill[key], sfInvAskBill: sfRes[key] });
                        expAskDetList.push(exp);
                        exp = getShopOutListForAskExp({ qfInvAskBill: qfInvAskBill[key], sfInvAskBill: sfRes[key] });
                        expAskShopOutList.push(exp);
                    };
                });
                it('要货单按批次查验证', async function () {
                    let invAskBillList;
                    //要货按批次查
                    await invAskReqHandler.getInvAskBillList().then(res => invAskBillList = res.result.dataList.slice(0, expAskList.length));
                    common.isApproximatelyArrayAssert(expAskList, invAskBillList);
                });
                it('要货单按明细查验证', async function () {
                    let invAskBillDetList;
                    //要货按明细查
                    await invAskReqHandler.getInvAskBillDetList().then(res => invAskBillDetList = res.result.dataList.slice(0, expAskDetList.length));
                    common.isApproximatelyArrayAssert(expAskDetList, invAskBillDetList);
                });

            });
            describe('要货调拨验证', function () {
                before(async () => {
                    await common.loginDo({ logid: value.logidB });
                });
                it('要货调拨列表', async function () {
                    let shopOutListForAsk = await invAskReqHandler.getShopOutForInvAskList({ billno1: billNos[0], billno2: billNos[billNos.length - 1] });
                    if (value.logidB == '204') {
                        expAskShopOutList.forEach((data, index) => {
                            if (data.targetinvName != LOGINDATA.invname) expAskShopOutList.splice(index, 1);
                        });
                    };
                    common.isApproximatelyArrayAssert(expAskShopOutList, shopOutListForAsk.result.dataList);
                });
                it('要货调拨页面', async function () {
                    for (let i = 0; i < billIds.length; i++) {
                        let shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: billIds[i] });
                        let exp = getShopOutInfoForAskExp(qfInvAskBill[billIds[i]]);
                        // console.log(`exp=${JSON.stringify(exp)}`);
                        // console.log(`shopOutInfoForAsk=${JSON.stringify(shopOutInfoForAsk)}`);
                        common.isApproximatelyEqualAssert(exp, shopOutInfoForAsk.result, ['totalmoney']);
                    };
                });
            });

            describe.skip('绑定仓库--文一店发货库存验证--超时跳过', function () {
                let shopOutInfoForAsk, sfShopOutForAsk, invCqdBef, invCqdAft, invWydBef, invWydAft, invCkdBef, invCkdAft;
                before(async function () {
                    await common.loginDo({ logid: 400 });
                    let qlParams = {
                        interfaceid: 'ql-1932',
                        invid: BASICDATA.shopidWyd,
                        styleid: BASICDATA.styleidAgc001//sfBulkInvAskBill.params.details[1].styleid,
                    };

                    //文一店库存查询
                    invWydBef = await reqHandler.qlIFCHandler(qlParams);

                    //仓库店库存查询
                    invCkdBef = await reqHandler.qlIFCHandler(Object.assign(qlParams, { invid: BASICDATA.shopidCkd }));

                    //常青店库存查询
                    invCqdBef = await reqHandler.qlIFCHandler(Object.assign(qlParams, { invid: BASICDATA.shopidCqd }));

                    //文一店按要货调拨 发货
                    shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: billIds[1] });

                    // console.log(`shopOutInfoForAsk.result=${JSON.stringify(shopOutInfoForAsk.result)}`);
                    sfShopOutForAsk = await invAskReqHandler.saveShopOutForInvAskBill(shopOutInfoForAsk.result);

                    await common.delay(1500);
                    //常青店店库存查询
                    invCqdAft = await reqHandler.qlIFCHandler(qlParams);

                    //仓库店库存查询
                    invCkdAft = await reqHandler.qlIFCHandler(Object.assign(qlParams, { invid: BASICDATA.shopidCkd }));

                    //文一店库存查询
                    invWydAft = await reqHandler.qlIFCHandler(Object.assign(qlParams, { invid: BASICDATA.shopidWyd }));

                });

                it('文一店库存变化', async function () {
                    common.isApproximatelyEqualAssert(invWydBef.result, invWydAft.result);
                });
                it('文一店绑定的仓库店库存变化', async function () {
                    let expBef = _.cloneDeep(invCkdBef);
                    // console.log(`exp.result.dataList[0]=${JSON.stringify(exp.result.dataList[0])}`);
                    let exp1 = {};
                    sfShopOutForAsk.params.details.forEach(detail => {
                        const key = `${detail.styleid}-${detail.colorid}-${detail.sizeid}`;
                        exp1[key] = {
                            invnum: -detail.num,
                            // total: -detail.total,
                            availStockNum: -detail.num,
                        };
                    });
                    // console.log(`exp1=${JSON.stringify(exp1)}`);
                    let index = -1;
                    for (let i = 0; i < expBef.result.dataList.length; i++) {
                        if (expBef.result.dataList[i].colorid == sfBulkInvAskBill.params.details[1].colorid && expBef.result.dataList[i].sizeid == sfBulkInvAskBill.params.details[1].sizeid) {
                            index = i;
                            break;
                        };
                    };
                    if (index == -1) {
                        expBef.result.dataList.push({
                            invnum: -sfBulkInvAskBill.params.details[1].num,
                            availStockNum: 0,
                            onroadnum: 0,//在途数
                            ordernum: 0,
                            toltalnum: 0,//累计销
                            purednum: 0,//待入库
                            sizeid: sfBulkInvAskBill.params.details[1].sizeid,
                            colorid: sfBulkInvAskBill.params.details[1].colorid,
                            styleid: sfBulkInvAskBill.params.details[1].styleid,
                            invid: BASICDATA.shopidCkd,
                        });
                    } else {
                        exp1[`${sfBulkInvAskBill.params.details[1].styleid}-${sfBulkInvAskBill.params.details[1].colorid}-${sfBulkInvAskBill.params.details[1].sizeid}`].total = -common.mul(sfBulkInvAskBill.params.details[1].num, 100);;
                        expBef.result.dataList[index] = common.addObject(expBef.result.dataList[index], exp1[`${sfBulkInvAskBill.params.details[1].styleid}-${sfBulkInvAskBill.params.details[1].colorid}-${sfBulkInvAskBill.params.details[1].sizeid}`]);
                        expBef.result.dataList[index].availStockNum = Math.max(Number(expBef.result.dataList[index].invnum) + Number(expBef.result.dataList[index].onroadnum) + Number(expBef.result.dataList[index].purednum) - Number(expBef.result.dataList[index].ordernum), 0);
                    };
                    // console.log(`期望值=${JSON.stringify(expBef.result.dataList)}`);
                    // console.log(`act=${JSON.stringify(invCkdAft.result.dataList)}`);
                    common.isApproximatelyArrayAssert(expBef.result.dataList, invCkdAft.result.dataList);
                });
                it.skip('常青店库存变化--时间超过100S先跳过', async function () {
                    let expBef = _.cloneDeep(invCqdBef);
                    let exp1 = billExp.getCurInvListExp(sfShopOutForAsk);
                    // console.log(`exp1=${JSON.stringify(exp1)}`);
                    let index = -1;
                    for (let i = 0; i < expBef.result.dataList.length; i++) {
                        if (expBef.result.dataList[i].colorid == sfBulkInvAskBill.params.details[1].colorid && expBef.result.dataList[i].sizeid == sfBulkInvAskBill.params.details[1].sizeid) {
                            index = i;
                            break;
                        };
                    };

                    if (index == -1) {
                        expBef.result.dataList.push({
                            invnum: -sfBulkInvAskBill.params.details[1].num,
                            availStockNum: sfBulkInvAskBill.params.details[1].num,
                            onroadnum: sfBulkInvAskBill.params.details[1].num,//在途数
                            ordernum: 0,
                            toltalnum: 0,//累计销
                            purednum: 0,//待入库
                            sizeid: sfBulkInvAskBill.params.details[1].sizeid,
                            colorid: sfBulkInvAskBill.params.details[1].colorid,
                            styleid: sfBulkInvAskBill.params.details[1].styleid,
                            invid: BASICDATA.shopidCqd,
                        });
                    } else {
                        //total拼接有问题
                        exp1[`${sfBulkInvAskBill.params.details[1].styleid}-${sfBulkInvAskBill.params.details[1].colorid}-${sfBulkInvAskBill.params.details[1].sizeid}`].total = common.mul(sfBulkInvAskBill.params.details[1].num, 100);
                        expBef.result.dataList[index] = common.addObject(expBef.result.dataList[index], exp1[`${sfBulkInvAskBill.params.details[1].styleid}-${sfBulkInvAskBill.params.details[1].colorid}-${sfBulkInvAskBill.params.details[1].sizeid}`]);
                        expBef.result.dataList[index].availStockNum = Math.max(Number(expBef.result.dataList[index].invnum) + Number(expBef.result.dataList[index].onroadnum) + Number(expBef.result.dataList[index].purednum) - Number(expBef.result.dataList[index].ordernum), 0);
                    };
                    // console.log(`invCqdBef=${JSON.stringify(invCqdBef)}`);
                    // console.log(`expBef.result.dataList=${JSON.stringify(expBef.result.dataList)}`);
                    // console.log(`invCqdAft.result.dataList=${JSON.stringify(invCqdAft.result.dataList)}`);
                    common.isApproximatelyArrayAssert(expBef.result.dataList, invCqdAft.result.dataList);
                });
            });
        });
    };


    describe('分支流', function () {
        describe('按要货调拨分支', function () {
            let sfInvAskBill, shopOutInfoForAsk, sfShopOutForAsk, qfInvAskBill;
            before(async () => {
                await common.loginDo();
                await common.setGlobalParam('sc_invAskSendAutoEnd', 1);//要货调拨后自动终结要货单 为是
                sfInvAskBill = await invAskReqHandler.saveInvAskBill(invAskJson.invAskBillJson({ styleInfo }));
                await common.loginDo({ logid: 200 });
                //要货调拨详情
                shopOutInfoForAsk = await invAskReqHandler.getShopOutForInvAskInfo({ pk: sfInvAskBill.result.pk });
                shopOutInfoForAsk.result.details[0].num = common.sub(shopOutInfoForAsk.result.details[0].num, 1);
                shopOutInfoForAsk.result.details[1].num = common.add(shopOutInfoForAsk.result.details[1].num, 1);
                //按要货调拨保存
                sfShopOutForAsk = await invAskReqHandler.saveShopOutForInvAskBill(shopOutInfoForAsk.result);
                // console.log(`sfShopOutForAsk=${JSON.stringify(sfShopOutForAsk)}`);
                qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
            });
            after(async () => {
                await common.setGlobalParam('sc_invAskSendAutoEnd', 0);
            })
            it('一条明细多发，一条明细少发，总发货数仍等于总数', async function () {
                let exp = getAskBillInfoAfterSend(sfShopOutForAsk);
                common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
            });
            it('调拨单是否自动终结', async function () {
                expect(Number(qfInvAskBill.result.invalidflag), '要货调拨后自动终结要货单参数为是,调拨保存后，要货单终结invalidflag不为1').to.equal(1);
            });
            it('发货后修改要货单', async function () {
                let updateJson = _.cloneDeep(sfInvAskBill.params);
                updateJson.details[0].num = common.sub(updateJson.details[0].num, 5);
                updateJson.pk = sfInvAskBill.result.pk;
                updateJson.check = false;
                let updateBill = await invAskReqHandler.saveInvAskBill(updateJson);
                expect(updateBill.result).to.includes({ msgId: 'order_already_end', error: "该订单已被终结" });
            });
            describe('部分发货后转报单', function () {
                before(async () => {
                    let formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell });
                    formInfo.billid = sfInvAskBill.result.pk;
                    formInfo.result.details = getAskFormInfoExp(qfInvAskBill);
                    //按要货报单
                    sfPurOrder = await invAskReqHandler.saveFormByAsk(formInfo.result);
                    // console.log(`sfPurOrder.params=${JSON.stringify(sfPurOrder.params)}`);
                    qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                });
                it('部分发货+报单--要货详情验证', async function () {
                    let exp = Object.assign(getAskBillInfoAfterSend(sfShopOutForAsk), {
                        orderFlag: 1,
                        totalOrdernum: sfPurOrder.params.totalnum,
                        invalidflag: 1,
                    });
                    for (let i = 0; i < exp.details.length; i++) {
                        if (exp.details[i].id == sfPurOrder.params.details[0].askDetid)
                            exp.details[i] = Object.assign(exp.details[i], { ordernum: sfPurOrder.params.details[0].num, flag: 15 });
                    };
                    // console.log(`exp=${JSON.stringify(exp)}`);
                    // console.log(`qfInvAskBill.result=${JSON.stringify(qfInvAskBill.result)}`);
                    common.isApproximatelyEqualAssert(exp, qfInvAskBill.result);
                });
                it('部分发货+报单--要货按批次查验证', async function () {
                    let invAskBillList = await invAskReqHandler.getInvAskBillList({ invalidflag: 1, sendFlag: 1, orderFlag: 1 });
                    // console.log(`部分发货+报单 要货单按批次查invAskBillList=${JSON.stringify(invAskBillList.result.dataList[0])}`);
                    let exp = getInvAskListExp({ qfInvAskBill, sfInvAskBill });
                    // console.log(`部分发货+报单要货单按批次查exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, invAskBillList.result.dataList[0]);
                });
                it('部分发货+报单--要货按明细查验证', async function () {
                    let invAskBillDetList = await invAskReqHandler.getInvAskBillDetList({ flag: 15, billno1: sfInvAskBill.result.billno, billno2: sfInvAskBill.result.billno });
                    // console.log(`部分发货+报单按明细查act=${JSON.stringify(invAskBillDetList.result.dataList.slice(0, 2))}`);
                    let exp = getInvAskDetListExp({ qfInvAskBill, sfInvAskBill });
                    for (let i = 0; i < exp.length; i++) {
                        if (Number(exp[i].flagid) != 15) exp.splice(i, 1);
                    }
                    // console.log(`部分发货+报单要货单按明细查exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyArrayAssert(exp, invAskBillDetList.result.dataList);
                });
                it('部分发货——报单后，作废调拨单', async function () {
                    let cancelRes = await inOutHandler.cancelShopOutBill(sfShopOutForAsk.result.pk, false);
                    expect(cancelRes).to.includes({ msgId: 'already_ordered_not_allow_cancel' });

                });
            });

        });
        describe('要货报单分支', function () {
            let qfPurOrder;
            before(async () => {
                await common.loginDo();
                await common.setGlobalParam('sc_invAskToPureInv', 0);//要货报单采购订单所属门店  申请门店
                sfInvAskBill = await invAskReqHandler.saveInvAskBill(invAskJson.invAskBillJson({ styleInfo }));
                qfInvAskBill = await invAskReqHandler.getInvAskBillInfo({ pk: sfInvAskBill.result.pk });
                await common.loginDo({ logid: 200 });
                //获取报单详情
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                formInfo = await invAskReqHandler.getFormInfoByAsk({ pk: BASICDATA.shopidCqd, dwid: BASICDATA.dwidVell });
                // console.log(`formInfo=${JSON.stringify(formInfo)}`);
                formInfo.billid = sfInvAskBill.result.pk;
                formInfo.result.details = getAskFormInfoExp(qfInvAskBill);
                sfPurOrder = await invAskReqHandler.saveFormByAsk(formInfo.result);
                qfPurOrder = await purReqHandler.purOrderQueryBilling(sfPurOrder.result.pk);
            });
            after(async () => {
                await common.setGlobalParam('sc_invAskToPureInv', 1);//报单门店（仓库店）
            });
            it('按要货报单页面门店验证', async function () {
                expect(formInfo.result).to.includes({ invid: BASICDATA.shopidCqd });
            });
            it('采购订货详情门店验证', async function () {
                expect(qfPurOrder.result).to.includes({ invid: BASICDATA.shopidCqd });
            });
            it('采购订货按批次查门店验证', async function () {
                let purOrderList = await reqHandler.qlIFCHandler({ interfaceid: 'ql-22102', id1: sfPurOrder.result.billno, id2: sfPurOrder.result.billno });
                expect(purOrderList.result.dataList[0]).to.includes({ invname: '常青店' });
            });
            it('采购订货按明细查门店验证', async function () {
                let exp = purReqHandler.getPurOrderDetailSearchListExp(qfPurOrder.result);
                let purOrderDetList = await reqHandler.qlIFCHandler({ interfaceid: 'ql-22103', billno: sfPurOrder.result.billno });
                common.isArrayContainArrayAssert(exp, purOrderDetList.result.dataList);
            });
        });

    });

});

/**
 * 新增要货单拼接期望值
 * @param {object} sfInvAskBill 
 */
function getAskBillInfoBySave(sfInvAskBill) {
    let exp = _.cloneDeep(sfInvAskBill.params);
    exp = Object.assign({ invalidflag: 0, sendFlag: 0, orderFlag: 0 }, exp, getAskBillInfoFlagExp(sfInvAskBill));
    exp.details.forEach(detail => {
        // console.log(`detail=${JSON.stringify(detail)}`);
        detail = Object.assign(detail, { sendnum: 0, ordernum: 0, flag: 0 });
    });
    return exp;

}




/**
 * 拼接要货单详情期望
 * 只有报单或者调拨，传interfaceid
 * @param {Object} sfInvAskBill
 * @param {Object} sfRes //调拨保存或者报单
 * 
 */
function getAskBillInfoExp({ sfInvAskBill, sfRes }) {
    let exp = Object.assign({
        sendFlag: 0,
        flag: 0,
        orderFlag: 0,
        details: [],
        pk: sfInvAskBill.result.pk,
        billno: sfInvAskBill.result.billno
    }, format.dataFormat(sfInvAskBill.params, 'deliver;targetinvid;totalnum;prodate;invid'));
    let totalNum = 0, sfResDetails = sfRes.params.details;
    sfInvAskBill.params.details.forEach((detail) => {
        let data = Object.assign({
            ordernum: 0,
            sendnum: 0,
        }, format.dataFormat(detail, 'sizeid;colorid;styleid;rem;num;flag'));
        for (let j = 0; j < sfResDetails.length; j++) {
            let num = 0;
            if (detail.styleid == sfResDetails[j].styleid && detail.colorid == sfResDetails[j].colorid && detail.sizeid == sfResDetails[j].sizeid) {
                num = common.add(num, sfResDetails[j].num);
                data = Object.assign(_.cloneDeep(data), {
                    ordernum: sfRes.params.interfaceid == 'sf-22108-1' ? num : 0,
                    sendnum: sfRes.params.interfaceid == 'sf-2324-1' ? num : 0,
                });
                totalNum = common.add(totalNum, sfResDetails[j].num);
                //是否报单
                // if (sfRes.params.interfaceid == 'sf-22108-1') {
                if (detail.num > sfResDetails[j].num && sfResDetails[j].num > 0) {
                    data.flag = sfRes.params.interfaceid == 'sf-22108-1' ? 15 : 1;
                } else if (sfResDetails[j].num >= detail.num) {
                    data.flag = sfRes.params.interfaceid == 'sf-22108-1' ? 20 : 2;
                } else {
                    data.flag = 0;
                };

            };
        };
        exp.details.push(data);
        exp = Object.assign(exp, {
            totalOrdernum: sfRes.params.interfaceid == 'sf-22108-1' ? totalNum : 0,
            totalSendnum: sfRes.params.interfaceid == 'sf-2324-1' ? totalNum : 0,
        });
    })
    return exp;
}


/**
 * 拼接qfInvAskBill flag期望值
 */
function getFlagExp({ details, num, total, flag = 0 }) {
    details.forEach(detail => {
        let detailFlag = 0;
        if (detail[num] == 0) detailFlag = 0
        else if (detail[num] < detail[total]) detailFlag = 1
        else detailFlag = 2;
        flag = common.add(flag, detailFlag);
    });
    if (flag == 0) flag = 0
    else if (flag == common.mul(details.length, 2)) flag = 2
    else flag = 1;
    return flag;
};

function getAskBillInfoFlagExp(sfRes) {
    let exp = {};
    switch (sfRes.params.interfaceid) {
        case 'sf-2320-1'://要货新增
            exp.sendFlag = 0;//发货状态
            exp.orderFlag = 0;//报单状态
            break;
        case 'sf-2324-1'://调拨发货
            if (sfRes)
                exp.sendFlag = getFlagExp({ details: sfRes.params.details, num: 'num', total: 'appnum' });//发货状态
            exp.orderFlag = 0;//报单状态
            break;
        // case 'sf-22108-1'://按要货报单
        //     exp.sendFlag = 0;//发货状态
        //     exp.orderFlag = 0;//报单状态
        //     break;
        case 'sf-2319-1'://批量新增要货单
            exp.sendFlag = 0;//发货状态
            exp.orderFlag = 0;//报单状态
        default:
            break;
    };
    return exp;
};

/**
 * 拼接要货按批次查期望值
 * @param {object} params 
 */
function getInvAskListExp({ qfInvAskBill, sfInvAskBill }) {
    let exp = {
        totalDiffNum: 0,//差异数
        billno: sfInvAskBill.result.billno,
        // optime: sfInvAskBill.optime,
    };
    exp = Object.assign(exp, format.dataFormat(qfInvAskBill.result, 'sendFlag;orderFlag;totalSendnum;totalOrdernum;invalidflag;id=pk;totalnum;targetinvName=show_targetinvid;invName=show_invid;prodate'));
    exp.staffName = qfInvAskBill.result.show_deliver.split(',')[1];
    exp.totalDiffNum = common.sub(exp.totalnum, exp.totalOrdernum);
    return exp;

};

/**
 * 拼接要货按明细查期望值
 * @param {object} params 
 * @param {object} params.qfInvAskBill
 * @param {object} params.sfInvAskBill
 */
function getInvAskDetListExp({ qfInvAskBill, sfInvAskBill }) {
    //purprice,stdprice1,stdprice2,stdprice3,stdprice4,special
    let exp = [];
    let data = {
        opstaffname: qfInvAskBill.result.show_deliver.split(',')[1],
        billno: sfInvAskBill.result.billno,
        // optime: sfInvAskBill.optime,
        prodate: qfInvAskBill.result.prodate,
        invname: qfInvAskBill.result.show_invid,
        targetinvname: qfInvAskBill.result.show_targetinvid,
    };
    qfInvAskBill.result.details.forEach(detail => {
        let [stylecode, stylename] = detail.show_styleid.split(',');
        data = Object.assign({}, data, { stylecode, stylename }, format.dataFormat(detail, 'sendnum;propdresStyleid=styleid;num;id;rem;colorname=show_colorid;sizename=show_sizeid;flagid=flag;ordernum'));
        data.diffnum = common.sub(detail.num, detail.ordernum);
        exp.push(data);
    });
    return exp;
};

/**
 * 拼接要货调拨列表期望值
 * @param {object} qfRes 
 */
function getShopOutListForAskExp({ qfInvAskBill, sfInvAskBill }) {
    let sendFlag = ['未发货', '部分发货', '全部发货'];
    let staffName = qfInvAskBill.result.show_deliver.split(',')[1];
    let exp = {
        sendFlag: sendFlag[qfInvAskBill.result.sendFlag],//再议
        totalSendnum: 0,
        opstaffname: staffName,
        billno: sfInvAskBill.result.billno,
        // optime: sfInvAskBill.optime,
        staffName: qfInvAskBill.result.show_deliver.split(',')[1],
    };
    exp = Object.assign(exp, format.dataFormat(qfInvAskBill.result, 'invalidflag;id=pk;totalnum;targetinvName=show_targetinvid;invname=show_invid;prodate'));
    qfInvAskBill.result.details.forEach(detail => {
        exp.totalSendnum = common.add(exp.totalSendnum, detail.sendnum);
    });
    exp.totalDiffNum = common.sub(exp.totalnum, exp.totalSendnum);
    return exp;
};

/**
 * 拼接调拨详情期望值
 * @param {object} qfRes 
 */
function getShopOutInfoForAskExp(qfRes) {
    //details 中的checkprenum moverecvnum total，price pk
    //totalmoney 
    let exp = {
        totalnum: 0,
        type: 25,
        totalmoney: 0,
        details: [],
    };
    exp = Object.assign(exp, format.dataFormat(qfRes.result, 'prodate;show_shopid=show_targetinvid;billid=pk;shopid=targetinvid;invidref=invid;show_invidref=show_invid'));
    qfRes.result.details.forEach(detail => {
        let data = {
            // checkprenum: common.sub(detail.num, detail.sendnum),
            num: Math.max(common.sub(detail.num, detail.sendnum), 0),//发货数
            total: Math.max(common.mul(common.sub(detail.num, detail.sendnum), 200), 0),
            price: 200,//设置了调拨
        };
        exp.details.push(Object.assign(data, format.dataFormat(detail, 'sizeid;appnum=num;colorid;show_sizeid;show_sizeid;recvnum=sendnum;show_styleid;styleid;matid;billid=id;show_colorid;rem')));
        exp.totalnum = Math.max(common.add(exp.totalnum, common.sub(detail.num, detail.sendnum)), 0);
    });
    exp.totalmoney = common.mul(exp.totalnum, 200);
    return exp;
};

/**
 * 调拨保存后 要货详情期望值
 * @param {object} params 
 */
function getAskBillInfoAfterSend(sfRes) {
    let exp = {
        sendFlag: 0,
        orderFlag: 0,
        totalnum: 0,
        totalSendnum: 0,
        totalOrdernum: 0,
        // deliver: sfRes.result."356923",
        details: [],
        // show_deliver:sfRes.result. "000,总经理",
    };
    exp = Object.assign(exp, format.dataFormat(sfRes.params, 'flag;show_invid=show_invidref;targetinvid=shopid;prodate;invid=invidref;pk=billid;show_targetinvid=show_shopid'));
    sfRes.params.details.forEach(detail => {
        exp.details.push(Object.assign({ flag: detail.num >= detail.appnum ? 2 : 1 }, format.dataFormat(detail, 'num=appnum;sizeid;colorid;styleid;show_styleid;id=billid;sendnum=num;rem;matid;ordernum=recvnum')));
        exp.totalnum = common.add(exp.totalnum, detail.appnum);
        exp.totalSendnum = common.add(exp.totalSendnum, detail.num);
        exp.totalOrdernum = common.add(exp.totalOrdernum, detail.recvnum);
    });
    exp = Object.assign(exp, getAskBillInfoFlagExp(sfRes));
    return exp;
};

/**
 * 拼接要货报单详情 details里的期望值
 * @param {Object} qfRes 
 */
function getAskFormInfoExp(qfRes) {
    let details = [];
    for (const detail of qfRes.result.details) {
        let ordernum = common.sub(common.sub(detail.num, detail.sendnum), detail.ordernum);
        if (ordernum <= 0) continue;
        else {
            let data = {
                num: ordernum,
            };
            details.push(Object.assign(data, format.dataFormat(detail, 'sizeid;colorid;show_sizeid;show_styleid;styleid;askDetid=id;show_colorid;rem')));
        }
    };
    // details.push({
    //     // "sizeparentid": "1",
    //     // "invnum": "0",
    //     // "tagprice": "0",
    //     // "recvnum": "0",
    //     // "rowid": "2",
    //     // "matclsid": "0",
    //     // "total": "6400",
    //     // "price": "100",
    //     // "billid": "0",
    //     // "pk": "0",
    //     // "fileid": ""
    // });
    return details;
};

/**
 * 拼接按要货报单列表期望值
 * @param {object} param0 
 */
function getAskFormListExp({ qfInvAskBill, dwid = BASICDATA.dwidVell }) {
    let exp = {
        dwid,
        prodate1: qfInvAskBill.result.prodate,
        prodate2: qfInvAskBill.result.prodate,
        invid: qfInvAskBill.result.invid,
        invname: qfInvAskBill.result.show_invid,
        restnum: Math.max(common.sub(qfInvAskBill.result.totalnum, qfInvAskBill.result.totalSendnum), 0)
    };
    return exp;
};


/**
 * 拼接库存期望值
 * @param {Object} param
 */
function getInvExp({ sfRes, styleInfo }) {
    let invExp = {};
    sfRes.params.details.forEach(detail => {
        detail.total = common.mul(detail.num, styleInfo.purprice);
        //按要货调货
        if (sfRes.params.interfaceid == 'sf-2324-1') {
            //调入门店
            invExp[`${sfRes.params.invidref}-${detail.colorid}-${detail.sizeid}`] = format.dataFormat(detail, 'onroadnum=num;availStockNum=num;total');
            //调出门店
            invExp[`${sfRes.params.shopid}-${detail.colorid}-${detail.sizeid}`] = {
                invnum: -detail.num,
                total: -detail.total,
                availStockNum: -detail.num
            };
        } else {
            //按要货报单  
            invExp[`${sfRes.params.invid} -${detail.colorid} -${detail.sizeid} `] = format.dataFormat(detail, 'purednum=num;availStockNum=num');
        };
    });
    return invExp;
};

