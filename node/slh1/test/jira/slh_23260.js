'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const reqHandler = require('../../help/reqHandlerHelp');

//http://jira.hzdlsoft.com:7082/browse/SLH-23260
describe.skip('SLH-23260', function () {
	//查询2个月数据，需要延长超时限制
	this.timeout(60000);

	let salesMainSearchParmas, salesDetSearchParmas, purchaseMainSearchParmas, purchaseDetSearchParmas;
	before(async () => {
		let qlRes, qfUpdataLog;
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		const prodate = {
			prodate1: common.getDateString([0, 0, -7]),
			prodate2: common.getDateString([0, 0, -1]),
			pagesize: 15,
		};
		let params = {
			'ql-142201': prodate, //销售开单-按批次查
			'ql-1209': prodate, //销售开单-按明细查
			'ql-14433': prodate, //销售订货-按明细查
			'ql-15009': { //所有已核销
				dwid: BASICDATA.dwidXw
			},
			'ql-22301': prodate, //采购入库-按批次查
			'ql-22302': prodate, //采购入库-按明细查
		};
		qlRes = await common.getResults(params);

		qfUpdataLog = await reqHandler.queryBilling({
			interfaceid: 'qf-14211-3', //销售开单修改日志
			pk: qlRes['ql-15009'].dataList[0].id
		});
		let qfResPur = await reqHandler.queryBilling({
			interfaceid: 'qf-14212-1', //采购入库单明细
			pk: qlRes['ql-22301'].dataList[0].id,
		});

		salesMainSearchParmas = [{
			billno1: common.sub(qlRes['ql-142201'].dataList[0].billno, 100),
			billno2: qlRes['ql-142201'].dataList[0].billno,
		}, {
			orderno: qlRes['ql-14433'].dataList[0].billno
		}, {
			verifybillbillno: qfUpdataLog.verifybillBillno
		}, {
			shopid: LOGINDATA.shopid
		}, {
			invidck: LOGINDATA.invid
		}, {
			flag: qlRes['ql-142201'].dataList[0].flag
		}, {
			dwid: BASICDATA.dwidXw
		}, {
			dwfdid: BASICDATA.dwidLs
		}, {
			deliver: LOGINDATA.id,
		}, {
			rem: qlRes['ql-142201'].dataList[0].remark
		}, {
			dwtypeid: qlRes['ql-1209'].dataList[0].dwtypeid,
		}, {
			areaaccode: ''
		}, {
			invdisflag: qlRes['ql-142201'].dataList[0].invdisflag
		}, {
			createopid: LOGINDATA.id
		}, {
			printflag: qlRes['ql-142201'].dataList[0].printflag
		}, {
			modifyFlag: qlRes['ql-142201'].dataList[0].modifyFlag //是否修改
		}, {
			shipFlag: qlRes['ql-142201'].dataList[0].shipFlag //是否发货
		}, {
			shipperId: LOGINDATA.id
		}, {
			invdisopid: LOGINDATA.id
		}, {
			invalidflag: qlRes['ql-142201'].dataList[0].invalidflag
		}];
		salesDetSearchParmas = [{
			billno: qlRes['ql-142201'].dataList[0].billno
		}, {
			orderno: qlRes['ql-14433'].dataList[0].billno
		}, {
			sendinvid: LOGINDATA.shopid
		}, {
			shopid: LOGINDATA.shopid
		}, {
			dwid: BASICDATA.dwidXw
		}, {
			dwfdid: BASICDATA.dwidLs
		}, {
			styleclassid: '290153' //类别
		}, {
			deliver: LOGINDATA.id
		}, {
			styleid: BASICDATA.styleAgc001
		}, {
			stylename: qlRes['ql-1209'].dataList[0].mat_name
		}, {
			colorid: BASICDATA.coloridBaiSe
		}, {
			sizeid: BASICDATA.sizeidM
		}, {
			brandid: 1
		}, {
			marketdate1: common.getDateString([0, 0, -15]),
			marketdate2: common.getDateString([0, 0, -1])
		}, {
			numlower: 1
		}, {
			numupper: 10
		}, {
			typeid: 1 //1代表退货
		}, {
			stdpricetype: 1 //1代表零批价
		}, {
			rem: qlRes['ql-1209'].dataList[0].rem
		}, {
			dwtypeid: qlRes['ql-1209'].dataList[0].dwtypeid
		}, {
			areaaccode: ''
		}, {
			styledwid: BASICDATA.dwidVell
		}, {
			season: 1,
		}, {
			createopid: LOGINDATA.id
		}];
		purchaseMainSearchParmas = [{
			id1: common.sub(qlRes['ql-22301'].dataList[0].billno, 100),
			id2: qlRes['ql-22301'].dataList[0].billno,
		}, {
			shopid: qfResPur.result.invid,
		}, {
			invalidflag: qlRes['ql-22301'].dataList[0].invalidflag
		}, {
			flag: qlRes['ql-22301'].dataList[0].verifysum == 0 ? 0 : 1, //是否核销
		}, {
			dwid: qfResPur.result.dwid,
		}, {
			deliver: qfResPur.result.deliver,
		}, {
			remark: qfResPur.result.remark,
		}, {
			styleid: BASICDATA.styleAgc001,
		}];
		purchaseDetSearchParmas = [{
			mainBillno: qlRes['ql-22302'].dataList[0].mainBillno,
		}, {
			mainShopid: LOGINDATA.shopid,
		}, {
			mainInvid: LOGINDATA.invid
		}, {
			mainDwid: BASICDATA.dwidVell,
		}, {
			mainDeliver: qlRes['ql-22302'].dataList[0].seller,
		}, {
			concatPropdresStyleCodePropdresStyleName: qlRes['ql-22302'].dataList[0].propdresStyleCode,
		}, {
			propdresStyleid: BASICDATA.styleAgc001,
		}, {
			propdresColorid: BASICDATA.coloridBaiSe,
		}, {
			propdresSizeid: BASICDATA.sizeidM,
		}, {
			propdresStyleRespopid: LOGINDATA.id //买手
		}, {
			numlower: 10
		}, {
			numupper: 5
		}, {
			rem: qlRes['ql-22302'].dataList[0].rem
		}, {
			orgCode: BASICDATA.styleAgc001 //出厂款号
		}, {
			propdresStyleClassid: '290153',
		}, {
			propdresStyleBrandid: 1,
		}, {
			propdresStyleSeason: 1
		}];
	})

	//http://jira.hzdlsoft.com:7082/browse/SLH-23260
	describe.skip('SLH-23260.销售开单-按批次查与按明细查的合计 数据需要从同步表中获取,查询中心数据同步', function () {
		let res1 = [],
			res2 = []; //res1:查询中心中获取,res2:默认按老方式
		before(async function () {
			const params = format.qlParamsFormat({
				search_list: 0,
				prodate1: common.getDateString([0, 0, -3])
			}); //只获取汇总值

			//sales_sum_search_from 销售单汇总使用方式，0 默认按老方式，1 查询中心中获取
			await common.setGlobalParam('sales_sum_search_from', 1);
			res1 = await Promise.all([common.callInterface('ql-142201', params), common.callInterface('ql-1209', params)]);

			await common.setGlobalParam('sales_sum_search_from', 0);
			res2 = await Promise.all([common.callInterface('ql-142201', params), common.callInterface('ql-1209', params)]);
		});
		it('1.销售开单-按批次查合计检查', async function () {
			common.isApproximatelyEqualAssert(res2[0], res1[0], `销售开单-按批次查ql-142201,使用老方式与查询中心中获取的汇总值不匹配`);
		});
		it('2.销售开单-按明细查合计检查', async function () {
			common.isApproximatelyEqualAssert(res2[0], res1[0], `销售开单-按明细查ql-1209,使用老方式与查询中心中获取的汇总值不匹配`);
		});
	});

	//http://jira.hzdlsoft.com:7082/browse/SLH-23260
	describe.skip('SLH-23260.销售单汇总使用方式-查询中心中获取', function () {
		before(async function () {
			await common.setGlobalParam('sales_bill_sync_to_ecqc', 1);
			await common.setGlobalParam('sales_sum_search_from', 1);

		});
		it('1.销售开单-按批次查', async function () {
			const batchSearch = new SLH_23260('ql-142201');
			await batchSearch.test(salesMainSearchParmas);
		});
		it('2.销售开单-按明细查', async function () {
			const batchSearch = new SLH_23260('ql-1209');
			await batchSearch.test(salesDetSearchParmas);
		});
		it('3.采购入库-按批次查', async function () {
			const batchSearch = new SLH_23260('ql-22301');
			await batchSearch.test(purchaseMainSearchParmas);
		});
		it('3.采购入库-按明细查', async function () {
			const batchSearch = new SLH_23260('ql-22302');
			await batchSearch.test(purchaseDetSearchParmas);
		})
	});

	describe('SLH_23384.采购明细、销售明细性能优化', function () {
		before(async () => {
			// await common.setGlobalParam('sales_bill_sync_to_ecqc', 1); //是否同步销售单到查询中心 0为默认不同步1为同步
			// await common.setGlobalParam('sales_sum_search_from', 1); //销售单汇总使用方式 0为默认老方式1为查询中心获取
		});
		it('销售开单-按明细查', async function () {
			const batchSearch = new SLH_23260('ql-1209');
			await batchSearch.testSLH_23384(salesDetSearchParmas);
		});
		it('采购入库-按明细查', async function () {
			const batchSearch = new SLH_23260('ql-22302');
			await batchSearch.testSLH_23384(purchaseDetSearchParmas);
		});
	});
});



function SLH_23260(interfaceid) {
	this.interfaceid = interfaceid;
	this.defParam = format.qlParamsFormat({
		interfaceid: this.interfaceid,
		prodate1: common.getDateString([0, -2, 0]),
		prodate2: common.getDateString([0, 0, -1]),
	});
};
SLH_23260.prototype.test = async function (paramsArr) {
	let ret = true,
		defParam = this.defParam,
		qlParams = [];
	const promises = paramsArr.map(function (params) {
		const curParams = Object.assign({}, defParam, params);
		qlParams.push(curParams);
		return reqHandler.qlIFCHandler(curParams);
	});
	const datas = await Promise.all(promises);
	datas.forEach((data, index) => {
		console.log(`params=${JSON.stringify(qlParams[index])}`);
		if (data.error) {
			console.log(`请求出错,请求返回结果为:${JSON.stringify(data)}`);
		} else if (data.count == 0) {
			console.log(`查询无数据，无法进行汇总验证`);
		} else { //invalidflag
			const exp = common.getListSum(data.dataList, (data) => data.invalidflag == 0);
			ret = common.isApproximatelyEqual(exp, data.sumrow);
			if (ret.flag) {
				console.log(`结果正确`);
			} else {
				console.log(`汇总值错误 ${ret.errorMsg}`);
			};
		};
	});
};
//参数关闭和开启后，查询出的结果是一样的
SLH_23260.prototype.testSLH_23384 = async function (paramsArr) {
	let ret = true,
		defParam = this.defParam,
		qlParams = [];

	await common.setGlobalParam('search_designate_range', 0); //启用查询条件限定范围处理 0为不启用
	const promises = paramsArr.map(function (params) {
		const curParams = Object.assign({}, defParam, params);
		qlParams.push(curParams);
		return reqHandler.qlIFCHandler(curParams);
	});
	const datas1 = await Promise.all(promises);

	await common.setGlobalParam('search_designate_range', 1);
	const promises2 = paramsArr.map(function (params) {
		const curParams = Object.assign({}, defParam, params);
		qlParams.push(curParams);
		return reqHandler.qlIFCHandler(curParams);
	});
	const datas2 = await Promise.all(promises2);

	datas1.forEach((data, index) => {
		console.log(`params=${JSON.stringify(qlParams[index])}`);
		if (data.error) {
			console.log(`请求出错,请求返回结果为:${JSON.stringify(data)}`);
		} else if (data.count == 0) {
			console.log(`查询无数据，无法进行汇总验证`);
		} else {
			ret = common.isApproximatelyEqual(data, datas2[index]);
			if (ret.flag) {
				console.log(`结果正确`);
			} else {
				console.log(`汇总值错误 ${ret.errorMsg}`);
			}
		};
	});
};
