const common = require('../../../lib/common');
const basiceJson = require('../../help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData');
const salesResFmt = require('../../help/salesHelp/salesResFormat');

/**
 *
 * 开单显示库存,开启实时检查是否补货款,开启上次成交价做为本次开单价整合到一个接口
 * http://jira.hzdlsoft.com:7082/browse/SLH-22641
 *
 * 开单显示库存：cs-getinvnum-bycolorsize
 * 开启实时检查是否补货款：cs-checkColorSizeIsSecondSale
 * 开启上次成交价做为本次开单价：cs-getsalebill-lastprice(获取销售开单上次价)，cs-getsaleorder-lastprice(获取销售订货上次价)，cs-getlistpurprice(获取供应商上次价)
 *
 * 整合为
 * 获取款号库存，检查补货，上次价信息: cs-dresSpu-invsale-detailinfo
 * http://106.15.226.60:8080/showdoc/index.php?s=/2&page_id=1820
 *
 **/
describe('SLH-22641', function () {
	this.timeout(7000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('invnumList-显示库存', function () {
		let invInfo, curInvnum;
		before(async () => {
			[invInfo, curInvnum] = await Promise.all([getInfo({
				styleid: BASICDATA.styleidAgc001,
				shopid: LOGINDATA.invid,
				needsMark: 1,
				pagesize: 0,
			}), common.callInterface('ql-1932', { //货品管理-当前库存
				propdresStyleid: BASICDATA.styleidAgc001,
				invid: LOGINDATA.invid,
				pagesize: 0,
			})]);
		});
		it('1.needsMark检查', async function () {
			expect(invInfo.result).to.have.property('invnumList');
			expect(invInfo.result).not.to.have.any.keys('replenishDetList', 'lastPrice');
		});
		it('2.数据验证', async function () {
			common.isApproximatelyArrayAssert(curInvnum.dataList,
				salesResFmt.cs_getinvnumbycolorsizeFormat(invInfo.result.invnumList), [], `库存信息数据与当前库存信息不匹配`);
		});
	});

	describe('replenishDetList-查补货', function () {
		let replenishInfo, checkIsSecondSale;
		before(async () => {
			const sfRes = await common.editBilling(basiceJson.salesJson());
			sfRes.params.details.forEach(detail => detail.num = detail.num.toString());

			[replenishInfo, checkIsSecondSale] = await Promise.all([getInfo({
				needsMark: '10',
				dwid: sfRes.params.dwid,
				checkReplenishParam: {
					billid: sfRes.result.pk,
					billno: sfRes.result.billno,
					isPrint: 0,
					dresSku: sfRes.params.details,
				},
			}), common.callInterface('cs-checkColorSizeIsSecondSale', { //开启实时检查是否补货款
				jsonparam: {
					billno: sfRes.result.billno,
					bizType: 1100, //销售单
					dwid: sfRes.params.dwid,
					pk: sfRes.result.pk,
					details: sfRes.params.details,
				},
			})]);

		});
		it('1.needsMark检查', async function () {
			expect(replenishInfo.result).to.have.property('replenishDetList');
			expect(replenishInfo.result).not.to.have.any.keys('invnumList', 'lastPrice');
		});
		it('2.数据验证', async function () {
			common.isApproximatelyEqualAssert(replenishInfo.result.replenishDetList, checkIsSecondSale.dataList);
		});
	});

	describe('lastPrice-上次价', function () {
		let params, lastPriceInfo, lastPriceByBill;
		before(async () => {
			params = {
				needsMark: '100',
				styleid: BASICDATA.styleidAgc001,
				dwid: BASICDATA.dwidXw,
				shopid: LOGINDATA.invid,
				billType: 21, //单据类型，21销售单，150销售订单，10采购单，100采购订单
			};
			[lastPriceInfo, lastPriceByBill] = await Promise.all([getInfo(params),
			common.callInterface('cs-getsalebill-lastprice', {
				dwid: params.dwid,
				styleid: BASICDATA.styleidAgc001,
				shopid: LOGINDATA.invid,
			})]);
		});
		it('1.needsMark检查', async function () {
			expect(lastPriceInfo.result).to.have.property('lastPrice');
			expect(lastPriceInfo.result).not.to.have.any.keys('replenishDetList', 'invnumList');
		});
		it('2.销售单上次价数据检查', async function () {
			common.isApproximatelyEqualAssert(lastPriceByBill, lastPriceInfo.result.lastPrice);
		});
		it('3.销售订单上次价数据检查', async function () {
			params.billType = 150; //销售订单
			[lastPriceInfo, lastPriceByBill] = await Promise.all([getInfo(params),
			common.callInterface('cs-getsaleorder-lastprice', {
				clientid: params.dwid,
				styleid: BASICDATA.styleidAgc001,
				shopid: LOGINDATA.invid,
			})]);
			common.isApproximatelyEqualAssert(lastPriceByBill, lastPriceInfo.result.lastPrice);
		});
		it('3.供应商上次价数据检查', async function () {
			params.billType = 10; //采购单
			params.dwid = BASICDATA.dwidVell;
			[lastPriceInfo, lastPriceByBill] = await Promise.all([getInfo(params),
			common.callInterface('cs-getlistpurprice', {
				dwid: params.dwid,
				styleid: BASICDATA.styleidAgc001,
			})]);
			common.isApproximatelyEqualAssert(lastPriceByBill, lastPriceInfo.result.lastPrice);
		});
	});

});

async function getInfo(json) {
	//默认返回所有数据。需要返回哪几种数据，二进制表示，
	//第一位表示获取款号sku库存，第二位表示检查款号sku是否补货，第三位表示获取客户上次价，例如needsMark=7，则三种都返回
	let jsonparam = _.cloneDeep(json);
	jsonparam.needsMark = parseInt(jsonparam.needsMark, 2);
	let res = await common.callInterface('cs-dresSpu-invsale-detailinfo', {
		jsonparam: jsonparam,
	});
	expect(res, `cs-dresSpu-invsale-detailinfo请求失败,jsonparam:${JSON.stringify(jsonparam)}`).not.to.have.property('error');
	return {
		params: jsonparam,
		result: res
	};
};
