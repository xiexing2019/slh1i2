const common = require('../../../lib/common');
// const basiceJson = require('../../help/basiceJsonparam');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');

//http://jira.hzdlsoft.com:7082/browse/SLH-23860
//http://jira.hzdlsoft.com:7082/browse/SLHSEC-7312
describe('客户未发新模式', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	it('未发数验证', async function () {
		//客户未发(尺码头部)只取当前门店  销售订货-按明细查   当前库存
		const [dueoutRes, orderDetails, curInv] = await Promise.all([common.callInterface('ql-144313', {
			clientId: BASICDATA.dwidXw, styleId: BASICDATA.styleidAgc001, prodate1: common.getCurrentDate(), prodate2: common.getCurrentDate(),
		}), common.callInterface('ql-14431', format.qlParamsFormat({
			mainClientid: BASICDATA.dwidXw, propdresStyleid: BASICDATA.styleidAgc001, mainShopid: LOGINDATA.invid,
		}, true)), common.callInterface('ql-1932', format.qlParamsFormat({
			propdresStyleid: BASICDATA.styleidAgc001, invid: LOGINDATA.invid
		})).then((res) => getCurInvColorSize(res))]);

		const exp = getNotDeliverNumExp(orderDetails, curInv);
		common.isApproximatelyArrayAssert(exp.dataList, dueoutRes.dataList);
	});
});

/**
 * 拼接客户未发期望值
 * @description 排序规则 标签名称(订货标签 orderTagName),款号创建先后，订单日期，颜色创建先后(sid),尺码创建先后(sid)
 * @param {object} orderDetails 
 * @param {object} curInv
 */
function getNotDeliverNumExp(orderDetails, curInv) {
	let exp = {};
	orderDetails.dataList.forEach(element => {
		const notDeliverNum = common.sub(element.num, element.delivernum);
		//未发数=0不统计 --2018-9-19 xujxia
		//终结未发的明细不必统计
		if (element.flag != 3 && notDeliverNum > 0) {
			const key = `${element.propdresStyleCode}-${element.price}-${element.discount}`;
			const detailKey = `${element.propdresStyleCode}-${element.propdresColorid}-${element.propdresSizeid}`;
			if (!exp[key]) {
				exp[key] = {
					invNum: curInv.sumrow.invnum, //库存数
					styleId: curInv[detailKey].styleid,
					price: element.price,
					discount: element.discount,
					details: {}, //后续再转为数组
					styleName: element.propdresStyleName,
					styleCode: element.propdresStyleCode,
					realprice: common.mul(element.price, element.discount),
					notDeliverNum: 0,
					// orderDetIds: [],
				};
			};

			if (!exp[key].details[detailKey]) {
				exp[key].details[detailKey] = {
					colorName: element.propdresColorid,
					sizeId: curInv[detailKey].sizeid,
					invNum: curInv[detailKey].invnum,
					sizeName: element.propdresSizeid,
					colorId: curInv[detailKey].colorid,
					matId: curInv[detailKey].matid, //货品id
					notDeliverNum: 0,
					// orderDetIds: [], //23166649
					// mainKey: "10015574645--200.0-0.855"
				};
			};
			//库存
			// exp[key].details[detailKey].invNum = common.add(exp[key].details[detailKey].invNum, curInv[detailKey].invnum);
			// exp[key].invNum = common.add(exp[key].invNum, curInv[detailKey].invnum);

			//未发数(>0) = 数量-已发数
			exp[key].notDeliverNum = common.add(exp[key].notDeliverNum, notDeliverNum);
			exp[key].details[detailKey].notDeliverNum = common.add(exp[key].details[detailKey].notDeliverNum, notDeliverNum);
		};
		//订单id
		// exp[key].details[detailKey].orderDetIds.push(element.);
	});

	const dataList = Object.values(exp);
	dataList.forEach((data) => data.details = Object.values(data.details).sort((a, b) => `${a.colorId.padStart(3, 0)}${a.sizeId.padStart(3, 0)}` >= `${b.colorId.padStart(3, 0)}${b.sizeId.padStart(3, 0)}` ? 1 : -1));
	return {
		dataList,
		count: dataList.length
	};
};

function getCurInvColorSize(params) {
	let curInv = {};
	params.dataList.forEach((data) => {
		curInv[`${data.propdresStyleCode}-${data.propdresColorName}-${data.propdresSizeName}`] = data;
	});
	curInv.sumrow = params.sumrow;
	return curInv;
};
