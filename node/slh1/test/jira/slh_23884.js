const common = require('../../../lib/common');
const basiceJson = require('../../help/basiceJsonparam');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../help/reqHandlerHelp');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const urgentReqHandler = require('../../help/urgentHelp/urgentReqHandler');

//http://jira.hzdlsoft.com:7082/browse/SLH-23884
describe.skip('紧急模式服务端改进', function () {
	this.timeout(30000);
	let saveRes;
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();

		let salesJson = basiceJson.salesJson();

		let urgentBillList = [];
		//正常单
		urgentBillList.push(format.jsonparamFormat(basiceJson.salesJson()));
		urgentBillList.push(format.jsonparamFormat(basiceJson.salesJson()));
		//异常单
		urgentBillList[1].cash += 300;
		let a = getUrgentBillSaveTranslate(urgentBillList[0]);
		console.log(`a : ${JSON.stringify(a)}`);
		//保存紧急模式的单子，这里返回的是队列的id，实际可能是还没有保存成功，需要等待
		// saveRes = await reqHandler.sfIFCHandler({
		// 	interfaceid: 'cs-saveUrgentBillBatch',
		// 	jsonparam: {
		// 		urgentBillList: urgentBillList,
		// 		apiName: salesJson.interfaceid
		// 	}
		// });
		// saveRes = await urgentReqHandler.saveUrgentBill({
		// 	jsonparam: {
		// 		urgentBillList: urgentBillList,
		// 		apiName: salesJson.interfaceid
		// 	}
		// });
		//获取紧急模式的单据列表
		//qlUrgentBill 长度为0，接下去走，不为0，等待几秒就抛出异常
		let qlUrgentBill;
		for (let i = 0; i < saveRes.params.jsonparam.urgentBillList.length; i++) { }
		//查询列表


		//还在处理中，说明就有问题，需要抛出异常
		if (qlUrgentBill.dataList.length > 0) throw new Error(`紧急模式上传cs-saveUrgentBillBatch保存不成功`);
	});
	after(async function () {
		//删除紧急模式异常单据
		// qlUrgentBill = await urgentReqHandler.getUrgentBillList();
		// for(let i = 0; i < res.dataList.length; i++) {
		// 	await urgentReqHandler.cancelUrgentBill({
		// 		pk: res.dataList[i].id
		// 	});
		// };
		// res = await urgentReqHandler.getUrgentBillList();
		// expect(res.dataList.length, '紧急模式异常单据未删除完全').to.equal(0);
	})
	it('', async function () {
		console.log(`1 : ${JSON.stringify(1)}`);
	})
	it.skip('', async function () {
		let sfResSales = await common.editBilling(basiceJson.salesJson());
		console.log(`sfResSales : ${JSON.stringify(sfResSales)}`);
	});
	it.skip('', async function () {
		let res = await common.callInterface('ql-14210', {
			prodate1: common.getCurrentDate(),
			prodate2: common.getCurrentDate(),
			flagIn: 1, //状态类型，-1表示保存单据，0表示客户端删除，1表示保存失败，2表示保存中
		});
		let csResult = await reqHandler.csIFCHandler({
			interfaceid: 'cs-urgentBillPreview',
			pk: res.dataList[0].id,
		});
		let jsonparam = getUrgentBillSave(csResult.result);
		// let sf = await reqHandler.sfIFCHandler({
		// 	interfaceid: 'cs-urgentBillSave',
		// 	jsonparam: jsonparam
		// });
	})
	// for (let index = 0; index < saveRes.params.jsonparam.urgentBillList.length; index++) {
	it.skip('', async function () {

		let csResult = await reqHandler.csIFCHandler({
			interfaceid: 'cs-urgentBillPreview',
			pk: res.dataList[0].id,
		});
		let sf = await reqHandler.sfIFCHandler({
			interfaceid: 'cs-urgentBillSave',
			jsonparam: csResult.result
		});
		console.log(`sf : ${JSON.stringify(sf)}`);
		//删除紧急模式上传的数据
		for (let i = 0; i < res.dataList.length; i++) {
			let deleteRes = await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancelUrgentBill',
				pk: res.dataList[i].id
			});
			console.log(`deleteRes : ${JSON.stringify(deleteRes)}`);
		};
		res = await common.callInterface('ql-14210', {
			prodate1: common.getCurrentDate(),
			prodate2: common.getCurrentDate(),
			flagIn: 1, //状态类型，-1表示保存单据，0表示客户端删除，1表示保存失败，2表示保存中
		});
		// console.log(`res : ${JSON.stringify(res)}`);
		// //再次查询，ql-14210 ，这条单据的flagIn变成0
		// console.log(`sf : ${JSON.stringify(sf)}`);
	});
	// };

});


/**
 * 紧急模式上传成功的单据
 *
 * @param  {object} qlRes
 * @return {object}
 */
async function successUrgentBill(sfRes) {
	let qfRes = await salesReqHandler.salesQueryBilling(qlRes.billid); //传单据pk
	common.isApproximatelyEqualAssert(sfRes, qfRes);
};


/**
 * flag为1 还在处理中的单据，等待处理完，
 *
 * @param  {object} qlRes
 * @return {object}
 */
async function saveUrgengBilling(qlRes) {

};


/**
 * 紧急模式异常单据-修改再次保存
 *
 * @param  {object} qlRes description
 * @return {object}       description
 */
async function failedUrgentBillSave(qlRes) {
	//紧急模式上传失败的单据，再次保存
	let preRes = await urgentReqHandler.getUrgentBillPreview({
		pk: qlRes.id
	});
	//修改变成正常的单子，然后保存
	let params = getUrgentBillSave(preRes);
	let sfRes = await urgentReqHandler.saveUrgentFailedBill({
		jsonparam: params
	});

}


/**
 * 紧急模式异常单据-删除
 *
 * @param  {object} qlRes description
 * @return {object}       description
 */
async function failedUrgentBillDelete(qlRes) {
	//删除紧急模式保存失败的单据
	await urgentReqHandler.cancelUrgentBill({
		pk: qlRes.id
	});
	let res = await urgentReqHandler.getUrgentBillList({
		flagIn: 0,
	});
	let act = common.takeWhile(res.result.dataList, obj => obj.id == qlRes.id);
	expect(act.length).to.equal(1);
	expect(act.flag).to.equal(0);
	common.isApproximatelyEqualAssert(qlRes, act, ['flag']);
};


//失败的单据再次保存，格式需要转换
function getUrgentBillSaveTranslate(previewRes) {
	let jsonparam = _.cloneDeep(previewRes);
	for (let i = 0; i < previewRes.details.length; i++) {
		for (let key of Object.keys(previewRes.details[i])) {
			jsonparam[`details_${key}_${i}`] = previewRes.details[i][key];
		}
	};
	delete jsonparam.details;
	return jsonparam;
};
