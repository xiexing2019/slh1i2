'use strict';
const common = require('../../../lib/common.js');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler.js');
const mainReqHandler = require('../../help/basicInfoHelp/mainDataReqHandler.js');
// const reqHandler = require('../../../help/reqHandlerHelp.js');
const purRequestHandler = require('../../help/purchaseHelp/purRequestHandler.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler');
const mainTest = require('../../../reqHandler/slh1/index')

describe('主流程-mainLine', function () {
    this.timeout(30000);
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        // console.log(`BASICDATA=${JSON.stringify(BASICDATA)}`);
    });
    describe('主要流程', function () {
        let style, cust;
        it('新增货品', async function () {
            style = await mainTest.slh1MetManager.saveGoodFull({
                jsonparam: basiceJsonparam.addGoodJson()
            })
            let styleInfo = await mainTest.slh1MetManager.queryGoodFull({
                'pk': style.pk
            })
            common.isApproximatelyEqualAssert(style.param, styleInfo);
        });
        it('新增客户', async function () {
            cust = await mainReqHandler.saveCust(basiceJsonparam.addCustJson());
            let custInfo = await mainReqHandler.getCustInfo({
                pk: cust.result.val
            });
            common.isApproximatelyEqualAssert(cust.params, custInfo);
        });
        it('新增采购单', async function () {
            let json = basiceJsonparam.purchaseJson();
            json.details.forEach(ele => {
                ele.styleid = style.pk;
            });
            let sfPurRes = await common.editBilling(json);
            // console.log(`${JSON.stringify(sfPurRes)}`);
            let qfPurRes = await purRequestHandler.purQueryBilling({
                pk: sfPurRes.result.pk
            });
            // console.log(`\n${JSON.stringify(qfPurRes)}`);
            common.isApproximatelyEqualAssert(sfPurRes.params, qfPurRes.result, ['pk']);
        });
        it('新增销售单', async function () {
            let json = basiceJsonparam.salesJson();
            json.dwid = cust.result.val;
            json.details.forEach(ele => {
                ele.styleid = style.pk;
            });
            let sfSalesRes = await common.editBilling(json);
            let qfSalesRes = await salesReqHandler.salesQueryBilling({
                pk: sfSalesRes.result.pk
            })
            common.isApproximatelyEqualAssert(sfSalesRes.params, qfSalesRes.result, ['pk']);
        });
    });
});