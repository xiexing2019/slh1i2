'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 开单模式18 整单折扣+代收
 */
describe('开单模式18 整单折扣+代收--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 18);
		// console.log(`LOGINDATA = ${JSON.stringify(LOGINDATA)}`);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	describe('挂单', function () {
		let hangJson = {};
		before(async function () {
			hangJson = basiceJsonparam.salesJsonPaymenthod18();
			hangJson.invalidflag = 9; //挂单
		});

		it('170365.挂单转为销售单.rankA', async function () {
			let hangRes = await salesRequest.editSalesHangBill(hangJson); //新增挂单
			hangRes.params.pk = hangRes.result.pk;
			let sfRes = await salesRequest.hangToSalesBill(hangRes.params);
			let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result, ['pk']); //挂单转销售单后，数据验证

			let param = {
				id1: qfRes.result.billno,
				id2: qfRes.result.billno,
			};
			let result = await common.callInterface('ql-1432', format.qlParamsFormat(param, true)); //销售开单-物流商查询
			expect(result.dataList[0]).to.includes({
				agencyflag: '是',
				logisdwxxname: qfRes.result.show_logisDwid,
				shopname: qfRes.result.show_shopid,
				// id: qfRes.result.pk,//物流单id,单据明细未返回
				logisdwxxid: qfRes.result.logisDwid,
				dwxxname: qfRes.result.show_dwid,
				invalidflag: '0', //是否作废
				finpayagency: qfRes.result.agency,
				dwxxid: qfRes.result.dwid,
				billno: qfRes.result.billno,
				logispayflag: '否', //货款收讫 未核销
			}, `error: ${JSON.stringify(result)}`);
		});

		it('170366.二次挂单.rankA', async function () {
			let hangRes = await salesRequest.editSalesHangBill(hangJson); //新增挂单
			let qfRes = await salesRequest.salesQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			}); //查询挂单详细信息
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result); //挂单数据验证

			let json = hangRes.params;
			json.pk = qfRes.params.pk;
			json.details.push({ //添加明细
				"num": "3",
				"sizeid": "XL",
				"matCode": "Agc001",
				"rem": "明细2",
				"colorid": "BaiSe",
				"price": "200",
				"discount": "0.8"
			});
			json.agency += 480;
			hangRes = await salesRequest.editSalesHangBill(json); //二次挂单
			qfRes = await salesRequest.salesQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);
		});
	});

	describe('物流单+核销', function () {
		let billRes = {},
			qlRes = {}, //ql起始值
			curQlRes = {}, //开单后的结果
			qlParams = {},
			jsonparam = {},
			verifyJson = {}; //代收单核销的jsonparam
		before(async function () {
			qlParams = {
				'ql-1623': { //综合汇总
					invid: LOGINDATA.invid,
					today: true,
				},
				'ql-14451': { //按店员汇总
					shopid: LOGINDATA.invid,
					staffid: LOGINDATA.id,
					today: true,
				},
				'ql-14424': { //按金额汇总
					shopid: LOGINDATA.invid,
					today: true,
				},
			};
			jsonparam = basiceJsonparam.salesJsonPaymenthod18();
			qlRes = await common.getResults(qlParams); //获取起始值
		});

		it('170368.销售开单-代收后核销物流单.rankA', async function () {
			billRes = await common.editBilling(jsonparam); //新增销售单
			let verifyRes = await common.callInterface('ql-14521', format.qlParamsFormat({ //代收单列表
				shopid: billRes.params.shopid,
				logisPayflag: 0,
				dwid: billRes.params.dwid,
				logisDwid: billRes.params.logisDwid,
			}, true));

			verifyJson = {
				'billno': verifyRes.dataList[0].billno,
				'cash': 200,
				'card': 180,
				'remit': 100,
				'logisVerifybillids': verifyRes.dataList[0].id,
				'logisVerifysum': verifyRes.dataList[0].finpayagency,
				'remark': '物流商核销',
				'dwid': verifyRes.dataList[0].logisdwxxid,
			};
			let verifyBillRes = await common.callInterface('sf-1452', {
				jsonparam: format.verifyJsonFormat(verifyJson),
			});

			let param = format.qlParamsFormat({
				id1: verifyBillRes.billno,
				id2: verifyBillRes.billno,
				shopid: billRes.params.shopid,
				dwid: verifyRes.dataList[0].logisdwxxid, //物流商
				invalidflag: 0, //作废
			}, true);
			let verifyQlRes = await common.callInterface('ql-1442', param);
			verifyJson.billno = verifyBillRes.billno;
			expect(verifyQlRes, `代收收款查询无结果`).to.satisfy((res) => res.dataList.length > 0);
			common.isApproximatelyEqualAssert(verifyJson, verifyQlRes.dataList[0]);
			// console.log(`verifyQlRes = ${JSON.stringify(verifyQlRes)}`);
			//await common.delay(1000);
			curQlRes = await common.getResults(qlParams); //获取开单后数据

			// console.log(`curQlRes = ${JSON.stringify(curQlRes)}`);
		});

		//核销用现金，刷卡，汇款3种付款方式并在收支流水中进行校对，验证类型：代收收款
		it('170371.收支流水界面检查代收款核销单.rankA', async function () {
			let qlRes1 = await common.callInterface('ql-1352', format.qlParamsFormat({ //收支流水
				billtype: 80,
				accountInvid: LOGINDATA.invid,
			}, true));
			let exp = [{
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.cash,
			}, {
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.card,
			}, {
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.remit,
			}];
			common.isApproximatelyArrayAssert(exp, qlRes1.dataList.slice(0, exp.length));
		});
		it('170369.销售开单-销售汇总-按店员汇总，检查代收', async function () {
			let diff = {
				daishou: billRes.params.agency,
				actualnum: billRes.params.totalnum,
				salecount: 1, //笔数
				totalmoney: billRes.params.agency,
				actualsalesmoney_noothercost: billRes.params.agency,
				num: billRes.params.totalnum,
				outsum: billRes.params.agency,
			};
			let exp = common.addObject(qlRes['ql-14451'].dataList[0], diff);

			//avgprice 客单均价
			common.isApproximatelyEqualAssert(exp, curQlRes['ql-14451'].dataList[0], ['avgprice', 'backrate']); //
		});
		it('170370.销售开单-销售汇总-按金额汇总，检查代收.rankA', async function () {
			let diff = {
				daishou: billRes.params.agency,
			};
			let exp = common.addObject(qlRes['ql-14424'].dataList[0], diff);
			//avgprice 客单均价
			common.isApproximatelyEqualAssert(exp, curQlRes['ql-14424'].dataList[0], ['avgprice']);
		});
		//依赖170368
		it('170372.统计分析-综合汇总，检查代收.rankA', async function () {
			let diff = {
				daishou: billRes.params.agency,
				actualnum: billRes.params.totalnum,
				totalmoney: billRes.params.agency,
				actualsalesmoney: billRes.params.agency,
				num: billRes.params.totalnum,
				collection: billRes.params.agency,
			};
			let exp = common.addObject(qlRes['ql-1623'].dataList[0], diff);
			common.isApproximatelyEqualAssert(exp, curQlRes['ql-1623'].dataList[0]);
		});
	});
});
