'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 开单模式21 异地+代收
 */
describe('开单模式21 异地+代收--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 21);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	//发货门店不填写，则默认本门店
	it('170674.发货门店默认值验证.rankA', async function () {
		let json = basiceJsonparam.salesJsonPaymenthod21();
		delete json.invid;
		let sfRes = await common.editBilling(json);
		let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
		expect(qfRes.result.invid).to.equal(qfRes.result.shopid);
	});

	it('170682/170683.按订货开单+按批次查/物流单检查.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson21()); //新增订货单
		let qfRes = await salesOrderReqHandler.salesOrderQueryBilling(sfRes.result.pk);
		common.isApproximatelyEqualAssert(sfRes.params, qfRes.result); //主要检查门店
		qfRes.result.agency = Math.abs(qfRes.result.balance);
		qfRes.result.balance = 0;
		qfRes.result.logisDwid = BASICDATA.dwidSFKD;
		sfRes = await salesReqHandler.editSalesBillByOrder(qfRes.result); //按订货开单-全部发货
		qfRes.result.details.forEach((obj, i) => {
			sfRes.params.details[i].total = common.mul(obj.num, obj.price);
		});
		await agencyBillQlResCheck(sfRes, true); //销售单修改界面+按批次查+物流单 查询结果检查
	});

	describe('销售开单-挂单', function () {
		let [hangJson, hangRes] = [{}, {}];
		before(async function () {
			hangJson = basiceJsonparam.salesJsonPaymenthod21();
			hangJson.invalidflag = 9; //挂单
		});

		it('170674.挂单.rankA', async function () {
			hangRes = await salesReqHandler.editSalesHangBill(hangJson); //新增挂单
			let qfRes = await salesReqHandler.salesQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result); //挂单单据验证

			let param = format.qlParamsFormat({
				shopid: hangRes.params.shopid,
				dwid: hangRes.params.dwid,
				invalidflag: 9,
				remark: hangRes.params.remark,
			}, true);
			let qlRes = await common.callInterface('ql-14222', param); //销售开单-按挂单 查询
			let exp = {
				invname: '中洲店',
				shopname: '常青店',
				billno: 0,
				id: hangRes.result.pk,
				invalidflag: 9,
				agency: hangRes.params.agency,
				finpayPaysum: 0,
			};
			expect(qlRes.dataList).to.satisfy((arr) => common.isArrayContainObject(arr, exp), `查询结果中未找到pk为${exp.id}的挂单\ndataList:${JSON.stringify(qlRes.dataList)}`);
		});

		//按批次查界面、修改界面、物流单
		it('170675.挂单转销售单.rankA', async function () {
			hangRes.params.pk = hangRes.result.pk;
			let sfRes = await salesReqHandler.hangToSalesBill(hangRes.params); //挂单转销售单
			await agencyBillQlResCheck(sfRes); //销售单修改界面+按批次查+物流单 查询结果检查
		});
	});

	describe('物流单+核销', function () {
		let [paramQl1932, originRes, billRes, verifyJson] = [{}, {}, {}, {}]; //代收单核销的jsonparam
		before(async function () {
			paramQl1932 = {
				'propdresStyleid': BASICDATA.styleidAgc001,
				'propdresColorid': BASICDATA.coloridBaiSe,
				'propdresSizeid': BASICDATA.sizeidM,
			};
			originRes = await common.callInterface('ql-1932', paramQl1932);
			billRes = await common.editBilling(basiceJsonparam.salesJsonPaymenthod21()); //新增销售单
		});

		it('170671.检查库存变化', async function () {
			let getStock = function (res) {
				let arr = [0, 0]; //常青店 中洲店
				res.dataList.map((obj) => {
					if (obj.inventoryName == '常青店') arr[0] = obj.invnum;
					if (obj.inventoryName == '中洲店') arr[1] = obj.invnum;
				});
				return arr;
			};
			let curRes = await common.callInterface('ql-1932', paramQl1932);
			expect(billRes.params.details[0].num).to.satisfy((diffNum) => {
				let [a, b] = [getStock(originRes), getStock(curRes)];
				return a[0] == b[0] && a[1] - diffNum == b[1];
			});

			let param = {
				'propdresStyleid': BASICDATA.styleidAgc001,
				'styleid': BASICDATA.styleidAgc001,
				'search_list': 0,
				'search_count': 0,
				'search_sum': 1,
			};
			let resQl1932 = await common.callInterface('ql-1932', param);
			let resQl1542 = await common.callInterface('ql-1542', param);
			expect(resQl1932.sumrow.invnum).to.equal(resQl1542.sumrow.invnum);
		});

		it('170672.单据修改界面+按批次查+物流商 查询结果检查.rankA', async function () {
			await agencyBillQlResCheck(billRes); //销售单修改界面+按批次查+物流单 查询结果检查
		});

		it('170673.销售开单-代收后核销物流单.rankA', async function () {
			let verifyRes = await common.callInterface('ql-14521', format.qlParamsFormat({ //代收单列表
				shopid: billRes.params.shopid,
				logisPayflag: 0,
				dwid: billRes.params.dwid,
				logisDwid: billRes.params.logisDwid,
			}, true));

			verifyJson = {
				'billno': verifyRes.dataList[0].billno,
				'cash': 100,
				'card': 200,
				'remit': 300,
				'logisVerifybillids': verifyRes.dataList[0].id,
				'logisVerifysum': verifyRes.dataList[0].finpayagency,
				'remark': '物流商核销',
				'dwid': verifyRes.dataList[0].logisdwxxid,
			};
			let verifyBillRes = await common.callInterface('sf-1452', { //核销代收单
				jsonparam: format.verifyJsonFormat(verifyJson),
			});

			let param = format.qlParamsFormat({
				id1: verifyBillRes.billno,
				id2: verifyBillRes.billno,
				shopid: billRes.params.shopid,
				dwid: verifyRes.dataList[0].logisdwxxid, //物流商
				invalidflag: 0, //作废
			}, true);
			let verifyQlRes = await common.callInterface('ql-1442', param); //代收收款查询
			verifyJson.billno = verifyBillRes.billno;
			expect(verifyQlRes, `代收收款查询无结果`).to.satisfy((res) => res.dataList.length > 0);
			common.isApproximatelyEqualAssert(verifyJson, verifyQlRes.dataList[0]);
		});

		//核销用现金，刷卡，汇款3种付款方式并在收支流水中进行校对，验证类型：代收收款
		it('170673.收支流水界面检查代收款核销单.rankA', async function () {
			let qlRes = await common.callInterface('ql-1352', format.qlParamsFormat({ //收支流水
				billtype: 80,
				accountInvid: LOGINDATA.invid,
			}, true));
			let exp = [{
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.cash,
			}, {
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.card,
			}, {
				prodate: common.getCurrentDate('YY-MM-DD'),
				billtype: '代收收款',
				billno: verifyJson.billno,
				rem: '单位[顺丰快递]',
				money: verifyJson.remit,
			}];
			common.isApproximatelyArrayAssert(exp, qlRes.dataList.slice(0, exp.length));
		});
	});

	describe('店员权限', function () {
		before(async function () {
			await common.loginDo({ //店长登陆
				'logid': '004',
				'pass': '000000',
			});
		});
		after(async function () {
			await common.loginDo();
		});
		it('170679.销售开单-店长.rankA', async function () {

			let sfRes = await common.editBilling(basiceJsonparam.salesJsonPaymenthod21());
			let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);
		});
		it('170680.销售开单-挂单-店长.rankA', async function () {
			let hangRes = await salesReqHandler.editSalesHangBill(basiceJsonparam.salesJsonPaymenthod21()); //新增挂单
			let qfRes = await salesReqHandler.salesQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);
		});
		it('170649.销售订货-店长.rankA', async function () {
			let sfRes = await common.editBilling(basiceJsonparam.salesOrderJson21());
			let qfRes = await salesOrderReqHandler.salesOrderQueryBilling(sfRes.result.pk);
			common.isApproximatelyEqualAssert(sfRes.params, qfRes.result);

			let param = format.qlParamsFormat({
				orderno1: qfRes.result.billno,
				orderno2: qfRes.result.billno,
			}, true);
			let result = await common.callInterface('ql-14223', param); //销售开单-按订货开单
			expect(result.dataList[0], `按订货开单 门店错误\n${result.dataList[0]}`).to.includes({
				shopname: LOGINDATA.invname //门店是开单门店
			});
		});
		it('170650.销售订货-挂单-店长.rankA', async function () {
			let hangRes = await salesOrderReqHandler.editSalesOrderHangBill(basiceJsonparam.salesOrderJson21()); //新增销售订货挂单
			let qfRes = await salesOrderReqHandler.salesOrderQueryBilling({
				pk: hangRes.result.pk,
				isPend: 1,
			});
			common.isApproximatelyEqualAssert(hangRes.params, qfRes.result);
		});
	});

	describe.skip('销售开单同时订货', function () {
		before(async () => {
			await common.setGlobalParam('sales_orderwhensales', 1);
		});
		after(async () => {
			await common.setGlobalParam('sales_orderwhensales', 0);
		});
		it('170678,销售开单同时订货.rankA', async function () {

			let sfRes = await common.editBilling(basiceJsonparam.salesJsonPaymenthod21_1()); //异地发货+代收+开单同时订货
			// 验证开单信息
			let salesInfo = await salesReqHandler.salesQueryBilling(sfRes.result.pk); //销售开单明细查询
			//console.log(`salesInfo.result : ${JSON.stringify(salesInfo.result)}`);
			let batchSales = await common.callInterface('ql-142201', format.qlParamsFormat({
				'invid': LOGINDATA.invid,
				'billno1': sfRes.result.billno,
				'billno2': sfRes.result.billno,
				'dwid': sfRes.params.dwidXw
			}, true)); //销售开单-按批次查
			common.isApproximatelyEqualAssert(sfRes.params, salesInfo.result);
			common.isApproximatelyEqualAssert(salesInfo.result, batchSales.dataList[0], ['actualpay']); //代收时，按批次查界面实收为0，明细界面为600
			expect(salesInfo.result, `发货门店显示有误`).to.includes({
				'show_invid': '中洲店',
				'invid': BASICDATA.shopidZzd
			});
			expect(batchSales.dataList[0], `门店有误`).to.includes({
				"invname": "中洲店",
				"maininvname": LOGINDATA.depname
			});

			// 验证订货信息 billid2
			let orderInfo = await salesReqHandler.salesOrderQueryBilling(salesInfo.result.billid2); //qf未返回billid2
			//console.log(`orderInfo : ${JSON.stringify(orderInfo)}`);
			let batchOrder = await common.callInterface('ql-14433', format.qlParamsFormat({
				'clientid': LOGINDATA.dwidXw,
				'id1': orderInfo.result.billno,
				'id2': orderInfo.result.billno
			}, true)); //销售订货-按批次查
			let orderJsonparam = getExpectedOrderInfo(sfRes.params);
			common.isApproximatelyEqualAssert(orderJsonparam, orderInfo.result, ['cashaccountid', 'cardaccountid', 'remitaccountid']); //销售开单同时订货，订货单默认未付，故支付信息为空可忽略
			common.isApproximatelyEqualAssert(orderInfo.result, batchOrder.dataList[0], ['flag']);
			common.isApproximatelyEqualAssert({
				'show_invid': '中洲店',
				'invid': BASICDATA.shopidZzd
			}, orderInfo.result);
			common.isApproximatelyEqualAssert({
				'num': orderJsonparam.totalnum,
				"inventoryname": "中洲店",
				"invname": "常青店",
			}, batchOrder.dataList[0]);
		});
	});

});

/**
 * agencyBillQlResCheck - 异地+代收 销售单修改界面+按批次查+物流单 查询结果检查
 * @param {Object}    sfRes
 * @param {boolean} [byOrder=false] 是否订单 byOrder=true时 不检查物流单结果
 */
async function agencyBillQlResCheck(sfRes, byOrder = false) {
	let qfRes = await salesReqHandler.salesQueryBilling(sfRes.result.pk);
	common.isApproximatelyEqualAssert(common.mixObject(sfRes.params, sfRes.result), qfRes.result, ['version']); //单据修改界面数据验证
	//按订货入库，应该为核销预付款单 批次号-1
	let params = {
		'ql-142201': { //销售开单-按批次查
			billno1: qfRes.result.billno,
			billno2: qfRes.result.billno,
		},
		'ql-1432': { //销售开单-物流商查询
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
		},
	};
	let res = await common.getResults(params);
	common.isApproximatelyEqualAssert({
		billno: qfRes.result.billno,
		maininvname: '常青店',
		invname: '中洲店',
		daishou: qfRes.result.agency,
		distribretsum: qfRes.result.agency,
		actualpay: '0',
	}, res['ql-142201'].dataList[0]);
	if (byOrder == false) {
		common.isApproximatelyEqualAssert({
			agencyflag: '是',
			logisdwxxname: qfRes.result.show_logisDwid,
			shopname: qfRes.result.show_shopid,
			// id: qfRes.result.pk,//二代单据pk!=物流单pk
			logisdwxxid: qfRes.result.logisDwid,
			dwxxname: qfRes.result.show_dwid,
			invalidflag: '0', //是否作废
			finpayagency: qfRes.result.agency,
			dwxxid: qfRes.result.dwid,
			billno: qfRes.result.billno,
			logispayflag: '否', //货款收讫 未核销
		}, res['ql-1432'].dataList[0]);
	};
};

/*
 * 根据开单数据 生成销售订货的期望数据  (销售订货的客户、门店、店员、款号等数据跟销售开单数据一致)
 */
function getExpectedOrderInfo(salesJsonparam) {
	let salesOrderInfo = salesJsonparam;
	salesOrderInfo.interfaceid = "sf-14401-1";
	salesOrderInfo.cash = "0";
	salesOrderInfo.card = "0";
	salesOrderInfo.remit = "0";
	salesOrderInfo.srcType = "2";
	delete salesOrderInfo.balance;
	delete salesOrderInfo.totalmoney;
	salesOrderInfo.sellerid = salesOrderInfo.deliver;
	salesOrderInfo.clientid = salesOrderInfo.dwid;

	for (let i = 0; i < salesOrderInfo.details.length; i++) {
		salesOrderInfo.details[i].num = salesOrderInfo.details[i].recvnum;
		delete salesOrderInfo.details[i].recvnum;
	}

	return format.jsonparamFormat(salesOrderInfo);
}
