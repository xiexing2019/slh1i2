'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
// const salesOrderRequest = require('../../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 开单模式2 现金+刷卡+汇款+代收
 */

describe('现金+刷卡+汇款+代收--mainLine', function () {
	this.timeout(30000);
	let dwidYTKD; //圆通快递

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 2);

		let res = await common.callInterface('cs-getpk-dwxx', {
			name: '圆通快递'
		});
		dwidYTKD = res.id;
	});

	it('170090,开代收单，验证列表中代收金额是否正确.rankA', async function () {
		// 销售汇总--按客户销售
		let summaryList = await common.callInterface('ql-14457', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));

		// 开代收单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		let searchCondition = {
			'dwid': BASICDATA.dwidXw,
			'billno1': salesResult.result.billno,
			'billno2': salesResult.result.billno,
			'shopid': LOGINDATA.invid
		}
		let batchList = await common.callInterface('ql-142201', format.qlParamsFormat(searchCondition, true));
		let summaryListAft = await common.callInterface('ql-14457', format.qlParamsFormat({
			'dwid': BASICDATA.dwidXw
		}, true));
		common.isApproximatelyEqualAssert({
			'daishou': salesResult.params.agency
		}, batchList.dataList[0]);
		common.isApproximatelyEqualAssert({
			'daishou': common.add(summaryList.dataList[0].daishou, salesResult.params.agency)
		}, summaryListAft.dataList[0]);
	});

	it('170094,再次进入代收单，查看物流商信息是否正确', async function () {
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson()); // 开代收单
		let agencyInfo = await salesRequest.getAgencyInfo(salesResult.result.pk); //获取物流信息

		common.isApproximatelyEqualAssert(salesResult.params, agencyInfo);
	});

	it('170278,物流单查询', async function () {
		// 开代收单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'id1': salesResult.result.billno,
			'id2': salesResult.result.billno,
			'logisBillno': salesResult.params.logisBillno,
			'shopid': LOGINDATA.invid
		}, true));
		expect(logisList.dataList[0], `物流单货款收迄有误:${JSON.stringify(logisList.dataList[0])}`).to.includes({
			"logispayflag": "否"
		});
	});

	it('170279,物流单查询，客户查询条件', async function () {
		// 开代收单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'dwid': salesResult.params.dwid,
			'prodate1': common.getDateString([0, 0, -15]),
			'prodate2': common.getDateString([0, 0, 0])
		}));
		let isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].dwxxid != salesResult.params.dwid) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `物流单查询，客户查询条件有误:${JSON.stringify(logisList.dataList)}`).to.be.true;
	});

	it('170284,物流单查询，单独查询条件', async function () {
		// 开代收单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		// 日期查询条件
		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat({}, true));
		expect(Number(logisList.count) > 0, `日期查询条件错误`).to.be.true;
		let isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (!common.getDateString([0, 0, 0]).includes(logisList.dataList[i].prodate)) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `日期查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, 1]),
			'prodate2': common.getDateString([0, 0, 2])
		}));
		expect(Number(logisList.count) == 0, `日期查询条件错误`).to.be.true;

		// 客户查询条件
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'dwid': salesResult.params.dwid
		}));
		expect(Number(logisList.count) > 0, `客户查询条件错误`).to.be.true;
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].dwxxid != salesResult.params.dwid) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `客户查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'dwid': '0',
		}));
		expect(Number(logisList.count) == 0, `客户查询条件错误`).to.be.true;

		// 批次从...到...
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'id1': salesResult.result.billno,
			'id2': salesResult.result.billno
		}));
		expect(Number(logisList.count) > 0, `批次查询条件错误`).to.be.true;
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].billno != salesResult.result.billno) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `批次查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'id1': '-1',
			'id2': '-1'
		}));
		expect(Number(logisList.count) == 0, `批次查询条件错误`).to.be.true;

		// 门店
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'shopid': LOGINDATA.invid
		}));
		expect(Number(logisList.count) > 0, `门店查询条件错误`).to.be.true;
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].shopname != LOGINDATA.depname) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `门店查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'shopid': '0'
		}));
		expect(Number(logisList.count) == 0, `门店查询条件错误`).to.be.true;

		// 物流商
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisDwid': salesResult.params.logisDwid
		}));
		expect(Number(logisList.count) > 0, `物流商查询条件错误`).to.be.true;
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].logisdwxxid != salesResult.params.logisDwid) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `物流商查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisDwid': '0'
		}));
		expect(Number(logisList.count) == 0, `物流商查询条件错误`).to.be.true;

		// 运单号
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisBillno': salesResult.params.logisBillno
		}));
		expect(Number(logisList.count) > 0, `运单号查询条件错误`).to.be.true;
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].logisbillno != salesResult.params.logisBillno) {
				isRight = false;
				break;
			}
		}
		expect(isRight, `运单号查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisBillno': '-1'
		}));
		expect(Number(logisList.count) == 0, `运单号查询条件错误`).to.be.true;

		// 货款收讫
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisPayflag': '0'
		}));
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].logispayflag == '是') {
				isRight = false;
				break;
			}
		}
		expect(isRight, `货款收讫查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'logisPayflag': '1'
		}));
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].logispayflag == '否') {
				isRight = false;
				break;
			}
		}
		expect(isRight, `货款收讫查询条件错误`).to.be.true;

		// 是否作废
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'invalidflag': '0'
		}));
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].invalidflag == '1') {
				isRight = false;
				break;
			}
		}
		expect(isRight, `是否作废查询条件错误`).to.be.true;
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'prodate1': common.getDateString([0, 0, -1]),
			'prodate2': common.getDateString([0, 0, 0]),
			'invalidflag': '1'
		}));
		isRight = true;
		for (let i = 0; i < logisList.dataList.length; i++) {
			if (logisList.dataList[i].invalidflag == '0') {
				isRight = false;
				break;
			}
		}
		expect(isRight, `是否作废查询条件错误`).to.be.true;
	});

	it('170285,物流商查询，组合条件查询', async function () {
		// 开代收单
		let salesResult = await common.editBilling(basiceJsonparam.agencySalesJson());

		let searchCondition = {
			'dwid': salesResult.params.dwid,
			'id1': salesResult.result.billno,
			'id2': salesResult.result.billno,
			'logisBillno': salesResult.params.logisBillno,
			'shopid': LOGINDATA.invid,
			'logisDwid': salesResult.params.logisDwid,
			'invalidflag': '0',
			'logisPayflag': '0'
		};
		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat(searchCondition, true));
		expect(logisList, `物流单查询，组合查询条件有误`).to.includes({
			'count': '1'
		});
		expect(logisList.dataList[0], `物流单查询，组合查询条件有误`).to.includes({
			'logispayflagid': '0',
			'shopname': LOGINDATA.depname,
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': salesResult.params.logisDwid,
			'logisbillno': salesResult.params.logisBillno.toString(),
			'invalidflag': '0',
			'dwxxid': salesResult.params.dwid
		});

		searchCondition.invalidflag = '1';
		searchCondition.id1 = '-1';
		searchCondition.id2 = '-1';
		logisList = await common.callInterface('ql-1432', format.qlParamsFormat(searchCondition, true));
		expect(logisList, `物流单查询，组合查询条件有误`).to.includes({
			'count': '0'
		});
	});

	it('170782.修改物流商信息后,销售开单-物流单的物流信息验证.rankA', async function () {
		let sfRes = await common.editBilling(basiceJsonparam.agencySalesJson()); // 开代收单 顺丰快递
		let qfRes = await salesRequest.salesQueryBilling(sfRes.result.pk);
		let json = format.updateHashkey(qfRes.result);
		json.interfaceid = sfRes.params.interfaceid;
		json.action = 'edit';
		json.logisDwid = dwidYTKD;
		json.logisRem += 'edit';
		json.agency = 500;
		json.logisBillno += '123';
		json.show_logisDwid = '圆通快递';
		sfRes = await common.editBilling(json);

		let agencyInfo = await salesRequest.getAgencyInfo(sfRes.result.pk); //获取物流信息
		common.isApproximatelyEqualAssert(json, agencyInfo, ['pk']); //单据pk!=物流单pk
	});

	it('170852.输入过长的运单号/备注', async function () {
		let json = basiceJsonparam.agencySalesJson();
		json.logisBillno = common.getRandomStr(60);
		let sfRes = await common.editBilling(json, false);
		expect(sfRes.result).to.includes({
			error: '[运单号]值超过限制，最大允许长度为50'
		});

		json.logisBillno = common.getRandomStr(5);
		json.logisRem = common.getRandomStr(110);
		sfRes = await common.editBilling(json, false);
		expect(sfRes.result).to.includes({
			error: '[物流备注]值超过限制，最大允许长度为100'
		});
	});

});
