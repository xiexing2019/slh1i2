'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

/*
 * 开单模式7 现金+刷卡+汇款+整单折扣
 */

describe('现金+刷卡+汇款+整单折扣-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 7);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	it('170422.不开启价格验证,修改价格开单验证.rankA', async function () {
		await common.setGlobalParam('sales_pricecheck', 0);
		await common.loginDo({
			'logid': '005', //常青店开单员登陆
			'pass': '000000',
		});
		let json = basiceJsonparam.simpleSalesJson();
		json.details[0].price = 110; //修改价格低于适用价格
		let sfRes = await common.editBilling(json);

		await common.loginDo();
		//正常保存, 不能"提示价格验证错误，无法保存此开单"。因为根本没有开启价格验证
		expect(sfRes.result, `开单失败:${JSON.stringify(sfRes)}`).to.have.property('pk');
	});

	it('170078/170085,整单折扣支持3位小数', async function () {
		//销售订货
		let orderJson = basiceJsonparam.salesOrderJson();
		orderJson.maindiscount = '0.825';
		let orderResult = await common.editBilling(orderJson); //新增销售订单
		let orderInfo = await salesReqHandler.salesOrderQueryBilling(orderResult.result.pk); //获取销售订单详细信息
		common.isApproximatelyEqualAssert(orderResult.params, orderInfo.result);

		//销售开单
		let salesJson = basiceJsonparam.salesJson();
		salesJson.maindiscount = '0.825';
		let salesResult = await common.editBilling(salesJson); //新增销售单
		let salesInfo = await salesReqHandler.salesQueryBilling(salesResult.result.pk); //获取销售单详细信息
		//let balance = salesInfo.result.balance;
		common.isApproximatelyEqualAssert(salesResult.params, salesInfo.result);

		// 验证按订货开单成功
		let salesByOrder = await salesReqHandler.editSalesBillByOrder(orderInfo.result); //按订货开单
		if (USEECINTERFACE == 2) await common.delay(1000);
		salesInfo = await salesReqHandler.salesQueryBilling(salesByOrder.result.pk);
		//salesByOrder.params.finpayPaystat = '2'; //按订货开单设置的就是未付，这里将支付状态改为2是为了验证开单按批次查的支付状态

		common.isApproximatelyEqualAssert(salesByOrder.params, salesInfo.result, ['pk', 'orderversion', 'billno', 'lastbalance', 'show_dwfdid']);
	});
	//前端逻辑
	it.skip('171003.开单界面存在数据错误时检查.rankA-前端逻辑', async function () { });
});
