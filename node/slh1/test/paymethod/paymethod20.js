'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesRequest = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequest = require('../../help/salesOrderHelp/salesOrderRequestHandler');
/*
 *  开单模式20 现金+刷卡+汇款+配货员
 *  接口有问题，先放着 配货员字段不清楚是distopid，invdisopid
 */
describe('开单模式20 现金+刷卡+汇款+配货员--mainLine', function () {
	this.timeout(60000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 20); //开单模式20
		await common.setGlobalParam('sales_order_deliver_mode', 1); //按订货开单
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2); //开单模式2 现金+刷卡+代收+汇款
	});

	it('170631.配货员业绩统计-销售订货', async function () {
		let res = await common.editBilling(basiceJsonparam.salesOrderJson20()); //新增订货单
		let qfRes = await salesOrderRequest.salesOrderQueryBilling(res.result.pk);
		common.isApproximatelyEqualAssert(res.params, qfRes.result); //验证invdisopid

		// 先查一下 按配货员汇总列表
		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));

		// 按订货开单
		let sfRes = await salesRequest.editSalesBillByOrder(qfRes.result); //按订货开单
		let salesFormInfo = await salesRequest.salesQueryBilling(sfRes.result.pk);
		//sfRes.params.finpayPaystat = '2'; //按订货开单设置的就是未付，这里将支付状态改为2是为了验证开单按批次查的支付状态
		common.isApproximatelyEqualAssert(sfRes.params, salesFormInfo.result, ['orderversion', 'actualpay', 'version', 'billno']);

		// 订货单转销售单成功后 验证按配货员汇总列表
		let summaryListAfter = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));
		expect(summaryListAfter.dataList[0], `按配货员汇总错误`).to.includes(sumObjects(summaryList, sfRes.params));

	});

	it('170632.配货员业绩统计-销售开单', async function () {
		// 按配货员汇总
		let summaryList = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));

		//配货员为当前登录人
		let res = await common.editBilling(basiceJsonparam.salesJsonPaymethod20()); //新增销售单
		let qfRes = await salesRequest.salesQueryBilling(res.result.pk);
		common.isApproximatelyEqualAssert(res.params, qfRes.result);
		let qlParam = {
			id1: qfRes.result.billno,
			id2: qfRes.result.billno,
		};
		let qlRes = await common.callInterface('ql-142201', format.qlParamsFormat(qlParam, true));
		common.isApproximatelyEqual(qfRes.result, qlRes.dataList[0]);
		expect(qlRes.dataList[0], `按批次查列表'配货员'字段有误`).to.includes({
			'distristaffname': LOGINDATA.name
		});

		// 按配货员汇总
		let summaryListAfter = await common.callInterface('ql-14426', format.qlParamsFormat({
			'invdisopid': LOGINDATA.id,
			'shopid': LOGINDATA.invid
		}, true));
		expect(summaryListAfter.dataList[0], `按配货员汇总错误`).to.includes(sumObjects(summaryList, res.params));
	});
});


function sumObjects(obj, json) {
	if (obj.dataList.length == 0) {
		return {
			'alipay': (json.alipay || '0'),
			'weixinpay': (json.alipay || '0'),
			'remit': json.remit,
			'cash': json.cash,
			'card': json.card,
			'totalnum': json.totalnum,
			'totalsum': (json.totalnum || json.totalmoney)
		};
	} else {
		return {
			'alipay': ((Number(obj.dataList[0].alipay) || 0) + (Number(json.alipay) || 0)).toString(),
			'weixinpay': ((Number(obj.dataList[0].weixinpay) || 0) + (Number(json.weixinpay) || 0)).toString(),
			'remit': ((Number(obj.dataList[0].remit) || 0) + (Number(json.remit) || 0)).toString(),
			'cash': ((Number(obj.dataList[0].cash) || 0) + (Number(json.cash) || 0)).toString(),
			'card': ((Number(obj.dataList[0].card) || 0) + (Number(json.card) || 0)).toString(),
			'totalnum': ((Number(obj.dataList[0].totalnum) || 0) + (Number(json.totalnum) || 0)).toString(),
			'totalsum': ((Number(obj.dataList[0].totalsum) || 0) + (Number(json.totalsum) || Number(json.totalmoney) || 0)).toString()
		}
	}

}
