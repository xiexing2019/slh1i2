'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderRequestHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');
const basicInfoReqHandler = require('../../help/basicInfoHelp/basicInfoReqHandler');

/*
 * 开单模式5 现金+刷卡+汇款+产品折扣
 */

describe('现金+刷卡+汇款+产品折扣-slh2--mainLine', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 5);
		//await common.setGlobalParam('pricedec', 3); //货品单价精确到厘
		await common.setGlobalParam('sales_need_roundoff', 0); //总计不需要四舍五入
		await common.setGlobalParam('sales_smalltotal_need_roundoff', 0); //销售小计不需要四舍五入
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2);
		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('sales_show_lastprice', 0);
	});

	it('170080/170084,产品折扣支持3位小数', async function () {
		//  新增货品并查看折扣
		let goodJson = basiceJsonparam.addGoodJson();
		goodJson.discount = '0.955';
		let goodRes = await basicInfoReqHandler.editStyle({
			jsonparam: goodJson
		}); //新增货品
		let goodInfo = await common.fetchMatInfo(goodRes.pk);
		common.isApproximatelyEqualAssert(goodJson, goodInfo);

		// 用刚生成的款号进行正常的销售订货
		let orderRes = await common.editBilling(basiceJsonparam.salesOrderJson5(goodInfo));
		let orderInfo = await salesOrderRequestHandler.salesOrderQueryBilling({
			'pk': orderRes.result.pk,
			isPend: 0,
		});
		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);

		// 用刚生成的款号进行正常的销售开单
		let salesRes = await common.editBilling(basiceJsonparam.salesJsonPaymenthod5(goodInfo));
		let salesInfo = await salesReqHandler.salesQueryBilling(salesRes.result.pk);
		common.isApproximatelyEqualAssert(salesRes.params, salesInfo.result);
		//delete orderInfo.result.maindiscount;
		// 验证按订货开单成功
		let salesByOrder = await salesReqHandler.editSalesBillByOrder(orderInfo.result);
		// console.log(`variable : ${JSON.stringify(salesByOrder.params.details)}`);
		// console.log(`\nvariable : ${JSON.stringify(salesInfo.result.details)}`);
		//按订货开单默认未付，部分字段可忽略
		//	console.log(`salesByOrder.params : ${JSON.stringify(salesByOrder.params)}`);
		common.isApproximatelyEqualAssert(salesByOrder.params, salesInfo.result, ['pk', 'lastbalance', 'balance', 'cash', 'card', 'remit', 'remark', 'srcType', 'orderversion', 'billid', 'billno', 'show_dwfdid']);

	});

	it('170460,产品折扣模式下自动取上次折扣值.rankA', async function () {
		await common.setGlobalParam('sales_show_lastprice', 1);
		await common.setGlobalParam('sales_use_lastsaleprice', 1);
		//  新增货品并查看折扣
		// let goodJson = basiceJsonparam.addGoodJson();
		// goodJson.discount = '0.955';
		// let goodResult = await basicInfoReqHandler.editStyle({
		// 	jsonparam: goodJson,
		// });
		// let goodInfo = await common.fetchMatInfo(goodResult.pk);
		// await common.callInterface('cs-syncdata', {
		// 	types: 'brand_discount',
		// 	lastSyncTime: curTime, //不填同步所有数据，填写同步时间之后的数据 获取当前时间用于冒泡
		// 	offset: 0, //偏移量，0从第一条开始同步，10：从第10条开始同步
		// });
		// await common.delay(1000);
		// 用刚生产的客户进行正常的销售开单
		//goodInfo.discount = '0.8';
		//let salesJson = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod5(goodInfo));
		// let salesResult = await common.callInterface('sf-14211-1', {
		// 	'jsonparam': salesJson
		// });
		// expect(`产品折扣，销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');
		let json = basiceJsonparam.salesJsonPaymenthod5();
		for (let i = 0; i < json.details.length; i++) {
			json.details[i].discount = 0.8;
		}

		let salesResult = await common.editBilling(json);
		// console.log(`salesResult : ${JSON.stringify(salesResult)}`);
		// await common.callInterface('cs-syncdata', {
		// 	types: 'brand_discount',
		// 	lastSyncTime: curTime, //不填同步所有数据，填写同步时间之后的数据 获取当前时间用于冒泡
		// 	offset: 0, //偏移量，0从第一条开始同步，10：从第10条开始同步
		// });
		// await common.callInterface('cs-syncdata', {
		// 	types: 'dres_style',
		// 	lastSyncTime: curTime, //不填同步所有数据，填写同步时间之后的数据 获取当前时间用于冒泡
		// 	offset: 0, //偏移量，0从第一条开始同步，10：从第10条开始同步
		// });
		// await common.delay(1000);
		let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': salesResult.params.shopid,
			'dwid': salesResult.params.dwid,
			'styleid': salesResult.params.details[0].styleid,
			'bizType': 21, //默认21，返回销售单上次价，150表示销售订单上次价
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'discount': '0.8'
		});

		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('sales_show_lastprice', 0);
	});
});
