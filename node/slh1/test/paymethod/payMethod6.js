'use strict';
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const salesOrderReqHandler = require('../../help/salesOrderHelp/salesOrderRequestHandler');

/*
 * 开单模式6 现金+刷卡+汇款+客户折扣
 */

describe('现金+刷卡+汇款+客户折扣--mainLine', function () {

	this.timeout(30000);
	this.retries(2);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('paymethod', 6);
	});

	after(async () => {
		await common.setGlobalParam('paymethod', 2);
	});

	it('170077/170083,客户折扣支持3位小数.rankA-slh2', async function () {
		// 新增客户
		let custJson = basiceJsonparam.threeDecimalCustDiscountJson();
		let custRes = await salesReqHandler.addCust(custJson);

		// 检查客户折扣正常保存
		let custInfo = await common.callInterface('qf-1401', {
			'pk': custRes.result.val
		});
		common.isApproximatelyEqualAssert({
			'nameshort': custJson.nameshort,
			'discount': custJson.discount,
			'creditbalance': custJson.creditbalance
		}, custInfo);

		// 用刚生成的客户进行正常的销售订货
		let orderJson = basiceJsonparam.salesOrderJson6(custInfo);
		let orderRes = await common.editBilling(orderJson);

		let orderInfo = await salesOrderReqHandler.salesOrderQueryBilling(orderRes.result.pk); //获取销售订单信息
		common.isApproximatelyEqualAssert(orderRes.params, orderInfo.result);

		// 用刚生产的客户进行正常的销售开单
		let salesJson = basiceJsonparam.salesJsonPaymenthod6(custInfo);
		let salesRes = await common.editBilling(salesJson);
		let salesInfo = await salesReqHandler.salesQueryBilling(salesRes.result.pk);
		common.isApproximatelyEqualAssert(salesRes.params, salesInfo.result);

		// 验证按订货开单成功
		let salesByOrder = await salesReqHandler.editSalesBillByOrder(orderInfo.result);
		//按订货开单默认未付，部分字段可忽略
		common.isApproximatelyEqualAssert(salesByOrder.params, salesInfo.result, ['pk', 'lastbalance', 'balance', 'cash', 'card', 'remit', 'remark', 'srcType', 'orderversion', 'billid', 'billno', 'show_dwfdid']);
	});

	it('170542,开单模式6客户折扣模式增加物流商代收功能.rankA', async function () {

		// 新增客户
		let custJson = basiceJsonparam.threeDecimalCustDiscountJson();
		let custResult = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		let custInfo = await common.callInterface('qf-1401', {
			'pk': custResult.val
		});
		common.isApproximatelyEqualAssert({
			'nameshort': custJson.nameshort,
			'discount': custJson.discount,
			'creditbalance': custJson.creditbalance
		}, custInfo);

		// 用刚生产的客户进行正常的销售开单
		let salesJson = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod6(custInfo));
		salesJson.agency = Number(salesJson.remit || 0) + Number(salesJson.card || 0) + Number(salesJson.cash || 0);
		salesJson.logisDwid = BASICDATA.dwidSFKD;
		salesJson.card = 0;
		salesJson.cash = 0;
		salesJson.remit = 0;
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(`客户折扣，销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

		let logisList = await common.callInterface('ql-1432', format.qlParamsFormat({
			'dwid': salesJson.dwid,
			'id1': salesResult.billno,
			'id2': salesResult.billno,
			'logisDwid': salesJson.logisDwid,
			'shopid': salesJson.shopid
		}, true));
		common.isApproximatelyEqualAssert({
			'agencyflag': '是',
			'logispayflagid': '0',
			'prodate': common.getCurrentDate('YY-MM-DD'),
			'logisdwxxid': salesJson.logisDwid,
			'dwxxid': salesJson.dwid,
			'shopname': LOGINDATA.depname,
			'finpayagency': salesJson.agency.toString(),
			'billno': salesResult.billno
		}, logisList.dataList[0]);
	});

	it('170461,客户折扣模式下自动取上次折扣值.rankA-slh2', async function () {
		await common.setGlobalParam('sales_show_lastprice', 1);
		await common.setGlobalParam('sales_use_lastsaleprice', 1);

		// 新增客户
		let custJson = basiceJsonparam.threeDecimalCustDiscountJson();
		let custResult = await common.callInterface('sf-1401', {
			'jsonparam': custJson
		});
		expect(custResult, `新增客户失败:${JSON.stringify(custResult)}`).to.not.have.property('error');
		let custInfo = await common.callInterface('qf-1401', {
			'pk': custResult.val
		});

		// 用刚生产的客户进行正常的销售开单
		custInfo.discount = '0.9';
		let salesJson = format.jsonparamFormat(basiceJsonparam.salesJsonPaymenthod6(custInfo));
		let salesResult = await common.callInterface('sf-14211-1', {
			'jsonparam': salesJson
		});
		expect(`客户折扣，销售开单失败:${JSON.stringify(salesResult)}`).to.not.have.property('error');

		let lastPriceInfo = await common.callInterface('cs-getsalebill-lastprice', {
			'shopid': salesJson.shopid,
			'dwid': salesJson.dwid,
			'styleid': salesJson.details[0].styleid
		});
		expect(lastPriceInfo, `获取上次价接口有误`).to.includes({
			'discount': '0.9'
		});

		await common.setGlobalParam('sales_use_lastsaleprice', 0);
		await common.setGlobalParam('sales_show_lastprice', 0);
	});
});
