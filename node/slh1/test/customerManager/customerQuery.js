'use strict'
const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const basiceJson = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const listSearchHelp = require('../../help/listSearchHelp/listSearchHelp.js');
const reqHandlerHelp = require('../../help/reqHandlerHelp');

describe('往来管理', function () {
	this.timeout(30000);
	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
		await common.setGlobalParam('usescore', 1); //启用积分
	});

	describe('110001_客户查询.rankA-slh2', function () {

		it('多个条件查询', async function () {
			let xwInfo = {
				sellerid: BASICDATA.staffid000,
				dwname: '小王',
				dwid: BASICDATA.dwidXw
			};
			let qlParams = format.qlParamsFormat(xwInfo);
			let qlResult = await common.callInterface('ql-1339', qlParams);

			expect(qlResult, '查询出错').to.have.property('dataList');

			qlResult.dataList.map(data => {
				expect(data, '查询出错').to.includes({
					seller: '总经理',
					dwname: xwInfo.dwname
				});
			});
		});

		it('单个条件查询', async function () {
			let searchParams = {
				'ql-1339': [
					['sellerid', BASICDATA.staffid000, 'seller', '总经理'],
					['dwname', '小王'],
					['dwid', BASICDATA.dwidXw, 'dwname', '小王']
				]
			};
			let result = await listSearchHelp.searchVerity(searchParams);
			expect(result.verifyResult, `单个条件查询结果：${result.errMsg}`).to.be.equal(true);
		});
	});

	describe('110002_客户查询->销售明细--上下级模式.rankA', function () {

		let qfPk;
		before(async () => {
			let qlResult = await common.callInterface('ql-1339', format.qlParamsFormat({
				pagesize: 15
			}));
			expect(qlResult, `查询出错: ${JSON.stringify(qlResult)}`).to.have.property('dataList');

			qfPk = qlResult.dataList[0].id;
		});

		it('1', async function () {
			let qfResult = await common.callInterface('qf-1401', {
				pk: qfPk
			});
			expect(qfResult, `查询出错: ${JSON.stringify(qfResult)}`).to.includes({
				pk: qfPk
			});
		});

		it('2', async function () {
			let salesDetails = await common.callInterface('ql-15006', format.qlParamsFormat({
				pagesize: 15,
				dwid: BASICDATA.dwidXw
			}));
			expect(salesDetails, `查询出错: ${JSON.stringify(salesDetails)}`).to.have.property('dataList');
		})
	});

	describe('110005_客户查询->客户停用.rankA-slh2', function () {
		after(async () => {
			await common.callInterface('cs-2241-3', {
				pk: BASICDATA.dwidXw
			}); //启用客户 防止影响其他用例
		});

		it('110005_1', async function () {
			let result = await common.callInterface('cs-2241-2', {
				pk: BASICDATA.dwidXw
			}); //停用客户
			expect(result, '停用出错').to.have.property('error');
			expect(result.error, '停用出错').to.be.include('还存在余额或欠款');
		})

		it('110005_2', async function () {
			//新增
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam);

			//停用
			let result = await common.callInterface('cs-2241-2', {
				pk: custRes.result.val
			});
			expect(result, '停用出错').to.have.property('val');
			expect(result.val, '停用出错').to.be.equal('ok');
		})
	});


	describe('110008_客户查询->允许退货--是.rankA-slh2', function () {
		before(async () => {
			let qfResult = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});

			qfResult.noretflag = '0';
			qfResult.action = 'edit';
			let sfResult = await common.callInterface('sf-1401', {
				jsonparam: qfResult
			});
		});

		it('销售开单退款', async function () {
			let jsonParams = basiceJsonparam.salesJson();
			jsonParams.details.map(detail => {
				let num = detail.num;
				detail.num = 0 - num;
			});

			let salesResult = await common.editBilling(jsonParams);
			expect(salesResult.result, '退货失败').to.have.property('pk');
		});
	});

	describe('110009_客户查询->允许退货--否.rankA-slh2', function () {
		before(async () => {
			let qfResult = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});

			qfResult.noretflag = '1';
			qfResult.action = 'edit';
			let sfResult = await common.callInterface('sf-1401', {
				jsonparam: qfResult
			});
		});

		after(async () => {
			let qfResult = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});
			qfResult.noretflag = '0';
			qfResult.action = 'edit';
			let sfResult = await common.callInterface('sf-1401', {
				jsonparam: qfResult
			});
		});

		it('销售开单退款', async function () {
			let jsonParams = basiceJsonparam.salesJson();
			jsonParams.details.map(detail => {
				let num = detail.num;
				detail.num = 0 - num;
			});

			let salesResult = await common.editBilling(jsonParams, false);
			expect(salesResult.result, '退货失败').to.have.property('error');
			expect(salesResult.result.error, '退货失败').to.be.equal('该客户不允许退货，请检查');
		});
	});

	describe('110012_厂商查询-slh2', function () {
		it('查询条件，是否停用', async function () {
			//新增
			let sfParam = basiceJsonparam.addManufacturerJson();
			let dwname = sfParam.name;

			let sfRes = await common.callInterface('sf-2241', {
				jsonparam: sfParam
			});
			expect(sfRes, '新增厂商出错').to.have.property('val');
			expect(sfRes.val.length, '新增厂商出错').to.be.above(0);

			let dwPk = sfRes.val;

			//停用
			let stopRes = await common.callInterface('cs-2241-2', {
				pk: dwPk
			});
			expect(stopRes, '停用出错').to.have.property('val');
			expect(stopRes.val, '停用出错').to.be.equal('ok');

			//查询
			let qlRes = await common.callInterface('qd-2241', format.qlParamsFormat({
				delflag: '1',
				id: dwPk,
			}));
			expect(qlRes, '查询出错').to.have.property('dataList');
			expect(qlRes.dataList.length, '查询出错').to.be.above(0);
			expect(qlRes.dataList[0].name, '查询出错').to.be.equal(dwname);
		});
	});

	describe('110013_新增客户-slh2.', function () {
		after(async () => {
			await common.loginDo();
		});
		it('不存在相同的客户名称或手机号+新增客户.rankA', async function () {
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam);

			//查询
			let qlParam = {
				dwid: custRes.result.val
			}
			let qlRes = await common.callInterface('ql-1339', format.qlParamsFormat(qlParam));
			expect(qlRes.dataList[0].dwname, '查询出错').to.be.equal(sfParam.nameshort);

			//详情
			let qfRes = await common.callInterface('qf-1401', {
				pk: qlParam.dwid
			});
			expect(qfRes, '详情数据出错').to.includes({
				nameshort: sfParam.nameshort,
				invid: sfParam.invid
			});
		});
		it('2.不同门店新增同名客户', async function () {
			await common.loginDo({ logid: 200 });
			let sfRes = await salesReqHandler.addCust({ nameshort: '小王' }, false);
			expect(sfRes.result.error, `不同门店保存同名客户，保存成功${JSON.stringify(sfRes)}`).to.be.includes('已存在[小王]名称的[客户]');
		});
	});

	describe('110014_新增客户_存在相同的客户/厂商 名称或手机号+新增客户.rankA-slh2', function () {
		let xwInfo, vellInfo, key;
		before(async () => {
			key = USEECINTERFACE == 1 ? 'phone' : 'mobile';
			xwInfo = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});

			if (xwInfo[key] === undefined || xwInfo[key].length === 0) {
				xwInfo[key] = '13812345678';

				xwInfo.action = 'edit';
				await salesReqHandler.addCust(xwInfo); //修改客户信息
			};

			vellInfo = await common.callInterface('qf-2241', {
				pk: BASICDATA.dwidVell
			});
			if (vellInfo[key] === undefined || vellInfo[key].length === 0) {
				vellInfo[key] = '13812345679';

				vellInfo.action = 'edit';
				let sfRes = await common.callInterface('sf-2241', {
					jsonparam: vellInfo
				});
				expect(sfRes, '修改厂商信息失败').to.have.property('val');
			}
		});

		it('1.客户同名', async function () {
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.nameshort = xwInfo.nameshort;
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam, false);

			expect(custRes.result.error, '新增客户失败').to.be.include(`门店[常青店]已存在[${xwInfo.nameshort}]名称的[客户]`);
		});
		it('2.客户同手机号', async function () {
			let sfParam = basiceJsonparam.addCustJson();
			sfParam[key] = xwInfo[key];
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam, false);
			//	console.log(`variable : ${JSON.stringify(custRes)}`);
			expect(custRes.result.error, '新增客户失败').to.be.include(`门店[常青店]已存在[${xwInfo[key]}]号码的[客户]`);
		});

		it('3.厂商同名', async function () {
			let sfParam = basiceJsonparam.addManufacturerJson();
			sfParam.nameshort = vellInfo.nameshort;
			let sfRes = await common.callInterface('sf-2241', {
				jsonparam: sfParam
			});
			expect(sfRes.error, '新增厂商出错').to.be.include(`门店[常青店]已存在[${vellInfo.nameshort}]名称的[厂商]`);
		});

		it('4.厂商同手机号', async function () {
			let sfParam = basiceJsonparam.addManufacturerJson();
			sfParam.phone = vellInfo.phone;

			let sfRes = await common.callInterface('sf-2241', {
				jsonparam: sfParam
			});
			expect(sfRes.error, '新增厂商出错').to.be.include(`门店[常青店]已存在[${vellInfo.phone}]号码的[厂商]`)
		});
	});

	describe('110056_新增客户--客户代码.rankA-slh2', function () {
		let xwInfo;
		before(async () => {
			xwInfo = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});
			if (xwInfo.vipcode === undefined || xwInfo.vipcode.length === 0) {
				xwInfo.vipcode = 'xw001';

				xwInfo.action = 'edit';
				let sfRes = await common.callInterface('sf-1401', {
					jsonparam: sfRes
				});
				expect(sfRes, '修改失败').to.have.property('val');
			}
		});

		it('110056_期望2', async function () {
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			sfParam.vipcode = common.getRandomStr(6) + '是';
			let custRes = await salesReqHandler.addCust(sfParam);

			expect(custRes.result, '新增客户失败').to.be.property('val');
			let cusPk = custRes.result.val;

			let qfRes = await common.callInterface('qf-1401', {
				pk: cusPk
			});
			expect(qfRes.vipcode, '客户代码出错').to.be.equal(sfParam.vipcode);

			qfRes.action = 'edit';
			let sfRes = await common.callInterface('sf-1401', {
				jsonparam: qfRes
			});
			expect(sfRes, '保存出错').to.have.property('val');
		})

		it('110056_期望4', async function () {
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			sfParam.vipcode = xwInfo.vipcode;
			let custRes = await salesReqHandler.addCust(sfParam, false);

			expect(custRes.result.error, '新增客户失败').to.be.include(`已存在客户代码${xwInfo.vipcode}的客户：${xwInfo.nameshort}`);
		})
	});

	describe('110159_往来管理-客户账款-客户门店账.rankA-slh2', function () {
		it('条件查询', async function () {
			let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				sellerid: BASICDATA.staffid000,
				invid: BASICDATA.shopidCqd,
				deflag: '1'
			}));

			expect(qlRes, '查询出错').to.have.property('dataList');

			qlRes.dataList.map(data => {
				expect(data.invid, '门店信息出错').to.be.equal(BASICDATA.shopidCqd);
				expect(parseFloat(data.balance), '余额数据出错').to.be.below(0);
			});
		});
	});

	describe('110017_客户门店账->核对汇总金额和客户信息条数-slh2', function () {
		it('核对汇总金额和客户信息', async function () {
			let qlRes = await reqHandlerHelp.qlIFCHandler({
				interfaceid: 'ql-15001',
				sellerid: BASICDATA.staffid000,
				invid: BASICDATA.shopidCqd
			});

			expect(qlRes.result, '查询出错').not.to.have.property('error');

			let sum = '0';
			qlRes.result.dataList.forEach(data => {
				sum = common.add(sum, data.balance);
			});
			assert.equal(qlRes.result.dataList.length, qlRes.result.count, '信息条数出错');
			expect(Number(qlRes.result.sumrow.balance), '汇总金额出错').to.equal(Number(sum));
			//assert.equal(sum, qlRes.sumrow.balance, '汇总金额出错');
		});
	});

	//先不管
	// describe('110052_客户分店模式下不允许设置上下级', function () {

	// 	it('客户分店模式下不允许设置上级客户', async function () {

	// 		let qlResult = await common.callInterface('ql-1339', format.qlParamsFormat({
	// 			pagesize: 15
	// 		}));
	// 		let cusPk = qlResult.dataList[0].id;
	// 		let qfRes = await common.callInterface('qf-1401', {
	// 			pk: cusPk
	// 		});
	// 		qfRes.action = 'edit';

	// 		if(qfRes.superid === undefined || qfRes.superid.length === 0) {
	// 			qfRes.superid = qlResult.dataList[1].id;
	// 		}

	// 		let sfRes = await common.callInterface('sf-1401', {
	// 			jsonparam: qfRes
	// 		});
	// 		expect(sfRes.error, '保存失败').to.be.equal('客户分店模式下不允许设置上级客户');
	// 	});
	// });

	// describe('110017', async function () {
	// 	it('核对汇总金额和客户信息条数', async function () {
	// 		//列表数据
	// 		let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat());

	// 		let sumRes = await common.callInterface('ql-15001', format.qlParamsFormat({
	// 			search_count: 1,
	// 			search_list: 0,
	// 			search_sum: 1
	// 		}));

	// 		let allSum = '0';
	// 		qlRes.dataList.map(data => {
	// 			allSum = (Number(allSum) + Number(data.balance)).toString();
	// 		});

	// 		expect(qlRes.dataList.length.toString(), '信息条数出错').to.be.equal(sumRes.count);
	// 		expect(allSum, '汇总金额出错').to.be.equal(sumRes.sumrow.balance);
	// 	});
	// });

	describe('110021-slh2', function () {
		it('客户总账底部数据汇总', async function () {
			let csRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				search_count: 1,
				search_list: 0,
				search_sum: 1
			}));

			let ctRes = await common.callInterface('ql-1350', format.qlParamsFormat({
				search_count: 1,
				search_list: 0,
				search_sum: 1
			}));

			expect(csRes.sumrow.balance, '汇总数据有误').to.be.equal(ctRes.sumrow.dwbalance);
		});
	});

	describe('110018_客户账款->按上级单位.rankA', function () {
		it('1,2(条件查询)', async function () {
			let qlRes = await common.callInterface('ql-15005', format.qlParamsFormat({
				shopid: BASICDATA.shopidCqd
			})); //按上级单位接口
			expect(qlRes, '查询失败').to.have.property('dataList');

			for (let data of qlRes.dataList) {
				let result = await common.callInterface('ql-1339', format.qlParamsFormat({
					superid: data.id
				})); //客户查询接口，用上级单位查询条件查下级客户
				expect(result.dataList.length, `superid=${data.id},按上级单位查询存在没有下级的客户`).to.be.above(0); //验证按上级单位查出来的客户，是否有下级客户（只有上下级关系的上级客户有账款才出现在这里）
			}
		});
	});

	describe('110020_客户总账.rankA-slh2', function () {
		it('条件查询', async function () {
			let qlRes = await common.callInterface('ql-1350', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw
			}));
			expect(qlRes, '查询出错').to.have.property('dataList');
		});
	});

	describe('110026_欠款报警功能检查-slh2', function () {
		it('欠款报警', async function () {
			//新建带欠款报警值的客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			sfParam.alarmbalance = '1';
			let custRes = await salesReqHandler.addCust(sfParam);

			//开单(欠款)
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.card = 0;
			jsonparam.cash = 0;
			jsonparam.remit = 0;
			jsonparam.dwid = custRes.result.val;
			jsonparam = format.jsonparamFormat(jsonparam);
			let sfRes = await common.callInterface(jsonparam.interfaceid, {
				jsonparam
			});

			//客户门店帐列表查询
			let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				dwxxid: custRes.result.val
			}));
			expect(qlRes.dataList[0].warned, '欠款报警功能出错').to.be.equal('1');
		});
	});

	describe('110028_是否欠款报警查询-slh2', function () {
		it('查询条件是否欠款报警选择是', async function () {
			let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				alarm: '1'
			}));

			for (let data of qlRes.dataList) {
				expect(data.warned, '查询出错').to.be.equal('1');
			}
		});

		it('查询条件是否欠款报警选择否', async function () {
			let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				alarm: '0'
			}));

			for (let data of qlRes.dataList) {
				expect(data.warned, '查询出错').to.be.equal('0');
			}
		})
	});

	describe('110033_客户活跃度查询-slh2', function () {
		it('110033_3', async function () {
			//新建客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam);

			//查询
			let qlResult = await common.callInterface('ql-15004', format.qlParamsFormat({
				dwid: custRes.result.val
			}));
			expect(qlResult.dataList.length, '停用后，客户活跃度查询出错').to.be.equal(0);
		});

		// it('110033_4', async function() {
		//   //核对天数、最后一次拿货是否正确
		//   let qlRes = await common.callInterface('ql-15004', format.qlParamsFormat({
		//     pagesize: '15',
		//     sortField: 'datestilllasttake',
		//     sortfieldOrder: '1'
		//   }));

		//   for (let data of qlRes.dataList) {
		//     let check = data.lasttakedate === undefined || data.lasttakedate.length === 0;
		//     expect(check, '最后一次拿货日期空值校验').to.be.equal(false);

		//     let dateArr = data.lasttakedate.split(' ');
		//     let oDate = new Date(dateArr[0].replace(/-/g, "/"));
		//     let nowDate = new Date();
		//     let days = parseInt((nowDate.getTime() - oDate.getTime()) / (1000 * 60 * 60 * 24))
		//     expect(data.datestilllasttake, '天数、最后一次拿货出错').to.be.equal(days.toString());
		//   }

		//   //开单后核查
		//   let firstData = qlRes.dataList[0];
		//   let cusInfo = await common.callInterface('ql-1339', format.qlParamsFormat({
		//     dwname: firstData.dwname
		//   }));

		//   let jsonparam = basiceJsonparam.salesJson();
		//   jsonparam.dwid = cusInfo.dataList[0].id;
		//   jsonparam = format.jsonparamFormat(jsonparam);
		//   let sfRes = await common.callInterface(jsonparam.interfaceid, {jsonparam});

		//   qlRes = await common.callInterface('ql-15004', format.qlParamsFormat({
		//     dwid: cusInfo.dataList[0].id
		//   }));
		//   expect(qlRes.dataList[0].datestilllasttake, '开单后，验证未拿货天数错误').to.be.equal('0');
		// });
	});

	describe('110034_停用客户不应出现在客户活跃度中-slh2', function () {
		it('故意查询已停用的客户', async function () {
			//新建客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam);

			//开单
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.dwid = custRes.result.val;
			let sfRes = await common.editBilling(jsonparam);

			//查询
			let qlResult = await common.callInterface('ql-15004', format.qlParamsFormat({
				dwid: custRes.result.val
			}));
			expect(qlResult.dataList[0].dwname, '停用前，客户活跃度查询出错').to.be.equal(sfParam.nameshort);

			//停用
			let result = await common.callInterface('cs-2241-2', {
				pk: custRes.result.val
			});

			//查询
			qlResult = await common.callInterface('ql-15004', format.qlParamsFormat({
				dwid: custRes.result.val
			}));
			expect(qlResult.dataList.length, '停用后，客户活跃度查询出错').to.be.equal(0);
		});
	});

	describe('110035_客户未拿货天数-slh2', function () {
		it('对比客户门店帐界面的未拿货天数和客户活跃度界面的未拿货天数', async function () {
			//开单
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.card = 0;
			jsonparam.cash = 0;
			jsonparam.remit = 0;
			jsonparam.dwid = BASICDATA.dwidXw;
			jsonparam = format.jsonparamFormat(jsonparam);
			let sfRes = await common.callInterface(jsonparam.interfaceid, {
				jsonparam
			});

			//客户活跃度查询
			let qlResult = await common.callInterface('ql-15004', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw
			}));

			//客户门店账查询
			let result = await common.callInterface('ql-15001', format.qlParamsFormat({
				dwxxid: BASICDATA.dwidXw
			}));

			//获取该用户当前门店的数据
			let expectRes;
			for (let data of result.dataList) {
				if (data.invid === BASICDATA.shopidCqd) {
					expectRes = data
				}
			}

			expect(qlResult.dataList[0].num, '客户未拿货天数出错').to.be.equal(expectRes.datestilllasttake);
		});
	});

	describe('110058_异地仓库模式下查看客户活跃度的未拿货天数', function () {
		it('查看客户活跃度的未拿货天数', async function () {
			//开启异地仓库模式
			await common.setGlobalParam('scm_shop_inv_separate', '1');
			//查询客户门店账
			let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
				sortField: 'num',
				sortfieldOrder: '1',
				pagesize: '15',
				invid: BASICDATA.shopidCqd,
			}));
			//获取拿货天数大于0的客户
			let dwid;
			for (let data of qlRes.dataList) {
				if (parseInt(data.num) > 0) {
					dwid = data.dwid;
					break;
				};
			};
			expect(dwid, '缺少未拿货天数大于0的客户').to.not.be.undefined;

			//客户信用额度处理  如果客户的信用额度不是为0，那么就把他改为0，保证可以正常开单
			let qfRes = await common.callInterface('qf-1401', { pk: dwid });
			if (qfRes.creditbalance != 0) {
				qfRes.creditbalance = 0;
				qfRes.action = 'edit';
				await common.callInterface('sf-1401', {
					jsonparam: qfRes
				});
			};

			//开单
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.dwid = dwid;
			//销售开单
			// console.log(`销售开单时间=${JSON.stringify(common.getCurrentTime())}`);
			let sales = await common.editBilling(jsonparam);

			//客户活跃度查询
			// console.log(`客户活跃度查询时间=${JSON.stringify(common.getCurrentTime())}`);
			let qlResult = await reqHandlerHelp.qlIFCHandler({ interfaceid: 'ql-15004', dwid });

			common.isApproximatelyEqualAssert({ id: dwid, datestilllasttake: 0 }, qlResult.result.dataList[0], [], `销售开单时间:${JSON.stringify(sales)},客户活跃度查询错误:${JSON.stringify(qlResult.result.dataList[0])}`);

			//客户门店账查询
			let custAcct = await common.callInterface('ql-15001', format.qlParamsFormat({
				dwxxid: dwid
			}));

			//获取该用户当前门店的数据
			let res;
			for (let data of custAcct.dataList) {
				if (data.invid === BASICDATA.shopidCqd) {
					res = data;
				};
			};
			common.isApproximatelyEqualAssert({ dwid, num: 0, }, res, [], `拿货天数大于0的客户销售开单后拿货天数不等于0${JSON.stringify(res)}`);
		});
	});

	describe('110083_客户账款-slh2', function () {
		it('所有统计检查代收金额', async function () {
			//新建客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			let custRes = await salesReqHandler.addCust(sfParam);

			//开该客户的代收单
			let rem = common.getRandomStr(6);
			let salesParam = {
				"interfaceid": "sf-14211-1",
				"dwid": custRes.result.val,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${rem}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResult = await common.editBilling(salesParam);
			expect(salesResult.result, '代收开单出错').to.have.property('billno');

			let qlRes = await common.callInterface('ql-13502', format.qlParamsFormat({
				dwid: custRes.result.val
			}));
			expect(qlRes, '客户总账-所有统计查询出错').to.have.property('dataList');

			let salesData;
			for (let data of qlRes.dataList) {
				if (data.billno === salesResult.result.billno) {
					salesData = data;
					break
				}
			}
			assert.equal(salesParam.agency, Number(salesData.daishou), '客户总账-所有统计查询-代收数据金额有误');

			qlRes = await common.callInterface('ql-150012', format.qlParamsFormat({
				dwid: custRes.result.val,
				invid: BASICDATA.shopidCqd
			}));
			expect(qlRes, '客户门店帐-所有统计查询出错').to.have.property('dataList');

			for (let data of qlRes.dataList) {
				if (data.billno === salesResult.result.billno) {
					salesData = data;
					break
				};
			};
			assert.equal(salesParam.agency, salesData.daishou, '客户门店帐-所有统计查询-代收数据金额有误');
		});
	});

	describe('110087_新增客户增加性别-slh2', function () {
		it('新增/修改客户增加性别并检验', async function () {
			let genderArr = ['0', '1', '2'];

			for (let index = 0; index < genderArr.length; index++) {
				//新建客户带性别
				let sfParam = basiceJsonparam.addCustJson();
				sfParam.sex = genderArr[index];
				sfParam.invid = BASICDATA.shopidCqd;
				let custRes = await salesReqHandler.addCust(sfParam);
				expect(custRes.result, '新增客户出错').to.have.property('val');

				//修改性别保存
				sfParam.sex = genderArr[(index + 1) % genderArr.length];
				sfParam.action = "edit";
				sfParam.pk = custRes.result.val;
				custRes = await salesReqHandler.addCust(sfParam);
				expect(custRes.result, '新增客户出错').to.have.property('val');

				//查询验证
				let qfRes = await common.callInterface('qf-1401', {
					pk: custRes.result.val
				});
				expect(qfRes.sex, '修改性别验证出错').to.be.equal(sfParam.sex);
			};
		});
	});

	describe('110088_清除客户适用价格-slh2', function () {
		it('清除客户适用价格并验证', async function () {
			//新建客户带适用价格
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			sfParam.pricetype = '1';
			let custRes = await salesReqHandler.addCust(sfParam);
			expect(custRes.result, '新增客户出错').to.have.property('val');

			//清除适用价格
			sfParam.action = "edit";
			sfParam.pk = custRes.result.val;
			sfParam.pricetype = '9';
			custRes = await salesReqHandler.addCust(sfParam);
			expect(custRes.result, '新增客户出错').to.have.property('val');

			//查询验证
			let qfRes = await common.callInterface('qf-1401', {
				pk: custRes.result.val
			});
			expect(qfRes.pricetype, '修改性别验证出错').to.be.equal(sfParam.pricetype);
		});
	});

	describe('110121_客户查询(地址)-slh2', function () {
		it('模糊查询', async function () {
			//新建客户带地址
			let enStr = common.getRandomStr(5);
			let numStr = common.getRandomNumStr(5);
			let chStr = common.getRandomChineseStr(5);

			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCqd;
			sfParam.addr = 'cust' + enStr + numStr + chStr;
			let custRes = await salesReqHandler.addCust(sfParam);
			expect(custRes.result, '新增客户出错').to.have.property('val');
			// console.log(custRes.result);

			//列表查询
			let addrArr = [enStr, numStr, chStr];
			for (let addr of addrArr) {
				let qlRes = await common.callInterface('ql-1339', format.qlParamsFormat({
					addr
				}));
				// console.log(qlRes);
				expect(qlRes, '客户查询出错').to.have.property('dataList');

				let found = false;
				for (let data of qlRes.dataList) {
					if (data.id === custRes.result.val) {
						found = true;
						break;
					}
				}
				expect(found, '地址模糊查询失败').to.be.equal(true);
			}
		})
	});

	describe('110127_客户查询.rankA-slh2', function () {
		//客户分店功能基本没用了
		it.skip('修改客户总店门店不能影响客户分店门店', async function () {
			//新建客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.invid = BASICDATA.shopidCkd;
			let custRes = await salesReqHandler.addCust(sfParam);
			expect(custRes.result, '新增客户出错').to.have.property('val');

			//添加客户分店
			let addRes = await common.callInterface('sf-14011', {
				jsonparam: {
					action: 'add',
					nameshort: common.getRandomChineseStr(2),
					superid: custRes.result.val,
					invid: BASICDATA.shopidCqd
				}
			});
			expect(addRes, '添加客户分店出错').to.have.property('val');

			//修改门店
			sfParam.action = "edit";
			sfParam.pk = custRes.result.val;
			sfParam.invid = BASICDATA.shopidCqd;
			custRes = await salesReqHandler.addCust(sfParam);
			expect(custRes.result, '修改客户门店出错').to.have.property('val');

			//查询客户分店
			let qlRes = await common.callInterface('ql-14011', format.qlParamsFormat({
				superid: custRes.result.val,
			}));
			expect(qlRes, '查询客户分店出错').to.have.property('dataList');
			expect(qlRes.dataList.length, '查询客户分店出错').to.be.above(0);

			for (let shopData of qlRes.dataList) {
				expect(shopData.invName, '验证失败').to.be.equal('常青店');
			}
		})
	});


	describe('110128_客户回访-slh2', function () {
		it('修改回访后检查回访类型', async function () {
			let param = {
				action: 'add',
				custreturndate: common.getCurrentDate(),
				custreturntype: '1',
				dwid: BASICDATA.dwidXw,
				feedback: '反馈' + common.getRandomStr(6),
				returnperson: BASICDATA.staffid000,
				subject: '主题' + common.getRandomStr(3),
			}
			let sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '新增客户回访出错').to.have.property('val');
			param.pk = sfRes.val;
			param.subject = '修改主题' + common.getRandomStr(3);
			param.action = 'edit';
			sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '修改并保存客户回访出错').to.have.property('val');

			let qfRes = await common.callInterface('qf-15002', {
				jsonparam: {
					pk: param.pk
				}
			});
			expect(qfRes.custreturntype, '修改回访后检查回访类型出错').to.be.equal(param.custreturntype);
		});
	});

	// describe('100101_新增积分调整', function () {
	//   it('积分跨门店共享关闭，新增积分调整', async function () {
	//     let result = await common.setGlobalParam('sales_client_score_share', '1');
	//     console.log(result);
	//   });
	// });

	describe('100105_客户活跃度-slh2', function () {
		it('查询增加所属店员', async function () {
			let qlRes = await common.callInterface('ql-15004', format.qlParamsFormat({
				sellerid: BASICDATA.staffid000
			}));
			expect(qlRes, '查询出错').to.have.property('dataList');

			for (let data of qlRes.dataList) {
				expect(data.sellername, '查询增加所属店员').to.be.equal('总经理');
			}
		});
	});

	describe('110106_新增客户区域-slh2', function () {
		it('客户区域新增检查', async function () {
			//新增（只有名称）
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(5),
				type: 'area'
			}
			let res = await common.callInterface('sf-1413-1', {
				jsonparam: param
			});

			expect(res, `新增客户区域失败${JSON.stringify(res)}`).to.have.property('val'); //val为pk值

			//查询
			let area = await common.callInterface('ql-1413', format.qlParamsFormat({
				name: param.name
			}));
			expect(area, `查询新增客户区域失败${JSON.stringify(area)}`).to.have.property('dataList');

			//新增
			let sParam = {
				action: 'add',
				name: common.getRandomChineseStr(5),
				type: 'area',
				parentid: area.dataList[0].id
			}
			let sRes = await common.callInterface('sf-1413-1', {
				jsonparam: sParam
			});
			expect(sRes, `新增客户区域失败${JSON.stringify(sRes)}`).to.have.property('val'); //val为pk值

			//新增同名
			let tParam = {
				action: 'add',
				name: sParam.name,
				type: 'area'
			}
			let tRes = await common.callInterface('sf-1413-1', {
				jsonparam: tParam
			});
			expect(tRes, '新增同名客户区域提示错误失败').to.have.property('error');
			expect(tRes.error, '新增同名客户区域提示错误失败').to.be.equal(USEECINTERFACE == 1 ? '相同记录已存在' : `${sParam.name}已存在`);
		})
	});

	describe('110130_客户区域查询-slh2', function () {
		it('停用启用功能', async function () {
			//新增（只有名称）
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(7),
				type: 'area'
			}
			let res = await common.callInterface('sf-1413-1', {
				jsonparam: param
			});
			expect(res, '新增客户区域失败').to.have.property('val'); //val为pk值

			//查询
			let area = await common.callInterface('ql-1413', format.qlParamsFormat({
				name: param.name
			}));
			expect(area, '查询客户区域失败').to.have.property('dataList');
			let areaId = area.dataList[0].id;
			let name = area.dataList[0].name;
			expect(name, '查询客户区域失败').to.be.equal(param.name);

			//停用
			let stopRes = await common.callInterface('cs-1413-31', {
				jsonparam: {
					pk: areaId
				}
			});
			expect(stopRes, '停用客户区域失败').to.have.property('val');
			expect(stopRes.val, '停用客户区域失败').to.be.equal('ok');


			//启用cs-1413-32
			let startRes = await common.callInterface('cs-1413-32', {
				jsonparam: {
					pk: areaId
				}
			});
			expect(startRes, '启用客户区域失败').to.have.property('val');
			expect(startRes.val, '启用客户区域失败').to.be.equal('ok');
		});
	});

	describe('110111_客户区域查询-slh2', function () {
		it('修改保存', async function () {
			//新增
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(7),
				type: 'area'
			}
			let res = await common.callInterface('sf-1413-1', {
				jsonparam: param
			});
			expect(res, '新增客户区域失败').to.have.property('val'); //val为pk值

			//查询新增记录，获取pk
			let area = await common.callInterface('ql-1413', format.qlParamsFormat({
				name: param.name
			}));
			expect(area, '查询客户区域失败').to.have.property('dataList');
			let areaId = area.dataList[0].id;
			let name = area.dataList[0].name;
			expect(name, '查询客户区域失败').to.be.equal(param.name);

			//新增
			let sParam = {
				action: 'add',
				name: common.getRandomChineseStr(5),
				type: 'area',
			}
			let sRes = await common.callInterface('sf-1413-1', {
				jsonparam: sParam
			});
			expect(sRes, '新增客户区域失败').to.have.property('val'); //val为pk值

			//查询新增记录，获取pk
			let area2 = await common.callInterface('ql-1413', format.qlParamsFormat({
				name: sParam.name
			}));
			expect(area2, '查询客户区域失败').to.have.property('dataList');
			let areaId2 = area2.dataList[0].id;
			let name2 = area2.dataList[0].name;
			expect(name2, '查询客户区域失败').to.be.equal(sParam.name);

			//修改为同名
			param.action = 'edit';
			param.name = sParam.name;
			param.pk = areaId;
			let editRes = await common.callInterface('sf-1413-1', {
				jsonparam: param
			});
			expect(editRes, '修改同名客户区域出错').to.have.property('error');
			expect(editRes.error, '修改同名客户区域出错').to.be.equal(USEECINTERFACE == 1 ? '相同记录已存在' : `${sParam.name}已存在`);

			//修改
			param.name = common.getRandomChineseStr(5);
			param.parentid = areaId2;
			let editRes2 = await common.callInterface('sf-1413-1', {
				jsonparam: param
			});
			expect(editRes2, '修改客户区域出错').to.have.property('val');
		});
	});

	describe('110108_新增标签-slh2', function () {
		it('新增同名标签', async function () {
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(5)
			};
			let res = await common.callInterface('sf-1412-1', {
				jsonparam: param
			});
			expect(res, '新增成功').to.have.property('val');
			expect(res.val, '新增成功').to.be.equal('ok');

			res = await common.callInterface('sf-1412-1', {
				jsonparam: param
			});
			expect(res, '新增同名标签出错').to.have.property('error');
			expect(res.error, '新增同名标签出错').to.be.equal(USEECINTERFACE == 1 ? `标签名[${param.name}]已存在` : '同一门店已存在相同标签');
		});
	});

	describe('110110_新增标签.rankA-slh2', function () {
		it('不同门店，新增同名标签', async function () {
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(5)
			};
			let res = await common.callInterface('sf-1412-1', {
				jsonparam: param
			});
			expect(res, '新增成功').to.have.property('val');
			expect(res.val, '新增成功').to.be.equal('ok');

			//登录中洲店
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});

			res = await common.callInterface('sf-1412-1', {
				jsonparam: param
			});
			expect(res, '新增成功').to.have.property('val');
			expect(res.val, '新增成功').to.be.equal('ok');

			//登录常青店
			await common.loginDo();
		});
	});

	describe('110131_新增标签-slh2', function () {
		it('新增标签名称过长', async function () {
			let param = {
				action: 'add',
				name: common.getRandomChineseStr(7)
			};
			let res = await common.callInterface('sf-1412-1', {
				jsonparam: param
			});
			expect(res, '新增标签名称过长出错').to.have.property('error');
			expect(res.error, '新增标签名称过长出错').to.be.equal(USEECINTERFACE == 1 ? '标签名长度不能大于5' : `名称超过限制，最大允许长度为5`);
		});
	});

	describe('110109_客户标签-slh2', function () {
		it('模糊查询', async function () {
			let randomStr = common.getRandomStr(3);
			let nameArr = [randomStr + '重大', randomStr + '重要', randomStr + 'zd'];
			for (let name of nameArr) {
				let param = {
					action: 'add',
					name
				};
				let res = await common.callInterface('sf-1412-1', {
					jsonparam: param
				});
			}

			let qlRes = await common.callInterface('ql-1412', format.qlParamsFormat({
				name: randomStr + '重'
			}));
			expect(qlRes, '查询失败(重)').to.have.property('dataList');
			let resultArr = [false, false];
			for (let data of qlRes.dataList) {
				if (data.name === nameArr[0]) {
					resultArr[0] = true;

					if (resultArr[1]) {
						break;
					}
				}
				if (data.name === nameArr[1]) {
					resultArr[1] = true;

					if (resultArr[0]) {
						break;
					}
				}
			}
			expect(resultArr[0] && resultArr[1], '模糊查询（重）失败').to.be.equal(true);

			qlRes = await common.callInterface('ql-1412', format.qlParamsFormat({
				name: randomStr + 'Z'
			}));
			expect(qlRes, '查询失败(Z)').to.have.property('dataList');
			let findFlag = qlRes.dataList[0].name === nameArr[2];
			expect(findFlag, '模糊查询（Z）失败').to.be.equal(true);

			qlRes = await common.callInterface('ql-1412', format.qlParamsFormat({
				name: randomStr + 'z'
			}));
			expect(qlRes, '查询失败(z)').to.have.property('dataList');
			findFlag = qlRes.dataList[0].name === nameArr[2];
			expect(findFlag, '模糊查询（z）失败').to.be.equal(true);
		});
	});


	describe('110118_欠款客户检查.rankA-slh2', function () {
		it('客户修改界面，是否欠款显示为“是”', async function () {
			//新增欠款客户
			let sfParam = basiceJsonparam.addCustJson();
			sfParam.isdebt = '1';
			let custRes = await salesReqHandler.addCust(sfParam);

			let qfRes = await common.callInterface('qf-1401', {
				'jsonparam': {
					pk: custRes.result.val
				}
			});
			expect(qfRes.show_isdebt, '客户修改界面，是否欠款显示出错').to.be.equal("是");
		});
	});

	if (USEECINTERFACE == 1) {
		describe('110146_信用额度.rankA', function () {
			after(async () => {
				await common.setGlobalParam('sales_default_creditmoney', 0);
			});
			it('信用额度默认为1时，用看不到信用额度的角色新增客户', async function () {
				await common.loginDo({
					logid: '005',
					pass: '000000'
				});

				await common.setGlobalParam('sales_default_creditmoney', '1');

				let sfParam = basiceJsonparam.addCustJson();
				delete sfParam.creditbalance;
				let custRes = await salesReqHandler.addCust(sfParam);
				// console.log(`custRes : ${JSON.stringify(custRes)}`);
				await common.loginDo();

				let qfRes = await common.callInterface('qf-1401', {
					'jsonparam': {
						pk: custRes.result.val
					}
				});
				// console.log(`qfRes : ${JSON.stringify(qfRes)}`);
				expect(Number(qfRes.creditbalance), '验证失败').to.be.equal(1);
			});
		});
	};

	describe('110147_信用额度.rankA-slh2', function () {
		after(async () => {
			await common.setGlobalParam('sales_default_creditmoney', 0);
		});
		it('信用额度默认为1时，用看不到信用额度的角色修改客户', async function () {
			await common.setGlobalParam('sales_default_creditmoney', '1');
			let creditbalanceArr = [0, 1, 1200];
			for (let creditbalance of creditbalanceArr) {
				let sfParam = basiceJsonparam.addCustJson();
				sfParam.creditbalance = creditbalance;
				let custRes = await salesReqHandler.addCust(sfParam);

				await common.loginDo({
					logid: '005',
					pass: '000000'
				});
				await common.setGlobalParam('sales_default_creditmoney', '1');
				sfParam.action = 'edit';
				sfParam.pk = custRes.result.val;
				await salesReqHandler.addCust(sfParam);

				await common.loginDo();
				let qfRes = await common.callInterface('qf-1401', {
					'jsonparam': {
						pk: custRes.result.val
					}
				});
				expect(Number(qfRes.creditbalance), '验证失败').to.be.equal(creditbalance);
			}
		});
	});

	describe('110036_积分查询.rankA', function () {
		it('1.条件查询验证', async function () {
			let qlRes = await common.callInterface('ql-14510', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				invid: BASICDATA.shopidCqd
			}));
			expect(qlRes, '查询失败').to.have.property('dataList');

			let data = qlRes.dataList[0];
			expect(data.invid, '查询结果验证失败').to.be.equal(BASICDATA.shopidCqd);
			expect(data.dwid, '查询结果验证失败').to.be.equal(BASICDATA.dwidXw);
		});

		it('2.分别在客户查询,积分查询,销售开单核销界面对比积分', async function () {
			//积分查询
			let qlRes = await common.callInterface('ql-14510', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
			}));
			expect(qlRes, '积分查询ql-14510查询失败').to.have.property('dataList');
			let sqScore = 0;
			qlRes.dataList.forEach(data => {
				sqScore = common.add(sqScore, data.score);
			});

			//客户查询
			let csResult = await common.callInterface('ql-1339', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
			}));
			expect(csResult, '查询失败').to.have.property('dataList');
			await common.setGlobalParam('sales_client_score_share', 1); //积分跨门店共享 为共享
			//开单核销积分ql-15010
			let hxResult = await common.callInterface('cs-findOtherBalanceAndScore', {
				'jsonparam': {
					dwid: BASICDATA.dwidXw,
					invid: BASICDATA.shopidCqd
				}
			});

			expect(sqScore, '积分查询积分之和与客户查询积分不等').to.be.equal(Number(csResult.dataList[0].totalscore));
			expect(sqScore, '积分查询积分之和与开单核销积分不等').to.be.equal(Number(hxResult.score));
		});
	});

	describe('110153_积分查询', function () {
		it('积分调整单', async function () {
			//积分调整单
			let res = await common.callInterface('sf-14541-1', {
				jsonparam: {
					action: 'add',
					deliver: BASICDATA.staffid000,
					dwid: BASICDATA.dwidXw,
					finpayScore: '1',
					invid: BASICDATA.shopidCqd,
					remark: ''
				}
			});
			//console.log(`res : ${JSON.stringify(res)}`);
			expect(res, '新增积分调整单失败').to.have.property('pk');

			let qlRes = await common.callInterface('ql-14511', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				shopid: BASICDATA.shopidCqd,
				pagesize: '15'
			}));
			expect(qlRes, '查询失败').to.have.property('dataList');
			let find = false;
			let newData;
			for (let data of qlRes.dataList) {
				if (data.id === res.pk) {
					find = true;
					newData = data;
					break;
				}
			}
			expect(find, '查询失败').to.be.equal(true);
			expect(newData.typename, '类型验证有误').to.be.equal('调整单');
			expect(newData.totalnum, '实销数验证有误').to.be.equal('0');
			expect(newData.totalmoney, '实销额验证有误').to.be.equal('0');
		});
	});

	describe('110154_积分查询', function () {
		it('积分抵现', async function () {
			let jsonParams = basiceJsonparam.salesJson();
			jsonParams.details = [];
			jsonParams.details.push({
				matCode: '66666', //积分抵现
				price: '10',
				num: '-1',
				colorid: '0',
				sizeid: '0'
			});
			let sfRes = await common.editBilling(jsonParams, false);

			let qlRes = await common.callInterface('ql-14511', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				shopid: BASICDATA.shopidCqd,
				pagesize: '15'
			}));
			expect(qlRes, '查询失败').to.have.property('dataList');
			let newData = qlRes.dataList[0];
			expect(newData.typename, '类型验证有误').to.be.equal(USEECINTERFACE == 1 ? '兑换单' : '调整单');
		});
	});

	describe('110155_积分查询', function () {
		it('积分兑换', async function () {
			let result = await common.callInterface('sf-14520-1', {
				jsonparam: {
					dwid: BASICDATA.dwidXw,
					invid: BASICDATA.shopidCqd,
					finpayScore: 10,
					deliver: BASICDATA.staffid000,
					finpayCash: 10,
				}
			});

			let qlRes = await common.callInterface('ql-14511', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				shopid: BASICDATA.shopidCqd,
				pagesize: '15'
			}));
			expect(qlRes, '查询失败').to.have.property('dataList');
			let find = false;
			let newData;
			for (let data of qlRes.dataList) {
				if (data.id === result.pk) {
					find = true;
					newData = data;
					break;
				}
			}
			expect(find, '查询失败').to.be.equal(true);
			expect(newData.typename, '类型验证有误').to.be.equal('兑换单');
			expect(newData.totalnum, '实销数验证有误').to.be.equal('0');
			expect(newData.totalmoney, '实销额验证有误').to.be.equal('0');
		});
	});

	describe('110156_积分查询', function () {
		it('销售单', async function () {
			let jsonparam = basiceJsonparam.salesJson();
			jsonparam.shopid = BASICDATA.shopidCqd;
			jsonparam = format.jsonparamFormat(jsonparam);

			let sfRes = await common.callInterface(jsonparam.interfaceid, {
				jsonparam
			});

			let qlRes = await common.callInterface('ql-14511', format.qlParamsFormat({
				dwid: BASICDATA.dwidXw,
				shopid: BASICDATA.shopidCqd,
				pagesize: '15'
			}));
			expect(qlRes, '查询失败').to.have.property('dataList');
			let newData = qlRes.dataList[0];

			expect(Number(newData.finpayScoreadd), '销售单积分验证有误').to.be.above(0);

			// jsonparam.details.map(detail => {
			// 	detail.num = -(detail.num);
			// });

			// let sfRes2 = await common.callInterface(jsonparam.interfaceid, {
			// 	jsonparam
			// });
			// console.log(sfRes2);

			// let qlRes2 = await common.callInterface('ql-14511', format.qlParamsFormat({
			// 	dwid: BASICDATA.dwidXw,
			// 	shopid: BASICDATA.shopidCqd,
			// 	pagesize: '15'
			// }));
			// expect(qlRes2, '查询失败').to.have.property('dataList');
			// newData = qlRes2.dataList[0];

			// expect(Number(newData.finpayScoreadd), '退货积分验证有误').to.be.below(0);
		});
	});
});

describe('往来管理-账款-权限检查', function () {
	before(async function () {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});
	describe('往来管理(普通员工登录)', function () {
		before(async () => {
			await common.loginDo({
				logid: '005',
				pass: '000000'
			});
		});
		it('110057.非总经理角色修改有欠款或余款的客户的名称.rankA', async function () {
			let qfRes = await common.callInterface('qf-1401', {
				pk: BASICDATA.dwidXw
			});
			qfRes.action = 'edit';
			qfRes.nameshort = qfRes.nameshort + '1';

			let sfRes = await common.callInterface('sf-1401', {
				jsonparam: qfRes
			});
			expect(sfRes.error, '保存失败').to.be.equal(USEECINTERFACE == 1 ? '该单位欠款或余额，不可修改信息' : `该单位或他的下级单位在门店[${LOGINDATA.invname}]中还存在余额或欠款`);
		});
	});

	describe('往来管理(店长登录)', function () {
		this.timeout(30000);
		before(async () => {
			await common.loginDo({
				logid: '004',
				pass: '000000'
			});
		});

		describe('110055_客户门店账--店长登录', async function () {
			it('1', async function () {
				let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat());
				qlRes.dataList.forEach(data => {
					expect(data.invname, '保存失败').to.be.equal('常青店');
				});
			});

			it('3', async function () {
				//因为中洲店是肯定有账款的，所以就用常青店店长登录，查中洲店账款
				let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat({
					invid: BASICDATA.shopidZzd,
				}));
				expect(Number(qlRes.count), '店长查看非本门店客户帐出错').to.be.equal(0);
			});
		});


		describe('110031_店长查看客户门店帐等.rankA', async function () {
			it('1', async function () {
				//客户门店账
				let interfaceids = ['ql-15001'];

				for (let interfaceid of interfaceids) {
					let qlRes = await common.callInterface(interfaceid, format.qlParamsFormat({
						invid: BASICDATA.shopidZzd
					}));
					expect(qlRes.dataList.length, '查询出错').to.be.equal(0);
				}
			})
		});

		// describe('110143_客户账款-按上级单位', function () {
		// 	it('不跨门店下，店长导出的数据', async function () {
		// 		await common.setGlobalParam('sales_verify_overshop', 0);

		// 		let qlRes = await common.callInterface('ql-15005', format.qlParamsFormat({
		// 			shopid: BASICDATA.shopidZzd
		// 		}));
		// 		expect(qlRes, '查询失败').to.have.property('dataList');
		// 		expect(qlRes.dataList.length, '查询结果不符期望').to.be.equal(0);

		// 		await common.setGlobalParam('sales_verify_overshop', 1);
		// 	});
		// });
	});

	describe('往来管理(开单员登录)', function () {
		before(async () => {
			await common.loginDo({
				logid: '005',
				pass: '000000'
			});
		});
		describe('110032_开单员查看客户门店帐', async function () {
			it('110032_1', async function () {
				let qlRes = await common.callInterface('ql-15001', format.qlParamsFormat());
				qlRes.dataList.map(data => {
					expect(data.invid, '出现别的门店的客户帐款信息').to.be.equal(BASICDATA.shopidCqd);
				});
			});
		});
	});
});
