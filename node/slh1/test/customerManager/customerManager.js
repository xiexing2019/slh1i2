'use strict'
const common = require('../../../lib/common.js');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const custManager = require('../../../reqHandler/slh1/index');

describe('客户管理-mainLine', function () {
    this.timeout(60000);
    let newCustomer;
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        // 新增客户
        newCustomer = await custManager.slh1CustomerManager.saveCustomerFull(basiceJsonparam.addCustJson());
    });

    describe('新增客户', function () {
        it('查询客户列表', async function () {
            // 查询客户列表
            let qlRes = await custManager.slh1CustomerManager.queryCustomerList({ dwid: newCustomer.result.val }).then(
                res => res.result.dataList.find(data => data.id == newCustomer.result.val));
            expect(qlRes.dwname, '新增客户失败').to.be.equal(newCustomer.params.jsonparam.nameshort);
        });
        it('查询客户详情', async function () {
            let qfRes = await custManager.slh1CustomerManager.queryCustomerFull({ pk: newCustomer.result.val })
            common.isApproximatelyEqualAssert(newCustomer.params.jsonparam, qfRes);
        });
    });

    describe('修改客户', function () {
        let editCustomer;
        before(async () => {
            //修改客户信息
            let editParams = basiceJsonparam.addCustJson();
            editParams.pk = newCustomer.result.val;
            editCustomer = await custManager.slh1CustomerManager.saveCustomerFull(editParams);
        });
        it('查询客户列表', async function () {
            let qlRes = await custManager.slh1CustomerManager.queryCustomerList({ dwid: newCustomer.result.val }).then(
                res => res.result.dataList.find(data => data.id == newCustomer.result.val));
            expect(qlRes.dwname, '新增客户失败').to.be.equal(editCustomer.params.jsonparam.nameshort);
        });
        it('查询客户详情', async function () {
            let qfRes = await custManager.slh1CustomerManager.queryCustomerFull({ pk: newCustomer.result.val })
            common.isApproximatelyEqualAssert(editCustomer.params.jsonparam, qfRes);
        });
    });

    describe('停用客户', function () {
        let disableCust;
        before(async () => {
            // 停用客户
            disableCust = await custManager.slh1CustomerManager.disableCustomer({ pk: newCustomer.result.val });
        });
        it('查询客户列表', async function () {
            let qlRes = await custManager.slh1CustomerManager.queryCustomerList({ dwid: newCustomer.result.val }).then(
                res => res.result.dataList.find(data => data.id == newCustomer.result.val));
            expect(qlRes.delflag, '停用客户失败').to.be.equal('1');
        });
        it('查询客户详情', async function () {
            let qfRes = await custManager.slh1CustomerManager.queryCustomerFull({ pk: newCustomer.result.val });
            expect(qfRes.delflag, '停用客户失败').to.be.equal('1');
        });
    });

    describe('启用客户', function () {
        let enableCust;
        before(async () => {
            // 启用客户
            enableCust = await custManager.slh1CustomerManager.enableCustomer({ pk: newCustomer.result.val });
        });
        it('查询客户列表', async function () {
            let qlRes = await custManager.slh1CustomerManager.queryCustomerList({ dwid: newCustomer.result.val }).then(
                res => res.result.dataList.find(data => data.id == newCustomer.result.val));
            expect(qlRes.delflag, '启用客户失败').to.be.equal('0');
        });
        it('查询客户详情', async function () {
            let qfRes = await custManager.slh1CustomerManager.queryCustomerFull({ pk: newCustomer.result.val });
            expect(qfRes.delflag, '启用客户失败').to.be.equal('0');
        });
    });

    describe('客户积分调整', function () {
        let reviseIntegral, score1;
        before(async () => {
            //积分查询
            score1 = await custManager.slh1CustomerManager.queryScoreList({ dwid: BASICDATA.dwidXw }).then(
                res => res.result.dataList.find(data => data.dwid));
            // 新增积分调整
            reviseIntegral = await custManager.slh1CustomerManager.saveReviseScoreFull({
                invid: BASICDATA.shopidCqd,
                dwid: BASICDATA.dwidXw,
                finpayScore: '100'
            });
        });
        describe('新增积分调整', function () {
            it('查询积分调整列表', async function () {
                let qlRes = await custManager.slh1CustomerManager.queryReviseScoreList({ dwid: BASICDATA.dwidXw }).then(
                    res => res.result.dataList.find(data => data.billno == reviseIntegral.result.billno));
                common.isApproximatelyEqualAssert(reviseIntegral.params.jsonparam, qlRes);
            });
            it('查询客户积分', async function () {
                let score2 = await custManager.slh1CustomerManager.queryScoreList({ dwid: BASICDATA.dwidXw }).then(
                    res => res.result.dataList.find(data => data.dwid));
                expect(Number(score1.score) + Number(100)).to.be.equal(Number(score2.score));
            });
        });
    });

    describe('客户区域', function () {
        let newCustomerDistrict;
        before(async () => {
            //查询父客户区域
            let parendId = await custManager.slh1CustomerManager.queryCustomerDistrictList({ pagesize: 1 }).then(
                res => res.result.dataList.find(data => data.id));
            // 新增客户区域
            let params = basiceJsonparam.addCustomerDistrict({ parentid: parendId.id });
            newCustomerDistrict = await custManager.slh1CustomerManager.saveCustomerDistrictFull(params)
        });
        after(async () => {
            // 停用客户区域
            await custManager.slh1CustomerManager.disableCustomerDistrict({ pk: newCustomerDistrict.result.val });
        });
        describe('新增客户区域', function () {
            it('查询客户区域列表', async function () {
                let qlRes = await custManager.slh1CustomerManager.queryCustomerDistrictList({ name: newCustomerDistrict.params.jsonparam.name }).then(
                    res => res.result.dataList.find(data => data.id == newCustomerDistrict.result.val));
                common.isApproximatelyEqualAssert(newCustomerDistrict.params.jsonparam, qlRes);
            });
            it('查询客户区域详情', async function () {
                let qfRes = await custManager.slh1CustomerManager.queryCustomerDistrictFull({ pk: newCustomerDistrict.result.val });
                common.isApproximatelyEqualAssert(newCustomerDistrict.params.jsonparam, qfRes)
            });
        });

    });

});