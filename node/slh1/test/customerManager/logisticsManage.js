'use strict'
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');
const reqHandler = require('../../help/reqHandlerHelp');
const logisVerifyHelp = require('../../help/salesHelp/logisVerifyHelp.js');

describe('物流商管理', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('110091_物流商帐款', function () {
		it('查询排序翻页汇总', async function () {
			let qlRes = await common.callInterface('ql-1433', format.qlParamsFormat({
				dwid: BASICDATA.dwidSFKD,
				invid: BASICDATA.shopidCqd,
				search_list: '1',
				search_count: '1',
				search_sum: '1'
			}));

			expect(qlRes, '查询出错').to.have.property('dataList');

			let listData = qlRes.dataList[0];
			let sumrow = qlRes.sumrow;
			expect(listData.id, '条件查询出错').to.be.equal(BASICDATA.dwidSFKD);
			expect(listData.payedsum, '底部已收数据错误').to.be.equal(sumrow.payedsum);
			expect(listData.agencysum, '底部代收数据错误').to.be.equal(sumrow.agencysum);
			expect(listData.shouldask, '底部应收数据错误').to.be.equal(sumrow.shouldask);
		});
	});

	describe('110092_物流商帐款', function () {
		it('详细页面内容检查.rankA', async function () {
			//开代收单
			let rem = common.getRandomStr(6);
			let salesParam = {
				"interfaceid": "sf-14211-1",
				"dwid": BASICDATA.dwidXw,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${rem}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResult = await common.editBilling(salesParam);
			expect(salesResult.result, '代收开单出错').to.have.property('billno');

			let qlRes = await common.callInterface('ql-14331', format.qlParamsFormat({
				logisDwid: BASICDATA.dwidSFKD,
				shopid: BASICDATA.shopidCqd,
				dwid: BASICDATA.dwidXw,
				logisPayflag: '0'
			}));

			expect(qlRes, '物流商帐款-详细页面查询出错').to.have.property('dataList');

			let billno = salesResult.result.billno;
			let found = false;
			for (let data of qlRes.dataList) {
				if (data.billno === billno) {
					found = true;
					break;
				}
			}
			expect(found, '物流商帐款-详细页面查询出错').to.be.equal(true);
		});
	});

	describe('110093_物流商帐款', function () {
		before(async () => {
			await common.setGlobalParam('sales_verify_overshop', 1);
		});

		it('详细页面数据检查--开启跨门店核销', async function () {
			//新建物流商
			let param = {
				accountno: common.getRandomNumStr(13),
				addr: '杭州市星耀城',
				invid: BASICDATA.shopidZzd,
				nameshort: '物流商' + common.getRandomStr(3),
				phone: '131' + common.getRandomNumStr(8),
				rem: '备注',
				sellerid: BASICDATA.staffid000,
				zipcode: common.getRandomNumStr(6)
			};
			let sfRes = await common.callInterface('sf-2310', {
				jsonparam: param
			});
			expect(sfRes, `新增物流商保存出错:${JSON.stringify(sfRes)}`).to.have.property('val');

			//新增物流代收单（常青店）
			let rem = common.getRandomStr(6);
			let salesParam = {
				"interfaceid": "sf-14211-1",
				"dwid": BASICDATA.dwidXw,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": sfRes.val, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${rem}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResult = await common.editBilling(salesParam);
			expect(salesResult.result, '代收开单出错').to.have.property('billno');

			//登录中洲店
			await common.loginDo({
				'logid': '200',
				'pass': '000000',
			});

			//
			let remZzd = common.getRandomStr(6);
			let salesParamZzd = {
				"interfaceid": "sf-14211-1",
				"dwid": BASICDATA.dwidXw,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": sfRes.val, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${remZzd}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResultZzd = await common.editBilling(salesParamZzd);
			expect(salesResultZzd.result, '代收开单出错').to.have.property('billno');

			//登录常青店
			await common.loginDo();

			let qlRes = await common.callInterface('ql-14331', format.qlParamsFormat({
				logisDwid: sfRes.val,
				dwid: BASICDATA.dwidXw,
				logisPayflag: '0'
			}));

			expect(qlRes, '物流商帐款-详细页面查询出错').to.have.property('dataList');
			let billnoArr = [salesResult.result.billno, salesResultZzd.result.billno];

			for (let billno of billnoArr) {
				let found = false;
				for (let data of qlRes.dataList) {
					if (data.billno === billno) {
						found = true;
						break;
					}
				}
				expect(found, '物流商帐款-详细页面查询出错').to.be.equal(true);
			}
		});
	});

	describe('110094_物流商账款', function () {
		it('检查作废的物流单', async function () {
			//新增物流代收单（常青店）
			let rem = common.getRandomStr(6);
			let salesParam = {
				"interfaceid": "sf-14211-1",
				"dwid": BASICDATA.dwidXw,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": BASICDATA.dwidSFKD, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${rem}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResult = await common.editBilling(salesParam);
			expect(salesResult.result, '代收开单出错').to.have.property('billno');

			//作废
			let cancelResult = await common.callInterface('cs-cancel-saleout-bill', {
				jsonparam: {
					pk: salesResult.result.pk
				}
			});
			expect(cancelResult, '作废销售单出错').to.have.property('val');

			//查询
			let qlRes = await common.callInterface('ql-14331', format.qlParamsFormat({
				logisDwid: BASICDATA.dwidSFKD,
				shopid: BASICDATA.shopidCqd,
				dwid: BASICDATA.dwidXw,
				pagesize: 15
			}));
			expect(qlRes, '物流商帐款-详细页面查询出错').to.have.property('dataList');

			//验证
			let findFlag = false;
			for (let data of qlRes.dataList) {
				if (data.id === salesResult.result.pk) {
					findFlag = true;
					break;
				}
			}
			expect(findFlag, '作废掉的物流单继续显示').to.be.equal(false);
		});
	});

	describe('110095_物流商账款.rankA', function () {
		before(async () => {
			await common.setGlobalParam('sales_verify_overshop', 1);
		});

		it('选择门店后再去检查详细页面的代收单', async function () {
			let qlRes = await common.callInterface('ql-14331', format.qlParamsFormat({
				'logisDwid': BASICDATA.dwidSFKD,
				'shopid': BASICDATA.shopidCqd
			}));
			expect(qlRes, '物流商帐款-详细页面查询出错').to.have.property('dataList');

			let anotherShopFlag = false;
			for (let data of qlRes.dataList) {
				if (data.shopname !== '常青店') {
					anotherShopFlag = true;
					break;
				}
			}
			expect(anotherShopFlag, '其它门店的代收单不应显示').to.be.equal(false);
		});
	});

	describe('110096_物流商停用.rankA', function () {
		it('停用应收不为0的物流商', async function () {
			//新建物流商
			let param = {
				accountno: common.getRandomNumStr(13),
				addr: '杭州市星耀城',
				invid: LOGINDATA.invid,
				nameshort: '物流商' + common.getRandomStr(3),
				phone: '131' + common.getRandomNumStr(8),
				rem: '备注',
				sellerid: BASICDATA.staffid000,
				zipcode: common.getRandomNumStr(6)
			};
			let sfRes = await common.callInterface('sf-2310', {
				jsonparam: param
			});
			expect(sfRes, `新增物流商保存出错,${JSON.stringify(sfRes)}`).to.have.property('val');

			//新增物流代收单（常青店）
			let rem = common.getRandomStr(6);
			let salesParam = {
				"interfaceid": "sf-14211-1",
				"dwid": BASICDATA.dwidXw,
				"srcType": "1",
				"remark": "代收",
				"agency": 480,
				"logisDwid": sfRes.val, //物流商ID值
				"logisBillno": new Date().getTime(),
				"logisRem": `logisRem${rem}`,
				"details": [{
					"num": "1",
					"sizeid": "M",
					"matCode": "Agc001",
					"rem": "明细1",
					"colorid": "BaiSe",
					"price": "200",
				}, {
					"num": "2",
					"sizeid": "L",
					"matCode": "Agc001",
					"rem": "明细2",
					"colorid": "BaiSe",
					"price": "200",
				}],
			};
			let salesResult = await common.editBilling(salesParam);
			expect(salesResult.result, '代收开单出错').to.have.property('billno');
			if (USEECINTERFACE == 2) await common.delay(500);
			//停用物流商
			let stopRes = await reqHandler.csIFCHandler({
				interfaceid: 'cs-logisdelete',
				pk: sfRes.val,
				check: false,
			});
			if (USEECINTERFACE == 1) {
				expect(stopRes.result.error, '停用出错').to.be.includes('不允许停用');
			} else {
				expect(stopRes.result.error, '停用出错').to.be.includes('还存在余额或欠款');
			}
		});

		// it('停用应收为0的物流商', async function () {
		//   //新建物流商
		//   let param = {
		//     accountno: common.getRandomNumStr(13),
		//     addr: '杭州市星耀城',
		//     invid: BASICDATA.shopidZzd,
		//     nameshort: '物流商' + common.getRandomStr(3),
		//     phone: '131' + common.getRandomNumStr(8),
		//     rem: '备注',
		//     sellerid: BASICDATA.staffid000,
		//     zipcode: common.getRandomNumStr(6)
		//   };
		//   let sfRes = await common.callInterface('sf-2310', {jsonparam: param});
		//   expect(sfRes, '新增物流商保存出错').to.have.property('val');

		//   //停用物流商
		//   let stopRes = await common.callInterface('cs-logisdelete', {jsonparam: {
		//     pk: sfRes.val
		//   }});
		//   expect(stopRes, '停用出错').to.have.property('val');
		//   expect(stopRes.val, '停用出错').to.be.equal('ok');
		// });
	});

});
