'use strict'
const common = require('../../../lib/common.js');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const mainReqHandler = require('../../help/basicInfoHelp/mainDataReqHandler');
const custManager = require('../../../reqHandler/slh1/index');
// const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler.js');

describe('厂商管理-slh2', function () {
	this.timeout(60000);

	before(async () => {
		await common.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	});

	describe('厂商-mainLine', function () {
		let newFactory;
		before(async () => {
			// 新增厂商
			newFactory = await custManager.slh1CustomerManager.saveFactoryFull(basiceJsonparam.addManufacturerJson());
		});
		describe('新增厂商', function () {
			it('查询厂商列表', async function () {
				let qlRes = await custManager.slh1CustomerManager.queryFactoryList({ id: newFactory.result.val }).then(
					res => res.result.dataList.find(data => data.name == newFactory.params.jsonparam.name));
				common.isApproximatelyEqualAssert(newFactory.params.jsonparam, qlRes);
			});
			it('查询厂商详情', async function () {
				let qfRes = await custManager.slh1CustomerManager.queryFactoryFull({ pk: newFactory.result.val });
				common.isApproximatelyEqualAssert(newFactory.params.jsonparam, qfRes);
			});
		});
		describe('修改厂商', function () {
			let editFactory;
			before(async () => {
				// 修改厂商
				let editFactoryParams = basiceJsonparam.addManufacturerJson();
				editFactoryParams.pk = newFactory.result.val;
				editFactory = await custManager.slh1CustomerManager.saveFactoryFull(editFactoryParams);
			})
			it('查询厂家列表', async function () {
				let qlRes = await custManager.slh1CustomerManager.queryFactoryList({ id: newFactory.result.val }).then(
					res => res.result.dataList.find(data => data.name == editFactory.params.jsonparam.name));
				common.isApproximatelyEqualAssert(editFactory.params.jsonparam, qlRes);
			});
			it('查询厂家详情', async function () {
				let qfRes = await custManager.slh1CustomerManager.queryFactoryFull({ pk: newFactory.result.val });
				common.isApproximatelyEqualAssert(editFactory.params.jsonparam, qfRes);
			});
		});
	});



	describe('110038_新增厂商.rankA', function () {
		after(async () => {
			await common.loginDo();
		});
		it('1.新增同名厂商', async function () {
			let result = await common.callInterface('sf-2241', {
				jsonparam: {
					nameshort: 'Vell'
				}
			});
			expect(result.error, '保存同名厂商出错').to.be.includes('已存在[Vell]名称的[厂商]');

			let qfRes = await common.callInterface('qf-2241', {
				pk: BASICDATA.dwidVell
			});
			let phone = qfRes.phone;
			if (!phone || phone.length === 0) {
				phone = '139' + common.getRandomNumStr();
				qfRes.action = 'edit';
				qfRes.phone = phone;
				await common.callInterface('sf-2241', {
					jsonparam: qfRes
				});
			}

			result = await common.callInterface('sf-2241', {
				jsonparam: {
					nameshort: 'Vell4',
					phone: phone
				}
			});

			expect(result.error, '保存同手机号厂商出错').to.be.includes('已存在[' + phone + ']号码的[厂商]');
		});
		it('2.不同门店新增同名厂商', async function () {
			await common.loginDo({ logid: 200 });
			let sfRes = await mainReqHandler.saveSupplier({ nameshort: 'Vell', check: false });
			expect(sfRes.result.error, `不同门店保存同名厂商，保存成功${JSON.stringify(sfRes)}`).to.be.includes('已存在[Vell]名称的[厂商]');
		});

	});

	describe('110041_厂商门店账.rankA', function () {
		it('条件查询', async function () {
			let sfRes = await common.callInterface('sf-2241', {
				jsonparam: {
					nameshort: 'manu' + common.getRandomStr(6)
				}
			});
			expect(sfRes, '新增厂商出错').to.have.property('val');

			// 采购订货
			let purchaseParam = basiceJsonparam.purchaseOrderJson();
			purchaseParam.dwid = sfRes.val;
			let purchOrderResult = await common.callInterface('sf-22101-1', {
				'jsonparam': format.jsonparamFormat(purchaseParam)
			});

			let qlRes = await common.callInterface('ql-15003', format.qlParamsFormat({
				dwxxid: sfRes.val,
				invid: BASICDATA.shopidCqd
			}));
			expect(qlRes, '厂商门店账查询出错').not.to.have.property('error');
			expect(qlRes.dataList[0].invid, '厂商门店账查询出错').to.be.equal(BASICDATA.shopidCqd);
			expect(qlRes.dataList[0].dwid, '厂商门店账查询出错').to.be.equal(sfRes.val);
		});
	});

	describe('110042_厂商总账.rankA', function () {
		it('条件查询', async function () {
			// 采购订货
			let purchaseParam = basiceJsonparam.purchaseOrderJson();
			purchaseParam.dwid = BASICDATA.dwidVell;
			let purchOrderResult = await common.callInterface('sf-22101-1', {
				'jsonparam': format.jsonparamFormat(purchaseParam)
			});

			let qlRes = await common.callInterface('ql-15007', format.qlParamsFormat({
				dwid: BASICDATA.dwidVell
			}));
			expect(qlRes, '厂商总账查询出错').to.have.property('dataList');
			expect(qlRes.dataList[0].name, '厂商总账查询出错').to.be.equal('Vell');
		});
	});
	//厂商总账详细界面，累计未结有问题，
	describe('110043_厂商总账数值核对.rankA', function () {
		it('核对厂商总帐数值正确性', async function () {
			TESTCASE = {
				describe: "验证厂商总账数据是否正确",
				expect: "厂商总账的数据=厂商总帐详细界面中，在每个门店的第一条累计未结的和",
				jira: "SLHSEC-6797，修改采购单，账款详细页面相应记录被顶上第一条，而累计未结没有改变"
			}
			let qlRes = await common.callInterface('ql-15007', format.qlParamsFormat({
				dwid: BASICDATA.dwidVell,
			}));
			expect(qlRes, '厂商总账查询出错').not.to.have.property('error');

			//明细列表查询
			let detailsRes = await common.callInterface('ql-13471', format.qlParamsFormat({
				dwid: BASICDATA.dwidVell
			}));
			expect(detailsRes, '厂商总账-明细列表查询出错').to.have.property('dataList');
			//console.log(`detailsRes.dataList[0] : ${JSON.stringify(detailsRes.dataList[0])}`);
			let sum = 0,
				shopArr = [];
			detailsRes.dataList.forEach(detail => {
				if (!shopArr.includes(detail.shopname)) {
					shopArr.push(detail.shopname);
					sum += parseFloat(detail.finpayLastbalance);
				}
			});
			expect(sum, '').to.be.equal(Number(qlRes.dataList[0].balance));
		});
	});

	describe('110044_物流商查询', function () {
		it('条件查询', async function () {
			let qlRes = await common.callInterface('qd-2310', format.qlParamsFormat({
				name: '顺丰快递',
				invid: BASICDATA.shopidCqd
			}));
			expect(qlRes, '物流商查询失败').to.have.property('dataList');

			let data = qlRes.dataList[0];
			expect(data.nameshort, '物流商查询失败').to.be.equal('顺丰快递');
		});

		it('查看物流商明细信息', async function () {
			let detail = await common.callInterface('qf-2310', {
				jsonparam: {
					pk: BASICDATA.dwidSFKD
				}
			});

			expect(detail, '查看物流商明细信息失败').to.have.property('pk');
			expect(detail.nameshort, '物流商明细信息出错').to.be.equal('顺丰快递');
		})
	});

	describe('110046', function () {
		it('新增物流商.rankA', async function () {
			let jsonparam = {
				nameshort: common.getRandomStr(6),
				invid: BASICDATA.shopidCqd,
				phone: '139' + common.getRandomNumStr(8),
				sellerid: BASICDATA.staffid000
			}
			let res = await common.callInterface('sf-2310', {
				jsonparam
			});
			expect(res, '新增物流商失败').to.have.property('val');

			let qfRes = await common.callInterface('qf-2310', {
				pk: res.val
			});

			expect(qfRes.nameshort, '新增物流商信息有误').to.be.equal(jsonparam.nameshort);
			expect(qfRes.phone, '新增物流商信息有误').to.be.equal(jsonparam.phone);
			expect(qfRes.invid, '新增物流商信息有误').to.be.equal(jsonparam.invid);
			expect(qfRes.sellerid, '新增物流商信息有误').to.be.equal(jsonparam.sellerid);
		});
	});

	describe('110045', function () {

		it('物流商修改、停用、启用', async function () {
			//新增物流商，查询
			let jsonparam = {
				nameshort: common.getRandomStr(6),
				invid: BASICDATA.shopidCqd,
				phone: '139' + common.getRandomNumStr(8),
				sellerid: BASICDATA.staffid000
			}
			let res = await common.callInterface('sf-2310', {
				jsonparam
			});
			expect(res, '新增物流商失败').to.have.property('val');

			let qfRes = await common.callInterface('qf-2310', {
				pk: res.val
			});

			//修改保存,查询
			qfRes.action = 'edit';
			qfRes.nameshort += common.getRandomStr(1);
			//修改物流商
			let sfRes = await common.callInterface('sf-2310', {
				jsonparam: qfRes
			});
			expect(sfRes, '修改保存物流商失败').to.have.property('val');
			//查询物流商
			let detail = await common.callInterface('qd-2310', format.qlParamsFormat({
				name: qfRes.nameshort,
				invid: BASICDATA.shopidCqd
			}));

			expect(detail, '查询物流商出错').to.have.property('dataList');
			expect(detail.dataList[0].nameshort, '修改保存物流商出错').to.be.equal(qfRes.nameshort);

			//停用，查询
			let stopRes = await common.callInterface('cs-logisdelete', {
				pk: qfRes.pk
			});
			expect(stopRes, '停用物流商出错').to.have.property('val');
			expect(stopRes.val, '停用物流商出错').to.be.equal('ok');;

			detail = await common.callInterface('qd-2310', format.qlParamsFormat({
				name: qfRes.nameshort,
				invid: BASICDATA.shopidCqd,
				delflag: 1,
			}));

			expect(detail, '查询物流商出错').to.have.property('dataList');
			expect(detail.dataList[0].delflag, '修改保存物流商出错').to.be.equal('1');

			//启用，查询
			let startRes = await common.callInterface('cs-2310-undelete', {
				pk: qfRes.pk
			});
			expect(startRes, '启用物流商出错').to.have.property('val');
			expect(startRes.val, '启用物流商出错').to.be.equal('ok');;

			detail = await common.callInterface('qd-2310', format.qlParamsFormat({
				name: qfRes.nameshort,
				invid: BASICDATA.shopidCqd,
				//deflag: 0
			}));

			expect(detail, '查询物流商出错').to.have.property('dataList');
			expect(detail.dataList[0].delflag, '修改保存物流商出错').to.be.equal('0');
		});
	});

	describe('110047_新增回访', function () {
		it('新增客户回访并验证', async function () {
			let param = {
				action: 'add',
				custreturndate: common.getCurrentDate(),
				custreturntype: 1,
				dwid: BASICDATA.dwidXw,
				feedback: '反馈' + common.getRandomStr(6),
				returnperson: BASICDATA.staffid000,
				subject: '主题' + common.getRandomStr(3),
			}
			let sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '新增客户回访出错').to.have.property('val');

			//查询
			let qlRes = await common.callInterface('qd-15002', format.qlParamsFormat({
				custreturndate1: param.custreturndate,
				custreturndate2: param.custreturndate,
				dwid: param.dwid,
				subject: param.subject
			}));
			expect(qlRes, '查询客户回访出错').to.have.property('dataList');

			let data = qlRes.dataList[0];
			expect(data.dwxxName, '检查刚新增的回访单出错').to.be.equal('小王');
			expect(data.subject, '检查刚新增的回访单出错').to.be.equal(param.subject);
			expect(data.feedback, '检查刚新增的回访单出错').to.be.equal(param.feedback);
		});
	});

	describe('110049_客户回访记录修改和删除操作', function () {
		it('修改客户回访记录并验证', async function () {

			let param = {
				action: 'add',
				custreturndate: common.getCurrentDate(),
				custreturntype: 1,
				dwid: BASICDATA.dwidXw,
				feedback: '反馈' + common.getRandomStr(6),
				returnperson: BASICDATA.staffid000,
				subject: '主题' + common.getRandomStr(3),
			}
			let sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '新增客户回访出错').to.have.property('val');
			param.pk = sfRes.val;
			param.subject = '修改主题' + common.getRandomStr(3);
			param.action = 'edit';
			sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '修复客户回访出错').to.have.property('val');

			//查询
			let qlRes = await common.callInterface('qd-15002', format.qlParamsFormat({
				custreturndate1: param.custreturndate,
				custreturndate2: param.custreturndate,
				dwid: param.dwid,
				subject: param.subject
			}));
			expect(qlRes, '修改客户回访记录并验证出错').to.have.property('dataList');
		})

		it('删除客户回访记录并验证', async function () {
			let param = {
				action: 'add',
				custreturndate: common.getCurrentDate(),
				custreturntype: 1,
				dwid: BASICDATA.dwidXw,
				feedback: '反馈' + common.getRandomStr(6),
				returnperson: BASICDATA.staffid000,
				subject: '主题' + common.getRandomStr(3),
			}
			let sfRes = await common.callInterface('sf-15002', {
				jsonparam: param
			});
			expect(sfRes, '新增客户回访出错').to.have.property('val');
			param.pk = sfRes.val;

			let deleteRes = await common.callInterface('cs-15002', {
				pk: param.pk
			});
			expect(deleteRes, '删除客户回访出错').to.have.property('val');
			expect(deleteRes.val, '删除客户回访出错').to.be.equal('ok');

			let qlRes = await common.callInterface('qd-15002', format.qlParamsFormat({
				custreturndate1: param.custreturndate,
				custreturndate2: param.custreturndate,
				dwid: param.dwid,
				subject: param.subject
			}));
			expect(qlRes, '删除客户回访记录并验证出错').to.have.property('dataList');
			expect(qlRes.dataList.length, '删除客户回访记录并验证出错').to.be.equal(0);
		});
	});

	describe('110046_新增物流商.rankA', function () {
		it('输入所有信息', async function () {
			let param = {
				accountno: common.getRandomNumStr(13),
				addr: '杭州市星耀城',
				invid: BASICDATA.shopidZzd,
				nameshort: '物流商' + common.getRandomStr(3),
				phone: '131' + common.getRandomNumStr(8),
				rem: '备注',
				sellerid: BASICDATA.staffid000,
				zipcode: common.getRandomNumStr(6)
			};
			let sfRes = await common.callInterface('sf-2310', {
				jsonparam: param
			});
			expect(sfRes, '新增物流商保存出错').to.have.property('val');

			let qlRes = await common.callInterface('qd-2310', format.qlParamsFormat({
				name: param.nameshort,
				invid: param.invid,
				sellerid: param.sellerid,
				phone: param.phone
			}));
			expect(qlRes, '查询最新增加的物流商出错').to.have.property('dataList');

			let detail = qlRes.dataList[0];
			expect(detail.accountno, '查询最新增加的物流商信息有误').to.be.equal(param.accountno);
			expect(detail.rem, '查询最新增加的物流商信息有误').to.be.equal(param.rem);
			expect(detail.addr, '查询最新增加的物流商信息有误').to.be.equal(param.addr);
			expect(detail.nameshort, '查询最新增加的物流商信息有误').to.be.equal(param.nameshort);
		});
	});

	describe('110012_厂商查询', function () {
		it('条件查询', async function () {
			//新增厂商
			let jsonparam = {
				nameshort: 'manu' + common.getRandomStr(6),
				phone: '139' + common.getRandomNumStr(8),
			}
			let sfRes = await common.callInterface('sf-2241', {
				jsonparam
			});
			expect(sfRes, '新增厂商出错').to.have.property('val');

			//查询验证
			let qlRes = await common.callInterface('qd-2241', format.qlParamsFormat({
				phone: jsonparam.phone,
				id: sfRes.val
			}));
			expect(qlRes, '查询厂商出错').to.have.property('dataList');
			expect(qlRes.dataList[0].phone, '查询厂商出错').to.be.equal(jsonparam.phone);
			expect(qlRes.dataList[0].name, '查询厂商出错').to.be.equal(jsonparam.nameshort);

			//停用
			let stopRes = await common.callInterface('cs-2241-2', {
				pk: sfRes.val
			});
			expect(stopRes.val, '停用厂商失败').to.be.equal('ok');

			//查询验证
			qlRes = await common.callInterface('qd-2241', format.qlParamsFormat({
				phone: jsonparam.phone,
				id: sfRes.val,
				delflag: '1'
			}));
			expect(qlRes, '查询厂商出错').to.have.property('dataList');
			expect(qlRes.dataList[0].phone, '查询厂商出错').to.be.equal(jsonparam.phone);
			expect(qlRes.dataList[0].name, '查询厂商出错').to.be.equal(jsonparam.nameshort);
		});
	});

	describe('110086_厂商账款', function () {
		it('厂商名称条件查询', async function () {
			let interfaceArr = ['ql-15003', 'ql-15007'];

			for (let url of interfaceArr) {
				let qlResult = await common.callInterface(url, format.qlParamsFormat({
					name: "Vell"
				}));
				expect(qlResult, '查询失败').to.have.property('dataList');

				for (let data of qlResult.dataList) {
					expect(data.name, '查询结果有误').to.be.equal('Vell');
				}
			}
		});
	});
});


describe('往来管理(普通员工登录)', function () {
	this.timeout(30000);

	before(async () => {
		await common.loginDo({
			logid: '005',
			pass: '000000'
		});
		BASICDATA = await getBasicData.getBasicID();
	});


	describe('110061_非总经理登录，只显示本门店物流商', async function () {
		it('非总经理登录只显示本门店的物流商', async function () {
			let qdRes = await common.callInterface('qd-2310', format.qlParamsFormat())
			expect(qdRes, '查询物流商出错').to.have.property('dataList');

			qdRes.dataList.map(data => {
				expect(data.invName, '非总经理登只显示本门店的物流商失败').to.be.equal('常青店');
			});
		});
	});
});
