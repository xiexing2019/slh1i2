const Case = require("./Case.js");

class File {
  constructor(useType) {
    if (useType) {
      this.useType = useType;
    }
  }

  parseJsonObj(jsonObj) {
    this.name = jsonObj.name;
    this.beforeAll = jsonObj.before;
    this.afterAll = jsonObj.after;
    this.subNodes = [];
    this.useType = this.useType ? this.useType : jsonObj.useType;

    for (let node of jsonObj.subNodes) {
      if (node.type === 'it') {
        let useCase = new Case(node);
        useCase.useType = this.useType;
        this.subNodes.push(useCase);
      } else if (node.type === 'describe') {
        let describeObj = new File(this.useType);
        describeObj.parseJsonObj(node);
        this.subNodes.push(describeObj);
      }
    }
  }

  timeout(timeLimits) {
    this.timeLimits = timeLimits;
  }

  startTest() {
    let beforeAll = this.beforeAll;
    let afterAll = this.afterAll;
    let subNodes = this.subNodes;
    let timeLimits = this.timeLimits || 0;

    let testFunction = function () {
      if (timeLimits > 0) {
        this.timeout(timeLimits);
      }

      if (beforeAll && typeof beforeAll === 'function') {
        before(beforeAll);
      }

      if (afterAll && typeof afterAll === 'function') {
        after(afterAll);
      }

      for (let node of subNodes) {
        node.startTest();
      }
    }
    switch (this.useType) {
      case "only":
        describe.only(this.name, testFunction);
        break;

      case "skip":
        describe.skip(this.name, testFunction);
        break;

      default:
        describe(this.name, testFunction);
    }
  }
}

module.exports = File;