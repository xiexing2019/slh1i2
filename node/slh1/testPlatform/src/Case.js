class Case {
  constructor (jsonObj) {
    this.name = jsonObj.name;
    this.useCase = jsonObj.useCase;
    this.type = jsonObj.type;
    this.useType = jsonObj.useType ? jsonObj.useType : "normal";
  }

  startTest () {
    let type = typeof this.useCase;
    let testFunction;
    if (type === 'function') {
      testFunction = this.useCase;

      switch (this.useType) {
        case "only":
          it.only(this.name, testFunction);
          break;
        
        case "skip":
          it.skip(this.name, testFunction);
          break;
  
        default:
          it(this.name, testFunction);
      }
    }else{
      console.log(this.name + 'usecase type error');
    }
  }
}

module.exports = Case;