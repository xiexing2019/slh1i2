'use strict'
const commons = require('../../../lib/common');
const expect = require('chai').expect;
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJsonparam = require('../../help/basiceJsonparam.js');
const salesReqHandler = require('../../help/salesHelp/salesRequestHandler.js');

module.exports = {
	"name": "厂商管理-slh2",
	"type": "describe",
	"before": async () => {
		await commons.loginDo();
		BASICDATA = await getBasicData.getBasicID();
	},
	"after": async () => { },
	"subNodes": [
		{
			"name": "110038_新增厂商",
			"type": "describe",
			"before": async () => { },
			"after": async () => { },
			"subNodes": [
				{
					"name": "新增同名厂商",
					"type": "it",
					"useCase": async function () {
						let result = await commons.callInterface('sf-2241', {
							jsonparam: {
								nameshort: 'Vell'
							}
						});
						expect(result.error, '保存同名厂商出错').to.be.includes('已存在[Vell]名称的[厂商]');

						let qfRes = await commons.callInterface('qf-2241', {
							pk: BASICDATA.dwidVell
						});
						let phone = qfRes.phone;
						if (!phone || phone.length === 0) {
							phone = '139' + commons.getRandomNumStr(8);
							qfRes.action = 'edit';
							qfRes.phone = phone;
							await commons.callInterface('sf-2241', {
								jsonparam: qfRes
							});
						}

						result = await commons.callInterface('sf-2241', {
							jsonparam: {
								nameshort: `cs${commons.getRandomStr(8)}`,
								phone: phone
							}
						});
						expect(result.error, '保存同手机号厂商出错').to.be.includes('已存在[' + phone + ']号码的[厂商]');
					}
				}
			]
		}
	],
}
