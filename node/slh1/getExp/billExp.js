
const format = require('../../data/format');
let billExp = module.exports = {};

/**
 * 根据单据SF请求,拼接ql-1932期望值的变化量
 * @param {object} sfRes 
 * @return {object} {styleid-colorid-sizeid:exp}
 */
billExp.getCurInvListExp = function (sfRes) {
    let exp = {};
    sfRes.params.details.forEach(detail => {
        const key = `${detail.styleid}-${detail.colorid}-${detail.sizeid}`;
        switch (sfRes.params.interfaceid) {
            case 'sf-14212-1'://采购入库
                exp[key] = format.dataFormat(detail, `invnum=num;total;availStockNum=num`);
                break;
            case 'sf-2324-1'://要货调拨保存
                exp[key] = format.dataFormat(detail, `onroadnum=num;availStockNum=num`);
                break;

            default:
                break;
        };
    });
    return exp;
};