const common = require('../../lib/common');
const invCheckReqHandler = require('../help/invCheckHelp/invCheckHelp');

//定期做盘点处理
describe('盘点管理', function () {
    this.timeout(30000);
    before(async () => {
        await common.loginDo({ logid: '200' });
        // BASICDATA = await getBasicData.getBasicID();
    });
    it('定期做全盘处理', async function () {
        //全盘处理
        await invCheckReqHandler.processCheck({ checkall: 1 });
    });
});
