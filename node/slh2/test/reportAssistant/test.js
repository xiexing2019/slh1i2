const common = require('../../../lib/common');
const ecBi = require('../../../reqHandler/reportAssistant/bi');
const ecMdm = require('../../../reqHandler/slh2/mdm');
const nqMain = require('../../help/nqMain');


//报表助手统计的是下游分销商的数据
describe('报表助手冒泡-slh2', async function () {
    this.timeout(30000);
    let subFran = '分销商zPMh';
    before(async function () {
        await nqMain.login();
    });
    it.skip('个人中心', async function () {
        //ec-mdm-user-login 通过登陆接口返回的信息即可获取相关的信息
        // let info = await ecMdm.getDtoById({ id:, });
        // console.log(`info=${JSON.stringify(info)}`);

    });
    it.skip('获取个人信息', async function () {
        let info = await ecMdm.getDtoById();
        console.log(`info=${JSON.stringify(info)}`);

    });
    it.skip('门店列表', async function () {
        let shopList = await ecMdm.getShopList();
        // console.log(`shopList=${JSON.stringify(shopList)}`);
        expect(res.result.data.rows.length).to.above(0);

    });


    it('门店分析首页', async function () {
        const res = await ecBi.analysisByShopHomePage({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data).to.includes.keys('indexQuota', 'statQuota', 'branchRankList');

    });
    it('门店商品分析', async function () {
        const res = await ecBi.commodityAnalysisByShop({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });

    it('门店销售分析', async function () {
        //先查个分销商id
        let subFranList = await ecMdm.findSubFran({ nameLike: subFran }).then(res => res.result.data.rows[0]);
        // console.log(`subFranList=${JSON.stringify(subFranList)}`);

        const res = await ecBi.salesAnalysisByShop({ branchId: subFranList.tenantId, timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data).to.includes.keys('jointRatio', 'salesTagMoney', 'unitPrice', 'discountRate', 'backRatio');


    });

    it('门店会员分析', async function () {
        const res = await ecBi.memberAnalysisByShop({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data).to.includes.keys('memAcmtCount', 'memSalesRatio', 'memWeekAdd', 'memMonthAdd', 'rows');

    });
    it('时段分析', async function () {
        const res = await ecBi.timeSlotAnalysisByShop({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('支付统计', async function () {
        const res = await ecBi.payAnalysisByShop({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('库存结构', async function () {
        const res = await ecBi.invAnalysis();
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data).to.includes.keys('sumBySeason', 'rows', 'sumByYear');


    });
    it('类别分析', async function () {
        const res = await ecBi.spuAnalysisByClass({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('导购排名', async function () {
        const res = await ecBi.ownerSalesRankAnalysis({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('品牌分析', async function () {
        const res = await ecBi.brandSpuAnalysis({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('区域分析', async function () {
        const res = await ecBi.salesAnalysisByArea({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);

    });
    it('整体分析', async function () {
        const res = await ecBi.memberGlobalAnalysis({ timeKind: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data).to.includes.keys('memSalesInfo', 'totalSalesInfo', 'memSalesDaily', 'globalSalesDaily');


    });


});