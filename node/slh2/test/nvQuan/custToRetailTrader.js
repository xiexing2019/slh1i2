const common = require('../../../lib/common');
const format = require('../../../data/format');
const basiceJson = require('../../../slh1/help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData.js');
const reqHandler = require('../../../slh1/help/reqHandlerHelp');
const basicInfoReqHandler = require('../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const custReqHandler = require('../../../reqHandler/shopDiary/customerReqHandler');
// const salesReqHandler = require('../../../help/salesHelp/salesRequestHandler');
const nqLogin = require('../../../slh2/help/nqMain');
const slh2 = require('../../../reqHandler/slh2/index');

//http://jira.hzdlsoft.com:7082/browse/SLHSEC-7307
describe('客户转分销商-slh2', function () {
	this.timeout(30000);

	describe('客户转分销商', function () {
		let cust, sfSalesRes, styleSuperior, qlSalesBefore, qlSalesAfter, accountBefore, accountAfter;
		//新增客户，销售开单，然后转分销商
		// 分销商详情，分销商列表
		// 分销商登录，货品查询
		before(async () => {
			//上游登录
			// 新增客户，开单
			// 销售开单按批次查
			await nqLogin.login();
			BASICDATA = await getBasicData.getBasicID();
			//二代新增供应商和新增客户用ec-mdm-org-saveFull
			cust = await slh2.mdm.addSupplier({
				jsonParam: basiceJson.addCustMdmJson({
					name: 'cust' + common.getRandomStr(6)
				})
			});
			let salesJson = basiceJson.salesJson();
			salesJson.dwid = cust.result.data.val;
			//欠款
			[salesJson.card, salesJson.cash, salesJson.remit] = [0, 0, 0];
			//销售开单
			sfSalesRes = await common.editBilling(salesJson);
			styleSuperior = await await common.fetchMatInfo(sfSalesRes.params.details[0].styleid);
			// console.log(`styleSuperior : ${JSON.stringify(styleSuperior)}`);
			qlSalesBefore = await reqHandler.qlIFCHandler({
				interfaceid: 'ql-142201',
				prodate1: common.getCurrentDate(),
				prodate2: common.getCurrentDate(),
			});
			//客户账款
			accountBefore = await reqHandler.qlIFCHandler({
				interfaceid: 'ql-1350',
				dwid: cust.result.data.val,
			})
		});
		describe('主流程', function () {
			// console.log(`cust : ${JSON.stringify(cust)}`);
			let branchCode, saveRes, retailTraderInfo, retailTraderList;
			before('客户转分销商', async () => {
				branchCode = cust.params.jsonParam.org.name.slice(4);
				let json = basiceJson.addRetailTraderJson({
					branchCode,
					name: cust.params.jsonParam.org.name,
					parentId: LOGINDATA.branchId, //
				});
				json['1'].id = cust.result.data.val;
				//客户转分销商
				saveRes = await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: json,
				});
				// console.log(`saveRes : ${JSON.stringify(saveRes)}`);
				//分销商详细信息
				retailTraderInfo = await common.apiDo({
					apiKey: 'ec-mdm-org-getFull', //查询组织机构完整信息
					jsonParam: {
						'id': saveRes.result.data.val,
					},
				});
				//分销商列表信息
				retailTraderList = await common.apiDo({
					apiKey: 'ec-mdm-org-fran-findSubFran',
					jsonParam: {
						orderBy: 'updatedDate desc',
						pageNo: 1,
						pageSize: 20,
						id: saveRes.result.data.val
					},
				});
				// console.log(`retailTraderList : ${JSON.stringify(retailTraderList)}`);
				qlSalesAfter = await reqHandler.qlIFCHandler({
					interfaceid: 'ql-142201',
					prodate1: common.getCurrentDate(),
					prodate2: common.getCurrentDate(),
				});
				accountAfter = await reqHandler.qlIFCHandler({
					interfaceid: 'ql-1350',
					dwid: cust.result.data.val,
				});
			});
			it('分销商详情验证', async function () {
				let actual = format.dataFormat(retailTraderInfo.result.data, 'branchCode=1024.branchCode;parentId=org.parentId;capability=org.capability');
				let expect = format.dataFormat(saveRes.params.jsonParam, 'branchCode=1024.branchCode;parentId=org.parentId;capability=org.capability');
				common.isApproximatelyArrayAssert(expect, actual);
			});
			it('分销商列表验证', async function () {
				common.isApproximatelyEqualAssert(saveRes.params.jsonParam, retailTraderList.result.data.rows[0]);
			});
			it('销售按批次查没变', async function () {
				common.isApproximatelyArrayAssert(qlSalesBefore.result.dataList, qlSalesAfter.result.dataList);
			});
			it('账款没变', async function () {
				common.isApproximatelyEqualAssert(accountBefore.result, accountAfter.result);
			});
			it('相同分销商代码验证', async function () {
				//分销商列表
				let retailTraderList = await common.apiDo({
					apiKey: 'ec-mdm-org-fran-findSubFran',
					jsonParam: {
						orderBy: 'updatedDate desc',
						pageNo: 1,
						pageSize: 20,
					},
				});
				let json = basiceJson.addRetailTraderJson({
					branchCode: retailTraderList.result.data.rows[0].branchCode, //铺货用例
					name: '相同分销商代码' + common.getRandomStr(3),
					parentId: LOGINDATA.branchId,
				});
				json['1'].id = cust.result.data.val;
				let sfRes = await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: json,
					check: false,
				});
				expect(sfRes.result).to.includes({
					msg: `分销商代码[${retailTraderList.result.data.rows[0].branchCode}]已分配给[${retailTraderList.result.data.rows[0].nameShort}]，请使用其他编码`
				});
			});
			describe('分销商登录', function () {
				let styleList, styleInfo, purchaseList, invCurrent, brandList, styleClassList, supplierAccount;
				before('下游分销商登录', async function () {
					await nqLogin.login({
						logid: `${branchCode}000`
					});
					//货品查询
					styleList = await reqHandler.qlIFCHandler({
						interfaceid: 'ql-15110',
						// styleid: ,
					});
					styleInfo = await common.fetchMatInfo(styleList.result.dataList[0].id);
					//采购入库按批次查
					purchaseList = await reqHandler.qlIFCHandler({
						interfaceid: 'ql-22301',
						prodate1: common.getCurrentDate(),
						prodate2: common.getCurrentDate(),
					});
					//当前库存
					invCurrent = await reqHandler.qlIFCHandler({
						interfaceid: 'ql-1932'
					});
					//品牌列
					brandList = await basicInfoReqHandler.getDictList({
						typeid: 606 //品牌
					});
					// console.log(`brandList : ${JSON.stringify(brandList)}`);
					//类别
					styleClassList = await basicInfoReqHandler.getClassList();
					// console.log(`styleClassList : ${JSON.stringify(styleClassList)}`);
					supplierAccount = await reqHandler.qlIFCHandler({
						interfaceid: 'ql-15007'
					});
				});
				it('货品查询有这个款号', async function () {
					expect(styleList.result.dataList.length).to.equal(1);
					// common.isFieldsEqualAssert(style,);
					// common.isApproximatelyEqualAssert(style, styleList.result.dataList[0], ['']);
				});
				it('货品详情验证', async function () {
					// console.log(`styleSuperior : ${JSON.stringify(styleSuperior)}`);
					// console.log(`styleInfo : ${JSON.stringify(styleInfo)}`);
					// console.log(styleSuperior.dwid);
					// console.log(styleInfo.dwid);
					common.isApproximatelyEqualAssert(styleSuperior, styleInfo, ['purprice', 'shopid', 'dwid', 'show_dwid', 'marketdate', 'stdprice1', 'stdprice2', 'stdprice3', 'show_shopid']);
					common.isApproximatelyEqualAssert({
						purprice: common.mul(sfSalesRes.params.details[0].price, sfSalesRes.params.details[0].discount),
						show_dwid: '',
						dwid: '',
					}, styleInfo);

				});
				it('字典', async function () {
					let act = common.takeWhile(brandList.result.dataList, obj => obj.name == styleSuperior.show_brandid);
					expect(act.length).to.equal(1);
					let act1 = common.takeWhile(styleClassList.result.dataList, obj => obj.name == styleSuperior.show_classid);
					expect(act1.length).to.equal(1);
				})
				it('采购-按批次查为空', async function () {
					expect(purchaseList.result.dataList.length).to.equal(0);
				});
				it('当前库存为空', async function () {
					expect(invCurrent.result.dataList.length).to.equal(0);
				});
				it('账款为空', async function () {
					expect(supplierAccount.result.dataList.length).to.equal(0);
				});
			});
		});

		describe('异常流', function () {
			before(async () => {
				//上游登录
				await nqLogin.login();
			})
			it('同一个客户重复转分销商', async function () {
				let branchCode = common.getRandomStr(6);
				let json = basiceJson.addRetailTraderJson({
					branchCode,
					name: cust.params.jsonParam.org.name,
					parentId: LOGINDATA.branchId, //
				});
				json['1'].id = cust.result.data.val;
				//客户转分销商
				let saveRes = await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: json,
					check: false,
				});
				expect(saveRes.result).to.includes({
					msg: `已存在[${cust.params.jsonParam.org.name}]名字的[加盟商]`,
				});
			});
			it('修改客户后再转分销商', async function () {
				let custInfo = await slh2.mdm.getSupplierInfo({
					id: cust.result.data.val
				});
				custInfo.result.data.org.name = '修改客户' + common.getRandomStr(4);
				cust = await slh2.mdm.addSupplier({
					jsonParam: custInfo.result.data
				});
				let jsonRetailTrader = basiceJson.addRetailTraderJson({
					branchCode: cust.params.jsonParam.org.name.slice(4),
					name: cust.params.jsonParam.org.name,
					parentId: LOGINDATA.branchId,
				});
				jsonRetailTrader['1'].id = cust.result.data.val;
				let saveRes = await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: jsonRetailTrader,
					check: false,
				});
				expect(saveRes.result).to.includes({
					msg: '已经转换过分销商，请勿重复操作'
				});
			});
		});
	});
	describe.skip('客户转分销商，货品价格验证', function () {

	});
});
async function priceCheck(value1, value2) {
	//开第一个参数就按第一个参数来，关闭就按第二个参数
	// 第二个参数没开，价格为0
	await common.setGlobalParam('sync_agent_stdprice1_as_tagprice', value1); //推送总部吊牌价到分销商销售价1
	await common.setGlobalParam('busiarea_tagprice_update', value2); //商圈同步价格1，非省代客户价格1为吊牌价
	let cust = await slh2.mdm.addSupplier({
		jsonParam: basiceJson.addCustMdmJson({
			name: 'cust' + common.getRandomStr(6)
		}),
	});
	let salesJson = basiceJson.salesJson();
	salesJson.dwid = cust.result.data.val;
	//欠款
	[salesJson.card, salesJson.cash, salesJson.remit] = [0, 0, 0];
	//销售开单
	sfSalesRes = await common.editBilling(salesJson);
	branchCode = cust.params.jsonParam.org.name.slice(4);
	let json = basiceJson.addRetailTraderJson({
		branchCode,
		name: cust.params.jsonParam.org.name,
		parentId: LOGINDATA.branchId, //
	});
	json['1'].id = cust.result.data.val;
	//客户转分销商
	saveRes = await common.apiDo({
		apiKey: 'ec-mdm-org-saveFull',
		jsonParam: json,
	});
	await await nqLogin.login({
		logid: `${branchCode}000`
	});
};
