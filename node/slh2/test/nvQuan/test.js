const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const basiceJson = require('../../../slh1/help/basiceJsonparam');
const mainReqHandler = require('../../../slh1/help/basicInfoHelp/mainDataReqHandler');
const basicInfoReqHandler = require('../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData');
const reqHandler = require('../../../slh1/help/reqHandlerHelp');
const purReqHandler = require('../../../slh1/help/purchaseHelp/purRequestHandler');
const nqLogin = require('../../../slh2/help/nqMain');
//目前女权就是在cs3d2跑的
const epids = {
	'ytt-女权新2': '12985',
	'nq001': '13061',
	'nq002': '13085',
	'nq003': '13089',
	'autotestNv': '14449', //cs3d2
	'autotestNvd3': '22037' //cs3d3
};
const login = async (params = {}) => {
	if (params.asName) {
		params.epid = epids[params.asName];
		delete params.asName;
	} else {
		switch (caps.name) {
			case 'slh2_test':
				params.epid = epids['ytt-女权新2'];
				break;
			case 'cs3d2':
				params.epid = epids['autotestNv'];
				break;
			case 'cs3d3':
				params.epid = epids['autotestNvd3'];
				break;
			default:
				break;
		}

	};

	await common.loginDo(params);
	await getBasicData.getBankAccountIds(); //银行账户id
};

/**
 *  女权贵族相关用例
 *
 *
 */

describe('女权贵族-slh2', function () {
	this.timeout(30000);
	let branchCode = 'zPMh', //common.getRandomStr(4), //'o43h',
		upperLoginInfo = {}, //上游登陆信息
		retailTraderInfo = {}, //分销商信息
		rtShopId, //分销商门店id
		custInfo = {}, //
		custId = '710506', // 651115 '651099'710506 801534
		companyInfo = {},
		companyId = '710510', //651117 厂商id = '651101'   710510 801542
		styleInfo = {},
		styleId = '', //148597
		baseData = {}, //基本信息
		otherBranchCode = ''; //分销商B这里也有改  第263行

	before(async () => {
		//总部-总经理登陆
		// await login();
		await nqLogin.login();
		upperLoginInfo = LOGINDATA; //上游登陆信息
		// console.log(`LOGINDATA = ${JSON.stringify(LOGINDATA)}`);

		//特殊货品列表 jsonparam中需要判断是否特殊货品
		BASICDATA.specGoodsList = await getBasicData.getSpecGoodsList().then((res) => res.dataList);
		BASICDATA.styleid00000 = BASICDATA.specGoodsList.find((obj) => obj.name == '抹零').id;

		//新增货品+获取货品信息
		let styleRes = await editStyle();
		styleId = styleRes.pk;
		styleInfo = await common.fetchMatInfo(styleId); //
		// console.log(`styleInfo = ${JSON.stringify(styleInfo)}`);
	});

	after(async () => {
		console.log(`branchCode=${branchCode},custId=${custId},companyId=${companyId}`);
		//删除组织机构
		// let res = await common.apiDo({
		// 	apiKey: 'ec-mdm-org-delete',
		// 	jsonParam: {
		// 		id: retailTraderInfo.params.jsonParam.id,
		// 	},
		// });
		// console.log(`res = ${JSON.stringify(res)}`);
	});
	//parentId现在默认当前登录门店
	//若用nq001 帐套，需要确认下门店是否需要修改，现在使用默认门店
	describe.skip('300001.上下游绑定', function () {
		let saveRes = {};
		it('1.新增分销商', async () => {
			saveRes = await common.apiDo({
				apiKey: 'ec-mdm-org-saveFull',
				jsonParam: basiceJson.addRetailTraderJson({
					branchCode,
					name: `分销商${branchCode}`,
					parentId: LOGINDATA.branchId, //nq001 加盟商 '425021'
				}),
			});
			expect(saveRes.result).to.includes({
				msg: '保存成功!'
			});
		});
		it('2.查询分销商详细信息', async () => {
			retailTraderInfo = await common.apiDo({
				apiKey: 'ec-mdm-org-getFull', //查询组织机构完整信息
				jsonParam: {
					'id': saveRes.result.data.val,
				},
			});
			let actual = format.dataFormat(retailTraderInfo.result.data, 'branchCode=1024.branchCode;parentId=org.parentId;capability=org.capability');
			let expect = format.dataFormat(saveRes.params.jsonParam, 'branchCode=1024.branchCode;parentId=org.parentId;capability=org.capability');
			common.isApproximatelyArrayAssert(expect, actual);
		});
		//优先级低,后续补充
		it('3.查询当前小租户直属分销商列表', async () => {
			let qlParams = {
				apiKey: 'ec-mdm-org-fran-findSubFran',
				jsonParam: {
					orderBy: 'updatedDate desc',
					pageNo: 1,
					pageSize: 20,
					//nameLike: saveRes.params.jsonParam.org.name,
				},
			};
			let res = await common.apiDo(qlParams);
			for (let i = 0; i < res.result.data.rows.length; i++) {
				if (saveRes.params.jsonParam.org.name != res.result.data.rows[i].nameShort) {
					otherBranchCode = res.result.data.rows[i].branchCode;
					break;
				}
			};
			//console.log(`otherRetailTraderName : ${JSON.stringify(otherRetailTraderName)}`);

			qlParams.jsonParam.nameLike = saveRes.params.jsonParam.org.name;
			res = await common.apiDo(qlParams);
			// let res = await common.apiDo({
			// 	apiKey: 'ec-mdm-org-fran-findSubFran',
			// 	jsonParam: {
			// 		orderBy: 'updatedDate desc',
			// 		pageNo: 1,
			// 		pageSize: 20,
			// 		nameLike: saveRes.params.jsonParam.org.name,
			// 	},
			// });
			expect(saveRes.params.jsonParam.org.name, '分销商查询有误').to.equal(res.result.data.rows[0].nameShort);
		});
		//分销商总经理登陆-新增门店
		it('4.新增分销商门店A', async () => {
			await login({
				logid: `${branchCode}000`, //分销商总经理工号=分销商编号+000
			});

			let res = await common.apiDo({
				apiKey: 'ec-mdm-org-saveFull',
				jsonParam: basiceJson.addShopJson({
					name: `${branchCode}_shop1`
				}),
			});
			// console.log(`res = ${JSON.stringify(res)}`);
			rtShopId = res.result.data.val;
		});
		//分销商新门店-新增工号
		it('5.新增分销商员工工号', async () => {
			await login({
				logid: `${branchCode}000`, //分销商总经理工号=分销商编号+000
			});

			let roleIds = await common.getRoleList().then((res) => {
				let id;
				for (let i = 0, length = res.result.data.rows.length; i < length; i++) {
					if (res.result.data.rows[i].code == 'gm') { //总经理
						id = res.result.data.rows[i].id;
						break;
					};
				};
				return id;
			}); //获取角色列表

			let res = await common.apiDo({
				apiKey: 'ec-mdm-staff-save',
				jsonParam: basiceJson.addStaffJson({
					code: `${branchCode}Shop1000`,
					name: `分销商${branchCode}Shop1总经理`,
					shopId: rtShopId,
					roleIds
				}),
			});
			expect(res.result).to.includes({
				msg: '新增成功!',
			});
		});
		//一个分销商只能关联一个客户,不能重复关联
		it('6.上游新增客户,绑定分销商门店A', async () => {
			await login();
			let res = await common.apiDo({
				apiKey: 'ec-mdm-org-saveFull',
				jsonParam: basiceJson.addCustMdmJson({
					name: `cust${branchCode}${common.getRandomStr(4)}`,
					relFranId: rtShopId, //,
				}),
			});
			expect(res.result).to.includes({
				msg: '保存成功!'
			});

			custId = res.result.data.val;

			custInfo = await common.apiDo({
				apiKey: 'ec-mdm-org-getFull',
				jsonParam: {
					id: res.result.data.val,
				},
			});
			expect(custInfo.result.data['1'].relId, '上游绑定分销商门店A').to.equal(rtShopId);
			// console.log(`res.params : ${JSON.stringify(res.params)}`);
			// console.log(`custInfo.result : ${JSON.stringify(custInfo.result)}`);
			//common.isApproximatelyEqualAssert(res.params, custInfo.result);

		});
		//分销商总经理登陆-新增厂商
		it('7.下游门店A新增厂商,关联上游门店', async () => {
			await login({
				logid: `${branchCode}Shop1000`,
			});

			let res = await common.apiDo({
				apiKey: 'ec-mdm-org-saveFull',
				jsonParam: basiceJson.addCompanyJson({
					name: `厂商${branchCode}`,
					relFranId: upperLoginInfo.invid,
				}),
			});
			expect(res.result).to.includes({
				msg: '保存成功!'
			});

			companyId = res.result.data.val;

		});
	});

	describe('300002.铺货主流程', function () {
		let salesQfRes = {},
			purQfRes = {};
		before(async () => {
			//总部-总经理登陆
			await nqLogin.login();
			//获取基本信息 需要获取上下游绑定后的信息
			//暂时未使用
			// baseData = await common.callInterface('cs-getBaseDataByLogin', {
			// 	epid: LOGINDATA.epid,
			// });
		});
		it('1.上游未发货,下游查看款号A', async () => {
			//分销商门店A查看
			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
			//未发货状态下检查货品信息
			let qlRes = await common.callInterface('ql-15110', format.qlParamsFormat({
				styleid: styleInfo.pk,
			}));
			expect(qlRes.count).to.equal('0');

			//默认门店查看
			await nqLogin.login({
				logid: `${branchCode}000`, //分销商总经理工号=分销商编号+000
			});
			qlRes = await common.callInterface('ql-15110', format.qlParamsFormat({
				styleid: styleInfo.pk,
			}));
			expect(qlRes.count).to.equal('0');
		});
		it('2.上游铺货', async () => {
			await nqLogin.login();
			//现在铺货后下游操作不会对上游的单据产生影响
			let sfRes = await common.editBilling(salesJson({
				dwid: custId,
				styleInfo
			}));

			salesQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk,
			});

		});
		it('3.下游查看分销单_单据详情', async () => {
			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.params.pk,
			});
		});
		it('4.下游按分销入库查询_未入库', async () => {
			let params = format.qlParamsFormat({
				clientid: companyId,
				billno1: salesQfRes.result.billno,
				billno2: salesQfRes.result.billno,
				pagesize: 15
			}, true);
			let qlRes = await common.callInterface('ql-142202', params); //采购入库-按分销入库
			let exp = purReqHandler.getPurByDistributionListExp(salesQfRes.result, purQfRes.result)
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('5.分销入库', async () => {
			let sfRes = await editBillingByDistribution(purQfRes.result);

			let qfRes = await purReqHandler.purQueryBilling(sfRes.result.pk);

			let exp = purReqHandler.getPurMainSearchListExp(qfRes.result);

			let param = format.qlParamsFormat({
				id1: sfRes.result.billno,
				id2: sfRes.result.billno,
				shopid: qfRes.result.invid,
				invalidflag: 0,
				flag: 1,
				dwid: qfRes.result.dwid,
				deliver: qfRes.result.deliver,
				remark: qfRes.result.remark,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('6.下游使用款号A开单', async () => {
			//查客户区域
			let areaList = await basicInfoReqHandler.getDictList({ typeid: '1000' });
			if (areaList.result.dataList == 0) {
				//新增客户区域没有返回id
				await reqHandler.sfIFCHandler({ interfaceid: 'sf-1413-1', jsonparam: { name: `客户区域` + common.getRandomStr(5), } });
				areaList = await basicInfoReqHandler.getDictList({ typeid: '1000' });
			};
			//新增客户
			let cust = await mainReqHandler.saveCust({
				nameshort: 'cust' + common.getRandomStr(6),
				areaid: areaList.result.dataList[0].sid,
			});
			let json = salesJson({
				dwid: cust.result.val,
				styleInfo,
			}, (obj) => {
				obj.cash = 400;
				obj.details.forEach((detail) => detail.num = 1);
			});
			await common.editBilling(json);
		});
		it('7.分销入库后,下游查看款号', async () => {
			//分销商A门店A查看
			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
			let qlRes = await common.callInterface('ql-15110', format.qlParamsFormat({
				styleid: styleInfo.pk,
			}));
			expect(qlRes.count).to.equal('1');

			//分销商B默认门店查看
			await nqLogin.login({
				logid: `N92o6H000`, //分销商总经理工号=分销商编号+000
			});
			qlRes = await common.callInterface('ql-15110', format.qlParamsFormat({
				styleid: styleInfo.pk,
			}));
			expect(qlRes.count).to.equal('0');
		});
		it('8.下游按分销入库查询_全部入库', async () => {
			//分销商A门店A登录
			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
			let params = format.qlParamsFormat({
				clientid: companyId,
				billno1: salesQfRes.result.billno,
				billno2: salesQfRes.result.billno,
				pagesize: 15
			}, true);
			let qlRes = await common.callInterface('ql-142202', params); //采购入库-按分销入库
			expect(qlRes.count).to.equal('0');
		});
		it('300015.下游款号入库后检查', async () => {
			companyInfo = await common.callInterface('qf-2241', {
				pk: companyId,
			}); //获取厂商信息

			let styleInfo2 = await common.fetchMatInfo(styleId); //下游货品详细信息

			let exp = _.cloneDeep(styleInfo);
			exp.purprice = purQfRes.result.details[0].price; //进货价=采购入库中最后入库的款号单价
			exp.stdprice1 = exp.stdprice2 = exp.stdprice3 = exp.stdprice4 = exp.stdprice5 = 0;
			//系统参数：进货入库自动更新款号厂商——更新，那么厂商为入库的厂商
			exp.dwid = companyId;
			exp.show_dwid = companyInfo.nameshort;
			exp.shopid = LOGINDATA.invid;
			exp.show_shopid = LOGINDATA.invname;
			exp.purpricedetals = [{
				dwid: companyId,
				show_dwid: companyInfo.nameshort,
				price: exp.purprice,
			}];
			// console.log(`exp : ${JSON.stringify(exp)}`);
			// console.log(`styleInfo2 : ${JSON.stringify(styleInfo2)}`);
			common.isApproximatelyEqualAssert(exp, styleInfo2);
		});
	});

	describe('分销商是否允许建款', function () {
		let goods, junior;
		const typeIds = { 606: '品牌', '601': '颜色', 605: '尺码', 604: '尺码组', 631: '颜色组' };
		before(async () => {
			//分销商登录
			await nqLogin.login({
				logid: `${branchCode}000`,
			});
			junior = _.cloneDeep(LOGINDATA);
			//分销商新增货品
			goods = await editStyle();
			goodsInfo = await common.fetchMatInfo(goods.result.val);
			// console.log(`goods=${JSON.stringify(goods)}`);
		});
		it('分销商新增颜色尺码品牌', async () => {
			for (const key of Object.keys(typeIds)) {
				let dictSaveRes = await basicInfoReqHandler.saveDict({
					name: common.getRandomStr(4),
					typeid: key,
					check: false,
				});
				expect(dictSaveRes.result, `不允许建款的分销商新增字典成功`).to.includes({ error: '不允许操作继承的字典' });
			};
		});
		it('分销商新增款号--厂商是上游', async function () {
			//新增厂商
			let json = Object.assign(goods.params, { dwid: companyId });
			let updateRes = await basicInfoReqHandler.editStyle({ jsonparam: json, check: false });
			// console.log(`updata=${JSON.stringify(updata)}`);
			expect(updateRes.result, `下游分销商新建厂商是上游的款号，新增成功`).to.includes({ error: '不能将本地创建的款号的厂商设置为上游供应商' });
		});
		it('分销商采购入库', async function () {
			//分销商新增
			let json = salesJson({
				interfaceid: 'sf-14212-1',
				dwid: companyId, //厂商id
				styleInfo,
			});
			//修改第二条明细为分销商自建的款号
			json.details[1].styleid = goodsInfo.pk;
			let purRes = await common.editBilling(json, false);
			expect(purRes.result, `采购入库选择上游，款号是下游新建，采购入库成功`).to.includes({ error: `不允许将下游创建的款号操作到上游厂商,${goods.params.code}` });
		});
		describe('允许建款', function () {
			let juniorInfo;
			before(async () => {
				//总部-总经理登陆
				await nqLogin.login();
				//修改分销商可以建款
				juniorInfo = await common.apiDo({
					apiKey: 'ec-mdm-org-getFull', //查询组织机构完整信息
					jsonParam: {
						'id': junior.depid,
					},
				});
				//修改分销商为允许建款
				juniorInfo.result.data[4].createSpuFlag = 1;
				// console.log(`juniorInfo=${JSON.stringify(juniorInfo)}`);
				await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: juniorInfo.result.data,
				});
				//分销商登录
				await nqLogin.login({
					logid: `${branchCode}000`,
				});
			});
			after(async () => {
				//总部-总经理登陆
				await nqLogin.login();
				juniorInfo.result.data[4].createSpuFlag = 0;
				await common.apiDo({
					apiKey: 'ec-mdm-org-saveFull',
					jsonParam: juniorInfo.result.data,
				});
			});
			it('分销商新增颜色尺码品牌', async () => {
				for (const key of Object.keys(typeIds)) {
					let dictSaveRes = await basicInfoReqHandler.saveDict({
						name: common.getRandomStr(4),
						typeid: key,
						check: false,
					});
					let dictListInfo = await basicInfoReqHandler.getDictList({
						typeid: key,
					}).then(res => res.result.dataList.find(obj => obj.id == dictSaveRes.result.pk));
					expect(dictListInfo, `新增的字典:${typeIds.key}${key}在列表查询中查不到`).not.to.be.undefined;
					common.isApproximatelyEqualAssert(dictSaveRes.params.jsonparam, dictListInfo);
				};
			});
			it('分销商新增厂商不是上游的货品', async function () {
				//新增厂商
				let json = Object.assign(goods.params, { dwid: companyId });
				let updateRes = await basicInfoReqHandler.editStyle({ jsonparam: json, check: false });
				// console.log(`updata=${JSON.stringify(updata)}`);
				expect(updateRes.result, `下游分销商新建厂商是上游的货品，新增成功`).to.includes({ error: '不能将本地创建的款号的厂商设置为上游供应商' });
			});
			it('分销商采购入库', async function () {
				//分销商新增
				let json = salesJson({
					interfaceid: 'sf-14212-1',
					dwid: companyId, //厂商id
					styleInfo,
				});
				//修改第二条明细为分销商自建的款号
				json.details[1].styleid = goodsInfo.pk;
				let purRes = await common.editBilling(json, false);
				expect(purRes.result, `采购入库选择上游，款号选择下游新建，采购入库成功`).to.includes({ error: `不允许将下游创建的款号操作到上游厂商,${goods.params.code}` });
			});
		});

	});



	describe.skip('300010.按分销入库—特殊货品', async () => {
		let salesQfRes = {}, //上游销售单详情
			purQfResOrign = {},
			purQfRes = {}, //分销单详情
			purRes;
		before(async () => {
			await nqLogin.login();
			let sfRes = await common.editBilling(salesJson({
				dwid: custId,
				styleInfo
			}), (json) => {
				json.details.push({
					"rem": "Spec1",
					"styleid": BASICDATA.styleid00000,
					"price": 20,
					"num": -1,
					"colorid": "0",
					"sizeid": "0"
				});
			});
			salesQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk,
			});
			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
		});
		it('1.下游部分入库', async () => {
			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			purQfResOrign = _.cloneDeep(purQfRes);
			purQfRes.result.cash = common.sub(purQfRes.result.cash, 2000);
			purQfRes.result.details.forEach((detail) => {
				if (detail.styleid != BASICDATA.styleid00000) detail.num = common.sub(detail.num, 5);
			});

			purRes = await editBillingByDistribution(purQfRes.result); //部分入库

			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			let exp = purReqHandler.getPurByDistributionBillExp(purRes.params);
			common.isApproximatelyEqualAssert(exp, purQfRes.result);
		});
		it('2.检查特殊货品', async () => {
			purQfRes.result.details.forEach((obj) => {
				if (obj.styleid == BASICDATA.styleid00000) throw new Error(`部分入库后,分销单中显示特殊货品`);
			});
		});
		it('300021.作废带特殊货品的入库单', async () => {
			await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancel-pured-bill',
				pk: purRes.result.pk,
			});

			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			common.isApproximatelyEqualAssert(purQfResOrign.result, purQfRes.result, ['orderversion', 'version']);
		});
	});

	//分销入库现在只能全部入库
	describe.skip('300014.按分销入库_状态检查', function () {
		let salesQfRes = {}, //上游销售单详情
			purQfResOrign = {},
			purQfRes = {}, //分销单详情
			purSfRes1 = {}, //入库单1
			purSfRes2 = {}; //入库单2
		before(async () => {
			await nqLogin.login();
			let sfRes = await common.editBilling(salesJson({
				dwid: custId,
				styleInfo
			}));
			salesQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-14211-1',
				pk: sfRes.result.pk,
			});

			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
		});
		it('1.部分入库', async () => {
			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			purQfResOrign = _.cloneDeep(purQfRes);
			purQfRes.result.cash = common.sub(purQfRes.result.cash, 2000);
			purQfRes.result.details.forEach((detail) => detail.num = common.sub(detail.num, 5));

			purSfRes1 = await editBillingByDistribution(purQfRes.result); //部分入库

			//验证分销单
			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			let exp = purReqHandler.getPurByDistributionBillExp(purSfRes1.params);
			common.isApproximatelyEqualAssert(exp, purQfRes.result);
		});
		it('300012.1.验证部分入库生成的采购单', async () => {
			let qfRes = await purReqHandler.purQueryBilling(purSfRes1.result.pk); //获取入库单详情
			common.isApproximatelyEqualAssert(purSfRes1.params, qfRes.result, ['actualpay']);

			let exp = purReqHandler.getPurMainSearchListExp(qfRes.result);
			let param = format.qlParamsFormat({
				id1: purSfRes1.result.billno,
				id2: purSfRes1.result.billno,
				shopid: qfRes.result.invid,
				invalidflag: 0,
				flag: 1,
				dwid: qfRes.result.dwid,
				deliver: qfRes.result.deliver,
				remark: qfRes.result.remark,
			}, true);
			let qlRes = await common.callInterface('ql-22301', param); //采购入库-按批次查
			common.isApproximatelyEqualAssert(exp, qlRes.dataList[0]);
		});
		it('2.再次部分入库->全部入库', async () => {
			purSfRes2 = await editBillingByDistribution(purQfRes.result); //部分入库剩余内容

			let params = format.qlParamsFormat({
				clientid: companyId,
				billno1: salesQfRes.result.billno,
				billno2: salesQfRes.result.billno,
				pagesize: 15
			}, true);
			let qlRes = await common.callInterface('ql-142202', params); //采购入库-按分销入库
			expect(qlRes.count).to.equal('0');
		});
		it('3.作废入库单2->部分入库', async () => {
			//作废第二张入库单
			await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancel-pured-bill',
				pk: purSfRes2.result.pk,
			});

			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			let exp = purReqHandler.getPurByDistributionBillExp(purSfRes1.params); //变回部分入库
			common.isApproximatelyEqualAssert(exp, purQfRes.result);
		});
		it('4.作废入库单1->未入库', async () => {
			//作废第一张入库单
			await reqHandler.csIFCHandler({
				interfaceid: 'cs-cancel-pured-bill',
				pk: purSfRes1.result.pk,
			});
			purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: salesQfRes.result.pk,
			});
			common.isApproximatelyEqualAssert(purQfResOrign.result, purQfRes.result, ['orderversion', 'version']); //变回未入库
		});
	});

	describe.skip('修改单据', function () {
		//调试时顺序不能换,可以skip
		describe('下游入库后,上游修改/作废销售单', function () {
			let sfRes1, sfRes2, cancelQfRes;
			before(async () => {
				await nqLogin.login();
				sfRes1 = await common.editBilling(salesJson({
					dwid: custId,
					styleInfo
				}));

				sfRes2 = await common.editBilling(salesJson({
					dwid: custId,
					styleInfo
				}));

				let sfRes3 = await common.editBilling(salesJson({
					dwid: custId,
					styleInfo
				}));
				await reqHandler.csIFCHandler({
					interfaceid: 'cs-cancel-saleout-bill',
					pk: sfRes3.result.pk,
					check: true
				});

				await nqLogin.login({
					logid: `${branchCode}Shop1000`
				});
				let purQfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-142202-1',
					pk: sfRes1.result.pk,
				});
				purQfRes.result.cash = common.sub(purQfRes.result.cash, 2000);
				purQfRes.result.details.forEach((detail) => detail.num = common.sub(detail.num, 5));
				await editBillingByDistribution(purQfRes.result);

				purQfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-142202-1',
					pk: sfRes2.result.pk,
				});
				await editBillingByDistribution(purQfRes.result);

				cancelQfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-142202-1',
					pk: sfRes3.result.pk,
				});

				await nqLogin.login();
			});
			it('300019.下游未入库前上游作废', async () => {
				expect(cancelQfRes.result).to.includes({
					error: '查询的数据不存在',
				});
			});
			it('300023.1.下游部分入库后,上游修改单据保存', async () => {
				sfRes1.params.pk = sfRes1.result.pk;
				let sfRes = await common.editBilling(sfRes1.params, false);
				expect(sfRes.result).to.includes({
					error: '分销单下游已接收，不允许修改'
				});
			});
			it('300020.1.下游部分入库后,上游作废销售单', async () => {
				let res = await reqHandler.csIFCHandler({
					interfaceid: 'cs-cancel-saleout-bill',
					pk: sfRes1.result.pk,
					check: false
				});
				expect(res.result).to.includes({
					error: '分销单下游已接收，不允许作废'
				});
			});
			it('300023.2.下游全部入库后,上游修改单据保存', async () => {
				sfRes2.params.pk = sfRes2.result.pk;
				let sfRes = await common.editBilling(sfRes2.params, false);
				expect(sfRes.result).to.includes({
					error: '分销单下游已接收，不允许修改'
				});
			});
			it('300020.2.下游全部入库后,上游作废销售单', async () => {
				let res = await reqHandler.csIFCHandler({
					interfaceid: 'cs-cancel-saleout-bill',
					pk: sfRes2.result.pk,
					check: false
				});
				expect(res.result).to.includes({
					error: '分销单下游已接收，不允许作废'
				});
			});
		});
		it('300027.分销入库修改厂商', async () => {
			await nqLogin.login();
			let sfRes = await common.editBilling(salesJson({
				dwid: custId,
				styleInfo
			}));

			await nqLogin.login({
				logid: `${branchCode}Shop1000`,
			});
			let purQfRes = await reqHandler.queryBilling({
				interfaceid: 'qf-142202-1',
				pk: sfRes.result.pk,
			});
			delete purQfRes.result.dwid; //清除厂商
			let res = await editBillingByDistribution(purQfRes.result, false);
			expect(res.result).to.includes({
				error: '客户不能为空',
			});
		});

	});
	// 1、获取起始值
	// 2、开各种单据
	// 3、再获取一遍值
	// 4、后面的值=起始值+开单的值
	// let typeId = [1, 2, 3]; //进销存查询分组类别 1部门  2按款号 3 按颜色/尺码
	for (let i = 1; i < 4; i++) { //typeId.length
		// 依赖上面铺货用例  这里有问题  total会显示为-1，和曹工确认过 ，先不管了，这个功能要废弃
		describe.skip('手机版分销总图', function () {
			let beforeData;
			it('取起始值', async function () {
				//取起始值
				beforeData = await getAgentInvoicing(styleInfo, i);
				expect(Number(beforeData.result.data.count), `查询结果出错`).to.above(0);
				//上游铺货
			});
			it('1.下游分销商进行入库操作', async function () {
				let salesQfRes = {};
				//总部-总经理登陆
				await nqLogin.login();
				//上游铺货
				let sfRes = await common.editBilling(salesJson({
					dwid: custId,
					styleInfo
				}));
				salesQfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-14211-1',
					pk: sfRes.result.pk,
				});
				//salesQfRes 可以知道入库数
				//下游登录
				await nqLogin.login({
					logid: `${branchCode}Shop1000`,
				});
				//下游入库
				let purQfRes = await reqHandler.queryBilling({
					interfaceid: 'qf-142202-1',
					pk: salesQfRes.params.pk,
				});
				sfRes = await editBillingByDistribution(purQfRes.result); //下游入库
				// console.log(`sfRes : ${JSON.stringify(sfRes)}`);
				let exp = {
					stockNum: sfRes.params.details[0].num,
					sumPurNum: sfRes.params.details[0].num,
				}
				exp = await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);

				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			});
			//按分销退货
			it('2.下游分销商退货给上游', async function () {
				//下游采购退货给上游
				//上游-销售，按分销退货，选择数量保存
				//下游登录
				await nqLogin.login({
					logid: `${branchCode}Shop1000`,
				});
				let json = salesJson({
					interfaceid: 'sf-14212-1',
					dwid: companyId, //厂商id
					styleInfo
				}, (obj) => {
					obj.cash = -1000;
					obj.details.forEach((detail) => detail.num = -5);
				});
				let sfRes = await common.editBilling(json); //下游采购退货给上游
				let exp = {
					stockNum: sfRes.params.details[0].num,
					sumPurNum: sfRes.params.details[0].num,
				}
				exp = await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			});
			it('3.下游分销商既拿货又退货给上游', async function () {
				let json = salesJson({
					interfaceid: 'sf-14212-1',
					dwid: companyId, //厂商id
					styleInfo,
				});
				json.details[0].num = 5;
				json.details[1] = {
					'num': -5,
					'sizeid': styleInfo.sizeids.split(',')[0],
					'styleid': styleInfo.pk,
					'colorid': styleInfo.colorids.split(',')[0],
					'price': 200,
					'discount': 1,
				};
				json.cash = 0;
				let sfRes = await common.editBilling(json); //下游采购退货给上游
				let exp = {
					stockNum: sfRes.params.totalnum,
					sumPurNum: sfRes.params.totalnum,
					// stockNum: sfRes.params.details[0].num,
					// sumPurNum: sfRes.params.details[0].num,
				}
				exp = await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			})
			it('4.下游分销商开单', async function () {
				await nqLogin.login({
					logid: `${branchCode}Shop1000`,
				});
				let json = salesJson({
					styleInfo,
				}, (obj) => {
					obj.cash = 400;
					obj.details.forEach((detail) => detail.num = 1);
				});
				let sfRes = await common.editBilling(json);
				let exp = {
					saleOutNum: sfRes.params.details[0].num,
					saleOutMoney: sfRes.params.details[0].total,
					sumSaleNum: sfRes.params.details[0].num,
					sumSaleMoney: sfRes.params.details[0].total,
					stockNum: -Number(sfRes.params.details[0].num),
				}
				await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			});
			it('5.下游分销商开退货单', async function () {
				await nqLogin.login({
					logid: `${branchCode}Shop1000`,
				});
				let json = salesJson({
					styleInfo,
				}, (obj) => {
					obj.cash = -800;
					obj.details.forEach((detail) => detail.num = '-2');
				});
				let sfRes = await common.editBilling(json);
				let exp = {
					backNum: Math.abs(sfRes.params.details[0].num),
					backMoney: Math.abs(sfRes.params.details[0].total),
					stockNum: Math.abs(sfRes.params.details[0].num),
					sumSaleNum: sfRes.params.details[0].num,
					sumSaleMoney: sfRes.params.details[0].total,
				};
				exp = await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			});
			it('6.下游分销商既拿货退货', async function () {
				await nqLogin.login({
					logid: `${branchCode}Shop1000`,
				});
				let json = salesJson({
					styleInfo,
				});
				json.details[0].num = 5;
				json.details[1] = {
					'num': -5,
					'sizeid': styleInfo.sizeids.split(',')[0],
					'styleid': styleInfo.pk,
					'colorid': styleInfo.colorids.split(',')[0],
					'price': 200,
					'discount': 1,
				};
				json.cash = 0;
				let sfRes = await common.editBilling(json);
				let exp = {
					saleOutNum: sfRes.params.details[0].num,
					saleOutMoney: sfRes.params.details[0].total,
					backNum: Math.abs(sfRes.params.details[1].num),
					backMoney: Math.abs(sfRes.params.details[1].total),
					sumSaleNum: sfRes.params.totalnum,
					sumSaleMoney: sfRes.params.totalmoney,
				};
				exp = await getExpAgentInvoicing(exp, beforeData.result.data.rows[0]);
				beforeData = await getAgentInvoicing(styleInfo, i);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.rows[0]);
				common.isApproximatelyEqualAssert(exp, beforeData.result.data.sum);
			});
		});
	};
});
// });

//分销总图数据
async function getAgentInvoicing(styleInfo, typeId) {
	await nqLogin.login({
		dlProductCode: 'pocketslh',
	}); //上游手机版登录

	let queryData = await common.apiDo({
		apiKey: 'ec-bi-statAgentInvoicing',
		pageSize: 15, //每页显示记录数
		pageNo: 1, //当前页号
		jsonParam: {
			typeId: typeId, //typeId, //必填  进销存查询分组类别 1部门  2按款号 3 按分销商
			tenantSpuId: styleInfo.pk, //款号id
			// nameLike: "名称",
			// depId: "中间商",
			shopId: styleInfo.shopid,
			colorId: styleInfo.colorids.split(',')[0],
			sizeId: styleInfo.sizeids.split(',')[0],
			classId: styleInfo.classId,
			brandId: styleInfo.brandId,
			season: styleInfo.season,
			bizDateS: common.getCurrentDate(),
			bizDateE: common.getCurrentDate(),
		}
	});
	return queryData;
}
//拼接分销总图期望值
// exp中存放进销的变化量
async function getExpAgentInvoicing(exp, queryResult) {

	let ExpAgentInvoicing = common.addObject(exp, queryResult);

	if (ExpAgentInvoicing.backNum == 0 && ExpAgentInvoicing.saleOutNum == 0) ExpAgentInvoicing.backRate = 0;
	else ExpAgentInvoicing.backRate = common.mul(common.div(ExpAgentInvoicing.backNum, ExpAgentInvoicing.saleOutNum).toFixed(3), 100);
	return ExpAgentInvoicing;
}



/**
 * editBillingByDistribution - 按分销入库
 * @param {object}    qfResult     qf-142202-1结果
 * @param {boolean} [check=true] 断言是否开单成功
 *
 * @return {object} {params,result}
 */
async function editBillingByDistribution(qfResult, check = true) {
	qfResult = Object.assign(qfResult, {
		interfaceid: 'sf-14212-1',
		deliver: qfResult.respopid,
		inoutflag: 1,
	});
	let sfRes = await common.editBilling(qfResult, check);
	return sfRes;
};

async function editStyle() {
	// 获取基础信息[颜色: 601, 尺码: 605, 货品类别]
	// 颜色尺码各取10条(受参数max_num_style_color_size影响 颜色数 * 尺码数小于200)
	let params = [{
		apiKey: 'ec-config-list-dict',
		jsonParam: {
			'flag': 1,
			'typeId': '601',
			'unitId': LOGINDATA.epid,
			'orderBy': 'createdDate desc',
			'pageSize': 5,
			'cap': 0
		},
	}, {
		apiKey: 'ec-config-list-dict',
		jsonParam: {
			'flag': 1,
			'typeId': '605',
			'unitId': LOGINDATA.epid,
			'orderBy': 'createdDate desc',
			'pageSize': 5,
			'cap': 0
		},
	}, {
		apiKey: 'ec-dres-class-list',
		jsonParam: {
			'flag': 1,
			'searchList': 1,
			'searchSum': 1,
			'searchCount': 1
		},
	}, {
		apiKey: 'ec-config-list-dict',
		jsonParam: {
			'flag': 1,
			'typeId': '606',
			'unitId': LOGINDATA.epid,
			'orderBy': 'createdDate desc',
			'pageSize': 1,
			'cap': 0
		},
	}];
	let promises = params.map((param) => common.apiDo(param));
	let res = await Promise.all(promises);
	let [colorIds, sizeIds, classIds, brandIds] = res.map(obj =>
		obj.result.data.rows.map(info => obj.params.apiKey == 'ec-config-list-dict' ? info.codeValue : info.id));

	//新增货品
	//console.log(`colorIds : ${JSON.stringify(colorIds)}`);
	let json = basiceJson.addGoodJson();
	// colorIds = colorIds.slice(0, 3);
	// sizeIds = sizeIds.slice(0, 3);
	//console.log(`colorIds : ${JSON.stringify(colorIds)}`);
	json.colorids = _.join(colorIds, ',');
	json.sizeids = _.join(sizeIds, ',');
	json.classid = classIds[0];
	json.brandid = brandIds[0];
	let styleRes = await basicInfoReqHandler.editStyle({
		jsonparam: json,
	});
	return styleRes;
};

function salesJson({
	interfaceid = 'sf-14211-1',
	dwid = '',
	styleInfo
}, predicate) {
	let json = {
		interfaceid: interfaceid,
		dwid: dwid,
		srcType: 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		details: [{
			'num': common.getRandomNum(10, 100),
			'sizeid': styleInfo.sizeids.split(',')[0],
			'styleid': styleInfo.pk,
			'colorid': styleInfo.colorids.split(',')[0],
			'price': 200,
			'discount': 1,
		}, {
			'num': common.getRandomNum(10, 100),
			'sizeid': styleInfo.sizeids.split(',')[1],
			'styleid': styleInfo.pk,
			'colorid': styleInfo.colorids.split(',')[1],
			'price': 200,
			'discount': 1,
		}],
	};
	json.cash = (json.details[0].num + json.details[1].num) * 200;
	if (typeof predicate == 'function') predicate(json);
	return json;
};
