const common = require('../../../lib/common');
const format = require('../../../data/format');
const getBasicData = require('../../../data/getBasicData.js');
const basiceJson = require('../../../slh1/help/basiceJsonparam.js');
const salesOrderReqHandler = require('../../../slh1/help/salesOrderHelp/salesOrderRequestHandler');



describe('单据--slh2', function () {
    this.timeout(30000);

    before(async function () {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
    });

    //http://jira.hzdlsoft.com:7082/browse/SLHSEC-7310
    describe('按订货开单校验条码', function () {
        let orderJson, barCodeRes, sfRes, qfRes;
        before(async function () {
            orderJson = basiceJson.salesOrderJson();
            sfRes = await common.editBilling(orderJson);
            // console.log(`sfRes=${JSON.stringify(sfRes)}`);
            qfRes = await salesOrderReqHandler.salesOrderQueryBilling({ pk: sfRes.result.pk });
            barCodeRes = await common.callInterface('ql-1528', format.qlParamsFormat({
                styleid: sfRes.params.details[0].styleid,
            }, false));
        });
        it('1.款号在本次订货中', async function () {
            for (let index = 0; index < qfRes.result.details.length; index++) {
                const element = qfRes.result.details[index];
                const barcodeInfo = _.remove(barCodeRes.dataList, (data) => data.stylecode == element.stylecode && data.color == element.show_colorid && data.size == element.show_sizeid);
                expect(barcodeInfo, `没有查询到开单款号的条码${JSON.stringify(barCodeRes)}`).to.not.be.undefined;
                await common.apiDo({
                    apiKey: 'ec-trade-salesOrder-checkBarCode',
                    jsonParam: {
                        barCode: barcodeInfo[0].barcode,
                        id: sfRes.result.pk
                    },
                });
            };
        });
        it('2.款号不在本次订货中', async function () {
            const res = await common.apiDo({
                apiKey: 'ec-trade-salesOrder-checkBarCode',
                jsonParam: {
                    barCode: barCodeRes.dataList.shift().barcode,
                    id: sfRes.result.pk
                },
                check: false
            });
            expect(res.result, `查询不在订单中的条码，查询成功${JSON.stringify(res)}`).to.includes({
                msg: '订单中不存在此条码对应的商品',
                msgId: 'barcode_not_in_this_order',
            });
        });
    });

});     