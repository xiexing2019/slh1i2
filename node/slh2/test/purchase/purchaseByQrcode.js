"use strict";
const common = require('../../../lib/common');

const format = require('../../../data/format');
const basiceJson = require('../../../slh1/help/basiceJsonparam');
const getBasicData = require('../../../data/getBasicData.js');
const reqHandler = require('../../../slh1/help/reqHandlerHelp');
const basicInfoReqHandler = require('../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const purReqHandler = require('../../../slh1/help/purchaseHelp/purRequestHandler');
const mainDataReqHandler = require('../../../slh1/help/basicInfoHelp/mainDataReqHandler');
const tradeReqHandler = require('../../../reqHandler/slh2/trade');

//扫码入库的账套epid: '14901'  环境：cs3d2  账套名：gg  14901
describe('扫码入库-slh2', async function () {
	this.timeout(30000);


	describe('扫码入库-主流程', function () {

		let sfRes, styleInfo, supplierInfo, otherUnitStyle1, otherUnitStyle2, supplierAccount1, supplierAccount2, qfResFromScanCode, qlResFromScanCode;
		before(async () => {
			await common.loginDo();
			BASICDATA = await getBasicData.getBasicID();
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
			// console.log(`styleInfo : ${JSON.stringify(styleInfo)}`);
			sfRes = await common.editBilling(salesJson({
				styleInfo
			}));
			//获取单据所需的信息
			let needInfo = {
				targetUnitId: LOGINDATA.epid,
				billId: sfRes.result.pk,
				sn: LOGINDATA.sn,
			};
			//换账套登录
			await common.loginDo({
				epid: '22049'
			});
			// console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

			let supplierList = await mainDataReqHandler.getSupplierList({ delflag: 0 });
			if (supplierList.result.dataList.length == 0) {
				let sfSupplier = await mainDataReqHandler.saveSupplier({
					nameshort: '扫码入库' + common.getRandomStr(4),
				});
				supplierInfo = {
					supplierId: sfSupplier.result.val,
					name: sfSupplier.params.jsonparam.nameshort,
					balance: 0,
				};
			} else {
				supplierInfo = {
					supplierId: supplierList.result.dataList[0].id,
					name: supplierList.result.dataList[0].name
				};
				supplierAccount1 = await common.callInterface('ql-15007', format.qlParamsFormat({
					dwid: supplierInfo.supplierId
				}));

				//如果厂商没有账款，赋初值
				if (supplierAccount1.dataList.length == 0) {
					supplierInfo.balance = 0;
				} else {
					supplierInfo.balance = supplierAccount1.dataList[0].balance;
				}
			};
			// console.log(`supplierInfo : ${JSON.stringify(supplierInfo)}`);
			otherUnitStyle1 = await mainDataReqHandler.getStyleList({
				stylename: styleInfo.code
			});
			//扫码入库

			let purFromScan = await purBillFromScanCode({
				needInfo,
				supplierId: supplierInfo.supplierId,
			});
			await common.delay(1000);
			//采购入库-按批次查
			qlResFromScanCode = await common.callInterface('ql-22301', format.qlParamsFormat({}, true));
			//采购入库-详情
			qfResFromScanCode = await purReqHandler.purQueryBilling(qlResFromScanCode.dataList[0].id);
			// console.log(`qlResFromScanCode.dataList[0] : ${JSON.stringify(qlResFromScanCode.dataList[0])}`);
			// console.log(`qfResFromScanCode : ${JSON.stringify(qfResFromScanCode)}`);
			otherUnitStyle2 = await mainDataReqHandler.getStyleList({
				stylename: styleInfo.code
			});
			// console.log(`variable : ${JSON.stringify(otherUnitStyle2.dataList)}`);
			supplierAccount2 = await common.callInterface('ql-15007', format.qlParamsFormat({
				dwid: supplierInfo.supplierId
			}));
		});
		it('采购入库-详情验证', async function () {
			//扫码入库--一个款号
			//采购单详情rowid都是0 有问题
			// console.log(`sfRes.params=${JSON.stringify(sfRes.params)}`);
			let exp = Object.assign(_.cloneDeep(sfRes.params), {
				dwid: supplierAccount1.dataList[0].id,
				srcType: 6,
				actid: 10,
				type: 10,
				inoutflag: 1,
				deliver: LOGINDATA.id,
				invid: LOGINDATA.invid,
				shopid: LOGINDATA.invid,
				remark: "扫码导入",
				actualpay: 0,
			});
			exp.balance = -exp.totalmoney;//扫码导入默认未付
			exp.details.forEach(detail => detail.styleid = otherUnitStyle2.result.dataList[0].id);
			//
			common.isApproximatelyEqualAssert(exp, qfResFromScanCode.result, ['rowid']);
		});
		it('采购入库-按批次查验证', async function () {
			let exp = purReqHandler.getPurMainSearchListExp(qfResFromScanCode.result);
			common.isApproximatelyEqualAssert(exp, qlResFromScanCode.dataList[0]);
		});
		it('货品查询验证', async function () {
			if (otherUnitStyle1.result.dataList.length != 0) {
				let exp = common.addObject({
					invnum: sfRes.params.totalnum,
					invonroadnum: sfRes.params.totalnum,
				}, otherUnitStyle1.result.dataList[0]);
				exp.provideridname = supplierInfo.name;
				exp.optime = qfResFromScanCode.result.optime;
				// console.log(`exp : ${JSON.stringify(exp)}`);
				// console.log(`variable : ${JSON.stringify(otherUnitStyle2.result.dataList[0])}`);
				common.isApproximatelyEqualAssert(exp, otherUnitStyle2.result.dataList[0], ['optime']);
			} else {
				expect(otherUnitStyle2.result.dataList.length).to.equal(1);
				let exp = {
					"code": styleInfo.code,
					"flag": "0",
					"seasonname": "0",
					"discount": "1.000",
					"invonroadnum": sfRes.params.totalnum,
					"year1": "0",
					"sizeids": styleInfo.sizeids.slice(0, 3),
					"isGift": "0",
					"id": otherUnitStyle2.result.dataList[0].id,
					"rem": "扫码导入",
					"brand": "",
					"delflag": "0",
					"fileid": styleInfo.fileid,
					"stdprice1": "0.000",
					"stdprice5": "0.000",
					"optime": qfResFromScanCode.result.optime,
					"stdprice4": "0.000",
					"stdprice3": "0.000",
					"stdprice2": "0.000",
					"opname": LOGINDATA.name,
					"colorids": styleInfo.colorids.slice(0, 3),
					"marketdate": common.getCurrentDate(),
					"name": styleInfo.name,
					"purprice": "200.000",
					"provideridname": supplierInfo.name,
					"origid": styleInfo.pk
				};
				// console.log(`exp : ${JSON.stringify(exp)}`);
				// console.log(`otherUnitStyle2 : ${JSON.stringify(otherUnitStyle2.result.dataList[0])}`);
				common.isApproximatelyEqualAssert(exp, otherUnitStyle2.result.dataList[0]);
			}
		});
		it('厂商账款验证', async function () {
			//扫码入库默认未付
			let exp = _.cloneDeep(supplierAccount1);
			exp.dataList[0].balance = common.add(-sfRes.params.totalmoney, supplierInfo.balance);
			exp.sumrow.balance = common.add(-sfRes.params.totalmoney, supplierInfo.balance);
			common.isApproximatelyEqualAssert(exp, supplierAccount2);
		});

	});

	describe.skip('扫码入库压测', function () {
		let needInfo;
		before(async function () {
			await common.loginDo();
			needInfo = {
				targetUnitId: LOGINDATA.epid,
				billId: '215986',
				sn: LOGINDATA.sn,
			};
			await common.loginDo({
				epid: '13353'
			});

		})
		it('test', async function () {
			let supplierList = await mainDataReqHandler.getSupplierList();
			let supplierId = supplierList.result.dataList[0].id;
			let purFromScan = await purBillFromScanCode({
				needInfo,
				supplierId
			});
		})
	})

	describe('扫码入库分支流', async function () {
		let sfRes, styleInfo;
		it('异常流', async function () {
			await common.loginDo();
			BASICDATA = await getBasicData.getBasicID();
			styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc002);
			sfRes = await common.editBilling(salesJson({
				styleInfo
			}));
			let needInfo = {
				targetUnitId: LOGINDATA.epid,
				billId: sfRes.result.pk,
				sn: LOGINDATA.sn,
			};
			await common.loginDo({
				epid: '22049'
			});
			//换新的账套新增一个款号为Agc002的款号
			// await basicInfoReqHandler.editStyle({ jsonparam: basiceJson.addGoodJson({ code: 'Agc002' }) });
			let supplierListInfo = await mainDataReqHandler.getSupplierList({ delflag: 0 }).then(res => res.result.dataList[0]);
			// console.log(`supplierListInfo=${JSON.stringify(supplierListInfo)}`);
			let res = await purBillFromScanCode({
				needInfo,
				supplierId: supplierListInfo.id,
				check: false,
			});

			expect(res.result.data).to.eql({ [`${styleInfo.code}`]: "厂商款号和现有款号(Agc002,autotest,厂商2)冲突，请选择覆盖或者重设本地款号" });
		});
	});
});


/**
 * 扫码入库
 * @param  {object}
 *  @param  {object} needInfo targetUnitId，billId，sn
 */
async function purBillFromScanCode({
	needInfo,
	supplierId,
	coverFlag,
	dresCode,
	check = true,
}) {
	let dataFromScanCode = await tradeReqHandler.getDataFromScanCode({
		jsonParam: needInfo
	});
	dataFromScanCode.result.data.details.forEach(detail => {
		detail.coverFlag = coverFlag || 0; //是否覆盖 1覆盖、0不覆盖
		detail.dresCode = dresCode || '';
	});
	dataFromScanCode.result.data.compId = supplierId;
	dataFromScanCode.result.data.check = check;
	return tradeReqHandler.savePurBillFromScanCode(dataFromScanCode.result.data);
	// console.log(`里面res=${JSON.stringify(res)}\n`);

};

function salesJson({
	interfaceid = 'sf-14211-1',
	dwid = '',
	styleInfo
}, predicate) {
	let json = {
		interfaceid: interfaceid,
		dwid: dwid,
		srcType: 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
		details: [{
			'num': common.getRandomNum(10, 100),
			'sizeid': styleInfo.sizeids.split(',')[0],
			'styleid': styleInfo.pk,
			'colorid': styleInfo.colorids.split(',')[0],
			'price': 200,
			'discount': 1,
		}, {
			'num': common.getRandomNum(10, 100),
			'sizeid': styleInfo.sizeids.split(',')[1],
			'styleid': styleInfo.pk,
			'colorid': styleInfo.colorids.split(',')[1],
			'price': 200,
			'discount': 1,
		}],
	};
	json.cash = (json.details[0].num + json.details[1].num) * 200;
	if (typeof predicate == 'function') predicate(json);
	return json;
};
