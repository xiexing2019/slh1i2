const common = require('../../../lib/common');
const getBasicData = require('../../../data/getBasicData.js');
const format = require('../../../data/format');
const configReqHandler = require('../../../reqHandler/slh2/config');

describe('web端款号条码查询-fresh', function () {
    this.timeout(30000);
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
    });

    it('web端条码查询', async function () {
        const styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
        const exp = await getBarCodeExp(styleInfo);
        const res = await common.apiDo({
            apiKey: 'ec-dres-skuDetail-webList',
            pageSize: 0,
            jsonParam: {
                tenantSpuId: BASICDATA.styleidAgc001,
                orgSuppId: styleInfo.dwid,
                season: styleInfo.season,
                year: styleInfo.show_year1,
                classId: styleInfo.classid,
                brandId: styleInfo.brandid,
                marketDateS: styleInfo.marketdate,
                marketDateE: styleInfo.marketdate,
                flag: 1,//款号是否启用 1启用
                orderBy: 'id',//"排序"
                orderByDesc: false,//"升降序 true:降序"
            }
        });
        common.isApproximatelyArrayAssert(exp, res.result.data.rows);
    });
});


/**
 * 拼接条码查询期望值
 * @param {object} styleInfo 
 * 
 */
async function getBarCodeExp(styleInfo) {
    // let json = {
    //     "flag": 1, "updatedDate": "2018-08-22 14:58:56", "id": 1805730, "ecSeq": 1,
    //     "origId": 0, "branchId": 697330, "updatedBy": 387598, "shopSpuId": -1, "createdDate": "2018-06-21 11:31:37",
    //     "createdBy": 387598,
    // };
    let colorIds, sizeIds;
    const mapField = 'code;name;year=show_year1;classId=classid;tenantSpuId=pk;season;marketDate=marketdate;shopId=shopid;brandId=brandid;orgSuppId=dwid';
    const barInfo = format.dataFormat(styleInfo, mapField);
    barInfo.unitId = LOGINDATA.epid;
    //获取款号的颜色尺码
    const colorIdsStyle = styleInfo.colorids.split(',').sort((a, b) => a - b);
    const sizeIdsStyle = styleInfo.sizeids.split(',').sort((a, b) => a - b);
    //获取颜色尺码列表
    let colorList = await configReqHandler.getDictList({ typeId: 601, unitId: LOGINDATA.epid, });
    let sizeList = await configReqHandler.getDictList({ typeId: 605, unitId: LOGINDATA.epid, });
    //获取颜色尺码的sid和bizCode,sid是skuCode所需,bizCode是条码barcode所需
    colorIds = getDictInfo({ styleDict: colorIdsStyle, dictList: colorList.result.data.rows });
    sizeIds = getDictInfo({ styleDict: sizeIdsStyle, dictList: sizeList.result.data.rows });
    let exp = [];
    for (let index = 0; index < colorIds.length; index++) {
        sizeIds.forEach(sizeId => {
            exp.push(Object.assign({}, barInfo, {
                colorId: colorIds[index].sid, sizeId: sizeId.sid,
                barcode: `${barInfo.code}${colorIds[index].bizCode}${sizeId.bizCode}`,
                skuCode: `${barInfo.code}${colorIds[index].sid.padStart(3, 0)}${sizeId.sid.padStart(3, 0)}`
            }));
        });
    };
    return exp;
};

/**
 * 拼接颜色尺码sid和bizCode
 * @param {object} styleDict 款号的颜色尺码
 * @param {object} dictList  颜色尺码列表
 * @return {Array}  
 */
function getDictInfo({ styleDict, dictList }) {
    let result = [];
    for (let i = 0; i < styleDict.length; i++) {
        for (let j = 0; j < dictList.length; j++) {
            if (styleDict[i] == dictList[j].codeValue)
                result.push({ sid: styleDict[i], bizCode: dictList[j].bizCode });
        };
    };
    return result;
}
