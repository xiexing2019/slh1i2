const common = require('../../../lib/common');
const getBasicData = require('../../../data/getBasicData.js');
// const format = require('../../../data/format');
const invReqHandler = require('../../../reqHandler/slh2/inv');
const otherBillJson = require('../../help/json/otherBillJson');
const getBillExp = require('../../help/getExp/getBillExp');
const reqHandler = require('../../../slh1/help/reqHandlerHelp');

describe('期初入库管理-slh2', function () {
    this.timeout(30000);
    let styleInfo, qlDefParams;
    before(async () => {
        await common.loginDo();
        BASICDATA = await getBasicData.getBasicID();
        styleInfo = await common.fetchMatInfo(BASICDATA.styleidAgc001);
    });
    describe('新增期初入库单', function () {
        let initJson, saveInit, invStyle1, invStyle2, initBillInfo, initBillList1, initBillList2, initBillDetList;
        before(async () => {
            initBillList1 = await invReqHandler.getInitBillList();
            initJson = otherBillJson.initBillJson({ styleInfo });
            // console.log(`新增.billDetails=${JSON.stringify(initJson.billDetails)}`);
            //获取单据中款号的初始库存
            invStyle1 = await getStyleInv(initJson.billDetails);
            // console.log(`invStyle1=${JSON.stringify(invStyle1)}`);
            // 新增期初入库单
            saveInit = await invReqHandler.saveInitBill(initJson);
            // console.log(`saveInit=${JSON.stringify(saveInit)}`);
            //入库单明细
            initBillInfo = await invReqHandler.getInitBillInfoById({ id: saveInit.result.data.val });
            //入库单列表查询
            initBillList2 = await invReqHandler.getInitBillList();
            //入库单明细列表查询
            initBillDetList = await invReqHandler.getInitBillDetList({
                jsonParam: {
                    tenantSpuId: saveInit.params.jsonParam.billDetails[0].tenantSpuId,
                },
            });
            // console.log(`initBillDetList = ${JSON.stringify(initBillDetList)}`);
            invStyle2 = await getStyleInv(initJson.billDetails);
            // console.log(`invStyle2 = ${JSON.stringify(invStyle2)}`);
        });
        it('入库单详细验证', async function () {
            common.isApproximatelyEqualAssert(saveInit.params.jsonParam, initBillInfo.result.data);
        });
        it('期初入库单查询', async function () {
            // console.log(`initBillList1=${JSON.stringify(initBillList1)}`);
            common.isApproximatelyEqualAssert(initBillInfo.result.data, initBillList2.result.data.rows[0]);
            expect(common.add(initBillList1.result.data.sum.totalNum, initBillInfo.result.data.totalNum), '期初入库-入库单管理汇总值错误').to.equal(initBillList2.result.data.sum.totalNum);
        });
        it('期初入库单按明细查查询', async function () {
            let exp = getBillExp.getInitBillDetListExp(initBillInfo.result.data);
            // console.log(`exp = ${JSON.stringify(exp)}`);
            // console.log(`initBillDetList=${JSON.stringify(initBillDetList)}`);
            common.isApproximatelyArrayAssert(exp, initBillDetList.result.data.rows.slice(0, initJson.billDetails.length));
        });
        it.skip('库存验证', async function () {
            let exp = {};
            initJson.billDetails.forEach((detail) => {
                exp[`${detail.tenantSpuId}-${detail.colorId}-${detail.sizeId}`] = detail.num;
            });
            common.isApproximatelyArrayAssert(common.addObject(exp, invStyle1), invStyle2);
            // })
        });
        describe('修改入库单', function () {
            let updataBillRes, updataStyleInv1, updataStyleInv2;
            before(async () => {
                let updataJson = otherBillJson.initBillJson({ styleInfo });
                initBillList1 = await invReqHandler.getInitBillList();
                //获取初始库存
                // updataStyleInv1 = await getStyleInv(updataJson.billDetails);
                updataJson.id = saveInit.result.data.val;
                updataJson.billDetails.forEach((ele, index) => {
                    ele.id = initBillInfo.result.data.billDetails[index].id;
                    ele.rem = `修改明细${index + 1}`
                });
                //修改入库单
                updataBillRes = await invReqHandler.saveInitBill(updataJson);
                // console.log(`updataBillRes=${JSON.stringify(updataBillRes)}`);
                //获取单据明细
                initBillInfo = await invReqHandler.getInitBillInfoById({ id: saveInit.result.data.val });
                // console.log(`initBillInfo = ${JSON.stringify(initBillInfo)}`);
                //入库单列表查询
                initBillList2 = await invReqHandler.getInitBillList();
                //入库单明细列表查询
                initBillDetList = await invReqHandler.getInitBillDetList({
                    jsonParam: {
                        tenantSpuId: updataBillRes.params.jsonParam.billDetails[0].tenantSpuId,
                    },
                });
                updataStyleInv2 = await await getStyleInv(updataJson.billDetails);
            });
            it('入库单详细验证', async function () {
                common.isApproximatelyEqualAssert(updataBillRes.params.jsonParam, initBillInfo.result.data);
            });
            it('期初入库单查询', async function () {
                common.isApproximatelyEqualAssert(initBillInfo.result.data, initBillList2.result.data.rows[0]);
                expect(common.add(initBillList1.result.data.sum.totalNum, common.sub(initBillInfo.result.data.totalNum, initJson.totalNum)), '期初入库-入库单管理汇总值错误').to.equal(initBillList2.result.data.sum.totalNum);
            });
            it('期初入库单按明细查查询', async function () {
                let exp = getBillExp.getInitBillDetListExp(initBillInfo.result.data);
                // console.log(`exp = ${JSON.stringify(exp)}`);
                common.isApproximatelyArrayAssert(exp, initBillDetList.result.data.rows.slice(0, updataBillRes.params.jsonParam.billDetails.length));
            });
            it('库存验证', async function () {
                let exp = {};
                updataBillRes.params.jsonParam.billDetails.forEach((detail) => {
                    exp[`${detail.tenantSpuId}-${detail.colorId}-${detail.sizeId}`] = detail.num;
                });
                // console.log(`修改后库存exp=${JSON.stringify(exp)}`);
                // console.log(`invStyle1=${JSON.stringify(invStyle1)}`);
                // console.log(`updataStyleInv2=${JSON.stringify(updataStyleInv2)}`);
                common.isApproximatelyEqualAssert(common.addObject(exp, invStyle1), updataStyleInv2);
            });

        });
        describe('作废入库单', function () {
            //单据作废前信息
            let cancleBillBef;
            before(async () => {
                //主要是为了获取初始汇总值
                initBillList1 = await invReqHandler.getInitBillList();
                //获取初始库存
                invStyle1 = await getStyleInv(initBillInfo.result.data.billDetails);

                //作废入库单
                cancleBillBef = await invReqHandler.getInitBillInfoById({ id: saveInit.result.data.val });
                await invReqHandler.disableInitBill({ id: saveInit.result.data.val });
                initBillInfo = await invReqHandler.getInitBillInfoById({ id: saveInit.result.data.val });
                //入库单列表查询
                initBillList2 = await invReqHandler.getInitBillList();
                // console.log(`initBillList参数 = ${JSON.stringify(initBillList2.params)}`);
                //入库单明细列表查询
                initBillDetList = await invReqHandler.getInitBillDetList();
                invStyle2 = await getStyleInv(initBillInfo.result.data.billDetails);
            });
            it('入库单详细验证', async function () {
                expect(initBillInfo.result.data.flag, '入库单作废后flag没有变成0').to.equal(-1);
                common.isApproximatelyEqualAssert(cancleBillBef.result.data, initBillInfo.result.data, ['flag']);
            });
            it('期初入库单查询', async function () {
                common.isApproximatelyEqualAssert(initBillInfo.result.data, initBillList2.result.data.rows[0], ['hashKey']);
                expect(common.sub(initBillList1.result.data.sum.totalNum, initBillInfo.result.data.totalNum), '期初入库-入库单管理汇总值错误').to.equal(initBillList2.result.data.sum.totalNum);
            });
            it('期初入库单按明细查查询', async function () {
                let act = common.takeWhile(initBillDetList.result.data.rows, obj => obj.billId == saveInit.result.val);
                expect(act.length, '作废后,期初入库按明细查仍有该单据的信息').to.equal(0);
            });
            it('库存验证', async function () {
                let exp = {};
                initBillInfo.result.data.billDetails.forEach((detail) => {
                    exp[`${detail.tenantSpuId}-${detail.colorId}-${detail.sizeId}`] = -detail.num;
                });
                common.isApproximatelyEqualAssert(common.addObject(exp, invStyle2), invStyle2);
            });
        });
    });
});

/**
 * 获取入库单每条明细款号的库存
 * @param {object} params
 * 
 */
async function getStyleInv(params) {
    let result = {};
    for (let i = 0; i < params.length; i++) {
        await reqHandler.qlIFCHandler({
            interfaceid: 'ql-1932',
            invid: LOGINDATA.invid,
            propdresStyleid: params[i].tenantSpuId,

        }).then(res => res.result.dataList.forEach(data => result[`${data.styleid}-${data.colorid}-${data.sizeid}`] = data.invnum));
    }
    return result;
};
