const common = require('../../../lib/common');
const format = require('../../../data/format');
let otherJson = module.exports = {};

/**
 * 期初入库json
 * @param {object} params
 * @param {object} params.styleInfo
 */
otherJson.initBillJson = (params) => {
    let json = {
        unitId: LOGINDATA.epid,
        shopId: LOGINDATA.invid,
        invId: LOGINDATA.invid,
        deliver: LOGINDATA.id,
        totalNum: 0,
        totalMoney: 0,
        bizDate: common.getCurrentDate(),
        rem: '整单备注' + common.getRandomStr(5),
    };
    let colorIds = params.styleInfo.colorids.split(','),
        sizeIds = params.styleInfo.sizeids.split(',');
    let barCodes = [];
    for (let index = 0; index < colorIds.length; index++) {
        let colorId = colorIds[index];
        sizeIds.forEach(sizeId => {
            barCodes.push({ colorId, sizeId })
        });
    };
    barCodes = common.randomSort(barCodes);//随机颜色尺码
    json.billDetails = Array(Number(params.count || 2)).fill({}).map((detail, index) => {
        detail = {
            tenantSpuId: params.styleInfo.pk,
            colorId: barCodes[index].colorId,
            sizeId: barCodes[index].sizeId,
            price: params.styleInfo.purprice,
            // shopSpuId: LOGINDATA.invid,
            discount: 1,
            num: common.getRandomNum(1, 100),
            rem: `明细${index}` + common.getRandomStr(3),
        };
        detail.commitNum = detail.num;//提交数量*
        detail.money = common.mul(detail.num, detail.price);
        json.totalNum += detail.num;
        json.totalMoney += detail.money;
        return detail;
    });
    return json;
}