const common = require('../../lib/common');
const caps = require('../../data/caps');
const getBasicData = require('../../data/getBasicData');

let nqMain = module.exports = {};

//目前女权就是在cs3d2跑的
const epids = {
	'ytt-女权新2': '12985',
	'nq001': '13061',
	'nq002': '13085',
	'nq003': '13089',
	'autotestNv': '14449', //cs3d2
	'autotestNvd3': '22037' //cs3d3
};

nqMain.login = async function (params = {}) {
	// console.log(`caps : ${JSON.stringify(caps)}`);
	if (params.asName) {
		params.epid = epids[params.asName];
		delete params.asName;
	} else {
		switch (caps.name) {
			case 'slh2_test':
				params.epid = epids['ytt-女权新2'];
				break;
			case 'cs3d2':
				params.epid = epids['autotestNv'];
				break;
			case 'cs3d3':
				params.epid = epids['autotestNvd3'];
				break;
			default:
				break;
		}
	};

	await common.loginDo(params);
	await getBasicData.getBankAccountIds(); //银行账户id
};
