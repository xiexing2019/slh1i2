const format = require('../../../data/format');
let getBillExp = module.exports = {};


/**
 * 获取期初入库明细列表期望值 brandId字段没有拼接
 * @param {object} params 
 */
getBillExp.getInitBillDetListExp = function (params) {
    let res = [];
    const mainExp = format.dataFormat(params, 'deliver;createdBy;createdDate;updatedBy;updatedDate;branchId;invId;unitId;shopId;billNo');
    params.billDetails.forEach(detail => {
        let exp = {};
        [exp.code, exp.name] = detail.ecCaption.tenantSpuId.split(',');
        exp = Object.assign(exp, mainExp, format.dataFormat(detail, 'sizeId;flag;colorId;commitNum;tenantSpuId;price;id;rem;ecSeq;shopSkuId;shopSpuId;money;billId'));
        res.push(exp);
    });
    return res;
}