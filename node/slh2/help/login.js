// import { login } from "../../chainDiary/help/chainReq";
const common = require('../../lib/common');
const mdm = require('../../reqHandler/slh2/mdm');
const slh2Account = require('../../slh2/data/slh2Account');
// const slh2 = require('../../reqHandler/slh2');
const login = module.exports = {};

// epid: { code: data }
const userLogin = new Map();

/**
 * 员工登录
 * 
 */
login.staffLogin = async function (params = { logid: slh2Account.staff1.logid, pass: slh2Account.staff1.pass, epid: slh2Account.staff1.unitid['二代俊账套'] }) {
    const key = params.epid + '_' + params.logid;
    if (userLogin.has(key)) {
        LOGINDATA = _.cloneDeep(userLogin.get(key));
        const sessionLite = await mdm.sessionidValid({ sessionId: LOGINDATA.sessionid })
            .then(res => res.result)
            .catch(error => console.log(error.message));
        if (sessionLite.code == 0) {
            Object.assign({}, LOGINDATA, sessionLite.data.sessionBean);
            return;
        }

    }

    //赋默认值，有变化的再根据外部传入的来更新，未传入的就根据默认值来
    params = Object.assign(
        {
            logid: '000',
            pass: '000000',
            epid: slh2Account.staff1.unitid['二代俊账套'],
            deviceno: '1C:9E:46:29:ED:D0',
            dlProductCode: 'ipadslh',
            dlProductVersion: '9.3904',
            language: 'zh-Hans-CN',
            regoptime: '2019-05-06 12:00:44'
        },
        params
    )
    const res = await mdm.slh2Login(params);
    userLogin.set(key, _.cloneDeep(LOGINDATA));


    return res;
};

login.webLogin = async function (params = { code: slh2Account.staff1.logid, password: slh2Account.staff1.pass, unitId: slh2Account.staff1.unitid['二代俊账套'] }) {
    const key = params.unitId + '_' + params.code;
    if (userLogin.has(key)) {
        LOGINDATA = _.cloneDeep(userLogin.get(key));
        const sessionLite = await mdm.isSessionValid({ sessionId: LOGINDATA.sessionId })
            .then(res => res.result)
            .catch(error => console.log(error.message));
        if (sessionLite.code == 0) {
            Object.assign({}, LOGINDATA, sessionLite.data.sessionBean);
            return;
        }

    }

    //赋默认值，有变化的再根据外部传入的来更新，未传入的就根据默认值来
    params = Object.assign(
        {
            code: '000',
            password: '000000',
            unitId: slh2Account.staff1.unitid['二代俊账套'],
            productCode: 'web',
            productVersion: '1.0'
        },
        params
    )
    const res = await mdm.userLogin(params);
    if (res.result.data.hasOwnProperty('sessionId')) {
        LOGINDATA = Object.assign({}, res.params, res.result.data);


    }
    userLogin.set(key, _.cloneDeep(LOGINDATA));


    return res;
}


