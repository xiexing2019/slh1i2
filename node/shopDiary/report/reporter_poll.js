const mocha = require('mocha');
const moment = require('moment');
const dingtalkRobot = require('../../lib/dingtalkRobot');

// 笑铺日记群
const accessToken = 'cfb6503a498279a03474fe2550b8bec73d6f5d8738e05df3686c283871f9faf2';

class SDreporter extends mocha.reporters.Base {
    constructor(runner, options) {
        super(runner, options);

        let passCaseNum = 0, failCaseNum = 0, warnCaseNum = 0;

        runner.on('start', function (params) {
            console.log(`测试开始 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')}`);
        });

        runner.on('pass', function (test) {
            passCaseNum++;
        });

        runner.on('fail', async function (test, err) {
            failCaseNum++;

            const errorMsg = `${test.titlePath().join('-')}\nError: ${err.message}`;
            await dingtalkRobot.sendMsg(accessToken, {
                msgtype: 'text',
                text: { content: errorMsg }
            });
        });

        runner.on('end', async function () {
            const total = passCaseNum + failCaseNum + warnCaseNum;
            // 输出拱金工判断 参数名称写死
            console.log(`\n测试结束 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')} \n   total=${total}, passes=${passCaseNum}, failures=${failCaseNum}, warns=${warnCaseNum}`);
            if (failCaseNum) {
                await dingtalkRobot.sendMsg(accessToken, {
                    msgtype: 'text',
                    text: { content: `线上轮询完毕,用例数:${total},通过数:${passCaseNum},失败数:${failCaseNum}` },
                    at: { isAtAll: failCaseNum == 0 ? false : true },
                });
            }
            process.exit(0);
        });
    }
}


module.exports = SDreporter;