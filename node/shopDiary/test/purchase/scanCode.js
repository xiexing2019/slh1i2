const common = require('../../../lib/common');
const codeJson = require('../../help/json/codeJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');


describe.skip('扫码入库', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('新增采购单-扫码入库', function () {
        let scanRes, scanJson, sfRes;
        before(async () => {
            scanRes = await slh2.trade.getDataFromScanCode(codeJson.code1);
            console.log(scanRes);
            //获取扫码入库数据
            scanJson = scanRes.result.data;
            scanJson.compId = BASICDATA.ids.dwidVell;
            scanJson.suppName = '';
            scanJson.details[0].dresCode = '';
            scanJson.details[0].coverFlag = 0;
            scanJson.details[0].isPublic = 0;
            delete scanJson.details[0].localTenantSpuId;
            delete scanJson.details[0].tenantSpuId;
            console.log(`scanJson : ${JSON.stringify(scanJson)}`);
            sfRes = await slh2.trade.savePurBillFromScanCode(scanJson);
            console.log(sfRes);
            //保存数据生成采购单
            //await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        after(async () => {
            //作废单据
            await slh2.trade.deletePurchaseBill({
                isPend: 0,
                id: sfRes.result.data.val
            });
        });
        it('', async function () {

        });
        it('取上游条码-条码重复校验', async function () {

        });
        it('本地款号重复校验', async function () {

        });
        it('拆色拆码', async function () {

        });
    });

});