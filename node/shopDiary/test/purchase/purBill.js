'use strivt';
const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe('进货-online', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
		//console.log(`styleInfo=${JSON.stringify(BASICDATA.styleInfo)}`);
	});

	describe('新增进货单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			const json = basicJson.purJson();
			//获取起始库存,供应商起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			//新增进货单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			//进货单列表.开单后库存,供应商积分和余额
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([slh2.trade.getPurchaseBillList({ searchToken: sfRes.result.data.billNo }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.进货单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.进货单列表', async function () {
			const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
		});
		it('库存流水验证', async function () {
			// console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
		it('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it.skip('积分余额', async function () {

		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		it.skip('进货单列表查询条件', async function () {
			//searchToken  单据号 备注 供应商  
			// printFlag 1 已打  0 未打

		});
		it.skip('底部数据汇总', async function () {

		});
		it.skip('进货单列表排序', async function () {

		});
		describe.skip('进货单数据隔离', function () {
			before(async () => {
				await loginReq.changeShopFast({ shopName: '门店二' });
			});
			after(async () => {
				await loginReq.changeShop();
			});
			it('其他门店查询单据', async function () {
				let purList = await slh2.trade.getPurchaseBillList().then(res => res.result.data.rows.find(data => data.id == sfRes.result.data.val));
				expect(purList).to.be.undefined;
			});
		});

	});
	describe('修改进货单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, dwBalance1, dwBalance2, accountList1, accountList2;
		before(async () => {
			let json = basicJson.purJson();
			const promises = [slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
			slh2.acct.getBalanceAndScore({
				traderCap: 2,
				traderId: json.main.compId
			}),
			slh2.fin.getAccountList()
			];
			//获取起始库存,供应商起始积分和余额
			[invSku1, dwBalance1, accountList1] = await Promise.all(promises);
			//新增进货单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			let params = {
				rem: '修改进货单',
			};
			json = basicJson.purJson(params);
			json.main.id = sfRes.result.data.val; //修改时必传
			json.main.ver = sfRes.result.data.ver; //修改时必传，
			//修改进货单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json,
			});
			//进货单列表.开单后库存,供应商积分和余额
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, dwBalance2, accountList2] = await Promise.all([slh2.trade.getPurchaseBillList(), ...promises]);
		});
		it('1.进货单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data, ['ver']);
		});
		it('2.进货单列表', async function () {
			const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data, exp), invSku2.result.data);
		});
		it('库存流水验证', async function () {
			// console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
		it('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		it('5.供应商积分/余额检查', async function () {
			const exp = getExpByBill.getBalanceAndScoreExp(qfRes.result.data);
			// console.log(`dwBalance1.result.data=${JSON.stringify(dwBalance1.result.data)}`);
			// console.log(`dwBalance2.result.data=${JSON.stringify(dwBalance2.result.data)}`);
			common.isApproximatelyEqualAssert(common.addObject(dwBalance1.result.data, exp), dwBalance2.result.data);
		});
	});
	describe('新增退货单', function () {
		//先开单，再查，再作废，再查
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			let json = basicJson.purJson({
				details: [{
					num: -5,
				}, {
					num: -10,
				}]
			});
			//获取起始库存,供应商起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);

			//新增进货单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			//进货单列表.开单后库存,供应商积分和余额
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([slh2.trade.getPurchaseBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.进货单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.进货单列表', async function () {
			const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			//退货时数量为负数，
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data, exp), invSku2.result.data);
		});
		it('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			//console.log(exp);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		it('库存流水验证', async function () {
			// console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
	});
	describe('新增进货单，同款同颜色尺码，数量一样，既拿货又退货', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			const json = basicJson.purJson({ details: [{ num: 10 }, { num: -10 }] });
			json.details[1].colorId = json.details[0].colorId;
			json.details[1].sizeId = json.details[0].sizeId;
			//获取起始库存,供应商起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			//新增进货单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			//进货单列表.开单后库存,供应商积分和余额
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([slh2.trade.getPurchaseBillList({ searchToken: sfRes.result.data.billNo }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.进货单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.进货单列表', async function () {
			const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
		});
		it('库存流水验证', async function () {
			// console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
		it.skip('4.支付账户信息检查', async function () {
			common.isApproximatelyEqualAssert(accountList1.result.data, accountList2.result.data);
		});
		it.skip('账户流水', async function () {

		});
		it.skip('进货单列表查询条件', async function () {
			//searchToken  单据号 备注 供应商  
			// printFlag 1 已打  0 未打

		});
		it.skip('底部数据汇总', async function () {

		});
		it.skip('进货单列表排序', async function () {

		});

	});
	describe('作废进货单', function () {
		let sfRes, qfRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			const json = basicJson.purJson();
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			invSku1 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
			// console.log(`invSku1.result.data=${JSON.stringify(invSku1.result.data)}`);
			accountList1 = await slh2.fin.getAccountList();
			//作废单据
			await sspd.trend.deletePurchaseBill({
				isPend: 0,
				id: sfRes.result.data.val
			});
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
			accountList2 = await slh2.fin.getAccountList();
		});
		it('1.进货单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
			expect(qfRes.result.data.main.flag).to.equal(-1);
		});
		it('2.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			common.isApproximatelyEqualAssert(common.subObject(invSku1.result.data, exp), invSku2.result.data);
		});
		it('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			expect(invFlowList, '作废销售单后，库存流水仍然可以查到').to.be.undefined;
		});
		it('3.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			//console.log(exp);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		describe('进货单列表', function () {
			let qlRes = [];
			before(async function () {
				qlRes = await Promise.all([slh2.trade.getPurchaseBillList(), slh2.trade.getPurchaseBillList({
					jsonParam: {
						flag: 1
					}
				}),
				slh2.trade.getPurchaseBillList({
					jsonParam: {
						flag: -1
					}
				})
				]);
			});
			it('1.作废单据信息验证', async function () {
				const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(exp, qlRes[0].result.data.rows[0]);
			});
			it('2.不传flag,显示全部', async function () {
				expect(qlRes[0].result.data.total).to.equal(common.add(qlRes[1].result.data.total, qlRes[2].result.data.total))
			});
			it('3.flag=1时,只显示正常单据', async function () {
				for (let i = 0; i < qlRes[1].result.data.rows.length; i++) {
					const element = qlRes[1].result.data.rows[i];
					if (element.flag !== 1) {
						throw new Error('flag=1时,显示作废数据');
					};
				};
			});
			it('4.flag=-1时,只显示作废单据', async function () {
				for (let i = 0; i < qlRes[2].result.data.rows.length; i++) {
					const element = qlRes[2].result.data.rows[i];
					if (element.flag !== -1) {
						throw new Error('flag=-1时,显示正常数据');
					};
				};
			});
		});

	});

	describe('挂单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, invFlowListBef, dwBalance1, dwBalance2, accountList1, accountList2, getPurBillList, getPurBillListCount1, getPurBillListCount2;
		before(async () => {
			const json = basicJson.purJson({
				isPend: 1
			});
			//console.log(json);
			const promises = [slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
			slh2.acct.getBalanceAndScore({
				traderCap: 2,
				traderId: json.main.compId
			}),
			slh2.fin.getAccountList()
			];
			//获取库存流水起始值
			invFlowListBef = await slh2.inv.getInvFlowList({ tenantSpuId: json.details[0].tenantSpuId });
			//获取起始库存,供应商起始积分和余额,进货单列表count
			[invSku1, dwBalance1, accountList1] = await Promise.all(promises);
			getPurBillList = await slh2.trade.getPurchaseBillList();
			getPurBillListCount1 = getPurBillList.result.data.count;
			//新增进货单-挂单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			//console.log(sfRes);
			//进货单列表.开单后库存,供应商积分和余额
			qfRes = await slh2.trade.getPurchaseBillFull({
				isPend: 1,
				id: sfRes.result.data.val
			});
			//console.log(qfRes.result.data);
			//console.log(LOGINDATA);
			await common.delay(10000);
			[qlRes, invSku2, dwBalance2, accountList2] = await Promise.all([billReqHandler.getPurchasePendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			}), ...promises]);
			//console.log(qlRes);
			//console.log('~~~~~~~~~~~~~~~~~');
			// console.log(`qlRes.result.data[rows] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
			// console.log(`qfRes.result : ${JSON.stringify(qfRes.result)}`);
			getPurBillList = await slh2.trade.getPurchaseBillList();
			getPurBillListCount2 = getPurBillList.result.data.count;
		});
		it('1.进货单挂单详情', async function () {
			const exp = getExpByBill.getPurBillFullExp(sfRes);
			common.isApproximatelyEqualAssert(exp, qfRes.result.data);
		});
		it.skip('2.进货挂单列表', async function () {
			const exp = getExpByBill.getPurPendListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]); //挂单列表
		});
		it('3.库存检查', async function () {
			common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
		});
		it('库存流水验证', async function () {
			let invFlowListAft = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId });
			common.isApproximatelyEqualAssert(invFlowListBef.result.data, invFlowListAft.result.data);
		});
		it('4.供应商积分/余额检查', async function () {
			common.isApproximatelyEqualAssert(dwBalance1.result.data, dwBalance2.result.data);
		});
		it('5.支付账户信息检查', async function () {
			common.isApproximatelyEqualAssert(accountList1, accountList2);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			expect(accountFlowList, '新增销售单-挂单后，账户流水可以查到记录').to.be.undefined;
		});
		it('6.进货单列表查询挂单列表', async function () {
			expect(Number(getPurBillListCount2), '进货单列表查询挂单有误').to.equal(Number(getPurBillListCount1));
		});
		it('7.删除挂单', async function () {
			//console.log(qlRes.result.data);
			//console.log('~~~~~~~~~~~~~~~~~');
			let qlRes1 = await sspd.trend.getPendBillList({
				jsonParam: {
					shopId: LOGINDATA.shopId
				}
			});
			await sspd.trend.deletePurchaseBill({
				isPend: 1,
				id: sfRes.result.data.val,
			});
			let qlRes2 = await sspd.trend.getPendBillList({
				jsonParam: {
					shopId: LOGINDATA.shopId
				}
			});
			//console.log(qlRes.result.data);
			//console.log(`qlRes.result.data.count : ${JSON.stringify(qlRes.result.data.count)}`);
			//console.log(`qlRes2.result.data.count : ${JSON.stringify(qlRes2.result.data.count)}`);
			expect(common.sub(qlRes1.result.data.count, 1), '作废挂单，挂单列表显示有误').to.equal(Number(qlRes2.result.data.count));
		})
		it('8.挂单转正式单', async function () {
			let json = basicJson.purJson({
				isPend: 1
			});
			//新增挂单
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: json
			});
			let qlRes1 = await billReqHandler.getPurchasePendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			});
			delete json.isPend;
			json.pendId = sfRes.result.data.val;
			let sfResPur = await billReqHandler.savePurchaseBill({
				jsonParam: json,
			});
			let qlRes2 = await billReqHandler.getPurchasePendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			});
			//挂单转正式单，挂单列表中没有该张单据
			expect(common.sub(qlRes1.result.data.count, 1), '挂单转正式单后，挂单列表中数据有误').to.equal(Number(qlRes2.result.data.count));
			let qfResPur = await slh2.trade.getPurchaseBillFull({
				isPend: 0,
				id: sfResPur.result.data.val,
			});
			let qlResPur = await slh2.trade.getPurchaseBillList({
				jsonParam: {
					searchToken: sfResPur.result.data.billNo,
				}
			});
			let purExp = getExpByBill.getPurBillListExp(qfResPur.result.data);
			//挂单转正式单，进货单列表中有该张单据，并且和qf中一样
			common.isApproximatelyEqualAssert(purExp, qlResPur.result.data.rows[0]);
		});
	});
	for (let i = 0; i < 3; i++) {
		describe('供应商账款', function () {
			let qfRes, dwBalance1, dwBalance2, suppAcct1, suppAcct2;
			before(async () => {
				let json = basicJson.purJson();
				//起始供应商积分和对账单;
				[dwBalance1, suppAcct1] = await Promise.all([slh2.acct.getBalanceAndScore({
					traderKind: 1,
					traderCap: 2,
					traderId: json.main.compId
				}), slh2.trade.getSuppSimpleAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				})]);
				if (i == 0)
					json.payways = [];
				if (i == 2) {
					json.payways[0].money = common.add(json.payways[0].money, 10);
				} // before
				//console.log(`json : ${JSON.stringify(json)}`);
				let sfRes = await billReqHandler.savePurchaseBill({
					jsonParam: json
				});
				qfRes = await slh2.trade.getPurchaseBillFull({
					isPend: 0,
					id: sfRes.result.data.val
				});
				//after
				//开单后供应商积分余额，对账单列表
				[dwBalance2, suppAcct2] = await Promise.all([slh2.acct.getBalanceAndScore({
					traderKind: 1,
					traderCap: 2,
					traderId: json.main.compId
				}), slh2.trade.getSuppSimpleAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				})]);
			});
			it('积分、余额', async function () {
				const exp = getExpByBill.getBalanceAndScoreExp(qfRes.result.data);
				let exp2 = common.addObject(dwBalance1.result.data, exp);
				common.isApproximatelyEqualAssert(exp2, dwBalance2.result.data);
			}) //结余/积分
			it('供应商对账单', async function () {
				const exp = getExpByBill.getSuppSimpleAcctExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(exp, suppAcct2.result.data.rows[0]);
				common.isApproximatelyEqualAssert({
					total: common.add(suppAcct1.result.data.total, 1),
					beginMoney: common.sub(suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].lastBalance, suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].balance),
					endMoney: common.add(suppAcct1.result.data.endMoney, qfRes.result.data.fin.balance),
					sum: {
						payMoney: common.add(qfRes.result.data.fin.payMoney, suppAcct1.result.data.sum.payMoney),
						totalMoney: common.add(qfRes.result.data.main.totalMoney, suppAcct1.result.data.sum.totalMoney),
					},
				}, suppAcct2.result.data, ['beginMoney']);
			}) //对账单
		});
		describe('作废单据供应商账款', function () {
			let qfRes, dwBalance1, dwBalance2, suppAcct1, suppAcct2;
			before(async () => {
				let json = basicJson.purJson();
				if (i == 0)
					json.payways = [];
				if (i == 2) {
					json.payways[0].money = common.add(json.payways[0].money, 10);
				}
				let sfRes = await billReqHandler.savePurchaseBill({
					jsonParam: json
				});
				qfRes = await slh2.trade.getPurchaseBillFull({
					isPend: 0,
					id: sfRes.result.data.val
				});
				//起始供应商积分和对账单;
				[dwBalance1, suppAcct1] = await Promise.all([slh2.acct.getBalanceAndScore({
					traderKind: 1,
					traderCap: 2,
					traderId: json.main.compId
				}), slh2.trade.getSuppSimpleAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				})]);
				//作废单据后，供应商积分、余额和对账单列表
				await sspd.trend.deletePurchaseBill({
					isPend: 0,
					id: sfRes.result.data.val,
				});
				[dwBalance2, suppAcct2] = await Promise.all([slh2.acct.getBalanceAndScore({
					traderKind: 1,
					traderCap: 2,
					traderId: json.main.compId
				}), slh2.trade.getSuppSimpleAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				})]);
				//console.log(`suppAcct2 : ${JSON.stringify(suppAcct2)}`);
			});
			it('作废后供应商余额/积分', async function () {
				const exp = getExpByBill.getBalanceAndScoreExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.subObject(dwBalance1.result.data, exp), dwBalance2.result.data);
			});
			it('作废后供应商对账单', async function () {
				common.isApproximatelyEqualAssert(suppAcct1.result.data.rows[1], suppAcct2.result.data.rows[0], ['ecSeq']);
				let exp = {
					total: common.sub(suppAcct1.result.data.total, 1),
					beginMoney: common.sub(suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].lastBalance, suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].balance),
					endMoney: common.sub(suppAcct1.result.data.endMoney, qfRes.result.data.fin.balance),
					sum: {
						payMoney: common.sub(suppAcct1.result.data.sum.payMoney, qfRes.result.data.fin.payMoney),
						totalMoney: common.sub(suppAcct1.result.data.sum.totalMoney, qfRes.result.data.main.totalMoney),
					},
				};
				common.isApproximatelyEqualAssert({
					total: common.sub(suppAcct1.result.data.total, 1),
					beginMoney: common.sub(suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].lastBalance, suppAcct1.result.data.rows[suppAcct1.result.data.rows.length - 1].balance),
					endMoney: common.sub(suppAcct1.result.data.endMoney, qfRes.result.data.fin.balance),
					sum: {
						payMoney: common.sub(suppAcct1.result.data.sum.payMoney, qfRes.result.data.fin.payMoney),
						totalMoney: common.sub(suppAcct1.result.data.sum.totalMoney, qfRes.result.data.main.totalMoney),
					},
				}, suppAcct2.result.data, ['beginMoney']);
			})
		});
	};
	describe('进货单数据隔离', function () {
		before(async () => {
			await loginReq.changeShopFast({ shopName: '门店二' });
		});
		after(async () => {
			await loginReq.changeShop();
		});
		it('其他门店查询单据', async function () {
			let purList = await slh2.trade.getPurchaseBillList({ proDateGte: common.getDateString([0, 0, -7]), proDateLte: common.getCurrentDate(), }).then(res => res.result.data.rows.find(data => data.shopId != LOGINDATA.shopId));
			expect(purList).to.be.undefined;
		});
	});
});
