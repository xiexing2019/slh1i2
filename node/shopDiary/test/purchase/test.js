"use strict";
const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const loginReq = require('../../help/loginReq');

describe('进货单', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('', function () {
		let sfRes;
		before(async () => {
			sfRes = await billReqHandler.savePurchaseBill({
				jsonParam: basicJson.purJson(),
			});
		});
		it.skip('采购付款单打印', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-trade-finPurOutBill-printBill',
				jsonParam: {
					id: sfRes.result.data.val,
					// printerMac:  //打印机mac地址
					// printerType: , //打印机类型
				},
			});
		});
		it.skip('简化采购单列表', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-trade-purBill-findSimpleBill',
				wrapper: true,
				proDateGte: common.getCurrentDate(),
				proDateLte: common.getCurrentDate(),
				// printFlag: , //打印标记
				// deliverFlagIn: , //入库标识，多个逗号隔开
				// deliverFlag: , //入库标识
				// flag: , //单据状态
				// combCond: , //组合条件，支持批次号，厂商名称，单据备注
			});
		});
	});
});
