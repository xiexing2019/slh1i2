"use strict"
const common = require('../../../lib/common');
const loginReq = require('../../help/loginReq');

describe('消息中心', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe('', function () {
		it.skip('获取消息未读数', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-message-getUnreadMsgCount',
				//tagOneIn: , //消息标签

			});
		});
		it.skip('拉取消息列表', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-message-findMsgList',
				//tagOneIn: , //消息标签，多个逗号隔开

			});
		});
		it.skip('更新消息为已读', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-message-makeMsg2Read',
				//msgIds: , //消息ID逗号隔开

			});
		});
		it.skip('获取消息详情', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-message-getMessageById',
				//id: , //主键

			});
		});
	})



})
