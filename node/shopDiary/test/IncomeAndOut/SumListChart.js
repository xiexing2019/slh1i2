const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('收支汇总-online', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('列表-图表', function () {
        let SumList, SumChart, sfRes1, sfRes2, sfRes3;
        let dataArr = [common.getDateString([-1, 0, 0]), common.getDateString([0, -1, 0]), common.getDateString([0, 0, -1])];
        before(async () => {
            let json1 = basicJson.finInJson({ proDate: dataArr[0] });
            sfRes1 = await sspd.finInBill.saveFinInBill(json1);
            await common.delay(500);
            //新增去年的收入单
            let json2 = basicJson.finInJson({ proDate: dataArr[1] });
            sfRes2 = await sspd.finInBill.saveFinInBill(json2);
            await common.delay(500);
            //新增上个月的收入单
            let json3 = basicJson.finInJson({ proDate: dataArr[2] });
            sfRes3 = await sspd.finInBill.saveFinInBill(json3);
            await common.delay(500);
            //新增昨天的收入单
            SumList = await sspd.finInExpBill.finInExpSumList();
            SumChart = await sspd.finInExpBill.finInExpSumChart();
            await common.delay(3000);
            //console.log(`sfRes.result.data : ${JSON.stringify(sfRes.result.data)}`);
            //console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            //console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes)}`);
        });
        it.skip('test1', async function () {
            console.log(`SumList : ${JSON.stringify(SumList)}`);
            console.log(`SumChart : ${JSON.stringify(SumChart)}`);
        });
        it.skip('test2', async function () {
            console.log(Math.round(0.01212 * 10000) / 10000);
        });
        it('筛选条件-按年', async function () {
            const yearRes = await sspd.finInExpBill.finInExpSumList({ proDateGte: dataArr[0], proDateLte: dataArr[0] });
            //console.log(`yearRes : ${JSON.stringify(yearRes)}`);
            expect(yearRes.result.data.rows[0], '按年筛选功能异常').to.not.be.undefined;
        });
        it('筛选条件-按月', async function () {
            const monthRes = await sspd.finInExpBill.finInExpSumList({ proDateGte: dataArr[1], proDateLte: dataArr[1] });
            expect(monthRes.result.data.rows[0], '按月筛选功能异常').to.not.be.undefined;
        });
        it('筛选条件-按日', async function () {
            const dayRes = await sspd.finInExpBill.finInExpSumList({ proDateGte: dataArr[2], proDateLte: dataArr[2] });
            expect(dayRes.result.data.rows[0], '按日筛选功能异常').to.not.be.undefined;
        });
        it('列表汇总值校验', async function () {
            let expMoney = 0;
            SumList.result.data.rows.forEach(obj => expMoney += obj.money);
            //console.log(SumList.result.data.sum.money);
            //console.log(`SumList : ${JSON.stringify(expMoney)}`);
            expect(SumList.result.data.sum.money, '列表汇总值有误').to.equal(expMoney);
        });
        it('图表比率校验', async function () {
            const autoListRes = SumList.result.data.rows.find(obj => obj.catId == BASICDATA.finInCatId.codeValue);
            const autoChartRes = SumChart.result.data.rows.find(obj => obj.catId == BASICDATA.finInCatId.codeValue);
            //console.log(`SumChart : ${JSON.stringify(SumChart)}`);
            let expRatio = Math.round((autoListRes.money / SumList.result.data.sum.money) * 10000) / 10000;
            //console.log(Math.round((autoListRes.money / SumList.result.data.sum.money) * 10000) / 10000);
            //console.log(autoChartRes.ratio);
            expect(expRatio, '图表比率有误').to.equal(autoChartRes.ratio);
        });
    });
});