const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('收支-收入-online', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('新增收入单', function () {
        let sfRes, qfRes, qlRes;
        before(async () => {
            let json = basicJson.finInJson();
            //新增收入单
            sfRes = await sspd.finInBill.saveFinInBill({
                jsonParam: json
            });
            await common.delay(1000);
            //收入单详情.开单后账户流水
            qfRes = await sspd.finInExpBill.getFinInExpBillFull({
                id: sfRes.result.data.val
            });
            await common.delay(3000);
            qlRes = await sspd.finInExpBill.getFinInExpBillList().then(res => res.result.data.rows[0].oneDetDTOList.find(obj => obj.id == sfRes.result.data.val));
            //console.log(`sfRes.result.data : ${JSON.stringify(sfRes.result.data)}`);
            //console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            //console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes)}`);
        });
        it('收入单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('收支列表', async function () {
            expect(qfRes.result.data.main.totalMoney).to.equal(qlRes.totalMoney);
        });
        it('账户流水', async function () {
            this.retries(5);
            await common.delay(500);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: BASICDATA[LOGINDATA.shopName].accountList['现'].id }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            //console.log(`accountFlowList : ${JSON.stringify(accountFlowList)}`);
            //console.log(`exp : ${JSON.stringify(exp[BASICDATA[LOGINDATA.shopName].accountList['现'].id])}`);
            common.isApproximatelyEqualAssert(exp[BASICDATA[LOGINDATA.shopName].accountList['现'].id], accountFlowList);
        });
    });

    describe('修改收入单', function () {
        let sfRes, qfRes, qlRes;
        before(async () => {
            let json = basicJson.finInJson();
            //新增收入单
            sfRes = await sspd.finInBill.saveFinInBill({
                jsonParam: json
            });
            await common.delay(1000);
            let params = {
                rem: '修改收入单',
            };
            json = basicJson.finInJson(params);
            json.main.id = sfRes.result.data.val; //修改时必传单据id
            //修改收入单
            sfRes = await sspd.finInBill.saveFinInBill({
                jsonParam: json
            });
            await common.delay(1000);
            //收入单详情
            qfRes = await sspd.finInExpBill.getFinInExpBillFull({
                id: sfRes.result.data.val
            });
            //收支列表中的明细
            qlRes = await sspd.finInExpBill.getFinInExpBillList().then(res => res.result.data.rows[0].oneDetDTOList.find(obj => obj.id == sfRes.result.data.val));
        });
        it('收入单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('收支列表', async function () {
            expect(qfRes.result.data.main.totalMoney).to.equal(qlRes.totalMoney);
        });
        it('账户流水', async function () {
            this.retries(5);
            await common.delay(500);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: BASICDATA[LOGINDATA.shopName].accountList['现'].id }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            //console.log(`accountFlowList : ${JSON.stringify(accountFlowList)}`);
            //console.log(`exp : ${JSON.stringify(exp[BASICDATA[LOGINDATA.shopName].accountList['现'].id])}`);
            //账户流水里的rem字段没更新 qiuxs说不管
            common.isApproximatelyEqualAssert(exp[BASICDATA[LOGINDATA.shopName].accountList['现'].id], accountFlowList, ['rem']);
        });
    });

    describe('作废收入单', function () {
        let sfRes, qlRes;
        before(async () => {
            let json = basicJson.finInJson();
            sfRes = await sspd.finInBill.saveFinInBill({
                jsonParam: json
            });
            await common.delay(1000);
            //作废单据
            await sspd.finInBill.deleteFinInBill({ id: sfRes.result.data.val });
            await common.delay(1000);
            qlRes = await sspd.finInExpBill.getFinInExpBillList().then(res => res.result.data.rows[0].oneDetDTOList.find(obj => obj.id == sfRes.result.data.val));
        });
        it('收支列表', async function () {
            expect(qlRes, '收支列表中还能查到作废收入单的记录').to.be.undefined;
        });
        it('账户流水', async function () {
            this.retries(5);
            await common.delay(500);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: BASICDATA[LOGINDATA.shopName].accountList['现'].id }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            //console.log(`accountFlowList : ${JSON.stringify(accountFlowList)}`);
            //console.log(`exp : ${JSON.stringify(exp[BASICDATA[LOGINDATA.shopName].accountList['现'].id])}`);
            expect(accountFlowList, '账户流水中还能查到作废收入单的记录').to.be.undefined;
        });
    });

});
