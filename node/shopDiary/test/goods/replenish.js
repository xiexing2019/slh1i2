"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('补货', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginBySeq();
        await slh2.config.saveParamValue({
            ownerKind: 7,
            data: [{ code: "sales_backmat_secondsale_check", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
        });
        //开启补货验证
    });
    after(async () => {
        await slh2.config.saveParamValue({
            ownerKind: 7,
            data: [{ code: "sales_backmat_secondsale_check", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
        });
        //关闭补货验证
    });
    describe('按款号+颜色尺码', function () {
        let sfRes, styleInfo, json, json2, goodjson, returnRes1, returnJson;
        let dresRes = {};
        before(async () => {
            goodjson = basicJson.addGoodJson({ name: `补货验证${common.getRandomStr(6)}`, spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }] });
            //console.log(goodjson);
            dresRes = await slh2.dres.saveStyle({ jsonParam: goodjson });
            //新增负/0库存货品
            styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
            //获取货品详情
            json = basicJson.salesJson({ styleInfo: styleInfo });
            //console.log(json);
            json2 = _.cloneDeep(json);
            //delete json2.main.hashKey;
            //console.log(json2);
            await common.delay(1000);
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            await common.delay(500);
            returnJson = getReturnBilljson(json);
        });
        it.skip('', async function () {
            //console.log(custInfo1);
            console.log(`json : ${JSON.stringify(returnJson)}`);
            console.log(`json : ${JSON.stringify(json)}`);
        });
        it('第一次开单-补货退货验证', async function () {
            returnRes1 = await sspd.salesBill.saveSalesBill({
                jsonParam: returnJson
            });
            expect(returnRes1.result.msg, '第一次开单后退货就进行了退货验证').to.equal("保存成功!");
        });
        it.skip('购买记录查询', async function () {
            let searchInfo = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                },
            })
            expect(searchInfo.result.data.total, 'A门店人数受到B门店影响').to.equal(custInfo1.result.data.total);
        });
        describe('设置参数0', function () {
            let sfRes2, returnRes2;
            before(async () => {
                await slh2.config.saveParamValue({
                    ownerKind: 7,
                    data: [{ code: "sales_seconsale_bystyle", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
                });
                //参数开启状态
                sfRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: json2
                });
                await common.delay(500);
            });
            it('第二次开单-补货退货验证', async function () {
                returnRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: returnJson,
                    check: false
                });
                //console.log(returnRes2);
                expect(returnRes2.result.msgId, '补货退货验证失败').to.equal("style_not_allow_return");
            });
        });
    });
    describe('按款号+颜色', function () {
        let sfRes, styleInfo, json, json2, goodjson, returnRes1, returnJson;
        let dresRes = {};
        let sizeIds = [1, 2, 3, 4, 5];
        let buySize = [];
        let otherSize = [];
        before(async () => {
            goodjson = basicJson.addGoodJson({ name: `补货验证${common.getRandomStr(6)}`, spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }] });
            dresRes = await slh2.dres.saveStyle({ jsonParam: goodjson });
            //新增负/0库存货品
            styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
            //获取货品详情
            json = basicJson.salesJson({ styleInfo: styleInfo });
            json2 = _.cloneDeep(json);
            //console.log(json);
            await common.delay(1000);
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            await common.delay(500);
            returnJson = getReturnBilljson(json);
            //console.log(buySize);
            //getSizeIds(json);
            json.details.forEach(element => {
                buySize.push(element.sizeId);
            });
            //console.log(buySize);
            //console.log(sizeIds);
            otherSize = common.arrRemoveSameEle(sizeIds, buySize);
            //console.log(otherSize);

        });
        it('第一次开单-补货退货验证', async function () {
            returnRes1 = await sspd.salesBill.saveSalesBill({
                jsonParam: returnJson
            });
            expect(returnRes1.result.msg, '第一次开单后退货就进行了退货验证').to.equal("保存成功!");
        });
        it.skip('购买记录查询', async function () {
            expect(newCust1.result.data.rows[0].balance, 'B门店余额等于AB总余额').to.equal(custInfo1.result.data.rows[0].balance);
        });
        describe('设置参数1', function () {
            let sfRes2, returnRes2;
            before(async () => {
                await slh2.config.saveParamValue({
                    ownerKind: 7,
                    data: [{ code: "sales_seconsale_bystyle", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
                });
                //参数开启状态
                sfRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: json2
                });
                returnJson.details.forEach(element => {
                    element.sizeId = otherSize[0];
                });
            });
            it('第二次开单-补货退货验证', async function () {
                returnRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: returnJson,
                    check: false
                });
                expect(returnRes2.result.msgId, '补货退货验证失败').to.equal("style_not_allow_return");
            });
        });
    });
    describe('按款号', function () {
        let sfRes, styleInfo, json, json2, goodjson, returnRes1, returnJson;
        let dresRes = {};
        let sizeIds = [1, 2, 3, 4, 5];
        let buySize = [], buyColor = [];
        let otherSize = [], otherColor = [];
        before(async () => {
            goodjson = basicJson.addGoodJson({ name: `补货验证${common.getRandomStr(6)}`, spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }] });
            dresRes = await slh2.dres.saveStyle({ jsonParam: goodjson });
            //新增负/0库存货品
            styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
            //获取货品详情
            json = basicJson.salesJson({ styleInfo: styleInfo });
            json2 = _.cloneDeep(json);
            await common.delay(1000);
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            await common.delay(500);
            returnJson = getReturnBilljson(json);
            //console.log(buySize);

            //getSizeIds(json);
            json.details.forEach(element => {
                buySize.push(element.sizeId);
                buyColor.push(element.colorId);
            });
            //console.log(buySize);
            //console.log(sizeIds);
            otherSize = common.arrRemoveSameEle(sizeIds, buySize);
            otherColor = common.arrRemoveSameEle(sizeIds, buyColor);
            //console.log(otherSize);

        });
        it('第一次开单-补货退货验证', async function () {
            returnRes1 = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            expect(returnRes1.result.msg, '第一次开单后退货就进行了退货验证').to.equal("保存成功!");
        });
        it.skip('购买记录查询', async function () {
            expect(newCust1.result.data.rows[0].balance, 'B门店余额等于AB总余额').to.equal(custInfo1.result.data.rows[0].balance);
        });
        describe('设置参数2', function () {
            let sfRes2, returnRes2;
            before(async () => {
                await slh2.config.saveParamValue({
                    ownerKind: 7,
                    data: [{ code: "sales_seconsale_bystyle", domainKind: "business", ownerId: LOGINDATA.unitId, val: "2" }]
                });
                //参数开启状态
                sfRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: json2
                });
                returnJson.details.forEach(element => {
                    element.sizeId = otherSize[0];
                    element.colorId = otherColor[0];
                });
            });
            it('第二次开单-补货退货验证', async function () {
                returnRes2 = await sspd.salesBill.saveSalesBill({
                    jsonParam: returnJson,
                    check: false
                });
                expect(returnRes2.result.msgId, '补货退货验证失败').to.equal("style_not_allow_return");
            });
        });
    });
});


/**
 * 根据第一次开单json获取退货时的Jjson
 * @param {Object} json
 */

function getReturnBilljson(obj) {
    delete obj.main.hashKey;
    obj.main.totalNum = -obj.main.totalNum;
    obj.main.totalMoney = -obj.main.totalMoney;
    obj.payways[0].money = -obj.payways[0].money;
    obj.details.forEach(element => {
        element.money = - element.money;
        element.num = - element.num;
    });
    return obj;
};