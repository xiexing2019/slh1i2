"use strict";
const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');


describe('货品', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe('货品管理', function () {
		it('1.相同款号验证', async function () {
			let sfRes = await slh2.dres.saveStyle({
				jsonParam: basicJson.addGoodJson({
					code: 'agc001',
				}),
				check: false,
			});
			expect(sfRes.result.msg, `新增相同款号成功`).to.includes('款号不允许重复');
		});
		// TODO 特殊符号穷举? 
		it('2.特殊字符验证', async function () {
			let sfRes = await slh2.dres.saveStyle({
				jsonParam: basicJson.addGoodJson({
					name: '$' + common.getRandomStr(6),
				}),
				check: false,
			});
			expect(sfRes.result.msgId, '特殊字符款号新增成功').to.includes('error_exist_illegal_char');
		});
		it('3.新增款号可以开单', async function () {
			let sfRes = await slh2.dres.saveStyle({
				jsonParam: basicJson.addGoodJson(),
			});
			//console.log(sfRes);
			let styleInfo = await slh2.dres.getStyleInfo({
				id: sfRes.result.data.val,
			});
			//console.log(styleInfo);
			await common.delay(8000);
			await billReqHandler.savePurchaseBill({
				jsonParam: basicJson.purJson({
					styleInfo: styleInfo.result.data
				}),
			});
			await common.delay(8000);
			await sspd.salesBill.saveSalesBill({
				jsonParam: basicJson.salesJson({
					styleInfo: styleInfo.result.data,
				}),
			});
			await common.delay(8000);
		});
		describe('款号增改查停用启用', function () {
			let sfRes, styleInfo, purList1, areaDiaryStockNum1, areaDiaryStockNum2, trendDiaryStockNum1, trendDiaryStockNum2;
			before(async () => {
				//起始值
				areaDiaryStockNum1 = await sspd.area.areaDiary();
				trendDiaryStockNum1 = await sspd.trend.trendDiary();
				purList1 = await slh2.trade.getPurchaseBillList({ pageSize: 0 });
				//新增款号
				let json = basicJson.addGoodJson({
					spuStockNum: 3, //生成3条起始库存明细
				});
				sfRes = await slh2.dres.saveStyle(json, true);

				await common.delay(8000); //

				styleInfo = await slh2.dres.getStyleInfo({
					id: sfRes.result.data.val,
				});
				// console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
				areaDiaryStockNum2 = await sspd.area.areaDiary();
				trendDiaryStockNum2 = await sspd.trend.trendDiary();
			});
			it('1.款号详情', async function () {
				common.isApproximatelyEqualAssert(sfRes.params.jsonParam, styleInfo.result.data, ['spuStockNum']);
				common.isApproximatelyArrayAssert(sfRes.params.jsonParam.spuStockNum, styleInfo.result.data.spuStockNum);
			});
			it('2.款号列表查询', async function () {
				let searchToken = [styleInfo.result.data.spuCommonDto.name, styleInfo.result.data.spuCommonDto.code, styleInfo.result.data.spuExtraDto.rem, styleInfo.result.data.spuBarcodes[0].barcode];
				for (let i = 0; i < searchToken.length; i++) {
					let qlRes = await slh2.dres.getStyleInfoList({
						jsonParam: {
							searchToken: searchToken[i]
						},
					});
					common.isApproximatelyEqualAssert(styleInfo.result.data.spuCommonDto, qlRes.result.data.rows[0]);

				};
			});
			it('进货单列表查询', async function () {
				let purList2 = await slh2.trade.getPurchaseBillList({ pageSize: 0 });
				expect(purList2.result.data.rows.length).to.equal(purList1.result.data.rows.length);
			});
			it('库存流水', async function () {
				let invList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.result.data.val });
				let exp = {
					// remainNum: 64,暂时不管
					bizType: 3101,
					code: sfRes.params.jsonParam.spuCommonDto.code,
					name: sfRes.params.jsonParam.spuCommonDto.name,
					bizDate: sfRes.params.jsonParam.spuCommonDto.marketDate,
					bizTypeCaption: "建款入库",
					addNum: 0,//重新计算
					classId: sfRes.params.jsonParam.spuExtraDto.classId || 0,
					rem: '',
					inOutType: 2,//出库 1 入库 2
					tenantSpuId: sfRes.result.data.val,
					billId: 0,
					unitId: LOGINDATA.unitId,
					shopId: LOGINDATA.shopId,
					billNo: 0,
					skuFlows: [],
				};
				sfRes.params.jsonParam.spuStockNum.forEach(ele => {
					exp.skuFlows.unshift({
						colorId: ele.colorId,
						sizeId: ele.sizeId,
						num: ele.stockNum,
						inOutType: 2,
						// remainNum:ele.stockNum
					});
					exp.addNum = common.add(exp.addNum, ele.stockNum);
				});
				common.isApproximatelyEqualAssert(exp, invList.result.data.rows[0]);

			});
			it('3.area-diary验证', async function () {
				let exp = { addNum: 0 };
				styleInfo.result.data.spuStockNum.forEach(obj => exp.addNum = common.add(exp.addNum, obj.stockNum));
				exp.stockNum = exp.addNum;
				exp.stockMoney = common.mul(exp.addNum, styleInfo.result.data.purprices[0].purPrice);
				console.log(areaDiaryStockNum1.result.data.invDiary);
				console.log(exp);
				console.log(areaDiaryStockNum2.result.data.invDiary);
				common.isApproximatelyEqualAssert(common.addObject(areaDiaryStockNum1.result.data.invDiary, exp), areaDiaryStockNum2.result.data.invDiary);
			});
			it.skip('4.trend-diary验证', async function () {
				const exp = getExpByBill.getTrendDiary(styleInfo.result.data);
				common.isApproximatelyEqualAssert(common.add(trendDiaryStockNum1.result.invQuota, exp), trendDiaryStockNum2.result.invQuota);
			});
			describe('5.修改款号', function () {
				before(async () => {
					let params = {
						code: 'changecode' + common.getRandomStr(6),
						name: '修改name' + common.getRandomStr(6),
						rem: '比较随意',
						brandId: BASICDATA.brandIds[1].codeValue,
						classId: BASICDATA.classIds[0].id,
						action: 'edit',
					};
					let json = basicJson.addGoodJson(params);
					json.spuCommonDto.id = sfRes.result.data.val;
					sfRes = await slh2.dres.saveStyle({
						jsonParam: json
					});
					styleInfo = await slh2.dres.getStyleInfo({
						id: sfRes.result.data.val,
					});
				});
				it('款号详情', async function () {
					//console.log(BASICDATA.brandIds);
					common.isApproximatelyEqualAssert(sfRes.params.jsonParam, styleInfo.result.data);
				});
				it('款号列表查询', async function () {
					let searchToken = [styleInfo.result.data.spuCommonDto.name, styleInfo.result.data.spuCommonDto.code, styleInfo.result.data.spuExtraDto.rem, styleInfo.result.data.spuBarcodes[0].barcode];
					for (let i = 0; i < searchToken.length; i++) {
						let qlRes = await slh2.dres.getStyleInfoList({
							jsonParam: {
								marketDateStart: common.getCurrentDate(),
								marketDateEnd: common.getCurrentDate(),
								searchToken: searchToken[i]
							},
						});
						common.isApproximatelyEqualAssert(styleInfo.result.data.spuCommonDto, qlRes.result.data.rows[0]);
					};
				});
			});
			describe('4.停用启用款号--停用后款号追加日期', function () {
				it.skip('1.停用，启用款号', async function () {
					let styleInfoBef = await await slh2.dres.getStyleInfo({
						id: styleInfo.result.data.spuCommonDto.id,
					});
					await slh2.dres.disableStyle({
						ids: styleInfo.result.data.spuCommonDto.id
					});
					styleInfo = await slh2.dres.getStyleInfo({
						id: styleInfo.result.data.spuCommonDto.id,
					});
					styleInfoBef.result.data.spuCommonDto.flag = 0;
					styleInfoBef.result.data.spuCommonDto.code = `${sfRes.params.jsonParam.spuCommonDto.code + '_' + common.getCurrentDate()}`;
					styleInfoBef.result.data.spuCommonDto.ecCaption.flag = '停用';
					common.isApproximatelyEqualAssert(styleInfoBef.result.data, styleInfo.result.data);
					let qlRes = await slh2.dres.getStyleInfoList({
						jsonParam: {
							searchToken: styleInfo.result.data.spuCommonDto.code,
						},
					});
					expect(Number(qlRes.result.data.count), '停用款号后列表查询是否启用为启用仍然可以查到该款号').to.equal(0);
					qlRes = await slh2.dres.getStyleInfoList({
						jsonParam: {
							searchToken: styleInfo.result.data.spuCommonDto.code,
							flag: 0,
						},
					});
					expect(Number(qlRes.result.data.count), '停用款号后列表查询是否启用为停用查不到该款号').to.equal(1);
				});
				it.skip('2.启用款号', async function () {
					let styleInfoBef = await await slh2.dres.getStyleInfo({
						id: styleInfo.result.data.spuCommonDto.id,
					});
					await slh2.dres.enableStyle({
						ids: styleInfo.result.data.spuCommonDto.id,
					});
					styleInfo = await slh2.dres.getStyleInfo({
						id: styleInfo.result.data.spuCommonDto.id,
					});
					styleInfoBef.result.data.spuCommonDto.flag = 1;
					styleInfoBef.result.data.spuCommonDto.code = sfRes.params.jsonParam.spuCommonDto.code;
					styleInfoBef.result.data.spuCommonDto.ecCaption.flag = '正常';
					common.isApproximatelyEqualAssert(styleInfoBef.result.data, styleInfo.result.data);
					// expect(Number(styleInfo.result.data.spuCommonDto.flag), '启用用款号后flag没有变成1').to.equal(1);
				});

			});
			describe.skip('数据隔离', function () {
				let noShareStyle;
				before(async () => {
					noShareStyle = await slh2.dres.saveStyle(basicJson.addGoodJson({ code: `noShare${common.getRandomStr(6)}`, isPublic: 0 }));
					// console.log(`noShareStyle=${JSON.stringify(noShareStyle)}`);
				});
				afterEach(async () => {
					await loginReq.changeShop({ shopName: '常青店' });
				});
				it.skip('款号--同步到多门店', async function () {
					let styleListShop1 = await slh2.dres.getStyleInfoList({ jsonParam: { code: styleInfo.result.data.spuCommonDto.code, } });
					await mainReqHandler.changeShopFast({ shopName: '门店二' });
					let styleListShop2 = await slh2.dres.getStyleInfoList({ jsonParam: { code: styleInfo.result.data.spuCommonDto.code, } });
					common.isApproximatelyEqualAssert(styleListShop1.result, styleListShop2.result, ['shopId', 'invNum']);

					common.isApproximatelyEqualAssert({ invNum: 0, shopId: LOGINDATA.shopId }, styleListShop2.result.data.rows[0]);
				});
				it('款号不同步到多门店', async function () {
					await mainReqHandler.changeShopFast({ shopName: '门店二' });
					let styleListShop2 = await slh2.dres.getStyleInfoList({ jsonParam: { code: noShareStyle.params.jsonParam.spuCommonDto.code, } });
					expect(styleListShop2.result.data.rows.length).to.equal(0);
				});
				it('其他门店新增同名款号', async function () {
					await mainReqHandler.changeShopFast({ shopName: '门店二' });
					let sfStyle = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({ code: noShareStyle.params.jsonParam.spuCommonDto.code }), check: false });
					expect(sfStyle.result).to.includes({ msg: `款号不允许重复[${noShareStyle.params.jsonParam.spuCommonDto.code}]`, msgId: "style_code_not_allow_repeat" });
				});
			});
		});

		describe.skip('新增货品直接入库是否入账', function () {
			describe('入账', function () {
				before(async () => {
					//新增入账的货品
					//查询采购单列表 和详情
					//厂商账款--对账单

				});
				it('', async function () {
					let sfRes, qfRes, purBillListRes, supAcct1, supAcct2;
					let json = basicJson.addGoodJson({
						spuStockNum: 3, //生成3条起始库存明细
						stdprice1: 400,
					});
					console.log(`json=${JSON.stringify(json)}`);
					sfRes = await slh2.dres.saveStyle({ jsonParam: json, });
					console.log(`新增货品=${JSON.stringify(sfRes)}`);
					await common.delay(3000);//更新的很慢
					purBillListRes = await slh2.trade.getPurchaseBillList().then(res => res.result.data.rows[0]);
					qfRes = await slh2.trade.getPurchaseBillFull({
						isPend: 0,
						id: purBillListRes.id,
					});
					supAcct1 = await slh2.trade.getSuppSimpleAcctCheck({
						shopId: LOGINDATA.shopId,
						compId: json.purprices[0].orgSuppId,
					})
					console.log(`purBilllistRes=${JSON.stringify(purBillListRes)}`);
					console.log(`qfRes=${JSON.stringify(qfRes)}`);
					let purBillInfoExp = getExpByBill.getInitPurBillInfoExp(sfRes);
					console.log(`exp=${JSON.stringify(exp)}`);
					common.isApproximatelyEqualAssert(purBillInfoExp, qfRes.result.data);
					let supAcctExp = getExpByBill.getSuppSimpleAcctExp(qfRes.result.data);
				});
				it('采购单详情', async function () {

				});
				it('采购单列表', async function () {

				});
				it('厂商账款', async function () {

				});
			});
			describe('异常流', function () {
				it('有进货价有库存无厂商', async function () {
					let json = basicJson.addGoodJson({
						spuStockNum: 3, //生成3条起始库存明细
						dwid: '', //厂商为空
					});
					let sfRes = await slh2.dres.saveStyle({ jsonParam: json, check: false });
					expect(sfRes.result, '新增进货价不为0有起始库存厂商为空的款号成功').to.includes({ "msg": "货品有进货价并录入库存之后，必须选择厂商！", "msgId": "when_has_pured_dwxx_can_be_null" });
				});
				it('无进货价有库存有厂商', async function () {
					let purList1 = await slh2.trade.getPurchaseBillList({ pageSize: 0, });
					let json = basicJson.addGoodJson({
						spuStockNum: 3, //生成3条起始库存明细
						purPrice: 0, //厂商为空
					});
					await slh2.dres.saveStyle({ jsonParam: json });
					let purList2 = await slh2.trade.getPurchaseBillList({ pageSize: 0, });
				});
			});
		});
	});
});

/**
 * 新增货品入账验证
 * @param {object} params
 * @param {object} params.spuStockNum 起始库存
 * @param {object} params.dwid  厂商
 * @param {object} params.purPrice  进货价
 */
async function isDirectInvCheck(params) {
	//directInv为true 直接入库  
	let json = basicJson.addGoodJson({
		params
	});
	if (params.hasOwnPerperty('directInv')) {
		delete json.directInv;
		//获取采购单列表
		let purBillList1 = await slh2.trade.getPurchaseBillList();
		//新增货品
		let sfRes = await slh2.dres.saveStyle({ jsonParam: json, check: false });
		let purBillList2 = await slh2.trade.getPurchaseBillList();
		expect(sfRes.result.data, '新增货品失败').to.includes.keys('val');
		common.isApproximatelyEqualAssert(purBillList1.result.data, purBillList2.result.data, `直接入库生成采购单params=${sfRes.params}`);
	} else {
		//记账入库


	};

};