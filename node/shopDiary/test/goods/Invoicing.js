const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe('商品进销存数量', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
        //styleInfo = await styleReqHandler.getStyleInfo({ id: 470613 }).then((res) => res.result.data);
    });
    describe('销售', function () {
        let json, sfRes, qfRes, invoiceBef, invoiceAft;
        before(async () => {
            json = basicJson.salesJson();
            //json = basicJson.salesJson({ styleInfo: styleInfo });
            //获取起始库存,客户起始积分和余额
            invoiceBef = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //console.log(sfRes);
            //销售单详情.开单后库存,客户积分和余额
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //console.log(`qfRes=${JSON.stringify(qfRes)}`);
            await common.delay(10000);
            invoiceAft = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
        });
        it('商品销售数', async function () {
            expect(qfRes.result.data.main.totalNum, '进销存销售数异常').to.equal(invoiceAft.result.data.salesNum - invoiceBef.result.data.salesNum);
        });
        it('商品库存', async function () {
            expect(qfRes.result.data.main.totalNum, '进销存库存异常').to.equal(invoiceBef.result.data.stockNum - invoiceAft.result.data.stockNum);
        });
        describe('作废单据', function () {
            let invoiceDel;
            before(async () => {
                await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
                await common.delay(500);
                //作废单据后
                invoiceDel = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            });
            it('商品销售数', async function () {
                expect(invoiceBef.result.data.salesNum, '进销存销售数异常').to.equal(invoiceDel.result.data.salesNum);
            });
            it('商品库存', async function () {
                expect(invoiceBef.result.data.stockNum, '进销存库存异常').to.equal(invoiceDel.result.data.stockNum);
            });
        });
    });
    describe('采购', function () {
        let json, sfRes, qfRes, invoiceBef, invoiceAft;
        before(async () => {
            json = basicJson.purJson();
            //获取起始库存,客户起始积分和余额
            invoiceBef = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            //新增采购单
            sfRes = await billReqHandler.savePurchaseBill({
                jsonParam: json
            });
            qfRes = await slh2.trade.getPurchaseBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //console.log(`qfRes=${JSON.stringify(qfRes)}`);
            await common.delay(10000);
            invoiceAft = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
        });
        it('商品采购数', async function () {
            expect(qfRes.result.data.main.totalNum, '进销存采购数异常').to.equal(invoiceAft.result.data.purNum - invoiceBef.result.data.purNum);
        });
        it('商品库存', async function () {
            expect(qfRes.result.data.main.totalNum, '进销存库存异常').to.equal(invoiceAft.result.data.stockNum - invoiceBef.result.data.stockNum);
        });
        describe('作废单据', function () {
            let invoiceDel;
            before(async () => {
                await slh2.trade.deletePurchaseBill({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(1000);
                //作废单据后
                invoiceDel = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            });
            it('商品采购数', async function () {
                expect(invoiceBef.result.data.purNum, '进销存采购数异常').to.equal(invoiceDel.result.data.purNum);
            });
            it('商品库存', async function () {
                expect(invoiceBef.result.data.stockNum, '进销存库存异常').to.equal(invoiceDel.result.data.stockNum);
            });
        });
    });
    describe('退货', function () {
        let json, sfRes, qfRes, invoiceBef, invoiceAft;
        before(async () => {
            json = basicJson.salesJson({
                details: [{
                    num: -5,
                }, {
                    num: -10
                }]
            });
            invoiceBef = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            invoiceAft = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
        });
        it('商品退货数', async function () {
            expect(-qfRes.result.data.main.totalNum, '进销存退货数异常').to.equal(invoiceAft.result.data.returnNum - invoiceBef.result.data.returnNum);
        });
        it('商品库存', async function () {
            expect(-qfRes.result.data.main.totalNum, '进销存库存异常').to.equal(invoiceAft.result.data.stockNum - invoiceBef.result.data.stockNum);
        });
        describe('作废单据', function () {
            let invoiceDel;
            before(async () => {
                await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
                await common.delay(500);
                //作废单据后
                invoiceDel = await slh2.inv.findInvoicingNum({ tenantSpuId: json.details[0].tenantSpuId });
            });
            it('商品销售数', async function () {
                expect(invoiceBef.result.data.returnNum, '进销存销售数异常').to.equal(invoiceDel.result.data.returnNum);
            });
            it('商品库存', async function () {
                expect(invoiceBef.result.data.stockNum, '进销存库存异常').to.equal(invoiceDel.result.data.stockNum);
            });
        });
    });
    describe('调出', function () {
        let json, moveOutBill, qfRes, invoiceBef, invoiceAft;
        before(async function () {
            json = await basicJson.moveOutJson();
            invoiceBef = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
            moveOutBill = await slh2.inv.saveMoveOutBill(json);
            qfRes = await slh2.inv.getMoveBillFull({ id: moveOutBill.result.data.val });
            invoiceAft = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
        });
        it('商品调出数', async function () {
            expect(qfRes.result.data.totalNum, '进销存调出数异常').to.equal(invoiceAft.result.data.moveOutNum - invoiceBef.result.data.moveOutNum);
        });
        it('商品库存', async function () {
            expect(qfRes.result.data.totalNum, '进销存库存异常').to.equal(invoiceBef.result.data.stockNum - invoiceAft.result.data.stockNum);
        });
        describe('作废单据', function () {
            let invoiceDel;
            before(async () => {
                await slh2.inv.cancelMoveOutBill({ id: moveOutBill.result.data.val });
                await common.delay(1000);
                //作废单据后
                invoiceDel = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
            });
            it('商品调出数', async function () {
                expect(invoiceBef.result.data.moveOutNum, '进销存调出数异常').to.equal(invoiceDel.result.data.moveOutNum);
            });
            it('商品库存', async function () {
                expect(invoiceBef.result.data.stockNum, '进销存库存异常').to.equal(invoiceDel.result.data.stockNum);
            });
        });
    });
    describe('调入', function () {
        let json, moveOutBill, moveBillList, qfRes, invoiceBef, invoiceAft;
        before(async function () {
            json = await basicJson.moveOutJson();
            moveOutBill = await slh2.inv.saveMoveOutBill(json);
            await loginReq.changeShopFast({ shopName: '门店二' });
            await common.delay(500);
            invoiceBef = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
            moveBillList = await slh2.inv.getMoveBillList({ inOutType: 2 }).then(res => res.result.data.rows.find(obj => obj.outBillId == moveOutBill.result.data.val));
            console.log(moveBillList);
            qfRes = await slh2.inv.getMoveBillFull({ id: moveBillList.id });
            await slh2.inv.saveMoveInBill(qfRes.result.data);
            //接受调入单
            await common.delay(500);
            invoiceAft = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
        });
        it('商品调入数', async function () {
            expect(qfRes.result.data.totalNum, '进销存调出数异常').to.equal(invoiceAft.result.data.moveInNum - invoiceBef.result.data.moveInNum);
        });
        it('商品库存', async function () {
            expect(qfRes.result.data.totalNum, '进销存库存异常').to.equal(invoiceAft.result.data.stockNum - invoiceBef.result.data.stockNum);
        });
        describe('作废单据', function () {
            let invoiceDel;
            before(async () => {
                await slh2.inv.cancelMoveInBill({ id: moveBillList.id });
                await common.delay(1000);
                //作废单据后
                invoiceDel = await slh2.inv.findInvoicingNum({ tenantSpuId: json.billDetails[0].tenantSpuId });
            });
            after(async () => {
                await slh2.inv.cancelPreinMoveBill({ id: moveBillList.id });
                //撤销待入库单
                await loginReq.changeShop();
                //切回常青店
                await common.delay(1000);
            });
            it('商品调入数', async function () {
                expect(invoiceBef.result.data.moveInNum, '进销存调入数异常').to.equal(invoiceDel.result.data.moveInNum);
            });
            it('商品库存', async function () {
                expect(invoiceBef.result.data.stockNum, '进销存库存异常').to.equal(invoiceDel.result.data.stockNum);
            });
        });
    });
});