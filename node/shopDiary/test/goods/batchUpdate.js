"use strict";
const common = require('../../../lib/common');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('批量修改货品属性-online', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('批量特价', function () {
        //批量调整货品属性后同步商城
        let idsCollection = [];
        let spuIds, style1, style2, Befstyle1, Befstyle2, qlRes, qlRes2;
        before(async () => {
            qlRes = await slh2.dres.getStyleInfoList({
                jsonParam: {
                    special: 0
                },
            });
            //console.log(qlRes.result.data.total);
            const getGoods = qlRes.result.data.rows.slice(0, 2);

            getGoods.forEach(obj => {
                idsCollection.push(obj.tenantSpuId);
            });
            console.log(idsCollection.toString());
            spuIds = idsCollection.toString();
            Befstyle1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
            Befstyle2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
            await slh2.dres.batchUpdateProp({ tenantSpuIds: spuIds });
            //批量设置特价
            await common.delay(500);
            style1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
            style2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
            qlRes2 = await slh2.dres.getStyleInfoList({
                jsonParam: {
                    tenantSpuIds: spuIds
                },
            });
            //await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify()}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify()}`);
        });
        it('test', async function () {

        });
        it('批量设为特价前-列表信息', async function () {
            const ql1 = qlRes.result.data.rows.find(obj => obj.tenantSpuId == idsCollection[0]);
            const ql2 = qlRes.result.data.rows.find(obj => obj.tenantSpuId == idsCollection[1]);
            //console.log(ql1);
            //console.log(ql2);
            expect(ql1.special).to.equal(0);
            expect(ql2.special).to.equal(0);
        });
        it('批量设为特价前-货品详情', async function () {
            expect(Befstyle1.result.data.spuCommonDto.special).to.equal(0);
            expect(Befstyle2.result.data.spuCommonDto.special).to.equal(0);
        });
        it('批量设为特价后-货品详情', async function () {
            expect(style1.result.data.spuCommonDto.special).to.equal(1);
            expect(style2.result.data.spuCommonDto.special).to.equal(1);
        });
        it('批量设为特价后-列表信息', async function () {
            expect(qlRes2.result.data.rows[0].special).to.equal(1);
            expect(qlRes2.result.data.rows[1].special).to.equal(1);
        });
        describe('批量特价-还原为非特价', function () {
            let Aftstyle1, Aftstyle2, qlRes3;
            before(async () => {
                await slh2.dres.batchUpdateProp({ tenantSpuIds: spuIds, val: 0 });
                //批量还原为非特价
                await common.delay(500);
                Aftstyle1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
                Aftstyle2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
                qlRes3 = await slh2.dres.getStyleInfoList({
                    jsonParam: {
                        tenantSpuIds: spuIds
                    },
                });
            });
            it('还原为非特价后-列表信息', async function () {
                expect(qlRes3.result.data.rows[0].special).to.equal(0);
                expect(qlRes3.result.data.rows[1].special).to.equal(0);
            });
            it('还原为非特价后-货品详情', async function () {
                expect(Aftstyle1.result.data.spuCommonDto.special).to.equal(0);
                expect(Aftstyle2.result.data.spuCommonDto.special).to.equal(0);
            });
        });
    });
    describe.skip('批量折扣', function () {
        //没有需求 服务端功能已完成
    });
    describe('批量调价', function () {
        describe('统一价格', function () {
            let idsCollection = [];
            let spuIds, style1, style2, qlRes, qlRes2, adjustRes;
            before(async () => {
                qlRes = await slh2.dres.getStyleInfoList();
                //console.log(qlRes.result.data.total);
                const getGoods = qlRes.result.data.rows.slice(0, 2);

                getGoods.forEach(obj => {
                    idsCollection.push(obj.tenantSpuId);
                });
                console.log(idsCollection.toString());
                spuIds = idsCollection.toString();
                adjustRes = await slh2.dres.adjustPriceInBatch({ spuIds: spuIds });
                //批量设置特价
                await common.delay(500);
                style1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
                style2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
                qlRes2 = await slh2.dres.getStyleInfoList({
                    jsonParam: {
                        tenantSpuIds: spuIds
                    },
                });
            });
            it('批量调价后-货品详情', async function () {
                expect(style1.result.data.spuCommonDto.stdprice1).to.equal(300);
                expect(style2.result.data.spuCommonDto.stdprice1).to.equal(300);
            });
            it('批量调价后-列表信息', async function () {
                expect(qlRes2.result.data.rows[0].stdprice1).to.equal(300);
                expect(qlRes2.result.data.rows[1].stdprice1).to.equal(300);
            });
            it('调价记录列表查询', async function () {
                const qladjust = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust).to.not.be.undefined;
            });
            it('作废调价记录后-调价记录列表查询', async function () {
                await slh2.dres.adjustPriceRevoke({ id: adjustRes.result.data.val });
                const qladjust2 = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust2).to.be.undefined;
            });
        });
        describe('批量加减', function () {
            let idsCollection = [];
            let spuIds, style1, style2, qlRes, qlRes2, adjustRes;
            before(async () => {
                qlRes = await slh2.dres.getStyleInfoList();
                //console.log(qlRes.result.data.total);
                const getGoods = qlRes.result.data.rows.slice(0, 2);

                getGoods.forEach(obj => {
                    idsCollection.push(obj.tenantSpuId);
                });
                console.log(idsCollection.toString());
                spuIds = idsCollection.toString();
                adjustRes = await slh2.dres.adjustPriceInBatch({ spuIds: spuIds, typeid: 1, stdprice1: 100 });
                //批量设置特价
                await common.delay(500);
                style1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
                style2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
                qlRes2 = await slh2.dres.getStyleInfoList({
                    jsonParam: {
                        tenantSpuIds: spuIds
                    },
                });
            });
            it('批量调价后-货品详情', async function () {
                expect(style1.result.data.spuCommonDto.stdprice1).to.equal(qlRes.result.data.rows[0].stdprice1 + 100);
                expect(style2.result.data.spuCommonDto.stdprice1).to.equal(qlRes.result.data.rows[1].stdprice1 + 100);
            });
            it('批量调价后-列表信息', async function () {
                expect(qlRes2.result.data.rows[0].stdprice1).to.equal(qlRes.result.data.rows[0].stdprice1 + 100);
                expect(qlRes2.result.data.rows[1].stdprice1).to.equal(qlRes.result.data.rows[1].stdprice1 + 100);
            });
            it('调价记录列表查询', async function () {
                const qladjust = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust).to.not.be.undefined;
            });
            it('作废调价记录后-调价记录列表查询', async function () {
                await slh2.dres.adjustPriceRevoke({ id: adjustRes.result.data.val });
                const qladjust2 = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust2).to.be.undefined;
            });
        });
        describe('批量乘以', function () {
            let idsCollection = [];
            let spuIds, style1, style2, Befstyle1, Befstyle2, qlRes, qlRes2, adjustRes;
            before(async () => {
                qlRes = await slh2.dres.getStyleInfoList();
                //console.log(qlRes.result.data.total);
                const getGoods = qlRes.result.data.rows.slice(0, 2);

                getGoods.forEach(obj => {
                    idsCollection.push(obj.tenantSpuId);
                });
                console.log(idsCollection.toString());
                spuIds = idsCollection.toString();
                Befstyle1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
                Befstyle2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
                adjustRes = await slh2.dres.adjustPriceInBatch({ spuIds: spuIds, typeid: 2, stdprice1: 0.5, stdprice2: 1.5, stdprice3: 1, stdprice4: 1, stdprice5: 1 });
                //批量设置特价
                await common.delay(500);
                style1 = await slh2.dres.getStyleInfo({ id: idsCollection[0] });
                style2 = await slh2.dres.getStyleInfo({ id: idsCollection[1] });
                qlRes2 = await slh2.dres.getStyleInfoList({
                    jsonParam: {
                        tenantSpuIds: spuIds
                    },
                });
            });
            it('批量调价后-货品详情', async function () {
                expect(style1.result.data.spuCommonDto.stdprice1).to.equal(Befstyle1.result.data.spuCommonDto.stdprice1 * 0.5);
                expect(style2.result.data.spuCommonDto.stdprice2).to.equal(Befstyle2.result.data.spuCommonDto.stdprice2 * 1.5);
            });
            it('批量调价后-列表信息', async function () {
                expect(qlRes2.result.data.rows[0].stdprice1).to.equal(qlRes.result.data.rows[0].stdprice1 * 0.5);
                expect(qlRes2.result.data.rows[1].stdprice2).to.equal(qlRes.result.data.rows[1].stdprice2 * 1.5);
            });
            it('调价记录列表查询', async function () {
                const qladjust = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust).to.not.be.undefined;
            });
            it('作废调价记录后-调价记录列表查询', async function () {
                await slh2.dres.adjustPriceRevoke({ id: adjustRes.result.data.val });
                const qladjust2 = await slh2.dres.adjustPriceInBatchList({ tenantSpuIds: spuIds }).then(res => res.result.data.rows.find(obj => obj.id == adjustRes.result.data.val));
                expect(qladjust2).to.be.undefined;
            });
        });
    });
});
