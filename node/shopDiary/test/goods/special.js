"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const sspdConfig = require('../../../reqHandler/shopDiary/sspdConfig');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('特价货品', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    after(async () => {

    });
    describe('', function () {
        let sfRes, balanceBef, balanceAft, styleInfo, json, goodjson, res;
        let dresRes = {};
        before(async function () {
            goodjson = basicJson.addGoodJson({ name: `特价货品${common.getRandomStr(6)}`, special: true });
            dresRes = await slh2.dres.saveStyle({ jsonParam: goodjson });
            //新增负/0库存货品
            styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
            //获取货品详情
            json = basicJson.salesJson({ styleInfo: styleInfo });
            balanceBef = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            //客户起始积分和余额
            await slh2.config.saveParamValue({
                ownerKind: 7,
                data: [{ code: "sales_special_stdprice", domainKind: "business", ownerId: LOGINDATA.unitId, val: "2" }]
            });
            await common.delay(1000);
            //后台修改适用价(第二个价格)
            res = await sspdConfig.getParamInfo({
                productType: "slh", version: "1.0.0", code: "sales_special_stdprice", domainKind: "business", ownerId: LOGINDATA.unitId
            });
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json,
            });
        });
        it.skip('t', async function () {
            console.log(goodjson);
            console.log(dresRes);
            //console.log(styleInfo);
            //console.log('******************');
            //console.log(json);
        });
        it('后台参数（特价商品适用价）校验', async function () {
            //查询
            expect(res.result.data.val, '参数异常').to.equal('2');
        });
        it('特价货品按钮校验', async function () {
            //特价货品详情
            //默认special为0
            expect(styleInfo.spuCommonDto.special, '参数异常').to.equal(1);
        });
        it.skip('特价货品自定义校验', async function () {
            //ec-ui-selfView-saveSspdSelfView
            //展示不展示按钮
            //uiView
            expect(uiView.result.msg, '参数异常').to.equal('50');
        });
        it('特价货品不积分校验', async function () {
            balanceAft = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            //特价货品购买，退货 不加减积分
            //先是特价又改了
            expect(balanceAft.result.data.score, '积分异常').to.equal(balanceBef.result.data.score);
        });
        it.skip('特价货品不打折校验', async function () {
            //三种折扣模式
            //特价货品下单
            //最后按特价计算
            //目前服务端为限制
            expect(sfRes.result.data, '参数异常').to.equal('50');
        });
        it('特价货品不受上次价控制校验', async function () {
            const lastPrice1 = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
            // console.log(`lastPrice=${JSON.stringify(lastPrice)}`);
            expect(lastPrice1.result.data.rows[0]).to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: '' });
            //开启上次价情况下
            //先买一个不是特价的货品，记下他的上次价
            //设他为特价
            //调接口看他有没有上次价
            //开单时改价格，看价格
        });
        it('特价改成不特价，上次价', async function () {
            Object.assign(goodjson.spuCommonDto, { special: false, id: dresRes.result.data.tenantSpuId });
            await slh2.dres.saveStyle({ jsonParam: goodjson });
            const lastPrice2 = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
            // console.log(`lastPrice=${JSON.stringify(lastPrice)}`);
            expect(lastPrice2.result.data.rows[0]).to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: dresRes.params.jsonParam.spuCommonDto.stdprice2 });
        });
        it.skip('特价货品选款界面特价标记校验', async function () {
            //选款界面 数据同步接口special为1即为特价款
            expect(paramInfo.result.data.favor_money_ceil, '参数异常').to.equal('50');
        });
        it.skip('特价货品打印校验', async function () {
            //ec-sspd-salesBill-printbill接口
            //验证xml文件特，特赠
            expect(paramInfo.result.data.favor_money_ceil, '参数异常').to.equal('50');
        });
    });
});
