"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2/index');
const loginReq = require('../../help/loginReq');

describe('主数据', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe.skip('款号', function () {
		let sfRes;
		before(async () => {
			sfRes = await common.apiDo({
				apiKey: 'ec-dres-spu-save',
				jsonParam: basicJson.addGoodJson({
					spuStockNum: 3
				}),
			});
		});
		it('款号详情', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-getBeanById',
				id: sfRes.result.data.val,
				wrapper: true,
			});
			// console.log(rs.result.data);

		});
		it('款号停用', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-disable',
				ids: sfRes.result.data.val,
				shopId: LOGINDATA.shopId,
			});
		});
		it('款号启用', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-enable',
				ids: sfRes.result.data.val,
				shopId: LOGINDATA.shopId,
			});
		});

		it('款号名称搜索', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-name-list',
				searchToken: sfRes.params.jsonParam.spuCommonDto.code, //搜索串，匹配商品名称、款号、备注
			});
			//console.log(`rs : ${JSON.stringify(rs)}`);
		});

		it.skip('设置款号封面', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-update-imgs',
				id: sfRes.result.data.val,
				//docIds: ,//图片列表，逗号分隔，需要设置为封面的，放在第一个
			})
		})

		it('款号(SPU)搜索', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-dres-spu-list',
				jsonParam: {
					code: sfRes.params.jsonParam.spuCommonDto.code,
					brandId: sfRes.params.jsonParam.spuExtraDto.brandId,
					shopId: LOGINDATA.shopId,
					classId: sfRes.params.jsonParam.spuCommonDto.classId,
					marketDateStart: common.getCurrentDate(),
					marketDateEnd: common.getCurrentDate(),
					flag: 1, //1显示有效款号
					// stockNumGt: , //取值0,代表显示库存>0的商品
					// colorSizeMode: , //1-查询有颜色尺码的 0-查询均色均码的，为空代表全显示
					// needStockNum: , //是否需要支持库存查询、排序和库存成本汇总， false-不需要(默认)，true-需要，注：支持库存会影响性能
					// searchToken: , //模糊检索串，匹配商品名称、款号、备注
					// orderBy: '', //排序字段，默认是按款号字母顺序sSpu.code; 按上架日期sSpu.market_date; 按库存排序(需要needStockNum=true): stockNum
					orderByDesc: true,
					wrapper: true,
				},
			});
		});
		it.skip('款号标签打印', async function () {
			await common.apiDo({
				apiKey: 'ec-dres-spu-print-barcode',
				jsonParam: {
					//printerMac: , //
					//	printerType: , //
					//slhVersionL: , //软件版本
					details: {
						tenantSpuId: sfRes.result.data.val,
						// colorId: ,
						// sizeId: ,
						// num: ,
					}
				}
			})
		});
		it('查询打印条形码简单规则设置', async function () {
			//无参数
			await common.apiDo({
				apiKey: 'ec-dres-fetchSimpleRuleSetConfig',

			})
		});
		it.skip('保存打印条形码简单规则设置', async function () {
			await common.apiDo({
				apiKey: 'ec-dres-saveRuleSetConfig',
				jsonParam: [{
					// ruleItemList: , //设置项编码
					// name: , //款号
					// pos: , //位置从1开始
					length: 5, //长度(0或null表示不限制,长度不够后面添加0)
				}, {
					// code: ,
					// name: , //颜色
					pos: 2,
					length: 5
				}, {
					// code: ,
					// name: , //尺码
					pos: 3,
					length: 5
				},],
				styleUseBarcode: 1 //是否启用 0关闭 1启用
			})
		});
		it('查询类目初始化数据', async function () {
			await common.apiDo({
				apiKey: 'ec-config-spuStdClass-tree',
				includeSubs: true, //是否包含下一级,这里传true
			});
		});
		it.skip('导入类目数据', async function () {
			//
			await common.apiDo({
				apiKey: 'ec-dres-copyStdClass',

			});
		});
		it('商品(SKU)搜索', async function () {
			await common.apiDo({
				apiKey: 'ec-dres-shopSku-list',
				jsonParam: {
					code: sfRes.params.jsonParam.spuCommonDto.code,
					brandId: sfRes.params.jsonParam.spuExtraDto.brandId,
					shopId: LOGINDATA.shopId,
					classId: sfRes.params.jsonParam.spuCommonDto.classId,
					marketDateStart: common.getCurrentDate(),
					marketDateEnd: common.getCurrentDate(),
					flag: 1, //1显示有效款号
					// stockNumGt: , //取值0,代表显示库存>0的商品
					// colorSizeMode: , //1-查询有颜色尺码的 0-查询均色均码的，为空代表全显示
					// needStockNum: , //是否需要支持库存查询、排序和库存成本汇总， false-不需要(默认)，true-需要，注：支持库存会影响性能
					// searchToken: , //模糊检索串，匹配商品名称、款号、备注
					// orderBy: '', //排序字段，默认是按款号字母顺序sSpu.code; 按上架日期sSpu.market_date; 按库存排序(需要needStockNum=true): stockNum
					orderByDesc: true,
					wrapper: true,
				},
			});
		});
		it('单品库存流水', async function () {
			await common.apiDo({
				apiKey: 'ec-inv-spu-flow-list',
				jsonParam: {
					tenantSpuId: BASICDATA.styleInfo.agc001,
					// shopSpuId:,
					shopId: LOGINDATA.shopId,
					bizDateStart: common.getCurrentDate(),
					bizDateEnd: common.getCurrentDate(),
				}
			})
		})
	});

	describe.skip('字典类', function () {
		describe('颜色组', function () {
			it('新增颜色组', async function () {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						showOrder: 0, //顺序号
						//	parentCode: 1, //父级编码对应父级字典codeValue
						typeId: 631,
					}
				})
			})
			it('同步颜色组', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_color_group',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});
		describe('颜色', function () {
			it('新增颜色', async function () {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						showOrder: 0, //顺序号
						parentCode: 1, //父级编码对应父级字典codeValue
						typeId: 601,
					}
				})
			})
			it('同步颜色', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_color',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});

		describe('尺码组', function () {
			it('新增尺码组', async function () {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						showOrder: 0, //顺序号
						//	parentCode: 1, //父级编码对应父级字典codeValue
						typeId: 604,
					}
				})
			})
			it('同步尺码组', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_size_group',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});
		describe('尺码', function () {
			it('新增尺码', async function () {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						showOrder: 0, //顺序号
						parentCode: 1, //父级编码对应父级字典codeValue
						typeId: 605,
					}
				})
			})
			it('同步尺码', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_size',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});

		describe('品牌', function () {
			it('新增品牌', async function () {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						unitId: LOGINDATA.unitId, //顺序号
						rem: '',
						typeId: 606,
					}
				})
			})
			it('同步品牌', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_brand',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});
		describe('类别', function () {
			let sfRes;
			before(async () => {
				sfRes = await common.apiDo({
					apiKey: 'ec-dres-class-save',
					jsonParam: {
						name: common.getRandomStr(4),
						parentId: null,
						rem: '',
					}
				})
			})
			it('类别明细', async function () {
				await common.apiDo({
					apiKey: 'ec-dres-class-getById',
					id: sfRes.result.data.val,
					wrapper: true,
				});
			});
			it('同步类别', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'dres_style_class',
					pageNo: 1,
					pageSize: 2000,
					lastSyncTime: common.getCurrentTime(),
				});
			});
		});

		describe('材质', function () {
			let sfRes;
			before(async () => {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						unitId: LOGINDATA.unitId, //顺序号
						typeId: 637,
						rem: '',
					}
				})
			})
			it('材质列表', async function () {
				await common.apiDo({
					apiKey: 'ec-slh2-conf-dictListWithDresNum',
					jsonParam: {
						typeId: 637,
						flag: 1,
						unitId: LOGINDATA.unitId,
					},
					pageSize: 0,
					wrapper: true,
					orderBy: 'updatedDate',
					orderByDesc: true,
				});
			});
			it('同步品牌', async function () {
				await common.apiDo({
					apiKey: 'ec-syncdata',
					type: 'sc_dict_brand',
					pageNo: 1,
					pageSize: 2000,

				});
			});
		});
		describe('风格', function () {
			let sfRes;
			before(async () => {
				await common.apiDo({
					apiKey: 'ec-config-save-dict',
					jsonParam: {
						codeName: common.getRandomStr(4),
						unitId: LOGINDATA.unitId, //顺序号
						typeId: 835,
						rem: '',
					}
				})
			})
			it('风格列表', async function () {
				await common.apiDo({
					apiKey: 'ec-slh2-conf-dictListWithDresNum',
					jsonParam: {
						typeId: 835,
						flag: 1,
						unitId: LOGINDATA.unitId,
					},
					pageSize: 0,
					wrapper: true,
					orderBy: 'updatedDate',
					orderByDesc: true,
				});
			});
		});

	});
	describe.skip('员工', function () {
		let sfRes;
		before(async () => {
			let params = {
				userName: common.getRandomStr(6),
				mobile: common.getRandomNumStr(11),
				roleName: '店长',
				rem: '新增员工店长',
			}
			sfRes = await loginReq.staffSave(params);

		})
		it('员工详情', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-getDtoById',
				id: sfRes.result.data.val,
				wrapper: true,
			});
		});
		it('员工列表', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-list',
				jsonParam: {
					flag: 1, //1为启用
					wrapper: true
				},

			});
		});
		it('停用员工', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-disable',
				id: sfRes.result.data.val,
			});
		});
		it('启用员工', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-enable',
				id: sfRes.result.data.val,
			});
		});
		it('已授权的员工列表', async function () {
			await mainReqHandler.getStaffList();
		})
		it('对员工授权', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-authorizeStaff',
				jsonParam: {
					staffId: sfRes.result.data.val, //员工id
					authPhone: sfRes.params.jsonParam.staff.mobile, //授权手机号。
					verifyCode: '0000', //验证码,写死
				},
			});
		});
		it('撤销员工授权', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-unauthorizeStaff',
				jsonParam: {
					id: sfRes.result.data.val,
				},
			});
		});
		//取消自己在本门店的授权，取消后会话即刻失效，并无法再次登陆到本门店
		it.skip('取消自己在本门店的授权', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-unauthorizeSelf',
			});
		});
	});

	describe.skip('员工管理(笑铺)-已经流程化', function () {
		let sfRes;
		before(async () => {
			sfRes = await common.apiDo({
				apiKey: 'ec-mdm-staff-save',
				jsonParam: {
					staff: {
						userName: common.getRandomStr(6),
						mobile: "",
						//roleIds: ,
						rem: '',
						//code: params.mobile, //工号001（新增，对于笑铺就是手机号*）
						depId: LOGINDATA.depId
					}
				}

			});
		});
		it('员工列表', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-staff-listCtlLevel',
				pageSize: 0,
				jsonParam: {
					wrapper: true,
				}
			});
		});
	});



	describe('账户', function () {
		let sfRes;
		before(async () => {
			sfRes = await common.apiDo({
				apiKey: 'ec-fin-simple-acct-save',
				jsonParam: {
					name: common.getRandomStr(4), //户名
					chanId: 10000,
					acctType: 13,
					balMoney: '',
					rem: '',
					flag: 1,
					printFlag: 0,
					bankingName: '',
					acctNo: '',
					payCap: 4, //当账户类型位银行账户时传4，其他情况不传
				}
			});
			//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
		});
		after(async () => {
			await common.apiDo({
				apiKey: 'ec-fin-acct-disable',
				id: sfRes.result.data.val,
			});
		});
		it('根据ID获取账户详情', async function () {
			await common.apiDo({
				apiKey: 'ec-fin-acct-getById',
				id: sfRes.result.data.val,
				wrapper: true,
			});
		});
		it('查询账户列表', async function () {
			await common.apiDo({
				apiKey: 'ec-fin-acct-list-report', //ec-fin-acct-list-by-tenantReal
				shopId: LOGINDATA.shopId,
				wrapper: true,
				traderCap: 1024, //交易方能力位，固定传1024
			});
		});
		it.skip('停用账户', async function () {
			await common.apiDo({
				apiKey: 'ec-fin-acct-disable',
				id: sfRes.result.data.val,
			});
		});
		it.skip('启用账户', async function () {
			await common.apiDo({
				apiKey: 'ec-fin-acct-enable',
				id: sfRes.result.data.val,
			});
		});
	});


	describe.skip('物流商', function () {
		let sfRes;
		before(async () => {
			let json = {
				'8': {
					staffId: LOGINDATA.userId,
					shopId: LOGINDATA.shopId,
				}
			};
			json = Object.assign(json, basicJson.addSupplierJson());
			json.org.capability = 8; //代表供应商
			json.org.name = '物流商' + common.getRandomStr(6);
			delete json[2];
			delete json.org.balance;
			sfRes = await slh2.mdm.addSupplier({
				jsonParam: json
			});
		});
		it('物流商详情', async function () {
			await slh2.mdm.getSupplierInfo({
				id: sfRes.result.data.val,
			})
		});
		it('物流商列表', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-org-logi-list',
				id: sfRes.result.data.val,
				nameLike: sfRes.params.jsonParam.org.name,
				addrLike: sfRes.params.jsonParam.address.detailAddr,
				shopId: LOGINDATA.shopId,
			});
		});
		it('停用物流商', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-org-disable',
				id: sfRes.result.data.val,
			});
		});
		it('启用物流商', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-org-enable',
				id: sfRes.result.data.val,
			});
		});
		it.skip('批量导入物流商', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-org-logi-batchImport',
			});
		});
	});

	describe.skip('积分', function () {

		it('积分规则保存', async function () {
			await common.apiDo({
				apiKey: 'ec-score-rule-scoreRule-saveRuleFromMeta',
				jsonParam: {
					domainKind: 21, //积分规则类型：21(按客户消费)/23(按客户充值)
					money: 10, //表示M钱产生N积分的M金额；缺省为1
					score: 1, //表示M钱产生N积分的N积分
				},
			});
		});
		it('积分规则查询', async function () {
			//没有参数，
			await common.apiDo({
				apiKey: 'ec-score-rule-scoreRule-getRuleToMeta',

			});
		});
		it('重算积分', async function () {
			//没有参数，
			await common.apiDo({
				apiKey: 'ec-sspd-recalculationSspdScore',

			});
		});
	});


});
