"use strict";
const common = require('../../../lib/common');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('组织结构管理-门店、员工、账户管理', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe('门店管理', function () {
		describe('修改门店', function () {
			let updateJson, shopRes, shopInfo;
			before(async () => {
				updateJson = {
					"128": {
						"typeId": "1",
						"qrContent1": "",
						"shopQrCodes": []
					},
					"256": {
						"typeId": "1"
					},
					"address": {
						"detailAddr": '地址' + common.getRandomStr(20)
					},
					"org": {
						"name": '修改门店' + common.getRandomStr(5),
						"telephone": '12' + common.getRandomNumStr(9),
						"fileId": "",
						"organType": "2",
						"capability": "384",
						"id": LOGINDATA.shopId,
						"flag": 1,
						"remark": '备注' + common.getRandomChineseStr(5),
					}
				};
				shopRes = await slh2.mdm.saveShop(updateJson);
				//共用一个接口
				shopInfo = await slh2.mdm.getSupplierInfo({
					id: LOGINDATA.shopId
				});

			});
			after(async () => {
				updateJson.org.name = '常青店';
				await slh2.mdm.saveShop(updateJson);
			});
			it('修改门店详情验证', async function () {
				common.isApproximatelyEqualAssert(shopRes.params.jsonParam, shopInfo.result.data);
			});
		})
		describe('切换门店', function () {
			after(async () => {
				await loginReq.changeShop();
			})
			it('1.切换门店', async function () {
				let shopListByDevice = await slh2.mdm.getShopListByDevice();
				let loginExp = shopListByDevice.result.data.rows.find(obj => obj.id != LOGINDATA.shopId);
				await slh2.mdm.changeUnit({
					shopId: loginExp.id
				});
				expect(loginExp.id, '切换门店后登陆信息中的和切换门店前的unitId一样，没有切换成功').to.equal(LOGINDATA.shopId);
			});
		});
	});

	describe('员工管理', function () {
		let staffParams, staff, staffInfo, staffList;
		before(async () => {
			//新增员工-获取职位接口
			staffParams = {
				userName: common.getRandomStr(6),
				mobile: common.getRandomNumStr(11),
				depId: LOGINDATA.depId,
				rem: '',
			};
			let roleList = await common.apiDo({
				apiKey: 'ec-om-role-list', //角色列表
				jsonParam: {
					wrapper: true,
					flag: 1,
					showFlag: 1//不传返回所有的角色包括老板和管理员
				},
			});
			let randomNum = common.getRandomNum(0, roleList.result.data.rows.length - 1);
			staffParams.roleIds = roleList.result.data.rows[randomNum].id;
			staff = await slh2.mdm.saveStaff(staffParams);
			staffInfo = await slh2.mdm.getStaffInfo({
				id: staff.result.data.val
			});
			staffList = await slh2.mdm.getStaffList({
				id: staff.result.data.val
			});
			//console.log(`staffInfo : ${JSON.stringify(staffInfo)}`);
		});
		after(async () => {
			//停用员工
			await slh2.mdm.disableStaff({
				id: staff.result.data.val
			});
		});
		it('员工详情验证', async function () {
			common.isApproximatelyEqualAssert(staff.params.jsonParam, staffInfo.result.data);
		});
		it('员工列表验证', async function () {
			expect(staffList.result.data.rows.length, '新增员工后列表中不止有一个相同id的员工').to.equal(1);

			common.isApproximatelyEqualAssert(staff.params.jsonParam.staff, staffList.result.data.rows[0]);
		});
		it('停用员工', async function () {
			await slh2.mdm.disableStaff({
				id: staff.result.data.val
			});
			staffInfo = await slh2.mdm.getStaffInfo({
				id: staff.result.data.val
			});
			expect(staffInfo.result.data.staff.flag, '停用的员工flag没变0').to.equal(0);
			staffList = await slh2.mdm.getStaffList({
				id: staff.result.data.val,
				flag: 1,
			});
			expect(staffList.result.data.rows.length, '只显示启用员工查询条件显示了停用的员工').to.equal(0);
			staffList = await slh2.mdm.getStaffList({
				id: staff.result.data.val,
				//flag: 1,
			});
			expect(staffList.result.data.rows[0].flag, '停用的员工在列表中flag不为0').to.equal(0);

		});
		it('启用员工', async function () {
			await slh2.mdm.enableStaff({
				id: staff.result.data.val
			});
			staffInfo = await slh2.mdm.getStaffInfo({
				id: staff.result.data.val,
			});
			expect(staffInfo.result.data.staff.flag, '启用的员工flag没变1').to.equal(1);
			staffList = await slh2.mdm.getStaffList({
				id: staff.result.data.val,
				//flag: 1,
			});
			expect(staffList.result.data.rows[0].flag, '启用的员工在列表中flag不为1').to.equal(1);

		});
		describe('修改员工', function () {
			before(async () => {
				let params = {
					//id: staff.result.data.val,
					userName: '修改' + common.getRandomStr(6),
					mobile: common.getRandomNumStr(11),
					depId: LOGINDATA.depId,
					rem: '备注' + common.getRandomChineseStr(8),
				};
				staff = await slh2.mdm.saveStaff(Object.assign(staffInfo.result.data.staff, params));
				staffInfo = await slh2.mdm.getStaffInfo({
					id: staff.result.data.val
				});
				staffList = await slh2.mdm.getStaffList({
					id: staff.result.data.val
				});
			});
			it('员工详情验证', async function () {
				common.isApproximatelyEqualAssert(staff.params.jsonParam, staffInfo.result.data, ['ver', 'namePy']);
			});
			it('员工列表验证', async function () {
				expect(staffList.result.data.rows.length, '新增员工后列表中不止有一个相同id的员工').to.equal(1);
				staff.params.jsonParam.staff.ecCaption.updatedBy = _.last(staff.params.jsonParam.staff.ecCaption.updatedBy.split(','));
				staff.params.jsonParam.staff.ecCaption.createdBy = _.last(staff.params.jsonParam.staff.ecCaption.createdBy.split(','));
				common.isApproximatelyEqualAssert(staff.params.jsonParam.staff, staffList.result.data.rows[0], ['ver', 'namePy']);
			});
		});

		describe('员工数据隔离', function () {
			let otherShopStaff;
			before(async () => {
				await loginReq.changeShopFast({ shopName: '门店二' });
			});
			after(async () => {
				await slh2.mdm.disableStaff({
					id: otherShopStaff.result.data.val
				});
				await loginReq.changeShop();
			});
			it('其他门店不能看到员工', async function () {
				let otherShopStaffList = await slh2.mdm.getStaffList().then(res => res.result.data.rows.find(data => data.id == staff.result.data.val));
				expect(otherShopStaffList).to.be.undefined;
			});
			it('其他门店可以新增同名同手机号员工', async function () {
				otherShopStaff = await slh2.mdm.saveStaff(Object.assign(staffParams, { userName: staffInfo.result.data.staff.userName, mobile: staffInfo.result.data.staff.mobile, depId: LOGINDATA.depId, }));
			});
		});
	});

	describe('授权管理', function () {
		let staffParams,
			staff,
			authorizeList = {},
			staffInfo = {};
		before('选择一个未授权的员工进行授权', async function () {
			//获取已授权员工列表

			authorizeList = await slh2.mdm.getAuthorizedStaffList();
			expect(common.sub(authorizeList.result.data.totalAuthorizeNum, authorizeList.result.data.authorizedNum), `授权数不足`).to.above(0);
			staffParams = {
				userName: common.getRandomStr(6),
				mobile: common.getRandomNumStr(11),
				depId: LOGINDATA.depId,
				rem: '',
			};
			//console.log(staffParams);
			staff = await slh2.mdm.saveStaff(staffParams);
			await slh2.mdm.disableStaff({
				id: staff.result.data.val
			});
			//创建一个员工并停用
			//从员工列表中找一个未授权的员工
			const staffList = await slh2.mdm.getStaffList({
				flag: 1
			});
			staffInfo = staffList.result.data.rows.find((ele) => ele.authFlag == 0 && ele.mobile != '');
			console.log(staffInfo);
			await slh2.mdm.authorizeStaff({
				staffId: staffInfo.id,
				authPhone: staffInfo.mobile,
				verifyCode: '0000'
			});
			await slh2.sync.syncData({ type: 'hr_staff' });
		});
		after(async function () {
			await loginReq.sspdLoginBySeq();
			await slh2.mdm.unauthorizeStaff({
				staffId: staffInfo.id
			});
		});
		it('1.授权后,已授权员工列表检查', async function () {
			this.retries(3);
			await common.delay(500);
			const listRes = await slh2.mdm.getAuthorizedStaffList();
			console.log(`listRes : ${JSON.stringify(listRes)}`);
			expect(listRes.result.data.authorizedNum).to.equal(authorizeList.result.data.authorizedNum + 1);
			common.isApproximatelyEqualAssert(_.last(listRes.result.data.shopList).staffs, staffInfo);
			//common.isArrayContainObjectAssert(listRes.result.data.shopList.staffs, staffInfo);
		});
		it('2.授权后,员工列表检查', async function () {
			const staffInfo2 = await slh2.mdm.getStaffList({
				flag: 1
			}).then((res) => res.result.data.rows.find((ele) => ele.id == staffInfo.id));
			expect(staffInfo2.authFlag, '授权后员工列表查询该员工的authFlag不为1').to.equal(1);
		});
		it('3.授权后,员工登录', async function () {
			let result = await loginReq.sspdLogin({
				mobile: staffInfo.mobile,
			});
		});
		describe('取消授权', function () {
			before('取消对员工的授权', async function () {
				await loginReq.sspdLoginBySeq();
				await slh2.mdm.unauthorizeStaff({
					staffId: staffInfo.id
				});

			});
			after(async function () {
				//为了后面的用例，需要重新进行授权
				await loginReq.sspdLoginBySeq();
				await slh2.mdm.authorizeStaff({
					staffId: staffInfo.id,
					authPhone: staffInfo.mobile,
					verifyCode: '0000'
				});
			})
			it('1.取消授权后,已授权员工列表检查', async function () {
				const listRes = await slh2.mdm.getAuthorizedStaffList();
				let result = common.takeWhile(listRes.result.data, obj => obj.id = staffInfo.id);
				expect(result.length, `取消授权后，已授权员工列表仍然可以看到该员工\n请求：${JSON.stringify(listRes)}`).to.equal(0);
			});
			it('2.取消授权后,员工列表检查', async function () {
				const listRes = await slh2.mdm.getStaffList({
					flag: 1
				}).then((res => res.result.data.rows.find(ele => ele.id == staffInfo.id)));
				expect(listRes.authFlag, `取消授权后员工列表中该员工authFlag不为0\n请求：${JSON.stringify(listRes)}\n`).to.equal(0);
			});
			it('3.取消授权后,员工登录', async function () {
				let errorMsg = '';
				try {
					await loginReq.sspdLogin({
						mobile: staffInfo.mobile,
					});
				} catch (e) {
					errorMsg = e;
				};
				console.log(`errorMsg : ${JSON.stringify(errorMsg)}`);
			});
		});
		describe('停用/启用', function () {
			before(async function () {
				//停用已授权的员工,该员工变成未授权

				await slh2.mdm.disableStaff({
					id: staffInfo.id
				});
			});
			it('1.停用已授权的员工,已授权员工列表查询', async function () {
				const listRes = await slh2.mdm.getAuthorizedStaffList();
				let result = common.takeWhile(listRes.result.data.shopList, obj => obj.id == staffInfo.id);
				expect(result.length, `停用已授权员工，已授权员工列表显示该员工`).to.equal(0);
			});
			it('2.停用已授权的员工，员工列表检查--是否停用和是否授权', async function () {
				const listRes = await slh2.mdm.getStaffList().then((res => res.result.data.rows.find(ele => ele.id == staffInfo.id)));
				expect(listRes.authFlag, '停用已授权的员工，authFlag没有变成0').to.equal(0);
				expect(listRes.flag, '停用已授权的员工，flag没有变成0').to.equal(0);
			});
			it.skip('停用已授权的员工，员工登录', async function () {
				await loginReq.sspdLogin({
					mobile: staffInfo.mobile
				});
			});
			it('启用后，员工不是授权状态', async function () {
				await slh2.mdm.enableStaff({
					id: staffInfo.id
				});
				let staffInfo2 = await slh2.mdm.getStaffList({
					id: staffInfo.id
				});
				expect(staffInfo2.result.data.rows[0].authFlag, '启用员工，authFlag不是为0').to.equal(0);
			});


		});
	});

	describe('账户管理', function () {
		let sfResAccount, accountInfo, accountList;
		before(async () => {
			let params = {
				name: '账户' + common.getRandomStr(4), //户名
				chanId: 10000,
				acctType: 13,
				balMoney: common.getRandomNum(0, 100),
				rem: common.getRandomStr(5) + common.getRandomChineseStr(6),
				flag: 1,
				printFlag: 0,
				bankingName: common.getRandomChineseStr(6), //户名
				acctNo: common.getRandomNumStr(10), //账号
				payCap: 4, //当账户类型位银行账户时传4，其他情况不传
			};
			sfResAccount = await slh2.fin.saveAccount({
				jsonParam: params
			});
			accountInfo = await slh2.fin.getAccountInfo({
				id: sfResAccount.result.data.val
			});
			accountList = await slh2.fin.getAccountList();
			// accountFlowList = await depReqHandler.getAccountFlowList({ acctId: sfResAccount.result.data.val });

		});
		after(async () => {
			//停用
			await slh2.fin.disableAccount({
				id: sfResAccount.result.data.val
			});
		})
		it('详情验证', async function () {
			common.isApproximatelyEqualAssert(sfResAccount.params.jsonParam, accountInfo.result.data);
		});
		it('列表验证', async function () {
			let act = common.takeWhile(accountList.result.data.rows, obj => obj.id == sfResAccount.result.data.val);
			expect(act.length, '账户列表中相同id的账户有多个').to.equal(1);
			common.isApproximatelyEqualAssert(sfResAccount.params.jsonParam, act[0]);
		});
		it.skip('账户流水初始余额验证', async function () {
			console.log(`accountFlowList=${JSON.stringify(accountFlowList)}`);
		});
		it('每个门店只允许一个现金账户', async function () {
			let params = {
				name: common.getRandomChineseStr(5),
				chanId: 0,
				acctType: 12,
				balMoney: common.getRandomNum(0, 100), //起始值
				rem: common.getRandomChineseStr(10),
				flag: 1,
				printFlag: 0,
				bankingName: "",
				acctNo: "",
			};
			let message = await slh2.fin.saveAccount({
				jsonParam: params,
				check: false
			});
			expect(message.result).to.includes({
				msg: '现金账户每个门店仅允许有一个'
			});
		})
		describe('修改', function () {
			before(async () => {
				let params = Object.assign(sfResAccount.params.jsonParam, {
					id: sfResAccount.result.data.val,
					name: '修改账户' + common.getRandomStr(4), //户名
					rem: '修改备注' + common.getRandomStr(5) + common.getRandomChineseStr(6),
					bankingName: '修改' + common.getRandomChineseStr(3), //户名
					acctNo: common.getRandomNumStr(10), //账号
					shopId: LOGINDATA.shopId,
				});
				sfResAccount = await slh2.fin.saveAccount({
					jsonParam: params
				});
				accountInfo = await slh2.fin.getAccountInfo({
					id: sfResAccount.result.data.val
				});
				accountList = await slh2.fin.getAccountList();
			})
			it('详情验证', async function () {
				common.isApproximatelyEqualAssert(sfResAccount.params.jsonParam, accountInfo.result.data);
			});
			it('列表验证', async function () {
				let act = common.takeWhile(accountList.result.data.rows, obj => obj.id == sfResAccount.result.data.val);
				expect(act.length, '账户列表中相同id的账户有多个').to.equal(1);
				common.isApproximatelyEqualAssert(sfResAccount.params.jsonParam, act[0]);
			});

		});
		describe('停用', function () {
			before(async () => {
				await slh2.fin.disableAccount({
					id: sfResAccount.result.data.val
				});
				accountInfo = await slh2.fin.getAccountInfo({
					id: sfResAccount.result.data.val
				});
				accountList = await slh2.fin.getAccountList({ flag: 0 });
			})
			it('详情flag验证', async function () {
				common.isApproximatelyEqualAssert({
					flag: 0,
					balMoney: sfResAccount.params.jsonParam.balMoney,
				}, accountInfo.result.data);
			});
			it('列表flag验证', async function () {
				let act = common.takeWhile(accountList.result.data.rows, obj => obj.id == sfResAccount.result.data.val);
				expect(act.length, '账户列表中相同id的账户有多个').to.equal(1);
				common.isApproximatelyEqualAssert({
					flag: 0,
					balMoney: sfResAccount.params.jsonParam.balMoney,
				}, act[0]);
			});

		});
		describe('启用', function () {
			before(async () => {
				await slh2.fin.enableAccount({
					id: sfResAccount.result.data.val
				});
				accountInfo = await slh2.fin.getAccountInfo({
					id: sfResAccount.result.data.val
				});
				accountList = await slh2.fin.getAccountList();
			})
			it('详情flag验证', async function () {
				common.isApproximatelyEqualAssert({
					flag: 1,
					balMoney: sfResAccount.params.jsonParam.balMoney,
				}, accountInfo.result.data);
			});
			it('列表flag验证', async function () {
				let act = common.takeWhile(accountList.result.data.rows, obj => obj.id == sfResAccount.result.data.val);
				expect(act.length, '账户列表中相同id的账户有多个').to.equal(1);
				common.isApproximatelyEqualAssert({
					flag: 1,
					balMoney: sfResAccount.params.jsonParam.balMoney,
				}, act[0]);
			});

		});
		describe('账户流水', function () {
			let acctInfo, acctFlowList;
			before(async () => {
				//获取现金账户idf
				// console.log(`accountList.result=${JSON.stringify(accountList.result)}`);
				let acctInfoList = common.takeWhile(accountList.result.data.rows, obj => obj.name == '现金账户');
				acctInfo = acctInfoList.shift();
				acctFlowList = await slh2.fin.getAccountFlowList({ acctId: acctInfo.id });
			});
			it('账户流水验证', async function () {
				let inSumMoney = 0, //总收入
					outSumMoney = 0;//总支出
				acctFlowList.result.data.rows.forEach(ele => {
					if (ele.money >= 0) { inSumMoney = common.add(inSumMoney, ele.money); }
					else { outSumMoney = common.add(outSumMoney, ele.money); };
					expect(ele.acctId, '账户流水中出现不是本账户的流水').to.equal(acctInfo.id);
				});
				common.isApproximatelyEqualAssert({
					inSumMoney: inSumMoney,
					outSumMoney: -outSumMoney,
				}, acctFlowList.result.data.sum);
			});
		});

	});

	describe('职位管理', function () {
		describe.skip('职位管理主流程', function () {

		});
		describe('数据隔离验证', function () {
			let shop1PrivsList;
			before(async () => {
				shop1PrivsList = await slh2.om.getPrivsList();
				await loginReq.changeShopFast({ shopName: '门店二' });
			});
			after(async () => {
				await loginReq.changeShop();
			});
			it('其他门店', async function () {
				let shop2PrivsList = await slh2.om.getPrivsList();
				common.isApproximatelyEqualAssert(shop1PrivsList.result, shop2PrivsList.result);
			});
		});
	});
});
