// const common = require('../../../lib/common');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('账户管理', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });

    //http://jira.hzdlsoft.com:7082/browse/SP-1027
    describe('SP-1027.开单支持默认收款方式', function () {
        it('设置默认账户', async function () {
            await slh2.fin.setDefaultAccount({ id: BASICDATA[LOGINDATA.shopName].accountList['支'].id, defaultFlag: 1 });
            await defaultAccountCheck({ id: BASICDATA[LOGINDATA.shopName].accountList['支'].id });
        });
        it('取消默认账户', async function () {
            await slh2.fin.setDefaultAccount({ id: BASICDATA[LOGINDATA.shopName].accountList['支'].id, defaultFlag: 0 });
            await defaultAccountCheck({});
        });
        it('设置默认账户', async function () {
            await slh2.fin.setDefaultAccount({ id: BASICDATA[LOGINDATA.shopName].accountList['现'].id, defaultFlag: 1 });
            await defaultAccountCheck({ id: BASICDATA[LOGINDATA.shopName].accountList['现'].id });
        });
    });

});

/**
 * 默认账户设置校验
 * @param {string} id 默认账户id,不传为没有
 */
function defaultAccountCheck({ id = '' }) {
    slh2.fin.getAccountList().then((res) => {
        expect(res.result.data.rows.length, `租户实体账户列表查询无数据`).to.be.above(0);
        res.result.data.rows.forEach((data) => {
            if (data.id == id) {
                expect(data.defaultFlag, '默认账户 defaultFlag值错误').to.equal(1);
            } else {
                expect(data.defaultFlag, '非默认账户 defaultFlag值错误').to.equal(0);
            }
        });
    });
};