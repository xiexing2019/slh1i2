"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('字典类-online', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	//颜色组 尺码组 颜色 尺码 品牌 面料 风格
	let typeId = [631, 604, 601, 605, 606, 637, 835];
	for (let i = 0; i < typeId.length; i++) {
		describe('字典管理', function () {
			let sfRes, dictInfo;
			before(async () => {
				sfRes = await slh2.config.saveDict({
					jsonParam: basicJson.dictJson({
						typeId: typeId[i],
					}),
				});
				// console.log(`xinz=${JSON.stringify(sfRes)}`);
			});
			after(async () => {
				await slh2.config.disableDict({
					id: sfRes.result.data.val,
				});
			});
			if (typeId[i] == 631 || typeId[i] == 604 || typeId[i] == 601 || typeId[i] == 605 || typeId[i] == 606) {
				it(`${typeId[i]}数据同步`, async function () {
					let syncDictData = await syncDictDataList(typeId[i]);
					let act = syncDictData.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
					common.isApproximatelyEqualAssert(sfRes.params.jsonParam, act);
				});
			};
			if (typeId[i] == 606 || typeId[i] == 637 || typeId[i] == 835) {
				it(`${typeId[i]}品牌，材质，风格详情`, async function () {
					dictInfo = await slh2.config.getDictInfo({
						id: sfRes.result.data.val,
						typeId: typeId[i],
					});
					common.isApproximatelyEqualAssert(sfRes.params.jsonParam, dictInfo.result.data);
				});
				it(`${typeId[i]}品牌，材质，风格列表`, async function () {
					let qlRes = await slh2.config.getDictListWithDresNum({ typeId: typeId[i], unitId: LOGINDATA.unitId });
					let act = qlRes.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
					common.isApproximatelyEqualAssert(dictInfo.result.data, act);
				});
			};
			describe('数据隔离', function () {
				before(async () => {
					await loginReq.changeShopFast({ shopName: '门店二' });
				});
				after(async () => {
					await loginReq.changeShop();
				});
				if (typeId[i] == 631 || typeId[i] == 604 || typeId[i] == 601 || typeId[i] == 605 || typeId[i] == 606) {
					it(`${typeId[i]}数据同步`, async function () {
						let syncDictData = await syncDictDataList(typeId[i]);
						let act = syncDictData.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
						common.isApproximatelyEqualAssert(sfRes.params.jsonParam, act);
					});
				};
				if (typeId[i] == 606 || typeId[i] == 637 || typeId[i] == 835) {
					it(`${typeId[i]}品牌，材质，风格详情`, async function () {
						dictInfo = await slh2.config.getDictInfo({
							id: sfRes.result.data.val,
							typeId: typeId[i],
						});
						common.isApproximatelyEqualAssert(sfRes.params.jsonParam, dictInfo.result.data);
					});
					it(`${typeId[i]}品牌，材质，风格列表`, async function () {
						let qlRes = await slh2.config.getDictListWithDresNum({ typeId: typeId[i], unitId: LOGINDATA.unitId });
						let act = qlRes.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
						common.isApproximatelyEqualAssert(dictInfo.result.data, act);
					});
				};


			});

			describe(`修改字典类${typeId[i]}`, function () {
				before(async () => {
					let json = basicJson.dictJson({
						typeId: typeId[i],
						id: sfRes.result.data.val,
					});
					json.codeName = `修${typeId[i] + common.getRandomStr(5)}`;
					// console.log(`json.codeName=${JSON.stringify(json.codeName)}`);
					//修改字典，修改成功没有返回pk
					sfRes = await slh2.config.saveDict({
						jsonParam: json
					});
					// console.log(`sfRes=${JSON.stringify(sfRes)}`);
				});
				if (typeId[i] == 631 || typeId[i] == 604 || typeId[i] == 601 || typeId[i] == 605 || typeId[i] == 606) {
					it(`${typeId[i]}数据同步`, async function () {
						let syncDictData = await syncDictDataList(typeId[i]);
						let act = syncDictData.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
						common.isApproximatelyEqualAssert(sfRes.params.jsonParam, act);
					});
				};
				if (typeId[i] == 606 || typeId[i] == 637 || typeId[i] == 835) {
					it(`${typeId[i]}品牌，材质，风格详情`, async function () {
						dictInfo = await slh2.config.getDictInfo({
							id: sfRes.result.data.val,
							typeId: typeId[i],
						});
						common.isApproximatelyEqualAssert(sfRes.params.jsonParam, dictInfo.result.data);
					});
					it(`${typeId[i]}品牌，材质，风格列表`, async function () {
						let qlRes = await slh2.config.getDictListWithDresNum({
							typeId: typeId[i],
							unitId: LOGINDATA.unitId
						});
						let act = qlRes.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
						common.isApproximatelyEqualAssert(dictInfo.result.data, act);

					});
				};
			});
			describe('停用启用字典类', function () {
				describe(`停用字典类${typeId[i]}`, function () {
					before(async () => {
						await slh2.config.disableDict({
							id: sfRes.result.data.val,
						});
						// console.log(`res=${JSON.stringify(res)}`);
					});
					if (typeId[i] == 631 || typeId[i] == 604 || typeId[i] == 601 || typeId[i] == 605 || typeId[i] == 606) {
						it(`${typeId[i]}数据同步`, async function () {
							let syncDictData = await syncDictDataList(typeId[i]);
							let act = syncDictData.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
							// console.log(`act=${JSON.stringify(act)}`);
							expect(Number(act.flag), `停用后${typeId[i]}flag没有变成0`).to.equal(0);
							common.isApproximatelyEqualAssert(sfRes.params.jsonParam, act, ['flag']);
						});
					};
					if (typeId[i] == 606 || typeId[i] == 637 || typeId[i] == 835) {
						it(`${typeId[i]}品牌，材质，风格详情`, async function () {
							dictInfo = await slh2.config.getDictInfo({
								id: sfRes.result.data.val,
								typeId: typeId[i],
							});
							// console.log(`dictInfo=${JSON.stringify(dictInfo)}`);
							expect(Number(dictInfo.result.data.flag), `停用后,${typeId[i]}字典详情flag没有变成0`).to.equal(0);
							common.isApproximatelyEqualAssert(sfRes.params.jsonParam, dictInfo.result.data, ['flag']);

						});
						it(`${typeId[i]}品牌，材质，风格列表`, async function () {
							let qlRes = await slh2.config.getDictListWithDresNum({
								typeId: typeId[i],
							});
							let act = qlRes.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
							// console.log(`act=${JSON.stringify(act)}`);
							// expect(act.flag, `停用字典类，${typeId[i]}列表中该字典flag不为0`).to.equal(0);
							common.isApproximatelyEqualAssert(dictInfo.result.data, act);
						});
					};
				});
				describe(`启用字典类${typeId[i]}`, function () {
					before(async () => {
						await slh2.config.enableDict({
							id: sfRes.result.data.val,
						});
					});
					if (typeId[i] == 631 || typeId[i] == 604 || typeId[i] == 601 || typeId[i] == 605 || typeId[i] == 606) {
						it(`${typeId[i]}数据同步`, async function () {
							let syncDictData = await syncDictDataList(typeId[i]);
							let act = syncDictData.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
							expect(Number(act.flag), `启用后${typeId[i]}flag没有变成1`).to.equal(1);
							common.isApproximatelyEqualAssert(sfRes.params.jsonParam, act, ['flag']);
						});
					};
					if (typeId[i] == 606 || typeId[i] == 637 || typeId[i] == 835) {
						it(`${typeId[i]}品牌，材质，风格详情`, async function () {
							dictInfo = await slh2.config.getDictInfo({
								id: sfRes.result.data.val,
								typeId: typeId[i],
							});
							expect(Number(dictInfo.result.data.flag), `启用后,${typeId[i]}字典详情flag没有变成1`).to.equal(1);
							common.isApproximatelyEqualAssert(sfRes.params.jsonParam, dictInfo.result.data, ['flag']);
						});
						it(`${typeId[i]}品牌，材质，风格列表`, async function () {
							let qlRes = await slh2.config.getDictListWithDresNum({
								typeId: typeId[i],
								unitId: LOGINDATA.unitId
							});
							let act = qlRes.result.data.rows.find((obj) => (obj.id == sfRes.result.data.val));
							expect(act.flag, `停用字典类，${typeId[i]}列表中该字典flag不为1`).to.equal(1);
							common.isApproximatelyEqualAssert(dictInfo.result.data, act);

						});
					};
				});

			});

		});

	};
	describe.skip('字典类--单位', function () {
		//ec-config-list-dict  {"typeId":"633","flag":1,"unitId":21413}
		//ec-config-save-dict  {"typeId":"633","unitId":21413,"codeName":"双","rem":""}
		//ec-config-disable-dict 
	});

	describe('类别', function () {
		let sfRes, classInfo, qlRes;
		before(async () => {
			sfRes = await slh2.dres.saveClass({
				name: `类别${common.getRandomStr(6)}`,
			});
			classInfo = await slh2.dres.getClassInfo({
				id: sfRes.result.data.val,
			});
			qlRes = await slh2.dres.getClassList({
				flag: 1
			});
			// console.log(`qlRes=${JSON.stringify(qlRes)}`);
		});
		after(async () => {
			await slh2.dres.disableClass({ id: sfRes.result.data.val });
		})
		it('类别详情', async function () {
			common.isApproximatelyEqualAssert({
				name: sfRes.params.jsonParam.name,
				flag: 1,
				rem: sfRes.params.jsonParam.rem,
				id: sfRes.result.data.val,
				unitId: LOGINDATA.unitId,
				hasChild: false,
			}, classInfo.result.data);
		});
		it('类别列表查询', async function () {
			// console.log(`sfRes : ${JSON.stringify(sfRes)}`);
			let act = qlRes.result.data.options.find(obj => obj.id == sfRes.result.data.val);
			// console.log(`act=${JSON.stringify(act)}`);
			common.isApproximatelyEqualAssert({
				value: sfRes.params.jsonParam.name,
				id: sfRes.result.data.val,
				flag: 1,
				deep: 1,//先写一层的类别
				hasChild: 'false',
				code: sfRes.result.data.val,
				// parentCode: 0,//默认没有父类别
			}, act);
		});
		describe('修改类别', function () {
			before(async () => {
				sfRes = await slh2.dres.saveClass({
					name: '修类别' + common.getRandomStr(6),
					rem: '修备注' + common.getRandomStr(8),
				});
				// console.log(`sfRes=${JSON.stringify(sfRes)}`);
				classInfo = await slh2.dres.getClassInfo({
					id: sfRes.result.data.val,
				});
				// console.log(`classInfo : ${JSON.stringify(classInfo)}`);
				qlRes = await slh2.dres.getClassList({
					flag: 1
				});
			});
			it('类别详情', async function () {
				// expect(Number(classInfo.result.data.)).to.equal(1);
				common.isApproximatelyEqualAssert({
					name: sfRes.params.jsonParam.name,
					flag: 1,
					rem: sfRes.params.jsonParam.rem,
					id: sfRes.result.data.val,
					unitId: LOGINDATA.unitId,
					// hasChild: false,
				}, classInfo.result.data);
			});
			it('类别列表查询', async function () {
				// console.log(`sfRes : ${JSON.stringify(sfRes)}`);
				let act = qlRes.result.data.options.find(obj => obj.id == sfRes.result.data.val);
				common.isApproximatelyEqualAssert({
					value: sfRes.params.jsonParam.name,
					id: sfRes.result.data.val,
					flag: 1,
					deep: 1,//先写一层的类别
					hasChild: 'false',
					code: sfRes.result.data.val,
					// parentCode: 0,//默认没有父类别
				}, act);
			});
		});
		describe('停用启用类别', function () {
			describe('停用', function () {
				before(async () => {
					await slh2.dres.disableClass({ id: sfRes.result.data.val });
					classInfo = await slh2.dres.getClassInfo({
						id: sfRes.result.data.val,
					});
					// console.log(`classInfo : ${JSON.stringify(classInfo)}`);
					qlRes = await slh2.dres.getClassList();


				});
				it('类别详情', async function () {
					// expect(Number(classInfo.result.data.)).to.equal(1);
					common.isApproximatelyEqualAssert({
						name: sfRes.params.jsonParam.name,
						flag: 0,
						rem: sfRes.params.jsonParam.rem,
						id: sfRes.result.data.val,
						unitId: LOGINDATA.unitId,
						// hasChild: false,
					}, classInfo.result.data);
				});
				it('类别列表查询', async function () {
					// console.log(`sfRes : ${JSON.stringify(sfRes)}`);
					let act = qlRes.result.data.options.find(obj => obj.id == sfRes.result.data.val);
					common.isApproximatelyEqualAssert({
						value: sfRes.params.jsonParam.name,
						id: sfRes.result.data.val,
						flag: 0,
						deep: 1,//先写一层的类别
						hasChild: 'false',
						code: sfRes.result.data.val,
						// parentCode: 0,//默认没有父类别
					}, act);
				});
			});
			describe('启用', function () {
				before(async () => {
					await slh2.dres.enableClass({ id: sfRes.result.data.val });
					classInfo = await slh2.dres.getClassInfo({
						id: sfRes.result.data.val,
					});
					// console.log(`classInfo : ${JSON.stringify(classInfo)}`);
					qlRes = await slh2.dres.getClassList();
				});
				it('类别详情', async function () {
					// expect(Number(classInfo.result.data.)).to.equal(1);
					common.isApproximatelyEqualAssert({
						name: sfRes.params.jsonParam.name,
						flag: 1,
						rem: sfRes.params.jsonParam.rem,
						id: sfRes.result.data.val,
						unitId: LOGINDATA.unitId,
						// hasChild: false,
					}, classInfo.result.data);
				});
				it('类别列表查询', async function () {
					// console.log(`sfRes : ${JSON.stringify(sfRes)}`);
					let act = qlRes.result.data.options.find(obj => obj.id == sfRes.result.data.val);
					common.isApproximatelyEqualAssert({
						value: sfRes.params.jsonParam.name,
						id: sfRes.result.data.val,
						flag: 1,
						deep: 1,//先写一层的类别
						hasChild: 'false',
						code: sfRes.result.data.val,
						// parentCode: 0,//默认没有父类别
					}, act);
				});
			});

		});
		describe('数据隔离验证', function () {
			before(async () => {
				await loginReq.sspdLoginBySeq();
				await loginReq.changeShopFast({ shopName: '门店二' });
			});
			after(async () => {
				await loginReq.changeShop();
			});
			it('其他门店', async function () {
				let otherShopClassInfo = await slh2.dres.getClassList({
					flag: 1
				}).then(res => res.result.data.options.find(obj => obj.id == sfRes.result.data.val));
				common.isApproximatelyEqualAssert({
					value: sfRes.params.jsonParam.name,
					id: sfRes.result.data.val,
					flag: 1,
					deep: 1,//先写一层的类别
					hasChild: 'false',
					code: sfRes.result.data.val,
					// parentCode: 0,//默认没有父类别
				}, otherShopClassInfo);
			});
		});
	});


	describe('价格类型402', function () {
		describe('主流程', function () {
			let updatePriceBefore, updatePriceRes;
			before(async () => {
				//修改价格名字和进货价比例，停用启用
				updatePriceBefore = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find((obj) => (obj.codeValue == 5)));
				//console.log(`updatePriceBefore=${JSON.stringify(updatePriceBefore)}`);
				let discount = common.getRandomNum(1, 1000, 1);
				updatePriceRes = await slh2.config.saveDict({
					jsonParam: Object.assign(_.cloneDeep(updatePriceBefore), {
						codeName: `修改价格${common.getRandomStr(6)}`,
						props: { isShow: 1, discount: discount },
						discount,
					}),
				});
			});
			after(async () => {
				//恢复价格类型名称
				await slh2.config.saveDict({ jsonParam: updatePriceBefore });
			});
			it('价格类型列表', async function () {
				let afterUpdatePrice = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find(data => data.id == updatePriceRes.params.jsonParam.id));
				//console.log(`1=${JSON.stringify(updatePriceRes.params.jsonParam)}`);
				//console.log(`afterUpdatePrice=${JSON.stringify(afterUpdatePrice)}`);
				common.isApproximatelyEqualAssert(updatePriceRes.params.jsonParam, afterUpdatePrice, ['updatedDate']);
			});
			describe('停用启用价格类型', function () {
				describe('停用价格类型', function () {
					before(async () => {
						await slh2.config.disableDict({ id: updatePriceBefore.id });
					});
					it('价格类型列表', async function () {
						let afterUpdatePrice = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find(data => data.id == updatePriceRes.params.jsonParam.id));
						expect(afterUpdatePrice.flag, '停用后价格类型flag没有变成0').to.equal(0);
						//common.isApproximatelyEqualAssert({ flag: 0 }, afterUpdatePrice);
						common.isApproximatelyEqualAssert(updatePriceRes.params.jsonParam, afterUpdatePrice, ['flag', 'isShow', 'updatedDate']);
					});
				});
				describe('启用价格类型', function () {
					before(async () => {
						await slh2.config.enableDict({ id: updatePriceBefore.id });
					});
					it('价格类型列表', async function () {
						let afterUpdatePrice = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find(data => data.id == updatePriceRes.params.jsonParam.id));
						expect(afterUpdatePrice.flag, '启用后价格类型flag没有变成1').to.equal(1);
						//common.isApproximatelyEqualAssert({ flag: 1 }, afterUpdatePrice);
						common.isApproximatelyEqualAssert(updatePriceRes.params.jsonParam, afterUpdatePrice, ['flag', 'isShow', 'updatedDate']);
					});
				});
			});
			describe.skip('设置默认价格', function () {
				before(async () => {
					//先获取下参数
					//再修改价格类型的默认价格参数
					//再获取参数
				});

			});
		});
		describe('异常', function () {
			it('相同名称验证', async function () {
				let updatePriceBefore = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find((obj) => (obj.codeValue == 4)));
				let res = await slh2.config.saveDict({
					jsonParam: Object.assign(updatePriceBefore, { codeName: '进货价' }),
					check: false,
				});
				expect(res.result, '新增相同价格名称“进货价”成功').to.includes({ msgId: "dict_name_repeat" });
			})
			it('第一个价格不允许停用', async function () {
				let priceTypeList = await slh2.config.getDictList({ typeId: 402, unitId: LOGINDATA.unitId });
				let firstId = priceTypeList.result.data.rows.find((obj) => (obj.codeValue == 1));
				//console.log(firstId);
				let res = await slh2.config.disableDict({ id: firstId.id, check: false });
				//console.log(res.result);
				expect(res.result, '停用第一个价格成功').to.includes({ msgId: 'first_price_type_canot_stop', msg: "第一个价格不能停用" });
			});
		});
		describe('数据隔离', function () {
			let shop1PriceType, shop2PriceType;
			before(async () => {
				shop1PriceType = await slh2.config.getDictList();
				await loginReq.sspdLoginBySeq();
				await loginReq.changeShopFast({ shopName: '门店二' });
				shop2PriceType = await slh2.config.getDictList();
			});
			after(async () => {
				await loginReq.changeShop();
			});
			it('数据隔离验证', async function () {
				common.isApproximatelyArrayAssert(shop1PriceType.result.data.rows, shop2PriceType.result.data.rows);
			});
		});
	});
});



//数据同步 列表
async function syncDictDataList(typeId) {
	let syncDictData;
	switch (typeId) {
		case 631:
			syncDictData = await slh2.sync.syncData({
				type: 'sc_dict_color_group'
			})
			break;
		case 604:
			syncDictData = await slh2.sync.syncData({
				type: 'sc_dict_size_group'
			})
			break;
		case 601:
			syncDictData = await slh2.sync.syncData({
				type: 'sc_dict_color'
			})
			break;
		case 605:
			syncDictData = await slh2.sync.syncData({
				type: 'sc_dict_size'
			})
			break;
		case 606:
			syncDictData = await slh2.sync.syncData({
				type: 'sc_dict_brand'
			})
			break;
		default:
			break;
	};
	return syncDictData;
};
