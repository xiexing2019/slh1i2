const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/json/basicJson');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe('销售营业汇总', function () {
    this.timeout(30000);

    before(async function () {
        await loginReq.sspdLoginAndChangShop();
    });
    describe.skip('货品分析', function () {

    });
    describe.skip('客户分析', function () {

    });
    describe.skip('店员业绩', function () {

    });
    describe.skip('利润统计', function () {

    });
    describe.skip('销售汇总', function () {
        it('当天查询', async function () {
            //销售单列表 销售营业汇总
            const [salesList, summaryByDate] = await Promise.all([sspd.salesBill.getSalesBillList({ jsonParam: { pageSize: 200, flag: 1 } }), common.apiDo({ apiKey: 'ec-sspd-salesBill-summaryByDate' })]);
            //console.log(salesList.result.data.sum);
            //console.log(salesList.result.data.rows);
            const exp = getSummaryByDateExp(salesList.result.data);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //console.log(`summaryByDate.result.data=${JSON.stringify(summaryByDate.result.data)}`);
            common.isApproximatelyEqualAssert(exp, summaryByDate.result.data);
        });
        it.skip('日期查询', async function () {
            const [salesList, summaryByDate] = await Promise.all([sspd.salesBill.getSalesBillList({ jsonParam: { pageSize: 400, proDateGte: common.getDateString([0, 0, -1]) } }), common.apiDo({ apiKey: 'ec-sspd-salesBill-summaryByDate', proDateGte: common.getDateString([0, 0, -1]), proDateLte: common.getCurrentDate() })]);
            const exp = getSummaryByDateExp(salesList.result.data);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //console.log(`summaryByDate.result.data=${JSON.stringify(summaryByDate.result.data)}`);
            common.isApproximatelyEqualAssert(exp, summaryByDate.result.data);
        });
        describe.skip('数据隔离验证', function () {
            let shop1Summary, shop1Profit, shop1SumBySeller, shop1SumCustSales, shop1Style, shop1SalesSum,
                proDate = { proDateGte: common.getDateString([0, 0, -7]), proDateLte: common.getCurrentDate() };
            before(async () => {

                shop1Summary = await common.apiDo(Object.assign({ apiKey: 'ec-sspd-salesBill-summaryByDate' }, proDate));
                shop1Profit = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findProfitBySku', ...proDate });
                // console.log(`shop1Profit=${JSON.stringify(shop1Profit)}`);
                shop1SumBySeller = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findSumByOwner', ...proDate });
                // console.log(`shop1SumBySeller=${JSON.stringify(shop1SumBySeller)}`);
                shop1SumCustSales = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findSumByComp', ...proDate });
                // console.log(`shop1SumCustSales=${JSON.stringify(shop1SumCustSales)}`);
                shop1Style = await common.apiDo({ apiKey: 'ec-sspd-salesBillDetail-findSumBySpu', ...proDate })
                shop1SalesSum = await common.apiDo(Object.assign({ apiKey: 'ec-sspd-salesBillDetail-findSalesSums', }, proDate));

                await loginReq.changeShopFast({ shopName: '门店二' });
            });
            after(async () => {
                await loginReq.changeShop({ shopName: '常青店' });
            });
            it('其他门店-销售汇总', async function () {
                let shop2Summary = await common.apiDo({ apiKey: 'ec-sspd-salesBill-summaryByDate', ...proDate });
                expect(shop2Summary.result).not.to.be.eql(shop1Summary.result);
            });
            it('其他门店-利润汇总', async function () {
                let shop2Profit = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findProfitBySku', ...proDate });
                expect(shop2Profit.result).not.to.be.eql(shop1Profit.result);
            });

            it('其他门店-店员汇总', async function () {
                let shop2SumBySeller = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findSumByOwner', ...proDate });
                expect(shop2SumBySeller.result).not.to.be.eql(shop1SumBySeller.result);
            });
            it('其他门店-客户分析汇总', async function () {
                let shop2SumCustSales = await common.apiDo({ apiKey: 'ec-sspd-salesBill-findSumByComp', ...proDate });
                expect(shop2SumCustSales.result).not.to.be.eql(shop1SumCustSales.result);
            });
            it('其他门店-货品分析汇总', async function () {
                let shop2Style = await common.apiDo({ apiKey: 'ec-sspd-salesBillDetail-findSumBySpu', ...proDate });
                expect(shop2Style.result).not.to.be.eql(shop1Style.result);
            });
            it('其他门店-货品分析-销售数据汇总', async function () {
                let shop2SalesSum = await common.apiDo(Object.assign({ apiKey: 'ec-sspd-salesBillDetail-findSalesSums', }, proDate));
                expect(shop2SalesSum.result).not.to.be.eql(shop1SalesSum.result);
            });
        });
    });

});



/**
 * 拼接销售营业汇总期望值
 * @description totalMoney:不包含抹零和其他金额的总额
 * @param {object} salesList 
 */
function
    getSummaryByDateExp(salesList) {
    const payTypes = basicJson.payType;
    let exp = { otherCost: 0, wxpay: 0, wxpayCount: 0, alipay: 0, alipayCount: 0, totalMoney: 0, salesNum: 0, favorMoney: 0, backMoney: 0, debts: 0, backNum: 0, payMoney: 0, balance: 0, totalNum: 0, payCount: 0, salesMoney: 0, scoreDeduction: 0, cash: 0, cashCount: 0, card: 0, cardCount: 0, tpCount: 0, tp: 0 };
    const mapField = 'otherCost;favorMoney;totalNum;backNum;backMoney;payMoney;salesNum;salesMoney';
    //console.log(salesList.rows);
    salesList.rows.forEach((data) => {
        if (data.flag != 1) return;//排除作废单

        exp = common.addObject(exp, format.dataFormat(data, mapField));
        /*console.log(data.totalMoney);
        console.log(data.favorMoney);
        console.log(-data.otherCost);*/
        exp.totalMoney = [data.totalMoney, data.favorMoney, -data.otherCost].reduce((a, b) => common.add(a, b), exp.totalMoney);
        //现金
        if (data[payTypes.cash.finIOPay] != 0) {
            exp.cash = common.add(exp.cash, data[payTypes.cash.finIOPay]);
            exp.cashCount++;
            exp.payCount++;
        };
        //刷卡
        if (data[payTypes.bankCard.finIOPay] != 0) {
            exp.card = common.add(exp.card, data[payTypes.bankCard.finIOPay]);
            exp.cardCount++;
            exp.payCount++;
        };
        //支付宝
        if (data[payTypes.aliF2F.finIOPay] != 0) {
            exp.alipay = common.add(exp.alipay, data[payTypes.aliF2F.finIOPay]);
            exp.alipayCount++;
            exp.payCount++;
        };
        //微信
        if (data[payTypes.wxF2F.finIOPay] != 0) {
            exp.wxpay = common.add(exp.wxpay, data[payTypes.wxF2F.finIOPay]);
            exp.wxpayCount++;
            exp.payCount++;
        };
        //在线支付
        if (data.onlinePayFlag == 1) {
            exp.tp = common.add(exp.tp, data[payTypes.other.finIOPay]);
            exp.tpCount++;
            exp.payCount++;
        };

        // 结余 欠款
        //const diff = common.sub(data.payMoney, data.totalMoney);
        const diff = data.balance;
        const payKey = diff > 0 ? 'balance' : 'debts';
        exp[payKey] = common.add(exp[payKey], Math.abs(diff));

        return exp;
    });
    return exp;
};