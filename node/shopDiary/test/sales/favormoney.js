"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('抹零', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginByStaff();
    });
    after(async () => {
        await slh2.config.saveParamValue({
            data: [{ code: "favor_money_ceil", domainKind: "business", ownerId: LOGINDATA.shopId, val: "0" }]
        });
    });
    describe('抹零', function () {
        let sfRes, qfRes, json1, json2, json3, paramInfo, sfRes1, sfRes2, sfRes3;
        before(async () => {
            json1 = basicJson.salesJson();
            json1.main.favorMoney = 50;
            json1.payways[0].money -= json1.main.favorMoney;

            json2 = basicJson.salesJson();
            json2.main.favorMoney = 100;
            json2.payways[0].money -= json2.main.favorMoney;

            json3 = basicJson.salesJson();
            json3.main.favorMoney = 100;
            json3.payways[0].money -= json3.main.favorMoney;

            sfRes1 = await sspd.salesBill.saveSalesBill({ jsonParam: json1 });
            await common.delay(500);
            sfRes2 = await sspd.salesBill.saveSalesBill({ jsonParam: json2 });
            await common.delay(500);
            sfRes3 = await sspd.salesBill.saveSalesBill({ jsonParam: json3 });
            await common.delay(500);
            //未设置抹零上限前先初始化三个单据
            await slh2.config.saveParamValue({
                data: [{ code: "favor_money_ceil", domainKind: "business", ownerId: LOGINDATA.shopId, val: "50" }]
            });
            paramInfo = await slh2.config.getParamValues({
                codes: "favor_money_ceil", domainKind: "business", ownerId: LOGINDATA.shopId, ownerKind: 9
            });
            //console.log(paramInfo);
            //val为抹零上限
            /*qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val
            });*/
            //销售单详情
            //console.log(json);
        });
        it('1.获取参数信息', async function () {
            expect(paramInfo.result.data.favor_money_ceil, '参数异常').to.equal('50');
        });
        it.skip('2.抹零不超过上限', async function () {
            json.main.favorMoney = 50;
            json.payways[0].money -= json.main.favorMoney;
            sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: json, check: false });
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it.skip('3.抹零超过上限', async function () {
            json.main.favorMoney = 50;
            json.payways[0].money -= json.main.favorMoney;
            sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: json, check: false });
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('4.旧单据未超过上限，修改抹零到上限之上', async function () {
            let params = {
                favorMoney: 51,
                rem: '修改抹零到上限之上'
            };
            json1 = basicJson.salesJson(params);
            json1.payways[0].money -= json1.main.favorMoney;
            json1.main.id = sfRes1.result.data.val; //修改时必传单据id
            json1.main.ver = sfRes1.result.data.ver; //修改时必传
            //修改销售单
            sfRes1 = await sspd.salesBill.saveSalesBill({
                jsonParam: json1,
                check: false
            });
            //console.log(sfRes1);
            expect(sfRes1.result.code, '抹零有误').to.equal(-10);
        });
        it('5.旧单据超过上限，修改抹零到上限之上', async function () {
            let params = {
                favorMoney: 51,
                rem: '修改抹零到上限之上',
            };
            json2 = basicJson.salesJson(params);
            json2.payways[0].money -= json2.main.favorMoney;
            json2.main.id = sfRes2.result.data.val; //修改时必传单据id
            json2.main.ver = sfRes2.result.data.ver; //修改时必传
            //修改销售单
            sfRes2 = await sspd.salesBill.saveSalesBill({
                jsonParam: json2
            });
            expect(sfRes2.result.code, '抹零有误').to.equal(0);
        });
        it('6.旧单据超过上限，修改抹零到上限之下，再修改到之上', async function () {
            let params = {
                favorMoney: 49,
                rem: '修改抹零到上限之下，再修改到之上',
            };
            json3 = basicJson.salesJson(params);
            json3.payways[0].money -= json3.main.favorMoney;
            json3.main.id = sfRes3.result.data.val; //修改时必传单据id
            json3.main.ver = sfRes3.result.data.ver; //修改时必传
            //修改销售单
            sfRes3 = await sspd.salesBill.saveSalesBill({
                jsonParam: json3
            });
            let params2 = {
                favorMoney: 51,
                rem: '修改抹零到上限之下，再修改到之上',
            };
            json3 = basicJson.salesJson(params2);
            json3.payways[0].money -= json3.main.favorMoney;
            json3.main.id = sfRes3.result.data.val; //修改时必传单据id
            json3.main.ver = sfRes3.result.data.ver; //修改时必传
            //修改销售单
            sfRes3 = await sspd.salesBill.saveSalesBill({
                jsonParam: json3,
                check: false
            });
            expect(sfRes3.result.code, '抹零有误').to.equal(-10);
        });
    });
});