const common = require('../../../lib/common');
const format = require('../../../data/format');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const slh2 = require('../../../reqHandler/slh2/index');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

//目前在asdd1环境测试、笑铺支持销售单，收款单，充值单在线支付
//扫码判断是支付宝还是微信支付
describe.skip('在线支付', function () {
    this.timeout(30000);
    let sfRes, qfRes, salesListRes2, custAcct1, custAcct2, accountFlowList1 = {}, accountFlowList2 = {};

    before(async function () {
        await loginReq.sspdLoginAndChangShop();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const checkRes = await slh2.config.checkOnlinePayChannel();
        expect(checkRes.result.data.val, `当前登录企业未开通在线支付`).to.be.true;
    });

    it.skip('企业开通在线支付', async function () {
        //测试环境 payChannelCodes写死
        await common.apiDo({
            apiKey: 'ec-config-ugr-tenant-updatePayChannel', uniCode: LOGINDATA.uniCode,
            payChannelCodes: [{ configType: 1, channelCode: 20971581522633414348 }, { configType: 2, channelCode: 20971581522633414348 }]
        });
    });

    describe('未付', function () {
        before(async function () {
            let json = basicJson.salesPayOnlineJson();
            // console.log(`json=${JSON.stringify(json)}`);
            // salesListRes1 = await sspd.salesBill.getSalesBillList({ jsonParam: { hasAliOnlinePay: 1, onlinePayFlagIn: '0,-1' } });
            //客户对账单
            custAcct1 = await accCheck.getSimpleCustAcctCheck({ shopId: json.main.shopId, compId: json.main.compId, pageSize: 0, hasOnlinePay: 1, });

            //账户流水
            for (const ele of json.payways) {
                await slh2.fin.getAccountFlowList({ acctId: ele.accountId, }).then(res => accountFlowList1[ele.accountId] = res.result.data.rows[0]);
            };

            //新增未付销售单
            sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: json });
            // console.log(`sfRes=${JSON.stringify(sfRes)}`);
            //销售单详情
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            // console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
            //销售单列表 查未付
            salesListRes2 = await sspd.salesBill.getSalesBillList({ jsonParam: { shopId: LOGINDATA.shopId, onlinePayFlagIn: '0,-1', hasOnlinePay: 1, } });
            // console.log(`salesListRes2=${JSON.stringify(salesListRes2)}`);
            // console.log(`qlRes.result.data.rows[0]=${JSON.stringify(qlRes.result.data.rows[0])}`);
            //客户积分明细
            //console.log(salesListRes2);
            //客户对账单
            custAcct2 = await accCheck.getSimpleCustAcctCheck({ shopId: sfRes.params.jsonParam.main.shopId, compId: sfRes.params.jsonParam.main.compId, pageSize: 0, hasOnlinePay: 1, });
            // console.log(`custAcct2=${JSON.stringify(custAcct2)}`);
            //
            for (const ele of sfRes.params.jsonParam.payways) {
                await slh2.fin.getAccountFlowList({ acctId: ele.accountId, }).then(res => accountFlowList2[ele.accountId] = res.result.data.rows[0]);
            };
        });
        it('销售单详情', async function () {
            let exp = _.cloneDeep(sfRes.params.jsonParam);
            //exp.fin.balance = -exp.main.totalMoney;
            common.isApproximatelyEqualAssert(exp, qfRes.result.data, ['ver']);
        });
        it('销售单列表数据验证', async function () {
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            //console.log(`exp = ${JSON.stringify(exp)}`);
            // console.log(`salesListRes2.result.data.rows[0] = ${JSON.stringify(salesListRes2.result.data.rows[0])}`);
            common.isApproximatelyEqualAssert(exp, salesListRes2.result.data.rows[0]);
        });
        it('销售单列表查询条件--在线支付验证', async function () {
            salesListRes2.result.data.rows.forEach(ele => {
                //expect(Number(ele.payMoney), '查询条件[在线支付未付]失效,payMoney不为0').to.equal(0);
                expect(ele.onlinePayType, '查询条件[在线支付单据]失效').to.equal(16384);
            });
        });
        it('销售单汇总验证', async function () {
            const sumExp = salesListRes2.result.data.rows.reduce((obj1, obj2) => common.addObject(obj1, obj2), {});
            common.isApproximatelyEqualAssert(sumExp, salesListRes2.result.data.sum);
        });
        //edit score -> 改为取值 不要写死
        it('客户积分明细', async function () {
            //获取当前账套积分规则
            this.retries(5);
            await common.delay(500);
            let scoreRule = await slh2.score.getScoreRule();
            let custScoreDetail = await slh2.score.getCustScoreDetail({
                shopId: sfRes.params.jsonParam.main.shopId,
                traderId: sfRes.params.jsonParam.main.compId,
            });
            let exp = getExpByBill.getCustScoreDetailExp(qfRes.result.data);
            exp.score = format.numberFormat(common.div(sfRes.params.jsonParam.main.totalMoney, common.div(scoreRule.result.data[21].money, scoreRule.result.data[21].score)), 3);
            common.isApproximatelyEqualAssert(exp, custScoreDetail.result.data.rows[0]);
        });
        //edit 统计数据验证需要封装
        it('客户对账单', async function () {
            this.retries(5);
            await common.delay(500);
            let exp = getExpByBill.getCustSimpleAcctExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, custAcct2.result.data.rows[0]);
            //统计数据验证
            let expSum = getExpByBill.getCustAcctSumExp(custAcct1, qfRes);
            common.isApproximatelyEqualAssert(expSum, custAcct2.result.data);
        });
        it('客户对账单-移动支付条件验证', async function () {
            custAcct2.result.data.rows.forEach(ele => {
                // console.log(`ele=${JSON.stringify(ele)}`);
                expect(ele, '移动支付查询条件失效').to.includes.keys('onlinePayFlag', 'onlinePayMoney', 'onlinePayType');
            });
        });
        it('列表', async function () {
            common.isApproximatelyEqualAssert(accountFlowList1, accountFlowList2);
        });
    });

    describe('已付', function () {
        let payCallbackRes, custScoreDetail1, custScoreDetail2;
        before(async function () {
            salesListRes1 = await sspd.salesBill.getSalesBillList({ jsonParam: { pageSize: 0, hasOnlinePay: 1, } });
            //客户积分明细
            custScoreDetail1 = await slh2.score.getCustScoreDetail({
                shopId: sfRes.params.jsonParam.main.shopId,
                traderId: sfRes.params.jsonParam.main.compId,
            });
            //客户对账单
            custAcct1 = await accCheck.getSimpleCustAcctCheck({ shopId: sfRes.params.jsonParam.main.shopId, compId: sfRes.params.jsonParam.main.compId, pageSize: 0, hasOnlinePay: 1 });
            // console.log(`custAcct1 = ${JSON.stringify(custAcct1.result.data.endMoney)}`);
            //
            await sspd.salesBill.createSalesBillPay({ billId: sfRes.result.data.val });
            payCallbackRes = await billReqHandler.misSalesBillPayCallback({ mainId: sfRes.result.data.val });
            // qfRes.result.data.fin.payBillNo = res.params.payId;
            //获取销售单详情
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });

            salesListRes2 = await sspd.salesBill.getSalesBillList({ jsonParam: { pageSize: 0, hasOnlinePay: 1, } });
            //客户积分明细
            custScoreDetail2 = await slh2.score.getCustScoreDetail({
                shopId: sfRes.params.jsonParam.main.shopId,
                traderId: sfRes.params.jsonParam.main.compId,
            });
            //客户对账单
            custAcct2 = await accCheck.getSimpleCustAcctCheck({ shopId: sfRes.params.jsonParam.main.shopId, compId: sfRes.params.jsonParam.main.compId, pageSize: 0, hasOnlinePay: 1 });
            // console.log(`custAcct2 = ${JSON.stringify(custAcct2.result.data.endMoney)}`);
            //账户流水
            for (const ele of sfRes.params.jsonParam.payways) {
                await slh2.fin.getAccountFlowList({ acctId: ele.accountId, }).then(res => accountFlowList2[ele.accountId] = res.result.data.rows[0]);
            };
        });
        it('销售单详情', async function () {
            let exp = _.cloneDeep(sfRes.params.jsonParam);
            exp.payways[0] = Object.assign(exp.payways[0], {
                payFlag: 1, typeId: 16386
            });
            exp.fin.payBillNo = payCallbackRes.params.payId;
            common.isApproximatelyEqualAssert(exp, qfRes.result.data, ['ver']);
        });
        it('销售单列表数据验证', async function () {
            TESTCASE = {
                jira: 'SHOPDIARY-2757',
            };
            // console.log(`qfRes.result.data=${JSON.stringify(qfRes.result.data)}`);
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            //console.log(exp);
            //console.log(salesListRes2.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, salesListRes2.result.data.rows[0], ['pay9']);
        });
        it('销售单列表底部汇总验证', async function () {
            const sumExp = salesListRes2.result.data.rows.reduce((obj1, obj2) => common.addObject(obj1, obj2), {});
            common.isApproximatelyEqualAssert(sumExp, salesListRes2.result.data.sum);
        });
        it.skip('销售单查询条件-在线支付验证', async function () {
            salesListRes2.result.data.rows.forEach(ele => {
                expect(ele.onlinePayType, `查询条件[支付宝]失效${salesListRes2.params}`).to.equal(2 || 16384);

            });
        });
        it('客户积分明细', async function () {
            common.isApproximatelyEqualAssert(custScoreDetail1.result.data, custScoreDetail2.result.data);
        });
        //edit 统计数据验证需要封装
        it.skip('客户对账单', async function () {
            //查询条件支付宝
            // let onlinePayways = qfRes.result.data.payways.find(ele => ele.tallyType == 2);
            // console.log(`onlinePayways=${JSON.stringify(onlinePayways)}`);
            let exp = getExpByBill.getCustSimpleAcctExp(qfRes.result.data);
            // console.log(`exp = ${JSON.stringify(exp)}`);
            // console.log(`custAcct2.result.data.rows = ${JSON.stringify(custAcct2.result.data.rows)}`);
            common.isApproximatelyEqualAssert(exp, custAcct2.result.data.rows[0]);
            let expSum = getExpByBill.getCustAcctSumExp(custAcct1, qfRes);
            //let expSumtrue = {};
            //console.log(`expSum=${JSON.stringify(expSum)}`);
            //console.log(`expSum=${JSON.stringify(custAcct2.result.data)}`);
            common.isApproximatelyEqualAssert(expSum, custAcct2.result.data);
            // common.isApproximatelyEqualAssert({
            //     total: custAcct1.result.data.total,
            //     beginMoney: common.sub(custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].lastBalance, custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].balance),
            //     endMoney: common.add(custAcct1.result.data.endMoney, onlinePayways.money),
            //     sum: {
            //         payMoney: common.add(custAcct1.result.data.sum.payMoney, onlinePayways.money),
            //         totalMoney: custAcct1.result.data.sum.totalMoney,
            //     },
            // }, custAcct2.result.data);
        });
        it('客户对账单-在线支付查询条件验证', async function () {
            custAcct2.result.data.rows.forEach((ele) => {
                expect(ele, '移动支付查询条件失效').to.includes.keys('onlinePayFlag', 'onlinePayMoney', 'onlinePayType');
            });
        });
        it.skip('账户流水列表', async function () {
            let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, accountFlowList2);
        });
    });


});
