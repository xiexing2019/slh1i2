const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');
const slh2 = require('../../../reqHandler/slh2/index');

describe('储值支付与还款-online', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('新增销售单-储值支付', function () {
        let json, sfRes, qfRes, dwBalance1;
        before(async () => {
            json = basicJson.salesJson();
            json.main.compId = BASICDATA.ids.dwidXl;
            json.payways[0].typeId = 16;
            delete json.payways[0].accountId;
            //console.log(json);
            dwBalance1 = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            //获取客户起始积分和余额
            //新增销售单,采用储值支付
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //console.log(sfRes);
            //销售单详情.开单后客户余额
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        it('单据详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });

        it('列表信息', async function () {
            //this.retries(5);
            //await common.delay(2000);
            const qlRes = await sspd.salesBill.getSalesBillList().then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            //console.log(`exp : ${JSON.stringify(exp)}`);
            //console.log(`qlRes : ${JSON.stringify(qlRes)}`);
            common.isApproximatelyEqualAssert(exp, qlRes);
        });
        it('客户余额', async function () {
            let dwBalance2 = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            const exp = getStoreValueCostExp(qfRes.result.data);
            //console.log(exp);
            //console.log(`dwBalance1.result.data=${JSON.stringify(dwBalance1.result.data)}`);
            //console.log(`dwBalance2.result.data=${JSON.stringify(dwBalance2.result.data)}`);
            common.isApproximatelyEqualAssert(common.addObject(dwBalance1.result.data, exp), dwBalance2.result.data);
        });

    });
    describe('新增销售单-还款', function () {
        let json, sfRes, qfRes, dwBalance1;
        before(async () => {
            json = basicJson.salesJson();
            dwBalance1 = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            //获取客户起始积分和余额
            json.payways[0].money -= dwBalance1.result.data.balance;
            json.payways[1] = {
                title: "还款",
                value: dwBalance1.result.data.balance,
                editable: true,
                type: -16,
                typeId: 16,
                money: dwBalance1.result.data.balance
            }
            json.fin.balance = 0;
            //console.log(json);
            //新增销售单,全部还款
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //console.log(sfRes);
            //销售单详情.开单后客户余额
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //console.log(qfRes.result.data);
            await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        after(async () => {
            await sspd.salesBill.deleteSalesBill({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //作废该单据保证客户为欠款
        });
        it('单据详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });

        it('列表信息', async function () {
            //this.retries(5);
            //await common.delay(2000);
            const qlRes = await sspd.salesBill.getSalesBillList().then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            //console.log(`exp : ${JSON.stringify(exp)}`);
            //console.log(`qlRes : ${JSON.stringify(qlRes)}`);
            common.isApproximatelyEqualAssert(exp, qlRes);
        });
        it('客户余额', async function () {
            let dwBalance2 = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            });
            const exp = getStoreValueCostExp(qfRes.result.data);
            //console.log(exp);
            //console.log(`dwBalance1.result.data=${JSON.stringify(dwBalance1.result.data)}`);
            //console.log(`dwBalance2.result.data=${JSON.stringify(dwBalance2.result.data)}`);
            common.isApproximatelyEqualAssert(common.addObject(dwBalance1.result.data, exp), dwBalance2.result.data);
        });

    });
});


/**
 * 获取储值支付扣除的客户余额
 * @param {Object} qfRes.result.data
 */

function getStoreValueCostExp(obj) {
    let exp = {
        score: obj.fin.compKind == 1 ? 0 : Math.ceil(common.div(obj.main.totalMoney, 50) * 1000) / 1000,
        balance: -obj.fin.storeValueCost
    };
    return exp;
};
