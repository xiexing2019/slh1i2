const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2/index');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe.skip('补登销售单', function () {
    this.timeout(30000);
    let cust, styleInfo, dresRes = {}, dataArr = [common.getDateString([0, 0, -1]), common.getCurrentDate(), common.getDateString([0, 0, 1])];
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
        dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({ spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }, { "colorId": 2, "sizeId": 5, "stockNum": 5 }, { "colorId": 3, "sizeId": 4, "stockNum": 5 }] }) });

        //新增货品
        await common.delay(500);
        cust = await slh2.mdm.addCust(basicJson.addCustJson({ userName: `补登${common.getRandomStr(6)}` }));
        await common.delay(1000);
        styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then(res => res.result.data);
        //新增今天的单据
        let json = basicJson.salesJson({ dwid: cust.result.data.val, styleInfo });
        json.payways = [];
        await sspd.salesBill.saveSalesBill({ jsonParam: json });
        await common.delay(1000);
        //console.log(style);
    });
    for (let i = 0; i < dataArr.length; i++) {
        describe('新增昨天的销售单', function () {
            let supplementSales, supplementSalesInfo, dwBalanceBef, custAcctBef;
            before(async () => {
                //获取期初余额
                dwBalanceBef = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderKind: 2,
                    traderId: cust.result.data.val
                });
                custAcctBef = await getCustAcctForData({ custId: cust.result.data.val });
                // console.log(`acctBef=${JSON.stringify(acctBef)}`);
                //补登昨天的单据 欠款
                let json = basicJson.salesJson({ dwid: cust.result.data.val, styleInfo, proDate: dataArr[i] });
                json.payways = [];
                supplementSales = await sspd.salesBill.saveSalesBill({ jsonParam: json });
                await common.delay(1000);
                supplementSalesInfo = await sspd.salesBill.getSalesBillFull({ isPend: 0, id: supplementSales.result.data.val });
                // console.log(`supplementSalesInfo=${JSON.stringify(supplementSalesInfo)}`);

            });
            it('对账单验证', async function () {
                await custAcctCheck({ custAcctBef, billInfo: supplementSalesInfo.result.data });
            });
            it('销售单详情', async function () {
                common.isApproximatelyEqualAssert(supplementSales.params.jsonParam, supplementSalesInfo.result.data);
            });
            it('销售单列表', async function () {
                let supplementSalesList = await sspd.salesBill.getSalesBillList({
                    proDateGte: dataArr[i],
                    proDateLte: dataArr[i]
                }).then(res => res.result.data.rows.find(obj => obj.id == supplementSales.result.data.val));
                expect(supplementSalesList, `销售单列表查询昨天的单据，没有查到数据`).to.not.be.undefined;
                let exp = getExpByBill.getSalesBillListExp(supplementSalesInfo.result.data);
                common.isApproximatelyEqualAssert(exp, supplementSalesList);
            });
            it('客户积分和余额', async function () {
                let dwBalanceAft = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderKind: 2,
                    traderId: cust.result.data.val
                });
                let exp = getExpByBill.getBalanceAndScoreExp(supplementSalesInfo.result.data);
                common.isApproximatelyEqualAssert(common.addObject(exp, dwBalanceBef.result.data), dwBalanceAft.result.data);
            });
            it.skip('积分明细', async function () {
                let scoreDetail = await slh2.score.getCustScoreDetail({
                    traderId: cust.result.data.val, proDateGte: dataArr[i],
                    proDateLte: dataArr[i]
                }).then(res => res.result.data.rows.find(obj => obj.billNo == supplementSales.result.data.billNo));
                //console.log(`新增scoreDetail=${JSON.stringify(scoreDetail)}`);
                expect(scoreDetail, `积分明细`).to.not.be.undefined;
                let exp = getExpByBill.getCustScoreDetailExp(supplementSalesInfo.result.data);
                exp.score = format.numberFormat(common.div(supplementSales.params.jsonParam.main.totalMoney, 50), 3);
                common.isApproximatelyEqualAssert(exp, scoreDetail);
            });
            it('库存流水', async function () {
                let invFlow = await slh2.inv.getInvFlowList({
                    tenantSpuId: styleInfo.tenantSpuId, bizDateStart: dataArr[i],
                    bizDateEnd: dataArr[i]
                }).then(res => res.result.data.rows.find(obj => obj.billId == supplementSales.result.data.val));
                expect(invFlow).to.not.be.undefined;
                let exp = getExpByBill.getInvFlowListExp(supplementSalesInfo.result.data);
                common.isApproximatelyEqualAssert(exp[styleInfo.tenantSpuId], invFlow);
            });
            describe('修改昨天的销售单', function () {
                before(async () => {
                    let json = basicJson.salesJson({ dwid: cust.result.data.val, styleInfo, proDate: dataArr[i] });
                    json.main.id = supplementSales.result.data.val;
                    //修改单据
                    supplementSales = await sspd.salesBill.saveSalesBill({ jsonParam: json });
                    await common.delay(1000);
                    supplementSalesInfo = await sspd.salesBill.getSalesBillFull({ isPend: 0, id: supplementSales.result.data.val });
                });
                it('对账单验证', async function () {
                    await custAcctCheck({ custAcctBef, billInfo: supplementSalesInfo.result.data });
                });
                it('销售单详情', async function () {
                    common.isApproximatelyEqualAssert(supplementSales.params.jsonParam, supplementSalesInfo.result.data);
                });
                it('销售单列表', async function () {
                    let supplementSalesList = await sspd.salesBill.getSalesBillList({
                        proDateGte: dataArr[i],
                        proDateLte: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.id == supplementSales.result.data.val));
                    expect(supplementSalesList, `销售单列表查询昨天的单据，没有查到数据`).to.not.be.undefined;
                    let exp = getExpByBill.getSalesBillListExp(supplementSalesInfo.result.data);
                    common.isApproximatelyEqualAssert(exp, supplementSalesList);
                });
                it('客户积分和余额', async function () {
                    let dwBalanceAft = await slh2.acct.getBalanceAndScore({
                        traderCap: 2,
                        traderKind: 2,
                        traderId: cust.result.data.val
                    });
                    let exp = getExpByBill.getBalanceAndScoreExp(supplementSalesInfo.result.data);
                    common.isApproximatelyEqualAssert(common.addObject(exp, dwBalanceBef.result.data), dwBalanceAft.result.data);
                });
                it.skip('积分明细', async function () {
                    let scoreDetail = await slh2.score.getCustScoreDetail({
                        traderId: cust.result.data.val, proDateGte: dataArr[i],
                        proDateLte: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.billNo == supplementSales.result.data.billNo));
                    //console.log(`新增scoreDetail=${JSON.stringify(scoreDetail)}`);
                    expect(scoreDetail, `积分明细`).to.not.be.undefined;
                    let exp = getExpByBill.getCustScoreDetailExp(supplementSalesInfo.result.data);
                    exp.score = format.numberFormat(common.div(supplementSales.params.jsonParam.main.totalMoney, 50), 3);
                    common.isApproximatelyEqualAssert(exp, scoreDetail);
                });
                it('库存流水', async function () {
                    let invFlow = await slh2.inv.getInvFlowList({
                        tenantSpuId: styleInfo.tenantSpuId, bizDateStart: dataArr[i],
                        bizDateEnd: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.billId == supplementSales.result.data.val));
                    expect(invFlow).to.not.be.undefined;
                    let exp = getExpByBill.getInvFlowListExp(supplementSalesInfo.result.data);
                    common.isApproximatelyEqualAssert(exp[styleInfo.tenantSpuId], invFlow);
                });

            });
            describe('作废昨天的单据', function () {
                before(async () => {
                    //作废昨天的单据
                    await sspd.salesBill.deleteSalesBill({ isPend: 0, id: supplementSales.result.data.val });
                    await common.delay(1000);
                    supplementSalesInfo = await sspd.salesBill.getSalesBillFull({ isPend: 0, id: supplementSales.result.data.val });
                });
                it('对账单验证', async function () {
                    let custAcctAftCancel = await getCustAcctForData({ custId: cust.result.data.val });
                    common.isApproximatelyEqualAssert(custAcctBef, custAcctAftCancel);
                });
                it('销售单详情', async function () {
                    common.isApproximatelyEqualAssert(supplementSales.params.jsonParam, supplementSalesInfo.result.data);
                });
                it('销售单列表', async function () {
                    let supplementSalesList = await sspd.salesBill.getSalesBillList({
                        proDateGte: dataArr[i],
                        proDateLte: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.id == supplementSales.result.data.val));
                    expect(supplementSalesList, `销售单列表查询昨天的单据，没有查到数据`).to.not.be.undefined;
                    let exp = getExpByBill.getSalesBillListExp(supplementSalesInfo.result.data);
                    common.isApproximatelyEqualAssert(exp, supplementSalesList);
                });
                it('客户积分和余额', async function () {
                    let dwBalanceAft = await slh2.acct.getBalanceAndScore({
                        traderCap: 2,
                        traderKind: 2,
                        traderId: cust.result.data.val
                    });
                    common.isApproximatelyEqualAssert(dwBalanceBef.result.data, dwBalanceAft.result.data);
                });
                it.skip('积分明细', async function () {
                    let scoreDetail = await slh2.score.getCustScoreDetail({
                        traderId: cust.result.data.val, proDateGte: dataArr[i],
                        proDateLte: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.billNo == supplementSales.result.data.billNo));
                    //console.log(`新增scoreDetail=${JSON.stringify(scoreDetail)}`);
                    expect(scoreDetail, `积分明细`).to.not.be.undefined;
                    let exp = getExpByBill.getCustScoreDetailExp(supplementSalesInfo.result.data);
                    exp.score = format.numberFormat(common.div(supplementSales.params.jsonParam.main.totalMoney, 50), 3);
                    common.isApproximatelyEqualAssert(exp, scoreDetail);
                });
                it('库存流水', async function () {
                    let invFlow = await slh2.inv.getInvFlowList({
                        tenantSpuId: styleInfo.tenantSpuId, bizDateStart: dataArr[i],
                        bizDateEnd: dataArr[i]
                    }).then(res => res.result.data.rows.find(obj => obj.billId == supplementSales.result.data.val));
                    expect(invFlow, `库存流水查到了刚刚作废的单据${invFlow}`).to.be.undefined;
                });
            });
        });

    };

});

//关于补登 获取昨天，今天，明天的账款信息
async function getCustAcctForData({ custId }) {
    let result = {};
    let dataArr = {
        lastDay: {
            proDateGte: common.getDateString([0, 0, -1]),
            proDateLte: common.getDateString([0, 0, -1]),
        },
        today: {
            proDateGte: common.getCurrentDate(),
            proDateLte: common.getCurrentDate(),
        },
        nextDay: {
            proDateGte: common.getDateString([0, 0, 1]),
            proDateLte: common.getDateString([0, 0, 1]),
        },
    };
    let qlParam = {
        shopId: LOGINDATA.shopId,
        compId: custId,
        orderBy: 'proDate desc, createdDate desc',
    };
    for (const key of Object.keys(dataArr)) {
        await accCheck.getSimpleCustAcctCheck(Object.assign({}, qlParam, dataArr[key])).then(res => result[key] = {
            proDate: res.params.proDateLte,
            endMoney: res.result.data.endMoney,
            beginMoney: res.result.data.beginMoney,
            rows: res.result.data.rows.slice(0, 5)
        });
    };
    return result;
};

//obj=qfRes.result.data
function getAcctChange(obj) {
    let acct = {
        lastDay: {
            endMoney: 0
        },
        today: { endMoney: 0 },
        nextDay: { endMoney: 0 },
    };
    switch (obj.main.proDate.split(' ')[0]) {
        case common.getDateString([0, 0, -1]):
            acct = common.addObject(acct, {
                lastDay: {
                    endMoney: obj.fin.balance,
                },
                today: {
                    endMoney: obj.fin.balance,
                    beginMoney: obj.fin.balance,
                },
                nextDay: {
                    endMoney: obj.fin.balance,
                    beginMoney: obj.fin.balance,
                },
            });
            break;
        case common.getCurrentDate():
            acct = common.addObject(acct, {
                today: {
                    endMoney: obj.fin.balance,
                },
                nextDay: {
                    endMoney: obj.fin.balance,
                    beginMoney: obj.fin.balance,
                },
            });
            break;
        case common.getDateString([0, 0, 1]):
            acct = common.addObject(acct, {
                nextDay: {
                    endMoney: obj.fin.balance
                },
            });
            break;
        default:
            break;
    };
    return acct;
};

//客户对账单验证
async function custAcctCheck({ custAcctBef, billInfo, }) {
    let custAcctAft = await getCustAcctForData({ custId: billInfo.main.compId });
    let changAcct = getAcctChange(billInfo);
    common.isApproximatelyEqualAssert(common.addObject(changAcct, custAcctBef), custAcctAft, ['rows']);
    for (const key of Object.keys(custAcctAft)) {
        let custAcct = custAcctAft[key].rows.find(ele => ele.id == billInfo.main.id);
        if (billInfo.main.proDate.includes(custAcctAft[key].proDate)) {
            let exp = getExpByBill.getCustSimpleAcctExp(billInfo);
            common.isApproximatelyEqualAssert(exp, custAcct, ['lastBalance']);
            common.isApproximatelyEqualAssert(common.add(billInfo.fin.balance, custAcctBef[key].endMoney), custAcct.lastBalance);
        } else {
            expect(custAcct, `查询出不是开单日期的单据${JSON.stringify(custAcctAft)}`).to.be.undefined;
        };
    };
};

