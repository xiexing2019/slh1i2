const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');


describe.skip('电子小票', function () {
    this.timeout(30000);
    let dresTicket, MergeTicket, salesSum;
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('新增销售单', function () {
        let json, sfRes, qfRes;
        before(async () => {
            dresTicket = await slh2.trade.getDresStyleSaleTicket();
            MergeTicket = await slh2.trade.shareMergeSaleBillTicket();
            salesSum = await slh2.trade.getCustSalesSummary();
            json = basicJson.salesJson();
            //获取起始库存,客户起始积分和余额
            //新增销售单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            console.log(sfRes);
            //销售单详情.开单后库存,客户积分和余额
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        it('1.款号拿货详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('2.客户拿货汇总', async function () {
        });
        it('3.客户拿货统计', async function () {
        });
    });
});