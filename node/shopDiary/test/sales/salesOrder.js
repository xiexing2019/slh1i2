const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('销售订单', function () {
    this.timeout(30000);
    let salesDetList;
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('新增销售订单', function () {
        let json, sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, saledata, sfRes2, qfRes2;
        before(async () => {
            json = basicJson.salesJson();
            json.payways[0].tallyType = 1;
            json.payways[0].payFlag = 1;
            //获取起始库存,账户流水
            [invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
            salesDetList = await sspd.salesBill.getSalesDetList();
            //新增销售订单
            sfRes = await sspd.salesOrder.saveSalesOrderBill(json);
            //console.log(sfRes);
            //销售订单详情
            qfRes = await sspd.salesOrder.getSalesOrderBillFull({
                id: sfRes.result.data.val
            });
            await common.delay(3000);
            qlRes = await sspd.salesOrder.salesOrderBillList();
            //销售订单列表
            saledata = await sspd.salesBill.getBillFullFromOrder({ orderId: sfRes.result.data.val }).then(res => res.result.data);
            //console.log(saledata);
            //delete saledata.isPend;
            saledata.payways = [{
                accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,
                typeId: 1,
                money: 0
            }];

            await common.delay(10000);
            //console.log(saledata);
            //按订单获取销售单数据,该接口通常需要4秒以上返回才会完整
            sfRes2 = await sspd.salesBill.saveSalesBill({
                jsonParam: saledata
            });
            await common.delay(5000);
            console.log(sfRes2);
            //订单全部发货
            qfRes2 = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes2.result.data.val
            });
            //发货生成的销售单详情
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        it.skip('test', async function () {
            //console.log(saledata);
        });
        it('销售订单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('销售订单列表', async function () {
            const exp = getExpByBill.getSalesOrderBillListExp(qfRes.result.data);
            console.log(`exp : ${JSON.stringify(exp)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
        });
        it('账户流水', async function () {
            //await common.delay(10000);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
            let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
        });
        describe('销售订单发货', function () {
            let salesDetAft;
            before(async () => {
                salesDetAft = await sspd.salesBill.getSalesDetList();
                invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
            });
            it('销售明细列表', async function () {
                let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes2.result.data, invSku: invSku2.result.data });
                //console.log(exp);
                //console.log(salesDetAft.result.data.rows.slice(0, exp.length));
                //console.log(invSku2.result.data);
                common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
            });
            it('销售明细汇总验证', async function () {
                let exp = getSalesDetSumExp(qfRes2.result.data);
                common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum || 0), salesDetAft.result.data.sum);
            });
            it('库存检查', async function () {
                //this.retries(5);
                //await common.delay(5000);
                const exp = getExpByBill.getOrderSkuExpByStyleId(qfRes2.result.data);
                console.log(invSku1.result.data);
                console.log(exp);
                console.log(invSku2.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data, ['spuSalesNum']);
            });
            it('库存流水验证', async function () {
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes2.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes2.result.data.val));
                //console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
                let exp = getExpByBill.getInvFlowListExp(qfRes2.result.data);
                //console.log(`exp=${JSON.stringify(exp)}`);
                common.isApproximatelyEqualAssert(exp[sfRes2.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
    });

    describe('修改销售订单', function () {
        let sfRes, qfRes, qfRes1, qlRes, accountList1;
        before(async () => {
            let json = basicJson.salesJson();
            json.payways[0].tallyType = 1;
            json.payways[0].payFlag = 1;
            //获取起始库存,供应商起始积分和余额
            accountList1 = await slh2.fin.getAccountList();
            //获取销售订单明细列表
            salesDetList = await sspd.salesBill.getSalesDetList();
            //新增销售订单
            sfRes = await sspd.salesOrder.saveSalesOrderBill(json);
            //第一次获取订单详情，取各种Id
            qfRes1 = await sspd.salesOrder.getSalesOrderBillFull({
                id: sfRes.result.data.val
            });

            let params = {
                rem: '修改销售订单',
            };
            json = basicJson.salesJson(params);
            json.main.id = sfRes.result.data.val; //修改时必传单据id
            json.payways[0].id = qfRes1.result.data.payways[0].id;
            json.details[0].id = qfRes1.result.data.details[0].id;
            json.details[1].id = qfRes1.result.data.details[1].id;
            json.payways[0].billId = qfRes1.result.data.fin.id;
            json.fin.balance = 0;
            //修改销售订单
            sfRes = await sspd.salesOrder.saveSalesOrderBill(json);
            //销售订单详情
            qfRes = await sspd.salesOrder.getSalesOrderBillFull({
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            qlRes = await sspd.salesOrder.salesOrderBillList();
            await common.delay(500);
            preRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.prepayId
            });
        });
        it('1.销售订单详情', async function () {
            console.log(sfRes.params.jsonParam);
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data, ['ver']);
        });
        it('2.销售订单列表', async function () {
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
        });
        it('账户流水', async function () {
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
            let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
        });
        it('预付款单', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, preRes.result.data, ['ver']);
        });
        it('单据历程', async function () {
            let billProgress = await sspd.salesBill.getSalesBillList({ relBillId: sfRes.result.data.val }).then(res => res.result.data.rows.find(obj => obj.relBillId == sfRes.result.data.val));
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, billProgress.result.data, ['ver']);
        });
    });

    describe('作废销售订单', function () {
        let json, sfRes, qfRes, preRes, accountList1;
        before(async () => {
            json = basicJson.salesJson();
            json.payways[0].tallyType = 1;
            json.payways[0].payFlag = 1;
            sfRes = await sspd.salesOrder.saveSalesOrderBill(json);
            await common.delay(10000);
            accountList1 = await slh2.fin.getAccountList();
            await sspd.salesOrder.deleteSalesOrderBill({ isPend: 0, id: sfRes.result.data.val });
            await common.delay(10000);

            qfRes = await sspd.salesOrder.getSalesOrderBillFull({
                isPend: 0,
                id: sfRes.result.data.val,
            });
            preRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.prepayId
            });
        });
        it('销售订单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
            expect(qfRes.result.data.main.flag).to.equal(-1);
        });
        it('账户流水', async function () {
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.prepayId));
            expect(accountFlowList, '作废销售订单后，账户流水仍然可以查到').to.be.undefined;
        });
        it('预付款单', async function () {
            expect(preRes.result.data.main.flag, '作废销售订单后，销售单列表仍然可以查到预付款单').to.equal(-1);
        });
        it('单据历程', async function () {
            let billProgress = await sspd.salesBill.getSalesBillList({ relBillId: sfRes.result.data.val }).then(res => res.result.data.rows.find(obj => obj.relBillId == sfRes.result.data.val));
            expect(billProgress.flag, '作废销售订单后，单据历程中状态不对').to.equal(-1);
        });
        describe('销售订单列表', function () {
            before(async function () {
                qlRes = await sspd.salesOrder.salesOrderBillList();
            });
            it('作废单据后订单列表验证', async function () {
                const exp = qlRes.result.data.rows.find(obj => obj.id == sfRes.result.data.val);
                expect(exp, '作废销售订单后，销售订单列表中还可以查到').to.be.undefined;
            });
        });
    });

    describe('终结销售订单', function () {
        let sfRes, qfRes, invSku1, invSku2, accountList1, accountList2;
        before(async () => {
            const json = basicJson.salesJson();
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            await common.delay(10000);
            [invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]),
                //作废单据
                await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
            await common.delay(10000);
            [qfRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val,
            }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
        });
        it('1.销售单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
            expect(qfRes.result.data.main.flag).to.equal(-1);
        });
        it('2.库存检查', async function () {
            this.retries(2); //库存不稳定
            const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
            common.isApproximatelyEqualAssert(common.subObject(invSku1.result.data, exp), invSku2.result.data, ['spuSalesNum']);
        });
        it('库存流水验证', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            expect(invFlowList, '作废销售单后，库存流水仍然可以查到').to.be.undefined;
        });
        it('3.支付账户信息检查', async function () {
            const exp = getExpByBill.getAccountListExp(qfRes.result.data);
            let jo1 = {},
                jo2 = {};
            accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
            accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
            common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
        });
        it('账户流水', async function () {
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            expect(accountFlowList, '作废销售单后，账户流水仍然可以查到').to.be.undefined;
        });
        it('销售明细列表验证', async function () {
            let salesDetAft = await sspd.salesBill.getSalesDetList().then(res => res.result.data.rows.find(obj => obj.billNo == sfRes.result.data.billNo));
            expect(salesDetAft, '作废销售单后销售明细列表中仍然可以找到').to.be.undefined;
        });
        describe('部分发货终结', function () {
            let qlRes = [];
            before(async function () {
                qlRes = await Promise.all([sspd.salesBill.getSalesBillList(), sspd.salesBill.getSalesBillList({
                    jsonParam: {
                        flag: 1
                    }
                }),
                sspd.salesBill.getSalesBillList({
                    jsonParam: {
                        flag: -1
                    }
                })
                ]);
            });
            it('1.作废单据信息验证', async function () {
                const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp, qlRes[0].result.data.rows[0]);
            });
            it('2.不传flag,显示全部', async function () {
                expect(qlRes[0].result.data.total).to.equal(common.add(qlRes[1].result.data.total, qlRes[2].result.data.total))
            });
            it('3.flag=1时,只显示正常单据', async function () {
                for (let i = 0; i < qlRes[1].result.data.rows.length; i++) {
                    const element = qlRes[1].result.data.rows[i];
                    if (element.flag !== 1) {
                        throw new Error('flag=1时,显示作废数据');
                    };
                };
            });
            it('4.flag=-1时,只显示作废单据', async function () {
                for (let i = 0; i < qlRes[2].result.data.rows.length; i++) {
                    const element = qlRes[2].result.data.rows[i];
                    if (element.flag !== -1) {
                        throw new Error('flag=-1时,显示正常数据');
                    };
                };
            });
        });
    });

    describe.skip('销售订单挂单', function () {
        let sfRes, qfRes, qlRes, invSku1, invSku2, invFlowListBef, dwBalance1, dwBalance2, accountList1, accountList2, salesBillList, getSalesBillListCount1, getSalesBillListCount2;
        before(async () => {
            const json = basicJson.salesJson({
                isPend: 1
            });
            //获取销售明细列表
            salesDetList = await sspd.salesBill.getSalesDetList();
            //获取起始库存,供应商起始积分和余额，销售单列表count
            [invSku1, dwBalance1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
            slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            }),
            slh2.fin.getAccountList()]);
            salesBillList = await sspd.salesBill.getSalesBillList();
            getSalesBillListCount1 = salesBillList.result.data.count;
            invFlowListBef = await slh2.inv.getInvFlowList({ tenantSpuId: json.details[0].tenantSpuId });
            //新增销售单-挂单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //console.log(sfRes);
            await common.delay(10000);
            //销售单列表.开单后库存,供应商积分和余额
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 1,
                id: sfRes.result.data.val
            });
            //console.log(qfRes);
            [qlRes, invSku2, dwBalance2, accountList2] = await Promise.all([sspd.trend.getSalesPendingList({
                jsonParam: {
                    compId: sfRes.params.jsonParam.main.compId
                }
            }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
            slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderKind: 2,
                traderId: json.main.compId
            }),
            slh2.fin.getAccountList()]);
            const res = qlRes.result.data.rows.find(obj => obj.id == sfRes.result.data.val);
            expect(res).to.not.be.undefined;
            //console.log(`qlRes : ${JSON.stringify(qlRes)}`);
            salesBillList = await sspd.salesBill.getSalesBillList();
            getSalesBillListCount2 = salesBillList.result.data.count;
        });
        it('1.销售单挂单详情', async function () {
            const exp = getExpByBill.getPurBillFullExp(sfRes);
            common.isApproximatelyEqualAssert(exp, qfRes.result.data);
        });
        it('2.销售挂单列表', async function () {
            const exp = getExpByBill.getSalesPendListExp(qfRes.result.data);
            //console.log(exp);
            //console.log(qlRes.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]); //挂单列表
        });
        it('3.库存检查', async function () {
            common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
        });
        it('库存流水验证', async function () {
            let invFlowListAft = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId });
            common.isApproximatelyEqualAssert(invFlowListBef.result.data, invFlowListAft.result.data);
        });
        it('4.客户积分/余额检查', async function () {
            common.isApproximatelyEqualAssert(dwBalance1.result.data, dwBalance2.result.data);
        });
        it('5.支付账户信息检查', async function () {
            //console.log(accountList1.result.data.rows[1]);
            //console.log(accountList2.result.data.rows[1]);
            common.isApproximatelyEqualAssert(accountList1.result, accountList2.result);
        });
        it('账户流水', async function () {
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            expect(accountFlowList, '新增销售单-挂单后，账户流水可以查到记录').to.be.undefined;
        });
        it('6.销售单列表查询挂单列表', async function () {
            expect(Number(getSalesBillListCount2), '进货单列表查询挂单有误').to.equal(Number(getSalesBillListCount1));
        });
        it('销售明细列表查询', async function () {
            let salesDetAft = await sspd.salesBill.getSalesDetList();
            common.isApproximatelyEqualAssert(salesDetList.result.data, salesDetAft.result.data);
        });
        it('7.删除挂单', async function () {
            await sspd.salesBill.deleteSalesBill({
                isPend: 1,
                id: sfRes.result.data.val,
            });
            let qlRes2 = await sspd.trend.getSalesPendingList({
                jsonParam: {
                    compId: sfRes.params.jsonParam.main.compId
                }
            });
            //console.log(`qlRes2.result.data.count : ${JSON.stringify(qlRes2.result.data.count)}`);
            expect(common.sub(qlRes.result.data.count, 1), '作废挂单，挂单列表显示有误').to.equal(Number(qlRes2.result.data.count));
        })
        it('8.挂单转正式单', async function () {
            let json = basicJson.salesJson({
                isPend: 1
            });
            //新增挂单
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            let qlRes1 = await sspd.trend.getSalesPendingList({
                jsonParam: {
                    compId: sfRes.params.jsonParam.main.compId
                }
            });
            delete json.isPend;
            json.pendId = sfRes.result.data.val;
            let sfResSales = await sspd.salesBill.saveSalesBill({
                jsonParam: json,
            });
            let qlRes2 = await sspd.trend.getSalesPendingList({
                jsonParam: {
                    compId: sfRes.params.jsonParam.main.compId
                }
            });
            //挂单转正式单，挂单列表中没有该张单据
            expect(common.sub(qlRes1.result.data.count, 1), '挂单转正式单后，挂单列表中数据有误').to.equal(Number(qlRes2.result.data.count));
            let qfResSales = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfResSales.result.data.val,
            });
            let qlResSales = await sspd.salesBill.getSalesBillList({
                jsonParam: {
                    searchToken: sfResSales.result.data.billNo,
                }
            });
            let salesExp = getExpByBill.getSalesBillListExp(qfResSales.result.data);
            //挂单转正式单，进货单列表中有该张单据，并且和qf中一样
            common.isApproximatelyEqualAssert(salesExp, qlResSales.result.data.rows[0]);
        });

    });
    describe('设置下次发货日期', function () {

        before(async () => {
            await sspd.salesOrder.setNextExpectDate({ id: sfRes.result.data.val, nextExpectDate: "2019-09-11" });
        });
        it('销售订单列表', async function () {
            let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
            //console.log(exp);
            //console.log(salesDetAft.result.data.rows.slice(0, exp.length));
            //console.log(invSku2.result.data);
            common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
        });
        it('销售订单详情', async function () {
            let exp = getSalesDetSumExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum || 0), salesDetAft.result.data.sum);
        });
    });
    describe('销售订单特殊货品', function () {
        let sfRes, qfRes, invSku1, invSku2, accountList1, accountList2;
        before(async () => {
            const json = basicJson.salesJson();
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            await common.delay(10000);
            [invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]),
                //作废单据
                await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
            await common.delay(10000);
            [qfRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val,
            }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
        });
        it('1.销售单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
            expect(qfRes.result.data.main.flag).to.equal(-1);
        });
        it('2.库存检查', async function () {
            this.retries(2); //库存不稳定
            const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
            common.isApproximatelyEqualAssert(common.subObject(invSku1.result.data, exp), invSku2.result.data, ['spuSalesNum']);
        });
        it('库存流水验证', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            expect(invFlowList, '作废销售单后，库存流水仍然可以查到').to.be.undefined;
        });
        it('3.支付账户信息检查', async function () {
            const exp = getExpByBill.getAccountListExp(qfRes.result.data);
            let jo1 = {},
                jo2 = {};
            accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
            accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
            common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
        });
        it('账户流水', async function () {
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
            expect(accountFlowList, '作废销售单后，账户流水仍然可以查到').to.be.undefined;
        });
        it('销售明细列表验证', async function () {
            let salesDetAft = await sspd.salesBill.getSalesDetList().then(res => res.result.data.rows.find(obj => obj.billNo == sfRes.result.data.billNo));
            expect(salesDetAft, '作废销售单后销售明细列表中仍然可以找到').to.be.undefined;
        });
        describe('部分发货终结', function () {
            let qlRes = [];
            before(async function () {
                qlRes = await Promise.all([sspd.salesBill.getSalesBillList(), sspd.salesBill.getSalesBillList({
                    jsonParam: {
                        flag: 1
                    }
                }),
                sspd.salesBill.getSalesBillList({
                    jsonParam: {
                        flag: -1
                    }
                })
                ]);
            });
            it('1.作废单据信息验证', async function () {
                const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp, qlRes[0].result.data.rows[0]);
            });
            it('2.不传flag,显示全部', async function () {
                expect(qlRes[0].result.data.total).to.equal(common.add(qlRes[1].result.data.total, qlRes[2].result.data.total))
            });
            it('3.flag=1时,只显示正常单据', async function () {
                for (let i = 0; i < qlRes[1].result.data.rows.length; i++) {
                    const element = qlRes[1].result.data.rows[i];
                    if (element.flag !== 1) {
                        throw new Error('flag=1时,显示作废数据');
                    };
                };
            });
            it('4.flag=-1时,只显示作废单据', async function () {
                for (let i = 0; i < qlRes[2].result.data.rows.length; i++) {
                    const element = qlRes[2].result.data.rows[i];
                    if (element.flag !== -1) {
                        throw new Error('flag=-1时,显示正常数据');
                    };
                };
            });
        });
    });
});

function getSalesDetSumExp(obj) {
    let exp = {
        money: obj.main.totalMoney,
        num: obj.main.totalNum,
        deliverNum: obj.main.totalNum,
        noDeliverNum: 0
    };
    return exp;
};