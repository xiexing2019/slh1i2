const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('销售单-online', function () {
	this.timeout(30000);
	let salesDetList;
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('新增销售单', function () {
		let json, sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			json = basicJson.salesJson();
			//获取起始库存,客户起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			salesDetList = await sspd.salesBill.getSalesDetList();
			//新增销售单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			console.log(sfRes);
			//销售单详情.开单后库存,客户积分和余额
			qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, accountList2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.fin.getAccountList()]);
			// console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
			// console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
		});
		it('1.销售单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.销售单列表', async function () {
			const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
			// console.log(`exp : ${JSON.stringify(exp)}`);
			// console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			//this.retries(5);
			//await common.delay(5000);
			invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			//console.log(invSku1.result.data);
			//console.log(exp);
			//console.log(invSku2.result.data);
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data, ['spuSalesNum']);
		});
		it.skip('4.支付账户信息检查', async function () {
			this.retries(5);
			await common.delay(500);
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			//console.log(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			//console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			//console.log(`exp=${JSON.stringify(exp)}`);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
		it('账户流水', async function () {
			//await common.delay(10000);
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		it.skip('销售单查询条件', async function () {

		});
		it.skip('销售单排序', async function () {

		});
		describe('销售明细验证', function () {
			let salesDetAft;
			before(async () => {
				salesDetAft = await sspd.salesBill.getSalesDetList();
			});
			it('销售明细列表', async function () {
				let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
				//console.log(exp);
				//console.log(salesDetAft.result.data.rows.slice(0, exp.length));
				//console.log(invSku2.result.data);
				common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
			});
			it('销售明细汇总验证', async function () {
				let exp = getSalesDetSumExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum || 0), salesDetAft.result.data.sum, ['compNum']);
			});
		});
	});

	describe('修改销售单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			let json = basicJson.salesJson();

			//获取起始库存,供应商起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			//获取销售明细列表
			salesDetList = await sspd.salesBill.getSalesDetList();
			//新增销售单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			let params = {
				rem: '修改销售单',
			};
			json = basicJson.salesJson(params);
			json.main.id = sfRes.result.data.val; //修改时必传单据id
			json.main.ver = sfRes.result.data.ver; //修改时必传
			//修改销售单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			//销售单列表.开单后库存,供应商积分和余额
			qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.销售单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data, ['ver']);
		});
		it('2.销售单列表', async function () {
			const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			/*console.log(exp);
			console.log(invSku1.result.data);
			console.log(invSku2.result.data);*/
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data, exp), invSku2.result.data, ['spuSalesNum']);
		});
		it('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			// console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			// console.log(`exp=${JSON.stringify(exp)}`);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);

		});
		it('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		describe('销售明细验证', function () {
			let salesDetAft;
			before(async () => {
				salesDetAft = await sspd.salesBill.getSalesDetList();
			});
			it('销售明细列表', async function () {
				let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
				//console.log(exp);
				//console.log(salesDetAft.result.data.rows);
				//console.log();
				common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
			});
			it('销售明细汇总验证', async function () {
				let exp = getSalesDetSumExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum), salesDetAft.result.data.sum);
			});
		});
	});
	describe('新增销售退货单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			const json = basicJson.salesJson({
				details: [{
					num: -5,
				}, {
					num: -10
				}]
			});
			//获取起始库存,供应商起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			//获取销售明细列表
			salesDetList = await sspd.salesBill.getSalesDetList();
			//新增销售单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			//销售单列表.开单后库存,供应商积分和余额
			qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.销售单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.销售单列表', async function () {
			const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data, exp), invSku2.result.data, ['spuSalesNum']);
		});
		it('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			// console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			// console.log(`exp=${JSON.stringify(exp)}`);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);

		});
		it('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		describe('销售明细验证', function () {
			let salesDetAft;
			before(async () => {
				salesDetAft = await sspd.salesBill.getSalesDetList();
			});
			it('销售明细列表', async function () {
				let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
				common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
			});
			it('销售明细汇总验证', async function () {
				let exp = getSalesDetSumExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum), salesDetAft.result.data.sum);
			});
		});
	});
	describe('新增销售单,同款号同颜色尺码，数量一样，既拿货又退货', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, accountList1, accountList2, custAcct1;
		before(async () => {
			let json = basicJson.salesJson({ details: [{ num: 10 }, { num: -10 }] });
			json.details[1].colorId = json.details[0].colorId;
			json.details[1].sizeId = json.details[0].sizeId;
			//获取起始库存,客户起始积分和余额
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			salesDetList = await sspd.salesBill.getSalesDetList();
			custAcct1 = await accCheck.getSimpleCustAcctCheck({
				shopId: json.main.shopId,
				compId: json.main.compId,
				pageSize: 0
			});
			// console.log(`dwBalance=${JSON.stringify(dwBalance)}`);
			//新增销售单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			//销售单列表.开单后库存,客户积分和余额
			qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val
			});
			//console.log(qfRes);
			await common.delay(10000);
			[qlRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			// console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
			// console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
		});
		it('1.销售单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.销售单列表', async function () {
			const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
		});
		it('3.库存检查', async function () {
			common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
		});
		it.skip('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			// console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
			let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
			// console.log(`exp=${JSON.stringify(exp)}`);
			common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
		});
		it.skip('4.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});

		it.skip('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });
			let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
			common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
		});
		it('客户对账单', async function () {
			let custAcct2 = await accCheck.getSimpleCustAcctCheck({
				shopId: sfRes.params.jsonParam.main.shopId,
				compId: sfRes.params.jsonParam.main.compId,
				pageSize: 0
			});
			common.isApproximatelyEqualAssert({
				id: sfRes.result.data.val,
				billNo: sfRes.result.data.billNo,
				lastBalance: custAcct1.result.data.rows[0].lastBalance,
			}, custAcct2.result.data.rows[0]);
		});
		it.skip('销售单查询条件', async function () {

		});
		it.skip('销售单排序', async function () {

		});
		describe('销售明细验证', function () {
			let salesDetAft;
			before(async () => {
				salesDetAft = await sspd.salesBill.getSalesDetList();
			});
			it('销售明细列表', async function () {
				let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
				common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
			});
			it('销售明细汇总验证', async function () {
				let exp = getSalesDetSumExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum || 0), salesDetAft.result.data.sum);
			});
		});

	});

	describe('作废销售单', function () {
		let sfRes, qfRes, invSku1, invSku2, accountList1, accountList2;
		before(async () => {
			const json = basicJson.salesJson();
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			await common.delay(10000);
			[invSku1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
			//作废单据
			await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
			await common.delay(10000);
			[qfRes, invSku2, accountList2] = await Promise.all([sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfRes.result.data.val,
			}), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId), slh2.fin.getAccountList()]);
		});
		it('1.销售单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
			expect(qfRes.result.data.main.flag).to.equal(-1);
		});
		it('2.库存检查', async function () {
			this.retries(2); //库存不稳定
			const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
			common.isApproximatelyEqualAssert(common.subObject(invSku1.result.data, exp), invSku2.result.data, ['spuSalesNum']);
		});
		it('库存流水验证', async function () {
			let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			expect(invFlowList, '作废销售单后，库存流水仍然可以查到').to.be.undefined;
		});
		it('3.支付账户信息检查', async function () {
			const exp = getExpByBill.getAccountListExp(qfRes.result.data);
			let jo1 = {},
				jo2 = {};
			accountList1.result.data.rows.forEach(element => jo1[element.id] = element);
			accountList2.result.data.rows.forEach(element => jo2[element.id] = element);
			common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			expect(accountFlowList, '作废销售单后，账户流水仍然可以查到').to.be.undefined;
		});
		it('销售明细列表验证', async function () {
			let salesDetAft = await sspd.salesBill.getSalesDetList().then(res => res.result.data.rows.find(obj => obj.billNo == sfRes.result.data.billNo));
			expect(salesDetAft, '作废销售单后销售明细列表中仍然可以找到').to.be.undefined;
		});
		describe('销售单列表', function () {
			let qlRes = [];
			before(async function () {
				qlRes = await Promise.all([sspd.salesBill.getSalesBillList(), sspd.salesBill.getSalesBillList({
					jsonParam: {
						flag: 1
					}
				}),
				sspd.salesBill.getSalesBillList({
					jsonParam: {
						flag: -1
					}
				})
				]);
			});
			it('1.作废单据信息验证', async function () {
				const exp = qlRes[0].result.data.rows.find(obj => obj.id == sfRes.result.data.val);
				expect(exp.flag, '作废销售单后，销售单列表中flag异常').to.equal(-1);
			});
			it('2.不传flag,显示全部', async function () {
				expect(qlRes[0].result.data.total).to.equal(common.add(qlRes[1].result.data.total, qlRes[2].result.data.total))
			});
			it('3.flag=1时,只显示正常单据', async function () {
				for (let i = 0; i < qlRes[1].result.data.rows.length; i++) {
					const element = qlRes[1].result.data.rows[i];
					if (element.flag !== 1) {
						throw new Error('flag=1时,显示作废数据');
					};
				};
			});
			it('4.flag=-1时,只显示作废单据', async function () {
				for (let i = 0; i < qlRes[2].result.data.rows.length; i++) {
					const element = qlRes[2].result.data.rows[i];
					if (element.flag !== -1) {
						throw new Error('flag=-1时,显示正常数据');
					};
				};
			});
		});
	});

	describe('挂单', function () {
		let sfRes, qfRes, qlRes, invSku1, invSku2, invFlowListBef, dwBalance1, dwBalance2, accountList1, accountList2, salesBillList, getSalesBillListCount1, getSalesBillListCount2;
		before(async () => {
			const json = basicJson.salesJson({
				isPend: 1
			});
			//获取销售明细列表
			salesDetList = await sspd.salesBill.getSalesDetList();
			//获取起始库存,供应商起始积分和余额，销售单列表count
			[invSku1, dwBalance1, accountList1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
			slh2.acct.getBalanceAndScore({
				traderCap: 2,
				traderKind: 2,
				traderId: json.main.compId
			}),
			slh2.fin.getAccountList()]);
			salesBillList = await sspd.salesBill.getSalesBillList();
			getSalesBillListCount1 = salesBillList.result.data.count;
			invFlowListBef = await slh2.inv.getInvFlowList({ tenantSpuId: json.details[0].tenantSpuId });
			//新增销售单-挂单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			//console.log(sfRes);
			await common.delay(10000);
			//销售单列表.开单后库存,供应商积分和余额
			qfRes = await sspd.salesBill.getSalesBillFull({
				isPend: 1,
				id: sfRes.result.data.val
			});
			//console.log(qfRes);
			[qlRes, invSku2, dwBalance2, accountList2] = await Promise.all([sspd.trend.getSalesPendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			}), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId),
			slh2.acct.getBalanceAndScore({
				traderCap: 2,
				traderKind: 2,
				traderId: json.main.compId
			}),
			slh2.fin.getAccountList()]);
			const res = qlRes.result.data.rows.find(obj => obj.id == sfRes.result.data.val);
			expect(res).to.not.be.undefined;
			//console.log(`qlRes : ${JSON.stringify(qlRes)}`);
			salesBillList = await sspd.salesBill.getSalesBillList();
			getSalesBillListCount2 = salesBillList.result.data.count;
		});
		it('1.销售单挂单详情', async function () {
			const exp = getExpByBill.getPurBillFullExp(sfRes);
			common.isApproximatelyEqualAssert(exp, qfRes.result.data);
		});
		it('2.销售挂单列表', async function () {
			const exp = getExpByBill.getSalesPendListExp(qfRes.result.data);
			//console.log(exp);
			//console.log(qlRes.result.data.rows[0]);
			common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]); //挂单列表
		});
		it('3.库存检查', async function () {
			common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
		});
		it('库存流水验证', async function () {
			let invFlowListAft = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId });
			common.isApproximatelyEqualAssert(invFlowListBef.result.data, invFlowListAft.result.data);
		});
		it('4.客户积分/余额检查', async function () {
			common.isApproximatelyEqualAssert(dwBalance1.result.data, dwBalance2.result.data);
		});
		it('5.支付账户信息检查', async function () {
			//console.log(accountList1.result.data.rows[1]);
			//console.log(accountList2.result.data.rows[1]);
			common.isApproximatelyEqualAssert(accountList1.result, accountList2.result);
		});
		it('账户流水', async function () {
			let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id, proDateGte: "2019-01-01" }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
			expect(accountFlowList, '新增销售单-挂单后，账户流水可以查到记录').to.be.undefined;
		});
		it('6.销售单列表查询挂单列表', async function () {
			expect(Number(getSalesBillListCount2), '进货单列表查询挂单有误').to.equal(Number(getSalesBillListCount1));
		});
		it('销售明细列表查询', async function () {
			let salesDetAft = await sspd.salesBill.getSalesDetList();
			common.isApproximatelyEqualAssert(salesDetList.result.data, salesDetAft.result.data);
		});
		it('7.删除挂单', async function () {
			await sspd.salesBill.deleteSalesBill({
				isPend: 1,
				id: sfRes.result.data.val,
			});
			let qlRes2 = await sspd.trend.getSalesPendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			});
			//console.log(`qlRes2.result.data.count : ${JSON.stringify(qlRes2.result.data.count)}`);
			expect(common.sub(qlRes.result.data.count, 1), '作废挂单，挂单列表显示有误').to.equal(Number(qlRes2.result.data.count));
		})
		it('8.挂单转正式单', async function () {
			let json = basicJson.salesJson({
				isPend: 1
			});
			//新增挂单
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: json
			});
			let qlRes1 = await sspd.trend.getSalesPendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			});
			delete json.isPend;
			json.pendId = sfRes.result.data.val;
			let sfResSales = await sspd.salesBill.saveSalesBill({
				jsonParam: json,
			});
			let qlRes2 = await sspd.trend.getSalesPendingList({
				jsonParam: {
					compId: sfRes.params.jsonParam.main.compId
				}
			});
			//挂单转正式单，挂单列表中没有该张单据
			expect(common.sub(qlRes1.result.data.count, 1), '挂单转正式单后，挂单列表中数据有误').to.equal(Number(qlRes2.result.data.count));
			let qfResSales = await sspd.salesBill.getSalesBillFull({
				isPend: 0,
				id: sfResSales.result.data.val,
			});
			let qlResSales = await sspd.salesBill.getSalesBillList({
				jsonParam: {
					searchToken: sfResSales.result.data.billNo,
				}
			});
			let salesExp = getExpByBill.getSalesBillListExp(qfResSales.result.data);
			//挂单转正式单，进货单列表中有该张单据，并且和qf中一样
			common.isApproximatelyEqualAssert(salesExp, qlResSales.result.data.rows[0]);
		});

	});



	for (let i = 0; i < 3; i++) {
		//关于积分的业务，单独验证
		describe('客户账款-对账单', function () {
			let qfRes, custAcct1, custAcct2, dwBalance1;
			before(async () => {
				let json = basicJson.salesJson();

				custAcct1 = await accCheck.getSimpleCustAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				});
				dwBalance1 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: json.main.compId
				});
				if (i == 0) json.payways = [];
				if (i == 2) json.payways[0].money = common.add(json.payways[0].money, 10);
				//新增销售单
				sfRes = await sspd.salesBill.saveSalesBill({
					jsonParam: json
				});
				qfRes = await sspd.salesBill.getSalesBillFull({
					isPend: 0,
					id: sfRes.result.data.val
				});
				//console.log(`qfRes.result : ${JSON.stringify(qfRes.result)}`);
				custAcct2 = await accCheck.getSimpleCustAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				});
			});

			it('客户对账单', async function () {
				let exp = getExpByBill.getCustSimpleAcctExp(qfRes.result.data);
				//console.log(`exp : ${JSON.stringify(exp)}`);
				common.isApproximatelyEqualAssert(exp, custAcct2.result.data.rows[0]);
				common.isApproximatelyEqualAssert({
					total: common.add(custAcct1.result.data.total, 1),
					beginMoney: common.sub(custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].lastBalance, custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].balance),
					endMoney: common.add(custAcct1.result.data.endMoney, qfRes.result.data.fin.balance),
					sum: {
						payMoney: common.add(qfRes.result.data.fin.payMoney, custAcct1.result.data.sum.payMoney),
						totalMoney: common.add(qfRes.result.data.main.totalMoney, custAcct1.result.data.sum.totalMoney),
					},
				}, custAcct2.result.data);
			});
			it('客户余额和积分', async function () {
				await common.delay(10000);
				let dwBalance2 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: sfRes.params.jsonParam.main.compId
				});
				const exp = getExpByBill.getBalanceAndScoreExp(qfRes.result.data);
				// console.log(`dwBalance1.result.data=${JSON.stringify(dwBalance1.result.data)}`);
				// console.log(`dwBalance2.result.data=${JSON.stringify(dwBalance2.result.data)}`);
				common.isApproximatelyEqualAssert(common.addObject(dwBalance1.result.data, exp), dwBalance2.result.data);
			});
		});
		describe('作废销售单后客户对账单', function () {
			let qfRes, dwBalance1, custAcct1, custAcct2;
			before(async () => {
				let json = basicJson.salesJson();

				if (i == 0) json.payways = [];
				if (i == 2) json.payways[0].money = common.add(json.payways[0].money, 10);
				//新增销售单
				sfRes = await sspd.salesBill.saveSalesBill({
					jsonParam: json
				});
				qfRes = await sspd.salesBill.getSalesBillFull({
					isPend: 0,
					id: sfRes.result.data.val
				});
				dwBalance1 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: json.main.compId
				});
				custAcct1 = await accCheck.getSimpleCustAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				});
				//作废单据
				await sspd.salesBill.deleteSalesBill({
					isPend: 0,
					id: sfRes.result.data.val
				});
				custAcct2 = await accCheck.getSimpleCustAcctCheck({
					shopId: json.main.shopId,
					compId: json.main.compId,
					pageSize: 0
				});
				//console.log(`custAcct1 : ${JSON.stringify(custAcct2)}`);
			});
			it('客户对账单', async function () {
				common.isApproximatelyEqualAssert({
					total: common.sub(custAcct1.result.data.total, 1),
					beginMoney: common.sub(custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].lastBalance, custAcct1.result.data.rows[custAcct1.result.data.rows.length - 1].balance),
					endMoney: common.sub(custAcct1.result.data.endMoney, qfRes.result.data.fin.balance),
					sum: {
						payMoney: common.sub(custAcct1.result.data.sum.payMoney, qfRes.result.data.fin.payMoney),
						totalMoney: common.sub(custAcct1.result.data.sum.totalMoney, qfRes.result.data.main.totalMoney),
					},
				}, custAcct2.result.data);
			});
			it('客户余额和积分', async function () {
				//console.log('查一遍积分和余额' + common.getCurrentTime());
				//this.retries(5);
				await common.delay(10000);
				let dwBalance2 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: sfRes.params.jsonParam.main.compId
				});
				const exp = getExpByBill.getBalanceAndScoreExp(qfRes.result.data);
				common.isApproximatelyEqualAssert(common.subObject(dwBalance1.result.data, exp), dwBalance2.result.data);
			});
		});
	};
	describe('销售单数据隔离', function () {
		before(async () => {
			await loginReq.changeShopFast({ shopName: '门店二' });
		});
		after(async () => {
			await loginReq.changeShop({ shopName: '常青店' });
		});
		it('其他门店销售单列表查询单据', async function () {
			let saleList = await sspd.salesBill.getSalesBillList({ proDateGte: common.getDateString([0, 0, -7]), proDateLte: common.getCurrentDate(), }).then(res => res.result.data.rows.find(data => data.shopId != LOGINDATA.shopId));
			expect(saleList).to.be.undefined;
		});
		it('其他门店销售明细查询单据--有问题', async function () {
			let salesDetAft = await sspd.salesBill.getSalesDetList({ proDateGte: common.getDateString([0, 0, -7]), proDateLte: common.getCurrentDate(), }).then(res => res.result.data.rows.find(obj => obj.shopId != LOGINDATA.shopId));
			expect(salesDetAft).to.be.undefined;
		});
	});
});

function getSalesDetSumExp(obj) {
	let exp = {
		money: obj.main.totalMoney,
		num: obj.main.totalNum,
		deliverNum: obj.main.totalNum,
		noDeliverNum: 0
	};
	return exp;
};

