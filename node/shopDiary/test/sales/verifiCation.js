const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe.skip('核销', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('销售核销 ', function () {
        let json, sfRes, qfRes;
        before(async () => {
            json = basicJson.salesJson();
            //console.log(json);
            json.payways = [];
            json.fin.balance = json.main.totalMoney;
            //置空payways欠款
            //新增销售单,欠款
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //console.log(sfRes);
            //销售单详情
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        it('', async function () {
            console.log(json);
        });
        it('单据详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });

        it('收付款单列表', async function () {
            const verifylist = await sspd.finIo.findCompVerifyList();
            console.log(verifylist);
        });
        it('核销', async function () {
            const verifylist = await sspd.finIo.findCompVerifyList();
            console.log(verifylist);
            let idsCollection = [];
            let verifyMoney = 0;
            verifylist.result.data.rows.forEach(obj => {
                idsCollection.push(obj.id);
                verifyMoney += obj.balance;
            });
            let verifyBillIds = idsCollection.toString();
            console.log(verifyBillIds);
            console.log(verifyMoney);
            let verJson = basicJson.salesJson();
            //console.log(json);
            verJson.fin.verifyBillIds = verifyBillIds;
            verJson.fin.verifyMoney = verifyMoney;
            //累计结余到本单
            const sfRes2 = await sspd.salesBill.saveSalesBill({
                jsonParam: verJson
            });
            console.log(sfRes2);
            //本单详情
            const qfRes2 = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes2.result.data.val
            });
            await common.delay(10000);
            console.log(qfRes2);
        });
    });
    describe('采购核销 ', function () {
        let json, sfRes, qfRes;
        before(async () => {
            json = basicJson.purJson();
            json.payways = [];
            json.fin.balance = json.main.totalMoney;
            //console.log(json);
            //新增采购单,欠款
            sfRes = await billReqHandler.savePurchaseBill({
                jsonParam: json
            });
            //console.log(sfRes);
            //采购单详情
            qfRes = await slh2.trade.getPurchaseBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
            // console.log(`qfRes.result.data : ${JSON.stringify(qfRes.result.data)}`);
            // console.log(`qlRes.result.data.rows[0] : ${JSON.stringify(qlRes.result.data.rows[0])}`);
        });
        it('单据详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });

        it('收付款单列表', async function () {

        });
        it('核销', async function () {

        });
    });
});
