const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2/index');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('上次价', function () {
    let styleInfo;
    this.timeout(30000);
    let dresRes = {};

    before(async function () {
        await loginReq.sspdLoginAndChangShop();
        // console.log(`BASICDATA=${JSON.stringify(BASICDATA.styleInfo.agc001)}`);

        const val = await slh2.config.getParamValues({ codes: 'sales_show_lastprice,ignorecolorsize', domainKind: 'business' });
        // console.log(val);

        dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
        styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
    });

    it('无预留价,无上次价', async function () {
        const lastPrice = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        // console.log(`lastPrice=${JSON.stringify(lastPrice)}`);
        expect(lastPrice.result.data.rows[0]).to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: '' });
    });

    it('取预留价', async function () {
        const lastPrice = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId, priceType: 0 });
        // console.log(`lastPrice=${JSON.stringify(lastPrice)}`);
        expect(lastPrice.result.data.rows[0]).to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: dresRes.params.jsonParam.spuCommonDto.stdprice1 });
    });

    it.skip('取上次价', async function () {
        // console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
        const sfRes = await sspd.salesBill.saveSalesBillPayOnline({ jsonParam: basicJson.salesPayOnlineJson({ styleInfo }) });
        const lastPrice = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        expect(lastPrice.result.data.rows[0]).to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: sfRes.params.jsonParam.details[0].realPrice.toString() });
    });
    it.skip('同款不同价-上次价', async function () {
        let json = basicJson.salesPayOnlineJson({ styleInfo, });
        //修改第一条明细的价格
        json.details[0].realPrice = 230;
        await sspd.salesBill.saveSalesBillPayOnline({ jsonParam: json });
        const lastPrice = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        //同款不同价上次价取值---单据中该款第一条明细的价格
        expect(lastPrice.result.data.rows[0], '同款不同价时上次价取值错误').to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: json.details[0].realPrice.toString() });
    });
    it('两个客户连续开单，第一个客户的上次价', async function () {
        let cust1 = await slh2.mdm.getCustList().then(res => res.result.data.rows.find(data => data.id != BASICDATA.ids.dwidXw));
        //cust1开单
        let sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: basicJson.salesJson({ styleInfo, price: 220, dwid: cust1.id }) });
        //小王开单
        await sspd.salesBill.saveSalesBill({ jsonParam: basicJson.salesJson({ styleInfo }) });
        const lastPrice = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, compId: cust1.id, shopId: LOGINDATA.shopId });
        expect(lastPrice.result.data.rows[0], '两个客户连续开单，第一个客户的上次价取值错误').to.includes({ tenantSpuId: dresRes.result.data.tenantSpuId.toString(), price: sfRes.params.jsonParam.details[0].realPrice.toString() });
    });
    describe('异常', function () {
        it('没有客户', async function () {
            const lastPriceList = await sspd.salesBill.getSalesDetList({ tenantSpuIds: dresRes.result.data.tenantSpuId, shopId: LOGINDATA.shopId });
            expect(lastPriceList.result.data.rows.length, '没有客户上次价接口返回了结果').to.equal(0);
        });

    })

});