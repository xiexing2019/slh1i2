"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe.skip('四舍五入', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginBySeq();
    });
    after(async () => {
        await slh2.config.saveParamValue({
            data: [{ code: "sales_roundoff_mode", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
        });
        await slh2.config.saveParamValue({
            data: [{ code: "sales_detail_roundoff_mode", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
        });
    });
    describe('总计需要四舍五入', function () {
        let sfRes, qfRes, styleInfo;
        before(async () => {
            await slh2.config.saveParamValue({
                data: [{ code: "sales_roundoff_mode", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
            });
            //val1需要0不需要
            //参数开启状态
            styleInfo = await slh2.dres.getStyleInfo({ id: 293545 }).then((res) => res.result.data);
            //获取货品详情
            const json = basicJson.salesJson({ styleInfo: styleInfo });
            //console.log(json);
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //新增销售单
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //销售单详情
        });
        it('1.销售单详情', async function () {
            //totalmoney
            //details里的Money
            //console.log(sfRes);
            //console.log(qfRes);
            //common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('2.销售单列表', async function () {
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
        });
    });
    describe.skip('小计需要四舍五入', function () {
        let sfRes, qfRes;
        before(async () => {
            await slh2.config.saveParamValue({
                data: [{ code: "sales_detail_roundoff_mode", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
            });
            //客户共享
            //val1需要0不需要
            //参数开启状态
            const json = basicJson.salesJson();
            sfRes = await sspd.salesBill.saveSalesBill({
                jsonParam: json
            });
            //新增销售单
            qfRes = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            //销售单详情
        });
        it('1.销售单详情', async function () {
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('2.销售单列表', async function () {
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
        });
    });
});