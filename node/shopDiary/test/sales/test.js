"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');


describe('销售单', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();

	});
	describe('', function () {
		let sfRes;
		before(async () => {
			sfRes = await sspd.salesBill.saveSalesBill({
				jsonParam: basicJson.salesJson(),
			})
		})

		it('销售单明细列表', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-salesBill-findDetailDTO',

			});
		});
		it('按批次对账单', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-sspd-accCheck-findAccCheckByBill',
				jsonParam: {},
				proDateGte: common.getCurrentDate(),
				proDateLte: common.getCurrentDate(),
				bizType: 1, //类型1为销售
				compId: BASICDATA.ids.dwidXw,
				shopId: LOGINDATA.shopId,
			});
			//console.log(`rs : ${JSON.stringify(rs)}`);
		});
		it('按明细对账单', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-sspd-accCheck-findAccCheckByDetail',
				jsonParam: {},
				proDateGte: common.getCurrentDate(),
				proDateLte: common.getCurrentDate(),
				bizType: 1, //类型1为销售
				compId: BASICDATA.ids.dwidXw,
				shopId: LOGINDATA.shopId,
			});
			//console.log(`rs : ${JSON.stringify(rs)}`);
		});
		it.skip('销售单打印', async function () {
			//笑铺目前只支持几种打印机  120思普瑞特890 180芝柯HDT334  190芝柯XT423 200思普瑞特POS587
			await common.apiDo({
				apiKey: 'ec-sspd-salesBill-printbill',
				jsonParam: {
					id: sfRes.result.data.val,
					// printerMac:  //打印机mac地址
					// printerType: , //打印机类型
				},
			});
		});
		it('销售单分享', async function () {
			let rs = await common.apiDo({
				apiKey: 'ec-sspd-salesBill-shareBill',
				jsonParam: {
					id: sfRes.result.data.val,
				},
			});
			//console.log(`rs : ${JSON.stringify(rs)}`);
		});
	});
});
