"use strict"
const common = require('../../../lib/common');
const fs = require('fs');
const path = require('path');
const convert = require('xml-js');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe.skip('权限', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('功能列表', function () {
		let exp = {};
		before(async function () {
			const sspdFuncXml = fs.readFileSync(path.join(__dirname, '../../doc/sspdFunc.xml'));
			const result = convert.xml2js(sspdFuncXml, {
				compact: true
			});
			exp = sspdFuncXmlFormat(result.menus.menu);
			//console.log(`exp=${JSON.stringify(exp)}`);
		});
		it('获取当前登录用户可访问功能', async function () {
			const res = await slh2.config.getUserFuncs({
				productVersion: LOGINDATA.productVersion
			});
			//console.log(res.result.data.rows);
			common.isApproximatelyEqualAssert(exp, res.result.data.rows);
		});
		it('所有功能列表', async function () {
			//参数sspd_show_multiple_price需打开，否则411-70权限获取不到
			await slh2.config.saveParamValue({
				check: false,
				data: [{
					code: "sspd_show_multiple_price",
					domainKind: "business",
					ownerId: LOGINDATA.unitId,
					val: "1"
				}]
			});
			//const res = await slh2.getAllFunc();
			common.isApproximatelyEqualAssert(exp, res.result.data.options);

		});

	})
	//笑铺不用这里的接口，
	describe.skip('职位', function () {
		before(async () => {
			await common.apiDo({
				apiKey: 'ec-om-role-saveAll',
				jsonParam: {
					//	name: ,
					flag: 1,
					funcs: [{
						//	id: ,
						//	name: ,
					}, {
						//id: ,
						//	name: ,
					}]
				},
			});
		});
		it('获取职位(角色)详情', async function () {
			await common.apiDo({
				apiKey: 'ec-om-role-getWithFuncById',
				//	id: , //职位编号
			});
		});
		it('系统职位(角色)重置', async function () {
			await common.apiDo({
				apiKey: 'ec-om-role-reset',
				//id: , //职位编号
			});
		});
	});

	describe.skip('角色', function () {
		let json;
		before(async () => {
			json = {
				name: '新增角色' + common.getRandomStr(6),
				sysFlag: 0,
				funcs: [],
				cols: [{
					colAlias: 'purprice', //是否查看进货价
					mode: 0, //0为否
				}, {
					colAlias: 'styleproviderid', //是否查看供应商
					mode: 0, //0为否
				}],
				showFlag: 1,
			};

			saveRes = await slh2.om.saveWithPrivs({
				jsonParam: json
			});
			privsInfo = await slh2.om.getPrivsInfo({
				id: saveRes.result.data.val
			})
			privsList = await slh2.om.getPrivsList().then(obj => common.takeWhile(obj.result.data.rows, ele => ele.id == save.result.data.val));
			//console.log(`sfRes : ${JSON.stringify(sfRes)}`);
		});
		after(async () => {
			await slh2.om.privsDisenable({
				id: sfRes.result.data.val
			})
		})
		it('角色详情', async function () {
			common.isApproximatelyEqualAssert(saveRes.params.jsonParam, privsInfo);
			await common.apiDo({
				apiKey: 'ec-om-role-getWithPrivs', //ec-om-role-getById
				id: sfRes.result.data.val,
				wrapper: true,
			});
		});
		it('角色列表', async function () {

			// common.isApproximatelyEqualAssert(, privsList.reset.data.rows[0]);
			await common.apiDo({
				apiKey: 'ec-om-role-list',
				pageSize: 0,
				jsonParam: {
					wrapper: true,
					flag: 1,
				},
			});
		});
		describe('', function () {
			before()

			it('停用角色', async function () {
				await common.apiDo({
					apiKey: 'ec-om-role-disable',
					id: sfRes.result.data.val,
				});
			});
			it('启用角色', async function () {
				await common.apiDo({
					apiKey: 'ec-om-role-enable',
					id: sfRes.result.data.val,
				});
			});
		})
	});

	describe('门店', function () {
		describe('门店', function () {
			it('门店列表', async function () {
				await common.apiDo({
					apiKey: 'ec-mdm-org-shop-getShop',
					id: LOGINDATA.shopId,
				});
			});


		});
		describe.skip('门店二维码', function () {
			let sfRes;
			before(async () => {
				sfRes = await common.apiDo({
					apiKey: 'ec-mdm-org-shop-qrcode-save',
					jsonParam: {
						shopId: LOGINDATA.shopId,
						// qrContent: , //二维码内容
						// qrImg: , //中间图片id
						// rem: , //二维码备注
					},
				});
			});
			it('删除门店二维码', async function () {
				await common.apiDo({
					apiKey: 'ec-mdm-org-shop-qrcode-delete',
					id: sfRes.result.data.val,
				});
			});
			it('获取门店二维码列表', async function () {
				await common.apiDo({
					apiKey: 'ec-mdm-org-shop-qrcode-findByShopId',
					wrapper: true,
					shopId: LOGINDATA.shopId,
				});
			});

			it('批量删除门店二维码', async function () {
				await common.apiDo({
					apiKey: 'ec-mdm-org-shop-qrcode-multiDelete',
					//	ids: , //
				});
			});

		});
	});
});

function sspdFuncXmlFormat(curMenu, parentMenu = {}) {
	let exp = [];
	if (_.isArray(curMenu)) {
		curMenu.forEach((_curMenu) => {
			if (_curMenu._attributes.termCap != 1) exp.push(sspdFuncXmlFormat(_curMenu, parentMenu)); //if (_curMenu._attributes.termCap != 1)
		});
		return exp;
	} else if (curMenu._attributes.termCap != 1) { //
		const parentCode = parentMenu.code ? `${parentMenu.code}-` : '';
		exp = {
			deep: common.add(parentMenu.deep || 0, 1),
			code: `${parentCode}${curMenu._attributes.id}`,
			hasChild: 'false',
			value: curMenu._attributes.name,
		};
		if (curMenu.menu) {
			if (!_.isArray(curMenu.menu)) curMenu.menu = [curMenu.menu];
			exp.items = sspdFuncXmlFormat(curMenu.menu, exp);
			exp.hasChild = exp.items && exp.items.length > 0 ? 'true' : 'false';
		};
		return exp;
	};
};
