"use strict";
const common = require('../../../lib/common');
const format = require('../../../data/format');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');
const staffManage = require('../../help/staffManage');

describe.skip('特殊授权', function () {
    this.timeout(30000);
    let checkRes, roleList, shopInfo, shopIds = [], shopId1 = [],
        authListBef = {}, authListAft = {},
        staffInfo = {};
    const staff = staffManage.setupStaff();
    before('给未授权的员工特殊授权', async function () {
        await loginReq.sspdLoginAndChangShop();
        checkRes = await slh2.mdm.checkSpeAuth();
        authListBef = await slh2.mdm.getSpeAuthedStaffList();
        //已特殊授权员工列表
        roleList = await slh2.om.getPrivsList();
        const randomNum = common.getRandomNum(0, roleList.result.data.rows.length - 1);
        //角色列表
        await staff.saveStaff({
            userName: '员工' + common.getRandomStr(6),
            mobile: '',
            depId: LOGINDATA.depId,
            roleIds: roleList.result.data.rows[randomNum].id,
            rem: '',
        });
        //新增员工
        //console.log(staff);
        shopInfo = await slh2.mdm.getShopListByDevice().then(res => res.result.data.rows);
        shopInfo.forEach(element => {
            shopIds.push({ id: element.id, unitId: element.unitId });
        });
        shopId1.push(shopIds[0]);
        console.log(shopId1);
        await staff.authorizeSpeStaff({
            authPhone: common.getRandomMobile(),
            verifyCode: '0000',
            shopIds: shopIds
        });
        //特殊授权
        console.log(staff);
        await common.delay(3000);
        authListAft = await slh2.mdm.getSpeAuthedStaffList();
    });
    after(async function () {
        //await loginReq.sspdLoginBySeq();
        /*await slh2.mdm.unathorizeSpeStaff({
            staffId: staff.id
        });*/
    });
    it('判断租户是否购买特殊授权', async function () {
        expect(checkRes.result.data.val).to.be.true;
    });
    it('授权后,特殊授权员工列表', async function () {
        let listElement = authListAft.result.data.staffList.find(obj => obj.code == staff.phone);
        console.log(`listElement : ${JSON.stringify(authListAft.result.data.staffList)}`);
        common.isApproximatelyEqualAssert(staff, listElement, ['id', 'mobile', 'depId'], '特殊员工列表信息有误');
        //console.log(listElement);
    });
    it('授权后,特殊授权数', async function () {
        expect(authListAft.result.data.authorizedNum - authListBef.result.data.authorizedNum).to.equal(1);
    });
    it('授权后,可登录门店列表', async function () {
        //console.log(staffInfo);
        this.retries(3);
        await common.delay(500);
        staffInfo = await staff.getSpeAuthedStaffInfo().then(res => res.result.data.shopList);
        common.isApproximatelyArrayAssert(shopIds, staffInfo, [], '可登录门店列表有误');
    });
    it('授权后,员工登录并切换门店', async function () {
        await loginReq.sspdLogin({
            mobile: staff.phone,
        });
        //console.log(LOGINDATA);
        await loginReq.changeShopFast({ shopName: '门店二' });
        await common.delay(3000);
        await loginReq.changeShopFast();
    });
    describe('取消特殊授权', function () {
        let authList;
        before('取消特殊授权', async function () {
            await loginReq.sspdLoginBySeq();
            await staff.unathorizeSpeStaff({
                authPhone: staff.phone,
                verifyCode: '0000'
            });
            await common.delay(3000);
            authList = await slh2.mdm.getSpeAuthedStaffList();
            //取消特殊授权，查询特殊授权员工列表
        });
        after(async function () {
            //重新特殊授权
            await loginReq.sspdLoginBySeq();
            await staff.authorizeSpeStaff({
                authPhone: staff.phone,
                verifyCode: '0000',
                shopIds: shopIds
            });
        })
        it('取消授权后,已授权员工列表检查', async function () {
            let listElement = authList.result.data.staffList.find(obj => obj.code == staff.phone);
            expect(listElement).to.be.undefined;
        });
        it('取消授权后,授权数', async function () {
            expect(authListBef.result.data.authorizedNum).to.equal(authList.result.data.authorizedNum);
        });
        it('取消授权后,获取验证码', async function () {
            const res = await slh2.confc.getServerURLByDevice({ bdomainCode: "slh", deviceNo: staff.phone, productType: 8 });
            expect(res.result.msgId).to.equal("device_not_exists_notice");
        });
        it.skip('取消授权后,员工登录', async function () {
            const loginRes = await loginReq.sspdLogin({
                mobile: staff.phone,
                check: false
            }).catch(err => err.message);
            console.log(loginRes);
        });
    });
    describe('单店停用特殊授权员工', function () {
        let staffList1 = {}, staffList2 = {};
        before(async function () {
            await staff.disableStaff();
            staffList1 = await slh2.mdm.getStaffList().then(res => res.result.data.rows.find(obj => obj.code == staff.phone));
            await loginReq.changeShopFast({ shopName: '门店二' });
            await common.delay(3000);
            staffList2 = await slh2.mdm.getStaffList().then(res => res.result.data.rows.find(obj => obj.code == staff.phone));
            await loginReq.changeShopFast();
            await common.delay(3000);
        });
        it('单门店停用已特殊授权的员工，本门店为停用', async function () {
            expect(staffList1.flag).to.equal(0);
        });
        it('单门店停用已特殊授权的员工，其他门店为停用', async function () {
            expect(staffList2.flag).to.equal(0);
        });
        describe('单门店启用已停用特殊授权员工', function () {
            let staffList3 = {};
            before(async function () {
                await staff.enableStaff();
                await staff.authorizeSpeStaff({
                    authPhone: staff.phone,
                    verifyCode: '0000',
                    shopIds: shopId1
                });
                await loginReq.changeShopFast({ shopName: '门店二' });
                staffList3 = await slh2.mdm.getStaffList().then(res => res.result.data.rows.find(obj => obj.code == staff.phone));
            });
            after(async function () {
                await staff.authorizeSpeStaff({
                    authPhone: common.getRandomMobile(),
                    verifyCode: '0000',
                    shopIds: shopIds
                });
                await common.delay(3000);
                await loginReq.changeShopFast();
            });
            it('单门店再启用，其他门店为停用', async function () {
                expect(staffList3.flag).to.equal(0);
            });
            it('单门店再特殊授权，其他门店均为特殊授权', async function () {
                expect(staffList3.authFlag).to.equal(1);
            });
        });
    });
    describe('编辑可用门店', function () {
        let staffList4 = {};
        before(async function () {
            await staff.authorizeSpeStaff({
                authPhone: staff.phone,
                verifyCode: '0000',
                shopIds: shopId1
            });
            await loginReq.changeShopFast({ shopName: '门店二' });
            staffList4 = await slh2.mdm.getStaffList().then(res => res.result.data.rows.find(obj => obj.code == staff.phone));
        });
        it('不可用门店内依然为特殊授权状态', async function () {
            expect(staffList4.authFlag).to.equal(1);
        });
        it.skip('绑定门店列表找不到该员工', async function () {

        });
        it('切不到不可用门店', async function () {
            staffInfo = await staff.getSpeAuthedStaffInfo().then(res => res.result.data.shopList);
            common.isApproximatelyArrayAssert(shopId1, staffInfo, [], '可切换门店列表有误');
        });
        it('不能切的门店里停用,其他门店正常', async function () {
            expect(staffList4.flag).to.equal(0);
        });
    });
});