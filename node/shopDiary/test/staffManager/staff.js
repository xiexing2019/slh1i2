"use strict"
const common = require('../../../lib/common');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe.skip('员工权限', function () {
    this.timeout(30000);
    before(async () => {
        //await loginReq.sspdLoginAndChangShop();
    });
    describe('不允许查看多门店库存', function () {
        before(async () => {
            await loginReq.sspdLoginAndChangShop();
            const json = { "name": "营业员", "sysFlag": 0, "funcs": [{ "id": "400", "name": "日记" }, { "id": "400-50", "name": "日记" }, { "id": "400-50-10", "name": "查看销售" }, { "id": "400-50-20", "name": "查看客户" }, { "id": "400-50-30", "name": "查看货品" }, { "id": "400-50-40", "name": "查看利润" }, { "id": "400-80", "name": "店员业绩" }, { "id": "401", "name": "卖货" }, { "id": "401-10", "name": "卖货查询" }, { "id": "401-20", "name": "卖货新增" }, { "id": "401-30", "name": "卖货修改" }, { "id": "401-40", "name": "发货" }, { "id": "401-50", "name": "卖货作废" }, { "id": "401-70", "name": "修改单价" }, { "id": "401-90", "name": "抹零" }, { "id": "404", "name": "货品" }, { "id": "404-10", "name": "货品查询" }, { "id": "404-20", "name": "查看货品流水" }, { "id": "404-30", "name": "货品新增" }, { "id": "404-40", "name": "货品修改" }, { "id": "405", "name": "客户" }, { "id": "405-10", "name": "客户查询" }, { "id": "405-20", "name": "客户积分调整" }, { "id": "405-30", "name": "客户充值" }, { "id": "405-40", "name": "客户收款" }, { "id": "405-50", "name": "客户新增" }, { "id": "405-60", "name": "客户修改" }, { "id": "411", "name": "货品属性" }, { "id": "411-10", "name": "颜色" }, { "id": "411-10-10", "name": "颜色查询" }, { "id": "411-10-20", "name": "颜色编辑" }, { "id": "411-20", "name": "尺码" }, { "id": "411-20-10", "name": "尺码查询" }, { "id": "411-20-20", "name": "尺码编辑" }, { "id": "411-30", "name": "分类" }, { "id": "411-30-10", "name": "分类查询" }, { "id": "411-30-20", "name": "分类编辑" }, { "id": "411-40", "name": "品牌" }, { "id": "411-40-10", "name": "品牌查询" }, { "id": "411-40-20", "name": "品牌编辑" }, { "id": "411-50", "name": "材质" }, { "id": "411-50-10", "name": "材质查询" }, { "id": "411-50-20", "name": "材质编辑" }, { "id": "411-60", "name": "风格" }, { "id": "411-60-10", "name": "风格查询" }, { "id": "411-60-20", "name": "风格编辑" }, { "id": "411-80", "name": "货品单位" }, { "id": "411-80-10", "name": "货品单位查询" }, { "id": "411-80-20", "name": "货品单位编辑" }, { "id": "416", "name": "客服" }], "cols": [{ "colAlias": "purprice", "mode": "0" }, { "colAlias": "styleproviderid", "mode": "0" }], "id": 104945, "flag": 1 }
            await slh2.om.saveWithPrivs(json);
            await common.delay(500);
            await loginReq.sspdLoginByStaff();
        });

        it('获取当前登录用户可访问功能', async function () {
            const res = await slh2.config.getUserFuncs({
                productVersion: LOGINDATA.productVersion
            }).then(res => res.result.data.rows.find(obj => obj.id == '420-10'));
            //console.log(res);
            expect(res, '该用户依然可以访问多门店库存').to.be.undefined;
        });
        it.skip('查看多门店库存', async function () {
            const res = await slh2.inv.getStockNumByOverStore();
            console.log(res);
            expect(res.result.data.total, '查看多门店库存成功').to.equal(1);
        });
    });
    describe('允许查看多门店库存', function () {
        before(async () => {
            await loginReq.sspdLoginAndChangShop();
            const json = { "name": "营业员", "sysFlag": 0, "funcs": [{ "id": "400", "name": "日记" }, { "id": "400-50", "name": "日记" }, { "id": "400-50-10", "name": "查看销售" }, { "id": "400-50-20", "name": "查看客户" }, { "id": "400-50-30", "name": "查看货品" }, { "id": "400-50-40", "name": "查看利润" }, { "id": "400-80", "name": "店员业绩" }, { "id": "401", "name": "卖货" }, { "id": "401-10", "name": "卖货查询" }, { "id": "401-20", "name": "卖货新增" }, { "id": "401-30", "name": "卖货修改" }, { "id": "401-40", "name": "发货" }, { "id": "401-50", "name": "卖货作废" }, { "id": "401-70", "name": "修改单价" }, { "id": "401-90", "name": "抹零" }, { "id": "404", "name": "货品" }, { "id": "404-10", "name": "货品查询" }, { "id": "404-20", "name": "查看货品流水" }, { "id": "404-30", "name": "货品新增" }, { "id": "404-40", "name": "货品修改" }, { "id": "405", "name": "客户" }, { "id": "405-10", "name": "客户查询" }, { "id": "405-20", "name": "客户积分调整" }, { "id": "405-30", "name": "客户充值" }, { "id": "405-40", "name": "客户收款" }, { "id": "405-50", "name": "客户新增" }, { "id": "405-60", "name": "客户修改" }, { "id": "411", "name": "货品属性" }, { "id": "411-10", "name": "颜色" }, { "id": "411-10-10", "name": "颜色查询" }, { "id": "411-10-20", "name": "颜色编辑" }, { "id": "411-20", "name": "尺码" }, { "id": "411-20-10", "name": "尺码查询" }, { "id": "411-20-20", "name": "尺码编辑" }, { "id": "411-30", "name": "分类" }, { "id": "411-30-10", "name": "分类查询" }, { "id": "411-30-20", "name": "分类编辑" }, { "id": "411-40", "name": "品牌" }, { "id": "411-40-10", "name": "品牌查询" }, { "id": "411-40-20", "name": "品牌编辑" }, { "id": "411-50", "name": "材质" }, { "id": "411-50-10", "name": "材质查询" }, { "id": "411-50-20", "name": "材质编辑" }, { "id": "411-60", "name": "风格" }, { "id": "411-60-10", "name": "风格查询" }, { "id": "411-60-20", "name": "风格编辑" }, { "id": "411-80", "name": "货品单位" }, { "id": "411-80-10", "name": "货品单位查询" }, { "id": "411-80-20", "name": "货品单位编辑" }, { "id": "416", "name": "客服" }, { "id": "420-10", "name": "查看多门店库存" }], "cols": [{ "colAlias": "purprice", "mode": "0" }, { "colAlias": "styleproviderid", "mode": "0" }], "id": 104945, "flag": 1 }
            await slh2.om.saveWithPrivs(json);
            await common.delay(500);
            await loginReq.sspdLoginByStaff();
        });
        it('获取当前登录用户可访问功能', async function () {
            const res = await slh2.config.getUserFuncs({
                productVersion: LOGINDATA.productVersion
            }).then(res => res.result.data.rows.find(obj => obj.id == '420-10'));
            //console.log(res);
            expect(res, '该用户不可访问多门店库存').to.includes({ id: '420-10' });
        });
        it('查看多门店库存', async function () {
            const res = await slh2.inv.getStockNumByOverStore();
            expect(res.result.data.total, '查看多门店库存失败').to.equal(2);
            //console.log(res);
        });
    })

});
