"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('库存调整', function () {
    this.timeout(30000);
    let sfRes, qlRes, styleInfo, styleAft, goodjson, expInv = 0;
    let dresRes = {};
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
        goodjson = basicJson.addGoodJson({ name: `库存调整${common.getRandomStr(6)}` });
        dresRes = await slh2.dres.saveStyle({ jsonParam: goodjson });
        //新增货品
        styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
        //获取货品详情
        //console.log(styleInfo);

        await common.delay(1000);

        let billDetails = getBillDetailsData(styleInfo);
        console.log(billDetails.billDetails);
        billDetails.billDetails.forEach(element => {
            expInv += element.commitNum;
        });
        console.log(expInv);
        //保存库存调整单
        sfRes = await slh2.inv.saveInvAdjustBill({
            jsonParam: billDetails,
        });
        qlRes = await slh2.dres.getStyleInfoList({
            jsonParam: {
                tenantSpuIds: styleInfo.tenantSpuId
            },
        }).then((res) => res.result.data.rows[0]);
        styleAft = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
    })
    it('货品详情-库存数', async function () {
        let InvAft = getStyleInvExp(styleAft);
        //console.log(`InvAft=${JSON.stringify(InvAft.invNum)}`);
        expect(InvAft.invNum).to.equal(expInv);
    });
    it('货品列表-库存数', async function () {
        this.retries(3);
        await common.delay(500);
        qlRes = await slh2.dres.getStyleInfoList({
            jsonParam: {
                tenantSpuIds: styleInfo.tenantSpuId
            },
        }).then((res) => res.result.data.rows[0]);
        //console.log(`qlRes=${JSON.stringify(qlRes.invNum)}`);
        expect(qlRes.invNum).to.equal(expInv);
    });
    it('库存流水', async function () {
        let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
        //console.log(`invFlowList=${JSON.stringify(invFlowList.remainNum)}`);
        expect(invFlowList.remainNum).to.equal(expInv);
    });
});

function getBillDetailsData(obj) {
    let expData = {
        billDetails: [
            { colorId: common.getRandomNum(1, 5), sizeId: common.getRandomNum(1, 5), commitNum: common.getRandomNum(5, 10), tenantSpuId: obj.tenantSpuId },
            { colorId: common.getRandomNum(1, 5), sizeId: common.getRandomNum(1, 5), commitNum: common.getRandomNum(5, 10), tenantSpuId: obj.tenantSpuId }]
    };
    return expData;
};

function getStyleInvExp(obj) {
    let exp = {};
    exp.invNum = 0;
    for (let i = 0; i < obj.spuStockNum.length; i++) {
        exp.invNum += obj.spuStockNum[i].stockNum;
    };
    return exp;
}
