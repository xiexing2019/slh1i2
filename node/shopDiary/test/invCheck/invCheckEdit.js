"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('盘点', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	})
	it.skip('', async function () {
		//console.log(json);
	});
	describe('新增盘点计划', function () {
		let json1, json2, sfRes, qfRes, qlRes, cpRes, cpRes2, invSku1, invSku2, checkplan, styleInfo, styleInfoId;
		before(async () => {
			styleInfoId = await slh2.dres.getStyleInfoList({ searchToken: "auto001" }).then((res) => res.result.data.rows[0].tenantSpuId);
			styleInfo = await slh2.dres.getStyleInfo({ id: styleInfoId }).then((res) => res.result.data);
			json1 = basicJson.invCheckPlanJson();
			checkplan = await slh2.inv.saveCheckPlan({
				jsonParam: json1
			});
			//console.log(checkplan);
			//新增盘点计划
			json2 = basicJson.invCheckJson({ planId: checkplan.result.data.val, styleInfo });
			//console.log(json2);
			//获取起始库存
			invSku1 = await slh2.inv.getInvSkuByStyleId(json2.billDetails[0].tenantSpuId);
			//console.log(`invSku1.result.data.rows=${JSON.stringify(invSku1.result.data)}`);
			//新增盘点单
			sfRes = await slh2.inv.saveInvCheck({
				jsonParam: json2
			});
			//盘点单详情
			qfRes = await slh2.inv.getCheckFull({
				id: sfRes.result.data.val,
			});
			// console.log(`qfRes : ${JSON.stringify(qfRes.result.data)}`);
			//盘点计划列表
			qlRes = await slh2.inv.getCheckPlanList({ searchToken: sfRes.result.data.billNo });
			//盘点计划详情
			cpRes = await slh2.inv.getCheckPlanDetail({
				id: checkplan.result.data.val,
				queryFrom: 0,
				orderBy: "billNo desc"
			});
			//获取执行盘点计划后的库存
			//invSku2 = await slh2.inv.getInvSkuByStyleId(json2.billDetails[0].tenantSpuId);
			//console.log(`invSku2.result.data.rows=${JSON.stringify(invSku2.result.data)}`);

			//执行盘点计划
			await slh2.inv.execCheckPlan({
				jsonParam: { id: checkplan.result.data.val }
			});
			await common.delay(10000);
			//太慢 等个十秒
			//再次查询盘点计划明细
			/*cpRes2 = await slh2.inv.getCheckPlanDetail({
				id: checkplan.result.data.val,
				queryFrom: 0,
				orderBy: "billNo desc"
			});*/
		});
		it.skip('', async function () {
			//console.log(json);
		});
		it('1.盘点单详情', async function () {
			common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
		});
		it('2.盘点计划列表', async function () {
			const newCheckPlan = qlRes.result.data.rows.find(obj => obj.id == checkplan.result.data.val);
			expect(newCheckPlan).to.not.be.undefined;
		});
		it.skip('3.库存检查', async function () {
			json.billDetails.forEach(detail => {
				const invKey = `${detail.tenantSpuId}_${detail.colorId}_${detail.sizeId}`;
				//stockNum:当前库存   commitNum:调整后数量
				if (!invSku1.result.data[invKey])
					invSku1.result.data[invKey] = { invKey, movingNum: 0, stockNum: 0 };
				invSku1.result.data[invKey].stockNum = detail.commitNum;
			});
			common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
		});
		it.skip('盘点单查询条件', async function () {
			//searchToken  名称、款号、条码、备注
			//盘点状态  clearOutFlag 1是全盘
			//profitLose: "0,1",//盈亏状态 1有盈亏/0无盈亏
			//显示已撤销盘点单  不显示 为1 显示不传flag

		});
		it('盘点计划详情', async function () {
			const checkPlanDetail = cpRes.result.data.checkBillList.find(obj => obj.id == qfRes.result.data.id);
			expect(checkPlanDetail).to.not.be.undefined;
		});
		it('盘点计划处理-实时库存', async function () {
			this.retries(3);
			await common.delay(500);
			cpRes2 = await slh2.inv.getCheckPlanDetail({
				id: checkplan.result.data.val,
				queryFrom: 0,
				orderBy: "billNo desc"
			});
			//console.log(cpRes2.result.data.planInfo.diffNum);
			//console.log(qfRes.result.data.billDetails[0].commitNum);
			//console.log(qfRes.result.data.billDetails[0].beforeNum);
			expect(cpRes2.result.data.planInfo.diffNum).to.equal(qfRes.result.data.billDetails[0].commitNum - qfRes.result.data.billDetails[0].beforeNum);
		});
		it.skip('盘点计划处理-库存流水', async function () {
			this.retries(3);
			await common.delay(500);
			expect(cpRes2.result.data.planInfo.diffNum).to.equal(qfRes.result.data.billDetails[0].commitNum - qfRes.result.data.billDetails[0].beforeNum);
		});

		describe('撤销与作废', function () {
			let json3, json4, sfRes, qfRes, qlRes, cpRes, checkplan;
			before(async () => {
				json3 = basicJson.invCheckPlanJson();
				checkplan = await slh2.inv.saveCheckPlan({
					jsonParam: json3
				});
				//console.log(checkplan);
				//新增盘点计划
				json4 = basicJson.invCheckJson({ planId: checkplan.result.data.val });

				//新增盘点单
				sfRes = await slh2.inv.saveInvCheck({
					jsonParam: json4
				});
				//盘点单详情
				qfRes = await slh2.inv.getCheckFull({
					id: sfRes.result.data.val,
				});
				// console.log(`qfRes : ${JSON.stringify(qfRes.result.data)}`);
				//作废盘点单
				await slh2.inv.deleteCheckBill({
					jsonParam: { id: sfRes.result.data.val }
				});
				//盘点计划详情
				cpRes = await slh2.inv.getCheckPlanDetail({
					id: checkplan.result.data.val,
					queryFrom: 0,
					orderBy: "billNo desc"
				});
				//撤销盘点计划
				await slh2.inv.deleteCheckPlan({
					jsonParam: { id: checkplan.result.data.val }
				});
				//盘点计划列表
				qlRes = await slh2.inv.getCheckPlanList();
			});
			it('作废盘点单', async function () {
				const deletedBill = cpRes.result.data.checkBillList.find(obj => obj.id == qfRes.result.data.id);
				expect(deletedBill).to.be.undefined;
			});
			it('撤销盘点计划', async function () {
				const deletedPlan = qlRes.result.data.rows.find(obj => obj.id == checkplan.result.data.val);
				expect(deletedPlan).to.be.undefined;
			});
		});

		describe.skip('数据隔离', function () {
			before(async () => {
				await loginReq.sspdLoginBySeq();
				await loginReq.changeShopFast({ shopName: '门店二' });
			});
			after(async () => {
				await loginReq.changeShop();
			});
			it('其他门店', async function () {
				let qlOtherShop = await slh2.inv.getCheckPlanList().then(res => res.result.data.rows.find(data => data.id == sfRes.result.data.val));
				expect(qlOtherShop).to.be.undefined;
			});
		});
	});
});
