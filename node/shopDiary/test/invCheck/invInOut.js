"use strict";
const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('负库存开单', function () {
    this.timeout(30000);
    before(async function () {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('必须先入库再出库', function () {
        let sfRes, qfRes, invSku1, invSku2, invFlowList1, invFlowList2;
        before(async function () {
            await slh2.config.saveParamValue({
                data: [{ code: "invinout_checknum", domainKind: "business", ownerId: LOGINDATA.shopId, val: "1" }]
            });
        });
        describe('负库存开销售单', function () {
            let styleInfo, json;
            let dresRes = {};
            before(async function () {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
                //新增负/0库存货品
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                json = basicJson.salesJson({ styleInfo: styleInfo });
                //获取起始库存,客户起始积分和余额
                invSku1 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                invFlowList1 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                //新增销售单
                sfRes = await sspd.salesBill.saveSalesBill({
                    jsonParam: json,
                    check: false
                });
            });
            it('负库存货品-新增销售单', async function () {
                //console.log(sfRes);
                expect(sfRes.result.msgId, '负库存销售参数有误').to.equal("mall_operation_error_stock_not_enough");
            });
            it('货品库存', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
            });
            it('货品库存流水', async function () {
                this.retries(5);
                await common.delay(500);
                invFlowList2 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                common.isApproximatelyEqualAssert(invFlowList1, invFlowList2);
            });
        });
        describe.skip('负库存开销售退货单', function () {
            before(async function () {
                const json = basicJson.salesJson();
                //获取起始库存,客户起始积分和余额
                [invSku1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
                salesDetList = await sspd.salesBill.getSalesDetList();
                //新增销售单
                sfRes = await sspd.salesBill.saveSalesBill({
                    jsonParam: json
                });
                //销售单列表.开单后库存,客户积分和余额
                qfRes = await sspd.salesBill.getSalesBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(10000);
                [qlRes, invSku2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
            });
            it('货品库存', async function () {
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('货品库存流水', async function () {
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe.skip('负库存开进货单', function () {
            let sfRes, qfRes, qlRes, invSku1, invSku2;
            before(async () => {
                const json = basicJson.purJson();
                //获取起始库存,供应商起始积分和余额
                [invSku1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
                //新增进货单
                sfRes = await billReqHandler.savePurchaseBill({
                    jsonParam: json
                });
                //进货单列表.开单后库存,供应商积分和余额
                qfRes = await slh2.trade.getPurchaseBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(10000);
                [qlRes, invSku2] = await Promise.all([slh2.trade.getPurchaseBillList({ searchToken: sfRes.result.data.billNo }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
            });
            it('3.库存检查', async function () {
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('库存流水验证', async function () {
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe('负库存开采购退货单', function () {
            let styleInfo, json;
            let dresRes = {};
            before(async function () {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
                //新增负库存货品
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                json = basicJson.purJson({ styleInfo: styleInfo, details: [{ num: -10 }] });
                //console.log(json);
                //获取起始库存,供应商起始积分和余额
                invSku1 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                invFlowList1 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                //新增进货单
                sfRes = await billReqHandler.savePurchaseBill({
                    jsonParam: json,
                    check: false
                });
            });
            it('负库存货品-新增采购退货单', async function () {
                expect(sfRes.result.msgId, '负库存销售参数有误').to.equal("mall_operation_error_stock_not_enough");
            });
            it('货品库存', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
            });
            it('货品库存流水', async function () {
                this.retries(5);
                await common.delay(500);
                invFlowList2 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                common.isApproximatelyEqualAssert(invFlowList1, invFlowList2);
            });
        });
        describe('负库存开调出单', function () {
            let styleInfo, json;
            let dresRes = {};
            before(async () => {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
                //新增负库存货品
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                json = await basicJson.moveOutJson({ styleInfo: styleInfo });
                //获取起始库存,供应商起始积分和余额
                invSku1 = await slh2.inv.getInvSkuByStyleId(styleInfo.tenantSpuId);
                invFlowList1 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                //新增调出单
                sfRes = await slh2.inv.saveMoveOutBill({
                    jsonParam: json,
                    check: false
                });
                console.log(sfRes);
            });
            it('负库存货品-新增调出单', async function () {
                expect(sfRes.result.msgId, '负库存销售参数有误').to.equal("mall_operation_error_stock_not_enough");
            });
            it('货品库存', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(styleInfo.tenantSpuId);
                common.isApproximatelyEqualAssert(invSku1.result.data, invSku2.result.data);
            });
            it('货品库存流水', async function () {
                this.retries(5);
                await common.delay(500);
                invFlowList2 = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                common.isApproximatelyEqualAssert(invFlowList1, invFlowList2);
            });
        });
    });

    describe('允许负库存', function () {
        let sfRes, qfRes, invSku1, invSku2, invFlowList, billval;
        before(async function () {
            await slh2.config.saveParamValue({
                data: [{ code: "invinout_checknum", domainKind: "business", ownerId: LOGINDATA.shopId, val: "0" }]
            });
        });
        describe('负库存开销售单', function () {
            let styleInfo, json;
            let dresRes = {};
            before(async function () {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
                //新增负/0库存货品
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                json = basicJson.salesJson({ styleInfo: styleInfo });
                //获取起始库存,客户起始积分和余额
                invSku1 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                //新增销售单
                sfRes = await sspd.salesBill.saveSalesBill({
                    jsonParam: json
                });
                //console.log(sfRes);
                qfRes = await sspd.salesBill.getSalesBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
            });
            it('货品库存', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('货品库存流水', async function () {
                this.retries(5);
                await common.delay(500);
                invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe.skip('负库存开销售退货单', function () {
            before(async function () {
                const json = basicJson.salesJson();
                //获取起始库存,客户起始积分和余额
                [invSku1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
                salesDetList = await sspd.salesBill.getSalesDetList();
                //新增销售单
                sfRes = await sspd.salesBill.saveSalesBill({
                    jsonParam: json
                });
                //销售单列表.开单后库存,客户积分和余额
                qfRes = await sspd.salesBill.getSalesBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(10000);
                [qlRes, invSku2] = await Promise.all([sspd.salesBill.getSalesBillList(), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
            });
            it('货品库存', async function () {
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('货品库存流水', async function () {
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe.skip('负库存开进货单', function () {
            let sfRes, qfRes, qlRes, invSku1, invSku2;
            before(async () => {
                const json = basicJson.purJson();
                //获取起始库存,供应商起始积分和余额
                [invSku1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
                //新增进货单
                sfRes = await billReqHandler.savePurchaseBill({
                    jsonParam: json
                });
                //进货单列表.开单后库存,供应商积分和余额
                qfRes = await slh2.trade.getPurchaseBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(10000);
                [qlRes, invSku2] = await Promise.all([slh2.trade.getPurchaseBillList({ searchToken: sfRes.result.data.billNo }), slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
            });
            it('3.库存检查', async function () {
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('库存流水验证', async function () {
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe('负库存开采购退货单', function () {
            let sfRes, qfRes, invSku1, invSku2, json;
            before(async () => {
                json = basicJson.purJson();
                //获取起始库存,供应商起始积分和余额
                [invSku1] = await Promise.all([slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId)]);
                //新增进货单
                sfRes = await billReqHandler.savePurchaseBill({
                    jsonParam: json
                });
                //进货单列表.开单后库存,供应商积分和余额
                qfRes = await slh2.trade.getPurchaseBillFull({
                    isPend: 0,
                    id: sfRes.result.data.val
                });
                await common.delay(10000);
                //qlRes = await slh2.trade.getPurchaseBillList({ searchToken: sfRes.result.data.billNo });
            });
            it('3.库存检查', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(json.details[0].tenantSpuId);
                const exp = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('库存流水验证', async function () {
                this.retries(5);
                await common.delay(500);
                let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
                common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            });
        });
        describe('负库存开调出单', function () {
            let styleInfo, json;
            let dresRes = {};
            before(async () => {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({}) });
                //新增负库存货品
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                json = await basicJson.moveOutJson({ styleInfo: styleInfo });
                //获取起始库存,供应商起始积分和余额
                invSku1 = await slh2.inv.getInvSkuByStyleId(styleInfo.tenantSpuId);
                //新增调出单
                sfRes = await slh2.inv.saveMoveOutBill({
                    jsonParam: json
                });
                //console.log(sfRes);
                billval = sfRes.result.data.val;
                qfRes = await slh2.inv.getMoveBillFull({ id: billval });
                //console.log(qfRes);
            });
            it('负库存货品-新增调出单', async function () {
                expect(sfRes.result.msg, '调拨失败').to.equal("保存成功!");
                //console.log(invSku1);
                //console.log(`invSku2=${JSON.stringify(invSku2)}`);
            });
            it.skip('调出单详情', async function () {

            });
            it('货品库存', async function () {
                this.retries(5);
                await common.delay(500);
                invSku2 = await slh2.inv.getInvSkuByStyleId(styleInfo.tenantSpuId);
                const exp = getExpByBill.getMoveInvExpByStyleId(qfRes.result.data);
                common.isApproximatelyEqualAssert(common.addObject(invSku1.result.data || 0, exp), invSku2.result.data);
            });
            it('货品库存流水', async function () {
                this.retries(5);
                await common.delay(500);
                invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: styleInfo.tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
                expect(qfRes.result.data.totalNum, '库存有误').to.equal(-invFlowList.addNum);
            });
        });
    });
});