"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2/index');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe('多门店参数校验', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginBySeq();
    });
    after(async () => {
        await slh2.config.saveParamValue({
            ownerKind: 7,
            data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
        });
        await slh2.config.saveParamValue({
            ownerKind: 7,
            data: [{ code: "sales_verify_overshop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
        });
        await loginReq.changeShop();
    });
    describe('客户允许按门店区分', function () {
        let custInfo1, custInfo2, totalNum, sfRes, newCust;
        before(async () => {
            await slh2.config.saveParamValue({
                ownerKind: 7,
                data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
            });
            //val1区分0不区分
            //参数关闭状态
            custInfo1 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                },
            });
            //console.log(custInfo1);
            await loginReq.changeShopFast({ shopName: '门店二' });
            await common.delay(5000);
            custInfo2 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                },
            })
            //console.log(custInfo2);
            totalNum = custInfo1.result.data.total + custInfo2.result.data.total;
            //获取A门店B门店的客户列表json(提取人数，计算总人数)
            sfRes = await slh2.mdm.addCust({ jsonParam: basicJson.addCustJson() });
            newCust = await slh2.mdm.getCustInfo({ id: sfRes.result.data.val, });
            await loginReq.changeShop();
            await common.delay(500);
            //B门店新增张三
            //console.log(custInfo1);
            //console.log(totalNum);
        });
        it.skip('', async function () {
            //console.log(custInfo1);
            console.log(totalNum);
            console.log(`custDetail : ${JSON.stringify(newCust)}`);
        });
        it('A门店查询张三', async function () {
            let searchInfo = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: newCust.result.data.userCust.userName
                },
            })
            expect(searchInfo.result.data.total, 'A门店能查询到B门店新增的客户').to.equal(0);
        });
        it('A门店人数', async function () {
            let searchInfo = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                },
            })
            expect(searchInfo.result.data.total, 'A门店人数受到B门店影响').to.equal(custInfo1.result.data.total);
        });
        describe('参数开启', function () {
            let sfRechargeBill, qfRechargeBill, BalanceAndScore, json;
            before(async () => {
                await slh2.config.saveParamValue({
                    ownerKind: 7,
                    data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
                });
            });
            it('A门店查询张三/跨门店客户列表验证', async function () {
                let searchInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: newCust.result.data.userCust.userName
                    },
                })
                expect(searchInfo.result.data.total, 'A门店不能查询到B门店新增的客户').to.equal(1);
            });
            it('A门店人数', async function () {
                let searchInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                    },
                })
                expect(searchInfo.result.data.total, 'A门店人数没有被参数影响').to.equal(totalNum + 1);
            });
        });
    });
    describe('客户允许跨门店核销', function () {
        let custInfo1, custInfo2, newCust1, newCust2, totalBal, totalBal2, json;
        before(async () => {
            await slh2.config.saveParamValue({
                ownerKind: 7,
                data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
            });
            //客户共享
            await slh2.config.saveParamValue({
                ownerKind: 7,
                data: [{ code: "sales_verify_overshop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
            });
            //val1允许跨门店核销0默认不允许
            //参数关闭状态
            custInfo1 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                },
            });
            //console.log(custInfo1);
            await loginReq.changeShopFast({ shopName: '门店二' });
            await common.delay(5000);
            custInfo2 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                },
            });
            //console.log(custInfo2);
            totalBal = custInfo1.result.data.rows[0].balance + custInfo2.result.data.rows[0].balance;
            //同一客户A,B门店的余额（计算总额）
            json = {
                main: {
                    shopId: LOGINDATA.shopId,
                    ownerId: LOGINDATA.userId,
                    compId: custInfo2.result.data.rows[0].id,
                    rechargeMoney: 100,
                    proDate: common.getCurrentDate(),
                    rem: '充值备注',
                },
                payways: [{ typeId: 1, typeName: "现金", money: 100, payFlag: 1, tallyType: 1 }],
            };
            await sspd.finInVipRechargeBill.saveRechargeBill(json);
            //B门店给小王充值100
            newCust2 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                },
            });
            //B门店查询小王余额
            await loginReq.changeShop();
            await common.delay(500);
            //切到A门店
            newCust1 = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                },
            });
            //A门店查询小王余额
            totalBal2 = newCust1.result.data.rows[0].balance + newCust2.result.data.rows[0].balance;
            //AB汇总余额
        });
        it('A门店余额不和B合并', async function () {
            expect(newCust1.result.data.rows[0].balance, 'B门店余额等于AB总余额').to.not.equal(totalBal + 100);
        });
        it('A门店余额验证（不变)', async function () {
            expect(newCust1.result.data.rows[0].balance, 'B门店余额等于AB总余额').to.equal(custInfo1.result.data.rows[0].balance);
        });
        describe('参数开启', function () {
            before(async () => {
                await slh2.config.saveParamValue({
                    ownerKind: 7,
                    data: [{ code: "sales_verify_overshop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
                });
                newCust1 = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                    },
                });
                await loginReq.changeShopFast({ shopName: '门店二' });
                await common.delay(5000);
                newCust2 = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                    },
                });
            });
            it('A门店查询余额=A+B', async function () {
                expect(newCust1.result.data.rows[0].balance, '多门店余额没汇总').to.equal(totalBal2);
            });
            it('B门店查询余额=A+B', async function () {
                expect(newCust2.result.data.rows[0].balance, '多门店余额没汇总').to.equal(totalBal2);
            });
        });
    });
});