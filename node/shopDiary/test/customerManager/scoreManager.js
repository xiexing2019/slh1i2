"use strict";
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2/index');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('积分管理-online', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('积分验证', function () {
		let sfResCust, custInfo, qlParams;
		before(async () => {
			//设置积分规则
			await slh2.score.saveScoreRule({
				money: 50,
				score: 1
			});
			//设置等级规则
			let vipRankRule = await slh2.mdm.getRankList();
			vipRankRule.result.data.rows[1] = {
				scoreValueClause: 50, //积分条件
				custRankName: '白银会员', //会员名称
				custRank: 1, //会员级别，纳入统一字典体系，typeId=650.其中0-普通会员 1-白银会员 2-黄金会员 3-铂金会员 4-钻石会员
				discount: 0.92
			}
			let rs = await slh2.mdm.saveRankRule(vipRankRule.result.data.rows);
			//新增客户
			let json = basicJson.addCustJson({
				userName: '积分' + common.getRandomStr(6)
			});
			sfResCust = await slh2.mdm.addCust({
				jsonParam: json
			});
			custInfo = await slh2.mdm.getCustList({
				id: sfResCust.result.data.val,
			});
			qlParams = {
				shopId: custInfo.result.data.rows[0].shopId,
				traderId: custInfo.result.data.rows[0].id,
			};
		});
		it('新增的客户积分', async function () {
			expect(Number(custInfo.result.data.rows[0].score), '新增客户积分不为0').to.equal(0);
		});
		it('新增客户等级', async function () {
			expect(Number(custInfo.result.data.rows[0].rank), '新增客户等级不为0').to.equal(0);
		});
		it('新增客户积分明细', async function () {
			let custScoreDetail = await slh2.score.getCustScoreDetail(qlParams);
			expect(Number(custScoreDetail.result.data.count), '新增客户积分明细返回count不为0').to.equal(0);
		});
		describe('新增客户开单', function () {
			let sfRes, qfRes;
			before(async () => {
				//开单
				let json = basicJson.salesJson({
					dwid: custInfo.result.data.rows[0].id,
				});
				sfRes = await sspd.salesBill.saveSalesBill({
					jsonParam: json
				});
				console.log(`sfRes : ${JSON.stringify(sfRes)}`);
				qfRes = await sspd.salesBill.getSalesBillFull({
					isPend: 0,
					id: sfRes.result.data.val,
				});
				await common.delay(3000);//rank可能没有更新，加延迟试试
			});
			it('客户积分', async function () {
				this.retries(5);
				await common.delay(500);
				custInfo = await slh2.mdm.getCustList({ id: sfResCust.result.data.val, });
				let expScore = Math.ceil(common.div(sfRes.params.jsonParam.main.totalMoney, 50) * 1000) / 1000;
				//
				console.log(expScore);
				expect(Number(expScore)).to.equal(Number(custInfo.result.data.rows[0].score));
			});
			it('客户等级', async function () {
				this.retries(5);
				await common.delay(500);
				custInfo = await slh2.mdm.getCustList({ id: sfResCust.result.data.val, });
				expect(Number(custInfo.result.data.rows[0].rank)).to.not.equal(0);
			});
			it('积分明细', async function () {
				this.retries(5);
				await common.delay(500);
				let custScoreDetail = await slh2.score.getCustScoreDetail(qlParams);
				//console.log(`custScoreDetail : ${JSON.stringify(custScoreDetail)}`);
				let exp = getExpByBill.getCustScoreDetailExp(qfRes.result.data);
				exp.score = format.numberFormat(common.div(sfRes.params.jsonParam.main.totalMoney, 50), 3);
				common.isApproximatelyEqualAssert(exp, custScoreDetail.result.data.rows[0]);
			});
			it.skip('作废销售单', async function () {
				//作废销售单 积分明细产生一条积分为负的记录（现在逻辑有变要修改）
				await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
				//取积分明细第一条，顺便验排序
				let custScoreDetail = await slh2.score.getCustScoreDetail(qlParams);//.then(res => res.result.data.rows[0]);

				let exp = getExpByBill.getCustScoreDetailExp(qfRes.result.data);
				exp.score = -format.numberFormat(common.div(sfRes.params.jsonParam.main.totalMoney, 50), 3);
				//common.isApproximatelyEqualAssert(exp, custScoreDetail);
				console.log(custScoreDetail);
				console.log(exp);
			});
		})

		describe('积分调整', function () {
			let custScoreDetail;
			before(async () => {
				//积分调整
				await slh2.score.saveScoreAdjustBill({
					shopId: LOGINDATA.shopId,
					traderId: custInfo.result.data.rows[0].id,
					ownerId: LOGINDATA.userId,
					score: 100,
				});
				custScoreDetail = await slh2.score.getCustScoreDetail(qlParams);
			});
			it('积分调整后积分明细验证', async function () {
				//笑铺没有查看积分调整单明细的界面
				common.isApproximatelyEqualAssert({
					shopId: LOGINDATA.shopId,
					traderId: custInfo.result.data.rows[0].id,
					ownerId: LOGINDATA.userId,
					score: 100,
					bizType: 6200
				}, custScoreDetail.result.data.rows[0])
			});
		});
		describe('积分抵扣', function () {
			let dwBalance1, dwBalance2, speSpuList, json, sfRes;
			before(async () => {
				json = basicJson.salesJson({ details: [{ num: 1 }, { num: 1 }] });
				speSpuList = await slh2.dres.getSpeicalSpuList({ codeIn: 66666 }).then(res => res.result.data.rows[0]);
				//查询特殊货品列表
				await slh2.dres.saveSpeicalSpu({
					code: "66666",
					name: "积分抵扣",
					negative: 1,
					profitcount: 0,
					scoreCount: 1,
					id: speSpuList.id
				});
				await common.delay(500);
				//保存积分抵扣规则（抵扣金额不计算积分）
				json.specials = [{ matId: speSpuList.id, price: json.payways[0].money, money: -json.payways[0].money, num: -1 }];
				json.payways = [];
				json.fin.balance = 0;
				//console.log(json);
				sfRes = await sspd.salesBill.saveSalesBill({
					jsonParam: json
				});
				//开单使用积分抵扣
				dwBalance1 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: json.main.compId
				});
				//客户原始积分
				await slh2.dres.saveSpeicalSpu({
					code: "66666",
					name: "积分抵扣",
					negative: 1,
					profitcount: 0,
					scoreCount: 0,
					id: speSpuList.id
				});
				await common.delay(500);
				//保存积分抵扣规则（抵扣金额计算积分）
				await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfRes.result.data.val });
				//作废该销售单
				dwBalance2 = await slh2.acct.getBalanceAndScore({
					traderCap: 2,
					traderKind: 2,
					traderId: json.main.compId
				});
			});
			it('作废积分抵扣单据，验证返还的积分', async function () {
				common.isApproximatelyEqualAssert(dwBalance1.result.data, dwBalance2.result.data);
			});
		});
	});


});
