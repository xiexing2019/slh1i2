"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2/index');
const sspd = require('../../../reqHandler/shopDiary');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const loginReq = require('../../help/loginReq');

describe('客户余额充值和收款-online', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginBySeq();
        //console.log(`BASICDATA=${JSON.stringify(BASICDATA)}`);
    });

    describe('客户余额充值', function () {
        let sfRechargeBill, qfRechargeBill, BalanceAndScore, json, myAccountList;
        before(async () => {
            myAccountList = await slh2.fin.getAccountList();
            json = basicJson.rechargeBillJson({ payways: [{ accountName: '现', money: 100, }, { accountName: '银', money: 200, }, { accountName: '支', money: 300, }] });
            //console.log(`json=${JSON.stringify(json)}`);
            //myAccount1 = await slh2.fin.getAccountList();
            //console.log(`myAccount1=${JSON.stringify(myAccount1.result.data)}`);
            sfRechargeBill = await sspd.finInVipRechargeBill.saveRechargeBill(json);
            // console.log(`sfRechargeBill=${JSON.stringify(sfRechargeBill)}`);
            qfRechargeBill = await sspd.finInVipRechargeBill.getRechargeBillInfo({ id: sfRechargeBill.result.data.val });
            // console.log(`qfRechargeBill=${JSON.stringify(qfRechargeBill)}`);
            await common.delay(10000);
        });
        it('充值单详情', async function () {
            let exp = _.cloneDeep(sfRechargeBill.params.jsonParam);
            exp.main = Object.assign({ flag: 1, billNo: sfRechargeBill.result.data.billNo, id: sfRechargeBill.result.data.val, ownerId: sfRechargeBill.params.jsonParam.main.ownerId }, exp.main);
            exp.fin = {
                balanceType: 4,//预付
                relBillMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                giftsMoney: sfRechargeBill.params.jsonParam.main.giftsMoney,
                typeId: 7100,//充值单
                payMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                balance: common.add(sfRechargeBill.params.jsonParam.main.rechargeMoney, sfRechargeBill.params.jsonParam.main.giftsMoney),
            };
            //console.log(sfRechargeBill);
            //console.log(exp);
            //console.log(qfRechargeBill.result.data);
            common.isApproximatelyEqualAssert(exp, qfRechargeBill.result.data, ['payways']);
            common.isApproximatelyArrayAssert(exp.payways, qfRechargeBill.result.data.payways);
        });
        it('充值明细列表', async function () {
            let rechargeBillList = await sspd.finInVipRechargeBill.getRechargeBillList({ compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
            expect(rechargeBillList, `充值单列表没有查到刚刚新增的单据`).to.not.be.undefined;
            let exp = getExpByBill.getRechargeBillListExp(qfRechargeBill.result.data);
            common.isApproximatelyEqualAssert(exp, rechargeBillList);
        });
        it('客户对账单-单据余额', async function () {
            let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfRechargeBill.params.jsonParam.main.shopId, compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
            expect(custAcct, `客户对账单没有查到刚刚新增的单据`).to.not.be.undefined;
            let exp = getExpByBill.getCustSimpleAcctExp(qfRechargeBill.result.data);
            BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderId: json.main.compId
            })
            //console.log(`qfRechargeBill=${JSON.stringify(qfRechargeBill.result.data)}`);
            //console.log(`custAcct=${JSON.stringify(custAcct)}`);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //console.log(`balance=${JSON.stringify(BalanceAndScore.result.data.balance)}`);
            expect(custAcct.lastBalance, '客户单据余额有误').to.equal(BalanceAndScore.result.data.balance);
            common.isApproximatelyEqualAssert(exp, custAcct);
        });
        it('客户列表（数据同步）', async function () {
            let custInfo = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                    //debtFlag:1,隐藏未欠款客户
                    //code: custInfo.result.data.userCust.id,
                },
            })
            BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderId: json.main.compId
            })
            //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
            expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
        });
        it('我的账户', async function () {
            let jo1 = {},
                jo2 = {};
            const exp = getExpByBill.getAccountListExp(qfRechargeBill.result.data);
            //const myAccount1 = await slh2.fin.getAccountList();
            //sfRechargeBill = await sspd.finInVipRechargeBill.saveRechargeBill(json);
            const myAccount2 = await slh2.fin.getAccountList();
            //const final = myAccount2.result.data.rows.find(obj => obj.id == exp[0].id);
            //console.log(`myAccount=${JSON.stringify(qfRechargeBill.result.data)}`);
            myAccountList.result.data.rows.forEach(element => jo1[element.id] = element);
            myAccount2.result.data.rows.forEach(element => jo2[element.id] = element);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //console.log(`myAccount2=${JSON.stringify(myAccount2)}`);
            //console.log(jo1);
            //console.log(jo2);
            common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
        });
        it.skip('BalanceAndScore', async function () {
            BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderId: json.main.compId
            })
            console.log(`BalanceAndScore=${JSON.stringify(BalanceAndScore.result.data.balance)}`);
        });
        it('账户流水', async function () {
            //let accountFlowList = {};
            await common.delay(10000);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: myAccountList.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
            let exp = getExpByBill.getAccountFlowListExp(qfRechargeBill.result.data);
            //console.log(exp);
            //console.log(accountFlowList.result.data.rows[0]);
            //console.log(`money=${JSON.stringify(accountFlowList.result)}`);
            common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
        });
        describe('修改充值单', function () {
            //let myAccountList;
            before(async () => {
                //myAccountList = await slh2.fin.getAccountList();
                let json = basicJson.rechargeBillJson();
                json.main.id = sfRechargeBill.result.data.val;
                //修改充值单
                sfRechargeBill = await sspd.finInVipRechargeBill.saveRechargeBill(json);
                // console.log(`sfRechargeBill=${JSON.stringify(sfRechargeBill)}`);
                qfRechargeBill = await sspd.finInVipRechargeBill.getRechargeBillInfo({ id: sfRechargeBill.result.data.val });
                await common.delay(10000);
            });
            it('充值单详情', async function () {
                let exp = _.cloneDeep(sfRechargeBill.params.jsonParam);
                exp.main = Object.assign({ flag: 1, billNo: sfRechargeBill.result.data.billNo, id: sfRechargeBill.result.data.val }, exp.main);
                exp.fin = {
                    balanceType: 4,//预付
                    relBillMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                    giftsMoney: sfRechargeBill.params.jsonParam.main.giftsMoney,
                    typeId: 7100,//充值单
                    payMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                    balance: common.add(sfRechargeBill.params.jsonParam.main.rechargeMoney, sfRechargeBill.params.jsonParam.main.giftsMoney),
                };
                common.isApproximatelyEqualAssert(exp, qfRechargeBill.result.data, ['payways']);
                common.isApproximatelyArrayAssert(exp.payways, qfRechargeBill.result.data.payways);
            });
            it('充值单明细列表', async function () {
                let rechargeBillList = await sspd.finInVipRechargeBill.getRechargeBillList({ compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
                expect(rechargeBillList, `充值单列表没有查到刚刚新增的单据`).to.not.be.undefined;
                let exp = getExpByBill.getRechargeBillListExp(qfRechargeBill.result.data);
                common.isApproximatelyEqualAssert(exp, rechargeBillList);
            });
            it('客户对账单-单据余额', async function () {
                let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfRechargeBill.params.jsonParam.main.shopId, compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
                expect(custAcct, `客户对账单没有查到刚刚新增的单据`).to.not.be.undefined;
                let exp = getExpByBill.getCustSimpleAcctExp(qfRechargeBill.result.data);
                // console.log(`exp=${JSON.stringify(exp)}`);
                // console.log(`custAcct=${JSON.stringify(custAcct)}`);
                BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderId: json.main.compId
                })
                //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
                expect(custAcct.lastBalance, '客户单据余额有误').to.equal(BalanceAndScore.result.data.balance);
                common.isApproximatelyEqualAssert(exp, custAcct);
            });
            it('客户列表（数据同步）', async function () {
                let custInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                        //debtFlag:1,隐藏未欠款客户
                        //code: custInfo.result.data.userCust.id,
                    },
                })
                BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderId: json.main.compId
                })
                //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
                expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it.skip('我的账户', async function () {
                let jo1 = {},
                    jo2 = {};
                const exp = getExpByBill.getAccountListExp(qfRechargeBill.result.data);
                const myAccount2 = await slh2.fin.getAccountList();
                myAccountList.result.data.rows.forEach(element => jo1[element.id] = element);
                myAccount2.result.data.rows.forEach(element => jo2[element.id] = element);
                console.log(qfRechargeBill.result.data);
                console.log(exp);
                //console.log(jo1);
                //console.log(jo2);
                common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
            });
            it('账户流水', async function () {
                //await common.delay(10000);
                let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: myAccountList.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
                let exp = getExpByBill.getAccountFlowListExp(qfRechargeBill.result.data);
                //console.log(exp);
                //console.log(accountFlowList.result.data.rows[0]);
                common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
            });
        });
        describe('作废充值单', function () {
            let myAccountList;
            before(async () => {
                myAccountList = await slh2.fin.getAccountList();
                //作废充值单
                await sspd.finInVipRechargeBill.deleteRechargeBill({ id: sfRechargeBill.result.data.val });
                //获取充值单详情
                qfRechargeBill = await sspd.finInVipRechargeBill.getRechargeBillInfo({ id: sfRechargeBill.result.data.val });
                await common.delay(10000);
            });
            it('充值单详情', async function () {
                let exp = _.cloneDeep(sfRechargeBill.params.jsonParam);
                exp.main = Object.assign({ flag: -1, billNo: sfRechargeBill.result.data.billNo, id: sfRechargeBill.result.data.val }, exp.main);
                exp.fin = {
                    balanceType: 4,//预付
                    relBillMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                    giftsMoney: sfRechargeBill.params.jsonParam.main.giftsMoney,
                    typeId: 7100,//充值单
                    payMoney: sfRechargeBill.params.jsonParam.main.rechargeMoney,
                    balance: common.add(sfRechargeBill.params.jsonParam.main.rechargeMoney, sfRechargeBill.params.jsonParam.main.giftsMoney),
                };
                common.isApproximatelyEqualAssert(exp, qfRechargeBill.result.data, ['payways']);
                common.isApproximatelyArrayAssert(exp.payways, qfRechargeBill.result.data.payways);
            });
            it('充值单明细列表', async function () {
                let rechargeBillList = await sspd.finInVipRechargeBill.getRechargeBillList({ compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
                let exp = getExpByBill.getRechargeBillListExp(qfRechargeBill.result.data);
                common.isApproximatelyEqualAssert(exp, rechargeBillList);
            });
            it('客户对账单-单据信息', async function () {
                let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfRechargeBill.params.jsonParam.main.shopId, compId: sfRechargeBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfRechargeBill.result.data.val));
                expect(custAcct, `作废充值单后，客户对账单仍然可以查到作废的单据信息`).to.be.undefined;
            });
            it('客户列表（数据同步）', async function () {
                let custInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                        //debtFlag:1,隐藏未欠款客户
                        //code: custInfo.result.data.userCust.id,
                    },
                })
                BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderId: json.main.compId
                })
                //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
                expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it('我的账户', async function () {
                let jo1 = {},
                    jo2 = {};
                const exp = getExpByBill.getAccountListExp(qfRechargeBill.result.data);
                const myAccount2 = await slh2.fin.getAccountList();
                myAccountList.result.data.rows.forEach(element => jo1[element.id] = element);
                myAccount2.result.data.rows.forEach(element => jo2[element.id] = element);
                common.isApproximatelyEqualAssert(common.subObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
            });
            it('账户流水', async function () {
                //await common.delay(10000);
                let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: myAccountList.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
                let exp = getExpByBill.getAccountFlowListExp(qfRechargeBill.result.data);
                //console.log(exp);
                //console.log(accountFlowList.result.data.rows[0]);
                common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
            });
        });
    });
    describe('客户收款', function () {
        let sfReceiptBill, qfReceiptBill, json, BalanceAndScore;
        before(async () => {
            json = basicJson.receiptBillJson({ payways: [{ accountName: '现', money: 150, }, { accountName: '银', money: 220, }, { accountName: '支', money: 130, }] });
            sfReceiptBill = await sspd.salesBill.saveSalesBill({ jsonParam: json });
            qfReceiptBill = await sspd.salesBill.getSalesBillFull({
                isPend: 0,
                id: sfReceiptBill.result.data.val
            });
            //console.log(`sfReceiptBill=${JSON.stringify(sfReceiptBill)}`);
            // console.log(`qfReceiptBill=${JSON.stringify(qfReceiptBill)}`);
        });
        it('收款单详情', async function () {
            let exp = _.cloneDeep(sfReceiptBill.params.jsonParam);
            exp.main = Object({ flag: 1, id: sfReceiptBill.result.data.val, billNo: sfReceiptBill.result.data.billNo, subType: 1112, salesMoney: 0 }, exp.main);
            exp.fin = {
                relBillMoney
                    : sfReceiptBill.params.jsonParam.main.totalMoney,
                balanceType: 4,
                compKind: 2,
                payMoney: sfReceiptBill.params.jsonParam.main.payways,
                balance: sfReceiptBill.params.jsonParam.main.totalMoney,
                typeId: 1103,//收款
            };
            common.isApproximatelyEqualAssert(exp, qfReceiptBill.result.data, ['payways']);
            common.isApproximatelyArrayAssert(exp.payways, qfReceiptBill.result.data.payways);
        });
        it('收款单明细列表', async function () {
            let receiptBillList = await sspd.salesBill.getSalesBillListDTL({ compId: sfReceiptBill.params.jsonParam.main.compId, flag: 1, srcType: 8, }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
            expect(receiptBillList, `收款单列表没有查到刚刚新增的单据`).to.not.be.undefined;
            let exp = getExpByBill.getSalesBillListExp(qfReceiptBill.result.data);
            common.isApproximatelyEqualAssert(exp, receiptBillList);
        });
        it('客户对账单-单据余额', async function () {
            let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfReceiptBill.params.jsonParam.main.shopId, compId: sfReceiptBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
            expect(custAcct, `客户对账单没有查到刚刚新增的单据`).to.not.be.undefined;
            let exp = getExpByBill.getCustSimpleAcctExp(qfReceiptBill.result.data);
            common.isApproximatelyEqualAssert(exp, custAcct);
        });
        it('客户列表（数据同步）', async function () {
            let custInfo = await slh2.mdm.getCustList({
                jsonParam: {
                    flag: 1, //是否启用
                    orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                    orderByDesc: true, //降序
                    userName: "小王"
                    //debtFlag:1,隐藏未欠款客户
                    //code: custInfo.result.data.userCust.id,
                },
            })
            BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderId: json.main.compId
            })
            //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
            expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
        });
        it.skip('我的账户', async function () {
            let jo1 = {},
                jo2 = {};
            const exp = getExpByBill.getAccountListExp(qfRechargeBill.result.data);
            const myAccount2 = await slh2.fin.getAccountList();
            myAccountList.result.data.rows.forEach(element => jo1[element.id] = element);
            myAccount2.result.data.rows.forEach(element => jo2[element.id] = element);
            common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
        });

        describe('修改收款单', function () {
            before(async () => {
                let json = basicJson.receiptBillJson();
                json.main.id = sfReceiptBill.result.data.val;
                //修改收款单
                sfReceiptBill = await sspd.salesBill.saveSalesBill({ jsonParam: json });
                qfReceiptBill = await sspd.salesBill.getSalesBillFull({
                    isPend: 0,
                    id: sfReceiptBill.result.data.val
                });
            });
            it('收款单详情', async function () {
                let exp = _.cloneDeep(sfReceiptBill.params.jsonParam);
                exp.main = Object({ flag: 1, id: sfReceiptBill.result.data.val, billNo: sfReceiptBill.result.data.billNo, subType: 1112, salesMoney: 0 }, exp.main);
                exp.fin = {
                    relBillMoney: sfReceiptBill.params.jsonParam.main.totalMoney,
                    balanceType: 4,
                    compKind: 2,
                    payMoney: sfReceiptBill.params.jsonParam.main.payways,
                    balance: sfReceiptBill.params.jsonParam.main.totalMoney,
                    typeId: 1103,//收款
                };
                common.isApproximatelyEqualAssert(exp, qfReceiptBill.result.data, ['payways']);
                common.isApproximatelyArrayAssert(exp.payways, qfReceiptBill.result.data.payways);
            });
            it('收款单明细列表', async function () {
                let receiptBillList = await sspd.salesBill.getSalesBillListDTL({ compId: sfReceiptBill.params.jsonParam.main.compId, flag: 1, srcType: 8, }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
                expect(receiptBillList, `收款单列表没有查到刚刚新增的单据`).to.not.be.undefined;
                let exp = getExpByBill.getSalesBillListExp(qfReceiptBill.result.data);
                common.isApproximatelyEqualAssert(exp, receiptBillList);
            });
            it('客户对账单-单据余额', async function () {
                let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfReceiptBill.params.jsonParam.main.shopId, compId: sfReceiptBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
                expect(custAcct, `客户对账单没有查到刚刚新增的单据`).to.not.be.undefined;
                let exp = getExpByBill.getCustSimpleAcctExp(qfReceiptBill.result.data);
                common.isApproximatelyEqualAssert(exp, custAcct);
            });
            it('客户列表（数据同步）', async function () {
                let custInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                        //debtFlag:1,隐藏未欠款客户
                        //code: custInfo.result.data.userCust.id,
                    },
                })
                BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderId: json.main.compId
                })
                //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
                expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it.skip('我的账户', async function () {

            });

        });
        describe('作废收款单', function () {
            before(async () => {
                await sspd.salesBill.deleteSalesBill({ isPend: 0, id: sfReceiptBill.result.data.val });
                qfReceiptBill = await sspd.salesBill.getSalesBillFull({
                    isPend: 0,
                    id: sfReceiptBill.result.data.val
                });
            });
            it('收款单详情', async function () {
                let exp = _.cloneDeep(sfReceiptBill.params.jsonParam);
                exp.main = Object({ flag: -1, id: sfReceiptBill.result.data.val, billNo: sfReceiptBill.result.data.billNo, subType: 1112, salesMoney: 0 }, exp.main);
                exp.fin = {
                    relBillMoney: sfReceiptBill.params.jsonParam.main.totalMoney,
                    balanceType: 4,
                    compKind: 2,
                    payMoney: sfReceiptBill.params.jsonParam.main.payways,
                    balance: sfReceiptBill.params.jsonParam.main.totalMoney,
                    typeId: 1103,//收款
                };
                common.isApproximatelyEqualAssert(exp, qfReceiptBill.result.data, ['payways']);
                common.isApproximatelyArrayAssert(exp.payways, qfReceiptBill.result.data.payways);
            });
            it('收款单列表明细', async function () {
                let receiptBillList = await sspd.salesBill.getSalesBillListDTL({ compId: sfReceiptBill.params.jsonParam.main.compId, srcType: 8, }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
                expect(receiptBillList, `收款单列表没有查到刚刚新增的单据`).to.not.be.undefined;
            });
            it('客户对账单-单据信息', async function () {
                let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: sfReceiptBill.params.jsonParam.main.shopId, compId: sfReceiptBill.params.jsonParam.main.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfReceiptBill.result.data.val));
                expect(custAcct, `作废收款单后，客户对账单仍然可以查到作废的单据信息`).to.be.undefined;
            });
            it('客户列表（数据同步）', async function () {
                let custInfo = await slh2.mdm.getCustList({
                    jsonParam: {
                        flag: 1, //是否启用
                        orderBy: 'updatedDate', //排序方式默认按修改时间，balance
                        orderByDesc: true, //降序
                        userName: "小王"
                        //debtFlag:1,隐藏未欠款客户
                        //code: custInfo.result.data.userCust.id,
                    },
                })
                BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderCap: 2,
                    traderId: json.main.compId
                })
                //console.log(`custInfobalance=${JSON.stringify(custInfo.result.data.rows[0].balance)}`);
                expect(custInfo.result.data.rows[0].balance, '客户列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it.skip('我的账户', async function () {

            });
        });
    });

    //付款单不允许组合支付
    describe('付款单', function () {
        let sfPurOutBill, qfPurOutBill;
        before(async () => {
            sfPurOutBill = await slh2.trade.savePurOutBill(basicJson.purOutBillJson());
            //console.log(basicJson.purOutBillJson());
            qfPurOutBill = await slh2.trade.getPurOutBillInfo({ id: sfPurOutBill.result.data.val });
        });
        it('', async function () {

        });
        it('付款单详情', async function () {
            let exp = _.cloneDeep(sfPurOutBill.params.jsonParam);
            exp = Object.assign({
                flag: 1,
                billNo: sfPurOutBill.result.data.billNo,
                id: sfPurOutBill.result.data.val,
                createdBy: LOGINDATA.userId,
                unitId: LOGINDATA.unitId,
            }, exp);
            common.isApproximatelyEqualAssert(exp, qfPurOutBill.result.data);
        });
        it('付款单明细列表', async function () {
            let purOutBillList = await slh2.trade.getPurOutBillList({ compId: sfPurOutBill.params.jsonParam.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
            expect(purOutBillList).to.not.be.undefined;
            common.isApproximatelyEqualAssert(qfPurOutBill.result.data, purOutBillList);
        });
        it('供应商对账单-单据余额', async function () {
            let supplierAcct = await slh2.trade.getSuppSimpleAcctCheck({ shopId: sfPurOutBill.params.jsonParam.shopId, compId: sfPurOutBill.params.jsonParam.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
            expect(supplierAcct).to.not.be.undefined;
            let exp = getExpByBill.getSuppSimpleAcctExp(qfPurOutBill.result.data);
            common.isApproximatelyEqualAssert(exp, supplierAcct);
        });
        it('供应商列表（数据同步）', async function () {
            let supplierList = await slh2.mdm.getSupplierList({
                pageNo: 1,
                pageSize: 20,
                shopId: LOGINDATA.shopId,
                includeBalance: true,
                jsonParam: { searchToken: "Vell", flag: 1, orderBy: "updatedDate", orderByDesc: true }
            });
            let BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderKind: 1,
                traderCap: 2,
                traderId: supplierList.result.data.rows[0].compId
            })
            //console.log(LOGINDATA);
            //console.log(supplierList.result.data.rows[0]);
            expect(supplierList.result.data.rows[0].balance, '供应商列表余额有误').to.equal(BalanceAndScore.result.data.balance);
        });
        it.skip('我的账户', async function () {

        });

        describe('修改付款单', function () {
            before(async () => {
                let json = basicJson.purOutBillJson({ accountId: BASICDATA[LOGINDATA.shopName].accountList['银'].id, rem: '修改付款单' });
                json.id = sfPurOutBill.result.data.val;
                sfPurOutBill = await slh2.trade.savePurOutBill(json);
                qfPurOutBill = await slh2.trade.getPurOutBillInfo({ id: sfPurOutBill.result.data.val });
            });
            it('付款单详情', async function () {
                let exp = _.cloneDeep(sfPurOutBill.params.jsonParam);
                exp = Object.assign({
                    flag: 1,
                    billNo: sfPurOutBill.result.data.billNo,
                    id: sfPurOutBill.result.data.val,
                    createdBy: LOGINDATA.userId,
                    unitId: LOGINDATA.unitId,
                }, exp);
                common.isApproximatelyEqualAssert(exp, qfPurOutBill.result.data);
            });
            it('付款单列表', async function () {
                let purOutBillList = await slh2.trade.getPurOutBillList({ compId: sfPurOutBill.params.jsonParam.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
                expect(purOutBillList).to.not.be.undefined;
                common.isApproximatelyEqualAssert(qfPurOutBill.result.data, purOutBillList);
            });
            it('供应商对账单-单据余额', async function () {
                let supplierAcct = await slh2.trade.getSuppSimpleAcctCheck({ shopId: sfPurOutBill.params.jsonParam.shopId, compId: sfPurOutBill.params.jsonParam.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
                expect(supplierAcct).to.not.be.undefined;
                let exp = getExpByBill.getSuppSimpleAcctExp(qfPurOutBill.result.data);
                common.isApproximatelyEqualAssert(exp, supplierAcct);
            });
            it('供应商列表（数据同步）', async function () {
                let supplierList = await slh2.mdm.getSupplierList({
                    pageNo: 1,
                    pageSize: 20,
                    shopId: LOGINDATA.shopId,
                    includeBalance: true,
                    jsonParam: { searchToken: "Vell", flag: 1, orderBy: "updatedDate", orderByDesc: true }
                });
                let BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderKind: 1,
                    traderCap: 2,
                    traderId: supplierList.result.data.rows[0].compId
                })
                //console.log(LOGINDATA);
                //console.log(supplierList.result.data.rows[0]);
                expect(supplierList.result.data.rows[0].balance, '供应商列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it.skip('我的账户', async function () {

            });
        });
        describe('作废付款单', function () {
            before(async () => {
                await slh2.trade.deletePurOutBill({ id: sfPurOutBill.result.data.val });
                qfPurOutBill = await slh2.trade.getPurOutBillInfo({ id: sfPurOutBill.result.data.val });
            });
            it('付款单详情', async function () {
                let exp = _.cloneDeep(sfPurOutBill.params.jsonParam);
                exp = Object.assign({
                    flag: -1,
                    billNo: sfPurOutBill.result.data.billNo,
                    id: sfPurOutBill.result.data.val,
                    createdBy: LOGINDATA.userId,
                    unitId: LOGINDATA.unitId,
                }, exp);
                common.isApproximatelyEqualAssert(exp, qfPurOutBill.result.data);
            });
            it('付款单明细列表', async function () {
                let purOutBillList = await slh2.trade.getPurOutBillList({ compId: sfPurOutBill.params.jsonParam.compId, }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
                expect(purOutBillList).to.not.be.undefined;
                common.isApproximatelyEqualAssert(qfPurOutBill.result.data, purOutBillList);
            });
            it('供应商对账单-单据信息', async function () {
                let supplierAcct = await slh2.trade.getSuppSimpleAcctCheck({ shopId: sfPurOutBill.params.jsonParam.shopId, compId: sfPurOutBill.params.jsonParam.compId }).then(res => res.result.data.rows.find(obj => obj.id == sfPurOutBill.result.data.val));
                expect(supplierAcct, `作废付款单供应商对账单仍然查到作废的单据信息:${JSON.stringify(supplierAcct)}`).to.be.undefined;
            });
            it('供应商列表（数据同步）', async function () {
                let supplierList = await slh2.mdm.getSupplierList({
                    pageNo: 1,
                    pageSize: 20,
                    shopId: LOGINDATA.shopId,
                    includeBalance: true,
                    jsonParam: { searchToken: "Vell", flag: 1, orderBy: "updatedDate", orderByDesc: true }
                });
                let BalanceAndScore = await slh2.acct.getBalanceAndScore({
                    traderKind: 1,
                    traderCap: 2,
                    traderId: supplierList.result.data.rows[0].compId
                })
                //console.log(LOGINDATA);
                //console.log(supplierList.result.data.rows[0]);
                expect(supplierList.result.data.rows[0].balance, '供应商列表余额有误').to.equal(BalanceAndScore.result.data.balance);
            });
            it.skip('我的账户', async function () {

            });
        });
    });

});

