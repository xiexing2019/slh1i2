"use strict";
const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2/index');
const loginReq = require('../../help/loginReq');

describe('往来管理-供应商管理-online', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe('供应商管理', function () {
		it('相同名称验证', async function () {
			let sfRes = await slh2.mdm.addSupplier({
				jsonParam: basicJson.addSupplierJson({
					name: 'Vell',
				}),
				check: false
			});
			expect(sfRes.result, '新增相同名称供应商失败').to.includes({
				msg: `门店[${LOGINDATA.shopName}]已存在[Vell]名称的[供应商]`
			});
		});
		it('相同手机验证', async function () {
			let sfRes = await slh2.mdm.addSupplier({
				jsonParam: basicJson.addSupplierJson({
					telephone: '13801234567',
				}),
				check: false
			});
			expect(sfRes.result, '新增相同手机号失败').to.includes({
				msg: `门店[${LOGINDATA.shopName}]已存在[13801234567]号码的[供应商]`
			});
		});
		it('特殊字符验证', async function () {
			let params = {
				name: '$' + common.getRandomNumStr(6)
			}
			let json = basicJson.addSupplierJson(params);
			let supplierInfo = await slh2.mdm.addSupplier({
				jsonParam: json,
				check: false
			});
			expect(supplierInfo.result, '特殊字符能够保存成功').to.includes({
				msgId: 'error_exist_illegal_char'
			});

		});
		it('非老板角色修改有余额的供应商', async function () {
			let json = basicJson.addSupplierJson({
				balance: 100
			});
			let sfRes = await slh2.mdm.addSupplier({
				jsonParam: json
			});
			//非总经理登录
			caps.name.includes('sd_cg4') ? await loginReq.sspdLoginByCode({
				seq: '004'
			}) : await loginReq.sspdLoginBySeq({
				seq: '004'
			});
			json.org.name = '非总经理修改' + common.getRandomStr(6);
			json.org.id = sfRes.result.data.val;
			sfRes = await slh2.mdm.addSupplier({
				jsonParam: json,
				check: false
			});
			expect(sfRes.result, `非总经理修改有余额供应商名称修改成功${JSON.stringify(sfRes)}`).to.includes({
				msg: `该单位或他的下级单位在门店[${LOGINDATA.shopName}]中还存在余额或欠款`
			});
			await loginReq.sspdLoginBySeq();
		});
		it('新增完的供应商可以开单', async function () {
			let sfRes = await slh2.mdm.addSupplier({
				jsonParam: basicJson.addSupplierJson()
			});
			let purJson = basicJson.purJson({
				dwid: sfRes.result.data.val
			});
			let sfPurBill = await billReqHandler.savePurchaseBill({
				jsonParam: purJson
			});
		})
		describe('供应商增删改查', function () {
			let supplierInfo, sfRes,
				qlParams = {
					apiKey: 'ec-mdm-org-supp-list',
					pageNo: 1,
					pageSize: 0,
					jsonParam: {
						flag: 1, //是否启用
						orderBy: 'updatedDate', //排序方式默认按修改时间，balance
						orderByDesc: true //降序
					},
				};
			before(async () => {
				//新增供应商
				let json = basicJson.addSupplierJson();
				sfRes = await slh2.mdm.addSupplier({
					jsonParam: json
				});
				supplierInfo = await slh2.mdm.getSupplierInfo({
					id: sfRes.result.data.val
				});
			});
			it('供应商详情验证', async function () {

				common.isApproximatelyEqualAssert(sfRes.params.jsonParam, supplierInfo.result.data, [], '供应商详细信息有误');
			});
			it.skip('供应商对账单', async function () {

			});
			it('修改供应商信息', async function () {
				let json = {
					'2': {
						shopId: LOGINDATA.shopId,
						type: 0
					},
					org: {
						name: '修改供应商' + common.getRandomStr(6),
						id: sfRes.result.data.val,
						telephone: common.getRandomNumStr(11),
						remark: common.getRandomStr(8) + common.getRandomChineseStr(6),
						organType: '1', //必填 固定1 组织类型为公司
						capability: '2' //必填 固定2 能力位为供应商
					},
					address: {
						detailAddr: '浙江台州，浙江台州'
					},
				};
				let res = await slh2.mdm.addSupplier({
					jsonParam: json
				});
				supplierInfo = await slh2.mdm.getSupplierInfo({
					id: sfRes.result.data.val
				});
				common.isApproximatelyEqualAssert(json, supplierInfo.result.data, [], '修改供应商有误');
			});
			it('供应商列表查询', async function () {
				let searchToken = [supplierInfo.result.data.org.name, supplierInfo.result.data.org.remark, supplierInfo.result.data.org.telephone];
				for (let i = 0; i < searchToken.length; i++) {
					qlParams.jsonParam.searchToken = searchToken[i];
					let qlRes = await common.apiDo(qlParams);
					common.isApproximatelyEqualAssert(supplierInfo.result.data.org, qlRes.result.data.rows[0], [], '供应商查询列表有误');
				};
			});

			describe('停用启用供应商', function () {
				it('停用、启用供应商', async function () {
					let res = await common.apiDo({
						apiKey: 'ec-mdm-org-disable',
						id: supplierInfo.result.data.org.id
					});
					supplierInfo = await slh2.mdm.getSupplierInfo({
						id: supplierInfo.result.data.org.id
					});
					//console.log(`supplierInfo.result.data.org.flag : ${JSON.stringify(supplierInfo.result.data.org.flag)}`);
					expect(Number(supplierInfo.result.data.org.flag), '客户停用后flag没有变成0').to.equal(0);
					//停用后，是否停用查询条件验证
					qlParams.jsonParam.searchToken = supplierInfo.result.data.org.name;
					let qlRes = await common.apiDo(qlParams);
					expect(Number(qlRes.result.data.count), '是否停用-启用查询条件失效').to.equal(0);
					qlParams.jsonParam.flag = 0;
					qlRes = await common.apiDo(qlParams);
					expect(Number(qlRes.result.data.count), '是否停用-停用查询条件失效').to.equal(1);

				});
				it('启用供应商', async function () {
					let res = await common.apiDo({
						apiKey: 'ec-mdm-org-enable',
						id: supplierInfo.result.data.org.id
					});
					supplierInfo = await slh2.mdm.getSupplierInfo({
						id: supplierInfo.result.data.org.id
					});
					expect(Number(supplierInfo.result.data.org.flag), '客户启用后flag没有变成1').to.equal(1);
				})
			});
			describe('数据隔离', function () {
				before(async () => {
					//await loginReq.sspdLoginAndChangShop({ shopName: '门店二' });
					await loginReq.sspdLoginBySeq();
					await loginReq.changeShopFast({ shopName: '门店二' });
				});
				after(async () => {
					await loginReq.changeShop();
				});
				it('其它门店查询', async function () {
					let res = await slh2.mdm.getSupplierList({ nameLike: supplierInfo.result.data.org.name, shopId: LOGINDATA.shopId });
					expect(res.result.data.rows.length).to.equal(0);
				});
				it.skip('其它门店新增相同名称的供应商', async function () {
					await slh2.mdm.addSupplier(
						basicJson.addSupplierJson({ name: supplierInfo.result.data.org.name })
					);
				});
			});

		});

	});
	describe.skip('供应商管理查询', function () {
		it('隐藏未欠款供应商', async function () {

		});
		it('供应商排序', async function () {
			//默认，欠款最多，余额最多
		});
	});
});



