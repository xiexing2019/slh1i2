"use strict";
const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const loginReq = require('../../help/loginReq');

describe('往来管理-客户管理-online', function () {
	this.timeout(30000);

	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('客户管理', function () {
		describe('主流程', function () {
			let sfRes, custInfo,
				qlParams = {
					apiKey: 'ec-mdm-user-cust-list',
					jsonParam: {
						flag: 1, //是否启用
						orderBy: 'updatedDate', //排序方式默认按修改时间，balance
						orderByDesc: true, //降序
						//debtFlag:1,隐藏未欠款客户
						//code: custInfo.result.data.userCust.id,
					},
				};
			before(async () => {
				sfRes = await slh2.mdm.addCust({ jsonParam: basicJson.addCustJson() });
				custInfo = await slh2.mdm.getCustInfo({
					id: sfRes.result.data.val,
				});
			});
			it('客户详情验证', async function () {
				common.isApproximatelyEqualAssert(sfRes.params.jsonParam, custInfo.result.data, [], '客户详情信息有误');
			});
			it('客户列表查询', async function () {
				qlParams.jsonParam.rank = 0; //积分为0为普通会员
				let searchToken = [custInfo.result.data.userCust.userName, custInfo.result.data.userCust.rem, custInfo.result.data.userCust.mobile];
				for (let i = 0; i < searchToken.length; i++) {
					qlParams.jsonParam.searchToken = searchToken[i];
					let qlRes = await common.apiDo(qlParams);
					common.isApproximatelyEqualAssert(custInfo.result.data.userCust, qlRes.result.data.rows[0], [], '客户查询列表有误');
					common.isApproximatelyEqualAssert({
						score: 0,
						balance: 0,
						rank: 0 //积分为0为普通会员
					}, qlRes.result.data.rows[0], [], '新增客户时没填积分和余额，默认值不是0');
				} //delete qlParams.jsonParam.searchToken;
			});
			it('修改客户信息', async function () {
				let json = {
					address: {
						detailAddr: '浙江温州，浙江温州'
					},
					userCust: {
						userName: '修改客户' + common.getRandomStr(6),
						id: custInfo.result.data.userCust.id,
						mobile: common.getRandomNumStr(11),
						gender: 2, //1 女 2男,
						birthday: common.getDateString([0, 0, -1]),
						rem: common.getRandomStr(8) + common.getRandomChineseStr(6),
						capability: '2', //客户固定2
					},
				};
				//修改客户
				let res = await slh2.mdm.addCust({
					jsonParam: json,
				});
				custInfo = await slh2.mdm.getCustInfo({
					id: res.result.data.val,
				});
				common.isApproximatelyEqualAssert(json, custInfo.result.data, [], '修改客户有误');
			});
			it('停用/启用客户', async function () {
				await slh2.mdm.disableCust({
					id: sfRes.result.data.val
				});
				custInfo = await slh2.mdm.getCustInfo({
					id: custInfo.result.data.userCust.id,
				});
				expect(Number(custInfo.result.data.userCust.flag), '客户停用后flag没有变成0').to.equal(0);
				qlParams.jsonParam.searchToken = custInfo.result.data.userCust.userName;
				//是否停用查询条件验证
				let qlRes = await common.apiDo(qlParams);
				expect(Number(qlRes.result.data.count), '是否停用查询条件失效').to.equal(0);

				qlParams.jsonParam.flag = 0;
				qlRes = await common.apiDo(qlParams);
				expect(Number(qlRes.result.data.count), '是否停用查询条件失效').to.equal(1);

				await slh2.mdm.enableCust({
					id: sfRes.result.data.val
				})
				custInfo = await slh2.mdm.getCustInfo({
					id: custInfo.result.data.userCust.id,
				});
				expect(Number(custInfo.result.data.userCust.flag), '客户启用后flag没有变成1').to.equal(1);
			});
			it('新增完的客户可以开单', async function () {
				let sfCustRes = await slh2.mdm.addCust({
					jsonParam: basicJson.addCustJson(),
				});
				let salesJson = basicJson.salesJson({
					dwid: sfCustRes.result.data.val,
				});

				await sspd.salesBill.saveSalesBill({
					jsonParam: salesJson
				});
			});
			describe.skip('数据隔离', function () {
				before(async () => {
					await loginReq.changeShopFast({ shopName: '门店二' });
				});
				after(async () => {
					await loginReq.changeShop();
				});
				it('查看其它门店', async function () {
					let res = await slh2.mdm.getCustList({ userName: custInfo.result.data.userCust.userName, shopId: LOGINDATA.shopId });
					expect(res.result.data.rows.length).to.equal(0);
				});
				it('其他门店可以新增相同名称的客户', async function () {
					await slh2.mdm.addCust(
						basicJson.addCustJson({ mobile: custInfo.result.data.userCust.mobile, userName: custInfo.result.data.userCust.userName }),
					);
				});

			});
		});
		describe('异常流', function () {
			it.skip('相同名称验证', async function () {
				let sfCustRes = await slh2.mdm.addCust({
					jsonParam: basicJson.addCustJson({
						userName: '小王',
					}),
					check: false,
				});
				//console.log(sfCustRes.result.msg);
				expect(sfCustRes.result.msg, `新增相同名称客户成功${JSON.stringify(sfCustRes)}`).to.includes('[小王]此用户已存在');
			});
			it('相同手机号验证', async function () {
				let sfCustRes = await slh2.mdm.addCust({
					jsonParam: basicJson.addCustJson({
						mobile: '13812345678',
					}),
					check: false,
				});
				expect(sfCustRes.result, `新增相同手机号客户失败${JSON.stringify(sfCustRes)}`).to.includes({ msgId: '门店[常青店]已存在[13812345678]号码的[客户]' });

			});
			it('特殊字符验证', async function () {
				let params = {
					userName: '$' + common.getRandomNumStr(6),
				}
				let json = basicJson.addCustJson(params);
				let sfCustRes = await slh2.mdm.addCust({
					jsonParam: json,
					check: false,
				});
				expect(sfCustRes.result.msgId, '特殊字符能够保存成功').to.includes('error_exist_illegal_char');
				//console.log(`custInfo : ${JSON.stringify(custInfo)}`);
			});
		});
	});
	describe('新增有初始余额和积分的客户', function () {
		let json, sfCustRes, custInfo;
		before(async () => {
			json = basicJson.addCustJson({
				initScore: 100,
				balance: 1,
			});
			sfCustRes = await slh2.mdm.addCust({
				jsonParam: json,
			});
		});
		it('客户详情', async function () {
			this.retries(5);
			await common.delay(2000);
			custInfo = await slh2.mdm.getCustList({ "flag": 1, "orderBy": "updatedDate", "orderByDesc": "true" }).then(res => res.result.data.rows.find(obj => obj.id == sfCustRes.result.data.val));
			//.then(res => res.result.data.rows.find(obj => obj.id == sfCustRes.result.data.val))
			let exp = Object.assign({}, { id: sfCustRes.result.data.val }, format.dataFormat(sfCustRes.params.jsonParam.userCust, 'shopId;userName;score=initScore;balance;capability'));
			common.isApproximatelyEqualAssert(exp, custInfo, [], '客户详情信息有误');
		});
		it('积分明细', async function () {
			let custScoreDetail = await slh2.score.getCustScoreDetail({ shopId: LOGINDATA.shopId, traderId: sfCustRes.result.data.val });
			common.isApproximatelyEqualAssert({
				bizType: 5801,
				proDate: common.getCurrentDate(),
				traderKind: 2,
				// updatedDate: "2018-10-31 14:10:41",
				ownerId: LOGINDATA.userId,
				traderId: sfCustRes.result.data.val,
				relBillMoney: 0,
				score: json.userCust.initScore,
				rem: "期初积分",
				shopId: LOGINDATA.shopId,
				billNo: 0,
				updatedBy: LOGINDATA.userId,
				relBillNum: 0,
				balScore: json.userCust.initScore
			}, custScoreDetail.result.data.rows[0]);
		});
		it('客户对账单', async function () {
			let custAcct = await accCheck.getSimpleCustAcctCheck({ compId: sfCustRes.result.data.val });
			common.isApproximatelyEqualAssert({
				total: 0,
				initBalance: json.userCust.balance,
			}, custAcct.result.data);
		});
		it('不能停用', async function () {
			let disableRes = await slh2.mdm.disableCust({ id: sfCustRes.result.data.val, check: false });
			expect(disableRes.result, '停用有账款的客户成功').to.includes({ msg: "该单位或他的下级单位在门店[常青店]中还存在余额或欠款", msgId: "mdm_org_has_balance" });
		});
		it('非老板角色修改有余额的客户', async function () {
			//非总经理登录
			caps.name.includes('sd_cg4') ? await loginReq.sspdLoginByCode({
				seq: '004'
			}) : await loginReq.sspdLoginBySeq({
				seq: '004'
			});
			let updateJson = _.cloneDeep(json);
			updateJson.userCust.userName = '非总经理修改' + common.getRandomStr(6);
			updateJson.userCust.id = sfCustRes.result.data.val;
			let updateRes = await slh2.mdm.addCust({
				jsonParam: updateJson,
				check: false,
			});
			expect(updateRes.result.msg, `非总经理修改有余额客户名称修改成功${JSON.stringify(updateRes)}`).to.includes(`该单位或他的下级单位在门店[${LOGINDATA.shopName}]中还存在余额或欠款`);
			await loginReq.sspdLoginBySeq();
		});
		describe('修改客户', function () {
			before(async () => {
				await loginReq.sspdLoginAndChangShop();
				// console.log(`sfCustRes.result.data.val=${JSON.stringify(sfCustRes)}`);
				let updateJson = {
					address: {
						detailAddr: '浙江温州，浙江温州'
					},
					userCust: {
						userName: '修改客户' + common.getRandomStr(6),
						id: sfCustRes.result.data.val,
						mobile: common.getRandomNumStr(11),
						gender: 2, //1 女 2男,
						birthday: common.getDateString([0, 0, -1]),
						rem: common.getRandomStr(8) + common.getRandomChineseStr(6),
						capability: '2', //客户固定2
					},
				};
				sfCustRes = await slh2.mdm.addCust({
					jsonParam: updateJson,
				});
				custInfo = await slh2.mdm.getCustList({ "flag": 1, "orderBy": "updatedDate", "orderByDesc": "true" });

			});
			it('详情验证', async function () {
				//console.log(json.userCust.balance,custInfo.result.data.rows[0]);
				this.retries(10);
				await common.delay(1000);
				common.isApproximatelyEqualAssert({
					id: sfCustRes.result.data.val,
					shopId: LOGINDATA.shopId,
					score: json.userCust.initScore,
					balance: json.userCust.balance,
					capability: json.userCust.capability,
				}, custInfo.result.data.rows[0], [], '客户详情信息有误');
			});
			it('积分明细', async function () {
				let custScoreDetail = await slh2.score.getCustScoreDetail({ shopId: LOGINDATA.shopId, traderId: sfCustRes.result.data.val });
				common.isApproximatelyEqualAssert({
					bizType: 5801,
					proDate: common.getCurrentDate(),
					traderKind: 2,
					// updatedDate: "2018-10-31 14:10:41",
					ownerId: LOGINDATA.userId,
					traderId: sfCustRes.result.data.val,
					relBillMoney: 0,
					score: json.userCust.initScore,
					rem: "期初积分",
					shopId: LOGINDATA.shopId,
					billNo: 0,
					updatedBy: LOGINDATA.userId,
					relBillNum: 0,
					balScore: json.userCust.initScore
				}, custScoreDetail.result.data.rows[0]);
			});
			it('客户对账单', async function () {
				let custAcct = await accCheck.getSimpleCustAcctCheck({ compId: sfCustRes.result.data.val });
				common.isApproximatelyEqualAssert({
					total: 0,
					initBalance: json.userCust.balance,
				}, custAcct.result.data);
			});
			it('不能停用', async function () {
				let disableRes = await slh2.mdm.disableCust({ id: sfCustRes.result.data.val, check: false });
				expect(disableRes.result, '停用有账款的客户成功').to.includes({ msg: "该单位或他的下级单位在门店[常青店]中还存在余额或欠款", msgId: "mdm_org_has_balance" });
			});
		});
	});

	describe.skip('客户管理查询条件', function () {
		it.skip('隐藏未欠款客户条件查询', async function () {

		});
		it.skip('客户管理排序', async function () {
			//VIP等级  ,默认，欠款最多，余额最多
		});
	});
	describe.skip('客户画像', function () {
		//ec-bi-cust-impression-combData
	});
	describe('客户生日', function () {
		describe('主流程', function () {
			let sfRes, custInfo;
			before(async () => {
				sfRes = await slh2.mdm.addCust({ jsonParam: basicJson.addCustJson({ birthday: common.getCurrentDate() }) });
				custInfo = await slh2.mdm.getCustInfo({
					id: sfRes.result.data.val,
				});
			});
			it('生日提醒列表', async function () {
				const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
				common.isApproximatelyEqualAssert(birth, custInfo.result.data.userCust, [], '生日提醒列表信息有误');
			});
			it('停用客户-生日提醒', async function () {
				await slh2.mdm.disableCust({
					id: sfRes.result.data.val
				});
				custInfo = await slh2.mdm.getCustInfo({
					id: custInfo.result.data.userCust.id,
				});
				const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
				expect(birth, `停用客户后，生日提醒中依然可以查看到客户信息`).to.be.undefined;
			});
			it('启用客户-生日提醒', async function () {
				await slh2.mdm.enableCust({
					id: sfRes.result.data.val
				})
				custInfo = await slh2.mdm.getCustInfo({
					id: custInfo.result.data.userCust.id,
				});
				const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
				common.isApproximatelyEqualAssert(birth, custInfo.result.data.userCust, [], '生日提醒列表信息有误');
			});
			it('修改客户生日到昨天', async function () {
				let json = {
					address: {
						detailAddr: '浙江温州，浙江温州'
					},
					userCust: {
						userName: '修改生日' + common.getRandomStr(6),
						id: custInfo.result.data.userCust.id,
						mobile: common.getRandomNumStr(11),
						gender: 2, //1 女 2男,
						birthday: common.getDateString([0, 0, -1]),
						rem: common.getRandomStr(8) + common.getRandomChineseStr(6),
						capability: '2', //客户固定2
					},
				};
				//修改客户
				let res = await slh2.mdm.addCust({
					jsonParam: json,
				});
				custInfo = await slh2.mdm.getCustInfo({
					id: res.result.data.val,
				});
				const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
				expect(birth, `修改生日后，生日提醒中依然可以查看到客户信息`).to.be.undefined;
			});
			it('修改客户生日到30天之后', async function () {
				let json = {
					address: {
						detailAddr: '浙江温州，浙江温州'
					},
					userCust: {
						userName: '修改生日' + common.getRandomStr(6),
						id: custInfo.result.data.userCust.id,
						mobile: common.getRandomNumStr(11),
						gender: 2, //1 女 2男,
						birthday: common.getDateString([0, 1, 1]),
						rem: common.getRandomStr(8) + common.getRandomChineseStr(6),
						capability: '2', //客户固定2
					},
				};
				//修改客户
				let res = await slh2.mdm.addCust({
					jsonParam: json,
				});
				custInfo = await slh2.mdm.getCustInfo({
					id: res.result.data.val,
				});
				const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
				expect(birth, `修改生日后，生日提醒中依然可以查看到客户信息`).to.be.undefined;
			});
			describe('跨门店生日提醒', function () {
				let sfRes, custInfo;
				before(async () => {
					sfRes = await slh2.mdm.addCust({ jsonParam: basicJson.addCustJson({ birthday: common.getCurrentDate() }) });
					custInfo = await slh2.mdm.getCustInfo({
						id: sfRes.result.data.val,
					});
					await slh2.config.saveParamValue({
						ownerKind: 7,
						data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "0" }]
					});
					//val1区分要0不区分
					//客户共享状态
					await loginReq.changeShopFast({ shopName: '门店二' });
					await common.delay(5000);
					//console.log(LOGINDATA);
				});
				after(async () => {
					await slh2.config.saveParamValue({
						ownerKind: 7,
						data: [{ code: "client_search_diffby_shop", domainKind: "business", ownerId: LOGINDATA.unitId, val: "1" }]
					});
					await loginReq.changeShop();
				});
				it('生日提醒列表-跨门店', async function () {
					this.retries(5);
					await common.delay(2000);
					const birth = await slh2.mdm.listByBirth({ orderBy: "birthday", flag: 1, wrapper: true }).then(res => res.result.data.rows.find(obj => obj.id == sfRes.result.data.val));
					//console.log(birth);
					common.isApproximatelyEqualAssert(birth, custInfo.result.data.userCust, [], '生日提醒列表信息有误');
				});
			});
		});
	});
});
