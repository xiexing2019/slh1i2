'use strivt';
const common = require('../../../lib/common');
const loginReq = require('../../help/loginReq');

describe('客户', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});
	describe('批量保存客户', function () {
		let sfRes;
		// before(async() => {
		// 	await common.apiDo({
		// 		apiKey: 'ec-mdm-user-cust-saveInBatch',
		// 		jsonParam: {
		//
		// 		}
		// 	});
		// })

		it.skip('会员删除', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-user-cust-delete',
				// id: ,
				// must: , //true物理删除
			});
		});
		it('尺码偏好图表', async function () {
			await common.apiDo({
				apiKey: 'ec-bi-cust-impression-size-preference',
				wrapper: true,
				unitId: LOGINDATA.unitId,
				shopId: LOGINDATA.shopId,
				compId: BASICDATA.ids.dwidXw,
			});
		});
		it('类别偏好图表', async function () {
			await common.apiDo({
				apiKey: 'ec-bi-cust-impression-class-preference',
				wrapper: true,
				unitId: LOGINDATA.unitId,
				shopId: LOGINDATA.shopId,
				compId: BASICDATA.ids.dwidXw,
			});
		});
		it('客户画像综合数据', async function () {
			await common.apiDo({
				apiKey: 'ec-bi-cust-impression-combData',
				wrapper: true,
				unitId: LOGINDATA.unitId,
				shopId: LOGINDATA.shopId,
				compId: BASICDATA.ids.dwidXw,
				traderKind: 2,
				fields: 'custName,buyCount,docId,cityName,buySum,totalMoney,buySumAvg,firstDate,monthMoney,avgBillPrice,liningPreference,colorPreference,stylePreference,classPreference,rate',
			});
		});

		it('积分明细', async function () {
			await common.apiDo({
				apiKey: 'ec-score-scoreAcctFlow-listByFlowCust',
				wrapper: true,
				pageNo: 1,
				pageSize: 15,
				shopId: LOGINDATA.shopId,
				traderId: BASICDATA.ids.dwidXw,
				traderKind: 2, //交易方类型,固定为2
				jsonParam: {
					flag: 1,
					updatedTimeGte: common.getCurrentDate(),
					updatedTimeLte: common.getCurrentDate(),
					orderBy: "updatedDate desc,id desc"
				}
			});
		});
		it('客户充值明细', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-finInVipRechargeBill-listWithPayWays',
				wrapper: true,
				jsonParam: {
					billNoGte: 1,
					billNoLte: 200,
					proDateGte: common.getDateString([0, -1, 0]),
					proDateLte: common.getCurrentDate(),
					shopId: LOGINDATA.shopId,
					compId: BASICDATA.ids.dwidXw,
					ownerId: LOGINDATA.userId,
					//rem: ,
				},
			});
		});
		it.skip('修改客户头像', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-user-update',
				jsonParam: {
					id: BASICDATA.ids.dwidXw,
					//avatar: , //头像id
				},
			});
		});
	})

	describe('积分调整', function () {
		it('积分调整', async function () {
			await common.apiDo({
				apiKey: 'ec-score-scoreAdjustBill-save',
				jsonParam: {
					shopId: LOGINDATA.shopId,
					traderId: BASICDATA.ids.dwidXw,
					traderKind: 2,
					ownerId: LOGINDATA.ownerId,
					score: 100,
				},
			});
		});
		it('积分列表', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-user-cust-list',
				wrapper: true,
				pageNo: 1,
				pageSize: 15,
				jsonParam: {
					id: BASICDATA.ids.dwidXw,
					forceNewest: 1,
				},
			});
		});
		it('同步', async function () {
			await common.apiDo({
				apiKey: 'ec-syncdata',
				type: 'sc_user_cust',
				pageNo: 1,
				pageSize: 2000,
				lastSyncTime: common.getCurrentTime(),
			});
		})
	});

	describe('会员', function () {
		//已添加用例
		it.skip('会员级别规则保存', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-cust-rule-save',
				jsonParam: [{
					scoreValueClause: 0, //积分条件
					custRankName: '普通会员', //会员名称
					custRank: 0, //会员级别，纳入统一字典体系，typeId=650.其中0-普通会员 1-白银会员 2-黄金会员 3-铂金会员 4-钻石会员
					discount: 0.95, //折扣，不是必传
				}]
			});
		});
		//已添加用例
		it.skip('会员级别规则查询', async function () {
			//没有参数
			await common.apiDo({
				apiKey: 'ec-mdm-cust-rule-get',

			});
		});
		it('会员生日提醒列表', async function () {
			await common.apiDo({
				apiKey: 'ec-mdm-user-cust-listByBirth',
				jsonParam: {
					// code: ,
					// userName: ,
					orderBy: 'birthday', //排序，birthday-按生日排序，如果是其它，则按其它字段排序
					flag: 1, //有效会员
					wrapper: true,
				},
			});
		});

	});
});
