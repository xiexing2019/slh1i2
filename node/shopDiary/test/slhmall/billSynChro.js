const common = require('../../../lib/common');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const basicJson = require('../../help/json/basicJson');
const getExpByBill = require('../../help/getExpByBill');
const accCheck = require('../../../reqHandler/shopDiary/accCheck');
const loginReq = require('../../help/loginReq');
const sspd = require('../../../reqHandler/shopDiary');

describe.skip('微商城对接笑铺', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });
    describe('订单同步', function () {
        let sfRes, qfRes, qlRes;
        before(async () => {
            const json = basicJson.slhMallBillJson();
            //console.log(json);
            sfRes = await billReqHandler.saveMallBill(json);
            console.log(sfRes.params);
            await common.delay(1000);
            //销售单列表中查询刚刚新增的单据信息
            qlRes = await sspd.salesBill.getSalesBillList().then(res => res.result.data.rows.find(obj => obj.smallBillId == sfRes.params.billId));
            //销售单详情.开单后库存,客户积分和余额
            qfRes = await sspd.salesBill.getSalesBillFull({ isPend: 0, id: qlRes.id });
        });
        it('1.优惠券订单同步', async function () {
            console.log(qlRes);
            expect(sfRes.params.totalMoney, '订单金额异常').to.equal(qlRes.totalMoney);
            //微商城账户，流水
            //客户对账单保存起来
        });
        it.skip('2.未发货退款', async function () {
            json.main.favorMoney = 50;
            json.payways[0].money -= json.main.favorMoney;
            sfRes = await billReqHandler.RefundMallBill({ jsonParam: json, check: false });
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it.skip('3.收货仅退款', async function () {
            json.main.favorMoney = 50;
            json.payways[0].money -= json.main.favorMoney;
            sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: json, check: false });
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it.skip('4.收货退货退款', async function () {
            let params = {
                favorMoney: 51,
                rem: '修改抹零到上限之上'
            };
            json1 = basicJson.salesJson(params);
            json1.payways[0].money -= json1.main.favorMoney;
            json1.main.id = sfRes1.result.data.val; //修改时必传单据id
            json1.main.ver = sfRes1.result.data.ver; //修改时必传
            //修改销售单
            sfRes1 = await billReqHandler.ReturnMallBilll({
                jsonParam: json1,
                check: false
            });
            expect(sfRes1.result.code, '抹零有误').to.equal(-10);
        });
        it.skip('5.多明细买货又退货', async function () {
            json.main.favorMoney = 50;
            json.payways[0].money -= json.main.favorMoney;
            sfRes = await sspd.salesBill.saveSalesBill({ jsonParam: json, check: false });
            common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it.skip('6.订单提现', async function () {
            //qlRes.totalMoney   smallBillId
            const cashOutJson = basicJson.MallCashOutJson({ remit: qlRes.totalMoney - 100, handleFee: 100, billId: qlRes.smallBillId });
            const cashOut = await billReqHandler.CashOutMallBill(cashOutJson);
            console.log(cashOut);
            //客户对账单余额
            //微商城账户增加，流水=提现订单的实退
            //common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
        });
        it('客户对账单-单据余额', async function () {
            let custAcct = await accCheck.getSimpleCustAcctCheck({ shopId: qlRes.shopId, compId: qlRes.compId }).then(res => res.result.data.rows.find(obj => obj.id == qlRes.id));
            expect(custAcct, `客户对账单没有查到刚刚新增的单据`).to.not.be.undefined;
            BalanceAndScore = await slh2.acct.getBalanceAndScore({
                traderCap: 2,
                traderId: qlRes.compId
            })
            expect(custAcct.lastBalance, '客户单据余额有误').to.equal(BalanceAndScore.result.data.balance);
        });
        it.skip('我的账户', async function () {
            let jo1 = {},
                jo2 = {};
            const exp = getExpByBill.getAccountListExp(qfRechargeBill.result.data);
            //const myAccount1 = await slh2.fin.getAccountList();
            //sfRechargeBill = await sspd.finInVipRechargeBill.saveRechargeBill(json);
            const myAccount2 = await slh2.fin.getAccountList();
            //const final = myAccount2.result.data.rows.find(obj => obj.id == exp[0].id);
            //console.log(`myAccount=${JSON.stringify(qfRechargeBill.result.data)}`);
            myAccountList.result.data.rows.forEach(element => jo1[element.id] = element);
            myAccount2.result.data.rows.forEach(element => jo2[element.id] = element);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //console.log(`myAccount2=${JSON.stringify(myAccount2)}`);
            //console.log(jo1);
            //console.log(jo2);
            common.isApproximatelyEqualAssert(common.addObject(jo1, exp), jo2, ['toInMoney', 'toOutMoney']);
        });
        it('账户流水', async function () {
            this.retries(5);
            await common.delay(2000);
            let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: 1279041 });
            //let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
            //console.log(exp);
            console.log(accountFlowList.result.data.rows[0]);
            //console.log(`money=${JSON.stringify(accountFlowList.result)}`);
            //common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
        });
    });
});