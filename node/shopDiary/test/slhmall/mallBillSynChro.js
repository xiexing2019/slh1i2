"use strict";
const common = require('../../../lib/common');
const slhmallJson = require('../../help/json/slhmallJson');
const mallHandler = require('../../../reqHandler/shopDiary/mallHandler');
const mallAccount = require('../../../reqHandler/shopDiary/mallAccount');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe('商城订单同步到笑铺', function () {
    this.timeout(30000);
    let purRes, mallBill, ssBillFull, mallBillList, clientInfo;
    before(async function () {
        await mallHandler.userLoginWithWx();
        clientInfo = _.cloneDeep(LOGINDATA);
        //console.log(LOGINDATA);
        //登录买家小程序
        const dresList = await mallHandler.getDresSpuList({ tenantId: mallAccount.seller1.tenantId });
        //console.log(dresList);
        //商城新增订单
        const spuId = dresList.result.data.rows[0].id;
        const dresFull = await mallHandler.getFullForBuyer({ spuId: spuId, _tid: mallAccount.seller1.tenantId, _cid: LOGINDATA.clusterCode });
        await common.delay(500);
        //console.log(`dresFull=${JSON.stringify(dresFull)}`);
        await mallHandler.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = slhmallJson.purJson({ styleInfo: dresFull });

        purRes = await mallHandler.createPurBill(purJson);
        //console.log(`purRes=${JSON.stringify(purRes)}`);


        const payRes = await mallHandler.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await mallHandler.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

        ssBillFull = await mallHandler.findPurBillFull({ id: purRes.result.data.rows[0].billId });
        //console.log(`ssBillFull=${JSON.stringify(ssBillFull)}`);
        await loginReq.sspdLoginBySeq();
        //登录笑铺
        await common.delay(10000);
        mallBillList = await sspd.salesOrder.salesOrderBillList().then(res => res.result.data.rows.find(obj => obj.sMallBillNo == purRes.result.data.rows[0].billNo));
        //console.log(`mallBillList=${JSON.stringify(mallBillList)}`);
        mallBill = await sspd.salesOrder.getSalesOrderBillFull({ id: mallBillList.id });
        //console.log(`mallBill=${JSON.stringify(mallBill)}`);
    });
    it.skip('test', async function () {
        console.log(LOGINDATA);
    });
    it('商城订单-商城单号', async function () {
        expect(mallBill.result.data.main.sMallBillNo, '商城单号异常').to.equal(ssBillFull.result.data.bill.billNo);
    });
    it('未发货-订单状态', async function () {
        expect(mallBill.result.data.main, `订单状态异常`).to.includes({
            doneFlag: 0
        });
    });
    it('未发货-已发货意向金额', async function () {
        expect(mallBill.result.data.main, `已发货意向金额异常`).to.includes({
            deliveredWishMoney: 0
        });
    });
    it('未发货-已支付意向金额', async function () {
        expect(mallBill.result.data.main.paidWishMoney, '已支付意向金额异常').to.equal(ssBillFull.result.data.bill.money);
    });
    it.skip('商城订单-订单明细', async function () {
        expect(mallBill.result.data.details, `订单明细异常`).to.includes({

        });
    });
    it.skip('笑铺账户流水', async function () {
        let accountFlowList = await slh2.fin.getAccountFlowList({ acctId: accountList1.result.data.rows[0].id });//.then(res => res.result.data.rows.find(obj => obj.billId == sfRechargeBill.result.data.val));
        let exp = getExpByBill.getAccountFlowListExp(qfRes.result.data);
        common.isApproximatelyEqualAssert(exp, accountFlowList.result.data.rows[0]);
    });
    it.skip('笑铺库存流水', async function () {
        let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: sfRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == sfRes.result.data.val));
        //console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
        let exp = getExpByBill.getInvFlowListExp(qfRes.result.data);
        //console.log(`exp=${JSON.stringify(exp)}`);
        common.isApproximatelyEqualAssert(exp[sfRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
    });
    describe('发货后笑铺销售单详情', function () {
        let mallbill2;
        before(async () => {
            await mallHandler.smallLogin({ captcha: '000000', authcType: 2 });
            await mallHandler.changeUserShop();
            //切到对应门店
            //console.log(LOGINDATA);
            //商城卖家登录，保存登录信息
            const qlRes = await mallHandler.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
            //console.log(qlRes);
            const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            const qfRes = await mallHandler.salesFindBillFull({ id: salesListRes.bill.id });
            const details = qfRes.result.data.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });
            await mallHandler.deliverSalesBill({
                main: {
                    logisCompId: 90,
                    logisCompName: "顺丰速运",
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: clientInfo.tenantId,
                    // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            }
            );
            //商城卖家发货
            await common.delay(1000);
            await loginReq.sspdLoginBySeq();
            //笑铺商城订单再查询
        });
        it('已发货-订单状态', async function () {
            this.retries(5);
            await common.delay(500);
            mallbill2 = await sspd.salesOrder.getSalesOrderBillFull({ id: mallBillList.id });
            expect(mallbill2.result.data.main, `订单状态异常`).to.includes({
                doneFlag: 2
            });
        });
        it('已发货-已发货意向金额', async function () {
            this.retries(5);
            await common.delay(500);
            mallbill2 = await sspd.salesOrder.getSalesOrderBillFull({ id: mallBillList.id });
            expect(mallbill2.result.data.main.deliveredWishMoney, `已发货意向金额异常`).to.equal(ssBillFull.result.data.bill.money)
        });
    });
    describe.skip('发货后笑铺库存', function () {
        let salesDetAft;
        before(async () => {
            salesDetAft = await sspd.salesBill.getSalesDetList();
        });
        it('销售明细列表', async function () {
            let exp = getExpByBill.getSalesDetListExp({ qfRes: qfRes.result.data, invSku: invSku2.result.data });
            common.isApproximatelyEqualAssert(exp, salesDetAft.result.data.rows.slice(0, exp.length));
        });
        it('销售明细汇总验证', async function () {
            let exp = getSalesDetSumExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(common.addObject(exp, salesDetList.result.data.sum || 0), salesDetAft.result.data.sum);
        });
    });
});