"use strict";
const common = require('../../../lib/common');
const salesBill = require('../../../reqHandler/shopDiary/salesBill');
const loginReq = require('../../help/loginReq');

describe('客户分析', function () {
    this.timeout(30000);
    before(async () => {
        await loginReq.sspdLoginAndChangShop();
    });

    it('客户按储值金额排序', async function () {
        const rechargeList = await salesBill.getCustRechargeNum().then((res) => res.result.data.rows);
        console.log(rechargeList);
        for (let i = 0; i < rechargeList.length - 1; i++) {
            let flag = (rechargeList[i].recharge ? rechargeList[i].recharge : 0) >= (rechargeList[i + 1].recharge ? rechargeList[i + 1].recharge : 0);
            expect(flag, `客户分析中，客户按储值金额排序错误，当前：${JSON.stringify(rechargeList[i])}，下一个${JSON.stringify(rechargeList[i + 1])}`).to.be.true;
        };
    });
    it('客户按收款金额排序', async function () {
        const receiveList = await salesBill.getCustRechargeNum({ orderBy: 'receiveMoney' }).then((res) => res.result.data.rows);
        console.log(receiveList);
        for (let i = 0; i < receiveList.length - 1; i++) {
            let flag = (receiveList[i].receiveMoney ? receiveList[i].receiveMoney : 0) >= (receiveList[i + 1].receiveMoney ? receiveList[i + 1].receiveMoney : 0);
            expect(flag, `客户分析中，客户按收款金额排序错误，当前：${JSON.stringify(receiveList[i])}，下一个${JSON.stringify(receiveList[i + 1])}`).to.be.true;
        };
    });
});