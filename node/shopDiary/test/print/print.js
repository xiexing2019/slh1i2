const common = require('../../../lib/common');
const printTemplate = require('../../help/json/printTemplate');
const getExpByBill = require('../../help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('打印', function () {
    this.timeout(30000);

    before(async function () {
        await loginReq.sspdLoginAndChangShop();
    });

    describe('标签打印', function () {
        let saveRes;
        let template = {};
        before('保存标签打印模板', async function () {
            template = printTemplate.template1;
            template.name = common.getRandomStr(5);
            template.shopId = LOGINDATA.shopId;
            saveRes = await slh2.config.savePrintBarcodeConfig(template);
        });
        after(async () => {
            await slh2.config.deletePrintTemp({ id: saveRes.result.data.val });
            //删除该模板保证可以新增模板（最多五套）
        });
        it.skip('test', async function () {
            console.log(exp);
        });
        it('查询模板列表', async function () {
            const res = await slh2.config.getPrintBarcodeTempList({ shopId: LOGINDATA.shopId, flag: 1 }).then(res => res.result.data.rows.find(data => data.id == saveRes.result.data.val));
            expect(res.name).to.equal(saveRes.params.jsonParam.name);
        });
        it('查询打印配置', async function () {
            let exp = getExpByBill.getPrintConfigExp(saveRes.params.jsonParam);
            const res = await slh2.config.getPrintBarcodeConfig({ shopId: LOGINDATA.shopId, id: saveRes.result.data.val });
            common.isApproximatelyEqualAssert(exp, res.result.data, [], '打印配置与保存的不一致');
        });
        describe('停用启用打印模板', function () {
            describe('停用', function () {
                before(async () => {
                    await slh2.config.disablePrintTemp({ id: saveRes.result.data.val });
                });
                it('查询模板列表', async function () {
                    const res = await slh2.config.getPrintBarcodeTempList({ shopId: LOGINDATA.shopId, flag: 1 }).then(res => res.result.data.rows.find(data => data.id == saveRes.result.data.val));
                    expect(res, `停用模板后，列表查询flag=1还能查到`).to.be.undefined;
                });
                it('查询打印配置', async function () {
                    const res = await slh2.config.getPrintBarcodeConfig({ shopId: LOGINDATA.shopId, id: saveRes.result.data.val });
                    expect(res.result.data.flag, `停用模板后，配置详情查询flag不为0`).to.equal(0);
                });
            });
            describe('启用', function () {
                before(async () => {
                    await slh2.config.enablePrintTemp({ id: saveRes.result.data.val });
                });
                it('查询模板列表', async function () {
                    const res = await slh2.config.getPrintBarcodeTempList({ shopId: LOGINDATA.shopId, flag: 1 }).then(res => res.result.data.rows.find(data => data.id == saveRes.result.data.val));
                    expect(res, `启用模板后，列表查询flag=1查不到模板`).to.not.be.undefined;
                });
                it('查询打印配置', async function () {
                    const res = await slh2.config.getPrintBarcodeConfig({ shopId: LOGINDATA.shopId, id: saveRes.result.data.val });
                    expect(res.result.data.flag, `启用模板后，配置详情查询flag不为1`).to.equal(1);
                });
            });

        });
        describe('设置默认打印模板', function () {
            before('', async function () {
                await slh2.config.setDefaultPrintTemp({ id: saveRes.result.data.val });
            });
            it('查询模板列表', async function () {
                const res = await slh2.config.getPrintBarcodeTempList({ shopId: LOGINDATA.shopId, flag: 1 }).then(res => res.result.data.rows.find(data => data.id == saveRes.result.data.val));
                expect(res.defaultFlag, `设为默认模板后，列表查询defaultFlag不为1`).to.equal(1);
            });
            it('查询打印配置', async function () {
                const res = await slh2.config.getPrintBarcodeConfig({ shopId: LOGINDATA.shopId, id: saveRes.result.data.val });
                expect(res.result.data.defaultFlag, `设为默认模板后，打印配置defaultFlag不为1`).to.equal(1);
            });
        });
        describe('修改打印模板', function () {
            let updateRes;
            before('', async function () {
                template.name = common.getRandomStr(5);
                template.id = saveRes.result.data.val;
                template.width = common.getRandomNum(20, 30);
                template.high = common.getRandomNum(30, 40);
                template.leftSize = common.getRandomNum(10, 20);//左边距
                template.printCount = common.getRandomNum(1, 5);
                template.printCountByStockNum = 0;
                updateRes = await slh2.config.savePrintBarcodeConfig(template);
            });
            it('查询模板列表', async function () {
                const res = await slh2.config.getPrintBarcodeTempList({ shopId: LOGINDATA.shopId, flag: 1 }).then(res => res.result.data.rows.find(data => data.id == updateRes.result.data.val));
                expect(res.name).to.equal(updateRes.params.jsonParam.name);
            });
            it('查询打印配置', async function () {
                let exp = getExpByBill.getPrintConfigExp(updateRes.params.jsonParam);
                const res = await slh2.config.getPrintBarcodeConfig({ shopId: LOGINDATA.shopId, id: updateRes.result.data.val });
                common.isApproximatelyEqualAssert(exp, res.result.data, [], '打印配置与保存的不一致');
            });
        });
    });
});