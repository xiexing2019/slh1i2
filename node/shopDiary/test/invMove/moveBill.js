const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../../help/loginReq');

describe.skip('调拨', function () {
    this.timeout(30000);

    let json, moveOutBill, moveInBill, moveOutList, billval, areaDiaryStockNum1, areaDiaryStockNum2, moveNum, moveMoney;
    before(async function () {
        await loginReq.sspdLoginAndChangShop();
    });

    describe('调出', function () {
        let exp = { addNum: 0, addMoney: 0, listMoney: 0, listNum: 0 };
        before(async function () {
            //查询常青店首页库存信息
            areaDiaryStockNum1 = await sspd.area.areaDiary();
            //console.log(`moveOutBill=${JSON.stringify(moveOutBill)}`);
        });
        it('新增调出单', async function () {
            json = await basicJson.moveOutJson();
            /*moveOutBill = await slh2.inv.saveMoveOutBill(json);
            billval = moveOutBill.result.data.val;
            expect(moveOutBill.result, '新增调出单失败').to.includes({ msg: `保存成功!` });*/
            console.log(json);
        });
        it('调出单详情', async function () {
            moveBillFull = await slh2.inv.getMoveBillFull({ id: billval });
            moveBillFull.result.data.billDetails.forEach(obj => exp.addNum = common.add(exp.addNum, obj.commitNum));
            moveBillFull.result.data.billDetails.forEach(obj => exp.addMoney = common.add(exp.addMoney, common.mul(obj.commitNum, obj.price)));
            //console.log(moveBillFull);
            //console.log(moveBillFull.result.data.billDetails);
            //console.log(exp);
            //总件数，总金额
            common.isApproximatelyEqualAssert(exp.addNum, moveBillFull.result.data.totalNum);
            common.isApproximatelyEqualAssert(exp.addMoney, moveBillFull.result.data.totalMoney);
        });
        it('调出单列表-单据信息', async function () {
            moveOutList = await slh2.inv.getMoveBillList({ inOutType: 1 });
            //console.log(`moveOutBill=${JSON.stringify(moveOutList)}`);
            //断言最上面的单号，件数，金额
            common.isApproximatelyEqualAssert(exp.addNum, moveOutList.result.data.rows[0].totalNum);
            common.isApproximatelyEqualAssert(exp.addNum, moveOutList.result.data.rows[0].totalMoney);
            common.isApproximatelyEqualAssert(exp.addNum, moveBillFull.result.data.billNo);
        });
        it('调出单列表-底部汇总', async function () {
            moveOutList = await slh2.inv.getMoveBillList({ inOutType: 1 });
            //console.log(`moveOutBill=${JSON.stringify(moveOutList)}`);
            moveOutList.result.data.rows.forEach(obj => exp.listNum = common.add(exp.addNum, obj.totalNum));
            moveOutList.result.data.rows.forEach(obj => exp.listMoney = common.add(exp.addMoney, obj.totalMoney));
            //断言最上面的单号，件数，金额
            common.isApproximatelyEqualAssert(exp.listNum, moveOutList.result.data.sum.totalNum);
            common.isApproximatelyEqualAssert(exp.listMoney, moveOutList.result.data.sum.totalMoney);
        });
        it('area-diary验证', async function () {
            moveNum = moveOutList.result.data.rows[0].totalNum;
            moveMoney = moveOutList.result.data.rows[0].totalMoney;
            let exp = {};
            exp.addNum = -moveNum;
            exp.stockNum = -moveNum;
            exp.stockMoney = -moveMoney;
            areaDiaryStockNum2 = await sspd.area.areaDiary();
            common.isApproximatelyEqualAssert(common.addObject(areaDiaryStockNum1.result.data.invDiary, exp), areaDiaryStockNum2.result.data.invDiary);
        });
        it.skip('款号库存流水-调拨数', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: BASICDATA.styleInfo.agc001.tenantSpuId, bizDateStart: common.getDateString([0, -1, 0]) });
            console.log(invFlowList);
        });

    });

    describe.skip('调入', function () {
        before(async function () {
            await loginReq.changeShopFast({ name: '门店二' });
            //const moveBillList = await slh2.inv.getMoveBillList({ inOutType: 2 });
            //console.log(`moveBillList=${JSON.stringify(moveBillList)}`);
        });
        it('调入单列表', async function () {
            moveBillList = await slh2.inv.getMoveBillList({ inOutType: 2 });
        });
        it('单据详情', async function () {
            moveBillFull = await slh2.inv.getMoveBillFull({ id: billval });
            console.log(moveBillFull);
        });
        it('调拨入库', async function () {

        });
        it('area-diary验证', async function () {

        });
        it('作废调拨单', async function () {

        });
        it('撤销调拨单', async function () {

        });
        it('', async function () {

        });
    });

});