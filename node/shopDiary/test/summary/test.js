"use strict";
const common = require('../../../lib/common');
const loginReq = require('../../help/loginReq');


describe('统计', function () {
	this.timeout(30000);
	before(async () => {
		await loginReq.sspdLoginAndChangShop();
	});

	describe('', function () {
		it('笑铺首页', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-today-diary',
				shopId: LOGINDATA.shopId,
				now: common.getCurrentDate(),
			});
		});
		//日记
		it('智能引擎', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-trend-diary',
				timeKind: 1, //1-按日 2-按周 3-按月 4-按年
				now: common.getCurrentDate(),
				shopId: LOGINDATA.shopId,
			});
		});
		it('自定义统计', async function () {
			await common.apiDo({
				apiKey: 'ec-sspd-area-diary',
				startDate: common.getCurrentDate(),
				endDate: common.getCurrentDate(),
				shopId: LOGINDATA.shopId,
			});
		});
	});

})
