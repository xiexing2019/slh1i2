const common = require('../../lib/common');
const basicJson = require('../help/json/basicJson');
const mainReqHandler = require('../../reqHandler/shopDiary/mainReqHandler');
const billReqHandler = require('../../reqHandler/shopDiary/bill');
const readline = require('readline');
const loginReq = require('../help/loginReq');


const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

rl.setPrompt('请输入开单的数量与明细条目数(默认2),以逗号分隔:');
rl.prompt();

rl.on('line', async function (line) {
	const [count, detailsCount = 2] = line.trim().split(',');
	//退出进程
	if (count == 'close') rl.close();

	if (isNaN(count)) {
		process.stdout.write('未识别,请重新输入:');
		return;
	};

	//登录+获取常用数据
	await loginReq.sspdLoginBySeq();

	const apiKeys = ['ec-trade-purBill-saveBill', 'ec-sspd-saveBill'];
	for (let index = 0; index < apiKeys.length; index++) {
		const apiKey = apiKeys[index];
		let params = {
			count: detailsCount
		};
		if (apiKey == 'ec-sspd-saveBill') {
			params.dwid = BASICDATA.ids.dwidXw;
			params.billType = 'sales';
		};

		const arr = new Array(Number(count)).fill({});
		const promises = arr.map(async (obj, index) => {
			let json = basicJson.purJson(params);
			json = billReqHandler.jsonParamFormat(json);
			const sfRes = await common.apiDo({
				apiKey: apiKey,
				jsonParam: json,
				check: false
			});

			return {
				no: index,
				duration: sfRes.duration,
				code: sfRes.result.code,
				msg: sfRes.result.msg
			};
		});
		const res = await Promise.all(promises);

		let [totalTime, minTime, maxTime, passes] = [0, 5000, 0, 0];
		for (let i = 0; i < res.length; i++) {
			if (res[i].code < 0) {
				continue;
			};
			const time = res[i].duration;
			maxTime = time > maxTime ? time : maxTime;
			minTime = time < minTime ? time : minTime;
			totalTime += time || 0;
			passes++;
		};
		if (res.length == 0) minTime = maxTime;
		console.log(`\napiKey=${apiKey}`);
		console.log(`最大开单时间为${maxTime}ms  最小开单时间为${minTime}ms\n平均开单时间为${common.div(totalTime, res.length).toFixed(0)}ms\n开单总数=${count}, 成功=${passes}, 失败=${count - passes}`);
		console.log(`result=${JSON.stringify(res)}`);
	};

	rl.prompt();
});

rl.on('close', function () {
	console.log('结束!');
	process.exit(0);
});