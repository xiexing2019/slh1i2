"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');
const loginReq = require('../../help/loginReq');

describe('加款号', function () {
    this.timeout(30000);
    let idsCollection = [];
    before(async function () {
        await loginReq.sspdLoginBySeq();
    });
    after(async () => {
        //console.log(idsCollection.toString());
    });
    for (let n = 0; n < 1500; n++) {
        describe('大量新增款号', function () {
            let styleInfo;
            let dresRes = {};
            before(async function () {
                dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({ spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }, { "colorId": 2, "sizeId": 5, "stockNum": 5 }, { "colorId": 3, "sizeId": 4, "stockNum": 5 }] }) });
                //新增有库存货品
                await common.delay(500);
                styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
                //获取货品详情
                idsCollection.push(dresRes.result.data.tenantSpuId);
            });
            it('创建款号', async function () {
                console.log(n);
            });
        });
    };
    //console.log(idsCollection);
});