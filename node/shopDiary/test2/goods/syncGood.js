"use strict";
const common = require('../../../lib/common');
const basicJson = require('../../help/json/basicJson');
const slh2 = require('../../../reqHandler/slh2');

describe('笑铺货品同步到商城', function () {
    this.timeout(30000);
    let styleInfo, mallSpu, spuList, classIds, brandIds, fabric, form;
    let dresRes = {};
    before(async function () {
        await mainReqHandler.sspdLoginBySeq();
        classIds = await slh2.dres.saveClass().then(res => res.result.data.val);
        brandIds = await slh2.config.saveDict({ jsonParam: basicJson.dictJson({ typeId: 606 }) }).then(res => res.result.data.codeValue);
        fabric = await slh2.config.saveDict({ jsonParam: basicJson.dictJson({ typeId: 637 }) }).then(res => res.result.data.codeValue);
        form = await slh2.config.saveDict({ jsonParam: basicJson.dictJson({ typeId: 835 }) }).then(res => res.result.data.codeValue);
        dresRes = await slh2.dres.saveStyle({ jsonParam: basicJson.addGoodJson({ classId: classIds, brandId: brandIds, styleId: form, liningId: fabric, spuStockNum: [{ "colorId": 1, "sizeId": 1, "stockNum": 5 }, { "colorId": 1, "sizeId": 2, "stockNum": 5 }, { "colorId": 2, "sizeId": 5, "stockNum": 5 }, { "colorId": 3, "sizeId": 4, "stockNum": 5 }] }) });
        //新增有库存货品
        await common.delay(1000);
        styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
        //获取货品详情
        //await depReqHandler.mallSpuSync({ ids: dresRes.result.data.tenantSpuId });
        //笑铺货品同步
        await common.delay(3000);
        spuList = await common.get(`${LOGINDATA.extProps.slh_mall_host}/spb/api.do?_cid=${LOGINDATA.extProps.sMallSession.clusterCode}&_tid=${LOGINDATA.extProps.sMallSession.tenantId}`,
            {
                apiKey: 'ec-spdresb-dresSpu-findSellerSpuList',
                jsonParam: { flag: 1, nameLike: styleInfo.spuCommonDto.name },
                sessionId: LOGINDATA.extProps.sMallSession.sessionId,
            });
        //console.log(spuList);
        mallSpu = await common.get(`${LOGINDATA.extProps.slh_mall_host}/spb/api.do?_cid=${LOGINDATA.extProps.sMallSession.clusterCode}&_tid=${LOGINDATA.extProps.sMallSession.tenantId}`,
            {
                apiKey: 'ec-spdresb-dresSpu-getFullById',
                jsonParam: { id: spuList.result.data.rows[0].id },
                sessionId: LOGINDATA.extProps.sMallSession.sessionId
            });
        //console.log(mallSpu);
    });
    it.skip('新增字典', async function () {
        console.log(classIds);
        console.log(brandIds);
        console.log(fabric);
        console.log(form);
    });
    it('货品款号同步', async function () {
        //console.log(styleInfo);
        //console.log(mallSpu);
        expect(styleInfo.spuCommonDto.code, '款号同步失败').to.equal(mallSpu.result.data.spu.code);
    });
    it('货品图片同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.docId, '图片同步失败').to.equal(mallSpu.result.data.spu.ecCaption.docContent[0].docUrl);
    });
    it('货品品牌同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.brandId, '品牌同步失败').to.equal(mallSpu.result.data.spu.ecCaption.brandId);
    });
    it('货品颜色同步', async function () {
        expect(styleInfo.spuCommonDto.ecCaption.colorIds, '颜色同步失败').to.equal(mallSpu.result.data.spu.spec2Names);
    });
    it('货品尺码同步', async function () {
        expect(styleInfo.spuCommonDto.ecCaption.sizeIds, '尺码同步失败').to.equal(mallSpu.result.data.spu.spec1Names);
    });
    it('货品价格同步', async function () {
        expect(styleInfo.spuCommonDto.stdprice1, '价格同步失败').to.equal(mallSpu.result.data.spu.pubPrice);
    });
    it('货品名称同步', async function () {
        expect(styleInfo.spuCommonDto.name, '名称同步失败').to.equal(mallSpu.result.data.spu.name);
    });
    it('货品类别同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.classId, '类别同步失败').to.equal(mallSpu.result.data.spu.ecCaption.classId);
    });
    it('货品风格同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.styleId, '风格同步失败').to.equal(mallSpu.result.data.spu.ecCaption.theme);
    });
    it('货品季节同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.season, '季节同步失败').to.equal(mallSpu.result.data.spu.ecCaption.season);
    });
    it('货品面料同步', async function () {
        expect(styleInfo.spuExtraDto.ecCaption.liningId, '面料同步失败').to.equal(mallSpu.result.data.spu.ecCaption.fabric);
    });
    it('货品库存同步', async function () {
        this.retries(5);
        await common.delay(1000);
        const spuStock = await common.get(`${LOGINDATA.extProps.slh_mall_host}/spb/api.do?_cid=${LOGINDATA.extProps.sMallSession.clusterCode}&_tid=${LOGINDATA.extProps.sMallSession.tenantId}`,
            {
                apiKey: 'ec-spdresb-dresSpu-getFullById',
                jsonParam: { id: spuList.result.data.rows[0].id },
                sessionId: LOGINDATA.extProps.sMallSession.sessionId
            });
        expect(spuStock.result.data.spu.stockNum, '库存同步失败/过慢').to.equal(20);
        //需要重试
    });
    it.skip('货品厂商同步', async function () {
        expect(mallSpu.result.data.spu.ssDresManuFacturer, '厂商同步失败').to.equal('Vell');
        //需要重试
    });
});