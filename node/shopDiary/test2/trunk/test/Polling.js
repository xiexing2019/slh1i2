const common = require('../../../../lib/common');
const slh2 = require('../../../../reqHandler/slh2');
const loginReq = require('../../../help/loginReq');

/**
 * 线上30分钟定时执行
 * 持续集成
 **/
describe('笑铺轮询', async function () {
    this.timeout(TESTCASE.timeout);
    before(async function () {
        await loginReq.sspdLoginAndChangShop();
    });
    it('品牌字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_brand',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('条码表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'dres_style_detail',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('客户等级字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_rank',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('季节字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_season',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('计量单位字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_unit',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('尺码组字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_size_group',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('颜色组字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_color_group',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('尺码字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_size',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('颜色字典表-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_dict_color',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('货品类别-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'dres_style_class',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('货品-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'dres_style',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);

    });
    it('厂商-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_org_supp',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);

    });
    it('个人客户-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'sc_user_cust',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);

    });
    it('员工-数据同步', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-syncdata',
            type: 'hr_staff',
            pageNo: 1,
            pageSize: 2000,

        });
        // console.log(`res=${JSON.stringify(res)}`);

    });
    it('获取当前登录用户可访问功能', async function () {
        const res = await slh2.config.getUserFuncs({
            productVersion: LOGINDATA.productVersion
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('获取门店详情', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-mdm-org-getFull',
            jsonParam: {
                id: LOGINDATA.shopId,
            },
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('获取供应商详情', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-mdm-org-getFull',
            jsonParam: {
                id: BASICDATA.ids.dwidVell,
            },
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('门店列表', async function () {
        const res = await slh2.mdm.getShopList({ wrapper: true, flag: 1, ignore: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('智能引擎', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-sspd-trend-diary',
            timeKind: 1, //1-按日 2-按周 3-按月 4-按年
            now: common.getCurrentDate(),
            shopId: LOGINDATA.shopId,
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('会员级别规则查询', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-mdm-cust-rule-get',

        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('自定义统计', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-sspd-area-diary',
            startDate: common.getCurrentDate(),
            endDate: common.getCurrentDate(),
            shopId: LOGINDATA.shopId,
        });
    });
    it('字典列表', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-config-list-dict',
            jsonParam: { typeId: 402, unitId: LOGINDATA.unitId }
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('获取个人快捷方式收藏列表', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-config-func-shortcut-findByOwnerShortcuts',
            productType: 128
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('注册绑定，同时更新当前用户和设备的embGuid', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-mdm-user-registerMessageCid',
            jsonParam: { cid: "80d27e001d4675106b636f8180ae4d76" }
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('关于公司', async function () {
        const res = await slh2.config.getParamValues({ codes: 'sales_verify_overshop,sales_client_score_share,client_search_diffby_shop,sales_diff_client_byinvid,mall_open', domainKind: 'business' });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('腾讯云', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-bizhelper-txCloud-getSign'
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('根据code获取特殊货品', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-dres-speicalSpu-getByCode',
            code: 66666
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('积分规则查询', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-score-rule-scoreRule-getRuleToMeta',
            domainKind: 22
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('检查当前企业在线支付开通状态', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-config-ugr-tenant-checkOnlinePayChannel'
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it('权限配置', async function () {
        const res = await common.apiDo({
            apiKey: 'ec-config-priv-roleCol-getInBatch',
            jsonParam: { roleId: LOGINDATA.roleIds }
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });
});