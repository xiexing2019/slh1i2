'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const getBasicData = require('../../data/getBasicData');
const format = require('../../data/format');
const sspdBasicJson = require('./json/basicJson');
const sspdAccount = require('./sspdAccount');
const slh2 = require('../../reqHandler/slh2');

let loginReq = module.exports = {};

const { defParams, mobiles, defPlatparams } = sspdAccount;


/**
 * syncUnitId - 更新udid
 * @description 根据手机号获取可访问的单元id；对于笑铺来说通常就一个单元
 * @param {Object} deviceNo 对于笑铺而言为手机号
 * @param {Object} unitName 对于笑铺而言为账套名称
 */
loginReq.syncUnitId = async ({ deviceNo, name = mobiles[caps.name]['unitName'] }) => {
    const res = await slh2.config.findDeviceUnit({ deviceNo, defParams });
    try {
        caps.unitId = res.result.data.rows.find(obj => obj.name == name || obj.name == '默认账套').id;
    } catch (error) {
        throw new Error(`更新udid失败:\n${JSON.stringify(res)}\n${error}`);
    };
};

/**
 * sspdLogin - 店员登录
 * @description 笑铺员工根据手机号和验证码登录
 * @param {Object} params
 * @param {string} params.mobile 手机号
 * @param {string} [params.verifyCode='0000'] 验证码
 * @param {boolean} [params.mustVerify=false] 默认不会真正发送短信验证码
 */
loginReq.sspdLogin = async (params) => {
    const res = await slh2.mdm.sspdLogin(Object.assign({ unitId: caps.unitId, verifyCode: '0000', mustVerify: false }, defParams, params));
    try {
        LOGINDATA = res.result.data; //更新登录信息
    } catch (error) {
        throw new Error(`更新LOGINDATA失败:\n${JSON.stringify(res)}`);
    };
};

/**
 * mdmUserLogin - 笑铺后台登录
 * @description  笑铺后台登录
 * @param {Object} params
 */
loginReq.mdmUserLogin = async (params) => {
    params = format.packJsonParam(Object.assign({
        //unitId: caps.unitId,
        //code:'slhadmin',
        password: '000000',
        token: ''
    }, defPlatparams, params));
    const res = await slh2.mdm.userLogin(params);
    await common.delay(500);
    return res;
};

/**
 * sendCode 发送验证码
 * @param {Object} params
 * @param {string} params.mobile 手机号
 */
loginReq.sendCode = async (params) => {
    const res = await slh2.mdm.sendCode({ unitId: caps.unitId, phone: params.mobile, ...defParams });
    // console.log(res);
    return res;
};

/**
 * 根据手机号查询验证码
 * @param {Object} params
 * @param {string} params.mobile 手机号
 */
loginReq.getCode = async (params) => {
    const res = await slh2.bizhelper.getVerifyCode({ sessionKey: params.mobile, ...defParams });
    // console.log(res);
    return res;
};

/**
 * 笑铺日志登录+获取常用数据
 * @description 相同手机号时不重新登录
 * @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
 */
loginReq.sspdLoginBySeq = async function ({ seq = '000', name } = {}) {
    if (!mobiles[caps.name]) throw new Error(`本环境没有自动化的笑铺帐套`);
    const mobile = mobiles[caps.name][seq];
    // 更新udid
    await loginReq.syncUnitId({ deviceNo: mobile, name });
    // 相同手机+帐套时 不重新登录
    if (mobile == LOGINDATA.deviceNo && LOGINDATA.unitId == caps.unitId) return;

    await loginReq.sspdLogin({ mobile });
    // 获取基本信息
    // 先获取未隔离的信息(如字典),再切换到每个门店时获取相应的信息(如客户)
    await loginReq.getCommonBasicData();
    await loginReq.getBasicData();
};

/**
 * 笑铺日记员工登录+获取常用数据
 * @description 相同手机号时不重新登录
 * @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
 */
loginReq.sspdLoginByStaff = async function ({ seq = '004', name } = {}) {
    await loginReq.sspdLoginBySeq({ seq, name });
};

/**
 * 真实验证码登录
 * @description 相同手机号时不重新登录
 * @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
 */
loginReq.sspdLoginByCode = async function ({ seq = '000', name } = {}) {
    if (!mobiles[caps.name]) throw new Error(`本环境没有自动化的笑铺帐套`);
    const mobile = mobiles[caps.name][seq];
    // 更新udid
    await loginReq.syncUnitId({ deviceNo: mobile, name });

    //发送验证码
    await loginReq.sendCode({ mobile });
    //查询验证码
    const verifyCode = await loginReq.getCode({ mobile }).then(res => res.result.data.val);
    console.log(verifyCode);

    // 相同手机+帐套时 不重新登录
    if (mobile == LOGINDATA.deviceNo && LOGINDATA.unitId == caps.unitId) return;

    await loginReq.sspdLogin({ mobile, verifyCode });
    // 获取基本信息
    // 先获取未隔离的信息(如字典),再切换到每个门店时获取相应的信息(如客户)
    await loginReq.getCommonBasicData();
    await loginReq.getBasicData();
};

/**
 * 切换门店(不获取信息)
 * @param {Object} params
 * @param {string} params.shopName 门店名称
 */
loginReq.changeShopFast = async ({ shopName = '常青店' } = {}) => {
    // 获取可切换门店列表
    if (shopName == LOGINDATA.shopName) return;
    const shopInfo = await slh2.mdm.getShopListByDevice().then(res => res.result.data.rows.find(data => data.name == shopName));
    if (!shopInfo) throw new Error(`没有查到要切换的门店:${shopName}`);
    // 切换门店
    await slh2.mdm.changeUnit({ shopId: shopInfo.id, unitId: LOGINDATA.unitId });
};

/**
 * 切换门店
 * @param {Object} params
 * @param {string} params.shopName 门店名称
 */
loginReq.changeShop = async ({ shopName = '常青店' } = {}) => {
    await loginReq.changeShopFast({ shopName })
    // 获取门店基本信息
    await loginReq.getBasicData();
};

/**
 * 登录加切换门店
 */
loginReq.sspdLoginAndChangShop = async ({ shopName = '常青店' } = {}) => {
    await loginReq.sspdLoginBySeq();
    await loginReq.changeShop({ shopName });
};

/**
 * staffSave - 新增店员
 * @description 为了方便验证 一个店员只对应一个角色
 * @param {Object} params
 * @param {string} params.roleName 角色名 (老板,店长...)
 * @param {string} params.mobile 手机号
 * @param {string} params.userName 用户名
 */
loginReq.staffSave = async (params) => {
    const roleList = await slh2.om.getPrivsList({ wrapper: true, flag: 1 }).then(res => res.result.data.rows.find(obj => obj.name === params.roleName));
    if (roleList.length < 1) {
        throw new Error(`新增店员失败,未找到角色:${params.roleName}`);
    };
    await slh2.mdm.saveStaff({
        verifyCode: '0000',
        staff: {
            userName: params.userName,
            mobile: params.mobile,
            roleIds: roleList.id,
            rem: params.rem,
            code: params.mobile, //工号001（新增，对于笑铺就是手机号*）
            depId: LOGINDATA.depId
        }
    });
};

/**
 * 获取门店基本数据
 * @description accountList:实体账户列表
 */
loginReq.getBasicData = async function () {
    // 相同店铺不再重新获取
    if (BASICDATA.hasOwnProperty(LOGINDATA.shopName) && BASICDATA[LOGINDATA.shopName].ids) {
        BASICDATA.ids = BASICDATA[LOGINDATA.shopName].ids;
        return;
    };
    // 常用id

    BASICDATA[LOGINDATA.shopName].ids = await getBasicData.getDictIds(basicDataArr);

    // 实体账户列表 使用简称作为key
    BASICDATA[LOGINDATA.shopName].accountList = await slh2.fin.getAccountList()
        .then((res) => {
            let json = {};
            res.result.data.rows.forEach(element => {
                json[element.nameAbbr] = element;
                json[element.nameAbbr].typeId = getAccountTypeId(element);
            });
            return json;
        });

    // if (BASICDATA.ids) return;

    BASICDATA.ids = BASICDATA[LOGINDATA.shopName].ids;
    if (!BASICDATA.hasOwnProperty('styleInfo')) BASICDATA.styleInfo = {};
    BASICDATA.styleInfo.agc001 = await slh2.dres.getStyleInfo({
        id: BASICDATA.ids.styleidAgc001
    }).then(res => res.result.data);
};



/**
 * 获取银行账户typeId (简单匹配.不一定准确)
 * @param {object} params 
 * @param {string} params.acctType 12:现金,13:银行,14:第三方记账账户,15:第三方在线支付账户,16微商城账户
 * @param {string} params.chanId acctType=14时 1:支付宝 4:微信
 * @return {string} typeId
 */
function getAccountTypeId(params) {
    let typeName = { '12': 'cash', '13': 'bankCard', '14': '', '15': 'other', '16': 'bankCard' }[params.acctType];
    if (params.acctType == 14) typeName = params.chanId == 1 ? 'aliF2F' : 'wxF2F';
    // console.log(`typeName=${typeName},acctType=${params.acctType},chanId=${params.chanId}`);
    return sspdBasicJson.payType[typeName].id;
};

/**
 * 获取帐套基本数据
 */
loginReq.getCommonBasicData = async function () {
    if (BASICDATA.colorIds) return;

    BASICDATA.colorIds = await slh2.sync.syncData({
        type: 'sc_dict_color'
    }).then((res) => res.result.data.rows);

    BASICDATA.sizeIds = await slh2.sync.syncData({
        type: 'sc_dict_size'
    }).then((res) => res.result.data.rows);

    BASICDATA.brandIds = await slh2.sync.syncData({
        type: 'sc_dict_brand'
    }).then((res) => res.result.data.rows);

    BASICDATA.classIds = await slh2.sync.syncData({
        type: 'dres_style_class'
    }).then((res) => res.result.data.rows);

    BASICDATA.season = await slh2.sync.syncData({
        type: 'sc_dict_season'
    }).then((res) => res.result.data.rows);

    BASICDATA.fabric = await slh2.config.getDictListWithDresNum({
        typeId: 637, flag: 1
    }).then((res) => res.result.data.rows);

    BASICDATA.form = await slh2.config.getDictListWithDresNum({
        typeId: 835, flag: 1
    }).then((res) => res.result.data.rows);

    BASICDATA.finInCatId = await common.apiDo({
        apiKey: 'ec-config-list-dict',
        jsonParam: { typeId: 1002, flag: 1 }
    }).then((res) => res.result.data.rows.find(obj => obj.codeName == "自动化收入"));

    BASICDATA.finExpCatId = await common.apiDo({
        apiKey: 'ec-config-list-dict',
        jsonParam: { typeId: 1003, flag: 1 }
    }).then((res) => res.result.data.rows.find(obj => obj.codeName == "自动化支出"));
    // 门店信息
    await slh2.mdm.getShopListByDevice().then(res => res.result.data.rows.forEach(element => {
        BASICDATA[element.name] = { shopId: element.id };
    }));
    //特殊授权员工
    BASICDATA.speAuthStaff = slh2.mdm.getStaffList({ flag: 1, userName: "特殊授权" }).then((res) => res.result.data.rows[0]);
};

loginReq.getChainDiaryData = async function () {
    if (BASICDATA.colorIds) return;

    BASICDATA.colorIds = await slh2.sync.syncData({
        type: 'sc_dict_color'
    }).then((res) => res.result.data.rows);

    BASICDATA.sizeIds = await slh2.sync.syncData({
        type: 'sc_dict_size'
    }).then((res) => res.result.data.rows);

    BASICDATA.brandIds = await slh2.sync.syncData({
        type: 'sc_dict_brand'
    }).then((res) => res.result.data.rows);

    BASICDATA.classIds = await slh2.sync.syncData({
        type: 'dres_style_class'
    }).then((res) => res.result.data.rows);

    BASICDATA[LOGINDATA.shopName] = { shopId: LOGINDATA.shopId };
    basicDataArr[1].apiKey = 'ec-mdm-org-cust-list';
    await loginReq.getBasicData();
};

const basicDataArr = [{
    apiKey: 'ec-dres-spu-list',
    key: 'styleidAgc001',
    searchToken: 'agc001'
}, {
    apiKey: 'ec-mdm-user-cust-list',
    key: 'dwidXw',
    jsonParam: { searchToken: '小王' }
}, {
    apiKey: 'ec-mdm-user-cust-list',
    key: 'dwidXl',
    jsonParam: { searchToken: '小李' }
}, {
    apiKey: 'ec-mdm-org-supp-list',
    key: 'dwidVell',
    nameLike: 'Vell'
}];