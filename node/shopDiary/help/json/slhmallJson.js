const common = require('../../../lib/common');
const caps = require('../../../data/caps');

let slhmallJson = module.exports = {};

/**
 * 拼接采购订单参数
 * @description ec-sppur-purBill-createFull
 * @param {object} params
 * @param {object} params.styleInfo 款号信息
 * @param {string} [params.shipPayKind=0] 0-快递(默认) 1-预先支付（未启用） 2-客户自提
 * @param {string} [params.payKind=1] 1 预付，2 货到付款，3 线下支付
 * @param {number} [param.count=2] 添加到单据sku数量
 */
slhmallJson.purJson = (params) => {
    const [styleInfo, trader] = [params.styleInfo.result.data, params.styleInfo.params];
    let docId;
    if (typeof (styleInfo.spu.ecCaption.docHeader[0]) == "undefined") {
        docId = '';
    } else {
        docId = styleInfo.spu.ecCaption.docHeader[0].docId;
    }
    const detailSpu = {
        spuId: styleInfo.spu.id,
        spuTitle: styleInfo.spu.title,
        spuCode: styleInfo.spu.code,
        spuDocId: styleInfo.spu.ecCaption.docHeader[0].docId  //这里要传，不然导致下订单之后订单款号没有图片
    };
    let main = {
        sellerId: trader._tid,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: 0,//运费。若为到付，则为0
        shipPayKind: params.shipPayKind || 0, //shipPayKind  int  是
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: params.payKind || 1,//2,//付款方式。 1 预付，2 货到付款，3 线下支付
        couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: LOGINDATA.defAddressId,//收货地址ID 取当前用户默认地址 需要登录时获取
    }, details = [];
    common.randomSort(styleInfo.skus).slice(0, params.hasOwnProperty('count') ? params.count : 2).forEach((sku) => {
        const detail = Object.assign(new slhmallJson.GetDetail(sku), detailSpu);
        //console.log(detail);
        main.originalMoney += detail.originalPrice;
        main.money = common.add(main.money, detail.money);
        main.totalNum += detail.num;
        details.push(detail);
        //console.log(details);
    });
    main.totalMoney = [main.money, -main.shopCoupsMoney, main.shipFeeMoney].reduce((a, b) => common.add(a, b), 0);
    return { orders: [{ main, details }] };
};

slhmallJson.GetDetail = function (sku) {
    this.skuId = sku.id;
    this.spec1 = sku.spec1;
    this.spec1Name = sku.ecCaption.spec1 || '';
    this.spec2 = sku.spec2;
    this.spec2Name = sku.ecCaption.spec2 || '';
    this.spec3 = sku.spec3;
    this.spec3Name = sku.ecCaption.spec3 || '';
    this.num = common.getRandomNum(1, 10);
    this.originalPrice = slhmallJson.changeBillSkuPrice(sku.pubPrice);
    this.price = this.originalPrice;
    this.money = common.mul(this.num, this.price);
};

/**
 * 修改单据商品价格
 * @description 线上环境需要控制单据总价
 * @param {*} price
 */
slhmallJson.changeBillSkuPrice = function (price) {
    return caps.name.includes('cs3d1') && Number(price) >= 100 ? common.div(price, 100) : price;
};