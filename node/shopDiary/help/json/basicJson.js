const common = require('../../../lib/common');

let sspdBasicJson = module.exports = {};

//新增货品
//ec-dres-spu-save
sspdBasicJson.addGoodJson = function (params = {}) {
	let jsonParam = {},
		barCodes = [],
		str = common.getRandomStr(7),
		colorIds = params.colorIds || BASICDATA.colorIds.slice(1, 6).map((info) => info.codeValue),
		sizeIds = params.sizeIds || BASICDATA.sizeIds.slice(1, 6).map((info) => info.codeValue);
	//console.log(`BASICDATA.colorIds : ${JSON.stringify(BASICDATA)}`);

	jsonParam.spuCommonDto = {
		code: params.code || `${str}`,
		name: params.name || `货品${str}`,
		colorIds: colorIds.join(','),
		sizeIds: sizeIds.join(','),
		special: params.special || false,
		marketDate: common.getCurrentDate(),
		flag: 1, //是否停用
		stdprice1: params.stdprice1 || '200', //销售价
		stdprice2: params.stdprice2 || '100'
	};

	for (let index = 0; index < colorIds.length; index++) {
		const colorId = colorIds[index];
		sizeIds.forEach(sizeId => {
			barCodes.push({
				colorId,
				sizeId
			});
		});
	};
	common.randomSort(barCodes);

	jsonParam.spuExtraDto = {
		brandId: params.brandId || '', //BASICDATA.brandIds.shift(),
		rem: params.rem || '',
		classId: params.classId || '', //BASICDATA.classIds.shift(),
		tagPrice: params.hasOwnProperty('tagPrice') ? params.tagPrice : 200, //销售价
		styleId: params.styleId || '',
		liningId: params.liningId || '',
		season: params.season || '1',
		onlineFlag: 0,
		videoId: '',
		coverUrl: '',
		videoUrl: '',
		docId: 'doc38540653',
		isPublic: params.hasOwnProperty('isPublic') ? params.isPublic : 1,//"是否同步其他门店，0或1"
	};

	//价格
	jsonParam.purprices = [{
		orgSuppId: params.hasOwnProperty('dwid') ? params.dwid : BASICDATA.ids.dwidVell, //供应商id
		purPrice: params.hasOwnProperty('purPrice') ? params.purPrice : 100, //进货价
		isDef: 1, //是否默认 1是0否，款号维护中基础信息中的厂商和价格传1其他传0
	}];
	//库存，修改时不传
	if (params.action === 'edit') {
		jsonParam.spuStockNum = [];
	} else {
		if (typeof params.spuStockNum == 'object') {
			jsonParam.spuStockNum = params.spuStockNum;
		} else {
			const stockCount = Number(params.spuStockNum) || 0;
			jsonParam.spuStockNum = new Array(stockCount).fill({}).map((obj, index) => {
				return {
					colorId: barCodes[index].colorId,
					sizeId: barCodes[index].sizeId,
					stockNum: common.getRandomNum(10, 100)
				};
			});
		};
	};

	//条码
	jsonParam.spuBarcodes = [];

	return jsonParam;
};

//新增字典
sspdBasicJson.dictJson = function (params = {}) {
	//console.log(`params.typeId : ${JSON.stringify(params.typeId)}`);
	let jsonParam = Object.assign({
		codeName: params.typeId + common.getRandomStr(6),
	}, params);
	if (jsonParam.id) { jsonParam.flag = 1 };
	switch (params.typeId) {
		case 631:
		case 604:
			jsonParam.showOrder = params.showOrder || 0;
			break;
		case 601:
			jsonParam.parentCode = params.parentCode || 1;
			jsonParam.showOrder = params.showOrder || 0;
			jsonParam.flag = 1;
			break;
		case 605:
			jsonParam.parentCode = params.parentCode || 1;
			jsonParam.showOrder = params.showOrder || 0;
			break;
		case 606:
			jsonParam.unitId = LOGINDATA.unitId;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 637:
			jsonParam.unitId = LOGINDATA.unitId;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 835:
			jsonParam.unitId = LOGINDATA.unitId;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 752:
			jsonParam.flag = 1;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 753:
			jsonParam.flag = 1;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 754:
			jsonParam.flag = 1;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		case 846:
			jsonParam.flag = 1;
			jsonParam.rem = params.rem || common.getRandomStr(5);
			break;
		default:
			break;
	};
	return jsonParam;
};

//客户
sspdBasicJson.addCustJson = (params = {}) => {
	//ec-mdm-user-cust-save
	let jsonParam = {},
		str = common.getRandomStr(6);
	jsonParam.address = {
		detailAddr: params.address || '', //地址
	};
	jsonParam.userCust = {
		shopId: params.shopId || LOGINDATA.shopId,
		userName: params.userName || `客户${str}`,
		// shopId: params.shopId || LOGINDATA.shopId,
		mobile: params.mobile || '',
		gender: params.gender || '', //性别
		birthday: params.birthday || '',
		//	address: params.address || params.detailAddr || '', //地址
		initScore: params.initScore || '', //积分
		balance: params.balance || '', //余额
		rem: params.rem || '',
		//parentId: params.parentId || '',笑铺可以不用传
		capability: '2', //客户固定2
	}
	return jsonParam;
};

//供应商
sspdBasicJson.addSupplierJson = (params = {}) => {
	//ec-mdm-org-saveFull
	let jsonParam = {
		'2': {
			shopId: LOGINDATA.shopId,
			type: 0
		}
	},
		str = common.getRandomStr(6);

	jsonParam.org = {
		name: params.name || `供应商${str}`,
		telephone: params.telephone || '',
		rem: params.rem || '',
		balance: params.balance || '',
		organType: '1', //必填 固定1 组织类型为公司
		capability: '2', //必填 固定2 能力位为供应商
	};
	jsonParam.address = {
		detailAddr: params.detailAddr || '',
	};
	return jsonParam;
};

sspdBasicJson.custJson2 = function (params) {
	let json = {
		"1": {
			"areaId": "",
			"shopId": "",
			"staffId": "",
			"typeId": "",
			"priceType": "",
			"discount": 1,
			"rank": "",
			"backRate": "",
			"noRetFlag": 0,
			"isDebt": 0,
			"linkFlag": 0,
			"relFranId": "",
			"authCode": "",
			"labelCode": "",
			"extraSrc": "",
			"creditBalance": "",
			"alarmBalance": "", "slbFlag": 0,
			"clientSign": "", "backCycle": "",
			"bodyTypeId": "", "bodyHeight": "",
			"bodyWeight": "", "hobby": "", "familyInfo": "",
			"skinColorId": "", "dressStyleId": "", "disposTypeId": "",
			"dressColorId": "", "coatSizeId": "", "pantsSizeId": "",
			"footSizeId": "",
			"ecCaption": {
				"shopId": "", "staffId": "",
				"typeId": "", "priceType": "", "areaId": "", "rank": "", "relFranId": "",
				"bodyTypeId": "", "skinColorId": "", "dressStyleId": "", "disposTypeId": "", "dressColorId": "",
				"coatSizeId": "", "pantsSizeId": "", "footSizeId": ""
			}
		},
		"address": { "provinceCode": "", "cityCode": "", "countyCode": "", "detailAddr": "" },
		"owner": { "mobile": "", "userName": "", "birthday": "", "birthLunarFlag": 0, "gender": 0 },
		"org": {
			"name": `客户${common.getRandomStr(6)}`,
			"code": "",
			"organType": 1,
			"capability": 1,
			"compId": "",
			"parentId": "", "telephone": "",
			"addrId": "", "areaAccode": "",
			"contacts": { "fax": "", "weixin": "" },
			"fax": "", "weixin": "", "showOrder": "", "tenantId": "", "remark": "", "accode": "", "createdDate": "",
			"updatedDate": "", "ver": "", "ecCaption": { "parentId": "" }
		}
	};
	return json;
};

/**
 * 支付类型
 * 备注掉的为服务端待废弃
 * id:typeId finIOPay:支付字段名
 */
sspdBasicJson.payType = {
	unknown: { id: -1 },//未指定
	offLine: { id: 0 },//线下协商交易
	cash: { id: 1, finIOPay: 'pay1' },//现金
	aliF2F: { id: 2, finIOPay: 'pay5' }, //支付宝(支付宝大类) 1<<1
	bankCard: { id: 4, finIOPay: 'pay2' },//刷卡 1<<2
	coupons: { id: 8 },//优惠券 1<<3
	slhAccount: { id: 16, finIOPay: 'pay7' },//储值支付 1<<4
	// ali: 32,//支付宝PC上即时到账 1<<5
	// wxJSApi: { id: 64, finIOPay: 'pay8' },//微信公众号支付 1<<6
	wxF2F: { id: 256, finIOPay: 'pay6' },//微信大类 1<<8
	// wxApp: 512,//APP端调用微信支付 1<<9
	credit: { id: 128 },//信用 赊账 1<<7
	rules: { id: 1024 },//卡券和促销规则支付（卡券优惠部分）1<<10
	score: { id: 2048 },//积分 1<<11
	// aliApp: 4096,//支付宝APP支付 1<<12
	other: { id: 16384, finIOPay: 'pay9' },//在线支付 1<<14
	deposit: { id: 32768 },//定金 1<<15
	given: { id: 655536 },//免单/赠送 1<<16
	remit: { id: 131072, finIOPay: 'pay3' },//汇款 1<<17
	agency: { id: 262144, finIOPay: 'pay4' },//代收 1<<18
};

sspdBasicJson.purJson = function (params = {}) {
	if (!params.billType) params.billType = 'pur';
	let jsonParam = {
		main: {
			compId: params.dwid || BASICDATA.ids.dwidVell, //供应商
			deliverFlag: 1, //发货状态(0 没有发货，1 开单且直接全部发货，2 部分发货，3 已完成发货)
			srcType: 1,//开单类型，正常开单：1"
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			favorMoney: params.favorMoney || 0, //抹零或优惠金额*，没有为0
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '整单备注',
			mainDiscount: params.mainDiscount || 1,
		},
		fin: {
			//笑铺现在没有核销功能
			// verifyBillIds: "核销单据ID（收付款单的ID），以逗号间隔",
			// verifyMoney: "核销金额",
			// balance: 0
		},
		payways: [],
	};
	//明细 details
	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
		throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
	};
	const barCodes = common.randomSort(styleInfo.spuBarcodes),
		priceKey = params.billType == 'pur' ? 'purPrice' : 'price';
	let actualPay = 0;
	let tempDetail = params.details;
	jsonParam.details = new Array(params.count || 2).fill({}).map((detail, index) => {
		detail = {
			tenantSpuId: styleInfo.tenantSpuId,
			colorId: barCodes[index].colorId,
			sizeId: barCodes[index].sizeId,
			num: (tempDetail && tempDetail[index] && tempDetail[index].num) || common.getRandomNum(10, 100),
			price: styleInfo.spuCommonDto[priceKey],
			rem: `明细${index + 1}`
		};
		detail.realPrice = common.mul(detail.price, jsonParam.main.mainDiscount);
		actualPay += common.mul(detail.num, detail.realPrice);
		return detail;
	});

	//挂单
	if (params.isPend == 1) {
		jsonParam.isPend = 1;
		return jsonParam;
	};

	//支付 （typeId 1 现金，4 刷卡，131072  汇款，262144 代收，2 支付宝，256 微信，16零钱）
	if (params.payways && params.payways.length > 0) {
		try {
			jsonParam.payways = params.payways.map((payway) => {
				// e.g.{ accountName:'现', money:1000 } { accountId:'69024', money 1000 }
				payway.accountId = params.accountId || BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].id;
				payway.typeId = params.typeId || BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].typeId;
				delete payway.accountName;
				return payway;
			});
		} catch (error) {
			throw new Error(`获取账户信息出错:${JSON.stringify(params.payways)}\n` + error);
		};
	} else {
		jsonParam.payways = [{
			typeId: params.typeId || BASICDATA[LOGINDATA.shopName].accountList['现'].typeId,
			accountId: params.accountId || BASICDATA[LOGINDATA.shopName].accountList['现'].id,
			money: actualPay
		}];
	};

	return jsonParam;
};

sspdBasicJson.salesJson = function (params = {}) {
	if (!params.dwid) params.dwid = BASICDATA.ids.dwidXw;
	params.billType = 'sales';
	return sspdBasicJson.purJson(params);
};

sspdBasicJson.purPayOnlineJson = function (params = {}) {
	if (!params.billType) params.billType = 'pur';
	let jsonParam = {
		main: {
			compId: params.dwid || BASICDATA.ids.dwidVell, //供应商
			deliverFlag: 1, //发货状态(0 没有发货，1 开单且直接全部发货，2 部分发货，3 已完成发货)
			srcType: 1,//开单类型，正常开单：1"
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			favorMoney: params.favorMoney || 0, //抹零或优惠金额*，没有为0
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '',
			mainDiscount: params.hasOwnProperty('mainDiscount') ? params.mainDiscount : 1,
		},
		fin: {
			//笑铺现在没有核销功能
			// verifyBillIds: "核销单据ID（收付款单的ID），以逗号间隔",
			// verifyMoney: "核销金额",
			// balance: 0
		},
		payways: [],
	};
	//明细 details
	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
		throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
	};
	const barCodes = common.randomSort(styleInfo.spuBarcodes),
		priceKey = params.billType == 'pur' ? 'purPrice' : 'price';
	let actualPay = 0;
	let tempDetail = params.details;
	jsonParam.details = new Array(params.count || 2).fill({}).map((detail, index) => {
		detail = {
			tenantSpuId: styleInfo.tenantSpuId,
			colorId: barCodes[index].colorId,
			sizeId: barCodes[index].sizeId,
			num: (tempDetail && tempDetail[index] && tempDetail[index].num) || common.getRandomNum(5, 10),
			price: params.hasOwnProperty('price') ? params.price : styleInfo.spuCommonDto[priceKey],
		};
		detail.realPrice = common.getRandomNum(1, 6);//    common.mul(detail.price, jsonParam.main.mainDiscount);
		actualPay += common.mul(detail.num, detail.realPrice);
		return detail;
	});

	jsonParam.payways = [{
		typeId: BASICDATA[LOGINDATA.shopName].accountList['收'].typeId,
		accountId: BASICDATA[LOGINDATA.shopName].accountList['收'].id,
		money: actualPay,
		payFlag: 0,
		tallyType: 2,
	}];

	return jsonParam;
};

sspdBasicJson.salesPayOnlineJson = function (params = {}) {
	if (!params.dwid) params.dwid = BASICDATA.ids.dwidXw;
	params.billType = 'sales';
	return sspdBasicJson.purPayOnlineJson(params);
};

/**
 * 盘点计划
 * @param {object} params 
 */
sspdBasicJson.invCheckPlanJson = function (params = {}) {
	let jsonParam = {
		invId: LOGINDATA.shopId,
		unitId: LOGINDATA.unitId,
		shopId: LOGINDATA.shopId,
		checkType: params.checkType || 5 //计划类别：1、按类别 2、按品牌 3、按厂商 4、按组合 5、按款号
	};
	return jsonParam;
};

/**
 * 盘点单
 * @param {object} params 
 */
sspdBasicJson.invCheckJson = function (params = {}) {
	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	//console.log(styleInfo);
	let jsonParam = {
		invId: LOGINDATA.shopId,
		unitId: LOGINDATA.unitId,
		shopId: LOGINDATA.shopId,
		deliver: params.deliver || LOGINDATA.userId,
		planId: params.planId,
		rem: params.rem || '',
	};
	const stockNumber = common.randomSort(styleInfo.spuStockNum);
	let tempDetail = params.details;
	jsonParam.billDetails = new Array(params.count || 1).fill({}).map((detail, index) => {
		detail = {
			tenantSpuId: styleInfo.tenantSpuId,
			colorId: stockNumber[index].colorId,
			sizeId: stockNumber[index].sizeId,
			commitNum: (tempDetail && tempDetail[index] && tempDetail[index].num) || common.getRandomNum(1, 100),
			beforeNum: stockNumber[index].stockNum
		};
		return detail;
	});
	return jsonParam;
};

sspdBasicJson.moveOutJson = function (params = {}) {
	let jsonParam = {
		//id: '',
		unitId: LOGINDATA.unitId,
		shopId: LOGINDATA.shopId,
		//invId: LOGINDATA.shopId,//仓库id*
		otherShopId: BASICDATA['门店二'].shopId,//对方门店id*
		//otherInvId: BASICDATA['门店二'].shopId,
		bizDate: common.getCurrentDate(),
		deliver: LOGINDATA.userId,
		// receiver: "接收人",
		totalNum: 0,
		totalMoney: 0,
		rem: '备注',
		billDetails: [],
	};

	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
		throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
	};
	const barCodes = common.randomSort(styleInfo.spuBarcodes);
	const detailNum = common.getRandomNum(5, 20);
	jsonParam.billDetails = new Array(params.count || 2).fill({}).map((detail, index) => {
		detail = {
			//id: '',//明细id 修改时必填
			//billId: '',//调拨单主键 修改时必填
			unitId: jsonParam.unitId,
			// shopSpuId: "门店spuId",
			// shopSkuId: "门店skuId",
			tenantSpuId: styleInfo.tenantSpuId,
			colorId: barCodes[index].colorId,
			sizeId: barCodes[index].sizeId,
			commitNum: detailNum,
			price: styleInfo.spuCommonDto.purPrice,
			realPrice: styleInfo.spuCommonDto.purPrice,
			money: common.mul(detailNum, styleInfo.spuCommonDto.purPrice),
			rem: `备注${index}`,
			// rowId: "尺码表头行id  尺码表头情况下必填"
		};
		jsonParam.totalMoney += detail.money;
		jsonParam.totalNum += detail.commitNum;
		return detail;
	});
	return jsonParam;
};


/**
 * 充值单
 */
sspdBasicJson.rechargeBillJson = function (params = {}) {
	// let jsonParam = params.online ? sspdBasicJson.purPayOnlineJson() : sspdBasicJson.purJson();
	let jsonParam = {
		main: {
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			compId: params.compId || BASICDATA.ids.dwidXw,
			rechargeMoney: params.payways ? 0 : common.getRandomNum(10, 500),
			giftsMoney: params.hasOwnProperty('giftsMoney') ? params.giftsMoney : common.getRandomNum(1, 10),
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '充值备注',
		},
		payways: [],
	};
	//支付 （typeId 1 现金，4 刷卡，131072  汇款，262144 代收，2 支付宝，256 微信，16零钱）
	if (params.payways && params.payways.length > 0) {
		try {
			jsonParam.payways = params.payways.map((payway) => {
				// e.g.{ accountName:'现', money:1000 } { accountId:'69024', money 1000 }
				payway.accountId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].id;
				payway.typeId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].typeId;
				jsonParam.main.rechargeMoney = common.add(jsonParam.main.rechargeMoney, payway.money);
				delete payway.accountName;
				return payway;
			});
		} catch (error) {
			throw new Error(`获取账户信息出错:${JSON.stringify(params.payways)}\n` + error);
		};
	} else {
		jsonParam.payways = [{
			typeId: BASICDATA[LOGINDATA.shopName].accountList['现'].typeId,
			accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,
			money: jsonParam.main.rechargeMoney,
		}];
	};

	return jsonParam;
};

//收款单
sspdBasicJson.receiptBillJson = function (params = {}) {
	let jsonParam = {
		main: {
			srcType: 8,
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			compId: params.compId || BASICDATA.ids.dwidXw,
			payways: params.payways ? 0 : common.getRandomNum(10, 500),
			// totalMoney: 0,
			favorMoney: params.hasOwnProperty('favorMoney') ? params.favorMoney : common.getRandomNum(1, 50),
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '收款单备注',
		},
		payways: [],
	};
	//支付 （typeId 1 现金，4 刷卡，131072  汇款，262144 代收，2 支付宝，256 微信，16零钱）
	if (params.payways && params.payways.length > 0) {
		try {
			jsonParam.payways = params.payways.map((payway) => {
				// e.g.{ accountName:'现', money:1000 } { accountId:'69024', money 1000 }
				payway.accountId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].id;
				payway.typeId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].typeId;
				jsonParam.main.payways = common.add(jsonParam.main.payways, payway.money);
				delete payway.accountName;
				return payway;
			});
		} catch (error) {
			throw new Error(`获取账户信息出错:${JSON.stringify(params.payways)}\n` + error);
		};
	} else {
		jsonParam.payways = [{
			typeId: BASICDATA[LOGINDATA.shopName].accountList['现'].typeId,
			accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,
			money: jsonParam.main.payways,
		}];
	};
	jsonParam.main.totalMoney = common.add(jsonParam.main.payways, jsonParam.main.favorMoney);
	return jsonParam;
};

/**
 * 付款单--不允许组合支付
 * { accountName:'现', payMoney:1000 } 
 */
sspdBasicJson.purOutBillJson = function (params = {}) {
	let jsonParam = Object.assign({}, {
		shopId: LOGINDATA.shopId,
		ownerId: LOGINDATA.userId,
		compId: BASICDATA.ids.dwidVell,
		accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,//账户id
		payMoney: common.getRandomNum(10, 500),
		proDate: common.getCurrentDate(),
		rem: '付款单备注',
		payCap: 4,//银行支付必传
	}, params);
	return jsonParam;
}

/**
 * 商城订单同步
 */
sspdBasicJson.slhMallBillJson = function (params = {}) {
	let json = {
		proDate: params.proDate || common.getCurrentDate(),
		ownId: 887161,
		billId: `${Date.now()}${common.getRandomNumStr(3)}`,
		slhUnitId: LOGINDATA.unitId,
		mallShopTenantId: 1319,
		remark: '商城测试单',
		details: [],
		totalMoney: 0
	};
	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
		throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
	};
	const priceKey = params.billType == 'pur' ? 'purPrice' : 'price';
	let tempDetail = params.details;
	json.details = new Array(params.count || 2).fill({}).map((detail, index) => {
		detail = {
			billId: `${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`,
			skuCode: '190428000071',//styleInfo.skuCode,
			num: (tempDetail && tempDetail[index] && tempDetail[index].num) || common.getRandomNum(10, 100),
			price: styleInfo.spuCommonDto[priceKey],
		};
		json.totalMoney += common.mul(detail.num, detail.price);
		return detail;
	});
	return json;
}

/**
 * 商城订单提现
 */
sspdBasicJson.MallCashOutJson = function (params = {}) {
	let json = {
		ownId: 887161,
		remit: params.remit,
		handleFee: params.handleFee || 50,
		billId: params.billId,
		slhUnitId: LOGINDATA.unitId,
		mallShopTenantId: 1319,
		remark: '商城订单提现',
	};
	return json;
}

/**
 * 商城订单退货退款
 */
sspdBasicJson.ReturnMallBillJson = function (params = {}) {
	let json = {
		proDate: params.proDate || common.getCurrentDate(),
		ownId: 887161,
		billId: `${Date.now()}${common.getRandomNumStr(3)}`,
		slhUnitId: LOGINDATA.unitId,
		mallShopTenantId: 1319,
		remark: '商城测试单',
		isReturn: 1,
		details: [],
		totalMoney: 0,
		totalNum: 0,
		rechargeFlag: 1,
		refundMoney: 0
	};
	const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
	if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
		throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
	};
	const priceKey = params.billType == 'pur' ? 'purPrice' : 'price';
	let tempDetail = params.details;
	json.details = new Array(params.count || 2).fill({}).map((detail, index) => {
		detail = {
			billId: `${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`,
			skuCode: '190428000071',//styleInfo.skuCode,
			num: (tempDetail && tempDetail[index] && tempDetail[index].num) || common.getRandomNum(10, 100),
			price: styleInfo.spuCommonDto[priceKey],
		};
		json.totalMoney += common.mul(detail.num, detail.price);
		return detail;
	});
	return json;
}

/**
 * 收入单
 */
sspdBasicJson.finInJson = function (params = {}) {
	const realMoney = common.getRandomNum(100, 1000);
	let jsonParam = {
		main: {
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			inExpType: 1,
			totalMoney: realMoney,
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '自动化收入单',
			fileId: ''
		},
		fin: {
			balance: 0
		},
		payways: [],
		details: [
			{
				catId: params.catId || BASICDATA.finInCatId.codeValue,
				money: realMoney,
				rem: params.rem || '自动化收入单'
			}
		]
	};

	//支付 （typeId 1 现金，4 刷卡，131072  汇款，262144 代收，2 支付宝，256 微信，16零钱）
	if (params.payways && params.payways.length > 0) {
		try {
			jsonParam.payways = params.payways.map((payway) => {
				// e.g.{ accountName:'现', money:1000 } { accountId:'69024', money 1000 }
				payway.accountId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].id;
				payway.typeId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].typeId;
				delete payway.accountName;
				return payway;
			});
		} catch (error) {
			throw new Error(`获取账户信息出错:${JSON.stringify(params.payways)}\n` + error);
		};
	} else {
		jsonParam.payways = [{
			typeId: BASICDATA[LOGINDATA.shopName].accountList['现'].typeId,
			accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,
			money: realMoney
		}];
	};

	return jsonParam;
};

/**
 * 支出单
 */
sspdBasicJson.finExpJson = function (params = {}) {
	const realMoney = common.getRandomNum(100, 1000);
	let jsonParam = {
		main: {
			shopId: LOGINDATA.shopId,
			ownerId: LOGINDATA.userId,
			inExpType: 2,
			totalMoney: realMoney,
			proDate: params.proDate || common.getCurrentDate(),
			rem: params.rem || '自动化支出单',
			fileId: ''
		},
		fin: {
			balance: 0
		},
		payways: [],
		details: [
			{
				catId: params.catId || BASICDATA.finExpCatId.codeValue,
				money: realMoney,
				rem: params.rem || '自动化支出单'
			}
		]
	};

	//支付 （typeId 1 现金，4 刷卡，131072  汇款，262144 代收，2 支付宝，256 微信，16零钱）
	if (params.payways && params.payways.length > 0) {
		try {
			jsonParam.payways = params.payways.map((payway) => {
				// e.g.{ accountName:'现', money:1000 } { accountId:'69024', money 1000 }
				payway.accountId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].id;
				payway.typeId = BASICDATA[LOGINDATA.shopName].accountList[payway.accountName].typeId;
				delete payway.accountName;
				return payway;
			});
		} catch (error) {
			throw new Error(`获取账户信息出错:${JSON.stringify(params.payways)}\n` + error);
		};
	} else {
		jsonParam.payways = [{
			typeId: BASICDATA[LOGINDATA.shopName].accountList['现'].typeId,
			accountId: BASICDATA[LOGINDATA.shopName].accountList['现'].id,
			money: realMoney
		}];
	};

	return jsonParam;
};
