const name = require('../../../data/caps').name;

/**
 *
 */
const codeJson = {
    cs3d2: {
        code1: { "t": "1", "sn": "1566551898544", "s": "902209", "c": "902473", "mt": "20094", "targetUnitId": "25525", "billId": "576365" }
    },
    cs3d3: {
        code1: { t: 1, sn: '1529542058994', s: 847073, c: 896669, targetUnitId: 22885, billId: 572389 }
    },
    cs3d1: {
        code1: {}
    },
    sd_cg4: {
        code1: {}
    },
};



module.exports = codeJson[name];