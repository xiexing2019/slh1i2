
module.exports = {
    defParams: {
        productCode: 'sspdios',
        productVersion: '3.5.5'
    },
    defPlatparams: {
        productCode: 'web',
        productVersion: '1.0'
    },
    mobiles: {
        slh2_test: {
            '000': '15879981652',
            '004': '13333333331',
        },
        slh2_cb3: {
            // '000': '15821127045',
            '000': '18267150617',
            '004': '17682316809',
        },
        cs3d1: {
            //'000': '13112345678',//内嵌商城 估计凉了
            //'000': '18967130000',
            //'004': '13112345004',//内嵌商城 估计凉了
            //'unitName': '多门店'//内嵌商城 估计凉了
            '000': '13811115556',
            '004': '13355558888',
            'unitName': '北京店'
        },
        cs3d2: {
            //'000': '15800001652',
            //'004': '15800041652',
            //'000': '15879981000',
            //'004': '15879981004',
            //'104': '15879981104',
            //'000': '18760950000',
            '000': '18760953333',
            '004': '18760953004',
            '006': '18623451234',
            '007': '15712367890',  //性能测试专用
            //'000': '17856785678',  //账套结转测试
            //'000': '18967130000',
            //'004': '18760950004',
            //'004': '18711110000',
            //'004': '18967130001',
            //'unitName': '微商城2'
            'unitName': '陈梦雅5'
            //'unitName': 'xx'  //性能测试专用
            //'unitName': 'test1' //账套结转测试
        },
        cs3d3: {
            // '000': '15879981650',
            // '004': '15879981654',
            // '000': '15879980000',
            // '004': '15879980004',
            //'000': '15879982000',
            //'004': '15879982004',
            '000': '18588888881',
            '004': '18588888004',
            'unitName': 'qd3unit1'
            //'000': '15858156371',
            //'004': '15858156004',
            //'unitName': '常青店'
            //'000': '15879983333', //新建
        },
        asdd1: {
            '000': '15879900000',
            '004': '15879900004',
        },
        sd_online: {
            '000': '18267150617',//qiuxs
            '004': '17682316809',
            'unitName': '默认账套'
        },
        sd_cg4: {
            '000': '18760959806',//cmy
            '004': '13175051890',
            'unitName': '杭州店'
        }
    },
};

