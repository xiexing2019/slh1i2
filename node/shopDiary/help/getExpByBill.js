'use strict';
const common = require('../../lib/common');
const format = require('../../data/format');
const sspdBasicJson = require('./json/basicJson');

/**
 * 根据单据详细信息获取期望值
 */
let getExpByBill = module.exports = {};

//采购

/**
 * 获取采购单详情期望值
 * @param {Object} sfRes
 */
getExpByBill.getPurBillFullExp = (sfRes) => {
	let exp = _.cloneDeep(sfRes.params.jsonParam);
	if (exp.isPend == 1) {
		exp.pendId = sfRes.result.data.val;
	};
	return exp;
};


/**
 * 获取采购单列表期望值
 * @param {Object} qfRes.result.data
 *
 */
getExpByBill.getPurBillListExp = (obj) => {
	//main
	const mapfield = 'compId;otherCost;relBillId;shopId;invId;billNo;purNum;finIoId;createdDate;belongCompId;printFlag;backMoney;fileId;flag;createdBy;printCount;ownerId;srcType;id;backNum;purMoney;deliverFlag;returnFlag;relBillNo;favorMoney;totalNum;proDate;rem;totalMoney;updatedBy;updatedDate;ecCaption;suppName=ecCaption.compId';
	let exp = format.dataFormat(obj.main, mapfield);
	exp = Object.assign(exp, format.dataFormat(obj, 'payMoney=fin.payMoney;payBalance=fin.verifyMoney'));
	if (!exp.updatedBy) {
		exp.updatedBy = exp.createdBy;
		exp.updatedDate = exp.createdDate;
	};

	return exp;
};

/**
 * 进货单挂单列表期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getPurPendListExp = (obj) => {
	//main
	const mapfield = 'compId;shopId;purNum;createdDate;flag;createdBy;ownerId;id;backNum;totalNum;proDate;rem;totalMoney;updatedBy;updatedDate;ecCaption';
	let exp = format.dataFormat(obj.main, mapfield);
	exp = Object.assign(exp, format.dataFormat(obj, 'balance=fin.balance'));
	exp.invId = exp.shopId;
	exp.payMoney = 0;
	exp.typeId = 1300;
	exp.proDate = exp.proDate.split(" ").shift();
	if (!exp.updatedBy) {
		exp.updatedBy = exp.createdBy;
		exp.updatedDate = exp.createdDate;
	};
	return exp;
};

/**
 * 建款同时入库 采购单详情期望值
 * @param {object} obj sfRes新增货品参数
 * 
 */
getExpByBill.getInitPurBillInfoExp = (sfRes) => {
	let shareParam = {
		belongCompId: sfRes.params.jsonParam.purprices[0].orgSuppId,
		compId: sfRes.params.jsonParam.purprices[0].orgSuppId,
		flag: 1,
		ownerId: LOGINDATA.userId,
		unitId: LOGINDATA.unitId,
		shopId: LOGINDATA.shopId,
		proDate: sfRes.params.jsonParam.spuCommonDto.marketDate,
	};
	let totalNum = 0, totalMoney = 0, exp = { isPend: 0, details: [] };
	sfRes.params.jsonParam.spuStockNum.forEach((ele) => {
		exp.details.push(Object.assign({
			price: sfRes.params.jsonParam.purprices[0].purPrice,
			dresName: sfRes.params.jsonParam.spuCommonDto.name,
			dresCode: sfRes.params.jsonParam.spuCommonDto.code,
			tenantSpuId: sfRes.result.data.tenantSpuId,
			createdBy: LOGINDATA.userId,
			unitId: LOGINDATA.unitId,
			money: common.mul(ele.stockNum, sfRes.params.jsonParam.purprices[0].purPrice),
		}, format.dataFormat(ele, 'colorId;sizeId;num=stockNum;deliverNum=stockNum')));
		// exp.details[index] = 
		totalNum = common.add(ele.stockNum, totalNum);
	});

	totalMoney = common.mul(totalNum, sfRes.params.jsonParam.purprices[0].purPrice)
	exp.main = Object.assign({}, shareParam, {
		srcType: 7,//建款同时入库	
		totalNum: totalNum,
		printFlag: 0,
		deliverFlag: 1,
		purNum: totalNum,
		subType: 1301,
		totalMoney: totalMoney,
		rem: '',
	});
	exp.fin = Object.assign({}, shareParam, {
		typeId: 1300,
		payMoney: 0,
		balance: -totalMoney,
		balanceType: 1,//未付
	});
	return exp;
};

//销售

/**
 * 销售单列表期望值
 * 
 */
getExpByBill.getSalesBillListExp = (obj) => {
	const mapfield = 'compId;score;otherCost;relBillId;deliverBillId;finIoId;shopId;billNo;mainDiscount;invId;createdDate;accode;belongCompId;printFlag;salesNum;priceType;backMoney;salesMoney;fileId;flag;hashKey;createdBy;returnFlag;unitId;printCount;ownerId;srcType;id;backNum;branchId;deliverFlag;relBillNo;favorMoney;restNum;totalNum;proDate;rem;logisBillId;totalMoney;deliverNum;ecCaption;customerName=ecCaption.compId';
	let exp = Object.assign(format.dataFormat(obj.main, mapfield), format.dataFormat(obj.fin, 'payMoney;updatedDate;updatedBy'));
	if (!obj.hasOwnProperty('payways')) return exp;

	// onlinePayFlag = ['支付失败', '支付中', '支付成功'];
	// payFlag -1:失败,0:支付中,1:支付成功
	// tallyType  1 为记账，2为在线支付
	const finIOPay = {};
	_.forEach(sspdBasicJson.payType, (value) => {
		if (value.hasOwnProperty('finIOPay')) {
			finIOPay[value.id] = { name: value.finIOPay, money: 0 };
		};
	});
	obj.payways.forEach(ele => {
		if (ele.tallyType == 2) {
			//在线支付
			exp.onlinePayType = ele.typeId > sspdBasicJson.payType.other.id ? common.sub(ele.typeId, sspdBasicJson.payType.other.id) : ele.typeId;
			exp.onlinePayFlag = ele.payFlag;
			exp.onlinePayMoney = ele.payFlag == -1 ? 0 : ele.money;
			finIOPay[sspdBasicJson.payType.other.id].money = ele.payFlag != 1 ? 0 : ele.money; //在线支付
		} else {
			//记账
			finIOPay[ele.typeId].money = common.add(finIOPay[ele.typeId].money, ele.money);
		};
	});
	_.forEach(finIOPay, (value) => exp[value.name] = value.money);
	return exp;
};

/**
 * 销售单挂单列表期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getSalesPendListExp = (obj) => {
	const mapfield = 'compId;ownerId;id;backNum;shopId;flag;createdDate;favorMoney;salesNum;totalNum;proDate;createdBy;rem;totalMoney;unitId;customerName=ecCaption.compId;ecCaption';
	let exp = format.dataFormat(obj.main, mapfield);
	exp = Object.assign(exp, format.dataFormat(obj.fin, 'balance'));
	exp.payMoney = 0;
	exp.typeId = 1112;
	if (!exp.updatedBy) {
		exp.updatedBy = exp.createdBy;
		exp.updatedDate = exp.createdDate;
	};
	exp.branchId = LOGINDATA.branchId;
	if (obj.isPend == 1 && LOGINDATA.productVersion <= 1) exp.flag = 1;
	exp.ecCaption.typeId = '销售开单';
	exp.ecCaption.updatedBy = exp.ecCaption.createdBy;
	exp.proDate = exp.proDate.split(" ").shift();
	return exp;
};

/**
 * 销售订单列表期望值
 * 
 */
getExpByBill.getSalesOrderBillListExp = (obj) => {
	const mapfield = 'compId;score;otherCost;relBillId;deliverBillId;finIoId;shopId;billNo;mainDiscount;invId;createdDate;accode;belongCompId;printFlag;salesNum;priceType;backMoney;salesMoney;fileId;flag;hashKey;createdBy;returnFlag;unitId;printCount;ownerId;srcType;id;backNum;branchId;deliverFlag;relBillNo;favorMoney;restNum;totalNum;proDate;rem;logisBillId;totalMoney;deliverNum;ecCaption;customerName=ecCaption.compId';
	let exp = Object.assign(format.dataFormat(obj.main, mapfield), format.dataFormat(obj.fin, 'payMoney;updatedDate;updatedBy'));
	exp.restNum = exp.totalNum - exp.deliverNum;
	if (!obj.hasOwnProperty('payways')) return exp;

	// onlinePayFlag = ['支付失败', '支付中', '支付成功'];
	// payFlag -1:失败,0:支付中,1:支付成功
	// tallyType  1 为记账，2为在线支付
	const finIOPay = {};
	_.forEach(sspdBasicJson.payType, (value) => {
		if (value.hasOwnProperty('finIOPay')) {
			finIOPay[value.id] = { name: value.finIOPay, money: 0 };
		};
	});
	obj.payways.forEach(ele => {
		if (ele.tallyType == 2) {
			//在线支付
			exp.onlinePayType = ele.typeId > sspdBasicJson.payType.other.id ? common.sub(ele.typeId, sspdBasicJson.payType.other.id) : ele.typeId;
			exp.onlinePayFlag = ele.payFlag;
			exp.onlinePayMoney = ele.payFlag == -1 ? 0 : ele.money;
			finIOPay[sspdBasicJson.payType.other.id].money = ele.payFlag != 1 ? 0 : ele.money; //在线支付
		} else {
			//记账
			finIOPay[ele.typeId].money = common.add(finIOPay[ele.typeId].money, ele.money);
		};
	});
	_.forEach(finIOPay, (value) => exp[value.name] = value.money);
	return exp;
};

/**
 * 拼接销售单按明细查期望值
 * @param {Object} qfRes 销售单明细
 * @param {Object} invSku 库存
 */
getExpByBill.getSalesDetListExp = function ({ qfRes, invSku }) {
	let exp = [];
	let data = format.dataFormat(qfRes.main, 'proDate;billNo;shopId;invId;ownerId;compId;remark=rem');
	qfRes.details.forEach((detail) => {
		exp.push(Object.assign({}, {
			invNum: invSku[`${detail.tenantSpuId}_${detail.colorId}_${detail.sizeId}`].stockNum,// + invSku[`${detail.tenantSpuId}_${detail.colorId}_${detail.sizeId}`].orderNum
		}, data, format.dataFormat(detail, 'dresCode;num;discount;updatedDate;tenantSpuId;price;id;rem;deliverNum;stdPriceType;dresName;typeId;specialFlag;realPrice;sizeId;colorId;createdDate;money;createdBy;tenantSkuId;classId=dresBranchSpu.classId;unit')));
	});
	return exp;
};

//盘点

/**
 * 盘点单列表期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getCheckListExp = (obj) => {
	const mapfield = 'hashKey;updatedDate;updatedBy;id;totalNum;profitLose;flag;createdBy;deliver;branchId;billNo;shopId;ecCaption;rem;invId;bizDate;createdDate;totalMoney;unitId;accode'
	let exp = format.dataFormat(obj, mapfield);
	exp.beforeAdjustNum = 0;
	exp.commitNum = 0;
	for (let i = 0; i < obj.billDetails.length; i++) {
		exp.beforeAdjustNum += obj.billDetails[i].beforeAdjustNum;
		exp.commitNum += obj.billDetails[i].commitNum;
	};
	return exp;
};


//货品

//获取库存期望值
getExpByBill.getInvSkuExpByStyleId = (obj) => {
	let exp = {},
		invKey;
	obj.details.forEach(element => {
		invKey = `${element.tenantSpuId}_${element.colorId}_${element.sizeId}`;
		exp[invKey] = {
			invKey: invKey,
			movingNum: 0, //在途数 笑铺没调拨，后续再补
			stockNum: obj.main.subType == 1112 ? -element.num : element.num, //库存数,销售单时库存减少
		};
	});
	return exp;
};

//获取库存期望值（调拨）
getExpByBill.getMoveInvExpByStyleId = (obj) => {
	let exp = {},
		invKey;
	obj.billDetails.forEach(element => {
		invKey = `${element.tenantSpuId}_${element.colorId}_${element.sizeId}`;
		exp[invKey] = {
			invKey: invKey,
			movingNum: 0, //在途数 笑铺没调拨，后续再补
			stockNum: obj.inOutType == 1 ? -element.commitNum : element.commitNum, //库存数,调出单时库存减少
		};
	});
	return exp;
};

//获取库存期望值(订货)
getExpByBill.getOrderSkuExpByStyleId = (obj) => {
	let exp = {},
		invKey;
	obj.details.forEach(element => {
		invKey = `${element.tenantSpuId}_${element.colorId}_${element.sizeId}`;
		exp[invKey] = {
			invKey: invKey,
			movingNum: 0, //在途数 笑铺没调拨，后续再补
			stockNum: obj.main.subType == 1112 ? -element.num : element.num, //库存数,销售单时库存减少
			//orderNum: element.num //订货数,开销售订单时增加,发货时还原
		};
	});
	return exp;
};

//库存流水期望值
/**
 * @param {Object} obj qf.result.data
 * @param {Object} 
 */
getExpByBill.getInvFlowListExp = (obj) => {
	let flowListExp = {};
	let exp = format.dataFormat(obj.main, 'bizDate=proDate;billId=id;unitId;shopId;billNo');//rem有问题
	exp.bizType = obj.main.subType == 1301 ? 1300 : obj.main.subType;
	exp.bizTypeCaption = obj.fin.ecCaption.typeId;
	obj.details.forEach(ele => {
		let detailInOutType;
		if ((exp.bizType == 1112 && ele.num >= 0) || (exp.bizType == 1300 && ele.num < 0)) {
			detailInOutType = 1;
		} else { detailInOutType = 2; };

		if (!flowListExp[ele.tenantSpuId]) {
			// console.log(`\nexp1=${JSON.stringify(exp)}`);
			flowListExp[ele.tenantSpuId] = Object.assign({}, exp, {
				tenantSpuId: ele.tenantSpuId,
				code: ele.dresCode,
				name: ele.dresName,
				addNum: 0,//重新计算 
				skuFlows: [],
			});
		};
		flowListExp[ele.tenantSpuId].skuFlows.unshift({
			colorId: ele.colorId,
			sizeId: ele.sizeId,
			num: Math.abs(ele.num),
			inOutType: detailInOutType,//1 出库  2 入库
		});
		flowListExp[ele.tenantSpuId].addNum = exp.bizType == 1112 ? common.sub(flowListExp[ele.tenantSpuId].addNum, ele.num) : common.add(flowListExp[ele.tenantSpuId].addNum, ele.num);
	});
	for (let value of Object.values(flowListExp)) {
		if (value.addNum == 0) {
			value.inOutType = value.bizType == 1112 ? 1 : 2;
		} else {
			value.inOutType = value.addNum > 0 ? 2 : 1;
		};
	};
	return flowListExp;
};



//账款积分

/**
 * 获取积分，余额期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getBalanceAndScoreExp = (obj) => {
	return {
		//供应商没有积分
		score: obj.fin.compKind == 1 ? 0 : Math.ceil(common.div(obj.main.totalMoney, 50) * 1000) / 1000,
		balance: obj.fin.balance
	};
};

/**
 * 供应商对账单期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getSuppSimpleAcctExp = (obj) => {
	let exp = {};
	if (obj.main) {
		const mapfield = 'id;billNo;favorMoney;totalMoney;bizType=subType';
		exp = format.dataFormat(obj.main, mapfield);
		exp = Object.assign(exp, format.dataFormat(obj, 'balance=fin.balance;payMoney=fin.payMoney;lastBalance=fin.lastBalance;createdDate=fin.createdDate'));
		exp.ecCaption = {};
		exp.bizType == 1301 ? exp.ecCaption.bizType = "进货" : exp.ecCaption.bizType = "销售";
		//	console.log(`exp : ${JSON.stringify(exp)}`);
	} else {
		exp = format.dataFormat(obj, 'totalMoney=0;favorMoney=0;proDate;createdDate;payMoney;balance=payMoney;id;billNo');
		exp.bizType = '5102';//付款单
	}
	return exp;
};


/**
 * 客户对账单期望值
 *
 */
getExpByBill.getCustSimpleAcctExp = (obj) => {
	//rechargeMoney指充值金额  giftsMoney指赠与金额，先不验
	const mapfield = 'id;billNo;totalMoney;createdDate;favorMoney;rechargeMoney;giftsMoney;proDate';
	let exp = format.dataFormat(obj.main, mapfield);
	exp = Object.assign(exp, format.dataFormat(obj.fin, 'payMoney;lastBalance;balance'));
	exp.bizType = obj.fin.typeId == '1112' ? 1101 : obj.fin.typeId; //固定为1101
	exp.ecCaption = {};
	exp.ecCaption.bizType = obj.fin.typeId == '1112' ? '销售' : obj.fin.ecCaption.typeId;
	//7100 充值单 1103收款单totalMoney为0
	exp.totalMoney = [7100, 1103].includes(obj.fin.typeId) ? 0 : obj.main.totalMoney;
	if (obj.hasOwnProperty('payways')) {
		obj.payways.forEach(ele => {
			if (ele.tallyType == 2) {
				exp.onlinePayType = ele.typeId;
				exp.onlinePayFlag = ele.payFlag;
				//在线支付失败
				exp.onlinePayMoney = ele.payFlag == -1 ? 0 : ele.money;
			};
		});
	};
	return exp;
};

/**
 * 拼接客户对账单汇总期望值
 * @param {object} acctBef 
 * @param {object} qfRes 
 */
getExpByBill.getCustAcctSumExp = (acctBef, qfRes) => {
	let exp = {
		total: common.add(acctBef.result.data.total, 1),
		initBalance: acctBef.result.data.initBalance || 0,// 0,//客户初始余额
		beginMoney: acctBef.result.data.beginMoney,//common.sub(acctBef.result.data.rows[acctBef.result.data.rows.length - 1].lastBalance, acctBef.result.data.rows[acctBef.result.data.rows.length - 1].balance),
		endMoney: common.add(acctBef.result.data.endMoney, qfRes.result.data.fin.balance),
		sum: {
			payMoney: common.add(qfRes.result.data.fin.payMoney, acctBef.result.data.sum.payMoney),
			totalMoney: common.add(qfRes.result.data.main.totalMoney, acctBef.result.data.sum.totalMoney),
			rechargeMoney: acctBef.result.data.sum.rechargeMoney || 0,//余额充值
		},
	};
	//修改单据的支付状态
	if (acctBef.result.data.rows[0].id == qfRes.result.data.main.id) {
		exp.total = acctBef.result.data.total;
		exp.endMoney = common.add(acctBef.result.data.endMoney, qfRes.result.data.fin.payMoney);
		exp.sum.totalMoney = acctBef.result.data.sum.totalMoney;
	};

	return exp;
};

/**
 * 拼接余额充值列表期望值
 */
getExpByBill.getRechargeBillListExp = (obj) => {

	let exp = Object.assign({ payways: obj.payways }, format.dataFormat(obj.main, 'compId;createdBy;createdDate;giftsMoney;billNo;shopId;rem;id;finIoId;ownerId;proDate;flag;rechargeMoney'));
	exp = Object.assign({}, exp, format.dataFormat(obj.fin, 'balanceAdd=balance;updatedBy;balanceTotal=lastBalance;updatedDate'));
	return exp;
};

//积分

/**
 * 积分明细期望值 ec-score-scoreAcctFlow-listByFlowCust
 * @description score需要根据单据totalMoney与积分规则自行计算,没有拼接
 */
getExpByBill.getCustScoreDetailExp = (obj) => {
	const mapfield = 'billNo;proDate;shopId;ownerId;rem;traderId=compId;ecCaption';
	let exp = format.dataFormat(obj.main, mapfield);
	exp = Object.assign(exp, format.dataFormat(obj, 'relBillNum=fin.relBillNum;relBillMoney=fin.relBillMoney;updatedDate=fin.updatedDate;updatedBy=fin.updatedBy'))
	exp.ecCaption.traderId = exp.ecCaption.compId;
	exp.traderKind = 2;
	return exp;
};

//账户

/**
 * 获取支付账户信息期望值
 * @param {Object} qfRes.result.data
 */
getExpByBill.getAccountListExp = (obj) => {
	let exp = {};
	obj.payways.forEach(element => {
		exp[element.accountId] = {
			//id:element.accountId,
			balMoney: element.money
		};
	});
	return exp;
};

/**
 * 获取支付账户信息期望值2
 * @param {Object} qfRes.result.data
 */
getExpByBill.getAccountListExp2 = (obj) => {
	let exp = {};
	obj.payways.forEach(element => {
		exp[element.accountId] = {
			toOutMoney: element.money
			//balMoney: element.money
		};
	});
	return exp;
};

//账户
/**
 * 账户流水期望值
 * @param {object} obj qfRes.result.data
 */
getExpByBill.getAccountFlowListExp = (obj) => {
	//tradeType flowType qiuxs开发说不管
	let flowExp = {};
	if (obj.main) {
		let exp = format.dataFormat(obj.main, 'proDate;ownerId;billId=id;shopId;billNo;accode');
		exp = Object.assign(exp, format.dataFormat(obj.fin, 'bizType=typeId;finBillId=id'));
		// exp.rem = `单位[${obj.main.ecCaption.compId}]`;"typeId": 1112,
		exp.rem = [1300, 1100, 1112, 1103].includes(obj.fin.typeId) ? `单位[${obj.main.ecCaption.compId}]` : obj.main.rem;
		obj.payways.forEach(data => {
			flowExp[data.accountId] = Object.assign({
				subAcct: data.tallyType == 2 ? 5 : 1,//1 余额 2 已冻结 3 待入 4 待出 5 在线 关注1和5
				payType: data.tallyType == 2 ? common.sub(data.typeId, sspdBasicJson.payType.other.id) : data.typeId,
			}, exp, format.dataFormat(data, 'tallyType;money;acctId=accountId;updatedBy;updatedDate'));
		});
	} else {
		//收款单
		flowExp = format.dataFormat(obj, 'proDate;updatedDate;ownerId;rem;shopId;billNo;updatedBy;acctId=accountId;billId=id;finBillId=finIoId;');
		flowExp = Object.assign({}, {
			bizType: 5102,//采购付款
			bizTypeCat: 5100,//收付款单
			tallyType: 1,//记账  付款单没有在线支付
			subAcct: 1,//余额  5代表在线支付
			money: -obj.payMoney,
		}, flowExp);
	};
	return flowExp;
}

//

/**
 * area-diary期望值拼接
 * @param {Object} qfRes.result.data//新增款号
 */
getExpByBill.getAreaDiary = (obj) => {
	let exp = {};
	exp.addNum = 0;
	for (let i = 0; i < obj.spuStockNum.length; i++) {
		exp.addNum += obj.spuStockNum[i].stockNum;
	};
	exp.purBillNum = 1;
	exp.purMoney = common.mul(exp.addNum, obj.purprices[0].purPrice);
	exp.purNum = exp.addNum;
	exp.stockNum = exp.addNum;
	exp.stockMoney = common.mul(exp.addNum, obj.purprices[0].purPrice);
	return exp;
}


/**
 * trend-diary期望值拼接
 * @param {Object} qfRes.result.data//新增款号
 */
getExpByBill.getTrendDiary = (obj) => {
	//let exp
}

/**
 * 标签打印配置期望值 ec-config-print-barcode-getPrintBarcodeConfig
 * @description printConfig没有拼接
 */
getExpByBill.getPrintConfigExp = (obj) => {
	const mapfield = 'name;width;shopId;high;leftSize;printCount;printCountByStockNum';
	let exp = format.dataFormat(obj, mapfield);
	return exp;
};

/**
 * 要货单列表期望值 ec-inv-askBill-listBill
 * @description
 */
getExpByBill.getAskBillListExp = (obj) => {
	const mapfield = 'flag;auditBy;deliver;auditFlag;id;shopId;deliverNum;billNo;branchId;doneFlag;needAudit;totalMoney;totalNum;targetShopId;createdBy';
	let exp = format.dataFormat(obj, mapfield);
	return exp;
};