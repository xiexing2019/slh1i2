const common = require('../../lib/common');
const caps = require('../../data/caps');
const format = require('../../data/format');
const slh2 = require('../../reqHandler/slh2');
const sspd = require('../../reqHandler/shopDiary');
const loginReq = require('../help/loginReq');

class Staff {
    constructor() {
        /**  员工ID  */
        this.id = '';
        /**  单元id  */
        this.unitId = '';
        /**  姓名  */
        this.userName = '';
        /**  手机号  */
        this.mobile = '';
        /**  授权手机号  */
        this.phone = '';
        /**  门店/部门id  */
        this.depId = '';
        /**  角色  */
        this.roleIds = '';
        /**  启用停用标志  */
        this.flag = 1;
        /**  普通授权标志  */
        this.authFlag = 0;
        /**  特殊授权标志  */
        this.speFlag = 0;
        /**  备注  */
        this.rem = '';
    }

    /**
     * 新建员工
     * @param {object} params 
     */
    async saveStaff(params = {}) {
        const saveStaff = await slh2.mdm.saveStaff(params);
        //console.log(saveStaff);
        this.userName = params.userName;
        this.mobile = params.mobile;
        this.rem = params.rem;
        this.roleIds = params.roleIds;
        this.depId = params.depId;
        this.unitId = LOGINDATA.unitId;
        !this.id && (this.id = saveStaff.result.data.val);
        await common.delay(3000);
    }

    /**
     * 查询员工列表
     * @param {object} params 
     */
    async getStaffList(params = {}) {
        const staff = await slh2.mdm.getStaffList(params).then(res => res.result.data.rows.find(obj => obj.code == this.phone));
        return staff;
    }

    /**
     * 查询员工(笑铺不用)
     * @param {object} params 
     */
    async getStaffInfo() {
        const staffInfo = await slh2.mdm.getStaffInfo({}).then(res => res.result.data);
        return staffInfo;
    }

    /**
     * 停用
     */
    async disableStaff() {
        await slh2.mdm.disableStaff({ id: this.id });
        this.flag = 0;
    }

    /**
     * 启用
     */
    async enableStaff() {
        await slh2.mdm.enableStaff({ id: this.id });
        this.flag = 1;
    }

    /**
     * 普通授权
     * @param {object} params 
     */
    async authorizeStaff(params) {
        await slh2.mdm.authorizeStaff({ userId: this.userId, roleId: this.roleId, ...params });
        this.authFlag = 1;
    }

    /**
     * 取消普通授权
     * @param {object} params 
     */
    async unauthorizeStaff(params) {
        await slh2.mdm.unauthorizeStaff({ userId: this.userId, roleId: this.roleId, ...params });
        this.authFlag = 0;
    }

    /**
     * 特殊授权
     * @param {object} params 
     */
    async authorizeSpeStaff(params = {}) {
        await slh2.mdm.authorizeSpeStaff({ staffId: this.id, ...params });
        this.phone = params.authPhone;
        this.authFlag = 1;
        this.speFlag = 1;
        await common.delay(2000);
    }

    /**
     * 取消特殊授权
     * @param {object} params 
     */
    async unathorizeSpeStaff(params = {}) {
        await slh2.mdm.unathorizeSpeStaff({ staffId: this.id, authPhone: this.phone, ...params });
        this.speFlag = 0;
    }

    /**
     * 当前租户下所有已特殊授权员工列表，按门店(账套)分组 
     */
    async getSpeAuthedStaffList() {
        return slh2.mdm.getSpeAuthedStaffList();
    }

    /**
     * 获取员工特殊授权信息
     * @param {object} params 
     */
    async getSpeAuthedStaffInfo(params) {
        return slh2.mdm.getSpeAuthedStaffInfo({ staffId: this.id, authPhone: this.phone, ...params });
    }

    /**
     * 获取员工详情
     */
    getStaff() {
        return {
            id: this.id,
            unitId: this.unitId,
            userName: this.userName,
            phone: this.phone,
            roleIds: this.roleIds,
            rem: this.rem,
            flag: this.flag,
            authFlag: this.authFlag,
            speFlag: this.speFlag
        }
    }
};

const staffManage = module.exports = {
    Staff
};

staffManage.setupStaff = function () {
    return new Staff();
};