'use strict';
const common = require('../../lib/common');
const mainReqHandler = require('../../reqHandler/shopDiary/mainReqHandler');
const loginReq = require('../help/loginReq');

(async function () {
    const mobile = '15879983333';
    await loginReq.syncUnitId(mobile);
    await loginReq.sspdLogin({
        mobile
    });

    await dresClass(['登山服']);



})();

async function dresClass(classList) {
    let [newList, enableList] = [[], []];
    const promises = classList.map(name => {
        return common.apiDo({
            apiKey: 'ec-dres-spu-class-getByName',
            jsonParam: {
                name: name,
            },
            check: false,
        });
    });
    const res = await Promise.all(promises);
    res.forEach((obj, index) => {
        if (obj.result.code < 0) {
            newList.push(classList[index]);
        } else if (obj.result.data.flag != 0) {
            enableList.push(obj.result.data.id);
        };
    });

    for (let i = 0; i < newList.length; i++) {
        await mainReqHandler.dresClassSave({
            name: newList[i]
        });
    };

    for (let i = 0; i < enableList.length; i++) {
        await common.apiDo({
            apiKey: 'ec-dres-class-enable',
            id: enableList[i]
        });
    };
};