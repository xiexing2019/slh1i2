const mainReqHandler = require('../../reqHandler/shopDiary/mainReqHandler');
const sspdConfig = require('../../reqHandler/shopDiary/sspdConfig');
const slh2 = require('../../reqHandler/slh2');
const loginReq = require('../help/loginReq');

describe('帐套维护', function () {
    this.timeout(30000);


    it('初始化帐套行业', async function () {
        await loginReq.sspdLoginBySeq();

        //判断是否设定了行业
        if (LOGINDATA.extProps.commonIndustry == 0) {
            const res = await sspdConfig.sysDictTree({ codeId: 0 });
            const industryId = res.result.data.options.find((data) => data.value = '服装箱包').code;
            await slh2.config.transUpdateIndustry({ industryId });
        };
    });

});
