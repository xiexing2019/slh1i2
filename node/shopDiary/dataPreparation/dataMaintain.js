const common = require('../../lib/common');
const basicJson = require('../../shopDiary/help/json/basicJson');
const configReqHandler = require('../../reqHandler/slh2/config');
const billReqHandler = require('../../reqHandler/shopDiary/bill');
const slh2 = require('../../reqHandler/slh2');
const sspd = require('../../../reqHandler/shopDiary');
const loginReq = require('../help/loginReq');

const sspd = require('../../../reqHandler/shopDiary');
(async () => {
    // this.timeout(30000);
    const mobile = '15488888888';
    const unitId = '27265';
    await loginReq.sspdLogin({ mobile, unitId });
    await loginReq.changeShopFast({ shopName: '1' });
    // let colorIds, sizeIds, style, cust, supplier;
    //新增三个衣服尺码-xs，2xs,3xs
    let codeName = ['xs', '2xs', '3xs'];
    await saveDictMaintain({ codeName, typeId: 605 });
    //新增尺码
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 601 }));
    //新增颜色
    await common.delay(500);
    await slh2.dres.saveClass();
    //新增分类
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 606 }));
    //新增品牌
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 637 }));
    //新增材质
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 835 }));
    //新增风格
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 752 }));
    //新增执行标准
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 753 }));
    //新增安全类别
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 754 }));
    //新增等级
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 846 }));
    //新增检验员
    await common.delay(500);
    await slh2.config.saveDict(basicJson.dictJson({ typeId: 845 }));
    //新增产地
    await common.delay(500);
    let updatePriceBefore = await configReqHandler.getDictList({ typeId: 402, unitId: LOGINDATA.unitId }).then(res => res.result.data.rows.find((obj) => (obj.codeValue == 3)));
    //console.log(`updatePriceBefore=${JSON.stringify(updatePriceBefore)}`);
    let discount = common.getRandomNum(1, 1000, 1);
    updatePriceRes = await slh2.config.saveDict({
        jsonParam: Object.assign(_.cloneDeep(updatePriceBefore), {
            codeName: `修改价格${common.getRandomStr(6)}`,
            props: { isShow: 1, discount: discount },
            discount,
        }),
    });
    //修改价格类型
    console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    let dictIds = await slh2.sync.syncData({ type: 'sc_dict_size' }).then(res => res.result.data.rows);
    // console.log(`dictIds=${JSON.stringify(dictIds)}`);
    let sizeIds = dictIds.slice(1, 8).map(info => info.codeValue);
    // console.log(`sizeIds=${JSON.stringify(sizeIds)}`);
    dictIds = await slh2.sync.syncData({ type: 'sc_dict_color' }).then(res => res.result.data.rows);
    let colorIds = dictIds.slice(1, 21).map(info => info.codeValue);
    let custid, suppid;
    await slh2.mdm.addCust({ jsonParam: basicJson.addCustJson({ userName: '小王' }) }).then(res => {
        custid = res.result.data.val;
        console.log('客户新增完毕')
    });
    //新增客户
    await slh2.mdm.addSupplier(basicJson.addSupplierJson({ name: 'Vell' })).then(res => {
        suppid = res.result.data.val;
        console.log('供应商新增完毕');
    });
    //新增供应商
    let styleJson = basicJson.addGoodJson({ code: 'agc001', name: 'auto001', dwid: suppid, colorIds, sizeIds });
    let dresRes = await slh2.dres.saveStyle(styleJson);
    //新增货品
    let styleInfo = await slh2.dres.getStyleInfo({ id: dresRes.result.data.tenantSpuId }).then((res) => res.result.data);
    //获取货品详情
    let params = {
        userName: common.getRandomStr(6),
        mobile: '12' + common.getRandomNumStr(9),
        roleName: '店长',
        rem: '新增员工店长',
    }
    await loginReq.staffSave(params);
    //新增员工
    let myAccountList = await slh2.fin.getAccountList().then(res => res.result.data.rows.find((obj) => (obj.nameAbbr == "现")));
    //获取账户
    const salesBillJson = basicJson.salesJson({ styleInfo: styleInfo, dwid: custid, details: [{ num: 5 }, { num: 10 }], typeId: 1, accountId: myAccountList.id });
    await sspd.salesBill.saveSalesBill({ jsonParam: salesBillJson });
    await common.delay(500);
    //开销售单
    const purBillJson = basicJson.purJson({ styleInfo: styleInfo, dwid: suppid, details: [{ num: 5 }, { num: 10 }], typeId: 1, accountId: myAccountList.id });
    sfRes = await billReqHandler.savePurchaseBill({
        jsonParam: purBillJson
    });
    await common.delay(500);
    //开采购单
    //获取可用门店列表
    let shopInfoArr = await slh2.mdm.getShopListByDevice().then(res => res.result.data.rows);
    let shopNameArr = shopInfoArr.map(info => info.name);

    console.log(`shopNameArr=${JSON.stringify(shopNameArr)}`);
})();
/**
 * 维护字典类数据
 * @param {Arr} params 
 */
async function saveDictMaintain({ codeName = [], typeId }) {
    let dictIds = await slh2.sync.syncData({ type: 'sc_dict_size' });
    //console.log(`dictIds=${JSON.stringify(dictIds)}`);
    console.log(`codeName=${JSON.stringify(codeName)}`);
    for (const value of codeName) {
        console.log(`value=${JSON.stringify(value)}`);
        let act = dictIds.result.data.rows.find(obj => obj.name = value);
        console.log(`act=${JSON.stringify(act)}`);
        // if (act) { continue; }
        let res = await slh2.config.saveDict(basicJson.dictJson({ codeName: value, typeId }));
        console.log(`res=${JSON.stringify(res)}`);
    };

};