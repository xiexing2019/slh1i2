const caps = require('../data/caps');
const common = require('../lib/common');
const Mochawesome = require('mochawesome');
const mail = require('../lib/mail');


class HtmlReport extends Mochawesome {
    constructor(runner, options) {
        super(runner, options);

        const envName = {
            'app1': '审核',
            'abz1': '线上',
            'yans1': '演示'
        }[caps.name];
        if (!envName) throw new Error(`环境变量'${caps.name}'错误,请检查`);
        const isOnline = envName == '线上';

        let passNum = 0, failNum = 0;

        runner.on('suite', function (suite) {
            if (isOnline && suite.title.includes('offline')) {
                suite.pending = true;
            }
        });

        // runner.on('hook', function (hook) {
        //     // console.log(hook);
        // });

        runner.on('test', function (test) {
            if (isOnline && test.title.includes('offline')) {
                test.pending = true;
            }
        });

        runner.on('pass', function (test) {
            passNum++;
            // console.log(test.titlePath().join('-'));
        });

        runner.on('fail', function (test, err) {
            failNum++;
            const errorMsg = `${test.titlePath().join('-')}\nError: ${err.message}`;
            console.log(errorMsg);
        });

        runner.on('end', async function () {
            /** 邮件主题 */
            const subject = `【商陆花一代${common.getCurrentDate()}${envName}环境】自动化用例报告`;
            /** 收件人 */
            // const addressee = ['叶建兵<yejianbing1303@dingtalk.com>'];
            const addressee = ['欧工<qq272963@dingtalk.com>', '舒琼<toshuqiong@dingtalk.com>',
                '陆星欣<luxxhz@dingtalk.com>', '汪凯阳<wangkaiyang@hzecool.com>',
                '付琛<fuchen4313@dingtalk.com>', '叶建兵<yejianbing1303@dingtalk.com>'];
            /** 内容 */
            const text = `自动化用例问题，自动化人员要第一时间定位。如果是开发问题要及时指派解决。\n每日要做到百分百通过`;

            // await mail.sendMail(addressee.join(','), { subject: subject, text: text, html: `<table border=0 cellspacing=10><tr><th>总用例数</th><th>通过</th><th>失败</th></tr><tr><td>${passNum + failNum}</td><td>${passNum}</td><td>${failNum}</td></tr></table><a href="http://test.hzdlsoft.com:4459/auto_test/slh1_new/mochawesome.${common.getCurrentDate().replace(/-/g, '')}.html">日志链接</a>` });
            await mail.sendMail(addressee.join(','), { subject: subject, text: text, html: `<table border=0 cellspacing=10><tr><th>总用例数</th><th>通过</th><th>失败</th></tr><tr><td>${passNum + failNum}</td><td>${passNum}</td><td>${failNum}</td></tr></table><a href="http://192.168.0.23:8081/jenkins/job/autotest/job/auto_test/result">日志链接</a>` });
            await common.delay(2000);
            process.exitCode = 0;
            console.log(`执行完成`);
        });

    }
}


module.exports = HtmlReport;
