const name = require('../../data/caps').name;

/**
 * 
 * confc.ec_staff
 */
const account = {
    sp_dev: {
        seller: { mobile: '13750850013', shopName: '文一店', market: 'auto市场' },
        seller2: { mobile: '13750850013', shopName: '中洲店', market: 'auto市场' },
        client: { mobile: '15988413448' },
        // slhConSp: { adev1: { epid: 15545, shopName: '花木牛仔屋' }, app1: { epid: 15721, shopName: '小衣柜' } },
        ecStaff: { code: '000', pass: '000000' },
        wmsStaff: { code: 'autotest', pass: '000000' },
        wareHouse: { name: 'auto仓库', id: 91351 }
    },
    sp_test: {
        // seller: { mobile: '17764509961', shopName: '岂曰无衣', market: 'auto市场' },
        seller: { mobile: '13750850013', shopName: '常青店', market: 'auto市场' },
        seller2: { mobile: '13750850013', shopName: '中洲店', market: 'auto市场' },
        // 认证用户
        client: { mobile: '15988413448' },  //12201811290
        // 未认证用户
        // client: { mobile: '15988413448' },
        // epid: { adev1: 15545, app1: 15721 },  // epid: 15485 
        slhConSp: { adev1: { epid: 15545, shopName: '花木牛仔屋' }, app1: { epid: 15721, shopName: '小衣柜', } },
        ecStaff: { code: '000', pass: '000000' },
        ecStaff2: { code: 'disStaffA', pass: '000000' },
        wmsStaff: { code: 'lxxx', pass: '000000' },
        wareHouse: { name: '自动化仓库', id: 83430 },
        // 流量入口 sp_source_entry
        sourceEntry: {
            autoTest: {
                sourceId: 28,
                sourceType: 1,
                spDis: { mobile: '12810000000', id: 86233, name: '分销lxx', tenantCode: '000000000000249' },
                levelOne: { mobile: '12903147389', id: 86613, channelCode: '00000078757' }
            },
        },
    },
    sp_chk: {
        seller: { mobile: '13750850013', shopName: '常青店', market: 'auto市场' },
        seller2: { mobile: '13750850013', shopName: '中洲店', market: 'auto市场' },
        seller3: { mobile: '13750850013', shopName: '文一店', market: '' },
        client: { mobile: '15988413448' },
        // epid: { adev1: 15545, app1: 15721 },//9541 15721
        slhConSp: { adev1: { epid: 15545, shopName: '花木牛仔屋' }, app1: { epid: 15721, shopName: '小衣柜' } },
        ecStaff: { code: 'autotest', pass: '000000' },
        wmsStaff: { code: 'lxx', pass: '000000' },
        wareHouse: { name: 'auto', whTenantId: 97586 },
        // 流量入口 sp_source_entry
        sourceEntry: {
            autoTest: {
                sourceId: 28,
                sourceType: 1,
                spDis: { mobile: '12810000000', id: 98198, name: '分销lxx', tenantCode: '000000000000482' },
                levelOne: { mobile: '12903145543', id: 305390, channelCode: '00000293858' }
            },
        },
        // sourceEntry: {
        //     autoTest: {
        //         sourceId: 102,
        //         sourceType: 1,
        //         spDis: { mobile: '12904125555', id: 308242, name: '猪猪顺', tenantCode: '000000000000522' },
        //         levelOne: { mobile: '12903145543', id: 305390, channelCode: '00000293858' }
        //     },
        // },
    },
    sp_pre: {
        seller: { mobile: '13750850013', shopName: '常青店', market: 'auto市场' },
        seller2: { mobile: '13750850013', shopName: '中洲店', market: 'auto市场' },
        client: { mobile: '15988413448' },
        epid: { adev1: 15545, app1: 15721 },//9541 15721
        // slhConSp: {},
        ecStaff: { code: '000', pass: '000000' },
        wmsStaff: { code: 'xx', pass: '000000' },
        wareHouse: { name: '星仓', id: 94267 },
        sourceEntry: {
            autoTest: {
                sourceId: 28,
                sourceType: 1,
                spDis: { mobile: '12810000000', id: 95568, name: '分销lxx', tenantCode: '000000000000364' },
                levelOne: { mobile: '15757102016' }
            },
        },
    },
    sp_online: {
        seller: { mobile: '12905140000', shopName: 'Aster' },
        seller2: { mobile: '15879981652', shopName: '西湖店', shopTypeId: 1, shopPlatRatio: 10 },//免租
        // sellerSLH: { shopName: '言匠', tenantId: 1377 },// slh对接sp
        client: { mobile: '13750850013', shopName: '陆星欣' },
        ecStaff: { code: 'testadmin', pass: 'vhPP7KOfQU' },
        // ecStaff: { code: 'gmz66', pass: '000000' },
        sourceEntry: {
            autoTest: {
                sourceId: 28,
                sourceType: 1,
                spDis: { mobile: '12810000000', id: 95568, name: '分销lxx' },
                levelOne: { mobile: '15757102016' }
            },
        },
    },
};



module.exports = account[name];