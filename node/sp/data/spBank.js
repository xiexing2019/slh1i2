
let spBank = module.exports = {};

/**
 *  中信个人账户
 */
spBank.personalAcct = [
    { name: '在市谋', settleAcctNo: '6217730700880774' },//371329198804011179
    { name: '慈增修', settleAcctNo: '6217730700491713' },
    { name: '汝棠皓', settleAcctNo: '6217730700880790' },
];

/**
 * 中信企业账户
 */
spBank.businessAcct = [
    { name: '小夏', settleAcctNo: '8110701013101246362' },
    { name: '小宇', settleAcctNo: '8110701013601246360' },
    { name: '小雷', settleAcctNo: '8110701012901246359' },
];

