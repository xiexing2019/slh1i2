'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const spReq = require('../help/spReq');
const mockJsonParam = require('../help/mockJsonParam');
const spDoc = require('../../reqHandler/sp/doc_server/doc');
const spugr = require('../../reqHandler/sp/global/spugr');
// const mysql = require('mysql2');
const mysql = require('../../reqHandler/sp/mysql');
const spCaps = require('../../reqHandler/sp/spCaps');
const spmdm = require('../../reqHandler/sp/biz_server/spmdm');
const spAct = require('../../reqHandler/sp/global/spAct');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const spdchg = require('../../reqHandler/sp/global/spdchg');
const spMdm = require('../../reqHandler/sp/biz_server/spmdm');
const spConfig = require('../../reqHandler/sp/global/spConfig');
const docManager = require('../../reqHandler/sp/doc_server/doc');
const billManage = require('../help/billManage');
const dresManage = require('../help/dresManage');
const configParam = require('../help/configParamManager');
const dingtalkRobot = require('../help/dingtalkRobot');
const spCoupon = require('../../reqHandler/sp/global/spCoupon');
const spConfb = require('../../reqHandler/sp/biz_server/spConfb');
const wmsWarehouse = require('../../reqHandler/sp/wms/wmsWarehouse');
const wmsStorage = require('../../reqHandler/sp/wms/storage');
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');
const moment = require('moment');
// const zentao = require('../../lib/zentao');
const spAdmin = require('../../reqHandler/sp/global/spAdmin');

describe('自测', function () {
    this.timeout(300000);
    it.skip('注册', async function () {
        const res = await spReq.userRegister({
            mobile: '15879981652',
            userName: 'lxx'
        });
        console.log(res);
    });

    it.skip('删除用户', async function () {
        await spReq.spLogin({ code: '15988413448' });
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        // const res = await spugr.deleteUser();
        // console.log(res);
    });

    it.skip('注册绑定embGuid', async function () {
        await spReq.spLogin({ code: '15988413448' });
        const res = await spmdm.registerUserMessageCid();
        console.log(res);
    });

    it.skip('slh老板登录', async function () {
        await common.loginDo({ epid: '14777' });

        await spugr.slhUserLogin({ token: '1400221154483', code: '000', slhSessionId: LOGINDATA.sessionid, tenantId: 68878 });
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    it.skip('管理员审批店铺请求', async function () {
        await spAuth.staffLogin();
        // const shopAuditsRes = await spCommon.findBuyerShopAudits({ flag: 0, pageSize: 1000 });
        // console.log(`shopAuditsRes=${JSON.stringify(shopAuditsRes.params)}`);
        // const auditInfo = shopAuditsRes.result.data.rows.find(element => !element.shopName.includes('买家店铺'));
        // const auditInfo = common.takeWhile(shopAuditsRes.result.data.rows, (element) => !element.shopName.includes('买家店铺'));
        // console.log(`auditInfo=${JSON.stringify(auditInfo)}`);
        const agreeRes = await spCommon.changeAuditRecord({ shopId: 650, flag: 9 });//auditInfo.auditId
        console.log(`agreeRes=${JSON.stringify(agreeRes)}`);
    });

    it.skip('切换登录', async function () {
        await spReq.spSellerLogin();
        const list = await spugr.findAvailableTenantIds();
        console.log(`list=${JSON.stringify(list)}`);


        // await spReq.spSellerLogin({ shopName: '少年魔法师' });
        await spReq.spClientLogin();
        console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);


        await spReq.spSellerLogin();
        console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    it.skip('', async function () {
        let s = await spReq.loginGoodShopBySlh({ logid: '200' });
        console.log(`s=${JSON.stringify(s)}`);
        console.log(`\n=${JSON.stringify(LOGINDATA)}`);
        let shopInfo = await spugr.getShopDetail();
        console.log(`\nshopInfo=${JSON.stringify(shopInfo)}`);
        let info = await spugr.getUser();
        console.log(`\ninfo=${JSON.stringify(info)}`);
    })

    it.skip('test', async function () {
        // await spReq.spLogin({ code: '15988413448' });
        const res = await spugr.getDictList({ typeId: 2009, flag: 1 });
        console.log(`res=${JSON.stringify(res)}`);
    });

    it.skip('店铺上线', async function () {
        await spReq.spSellerLogin({ shopName: '西湖店' });
        let sellerTenantId = LOGINDATA.tenantId;
        await spAuth.staffLogin();
        //强制上线
        // await spugr.onlineShopById({ id: sellerTenantId, forceFlag: true, autoTestToken: 'autotest', shopTypeId: 1, shopPlatRatio: 10 });

    });

    it.skip('es测试', async function () {
        await spdresb.esBaseSearch({
            query: {
                term: {
                    id: 99458,
                }
            }
        });
        // console.log(`res=${JSON.stringify(res)}`);
    });

    it.skip('添加商品', async function () {
        await spAuth.staffLogin();
        const classId = 1013;
        let classInfo = await spdresb.findByClass({ classId });
        let arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };
        const array = ['中洲店'];//'常青店', '文一店', '文二店', '文三店' , '中洲店'
        for (let index = 0; index < array.length; index++) {
            const shopName = array[index];
            await saveDres({ code: '13750850013', shopName: shopName }, classId);
        }
        // await saveDres({ code: '15879981652', shopName: '酷卡迪童装店' }, classInfo);
    });

    it.skip('禅道', async function () {

        const res = await zentao.testCaseView({ id: '60', zentaosid: 'btls2umhhft3l9moos2gglk6e2' });
        console.log(`res=${JSON.stringify(res)}`);

    });

    it.skip('节假日-支付校验', async function () {
        await spReq.spSellerLogin();
        // await spReq.saveDresFull();
        const sellerTenantId = LOGINDATA.tenantId;
        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);

        await spReq.spClientLogin();
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });
        // console.log(`searchDres=${JSON.stringify(searchDres)}`);

        const style = searchDres.result.data.dresStyleResultList.shift();
        const styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: style.detailUrl });

        const payType = '1048576';// 1048576  4096  2097152
        const array = ['2019-05-04'];
        // const array = ['2019-02-02'];
        for (let index = 0; index < array.length; index++) {
            await common.delay(100);
            const payDate = moment(array[index]).format('YYYY-MM-DD HH:mm:ss');
            const purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            const res = await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, payDate, payType });
            console.log(`\npayDate:${payDate} payType:${payType}`);
            console.log(res.reqUrl);

        }
    });

    it.skip('设置白名单', async function () {
        // await spReq.spSellerLogin({ shopName: '文一店' });
        await spReq.spClientLogin();
        const sellerInfo = LOGINDATA;

        await spAuth.staffLogin();
        //shadowFlag 0:正常 1:白名单
        await spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 0, tenantType: 0 });

    });

    it.skip('商品全量同步到es', async function () {
        await spReq.spSellerLogin();
        console.log(LOGINDATA.unitId);

        await spdresb.exportEs({ unitId: LOGINDATA.unitId });
    });

    it.skip('检查频道商品', async function () {
        await spAuth.staffLogin();
        const dresList = await spAct.getActChannelSearchDres({ channelId: 2974 });
        console.log(`dresList=${JSON.stringify(dresList)}`);

    });

    it.skip('', async function () {
        // await spReq.spSellerLogin();
        // await spAuth.staffLogin();
        // const onSeason = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_default_onseason_code' });
        // const offSeason = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_default_offseason_code' });
        // console.log(onSeason);
        // console.log(offSeason);
        // const seasonList = await spugr.getDictList({ typeId: 2034, flag: 1 })
        //     .then((res) => res.result.data.rows);
        // console.log(seasonList);
        // await spReq.spSellerLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await spReq.spClientLogin();
        const dresList = await spdresb.searchDres({ searchToken: '化妆品' });
        console.log(dresList.reqUrl);
        console.log(`dresList=${JSON.stringify(dresList)}`);

    });

    it.skip('设置展示款', async function () {
        await spReq.spClientLogin();
        let searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, orderBy: 'slhDate', orderByDesc: true, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
        let spus = searchRes.slice(0, 5).map((res, index) => {
            return { spuId: res.id, spuTitle: res.title, spuDocId: JSON.parse(res.docHeader)[0].docId, showOrder: 10 - index };
        });
        await spAuth.staffLogin();
        setSpusRes = await spCommon.setGoodShopSpus({ sellerUnitId: sellerInfo.unitId, platSpus: spus });
    });

    it.skip('', async function () {
        await spReq.spSellerLogin({ shopName: '中洲店' });
        const shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
        console.log(`shopInfo=${JSON.stringify(shopInfo)}`);

        const areaJson = await mockJsonParam.getAreaJson({ provinceLabel: '浙江省' });
        const json = {
            id: shopInfo.tenantId,
            provCode: areaJson.provinceValue,
            cityCode: areaJson.provinceValue,
            areaCode: areaJson.provinceValue,
            deliverProvCode: areaJson.provinceValue,
            deliverCityCode: areaJson.cityValue,
            deliverAreaCode: areaJson.countyValue,
            deliverDetailAddr: '滨江星耀城16楼',
        };
        // saveParam.id = sellerTenantId;
        // saveParam.name = name;
        const saveRes = await spugr.updateShop(json);
        const shopInfo2 = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
        console.log(`\nshopInfo=${JSON.stringify(shopInfo2)}`);

    });

    // task#1838 在查询商品明细时，如果出现下架商品的情况，此时触发一个商品重新下架的操作
    it.skip('', async function () {
        const spuId = '3317745';
        await spReq.spClientLogin();
        const dresInfo = await spdresb.searchDres({ queryType: 0, keyWords: { id: [spuId] } }).then(res => res.result.data.dresStyleResultList[0]);
        console.log(`dresInfo=${JSON.stringify(dresInfo)}`);

        await spReq.spSellerLogin();
        const dresInfo2 = await spdresb.getFullById({ id: spuId }).then(res => res.result.data);
        console.log(`\ndresInfo2=${JSON.stringify(dresInfo2)}`);

        await spReq.spClientLogin();
        const dresInfo3 = await spdresb.searchDres({ queryType: 0, keyWords: { id: [spuId] } }).then(res => res.result.data.dresStyleResultList[0]);
        console.log(`\ndresInfo=${JSON.stringify(dresInfo3)}`);
    });

    it.skip('门店商户', async function () {
        await spReq.spSellerLogin({ shopName: '中洲店' });
        let json = mockJsonParam.merchantJson({ contact: 'auto' });
        const res = await spugr.createMerchat(json);
        console.log(`res=${JSON.stringify(res)}`);

    });

    it.skip('修改系统参数', async function () {
        await spAuth.staffLogin();
        const paramInfo = await configParam.getParamInfo({ code: 'bill_confirm_time_out', domainKind: 'system' });
        await paramInfo.updateParam({ val: 15 });
        // console.log(paramInfo);
    });

    it.skip('店铺审核状态修改接口', async function () {
        await spReq.spSellerLogin();
        const sellerTenantId = LOGINDATA.tenantId;

        await spAuth.staffLogin();
        const res = await spugr.updateShopSpuCheckFlag({ id: sellerTenantId, checkFlag: 0 });
        console.log(`res=${JSON.stringify(res)}`);

    });

    it.skip('sp_search_history_detail', async function () {
        await spReq.spClientLogin();
        // console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const res = await spdresb.searchDres({ queryType: 4, searchToken: '上衣' });
        console.log(res);
        console.log(res.result.data.dresStyleResultList.length);
        // await spdresb.searchDres({ queryType: 0, searchToken: '上衣' });

        const res2 = await spugr.findShopBySearchToken({ searchToken: '常青店' });
        console.log(res2);
        // await spAuth.staffLogin();
        // console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
        // await spdresb.searchDres({ queryType: 4, searchToken: '上衣' });
    });

    it.skip('', async function () {
        await spReq.spSellerLogin();
        const res = await spugr.getSaasList({});
        console.log(res);

    });

    it.skip('商品搜索', async function () {
        const ids = [2288344, 2361896, 11038203];
        await spReq.spClientLogin();
        const res = await spdresb.searchDres({ queryType: 0, shadowFlag: 2, keyWords: { id: ids } });
        console.log(`res=${JSON.stringify(res)}`);

    });

    it.skip('店铺上线', async function () {

        await spReq.spSellerLogin();
        const sellerTenantId = LOGINDATA.tenantId;
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        await spAuth.staffLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await spugr.onlineShopById({ id: sellerTenantId, forceFlag: true, autoTestToken: 'autotest' });

    });

    it.skip('', async function () {
        await spAuth.staffLogin();

        // const res = await spdchg.updateChannelDetail({ id: '1057794', channelId: 4530, purLimitNum: 3 });
        // console.log(`res=${JSON.stringify(res)}`);

        // const res = await spAct.checkPayIntime({ jsonParam: [{ actKind: 2, actId: 3122, spuId: 74557 }] });
        // console.log(`res=${JSON.stringify(res)}`);
        // const res = await spugr.clearOpenId({ mobile: '18795607315' });
        const res = await spAuth.getWhiteList();
        console.log(res);



    });

    it.skip('一键代发', async function () {
        const dres1 = dresManage.setupDres(), dres2 = dresManage.setupDres();

        await spReq.spClientLogin();//
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const addressInfo = await spMdm.getUserDefaultRecInfo().then(res => res.result.data);
        // console.log(addressInfo);
        addressInfo.addressId = addressInfo.recInfo.id;
        addressInfo.provCode = addressInfo.address.provinceCode;

        const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [75081, 74121] } }).then(res => res.result.data.dresStyleResultList);
        dres1.setByEs(dresList[0]);
        const dres1Detail = await dres1.getDetailForBuyer();
        dres2.setByEs(dresList[1]);
        const dres2Detail = await dres2.getDetailForBuyer();
        const purJson = mockJsonParam.purJson({ styleInfo: dres1Detail });
        const purJson2 = mockJsonParam.purJson({ styleInfo: dres2Detail });
        // purJson2.
        purJson.orders.push(...purJson2.orders);
        console.log(`purJson=${JSON.stringify(purJson)}`);


        // const orders = [...this.cartList.values()].map(order => {
        //     const orderSpus = [...order.carts.values()].map((cart) => {
        //         return {
        //             spuId: cart.spuId,
        //             orderNum: cart.skuNum,
        //             orderMoney: cart.price
        //         };
        //     });
        //     return {
        //         sellerId: order.trader.tenantId,
        //         orderSpus
        //     }
        // });

        const orders = purJson.orders.map(order => {
            const orderSpus = order.details.map((detail) => {
                return {
                    spuId: detail.spuId,
                    orderNum: detail.num,
                    orderMoney: detail.money
                };
            });
            return {
                sellerId: order.main.sellerId,
                orderSpus
            }
        });
        const feeRes = await spConfb.evalShipFee({ provinceCode: addressInfo.provCode, addressId: addressInfo.addressId, orders }).then(res => res.result.data);
        purJson.combOrders = feeRes.singleFogs;
        const purRes = await spTrade.savePurBill(purJson);
        console.log(`purRes=${JSON.stringify(purRes)}`);

        // const purRes2 = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresDetail }));
    });

    it.skip('', async function () {
        const list1 = [183585, 183593, 183601, 183605, 183617, 183621, 183629, 183641, 183649, 183657, 183665, 183673, 183681, 183689,
            183697, 183701, 183713, 183721, 183729, 183737];
        const list2 = [183581, 183589, 183597, 183609, 183613, 183625, 183633, 183637, 183645, 183653, 183661, 183669, 183677, 183685,
            183693, 183705, 183709, 183717, 183725, 183733];

        await spReq.spSellerLogin();
        // const res = await spTrade.pickUpGood({ id: 183573 });
        // console.log(`res=${JSON.stringify(res)}`);
        for (let index = 0; index < list1.length; index++) {
            const res = await spTrade.pickUpGood({ id: list1[index] });
            console.log(`res=${JSON.stringify(res)}`);
        }

        await spReq.spSellerLogin({ shopName: '中洲店' });
        for (let index = 0; index < list2.length; index++) {
            const res2 = await spTrade.pickUpGood({ id: list2[index] });
            console.log(`res2=${JSON.stringify(res2)}`);
        }
        // const res2 = await spTrade.pickUpGood({ id: 183565 });
        // console.log(`res2=${JSON.stringify(res2)}`);
    });
    it.skip('', async function () {
        await wmsWarehouse.wmsStaffLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        const missionList = await wmsStorage.getManageMissionList({ orderType: 2, unitId: LOGINDATA.unitId, warehouseId: LOGINDATA.tenantId, pageNo: 1, pageSize: 42 }).then(res => res.result.data.rows);
        // console.log(`missionList=${JSON.stringify(missionList)}`);
        for (let index = 0; index < missionList.length; index++) {
            const element = missionList[index];
            const res = await wmsStorage.getGoods({ id: element.missionId, unitId: element.unitId, consigneeId: element.consigneeId });
            console.log(`res=${JSON.stringify(res)}`);
        }

    });
    it.skip('colligateScoreSell', async function () {
        await spReq.spClientLogin();

        const dresList = [];
        for (let pageNo = 1; pageNo <= 3; pageNo++) {
            await spdresb.searchDres({ queryType: 0, orderBy: 'colligateScoreSell', orderByDesc: true, shadowFlag: 0, pageNo: 1, pageSize: 20 }).then(res => {
                // console.log(`res=${JSON.stringify(res)}`);

                console.log(res.reqUrl);
                // console.log(`opTime=${res.opTime} duration=${res.duration}`);
                // dresList.push(...res.result.data);//
            });
        }
        for (let pageNo = 1; pageNo <= 3; pageNo++) {
            await spdresb.searchDresInc({ queryType: 0, orderBy: 'colligateScoreSell', orderByDesc: true, pageNo: 1, pageSize: 20 }).then(res => {
                // console.log(`res=${JSON.stringify(res)}`);

                console.log(res.reqUrl);
                // console.log(`opTime=${res.opTime} duration=${res.duration}`);
                // dresList.push(...res.result.data.rows);//
            });
        }
        // dresList.forEach((dres, index) => {
        //     console.log(`位置=${index + 1} labels=${dres.labels} slhSalesNums30=${dres.slhSalesNums30}`);
        //     const labels = dres.labels.split(' ');
        //     // 奇数位展示一批商家，偶数位展示二批商家  标签包含15位一批
        //     const isInPlace = index % 2 == 1 ? !labels.includes('15') : labels.includes('15');
        //     if (!isInPlace) {
        //         console.log(`排序错误 index=${index + 1}`);
        //     }
        // });
    });
    it.skip('动销数据压测', async function () {
        // await spReq.spClientLogin();
        // console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);

        // await spReq.spSellerLogin();
        // console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
        // const res = await docManager.getDocByDocUrl({ docUrl: 'http://139.196.124.16:8021/slh/readFile.do?fileid=35569925&epid=16433' });
        // console.log(res);
        await spReq.spSellerLogin();

        const spbSql = await mysql({ dbName: 'spbMysql' });
        const dresList = await spbSql.query(`SELECT spu_id AS spuId,'code' AS spuCode,'name' AS spuName FROM spdresb001.dres_spu WHERE src_tenantId=${LOGINDATA.tenantId} AND saas_id=1 ORDER BY created_date LIMIT 500`).then(res => res[0]);
        await spbSql.end();

        dresList.forEach(dres => {
            dres.pushFlag = 1;
            dres.pushPhone = LOGINDATA.mobile;
            dres.pushTenantId = LOGINDATA.tenantId;
            dres.pushTenantName = LOGINDATA.tenantName;
            dres.sureFlag = 1;
            dres.typeId = 1;
        });

        await spAdmin.offLineDynamicSell({ dresSpuUnusualList: dresList });
    });

    it.skip('对店铺设置商品是否店铺外隐藏', async function () {
        await spAuth.staffLogin();
        const res = await spugr.setShopSpuHideFlag({ id: 80131, hideFlag: 0 });
        console.log(res);

    });
    it('', async function () {
        await spReq.spClientLogin();

        const dresList = await spdresb.searchDresInc({ keyWords: { id: [10761659, 12461747] }, shadowFlag: 2, pageNo: 1, pageSize: 20 })
            .then(res => res.result.data.rows);
        console.log(`dres=${JSON.stringify(dresList)}`);
        // const dresList = await spdresb.searchDresInc({ searchToken: '连衣裙', shadowFlag: 2, pageNo: 1, pageSize: 20 })
        //     .then(res => {
        //         console.log(`count=${res.result.data.count}`);
        //         return res.result.data.rows;
        //     });



    });

});

async function saveDres(loginInfo, classId) {
    console.log(loginInfo);

    await spReq.spSellerLogin(loginInfo);

    for (let index = 0; index < 5; index++) {
        const res = await spdresb.saveFull(mockJsonParam.styleJson({ classId }));
        console.log(`tenantId=${LOGINDATA.tenantId},spuId=${res.result.data.spuId}`);

    }


};

