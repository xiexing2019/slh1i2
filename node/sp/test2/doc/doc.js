const fs = require('fs');
const path = require('path');
// const mysql = require('../../../reqHandler/sp/mysql');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const docManage = require('../../../reqHandler/sp/doc_server/doc');

const docsId = 'docs155476730432221269-221-spdoc01.do',
    docxId = '';

(async function () {
    await spReq.spClientLogin();


    const dresList = await spdresb.searchDres({ queryType: 0, pageNo: 1, pageSize: 10 }).then(res => res.result.data.dresStyleResultList);
    console.log(dresList[0].docHeader);

    const docUrls = [];
    JSON.parse(dresList[0].docHeader).forEach(val => val.typeId == 1 && docUrls.push(val.docId));

    for (let index = 0; index < docUrls.length; index++) {
        const element = docUrls[index];
        await docManage.getDocStream({ id: element, thumbFlag: 0 });
    }





})();