'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const spReq = require('../help/spReq');
const mockJsonParam = require('../help/mockJsonParam');
const spDoc = require('../../reqHandler/sp/doc_server/doc');
const spugr = require('../../reqHandler/sp/global/spugr');
const spmdm = require('../../reqHandler/sp/biz_server/spmdm');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const spAccount = require('../data/spAccount');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const spdchg = require('../../reqHandler/sp/global/spdchg');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const dataPrepare = require('../help/wmsHelp/dataPrepare');
const fs = require('fs');
const path = require('path');

describe('数据维护', function () {
    this.timeout(300000);
    let sellerMobile = spAccount.seller.mobile,      //等到配置文件好了之后，从配置文件里面拿
        clientMobile = spAccount.client.mobile;

    // before('添加白名单', async function () {
    //     await spAuth.staffLogin();
    //     await spAuth.saveWhiteList(sellerMobile);
    //     await spAuth.saveWhiteList(clientMobile);
    // });

    describe.skip('文档数据维护', function () {
        let localPath, docData;
        before(async function () {
            localPath = path.join(__dirname, '../data/doc.json');

            docData = JSON.parse(fs.readFileSync(localPath));
        });
        after(async function () {
            fs.writeFile(localPath, JSON.stringify(docData), (err) => {
                if (err) console.error(err);
            });
        });
        it('qrCode', async function () {
            let images = [];
            const docPath = path.join(__dirname, `../doc/首页换图/分享二维码/qrCode.png`);
            await spDoc.upload({ path: docPath }).then(res => images.push(res));
            docData[caps.name].image.qrCode = images;
        });
        it('shopLogo', async function () {
            let images = [];
            for (let index = 1; index < 4; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/门店logo/店铺logo${index}.jpeg`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.shopLogo = images;
        });
        it('shopBackground', async function () {
            let images = [];
            for (let index = 1; index < 6; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/门店背景图/background${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.shopBackground = images;
        });
        it('model', async function () {
            let images = [];
            for (let index = 1; index < 11; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/模特图片/model${index}.jpg`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.model = images;
        });
        it('dres', async function () {
            let images = [];
            for (let index = 1; index < 6; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/商品图片/门店商品/dres${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.dres = images;
        });
        it('medal', async function () {
            let images = [];
            const docPath = path.join(__dirname, `../doc/首页换图/勋章/medal.png`);
            await spDoc.upload({ path: docPath }).then(res => images.push(res));
            docData[caps.name].image.medal = images;
        });
        it('banner', async function () {
            let images = [];
            for (let index = 1; index < 5; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/商品图片/首页/banner${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.banner = images;
        });
    });

    describe.skip('web端维护', async function () {
        before(async function () {
            await spAuth.staffLogin();
        });
        it('保存筛选配置-推荐下拉', async function () {
            // const configList = await spCommon.getSpFilterConfigList({ filterType: 1 });
            // console.log(`configList=${JSON.stringify(configList)}`);

            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 1, typeName: '标签', showOrder: 1 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 2, typeName: '勋章', showOrder: 2 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 3, typeName: '风格', showOrder: 3 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 4, typeName: '价格', showOrder: 4 });
        });
        it('公司信息管理', async function () {
            // spAdmin 公司信息管理
        });
    });

    describe.skip('卖家基本信息维护', function () {
        const name = 'Aster',
            sellerMobile = '12905140000';// spAccount.seller.mobile;// common.getRandomMobile()
        let sellerTenantId;
        it('注册卖家，开通店铺', async function () {
            await spAuth.staffLogin();
            await spAuth.saveWhiteList(sellerMobile);

            //需要修改为门店名称
            // const name = '高胖胖3号店';//spAccount.seller.shopName;
            // const sellerMobile = common.getRandomMobile();
            await spReq.spLogin({ code: sellerMobile });
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            //创建门店
            await spReq.createEntTenant({
                name: name,
            });

            //切换门店
            sellerTenantId = await spugr.findAvailableTenantIds().then((res) => {
                console.log(`res=${JSON.stringify(res)}`);
                return _.findKey(res.result.data, (val) => val == name);
            });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            console.log(`\nsellerTenantId=${JSON.stringify(sellerTenantId)}`);

            let saveParam = await mockJsonParam.shopJson();
            saveParam.id = sellerTenantId;
            saveParam.name = name;
            const saveRes = await spugr.updateShop(saveParam);
            console.log(`\nsaveRes=${JSON.stringify(saveRes)}`);
            await common.delay(500);

            await spAuth.staffLogin();
            await spugr.setShadowFlag({ id: sellerTenantId, shadowFlag: 1, tenantType: 1 });
            await spugr.onlineShopById({ id: sellerTenantId, forceFlag: true, autoTestToken: 'autotest' });
        });

        // it('设置影子标志', async function () {
        //     await spAuth.staffLogin();

        // });

        it('卖家注册消息', async function () {
            await spReq.spLogin({ code: sellerMobile });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            await spmdm.registerUserMessageCid({ productCode: 'ipadslh' });
        });

        it('门店商户信息维护', async function () {
            let json = mockJsonParam.merchantJson({ mobile: sellerMobile, contact: 'auto' });
            await spugr.createMerchat(json);
        });

        it('卖家店铺新增商品', async function () {
            await spReq.spLogin({ code: sellerMobile });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            for (let i = 0; i < 10; i++) {
                const res = await spReq.saveDresFull();
                console.log(`res=${JSON.stringify(res.result)}`);

                // await common.delay(100);
            };
        });

        it.skip('店铺下线', async function () {
            await spReq.spSellerLogin();
            let tenantId = LOGINDATA.tenantId;
            await spAuth.staffLogin();
            await spugr.offlineShopById({ id: tenantId });
        });
    });

    describe.skip('买家基本信息维护', function () {

        it('买家头像等信息维护', async function () {
            // await spAuth.staffLogin();
            // await spAuth.saveWhiteList('12903250000');
            const clientMobile = '18795607315';

            await spReq.spLogin({ code: clientMobile });
            console.log(`LOINGDATA=${JSON.stringify(LOGINDATA)}`);

            // let areaJson = await mockJsonParam.getAreaJson();
            // let submitAuditRes = await spCommon.submitAuditRecord(await mockJsonParam.submitAuditJson({ areaJson }));
            // console.log(`submitAuditRes=${JSON.stringify(submitAuditRes)}`);

            // await spAuth.staffLogin();
            // let changeRes = await spCommon.changeAuditRecord({
            //     shopId: submitAuditRes.result.data.id,
            //     flag: 1,
            //     auditRemark: '条件符合审核条件，同意了'
            // });

            // await spReq.spLogin({ code: clientMobile });
            // console.log(`LOINGDATA=${JSON.stringify(LOGINDATA)}`);
            await spmdm.registerUserMessageCid();
        });
    });

    describe.skip('分销渠道维护', async function () {
        let sellerA, sellerB, spDis = new mockJsonParam.SpDisInfo();
        before(async function () {
            await spReq.spSellerLogin();
            sellerA = _.cloneDeep(LOGINDATA);

            await spReq.spSellerLogin({ shopName: '中洲店' });
            sellerB = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();
            // 获取省市区
            const provCode = await spAuth.getDictTree({ hierId: '850' }).then(res => res.result.data.options.find(province => province.value == '浙江省').code);
            const cityCode = await spAuth.getDictTree({ hierId: '850', codeId: provCode }).then(res => res.result.data.options.find(city => city.value == '杭州市').code);
            const areaCode = await spAuth.getDictTree({ hierId: '850', codeId: cityCode }).then(res => res.result.data.options.find(area => area.value == '江干区').code);

            Object.assign(spDis, {
                name: '分销lxx',
                provCode,
                cityCode,
                areaCode,
                detailAddr: '四季青服装市场',
                disType: 2,//分销商类型 2:渠道类型，3：第三方类型
                mobile: '12810000000',
                disLevels: 3,
                totalProfit: 0.2,//总分润比例(其他分润比例总和，没有的就是0)
                platProfit: 0.1,
                disChannelProfit: 0.05,
                levelOneProfit: 0.025,
                levelTwoProfit: 0.015,
                levelThreeProfit: 0.01,
            });
        });
        it('新增分销频道', async function () {
            const channel = mockJsonParam.getChannelJson();
            channel.name = '自动化分销频道';
            channel.filterShopIds = `${sellerA.tenantId},${sellerB.tenantId}`;
            // 上架日期从 服务端会在新增时直接初始化数据
            channel.filterMarketDateBeign = common.getDateString([-1, 0, 0]);
            const channelRes = await spdchg.saveChannelWithRule(channel);
            spDis.channelId = channelRes.result.data.val;
        });
        it('新增/修改分销商', async function () {
            const disList = await spugr.getSpDisList({ nameLike: spDis.name }).then(res => res.result.data.rows);
            const _dis = disList.find(dis => dis.name == spDis.name);
            if (_dis) spDis.id = _dis.id;

            // http://106.15.226.60:8080/showdoc/index.php?s=/32&page_id=2722
            const disRes = await spugr.saveSpDis(spDis);
            spDis.id = disRes.result.data.val;
            console.log(disRes);
        });
        it('维护分销商商户信息', async function () {
            const spDisMerchantInfo = mockJsonParam.getSpDisMerchantJson(spDis);
            spDisMerchantInfo.mchName = '商户lxx';
            const saveRes = await spdchg.saveDisMerchant(spDisMerchantInfo);
            console.log(`saveRes=${JSON.stringify(saveRes)}`);

            // 手动审核通过  (一段时间后自动会通过) 
            // await spugr.updateSpMerchatFlagByPerson({ id: saveRes.result.data.val, flag: 1 });
        });
    });

    describe.skip('slh注册平台', function () {
        let tenantId;
        const name = '花木牛仔屋'   //需要开通的好店的名称
        it('slh登录', async function () {
            await spReq.slhStaffLogin();

            let createJson = await mockJsonParam.createTetantBySlhJson();
            createJson.name = name;
            createJson.slhShopId = LOGINDATA.invid;
            createJson.storeInvIds = LOGINDATA.invid;
            createRes = await spugr.createGoodShopBySlh(createJson);
        });

        it('门店基本信息维护', async function () {
            await spReq.loginGoodShopBySlh();
            let saveParam = await mockJsonParam.shopJson();
            saveParam.id = LOGINDATA.tenantId;
            delete saveParam.name;
            await spugr.updateShop(saveParam);
        });

        it('门店商户信息维护', async function () {
            let json = mockJsonParam.merchantJson({ mobile: '15988413448', contact: 'auto' });
            await spugr.createMerchat(json);
        });

        it('门店上线', async function () {
            await spAuth.staffLogin();
            await spugr.onlineShopById({ id: tenantId });
        });
    });

    describe.skip('脚本执行完毕后检查', function () {

        it('脚本执行完检查最近购买的店铺列表', async function () {
            await spReq.spClientLogin();
            let recentBuyShopList = await spTrade.getRecentBuyShopList({ pageSize: 10, pageNo: 1 }).then(res => res.result.data.rows);
            let info = recentBuyShopList.find(res => res.shopName == spAccount.seller.shopName);
            if (info == undefined) {
                throw new Error('店铺下线之后，最近购买列表里面此门店找不到了，应该依然存在');
            } else {
                expect(info.shopFlag, '店铺下线之后，但是状态字段还不是下线状态').to.be.equal(0);
            }
        });
    });

    describe.skip('清理数据脚本', function () {

        it('清理banner', async function () {
            await spAuth.staffLogin();
            const info = await spCommon.findBanner({ pageSize: 100, pageNo: 1, jsonParam: { flag: 0 } });
            //剩下3个
            for (let i = 0; i < info.result.data.rows.length - 3; i++) {
                await spCommon.deleteBanner({ id: info.result.data.rows[i].id });
                console.log(`删除了${info.result.data.rows[i].id}`);
            };
        });
    });

    describe.skip('影子系统', function () {
        let clientInfo, sellerInfo;
        describe('卖家影子系统', function () {

            before('给卖家设置影子系统', async function () {
                await spReq.spSellerLogin();
                sellerInfo = LOGINDATA;

                await spAuth.staffLogin();
                //去店铺影子列表里面查卖家
                let shadowList = await spugr.findAllShopByParams({ name: sellerInfo.shopName, shadowFlag: 1 });
                let info = shadowList.result.data.rows.find(shop => shop.id == sellerInfo.tenantId);
                if (info == undefined) {
                    console.log('进入到设置影子分支了');
                    //如果列表里面没有找到卖家店铺，那么就将卖家店铺设置为影子店铺
                    await spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 1, tenantType: 1 });
                };
            });

            it('检验', async function () {
                await common.delay(3000);
                let shadowList = await spugr.findAllShopByParams({ name: sellerInfo.shopName, shadowFlag: 0 });
                let info = shadowList.result.data.rows.find(shop => shop.id == sellerInfo.tenantId);
                expect(info, `给店铺设置了影子，但是正常店铺还是可以搜索到此门店${JSON.stringify(info)}`).to.be.undefined;
            });

            it('es搜索检验', async function () {

                let searchDres = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, shadowFlag: 0 });
                let dresInfo = searchDres.result.data.dresStyleResultList;
                expect(dresInfo.length, `店铺添加到白名单了，但是还是可以搜索到商品`).to.be.equal(0);
            });
        });

        describe('买家', function () {
            let tenantIds;
            before('', async function () {
                await spReq.spClientLogin();
                clientInfo = LOGINDATA;

                await spAuth.staffLogin();
                let whiteList = await spugr.getBuyerList({ nameLike: LOGINDATA.nickName, mobile: clientInfo.code, shadowFlag: 1 });
                let buyer = whiteList.result.data.rows.find(val => val.buyerTenantId == clientInfo.tenantId);

                if (buyer == undefined) {
                    console.log('进入添加白名单');
                    await spugr.setShadowFlag({ id: clientInfo.tenantId, shadowFlag: 1, tenantType: 0 });
                };
            });

            // it('验证', async function () {
            //     await common.delay(2000);
            //     await spReq.spClientLogin();
            //     let res = await spugr.findShopBySearchToken({ pageSize: 30, pageNo: 1, shadowFlag: 1 });
            //     let ids = res.result.data.rows.map(val => {
            //         return { name: val.name, shadowFlag: val.shadowFlag };
            //     });
            //     console.log(`ids=${JSON.stringify(ids)}`);
            // });

            it('es', async function () {
                await common.delay(2000);
                await spReq.spClientLogin();
                let res = await spdresb.searchDres({ pageSize: 40, pageNo: 1, tenantId: sellerInfo.tenantId, queryType: 0 }).then(res => res.result.data.dresStyleResultList);
                expect(res.length, '买家和卖家都在白名单，但是买家搜不到卖家的商品').to.be.above(0);
            });

            it('es搜索', async function () {
                await spAuth.staffLogin();
                //查询白名单里面的店铺
                let shadowList = await spugr.findAllShopByParams({ pageSize: 40, pageNo: 1, shadowFlag: 1 });
                tenantIds = shadowList.result.data.rows.map(val => val.id);
                await spReq.spClientLogin();
                let ids;
                for (let i = 0; i < 8; i++) {
                    let res = await spdresb.searchDres({ pageSize: 40, pageNo: 1, queryType: i }).then(res => res.result.data.dresStyleResultList);
                    ids = common.dedupe(res.map(val => val.tenantId));
                    expect(tenantIds, `白名单里面的租户${tenantIds} ,es搜出来的租户${ids},不是白名单店铺的商品也可以搜索出来`).to.include.members(ids);
                };
            });

            it('查询店铺', async function () {
                let shop = await spugr.findShopBySearchToken({ pageSize: 40, pageNo: 1 }).then(res => res.result.data.rows);
                let ids = shop.map(val => val.tenantId);
                expect(tenantIds, `白名单里面的租户${tenantIds} ,es搜出来的租户${ids},不是白名单店铺的商品也可以搜索出来`).to.include.members(ids);
            });
        });

        describe.skip('移除', function () {

            it('移除卖家', async function () {
                await spReq.spSellerLogin();
                sellerInfo = LOGINDATA;

                await spAuth.staffLogin();
                await spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 0, tenantType: 1 });
            });

            it('移除买家', async function () {
                await spReq.spClientLogin();
                clientInfo = LOGINDATA;

                await spAuth.staffLogin();
                await spugr.setShadowFlag({ id: clientInfo.tenantId, shadowFlag: 0, tenantType: 0 });
            });
        });
    });

    describe.skip('仓储数据准备脚本', function () {
        let dp;
        it('准备市场', async function () {
            await spAuth.staffLogin();
            dp = new dataPrepare.PrepareData();
            await dp.dataPrepare();
            // console.log(dp);
        });

        it('修改两个店铺的市场', async function () {
            await spReq.spSellerLogin();
            let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
            shopInfo.marketId = dp.marketId;
            await spugr.updateShop(shopInfo);
        });

        it('修改中洲店的市场', async function () {
            await spReq.spSellerLogin({ shopName: '中洲店' });
            let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
            shopInfo.marketId = dp.marketId;
            await spugr.updateShop(shopInfo);
        });
    });
});
