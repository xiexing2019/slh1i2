const common = require('../../lib/common');
const spugr = require('../../reqHandler/sp/global/spugr');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../reqHandler/sp/confc/spAuth');


(async () => {
    await spAuth.staffLogin();

    const shopList = await spugr.findAllShopByParams({ flag: 1, orderBy: 'updatedDate', orderByDesc: true, pageSize: 0 }).then(res => res.result.data.rows);
    const failShops = [];
    //
    for (let index = 0; index < shopList.length; index++) {
        const shopInfo = shopList[index];
        const dealRes = await spdresb.autoDealDresSpuMarket({ flag: 1, opType: 1, unitId: shopInfo.unitId, tenantId: shopInfo.tenantId, check: false });
        console.log(`shopName=${shopInfo.name}`);
        console.log(`dealRes=${JSON.stringify(dealRes)}`);
        if (dealRes.result.code < 0) {
            failShops.push({ unitId: shopInfo.unitId, tenantId: shopInfo.tenantId });
            await common.delay(1000);
        };
        await common.delay(1000);
    };

    console.log(failShops);


})();