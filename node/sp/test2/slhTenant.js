'use strict';
const common = require('../../lib/common');
const mockJsonParam = require('../help/mockJsonParam');
const spugr = require('../../reqHandler/sp/global/spugr');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const spReq = require('../help/spReq');

describe('slh开通好店', function () {
    this.timeout(30000);
    describe.skip('开通好店', function () {
        let bankList, areaJson;
        before('商陆花用户登录', async function () {
            //slh用户登录
            await spReq.slhStaffLogin();
        });

        after('切回原来的环境', function () {
            spReq.changeToCommonEnv();
        });

        //开通好店，新的环境开通一次即可   这里一定要完善店铺的信息，不然ipad上还停留在开通步骤中
        it.skip('ipad开通商陆好店，并且完善店铺信息', async function () {
            //获取邀请码,银行等信息
            await spAuth.staffLogin();
            areaJson = await mockJsonParam.getAreaJson();
            let authCode = await spCommon.findAuthCodes();
            //筛选出有效的邀请码
            let info = authCode.result.data.rows.find(res => res.flag == 1);
            //slh员工登陆，获取开通门店的数据并且开通门店
            await spReq.slhStaffLogin();
            bankList = await spugr.getBank({ province: areaJson.provinceCode });
            let tenantJson = mockJsonParam.createTetantJson({ areaJson, bank: bankList.result.data.rows[0], authCode: info.id });
            await spugr.createTenantBySlh(tenantJson);

            //登录好店
            await spugr.slhStaffLoginSp();
            //完善店铺相关信息。
            let saveParam = await mockJsonParam.shopJson();
            saveParam.id = LOGINDATA.tenantId;
            saveParam.name = '言匠';
            await spugr.updateShop(saveParam);
        });

        it('判断店铺是否开通好店', async function () {
            //await spReq.slhStaffLogin();
            let isOpen = await spugr.isOpen({ slhShopId: LOGINDATA.invid });
            expect(isOpen.result.data.isOpen, '已经开通的门店还是显示为未开通').to.be.true;
        });

        it('获取已经开通的门店列表', async function () {
            let res = await spugr.hasOpenShopList();
            let info = res.result.data.rows.find(res => res.slhShopId == LOGINDATA.invid);
            if (info == undefined) {
                throw new Error('已经开通的门店在门店列表中未显示');
            } else {
                common.isApproximatelyEqualAssert({ slhShopId: LOGINDATA.invid, name: LOGINDATA.invname }, info);
            };
        });

        it('设置发布规则', async function () {
            let savePublishRes = await spugr.savePublishRule({ slhShopId: LOGINDATA.invid, pubDelay: 1, pubDelayDay: 5, pubPriceType: 2 });
            //查询规则
            let res = await spugr.getPublishRule({ slhShopId: LOGINDATA.invid });
            common.isApproximatelyEqualAssert(savePublishRes.params.jsonParam, res.result.data);
        });

    });

    //开通门店需要准备： 账套，里面新建常青店  并且包括角色工号 '200'   
    describe('平台门店商户', async function () {

        describe('门店开通好店', function () {
            let createTetantRes, commonInfo, tenantId, slhShopId;
            before('登录', async function () {
                await spReq.slhStaffLogin();
                commonInfo = LOGINDATA;
            });

            it('开通好店', async function () {
                await spReq.slhStaffLogin({ logid: '200' });  //常青店登录，获取门店id
                slhShopId = LOGINDATA.invid;
                await spAuth.staffLogin();
                let createJson = await mockJsonParam.createTetantBySlhJson();
                await spReq.slhStaffLogin();   //默认 000工号登录
                createJson.slhShopId = slhShopId; //登录的是默认店，帮常青店开通
                createJson.storeInvIds = slhShopId; //仓库（常青店）
                createTetantRes = await spugr.createGoodShopBySlh(createJson);
                console.log(`新开通商陆好店(创建平台租户) 新接口=${JSON.stringify(createTetantRes)}`);
                let info = await spugr.slhStaffLoginSp({ check: false });
                expect(info.result, '没开通好店也能登录好店').to.includes({ "msgId": "sp_tenant_not_open" });
            });


            it('常青店登录好店管理员上线好店', async function () {
                tenantId = await spReq.loginGoodShopBySlh({ logid: '200' }).then(res => res.result.data.tenantId);   //常青店总经理登录好店
                let shopInfo = await spugr.getShopDetail();
                // common.isApproximatelyEqualAssert(createTetantRes.params.jsonParam, shopInfo.result.data);
                await spAuth.staffLogin();
                await spugr.onlineShopById({ id: tenantId });
            });

            it('门店基本信息维护', async function () {
                await spReq.loginGoodShopBySlh({ logid: '200' });
                //完善店铺相关信息。
                let saveParam = await mockJsonParam.shopJson();
                console.log(`saveParam=${JSON.stringify(saveParam)}`);
                saveParam.id = LOGINDATA.tenantId;
                delete saveParam.name;
                await spugr.updateShop(saveParam);
            });

            //第一遍执行要打开 断言
            it('门店关联好店信息', async function () {
                await spReq.slhStaffLogin({ logid: '200' });
                let res = await spugr.getShopBySlhShopId({ slhShopId: slhShopId });
                // common.isApproximatelyEqualAssert(createTetantRes.params.jsonParam, res.result.data);
            });

            //将刚开通的好店，绑定的门店从常青店---默认店
            it('好店门店仓库绑定修改', async function () {
                await spReq.slhStaffLogin({ logid: '200' });
                //绑定门店从常青店----默认店
                let updateRes = await spugr.updateGoodShopStoreInv({ spTenantId: tenantId, slhShopId: commonInfo.invid, storeSyncInvIds: commonInfo.invid });
                await spReq.slhStaffLogin();
                let info = await spugr.getShopBySlhShopId({ slhShopId: LOGINDATA.invid });
                common.isApproximatelyEqualAssert(updateRes.params.jsonParam, info.result.data);
                await spugr.slhStaffLoginSp();   //默认店登录好店
                //用常青店登录，应该登不上去了
                await spReq.slhStaffLogin({ logid: '200' });
                let res = await spugr.slhStaffLoginSp({ check: false });
                expect(res.result, '常青店换绑后还能登陆好店').to.includes({ "msgId": "sp_tenant_not_open" });
            });

            //默认店登录，将绑定门店改为常青店
            after('数据还原', async function () {
                await spReq.slhStaffLogin();
                await spugr.updateGoodShopStoreInv({ spTenantId: tenantId, slhShopId: slhShopId, storeSyncInvIds: slhShopId });
            });
        });

        describe('门店商户信息完善', function () {
            let merchantInfo;
            before('通过常青店登录好店', async function () {
                await spAuth.staffLogin();
                await spugr.updateSpMerchatFlagByPerson({ id: '1002', flag: 5 });
                await spReq.loginGoodShopBySlh({ logid: '200' });
                merchantInfo = await spugr.findMerchant().then(res => res.result.data);
            });

            //注，这里是直接提交到清分系统的，会调用第三方接口进行相关信息审核的
            it('修改', async function () {
                let json = mockJsonParam.merchantJson();
                if (Object.keys(merchantInfo).length != 0) {
                    json.id = merchantInfo.id;
                };
                await spugr.createMerchat(json);
            });

            it('查看账户信息', async function () {
                const res = await spugr.findMerchant().then(res => res.result.data);
                console.log(`res=${JSON.stringify(res)}`);
            });
        });
    });
});
