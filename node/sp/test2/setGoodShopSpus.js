const common = require('../../lib/common');
const spugr = require('../../reqHandler/sp/global/spugr');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../reqHandler/sp/global/spCommon');


(async function () {
    const showNum = 9, docNum = 3;

    await spAuth.staffLogin();

    // 获取所有门店单元id
    let totalPageNo = 0;
    const shopList = [];
    await spugr.findAllShopByParams({ pageSize: 20, pageNo: 1, shadowFlag: 0, orderBy: 'updatedDate', orderByDesc: true })
        .then(res => {
            const data = res.result.data;
            totalPageNo = Math.ceil(data.total / 20);
            data.rows.forEach(row => {
                shopList.push({ name: row.name, unitId: row.unitId, tenantId: row.tenantId });
            });
        });
    for (let pageNo = 2; pageNo <= totalPageNo; pageNo++) {
        await spugr.findAllShopByParams({ pageSize: 20, pageNo, shadowFlag: 0, orderBy: 'updatedDate', orderByDesc: true })
            .then(res => {
                res.result.data.rows.forEach(row => {
                    shopList.push({ name: row.name, unitId: row.unitId, tenantId: row.tenantId });
                });
            });
    }
    console.log(`总店铺数${shopList.length}`);

    // 设置展示款 
    for (let index = 0; index < shopList.length; index++) {
        await common.delay(100);
        const sellerInfo = shopList[index];
        let msg = `\nname=${sellerInfo.name},unitId=${sellerInfo.unitId},tenantId=${sellerInfo.tenantId}`;
        await common.delay(100);
        // 查询店铺已配置的展示款
        const shopSpus = await spCommon.getGoodShopSpus({ sellerUnitId: sellerInfo.unitId })
            .then(res => res.result.data.rows);
        // console.log(`shopSpus=${JSON.stringify(shopSpus)}`);
        await common.delay(100);
        const searchSpuIds = shopSpus.map(data => data.spuId);
        await spdresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: searchSpuIds } })
            .then(res => res.result.data.dresStyleResultList.forEach((dres) => {
                const docNums = JSON.parse(dres.docHeader).length;
                if (docNums < 3) {
                    msg += `\n移除商品 id:${dres.id} title:${dres.title}`;
                    _.remove(shopSpus, (shopSpu) => {
                        return shopSpu.spuId == dres.id;
                    });
                    // console.log(`shopSpus=${JSON.stringify(shopSpus)}`);
                }
            }));

        if (shopSpus.length >= showNum) {
            console.log(`${msg},展示款满足9款,跳过`);
            continue;
        }

        // 取店铺前6款
        // const spus = await spdresb.searchDres({ pageSize: 6, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId })
        //     .then(res => res.result.data.dresStyleResultList.map((dres, index) => {
        //         return { spuId: dres.id, spuTitle: dres.title, spuDocId: JSON.parse(dres.docHeader)[0].docId, showOrder: 0 };
        //     }));

        // 优先选取商品图片>=3张的商品,若不足则直接取最新的商品补充
        const spus = [];
        for (let _pageNo = 1; _pageNo <= 5; _pageNo++) {
            await common.delay(500);
            const _spus = await getSpus({ tenantId: sellerInfo.tenantId, pageNo: _pageNo });
            spus.push(..._spus);
            if (spus.length >= showNum) break;
        }

        if (spus.length < showNum) {
            await common.delay(300);
            const _spus = await spdresb.searchDres({ pageSize: 6, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId })
                .then(res => res.result.data.dresStyleResultList.map((dres, index) => {
                    return { spuId: dres.id, spuTitle: dres.title, spuDocId: JSON.parse(dres.docContent)[0].docId, showOrder: 0 };
                }));
            spus.push(..._spus);
        }

        // 去重 
        const platSpus = _.unionWith(shopSpus, spus, (arr1, arr2) => {
            return arr1.spuId == arr2.spuId;
        }).slice(0, showNum);

        await spCommon.setGoodShopSpus({ sellerUnitId: sellerInfo.unitId, platSpus });
        console.log(`${msg},\npass`);
    }
})();

async function getSpus({ tenantId, pageNo }) {
    const spus = [];

    await spdresb.searchDres({ pageSize: 20, pageNo, queryType: 4, tenantId })
        .then(res => {
            res.result.data.dresStyleResultList.forEach(dres => {
                const docContent = JSON.parse(dres.docContent);
                if (docContent.length >= 3) {
                    spus.push({ spuId: dres.id, spuTitle: dres.title, spuDocId: docContent[0].docId, showOrder: 0 });
                };
            });
        });

    return spus;
};