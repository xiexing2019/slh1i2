const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spugr = require('../../../reqHandler/sp/global/spugr');

// 按销售额分别标记为Vip1~5，
// 其中Vip5，；Vip4，5000<=X<10000；Vip3，1000<=X<5000；Vip2，300<=X<1000；Vip1，0<=X<300
(async function () {
    await spAuth.staffLogin();
    const labels = await spugr.getDictList({ typeId: '2024', flag: 1, pageSize: 100, pageNo: 1 }).then((res) => res.result.data.rows);

    const labelJYY = labels.find(data => data.codeName == '计缘缘').codeValue;
    const labelDT = labels.find(data => data.codeName == '戴婷').codeValue;
    const labelMXQ = labels.find(data => data.codeName == '马小淇').codeValue;
    console.log(`计缘缘=${labelJYY},戴婷=${labelDT},马小淇=${labelMXQ}`);

    const buyerList = await spCommon.findBuyerShops({ orderBy: 'createdDate', orderByDesc: true, pageNo: 1, pageSize: 100 }).then(res => res.result.data.rows);

    for (let index = 0; index < buyerList.length; index++) {
        try {
            const buyer = buyerList[index];
            const day = buyer.createdDate.split(/\s/)[0];
            const remainder = Number(_.last(day.split('-'))) % 3;
            // 余数为1的，标记给计缘缘；余数为2的，标记给戴婷；余数为0的，标记给马小淇
            const operationTag = [labelMXQ, labelJYY, labelDT][remainder];
            console.log(`mobile=${buyer.mobile},创建时间=${day},余数=${remainder},标签=${operationTag}`);

            const buyerLabels = _.cloneDeep(buyer.tags);
            console.log('修改前标记:%s', buyerLabels);
            if (buyerLabels.some(label => [labelMXQ, labelJYY, labelDT].includes(label))) {
                console.log(`已含有运营标记,不做修改`);
                continue;
            }
            buyerLabels.push(operationTag);
            console.log('修改后标记:%s', buyerLabels);

            const auditRes = await spAdmin.addLabelAuditRecord({ type: 2, auditObjId: buyer.id, labels: buyerLabels.join(',') });
            // console.log(auditRes.result.data.val);
            await common.delay(100);
            await spAdmin.passLabelAudit({ id: auditRes.result.data.val });
        } catch (error) {
            console.log(error);
        }
    }

})();