const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const sp = require('../../../reqHandler/sp');

/**
 * 买家标记数据修复
 * 之前待审核的店铺不能给与买家标记，现在权限修改，可以修改
 * 将之前待审核店铺的审核记录通过审核
 */
(async function () {

    await spAuth.staffLogin();

    const auditList = await sp.spAdmin.getLabelAuditRecordList({ type: 2, auditFlag: 0, pageSize: 200, pageNo: 1 })
        .then(res => res.result.data.rows);

    for (let index = 0; index < auditList.length; index++) {
        const element = auditList[index];

        try {
            if (element.createdName == '苏维伟') {
                console.log(`mobile=${element.mobile},id=${element.id},auditContent=${element.auditContent},createdDate=${element.createdDate}`);
                await sp.spAdmin.passLabelAudit({ id: element.id });
            }
        } catch (error) {
            console.log(error);
        }
    }

})();