const fs = require('fs');
const path = require('path');
const spReq = require('../help/spReq');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');

/**
 * es搜索检测
 * 按李果要求,目前只记录出现互斥词的情况, 不带同义词先不记录
 * http://c.hzdlsoft.com:7082/Wiki.jsp?page=Es-autotest
 */


const docData = fs.readFileSync(path.join(__dirname, '自动化搜索数据.txt')).toString().split(`\r\n`);
// 匹配规则 类别，性别，材质，板式1，板式2，尺码
let [classList, sexList, fabricList, styleList1, styleList2, sizeList, ...searchTokenList] = docData;
searchTokenList = Array.from(new Set(searchTokenList));

const classMap = getSearchMap(classList),
    fabricMap = getSearchMap(fabricList),
    styleMap1 = getSearchMap(styleList1),
    styleMap2 = getSearchMap(styleList2),
    sizeMap = getSearchMap(sizeList);

(async () => {

    //使用queryType=0 需要使用买家登录
    await spReq.spClientLogin();
    // 查询商品类别树 
    const dresClassMap = new Map();
    await spdresb.findClassTreeList({ incDirectSub: true }).then(res => {
        res.result.data.rows.forEach(dresClass => {
            const classNames = new Set([dresClass.typeName]);
            dresClass.subItems.forEach(subItem => classNames.add(subItem.typeName));
            if (dresClass.alias) {
                classNames.add(dresClass.alias);
                dresClassMap.set(dresClass.alias, classNames);
            }
            dresClassMap.set(dresClass.typeName, classNames);
        });
    });


    // 搜索校验
    for (let index = 0; index < searchTokenList.length; index++) {
        const searchToken = searchTokenList[index];
        let msg = '';

        // 获取性别关联的主营类目名称
        const masterClassList = getMasterClassByToken(searchToken);
        // 获取搜索规则关键字
        const _fabricMap = getSearchRoleMap(searchToken, fabricMap),
            _styleMap1 = getSearchRoleMap(searchToken, styleMap1),
            _styleMap2 = getSearchRoleMap(searchToken, styleMap2),
            _sizeMap = getSearchRoleMap(searchToken, sizeMap);

        // 
        let _classMap = getSearchRoleMap(searchToken, dresClassMap);
        if (_classMap.get('same').length == 0) {
            _classMap = getSearchRoleMap(searchToken, classMap);
        }

        // es搜索
        const searchList = await esSearch({ searchToken });
        if (searchList.length == 0) continue;

        searchList.forEach((val) => {
            const search = new Es({ dresInfo: val, searchToken });

            //检查类别
            search.checkDresClass(_classMap);

            //检查性别
            search.checkSex(masterClassList);

            //检查材质
            search.checkTitle(_fabricMap);

            //检查板式1
            search.checkTitle(_styleMap1);

            //检查板式2
            search.checkTitle(_styleMap2);

            //检查尺码
            search.checkTitle(_sizeMap);

            if (!search.flag) msg += `\nmasterClassName=${val.masterClassName} title=${val.title}`;
        });

        if (msg.length > 0) {
            console.log(msg);
        };

    };


})();

/**
 * 
 * @param {string} str 
 * @return {Map}
 */
function getSearchMap(str) {
    const map = new Map();
    const arr = str.split(' ').map(val => val.split(','));
    arr.forEach((ele) => ele.forEach(key => map.set(key, ele)));
    return map;
};

/**
 * 根据搜索关键字获取匹配主营类目
 * @description
 * 1. 输入包含男不含童和女
 * 2. 输入包含女不含童和男
 * 3. 童是第一优先级 比如输入女童就是童，有童只按照童处理
 * 4. 同时有男女 按男 女 都搜处理
 */
function getMasterClassByToken(searchToken) {
    if (searchToken.includes('童')) {
        return ['童装', '衣饰', '箱包', '内衣', '运动户外', '童鞋'];
    };

    let masterClass = [];
    if (searchToken.includes('男')) {
        masterClass.push(...['男装', '鞋靴', '衣饰', '箱包', '内衣', '运动户外']);
    };
    if (searchToken.includes('女')) {
        masterClass.push(...['女装', '鞋靴', '衣饰', '箱包', '内衣', '运动户外']);
    };

    return Array.from(new Set(masterClass));
};

/**
 * es搜索
 * @param {object} params 
 * @param {string} params.searchToken 
 */
async function esSearch(params) {
    const searchRes = await spdresb.searchDres({ pageSize: 100, pageNo: 1, queryType: 0, shadowFlag: 0, ...params });
    console.log(`\n\n\nsearchToken = %s   查询时间= %s ms`, params.searchToken, searchRes.duration);

    if (searchRes.result.data.dresStyleResultList.length == 0) console.log(`查询无结果`);
    if (searchRes.result.data.count > 5000) console.log(`查询总条目数:${searchRes.result.data.count}`);
    return searchRes.result.data.dresStyleResultList;
};

/**
 * 查询关键字下标
 * @param {string} str 
 * @param {Map} keys 
 */
function findKeysMap(str, map) {
    let keyMap = new Map();
    map.forEach((key) => {
        const index = str.indexOf(key);
        if (index > -1) keyMap.set(key, index);
    })
    return keyMap;
};

/**
 * 获取字段关键字
 * @param {string} str 
 * @param {Map} map 
 * @return {Map} {same:[],differ:[]}
 */
function getSearchRoleMap(str, map) {
    let roleMap = new Map();
    let sameArr = [], differArr = [];
    for (const [key, value] of map) {
        if (!str.includes(key)) {
            differArr.push(...value);
            continue;
        }
        sameArr.push(...value);
    }
    sameArr = [...new Set(sameArr)];
    _.remove(differArr, (val) => sameArr.includes(val));
    roleMap.set('same', sameArr);
    roleMap.set('differ', [...new Set(differArr)]);
    return roleMap;
};


class Es {
    constructor({ dresInfo, searchToken }) {
        this.dresInfo = dresInfo;
        this.searchToken = searchToken;
        this.flag = true;
    }

    /**
     * 通用校验
     * @description 
     * 1. 目前只记录出现互斥词的情况, 不带同义词先不记录
     * 2. 带关键字 则不判断互斥词
     * @param {Map} roleMap 关键字规则
     */
    checkTitle(roleMap) {
        const sameArr = roleMap.get('same');
        if (!this.flag || sameArr.length < 1) return;

        // 是否包含关键字
        const sameFlag = sameArr.find((key) => this.dresInfo.title.includes(key));
        if (sameFlag) return;//带关键字 则不判断互斥词

        // 是否包含互斥词
        const differFlag = roleMap.get('differ').find((key) => this.dresInfo.title.includes(key));
        if (differFlag) this.flag = false;
    }

    /**
     * 性别校验
     * @param {Array} masterClass 关联主营类目列表
     */
    checkSex(masterClassList) {
        // 无性别关键字跳过
        if (!this.flag || masterClassList.length < 1) return;
        const map = findKeysMap(this.dresInfo.masterClassName, masterClassList);
        if (map.size < 1) this.flag = false;
    }

    /**
     * 商品类别校验
     * title或者类别
     * @param {Map} roleMap 关键字规则
     */
    checkDresClass(roleMap) {
        const sameArr = roleMap.get('same');
        if (!this.flag || sameArr.length < 1) return;

        // 是否包含关键字
        const sameFlag = sameArr.find((key) => this.dresInfo.title.includes(key) || this.dresInfo.className == key || (this.dresInfo.esProps && this.dresInfo.esProps.className2 == key || this.dresInfo.esProps.className3 == key || this.dresInfo.esProps.className4 == key));
        if (sameFlag) return;//带关键字 则不判断互斥词

        // 是否包含互斥词
        const differFlag = roleMap.get('differ').find((key) => this.dresInfo.title.includes(key) || this.dresInfo.className == key || (this.dresInfo.esProps && this.dresInfo.esProps.className2 == key || this.dresInfo.esProps.className3 == key || this.dresInfo.esProps.className4 == key));
        if (differFlag) this.flag = false;
    }

};




