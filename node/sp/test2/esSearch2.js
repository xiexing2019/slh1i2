const spReq = require('../help/spReq');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const spDoc = require('../../reqHandler/sp/doc_server/doc');

(async function () {
    // 数据记录
    const tabularData = [];

    await spReq.spClientLogin();

    //查询50个商品用于查询详情
    const dresList = await spdresb.searchDres({ pageSize: 50, pageNo: 1, queryType: 0, shadowFlag: 0 }).then(res => res.result.data.dresStyleResultList);

    for (let index = 0; index < dresList.length; index++) {
        const dres = dresList[index];
        const data = { spuId: dres.id };

        // 查询商品详情
        await spReq.getFullForBuyerByUrl({ detailUrl: dres.detailUrl });

        // 获取文件内容
        const promises = [];
        JSON.parse(dres.docContent).forEach((docInfo) => {
            if (docInfo.typeId == 1) promises.push(spDoc.getDocStream({ id: docInfo.docId }));
        });
        await Promise.all(promises).then(list => {
            list.forEach((res, index) => {
                data[`图片${index}`] = res.duration;
            });
        });

        tabularData.push(data);
    }

    console.log(`\n\n商品详情查询+读取图片`);
    console.table(tabularData);//

})();