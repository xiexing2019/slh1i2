const fs = require('fs');
const path = require('path');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spbi = require('../../../reqHandler/sp/global/spbi');
const esSearchHelp = require('../../help/esSearchHelp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

(async function () {
    const shopList = fs.readFileSync(path.join(__dirname, 'shop.txt')).toString().split('\n');
    // console.log(shopList);

    await spAuth.staffLogin();

    // 
    for (let index = 0; index < shopList.length; index++) {
        try {
            await common.delay(500);
            const name = shopList[index];
            console.log(`\n\nname=${name}`);
            const shop = await spugr.findAllShopByParams({ name, pageSize: 20, pageNo: 1, shadowFlag: 0 })
                .then(res => res.result.data.rows);
            if (shop.length == 1) {
                const tenantId = shop[0].tenantId;
                console.log(`tenantId=${tenantId}`);
                await spdresb.hideAllDresSpu({ id: tenantId, hideFlag: 0 });
                console.log(`完成`);
            } else {
                console.log('%j', shop);
            }
        } catch (error) {
            console.log(error);
        }
    }

})()