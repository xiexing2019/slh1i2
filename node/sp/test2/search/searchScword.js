const moment = require('moment');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spconfg = require('../../../reqHandler/sp/global/spconfg');
const dingtalkRobot = require('../../help/dingtalkRobot');

(async function () {
    let checkMsg = '', errNum = 0;
    const isFirst = moment().isBetween(`${moment().format('YYYY-MM-DD')} 09:00:00`, `${moment().format('YYYY-MM-DD')} 09:10:00`, null, '[)');

    // 搜索专用 防止与其他脚本冲突
    await spReq.spClientLogin({ code: '12190507002' });
    const scwordList = await getScwords();

    for (let index = 0; index < scwordList.length; index++) {
        const { scword, expectNum } = scwordList[index];
        try {
            let count = 0;
            await spdresb.searchDres({ queryType: 0, pageNo: 1, pageSize: 5, searchToken: scword, shadowFlag: 2 }).then(res => count = res.result.data.count);
            if (count == 0 || count < expectNum) {
                errNum++;
                checkMsg += `\n${scword} (期望结果数:${expectNum}, 实际结果数${count})`;
            }
        } catch (error) {
            console.log(error);
        }
    }
    // 
    let content = `搜索自检结果:  搜索关键词总数${scwordList.length}\n异常结果数:${errNum}${isFirst ? '\n(自检程序每10分钟执行一次, 当日无异常不再继续播报, 如有异常立即提醒)' : ''}`;
    if (isFirst || errNum > 0) {
        if (errNum > 0) content = `${content} \n异常关键词: ${checkMsg} `;
        //appendSpOnlineMsg appendEsSearchMsg
        await dingtalkRobot.appendEsSearchMsg({
            msgtype: 'text',
            text: { content: content },
        });

        await dingtalkRobot.appendSpOnlineMsg({
            msgtype: 'text',
            text: { content: content },
        });
    }


})();

/**
 * 获取所有自检关键词
 */
async function getScwords() {
    const scwordList = [];
    let totalPageNo = 0;

    await spconfg.getSpSearchScwordList({ pageSize: 20, pageNo: 1 })
        .then(res => {
            const data = res.result.data;
            totalPageNo = Math.ceil(data.total / 20);
            scwordList.push(...data.rows);
        });

    for (let pageNo = 2; pageNo <= totalPageNo; pageNo++) {
        await spconfg.getSpSearchScwordList({ pageSize: 20, pageNo })
            .then(res => {
                scwordList.push(...res.result.data.rows);
            });

    }

    return scwordList;
};