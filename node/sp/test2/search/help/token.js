const common = require('../../../../lib/common');
const format = require('../../../../data/format');
const EventEmitter = require('events').EventEmitter;
const elasticsearch = require('../../../../reqHandler/sp/elasticsearch');

class SearchToken extends EventEmitter {
    constructor(params) {
        super();

        /** 搜索文本 */
        this.text = params.searchToken;
        /** 分词 */
        this.tokens = [];
        /** 一级类目 */
        this.firstClass = params.firstClass;
        /** 标准类目(主营类目过滤) */
        this.standardClassList = {};// new Set();
        this.city = '';
        this.trade = [];
        this.marketList = new Set();


    }

    /**
     * elasticsearch 分词
     */
    async analyze() {
        this.tokens = await elasticsearch.analyze(this.text).then(res => res.map(ele => ele.token));
        console.log(`tokens=${JSON.stringify(this.tokens)}`);
        return this;
    }

    /**
     * 获取一级类目范围
     * @description 
     * 1. 输入包含男不含童和女
     * 2. 输入包含女不含童和男
     * 3. 童是第一优先级 比如输入女童就是童，有童只按照童处理
     * 4. 同时有男女 按男 女 都搜处理 
     * 5. 不带性别关键字 则按主营/副营类目匹配
     * @param {object[]} [classList] 
     */
    getFirstClassByToken(classList = ['女装']) {
        // 性别字段仅在类别判断时
        _.remove(this.tokens, (token) => ['男', '男装', '女', '女装', '童', '童装'].includes(token));

        if (this.text.includes('童')) {
            _.remove(this.firstClass, (val) => val == '男装' || val == '女装');
            return this;
        }

        if (this.text.includes('男') && this.text.includes('女')) {
            _.remove(this.firstClass, (val) => val == '童装');
            return this;
        };

        if (this.text.includes('男')) {
            _.remove(this.firstClass, (val) => val == '童装' || val == '女装');
            return this;
        };

        if (this.text.includes('女')) {
            _.remove(this.firstClass, (val) => val == '童装' || val == '男装');
            return this;
        };

        this.firstClass = classList;
        return this;
    }

    matchShop() {

    }

    /**
     * 匹配商圈
     * @param {object} tradeList 
     */
    matchTrade(tradeList) {
        this.trade = common.takeWhile(tradeList, (trade) => {
            return this.text == trade.name || this.tokens.includes(trade.name);
        });
        this.trade.forEach(trade => trade.marketList.forEach(market => this.marketList.add(market)));
        console.log('匹配到商圈:%j', this.trade);
        return this;
    }

    /**
     * 匹配市场
     * @param {object} marketList 
     */
    matchMarket(marketList) {
        if (this.trade.length > 0) return this;

        const _marketList = common.takeWhile(marketList, (market) => {
            return this.text == market.name || this.tokens.includes(market.name);
        });
        _marketList.forEach(market => this.marketList.add(market))
        console.log('匹配到市场:%j', _marketList);
        return this;
    }

    /**
     * 匹配类别
     * @param {object} dresClassTree 
     */
    matchClass(dresClassTree) {
        console.log('一级类目范围:%j', this.firstClass);
        const classIds = [], alias = [];
        this.firstClass.forEach(level1Name => {
            const level1Class = dresClassTree[level1Name];
            const matchLevel1Name = matchClassAlias({ classInfo: level1Class, searchToken: this.text });
            // 若匹配到 则为当前类别与其子类别
            // 匹配不到 子类别再进行判断
            if (matchLevel1Name.isMatch) {
                const json1 = getAllClassIds({ classInfo: level1Class });
                classIds.push(...json1.classIds);
                alias.push(...json1.alias);
            } else {
                level1Class.subItems.forEach(level2Class => {
                    const matchLevel2Name = matchClassAlias({ classInfo: level2Class, searchToken: this.text });
                    if (matchLevel2Name.isMatch) {
                        const json2 = getAllClassIds({ classInfo: level2Class });
                        classIds.push(...json2.classIds);
                        alias.push(...json2.alias);
                    } else {
                        level2Class.subItems.forEach(level3Class => {
                            const matchLevel3Name = matchClassAlias({ classInfo: level3Class, searchToken: this.text });
                            if (matchLevel3Name.isMatch) {
                                const json3 = getAllClassIds({ classInfo: level3Class });
                                classIds.push(...json3.classIds);
                                alias.push(...json3.alias);
                            }
                        });
                    }
                });
            }
        });
        // return { classIds: common.dedupe(classIds), alias: common.dedupe(alias) };
        this.standardClassList = { classIds: common.dedupe(classIds), alias: common.dedupe(alias) };
        console.log('标准类目范围:%j', this.standardClassList);

        return this;
    }

    /**
     * 校验商品
     * @description 
     * @param {object} dres 
     */
    checkDres(dres) {
        const _tokens = [...this.tokens];

        // 店铺校验 (后续再优化校验方式 目前暂无法准确判断搜索词是否为店铺)
        if (this.text == dres.tenantName) return;
        _.remove(_tokens, (token) => token == dres.tenantName);

        // 类别
        if (this.standardClassList.classIds.includes(dres.classId)) {
            _.remove(_tokens, (token) => this.standardClassList.alias.includes(token));
        }

        // 城市
        _.remove(_tokens, (token) => dres.city.includes(token));

        // 商圈 市场
        const [trade, market = ''] = dres.market.split(/\s/);
        if ([trade, market].includes(this.text)) return;
        _.remove(_tokens, (token) => [trade, market].includes(token));

        // 货品标签
        const labelItemCaptions = dres.labelItemCaptions.split(/\s/);
        _.remove(_tokens, (token) => labelItemCaptions.includes(token));

        // 商品标签
        const labelNames = dres.labelNames;
        _.remove(_tokens, (token) => labelNames.includes(token));

        // 标题
        _.remove(_tokens, (token) => dres.title.includes(token));

        // 名称
        _.remove(_tokens, (token) => dres.name == token);

        if (_tokens.length > 0) {
            console.log('%j\n%j', _tokens, format.dataFormat(dres, 'spuId;title;name;tenantId;tenantName;className;labelItemCaptions;labelNames;market;city'));
        }
    }



}

module.exports = SearchToken;

/**
 * 搜索关键词中是否包含类别名称或别名
 * @param {objec} classInfo 
 * @param {string} searchToken 
 */
function matchClassAlias({ classInfo, searchToken }) {
    // 关键词包含任意一个类别名称 即为匹配
    const data = { isMatch: classInfo.alias.some(name => searchToken.toLowerCase().includes(name.toLowerCase())) };
    if (data.isMatch) {
        // data.alias = classInfo.alias;
        console.log(`类别:${classInfo.typeName} 类别id:${classInfo.id} 匹配范围:${classInfo.alias}`);
    }
    return data;
};

/**
 * 获取当前类别与其子类别的所有id
 */
function getAllClassIds({ classInfo }) {
    const json = { classIds: [classInfo.id], alias: classInfo.alias };
    if (classInfo.hasOwnProperty('subItems')) {
        classInfo.subItems.forEach(subItem => {
            const _json = getAllClassIds({ classInfo: subItem });
            json.classIds.push(..._json.classIds);
            json.alias.push(..._json.alias);
        });
    }
    return json;
};