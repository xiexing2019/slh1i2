const fs = require('fs');
const path = require('path');
const elasticsearch = require('../../../reqHandler/sp/elasticsearch');
// const mysql = require('../../../reqHandler/sp/mysql');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spbi = require('../../../reqHandler/sp/global/spbi');
const esSearchHelp = require('../../help/esSearchHelp');

/**
 * searchToken  
 * 1.是否包含性别关键词(男，女，童)  筛选类目范围
 * 2.对token进行分词  匹配按 类目，标签，标题的顺序进行匹配度校验
 * searchToken  取每日搜索关键词的前100个  校验对应搜索结果的前100个商品
 * 
 */

/**
 * 最大搜索页数
 */
const maxPageNum = 5;

/**
 * es搜索默认参数
 */
const esDefParams = { queryType: 0, pageSize: 20, shadowFlag: 2 };

// 日志地址
const searchLogPath = path.join(__dirname, '/log/搜索记录2.txt'),
    dresLogPath = path.join(__dirname, '/log/执行日志2.txt');
// 删除老的日志
try {
    fs.unlinkSync(searchLogPath);
} catch (error) { } finally {
    appendLog('搜索记录 已删除');
};
try {
    fs.unlinkSync(dresLogPath);
} catch (error) { } finally {
    appendLog('款号记录 已删除');
};

(async () => {
    await spReq.spClientLogin();

    // 查询商品类别树 
    const dresClassTree = {};
    await spdresb.findClassTreeList({ incDirectSub: true }).then(res => {
        // 2级类目
        res.result.data.rows.forEach(level1 => {
            const classInfo = {
                typeName: level1.typeName,
                alias: getAlias({ name: level1.typeName, alias: level1.alias }),
                id: level1.id,
                subItems: [],
            };
            classInfo.subItems = level1.subItems.map(level2 => {
                const subItems = level2.subItems.map(level3 => { return { id: level3.id, typeName: level3.typeName, alias: getAlias({ name: level3.typeName, alias: level3.alias }) } });
                return {
                    typeName: level2.typeName,
                    alias: getAlias({ name: level2.typeName, alias: level2.alias }),
                    id: level2.id,
                    subItems
                }
            });
            dresClassTree[level1.typeName] = classInfo;
        });
    });
    // console.log(dresClassTree);

    // 搜索词统计明细
    const searchHistoryList = await spbi.findSpSearchHistoryDetailByToken({ pageNo: 1, pageSize: 100, orderBy: 'searchTimes' }).then(res => res.result.data.rows);

    // searchHistoryList.length
    for (let index = 0; index < 2; index++) {
        await common.delay(500);
        const searchCheck = new SearchCheck(searchHistoryList[index]);

        // 计算最大搜索页数 
        const totalPageNo = Math.min(maxPageNum, Math.ceil(common.div(searchHistoryList[index].maxSearchNum, 20)));

        // elasticsearch 分词
        await searchCheck.analyze();


        try {
            for (let pageNo = 1; pageNo <= totalPageNo; pageNo++) {
                await spdresb.searchDres({ pageNo, searchToken: searchCheck.searchToken, ...esDefParams }).then(res => {
                    // 取关键字段
                    res.result.data.dresStyleResultList.forEach(dres => {
                        const { id, classId, className, labelItemIds, labelItemCaptions, title, detailUrl } = dres;
                        searchCheck.addDres({ id, className, labelItemCaptions, title, detailUrl });
                    });
                });
            }

            // 匹配类别
            searchCheck.matchClass(dresClassTree);
            // 匹配标签
            searchCheck.matchLabel();
            // 匹配标题
            searchCheck.matchTitle();
            // 校验
            searchCheck.check();
        } catch (error) {
            appendLog(`\n\nsearchToken=${searchCheck.searchToken}`);
            appendLog(error);
        }
    }


})();


/**
 * 追加搜索日志
 * @param {string} msg 
 */
function appendSearchLog(msg) {
    fs.appendFileSync(searchLogPath, `\n${msg}`);
};


/**
 * 追加执行日志
 * @param {string} msg 
 */
function appendLog(msg) {
    fs.appendFileSync(dresLogPath, `\n${msg}`);
};

/**
 * alias转数组
 * @param {string} aliasStr 
 */
function getAlias({ name, alias }) {
    if (!alias) return [name];
    const arr = alias.split(/,|，|、/);
    arr.push(name);
    return common.dedupe(arr); // |\/ 
};


/**
 * 根据搜索关键字 排除性别不匹配的类别
 * @description
 * 1. 输入包含男不含童和女
 * 2. 输入包含女不含童和男
 * 3. 童是第一优先级 比如输入女童就是童，有童只按照童处理
 * 4. 同时有男女 按男 女 都搜处理
 */
function getLevel1ByToken(dresClassTree, searchToken) {
    const level1 = Object.keys(dresClassTree);

    if (searchToken.includes('童')) {
        _.remove(level1, (val) => val == '男装' || val == '女装');
        return level1;
    };

    if (searchToken.includes('男') && searchToken.includes('女')) {
        _.remove(level1, (val) => val == '童装');
        return level1;
    };

    if (searchToken.includes('男')) {
        _.remove(level1, (val) => val == '童装' || val == '女装');
        return level1;
    };

    if (searchToken.includes('女')) {
        _.remove(level1, (val) => val == '童装' || val == '男装');
        return level1;
    };

    return level1;
};

/**
 * 根据搜索关键词 获取匹配的类别数组
 * @param {object} params 
 * @param {object} params.dresClassTree
 * @param {Array} params.level1Names 符合条件的1级类目名称(根据性别筛选)
 * @param {string} params.searchToken
 * @return {Array} 
 */
function getClassIds({ dresClassTree, level1Names, searchToken }) {
    let classNames = [], alias = [];
    level1Names.forEach(level1Name => {
        const level1Class = dresClassTree[level1Name];
        const matchLevel1Name = matchClassAlias({ classInfo: level1Class, searchToken });
        // 若匹配到 则为当前类别与其子类别
        // 匹配不到 子类别再进行判断
        if (matchLevel1Name.isMatch) {
            const json1 = getAllClassNames({ classInfo: level1Class });
            classNames.push(...json1.classNames);
            alias.push(...json1.alias);
        } else {
            level1Class.subItems.forEach(level2Class => {
                const matchLevel2Name = matchClassAlias({ classInfo: level2Class, searchToken });
                if (matchLevel2Name.isMatch) {
                    const json2 = getAllClassNames({ classInfo: level2Class });
                    classNames.push(...json2.classNames);
                    alias.push(...json2.alias);
                } else {
                    level2Class.subItems.forEach(level3Class => {
                        const matchLevel3Name = matchClassAlias({ classInfo: level3Class, searchToken });
                        if (matchLevel3Name.isMatch) {
                            const json3 = getAllClassNames({ classInfo: level3Class });
                            classNames.push(...json3.classNames);
                            alias.push(...json3.alias);
                        }
                    });
                }
            });
        }
    });

    return { classNames: common.dedupe(classNames), alias: common.dedupe(alias) };
};

/**
 * 搜索关键词中是否包含类别名称或别名
 * @param {objec} classInfo 
 * @param {string} searchToken 
 */
function matchClassAlias({ classInfo, searchToken }) {
    // 关键词包含任意一个类别名称 即为匹配
    const data = { isMatch: classInfo.alias.some(name => searchToken.toLowerCase().includes(name.toLowerCase())) };
    if (data.isMatch) {
        // data.alias = classInfo.alias;
        appendLog(`匹配类别:${classInfo.typeName} 类别id:${classInfo.id} 匹配范围:${classInfo.alias}`);
    }
    return data;
};

/**
 * 获取当前类别与其子类别的所有类别名称
 */
function getAllClassNames({ classInfo }) {
    const json = { classNames: [classInfo.typeName], alias: classInfo.alias };
    if (classInfo.hasOwnProperty('subItems')) {
        classInfo.subItems.forEach(subItem => {
            const _json = getAllClassNames({ classInfo: subItem });
            json.classNames.push(..._json.classNames);
            json.alias.push(..._json.alias);
        });
    }
    return json;
};


class SearchCheck {
    constructor(params) {
        appendSearchLog(`searchToken=${params.searchToken}\n\tsearchTimes=${params.searchTimes} minSearchNum=${params.minSearchNum} maxSearchNum=${params.maxSearchNum}`);
        appendLog(`\n\nsearchToken=${params.searchToken}`)

        /** 搜索关键词 */
        this.searchToken = params.searchToken;
        /** 分词结果 */
        this.tokens = [];
        /** 商品列表 */
        this.dresList = [];
    }

    /**
     * elasticsearch 分词
     */
    async analyze() {
        const tokens = await elasticsearch.analyze(this.searchToken).then(res => res.map(ele => ele.token));
        appendLog(`分词结果 ${JSON.stringify(tokens)}`);
        this.tokens = tokens;
    }

    /**
     * 添加商品
     * @param {object} dres 
     */
    addDres(dres) {
        dres.sortBit = 0;
        dres.tokens = _.cloneDeep(this.tokens);
        this.dresList.push(dres);
        return this;
    }

    removeToken({ tokenList, token }) {
        _.remove(tokenList, (val) => val == token);
    }

    matchClass(dresClassTree) {
        const level1Names = getLevel1ByToken(dresClassTree, this.searchToken);
        appendLog(`类别范围:${JSON.stringify(level1Names)}`);

        const classRange = getClassIds({ dresClassTree, level1Names, searchToken: this.searchToken });
        // appendLog(`classRange=${JSON.stringify(classRange)}`);
        if (classRange.classNames.length > 0) {
            this.dresList.forEach(dres => {
                dres.tokens.forEach(token => {
                    if (classRange.alias.includes(token)) {
                        this.removeToken({ tokenList: dres.tokens, token });
                        dres.sortBit += 100;
                    }
                });
            });
        }
    }

    matchLabel() {
        this.dresList.forEach(dres => {
            if (dres.labelItemCaptions && dres.labelItemCaptions != 0) {
                const labels = dres.labelItemCaptions.split(' ');
                const labelTokens = common.takeWhile(dres.tokens, (token) => labels.includes(token));
                _.remove(dres.tokens, (token) => labelTokens.includes(token));
                dres.sortBit += labelTokens.length * 10;
            }
        });
    }

    matchTitle() {
        this.dresList.forEach(dres => {
            const titleTokens = common.takeWhile(dres.tokens, (token) => dres.title.includes(token));
            _.remove(dres.tokens, (token) => titleTokens.includes(token));
            dres.sortBit += titleTokens.length;
        });
    }

    check() {

        this.dresList.forEach(dres => {
            appendLog(JSON.stringify(dres));
            if (dres.tokens.length > 0) {
                appendSearchLog(`id=${dres.id} 含有未匹配的分词 ${dres.tokens}`);
            }
        });

        esSearchHelp.orderAssert({ dataList: this.dresList, path: 'sortBit', orderByDesc: true });
    };


}

