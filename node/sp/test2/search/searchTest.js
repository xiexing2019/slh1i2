const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');

(async function () {
    const ids = [2288344, 2361896, 11038203];
    await spReq.spClientLogin();
    const res = await spdresb.searchDres({ queryType: 0, shadowFlag: 2, keyWords: { id: ids } });
    console.log('%j', res);
})();