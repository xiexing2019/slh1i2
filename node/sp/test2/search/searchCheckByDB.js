const fs = require('fs');
const path = require('path');
const mysql = require('../../../reqHandler/sp/mysql');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const SearchToken = require('./help/token');

// dresb搜索 校验es
// 匹配字段 title className 李果那不对classId进行判断

// 日志地址
const searchLogPath = path.join(__dirname, '/log/搜索记录.txt'),
    dresLogPath = path.join(__dirname, '/log/款号记录.txt');
// 删除老的日志
try {
    fs.unlinkSync(searchLogPath);
} catch (error) { } finally {
    console.log('搜索记录 已删除');
};
try {
    fs.unlinkSync(dresLogPath);
} catch (error) { } finally {
    console.log('款号记录 已删除');
};

/** 搜索关键词 */
const searchTokens = [...new Set(fs.readFileSync(path.join(__dirname, '/data/searchToken.txt')).toString().split(`\n`))];
// console.log(searchTokens);

/** 商品表 */
const tableArr = ['spdresb001', 'spdresb002', 'spdresb003', 'spdresb004', 'spdresb005', 'spdresb006', 'spdresb007', 'spdresb008', 'spdresb009', 'spdresb010'];

/** 关键词影响范围 */
const searchRange = `CONCAT(code,title,name,labels,props)`;

(async function () {
    const dresbMySql = await mysql({ dbName: 'spdresb' });

    await spReq.spClientLogin();

    // 查询商品类别树 
    const dresClassTree = {};
    await spdresb.findClassTreeList({ incDirectSub: true }).then(res => {
        // 2级类目
        res.result.data.rows.forEach(level1 => {
            const classInfo = {
                typeName: level1.typeName,
                alias: getAlias({ name: level1.typeName, alias: level1.alias }),
                id: level1.id,
                subItems: [],
            };
            classInfo.subItems = level1.subItems.map(level2 => {
                const subItems = level2.subItems.map(level3 => { return { id: level3.id, typeName: level3.typeName, alias: getAlias({ name: level3.typeName, alias: level3.alias }) } });
                return {
                    typeName: level2.typeName,
                    alias: getAlias({ name: level2.typeName, alias: level2.alias }),
                    id: level2.id,
                    subItems
                }
            });
            dresClassTree[level1.typeName] = classInfo;
        });
    });
    const firstClass = Object.keys(dresClassTree);
    console.log('一级类目:%j', firstClass);

    // console.log(`dresClassTree=${JSON.stringify(dresClassTree)}`);

    const tradeList = await getAllTrades();
    console.log(`商圈总数 ${tradeList.length}`);
    const marketList = _.flatten(tradeList.map(trade => trade.marketList));
    console.log(`市场总数 ${marketList.length}`);

    // 防止出现性能问题 不并发
    for (let index = 0; index < searchTokens.length; index++) {
        const time = Date.now();
        try {
            const searchToken = new SearchToken({ searchToken: searchTokens[index], firstClass: firstClass });
            console.log(`\n\nsearchToken=${searchToken.text}`);
            appendSearchLog(`\n\nsearchToken=${searchToken.text}`);

            // elasticsearch 分词
            await searchToken.analyze();
            // 确认关键词中带有的 类别/商圈/市场 信息
            searchToken.getFirstClassByToken()
                .matchClass(dresClassTree)
                .matchTrade(tradeList)
                .matchMarket(marketList);

            // 匹配店铺
            // const shopList = await spugr.findShopBySearchToken({ searchToken, shadowFlag: 2 }).then(res => res.result.data.rows);
            // console.log(`shopList=${JSON.stringify(shopList)}`);
            // const shop = shopList.find(val => val.name == searchToken);

            // // 直接匹配 所有关键词都需要出现
            // const result1 = await dresbQuery({ dresbMySql, queryCond: '', tokens });
            // const searchResult1 = result1.spuIds;

            // // 获取关键字中匹配的商品类别
            // // 出现在类目中的关键词 不必一定要再出现在其他字段中
            // let searchResult2 = [];
            // const level1Names = getLevel1ByToken(dresClassTree, searchToken);
            // const classRange = getClassIds({ dresClassTree, level1Names, searchToken });
            // if (classRange.classIds.length > 0) {
            //     let classIdCondition = '';
            //     const classIdsStr = JSON.stringify(classRange.classIds).replace(/\[|\]/g, '');
            //     classIdCondition = ` AND class_id IN (${classIdsStr})`;

            //     // 去除类目中已经出现的关键词
            //     _.remove(tokens, (token) => classRange.alias.some(name => name.toLowerCase() == token.toLowerCase()));
            //     _.remove(tokens, (token) => ['男', '女', '童'].includes(token));// 匹配到类别时，去除性别  暂时这样处理

            //     const result2 = await dresbQuery({ dresbMySql, queryCond: classIdCondition, tokens });
            //     searchResult2 = result2.spuIds;
            // }

            // // 合并去重 2种查询方式的结果
            // const databaseSpuIds = common.dedupe(_.concat(searchResult1, searchResult2));

            // // console.log(`databaseSpuIds=${JSON.stringify(databaseSpuIds)}`);
            // appendSearchLog(`搜索结果`);
            // appendSearchLog(`   数据库 商品总数=${databaseSpuIds.length}`);


            // es搜索
            const esParams = { queryType: 0, searchToken: searchToken.text, pageSize: 100, shadowFlag: 2 },
                esSpuIds = [];
            let totalPageNo = 0;
            const esSearchNum = await spdresb.searchDres({ pageNo: 1, ...esParams }).then(res => {
                totalPageNo = Math.ceil(common.div(res.result.data.count, 100));
                // console.log(`dres=${JSON.stringify(res.result.data.dresStyleResultList[0])}`);
                res.result.data.dresStyleResultList.forEach(dres => {
                    esSpuIds.push(dres.id);
                    searchToken.checkDres(dres);
                });
                return res.result.data.count;
            });
            // for (let pageNo = 2; pageNo <= totalPageNo; pageNo++) {
            //     await spdresb.searchDres({ pageNo, ...esParams }).then(res => {
            //         res.result.data.dresStyleResultList.forEach(dres => esSpuIds.push(dres.id));
            //     });
            // }
            console.log(`totalPageNo=${totalPageNo}`);
            appendSearchLog(`   es搜索 商品总数=${esSearchNum}`);


            // // 匹配度校验
            // dresCheck({ searchToken, databaseSpuIds, esSpuIds });

        } catch (error) {
            console.log(error);
        } finally {
            const duration = Date.now() - time;
            console.log(`duration=${duration}`);
        }
    };

    await dresbMySql.end();

})();

/**
 * 追加搜索日志
 * @param {string} msg 
 */
function appendSearchLog(msg) {
    fs.appendFileSync(searchLogPath, `\n${msg}`);
};

/**
 * alias转数组
 * @param {string} aliasStr 
 */
function getAlias({ name, alias }) {
    if (!alias) return [name];
    const arr = alias.split(/,|，|、/);
    arr.push(name);
    return common.dedupe(arr); // |\/ 
};


//
async function dresbQuery({ dresbMySql, queryCond, tokens }) {
    // 搜索范围 
    let searchRangeCondition = '';
    tokens.forEach(token => searchRangeCondition += ` AND ${searchRange} LIKE '%${token}%'`);
    console.log(`tokens=${JSON.stringify(tokens)}\n`);

    const spuIds = [];
    const promises = tableArr.map(tableName => dresbMySql.query(`SELECT id FROM ${tableName}.dres_spu WHERE flag=1 AND hide_flag=0 ${queryCond} ${searchRangeCondition}`));
    await Promise.all(promises).then(res => {
        res.forEach(data => {
            const [rows] = data;
            rows.forEach(row => spuIds.push(row.id));
        });
    });
    return { spuIds };
};

/**
 * 匹配度校验
 */
function dresCheck({ searchToken, databaseSpuIds, esSpuIds }) {
    const sameSpuIds = _.intersection(databaseSpuIds, esSpuIds);
    const diffDatabaseSpuIds = _.difference(databaseSpuIds, sameSpuIds),
        diffEsSpuIds = _.difference(esSpuIds, sameSpuIds);
    const msg = `\n\n\n${searchToken}\n\t数据库:${JSON.stringify(diffDatabaseSpuIds)}\n\n\tes搜索:${JSON.stringify(diffEsSpuIds)}\n\n\t相同:${JSON.stringify(sameSpuIds)}`;
    appendSearchLog(`匹配结果`);
    appendSearchLog(`   数据库 特有数量:${diffDatabaseSpuIds.length}`);
    appendSearchLog(`   es搜索 特有数量:${diffEsSpuIds.length}`);
    appendSearchLog(`   相同商品数量:${sameSpuIds.length}`);
    fs.appendFileSync(dresLogPath, msg);
};


async function getAllTrades() {
    const pageSize = 50;

    function getTrade(data) {
        return {
            id: data.id,
            name: data.typeName,
            marketList: data.marketList.map(val => { return { id: val.id, name: val.typeName } })
        }
    }

    let totalPageNo = 0;
    const tradeList = await spCommon.getCatConfigTradeList({ flag: 1, pageSize, pageNo: 1 }).then(res => {
        totalPageNo = Math.ceil(common.div(res.result.data.count, pageSize));
        return res.result.data.rows.map(row => getTrade(row));
    });

    for (let pageNo = 2; pageNo <= totalPageNo; pageNo++) {
        await spCommon.getCatConfigTradeList({ flag: 1, pageSize, pageNo })
            .then(res => tradeList.push(...res.result.data.rows.map(row => getTrade(row))));
    }
    return tradeList;
};

