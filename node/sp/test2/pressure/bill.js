const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spSql = require('../../../reqHandler/sp/mysql');
const base = require('../../pressure/base');
const { spawn } = require('child_process');
const readline = require('readline');


let suit = [];
const sellerMobile = '13750850013';
const userNum = 2;

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: '请输入用户数量:',
});

rl.prompt();

rl.on('line', async function () {
    const spgSql = await spSql({ dbName: 'spgMysql' });
    const [clientList] = await spgSql.query(`SELECT a.mobile FROM spugr.sp_tenant a,spugr.sp_unit b WHERE a.id=b.tenant_id AND a.type_id=1 AND a.mobile LIKE '12%' AND LENGTH(a.mobile)=11 AND b.flag=1 ORDER BY a.created_date DESC`);
    const [sellerList] = await spgSql.query(`SELECT id AS tenantId,mobile FROM spugr.sp_tenant WHERE mobile=${sellerMobile} AND audit_flag=1 AND flag=1 ORDER BY created_date DESC`);
    await spgSql.end();

    // 初始化
    suit = new Array(userNum).fill({}).map((val, index) => new base.SuitCase({ client: clientList[index], seller: sellerList[index] }));

    // 买家登录
    let promise = suit.map(async (val) => val.client.spLogin());
    await Promise.all(promise);

    // 卖家登录 同一个手机号,需要一个个切换登录
    for (let index = 0; index < suit.length; index++) {
        await suit[index].seller.spSellerLogin();
    }

    // 获取商品需要的字典信息
    const classInfo = await spdresb.findByClass({ classId: 1085 }).then(res => res.result.data);
    const arr = ['606', '850', '2001', '2002', '2003', '2004'];
    classInfo.spuProps.forEach(element => arr.push(element.dictTypeId));
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
            .then((res) => res.result.data.rows);
    };

    // 商品维护
    promise = suit.map(async (val) => val.seller.checkDres({ classInfo }));
    await Promise.all(promise);

    // 测试脚本
    promise = suit.map(async (val) => val.testMain());
    await Promise.all(promise);
});


rl.on('close', async function (params) {

});

