const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spIM = require('../../../reqHandler/sp/biz_server/spIM');
const spmdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spAct = require('../../../reqHandler/sp/global/spAct');
const spbi = require('../../../reqHandler/sp/global/spbi');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

const reqNum = 300;

const list = ['spconfb.findFeeRuleList()'
    , 'spdresb.searchDres({ queryType: 4, searchToken: common.getRandomStr(3) })'
    , 'spdresb.getDresShareList()'
    , 'spdresb.searchAssoWord({ searchToken: `商品` })'
    , 'spdresb.dresClassConfigList()'
    , 'spIM.getNimLoginInfo()'
    // , 'spmdm.getUserRecInfoList()'
    , 'spTrade.showCartList()'
    , 'spTrade.purFindBills()'
    , 'spTrade.salesFindBills()'
    , 'spUp.getFavorSpuList()'
    , 'spAct.getActList({ queryFrom: 2 })'
    , 'spbi.getBuyersAnalysis({ timeKind: 1 })'
    , 'spCommon.findCatConfig({ type: 7 })'
    , 'spCommon.findSpGoodShops({ flag: 1 })'];

(async function () {

    await spReq.spClientLogin();
    // console.log(LOGINDATA);


    const promises = getFuncList(list, reqNum).map((cond) => eval(cond));
    const result = await Promise.all(promises);
    result.forEach((res) => {
        console.log(`opTime:${res.opTime},   duration:${res.duration},   apiKey:${res.params.apiKey}`);
    })

})();

function getFuncList(list, reqNum) {
    list = common.randomSort(list);
    const concatNum = Math.ceil(reqNum / list.length);
    for (let index = 1; index < concatNum; index++) {
        list = list.concat(list);
    }
    return list.slice(0, reqNum);
};