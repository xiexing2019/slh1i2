const disReq = require('../../help/disHelp/disReq');
const disBill = require('../../help/disHelp/disBill');
const disManage = require('../../help/disHelp/disManage');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');

(async function () {

    await disReq.userLogin({ code: '15757102016' });
    const userInfo = disManage.setByLoginData(LOGINDATA);
    console.log(userInfo);

    await disManage.prepareRecAddress(userInfo);

    const spDisBill = disBill.spDisBillManage({ userInfo });

    // 默认收货地址 需要维护
    await spDisBill.getRecAddressInfo();

    // 搜索商品
    const dresList = await disReq.searchDres({ pageSize: 20, pageNo: 1 }).then(res => res.result.data.dresStyleResultList);
    // console.log(`dresList[0]=${JSON.stringify(dresList[0])}`);

    const dresFull = await disReq.getDresFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => {
        // console.log(res);
        return res.result.data;
    });//
    // console.log(dresFull);


    spDisBill.getPurJson(dresFull);

    // await spDisBill.evalDisShipFee();

    await spDisBill.saveDisPurBill();






})();