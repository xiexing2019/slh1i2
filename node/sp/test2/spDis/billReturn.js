const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const disBillManage = require('../../help/disHelp/disBill');

describe('分销下单-仅退款', async function () {
    this.timeout(30000);
    LOGINDATA = {
        sessionId: 'spugrs6-18-F2DAF155-8502-AF1E-AC49-6CDC975FE97D',
        tenantId: 95416,
        clusterCode: 'spbs7'
    }
    const billNo = '56000', backFlag = 1;

    let disBill = disBillManage.spDisBillManage(), returnBillId;
    describe('创建退款申请', async function () {
        before(async function () {
            // 查询待发货列表
            const purList = await spTrade.purFindBills({ statusType: 2, srcType: 1 }).then(res => res.result.data.rows);
            // 查询想退的单子
            const purBillId = purList.find(purBill => purBill.bill.billNo == billNo).bill.id;
            if (!purBillId) {
                throw new Error(`未找到单据 billNo=${billNo}`);
            }
            // 取第一条数据 查询单据详情
            const purBillFull = await spTrade.findPurBillFull({ id: purBillId }).then(res => res.result.data);
            console.log(`采购单详情:`);
            console.log(purBillFull);

            // 根据采购单详情 拼接退货单参数
            const returnJson = await disBill.getReturnJson({ typeId: 0, backFlag, purBillFull });
            console.log(`\nreturnJson=${JSON.stringify(returnJson)}`);

            returnBillId = await spTrade.saveReturnBill(returnJson).then(res => {
                console.log(`res=${JSON.stringify(res)}`);
                return res.result.data.val;
            });
            console.log(`\nreturnBillId=${JSON.stringify(returnBillId)}`);

        });
        it('', async function () {

        });
    });


});