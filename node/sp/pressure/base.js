const common = require('../../lib/common');
const spReq = require('../help/spReq');
const spugr = require('../../reqHandler/sp/global/spugr');
const spSql = require('../../reqHandler/sp/mysql');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../help/mockJsonParam');
const spCaps = require('../../reqHandler/sp/spCaps');

/** 用户 */
class User {
    constructor(userInfo = {}) {
        /** 手机号 */
        this.mobile = '';
        /** 集群id */
        this.clusterId = '';
        /** 租户id */
        this.tenantId = '';
        /** 会话id */
        this.sessionId = '';

        common.update(this, userInfo);
    }

    /**
     * 用户登录
     */
    async spLogin() {
        const loginInfo = await spugr.spLogin({ code: this.mobile, captcha: '000000', authcType: 2 })
            .then(res => res.result.data);
        common.update(this, loginInfo);
    }

};

/** 卖家 */
class Seller extends User {
    constructor(sellerInfo) {
        super();
        common.update(this, sellerInfo);
        this.spuIds = new Set();
    }

    /** 卖家登录 并切换到门店 */
    async spSellerLogin() {
        const loginInfo = await spugr.spLogin({ code: this.mobile, captcha: '000000', authcType: 2 }).then(res => res.result.data);
        const changeTenant = await spugr.changeTenantInSession({ tenantId: this.tenantId, sessionId: loginInfo.sessionId }).then(res => res.result.data);
        common.update(this, changeTenant);
        this.clusterId = changeTenant.clusterCode;
    }

    /**
     * 检查卖家是否有商品,无则新增
     * @description 先只用一个商品,后续再优化
     */
    async checkDres({ classId }) {
        const dresList = await spdresb.searchDres({ queryType: 4, tenantId: this.tenantId, sessionId: this.sessionId })
            .then(res => res.result.data.dresStyleResultList);
        if (dresList.length == 0) {
            await this.addDres({ classId });
            return;
        }

        this.spuIds.add(dresList[0].id);
    }

    async addDres({ classId }) {
        const json = mockJsonParam.styleJson({ classId });
        const saveRes = await spdresb.saveFull(json);
        this.spuIds.add(saveRes.result.data.spuId);
    }

};

class SuitCase {
    constructor({ client, seller }) {
        this.case = {
            state: {},
            failNum: 0,
            count: 0,
            duration: 0
        };
        this.client = new User(client);
        this.seller = new Seller(seller);
    }


    async testMain() {
        try {
            LOGINDATA = { _tid: this.client.tenantId, _cid: this.client.clusterId, sessionId: this.client.sessionId };


            const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: this.seller.spuIds.shift() } });

            const dresFull = spReq.getFullForBuyerByUrl({ detailUrl: dresList.result.data.dresStyleResultList.shift() });

            const purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresFull }));

            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });

            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

        } catch (error) {
            console.log(`\nclient tenantId=${this.client.tenantId}   seller tenantId=${this.seller.tenantId}`);
            console.log(error);
            this.case.failNum++;
        } finally {
            this.case.count++;
        }

    }
}

module.exports = {
    User,
    Seller,
    SuitCase
}