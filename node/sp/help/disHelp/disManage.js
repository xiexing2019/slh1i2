const common = require('../../../lib/common');
const spAccount = require('../../data/spAccount');
const spBank = require('../../data/spBank');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const disReq = require('../../help/disHelp/disReq');

const disManage = module.exports = {};

/**
 * 初始化渠道商
 */
disManage.setByLoginData = function (params) {
    const dis = new SpDis();
    common.update(dis, params);
    dis.disId = params.extProps.disId;
    return dis;
};

class SpDis {
    constructor() {
        /** 集群代码 */
        this.clusterCode = '';
        /** 集群id */
        this.clusterId = '';
        /** 租户id */
        this.tenantId = '';
        /** 用户id */
        this.userId = '';
        /** 单元id */
        this.unitId = '';

        /** 用户名称 */
        this.userName = '';
        /** 租户名称 */
        this.tenantName = '';
        /** 用户手机号 */
        this.mobile = '';

        /** 绑定的卖家租户id */
        this.disId = '';
        this.sourceId = spAccount.sourceEntry.autoTest.sourceId;
        this.sourceType = spAccount.sourceEntry.autoTest.sourceType;
        /** 会话id */
        this.sessionId = '';
    }
}

/**
 * 维护用户默认收货地址
 * @description 为了方便新增用户开单
 */
disManage.prepareRecAddress = async function (userInfo) {
    const defAddress = await spMdm.getUserDefaultRecInfo({ check: false }).then(res => res.result);

    // 有默认地址则返回
    if (defAddress.hasOwnProperty('data')) return;

    // 新增收货地址 并设为默认
    await spMdm.saveUserRecInfo({
        recInfo: {
            linkMan: userInfo.userName,
            isDefault: 1,//是否默认收货地址（1 是，0 不是）
            type: 1,//地址类型（1 买家收货地址；2 卖家退货地址）
            telephone: userInfo.mobile
        },
        address: {
            provinceCode: 110000,// 省编码
            cityCode: 110100,// 市编码
            countyCode: 110101,// 区县编码
            townCode: 0,// 镇街道编码
            detailAddr: common.getRandomStr(5),// 详细地址(不含省市县镇)
        }
    });

};

/**
 * 初始化合伙人
 */
disManage.setupDisPartner = function (params) {
    return new DisPartner().getRandom();
};

// 合伙人
class DisPartner {
    constructor(params) {
        /** id */
        this.id = '';
        /** 1-启用 0-禁用 -1删除 */
        this.flag = 1;
        /** 认证状态位 -1-拒绝 1-已认证 0-待审核 */
        this.auditFlag = 0;
        /** 买家租户id */
        this.buyerTenantId = '';
        /** 卖家租户id */
        this.sellerTenantId = '';
        /** 手机号 */
        this.mobile = '';
        this.nickName = '';
        this.tenantName = '';
        this.userName = '';
    }

    getRandom() {
        this.mobile = common.getRandomMobile();
        this.tenantName = this.userName = this.mobile;
        return this;
    }

    /**
     * 更新合伙人信息
     * @param {object} params 
     */
    update(params) {
        common.update(this, params);
        return this;
    }

    /**
     * 微信登录
     * @description 会根据合伙人状态。进行断言 
     */
    async login() {
        // 待审核状态 登录
        if (this.auditFlag == 0) {
            const res = await disReq.spMerchantLogin({ code: this.mobile, check: false });
            expect(res.result).to.include({ msgId: 'dis_buyer_to_audit' });
            return;
        }

        if (this.auditFlag == -1) {
            const res = await disReq.spMerchantLogin({ code: this.mobile, check: false });
            expect(res.result).to.include({ msgId: 'dis_buyer_audit_reject' });
            return;
        }

        const res = await disReq.spMerchantLogin({ code: this.mobile });
        console.log(res);

    }

    /**
     * 渠道买家认证
     * @description 渠道员工操作
     * @param {object} params 
     * @param {string} params.auditFlag 认证状态位 -1-拒绝 1-已认证 0-待审核 
     */
    async authenticateForSource({ auditFlag }) {
        await spugr.authenticateForSource({ id: this.id, sellerTenantId: this.sellerTenantId, buyerTenantId: this.buyerTenantId, auditFlag });
        this.auditFlag = auditFlag;
    }

    /**
     * 合伙人是否已认证 校验
     */
    async hasAuthenticatedForSourceCheck() {
        const val = await spugr.hasAuthenticatedForSource({ mobile: this.mobile }).then(res => res.result.data.val);
        expect(val).to.equal(this.auditFlag == 1 ? 1 : 0);
    }

    /**
     * 渠道分销商修改用户手机号
     * @description 渠道员工操作
     * @param {object} params 
     * @param {string} params.mobile 新手机号
     */
    async changeMobileForSource({ mobile }) {
        await spugr.changeMobileForSource({ id: this.id, sellerTenantId: this.sellerTenantId, buyerTenantId: this.buyerTenantId, captcha: this.mobile, mobile });
        this.mobile = mobile;
    }

    /**
     * 修改合伙人状态
     * @param {object} params 
     * @param {string} params.flag 状态 1-启用 0-禁用 -1删除
     */
    async changeFlag({ flag }) {
        await spugr.changeDisPartnerFlag({ id: this.id, flag });
        this.flag = flag;
    }

};

disManage.setupDisClient = function (params) {
    return new DisClient().getRandom();
};

/**
 * 小C
 */
class DisClient {
    constructor(params) {
        /** id */
        this.id = '';
        /** 1-启用 0-禁用 -1删除 */
        this.flag = 1;
        /** 认证状态位 -1-拒绝 1-已认证 0-待审核 */
        this.auditFlag = 0;
        /** 买家租户id */
        this.buyerTenantId = '';
        /** 卖家租户id */
        this.sellerTenantId = '';
        /** 手机号 */
        this.mobile = '';
        this.nickName = '';
        this.tenantName = '';
        this.userName = '';
    }

    getRandom() {
        this.mobile = common.getRandomMobile();
        this.tenantName = this.userName = this.mobile;
        return this;
    }

    /**
     * 更新合伙人信息
     * @param {object} params 
     */
    update(params) {
        common.update(this, params);
        return this;
    }

    /**
     * 微信登录
     * @description 会根据合伙人状态。进行断言 
     */
    async login() {
        // 待审核状态 登录
        // if (this.auditFlag == 0) {
        //     const res = await disReq.disClientLogin({ code: this.mobile, check: false });
        //     expect(res.result).to.include({ msgId: 'dis_buyer_to_audit' });
        //     return;
        // }

        // if (this.auditFlag == -1) {
        //     const res = await disReq.disClientLogin({ code: this.mobile, check: false });
        //     expect(res.result).to.include({ msgId: 'dis_buyer_audit_reject' });
        //     return;
        // }

        await disReq.disClientLogin({ code: this.mobile });
    }

    /**
     * 渠道买家认证
     * @description 渠道员工操作
     * @param {object} params 
     * @param {string} params.auditFlag 认证状态位 -1-拒绝 1-已认证 0-待审核 
     */
    async authenticateForSource({ auditFlag }) {
        await spugr.authenticateForSource({ id: this.id, sellerTenantId: this.sellerTenantId, buyerTenantId: this.buyerTenantId, auditFlag });
        this.auditFlag = auditFlag;
    }

    /**
     * 合伙人是否已认证 校验
     */
    async hasAuthenticatedForSourceCheck() {
        const val = await spugr.hasAuthenticatedForSource({ mobile: this.mobile }).then(res => res.result.data.val);
        expect(val).to.equal(this.auditFlag == 1 ? 1 : 0);
    }

    /**
     * 渠道分销商修改用户手机号
     * @description 渠道员工操作
     * @param {object} params 
     * @param {string} params.mobile 新手机号
     */
    async changeMobileForSource({ mobile }) {
        await spugr.changeMobileForSource({ id: this.id, sellerTenantId: this.sellerTenantId, buyerTenantId: this.buyerTenantId, captcha: this.mobile, mobile });
        this.mobile = mobile;
    }

};


disManage.setupDisPartnerMerchant = function (params) {
    return new SpMerchant();
};

/**
 * 门店商户
 */
class SpMerchant {
    constructor(params) {
        /**  编号ID */
        this.id = '';
        /**  单元id（租户） */
        this.unitId = '';
        /**  门店id (sp_shop的id) */
        this.shopId = '';
        /**  清分商户id(清分系统里的eb_merchant.id) */
        this.mchId = '';
        /**  商户号(对应一代的sn) */
        this.mchNo = '';
        /**  商户全称 */
        this.mchName = '';
        /**  商户简称 */
        this.mchAbbr = '';
        /**  商户地址 */
        this.mchAddr = '';
        /**  联系人 */
        this.contact = '';
        /**  联系电话 */
        this.mobile = '';
        /**  清分账户id(清分系统里的eb_settle_account.id) */
        this.settleAcctId = '';
        /**  账户号（银行卡号） */
        this.settleAcctNo = '';
        /**  账户名 */
        this.settleAcctName = '';
        /**  账户类型（1-我行个人账户;2-我行企业账户;3-他行个人账户;4-他行企业账户） */
        this.settleAcctType = 0;
        /**  银行名称 */
        this.bankName = '';
        /**  银行编码 */
        this.bankCode = '';
        /**  1 有效 0 无效 */
        this.flag = 0;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  提交信息至账务系统失败原因 */
        this.errorMsg = '';
        /**  账户审批状态 */
        this.accountFlag = 0;
        /**  商户审核状态 */
        this.merchantFlag = 0;
    }
};

// 提交商户信息和提交账户信息分开提交 
// 但是目前还是用旧的。没有分离的版本
// 以下为分离的商户信息与账户信息

/**
 * 初始化商户信息
 */
disManage.setupSpMerchantMsg = function () {
    return new SpMerchantMsg();
};

/**
 * 渠道用户绑定商户信息
 * @description 
 */
class SpMerchantMsg {
    constructor() {
        /** 门店商户主键id */
        this.id = '';
        /** 联系电话 */
        this.mobile = '';
        /** 商户名称，创建门店商户时，若传空直接用租户名称 */
        this.mchName = '';
        /** 商户简称（选填，不传则用租户全称，若无租户全称则用contact） */
        this.mchAbbr = '';
        /** 商户详细地址 */
        this.detailAddr = '';
        /** 申请人姓名 */
        this.contact = '';
    }

    getRandom() {
        this.mobile = LOGINDATA.mobile;
        this.mchName = common.getRandomChineseStr(3);
        this.contact = LOGINDATA.userName;
        this.mchAbbr = this.contact;
        this.detailAddr = `杭州市滨江区${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`;
    }

    async save() {
        const res = await spugr.saveSpMerchantMsg(this);
        this.id = res.result.data.id;
        if (res.result.data.errorMsg) {
            console.error(errorMsg);
        }
    }
};

/**
 * 初始化账户信息
 */
disManage.setupSpMerchantAccountMsg = function () {
    return new SpMerchantAccountMsg();
};

/**
 * 渠道用户绑定商户信息
 * @description 
 */
class SpMerchantAccountMsg {
    constructor() {
        /** 门店商户主键id */
        this.id = '';
        /** 账户号 */
        this.settleAcctNo = '';
        /** 账户名 */
        this.settleAcctName = '';
        /** 账户类型（1-个人账户;2-企业账户） */
        this.settleAcctType = '';
        /** 银行名称（非中信账户必选，中信账户选填） */
        this.bankName = '';
        /** 银行编码（选填，若非中信账户不填银行编码，置商户状态为人工审核状态） */
        this.bankCode = '';
        /** 若为1代表为中信银行，若为2代表为非中信银行（必选） */
        this.cncbFlag = '';
    }

    getRandom() {
        const bankInfo = spBank.personalAcct[common.getRandomNum(0, spBank.personalAcct.length - 1)];
        this.settleAcctNo = bankInfo.settleAcctNo;
        this.settleAcctName = bankInfo.name;
        this.settleAcctType = 1;
        this.bankName = '中信银行';
        this.bankCode = 10469565;
        this.cncbFlag = 1;
    }

    async save() {
        const res = await spugr.saveSpMerchantAccountMsg(this);
        this.id = res.result.data.id;
        if (res.result.data.errorMsg) {
            console.error(errorMsg);
        }
    }
};