const common = require('../../../lib/common');
// const EventEmitter = require('events').EventEmitter;
const format = require('../../../data/format');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spConfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
// const storageAction = require('../../../reqHandler/sp/wms/storageAction');

const disBillManage = module.exports = {};

disBillManage.spDisBillManage = function () {
    return new SpDisBill();
};

/**
 * 分销商单据
 */
class SpDisBill {
    constructor(params) {
        /** 用户信息(登录信息) */
        this.userInfo = {};
        /** 单据 */
        this.bills = {};
        this.purJson = {};
        /** 购物车 */
        this.cartList = [];
        /** 地址 */
        this.addressInfo = {};
    }

    /**
     * 设置用户信息
     */
    setUser({ userInfo }) {
        this.userInfo = userInfo;
        return this;
    }

    /** 初始化购物车 */
    setupByCart(cart) {
        this.cartList = cart.cartList;
        return this;
    }

    async getRecAddressInfo() {
        const addressInfo = await spMdm.getUserDefaultRecInfo().then(res => res.result.data);

        // 服务端 最开始对接时的问题 开单需要的地址id为recInfo.id 而不是addressId
        this.addressInfo.addressId = addressInfo.recInfo.id;
        this.addressInfo.provCode = addressInfo.address.provinceCode;
        return this;
    }

    /**
     * 估算商品运费-分销
     * @description 分销运费模式为合包
     */
    async evalDisShipFee() {
        if (!this.addressInfo.addressId) {
            await this.getRecAddressInfo();
        }

        const orders = new Map();
        this.purJson.orders.forEach(order => {
            if (!orders.has(order.main.warehouseId)) {
                orders.set(order.main.warehouseId, { warehouseId: order.main.warehouseId, orderSpus: new Map() });
            }

            const orderSpus = orders.get(order.main.warehouseId).orderSpus;
            order.details.forEach(detail => {
                if (orderSpus.has(detail.spuId)) {
                    const orderSpu = orderSpus.get(detail.spuId);
                    orderSpu.orderNum += detail.num;
                    orderSpu.orderMoney += detail.money;
                    orderSpus.set(detail.spuId, orderSpu);
                } else {
                    orderSpus.set(detail.spuId, {
                        spuId: detail.spuId,
                        orderNum: detail.num,
                        orderMoney: detail.money,
                        orgSellerId: detail.orgSellerId,
                    });
                }
            });
            orders.set(order.main.warehouseId, { warehouseId: order.main.warehouseId, orderSpus: orderSpus });
        });
        for (const order of orders.values()) {
            order.orderSpus = [...order.orderSpus.values()];
        }


        const reqParams = {
            provinceCode: this.addressInfo.provCode,
            srcType: this.userInfo.sourceType,
            srcId: this.userInfo.sourceId,
            sellerId: this.userInfo.disId,
            orders: [...orders.values()]
        };
        // console.log(`reqParams=${JSON.stringify(reqParams)}`);

        const res = await spConfb.evalDisShipFee(reqParams);
        // this.shipFee = res.result.data;
        console.log(`\nres.result.data.fees=${JSON.stringify(res.result.data.fees)}\n`);

        this.purJson.orders[0].main.shipFeeMoney = res.result.data.fees[0].fee;
        this.purJson.orders[0].sellerFee = res.result.data.fees[0].seller;
        return res;
    }

    /**
     * 拼接采购单参数
     * @description 根据购物车信息
     */
    getPurJsonByCart() {
        const json = { orders: [], combOrders: [] };

        // 合包
        if (this.shipFee.isCombinative) {
            json.combOrders = this.shipFee.combinatives;
            json.combOrders.forEach(combOrder => {
                delete combOrder.combBillId;
                delete combOrder.combedBillIds;
                delete combOrder.combedBillNos;
            });
        }

        // 一件代发
        if (this.shipFee.isSingleFog) {
            json.combOrders = this.shipFee.singleFogs;
        }

        json.orders = [...this.cartList.values()].map((data, index) => {
            const main = {
                sellerId: data.trader.tenantId,
                money: 0,//成交金额。按买家适用价格计算得到的金额合计
                originalMoney: 0,//订单原金额， 按pubPrice计算金额
                totalNum: 0,//
                payKind: 1,//付款方式。1 预付，2 货到付款。
                shopCoupsMoney: 0,//店家卡券抵扣金额
                buyerRem: 'savePurBill' + common.getRandomStr(5),//买家备注
                couponsIds: '',//使用的券ids。该订单使用的优惠券编号列表，多个逗号分隔
                mineCouponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
                hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
                addressId: this.addressInfo.addressId,
                // wsAddressId: '',//仓库收货地址ID
                // srcType: 0,//app下单，1：开放平台，2: 买家分享，3：卖家分享
                // srcId: '',//订单来源的租户id：srcType=2/3(买家/卖家分享)时记录买家/卖家的租户id
            };
            const details = [...data.carts.values()].map(cart => {
                const detail = format.dataFormat(cart, 'cartId=id;spuId;skuId;spuTitle;spuCode;spec1;spec1Name;spec2;spec2Name;spec3;spec3Name;num=skuNum;originalPrice;price;tagId;tagKind');
                detail.spuDocId = cart.spuPic.split(',').shift();
                detail.money = common.mul(detail.num, detail.price);
                main.money += detail.money;
                main.totalNum += detail.num;
                if (!detail.originalPrice) detail.originalPrice = detail.price;
                main.originalMoney = detail.originalPrice;
                return detail;
            });

            // 运费 若为到付，则为0
            if (this.shipFee.isCombinative || this.shipFee.isSingleFog) {
                main.shipFeeMoney = json.combOrders.find(combOrder => combOrder.sellerIds.includes(main.sellerId)).fee;
            } else {
                main.shipFeeMoney = this.shipFee.fees.find(fee => fee.sellerId == main.sellerId).fee;
            }

            // 总应收金额
            main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;
            return { main, details };
        });

        return json;
    }

    /**
     * 拼接采购单参数
     * @description 单商品
     */
    getPurJson({ dresFull }) {
        const main = {
            money: 0,// 成交金额。按分销价格的金额合计
            originalMoney: 0,// 订单原金额， 按商品原始价格计算总金额
            totalNum: 0, // 总数量
            payKind: 1,// 付款方式。1 预付，2 货到付款
            shopCoupsMoney: 0, // 店家卡券抵扣金额
            shipFeeMoney: 0,// 运费。若为到付，则为0
            // mineCouponsIds: '',// 使用的券领用号ids。该订单使用的优惠券领用编号列表，
            // couponsIds: '',// 使用的券ids。该订单使用的优惠券编号列表
            buyerRem: `savePurBill${common.getRandomStr(5)}`,//买家备注
            hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
            addressId: this.addressInfo.addressId,

            sellerId: this.userInfo.disId,//分销商租户id
            srcType: this.userInfo.sourceType || 1,// 订单来源
            srcId: this.userInfo.sourceId,// 来源id
            warehouseId: dresFull.spu.warehouse.warehouseId,
            warehouseName: dresFull.spu.warehouse.warehouseName,
        };

        const spuDetail = format.dataFormat(dresFull.spu, 'spuId=id;spuCode=code;spuTitle=title;spuDocId=coverUrl;orgSellerId=shopId');
        const details = dresFull.skus.slice(0, 2).map(sku => {
            const detail = {
                skuId: sku.id,
                spuCode: sku.code,
                spec1: sku.spec1,
                spec1Name: sku.ecCaption.spec1,
                spec2: sku.spec2,
                spec2Name: sku.ecCaption.spec2,
                spec3: sku.spec3 || '',
                spec3Name: sku.ecCaption.spec3 || '',
                num: common.getRandomNum(5, 10),// 为了方便
                originalPrice: sku.pubPrice,
                price: sku.price,
                // rem: common.getRandomNum(1, 10),
            };
            detail.money = common.mul(detail.num, detail.price);
            main.money = common.add(main.money, detail.money);
            main.originalMoney += common.mul(detail.num, detail.originalPrice);
            main.totalNum += detail.num;
            return Object.assign(detail, spuDetail);
        });

        const sellerFee = [];

        this.purJson = { orders: [{ main, details, sellerFee }] };
        return this;
    }

    /**
     * 创建采购订单-分销
     */
    async saveDisPurBill() {
        // 开单前 重新获取运费然后拼接参数 保证数据有效性
        await this.evalDisShipFee();
        // 计算最终总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        this.purJson.orders.forEach(order => {
            const main = order.main;
            main.totalMoney = [main.money, -main.shopCoupsMoney, main.shipFeeMoney].reduce((a, b) => common.add(a, b));
        });
        console.log(`purJson=${JSON.stringify(this.purJson)}`);

        const res = await spTrade.saveDisPurBill(this.purJson);
        this.bills = res.result.data;
        console.log(this.bills);

        // 等待mq消费
        await common.delay(1000);
        return res;
    }

    /**
     * 支付并确认
     * @description 渠道版支付需要使用微信小程序支付 符合真实场景
     */
    async createPay() {
        const payRes = await spTrade.createPay({ orderIds: this.bills.rows.map(bill => bill.billId), payMoney: this.bills.totalMoney });
        console.log(`\npayRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, payType: '1048576' });
    }

    /**
     * 拼接退货单参数
     * @param {object} params
     * @param {string} params.typeId 类型。0-仅退款 1-退款退货 2-48小时极速退款
     * @param {string} params.backFlag 退货标志(0 未退货，1 全部退货，2 部分退货)
     * @param {object} params.purBillFull 采购单详情
     */
    async getReturnJson(params) {
        const purBill = params.purBillFull;
        const main = {
            typeId: params.typeId,
            purBillId: purBill.bill.id,
            totalNum: 0,
            totalMoney: 0,
            rem: common.getRandomStr(6),
            // score: 0,//应退积分
            hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        }, details = [];

        // 退货原因
        let returnReasonList = [];
        if (main.typeId == 0) {
            returnReasonList = await spugr.getDictList({ typeId: '2012', flag: 1 })
                .then((res) => res.result.data.rows);
        } else {
            returnReasonList = await spugr.getDictList({ typeId: '2005', flag: 1 })
                .then((res) => res.result.data.rows);
        }
        main.returnReason = returnReasonList[0].codeValue;
        main.returnReasonName = returnReasonList[0].codeName;

        const mapField = 'purDetailId=id;num=skuNum;price=skuPrice';
        params.purBillFull.skus.forEach((sku) => {
            const detail = format.dataFormat(sku, mapField);
            // 部分退后 再次退
            if (sku.backFlag == 2) {
                detail.num = sku.skuNum - sku.applyBackNum;
            }

            // 部分退 简单实现 直接减去部分商品数量
            if (params.backFlag == 2) {
                detail.num = Math.max(0, detail.num - 1);
            }
            detail.money = common.mul(detail.num, detail.price);
            details.push(detail);
            main.totalNum += detail.num;
            main.totalMoney = common.add(main.totalMoney, detail.money);
        });

        return { main, details };
    }

    /**
     * 查询采购单详情
     */
    async findPurBillFull() {
        return spTrade.findPurBillFull({ id: this.bills.rows[0].billId, srcType: 1 }).then(res => res.result.data);
    }

    async wmsGetGoods() {
        // const res = await storageAction.getGoods({ id:})
    }



};