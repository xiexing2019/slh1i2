const spAccount = require('../../data/spAccount');
const spReq = require('../spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const { URLSearchParams } = require('url');

let disReq = module.exports = {
    disInfo: spAccount.sourceEntry.autoTest,
};

/**
 * 渠道商 员工登录
 * @description 
 * 1. 目前写死 预发环境员工还未添加,等德贵提供相应接口后，补充到维护脚本中
 * 2. 员工绑定分销商 而不是渠道入口
 */
disReq.staffLogin = async function (params = {}) {
    params = Object.assign({
        code: '000',
        pass: '000000',
        tenantCode: this.disInfo.spDis.tenantCode,
    }, params);
    return spAuth.spStaffAuthcLogin(params);
};

/**
 * 用户登录
 * @param {object} params
 * @param {string} params.code
 */
disReq.userLogin = async function (params = {}) {
    params.sourceId = this.disInfo.sourceId;
    return spReq.spClientLogin(params);
};

/**
 * 合伙人 绑定微信并登录
 * @description 合伙人第一次绑定时调用
 * @param {object} params
 * @param {string} params.code
 */
disReq.spMerchantLogin = async function (params = {}) {
    params.sourceId = this.disInfo.sourceId;
    return spugr.userLoginWithBindOpenId(params);
};

/**
 * 合伙人 获取会话同时登录
 * @description 合伙人绑定后使用
 * @param {object} params
 * @param {string} params.openId
 */
disReq.spMerchantFetchSessionWithLogin = async function (params = {}) {
    params.sourceId = this.disInfo.sourceId;
    return spugr.wxFetchSessionWithLogin(params);
};

/**
 * 默认合伙人登录(一级)
 */
disReq.defMerchantLogin = async function (params = {}) {
    params.code = this.disInfo.levelOne.mobile;
    return this.userLogin(params);
};

// /**
//  * 买家 绑定微信并登录
//  * @description 合伙人第一次绑定时调用 
//  * @param {object} params
//  * @param {string} params.code
//  */
// disReq.disClientLogin = async function (params = {}) {
//     params = Object.assign({
//         sourceId: this.disInfo.sourceId,
//         channelCode: this.disInfo.levelOne.channelCode,
//     }, params);
//     return spugr.userLoginWithBindOpenId(params);
// };

/**
 * 小C 获取会话同时登录
 * @description 传了channelCode 即为小C  channelCode默认取配置中的一级合伙人
 * @param {object} params
 * @param {string} params.openId
 * @param {string} params.channelCode 分销买家租户的code，用来找到分销买家
 */
disReq.disClientFetchSessionWithLogin = async function (params = {}) {
    params = Object.assign({
        sourceId: this.disInfo.sourceId,
        channelCode: this.disInfo.levelOne.channelCode,
    }, params);
    return spugr.wxFetchSessionWithLogin(params);
};

/**
 * 全局商品搜索
 * @description es搜索 渠道查询需要sourceId与sourceType
 * @param {object} params
 */
disReq.searchDres = async function (params = {}) {
    params.queryType = 0;
    params.sourceId = this.disInfo.sourceId;
    params.sourceType = this.disInfo.sourceType;
    return spDresb.searchDres(params);
};

/**
 * 买家查询商品信息
 * @description 前置条件:全局搜索商品->获取商品detailUrl
 * @param {object} params 
 * @param {string} params.detailUrl 商品详情url
 */
disReq.getDresFullForBuyerByUrl = async function (params) {
    const strParams = _.last(params.detailUrl.split('?'));
    const urlParams = new URLSearchParams(strParams);
    return spDresb.getFullForBuyer({
        spuId: urlParams.get('spuId'),
        _cid: urlParams.get('_cid'),
        _tid: urlParams.get('_tid'),
        buyerId: LOGINDATA.tenantId,
        sourceId: this.disInfo.sourceId,
        sourceType: this.disInfo.sourceType
    });
};