const common = require('../../lib/common');
// const format = require('../../data/format');
const mockJsonParam = require('./mockJsonParam');
const spAct = require('../../reqHandler/sp/global/spAct');
const moment = require('moment');
const Dres = require('./dresManage').Dres;
const spdchg = require('../../reqHandler/sp/global/spdchg');

/** 日期格式 */
const fmt = 'YYYY-MM-DD HH:mm:ss';
/** 活动类型 */
const actType = {
    general: { id: 1, name: '普通' },
    timeLimit: { id: 2, name: '限时秒杀' },
    spike: { id: 3, name: '秒杀' },// 
    fullReduction: { id: 4, name: '满减' },
    fullDelivery: { id: 5, name: '满送' },
    fightGroup: { id: 6, name: '拼团' },
};
/** 组织形式 */
const orgType = {
    web: { id: 1, name: '平台组织' },
    signUp: { id: 2, name: '商家报名' },
};
/** 内容形式 */
const contType = {
    shop: { id: 1, name: '店铺' },
    dres: { id: 2, name: '商品' },
    typeAll: { id: 3, name: '店铺和商品' },
};
/** 受众对象 */
const audiObj = {
    sp: { id: 1, name: '好店客户' },
    OL: { id: 2, name: 'OL圈' },
    beautiful: { id: 3, name: '美丽衣单' },
    third: { id: 4, name: '第三方公众号' },
};
/** 活动状态 */
const actFlag = {
    cutOut: { id: -1, name: '删除' },
    disable: { id: 0, name: '停用' },
    create: { id: 1, name: '活动创建' },
    regist: { id: 2, name: '活动报名中' },
    notStart: { id: 3, name: '活动未开始' },
    processing: { id: 4, name: '活动进行中' },
    end: { id: 5, name: '活动结束' },
    forceStart: { id: 6, name: '活动强制开始' },
    forceEnd: { id: 7, name: '活动强制结束' },
};
const trendType = {
    0: '动态',
    1: '静态'
};

class Act {
    constructor() {
        /** 活动id */
        this.id = '';
        /** 活动状态 */
        this.flag = 0;
        /** 活动名称 */
        this.actName = '';
        /** 活动类型 */
        this.typeId = '';
        /** 组织形式 */
        this.orgType = '';
        /** 内容形式 */
        this.contType = '';
        /** 受众对象 */
        this.audiObj = '';
        /** 分成比率 */
        this.profitRate = 0;
        /** 活动开始后是否可以再添加商品*/
        this.trendType = 0;
        /** 活动开始时间 */
        this.actDateGte = '';
        /** 活动结束时间 */
        this.actDateLte = '';
        /** 活动描述 */
        this.actDesc = '';
        /** 报名开始时间 */
        this.entryDateGte = '';
        /** 报名结束时间 */
        this.entryDateLte = '';
        /** 活动展示图 */
        this.showDocId = '';
        /** 活动结束图 */
        this.endDocId = '';
        /** 是否立即开始，1是，0否 */
        this.isStart = 0;
        /** 频道id */
        this.channelId = '';
        /** 商品 spuId:{spuId,tenantId,clusterCode,channelId(没有就无)}*/
        this.spus = new Map();
        /** 门店 */
        this.shops = new Set();
        /** 频道信息 */
        this.channelInfo = {};
        /** 报名规则 */
        this.signUpRule = [];
        /** 活动规则 */
        // this.actRule = [];//目前版本不支持
    }

    /**
     * 获取报名规则元数据
     */
    async getSignUpRuleMap() {
        /** 报名规则元数据 */
        this.signUpRuleMap = new Map();
        const ruleField = await spAct.exportEntryRuleFieldBit().then(res => res.result.data);
        const ruleBase = await spAct.getRule().then(res => res.result.data);
        ruleBase.whenProps.forEach(val => {
            if (ruleField.hasOwnProperty(val.name)) val.ability = ruleField[val.name];
            this.signUpRuleMap.set(val.name, val);
        });
    }

    /**
     * 设置报名规则
     */
    setSignUpRule(ruleStr) {
        this.signUpRule = ruleStr.split(';').map(val => getRuleJson({ ruleStr: val, ruleMap: this.signUpRuleMap }));
        return this;
    }

    /**
     * 设置活动规则
     * @description 版本暂不支持 
     */
    async setActRule() {

    }

    /**
     * 生成随机活动信息
     * @param {object} params
     * @param {string} [params.typeId=1] 1普通 2限时购 3秒杀 4满减 5满送 6拼团 默认普通
     * @param {string} [params.isStart=0] 是否立即开始 0否 1是
     */
    mockRandomAct(params = {}) {
        this.actName = `活动${common.getRandomStr(5)}`;
        this.typeId = params.hasOwnProperty('typeId') ? params.typeId : actType.general.id;
        this.orgType = orgType.web.id;
        this.contType = contType.dres.id;//暂只有商品
        this.audiObj = audiObj.sp.id;
        this.profitRate = common.getRandomNum(5, 10) / 100;
        this.actDesc = `autotest${common.getRandomStr(5)}`;

        const fmt = 'YYYY-MM-DD';
        // 限时活动 
        if (this.typeId == 2) {
            /** 限时购活动时间对象列表 */
            this.flashList = [{
                startTime: `${common.getDateString([0, 0, 1], fmt)} 00:00:01`,
                endTime: `${common.getDateString([0, 0, 1], fmt)} 23:59:59`,// ,
                announce: 1,// 是否提醒 0否 1是 
                // id: ''
            }, {
                startTime: `${common.getDateString([0, 0, 2], fmt)} 00:00:01`,
                endTime: `${common.getDateString([0, 0, 2], fmt)} 23:59:59`,
                announce: 1,
                // id: ''
            }];
        } else {
            this.flashList = [];
        }

        // 是否立即开始
        if (params.isStart = 1) {
            this.isStart = 1;
            this.flashList.unshift({
                startTime: `${common.getCurrentDate()} 00:00:01`,
                endTime: `${common.getCurrentDate()} 23:59:59`,
                announce: 1,
            });
            this.actDateGte = common.getCurrentDate(fmt);
            this.actDateLte = common.getDateString([0, 0, 4], fmt);
            this.entryDateGte = '';
            this.entryDateLte = '';
        } else {
            this.actDateGte = common.getDateString([0, 0, 2], fmt);
            this.actDateLte = common.getDateString([0, 0, 4], fmt);
            this.entryDateGte = common.getCurrentDate(fmt);
            this.entryDateLte = common.getDateString([0, 0, 1], fmt);
        }

        return this;
    }

    /**
     * 保存活动
     * @description 新增活动时,会同时新建一个活动频道
     */
    async saveAct(params = {}) {
        //保存活动需要同时新增一个频道
        if (!this.channelId) {
            this.channelInfo = mockJsonParam.getChannelJson();
            delete this.channelInfo.id;
            delete this.channelInfo.name;
            this.channelInfo.filterClassIds = '1013';
            this.channelInfo.channelType = 1;
        }

        const _params = Object.assign({}, this, this.channelInfo, params);
        delete _params.signUpRuleMap;
        delete _params.flag;
        delete _params.spus;
        delete _params.shops;
        delete _params.channelInfo;

        // console.log(`_params=${JSON.stringify(_params)}`);
        const res = await spAct.saveActAndChannel(_params);

        await common.delay(1000);
        this.updateActInfo(_params);
        this.id = res.result.data.id;
        this.channelId = res.result.data.channelId;
        this.getActFlag();
        // console.log(`actId=${this.id},channelId=${this.channelId}`);
        return res;
    }

    /**
     * 更新活动信息
     * @param {object} params 
     */
    updateActInfo(params) {
        if (params.hasOwnProperty('addSpus')) {
            this.addSpus(params.addSpus);
            delete params.addSpus;
        }
        if (params.hasOwnProperty('delSpus')) {
            this.delSpus(params.delSpus);
            delete params.delSpus;
        }
        if (params.hasOwnProperty('shopIds')) {
            params.shopIds.split(',').forEach(shopId => this.shops.add(shopId));
            delete params.shopIds;
        }
        for (const key in params) {
            if (this.hasOwnProperty(key)) this[key] = params[key];
        }
        return this;
    }

    /**
     * 添加商品对象列表
     * @param {Array} arr [{spuId,tenantId,clusterCode(集群), channelId(没有就不传)}]
     */
    addSpus(arr) {
        arr.forEach(val => {
            const dresInfo = new SpDresChannelDetail();
            dresInfo.update(val);
            dresInfo.channelId = this.channelId;
            this.spus.set(val.spuId || val.id, dresInfo);
        });
        return this;
    }

    /**
     * 批量删除商品对象列表
     * @param {Array} arr [{id:频道商品的id,spuId, channelId}]
     */
    delSpus(arr) {
        arr.forEach(val => this.spus.delete(val.spuId));
        return this;
    }

    /**
     * 查询活动频道商品
     * @param {object} params 
     */
    async getActChannelSearchDres(params = {}) {
        return spAct.getActChannelSearchDres({ channelId: this.channelId, spuFlag: 1, shadowAll: true, ...params });
    }

    /**
     * 绑定门店
     * @param {string} shopIds  绑定门店ID，使用逗号分割
     * @param {string} delShopIds  解绑门店ID，使用逗号分割
     */
    async rebindShops({ shopIds = '', delShopIds = '' }) {
        await spAct.rebindShops({ actId: this.id, shopIds, delShopIds });
        shopIds.split(',').forEach(shopId => this.shops.add(shopId));
        delShopIds.split(',').forEach(shopId => this.shops.delete(shopId));
    }

    /**
     * 获取限时购列表 并设值flashList
     * @description act.typeId=2
     */
    async getActFlashSaleList() {
        const res = await spAct.getActFlashSaleList({ actId: this.id });
        this.flashList = res.result.data.rows;
        return res;
    }

    /**
     * 限时活动商品配置
     */
    async saveActFlashSaleDetail(params) {

        const res = await spAct.saveActFlashSaleDetail(params);
        // console.log(`res=${JSON.stringify(res)}`);

    }

    /**
     * 强制更新活动状态
     * @param {Number} flag actFlag
     */
    async updateActFlag(flag) {
        await spAct.updateActFlag({ id: this.id, flag });
        this.flag = flag;
    }

    /**
     * 获取活动状态
     * @description 根据时间判断
     */
    getActFlag() {
        // 强制开始/强制结束时 不管设置的时间
        if (this.flag == actFlag.forceStart.id || this.flag == actFlag.forceEnd.id) return;

        const curTime = common.getCurrentDate(fmt);
        // console.log(`curTime=${curTime}`);
        if (moment(curTime).isBefore(this.actDateGte)) {
            this.flag = actFlag.create.id;
        }
        // 组织形式->商家报名 + 在报名时间段内
        if (this.orgType == orgType.signUp.id && moment(curTime).isBetween(this.entryDateGte, this.entryDateLte, null, '[]')) {
            this.flag = actFlag.regist.id;
            return;
        }
        if (moment(curTime).isBetween(this.entryDateLte, this.actDateGte)) {
            this.flag = actFlag.notStart.id;
            return;
        }
        if (moment(curTime).isBetween(this.actDateGte, this.actDateLte, null, '[]')) {
            this.flag = actFlag.processing.id;
            return;
        }
        if (moment(curTime).isAfter(this.actDateLte)) {
            this.flag = actFlag.end.id;
        }
    }

    /**
     * 修改活动商品状态
     * @param {object} params
     * @param {string} params.spuId 商品ID
     * @param {string} params.flagToChange -1:手动下架（对应平台端的驳回），0:下架（对应平台端的删除），1：正常/上架（对应平台端的通过），2:待审核
     * @param {string} params.auditRemark 审核意见 频道类型为活动，活动类型为商家报名时，驳回时，必选
     */
    async changeActDresAuditFlag(params) {
        if (!this.spus.has(params.spuId)) console.error(`channelId:'${this.channelId}'中没有商品spuId:'${params.spuId}',不能修改活动商品状态，请检查`);
        await spdchg.changeChannelDresFlag(Object.assign({ channelId: this.channelId, actInfoOrgType: this.orgType }, params));
        const dresInfo = this.spus.get(params.spuId);
        dresInfo.flag = params.flagToChange;
        if (params.auditRemark) dresInfo.auditRemark = params.auditRemark;
        this.spus.set(params.spuId, dresInfo);
    }

    /**
     * 获取活动详情
     * @description ec-spact-actInfo-getDetail 期望值
     */
    getDetail() {
        //audiObjId entryLimitRuleId updatedDate actRuleId actPres entryLimitBit updatedBy createdDate createdBy tempId
        const exp = {
            id: this.id,
            actName: this.actName,
            flag: this.flag,
            showDocId: this.showDocId,
            endDocId: this.endDocId,
            entryDateGte: this.entryDateGte,
            entryDateLte: this.entryDateLte,
            actDateGte: this.actDateGte,
            actDateLte: this.actDateLte,
            typeId: this.typeId,
            trendType: this.trendType,
            orgType: this.orgType,
            audiObj: this.audiObj,
            contType: this.contType,
            actDesc: this.actDesc,
            actUrl: '',
            channelId: this.channelId,
            // contentType 从6->11 12-12-26
            actUrl: `{"jumpLink":{"param":{"actId":${this.id},"actName":"${this.actName}"}},"contentType":11}`,
            ecCaption: {
                orgType: _.values(orgType).find(val => val.id == this.orgType).name,
                flag: _.values(actFlag).find(val => val.id == this.flag).name,
                typeId: _.values(actType).find(val => val.id == this.typeId).name,
                contType: _.values(contType).find(val => val.id == this.contType).name,
                audiObj: _.values(audiObj).find(val => val.id == this.audiObj).name,
                trendType: trendType[this.trendType],
            }
        };
        // 删除的不显示 服务端便没有对flag进行翻译
        if (this.flag == -1) exp.ecCaption.flag = -1;

        if (this.typeId == 2) {
            exp.flashList = this.flashList;
        }

        return exp;
    }

    /**
     * 获取活动状态
     * @description ec-spact-actInfo-checkActStatus 期望值
     */
    getStatus() {
        const exp = {
            isStart: this.flag == actFlag.processing.id ? 1 : 0,
            channelId: this.channelId,
            showDocId: this.showDocId,
            endDocId: this.endDocId,
        };
        exp.msg = this.flag == actFlag.end.id || this.flag == actFlag.cutOut.id ? '活动已结束' : this.flag == actFlag.processing.id ? '' : '活动暂未开始';
        return exp;
    }

};

/**
 * 拼接规则
 * @param {string} ruleStr 规则字段(规则名称 判断符 值)空格分割 e.g.  avgDresPicCount >= 2
 * @param {Map} ruleMap 规则元数据
 */
function getRuleJson({ ruleStr, ruleMap }) {
    const [ruleName, oper, aExp] = ruleStr.split(' ');
    const rule = ruleMap.get(ruleName);
    let json = {
        loopAble: false,
        priority: 1,
        ruleName: ruleName,
        then: [
            {
                tExp: {
                    aExp: rule.ability,
                    oper: '+=',
                    prop: 'resultBit'
                },
                tType: 0
            }
        ],
        when: {
            items: [
                {
                    logicToken: '(p0)',
                    wExp: [
                        {
                            aExp: aExp,
                            oper: oper,
                            ph: 'p0',
                            prop: `$${ruleName}`
                        }
                    ],
                    wType: 0
                }
            ]
        }
    };
    return json;
};

function ActInfo() {
    /**  活动id */
    this.id = '';
    /**  活动名称 */
    this.actName = '';
    /**  活动类型 */
    this.typeId = 0;
    /**  组织形式 */
    this.orgType = 0;
    /**  内容形式 */
    this.contType = 0;
    /**  频道id */
    this.channelId = '';
    /**  活动规则 */
    this.actRuleId = '';
    /**  受众对象 */
    this.audiObj = 0;
    /**  受众对象id */
    this.audiObjId = '';
    /**  活动描述 */
    this.actDesc = '';
    /**  模板id */
    this.tempId = '';
    /**  报名开始时间 */
    this.entryDateGte = '';
    /**  报名截止时间 */
    this.entryDateLte = '';
    /**  活动报名限制规则 */
    this.entryLimitRuleId = '';
    /**  活动时效 */
    this.actPres = {};
    /**  活动展示图 */
    this.showDocId = '';
    /**  活动结束图 */
    this.endDocId = '';
    /**  活动链接 */
    this.actUrl = '';
    /**  状态：0停用 1活动创建 2活动报名中 3活动未开始 4活动进行中 */
    this.flag = 0;
    /**  创建人 */
    this.createdBy = '';
    /**  创建时间 */
    this.createdDate = '';
    /**  修改人 */
    this.updatedBy = '';
    /**  修改时间 */
    this.updatedDate = '';
    /**  报名限制位 */
    this.entryLimitBit = '';
};

/**
 * 活动频道商品信息
 */
class SpDresChannelDetail extends Dres {
    constructor() {
        super();
        /**  频道id */
        this.channelId = '';
        /**  频道ids */
        this.channelIds = '';
        /**  SPU ID */
        this.id = '';
        /**  SPU ID */
        this.spuId = '';
        /**  SPU所属租户 */
        this.tenantId = '';
        /**  SPU所属单元 */
        this.unitId = '';
        /**  标题 */
        this.title = '';
        /**  类别 */
        this.classId = '';
        /**  spu定价 */
        this.spuPrice = 0;
        /**  sku定价 */
        this.skuPrices = {};
        /**  采购价 */
        this.purPrice = 0;
        /**  状态 */
        this.flag = 0;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  库存 */
        this.invNum = 0;
        /**  活动累计销售数量 */
        this.actSalesNum = 0;
        /**  活动累计销售金额 */
        this.actSalesMoney = 0;
        /**  活动累计退货数量 */
        this.actBackNum = 0;
        /**  活动累计退货金额 */
        this.actBackMoney = 0;
        /**  活动总销售数量 */
        this.actTotalNum = 0;
        /**  活动总销售金额 */
        this.actTotalMoney = 0;
        /**  审核意见 */
        this.auditRemark = '';
    }

    /**
     * 更新活动频道商品信息
     * @param {object} dresInfo 商品信息
     */
    update(dresInfo) {
        const _dresInfo = dresInfo.hasOwnProperty('spu') ? this.setByDetail(dresInfo) : this.setByEs(dresInfo);
        common.update(this, _dresInfo.spu);//
        return this;
    }

    getChannelDresInfo() {
        const info = {
            channelDresFlag: this.flag,
        };
        return info;
    }

};

module.exports = Act;