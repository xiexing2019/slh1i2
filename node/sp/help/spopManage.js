const common = require('../../lib/common');
const spop = require('../../reqHandler/sp/spop/spo');

class OpApp {
    constructor() {
        this.appName = '';
        this.appKey = '';
        this.accessToken = '';
        this.appSecret = '';
        this.appTypeId = '';
        this._cid = '';
    }

    /**
     * 创建新应用
     * @description 目前是一个tid只能创建一个应用
     * @param {object} params 
     */
    async saveOpApp(params) {
        const res = await spop.saveOpApp(params);
        this.appName = res.params.appName;
        this.appTypeId = res.params.appTypeId;
        return res;
    }

    /**
     * 获取并更新accessToken
     */
    async refreshToken() {
        const data = await spop.refreshToken({ _cid: this._cid, appKey: this.appName, appSecret: this.appSecret }).then(res.result.data);
        this.accessToken = data.accessToken;
    }

    /**
     * 推荐商品查询(渠道)
     * @param {object} params 
     */
    async getSpuList(params) {
        params._cid = this._cid;
        params.accessToken = this.accessToken;
        return spop.getSpuList(params);
    }

};

class SellerApp extends OpApp {
    constructor(params) {
        super();
    }
};


module.exports = {
    OpApp,
    SellerApp
};