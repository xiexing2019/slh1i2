const common = require('../../lib/common');
const format = require('../../data/format');
const mockJsonParam = require('./mockJsonParam');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');

let cartHelp = module.exports = {};

/**
 * 批量新增购物车的预期结果构造实现
 * @param {Array} cartListRows 初始值中的 rows数组
 * @param {Array} saveCartInBatchsParam 批量添加购物车传的参数
 */
cartHelp.expectResult = (cartListRows, saveCartInBatchsParam) => {
    let hasTenantId = cartListRows.find(element => element.trader.tenantId == saveCartInBatchsParam.trader.tenantId);
    if (hasTenantId == undefined) {
        cartListRows.push(mockJsonParam.inBatchJsonFormat(saveCartInBatchsParam));   //要把添加的都push进去
    } else {
        for (let i = 0; i < saveCartInBatchsParam.carts.length; i++) {
            let hasSkuId = hasTenantId.carts.find(element => element.skuId == saveCartInBatchsParam.carts[i].skuId)
            if (hasSkuId == undefined) {
                hasTenantId.carts.push(saveCartInBatchsParam.carts[i]);
            } else {
                hasSkuId.skuNum = common.add(hasSkuId.skuNum, saveCartInBatchsParam.carts[i].skuNum);
                hasSkuId.skuPrice = saveCartInBatchsParam.carts[i].skuPrice;
            };
        };
    };
};

/****
 * 添加货品的预期结果的方法
 * @param cartListRows 初始值的rows数组
 * @param params 添加购物车传递的参数
 */
cartHelp.addCartexpectResult = (cartListRows, params) => {
    if (cartListRows.length == 0) {
        let rowsDat = {};
        let catArr = [];
        catArr[0] = params.cart;
        rowsDat.carts = catArr;
        rowsDat.trader = params.trader;
        cartListRows[0] = rowsDat
    } else {
        let hasTenantId = cartListRows.find(element => element.trader.tenantId == params.trader.tenantId);
        if (hasTenantId == undefined) {
            cartListRows.push(params);   //要把添加的都push进去
        } else {
            let hasSkuId = hasTenantId.carts.find(element => element.skuId == params.cart.skuId);
            if (hasSkuId == undefined) {
                hasTenantId.carts.push(params.cart);
            } else {
                hasSkuId.skuNum = common.add(hasSkuId.skuNum, params.cart.skuNum);
                hasSkuId.skuPrice = params.cart.skuPrice;
                hasSkuId.invNum = params.cart.invNum;
            };
        };
    };
};

/***
 * 删除货品的预期结果
 * @param cartListRows 原始数据rows数组
 * @param params 新增购物车传的json
 */
cartHelp.deleteCartExpect = (cartListRows, params) => {
    let index = _.findIndex(cartListRows, element => element.trader.tenantId == params.trader.tenantId);
    if (index > -1) {
        _.remove(cartListRows[index].carts, element => element.skuId == params.cart.skuId);
    }
    return cartListRows;
};


/**修改购物车断言
 * @param cartListRows 初始查询集合中的rows数组
 * @param saveResult 添加购物车返回的结果
 * @param updateResult 修改购物车返回的json
 * 
 * **/
cartHelp.updateCartExpect = (cartListRows, saveResult, updateResult) => {
    let tenantId = cartListRows.find(element => element.trader.tenantId == saveResult.params.jsonParam.trader.tenantId);
    if (tenantId == undefined) {

    } else {
        let rows = tenantId.carts.find(element => element.id == saveResult.result.data.val);
        if (rows == undefined) {

        } else {
            rows.skuNum = updateResult.params.jsonParam.skuNum;
            // rows.skuPrice = updateResult.params.jsonParam.skuPrice;
        };
    }
};


cartHelp.setUpCart = function (cartList) {
    return new CartManage(cartList);
};

class CartManage {
    constructor(cartList = []) {
        this.cartList = new Map();

        for (let index = 0; index < cartList.length; index++) {
            const element = cartList[index];
            const carts = new Map();
            element.carts.forEach(cart => carts.set(cart.skuId, new ShoppingCart().setUpByCart({ trader: element.trader, cart })));
            this.cartList.set(element.trader.tenantId, { trader: element.trader, carts });
        }
    }

    /**
     * 新增购物车
     * @param {*} params 
     */
    async saveCart(params) {
        const saveRes = await spTrade.saveCart(params);
        const { trader, cart } = saveRes.params.jsonParam;
        this.updateCart();
    }

    async batchSave(params) {
        const saveRes = await spTrade.saveCartInBatchs(params);
        console.log(`saveRes=%j${saveRes}`);

    }

    updateCart({ trader, cart }) {
        if (!this.cartList.has(trader.tenantId)) this.cartList.set(trader.tenantId, { trader, carts: new Map() });
        const _cart = this.cartList.get(trader.tenantId);
        if (_cart.carts.has(cart.spuId)) {

        } else {

        }
    }

    /**
     * 购物车列表校验
     * @param {*} cartList 
     */
    checkCartList(cartList) {
        const traderTenantIds = this.cartList.keys();
        traderTenantIds.forEach((traderTenantId) => {
            const cart = cartList.find(ele => ele.trader.tenantId == traderTenantId);
            expect(cart).not.to.be.undefined;

            const cartExp = this.cartList.get(traderTenantId);
            common.isApproximatelyEqualAssert(cartExp.trader, cart.trader);
            common.isApproximatelyArrayAssert(cartExp.carts, cart.carts);
        });
    }
}

/**
 * 进货车/购物车表
 */
class ShoppingCart {
    constructor() {
        /**  ID */
        this.id = '';
        /**  单元ID */
        this.unitId = '';
        /**  卖家id */
        this.sellerTenantId = '';
        /**  卖家名称 */
        this.sellerName = '';
        /**  卖家单元id */
        this.sellerUnitId = '';
        /**  卖家的商品sku */
        this.skuId = '';
        /**  卖家的商品spu */
        this.spuId = '';
        /**  spu名称 */
        this.spuName = '';
        /**  spu款号 */
        this.spuCode = '';
        /**  spu描述 */
        this.spuTitle = '';
        /**  spu图片，目前单个，多个以逗号分隔 */
        this.spuPic = '';
        /**  规格1 */
        this.spec1 = 0;
        /**  规格1名称 */
        this.spec1Name = '';
        /**  规格2 */
        this.spec2 = 0;
        /**  规格2名称 */
        this.spec2Name = '';
        /**  规格3 */
        this.spec3 = 0;
        /**  规格3名称 */
        this.spec3Name = '';
        /**  商品数量 */
        this.skuNum = 0;
        /**  价格(加入购物车时价格) */
        this.price = 0;
        /**  状态(1-有效(默认) 0-无效 卖家下线商品时应自动将该购物车记录置为无效) */
        this.flag = 0;
        /**  业务域代码（购物车比较通用，可以支持不同业务域） */
        this.bdomainCode = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  sku的编码 */
        this.skuCode = '';
        /**  原始价格 */
        this.originalPrice = 0;
        /**  卖家的商品spu的origId */
        this.spuOrigId = '';
        /**  标签id:活动id */
        this.tagId = '';
        /**  扩展属性字段 */
        this.extProps = {};
        /**  标签种类活动种类 */
        this.tagKind = 0;
    }

    setUpByCart({ trader, cart }) {
        common.update(this, Object.assign(format.dataFormat(trader, 'sellerUnitId=unitId;sellerTenantId=tenantId;sellerName=traderName'), cart));
        this.unitId = LOGINDATA.unitId;
        this.spuTitle = cart.title;
        this.price = cart.skuPrice;
        return this;
    }
}

