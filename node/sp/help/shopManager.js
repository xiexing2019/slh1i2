const common = require('../../lib/common');
const spugr = require('../../reqHandler/sp/global/spugr');

class SpShop {
    constructor() {
        /**  编号ID(等于sp_tenant_id) */
        this.id = '';
        /**  单元id（租户） */
        this.unitId = '';
        /**  店铺名称 */
        this.name = '';
        /**  存储LOGO图片fileid，单张 */
        this.logoPic = '';
        /**  店铺风格（多选，逗号隔开） */
        this.tags = '';
        /**  省编码 */
        this.provCode = 0;
        /**  市编码 */
        this.cityCode = 0;
        /**  区编码 */
        this.areaCode = 0;
        /**  市场id(从市场库中维护) */
        this.marketId = '';
        /**  楼层-2F -1F 1F 2F负数表示地下 */
        this.floor = 0;
        /**  档口号 */
        this.doorNo = '';
        /**  详细地址 */
        this.detailAddr = '';
        /**  营业执照和身份证照（证书照片）id，多张，逗号分隔 */
        this.certPic = '';
        /**  存储门头照片文件id，多张 */
        this.headPic = '';
        /**  存储店铺内景照片，多张 */
        this.contentPic = '';
        /**  短视频id */
        this.videoId = '';
        /**  短视频封面url */
        this.coverUrl = '';
        /**  短视频地址url */
        this.videoUrl = '';
        /**  店铺评分(如4.9分) */
        this.score = 0;
        /**  店铺被关注数 */
        this.concernNum = 0;
        /**  店铺点赞数 */
        this.likeNum = '';
        /**  乐观锁 */
        this.ver = 0;
        /**  状态(1-有效(默认) 0-无效 2 待审核) */
        this.flag = 0;
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  主营类目 */
        this.masterClassId = '';
        /**  发货地址省编码 */
        this.deliverProvCode = 0;
        /**  发货地址市编码 */
        this.deliverCityCode = 0;
        /**  发货地址区编码 */
        this.deliverAreaCode = 0;
        /**  发货地址详细地址 */
        this.deliverDetailAddr = '';
        /**  运费方式 */
        this.shipFeeWayId = 0;
        /**  市场名称 */
        this.marketName = '';
        /**  最后上新时间 */
        this.lastMarketDate = '';
        /**  短视频封面文件id */
        this.coverDocId = '';
        /**  门店开通的注册认证码 */
        this.authCode = '';
        /**  门店显示顺序 */
        this.shadowFlag = 0;
        /**  销售价格区间 */
        this.priceRange = {};
        /**  店铺标签（多选，逗号隔开） */
        this.labels = '';
        /**  门店是否启用，禁用 1 启用  0 禁用 */
        this.usingFlag = 0;
        /**  禁用原因 */
        this.unUnsingReason = '';
        /**  店铺上新能力位 1 能上新  0 不能上新 */
        this.specialFlag = 0;
        /**  销售方式（童装） 1 普通销售 2 批量销售 */
        this.salesWayId = 0;
        /**  自定义店内商品排序采用字典typId=2022 */
        this.goodsSort = 0;
        /**  排序类型1代表降序，2代表升序 */
        this.sortType = 0;
        /**  所在城市好店列表中显示位置 */
        this.cityShowOrder = 0;
        /**  门店类型(0:年租店铺 1:免租店铺) */
        this.typeId = 0;
        /**  佣金比例 */
        this.platCommissionRatio = 0;
        /**  门店别名 */
        this.alias = '';
        /**  物流市场与市场取自同一字典 */
        this.logisticsMarketId = '';
    }

    update(params) {
        common.update(this, params);
        return this;
    }

    /**
     * 获取店铺详情
     * @param {string} tenantId 
     */
    async getShopInfo({ tenantId }) {
        const shopInfo = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
        this.update(shopInfo);
    }
}

module.exports = SpShop;