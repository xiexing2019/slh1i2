const common = require('../../lib/common');
const spConfig = require('../../reqHandler/sp/global/spConfig');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const format = require('../../data/format');
const caps = require('../../data/caps');
const spugr = require('../../reqHandler/sp/global/spugr');
// const spCoupon = require('../../reqHandler/sp/global/spCoupon');
const spdchg = require('../../reqHandler/sp/global/spdchg');
const spBank = require('../data/spBank');
const spAccount = require('../data/spAccount');
const fs = require('fs');
const path = require('path');

//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/doc.json')))[caps.name];

let mockJsonParam = module.exports = {};

mockJsonParam.cartJson = (params = {}) => {
    // let s = common.getRandomNum(0, params.result.data.skus.length - 1);
    let count = params.count || 0;
    let json = {
        "trader": {
            tenantId: params.params._tid,
            traderName: "LS1",
            unitId: params.result.data.spu.unitId
        }
    };
    let spu = {
        spuName: params.result.data.spu.name,
        classId: params.result.data.spu.classId,
        spuId: params.result.data.spu.id,
        title: params.result.data.spu.title
    };
    function sku(index) {
        const dresSku = params.result.data.skus[index];
        let skus = {
            spec1: dresSku.spec1,
            spec2: dresSku.spec2,
            spec3: dresSku.spec3,
            spec1Name: dresSku.ecCaption.spec1 || '',
            spec2Name: dresSku.ecCaption.spec2 || '',
            spec3Name: dresSku.ecCaption.spec3 || '',
            skuId: dresSku.id,
            // 特殊库存时不取num
            skuNum: dresSku.stockFlag > 0 ? common.getRandomNum(1, 15) : Math.min(common.getRandomNum(1, 15), dresSku.num),
            skuPrice: dresSku.price
        };
        return skus;
    };
    let cartJson = {};
    if (count == 0) {
        cartJson.trader = json.trader;
        let skus = sku(count);
        skus.classId = spu.classId;
        skus.spuId = spu.spuId;
        skus.spuName = spu.spuName;
        skus.title = params.result.data.spu.title;
        skus.spuPic = params.result.data.spu.ecCaption.docContent[0].docId
        cartJson.cart = skus;
    } else {
        cartJson.trader = json.trader;
        cartJson.spu = spu;
        let arr = [];
        if (params.result.data.skus.length < count) {
            count = params.result.data.skus.length;
        };
        for (let i = 0; i < count; i++) {
            arr[i] = sku(i);
        };
        cartJson.carts = arr;
    };
    // console.log(params.result.data.spu.ecCaption.docHeader[0].docId);

    return cartJson;
};

mockJsonParam.cartJsonByAct = function (params = {}) {
    const { dres, count = 0 } = params;
    const cartJson = {
        trader: {
            tenantId: dres.tenantId,
            traderName: dres.tenantName,
            unitId: dres.unitId
        }
    };
    const spu = {
        spuName: dres.name,
        classId: dres.classId,
        spuId: dres.spuId,
        title: dres.title
    };
    function getSku(sku) {
        let skus = {
            spec1: sku.spec1,
            spec2: sku.spec2,
            spec3: sku.spec3,
            spec1Name: sku.ecCaption.spec1 || '',
            spec2Name: sku.ecCaption.spec2 || '',
            spec3Name: sku.ecCaption.spec3 || '',
            skuId: sku.id,
            // 特殊库存时不取num
            skuNum: sku.stockFlag > 0 ? common.getRandomNum(1, 15) : Math.min(common.getRandomNum(1, 15), sku.num),
            skuPrice: dres.bestPubprice
        };
        return skus;
    };

    const skus = JSON.parse(dres.skus);
    // 单明细调用新增购物车 多明细调用批量调用购物车
    if (count == 0) {
        const sku = getSku(skus[0]);
        skus.classId = spu.classId;
        skus.spuId = spu.spuId;
        skus.spuName = spu.spuName;
        skus.title = dres.title;
        skus.spuPic = JSON.parse(dres.ecCaption.docContent)[0].docId
        cartJson.cart = [sku];
    } else {
        cartJson.spu = spu;
        cartJson.carts = skus.slice(0, Math.min(skus.length, count)).map(sku => getSku(sku));
    };
    // console.log(dres.ecCaption.docHeader[0].docId);

    return cartJson;
}

mockJsonParam.updateCart = (params) => {

    let updateJson = {
        spec1: params.cart.spec1,
        spec2: params.cart.spec2,
        spec3: params.cart.spec3,
        spec1Name: params.cart.spec1Name,
        spec2Name: params.cart.spec2Name,
        spec3Name: params.cart.spec3Name,
        skuId: params.cart.skuId,
        skuNum: common.getRandomNum(1, 99),
        skuPrice: common.getRandomNum(88, 299)
    };
    return updateJson;
};

mockJsonParam.inBatchJsonFormat = (params) => {
    let spu = params.spu;
    let formatJson = {};
    let carts = [];
    formatJson.trader = params.trader;
    for (let i = 0; i < params.carts.length; i++) {
        let cart = params.carts[i];
        cart.spuId = params.spu.spuId;
        cart.spuName = params.spu.spuName;
        carts.push(cart);
    };
    formatJson.carts = carts;
    return formatJson;
};

mockJsonParam.slhBossRegisterJson = () => {
    let json = {
        token: common.getRandomStr(5),
        mobile: 12 + common.getRandomNumStr(9),
        name: 'slh老板' + common.getRandomNumStr(4),
        slhSessionId: common.getRandomStr(10),
        provCode: '330000',
        cityCode: '330100'
    };
    return json;
};

mockJsonParam.addAddrJson = (params) => {
    let json = {
        recInfo: {
            linkMan: common.getRandomChineseStr(3),
            telephone: common.getRandomMobile(),
            postcode: common.getRandomNumStr(6),  //邮编，6位
            type: 1
        },
        address: {
            provinceCode: params.provinceValue,
            cityCode: params.cityValue,
            countyCode: params.countyValue,
            detailAddr: `时代大厦${common.getRandomNumStr(5)}号`
        },
    };
    return json;
};

//运费json
mockJsonParam.saveFeeRuleJson = async (provCode, count) => {
    let proviceArray = (await mockJsonParam.getAreaJson()).proviceArr;
    _.remove(proviceArray, (element) => element == provCode);

    let rules = [];
    for (let i = 0; i <= count; i++) {
        rules.push(getJson(i))
    };
    function getJson(count) {
        return {
            feeType: 2,
            provIds: proviceArray[count],
            startNum: common.getRandomNum(20, 30), //起始数量，随机生成20-30之间的数作为起始数量
            startFee: common.getRandomNum(10, 15),  //起始费用，随机生成10-15之间的数作为起始费用
            addNum: 10,
            addFee: 1
        }
    };
    rules[rules.length - 1].provIds = provCode;
    return { rules: rules };
};

/**
 * 店铺认证信息保存
 * @param {Object} params 
*/
mockJsonParam.shopJson = async () => {
    const areaJson = await mockJsonParam.getAreaJson();
    // 获取市场
    const marketList = await spCommon.getMarket({ flag: 1 }).then(res => res.result.data.rows);
    const market = marketList.find(data => data.fullName == spAccount.seller.market) || marketList[0];
    console.log(market);

    const tagList = await spCommon.findCatConfig({ type: 2, showFlagBit: 1 });
    const masterClass = await spugr.getDictList({ "flag": 1, "typeId": 2010 }); //主营类目，2010字典里拿的
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    const json = {
        name: `平台企业${common.getRandomNumStr(6)}`,
        logoPic: docData.image.shopLogo[0].docId,//存储LOGO图片fileid，单张
        //存储门头照片文件id，多张，逗号分隔
        //客户端只能上传一张，并且没有做多张处理,因此自动化这里也只传一张
        headPic: _.take(docIdArr, 1).join(','),
        contentPic: _.takeRight(docIdArr, 3).join(','),//存储店铺内景照片，多张，逗号分隔
        certPic: _.takeRight(docIdArr, 2).join(','),//营业执照和身份证照（证书照片）id，多张，逗号分隔
        // videoId: common.getRandomNumStr(6),
        // videoUrl: common.getRandomStr(8),
        // coverUrl: common.getRandomStr(8),
        provCode: areaJson.provinceValue,
        cityCode: areaJson.cityValue,
        areaCode: areaJson.countyValue,
        tagsList: [tagList.result.data['2'][0].typeValue], //这里要用typeValue的 别用id
        masterClassId: masterClass.result.data.rows[0].codeValue,
        marketId: market.id,
        marketName: market.fullName,
        floor: common.getRandomNum(1, 99),
        doorNo: common.getRandomNumStr(3),
        detailAddr: '滨江星耀城',
        shipFeeWayId: 1,
        deliverProvCode: areaJson.provinceValue,
        deliverCityCode: areaJson.cityValue,
        deliverAreaCode: areaJson.countyValue,
        deliverDetailAddr: '滨江星耀城16楼',
    };
    return json;
};


/**
 * 拼接商品保存参数
 * @param {object} params 
 * @param {object} params.classId 类别id
 */
mockJsonParam.styleJson = (params) => {
    const randomStr = common.getRandomStr(5);

    const price = {
        tagPrice: 300,
        pubPrice: 200,
        price1: 190,
        price2: 180,
        price3: 170,
        price4: 160,
        price5: 150,
    };

    const spu = Object.assign({
        id: '',
        code: `款号${randomStr}`,
        name: `商品n${randomStr}`,
        classId: params.classId,
        title: `商品t${randomStr}`,
        // namePy:'',//自动生成
        shipOriginId: BASICDATA['850'][0].codeValue,//	发货地（国家行政区划，字典850）
        shipFeeWayId: BASICDATA['2001'][0].codeValue,//	运费方式（买家承担、包邮等，字典2001）
        brandId: '',//品牌（字典606）
        pubScope: BASICDATA['2003'].find((data) => data.codeName == '全部').codeValue || 1,//发布范围
        unit: '件',
        special: 0,
        isAllowReturn: 1,
        origin: '杭州',//产地（用户手工输入）
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
    }, price);
    const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
    // 商品内容区文档
    spu.docContent = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
    // 商品标题区文档 
    spu.docHeader = [_.head(spu.docContent)];

    //spec1Ids 规格1范围。
    //格式：{“规格值”:{“属性1名称”:”属性1值”},…}。
    //每个规格值可以定义一些附加属性。
    //目前支持“salesCaption”（销售标题）属性，以指定该款号的规格值在销售页面显示给买家的文本。
    //如“服装”类别的款号，包含颜色(spec1）和尺码(spec2）两种规格属性。
    //有一个款号可选颜色范围为2（红色）、6（灰色），其中红色显示给顾客的文本需要改为“2018升级版红色”。
    //spec1_ids可设置为{“2”:{“salesCaption”:”2018升级版红色”, “6”:{}}。
    spu.spec1Ids = {};
    common.randomSort(BASICDATA['605']).slice(0, 3).forEach(element => spu.spec1Ids[element.codeValue] = { salesCaption: element.codeName });
    spu.spec2Ids = {};
    common.randomSort(BASICDATA['601']).slice(0, 3).forEach(element => spu.spec2Ids[element.codeValue] = { salesCaption: element.codeName });
    spu.spec3Ids = {};

    let skus = [];
    const sku = { id: '', spec1: '', spec2: '', spec3: '', num: '10000', rem: '备注', ...price },
        spec1Ids = Object.keys(spu.spec1Ids),
        spec2Ids = Object.keys(spu.spec2Ids);

    spec1Ids.forEach((spec1Id) => {
        spec2Ids.forEach((spec2Id) => skus.push(Object.assign({}, sku, { spec1: spec1Id, spec2: spec2Id })));
    });

    // spu详情属性dto
    const detailPropsDTO = {
        // spu详情图图片内容
        pictureContent: common.randomSort(_.cloneDeep(docData.image.dres)).slice(0, 3).map(res => { return { docId: res.docId, docUrl: res.docUrl } }),
    };

    return {
        id: '',
        spu,
        skus,
        detailPropsDTO
    };
};

/**
 * 拼接采购订单参数
 * @description ec-sppur-purBill-createFull
 * @param {object} params 
 * @param {object} params.styleInfo 款号信息
 */
mockJsonParam.purJson = (params) => {
    const [styleInfo, trader] = [params.styleInfo.result.data, params.styleInfo.params];
    const detailSpu = {
        spuId: styleInfo.spu.id,
        spuTitle: styleInfo.spu.title,
        spuCode: styleInfo.spu.code,
        spuDocId: styleInfo.spu.ecCaption.docHeader[0].docId  //这里要传，不然导致下订单之后订单款号没有图片
    };
    let main = {
        sellerId: trader._tid,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: 0,//运费。若为到付，则为0
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: 2,//付款方式。1 预付，2 货到付款。
        couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: LOGINDATA.defAddressId,//收货地址ID 取当前用户默认地址 需要登录时获取 
    }, details = [];
    common.randomSort(styleInfo.skus).slice(0, 2).forEach((sku) => {
        const detail = Object.assign(new mockJsonParam.GetDetail(sku), detailSpu);
        main.originalMoney += detail.originalPrice;
        main.money += detail.money;
        main.totalNum += detail.num;
        details.push(detail);
    });
    main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;
    return { orders: [{ main, details }] };
};

mockJsonParam.GetDetail = function (sku) {
    this.skuId = sku.id;
    this.spec1 = sku.spec1;
    this.spec1Name = sku.ecCaption.spec1 || '';
    this.spec2 = sku.spec2;
    this.spec2Name = sku.ecCaption.spec2 || '';
    this.spec3 = sku.spec3;
    this.spec3Name = sku.ecCaption.spec3 || '';
    this.num = common.getRandomNum(1, 10);
    this.originalPrice = sku.pubPrice;
    this.price = this.originalPrice;
    this.money = common.mul(this.num, this.price);
};

/**
 * 拼接购物车 转采购订单的参数
 * @param {object} params 购物车查询列表的json(包括参数和返回)
 * @returns {object} 采购订单需要的JSON
*/
mockJsonParam.purJsonByCart = (params) => {
    let details = [];
    let main = {
        sellerId: params.result.data.rows[0].trader.tenantId,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: 0,//运费。若为到付，则为0
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: 2,//付款方式。1 预付，2 货到付款。
        couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: LOGINDATA.defAddressId//收货地址ID 取当前用户默认地址 需要登录时获取 
    };

    for (let i = 0; i < params.result.data.rows[0].carts.length; i++) {
        let info = format.dataFormat(params.result.data.rows[0].carts[i], "cartId=id;skuId;spuId;spuTitle=title;spec1;spec1Name;spec2;spec2Name;spec3;spec3Name;num=skuNum;originalPrice=skuPrice");
        info.money = common.mul(info.num, info.originalPrice);
        info.price = info.originalPrice;
        info.spuCode = info.spuTitle.replace('商品', '款号');
        main.originalMoney += info.originalPrice;
        main.money += info.money;
        main.totalNum += info.num;
        details.push(info);
    };
    //实付金额=商品总额-优惠卡券+运费
    main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;

    return { orders: [{ main, details }] };
};

/**
 * 拼接退货声请单参数
 * @description ec-sppur-billReturn-createFull
 * @param {object} params 采购单信息
 */
mockJsonParam.returnBillJson = (params) => {

    let returnReason = BASICDATA['2005'][common.getRandomNum(0, BASICDATA['2005'].length - 1)];
    const main = {
        typeId: params.bill.flag == 3 ? 0 : 1,//0-仅退款 1-退款退货 2-48小时极速退款
        purBillId: params.bill.id,
        totalNum: 0,
        totalMoney: 0,
        rem: common.getRandomStr(6),
        // score: 0,//应退积分
        returnReason: returnReason.codeValue,//退货原因（字典类别2005）
        returnReasonName: returnReason.codeName,
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
    }, details = [];

    const mapField = 'purDetailId=id;num=skuNum;price=skuPrice';
    params.skus.forEach((sku) => {
        const detail = format.dataFormat(sku, mapField);
        detail.money = common.mul(detail.num, detail.price);
        details.push(detail);
        main.totalNum += detail.num;
        main.totalMoney += detail.money;
    });
    //实退金额=商品总额-优惠金额-优惠券金额
    main.totalMoney = main.totalMoney - params.bill.favorMoney - params.bill.shopCoupsMoney;
    return { main, details };
};
/**
 * slh开通商陆好店Json
*/
mockJsonParam.createTetantJson = (params) => {
    // console.log(`params.bank=${JSON.stringify(params.bank)}`);
    let json = {
        slhShopId: LOGINDATA.invid,
        name: '言匠',
        mchAbbr: '言匠',
        contact: 'lxx',
        mobile: '15988413448',
        provCode: params.areaJson.provinceValue,
        cityCode: params.areaJson.cityValue,
        areaCode: params.areaJson.countyValue,
        detailAddr: '星耀城1期16楼',
        settleAcctNo: common.getRandomNumStr(16),
        settleAcctName: params.bank.lName,
        settleAcctType: 1,   //1个人账户 2企业账户
        bankName: params.bank.lName,
        bankCode: params.bank.bankCode,
        authCode: params.authCode,
        cncbFlag: 2,
    };
    return json;
};

/**
 * 
*/
mockJsonParam.createTetantBySlhJson = async () => {
    const areaJson = await mockJsonParam.getAreaJson();
    const getMarket = await spCommon.getMarket({});
    const tagList = await spCommon.findCatConfig({ type: 2, showFlagBit: 1 });  //店铺风格
    const masterClass = await spugr.getDictList({ "flag": 1, "typeId": 2010 }); //主营类目，2010字典里拿的
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    let json = {
        name: '花木牛仔屋',
        mobile: '15988413448',
        provCode: areaJson.provinceValue,
        cityCode: areaJson.cityValue,
        areaCode: areaJson.countyValue,
        detailAddr: '滨江星耀城',
        logoPic: docData.image.shopLogo[0].docId,
        tagsList: [tagList.result.data['2'][0].typeValue],  //店铺风格
        marketId: getMarket.result.data.rows[0].id, //市场标签  
        floor: common.getRandomNum(1, 50),
        doorNo: common.getRandomNumStr(3),
        headPic: _.take(docIdArr, 1).join(','),
        masterClassId: masterClass.result.data.rows[0].codeValue //主营类目
    };
    return json;
}

/**
 * 获取区域
 * @description 港澳台地区没有cityCode 实际传值时传任意值 不做判断
 * @param {object} params
 * @param {string} [params.provinceLabel] 省份名称
 */
mockJsonParam.getAreaJson = async function (params = {}) {
    const areaList = await spConfig.exportDistrictRegion().then(res => res.result.data.rows);
    // console.log(`areaList=${JSON.stringify(areaList)}`);

    //随机取一个省份信息
    const areaInfo = params.hasOwnProperty('provinceLabel') ? areaList.find(area => area.label == params.provinceLabel) : areaList[common.getRandomNum(0, areaList.length - 1)];

    let area = {
        provinceCode: areaInfo.label,
        provinceValue: areaInfo.value,
        cityCode: '',
        cityValue: 0,
        countyCode: '',
        countyValue: 0
    };

    if (areaInfo.hasOwnProperty('children')) {
        const cityInfo = areaInfo.children[common.getRandomNum(0, areaInfo.children.length - 1)];
        area.cityCode = cityInfo.label;
        area.cityValue = cityInfo.value;
        if (cityInfo.hasOwnProperty('children')) {
            const countyInfo = cityInfo.children[common.getRandomNum(0, cityInfo.children.length - 1)];
            area.countyCode = countyInfo.label;
            area.countyValue = countyInfo.value;
        };
    };
    //所有省份的provinceValue
    area.proviceArr = areaList.map(val => val.value);
    return area;
};

//保存勋章json
mockJsonParam.saveMedalJson = function (params = {}) {
    let json = {
        code: `xunC${common.getRandomStr(6)}`,
        name: `勋章${common.getRandomChineseStr(4)}`,
        logoDoc: docData.image.medal[0].docId,
        // ruleKind:,//规则类型(101准勋章；102快勋章；103新勋章；104多勋章)
        triggerType: 1,//触发类型(0自动；1手动)
        triggerPeriod: 0,//触发周期(0表示实时；1表示日；7表示周；30表示月)
        // showOrder:,//显示顺序
        // showFlag: 0,//显示标志，0-不可见 1-卖家可见 2-买家可见 3-卖家买家都可见  控制上新-推荐下拉筛选条件中勋章是否可见
        rem: '勋章备注' + common.getRandomStr(8),//
        // ver:,//乐观锁
        // flag:,//状态：(0-禁用 1-启用)
    };
    json = Object.assign(json, params);
    return json;
};

/**
 * 卖家设置爆款json
 * strategy 爆款设置策略  0-自动   1-手动
*/
mockJsonParam.saveDresHotConfigJson = function (strategy) {
    let json = {
        period: 7  //7天循环/7天内销售最好的款号
    };
    if (strategy == 1) {
        json.strategy = 1;
    } else {
        json.strategy = 0;
        json.hotNum = 5;//效率最好的5个款号
    };
    return { config: json };
};

/**
 * 买家认证信息
 * @description 填写inviteCode直接通过审核
 */
mockJsonParam.submitAuditJson = async function (params) {
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    // 主营类目需要从分类配置获取,从字典获取的无效
    let masterClassId, viceMasterClassId;
    await spCommon.findCatConfig({ type: 7 }).then(res => {
        const data = res.result.data['7'];
        masterClassId = data[0].typeValue;
        viceMasterClassId = data.slice(1, 3).map(row => row.typeValue).join(',');
    });

    let json = {
        shopName: `买家店铺${common.getRandomNumStr(5)}`,
        shopAddr: `滨江区${common.getRandomChineseStr(3)}路${common.getRandomNumStr(3)}号`,
        provCode: params.areaJson.provinceValue,
        cityCode: params.areaJson.cityValue,
        areaCode: params.areaJson.countyValue,
        headPic: docData.image.shopLogo[0].docId,
        contentPic: _.takeRight(docIdArr, 3).join(','),
        certPic: _.takeRight(docIdArr, 2).join(','),
        // inviteCode: 'autotest',//绑定业务员 徐世国 
        remark: '信息属实，请审核！',
        masterClassId,  //主营类目，现在是必填项了
        viceMasterClassId,//副营类目 选填
    };
    return json;
};

/**
 * 渠道用户创建门店商户
 */
mockJsonParam.merchantJson = function (params = {}) {
    const bankInfo = spBank.personalAcct[common.getRandomNum(0, spBank.personalAcct.length - 1)];
    return Object.assign({
        mobile: LOGINDATA.mobile,
        settleAcctNo: bankInfo.settleAcctNo,//账户号（银行卡号，必选）
        settleAcctName: bankInfo.name,//账户名（必选）
        settleAcctType: 1,//账户类型（1-个人账户;2-企业账户）（必选）
        BankName: '中信银行', //"银行名称（非中信账户必选，中信账户选填）",
        BankCode: 10469565,//"银行编码（选填，若非中信账户不填银行编码，置商户状态为人工审核状态）"
        detailAddr: `杭州市滨江区${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`,//商户地址（必选）
        // "mchName": "商户全称（不传则用原有商户全称）",
        contact: LOGINDATA.userName, //"申请人姓名",
        cncbFlag: 1,//"若为1代表为中信银行，若为2代表为非中信银行（必选）"
    }, params);
};


/**
 * 卡券
 */
mockJsonParam.cardCouponsJson = function () {
    // let json = {
    //     cardType,//	是	卡券大类：1优惠券、2礼品券、3运费券；取值调用ec-cardCoupons-dcodes&domain=coupTypeDomain
    //     quantity,//	是	库存数量
    //     color,//	是	卡券颜色；取值调用ec-cardCoupons - dcodes & domain=cardColorDomain
    //     shareBits,//	是	分享能力位：0没有能力 1可分享
    //     effectiveDate,//	是	生效时间(大于等于该时间) ，’yyyy-MM - dd’格式
    //     expiresDate,//	是	到期时间（小于等于该时间），’yyyy-MM - dd’格式
    //     receiveEffectPeriod,//	是	领取后有效时间，0表示无限制 其他表示具体数值
    //     receiveEffectPeriodUnit,//	否	领取后有效时间的单位，默认天：0年, 1月, 2天, 3时
    //     scopeType: 0,//卡券可见范围类型: 0普通券 1内部券
    //     getLimit: 1,	//领券限制，每个用户领券上限，如不填，则默认为1
    //     notice,	//	说明
    //     shareNotice,	//	分享说明
    //     fulfilValue,	//	是	达成金额
    //     execNum,	//	是	优惠金额
    //     domainKind,//	否	规则类型，先不传，后台自动填充
    //     // execType,//	否	先不传，后台自动填充；优惠类型:5满额减（cardType=1/3），9满额赠（cardType=2）
    //     execOther,//	是	cardType=2时代表赠送的物品id,格式如[{“id”:”赠送商品id”,num”:”赠送商品的数量”,”caption”:”赠送商品的名称”}]
    //     dims: [{// array	是	维度数组：每个成员代表一个维度，每个维度有下列三个元素；平台暂时只有一个成员；例子：分类：{“dimName”:”dimSpu”, “dimLevel”:”level_cat”, “dimValue”:”分类id”}，商品：{“dimName”:”dimSpu”, “dimLevel”:”level_spu”, “dimValue”:”商品id”}，门店：{“dimName”:”dimShop”, “dimLevel”:”level_shop”, “dimValue”:”店铺id”}
    //         dimName,//	是	维度名称：由ec-rule - get返回的数据，dimSpu
    //         dimValue,//	是	维度值：用户选择ec-dimension - codes & viewId=dimSpu返回的数据中的value
    //         dimValueCaption,//	是	维度值的中文说明：如款号名称、分类名称、门店名称
    //         dimLevel,//	是	维度层次：选择到dimValue时，有对应的levelName(level_spu/ level_cat)}],
    //     }]
    // };

    // return json;
};


// 推送消息长度没有限制
// 推送url格式也没有校验
/**
 * @param {Object} params
 * @param {Object} params.methodType 1 通知 2 命令 3 弹窗
 * @param {Object} params.sendScopeType 1 全部买家，2 全部卖家 3 部分买家 4 部分卖家
 */
mockJsonParam.spBroadcastTaskJson = function (params) {
    let json = {
        sendScope: params.sendScopeType == 1 ? common.getRandomNum(1, 2) : common.getRandomNum(3, 4),//类别 1 全部买家，2 全部卖家 3 部分买家 4 部分卖家
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        msgContent: 'msgContent' + common.getRandomStr(10),
        msgTitle: 'msgTitle' + common.getRandomStr(10),
        msgDocId: docData.image.dres[0].docId,
        msgOutUrl: 'https://www.google.com',
        tenantIdList: [1],
        rem: 'autotest',//服务端处理,不推送自动化生成的测试消息
    };
    json.methodType = params.methodType || common.getRandomNum(1, 2);
    json.linkType = common.getRandomNum(0, 2);
    if (json.methodType == 2) {
        json.typeId = 4;//推送日志
    } else {
        json.typeId = json.linkType == 2 ? [2, 3, 5, 6][common.getRandomNum(0, 3)] : 1;
    }
    if ([3, 4].includes(json.typeId)) json.tenantIdList = [1, 2];
    if (json.typeId == 3) json.couponIdList = [1, 2];

    if (json.methodType == 3) {
        json.sendScope = 1;
        json.alertMsg = {
            alertType: common.getRandomNum(0, 2),
            alertNums: 0,// 0 代表每次 其他值代表次数
        }
        switch (json.alertMsg.alertType) {
            case '0':
                json.alertMsg.alertDate = common.getCurrentDate();
                break;
            case '1':
                json.alertMsg.alertStartDate = common.getCurrentDate();
                json.alertMsg.alertEndDate = common.getCurrentDate();
                break;
            case '2':
                json.alertMsg.alertStartTime = common.getDateString([0, 0, 0], 'YYYY-MM-DD HH:mm:ss');
                json.alertMsg.alertEndTime = common.getDateString([0, 0, 1], 'YYYY-MM-DD HH:mm:ss');
                break;
            default:
                break;
        }
    } else {

    };

    return json;
};

/**
 * 首页banner 
 * @param {object} params 
 * @param {string} params.linkType 链接类型，1代表外链，2代表内链
 * @param {string} params.tenantId 
 */
mockJsonParam.bannerJson = function (params = {}) {
    let json = {
        itemId: docData.image.banner[common.getRandomNum(0, docData.image.banner.length - 1)].docId,
        itemUrl: '',
        itemType: '1',
        itemSeq: common.getRandomNum(3, 10),
        bannerKind: '0',
        startDate: common.getCurrentDate(),
        endDate: common.getDateString([0, 0, 1]),
        linkType: params.linkType || common.getRandomNum(1, 2),
    };
    // 外链
    if (json.linkType == 1) {
        json.jumpLink = { url: 'http://www.hzecool.com' };
    };
    // 内链
    if (json.linkType == 2) {
        json.contentType = common.getRandomNum(1, 3);
        json.jumpLink = json.contentType != 3 ? { 'key': '' } : { 'tenantId': params.tenantId };
    };

    return json;
};

class BaseJson {
    /**
     * 更新参数
     * @param {object} data 更新字段
     */
    update(data) {
        return Object.assign(this, data)
    }
};

/**
 * 频道
 * @description name与flag必填 需要再加任意一个条件。无条件会报服务端错误
 */
class ChannelJson extends BaseJson {
    constructor(params = {}) {
        super();
        this.id = '';
        this.name = `频道${common.getRandomStr(5)}`;
        /**是否启用（0：停用，1：启用） */
        this.flag = 1;
        /**频道类型 1.活动频道 2.分销商频道 3.第三方频道 */
        this.channelType = params.hasOwnProperty('channelType') ? params.channelType : 2;
        /** 是否自动审核（0：否，1：是） */
        this.autoAudit = 1;
        /**商品加载类型 (0.动态加载 1.静态加载) */
        this.loadType = 0;
    }

    fullJson() {
        this.filterClassIds = '1,1013,1085,1093';//类别ids
        this.filterStyleIds = BASICDATA['2016'].slice(0, 5).map(val => val.codeValue).join(',');//风格ids
        this.filterMasterClassIds = BASICDATA['2010'].slice(0, 5).map(val => val.codeValue).join(',');//主营类目ids
        this.filterShopIds = '';//店铺ids
        this.filterPriceBegin = common.getRandomNum(50, 100);//价格区间从
        this.filterPriceEnd = common.getRandomNum(300, 500);//价格区间到
        this.filterStoreBeign = common.getRandomNum(50, 100);//库存从
        this.filterStoreEnd = common.getRandomNum(90000, 100000);//库存到
        this.filterShopScoreBegin = common.getRandomNum(1, 3);//铺评分从
        this.filterShopScoreEnd = common.getRandomNum(3, 5);//店铺评分到
        this.priceRatio = common.getRandomNum(0.5, 1.5, 1);//加价乘系数
        this.priceAddition = common.getRandomNum(50, 100);//系数后额外再加
        this.initStartTime = common.getDateString([0, 0, -7]);//商品上架时间
        return this;
    }

    /**
     * 详情期望值
     */
    fullExp() {
        let exp = { ruleParams: {} };
        Object.keys(this).forEach((key) => {
            if (['id', 'name', 'flag', 'initStartTime'].includes(key)) {
                exp[key] = this[key];
            } else {
                exp.ruleParams[key] = this[key];
            }
        });
        return exp;
    }

    /**
     * 列表期望值
     */
    listExp() {
        return {
            id: this.id,
            name: this.name,
            flag: this.flag,
            ruleParams: this
        };
    }
};
mockJsonParam.getChannelJson = (params) => new ChannelJson(params);

/**
 * 分销商
 */
mockJsonParam.SpDisInfo = class extends BaseJson {
    constructor() {
        super();
        /** 分销商名称 */
        this.name = '';
        /** 省 */
        this.provCode = '';
        /** 市 */
        this.cityCode = '';
        /** 县区 */
        this.areaCode = '';
        /** 详细地址 */
        this.detailAddr = '';
        /** 频道 */
        this.channelId = '';
        /** 分销商类型 2:渠道类型，3：第三方类型 */
        this.disType = 2;
        /** 联系电话 */
        this.mobile = ``;
    }

    /**
     * 随机生成一个分销商信息
     */
    async mockDisInfo() {
        const areaInfo = await mockJsonParam.getAreaJson();
        const channelInfo = await spdchg.getChannelList({ flag: 1 }).then(res => res.result.data.rows.shift());
        this.name = `分销商${common.getRandomStr(5)}`;
        this.provCode = areaInfo.provinceValue;
        this.cityCode = areaInfo.cityValue;
        this.areaCode = areaInfo.countyValue;
        this.detailAddr = `${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`;
        this.channelId = channelInfo.id;
        this.mobile = `12${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`;
        this.profitPercent = common.getRandomNum(0, 1, 2);
        this.refundPercent = common.getRandomNum(0, 1, 2);

        if (this.disType == 2) {
            this.totalProfit = 0.2;//总分润比例(其他分润比例总和，没有的就是0)
            this.thisplatProfit = 0.1;
            this.disChannelProfit = 0.05;
            this.levelOneProfit = 0.025;
            this.levelTwoProfit = 0.015;
            this.levelThreeProfit = 0.01;
        }
        return this;
    }

    /**
     * 分销商保存
     * @param {object} params 
     */
    async saveSpDis(params) {
        if (!params) params = await this.mockDisInfo();
        const saveRes = await spugr.saveSpDis(params);
        this.id = saveRes.result.data.val;
        this.flag = 1;
        await common.delay(1000);
    }

};

/**
 * 分销商商户
 */
class SpDisMerchantInfo extends BaseJson {
    constructor(spDisInfo) {
        super();
        const bankInfo = spBank.personalAcct[common.getRandomNum(0, spBank.personalAcct.length - 1)];

        this.mobile = spDisInfo.mobile;//手机号(必选)，不传就用当前登录用户的手机号；也作为结算账户的联系电话*
        this.detailAddr = spDisInfo.detailAddr;//详细地址，也作商户地址
        this.mchAbbr = `销${spDisInfo.name.slice(-5)}`;//商户简称，不填用全称，全称是name
        this.contact = common.getRandomChineseStr(3);//结算账户的联系人*
        this.settleAcctNo = bankInfo.settleAcctNo;//结算账户号：如银行卡号*
        this.settleAcctName = bankInfo.name;//结算账户名称*
        this.settleAcctType = 1;//账户类型。1-个人账户;2-企业账户
        // this.bankName = `中信银行${spDisInfo.detailAddr.slice(0, 2)}分行`;//结算账户银行名称，账户类型>2非中信账户必填,中信账户选填
        // this.bankCode = '';//结算账户银行编码，账户类型>2选填
        this.cncbFlag = 1;//若为1代表为中信银行，若为2代表为非中信银行
        this.disId = spDisInfo.id;//分销商id*
    }

};
mockJsonParam.getSpDisMerchantJson = (spDisInfo) => new SpDisMerchantInfo(spDisInfo);

/**
 * 渠道用户创建门店商户
 * @param userInfo 用户信息
 */
mockJsonParam.channelMerchantJson = function (userInfo) {
    const bankInfo = spBank.personalAcct[common.getRandomNum(0, spBank.personalAcct.length - 1)];
    this.id = userInfo.id || '';
    this.mobile = userInfo.mobile;
    this.detailAddr = userInfo.detailAddr;//详细地址，也作商户地址
    this.settleAcctNo = bankInfo.settleAcctNo;//结算账户号：如银行卡号*
    this.settleAcctName = bankInfo.name;//结算账户名称*
    this.settleAcctType = 1;//账户类型。1-个人账户;2-企业账户
    this.cncbFlag = 1;//若为1代表为中信银行，若为2代表为非中信银行
};

/**
 * 分类配置
 */
mockJsonParam.CatConfig = class extends BaseJson {
    constructor() {
        super();
        this.id = '';
    }

    /**
     * 保存分类配置
     */
    async saveCatConfig(params) {
        const saveRes = await spCommon.saveCatConfig(params);
        // console.log(saveRes);
        this.id = saveRes.result.data.val;
        this.update(params);
        if (!this.hasOwnProperty('flag')) this.flag = 1;
    }

    /**
     * 删除分类配置
     */
    async delCatConfig() {
        await spCommon.delCatConfig({ id: this.id });
    }

    /**
     * 获取配置信息
     */
    getInfo() {
        if (this.hasOwnProperty('prop')) this.extProp = this.prop;
        return format.dataFormat(this, Object.keys(this).join(';'));
    }

};

/**
 * 分类配置-商圈
 */
mockJsonParam.CatConfigTrade = class extends mockJsonParam.CatConfig {
    constructor() {
        super();
        this.id = '';
        this.marketList = [];
    }

    /**
    * 关联市场与商圈
    */
    async relateMarketAndTrade({ market }) {
        await spCommon.relateMarketAndTrade({ marketId: market.id, tradeId: this.id });
        this.marketList.push(market);
        this.typeAccode = `.${this.id}.`;
        this.typeValue = 0;
    }
};

