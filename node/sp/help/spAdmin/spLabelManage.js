const common = require('../../../lib/common');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spugr = require('../../../reqHandler/sp/global/spugr');

const spLabelManage = module.exports = {};

/**
 * 初始化标签大类
 */
spLabelManage.setupLabelType = function (params) {
    return new LabelType();
};

/**
 * 标签大类
 */
class LabelType {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  标签名 */
        this.name = '';
        /**  状态 */
        this.flag = 1;
        /**  扩展属性 */
        this.props = {};
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  类型值 */
        this.typeValue = 0;
        /**  排序（升序) */
        this.showOrder = 0;
        /**  可见不可见 1仅运营可见，2仅卖家可见，3运营,卖家可见，4仅买家可见，5，运营,买家可见，7仅都可见，默认0代表都不可见 2进制能力位 买家/卖家/运营*/
        this.showFlag = 0;
    }

    getRandom() {
        this.name = `标签大类${common.getRandomStr(5)}`;
        this.showOrder = common.getRandomNum(1, 20);
        return this;
    }

    /**
     * 保存标签类型
     */
    async save() {
        const res = await spAdmin.saveLableType({ name: this.name, showOrder: this.showOrder, showFlag: this.showFlag });
        this.id = res.result.data.val;
    }

    /**
     * 启用
     */
    async enable() {
        await spAdmin.ableLableTypet({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAdmin.disableLableType({ id: this.id });
        this.flag = 0;
    }

};

/**
 * 初始化平台商品标签选型值
 */
spLabelManage.setupLabelItem = function (params) {
    return new LabelItem();
};

/**
 * 平台商品标签选型值
 */
class LabelItem {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充） */
        this.typeId = '';
        /**  标签选项值（不超过10个中文字符） */
        this.labelName = '';
        /**  标签code值 */
        this.labelValue = 0;
        /**  标签选项值拼音 */
        this.labelNamePy = '';
        /**  标签组别值 */
        this.groupType = 0;
        /**  通过状态(0-禁用 1 启用) */
        this.flag = 1;
        /**  备注 */
        this.rem = '';
        /**  排序（升序） */
        this.showOrder = 0;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    getRandom() {
        this.typeId = common.getRandomNum(1, 5);
        this.labelName = `标签${common.getRandomStr(6)}`;
        this.labelNamePy = this.labelName.replace('标签', 'biaoqian');
        this.rem = `商品标签选型值${common.getRandomStr(3)}`;
        this.showOrder = common.getRandomNum(5, 20);
        return this;
    }

    /**
     * 保存标签类型
     */
    async save() {
        const params = {
            id: this.id,
            typeId: this.typeId,
            labelName: this.labelName,
            // labelNamePy: this.labelNamePy,// 服务端自行处理
            rem: this.rem,
            showOrder: this.showOrder,
        };
        if (this.groupType != 0) params.groupType = this.groupType;
        const res = await spAdmin.saveSpLabelItemFull(params);
        this.id = res.result.data.val;
    }

    /**
     * 启用
     */
    async online() {
        await spAdmin.onlineSpLabelItem({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async offline() {
        await spAdmin.offlineSpLabelItem({ id: this.id });
        this.flag = 0;
    }

};


/**
 * 初始化审核记录
 */
spLabelManage.setupLabelAuditRecord = function (params) {
    return new LabelAuditRecord();
};

/**
 * 审核记录
 * @description 
 * 1. 标签审核功能
 * 2. http://zentao.hzdlsoft.com:6082/zentao/task-view-2178.html
 */
class LabelAuditRecord {
    constructor() {
        /**  id */
        this.id = '';
        /**  类型 1-卖家店铺标签 2-买家标记 3-商品标签 */
        this.bizType = 0;
        /**  审核对象id */
        this.auditObjId = '';
        /**  审核内容 */
        this.auditContent = {};
        /**  审核成功内容 */
        this.succesContent = {};
        /**  审核状态 0-待审核 1-通过 2-驳回 */
        this.auditFlag = 0;
        // /**  审核通过时间 */
        // this.auditDate = '';
        /**  审核意见 */
        this.auditRemark = '';
        /**  状态 */
        this.flag = 1;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    /**
     * 生成随机审核信息请求参数
     * @param {object} params 
     * @param {string} params.type 类型 1-卖家店铺标签 2-买家标记 3-商品标签
     * @param {string} params.auditObjId 审核对象id
     */
    async getRandom(params) {
        this.bizType = params.type;
        this.auditObjId = params.auditObjId;

        let labelList = [];
        if (this.bizType == 1) {
            // domain 0 店铺/商品 1 商品 2 店铺
            labelList = await spugr.getDictList({ typeId: 2020, flag: 1, domain: 0 }).then(res => res.result.data.rows);
            expect(labelList, `未查询到启用状态的标记,请检查`).to.have.lengthOf.above(0);
        }

        if (this.bizType == 2) {
            labelList = await spugr.getDictList({ typeId: 2024, flag: 1 }).then(res => res.result.data.rows);
            expect(labelList, `未查询到启用状态的买家标记,请检查`).to.have.lengthOf.above(0);
        }

        if (this.bizType == 3) {

        }

        this.auditContent = labelList.slice(0, 3).map(data => {
            // console.log(`data=${JSON.stringify(data)}`);
            return { id: data.codeValue, name: data.codeName };
        });

        return this;
    }

    /**
     * 新增标签审核
     */
    async addRecord() {
        const res = await spAdmin.addLabelAuditRecord({ type: this.bizType, auditObjId: this.auditObjId, labels: this.auditContent.map(data => data.id).join(',') });
        console.log(`res=${JSON.stringify(res)}`);
        this.id = res.result.data.val;
    }

    /**
     * 通过
     */
    async auditPass() {
        await spAdmin.passLabelAudit({ id: this.id });
        this.auditFlag = 1;
    };

    /**
     * 驳回
     */
    async auditReject() {
        await spAdmin.rejectLabelAudit({ id: this.id });
        this.auditFlag = 2;
    }

    /**
     * 标签审核查询
     */
    async getById() {
        return spAdmin.getLabelAuditRecordById({ id: this.id });
    }

};

