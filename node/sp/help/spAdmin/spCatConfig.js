class SpCatConfig {
    constructor() {
        /**  主键编号 */
        this.id = '';
        /**  分类类型: 1-城市,2-风格,3-市场,4-类目,5-区域,6-推荐城市,7-主营类目,8-店铺标签,9-商圈,13-新主营类目 */
        this.type = 0;
        /**  对应分类的id(字典类型或指定表的id：type=1,5城市/2风格时：typeId=dict.type_id；type=3市场时：=sc_market.id；type=4类目时：=dres_class.id; type=6, 主营类目字典id)(type=9,对应商圈typid传0),type = 13是传标准类目id */
        this.typeId = '';
        /**  对应分类的值(仅type=1/2时的字典项时：=dict.code_value；其他类型无效) */
        this.typeValue = 0;
        /**  对应分类的名称(type=1城市/2风格时：type_id+type_value确定dict.code_name；type=3市场时：type_id确定sc_market.short_name；type=4类目时：type_id确定dres_class.name)(type=9传商圈名称),type = 13传标准类目名称 */
        this.typeName = '';
        /**  对应分类的访问码(层级类型的分类的访问码，如商品类目的accode) */
        this.typeAccode = '';
        /**  显示标志，位操作(0-不可见 1-卖家可见 2-买家可见 3-卖家买家都可见) */
        this.showFlag = 0;
        /**  状态(0-禁用 1-启用) */
        this.flag = 0;
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  显示顺序 */
        this.showOrder = 0;
        /**  推荐次序 */
        this.seqOrder = 0;
        /**  图片ID */
        this.docId = '';
        /**  扩展属性字段，存储格式为{“key”:”value”}, 例如分类中重量{“perNum”:”1”}，如果商圈保存格式为{“tradeId”:”商圈id”},如果报存别名{“alias”:”别名”},若保存风格时，需传图片，则传{“docContent”:[{“docId”:”xxx”},{“docId”:”xxx”}]}。增加季节属性season值为long（add by jiangxj），若添加类目最高价，在prop中传classHighestPrice{“alias”: “”, “perNum”: 0.3, “classHighestPrice”: “1”}，若想清除传classHighestPrice字段直接传null */
        this.prop = {};
        /**  归属用户偏好设置1代表是，0代表否 */
        this.hobbyFlag = 0;
    }

}