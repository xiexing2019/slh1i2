const common = require('../../../lib/common');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];

const frontCategory = module.exports = {};

/**
 * 初始化前台类目集
 */
frontCategory.setupFrontCategoryAction = function (params) {
    return new FrontCategory();
};

/**
 * 前台类目集
 * @description spconfg.sp_front_category
 */
class FrontCategory {
    constructor() {
        /** id */
        this.id = '';
        /** 名称 */
        this.name = '';
        /** 备注 */
        this.rem = '';
        /** 状态，0 无效 ，1 有效 */
        this.flag = 1;
    }

    getRandom() {
        this.name = `前台类目集${common.getRandomStr(5)}`;
        this.rem = common.getRandomStr(5);
        return this;
    }

    /**
     * 前台类目新增
     */
    async save() {
        const res = await spAuth.saveSpFrontCategoryAction({ id: this.id, name: this.name, rem: this.rem });
        this.id = res.result.data.val;
    }

    /**
     * 获取类目信息
     */
    async getDetail() {
        return spAuth.getSpFrontCategoryActionById({ id: this.id });
    }

    /**
     * 启用
     */
    async enable() {
        await spAuth.enableSpFrontCategoryAction({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAuth.disableSpFrontCategoryAction({ id: this.id });
        this.flag = 0;
    }
};

/**
 * 初始化前台类目项目
 */
frontCategory.setupFrontCategoryDetailAction = function (params) {
    return new FrontCategoryDetailAction();
};

/**
 * 前台类目项目
 * @description spconfg.sp_front_category_detail
 */
class FrontCategoryDetailAction {
    constructor() {
        /** id */
        this.id = '';
        /** 前台类目集id */
        this.categoryId = '';
        // /** 商品集id */
        // this.dresSetId = '';
        /** 商品集名称 */
        this.dresSetName = '';
        /** 父id，根节点为0 */
        this.parentId = 0;
        /** 状态，0 无效 ，1 有效 */
        this.flag = 1;
        /** 商品过滤条件 */
        this.dresFilter = {};
        /** 扩展字段 */
        this.prop = {};
        /** 图片id */
        this.docId = '';
        /** 备注 */
        this.rem = '';
    }

    async getRandom(params) {
        this.categoryId = params.categoryId;
        this.parentId = params.hasOwnProperty('parentId') ? params.parentId : 0;
        this.dresSetName = `类目${common.getRandomStr(5)}`;
        this.rem = common.getRandomStr(5);
        this.docId = docData.image.shopLogo[0].docId;
        this.rem = common.getRandomStr(5);

        const classList = await spDresb.findClassTreeList({ flag: 1, parentId: 1 }).then(res => res.result.data.rows);
        this.prop = classList.slice(0, 3).map(classInfo => { return { typeName: classInfo.name, id: classInfo.id } });
        this.dresFilter = { keyWords: { classId: this.prop.map(data => data.id) } };
        return this;
    }

    /**
     * 前台类目新增
     */
    async save() {
        const res = await spAuth.saveSpFrontCategoryDetailAction(this);
        // console.log(res);
        this.id = res.result.data.val;
    }

    /**
     * 前台类目更新
     */
    async update() {
        await spAuth.updateSpFrontCategoryDetailAction(this);
    }

    /**
     * 启用
     */
    async enable() {
        await spAuth.enableSpFrontCategoryDetailAction({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAuth.disableSpFrontCategoryDetailAction({ id: this.id });
        this.flag = 0;
    }

    /**
     * 删除
     */
    async delete() {
        await spAuth.deleteSpFrontCategoryDetailAction({ id: this.id });
        this.flag = -1;
    }

    /**
     * 初始化前台类目项目信息
     */
    async init() {
        await spAuth.initSpFrontCategoryDetailAction({ categoryId: this.categoryId });
        // console.log(`res=${JSON.stringify(res)}`);
    }

};

/**
 * 初始化场景绑定类目
 */
frontCategory.setupSaasCategoryRef = function (params) {
    return new SaasCategoryRef();
};

/**
 * 不同场景绑定类目
 * @description spconfg.sp_saas_category_ref
 */
class SaasCategoryRef {
    constructor(params) {
        /**  主键 */
        this.id = '';
        /**  saas_id */
        this.saasId = '';
        /**  仓库id  */
        this.warehouseId = '';
        /**  仓库名称 */
        this.warehouseName = '';
        /**  类目集合id */
        this.categoryId = '';
        /**  备注 */
        this.rem = '';
        /**  状态 0-禁用 1-正常 -1删除 */
        this.flag = 1;
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  更新时间 */
        // this.updatedDate = '';
        // /**  更新时间 */
        // this.updatedBy = '';
    }

    /**
     * 新增类目关系
     */
    async save(params) {
        const res = await spAuth.saveSpSaasCategoryRef(params);
        common.update(this, params);
        this.id = res.result.data.val;
    }

    /**
     * 启用/禁用
     * @param {object} params 
     * @param {string} params.flag 0-禁用 1-启用 
     */
    async changeFlag(params) {
        const res = await spAuth.changeSpSaasCategoryRefFlag({ id: this.id, flag: params.flag });
        this.flag = params.flag;
        return res;
    }

    /**
     * 根据仓库id获取类目id
     */
    async getDetailByWareHouseId() {
        return spAuth.getSpSaasCategoryRefByWareHouseId({ warehouseId: this.warehouseId, saasId: this.saasId });
    }

    /**
     * 从仓库类目列表中查找
     */
    async findDetailFromList() {
        const list = await spAuth.getSpSaasCategoryRefList({ warehouseNameLike: this.warehouseName, flag: this.flag })
            .then(res => res.result.data.rows);
        const detail = list.find(data => data.id == this.id);
        expect(detail).not.to.be.undefined;
        return detail;
    }

}
