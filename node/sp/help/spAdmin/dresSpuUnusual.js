const common = require('../../../lib/common');
const format = require('../../../data/format');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

/**
 * 人工审核流程已去除 只保留自动处理
 * 
 * 异常价格下架改为搜索隐藏 买家搜索不到  服务端2.1.8
 * http://119.3.46.172:6082/zentao/task-view-2276.html
 * 
 * 
 * 好店异常价格商品下架
 * http://119.3.46.172:6082/zentao/story-view-176.html
 */


// 真实数据源为大数据推送,这里仅模拟,spuId与pushFlag为必须，其他是沉余字段,展示用

/**
 * 好店异常商品
 */
class DresSpuUnusual {
    constructor(spuInfo) {
        /**  主键id */
        this.id = '';
        /**  异常类型0 异常价格 */
        this.typeId = 0;
        /**  卖家租户id，区分门店 */
        this.pushTenantId = spuInfo.tenantId || '';
        /**  门店名称 */
        this.pushTenantName = spuInfo.tenantName || '';
        /**  电话 */
        this.pushPhone = '';
        /**  商品spuId */
        this.spuId = spuInfo.id || '';
        /**  商品编号 */
        this.spuCode = spuInfo.code || '';
        /**  商品名称 */
        this.spuName = spuInfo.name || '';
        /**  发布价 */
        this.pubPrice = spuInfo.pubPrice || 0;
        /**  历史最低销售价 */
        this.offlinePrice = Math.max(this.pubPrice - 20, 0);
        /**  历史销售均价 */
        this.avgPrice = common.getRandomNum(this.offlinePrice, this.pubPrice);
        /**  下架标示，1为立即下架，0为有异常嫌疑，需运营人工确认 */
        this.pushFlag = 0;
        /**  商品确认状态，0 需下架 1 需上架 */
        this.sureFlag = 0;
        /**  是否人工操作过，0 没有 1 有 */
        this.operateFlag = 0;
        // /**  最后操作时间 */
        // this.operateDate = '';
        // /**  推送数据时间 */
        // this.pushDate = '';
        /**  扩展属性 */
        this.props = {};
        // /**  操作人id(系统操作id为0) */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    offMarket() {
        this.operateFlag = 1;
        return res;
    }
}

/**
 * 异常商品管理
 */
class DresSpuUnusualManager {
    constructor() {
        this.spus = new Map();
    }

    /**
     * 添加测试商品
     * @param {array} dresList 
     */
    addDres(dresList) {
        dresList.forEach(dres => {
            const dresSpu = dres.hasOwnProperty('spu') ? dres.spu : dres;
            this.spus.set(dresSpu.id, new DresSpuUnusual(dresSpu));
        });
        return this;
    }

    /**
     * 修改测试商品
     * @param {object} params 
     * @param {string} params.spuId 
     */
    changeDres(params) {
        const dresSpu = this.spus.get(params.spuId);
        common.update(dresSpu, params);
        this.spus.set(params.spuId, dresSpu);
    }

    /**
     * 异常价格数据推送存储 
     * @param {object} params 
     * @param {array} params.spuIds  
     */
    async saveUnusualPriceDres(params) {
        const mapField = 'avgPrice;pubPrice;offlinePrice;pushFlag;pushPhone;pushTenantId;pushTenantName;spuCode;spuId;spuName;sureFlag;typeId';
        const dresSpuUnusualList = params.spuIds.map(spuId => {
            const dresSpu = this.spus.get(spuId);
            if (!dresSpu) throw new Error(`未找到待测商品spuId=${spuId},请检查`);
            const data = format.dataFormat(dresSpu, mapField);
            // data.hideFrom = 1;// 后台写死为1
            data.hideReason = `test${common.getRandomStr(3)}`;
            return data;
        });
        const res = await spCommon.saveUnusualPriceDres({ isFirst: 0, dresSpuUnusualList });
        // console.log(`异常价格数据推送存储:\n\t${JSON.stringify(res)}`);
        return res;
    }

    /**
     * 人工下架异常价格商品 
     * @param {object} params 
     * @param {array} params.spuIds  
     */
    async offMarket(params) {
        const mapField = 'pushTenantId;spuId;typeId'
        const jsonParam = params.spuIds.map(spuId => {
            const dresSpu = this.spus.get(spuId);
            if (!dresSpu) throw new Error(`未找到待测商品spuId=${spuId},请检查`);
            return format.dataFormat(dresSpu, mapField);
        });
        const res = await spCommon.offMarketDresSpuUnusual({ jsonParam });
        // console.log(res.reqUrl);


        return res;
    }

    /**
     * 根据spuId获取异常价格商品列表中对应的数据
     * @param {object} params 
     * @param {array} params.spuId 
     */
    async findSpuUnusualList(params) {
        const dresSpu = this.spus.get(params.spuId);
        if (!dresSpu) throw new Error(`未找到待测商品spuId=${spuId},请检查`);
        // console.log(dresSpu);
        const dresList = await spCommon.findSpuUnusualList().then(res => res.result.data.rows);//{ spuCode, sureFlag }
        // console.log(`dresList=${JSON.stringify(dresList)}`);
        const spu = dresList.find(row => row.spuCode == dresSpu.spuCode);
        expect(spu, `获取异常价格商品列表中未找到推送的异常价格商品 spuId=${dresSpu.spuId}`).not.to.be.undefined;
        return spu;
    }
}

module.exports = {
    DresSpuUnusualManager
};