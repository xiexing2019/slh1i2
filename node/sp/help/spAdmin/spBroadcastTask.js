const common = require('../../../lib/common');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const mockJsonParam = require('../mockJsonParam');

/** 消息推送任务 */
class SpBroadcastTask {
    constructor(params) {
        /**  租户ID */
        this.id = '';
        /**  消息类型 1通知 2活动 3优惠券 4推送日志 5店铺 6商品 7限时购活动 */
        this.typeId = 0;
        /**  开始推送时间 */
        this.startTime = '';
        /**  推送范围 */
        this.sendScope = 0;
        /**  推送租户ids */
        this.tenantIds = {};
        /**  推送的消息文本 */
        this.msgContent = '';
        /**  推送的图片id */
        this.msgDocId = '';
        /**  推送的内部链接 */
        this.msgInnerUrl = '';
        /**  推送的外部链接 */
        this.msgOutUrl = '';
        /**  优惠券id列表 */
        this.couponIds = {};
        /**  需推送数 */
        this.needSendNum = '';
        /**  已推送数 */
        this.haveSendNum = '';
        /**  备注 */
        this.rem = '';
        /**  状态 1创建 2生效 3推送中 4暂停 5结束 */
        this.flag = 0;
        /**  防重复提交 */
        this.hashKey = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  推送的消息标题 */
        this.msgTitle = '';
        /**  通知方式 1 通知 2 命令 3 弹窗 为3时,不可修改*/
        this.methodType = 0;
        /**  链接方式 0 无消息 1 外链 2 内链 */
        this.linkType = 0;
        /**  扩展属性 */
        this.props = {};
    }

    /**
     * 保存消息通知任务
     * @param {object} params 
     */
    async saveTask(params) {
        // methodType=3时不能修改 流程中单独校验
        if (this.id && (this.methodType == 3 || this.flag == 5)) {
            return;
        }
        // console.log(params);

        const res = await spCommon.saveSpBroadcastTask(params);
        common.update(this, params);
        this.id = res.result.data.val;
        this.flag = 2;
        this.needSendNum = params.tenantIdList.length;
    }

    /**
     * 停用消息通知任务
     */
    async stopTask() {
        if (this.flag == 5) return;
        await spCommon.stopSpBroadcastTask({ id: this.id });
        this.flag = 4;
    }

    /**
     * 启用消息通知任务
     */
    async startTask() {
        if (this.flag == 5) return;
        await spCommon.startSpBroadcastTask({ id: this.id });
        this.flag = 2;
    }

    /**
     * 查询消息通知任务信息
     * @description 详情
     */
    async getMsg() {
        return spCommon.getSpBroadcastTaskMsg({ id: this.id });
    }

}

module.exports = SpBroadcastTask;