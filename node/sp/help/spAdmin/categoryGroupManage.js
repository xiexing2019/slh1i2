const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

const categoryGroupManage = module.exports = {};

categoryGroupManage.setupCategoryGroup = function (params) {
    return new CategoryGroup();
};

/**
 * 类目集
 */
class CategoryGroup {
    constructor() {
        /** id */
        this.id = '';
        /** 名称 */
        this.name = '';
        /** 状态，0 无效 ，1 有效 */
        this.flag = 1;
    }

    getRandom() {
        this.name = `类目集${common.getRandomStr(5)}`;
        return this;
    }

    /**
     * 类目集新增
     */
    async save() {
        const res = await spAuth.saveSpCategoryGroup({ id: this.id, name: this.name, flag: this.flag });
        this.id = res.result.data.val;
    }

    /**
     * 通过id查询类目集信息 
     */
    async getDetail() {
        return spAuth.getSpCategoryGroupById({ id: this.id });
    }

    /**
     * 启用
     */
    async enable() {
        await spAuth.enableSpCategoryGroup({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAuth.disableSpCategoryGroup({ id: this.id });
        this.flag = 0;
    }

    /**
     * 删除
     */
    async delete() {
        await spAuth.deleteSpCategoryGroup({ id: this.id });
        this.flag = -1;
    }

};

