const common = require('../../../lib/common');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spugr = require('../../../reqHandler/sp/global/spugr');

const spLabelManage = module.exports = {};

/**
 * 初始化驳回理由管理
 */
spLabelManage.setupRejectReason = function (params) {
    return new RejectReason();
};

/**
 * 驳回理由管理
 */
class RejectReason {
    constructor() {
        /**  主键 */
        this.id = '';
        /**  驳回原因 */
        this.rejectReason = '';
        /**  序号 */
        this.showOrder = 0;
        /**  状态值 1-正常 0-禁用 -1删除 */
        this.flag = 0;
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  更新时间 */
        // this.updatedDate = '';
    }

    getRandom() {
        this.rejectReason = `驳回理由${common.getRandomStr(5)}`;
        this.showOrder = common.getRandomNum(1, 20);
        return this;
    }

    /**
     * 新增修改驳回理由
     */
    async save() {
        const res = await spAdmin.saveSpRejectReason({ id: this.id, showOrder: this.showOrder, rejectReason: this.rejectReason });
        if (!this.id) this.flag = 1;
        this.id = res.result.data.val;
    }

    /**
     * 从列表中查询
     * @param {object} params 
     */
    async getFromList(params = {}) {
        const res = await spAdmin.getSpRejectReasonList({ rejectReason: this.rejectReason, flag: this.flag, ...params });
        const data = res.result.data.rows.find(val => val.id = this.id);
        expect(data, `${res.reqUrl}\n\t未查询到驳回理由 id=${this.id}`).not.to.be.undefined;
        return data;
    }

    /**
     * 启用
     */
    async enable() {
        await spAdmin.enableSpRejectReason({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAdmin.disableSpRejectReason({ id: this.id });
        this.flag = 0;
    }

    /**
     * 删除
     */
    async deleteReason() {
        await spAdmin.deleteSpRejectReason({ id: this.id });
        this.flag = -1;
    }

};

