const common = require('../../../lib/common');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const EventEmitter = require('events').EventEmitter;

const main = module.exports = {};

/**
 *  商品最高价配置
 *  @description 商品价格高于最高价 商品设置为搜索隐藏hideFlag=>1
 */
class TopPriceConfig extends EventEmitter {
    constructor(params) {
        super();

        /**  id */
        this.id = '';
        /**  大租户id */
        this.saasId = '';
        /**  应用类型 1-搜索限价 2-上新限价 */
        this.confType = 0;
        /**  类目 0=默认通用 */
        this.classId = '';
        /**  城市 0=默认通用 */
        this.cityId = '';
        /**  最高价 */
        this.topPrice = 0;
        /**  状态 */
        this.flag = 0;
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';

        this.on('update', function (params) {
            common.update(this, params);
        });
    }

}

main.topPriceManage = function (params) {
    this.topPriceConfig = new TopPriceConfig();
};

/**
 * 商品最高价配置列表中查找目标配置
 * @description 默认取第一条
 */
main.findTopPriceConfigFromList = async function (params = {}, options, cb) {
    const configListRes = await spAdmin.getSpTopPriceConfigList({ flag: 1, ...params });
    const topPriceConfig = typeof cb == 'function' ? cb(configListRes.result.data.rows) : configListRes.result.data.rows[0];
    options.check && expect(topPriceConfig).not.to.be.undefined;
    return topPriceConfig;
};

