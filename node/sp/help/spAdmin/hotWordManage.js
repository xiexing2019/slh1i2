const common = require('../../../lib/common');
const format = require('../../../data/format');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

/**
 * 热门关键词
 */
class HotWord {
    constructor(wordInfo) {
        /**  主键 */
        this.id = '';
        /**  插入类型 0自动 1人工 */
        this.typeId = 0;
        /**  热词 唯一*/
        this.wordName = '';
        /**  生效开始时间 */
        this.startTime = '';
        /**  结束时间 */
        this.endTime = '';
        /**  状态值 1有效 0无效 */
        this.flag = 0;
        /**  备注 */
        this.rem = '';
        /**  链接地址 */
        this.linkType = 0;
        /**  序号 */
        this.showOrder = 0;
        /**  扩展属性 */
        this.props = {};
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  更新时间 */
        // this.updatedDate = '';

        common.update(this, wordInfo);
    }

    /**
    * 新增修改热门关键词
    */
    async saveHotWords(params = {}) {
        params = Object.assign({}, this, params);
        const saveRes = await spAdmin.saveHotWords(params);
        // console.log(`saveRes=${JSON.stringify(saveRes)}`);

        common.update(this, params);
        this.id = saveRes.result.data.val;
        this.flag = 1;
        this.typeId = 1;
        return saveRes;
    }

    /**
     * 上新关键词
     */
    async onLine() {
        await spAdmin.onLineHotWord({ id: this.id });
        this.flag = 1;
        return this;
    }

    /**
    * 下架关键词
    */
    async offLine() {
        await spAdmin.offLineHotWord({ id: this.id });
        this.flag = 0;
        return this;
    }
};

const setUpHotWord = function (params) {
    return new HotWord(params);
};

/**
 * 生成随机热词
 */
const getRandomHotWord = function () {
    const fmt = 'YYYY-MM-DD HH:mm:ss';
    return {
        wordName: common.getRandomChineseStr(3),
        showOrder: common.getRandomNum(100, 200),
        startTime: common.getCurrentDate(fmt),
        endTime: common.getDateString([0, 0, 1], fmt),
        linkType: 0
    }
};

/**
 * 从关键词列表中筛选出关键词
 * @param {object} params 
 * @param {number} params.size 
 */
const getEveHotWords = async function name(params) {
    const words = [], curTime = common.getCurrentDate('YYYY-MM-DD HH:mm:ss');
    const hotWordsList = await spAdmin.findHotWordsList({ flag: 1, typeId: 1 }).then(res => res.result.data.rows);
    hotWordsList.forEach((hotWord) => {
        if (hotWord.startTime <= curTime && hotWord.endTime >= curTime) words.push(hotWord.wordName);
    });
    const hotWordsList2 = await spAdmin.findHotWordsList({ flag: 1, typeId: 0 }).then(res => res.result.data.rows);
    hotWordsList2.forEach((hotWord) => {
        if (hotWord.startTime <= curTime && hotWord.endTime >= curTime) words.push(hotWord.wordName);
    });
    return words.slice(0, 10);
};

module.exports = {
    setUpHotWord,
    getRandomHotWord,
    getEveHotWords
};