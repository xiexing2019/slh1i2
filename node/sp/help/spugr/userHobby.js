const common = require('../../../lib/common');
const format = require('../../../data/format');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spugr = require('../../../reqHandler/sp/global/spugr');
const mysql = require('../../../reqHandler/sp/mysql');

/**
 * 用户偏好
 */
class UserHobby {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  租户id */
        this.tenantId = '';
        /**  单元id */
        this.unitId = '';
        /**  用户类别，1代表买家 */
        this.typeId = 0;
        /**  商品类目 */
        this.classId = '';
        /**  拿货价格区间取字典，字典类型2033 */
        this.priceRange = 0;
        /**  常拿货源地城市 */
        this.goodsSourceCity = '';
        /**  店铺风格，取字典，字典类型2004可能多个逗号分隔 */
        this.shopStyle = '';
        /**  扩展属性 */
        this.props = {};
        /**  状态 */
        this.flag = 0;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    async saveOrUpdate(params) {
        const saveRes = await spugr.saveOrUpdateSpUserHobby(params);
        // console.log(`saveRes=${JSON.stringify(saveRes)}`);

        common.update(this, params);
        this.tenantId = LOGINDATA.tenantId;
        this.unitId = LOGINDATA.unitId;
        this.flag = 1;
        this.id = saveRes.result.data.val;
        if (params.hasOwnProperty('manualInputClass')) this.props.manualInputClass = params.manualInputClass;

        return saveRes;
    };

    async checkInfoBySql() {
        const userHobbyInfo = await getUserHobby({ tenantId: this.tenantId });
        const exp = {};
        for (const key in userHobbyInfo) {
            exp[_.camelCase(key)] = userHobbyInfo[key];
        }
        common.isApproximatelyEqualAssert(exp, this);
    }

};

const setUpUserHobby = function () {
    return new UserHobby();
};

/**
 * 获取用户偏好信息
 * @param {string} tenantId 
 */
const getUserHobby = async function ({ tenantId }) {
    const spg = await mysql({ dbName: 'spgMysql' });
    const [userHobbyList] = await spg.query(`SELECT * FROM spugr.sp_user_hobby WHERE tenant_id=${tenantId}`);
    await spg.end();
    expect(userHobbyList, `用户偏好表中 tenant_id=${tenantId} 数据匹配有误`).to.have.lengthOf(1);
    return userHobbyList.shift();
};

const getRandomUserHobby = async function () {
    // 拿货价格区间
    BASICDATA['2033'] = await spugr.getDictList({ typeId: '2033', flag: 1 }).then((res) => res.result.data.rows);
    // 店铺风格
    BASICDATA['2004'] = await spugr.getDictList({ typeId: '2004', flag: 1 }).then((res) => res.result.data.rows);
    BASICDATA['850'] = await spugr.getDictList({ typeId: '850', flag: 1 }).then((res) => res.result.data.rows);

    const classIds = await spCommon.getCatConfigList({ type: 4, flag: 1 }).then(res => res.result.data.rows);

    return {
        priceRange: common.randomSort(BASICDATA['2004']).shift().codeValue,//字典2033
        classId: common.randomSort(classIds).slice(0, 2).map(info => info.typeId).join(','),
        manualInputClass: common.getRandomChineseStr(2),
        goodsSourceCity: common.randomSort(BASICDATA['850']).shift().codeValue,
        shopStyle: common.randomSort(BASICDATA['2004']).slice(0, 2).map(info => info.codeValue).join(','),//字典2004
        typeId: 1,
    };
};

module.exports = {
    getUserHobby,
    getRandomUserHobby,
    setUpUserHobby
};

