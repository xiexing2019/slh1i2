const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const mysql = require('../../../reqHandler/sp/mysql');

const saasLevelManage = module.exports = {};

saasLevelManage.setupSpSaasLevel = function (params) {
    return new SpSaasLevel();
};

class SpSaasLevel {
    constructor(params) {
        /**  主键 */
        this.id = '';
        /**  saasid */
        this.saasId = '';
        /**  等级名称 */
        this.levelName = '';
        /**  字典code */
        this.levelCode = '';
        /**  状态值 1-正常 0-禁用 -1 -删除 */
        this.flag = 1;
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  创建者 */
        // this.createdBy = '';
        // /**  更新时间 */
        // this.updatedDate = '';
        // /**  更新者 */
        // this.updatedBy = '';
    }

    getRandom() {
        const str = common.getRandomStr(4);
        this.saasId = 1;
        this.levelCode = `level${str}`;
        this.levelName = `等级${str}`;
        return this;
    }

    async save() {
        const res = await spugr.saveSpSaasLevel({ id: this.id, saasId: this.saasId, levelCode: this.levelCode, levelName: this.levelName });
        // console.log(res);
        this.id = res.result.data.val;
        // await this.findId();
    }

    async changeFlag(params) {
        await spugr.changeSpSaasLevelFlag({ id: this.id, flag: params.flag });
        this.flag = params.flag;
    }

    async findId() {
        const saasLevelList = await spugr.findSaasLevelList({ saasId: this.saasId }).then(res => res.result.data.rows);
        const saasLevel = saasLevelList.find(data => data.levelCode == this.levelCode);
        expect(saasLevel).not.to.be.undefined;
        this.id = saasLevel.id;
    }

}