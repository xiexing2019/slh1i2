const common = require('../../../lib/common');
const spconfg = require('../../../reqHandler/sp/global/spconfg');

const spSearchScword = module.exports = {};

spSearchScword.setupSearchScword = function (params) {
    return new SearchScword();
};

/**
 * 搜索自检关键词
 */
class SearchScword {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  自检关键词 */
        this.scword = '';
        /**  期望结果数 */
        this.expectNum = 0;
        /**  来源类型。0 人工录入，1 自动生成 */
        this.srcType = 0;
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    getRandom() {
        this.scword = common.getRandomChineseStr(5);
        this.expectNum = common.getRandomNum(100, 200);
        return this;
    }

    /**
     * 新增/修改自检关键词
     */
    async save() {
        const res = await spconfg.saveSpSearchScword({ id: this.id, scword: this.scword, expectNum: this.expectNum });
        this.id = res.result.data.val;
    }

    /**
     * 列表中查询本关键字
     */
    async getFromList(params = {}) {
        const list = await spconfg.getSpSearchScwordList({ scword: this.scword, ...params }).then(res => res.result.data.rows);
        const data = list.find(row => row.id == this.id);
        expect(data, `自检关键词列表中未查询到关键词 id=${this.id} scword=${this.scword}`).not.to.be.undefined;
        return data;
    }

    /**
     * 通过id查询自检关键词
     */
    async getDetailById(params = {}) {
        return spconfg.getSpSearchScwordById({ id: this.id, ...params });
    }

    /**
     * 删除
     */
    async deleteScwords() {
        await spconfg.deleteSpSearchScword({ id: this.id });
        this.flag = -1;
    }

};


