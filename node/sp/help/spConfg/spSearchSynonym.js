const common = require('../../../lib/common');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

const spSearchSynonym = module.exports = {};

spSearchSynonym.setupSynonym = function (params) {
    return new SearchSynonym();
};

/**
 * 搜索同义词
 */
class SearchSynonym {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  搜索名称 */
        this.searchName = '';
        /**  标准名称 */
        this.stdName = '';
        /**  以逗号分隔的所有同义词 */
        this.synonyms = [];
        /**  同义词 */
        this.synonymList = [];
        /**  状态 0 停用，1 正常 */
        this.flag = 1;
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    getRandom() {
        this.stdName = common.getRandomStr(5);
        this.searchName = this.stdName + common.getRandomChineseStr(2);
        this.synonymList = [this.stdName + common.getRandomChineseStr(2), this.stdName + common.getRandomChineseStr(2)];
        return this;
    }

    /**
     * 保存搜索同义词
     */
    async save() {
        const res = await spCommon.saveSpSearchSynonym({ id: this.id, searchName: this.searchName, stdName: this.stdName, synonymList: this.synonymList });
        this.id = res.result.data.val;
    }

    /**
     * 查看搜索同义词
     */
    async getDetail() {
        return spCommon.getSpSearchSynonym({ id: this.id });
    }

    /**
     * 获取期望值
     */
    getDetailExp() {
        this.synonyms = this.synonymList.toString();
        return this;
    }

    /**
     * 启用
     */
    async enable() {
        await spCommon.enableSpSearchSynonym({ id: this.id });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spCommon.disableSpSearchSynonym({ id: this.id });
        this.flag = 0;
    }

};


