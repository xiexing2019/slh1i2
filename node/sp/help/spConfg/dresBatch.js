const common = require('../../../lib/common');
const spconfg = require('../../../reqHandler/sp/global/spconfg');
const spugr = require('../../../reqHandler/sp/global/spugr');

/**
 * 起批量
 */
const batchManage = module.exports = {};

/**
 * 初始化混批模板
 */
batchManage.setupSpBatchTemplate = function (params) {
    return new SpBatchTemplate();
};

/**
 * 混批模板
 */
class SpBatchTemplate {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  起批模板名 */
        this.name = '';
        /**  混批量形如{"batchNum1":"""batchNum2":""} */
        this.batchNum = {};
        /**  状态 1代表启用,0代表停用,-1代表删除 */
        this.flag = 0;
        /**  扩展字段 */
        this.props = {};
        /**  备注 */
        this.rem = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    /**
     * 设置混批模板
     */
    async save(params) {
        const res = await spconfg.saveSpBatchTemplate(params);
        common.update(this, params);
        this.id = res.result.data.val;
        this.flag = 1;
    }

    /**
     * 停用启用删除混批模板
     * @param {object} params 
     * @param {string} params.flag 1代表启用,0代表停用,-1代表删除
     */
    async updateFlag(params) {
        await spconfg.updateSpBatchTemplateFlag({ id: this.id, flag: params.flag });
        this.flag = params.flag;
    }

    /**
     * 从混批列表中查找本模板
     * @param {object} params 
     */
    async findFromList(params = {}) {
        const res = await spconfg.findSpBatchTemplateList({ name: this.name, flag: this.flag, ...params });
        const template = res.result.data.rows.find(row => row.id == this.id);
        if (this.flag != -1) {
            expect(template, `${res.reqUrl}\n\t未查询到混批模板 id=${this.id}`).not.to.be.undefined;
            return template;
        } else {
            expect(template, `${res.reqUrl}\n\t查询到已删除的混批模板 id=${this.id}`).to.be.undefined;
        }
    }

};


/**
 * 初始化起批价表
 */
batchManage.setupBatchPrice = function (params) {
    return new SpBatchPrice();
};

/**
 * 起批价表
 * @description 已废弃
 */
class SpBatchPrice {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  租户id */
        this.tenantId = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';

        this.priceMap = new Map();
    }

    setUser(params) {
        this.unitId = params.unitId;
        this.tenantId = params.tenantId;
        this.saasId = params.saasId;
        return this;
    }

    /**
     * 设置单个混批价
     */
    async save(params) {
        params.unitId = this.unitId;
        params.tenantId = this.tenantId;
        params.saasId = this.saasId;

        const res = await spconfg.saveSpBatchPrice(params);
        common.update(this, params);
        this.id = res.result.data.val;
        this.flag = 1;
    }

    /**
     * 批量设置自定义混批价
     */
    async batchSave(params) {
        params.forEach(element => {
            element.unitId = this.unitId;
            element.tenantId = this.tenantId;
            element.saasId = this.saasId;
        });
        const res = await spconfg.batchSaveSpBatchPrice(params);
        console.log(`res=${JSON.stringify(res)}`);

        res.params.jsonParam.forEach(data => {
            const batchPrice = new BatchPrice();
            common.update(batchPrice, data);
            this.priceMap.set(data.name, batchPrice);
        });
    }

    /**
     * 停用启用混批价
     * @param {object} params 
     * @param {string} params.flag 1代表启用,0代表停用,-1代表删除
     */
    async updateFlag(params) {
        await spconfg.updateSpBatchPriceFlag({ id: this.id, flag: params.flag });
        this.flag = params.flag;
    }

    /**
     * 从混批价列表中查找
     * @param {object} params 
     */
    async findFromList(params = {}) {
        const res = await spconfg.findSpBatchPriceList({ unitId: this.unitId, tenantId: this.tenantId, flag: this.flag, ...params });
        const batchPrice = res.result.data.rows.find(row => row.id == this.id);
        if (this.flag != -1) {
            expect(batchPrice, `${res.reqUrl}\n\t未查询到混批价 id=${this.id}`).not.to.be.undefined;
            return batchPrice;
        } else {
            expect(batchPrice, `${res.reqUrl}\n\t查询到已删除的混批价 id=${this.id}`).to.be.undefined;
        }
    }

};

class BatchPrice {
    constructor(params) {
        /**  主键id */
        this.id = '';
        /**  租户id */
        this.tenantId = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';
        /**  名称 */
        this.name = '';
        /**  
         * 根据商陆花价格计算起批价的规则形如{"priceRate":"0.4" "priceAddNum""1" "priceType":"1"};
         * priceRate代表与商陆花价格相乘的比例，priceAddNum代表与商陆花价格相加的比例priceType代表价格类型 
         **/
        this.priceRule = {};
        /**  状态 1代表启用,0代表停用,-1代表删除*/
        this.flag = 0;
        /**  扩展字段 */
        this.props = {};
        /**  备注 */
        this.rem = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }
}

batchManage.setupMktSalesPlan = function () {
    return new MktSalesPlan();
};

/**
 *  促销规则计划
 */
class MktSalesPlan {
    constructor() {
        /**  编码 */
        this.id = '';
        /**  名称 */
        this.name = '';
        /**  事件类型 */
        this.planType = 0;
        /**  描述 */
        this.description = '';
        /**  大租户id */
        this.saasId = '';
        /**  门店ID */
        this.shopId = '';
        /**  单元ID */
        this.unitId = '';
        /**  赠送网关 格式如[{"id":"规则id""execType":"促销的类型""execNum":"促销的数量""ruleContent":"规则内容"}] */
        this.giveGateWay = {};
        /**  状态 */
        this.flag = 0;
        /**  拓展属性 */
        this.extProp = {};
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  适用规则id */
        this.ruleId = '';
    }

    setUser(params) {
        this.unitId = params.unitId;
        this.tenantId = params.tenantId;
        this.saasId = params.saasId;
        return this;
    }

    async findFromList(params = {}) {
        const dataList = await spconfg.findSpBatchPriceList({ unitId: this.unitId, tenantId: this.tenantId, ...params }).then(res => res.result.data.rows);
        console.log(`dataList=${JSON.stringify(dataList)}`);

        // const data=dataList.find(data=>data)
    }

}
