const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

const innerStaffMsg = module.exports = {};

innerStaffMsg.setupInnerStaffMsg = function (params) {
    return new InnerStaffMsg();
};

/**
 * 内部人员信息
 */
class InnerStaffMsg {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  对应类型1代表测试人员，2代表内部人员 */
        this.typeId = 0;
        /**  名称 */
        this.name = '';
        /**  租户id */
        this.tenantId = '';
        /**  手机号 */
        this.mobile = '';
        /**  状态 */
        this.flag = 1;
        /**  扩展字段 */
        this.props = {};
        /**  备注 */
        this.rem = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }

    getRandom({ userInfo = LOGINDATA } = {}) {
        if (!this.id) {
            this.mobile = userInfo.mobile;
            this.tenantId = userInfo.tenantId;
        }

        this.name = 'luxx';
        this.typeId = common.getRandomNum(1, 2);
        return this;
    }

    /**
     * 新增修改内部人员信息
     */
    async save() {
        const res = await spAuth.saveInnerStaffMsg({ id: this.id, mobile: this.mobile, typeId: this.typeId, name: this.name });
        this.id = res.result.data.val;
    }

    /**
     * 启用
     */
    async enable() {
        await spAuth.updateInnerStaffMsgFlag({ id: this.id, flag: 1 });
        this.flag = 1;
    };

    /**
     * 停用
     */
    async disable() {
        await spAuth.updateInnerStaffMsgFlag({ id: this.id, flag: 0 });
        this.flag = 0;
    }

};


