



let noticeHelp = module.exports = {};


//消息提醒
noticeHelp.msgRemindingJson = (params) => {
    let msgList = {
        71: { tagOneIn: 71, text: '您收到了新的订单', title: '新订单提醒' },
        72: { tagOneIn: 72, text: '买家已经付款', title: '买家已付款' },
        78: { tagOneIn: 78, text: '请尽快发货', title: '买家提醒' },
        90: { tagOneIn: 90, text: '新订单已发货', title: '订单已发货' },
        77: { tagOneIn: 77, text: '买家已经确认收货', title: '收货提醒' },
        92: { tagOneIn: 92, text: `${purRes.params.jsonParam.orders[0].details[0].spuTitle}`, title: '卖家已取消订单' },
        73: { tagOneIn: 73, text: '买家已经取消订单', title: '买家取消订单' }
    };
    return msgList;
};