const accountBizOrderManage = module.exports = {};

// accountBizOrderManage

/**
 * 业务订单明细
 */
class AccountBizOrderDetail {
    constructor(params) {
        /**  商户sn */
        this.mchNo = '';
        /**  商户结余金额单位为分 */
        this.amount = '';
        /**  附加信息json */
        this.attch = '';
        /**  交易日期，格式：YYYYmmDDHHMMSS */
        this.payDate = '';
        /**  支付渠道名称 */
        this.channelName = '';
        /**  支付渠道编号，中信定义 */
        this.channelType = '';
        /**  状态，0：新建，1：已提交，2:审核通过；3:审核不通过；4:已解冻 */
        this.status = 0;
        /**  业务类型 */
        this.bizType = 0;
        /**  业务订单号 */
        this.bizOrderId = '';
        /**  业务流水号，用于和清分系统交互 */
        this.transCode = '';
        /**  支付单号 */
        this.payId = '';
        /**  支付渠道流水号 */
        this.channelOrderId = '';
        /**  子订单号 */
        this.bizSubOrderId = '';
        /**  支付订单交易类型，1-支付 2-退货 空格-其他 */
        this.payType = '';
        /**  订单交易类型，1 -实时交易支付 2 -实时交易退货 3 -预付交易支付 4 -预付交易撤销 5 -预付交易完成 */
        this.orderType = 0;
        /**  清算资金来源，1-支付渠道 2-内部划转 */
        this.moneySource = 0;
        /**  渠道手续费承担方式，1-平台商户承担 2-子商户承担 */
        this.feeSource = 0;
        /**  订单金额，单位为分 */
        this.orderAmount = '';
        /**  支付金额，单位为分 */
        this.payAmount = '';
        /**  折扣金额，单位为分 */
        this.discountAmount = '';
        /**  平台分成金额，单位为分 */
        this.shareAmount = '';
        /**  平台垫款金额，单位为分 */
        this.prepayAmount = '';
        /**  渠道手续费，单位为分 */
        this.feeAmount = '';
        /**  描述 */
        this.description = '';
        /**  支付方式 */
        this.payWayType = 0;
        /**  单元id(租户) */
        this.unitId = '';
        /**  租户id */
        this.tenantId = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  更新时间 */
        // this.updatedDate = '';
    }
}