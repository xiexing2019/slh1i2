const format = require('../../data/format');

let getExp = module.exports = {};

getExp.searchDresExp = function (params) {
    const _this = params.result.data;
    let exp = {
        createdBy: _this.createdBy,
        detailUrl: `sp_domain_url/spb/api.do?apiKey=ec-spdresb-dresSpu-getFullForBuyer&_cid=&_tid=17&spuId=64&buyerId=469`,
        docHeader: '',
        id: _this.id,
        labels: [],
        pubPrice: 250,
        sellNum: 0,
        tagPrice: 100,
        title: "商品Ejf2k",
        updatedBy: _this.updatedBy
    };
    return exp;
};

getExp.medalExp = function (params) {
    let exp = {
        ruleKind: 0,
        showOrder: 0,
        triggerType: 1,
        triggerPeriod: 0,
    };
    exp = Object.assign(exp, params);
    return exp;
};

/**
 * 店铺日志流期望值
 * @description 当前角色应该为卖家
 * @param {object} params
 * @param {string} params.createdDate
 * @param {string} params.flowType
 * @param {object} params.clientInfo
 */
getExp.shopLogListExp = function (params) {
    const nameKey = [6, 7].includes(params.flowType) ? 'nickName' : 'nickName';
    const clientInfo = format.dataFormat(params.clientInfo, `buyerId=tenantId;buyerName=${nameKey};buyerMobile=mobile`);
    return {
        createdDate: params.createdDate,
        flowType: params.flowType,
        shopId: LOGINDATA.shopId,
        unitId: LOGINDATA.unitId,
        ...clientInfo
    };
};