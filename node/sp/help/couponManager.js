const common = require('../../lib/common');
const format = require('../../data/format');
const spCoupon = require('../../reqHandler/sp/global/spCoupon');
const spugr = require('../../reqHandler/sp/global/spugr');
const spAccount = require('../data/spAccount');
const spCaps = require('../../reqHandler/sp/spCaps');

let couponManager = module.exports = {};

couponManager.setUpCardCoupon = function () {
    return new CardCoupon();
};

/**
 * 卡券
 * @description 卡券模板 用户领用后才生成实例
 */
class CardCoupon {
    constructor(params) {
        /**  编号 */
        this.id = '';
        /**  卡券标题 */
        this.title = '';
        /**  副标题 */
        this.subTitle = '';
        /**  logo */
        this.logoId = '';
        /**  卡券大类 卡券大类：1优惠券、2礼品券、3运费券；取值调用ec-cardCoupons-dcodes&domain=coupTypeDomain*/
        this.cardType = 0;
        /**  分享能力位：0没有能力 1可分享；默认不分享 */
        this.shareBits = 0;
        /**  卡券所属范围类型 0普通券 1内部券；默认普通券*/
        this.scopeType = 0;
        /**  卡券颜色 */
        this.color = '';
        /**  卡券状态 */
        this.flag = 0;
        /**  库存数量 */
        this.quantity = '';
        /**  发行数量 */
        this.totalQuantity = '';
        /**  领券限制 */
        this.getLimit = 0;
        /**  二维码id */
        this.qrCodeId = '';
        /**  优惠详情 */
        this.description = '';
        /**  使用须知 */
        this.notice = '';
        /**  客服电话 */
        this.servicePhone = '';
        /**  所属门店 */
        this.shopId = '';
        /**  使用规则 */
        this.ruleId = '';
        /**  单元id */
        this.unitId = '';
        /**  领取后有效时间 */
        this.receiveEffectPeriod = 0;
        /**  领取后有效时间单位 默认天：0年, 1月, 2天, 3时 */
        this.receiveEffectPeriodUnit = 0;
        /**  自动翻倍 */
        this.autoDouble = 0;
        /**  卡券标签位 */
        this.couponTag = '';
        /**  审核人 */
        this.auditUserId = '';
        /**  审核时间 */
        this.auditDate = '';
        /**  分支ID，小租户ID */
        this.branchId = '';
        /**  达成金额 */
        this.fulfilValue = 0;
        /**  优惠金额 */
        this.execNum = 0;
        /**  冗余其他优惠项目 */
        this.execOther = [];
        /**  冗余维度信息 */
        this.dims = [];
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  生效时间 */
        this.effectiveDate = '';
        /**  过期时间 */
        this.expiresDate = '';
        /**  分享须知 */
        this.shareNotice = '';
        /**  生效门店 */
        this.effectShopId = '';
    }

    /**
     * 生成随机卡券
     * @description 默认发行数量限制为1 防止被其他人使用
     * @param {object} params 
     * @param {string} [params.cardType=1] 卡券大类：1优惠券、2礼品券、3运费券 
     */
    async getRandomJson(params) {
        this.cardType = params.hasOwnProperty('cardType') ? params.cardType : 1;
        this.scopeType = 0;
        this.shareBits = 0;
        this.totalQuantity = 1;
        this.effectiveDate = common.getCurrentDate();
        this.expiresDate = common.getDateString([0, 0, 1]);
        this.shareNotice = common.getRandomStr(5);
        this.receiveEffectPeriod = common.getRandomNum(1, 5);
        this.receiveEffectPeriodUnit = 2;
        this.getLimit = 1;
        this.fulfilValue = 50;
        this.execNum = 5;

        this.color = await spCoupon.getDcodes({ domain: 'cardColorDomain' })
            .then(res => res.result.data.options[0].value);

        // 冗余维度信息
        // 指定自动化的门店，防止被他人使用
        // 运费券 服务端限制 只能全平台使用 不能限制店铺
        if (this.cardType != 3) {
            const shopName = spAccount.seller.shopName;
            const tenantId = await spugr.findShopBySearchToken({ searchToken: shopName, shadowFlag: 2 })
                .then(res => res.result.data.rows.find(row => row.name == shopName || row.tenantName == shopName).id);

            this.dims = [{
                dimName: 'dimShop',
                dimLevel: 'level_shop',
                dimValue: tenantId,
                dimValueCaption: shopName
            }];
        }

        // if (this.cardType==2) {
        // this.execOther=[{id:'赠送商品id',num:'赠送商品的数量',caption:'赠送商品的名称'}]
        // }

        return this;
    }

    /**
     * 保存卡券
     */
    async saveCardCoupon() {
        const params = format.dataFormat(this, 'id;cardType;scopeType;shareBits;color;receiveEffectPeriodUnit;totalQuantity;getLimit;fulfilValue;receiveEffectPeriod;effectiveDate;expiresDate;shareNotice;execNum;dims');
        const saveRes = await spCoupon.saveCardCoupon(params);

        this.id = saveRes.result.data.val;
        this.flag = 1;

        const isSpAdmin = spCaps.isSpgSessionId();
        this.unitId = isSpAdmin ? -10 : LOGINDATA.unitId;
    }


};

/**
 * 用户领用卡券
 * @param {array} coupons 卡券数组 
 */
couponManager.receiveMyCoupons = async function ({ coupons }) {
    if (!coupons.length || coupons.length == 0) {
        console.warn(`参数coupons错误,请检查`);
    }

    const _coupons = coupons.map(coupon => {
        return {
            couponId: coupon.id,
            couponUnitId: coupon.unitId,
            hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
            // 服务端不做校验  暂时写死
            receiveChannelType: 1,
            receiveFrom: -10,
        };
    });

    // 领用卡券
    const res = await spCoupon.receiveMyCoupons({ coupons: _coupons });
    // console.log(`res=${JSON.stringify(res)}`);
    const mineCouponsTemp = res.result.data.rows.map((row, index) => {
        return new TempCoupon({ coupon: coupons[index], receiveReq: _coupons[index], receiveInfo: row });
    });
    return mineCouponsTemp;
};

/** 临时券 */
function TempCoupon({ coupon, receiveReq, receiveInfo }) {
    /**  手机号 */
    this.mobile = LOGINDATA.mobile;
    /**  临时券编号 */
    this.mineCouponId = receiveInfo.mineCouponId;
    /**  卡券编号 */
    this.couponId = coupon.id;
    /**  卡券单元ID */
    this.couponUnitId = coupon.unitId;
    /**  领取渠道类型 */
    this.receiveChannelType = receiveReq.receiveChannelType;
    /**  领取来源 */
    this.receiveFrom = receiveReq.receiveFrom;
    /**  分享渠道 */
    this.shareChannelType = receiveReq.shareChannelType || '';
    /**  头像地址 */
    this.avatar = '';
    /**  昵称 */
    this.nick = '';
    /**  卡券状态 */
    this.flag = coupon.flag;
    /**  哈希 */
    this.hashKey = receiveReq.hashKey;
    /**  领取编码 */
    this.receiveNo = receiveInfo.receiveNo;
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  达成金额 */
    this.fulfilValue = coupon.fulfilValue;
    /**  优惠金额 */
    this.execNum = coupon.execNum;
};