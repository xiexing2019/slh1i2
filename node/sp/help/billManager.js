const common = require('../../lib/common');
const EventEmitter = require('events').EventEmitter;
const format = require('../../data/format');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const spConfb = require('../../reqHandler/sp/biz_server/spconfb');
const spMdm = require('../../reqHandler/sp/biz_server/spmdm');
const bizOrderDetail = require('../../reqHandler/sp/ecAccount/bizOrderDetail');
const mockJsonParam = require('../help/mockJsonParam');
// const storageAction = require('../../reqHandler/sp/wms/storageAction');

let billManager = module.exports = {};

billManager.setUpPurBill = function (params) {
    return new BillEditor();
};

/**
 * 单据
 */
class BillInfo {
    constructor(params = {}) {
        this.billNo = '';
        this.purBillId = '';
        this.salesBillId = '';
        this.coupons = [];
        this.coupBill = {};
        /**交易者信息 */
        this.trader = params.hasOwnProperty('sellerInfo') ? format.dataFormat(params.sellerInfo, 'clusterCode;tenantId;tenantName;unitId') : {};
        /**订单信息 */
        this.main = new SpPurBill();
        /**明细信息 */
        this.sku = new Map();
        /**合包单信息 */
        // this.combBill = combBill;
    }

    getPurBillDetail() {
        const detail = this;
        detail.bill.id = this.purBillId;
        return detail;
    }

    getSalesBillDetail() {
        const _bill = this.bill;
        _bill.id = this.salesBillId;
        return {
            bill: _bill,
            skus: [...this.skus.values()],
        }
    }
};

/**
 * 单据编辑
 */
class BillEditor {
    constructor() {
        this.orders = [];
        this.platCoupons = {};
        this.combOrders = [];

        /** 商品(单商品下单) */
        this.dres = '';
        /** 购物车 */
        this.cartList = new Map();
        /** 单据信息 */
        this.bills = {
            /** 订单号 */
            billNo: '',
            /** 采购单id */
            purBillId: '',
            /** 销售单id */
            salesBillId: '',
            /** 支付明显id */
            payDetailId: '',
        };
        /** 地址信息 */
        this.addressInfo = {};
    }

    /**
     * 初始化设值
     * @param {array} orders 
     */
    setUp(orders) {
        if (!orders || !orders.length) throw new Error(`单据信息有误,请检查。\norders:${JSON.stringify(orders)}`);

        orders.forEach((order, index) => {
            const bill = new BillInfo();
            bill.setByOrder(order);
            this.bills.set(order.billNo || index, bill);
        });
        return this;
    }

    /**
     * 添加商品
     */
    addDres(dres) {
        this.dres = dres;
        return this;
    }

    /**
     * 获取当前登录角色默认收货地址
     * @description 若无默认收货地址 则新增
     */
    async getRecAddressInfo() {
        const result = await spMdm.getUserDefaultRecInfo({ check: false }).then(res => res.result);
        if (result.code < 0) {
            const json = mockJsonParam.addAddrJson({
                provinceCode: 110000,
                cityCode: 110100,
                countyCode: 110101,
            });
            json.recInfo.isDefault = 1;
            LOGINDATA.defAddressId = await spMdm.saveUserRecInfo(json).then(res => res.result.data.val);
        } else {
            LOGINDATA.defAddressId = result.data.recInfo.id;
        }
        return this;
    }

    /**
     * 估算运费
     */
    async evalShipFee() {

    }

    /**
     * 生成采购单参数
     * @param {object} params 
     */
    mockPurParam() {
        if (this.dres) {
            const dres = this.dres;
            // console.log(dres);

            const main = {
                sellerId: dres.tenantId,
                money: 0,//成交金额。按买家适用价格计算得到的金额合计
                shopCoupsMoney: 0,//店家卡券抵扣金额
                shipFeeMoney: 0,//运费。若为到付，则为0
                originalMoney: 0,//订单原金额， 按pubPrice计算金额
                // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
                totalNum: 0,
                payKind: 2,//付款方式。1 预付，2 货到付款。
                couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
                buyerRem: `savePurBill${common.getRandomStr(5)}`,
                hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
                addressId: LOGINDATA.defAddressId,//收货地址ID 取当前用户默认地址 需要登录时获取 
            }, details = [];
            // 价格取spu的价格
            const detailSpu = {
                spuId: dres.id,
                spuTitle: dres.spu.title,
                spuCode: dres.spu.code,
                spuDocId: JSON.parse(dres.spu.docHeader)[0].docId,
                originalPrice: dres.spu.pubPrice,
                price: dres.spu.pubPrice,
            };
            common.randomSort([...dres.skus.values()]).slice(0, 2).forEach((sku) => {
                const detail = Object.assign({
                    skuId: sku.id,
                    spec1: sku.spec1,
                    spec2: sku.spec2,
                    spec3: sku.spec3,
                    num: common.getRandomNum(1, 10),
                }, detailSpu);
                detail.money = common.mul(detail.num, detail.price);
                main.originalMoney += detail.originalPrice;
                main.money += detail.money;
                main.totalNum += detail.num;
                details.push(detail);
            });
            main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;
            this.orders = [{ main, details }];
        } else {

        };
        // this.bills.values.map(bill => bill.getJsonParam());
        return this;
    }

    /**
     * 保存单据
     */
    async savePurBill() {
        const res = await spTrade.savePurBill({ orders: this.orders });
        const data = res.result.data;
        // console.log(`data=${JSON.stringify(data)}`);

        // 单门店下单
        this.bills.totalNum = this.orders[0].main.totalNum;
        this.bills.totalMoney = data.totalMoney;
        this.bills.billNo = data.rows[0].billNo;
        this.bills.purBillId = data.rows[0].billId;
        this.bills.salesBillId = data.rows[0].salesBillId;
        // return res;
    }

    /**
     * 支付
     */
    async createPay() {
        const payRes = await spTrade.createPay({ orderIds: [this.bills.purBillId], payMoney: this.bills.totalMoney });
        // console.log(`res=${JSON.stringify(payRes)}`);
        this.bills.payDetailId = payRes.result.data.payDetailId;
        const receiveRes = await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
        // console.log(`\nreceiveRes=${JSON.stringify(receiveRes)}`);
    }

    /**
     * 根据支付ID查入账记录
     */
    async getBizOrderDetailBySubBizOrderId() {
        // console.log(this.bills);

        const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: this.bills.billNo })
            .then(res => res.result.data);

        expect(data.rows).to.have.lengthOf.above(0);
        // console.log(`data=${JSON.stringify(data)}`);
        return data.rows;
    }

    /**
     * 校验入账记录
     * @param {object} params 
     * @param {array} params.bizOrderDetail 入账记录
     * @param {array} params.priceRate 加价比率 原价为1 加10% 则传1.1
     */
    checkBizOrderDetail({ bizOrderDetail, tenantId, priceRate }) {
        const originalPrice = common.div(this.dres.spu.pubPrice, priceRate);
        const money = common.mul(originalPrice, common.sub(priceRate, 1));
        const exp = {
            orderAmount: common.mul(this.bills.totalNum, money) * 100,//单位分
            payAmount: common.mul(this.bills.totalNum, money) * 100,//单位分
        };
        exp.feeAmount = common.mul(exp.payAmount, 0.006);//收取千分之6手续费
        expect(bizOrderDetail.find(detail => detail.tenantId == tenantId)).to.includes(exp);
    }

};

billManager.combBillManage = function () {
    return new CombBill();
};

class CombBill {
    constructor() {
        /** 单据 */
        this.bills = {};
        /** 购物车 */
        this.cartList = [];
        /** 地址 */
        this.addressInfo = {};
        /** 运费 */
        this.shipFee = {
            /** 能否合包 */
            isCombinative: false,
            /** 能否一件代发 */
            isSingleFog: false,
        };
        /** 平台券 */
        this.platCoupons = [];
    }
    /** 初始化购物车 */
    setUpByCart(cart) {
        this.cartList = cart.cartList;
        return this;
    }

    /**
     * 获取默认地址
     * @description 当前角色
     */
    async getRecAddressInfo() {
        const addressInfo = await spMdm.getUserDefaultRecInfo().then(res => res.result.data);
        console.log(addressInfo);

        this.addressInfo.addressId = addressInfo.recInfo.id;
        this.addressInfo.provCode = addressInfo.address.provinceCode;
        return this;
    }

    /**
     * 运费合包计算(根据买家默认运费地址)
     */
    async defevalShipFee() {
        const orders = [...this.cartList.values()].map(order => {
            const orderSpus = [...order.carts.values()].map((cart) => {
                return {
                    spuId: cart.spuId,
                    orderNum: cart.skuNum,
                    orderMoney: cart.price
                };
            });
            return {
                sellerId: order.trader.tenantId,
                orderSpus
            }
        });
        const res = await spConfb.defevalShipFee({ orders });
        return res;
    }

    /**
     * 估算运费
     */
    async evalShipFee() {
        if (!this.addressInfo.id) {
            await this.getRecAddressInfo();
        }

        const orders = [...this.cartList.values()].map(order => {
            const orderSpus = [...order.carts.values()].map((cart) => {
                return {
                    spuId: cart.spuId,
                    orderNum: cart.skuNum,
                    orderMoney: cart.price
                };
            });
            return {
                sellerId: order.trader.tenantId,
                orderSpus
            }
        });

        const res = await spConfb.evalShipFee({ provinceCode: this.addressInfo.provCode, addressId: this.addressInfo.addressId, orders });
        this.shipFee = res.result.data;
        return res;
    }

    /**
     * 拼接采购单参数
     */
    getPurJson() {
        const json = { orders: [], combOrders: [] };

        // 合包
        if (this.shipFee.isCombinative) {
            json.combOrders = this.shipFee.combinatives;
            json.combOrders.forEach(combOrder => {
                delete combOrder.combBillId;
                delete combOrder.combedBillIds;
                delete combOrder.combedBillNos;
            });
        }

        // 一件代发
        if (this.shipFee.isSingleFog) {
            json.combOrders = this.shipFee.singleFogs;
        }

        json.orders = [...this.cartList.values()].map((data, index) => {
            const main = {
                sellerId: data.trader.tenantId,
                money: 0,//成交金额。按买家适用价格计算得到的金额合计
                originalMoney: 0,//订单原金额， 按pubPrice计算金额
                totalNum: 0,//
                payKind: 1,//付款方式。1 预付，2 货到付款。
                shopCoupsMoney: 0,//店家卡券抵扣金额
                buyerRem: 'savePurBill' + common.getRandomStr(5),//买家备注
                couponsIds: '',//使用的券ids。该订单使用的优惠券编号列表，多个逗号分隔
                mineCouponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
                hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
                addressId: this.addressInfo.addressId,
                // wsAddressId: '',//仓库收货地址ID
                // srcType: 0,//app下单，1：开放平台，2: 买家分享，3：卖家分享
                // srcId: '',//订单来源的租户id：srcType=2/3(买家/卖家分享)时记录买家/卖家的租户id
            };
            const details = [...data.carts.values()].map(cart => {
                const detail = format.dataFormat(cart, 'cartId=id;spuId;skuId;spuTitle;spuCode;spec1;spec1Name;spec2;spec2Name;spec3;spec3Name;num=skuNum;originalPrice;price;tagId;tagKind');
                detail.spuDocId = cart.spuPic.split(',').shift();
                detail.money = common.mul(detail.num, detail.price);
                main.money += detail.money;
                main.totalNum += detail.num;
                if (!detail.originalPrice) detail.originalPrice = detail.price;
                main.originalMoney = detail.originalPrice;
                return detail;
            });

            // 运费 若为到付，则为0
            if (this.shipFee.isCombinative || this.shipFee.isSingleFog) {
                main.shipFeeMoney = json.combOrders.find(combOrder => combOrder.sellerIds.includes(main.sellerId)).fee;
            } else {
                main.shipFeeMoney = this.shipFee.fees.find(fee => fee.sellerId == main.sellerId).fee;
            }

            // 总应收金额
            main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;
            return { main, details };
        });

        return json;
    }

    /**
     * 创建采购订单
     */
    async savePurBill(params) {
        // 开单前 重新获取运费然后拼接参数 保证数据有效性
        await this.evalShipFee();
        const purJson = this.getPurJson();
        // console.log(`purJson=${JSON.stringify(purJson)}`);

        const res = await spTrade.savePurBill(purJson);
        this.bills = res.result.data;
        // console.log(this.bills);

        // 等待mq消费
        await common.delay(1000);
        return res;
    }

    /**
     * 支付
     */
    async createPay() {
        const payRes = await spTrade.createPay({ orderIds: this.bills.rows.map(bill => bill.billId), payMoney: this.bills.totalMoney });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
    }

    async wmsGetGoods() {
        // const res = await storageAction.getGoods({ id:})
    }

};


function billDirector(params) {
    const bills = {};
    const operations = {};

    const reciveMsg = function () {
        const msg = Array.prototype.shift.call(arguments);
        operations[msg].apply(this, arguments);
    };

    return {
        reciveMsg: reciveMsg
    }
};


class BillManager {
    constructor(params) {
        this.purBills = new Map();
        this.salesBills = new Map();
    }

    async savePurBill(params) {
        const res = await spTrade.savePurBill(params);
        const data = res.result.data;
        // console.log(`data=${JSON.stringify(data)}`);

        data.rows.forEach(val => {
            const purBill = new SpPurBill();
            // purBill.emit('');
        });
    }

};

/**
 * 采购订单
 */
function SpPurBill() {
    /**  订单ID */
    this.id = '';
    /**  单元id */
    this.unitId = '';
    /**  saasId */
    this.saasId = '';
    /**  订单号 */
    this.billNo = '';
    /**  销售单ID */
    this.salesBillId = '';
    /**  卖家id(卖家租户ID) */
    this.sellerId = '';
    /**  卖家名称 */
    this.sellerName = '';
    /**  卖家单元id */
    this.sellerUnitId = '';
    /**  下单时间 */
    this.proTime = '';
    /**  订单结束时间 */
    this.finishTime = '';
    /**  总数量 */
    this.totalNum = 0;
    /**  订单原金额(按pubPrice计算金额) */
    this.originalMoney = 0;
    /**  订单成交金额(按买家实际价格计算的金额) */
    this.money = 0;
    /**  店家卡券抵扣金额 */
    this.shopCoupsMoney = 0;
    /**  卖家手工调整给的优惠金额 */
    this.favorMoney = 0;
    /**  运费 */
    this.shipFeeMoney = 0;
    /**  实际总额(total_money = money - shop_coups_money - favor_money + ship_fee_money) */
    this.totalMoney = 0;
    /**  付款方式(1 预付，2 货到付款) */
    this.payKind = 0;
    /**  支付状态 */
    this.payFlag = 0;
    /**  产生积分 */
    this.score = 0;
    /**  使用的卡券ID列表(多个卡券以逗号分隔) */
    this.couponsIds = '';
    /**  运费支付方式(0-货到付款 1-预先支付) */
    this.shipPayKind = 0;
    /**  收货地址 */
    this.addressId = '';
    /**  退货标志(0 未退货，1 全部退货，2 部分退货) */
    this.backFlag = 0;
    /**  退货数量 */
    this.backNum = 0;
    /**  退货金额 */
    this.backMoney = 0;
    /**  买家备注 */
    this.buyerRem = '';
    /**  物流公司 */
    this.logisCompid = '';
    /**  运单号 */
    this.waybillNo = '';
    /**  状态(-1 已作废，0 已关闭，1 正常。添加状态：-2 彻底删除，2 处理中，3 订单已生成，4 开始配货，5 订单已配货，6 订单已发货，7 订单签收，8 订单已确认，9 买家已评价，10 订单已完成，99 下单失败) */
    this.flag = 0;
    /**  防重复提交 */
    this.hashKey = '';
    /**  操作版本号 */
    this.opVer = 0;
    /**  失败原因 */
    this.failedMsg = '';
    /**  创建人 */
    this.createdBy = '';
    /**  创建时间 */
    this.createdDate = '';
    /**  修改人 */
    this.updatedBy = '';
    /**  修改时间 */
    this.updatedDate = '';
    /**  付款时间 */
    this.payTime = '';
    /**  发货时间 */
    this.deliverTime = '';
    /**  发货数 */
    this.deliverNum = 0;
    /**  物流 */
    this.logisData = {};
    /**  签收时间 */
    this.signTime = '';
    /**  签收数 */
    this.signNum = 0;
    /**  成交时间 */
    this.confirmTime = '';
    /**  正在处理状态 */
    this.procFlag = 0;
    /**  SPU图片 */
    this.spuDocId = '';
    /**  支付账单id */
    this.payBillId = '';
    /**  物流公司名称 */
    this.logisCompName = '';
    /**  发货所用时间 */
    this.deliverElapsed = '';
    /**  买家取消订单原因：数据字典typeId=2008 */
    this.cancelKind = 0;
    /**  卖家取消订单原因：数据字典typeId=2009 */
    this.sellerCancelKind = 0;
    /**  采购订单延长收货天数 */
    this.extendedReturnDay = 0;
    /**  采购订单延长收货次数 */
    this.extendedReturnNum = 0;
    /**  平台卡券抵扣金额 */
    this.platCoupsMoney = 0;
    /**  订单来源 */
    this.srcType = 0;
    /**  外部订单id */
    this.outBillId = '';
    /**  扩展属性 */
    this.extProps = {};
    /**  外部订单id */
    this.outBuyerId = '';
    /**  平台制的卖家券抵扣金额 */
    this.platShopCoupsMoney = 0;
    /**  扩展优惠券属性 */
    this.extCoupsProps = {};
    /**  合包订单号(MQ创建，延时性) */
    this.combineNo = '';
    /**  合包运费 */
    this.combineFee = 0;
    /**  仓库信息{仓库id仓库name仓库unitId仓库tenantId原始运费} */
    this.warehouse = {};
    /**  仓库id(是否合单的标志) */
    this.warehouseId = '';
    /**  申请退款数量 */
    this.applyBackNum = 0;
    /**  申请退款金额 */
    this.applyBackMoney = 0;
};

/**
 * 采购订单明细
 */
function SpPurDetail() {
    /**  ID */
    this.id = '';
    /**  单元id */
    this.unitId = '';
    /**  saasId */
    this.saasId = '';
    /**  销售单明细ID */
    this.salesDetailId = '';
    /**  单据id */
    this.billId = '';
    /**  商品sku编号 */
    this.skuId = '';
    /**  商品spu编号 */
    this.spuId = '';
    /**  商品标题 */
    this.spuTitle = '';
    /**  规格1 */
    this.spec1 = 0;
    /**  规格1名称 */
    this.spec1Name = '';
    /**  规格2 */
    this.spec2 = 0;
    /**  规格2名称 */
    this.spec2Name = '';
    /**  规格3 */
    this.spec3 = 0;
    /**  规格3名称 */
    this.spec3Name = '';
    /**  数量 */
    this.num = 0;
    /**  原价(即商品的pub_price) */
    this.originalPrice = 0;
    /**  单价(实际成交价格) */
    this.price = 0;
    /**  金额(money = num * price) */
    this.money = 0;
    /**  优惠金额(总表中favor_money，分摊到每条明细的金额) */
    this.favorMoney = 0;
    /**  退货标志(0 未退货，1 全部退货，2 部分退货) */
    this.backFlag = 0;
    /**  退货数 */
    this.backNum = 0;
    /**  退货金额 */
    this.backMoney = 0;
    /**  创建人 */
    this.createdBy = '';
    /**  创建时间 */
    this.createdDate = '';
    /**  修改人 */
    this.updatedBy = '';
    /**  修改时间 */
    this.updatedDate = '';
    /**  SPU图片 */
    this.spuDocId = '';
    /**  发货数 */
    this.deliverNum = 0;
    /**  购物车id */
    this.cartId = '';
    /**  spu的编码 */
    this.spuCode = '';
    /**  备注 */
    this.rem = '';
    /**  活动id */
    this.actId = '';
    /**  明细的扩展信息 */
    this.extProps = {};
    /**  活动类型 */
    this.tagKind = 0;
    /**  申请退款数量 */
    this.applyBackNum = 0;
    /**  申请退款金额 */
    this.applyBackMoney = 0;
    /**  申请退款标记 */
    this.applyBackFlag = 0;
    /**  配货标记0：未配货，1：已配货 */
    this.pickFlag = 0;
    /**  已配货数量 */
    this.pickedNum = 0;
};

/**
 * 销售订单
 */
class SpSalesBill extends EventEmitter {
    constructor(params) {
        super();
        /**  订单ID */
        this.id = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';
        /**  订单号 */
        this.billNo = '';
        /**  采购单ID */
        this.purBillId = '';
        /**  买家id（卖家租户ID） */
        this.buyerId = '';
        /**  买家名称 */
        this.buyerName = '';
        /**  买家单元id */
        this.buyerUnitId = '';
        /**  订单生成时间 */
        this.proTime = '';
        /**  订单结束时间 */
        this.finishTime = '';
        /**  总数量 */
        this.totalNum = 0;
        /**  订单原金额(按pubPrice计算金额) */
        this.originalMoney = 0;
        /**  订单成交金额(按买家实际价格计算的金额) */
        this.money = 0;
        /**  店家卡券优惠金额 */
        this.shopCoupsMoney = 0;
        /**  卖家手工调整给的优惠金额 */
        this.favorMoney = 0;
        /**  运费 */
        this.shipFeeMoney = 0;
        /**  实际总额(total_money = money - shop_coups_money - plat_coups_money - favor_money + ship_fee_money) */
        this.totalMoney = 0;
        /**  付款方式(1 预付，2 货到付款) */
        this.payKind = 0;
        /**  支付状态 */
        this.payFlag = 0;
        /**  产生积分 */
        this.score = 0;
        /**  使用的卡券ID列表(多个卡券以逗号分隔) */
        this.couponsIds = '';
        /**  运费支付方式(0-货到付款 1-预先支付) */
        this.shipPayKind = 0;
        /**  收货地址 */
        this.addressId = '';
        /**  退货标志(0 未退货，1 全部退货，2 部分退货) */
        this.backFlag = 0;
        /**  退货数量 */
        this.backNum = 0;
        /**  退货金额 */
        this.backMoney = 0;
        /**  买家备注 */
        this.buyerRem = '';
        /**  卖家备注 */
        this.sellerRem = '';
        /**  物流公司 */
        this.logisCompid = '';
        /**  运单号 */
        this.waybillNo = '';
        /**  状态(同sp_pur_bill.flag) */
        this.flag = 0;
        /**  失败原因 */
        this.failedMsg = '';
        /**  乐观锁 */
        this.ver = 0;
        /**  操作版本号 */
        this.opVer = 0;
        /**  国家ID(0表示不指定国家，默认为中国) */
        this.countryId = 0;
        /**  省ID(系统字典中地区类型（850）的code_value) */
        this.provId = 0;
        /**  市ID(系统字典中地区类型（850）的code_value) */
        this.cityId = 0;
        /**  区县ID(系统字典中地区类型（850）的code_value) */
        this.countyId = 0;
        /**  乡镇街道ID(系统字典中地区类型（850）的code_value) */
        this.townId = 0;
        /**  详细地址(除省市区外的地址，包括门牌号等。设置了乡镇街道ID时，详细地址中也不需要乡镇街道) */
        this.addrDetail = '';
        /**  收件人姓名 */
        this.receiveName = '';
        /**  收件人电话 */
        this.receivePhone = '';
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  付款时间 */
        this.payTime = '';
        /**  发货时间 */
        this.deliverTime = '';
        /**  发货数 */
        this.deliverNum = 0;
        /**  物流 */
        this.logisData = {};
        /**  签收时间 */
        this.signTime = '';
        /**  签收数 */
        this.signNum = 0;
        /**  成交时间 */
        this.confirmTime = '';
        /**  正在处理状态 */
        this.procFlag = 0;
        /**  物流公司名称 */
        this.logisCompName = '';
        /**  卖家取消订单原因：数据字典typeId=2009 */
        this.cancelKind = 0;
        /**  买家取消订单原因：数据字典typeId=2008 */
        this.buyerCancelKind = 0;
        /**  清分提现批次号 */
        this.transCode = '';
        /**  平台卡券抵扣金额 */
        this.platCoupsMoney = 0;
        /**  平台卡劵退款金额 */
        this.backCoupMoney = 0;
        /**  平台制的卖家券抵扣金额 */
        this.platShopCoupsMoney = 0;
        /**  订单来源 */
        this.srcType = 0;
        /**  扩展属性 */
        this.extProps = {};
        /**  扩展优惠券属性 */
        this.extCoupsProps = {};
        /**  合包订单号(MQ创建，延时性) */
        this.combineNo = '';
        /**  合包运费 */
        this.combineFee = 0;
        /**  仓库信息{仓库id仓库name仓库unitId仓库tenantId原始运费} */
        this.warehouse = {};
        /**  仓库id(是否合单的标志) */
        this.warehouseId = '';
        /**  同步订单id(外部订单id) */
        this.outBillId = '';
        /**  申请退款数量 */
        this.applyBackNum = 0;
        /**  申请退款金额 */
        this.applyBackMoney = 0;
    }
};

/**
 * 销售订单明细
 */
class SpSalesDetail extends EventEmitter {
    constructor() {
        super();

        /**  ID */
        this.id = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';
        /**  采购单明细ID */
        this.purDetailId = '';
        /**  单据id */
        this.billId = '';
        /**  商品sku编号 */
        this.skuId = '';
        /**  商品spu编号 */
        this.spuId = '';
        /**  商品标题 */
        this.spuTitle = '';
        /**  规格1 */
        this.spec1 = 0;
        /**  规格1名称 */
        this.spec1Name = '';
        /**  规格2 */
        this.spec2 = 0;
        /**  规格2名称 */
        this.spec2Name = '';
        /**  规格3 */
        this.spec3 = 0;
        /**  规格3名称 */
        this.spec3Name = '';
        /**  数量 */
        this.num = 0;
        /**  原价(即商品的pub_price) */
        this.originalPrice = 0;
        /**  单价(实际成交价格) */
        this.price = 0;
        /**  金额(money = num * price) */
        this.money = 0;
        /**  优惠金额(总表中favor_money，分摊到每条明细的金额) */
        this.favorMoney = 0;
        /**  退货标志(0 未退货，1 全部退货，2 部分退货) */
        this.backFlag = 0;
        /**  退货数 */
        this.backNum = 0;
        /**  退货金额 */
        this.backMoney = 0;
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  SPU图片 */
        this.spuDocId = '';
        /**  发货数量 */
        this.deliverNum = 0;
        /**  spu的编码 */
        this.spuCode = '';
        /**  备注 */
        this.rem = '';
        /**  活动id */
        this.actId = '';
        /**  明细的扩展信息 */
        this.extProps = {};
        /**  商品分润金额 */
        this.shareMoney = 0;
        /**  活动类型 */
        this.tagKind = 0;
        /**  申请退款数量 */
        this.applyBackNum = 0;
        /**  申请退款金额 */
        this.applyBackMoney = 0;
        /**  配货标记0：未配货，1：已配货 */
        this.pickFlag = 0;
        /**  已配货数量 */
        this.pickedNum = 0;
        /**  店铺卡券均摊金额 */
        this.shopCoupMoney = 0;
        /**  平台卡券均摊金额 */
        this.platCoupMoney = 0;
        /**  卡劵退款金额（平台卡劵） */
        this.backCoupMoney = 0;
        /**  申请的平台卡劵退款金额 */
        this.applyBackCoupMoney = 0;
    }
};