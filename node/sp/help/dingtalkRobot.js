let common = require('../../lib/common');

let dingtalkRobot = module.exports = {};

// 自动化群提醒
const autoLogToken = '9c55f9ae730c6e4af95455122b938e07e98328a82e946ca94813025d6da88e24';
// 线上自动化执行日志
const spOnlineLogToken = '452dab154e6b8fd7b161692496c0c421ff02b3ed2bfa2b454c0b9593bca240ad';

const esSearchLogToken = 'c55e1be3d0df8ddd96cabba59288ae93ad786912288c3a9e78c942466718acff';

dingtalkRobot.appendLogMsg = async function (params = {}) {
    return common.superagent.post(`https://oapi.dingtalk.com/robot/send?access_token=${autoLogToken}`)
        .send(params)
        .then((response) => {
            // console.log(`response=${JSON.stringify(response)}`);
        }).catch((err) => {
            console.warn(`请求失败:\n${JSON.stringify(err)}`);
        });
};

dingtalkRobot.appendSpOnlineMsg = async function (params) {
    await common.superagent.post(`https://oapi.dingtalk.com/robot/send?access_token=${spOnlineLogToken}`)
        .send(params)
        .then((response) => {
            // console.log(`response=${JSON.stringify(response)}`);
        }).catch((err) => {
            console.warn(`请求失败:\n${JSON.stringify(err)}`);
        });
};

dingtalkRobot.appendEsSearchMsg = async function (params) {
    await common.superagent.post(`https://oapi.dingtalk.com/robot/send?access_token=${esSearchLogToken}`)
        .send(params)
        .then((response) => {
            // console.log(`response=${JSON.stringify(response)}`);
        }).catch((err) => {
            console.warn(`请求失败:\n${JSON.stringify(err)}`);
        });
};