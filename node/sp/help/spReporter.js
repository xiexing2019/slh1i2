const mocha = require('mocha');
const supportsColor = process.browser ? null : require('supports-color');
const diff = require('diff');
const moment = require('moment');
const caps = require('../../data/caps');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spugr = require('../../reqHandler/sp/global/spugr');
const spReq = require('../help/spReq');
const spAccount = require('../data/spAccount');
const dingtalkRobot = require('./dingtalkRobot');
const common = require('../../lib/common');

const utils = mocha.utils;

/**
 * sp平台日志模板
 */
exports = module.exports = spReporter;

/**
 * 默认使用颜色 除了浏览器~
 */
const useColors = !process.browser && (supportsColor.stdout || process.env.MOCHA_COLORS !== undefined);

/**
 * 默认颜色
 */
exports.colors = {
	pass: 90,
	fail: 31,
	'bright pass': 92,
	'bright fail': 91,
	'bright yellow': 93,
	pending: 36,
	suite: 0,
	'error title': 0,
	'error message': 31,
	'error stack': 90,
	checkmark: 32,
	fast: 90,
	medium: 33,
	slow: 31,
	green: 32,
	light: 90,
	'diff gutter': 90,
	'diff added': 32,
	'diff removed': 31
};

const color = (exports.color = function (type, str) {
	return (!useColors ? String(str) : `\u001b[${exports.colors[type]}m${str}\u001b[0m`);
});

/**
 * 标签符号
 */
exports.symbols = {
	ok: '✓',
	err: '✖',
	dot: '․',
	comma: ',',
	bang: '!'
};
// 若运行平台为Windows 修改标签
if (process.platform === 'win32') {
	exports.symbols.ok = '\u221A';
	exports.symbols.err = '\u00D7';
	exports.symbols.dot = '.';
}

// exports.window = {
// 	width: 75
// };

function spReporter(runner) {
	mocha.reporters.Base.call(this, runner);
	// console.log(runner);

	const envName = caps.name;
	let passCaseNum = 0, //用例通过数
		failCaseNum = 0,//用例失败数
		warnCaseNum = 0,//警告数量
		sellerTenantId,//卖家tenantId 认证店铺 上下线使用
		sellerTenantId2;//卖家tenantId 免租店铺 上下线使用

	runner.on('start', async function () {
		console.log(`测试开始 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')}, url=${caps.url}, name=${envName}\n`);

		// await dingtalkRobot.appendLogMsg({
		// 	msgtype: 'text',
		// 	text: { content: `${envName} 脚本开始执行` },
		// 	// at: { isAtAll: true }
		// });

		if (envName != 'sp_online') return;

		//上线卖家店铺
		try {
			// 认证店铺
			// await spReq.spSellerLogin();
			// sellerTenantId = LOGINDATA.tenantId;
			// console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
			// console.log(`sellerTenantId=${sellerTenantId}`);


			// await common.delay(1000);
			// // 免租店铺
			// await spReq.spSellerLogin({ shopName: spAccount.seller2.shopName });
			// sellerTenantId2 = LOGINDATA.tenantId;

			// await spAuth.staffLogin();
			// 强制上线
			// await spugr.onlineShopById({ id: sellerTenantId, forceFlag: true, autoTestToken: 'autotest' });
			// await spugr.inUseNewShopById({ id: sellerTenantId, check: false });    //尝试启用上新能力

			// // 强制上线为免租店铺 
			// await spugr.onlineShopById({ id: sellerTenantId2, forceFlag: true, autoTestToken: 'autotest', shopTypeId: 1, shopPlatRatio: 10 });
			// await spugr.inUseNewShopById({ id: sellerTenantId2, check: false });    //尝试启用上新能力

			// await spReq.spSellerLogin();
		} catch (error) {
			console.log(` ${error}`);
			process.exit(0);
		};

	});

	runner.on('suite', function () {
		// console.log(this);
	});

	//测试开始
	// runner.on('test', function () {
	// 	TESTCASE = {};
	// });

	// runner.on('test end', function () {
	//
	// });

	runner.on('pass', function (test) {
		passCaseNum++;
		// console.log(color('green', `${passCaseNum}.%s`), test.titlePath().join('-'));
	});

	runner.on('fail', async function (test, err) {
		test.err = err;
		const errorMsg = `${test.titlePath().join('-').replace(/-onlineMain/g, '')}\nError: ${err.message}`;
		// console.log(errorMsg);

		if (showDiff(err)) {
			stringifyDiffObjs(err);
		}

		// 警告
		if (test.isWarn && !err.message.includes('服务端错误')) {
			warnCaseNum++;
		} else {
			failCaseNum++;
			if (envName == 'sp_online') {
				// console.log(err);

				await dingtalkRobot.appendSpOnlineMsg({
					msgtype: 'text',
					text: { content: errorMsg },
					// at: {
					// isAtAll: true
					// }
				});
			}
		}
		list({ failCaseNum, warnCaseNum, test });
	});

	// runner.on('pending', function (test) {
	//
	// });

	runner.on('end', async function () {
		console.log(`\n测试结束 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')} \n   total=${passCaseNum + failCaseNum + warnCaseNum}, passes=${passCaseNum}, failures=${failCaseNum}, warns=${warnCaseNum}`);

		// if (envName == 'sp_online') {
		// 	try {
		// 		await spAuth.staffLogin();
		// 		await spugr.offlineShopById({ id: sellerTenantId });
		// 		// await spugr.offlineShopById({ id: sellerTenantId2 });
		// 		await spAuth.staffLogout();
		// 	} catch (error) {
		// 		console.log(`下线卖家店铺失败,为避免影响实际客户,请下线测试店铺:\n   ${error}`);
		// 	};
		// };

		if (failCaseNum > 0 && (passCaseNum + failCaseNum + warnCaseNum) > 300) {
			await dingtalkRobot.appendLogMsg({
				msgtype: 'text',
				text: { content: `${envName} 脚本已执行完毕` },//,failCaseNum=${failCaseNum}
				// at: {
				// isAtAll: true
				// }
			});
		}
		process.exit(0);
	});

};


function showDiff(err) {
	return (
		err &&
		err.showDiff !== false &&
		sameType(err.actual, err.expected) &&
		err.expected !== undefined
	);
}

const objToString = Object.prototype.toString;

function sameType(a, b) {
	return objToString.call(a) === objToString.call(b);
}

function stringifyDiffObjs(err) {
	if (!utils.isString(err.actual) || !utils.isString(err.expected)) {
		err.actual = utils.stringify(err.actual);
		err.expected = utils.stringify(err.expected);
	}
}

const generateDiff = (exports.generateDiff = function (actual, expected) {
	return exports.inlineDiffs
		? inlineDiff(actual, expected)
		: unifiedDiff(actual, expected);
});

function unifiedDiff(actual, expected) {
	let indent = '      ';
	function cleanUp(line) {
		if (line[0] === '+') {
			return indent + colorLines('diff added', line);
		}
		if (line[0] === '-') {
			return indent + colorLines('diff removed', line);
		}
		if (line.match(/@@/)) {
			return '--';
		}
		if (line.match(/\\ No newline/)) {
			return null;
		}
		return indent + line;
	}
	function notBlank(line) {
		return typeof line !== 'undefined' && line !== null;
	}
	let msg = diff.createPatch('string', actual, expected);
	let lines = msg.split('\n').splice(5);
	return (
		'\n      ' +
		colorLines('diff added', '+ expected') +
		' ' +
		colorLines('diff removed', '- actual') +
		'\n\n' +
		lines.map(cleanUp)
			.filter(notBlank)
			.join('\n')
	);
}

function colorLines(name, str) {
	return str
		.split('\n')
		.map(function (str) {
			return color(name, str);
		})
		.join('\n');
}

/**
 * 输出错误日志
 */
function list({ test, failCaseNum, warnCaseNum }) {
	console.log();

	let fmt =
		color('error title', '  %s) %s:\n') +
		color('error message', '     %s') +
		color('error stack', '\n%s\n');

	// 
	let msg,
		err = test.err,
		message;

	if (err.message && typeof err.message.toString === 'function') {
		message = err.message + '';
	} else if (typeof err.inspect === 'function') {
		message = err.inspect() + '';
	} else {
		message = '';
	}
	let stack = err.stack || message;
	let index = message ? stack.indexOf(message) : -1;

	if (index === -1) {
		msg = message;
	} else {
		index += message.length;
		msg = stack.slice(0, index);
		// remove msg from stack
		stack = stack.slice(index + 1);
	}

	// uncaught
	if (err.uncaught) {
		msg = 'Uncaught ' + msg;
	}
	// 显示不同
	if (!exports.hideDiff && showDiff(err)) {
		stringifyDiffObjs(err);
		fmt = color('error title', '  %s) %s:\n%s') + color('error stack', '\n%s\n');
		let match = message.match(/^([^:]+): expected/);
		msg = '\n      ' + color('error message', match ? match[1] : msg);
		msg += generateDiff(err.actual, err.expected);
	}

	// 缩进
	stack = stack.replace(/^/gm, '  ');

	// 生成标题
	let testTitle = '';
	test.titlePath().forEach(function (str, index) {
		if (index !== 0) {
			testTitle += '\n     ';
		}
		for (let i = 0; i < index; i++) {
			testTitle += '  ';
		}
		testTitle += str;
	});
	testTitle += `\n  当前角色信息: ${JSON.stringify(LOGINDATA)}`;
	const caseNum = test.isWarn ? `warn:${warnCaseNum}` : `fail:${failCaseNum}`;
	console.log(fmt, caseNum, testTitle, msg, stack);
};

