const common = require('../../lib/common');
const format = require('../../data/format');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const basicJson = require('../help/basicJson');

class SpAudit {
    constructor(clientInfo = LOGINDATA) {
        /**  买家手机号 */
        this.mobile = clientInfo.mobile || '';
        /**  买家tenantId */
        this.tenantId = clientInfo.tenantId || '';

        // /**  编号 */
        // this.id = '';
        /**  审核业务类型 1-买家店铺审核 */
        this.bizType = 0;
        /**  审核对象(如买家店铺id) */
        this.auditObjId = '';
        /**  审核事项名称 */
        this.auditName = clientInfo.shopName || '';
        /**  通过状态(0-已提交 1-已通过 9-驳回 3-待提交) */
        this.flag = 0;
        // /**  结束时间 */
        // this.finishDate = '';
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  认证来源 1平台认证(已去除)，2bd认证街边码，3bd认证市场码，4卖家推荐，0认证来源未知（暂定自主认证),5买家推荐*/
        this.auditSrc = 0;
        /**  平台是否确认0平台未确认1平台已确认 */
        this.platConfirm = 0;
        // /**  第一次认证成功认证人id */
        // this.auditSrcId = 0;

        // /** 提交次数 */
        // this.auditCount = 0;

        /**  审核意见 */
        this.auditRemark = '';
        /**  主营类目id */
        this.masterClassId = '';
        /**  店铺详细地址 */
        this.shopAddr = '';
        /**  省编码 */
        this.provCode = '';
        /**  市编码 */
        this.cityCode = '';
        /**  区编码 */
        this.areaCode = '';
        /**  门头照片 */
        this.headPic = '';
        /**  店内照片 */
        this.contentPic = '';
        /**  营业执照和身份证照 */
        this.certPic = '';
        /**  备注 */
        this.remark = '';
        /**  邀请码，若有邀请码，直接审核通过 */
        this.inviteCode = '';
        /**  价格区间 [{price1,price2}] */
        this.priceRange = '';
    }

    get shopName() {
        return this.auditName;
    }
    set shopName(name) {
        this.auditName = name;
    }
    get auditFlag() {
        return this.flag;
    }

    /**
     * 更新
     * @param {*} params 
     */
    update(params) {
        common.update(this, params);
        return this;
    }

    /**
     * 生成随机审核信息
     */
    async createRandomInfo() {
        const areaJson = await basicJson.getAreaJson();
        return basicJson.submitAuditJson({ areaJson });
    }

    /**
     * 买家提交店铺审核
     * @param {*} params 
     */
    async submitAuditRecord(params) {
        const res = await spCommon.submitAuditRecord(params);

        this.update(params);

        // 提交返回的id为审核对象id
        this.auditObjId = res.result.data.id;
        if (params.shopName) this.auditName = params.shopName;
        // 时间直接取结果的
        if (res.result.data.hasOwnProperty('createdBy')) {
            this.createdBy = res.result.data.createdBy;
            this.createdDate = res.result.data.createdDate;
        }
        this.updatedBy = res.result.data.updatedBy;
        this.updatedDate = res.result.data.updatedDate;

        // 提交次数+1
        this.auditCount++;

        // 邀请码直接审核通过 类型为地推
        if (this.inviteCode) {
            this.flag = 1;
            this.auditSrc = 2;
        }
        return res;
    }

    /**
     * 审批审核记录(管理员)
     * @param {object} params 
     * @param {number} params.flag 0-已提交 1-已通过 9-驳回
     * @param {string} params.auditRemark 审核意见
     */
    async changeAuditRecord(params) {
        params.shopId = this.auditObjId;
        const res = await spCommon.changeAuditRecord(params);

        if (res.result.data.flag != 1) {
            return res;
        }

        this.update(params);
        this.updatedBy = LOGINDATA.userId;
        this.updatedDate = common.getCurrentTime();
        this.auditSrc = 0;
        this.platConfirm = 1;
        return res;
    }

    /**
     * 期望值 搜索查看买家店铺审核记录列表(管理员)
     * @description ec-spadmin-spBuyerShop-findBuyerShopAudits
     */
    getBuyerShopAuditsExp() {
        // auditCount;
        const exp = format.dataFormat(this, 'flag;masterClassId;provCode;cityCode;areaCode;inviteCode;platConfirm;headPic;contentPic;certPic;auditObjId;auditSrc;shopAddr;mobile;buyerTenantId=tenantId');
        exp.shopName = this.auditName;
        return exp;
    }

};

module.exports = SpAudit;

