const common = require('../../lib/common');
const spConfig = require('../../reqHandler/sp/global/spConfig');

class SpConfigParam {
    constructor(paramInfo = {}) {
        /** id */
        this.id = '';
        /** 参数代码 */
        this.code = '';
        /** 参数名称 */
        this.name = '';
        /** 参数值 */
        this.val = '';
        /** 备注 */
        this.remark = '';
        /** 配置域。system、business、role等值 */
        this.domainKind = '';
        /** 所属者类型。1 全局, 5 集群, 6 租户, 7 单元等 */
        this.ownerKind = '';
        /** 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID */
        this.ownerId = 0;

        /** 原始值 方便还原 */
        this.originVal = '';

        common.update(this, paramInfo);
    }

    /**
     * 修改参数基本信息
     * @param {object} params 
     * @param {string} params.val 
     */
    async updateParam(params) {
        if (this.val == params.val) return;

        await spConfig.saveOwnerVal({ ownerKind: this.ownerKind, data: [{ id: this.id, code: this.code, val: params.val, domainKind: this.domainKind, ownerId: this.ownerId }] });
        this.val = params.val;
    }
    // {"ownerKind":5,"data":[{"code":"sp_remind_deliver_first","remark":"单位小时，如24表示买家下单后24如果卖家未发货将提醒","updatedDate":"2019-02-12 11:25:57","needAuth":0,"inputType":1,"id":873,"val":"24","warnningTxt":"","optionHaspic":0,"codeVersion":0,"updatedBy":-1,"productIncExc":0,"productTypes":0,"readOnly":0,"domainKind":"system","catId":10,"bdomains":2,"ownerLevel":1,"isComp":0,"name":"卖家发货提醒第一次","showOrder":1,"splitRadio":"0","ownerId":0}]}

    /**
     * 还原参数
     */
    async returnOriginalParam() {
        if (this.originVal || this.val == this.originVal) return;

        await spConfig.saveOwnerVal({ ownerKind: 5, data: [{ id: this.id, code: this.code, val: params.val, domainKind: this.domainKind, ownerId: this.ownerId }] });
    }
}

/**
 * 获取全局区参数详细信息
 * @description 全局区使用时 ownerKind1,3,5没有区别 可以写死
 * @param {object} params
 * @param {string} params.domainKind 配置域* business 业务参数 system 系统参数
 * @param {string} params.code 编码
 * @param {string} [params.ownerId] 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID
 */
const getParamInfo = async function (params) {
    // 获取不到会抛异常 这里不再多进行校验
    const res = await spConfig.getParamInfo({ ownerKind: 1, ...params });

    const configParam = new SpConfigParam(res.result.data);
    // 保存原始值 用例完成后 看情况还原
    configParam.originVal = configParam.val;
    configParam.ownerKind = res.params.jsonParam.ownerKind;
    configParam.ownerId = [1, 5].includes(configParam.ownerKind) ? 0 : params.ownerId;
    return configParam;
};

module.exports = {
    getParamInfo
};