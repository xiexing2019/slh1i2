const common = require('../../lib/common');
const format = require('../../data/format');
const spDresb = require('../../reqHandler/sp/biz_server/spdresb');
// const spReq = require('./spReq');
// const basicJson = require('./basicJson');

class Dres {
    constructor() {
        /** 商品spuId */
        this.id = '';
        this.spu = new DresSpu();
        this.skus = new Map();
        this.detailUrl = '';
    }

    /**
     * 根据es搜索结果设值
     * @description 只有spu相关信息
     * @param {object} dresInfo 
     */
    setByEs(dresInfo) {
        expect(dresInfo, `es未查找到商品`).not.to.be.undefined;
        this.id = dresInfo.id;
        this.detailUrl = dresInfo.detailUrl || '';
        common.update(this.spu, dresInfo);
        // dresInfo.forEach()
        return this;
    }

    /**
     * 根据卖家搜索设置
     * @param {object} dresInfo 
     */
    setBySeller(dresInfo) {
        this.setByDetailUrl(dresInfo);
        this.spu.clusterCode = LOGINDATA.clusterCode;
        this.spu.tenantId = LOGINDATA.tenantId;
        return this;
    }

    setByDetailUrl(dresInfo) {
        this.id = dresInfo.id;
        this.updateSpu(dresInfo.spu);
        dresInfo.skus.forEach(sku => this.updateSku(sku));
        return this;
    }

    updateSpu(spuInfo) {
        common.update(this.spu, spuInfo);
        return this;
    }

    updateSku(skuInfo) {
        const sku = new DresSku();
        common.update(sku, skuInfo);
        sku.spuId = this.spu.id;
        sku.unitId = this.spu.unitId;
        this.skus.set(sku.id, sku);
        return this;
    }

    /**
     * 管理员修改商品类别
     * @param {object} params 
     */
    async updateSpuClassByAdmin(params) {
        Object.assign(params, { _cid: this.spu.clusterCode, unitId: this.spu.unitId, tenantId: this.spu.tenantId, id: this.spu.id });
        await spDresb.updateSpuClassByAdmin(params);
        this.updateSpu(params);

        // 平台改价 resetPriceFlag 1:重置改价为原价 0:不重置(默认)
        if (params.resetPriceFlag) {
            this.spu.pubPrice = this.spu.esProps.originalPrice || this.spu.pubPrice;
        } else {
            if (params.hasOwnProperty('changeMoney')) {
                if (!this.spu.esProps) this.spu.esProps = {};
                this.spu.esProps.originalPrice = this.spu.pubPrice;
                this.spu.pubPrice = params.changeMoney;
            }
        }

        // 标签  web端给商品打标签,拱es筛选过滤用
        ['seasonLabel', 'mainKindLabel', 'colorLabel', 'versionLabel', 'themeLabel', 'fabric'].forEach(key => {
            if (params.hasOwnProperty(key)) {
                this.spu[key] = params[key];
            }
        });
    }

    /**
     * 卖家更变价格类型到发布价
     * @param {object} params jsonParam
     * @param {number} params.priceType 价格类型(取值1~5，分别对应价格1到价格5)
     * @param {string} [params.discount=1] 折扣 默认1
     * @param {string} [params.discountType=1] 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认 1
     */
    async changePubPriceByPriceType(params) {
        const res = await spDresb.changePubPriceByPriceType(params);

        // 更新折扣
        // discountType 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认1
        const discountType = params.hasOwnProperty('discountType') ? params.discountType : 1;
        const discount = params.hasOwnProperty('discount') ? params.discount : 1;
        this.spu.discount = [1, discount, this.spu.discount][discountType];

        // 更新价格 
        // slhPrice为slh推送过来的价格  constructPrice为推送过来的最高价
        this.spu.pubPrice = Math.round(common.mul(this.spu[`price${params.priceType}`], this.spu.discount));

        return res;
    }

};

module.exports = Dres;

function DresSpu() {
    /**  主键id */
    this.id = '';
    /**  单元id */
    this.unitId = '';
    /**  集群id _cid */
    this.clusterCode = '';
    /**  租户id _tid */
    this.tenantId = '';
    /**  款号 */
    this.code = '';
    /**  名称 */
    this.name = '';
    /**  标题 */
    this.title = '';
    /**  名称拼音 */
    this.namePy = '';
    /**  类别 */
    this.classId = '';
    /**  规格1范围 */
    this.spec1Ids = {};
    /**  规格2范围 */
    this.spec2Ids = {};
    /**  规格3范围 */
    this.spec3Ids = {};
    /**  品牌 */
    this.brandId = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  计量单位 */
    this.unit = '';
    /**  上架日期 */
    this.marketDate = '';
    /**  季节：1-6 春夏 秋 冬 春 夏 秋冬 。 对应字典2034 */
    this.platSeason = '';
    /**  产地 */
    this.origin = '';
    /**  发货地 */
    this.shipOriginId = 0;
    /**  运费方式 */
    this.shipFeeWayId = 0;
    /**  商品标题区图片 */
    this.docHeader = {};
    /**  商品内容区图片 */
    this.docContent = {};
    /**  是否特价商品 */
    this.special = 0;
    /**  是否允许退货 */
    this.isAllowReturn = 0;
    /**  发布范围 */
    this.pubScope = 0;
    /**  标签 */
    this.labels = {};
    /**  详情页面URL */
    this.url = '';
    /**  状态  -2 平台下架  -4 卖家下架 -3 商品申请上架  -1 商品删除 0 商品下架(对应slh停用) 1商品上架(校验通过) 2商品待上架(校验不通过) */
    this.flag = 0;
    /**  备注 */
    this.rem = '';
    // /**  商品属性 */
    // this.props = {};
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  操作版本号 */
    this.opVer = 0;
    // /**  乐观锁 */
    // this.ver = 0;
    /**  商陆花商品ID */
    this.slhId = '';
    /**  数据Hash值 */
    this.hashKey = '';
    /**  被平台下架原因 */
    this.offmarketReason = '';
    /**  平台下架时间 */
    this.offmarketTime = '';
    /**  上架失败原因 */
    this.marketFailure = '';
    /**  折扣 */
    this.discount = 0;
    /**  商品排序 */
    this.showOrder = 0;
    /**  第三方同步时间 */
    this.slhDate = '';
    /**  商品上新能力位 0 无能力 。1 上新推荐能力 */
    this.specialFlag = 0;
    /**  上架商品不上新原因 */
    this.unRemmondReason = '';
    /**  商陆花停用标志，0 商陆花未停用  1 商陆花已经停用 */
    this.slhUsingFlag = 0;
    /**  平台停用标志，0 平台未停用 1 平台已经停用 */
    this.spPlatUsingFlag = 0;
};

function DresSku() {
    /**  ID */
    this.id = '';
    /**  单元ID */
    this.unitId = '';
    /**  款号 */
    this.spuId = '';
    /**  规格1 */
    this.spec1 = 0;
    /**  规格2 */
    this.spec2 = 0;
    /**  规格3 */
    this.spec3 = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  数量 */
    this.num = 0;
    /**  状态 */
    this.flag = 0;
    /**  备注 */
    this.rem = '';
    /**  创建人 */
    this.createdBy = '';
    /**  创建时间 */
    this.createdDate = '';
    /**  修改人 */
    this.updatedBy = '';
    /**  修改时间 */
    this.updatedDate = '';
    /**  商陆花SKU ID */
    this.slhId = '';
    /**  sku的编码 */
    this.code = '';
};

