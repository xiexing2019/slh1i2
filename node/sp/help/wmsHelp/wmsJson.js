'use strict'
const common = require('../../../lib/common');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');



class UserJson {

    constructor() {
        this.userJson = {};
    };

    /**
     * 保存用户
     * @param {Object} param 
    */
    async saveUser(params) {
        let saveRes = await wmsWarehouse.saveUser(Object.assign({}, this.addJson(), params));
        this.userJson = saveRes.params.jsonParam;
        this.userJson.id = saveRes.result.data.val;
        return saveRes;
    };

    addJson() {
        return {
            mobile: `12${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`,
            idCardNo: `330327199${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`,
            name: `拿货员${common.getRandomStr(4)}`,
            warehouseId: LOGINDATA.tenantId
        };
    };

    getUser() {
        return this.userJson;
    };

    /**
     * @description 修改用户
     * @param {Object} params 需要修改的字段的json 例如修改name，只需要传 {name:''} 即可
    */
    async updateUser(params) {
        let updateRes = await wmsWarehouse.saveUser(Object.assign({}, this.userJson, params));
        Object.assign(this.userJson, updateRes.params.jsonParam);
        return updateRes;
    };
};


class Shelf {
    constructor() {
        this.shelfJson = {
            shelfName: `auto货架${common.getRandomStr(4)}`,
            horizontal: common.getRandomNum(1, 3),
            vertical: common.getRandomNum(1, 3)
        }
    };

    async saveShelf(params) {
        let saveRes = await wmsWarehouse.saveShelves(Object.assign({}, this.shelfJson, params));
        this.shelfJson = Object.assign(this.shelfJson, params);
        this.setShelfList(saveRes);
        return saveRes;
    }

    //这里现在是从00开始的，等开发修复了改
    setShelfList(saveParam) {
        let shelfList = [];
        let horizontal = saveParam.params.horizontal;
        let vertical = saveParam.params.vertical;
        for (let i = 0; i < horizontal; i++) {
            for (let j = 0; j < vertical; j++) {
                shelfList.push(`${saveParam.params.shelfName}-${i}${j}`);
            }
        }
        this.shelfList = shelfList;
    };

    getShelfList() {
        return this.shelfList;
    }
    getShelf() {
        return this.shelfJson;
    };

    async delShelves() {
        await wmsWarehouse.delShelf({ shelfName: this.getShelf().shelfName });
    }
}

module.exports = { UserJson, Shelf };