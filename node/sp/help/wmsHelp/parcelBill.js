const common = require('../../../lib/common');
const combine = require('../../../reqHandler/sp/wms/combine');
const storage = require('../../../reqHandler/sp/wms/storage');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');

const parcelBillManager = module.exports = {};

/**
 * 模拟合包单参数
 */
parcelBillManager.mockCombBillJson = function () {
    return new MockCombBillJson();
};
class MockCombBillJson {
    constructor() {
        this.warehouse = {};
        this.sellers = new Map();
        this.buyer = {};
    }

    setWarehouse(warehouse) {
        this.warehouse = {
            tenantId: warehouse.tenantId,
            unitId: warehouse.unitId,
            saasId: warehouse.saasId,
        };
        return this;
    }

    async setBuyer(buyer) {
        const addressInfo = await spMdm.getUserDefaultRecInfo().then(res => res.result.data);
        this.buyer = {
            buyerUnitId: buyer.unitId,
            buyerCid: buyer.clusterCode,
            buyerName: buyer.tenantName,
            buyerPhone: buyer.mobile,
            buyerProvince: addressInfo.address.provinceCode,
            buyerCity: addressInfo.address.cityCode,
            buyerArea: addressInfo.address.countyCode,
            buyerAddr: addressInfo.address.detailAddr,
        };

        return this;
    }

    setSeller(seller) {
        const _seller = {
            sellerUnitId: seller.unitId,
            sellerCid: seller.clusterCode,
            sellerName: seller.name,
            sellerPhone: seller.contactPhone,//获取店铺详情不再返回手机号 需要优化取值来源
            sellerMarketId: seller.marketId,
            deliverProvince: seller.deliverProvCode,
            deliverCity: seller.cityCode,
            deliverArea: seller.areaCode,
            deliverMarket: 1,
            deliverAddr: seller.detailAddr,
        };
        this.sellers.set(seller.tenantId, _seller);
        return this;
    }

    getJson() {
        const combBillDetailList = [...this.sellers.values()].map(seller => {
            const mockId = common.getRandomNum(1000, 9999);
            return { salesBillId: mockId, purBillId: mockId, billNo: mockId, totalNum: common.getRandomNum(10, 20), ...seller };
        });
        return {
            tenantId: this.warehouse.tenantId,
            unitId: this.warehouse.unitId,
            saasId: this.warehouse.saasId,
            logisFeeOrig: common.getRandomNum(100, 200),
            combBillDetailList,
            combBillReceiver: this.buyer,
        }
    }
}

parcelBillManager.mockParcelBillJson = function () {
    return new MockParcelBillJson();
};
class MockParcelBillJson {
    constructor(params) {
        this.warehouse = {};
        this.parcelBill = {};
        this.dresMap = new Map();
    }
    setWarehouse(warehouse) {
        this.warehouse = {
            warehouseId: warehouse.tenantId,
            warehouseUnitId: warehouse.unitId,
            saasId: warehouse.saasId,
        };
        return this;
    }
    setCombBoll(params) {
        this.parcelBill = {
            combBillId: params.combBillId,
            salesBillNo: params.billNo,
            salesBillId: params.salesBillId,
        }
        return this;
    }
    addDres(dres) {
        this.dresMap.set(dres.id, dres);
        return this;
    }
    /**
     * 
     * @param {object} params 
     * @param {number} [params.dresNum=1] 
     * @param {number} [params.skuNum=1]  
     */
    getJson(params = {}) {
        const json = {
            parcelBill: {
                totalNum: 0,
                hashKey: common.getRandomNumStr(12),
                ...this.parcelBill
            },
            parcelDetails: [],
            ...this.warehouse
        };
        [...this.dresMap.values()].slice(0, params.dresNum || 1).forEach(dres => {
            dres.skus.slice(0, params.skuNum || 1).forEach(sku => {
                const detail = {
                    salesDetailId: common.getRandomNumStr(4),
                    spuCode: dres.spu.code,
                    spuTitle: dres.spu.title,
                    spuDocId: dres.spu.ecCaption.docHeader[0].docUrl,
                    spuId: dres.spu.spuId,
                    skuId: sku.id,
                    spec1: sku.spec1,
                    spec1Name: sku.ecCaption.spec1 || '',
                    spec2: sku.spec2,
                    spec2Name: sku.ecCaption.spec2 || '',
                    spec3: sku.spec3,
                    spec3Name: sku.ecCaption.spec3 || '',
                    num: common.getRandomNum(10, 20),
                };
                json.parcelBill.totalNum += detail.num;
                json.parcelDetails.push(detail);
            })
        });
        return json;
    }
}

parcelBillManager.setupParcelBill = function () {
    return new WmsParcelBill();
};
/**
 * 包裹单
 */
class WmsParcelBill {
    constructor(params) {
        /**  订单ID */
        this.id = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';
        /**  saasClusterId */
        this.saasClusterId = 0;
        // /**  生成时间 */
        // this.proTime = '';
        /**  主表ID（comb_bill） */
        this.combBillId = '';
        /**  订单号 */
        this.salesBillNo = '';
        /**  销售单ID */
        this.salesBillId = '';
        /**  卖家名称 */
        this.sellerName = '';
        /**  卖家号码 */
        this.sellerPhone = '';
        /**  状态 0分配骑手 1待取货*/
        this.flag = 0;
        /**  数量 */
        this.totalNum = 0;
        /**  收货号 */
        this.collectionNo = '';
        /**  骑手id */
        this.riderId = '';
        /**  骑手名称 */
        this.riderName = '';
        /**  分配骑手时间 */
        this.allotRiderDate = '';
        /**  确认取件数量 */
        this.pickNum = 0;
        /**  确认取货时间 */
        this.pickDate = '';
        /**  入库操作人 */
        this.consigneeId = '';
        /**  入库时间 */
        this.consigneeDate = '';
        /**  货架号 */
        this.shelfNo = '';
        /**  物流单表ID */
        this.logisBillId = 0;
        /**  发货人 */
        this.deliveryBy = 0;
        /**  发货时间 */
        this.deliveryTime = '';
        /**  收件单表ID (收货地址的id) */
        this.receiverId = '';
        /**  备注 */
        this.rem = '';
        /**  hashKey */
        this.hashKey = '';
        /**  扩展信息 */
        this.extProps = {};
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /** 包裹单明细 key:id*/
        this.details = new Map();
    }

    /**
     * 根据创建包裹(mock)请求 初始化
     * @param {object} params
     * @param {object} params.combBillRes
     * @param {object} params.parcelBillRes
     */
    setByMock(params) {
        const combBillJson = params.combBillRes.params.jsonParam,
            jsonParam = params.parcelBillRes.params.jsonParam,
            data = params.parcelBillRes.result.data;

        this.id = data.id;
        this.collectionNo = data.collectionNo;
        this.saasId = jsonParam.saasId;
        this.unitId = jsonParam.warehouseUnitId;
        this.combBillId = jsonParam.parcelBill.combBillId;
        this.salesBillNo = jsonParam.parcelBill.salesBillNo;
        this.salesBillId = jsonParam.parcelBill.salesBillId;
        this.totalNum = jsonParam.parcelBill.totalNum;
        this.hashKey = jsonParam.parcelBill.hashKey;

        const combBillDetail = combBillJson.combBillDetailList.find(data => data.salesBillId == this.salesBillId);
        this.sellerName = combBillDetail.sellerName;
        this.sellerPhone = combBillDetail.sellerPhone;
        this.sellerAddr = combBillDetail.deliverAddr;
        this.receiverId = params.combBillRes.result.data.details.find(data => data.salesBillId == this.salesBillId).receiverId;

        this.details.clear();
        data.details.forEach((detail) => {
            const parcelDetail = new WmsParcelDetail();
            common.update(parcelDetail, detail);
            parcelDetail.saasId = this.saasId;
            parcelDetail.unitId = this.unitId;
            this.details.set(detail.id, parcelDetail);
        })
        return this;
    }

    /**
     * 查询包裹列表 查询本单
     * @param {object} params 
     */
    async findBillFromList(params = {}) {
        const res = await storage.getParcelBillList({ collectionNo: this.collectionNo, searchFlag: 0, ...params });
        const bill = res.result.data.rows.find(data => data.parcelBill.id == this.id);
        expect(bill, `${res.reqUrl}\n\t未查询到包裹单 id=${this.id}\n\t`).not.to.be.undefined;
        return bill;
    }

    getListExp() {
        const parcelBill = _.cloneDeep(this);
        delete parcelBill.details;
        return { parcelBill, parcelDetails: [...this.details.values()] };
    }

    /**
     * 骑手查询收货列表 查询本单
     * @param {object} params 
     */
    async findBillFromRiderList(params = {}) {
        const res = await storage.getParcelRiderMissionList({ riderId: this.riderId, unitid: this.unitId, searchFlag: 0, ...params });
        const bill = res.result.data.rows.find(data => data.id == this.id);
        console.log(`bill=${JSON.stringify(bill)}`);
        expect(bill, `${res.reqUrl}\n\t未查询到包裹单 id=${this.id}\n\t`).not.to.be.undefined;
        return bill;
    }

    /**
     * 查询包裹详情
     */
    async getFull() {
        return storage.getParcelBillFull({ id: this.id }).then(res => res.result.data);
    }

    /**
     * 查询包裹详情
     */
    async getFullByRider() {
        return storage.getParcelFull({ id: this.id }).then(res => res.result.data);
    }

    getFullExp() {
        const exp = _.cloneDeep(this);
        delete exp.details;
        exp.parcelDetails = [...this.details.values()];
        return exp;
    }

    /**
     * 分配骑手
     * @param {object} params 
     * @param {string} params.id
     * @param {string} params.name
     */
    async allotRider(params) {
        await storage.allotParcelBillRider({ id: this.id, riderId: params.id });
        this.riderId = params.id;
        this.riderName = params.name;
        this.flag = 10;
    }

    /**
     * 确认取货
     */
    async confirmPick(params = {}) {
        const parcelDetails = [...this.details.values()].map(detail => {
            return { id: detail.id, pickNum: detail.num }
        });
        // params
        const pickNum = parcelDetails.reduce((a, b) => common.add(a.pickNum, b.pickNum), { pickNum: 0 });

        await storage.confirmPick({ id: this.id, pickNum, parcelDetails });

        this.flag = 20;
        this.pickNum = pickNum;
        parcelDetails.forEach(detail => {
            const _detail = this.details.get(detail.id);
            _detail.pickNum = detail.pickNum;
            this.details.set(detail.id, _detail);
        });
    }

    /**
     * 包裹入库
     */
    async consignee(params = {}) {
        const res = await storage.consigneeParcelBill({ id: this.id, type: 0, ...params });
        // console.log(`res=${JSON.stringify(res)}`);

        this.flag = 30;
        this.shelfNo = res.result.data.shelfNo;
        this.consigneeId = LOGINDATA.userId;
        this.consigneeDate = res.opTime;//可能有1S误差
    }

}

/**
 * 包裹单明细
 */
class WmsParcelDetail {
    constructor(params) {
        /**  ID */
        this.id = '';
        /**  单元id */
        this.unitId = '';
        /**  saasId */
        this.saasId = '';
        /**  销售单明细ID */
        this.salesDetailId = '';
        /**  包裹单id */
        this.parcelBillId = '';
        /**  spu的编码 */
        this.spuCode = '';
        /**  商品标题 */
        this.spuTitle = '';
        /**  SPU图片 */
        this.spuDocId = '';
        /**  商品spu编号 */
        this.spuId = '';
        /**  商品sku编号 */
        this.skuId = '';
        /**  规格1 */
        this.spec1 = 0;
        /**  规格1名称 */
        this.spec1Name = '';
        /**  规格2 */
        this.spec2 = 0;
        /**  规格2名称 */
        this.spec2Name = '';
        /**  规格3 */
        this.spec3 = 0;
        /**  规格3名称 */
        this.spec3Name = '';
        /**  数量 */
        this.num = 0;
        /**  确认取件数量 */
        this.pickNum = 0;
        /**  备注 */
        this.rem = '';
        /**  明细的扩展信息 */
        this.extProps = {};
        /**  创建时间 */
        this.createdDate = '';
        // /**  修改时间 */
        // this.updatedDate = '';
    }
}