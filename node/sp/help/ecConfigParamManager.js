const common = require('../../lib/common');
const ecConfig = require('../../reqHandler/slh2/config');

class EcConfigParam {
    constructor(paramInfo = {}) {
        this.id = '';
        this.code = '';
        this.name = '';
        this.val = '';
        this.remark = '';

        common.update(this, paramInfo);
    }

    /**
     * 修改参数基本信息
     * @param {object} params 
     * @param {string} params.val 
     */
    async updateParam(params) {
        if (this.val == params.val) return;

        await ecConfig.updateParamInfo({ id: this.id, name: this.name, val: params.val });
        this.val = params.val;
    }
}

/**
 * 获取系统参数信息
 * @param {string} domainKind 配置域* business 业务参数 system 系统参数
 * @param {string} nameOrCode 名称或编码模糊匹配
 */
const getParamInfo = async function ({ domainKind, nameOrCode }) {
    const res = await ecConfig.getParamList({ domainKind, nameOrCode });
    const paramInfo = res.result.data.rows.find(val => val.code == nameOrCode || val.name == nameOrCode);
    expect(paramInfo, `未找到参数${nameOrCode}`).not.to.be.undefined;
    return new EcConfigParam(paramInfo);
};

module.exports = {
    getParamInfo
};