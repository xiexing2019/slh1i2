const fs = require('fs');
const path = require('path');
const common = require('../../lib/common');

const filePath = path.join(__dirname, '../data/dresHotConfig.json');

const spChannel = module.exports = {};

/**
 * 获取本地已生效的卖家爆款设置信息
 */
spChannel.getLocalLastDresHotConfig = function () {
    const data = JSON.parse(fs.readFileSync(filePath));
    if (!data.hasOwnProperty(LOGINDATA.tenantId)) {
        console.error(`code:${LOGINDATA.code},tenantId:${LOGINDATA.tenantId},尚未保存爆款设置`);
    } else {
        return data[LOGINDATA.tenantId].lastDresHotConfig;
    };
};

/**
 * 保存卖家爆款设置信息到本地
 * @param {object} editData 
 */
spChannel.saveLocalDresHotConfig = function (editData) {
    const data = JSON.parse(fs.readFileSync(filePath));
    if (!data.hasOwnProperty(LOGINDATA.tenantId)) {
        data[LOGINDATA.tenantId] = { today: common.getCurrentDate(), curDresHotConfig: editData, lastDresHotConfig: editData };
    } else if (data[LOGINDATA.tenantId].today == common.getCurrentDate()) {
        data[LOGINDATA.tenantId].curDresHotConfig = editData;
    } else {
        data[LOGINDATA.tenantId].today = common.getCurrentDate();
        [data[LOGINDATA.tenantId].lastDresHotConfig, data[LOGINDATA.tenantId].curDresHotConfig] = [data[LOGINDATA.tenantId].curDresHotConfig, editData];
    };

    fs.writeFile(filePath, JSON.stringify(data), (err) => {
        if (err) console.error(err);
    });
};

