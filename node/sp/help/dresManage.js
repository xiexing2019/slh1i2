const common = require('../../lib/common');
const spCaps = require('../../reqHandler/sp/spCaps');
const sp = require('../../reqHandler/sp');
// const ss = require('../../reqHandler/ss');

class Dres {
    constructor() {
        /** 商品spuId */
        this.id = '';
        /**  集群id _cid */
        this.clusterCode = '';
        /**  租户id _tid */
        this.tenantId = '';
        /** spu */
        this.spu = new DresSpu();
        /** skus */
        this.skus = new Map();
    }

    /**
     * 保存商品
     * @param {object} jsonParam 
     */
    async saveDres(jsonParam) {
        const data = await sp.spdresb.saveFull(jsonParam).then(res => res.result.data);
        // console.log(`\ndata=${JSON.stringify(data)}`);

        this.id = data.spuId;
        this.clusterCode = LOGINDATA.clusterCode;
        this.tenantId = LOGINDATA.tenantId;

        // 更新spu信息
        this.updateSpu(Object.assign({ discount: 1, specialFlag: 1 }, jsonParam.spu));
        this.spu.id = data.spuId;
        this.spu.unitId = LOGINDATA.unitId;

        // 更新sku信息
        this.spu.stockNum = 0;
        jsonParam.skus.forEach((sku, index) => {
            sku.id = data.skuIds[index].skuId;
            this.updateSku(sku);
            this.spu.stockNum += Number(sku.num);
        });
    }

    /**
     * 卖家商品spu信息修改
     * @param {object} params spuInfo
     */
    async editDresSpu(params) {
        await sp.spdresb.editDresSpu({ id: this.id, ...params });
        this.updateSpu(params);
    }

    /**
     * 卖家商品sku库存添加
     * @param {object} params 
     */
    async addDresSkuNum(params) {
        await sp.spdresb.addDresSkuNum(params);
        params.skus.forEach(skuInfo => {
            const sku = this.skus.get(skuInfo.id);
            sku.num += Number(skuInfo.num);
            this.skus.set(sku.id, sku);
        });

        this.spu.stockNum = 0;
        [...this.skus.values()].forEach(sku => this.spu.stockNum += Number(sku.num));
    }

    /**
     * 根据es搜索结果设值
     * @description 只有spu相关信息
     * @param {object} dresInfo 
     */
    setByEs(dresInfo) {
        expect(dresInfo, `es未查找到商品`).not.to.be.undefined;
        this.id = dresInfo.id;
        this.clusterCode = dresInfo.clusterCode;
        this.tenantId = dresInfo.tenantId;
        common.update(this.spu, dresInfo);
        // dresInfo.forEach()
        return this;
    }

    /**
     * 搜索商品详情并更新商品信息
     */
    async searchAndSetByDetail() {
        const dresDetail = await this.getDetailForBuyer();
        this.setByDetail(dresDetail.result.data);
        return this;
    }

    /**
     * 根据卖家搜索设置
     * @param {object} dresInfo 
     */
    setBySeller(dresInfo) {
        this.setByDetail(dresInfo);
        this.clusterCode = LOGINDATA.clusterCode;
        this.tenantId = LOGINDATA.tenantId;
        return this;
    }

    /**
     * 根据商品明细设置
     * @description {id,spu,skus}结构
     * @param {object} dresInfo 
     */
    setByDetail(dresInfo) {
        this.id = dresInfo.id;
        this.updateSpu(dresInfo.spu);
        dresInfo.skus.forEach(sku => this.updateSku(sku));
        return this;
    }

    /**
     * 更新商品spu信息
     * @param {*} spuInfo 
     */
    updateSpu(spuInfo) {
        common.update(this.spu, spuInfo);
        return this;
    }

    /**
     * 更新商品sku信息
     * @param {*} skuInfo 
     */
    updateSku(skuInfo) {
        if (this.skus.has(skuInfo.id)) {
            const sku = this.skus.get(skuInfo.id);
            common.update(sku, skuInfo);
            this.skus.set(sku.id, sku);
        } else {
            const sku = new DresSku();
            common.update(sku, skuInfo);
            sku.spuId = this.spu.id;
            sku.unitId = this.spu.unitId;
            this.skus.set(sku.id, sku);
        }
        return this;
    }

    /**
     * es全局商品搜索 查询本商品
     * @description queryType 根据登录角色 全局会话:0 单元区会话:1
     * @param {object} [params={}] 搜索条件
     */
    async searchDresById(params = {}) {
        params = Object.assign({
            queryType: spCaps.isSpgSessionId() ? 0 : 4,
            keyWords: { id: [this.id] }
        }, params);
        const dresList = await sp.spdresb.searchDres(params)
            .then(res => res.result.data.dresStyleResultList);
        const dresSpu = dresList.find(data => data.id == this.id);
        expect(dresSpu, `全局商品搜索 未查询到商品 spuId=${this.id}`).not.to.be.undefined;
        return dresSpu;
    }

    /**
     * 卖家查询商品列表
     * @param {object} params 
     */
    async findSellerSpuList(params = {}) {
        params = Object.assign({
            nameLike: this.spu.title,
        }, params);
        const dresList = await sp.spdresb.findSellerSpuList(params).then(res => res.result.data.rows);
        const dresDetail = dresList.find(data => data.id == this.id);
        expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${this.id}`).not.to.be.undefined;
        return dresDetail;
    }

    /**
     * 查询租户商品信息
     * @description 卖家
     */
    async getFullById() {
        return sp.spdresb.getFullById({ id: this.id });
    }

    /**
     * 买家查询商品详情
     * @description 需要先es搜索 获取卖家_cid _tid
     */
    async getDetailForBuyer() {
        if (!this.clusterCode || !this.tenantId) {
            throw new Error(`商品 clusterCode=${this.clusterCode},tenantId=${this.tenantId},值有误`);
        }
        return sp.spdresb.getFullForBuyer({
            spuId: this.id,
            buyerId: LOGINDATA.tenantId,
            _cid: this.clusterCode,
            _tid: this.tenantId
        });
    }

    /**
     * 管理员修改商品类别
     * @param {object} params 
     */
    async updateSpuClassByAdmin(params) {
        Object.assign(params, { _cid: this.clusterCode, unitId: this.spu.unitId, tenantId: this.tenantId, id: this.id });
        await sp.spdresb.updateSpuClassByAdmin(params);
        this.updateSpu(params);

        // 平台改价 resetPriceFlag 1:重置改价为原价 0:不重置(默认)
        if (params.resetPriceFlag) {
            this.spu.pubPrice = this.spu.esProps.originalPrice || this.spu.pubPrice;
        } else {
            if (params.hasOwnProperty('changeMoney')) {
                if (!this.spu.esProps) this.spu.esProps = {};
                this.spu.esProps.originalPrice = this.spu.pubPrice;
                this.spu.pubPrice = params.changeMoney;
            }
        }

        // 划线价 不传或者传null 清空
        if (!params.markingPrice) {
            this.spu.markingPrice = 0;
        }

        // 标签  web端给商品打标签,拱es筛选过滤用
        ['seasonLabel', 'mainKindLabel', 'colorLabel', 'versionLabel', 'themeLabel', 'fabric'].forEach(key => {
            if (params.hasOwnProperty(key)) {
                this.spu[key] = params[key];
            }
        });
    }

    /**
     * 卖家更变价格类型到发布价
     * @param {object} params jsonParam
     * @param {number} params.priceType 价格类型(取值1~5，分别对应价格1到价格5)
     * @param {string} [params.discount=1] 折扣 默认1
     * @param {string} [params.discountType=1] 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认 1
     */
    async changePubPriceByPriceType(params) {
        const res = await sp.spdresb.changePubPriceByPriceType(params);

        // 更新折扣
        // discountType 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认1
        const discountType = params.hasOwnProperty('discountType') ? params.discountType : 1;
        const discount = params.hasOwnProperty('discount') ? params.discount : 1;
        this.spu.discount = [1, discount, this.spu.discount][discountType];

        // 更新价格 
        // slhPrice为slh推送过来的价格  constructPrice为推送过来的最高价
        this.spu.pubPrice = Math.round(common.mul(this.spu[`price${params.priceType}`], this.spu.discount));

        return res;
    }

    /**
     * 卖家上架商品
     */
    async onMarket() {
        await sp.spdresb.onMarket({ ids: [this.id] });
        this.spu.flag = 1;
    }

    /**
     * 卖家下架商品
     */
    async offMarket() {
        await sp.spdresb.offMarket({ ids: [this.id] });
        this.spu.flag = -4;
    }

    /**
     * 设置商品隐藏状态
     * @param {object} params 
     * @param {string} params.typeId 0无隐藏 1搜索隐藏 2买家隐藏-店铺不可见 3买家隐藏-店铺可见
     */
    async hideSpuShow(params) {
        await sp.spdresb.hideSpuShow({ id: this.id, tenantId: this.tenantId, unitId: this.spu.unitId, typeId: params.typeId });
        this.spu.hideFlag = params.typeId;
    }

    /**
     * 寻找相似商品
     * @param {object} params 
     */
    async searchSimilarDres(params = {}) {
        const similarList = await sp.spdresb.searchSimilarDres({ spuId: this.id, tenantId: this.tenantId, unitId: this.spu.unitId, ...params });
        return similarList;
    }

    /**
     * 获取库存期望值
     * @description 
     * 1. 下单库存校验 num lockNum 
     * 2. 下单 num不变 lockNum增加  支付 num增加 lockNum减少 取消 num不变 lockNum还原
     * @param {object} params 
     * @param {array} params.purDetails 采购单明细
     * @param {string} params.payFlag 支付状态 0未支付 1已支付
     */
    changeStockByBill(params) {
        params.purDetails.forEach(detail => {
            if (this.skus.has(detail.skuId)) {
                const sku = this.skus.get(detail.skuId);
                if (params.payFlag == 0) {
                    sku.lockNum = common.add(sku.lockNum, detail.num);
                } else {
                    sku.num = common.sub(sku.num, detail.num);
                    sku.lockNum = common.sub(sku.lockNum, detail.num);
                    this.spu.stockNum = common.sub(this.spu.stockNum, detail.num);
                }
                this.skus.set(sku.id, sku);
            }
        });
        return this;
    }

    /**
     * 创建订单综合校验
     * @param {object[]} params 
     * @param {string} params.tagId 
     * @param {string} params.tagKind 
     * @param {string} params.currentOrderNum 
     * @param {string} [params.skuId] 
     */
    async verifyCreateOrder(params) {
        const defParam = { tenantId: this.tenantId, spuId: this.id, spuName: this.spu.name, tagId: 0, tagKind: 0 };
        params = params.map(param => Object.assign({}, defParam, param));
        return sp.spTrade.verifyCreateOrder(params);
    }

    /**
     * 获取商品详情期望值
     */
    getDetailExp() {
        return {
            id: this.id,
            spu: this.spu,
            skus: [...this.skus.values()],
        }
    }

};

/**
 * 商品
 */
function DresSpu() {
    /**  主键id */
    this.id = '';
    /**  单元id */
    this.unitId = '';
    /**  款号 */
    this.code = '';
    /**  名称 */
    this.name = '';
    /**  标题 */
    this.title = '';
    /**  名称拼音 */
    this.namePy = '';
    /**  类别 */
    this.classId = '';
    /**  规格1范围 */
    this.spec1Ids = {};
    /**  规格2范围 */
    this.spec2Ids = {};
    /**  规格3范围 */
    this.spec3Ids = {};
    /**  品牌 */
    this.brandId = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  划线价 */
    this.markingPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  计量单位 */
    this.unit = '';
    /**  上架日期 */
    this.marketDate = '';
    /**  季节：1-6 春夏 秋 冬 春 夏 秋冬 。 对应字典2034 */
    this.platSeason = '';
    /**  产地 */
    this.origin = '';
    /**  发货地 */
    this.shipOriginId = 0;
    /**  运费方式 */
    this.shipFeeWayId = 0;
    /**  商品标题区图片 */
    this.docHeader = {};
    /**  商品内容区图片 */
    this.docContent = {};
    /**  是否特价商品 */
    this.special = 0;
    /**  是否允许退货 */
    this.isAllowReturn = 0;
    /**  发布范围 */
    this.pubScope = 0;
    /**  标签 */
    this.labels = {};
    /**  详情页面URL */
    this.url = '';
    /**  状态  -4 卖家下架 -3 商品申请上架 -2 平台下架 -1 商品删除 0 商品下架(对应slh停用) 1商品上架(校验通过) 2商品待上架(校验不通过) 3买家隐藏-店内不可见(hideFlag=2)*/
    this.flag = 1;
    /** 隐藏来源 0-正常 1-大数据隐藏 2-后台隐藏 3-其余 */
    this.hideFrom = 0;
    /** 商品隐藏标志 0 无隐藏状态 1 搜索隐藏 2 买家隐藏-店铺内不可见 3买家隐藏-店铺内可见 */
    this.hideFlag = 0;
    /** 商品当前审核状态，1代表已经审核过，0未审核过 初始状态 */
    this.checkFlag = 0;
    /** 同步能力位（2的N次方 1-特殊库存 2-特殊颜色尺码 */
    this.stockFlag = 0;
    /** 特殊尺码颜色库存 */
    this.stockNum = 0;
    /**  备注 */
    this.rem = '';
    // /**  商品属性 */
    // this.props = {};
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  操作版本号 */
    this.opVer = 0;
    // /**  乐观锁 */
    // this.ver = 0;
    /**  商陆花商品ID */
    this.slhId = '';
    /**  数据Hash值 */
    this.hashKey = '';
    /**  被平台下架原因 */
    this.offmarketReason = '';
    /**  平台下架时间 */
    this.offmarketTime = '';
    /**  上架失败原因 */
    this.marketFailure = '';
    /**  折扣 */
    this.discount = 0;
    /**  商品排序 */
    this.showOrder = 0;
    /**  第三方同步时间 */
    this.slhDate = '';
    /**  商品上新能力位 0 无能力 。1 上新推荐能力 */
    this.specialFlag = 0;
    /**  上架商品不上新原因 */
    this.unRemmondReason = '';
    /**  商陆花停用标志，0 商陆花未停用  1 商陆花已经停用 */
    this.slhUsingFlag = 0;
    /**  平台停用标志，0 平台未停用 1 平台已经停用 */
    this.spPlatUsingFlag = 0;
    // /** 尺码表信息 */
    // this.yardage_props = '';
    // /** 商品详情属性 */
    // this.detail_props = '';
    /** 平台打的商品评分 */
    this.platScore = 0;
    /** 商品标签 */
    this.platLabels = {};
};

/**
 * SKU定义
 */
function DresSku() {
    /**  ID */
    this.id = '';
    /**  单元ID */
    this.unitId = '';
    /**  款号 */
    this.spuId = '';
    /**  规格1 */
    this.spec1 = 0;
    /**  规格2 */
    this.spec2 = 0;
    /**  规格3 */
    this.spec3 = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  数量 */
    this.num = 0;
    /**  锁定库存 */
    this.lockNum = 0;
    /**  虚拟库存 */
    this.inventedNum = 0;
    /**  特殊库存标志位 */
    this.stockFlag = 0;
    /**  状态  好店商家下架-4 卖家申请上架针对平台下架 平台下架-2 删除-1 下架0 上架1 待上架2*/
    this.flag = 1;
    /**  备注 */
    this.rem = '';
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  商陆花SKU ID */
    this.slhId = '';
    /**  sku的编码 */
    this.code = '';
};

const dresManage = module.exports = {
    Dres
};

dresManage.setupDres = function () {
    return new Dres();
};


