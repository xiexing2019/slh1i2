'use strict';
const common = require('../../lib/common');
// const format = require('../../data/format');
const spugr = require('../../reqHandler/sp/global/spugr');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const docManager = require('../../reqHandler/sp/doc_server/doc');
const { URLSearchParams } = require('url');
const spAccount = require('../data/spAccount');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const caps = require('../../data/caps');
// const spCaps = require('../../reqHandler/sp/spCaps');
const mockJsonParam = require('./mockJsonParam');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const spconfb = require('../../reqHandler/sp/biz_server/spconfb');
const mysql = require('../../reqHandler/sp/mysql');

const dictTypeId = {
    size: 605,
    fabric: 637,//面料
    color: 601,
    season: 613,
    goodsSourceCity: 850,
    masterClass: 2010,//主营类目
    theme: 2016,//风格
    age: 2028,

};

let spReq = module.exports = {
    captcha: '000000',
    provCode: '330000',//省份代码，省份所对应的编号，省份如浙江 330000
    cityCode: '330100',//地市代码，市所对应的编号，市如杭州市 330100
};



/**
 * 登录用户 
 * {code:LOGINDATA}
 */
let spLoginUsers = {};

/**
 * sp平台登录
 * @param {object} params 
 * @param {string} params.code 登录名：手机号或邮箱等*
 */
spReq.spLogin = async function (params = {}) {
    // 已登录用户不重新登录
    // 校验会话，更新信息
    if (spLoginUsers.hasOwnProperty(params.code)) {
        LOGINDATA = _.cloneDeep(spLoginUsers[params.code]);
        const sessionLite = await spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId, check: false });
        if (sessionLite.result.code == 0) {
            // 更新登录信息
            Object.assign(LOGINDATA, sessionLite.result.data);
            return;
        };
    };
    params = Object.assign({
        code: '13750850013',
        // pass: '000000',
        // authcType: 1
        captcha: this.captcha,
        authcType: 2
    }, params);
    // 登录并更新登录信息
    const res = await spugr.spLogin(params);
    // console.log(`登录:${JSON.stringify(res)}`);
    spLoginUsers[params.code] = _.cloneDeep(LOGINDATA);
    return res;
};

/**
 * 平台卖家登录(已切换好门店)
 * @description 切换门店sessionId不变
 * @param {object} params
 * @param {string} params.code 登录名：手机号或邮箱等*
 * @param {string} params.shopName 门店名称 唯一
 */
spReq.spSellerLogin = async function ({ code = spAccount.seller.mobile, shopName = spAccount.seller.shopName } = {}) {
    await spReq.spLogin({ code });

    // const  tenantId = await spReq.createEntTenant({ name: spAccount.seller.shopName }).then((res) => res.result.data.val);
    const tenantId = await spugr.findAvailableTenantIds().then((res) => {
        // console.log(`res=${JSON.stringify(res)}`);
        return _.findKey(res.result.data, (val) => val == shopName);
    });
    if (!tenantId) throw new Error(`卖家登录,切换企业租户失败. 用户关联的用户列表未找到'${shopName}',请检查`);
    await spugr.changeTenantInSession({ tenantId });
    spLoginUsers[code] = _.cloneDeep(LOGINDATA);
};

/**
 * 平台买家登录
 * @param {object} params
 * @param {string} params.code 
 */
spReq.spClientLogin = async function (params = {}) {
    if (!params.code) params.code = spAccount.client.mobile;
    return spReq.spLogin(params);
};

/**
 * 退出登录
*/
spReq.spLogOut = async function () {
    delete spLoginUsers[LOGINDATA.code];
    await spugr.spLogOut();
};


/**
 * @description slh员工登入商陆花，备：如果不是adev1的账套，需要将环境同时修改
 * @param {Number} params.logid 工号，如果不传默认000
 * @param {Number} params.epid   账套id，如果不传就是默认adev1: 15485 
*/
spReq.slhStaffLogin = async function (params = {}) {
    let env;
    caps.name == 'sp_test' ? env = 'adev1' : env = 'app1';

    caps.updateEnvByName({ name: env });  //切到adev1，这里主要是为了拿到一代环境的url 
    await common.loginDo({ epid: params.epid || spAccount.slhConSp[env].epid, logid: params.logid || '000' });
    caps.epid = LOGINDATA.epid;
};

/**
 * @description slh员工登入好店,ipad客户登录到平台
*/
spReq.loginGoodShopBySlh = async function (params) {
    await spReq.slhStaffLogin(params);
    return spugr.slhStaffLoginSp();
};

/**
 * @description 切换到命令行输入的环境
*/
spReq.changeToCommonEnv = async function () {
    let argvInfo = process.argv;
    let envInfo = argvInfo.find(res => res.includes('--env='))
    let envName = _.last(envInfo.split("="));
    caps.updateEnvByName({ name: envName });
};

/**
 * 生成一个新的店铺并且完善基本信息
 * 新增租户，会自动切换到企业租户
 */
spReq.createShop = async function (params) {
    const tenantId = LOGINDATA.tenantId;
    const tenantRes = await spReq.createEntTenant(params);

    let saveParam = await mockJsonParam.shopJson();
    saveParam.name = tenantRes.params.jsonParam.name;
    saveParam.id = tenantRes.result.data.val;
    await spugr.updateShop(saveParam);

    let json = mockJsonParam.merchantJson();
    json.name = tenantRes.params.jsonParam.name;
    json.contact = tenantRes.params.jsonParam.name;
    json.mobile = LOGINDATA.mobile;
    const res = await spugr.createMerchat(json);

    await spugr.changeTenantInSession({ tenantId });

    await spAuth.staffLogin();
    await spugr.updateSpMerchatFlagByPerson({ id: res.result.data.id, flag: 1 });
    await spugr.onlineShopById({ id: tenantRes.result.data.val });
    return tenantRes;
};

/**
 * 注册用户
 */
spReq.userRegister = async function (params) {
    params = Object.assign({
        //手机号使用12开头,与服务端约定,不发送短信通知
        mobile: common.getRandomMobile(),
        pass: '000000',
        userName: 'luxx',
        provCode: this.provCode,
        cityCode: this.cityCode,
        captcha: this.captcha,
    }, params);
    await spAuth.staffLogin();
    await spAuth.saveWhiteList(params.mobile); //加入白名单
    return spugr.userRegister(params);
};

/**
 * 使用新注册用户登录
 * @description spugr.sp_unit.flag=1表示创建成功
 */
spReq.registerLogin = async function (params) {
    await spReq.spLogin(params);

    const spgSql = await mysql({ dbName: 'spgMysql' });
    let flag = await spgSql.query(`SELECT flag FROM spugr.sp_unit WHERE tenant_id=${LOGINDATA.tenantId}`).then(res => res[0][0].flag);
    if (flag != 1) {
        await common.delay(10000);
        flag = await spgSql.query(`SELECT flag FROM spugr.sp_unit WHERE tenant_id=${LOGINDATA.tenantId}`).then(res => res[0][0].flag);
    }
    await spgSql.end();

    if (flag != 1) {
        throw new Error(`创建单元${LOGINDATA.tenantId}失败 请重试`);
    }
};

/**
 * 注册一个新用户
 * @description 此方法注册的是一个已经审核通过的用户
 * @returns res 注册返回信息
 */
spReq.registerNewUser = async function () {
    let res;
    do {
        res = await spReq.userRegister({ check: false });
        // 添加日志 单元没有创建成功 code=-10 subCode=-11
        if (res.result.code == -1) throw new Error(res);
    } while (res.result.code < 0);
    await spReq.spLogin({ code: res.params.jsonParam.mobile });

    // 平台审核
    let areaJson = await mockJsonParam.getAreaJson();
    let submitAuditRes = await spCommon.submitAuditRecord(await mockJsonParam.submitAuditJson({ areaJson }));
    await spAuth.staffLogin();
    await spCommon.changeAuditRecord({
        shopId: submitAuditRes.result.data.id,
        flag: 1,
        auditRemark: '条件符合审核条件，同意了'
    });
    await spAuth.staffLogout();

    return res;
};

/**
 * 注册企业租户
 * @description 需要先用户注册-再企业注册
 * @description 新增租户，会自动切换到企业租户
 * @param {Object} params jsonParam
 */
spReq.createEntTenant = async function (params) {
    params = Object.assign({
        name: common.getRandomStr(5),
        capability: 1,//能力位，从右向左各位代表能力，1 服装批发，2 服装零售*
        provCode: this.provCode,
        cityCode: this.cityCode,
        captcha: this.captcha,//验证码
    }, params);
    const createRes = await spugr.createEntTenant(params);


    return createRes;
};

/**
 * 买家查询商品信息
 * @description 前置条件:全局搜索商品->获取商品detailUrl
 * @param {object} params 
 * @param {string} params.detailUrl 商品详情url
 */
spReq.getFullForBuyerByUrl = async function (params) {
    const strParams = _.last(params.detailUrl.split('?'));
    const urlParams = new URLSearchParams(strParams);
    return spdresb.getFullForBuyer({
        spuId: urlParams.get('spuId'),
        _cid: urlParams.get('_cid'),
        _tid: urlParams.get('_tid'),
        buyerId: LOGINDATA.tenantId
    });
};

/**
 * 管理员修改商品类别
 * @description 前置条件:全局搜索商品->获取商品detailUrl
 * @param {object} params 
 * @param {string} params.detailUrl 商品详情url
 * @param {string} params.unitId 商品归属的单元id
 */
spReq.updateSpuClassByAdmin = async function (params) {
    const strParams = _.last(params.detailUrl.split('?'));
    const urlParams = new URLSearchParams(strParams);
    delete params.detailUrl;
    return spdresb.updateSpuClassByAdmin({
        _cid: urlParams.get('_cid'),
        id: urlParams.get('spuId'),
        tenantId: urlParams.get('_tid'),
        ...params
    });
};

spReq.getDocData = async function () {
    let docData = { image: { dataList: [] }, vedio: { dataList: [] } };
    await spugr.getDictList({ typeId: '2002', flag: 1 })
        .then((res) => {
            res.result.data.rows.forEach((data) => {
                switch (data.codeName) {
                    case '图片':
                        docData.image.typeId = data.codeValue;
                        break;
                    case '视频':
                        docData.vedio.typeId = data.codeValue;
                        break;
                    default:
                        break;
                };
            })
        });

    const docList = await docManager.getDocListSome();

    //列表最多100条，暂时不需要这么多
    const length = Math.min(docList.result.data.rows.length, 20);
    for (let index = 0; index < length; index++) {
        const docId = docList.result.data.rows[index];
        const docUrl = await docManager.getDoc({ id: docId })
            .then((res) => res.result.data[docId]);

        if (common.isImage(docId)) {
            docData.image.dataList.push({ docId, docUrl });
        };
    };

    return docData;
};

/**
 * 白名单删除
 * @description 内部使用 
 */
spReq.delWhite = async function () {
    await spAuth.staffLogin();
    // 获取白名单列表
    const whiteList = await spAuth.getWhiteList().then(res => res.result.data.rows);
    // console.log(`whiteList=${JSON.stringify(whiteList)}`);
    // 排除常用手机号
    const usualMobiles = new Set(), deleteMobiles = new Set();
    Object.values(spAccount).forEach(data => {
        if (data.hasOwnProperty('mobile')) {
            usualMobiles.add(data.mobile);
        }
    });
    whiteList.forEach(data => {
        if (!usualMobiles.has(data.mobile)) {
            deleteMobiles.add(data.id);
        };
    });
    for await (const val of deleteMobiles) {
        await spAuth.deleteWhiteList({ id: val, check: false });
        await common.delay(100);
    }
    // 登出
    await spAuth.staffLogout();
};

/**
 * 执行任务
 */
spReq.executeTask = async function ({ urlDir, taskCode, unitId = LOGINDATA.unitId }) {
    return common.get(`${urlDir}/task/execute.do`, { taskCode, unitId });
};

/**
 * 前提需要卖家先登录
 * 新建一个商品，并且返回新建商品的信息
 * @param {function} cb 修改新增商品的jsonParam
 */
spReq.saveDresFull = async function (cb) {
    const classId = [1013, 1085, 1093][common.getRandomNum(0, 2)];
    let classInfo = await spdresb.findByClass({ classId });
    let arr = ['606', '850', '2001', '2002', '2003', '2004'];
    classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
            .then((res) => res.result.data.rows);
    };
    const json = mockJsonParam.styleJson({ classId });
    if (typeof cb == 'function') {
        cb(json);
    };
    let styleRes = await spdresb.saveFull(json);
    await common.delay(500);

    return styleRes;
};

/**
 * 新增运费，并且同时修改店铺的运费方式属性
*/
spReq.saveShipRule = async function (params) {
    let saveRes = await spconfb.saveShipRule(params);
    let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
    let shipFeeWayId;   //1 包邮，2自定义运费
    params.feeType == 0 ? shipFeeWayId = 1 : shipFeeWayId = 2;
    shopInfo.shipFeeWayId = shipFeeWayId;
    await spugr.updateShop(shopInfo);
    return saveRes;
};

/**
 * 获取字典列表
 */
spReq.getDictList = async function (dictArr) {
    if (!dictArr.length) return;
    for (let index = 0; index < dictArr.length; index++) {
        const typeId = dictTypeId[dictArr[index]];
        if (!typeId) {
            console.warn(`未找到${dictArr[index]}的typeId,请确认~`);
            continue;
        };
        if (!BASICDATA[typeId]) BASICDATA[typeId] = await spugr.getDictList({ typeId, flag: 1 })
            .then((res) => res.result.data.rows);
    };
};

