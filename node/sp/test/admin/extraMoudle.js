const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const mockJsonParam = require('../../help/mockJsonParam');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const format = require('../../../data/format');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spugr = require('../../../reqHandler/sp/global/spugr');

describe('运费模块', function () {
    this.timeout(TESTCASE.timeout);

    describe('运费模板', function () {
        let startProvCode, areaJson;
        before('数据准备', async function () {
            areaJson = await mockJsonParam.getAreaJson();
            startProvCode = areaJson.provinceValue; //随机取一个省份，然后给该省份添加模板
        });

        describe('运费按件数模板', function () {
            let saveRes, updateJson;
            before('获取按件数模板', async function () {
                await spAuth.staffLogin();

                //尝试删除随机省份对应的记录，因为这里有可能没有这个省对应的记录，所以check =false,一个省份只能有一个模板，所以要删除了已有的
                await spCommon.deleteFeeRule({ feeType: 1, startProvCode, check: false });

                //保存运费规则
                saveRes = await spCommon.saveFeeRule(saveRuleJson(startProvCode, areaJson, 1));
            });

            it('1.管理员查看运费列表', async function () {
                //根据查询条件  运费类型+发货地进行查询 
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 1, startProvCode }).then(res => res.result.data.rows);
                let ruleInfo = feeRuleList.find(element => element.startProvCode == startProvCode);
                if (ruleInfo == undefined) {
                    throw new Error(`列表中没有找到添加的运费规则，添加的运费规则是${saveRes.params.jsonParam}`);
                } else {
                    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, ruleInfo);
                };
            });

            it('2.列表排序', async function () {
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 1 }).then(res => res.result.data.rows);
                let feeRuleList1 = _.cloneDeep(feeRuleList);
                let sortArr = feeRuleList1.sort(function (obj1, obj2) {
                    let val1 = common.stringToDate(obj1.updatedDate);
                    let val2 = common.stringToDate(obj2.updatedDate);;
                    return val2 - val1;
                });
                common.isApproximatelyEqualAssert(sortArr, feeRuleList, [], '排序错误');
            });

            it('3.管理员查看运费模板详情', async function () {
                let feeRuleInfo = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 1 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(saveRes.params.jsonParam.rules, feeRuleInfo.rows);
            });

            it('4.修改运费', async function () {
                await spAuth.staffLogin();
                let feeRuleInfo = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 1 }).then(res => res.result.data);
                let id = feeRuleInfo.rows[0].id;   //取到刚刚添加的这个运费规则的第一条明细的id
                updateJson = _.cloneDeep(saveRes.params.jsonParam);
                updateJson.rules[0].id = id;
                updateJson.rules[0].startNum += 1;    //修改，将起始数量和起始运费加1
                updateJson.rules[0].startFee += 1;
                let updateRes = await spCommon.saveFeeRule(updateJson);
                let feeRuleInfoAfter = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 1 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(updateRes.params.jsonParam.rules, feeRuleInfoAfter.rows);
            });

            it('5.同一个发货地，增加相同的收货地', async function () {
                let saveJson = _.cloneDeep(updateJson);
                saveJson.rules.push(saveJson.rules[0]);
                delete saveJson.rules[1].id;
                saveJson.check = false;
                let res = await spCommon.saveFeeRule(saveJson);
                expect(res.result).to.includes({ "msgId": "provId_exist" });
            });

            it('6.删除运费模板', async function () {
                await spCommon.deleteFeeRule({ feeType: 1, startProvCode });
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 1, startProvCode }).then(res => res.result.data.rows);
                expect(feeRuleList.length, '删除后运费规则依然存在列表中').to.be.equal(0);
            });
        });

        describe('运费按重量模板', function () {
            let saveRes, updateJson;
            before('获取按重量模板', async function () {
                await spAuth.staffLogin();

                //尝试删除随机省份对应的记录，因为这里有可能没有这个省对应的记录，所以check =false,一个省份只能有一个模板，所以要删除了已有的
                await spCommon.deleteFeeRule({ feeType: 2, startProvCode, check: false });
                //保存运费规则
                saveRes = await spCommon.saveFeeRule(saveRuleJson(startProvCode, areaJson, 2));
            });

            it('1.管理员查看运费列表', async function () {
                //根据查询条件  运费类型+发货地进行查询 
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 2, startProvCode }).then(res => res.result.data.rows);
                let ruleInfo = feeRuleList.find(element => element.startProvCode == startProvCode);
                if (ruleInfo == undefined) {
                    throw new Error(`列表中没有找到添加的运费规则，添加的运费规则是${saveRes.params.jsonParam}`);
                } else {
                    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, ruleInfo);
                };
            });

            it('2.列表排序', async function () {
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 2 }).then(res => res.result.data.rows);
                let feeRuleList1 = _.cloneDeep(feeRuleList);
                let sortArr = feeRuleList1.sort(function (obj1, obj2) {
                    let val1 = common.stringToDate(obj1.updatedDate);
                    let val2 = common.stringToDate(obj2.updatedDate);;
                    return val2 - val1;
                });
                common.isApproximatelyEqualAssert(sortArr, feeRuleList, [], '排序错误');
            });

            it('3.管理员查看运费详情', async function () {
                let feeRuleInfo = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 2 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(saveRes.params.jsonParam.rules, feeRuleInfo.rows);
            });

            it('4.修改运费', async function () {
                let feeRuleInfo = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 2 }).then(res => res.result.data);
                let id = feeRuleInfo.rows[0].id;   //取到刚刚添加的这个运费规则的第一条明细的id
                updateJson = _.cloneDeep(saveRes.params.jsonParam);
                updateJson.rules[0].id = id;
                updateJson.rules[0].startNum += 1;    //修改，将起始数量和起始运费加1
                updateJson.rules[0].startFee += 1;
                let updateRes = await spCommon.saveFeeRule(updateJson);
                let feeRuleInfoAfter = await spCommon.findFeeRuleInfo({ startProvCode, feeType: 2 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(updateRes.params.jsonParam.rules, feeRuleInfoAfter.rows);
            });

            it('5.同一个发货地，增加相同的收货地', async function () {
                let saveJson = _.cloneDeep(updateJson);
                saveJson.rules.push(saveJson.rules[0]);
                delete saveJson.rules[1].id;
                saveJson.check = false;
                let res = await spCommon.saveFeeRule(saveJson);
                expect(res.result).to.includes({ "msgId": "provId_exist" });
            });

            it('6.删除运费', async function () {
                await spCommon.deleteFeeRule({ feeType: 2, startProvCode });
                let feeRuleList = await spCommon.findFeeRuleList({ feeType: 2, startProvCode }).then(res => res.result.data.rows);
                expect(feeRuleList.length, '删除后运费规则依然存在列表中').to.be.equal(0);
            });
        });

        describe('包邮按件数模板', function () {
            let saveRes, updateJson;
            before('保存包邮按件数模板', async function () {
                await spAuth.staffLogin();
                //  feeType 1 满件数
                await spCommon.deleteFreeRule({ startProvCode, freeType: 1 });
                //保存运费规则
                saveRes = await spCommon.saveFreeFule(saveFreeRuleJson(startProvCode, areaJson, 1));
            });


            it('1.管理员查看包邮按件数列表', async function () {
                //根据查询条件  运费类型+发货地进行查询 
                let feeRuleList = await spCommon.findFreeRuleList({ feeType: 1, startProvCode }).then(res => res.result.data.rows);
                let ruleInfo = feeRuleList.find(element => element.startProvCode == startProvCode);
                if (ruleInfo == undefined) {
                    throw new Error(`列表中没有找到添加的运费规则，添加的运费规则是${saveRes.params.jsonParam}`);
                } else {
                    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, ruleInfo);
                };
            });

            it('2.列表排序', async function () {
                let feeRuleList = await spCommon.findFreeRuleList({ feeType: 1 }).then(res => res.result.data.rows);
                let feeRuleList1 = _.cloneDeep(feeRuleList);
                let sortArr = feeRuleList1.sort(function (obj1, obj2) {
                    let val1 = common.stringToDate(obj1.updatedDate);
                    let val2 = common.stringToDate(obj2.updatedDate);;
                    return val2 - val1;
                });
                common.isApproximatelyEqualAssert(sortArr, feeRuleList, [], '排序错误');
            });

            it('3.管理员查看运费详情', async function () {
                let feeRuleInfo = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 1 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(saveRes.params.jsonParam.rules, feeRuleInfo.rows);
            });

            it('4.修改包邮按件数模板', async function () {
                let feeRuleInfo = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 1 }).then(res => res.result.data);
                let id = feeRuleInfo.rows[0].id;   //取到刚刚添加的这个运费规则的第一条明细的id
                updateJson = _.cloneDeep(saveRes.params.jsonParam);
                updateJson.rules[0].id = id;
                updateJson.rules[0].limitNum += 10;
                let updateRes = await spCommon.saveFreeFule(updateJson);
                let feeRuleInfoAfter = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 1 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(updateRes.params.jsonParam.rules, feeRuleInfoAfter.rows);
            });

            it('5.同一个发货地，增加相同的地区包邮地区', async function () {
                let saveJson = _.cloneDeep(updateJson);
                saveJson.rules.push(saveJson.rules[0]);
                delete saveJson.rules[1].id;
                saveJson.check = false;
                let res = await spCommon.saveFeeRule(saveJson);
                expect(res.result).to.includes({ "msgId": "provId_exist" });
            });

            it('6.删除运费', async function () {
                await spCommon.deleteFreeRule({ freeType: 1, startProvCode });
                let feeRuleList = await spCommon.findFreeRuleList({ freeType: 1, startProvCode }).then(res => res.result.data.rows);
                expect(feeRuleList.length, '删除后运费规则依然存在列表中').to.be.equal(0);
            });

        });

        describe('包邮按金额模板', function () {
            let saveRes, updateJson;
            before('保存包邮按金额模板', async function () {
                await spAuth.staffLogin();
                //  feeType 2 满金额
                await spCommon.deleteFreeRule({ startProvCode, freeType: 2 });
                //保存运费规则
                saveRes = await spCommon.saveFreeFule(saveFreeRuleJson(startProvCode, areaJson, 2));
            });


            it('1.管理员查看包邮按金额列表', async function () {
                //根据查询条件  运费类型+发货地进行查询 
                let feeRuleList = await spCommon.findFreeRuleList({ feeType: 2, startProvCode }).then(res => res.result.data.rows);
                let ruleInfo = feeRuleList.find(element => element.startProvCode == startProvCode);
                if (ruleInfo == undefined) {
                    throw new Error(`列表中没有找到添加的运费规则，添加的运费规则是${saveRes.params.jsonParam}`);
                } else {
                    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, ruleInfo);
                };
            });

            it('2.列表排序', async function () {
                let feeRuleList = await spCommon.findFreeRuleList({ feeType: 2 }).then(res => res.result.data.rows);
                let feeRuleList1 = _.cloneDeep(feeRuleList);
                let sortArr = feeRuleList1.sort(function (obj1, obj2) {
                    let val1 = common.stringToDate(obj1.updatedDate);
                    let val2 = common.stringToDate(obj2.updatedDate);;
                    return val2 - val1;
                });
                common.isApproximatelyEqualAssert(sortArr, feeRuleList, [], '排序错误');
            });

            it('3.管理员查看运费包邮详情', async function () {
                let feeRuleInfo = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 2 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(saveRes.params.jsonParam.rules, feeRuleInfo.rows);
            });

            it('4.修改包邮规则', async function () {
                let feeRuleInfo = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 2 }).then(res => res.result.data);
                let id = feeRuleInfo.rows[0].id;   //取到刚刚添加的这个运费规则的第一条明细的id
                updateJson = _.cloneDeep(saveRes.params.jsonParam);
                updateJson.rules[0].id = id;
                updateJson.rules[0].limitNum += 10;
                let updateRes = await spCommon.saveFreeFule(updateJson);
                let feeRuleInfoAfter = await spCommon.findFreeRuleInfo({ startProvCode, freeType: 2 }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(updateRes.params.jsonParam.rules, feeRuleInfoAfter.rows);
            });

            it('5.同一个发货地，增加相同的地区包邮地区', async function () {
                let saveJson = _.cloneDeep(updateJson);
                saveJson.rules.push(saveJson.rules[0]);
                delete saveJson.rules[1].id;
                saveJson.check = false;
                let res = await spCommon.saveFeeRule(saveJson);
                expect(res.result).to.includes({ "msgId": "provId_exist" });
            });

            it('6.删除运费', async function () {
                await spCommon.deleteFreeRule({ freeType: 2, startProvCode });
                let feeRuleList = await spCommon.findFreeRuleList({ freeType: 2, startProvCode }).then(res => res.result.data.rows);
                expect(feeRuleList.length, '删除后运费规则依然存在列表中').to.be.equal(0);
            });

        });

    });

    describe('运费监控(冒泡)', function () {
        let typeId = [1, 2];
        for (let i = 0; i < typeId.length; i++) {
            describe('管理员设置运费监控', function () {
                let deliverProvCode, areaJson, saveMonitorRes;
                before('管理员登录', async function () {
                    await spReq.spSellerLogin();
                    //deliverProvCode = await spugr.getShopDetail().then(res => res.result.data.deliverProvCode);

                    //let shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                    areaJson = await mockJsonParam.getAreaJson();
                    // console.log(`shipRuleList=${JSON.stringify(shipRuleList)}`);
                    deliverProvCode = areaJson.provinceValue;

                    await spAuth.staffLogin();
                    await spCommon.delMonitorById({ origPos: deliverProvCode, typeId: typeId[i] });
                    saveMonitorRes = await spCommon.saveMonitor(saveMonitorJson(typeId[i], deliverProvCode));
                });

                it('1.查询监控模板', async function () {
                    let res = await spCommon.getMonitorList({ typeId: typeId[i] }).then(res => res.result.data.rows);
                    let info = res.find(val => val.origPos == deliverProvCode);
                    expect(info, '添加了监控模板，但是查询不到此监控模板').to.not.be.undefined;
                });

                it('2.查看监控详情', async function () {
                    let monitorInfo = await spCommon.getMonitorInfo({ origPos: deliverProvCode, typeId: typeId[i] });
                    common.isApproximatelyArrayAssert(saveMonitorRes.params.jsonParam.targetPoses, monitorInfo.result.data.rows);
                });

                it('3.修改运费监控', async function () {
                    let monitorInfo = await spCommon.getMonitorInfo({ origPos: deliverProvCode, typeId: typeId[i] });

                    //修改运费监控的参数
                    let updateJson = _.cloneDeep(monitorInfo.result.data);
                    updateJson.typeId = monitorInfo.params.jsonParam.typeId;
                    updateJson.origPos = monitorInfo.params.jsonParam.origPos;
                    updateJson.targetPoses = updateJson.rows;
                    updateJson.targetPoses.push(_.cloneDeep(updateJson.rows[0]));
                    _.remove(areaJson.proviceArr, res => res == updateJson.rows[0].targetPos);
                    let targetPos = areaJson.proviceArr[common.getRandomNum(0, areaJson.proviceArr.length - 1)];

                    updateJson.targetPoses[1].targetPos = targetPos;
                    delete updateJson.rows;
                    delete updateJson.targetPoses[1].id;

                    let res = await spCommon.saveMonitor(updateJson);
                    let info = await spCommon.getMonitorInfo({ origPos: res.params.jsonParam.origPos, typeId: typeId[i] });

                    common.isApproximatelyArrayAssert(res.params.jsonParam.targetPoses, info.result.data.rows, ['ecSeq']);
                });

                it('4.删除监控', async function () {
                    await spCommon.delMonitorById({ origPos: deliverProvCode, typeId: typeId[i] });
                    let res = await spCommon.getMonitorList({ typeId: typeId[i] }).then(res => res.result.data.rows);
                    let info = res.find(val => val.origPos == deliverProvCode);
                    expect(info, '删除后该条记录还是展示在列表中').to.be.undefined;
                });
            });
        };

        describe('监控列表', async function () {

            it('查看异常监控列表-异常', async function () {
                await spAuth.staffLogin();
                await spCommon.monitoredShopsList({ wrapper: true, status: 1 }).then(res => res.result.data.rows);
            });

            it('查看运费监控列表-正常', async function () {
                await spCommon.monitoredShopsList({ wrapper: true, status: 0 }).then(res => res.result.data.rows);
            });

            it('查看运费监控列表-未刷新', async function () {
                await spCommon.monitoredShopsList({ wrapper: true, status: -1 }).then(res => res.result.data.rows);
            });

            it('查看卖家运费模板', async function () {
                let res = await spCommon.monitoredShopsList({ wrapper: true }).then(res => res.result.data.rows);
                await spCommon.getShopsLogisInfo({ unitId: res[0].unitId });
            });

            it('刷新运费', async function () {
                let res = await spCommon.monitoredShopsList({ wrapper: true }).then(res => res.result.data.rows);
                await common.delay(500);
                await spCommon.refreshLogis({ unitId: res[0].unitId, _cid: res[0].clusterCode, _tid: res[0].tenantId });
            });
        });
    });
});


/**
 * @description 保存运费规则的 参数生成
 * @param {String}  startProvCode   发货地的省份编码
 * @param {Object}  areaJson        省份对象，用来取省区列表
 * @param {Integer} feeType         计费类型，1-计件 2-计重 3-计体积，默认计重2  //这里只可传 1,2  3暂且服务端未实现
 * 
*/
function saveRuleJson(startProvCode, areaJson, feeType) {
    let json = {
        feeType: feeType,
        startProvCode: startProvCode,
        rules: [{
            feeType: feeType,
            startProvCode: startProvCode,
            provIds: areaJson.proviceArr.slice(0, common.getRandomNum(0, areaJson.proviceArr.length)).join(), //common.getRandomNum(0, areaJson.proviceArr.length)
            startNum: common.getRandomNum(5.1, 10.9, 1), //起始数量，小于等于该数量作为起始运费
            startFee: common.getRandomNum(20.1, 25.9, 1),//起始运费
            addNum: common.getRandomNum(2, 5),
            addFee: common.getRandomNum(5, 10),
        }],
    };
    return json;
};


/**
 * @description 保存包邮规则  的参数
 * @param {String}  startProvCode 发货地的省份编码
 * @param {Object}  areaJson  省份对象，用来取省区列表
 * @param {Integer} feeType  计费类型，1-满件数 2-满金额
*/
function saveFreeRuleJson(startProvCode, areaJson, feeType) {
    let json = {
        freeType: feeType,
        startProvCode: startProvCode,
        rules: [{
            freeType: feeType,
            startProvCode: startProvCode,
            provIds: areaJson.proviceArr.slice(0, common.getRandomNum(0, areaJson.proviceArr.length)).join(), //common.getRandomNum(0, areaJson.proviceArr.length)
            limitNum: common.getRandomNum(10, 20)    //满足该数量包邮
        }]
    };
    return json;
};


/**
 * @description  保存运费监控接口
 * 
*/

function saveMonitorJson(typeId, provCode, shipRuleList) {

    let json = {
        typeId,
        origPos: provCode,
        targetPoses: [{
            targetPos: provCode,
            basicStart: common.getRandomNum(0, 5),
            basicEnd: common.getRandomNum(10, 15),
            stepStart: common.getRandomNum(0, 5),
            stepEnd: common.getRandomNum(10, 15),
        }]
    };

    return json;
};