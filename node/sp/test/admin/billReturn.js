const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

describe('平台退货退款', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, styleRes;

    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin();
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        clientInfo = _.cloneDeep(LOGINDATA);

        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerInfo.tenantId, queryType: 4 });
        styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchDres.result.data.dresStyleResultList[0].detailUrl });
        // console.log(`styleRes=${JSON.stringify(styleRes)}`);


        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2005'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
    });

    describe('平台帮买家申请退款', async function () {
        let purBill, purRes, backViewFull, billReturnRes;
        before('平台取消订单', async function () {
            // 买家开单并支付
            // purBill = billManage.setUpPurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            // console.log(`purRes=${JSON.stringify(purRes)}`);

            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: purRes.params.jsonParam.orders[0].main.money });

            // 平台取消订单
            await spAuth.staffLogin();
            backViewFull = await spTrade.findBillBackViewFull({ id: purRes.result.data.rows[0].billId, _tid: clientInfo.tenantId, _cid: clientInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\nbackViewFull=${JSON.stringify(backViewFull)}`);
            billReturnRes = await spTrade.saveReturnBill({ _tid: clientInfo.tenantId, _cid: clientInfo.clusterCode, ...mockJsonParam.returnBillJson(backViewFull) });
        });
        it('查询单据退货预览详情', async function () {

        });
        it('订单查询', async function () {
            // console.log(`\nbillReturnRes=${JSON.stringify(billReturnRes)}`);

        });
    });



});