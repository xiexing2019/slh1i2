const common = require('../../../lib/common');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');

describe('字典', function () {
    this.timeout(TESTCASE.timeout);

    describe('商品类别', function () {
        before(async function () {
            //管理员登录
            // await spAuth.staffLogin();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        });

        describe('查询商品类别树', function () {
            let classList;
            before(async function () {
                await spReq.spSellerLogin();
                classList = await spdresb.findClassListForSeller({
                    parentId: 1,//上级类别。查询顶层时传1或不传
                    includeSubs: true,//是否取所有下级节点
                    isGlobal: 1
                });
            });
            it('1.全局商品搜索', async function () {
                const res = await spdresb.findClassListForSeller({
                    parentId: classList.result.data.options[0].code,//上级类别。查询顶层时传1或不传
                    includeSubs: true,//是否取所有下级节点
                    isGlobal: 1
                });
                common.isApproximatelyEqualAssert(classList.result.data.options[0].items, res.result.data.options, ['deep']);
            });
            it('2.includeSubs', async function () {
                const res = await spdresb.findClassListForSeller({
                    parentId: 0,//上级类别。查询顶层时传0或不传
                    includeSubs: false,//是否取所有下级节点
                    isGlobal: 1
                });
                res.result.data.options.forEach(element => {
                    expect(element).to.not.have.keys('items');
                });
            });
            it('3.查询商品类别属性定义', async function () {
                const res = await spdresb.findByClass({ classId: classList.result.data.options[0].code });//
                // console.log(`res=${JSON.stringify(res)}\n`);
            });
            it('4.查询不存在类别-字母', async function () {
                const res = await spdresb.findByClass({ classId: 'a', check: false });
                expect(res.result).to.includes({ msg: '商品类别不存在或是无效' });
            });
            it('5.查询不存在类别-数字', async function () {
                const res = await spdresb.findByClass({ classId: 9999999, check: false });
                expect(res.result).to.includes({ msg: '商品类别不存在或是无效' });
            });
        });

        describe('店铺内商品类别树', function () {
            before(async function () {
                await spReq.spSellerLogin();
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            });
            it('parentId', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1013, showFlag: 1, incDirectSub: true });
                res.result.data.rows.forEach((row) => expect(row.parentId).to.equal(res.params.jsonParam.parentId));
            });
            it('incDirectSub=true', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1, showFlag: 1, incDirectSub: true });
                res.result.data.rows.forEach(row => expect(row).to.have.property('subItems'));
            });
            it('incDirectSub=false', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1, showFlag: 1, incDirectSub: false });
                res.result.data.rows.forEach(row => expect(row).not.to.have.property('subItems'));
            });
            it('showFlag=1', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1, showFlag: 1, incDirectSub: true });
                res.result.data.rows.forEach(row => expect([1, 3]).to.include(row.showFlag));
            });
            it('showFlag=2', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1, showFlag: 2, incDirectSub: true });
                res.result.data.rows.forEach(row => expect([2, 3]).to.include(row.showFlag));
            });
            it('showFlag=3', async function () {
                const res = await spdresb.findClassConfigList({ parentId: 1, showFlag: 3, incDirectSub: true });
                res.result.data.rows.forEach(row => expect(row.showFlag).to.equal(3));
            });
        });

    });

    /**
     * 2.2.4 品牌库校验字段从name改为title
     * http://zentao.hzdlsoft.com:6082/zentao/task-view-2699.html
     */
    describe('品牌库', function () {
        const brandProtect = {
            name: `品牌${common.getRandomStr(5)}`,
            banGroundFlag: 1,    // 1禁止上传，0允许
            banSearchFlag: 1     // 1禁止搜索，0允许
        };
        before('新增品牌库', async function () {
            await spAuth.staffLogin();
            const saveRes = await spCommon.saveBrand(brandProtect);
            brandProtect.id = saveRes.result.data.val;
        });

        it('添加相同名称的品牌', async function () {
            const repetJson = _.cloneDeep(brandProtect);
            delete repetJson.id;
            repetJson.check = false;
            const saveRes = await spCommon.saveBrand(repetJson);
            expect(saveRes.result).to.includes({ msgId: 'brand_name_repeat' });
        });

        it('查看品牌库列表', async function () {
            const brandList = await spCommon.getBrandList({ pageSize: 20, pageNo: 1, name: brandProtect.name }).then(res => res.result.data.rows);
            const brandInfo = brandList.find(val => val.id == brandProtect.id);
            expect(brandInfo).not.to.be.undefined;
            const exp = _.cloneDeep(brandProtect);
            exp.createdBy = exp.updateBy = LOGINDATA.userId;
            common.isApproximatelyEqualAssert(exp, brandInfo, [], `添加的品牌信息和查询到的不一致`);
        });

        describe('买家卖家验证品牌库', function () {

            describe('卖家禁止-买家禁止', function () {

                it('卖家新增包含此品牌库的商品，检查上架', async function () {
                    await spReq.spSellerLogin();
                    const saveDres = await spReq.saveDresFull(dresJson => dresJson.spu.title = `${brandProtect.name}`);
                    const dresSpu = await spdresb.getFullById({ id: saveDres.result.data.spuId }).then(res => res.result.data.spu);
                    expect(dresSpu.flag, '商品包含品牌库敏感词，但是依然上架成功了').to.be.equal(2);
                    expect(dresSpu.marketFailure).to.be.equal(`含有${brandProtect.name}侵权关键字`);
                });

                it('买家es搜索', async function () {
                    await spReq.spClientLogin();
                    const dresList = await spdresb.searchDres({ searchToken: brandProtect.name }).then(res => res.result.data.dresStyleResultList);
                    expect(dresList, `搜索到带有敏感词的商品`).to.have.lengthOf(0);
                });
            });

            describe('卖家禁止，买家允许', function () {

                before('修改品牌库', async function () {
                    await spAuth.staffLogin();
                    brandProtect.banSearchFlag = 0;
                    brandProtect.banGroundFlag = 1;
                    await spCommon.updateBrand(brandProtect);
                });
                it('获取品牌列表', async function () {
                    const brandList = await spCommon.getBrandList({ pageSize: 20, pageNo: 1, name: brandProtect.name }).then(res => res.result.data.rows);
                    const brandInfo = brandList.find(val => val.id == brandProtect.id);
                    expect(brandInfo).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(brandProtect, brandInfo);
                });
                it('卖家新增商品', async function () {
                    await spReq.spSellerLogin();
                    let saveDres = await spReq.saveDresFull(dresJson => dresJson.spu.title = `${brandProtect.name}`);
                    let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });
                    expect(dresInfo.result.data.spu.flag).to.be.equal(2);
                });
                it('买家查看', async function () {
                    await spReq.spClientLogin();
                    const dresList = await spdresb.searchDres({ searchToken: brandProtect.name }).then(res => res.result.data.dresStyleResultList);
                    // expect(dresList).to.have.lengthOf.above(0);
                });
            });

            describe('卖家允许,买家禁止', function () {

                before('修改品牌库', async function () {
                    await spAuth.staffLogin();
                    brandProtect.banSearchFlag = 1;
                    brandProtect.banGroundFlag = 0;
                    await spCommon.updateBrand(brandProtect);
                });

                it('卖家新增商品', async function () {
                    await spReq.spSellerLogin();
                    let saveDres = await spReq.saveDresFull(dresJson => dresJson.spu.title = `${brandProtect.name}`);
                    let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });
                    expect(dresInfo.result.data.spu.flag).to.be.equal(1);
                });

                it('买家搜索', async function () {
                    await spReq.spClientLogin();
                    let dresList = await spdresb.searchDres({ searchToken: brandProtect.name }).then(res => res.result.data.dresStyleResultList);
                    expect(dresList, `搜索到带有敏感词的商品`).to.have.lengthOf(0);
                });
            });

            describe('卖家允许，买家允许', function () {

                before('修改品牌库', async function () {
                    await spAuth.staffLogin();
                    brandProtect.banSearchFlag = 1;
                    brandProtect.banGroundFlag = 1;
                    await spCommon.updateBrand(brandProtect);
                });

                it('买家搜索', async function () {
                    await spReq.spClientLogin();
                    let dresList = await spdresb.searchDres({ searchToken: brandProtect.name }).then(res => res.result.data.dresStyleResultList);
                    // expect(dresList).to.have.lengthOf.above(0);
                });

            });
        });

        describe('品牌库不包含中文', function () {
            //这里把品牌库修改为不包含中文的名称
            before('修改品牌库', async function () {
                await spAuth.staffLogin();
                brandProtect.name = common.getRandomStr(5);
                await spCommon.updateBrand(brandProtect);
            });

            it('卖家新增商品,商品前后有其他字符', async function () {
                await spReq.spSellerLogin();
                let saveDres = await spReq.saveDresFull(dresJson => dresJson.spu.title = `款${brandProtect.name}`);
                let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });
                expect(dresInfo.result.data.spu.flag, '商品包含品牌库敏感词，但是依然上架成功了').to.be.equal(2);
            });

            it('搜索', async function () {
                await spReq.spClientLogin();
                let res = await spdresb.searchDres({ searchToken: `款${brandProtect.name}` }).then(res => res.result.data.dresStyleResultList);
                expect(res.length, '搜索的名称涉嫌侵权，es依然可以搜索').to.be.equal(0);
            })

            it('卖家新增商品，商品前后有英文字符', async function () {
                await spReq.spSellerLogin();
                let saveDres = await spReq.saveDresFull(dresJson => dresJson.spu.title = `auto${brandProtect.name}`);
                let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });
                expect(dresInfo.result.data.spu.flag, `敏感词前面有英文字符，也过滤到了了,${JSON.stringify(dresInfo)}`).to.be.equal(1);
            });

            it('搜索', async function () {
                await spReq.spClientLogin();
                let searchRes = await spdresb.searchDres({ queryType: 4, needWait: 1, searchToken: `auto${brandProtect.name}` }).then(res => res.result.data.dresStyleResultList);
                expect(searchRes.length, `存在品牌保护库${brandProtect.name},搜索商品:auto${brandProtect.name},es搜索出的数量是0`).to.be.above(0);
            });

            it('删除品牌库', async function () {
                await spAuth.staffLogin();
                await spCommon.delBrand({ id: brandProtect.id });
                let brandList = await spCommon.getBrandList({ name: brandProtect.name }).then(res => res.result.data.rows);
                let brandDetail = brandList.find(res => res.id == brandProtect.id);
                expect(brandDetail, '删除品牌之后，列表中还是可以查到这个品牌').to.be.undefined;
            });
        });
    });
});
