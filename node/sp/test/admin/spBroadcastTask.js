const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const mysql = require('../../../reqHandler/sp/mysql');
const SpBroadcastTask = require('../../help/spAdmin/spBroadcastTask');
const spReq = require('../../help/spReq');
const spmdm = require('../../../reqHandler/sp/biz_server/spmdm');
// const spugr = require('../../../reqHandler/sp/global/spugr');

// 校验增删改查消息任务
// 推送无法校验
describe('消息通知任务', function () {
    this.timeout(30000);
    before(async function () {
        await spAuth.staffLogin();
    });

    describe('全部买家/全部卖家', async function () {
        let saveRes;
        before(async function () {
            const json = mockJsonParam.spBroadcastTaskJson({ sendScopeType: 1 });
            json.startTime = '';
            // console.log(`json=${JSON.stringify(json)}`);
            saveRes = await spCommon.saveSpBroadcastTask(json);
        });
        it('消息通知任务列表校验', async function () {
            const taskListInfo = await spCommon.getSpBroadcastTaskList().then(res => {
                return res.result.data.rows.find(val => val.id == saveRes.result.data.val);
            });
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, taskListInfo);
        });

        it('查询消息通知任务信息', async function () {
            const taskMsgInfo = await spCommon.getSpBroadcastTaskMsg({ id: saveRes.result.data.val });
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, taskMsgInfo.result.data, ['tenantIdList']);
            expect(taskMsgInfo.result.data.tenantIdList.length).to.above(0);
            // const sqlRes = await getTenantIds({ sendScope: saveRes.params.jsonParam.sendScope });
            // common.isApproximatelyArrayAssert(sqlRes.tenantIds, taskMsgInfo.result.data.tenantIdList);
        });
        it('认证用户查询消息列表', async function () {

        });
        it('未认证用户查询消息列表', async function () {
            await spReq.spLogin({ code: '15821120000' });
            const list = await spmdm.pullMessagesList({ tagOneIn: '300,301,302,202' });
            console.log(`list=${JSON.stringify(list)}`);
        });
        describe.skip('修改消息通知任务', function () {
            let editRes;
            before(async function () {
                let json = mockJsonParam.spBroadcastTaskJson({ sendScopeType: 1 });
                json.id = saveRes.result.data.val;
                // console.log(`taskMsgInfo.result.data.flag=${JSON.stringify(taskMsgInfo.result.data.flag)}`);
                editRes = await spCommon.saveSpBroadcastTask(json);

            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == saveRes.result.data.val));
                common.isApproximatelyEqualAssert(editRes.params.jsonParam, taskInfo);
            });
            it('查询消息通知任务信息', async function () {
                const taskMsgInfo = await spCommon.getSpBroadcastTaskMsg({ id: saveRes.result.data.val });
                common.isApproximatelyEqualAssert(editRes.params.jsonParam, taskMsgInfo.result.data, ['tenantIdList']);
                expect(taskMsgInfo.result.data.tenantIdList.length).to.above(0);
                // const sqlRes = await getTenantIds({ sendScope: editRes.params.jsonParam.sendScope });
                // common.isApproximatelyArrayAssert(sqlRes.tenantIds, taskMsgInfo.result.data.tenantIdList);
            });
        });
        describe.skip('停用消息通知任务', function () {
            before(async function () {
                await spCommon.stopSpBroadcastTask({ id: saveRes.result.data.val });
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == saveRes.result.data.val));
                expect(taskInfo.flag, `停用后，消息通知状态错误`).to.equal(4);
            });
            it('查询消息通知任务信息', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskMsg({ id: saveRes.result.data.val });
                expect(taskInfo.result.data.flag, '停用后，消息通知状态错误').to.equal(4);
            });
        });
        describe.skip('启用消息通知任务', function () {
            before(async function () {
                await spCommon.startSpBroadcastTask({ id: saveRes.result.data.val });
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == saveRes.result.data.val));
                expect(taskInfo.flag, `启用后，消息通知状态错误,消息列表${taskInfo}`).to.equal(1);
            });
            it('查询消息通知任务信息', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskMsg({ id: saveRes.result.data.val });
                expect(taskInfo.result.data.flag, `启用后，消息通知状态错误,消息详情${taskInfo}`).to.equal(1);
            });
        });
        describe.skip('删除消息通知任务', function () {
            before(async function () {
                await spCommon.deleteSpBroadcastTask({ id: saveRes.result.data.val });
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == saveRes.result.data.val));
                expect(taskInfo, `删除后，消息通知任务列表依然显示被删除任务,任务id:${saveRes.result.data.val}`).to.be.undefined;
            });
            it('查询消息通知任务信息', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskMsg({ id: saveRes.result.data.val, check: false });
                expect(taskInfo.result, `删除后,依然能查询到被删除任务,任务id:${saveRes.result.data.val}`).to.includes({ msgId: 'comn_entity_not_exist' });
            });
        });

    });

    describe('部分买家/部分卖家', async function () {
        const spBroadcastTask = new SpBroadcastTask();
        before(async function () {
            await spAuth.staffLogin();
            await spBroadcastTask.saveTask(mockJsonParam.spBroadcastTaskJson({ sendScopeType: 2 }));
        });
        it('消息通知任务列表校验', async function () {
            const taskListInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == spBroadcastTask.id));
            spBroadcastTask.flag = taskListInfo.flag; // 状态暂不校验 直接取当前状态
            common.isApproximatelyEqualAssert(spBroadcastTask, taskListInfo);
        });
        it('查询消息通知任务信息', async function () {
            const taskMsgInfo = await spBroadcastTask.getMsg().then(res => res.result.data);
            common.isApproximatelyEqualAssert(spBroadcastTask, taskMsgInfo);
        });
        describe('修改消息通知任务', async function () {
            before(async function () {
                let json = mockJsonParam.spBroadcastTaskJson({ sendScopeType: 2 });
                json.id = spBroadcastTask.id;
                if (spBroadcastTask.methodType != 3) {
                    //可以修改成功
                    await spBroadcastTask.saveTask(json);
                } else {
                    json.check = false;
                    const editRes = await spCommon.saveSpBroadcastTask(json);
                    expect(editRes.result, `修改消息通知任务成功:${editRes}`).to.includes({ msg: '当前消息推送任务不可保存', msgId: 'spadmin_spBroadcastTask_save_error' });
                };
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == spBroadcastTask.id));
                spBroadcastTask.flag = taskInfo.flag;
                common.isApproximatelyEqualAssert(spBroadcastTask, taskInfo);
            });
            it('查询消息通知任务信息', async function () {
                const taskMsgInfo = await spBroadcastTask.getMsg().then(res => res.result.data);
                common.isApproximatelyEqualAssert(spBroadcastTask, taskMsgInfo);
            });

        });

        describe('停用', async function () {
            before(async function () {
                if (spBroadcastTask.methodType != 3) {
                    await spBroadcastTask.stopTask();
                } else {
                    let disenableRes = await spCommon.stopSpBroadcastTask({ id: spBroadcastTask.id, check: false });
                    expect(disenableRes.result, `停用消息通知出错:${disenableRes}`).to.includes({ msg: '当前消息推送任务不可暂停', msgId: 'spadmin_spBroadcastTask_stop_error' });
                };
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == spBroadcastTask.id));
                spBroadcastTask.flag = taskInfo.flag;
                common.isApproximatelyEqualAssert(spBroadcastTask, taskInfo);
            });
            it('查询消息通知任务信息', async function () {
                const taskMsgInfo = await spBroadcastTask.getMsg().then(res => res.result.data);
                common.isApproximatelyEqualAssert(spBroadcastTask, taskMsgInfo);
            });
        });
        describe('启用', async function () {
            before(async function () {
                if (spBroadcastTask.methodType != 3) {
                    await spBroadcastTask.startTask();
                } else {
                    let enableRes = await spCommon.startSpBroadcastTask({ id: spBroadcastTask.id, check: false });
                    // console.log(`enableRes=${JSON.stringify(enableRes)}`);
                    expect(enableRes.result, `启用消息通知出错:${enableRes}`).to.includes({ msg: '当前消息推送任务不可重启', msgId: 'spadmin_spBroadcastTask_start_error' });
                };
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == spBroadcastTask.id));
                spBroadcastTask.flag = taskInfo.flag;
                common.isApproximatelyEqualAssert(spBroadcastTask, taskInfo);
            });
            it('查询消息通知任务信息', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskMsg({ id: spBroadcastTask.id });
                common.isApproximatelyEqualAssert(spBroadcastTask, taskInfo);
            });
        });
        describe('删除消息通知任务', function () {
            before(async function () {
                await spCommon.deleteSpBroadcastTask({ id: spBroadcastTask.id });
            });
            it('消息通知任务列表校验', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskList().then(res => res.result.data.rows.find(val => val.id == spBroadcastTask.id));
                expect(taskInfo, `删除后，消息通知任务列表依然显示被删除任务,任务id:${spBroadcastTask.id}`).to.be.undefined;
            });
            it('查询消息通知任务信息', async function () {
                const taskInfo = await spCommon.getSpBroadcastTaskMsg({ id: spBroadcastTask.id, check: false });
                expect(taskInfo.result, `删除后,依然能查询到被删除任务,任务id:${spBroadcastTask.id}`).to.includes({ msgId: 'comn_entity_not_exist' });
            });
        });
    });
});

/**
 * 获取买家tenantIds
 * @param {object} param
 * @param {object} param.sendScope 1 全部买家，2 全部卖家 
 * 
 */
async function getTenantIds({ sendScope }) {
    const spgMysql = await mysql({ dbName: 'spgMysql' });
    let unitIds;
    if (sendScope === 1) {
        //获取买家
        [unitIds] = await spgMysql.query(`select buyer_tenant_id as unitId from spadmin.sp_buyer_shop where audit_flag=1 `);
        // console.log(`tenantIds=${JSON.stringify(tenantIds)}`);
    } else {
        //获取卖家
        [unitIds] = await spgMysql.query(`select unit_id as unitId from spugr.sp_shop where flag=1`);
        // console.log(`tenantIds=${JSON.stringify(tenantIds.length)}`);
    };
    let tenantIds = unitIds.map(ele => ele.unitId);
    spgMysql.end();
    return { tenantIds };
};

