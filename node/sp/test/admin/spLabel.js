// const spugr = require('../../../reqHandler/sp/global/spugr');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spLabelManage = require('../../help/spAdmin/spLabelManage');
const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');

describe('商品标签', async function () {
    this.timeout(30000);

    before(async function () {
        await spAuth.staffLogin();
    });

    describe('标签类型', async function () {
        const labelType = spLabelManage.setupLabelType(),
            labelItem = spLabelManage.setupLabelItem();
        before(async function () {
            await labelType.getRandom().save();
        });
        after(async function () {
            if (labelType.id) {
                await labelType.disable();
            }
        });
        it('标签类型列表', async function () {
            const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name }).then(res => res.result.data.rows);
            const info = labelTypeList.find(data => data.id == labelType.id);
            expect(info).not.to.be.undefined;
            common.isApproximatelyEqualAssert(labelType, info);
        });
        it('标签大类添加标签', async function () {
            labelItem.getRandom();
            labelItem.typeId = labelType.id;
            await labelItem.save();
        });
        describe('修改-运营可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 1;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('修改-运营不可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 6;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('修改-卖家可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 2;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('修改-卖家不可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 5;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('修改-买家可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 4;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('修改-买家不可见', async function () {
            before(async function () {
                labelType.getRandom();
                labelType.showFlag = 3;
                await labelType.save();
            });
            it('标签类型列表-可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: labelType.showFlag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
            it('标签类型列表-不可见', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, showFlag: 0 }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).to.be.undefined;
            });
        });
        describe('停用类型', async function () {
            before(async function () {
                await labelType.enable();
            });
            it('标签类型列表', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, flag: labelType.flag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
        });
        describe('启用类型', async function () {
            before(async function () {
                await labelType.disable();
            });
            it('标签类型列表', async function () {
                const labelTypeList = await spAdmin.getLableTypeList({ name: labelType.name, flag: labelType.flag }).then(res => res.result.data.rows);
                const info = labelTypeList.find(data => data.id == labelType.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(labelType, info);
            });
        });
    });

    //此标签值是用来 管理员给商品打标签用的，传的时候传的是文本，是为了方便运营人员快速选择做的功能
    describe('标签值相关', async function () {
        const labelItem = spLabelManage.setupLabelItem();
        before(async function () {
            await labelItem.getRandom().save();
        });
        after(async function () {
            if (labelItem.id) {
                await labelItem.offline();
            }
        });
        it('标签选项值列表查询', async function () {
            const labelItemList = await spAdmin.getSpLabelItemList({ typeId: labelItem.typeId, labelNameLike: labelItem.labelName }).then(res => res.result.data.rows);
            const info = labelItemList.find(val => val.id == labelItem.id);
            common.isApproximatelyEqualAssert(labelItem, info);
        });
        it('新增同名标签', async function () {
            const params = { check: false, ...labelItem };
            params.id = '';
            const res = await spAdmin.saveSpLabelItemFull(params);
            expect(res.result).to.includes({ msgId: 'sp_label_item_name_repeat' });
        });
        describe('修改标签值', async function () {
            before(async function () {
                await labelItem.getRandom().save();
            });
            it('标签选项值列表查询', async function () {
                const labelItemList = await spAdmin.getSpLabelItemList({ typeId: labelItem.typeId, labelNameLike: labelItem.labelName }).then(res => res.result.data.rows);
                const info = labelItemList.find(val => val.id == labelItem.id);
                common.isApproximatelyEqualAssert(labelItem, info);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await labelItem.offline();
            });
            it('标签选项值列表查询', async function () {
                const labelItemList = await spAdmin.getSpLabelItemList({ typeId: labelItem.typeId, labelNameLike: labelItem.labelName, flag: labelItem.flag }).then(res => res.result.data.rows);
                const info = labelItemList.find(val => val.id == labelItem.id);
                common.isApproximatelyEqualAssert(labelItem, info);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await labelItem.online();
            });
            it('标签选项值列表查询', async function () {
                const labelItemList = await spAdmin.getSpLabelItemList({ typeId: labelItem.typeId, labelNameLike: labelItem.labelName, flag: labelItem.flag }).then(res => res.result.data.rows);
                const info = labelItemList.find(val => val.id == labelItem.id);
                common.isApproximatelyEqualAssert(labelItem, info);
            });
        });
    });

    // 标准类目 关联大类标签
    describe('类别标签关系', async function () {
        let classId, labelTypeId, labelItemId;
        before(async function () {
            // 获取标准类目id
            classId = await spConfig.getDresClassList({ flag: 1, parentId: 1 }).then(res => res.result.data.rows[0].id);

            // 获取该类目下原有的类别标签关系
            const classRelationList = await spAdmin.getSpLabelClassRelationList({ classId }).then(res => res.result.data.rows);
            if (classRelationList.length == 0) {
                this.skip();
            }
            labelTypeId = classRelationList[0].type;
            // 选取同类型的有效标签值 替换
            const labelItemListValid = await spAdmin.getSpLabelItemListValid()
                .then(res => common.takeWhile(res.result.data.rows, (data) => data.type == labelTypeId).map(data => data.id));
            labelItemId = labelItemListValid.slice(0, 5);
            // labelItemId = common.takeWhile(classRelationList, (data) => data.type == labelTypeId).map(data => data.labelId);

            // 保存标签类别关系值
            await spAdmin.saveSpLabelClassRelation({ classId, labelList: [{ labelItemId, labelTypeId }] });
        });
        it('分类下标签选项值列表', async function () {
            const classRelationList = await spAdmin.getSpLabelClassRelationList({ classId }).then(res => res.result.data.rows);
            const actual = common.takeWhile(classRelationList, (data) => data.type == labelTypeId).map(data => data.labelId);
            common.isApproximatelyEqualAssert(labelItemId, actual);
        });
        it('有效标签选项值列表', async function () {
            const list = await spAdmin.getSpLabelItemListValid().then(res => res.result.data.rows);
            list.forEach(itemInfo => expect(itemInfo).to.includes({ flag: 1 }));
        });
    });

    // 标签审核功能
    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2178.html
    describe('标签审核功能--卖家店铺标签', async function () {
        let sellerInfo;
        const record = spLabelManage.setupLabelAuditRecord();
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
            // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

            await spAuth.staffLogin();
            // const recordList = await spAdmin.getLabelAuditRecordList({ mobileLike: sellerInfo.mobile });
            // console.log(recordList);

            await record.getRandom({ type: 1, auditObjId: sellerInfo.tenantId });
            // 新增审核
            await record.addRecord();
            // record.id = 37;
            // console.log(record);
        });
        it('标签审核查询', async function () {
            const data = await record.getById();
            common.isApproximatelyEqualAssert(record, data);
        });
        describe('通过审核', async function () {
            before(async function () {
                await record.auditPass();
            });
            it('标签审核查询', async function () {
                const data = await record.getById();
                common.isApproximatelyEqualAssert(record, data);
            });
            it('混合搜索名称+手机号查询有效店铺接口', async function () {
                const shopList = await spugr.findShopBySearchToken({ shopIds: sellerInfo.shopId, labels: record.auditContent[0].id }).then(res => res.result.data.rows);
                // 审批通过后 信息校验
                expect(shopList[0].labels.split(',')).to.include.members(record.auditContent.map(data => String(data.id)));
                // tags搜索校验
                shopList.forEach(shop => expect(shop.labels.split(',')).to.includes(String(record.auditContent[0].id)));
            });
        });
    });

    describe('标签审核功能--买家标记', async function () {
        let clientInfo;
        const record = spLabelManage.setupLabelAuditRecord();
        before(async function () {
            await spReq.spClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();
            const shopList = await spCommon.findBuyerShops({ auditFlag: 1, mobileLike: clientInfo.mobile }).then(res => res.result.data.rows);
            await record.getRandom({ type: 2, auditObjId: shopList[0].id });
            // 新增审核
            await record.addRecord();
        });
        it('标签审核查询', async function () {
            const data = await record.getById();
            common.isApproximatelyEqualAssert(record, data);
        });
        describe('通过审核', async function () {
            before(async function () {
                await record.auditPass();
            });
            it('标签审核查询', async function () {
                const data = await record.getById();
                common.isApproximatelyEqualAssert(record, data);
            });
            it('混合搜索名称查询买家认证店铺', async function () {
                const shopList = await spCommon.findShopLite({ tenandIds: [clientInfo.tenantId], tags: record.auditContent[0].id }).then(res => res.result.data.rows);
                // 审批通过后 信息校验
                expect(shopList[0].tags).to.includes(record.auditContent.map(data => data.id));
                // tags搜索校验
                shopList.forEach(shop => expect(shop.tags).to.includes(record.auditContent[0].id));
            });
        });
    });

});