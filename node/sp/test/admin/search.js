const spugr = require('../../../reqHandler/sp/global/spugr');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spbi = require('../../../reqHandler/sp/global/spbi');
const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const esSearchHelp = require('../../help/esSearchHelp');
const hotWordManage = require('../../help/spAdmin/hotWordManage');


describe('搜索相关', async function () {
    this.timeout(TESTCASE.timeout);

    before('用户先登录', async function () {
        await spAuth.staffLogin();
    });

    // searchNumAvg 平均匹配数无法校验
    describe('搜索历史查看列表', async function () {
        describe('店铺搜索', async function () {
            const searchToken = '好店';
            let oldShopSearchList, oldDresSearchList, newShopSearchList, newDresSearchList, searchRes;
            before(async function () {
                [oldShopSearchList, oldDresSearchList] = await Promise.all([spbi.getSpSearchList({ searchToken, typeId: 1 }),
                spbi.getSpSearchList({ searchToken, typeId: 0 })]);

                await spReq.spClientLogin();
                searchRes = await spugr.findShopBySearchToken({ searchToken, pageSize: 10 });
                // console.log(searchRes);

                await spAuth.staffLogin();
                [newShopSearchList, newDresSearchList] = await Promise.all([spbi.getSpSearchList({ searchToken, typeId: 1 }),
                spbi.getSpSearchList({ searchToken, typeId: 0 })]);
            });
            it('门店搜索历史', async function () {
                this.test.isWarn = true;
                let exp = _.cloneDeep(oldShopSearchList.result.data.rows.shift());
                exp.searchNum = searchRes.result.data.total;
                exp.searchTimes++;
                exp.searchNumMax = Math.max(exp.searchNumMax, exp.searchNum);
                exp.searchNumMin = Math.min(exp.searchNumMin, exp.searchNum);
                exp.searchDate = exp.updatedDate = searchRes.opTime;
                exp.searchToken = searchToken;
                exp.typeId = 1;
                // exp.searchPageMax = Math.max(exp.searchPageMax, Math.ceil(common.div(exp.searchNum, searchRes.params.pageSize)));
                common.isApproximatelyEqualAssert(exp, newShopSearchList.result.data.rows.shift(), ['searchNumAvg']);
            });
            it('商品搜索历史', async function () {
                expect(oldDresSearchList.result).to.eql(newDresSearchList.result);
            });
        });
        describe('商品搜索', async function () {
            const searchToken = '商品';
            let oldShopSearchList, oldDresSearchList, newShopSearchList, newDresSearchList, searchRes;
            before(async function () {
                [oldShopSearchList, oldDresSearchList] = await Promise.all([spbi.getSpSearchList({ searchToken, typeId: 1 }),
                spbi.getSpSearchList({ searchToken, typeId: 0 })]);
                // console.log(`oldDresSearchList=${JSON.stringify(oldDresSearchList)}`);

                searchRes = await spdresb.searchDres({ searchToken, pageSize: 10 });
                // console.log(searchRes.params);

                await spAuth.staffLogin();
                [newShopSearchList, newDresSearchList] = await Promise.all([spbi.getSpSearchList({ searchToken, typeId: 1 }),
                spbi.getSpSearchList({ searchToken, typeId: 0 })]);
                // console.log(`newDresSearchList=${JSON.stringify(newDresSearchList)}`);
            });
            it('门店搜索历史', async function () {
                expect(oldShopSearchList.result).to.eql(newShopSearchList.result);
            });
            it('商品搜索历史', async function () {
                this.test.isWarn = true;
                let exp = _.cloneDeep(oldDresSearchList.result.data.rows.shift());
                exp.searchNum = searchRes.result.data.count;
                exp.searchTimes++;
                exp.searchNumMax = Math.max(exp.searchNumMax, exp.searchNum);
                exp.searchNumMin = Math.min(exp.searchNumMin, exp.searchNum);
                exp.searchDate = exp.updatedDate = searchRes.opTime;
                exp.searchToken = searchToken;
                exp.typeId = 0;
                // common.isFieldsEqualAssert(exp, newDresSearchList.result.data.rows.shift(), Object.keys(exp).join(';'));
                common.isApproximatelyEqualAssert(exp, newDresSearchList.result.data.rows.shift(), ['searchNumAvg']);
            });
        });
        describe('查询条件验证', async function () {
            it('搜索次数大于等于的查询条件验证', async function () {
                let shopSearchList = await spbi.getSpSearchList({ searchTimesGte: 5 }).then(res => res.result.data.rows);
                expect(shopSearchList.every(ele => ele.searchTimes >= 5), `搜索次数有小于5的结果`).to.be.true;
            });
            it('搜索时间小于等于的查询条件验证', async function () {
                let shopSearchList = await spbi.getSpSearchList({ searchDateLte: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
                expect(shopSearchList.every(ele => ele.searchDate <= common.getDateString([0, 0, -1])), `搜索时间有大于${common.getDateString([0, 0, -1])}的结果`).to.be.true;
            });
            it('搜索时间大于等于的查询条件验证', async function () {
                let shopSearchList = await spbi.getSpSearchList({ searchDateGte: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
                expect(shopSearchList.every(ele => ele.searchDate >= common.getDateString([0, 0, -1])), `搜索时间有小于${common.getDateString([0, 0, -1])}的结果`).to.be.true;
            });
        });
    });

    describe('热词管理', async function () {
        describe('关键词管理', async function () {
            let hotWord;
            before('新增关键词', async function () {
                hotWord = hotWordManage.setUpHotWord(hotWordManage.getRandomHotWord());
                await hotWord.saveHotWords();
            });
            after(async function () {
                if (hotWord.id) {
                    await hotWord.offLine();
                }
            });
            it('查询关键词列表(平台)', async function () {
                const hotWordsList = await spAdmin.findHotWordsList({ wordNameLike: hotWord.wordName }).then(res => res.result.data.rows);
                const wordInfo = hotWordsList.find(hotWord => hotWord.id == hotWord.id);
                expect(wordInfo).not.to.be.undefined;
                common.isApproximatelyEqualAssert(hotWord, wordInfo);
            });
            it('获取关键词详情', async function () {
                const wordInfo = await spAdmin.getHotWordDetailInfo({ id: hotWord.id }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(hotWord, wordInfo);
            });
            it('新增同名关键词', async function () {
                const json = hotWordManage.getRandomHotWord();
                json.wordName = hotWord.wordName;
                json.check = false;
                const result = await spAdmin.saveHotWords(json).then(res => res.result);
                expect(result).to.include({ msg: '关键词已经存在', msgId: 'word_has_exist' });
            });
            describe('修改关键词信息', async function () {
                before('新增关键词', async function () {
                    const json = hotWordManage.getRandomHotWord();
                    await hotWord.saveHotWords(json);
                });
                it('查询关键词列表(平台)', async function () {
                    const hotWordsList = await spAdmin.findHotWordsList({ wordNameLike: hotWord.wordName }).then(res => res.result.data.rows);
                    const wordInfo = hotWordsList.find(hotWord => hotWord.id == hotWord.id);
                    expect(wordInfo, `关键词列表未找到关键词 id:${hotWord.id} wordName:${hotWord.wordName}`).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
                it('获取关键词详情', async function () {
                    const wordInfo = await spAdmin.getHotWordDetailInfo({ id: hotWord.id }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
            });
            describe('停用热词', async function () {
                before(async function () {
                    await hotWord.offLine();
                });
                it('查询关键词列表(平台)', async function () {
                    const hotWordsList = await spAdmin.findHotWordsList({ wordNameLike: hotWord.wordName, flag: 0 }).then(res => res.result.data.rows);
                    const wordInfo = hotWordsList.find(hotWord => hotWord.id == hotWord.id);
                    expect(wordInfo, `关键词列表未找到关键词 id:${hotWord.id} wordName:${hotWord.wordName}`).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
                it('获取关键词详情', async function () {
                    const wordInfo = await spAdmin.getHotWordDetailInfo({ id: hotWord.id }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
            });
            describe('启用关键词', async function () {
                before(async function () {
                    await hotWord.onLine();
                });
                it('查询关键词列表(平台)', async function () {
                    const hotWordsList = await spAdmin.findHotWordsList({ wordNameLike: hotWord.wordName, flag: 1 }).then(res => res.result.data.rows);
                    const wordInfo = hotWordsList.find(hotWord => hotWord.id == hotWord.id);
                    expect(wordInfo, `关键词列表未找到关键词 id:${hotWord.id} wordName:${hotWord.wordName}`).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
                it('获取关键词详情', async function () {
                    const wordInfo = await spAdmin.getHotWordDetailInfo({ id: hotWord.id }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(hotWord, wordInfo);
                });
            });
        });
        describe('关键词查询-online', async function () {
            it('默认排序', async function () {
                const hotWordsList = await spAdmin.findHotWordsList({ flag: 1 }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(hotWordsList, 'showOrder', true);
            });
            it('获取推荐时间平台', async function () {
                await spAdmin.getHotWordsSuggestTime();
            });
            it('获取关键词', async function () {
                const hotWordsList = await hotWordManage.getEveHotWords({ size: 10 });
                const eveHotWords = await spAdmin.findEveHotWords({ size: 10 }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(hotWordsList, eveHotWords);
            });
            // 从搜索历史中推送过来 , showOrder根据searchTimes赋值
            it.skip('热词来源-热门搜索', async function () {
                const searchList = await spbi.getSpSearchList({ typeId: 0, orderBy: 'searchTimes', orderByDesc: true, pageSize: 10 })
                    .then(res => res.result.data.rows.map(info => { return { wordName: info.searchToken } }));
                const hotWordsList = await spAdmin.findHotWordsList({ flag: 1, typeId: 0, pageSize: 10 }).then(res => res.result.data.rows);
                console.log(`searchList=${JSON.stringify(searchList)}`);
                console.log(`\nhotWordsList=${JSON.stringify(hotWordsList)}`);
                common.isApproximatelyEqualAssert(searchList, hotWordsList);
            });
        });
    });

    describe('大数据同步综合评分以及系统评分到sp', async function () {
        let sellerInfo, spuIds, scoreParams;
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();

            // 选取商品 推送评分
            spuIds = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId })
                .then(res => res.result.data.dresStyleResultList.slice(0, 3).map(dres => dres.id));
            scoreParams = spuIds.map(spuId => {
                return { spuId, colligateScore: common.getRandomNum(80, 100), systemScore: common.getRandomNum(80, 100) };
            });
            // console.log(scoreParams);
            await spbi.batchUpdateScore({ jsonParam: [{ unitId: sellerInfo.unitId, dresSpuScoreDTOs: scoreParams }] });
            await common.delay(1000);

            // 买家登录 
            await spReq.spClientLogin();
        });
        it('es全局商品搜索', async function () {
            this.retries(2);
            await common.delay(500);
            const dresList = await await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: spuIds } })
                .then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                const exp = scoreParams.find(data => data.spuId == dres.id);
                expect(dres, `spuId=${dres.id} 评分同步错误`).to.includes({ colligateScore: exp.colligateScore, systemScore: exp.systemScore });
            });
        });
        it('买家查询商品详情', async function () {
            for await (const exp of scoreParams) {
                const dresDetail = await spdresb.getFullForBuyer({ spuId: exp.spuId, buyerId: LOGINDATA.tenantId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId }).then(res => res.result.data);
                // console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
                expect(dresDetail.spu, `spuId=${exp.spuId} 评分同步错误`).to.includes({ colligateScore: exp.colligateScore, systemScore: exp.systemScore });
            }
        });
    });

    const ruleArr = [
        { goodsSort: 1, goodsSortTrans: 'pubPrice' },
        { goodsSort: 2, goodsSortTrans: 'sellNum' },
        { goodsSort: 3, goodsSortTrans: 'marketDate' },
        { goodsSort: 4, goodsSortTrans: 'showOrder' }
    ];
    describe('店铺商品排序', async function () {
        let sellerInfo;
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();
        });
        for (let index = 0; index < ruleArr.length; index++) {
            const sortExp = ruleArr[index];
            for (let sortType = 1; sortType <= 2; sortType++) {
                sortExp.sortType = sortType;
                describe(`按${sortExp.goodsSortTrans}排序  ${sortExp.sortType == 1 ? '降序' : '升序'}校验`, async function () {
                    before('web修改自定义排序规则', async function () {
                        await spAuth.staffLogin();
                        await spugr.updateWebShopSortRule({ unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId, goodsSort: sortExp.goodsSort, sortType: sortExp.sortType });
                        // console.log(`res=${JSON.stringify(res)}`); const res = 
                    });
                    it('web端查询店铺的排序', async function () {
                        const sortRule = await spugr.findWebShopWithRule({ unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId }).then(res => res.result.data);
                        // console.log(`sortRule=${JSON.stringify(sortRule)}`);
                        expect(sortRule).to.includes(sortExp);
                    });
                    it('卖家查询商品列表', async function () {
                        await spReq.spSellerLogin();
                        const spuList = await spdresb.findSellerSpuList({ pageSize: 20, pageNo: 1 }).then(res => res.result.data.rows);
                        const path = sortExp.goodsSort == 2 ? 'salesNum' : sortExp.goodsSortTrans;
                        esSearchHelp.orderAssert({ dataList: spuList, path, orderByDesc: sortExp.sortType == 1 ? true : false });
                    });
                });
            }
        }
    });

});