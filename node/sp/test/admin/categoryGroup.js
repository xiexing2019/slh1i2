const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const categoryGroupManage = require('../../help/spAdmin/categoryGroupManage');
const common = require('../../../lib/common');

// 目前版本 没有用到
describe.skip('类目集', async function () {
    this.timeout(30000);
    before(async function () {
        await spAuth.staffLogin();
    });

    describe('类目集维护', async function () {
        const categoryGroup = categoryGroupManage.setupCategoryGroup();
        before('新增类目集', async function () {
            await spAuth.staffLogin();
            await categoryGroup.getRandom().save();
            // console.log(categoryGroup);
        });
        it('类目集列表查询', async function () {
            const list = await spAuth.findSpCategoryGroupList({ nameLike: categoryGroup.name, flag: categoryGroup.flag }).then(res => {
                // console.log(`res=${JSON.stringify(res)}`);
                return res.result.data.rows;
            });
            const info = list.find(data => data.id == categoryGroup.id);
            expect(info, `未找到类目集 id=${categoryGroup.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(categoryGroup, info);
        });
        it('通过id查询类目集信息', async function () {
            const detail = await categoryGroup.getDetail().then(res => res.result.data);
            common.isApproximatelyEqualAssert(categoryGroup, detail);
        });
        describe('修改', async function () {
            before(async function () {
                await frontCategoryAction.getRandom().save();
            });
            it('类目集列表查询', async function () {
                const list = await spAuth.findSpCategoryGroupList({ nameLike: categoryGroup.name, flag: categoryGroup.flag }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == categoryGroup.id);
                expect(info, `未找到类目集 id=${categoryGroup.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(categoryGroup, info);
            });
            it('通过id查询类目集信息', async function () {
                const detail = await categoryGroup.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(categoryGroup, detail);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await categoryGroup.disable();
            });
            it('类目集列表查询', async function () {
                const list = await spAuth.findSpCategoryGroupList({ nameLike: categoryGroup.name, flag: categoryGroup.flag }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == categoryGroup.id);
                expect(info, `未找到类目集 id=${categoryGroup.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(categoryGroup, info);
            });
            it('通过id查询类目集信息', async function () {
                const detail = await categoryGroup.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(categoryGroup, detail);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await categoryGroup.enable();
            });
            it('类目集列表查询', async function () {
                const list = await spAuth.findSpCategoryGroupList({ nameLike: categoryGroup.name, flag: categoryGroup.flag }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == categoryGroup.id);
                expect(info, `未找到类目集 id=${categoryGroup.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(categoryGroup, info);
            });
            it('通过id查询类目集信息', async function () {
                const detail = await categoryGroup.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(categoryGroup, detail);
            });
        });
    });


});
