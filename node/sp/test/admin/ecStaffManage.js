
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const common = require('../../../lib/common');

describe('ec员工管理', function () {
    let saveRoleRes;
    this.timeout(30000);
    describe('角色管理', function () {

        before('新增角色', async function () {
            await spAuth.staffLogin();
            let saveJson = saveRoleJson();
            saveRoleRes = await spAuth.saveRole(saveJson);
        });

        after('数据还原', async function () {
            await spAuth.disableRole({ id: saveRoleRes.result.data.val, check: false });
        });

        it('查询角色列表', async function () {
            let roleList = await spAuth.getRoleList({ code: saveRoleRes.params.jsonParam.code }).then(res => res.result.data.rows);
            let info = roleList.find(val => val.id == saveRoleRes.result.data.val);
            if (info == undefined) {
                throw new Error(`新增了角色，但是角色列表里面没有找到这个角色`);
            } else {
                common.isApproximatelyEqualAssert(saveRoleRes.params.jsonParam, info);
            };
        });

        it('查看角色详情', async function () {
            let roleInfo = await spAuth.getRoleInfo({ id: saveRoleRes.result.data.val, wrapper: true });
            common.isApproximatelyEqualAssert(saveRoleRes.params.jsonParam, roleInfo);
        });

        it('停用角色', async function () {
            await spAuth.disableRole({ id: saveRoleRes.result.data.val });
            let roleList = await spAuth.getRoleList({ code: saveRoleRes.params.jsonParam.code, flag: 0 }).then(res => res.result.data.rows);
            let info = roleList.find(val => val.id == saveRoleRes.result.data.val);
            if (info == undefined) {
                throw new Error(`停用角色后，停用列表里面找不到这个角色`);
            } else {
                expect(info.flag, `停用后flag不显示为0`).to.be.equal(0);
            };
        });

        it('启用角色', async function () {
            await spAuth.enableRole({ id: saveRoleRes.result.data.val });
            let roleList = await spAuth.getRoleList({ code: saveRoleRes.params.jsonParam.code, flag: 1 }).then(res => res.result.data.rows);
            let info = roleList.find(val => val.id == saveRoleRes.result.data.val);
            if (info == undefined) {
                throw new Error(`启用角色后，列表里面找不到这个角色`);
            } else {
                expect(info.flag, `启用后flag不显示为1`).to.be.equal(1);
            };
        });

        describe('权限管理', function () {
            let savePowerRes;
            before('获取权限列表', async function () {
                await spAuth.staffLogin();
                let powerList = await spAuth.getPowerList({ wrapper: true, pageSize: 200 });
                let ids = common.randomSort(powerList.result.data.rows).map(val => val.id);

                savePowerRes = await spAuth.savePower({
                    roleId: saveRoleRes.result.data.val,
                    funcIds: ids.slice(0, ids.length - 5)
                });
            });

            it('获取角色权限', async function () {
                let powerInfo = await spAuth.getPowerInfo({ roleId: saveRoleRes.result.data.val }).then(res => res.result.data.rows);
                common.isApproximatelyArrayAssert(powerInfo, savePowerRes.params.jsonParam.funcIds);
            });
        });
    });

    describe('员工管理', function () {
        let saveStaffRes;
        const roleIds = 353;
        before('新增用户', async function () {
            await spAuth.enableRole({ id: roleIds, check: false });
            let saveJson = saveStaffJson({ roleIds });
            saveStaffRes = await spAuth.saveStaff(saveJson);
        });

        it('员工列表', async function () {
            let staffList = await spAuth.getStaffList({ mobile: saveStaffRes.params.jsonParam.mobile });
            let info = staffList.result.data.rows.find(val => val.id == saveStaffRes.result.data.val);
            if (info == undefined) {
                throw new Error(`新增了员工，${saveStaffRes.params.jsonParam.code},但是列表中找不到这个员工`)
            } else {
                common.isApproximatelyEqualAssert(saveStaffRes.params.jsonParam, info);
            }
        });

        it('员工详情', async function () {
            let staffInfo = await spAuth.getStaffInfo({ id: saveStaffRes.result.data.val });
            common.isApproximatelyEqualAssert(saveStaffRes.params.jsonParam, staffInfo);
        });

        it('员工登陆', async function () {
            await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code });
            common.isApproximatelyEqualAssert(saveStaffRes.params.jsonParam, LOGINDATA);
        });

        it('停用员工', async function () {
            await spAuth.staffLogin();
            await spAuth.disableStaff({ id: saveStaffRes.result.data.val });
            let staffList = await spAuth.getStaffList({ mobile: saveStaffRes.params.jsonParam.mobile });
            let info = staffList.result.data.rows.find(val => val.id == saveStaffRes.result.data.val);
            expect(info.flag).to.be.equal(0);

            let loginInfo = await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code, check: false });
            expect(loginInfo.result).to.includes({ "msgId": "authc_user_invalid" });
        });

        it('启用员工', async function () {
            await spAuth.enableStaff({ id: saveStaffRes.result.data.val });
            let staffList = await spAuth.getStaffList({ mobile: saveStaffRes.params.jsonParam.mobile });
            let info = staffList.result.data.rows.find(val => val.id == saveStaffRes.result.data.val);
            expect(info.flag).to.be.equal(1);

            await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code });
        });

        it('获取当前用户权限', async function () {
            let powerInfo = await spAuth.getPowerInfo({ roleId: roleIds }).then(res => res.result.data.rows);
            let currentInfo = await spAuth.getCurrentPower();
            common.isApproximatelyArrayAssert(powerInfo, currentInfo.result.data.rows);
        });

        it('角色绑定员工后停用角色', async function () {
            let res = await spAuth.disableRole({ id: roleIds, check: false });
            expect(res.result).to.includes({ "msgId": "in_use_role_not_allow_disable" });
        });

        it('指定角色员工列表', async function () {
            let roleCode = await spAuth.getRoleInfo({ id: roleIds }).then(res => res.result.data.code);
            let roleList = await spAuth.getStaffListByRoleCode({ roleCode });
            let info = roleList.result.data.rows.find(val => val.id == saveStaffRes.result.data.val);

            if (info == undefined) {
                throw new Error(`角色列表下面没有找到绑定此角色的员工`);
            } else {
                common.isApproximatelyEqualAssert(saveStaffRes.params.jsonParam, info);
            };
        });

        describe('修改密码', async function () {
            let staffInfo, changePwdRes;
            before('修改密码', async function () {
                // 停用启用员工后会发送mq消息踢出会话，防止时间查被误踢
                await common.delay(1000);

                await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code });
                staffInfo = _.cloneDeep(LOGINDATA);
                changePwdRes = await spAuth.changePwd({ oldPwd: '000000', newPwd: 'autotest' });
            });

            after('数据还原停用员工', async function () {
                await spAuth.staffLogin();
                await spAuth.disableStaff({ id: saveStaffRes.result.data.val });
            });

            it('旧密码登陆', async function () {
                let info = await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code, check: false });
                expect(info.result).to.includes({ "msgId": "authc_pass_error" });
            });

            it('新密码登陆', async function () {
                await spAuth.staffLogin({ code: saveStaffRes.params.jsonParam.code, pass: changePwdRes.params.jsonParam.newPwd });
                common.isApproximatelyEqualAssert(staffInfo, LOGINDATA, ['pass', 'loginTime', 'sessionId', 'lastSessionTime']);
                expect(LOGINDATA.pass).to.be.equal(changePwdRes.params.jsonParam.newPwd);
            });
        });
    });
});



function saveRoleJson(params) {
    let json = Object.assign({
        code: `auto角色${common.getRandomStr(6)}`,
        name: `auto角色${common.getRandomStr(6)}`,
        level: common.getRandomNum(5, 10),
        rem: `auto角色${common.getRandomStr(6)}`,
    }, params);
    return json;
};

function saveStaffJson(params) {
    let json = Object.assign({
        code: common.getRandomNumStr(8),
        gender: common.getRandomNum(0, 2),
        nickName: common.getRandomChineseStr(3),
        mobile: common.getRandomMobile(),
        roleIds: '',
        name: common.getRandomChineseStr(3),
        rem: `auto员工新增`,
    }, params);
    return json;
};