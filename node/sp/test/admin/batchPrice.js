const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const dresBatch = require('../../help/spConfg/dresBatch');
const config = require('../../../reqHandler/sp/global/spConfig');
const spConfg = require('../../../reqHandler/sp/global/spconfg');
const configParamManager = require('../../help/configParamManager');

/**
 * 起批量和商品售价关联  
 * http://zentao.hzdlsoft.com:6082/zentao/task-view-2317.html
 * 起批量和商品售价以及最优价相关重构
 * http://zentao.hzdlsoft.com:6082/zentao/task-view-3301.html
 * 
 * 混批规则相关
 * 调用保存业务级别系统参数接口ec-config-param-saveOwnerVal，
 * 其中是否开启混批code = sp_new_batch_flag,其中1代表启用混批，0代表不启用。
 * 其中采用价格类型code = sp_price_type,其中1代表采用统一价格2代表采用分批价格
 * 若真正开启混批即混批开启且选择分批价格且混批规则已设置则将下架所有不符合混批规则的商品
 * 反之，若混批关闭或者价格类型不采用分批价格，则将上架所有因不符合混批规则下架的商品
 * 
 */
describe('起批量', async function () {
    this.timeout(TESTCASE.timeout);
    before(async function () {
        await spAuth.staffLogin();
    });

    it.skip('', async function () {
        const dictList = await config.findDictList({ targetUnitId: 88483, typeId: 402, flag: 1 });
        console.log(`dictList=${JSON.stringify(dictList)}`);
    });

    describe('设置混批模板', async function () {
        const batchTemplate = dresBatch.setupSpBatchTemplate();
        before('新增模板', async function () {
            const template = { name: `混批模板${common.getRandomStr(5)}`, batchNum: { batchNum1: 10, batchNum2: 15 } };
            await batchTemplate.save(template);
        });
        it('平台查询混批模板', async function () {
            const template = await batchTemplate.findFromList();
            common.isApproximatelyEqualAssert(batchTemplate, template);
        });
        describe('停用模板', async function () {
            before(async function () {
                await batchTemplate.updateFlag({ flag: 0 });
            });
            it('平台查询混批模板', async function () {
                const template = await batchTemplate.findFromList();
                common.isApproximatelyEqualAssert(batchTemplate, template);
            });
        });
        describe('启用模板', async function () {
            before(async function () {
                await batchTemplate.updateFlag({ flag: 1 });
            });
            it('平台查询混批模板', async function () {
                const template = await batchTemplate.findFromList();
                common.isApproximatelyEqualAssert(batchTemplate, template);
            });
        });
        describe('删除模板', async function () {
            before(async function () {
                await batchTemplate.updateFlag({ flag: -1 });
            });
            it('平台查询混批模板', async function () {
                await batchTemplate.findFromList();
            });
        });
    });

    describe('设置混批规则', async function () {
        const salesPlan = dresBatch.setupMktSalesPlan();
        let sellerInfo;
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
            salesPlan.setUser(sellerInfo);

            await spAuth.staffLogin();
            // 启用混批
            const spNewBatchFlag = await configParamManager.getParamInfo({ domainKind: 'business', code: 'sp_new_batch_flag', ownerKind: 6, ownerId: sellerInfo.tenantId });
            await spNewBatchFlag.updateParam({ val: 1 });
            // console.log(spNewBatchFlag);

            // 采用分批价
            const spPriceType = await configParamManager.getParamInfo({ domainKind: 'business', code: 'sp_price_type', ownerKind: 6, ownerId: sellerInfo.tenantId });
            await spPriceType.updateParam({ val: 2 });
            // console.log(spPriceType);

            // const priceList = await spConfg.findSpBatchPriceList({ unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            // console.log(`priceList=${JSON.stringify(priceList)}`);

            // priceList.push(...[{ priceRate: 0.9, priceAddNum: 10, name: '价格1', priceType: 1 }, { priceRate: 0.8, priceAddNum: 10, name: '价格2', priceType: 2 }, { priceRate: 0.7, priceAddNum: 10, name: '价格3', priceType: 3 }]);
            // await batchPrice.batchSave(_.uniqBy(priceList, 'name'));
            // console.log(batchPrice);

        });
        it('查询混批价', async function () {
            const priceList = await salesPlan.findFromList();
            // common.isApproximatelyEqualAssert()
        });
    });



});