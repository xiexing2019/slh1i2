// const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
// const spbi = require('../../../reqHandler/sp/global/spbi');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const mockJsonParam = require('../../help/mockJsonParam');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const esSearchHelp = require('../../help/esSearchHelp');

describe('spadmin', function () {
    this.timeout(TESTCASE.timeout);

    before('用户先登录', async function () {
        // await spugr.spLogOut({ check: false });
        await spAuth.staffLogin();
    });

    after('将注册的手机号从白名单去除', async function () {
        await spReq.delWhite();
    });

    //删除的Banner的flag为-1，列表查询接口特殊处理--不显示已删除的Banner
    describe.skip('首页Banner', function () {
        let saveBannerRes, editBannerRes;
        before('新增Banner', async function () {
            saveBannerRes = await spCommon.saveBanner(mockJsonParam.bannerJson());
            // console.log(`saveBannerRes=${JSON.stringify(saveBannerRes)}`);
        });

        it('Banner查询', async function () {
            const findBannerList = await spCommon.findBanner({
                pageSize: 10, pageNo: 1, jsonParam: {
                    flag: 1,
                    itemId: saveBannerRes.params.jsonParam.itemId,
                    itemType: saveBannerRes.params.jsonParam.itemType,
                    bannerKind: saveBannerRes.params.jsonParam.bannerKind
                }
            });
            const info = findBannerList.result.data.rows.find(element => element.id == saveBannerRes.result.data.val);
            common.isApproximatelyEqualAssert(getBannerInfoExp(saveBannerRes.params.jsonParam), info, []);
        });

        it('修改Banner', async function () {
            let json = mockJsonParam.bannerJson();
            json.id = saveBannerRes.result.data.val;
            json.check = false;
            editBannerRes = await spCommon.saveBanner(json);
            if (editBannerRes.result.code < 0) {
                //后续流程使用editBannerRes作为参数期望值
                [saveBannerRes, editBannerRes] = [editBannerRes, saveBannerRes];
                throw new Error(`修改Banner失败:\n\t${JSON.stringfy(saveBannerRes)}`);
            };
            const info = await spCommon.findBanner({
                pageSize: 10, pageNo: 1, jsonParam: {
                    flag: 1,
                    itemId: editBannerRes.params.jsonParam.itemId,
                    itemType: editBannerRes.params.jsonParam.itemType,
                    bannerKind: editBannerRes.params.jsonParam.bannerKind
                }
            }).then(res => res.result.data.rows.find(element => element.id == editBannerRes.result.data.val));
            common.isApproximatelyEqualAssert(getBannerInfoExp(editBannerRes.params.jsonParam), info, []);
        });

        it('停用Banner', async function () {
            await spCommon.disableBanner({ id: editBannerRes.result.data.val });
            const findBannerList = await spCommon.findBanner({ pageSize: 10, pageNo: 1, jsonParam: { flag: 0, itemId: editBannerRes.params.jsonParam.itemId } });
            const info = findBannerList.result.data.rows.find(element => element.id == editBannerRes.result.data.val);
            common.isApproximatelyEqualAssert(getBannerInfoExp(editBannerRes.params.jsonParam), info, []);
            expect(info.flag, 'flag错误').to.be.equal(0);
        });

        it('启用Banner', async function () {
            await spCommon.enableBanner({ id: editBannerRes.result.data.val });
            const findBannerList = await spCommon.findBanner({ pageSize: 10, pageNo: 1, jsonParam: { flag: 1, itemId: editBannerRes.params.jsonParam.itemId } });
            const info = findBannerList.result.data.rows.find(element => element.id == editBannerRes.result.data.val);
            common.isApproximatelyEqualAssert(getBannerInfoExp(editBannerRes.params.jsonParam), info, []);
            expect(info.flag, 'flag错误').to.be.equal(1);
        });

        it('删除Banner', async function () {
            await spCommon.deleteBanner({ id: editBannerRes.result.data.val });
            const findBannerList = await spCommon.findBanner({ pageSize: 50, pageNo: 1, jsonParam: { itemId: editBannerRes.params.jsonParam.itemId } });
            const info = findBannerList.result.data.rows.find(element => element.id == editBannerRes.result.data.val);
            expect(info, '删除的Banner查询不到').to.be.undefined;
        });

        it('添加停用的Banner并且删除', async function () {
            const sfRes = await spCommon.saveBanner(mockJsonParam.bannerJson());
            //停用
            await spCommon.disableBanner({ id: sfRes.result.data.val });
            //无法准确的定位到刚刚添加的这个banner，因为itemId 几乎一致，所以有时候找不到刚添加的banner，加大
            const info = await spCommon.findBanner({ pageSize: 100, pageNo: 1, jsonParam: { flag: 0, itemId: sfRes.params.jsonParam.itemId } })
                .then(res => res.result.data.rows.find(element => element.id == sfRes.result.data.val));
            expect(info.flag, '停用后flag错误').to.equal(0);
            //删除
            await spCommon.deleteBanner({ id: info.id });
            const info1 = await spCommon.findBanner({ pageSize: 100, pageNo: 1, jsonParam: { itemId: sfRes.params.jsonParam.itemId } })
                .then(res => res.result.data.rows.find(element => element.id == sfRes.result.data.val));
            expect(info1, '删除的Banner查询不到').to.be.undefined;
        });
        //如果结束日期早于今天，Banner会自动停用，这里是直接新增了一个结束时间早于今天的，然后查看他的状态
        it('Banner结束日期验证', async function () {
            let param = mockJsonParam.bannerJson();
            param.startDate = common.getDateString([0, 0, -20])
            param.endDate = common.getDateString([0, 0, -10]);
            const sfRes = await spCommon.saveBanner(param);
            const info = await spCommon.findBanner({ pageSize: 10, pageNo: 1, jsonParam: { flag: 0, itemId: param.itemId } }).then(res => res.result.data.rows.find(element => element.id == sfRes.result.data.val));
            common.isApproximatelyEqualAssert(getBannerInfoExp(sfRes.params.jsonParam), info);
            expect(info.flag, '结束日期早于今天，没有自动停用').to.be.equal(0);
            await spCommon.deleteBanner({ id: sfRes.result.data.val });   //删除banner 以免留下数据导致banner累计过多
        });
    });

    //最后需要禁用验证码
    describe('验证码', async function () {
        let saveAuthCodeRes;
        it('保存验证码', async function () {
            await spAuth.staffLogin();
            let randomNumStr = common.getRandomNumStr(6);
            saveAuthCodeRes = await spCommon.saveAuthCode({
                id: randomNumStr,   //验证码
                rem: '验证码' + randomNumStr,
                flag: 1
            });
        });

        it('查看验证码', async function () {
            const findAuthCodesList = await spCommon.findAuthCodes({ pageSize: 10, pageNo: 1, jsonParam: { nameLike: saveAuthCodeRes.params.jsonParam.rem } });
            common.isApproximatelyEqualAssert(saveAuthCodeRes.params.jsonParam, findAuthCodesList.result.data.rows[0]);
        });

        it('修改验证码', async function () {
            let updateJson = _.cloneDeep(saveAuthCodeRes.params.jsonParam);
            updateJson.action = 'edit';
            updateJson.rem = updateJson.rem.replace('验证码', '修改后的验证码');
            saveAuthCodeRes = await spCommon.saveAuthCode(updateJson);
            let findAuthCodesList = await spCommon.findAuthCodes({ pageSize: 10, pageNo: 1, jsonParam: { nameLike: saveAuthCodeRes.params.jsonParam.id } });
            common.isApproximatelyEqualAssert(saveAuthCodeRes.params.jsonParam, findAuthCodesList.result.data.rows[0]);
        });
    });

    //目前只有一条
    //获取公司信息时也只取spadmin sp_ecool_info表中flag=1的第一条数据
    //{"flag":1,"updatedBy":0,"ecoolPhone":"400-677-0909","remark":"","updatedDate":"2018-09-26 21:09:14","ecoolMail":"support@hzdlsoft.com","createdDate":"2018-09-26 21:09:14","createdBy":0,"ecoolWeixin":"商陆花官方订阅号","name":"","id":1,"ecoolAddress":"杭州市滨江区西兴街道江陵路1916号兴祺大厦1幢16F(地铁站江陵路A出口)","ecoolLogo":"docx1537434568444157-spdoc01.png","ecoolWeb":"http://www.hzdlsoft.com/"}
    describe('公司信息管理', function () {
        let ecInfoOrign, saveEcInfoRes;
        before(async function () {
            ecInfoOrign = await spCommon.getEcoolInfo().then(res => res.result.data);
        });
        after(async function () {
            const params = format.dataFormat(ecInfoOrign, 'id;name;ecoolAddress;ecoolWeixin;ecoolPhone;ecoolMail;ecoolWeb;ecoolLogo;customPhone');
            await spCommon.updateEcoolInfo(params);
        });
        it('修改公司信息', async function () {
            const updateRes = await spCommon.updateEcoolInfo(ecInfoJson({ id: ecInfoOrign.id }));
            const ecoolInfo = await spCommon.getEcoolInfo().then(res => res.result.data);
            updateRes.params.jsonParam.customWx = updateRes.params.jsonParam.customWx.join();
            common.isApproximatelyEqualAssert(updateRes.params.jsonParam, ecoolInfo);
        });
        it('新增公司信息', async function () {
            // const params = format.dataFormat(ecInfoOrign, 'ecoolAddress;ecoolWeixin;ecoolPhone;ecoolMail;ecoolWeb;ecoolLogo');
            saveEcInfoRes = await spCommon.saveEcoolInfo(ecInfoJson());
        });
        it('删除公司信息', async function () {
            await spCommon.deleteEcoolInfo({ id: saveEcInfoRes.result.data.val });
        });
    });

    //这里就拿type1进行验证
    describe('首页背景图管理', function () {
        let maxVersion, saveRes, info, saveResBef, productVer;
        before('管理员查询背景图片列表', async function () {
            await spAuth.staffLogin();
            let res = await spCommon.getBackGroundPhotoList({ pageSize: 50, pageNo: 1 }).then(res => res.result.data.rows);
            let newShopBackGrounds = [];
            //找到符合条件的背景图片，这里就针对type=1（今日上新门店）的背景图进行操作，筛选出有效的
            res.forEach(val => {
                if (val.type == 1 && val.flag == 1) {
                    newShopBackGrounds.push(val);
                };;
            });
            //找出包含最大版本号的这个对象
            maxVersion = esSearchHelp.maxVersion(newShopBackGrounds);

            if (maxVersion == undefined) {
                saveResBef = await spCommon.saveBackGroundPhoto({ name: 'newSpuDocId', url: 'doc35376690', type: 1, productVer: '1.0.0' });
                maxVersion = saveResBef.params.jsonParam;
            };

            productVer = maxVersion.productVer.split('.');
            productVer[productVer.length - 1] = parseInt(productVer[productVer.length - 1]) + 2;;
            let productVersion = productVer.join('.');
            //添加背景图片
            saveRes = await spCommon.saveBackGroundPhoto({ name: 'newSpuDocId', url: 'doc35376690', type: 1, productVer: productVersion });
            maxVersion.url = 'doc35376690';
        });

        after('删除新增的背景图', async function () {
            if (saveResBef != undefined) await spCommon.delBackGroundPhoto({ id: saveResBef.result.data.val, deleteIsTrue: 1, check: false }); //传1 表示物理删除
            await spCommon.delBackGroundPhoto({ id: saveRes.result.data.val, deleteIsTrue: 1, check: false }); //传1 表示物理删除
        });

        // 版本号+type 作为唯一键
        it('添加相同版本号，类型的背景图', async function () {
            let repeteJson = _.cloneDeep(saveRes.params.jsonParam);
            repeteJson.check = false;
            let res = await spCommon.saveBackGroundPhoto(repeteJson);
            expect(res.result).to.includes({ "msgId": "comn_entity_duplicated" });
        });

        it('查询', async function () {
            let backGroundList = await spCommon.getBackGroundPhotoList({ pageSize: 50, pageNo: 1 }).then(res => res.result.data.rows);
            info = backGroundList.find(val => val.id == saveRes.result.data.val); //在列表里面找刚刚添加的这条记录
            if (info == undefined) {
                throw new Error(`新增了背景图，但是列表中找不到这条记录`);
            } else {
                common.isApproximatelyEqualAssert(saveRes.params.jsonParam, info);
            };
        });

        it('版本号检查-相同版本号', async function () {
            let res = await spdresb.findHomePageData({ productVersion: saveRes.params.jsonParam.productVer });
            expect(res.result.data.newShopDocId).to.be.equal(saveRes.params.jsonParam.url);
        });

        it('版本号检查-大于版本号', async function () {
            let productVer = saveRes.params.jsonParam.productVer.split('.');
            productVer[productVer.length - 1] = parseInt(productVer[productVer.length - 1]) + 2;
            let productVersion = productVer.join('.');

            let res = await spdresb.findHomePageData({ productVersion });
            expect(res.result.data.newShopDocId).to.be.equal(saveRes.params.jsonParam.url);
        });

        it('版本号检查-两个版本之间', async function () {
            productVer[productVer.length - 1] = parseInt(productVer[productVer.length - 1]) + 1;
            let productVersion = productVer.join('.');
            let res = await spdresb.findHomePageData({ productVersion });

            expect(res.result.data.newShopDocId, `查询的版本号位于两个之间，但是没有取到低版本对应的图片`).to.be.equal(maxVersion.url);
        });

        it('修改', async function () {
            let updateJson = _.cloneDeep(info);
            updateJson.linkType = 2;
            updateJson.jumpLink = { url: 'https://spchk.hzecool.com/web/#/login' };
            updateJson.contentType = 1;
            let updateRes = await spCommon.updateBackGroundPhoto(updateJson);

            let backGroundList = await spCommon.getBackGroundPhotoList({ pageSize: 50, pageNo: 1 }).then(res => res.result.data.rows);
            info = backGroundList.find(val => val.id == saveRes.result.data.val);
            common.isApproximatelyEqualAssert(updateRes.params.jsonParam, info);
        });
    });

    describe('推荐位配置', async function () {
        let dresInfo;
        before(async function () {
            const dres = await spdresb.searchDres({ queryType: 4, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.dresStyleResultList.shift());
            dresInfo = await spReq.getFullForBuyerByUrl({ detailUrl: dres.detailUrl }).then(res => res.result.data);
            // console.log(`dresInfo=${JSON.stringify(dresInfo)}`);
        });
        describe('推荐商品', async function () {
            let recommendInfo, saveRes;
            before(async function () {
                recommendInfo = getRandomRecommend(dresInfo.spu);
                saveRes = await spCommon.saveRecommend(recommendInfo);
            });
            it('获取推荐位列表', async function () {
                const recommendList = await spCommon.getRecommendList(recommendInfo).then(res => res.result.data.rows);
                const actual = recommendList.find(val => val.id = saveRes.result.data.val);
                common.isApproximatelyEqualAssert(recommendInfo, actual);
            });
            describe('修改推荐位信息', async function () {
                before(async function () {
                    recommendInfo = getRandomRecommend(dresInfo.spu);
                    recommendInfo.id = saveRes.result.data.val;
                    saveRes = await spCommon.saveRecommend(recommendInfo);
                });
                it('获取推荐位列表', async function () {
                    const recommendList = await spCommon.getRecommendList(recommendInfo).then(res => res.result.data.rows);
                    const actual = recommendList.find(val => val.id = saveRes.result.data.val);
                    common.isApproximatelyEqualAssert(recommendInfo, actual);
                });
            });
        });
    });


    describe.skip('隐私权限', async function () {
        describe('查看电话', async function () {
            let remainTimes = 0, code = 'checkMobile';
            before(async function () {
                await spAuth.staffLogin();
                console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);


                const rolePrivacySet = await spCommon.getRolePrivacySetByBizKeys({ roleId })
                console.log(`rolePrivacySet=${JSON.stringify(rolePrivacySet)}`);

            });
            it('查看隐私', async function () {
                const res = await spCommon.getPrivacyTimes({ code });
                console.log(`res=${JSON.stringify(res)}`);

                const res2 = await spCommon.operatePrivate({ code });
                console.log(`res=${JSON.stringify(res2)}`);
            });
            it('', async function () {
                const res2 = await spCommon.setPrivacyOpTimesAuthCode({ authCode: 'test' });
                console.log(`\nres2=${JSON.stringify(res2)}`);

                const res = await spCommon.increasePrivateTimes({ code, authCode: 'test' });
                console.log(`res=${JSON.stringify(res)}`);
            });
            it('查看隐私', async function () {
                const res = await spCommon.operatePrivate({ code });
                console.log(`res=${JSON.stringify(res)}`);
            });
            it('', async function () {

            });
        });

    });

});

/**
 * 公司信息维护json
 * @param {object} params 
 */
function ecInfoJson(params) {
    return Object.assign({
        name: `${common.getRandomChineseStr(5)}公司`,
        ecoolAddress: `杭州市滨江区${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`,
        ecoolWeixin: `${common.getRandomChineseStr(3)}公共订阅号`,
        ecoolPhone: common.getRandomMobile(),
        ecoolMail: `${common.getRandomStr(8)}@hzdlsoft.com`,
        ecoolWeb: `http://www.${common.getRandomStr(5)}.com`,
        ecoolLogo: `${common.getRandomStr(10)}.jpg`,
        customPhone: common.getRandomMobile(),
        customWx: [`weixin_${common.getRandomStr(4)}`, `weixin_${common.getRandomStr(4)}`],
    }, params);
};

function getBannerInfoExp(params) {
    let json = _.cloneDeep(params);
    json.jumpLink = { param: json.jumpLink, contentType: json.contentType };
    return json;
};

/**
 * 获取随机资源位信息
 * @param {*} params 
 */
function getRandomRecommend(params) {
    const json = {
        masterClassId: params.masterClassId,//主营类目code_value
        cityCode: params.shipOriginId,//相应城市code_value
        spuId: params.id,//商品ID，banner ID
        spuTitle: params.title,//商品标题
        spuName: params.name,//商品名称
        showPage: common.getRandomNum(10, 20),//展示的页码
        showPosition: common.getRandomNum(10, 30),//每页中展示的位置
        spuFlag: params.flag,//商品是否上架，1：上架；0：未上架
        recommendFlag: 0,//是否推荐，1：已推荐：0：未推荐
        recommendType: 1,//推荐类型： 1 商品， 2 banner，默认为1
    };
    return json;
};

