
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const common = require('../../../lib/common');
const fs = require('fs');
const path = require('path')
const caps = require('../../../data/caps');
const spReq = require('../../help/spReq');
const format = require('../../../data/format');

describe('Banner相关', function () {
    this.timeout(30000);
    describe('Banner图片库', function () {
        let saveRes, updateRes;
        before('新增banner到图片库', async function () {
            await spAuth.staffLogin();
            saveRes = await spCommon.saveBannerInPool(bannerJson());   //新增一个Banner
        });

        after('数据还原', async function () {
            if (saveRes.result) {
                await spCommon.delBannerPool({ id: saveRes.result.data.val, check: false });
            }
        });

        it('查询Banner列表', async function () {
            let res = await spCommon.getBannerPoolList({ name: saveRes.params.jsonParam.name, flag: 1, type: saveRes.params.jsonParam.type });

            let info = res.result.data.rows.find(val => val.id == saveRes.result.data.val); //找添加的这条记录
            if (info == undefined) {
                throw new Error(`列表里面没有${saveRes.params.jsonParam.name}这个Banner`);
            } else {
                common.isApproximatelyEqualAssert(info, saveRes.params.jsonParam);
            };
        });

        it('查询Banner详情', async function () {
            let res = await spCommon.getBannerPoolInfo({ id: saveRes.result.data.val });
            common.isApproximatelyEqualAssert(res.result.data, saveRes.params.jsonParam);
        });

        it('修改Bannr', async function () {
            let updateJson = bannerJson();
            updateJson.id = saveRes.result.data.val;
            updateRes = await spCommon.saveBannerInPool(updateJson);
            let res = await spCommon.getBannerPoolList({ name: updateRes.params.jsonParam.name, flag: 1 });
            let info = res.result.data.rows.find(val => val.id == saveRes.result.data.val);
            common.isApproximatelyEqualAssert(updateRes.params.jsonParam, info)
        });

        describe('轮播图', function () {
            let saveBannerRes;
            before('保存轮播图', async function () {
                let json = saveJson();
                json.bannerInfoId = saveRes.result.data.val;
                saveBannerRes = await spCommon.saveBanner(json);
            });

            after('还原', async function () {
                await spCommon.deleteBanner({ id: saveBannerRes.result.data.val, check: false });
            });

            it('轮播图查询', async function () {
                let res = await spCommon.findBanner({ flag: 1, carouselName: saveBannerRes.params.jsonParam.carouselName });
                let info = res.result.data.rows.find(val => val.id == saveBannerRes.result.data.val);
                let exp = _.cloneDeep(saveBannerRes.params.jsonParam);
                exp.itemId = saveRes.params.jsonParam.coverDoc;

                if (info == undefined) {
                    throw new Error(`新增了轮播图，但是列表中找不到此轮播图,name:${saveBannerRes.params.jsonParam.carouselName}`);
                } else {
                    common.isApproximatelyEqualAssert(saveBannerRes.params.jsonParam, info);
                };
            });

            it('买家轮播图查询', async function () {
                await spReq.spClientLogin();
                let res = await spCommon.findBannerByKind({ bannerKind: 0, pageSize: 50, pageNo: 1 }).then(res => res.result.data.rows);
                let info = res.find(val => val.id == saveBannerRes.result.data.val);
                if (info == undefined) {
                    throw new Error(`买家单找不到新增的轮播图`);
                } else {
                    let results = Object.assign({}, saveBannerRes.params.jsonParam, updateRes.params.jsonParam);
                    let exp = format.dataFormat(results, 'bannerInfoId;bannerKind;bannerName=name;carouselName;flag;itemId=coverDoc;itemSeq;itemType;linkType');
                    common.isApproximatelyEqualAssert(exp, info);
                };
            });

            it('停用轮播图', async function () {
                await spAuth.staffLogin();
                await spCommon.disableBanner({ id: saveBannerRes.result.data.val });
                let res = await spCommon.findBanner({ flag: 0, carouselName: saveBannerRes.params.jsonParam.carouselName });
                let info = res.result.data.rows.find(val => val.id == saveBannerRes.result.data.val);
                if (info == undefined) {
                    throw new Error(`停用轮播图后，在停用列表里面没有找到轮播图`);
                } else {
                    expect(info.flag, `停用轮播图后，flag为改变`).to.be.equal(0);
                };
            });

            it('启用轮播图', async function () {
                await spCommon.enableBanner({ id: saveBannerRes.result.data.val });
                let res = await spCommon.findBanner({ flag: 1, carouselName: saveBannerRes.params.jsonParam.carouselName });
                let info = res.result.data.rows.find(val => val.id == saveBannerRes.result.data.val);
                if (info == undefined) {
                    throw new Error(`启用轮播图后，在列表里面没有找到轮播图`);
                } else {
                    expect(info.flag, `停用轮播图后，flag为改变`).to.be.equal(1);
                };
            });

            it('修改轮播图,结束时间早于今天，应该自动停用', async function () {
                let updateJson = saveJson();
                updateJson.bannerInfoId = saveRes.result.data.val;
                updateJson.endDate = common.getDateString([0, 0, -5]);
                updateJson.id = saveBannerRes.result.data.val;
                saveBannerRes = await spCommon.saveBanner(updateJson);
                let res = await spCommon.findBanner({ flag: 0, carouselName: saveBannerRes.params.jsonParam.carouselName });
                let info = res.result.data.rows.find(val => val.id == saveBannerRes.result.data.val);
                if (info == undefined) {
                    throw new Error(`过期了的banner没有自动禁用,name:${saveBannerRes.params.jsonParam.carouselName}`);
                } else {
                    expect(info.flag).to.be.equal(0);
                };
            });

            it('删除轮播图', async function () {
                await spCommon.deleteBanner({ id: saveBannerRes.result.data.val });
                let res = await spCommon.findBanner({ flag: 0, carouselName: saveBannerRes.params.jsonParam.carouselName });
                let info = res.result.data.rows.find(val => val.id == saveBannerRes.result.data.val);
                expect(info, `删除横幅之后，还可以找到此横幅`).to.be.undefined;
            });

            it('删除Banner', async function () {
                await spCommon.delBannerPool({ id: saveRes.result.data.val });
                let res = await spCommon.getBannerPoolList({ name: saveRes.params.jsonParam.name, flag: 1, type: saveRes.params.jsonParam.type });
                let info = res.result.data.rows.find(val => val.id == saveRes.result.data.val); //找添加的这条记录
                expect(info, `删除banner之后，依然能够找到这个Banner`).to.be.undefined;
            });
        });


    });

});

/**
 * 首页配置
 * 目前为临时方案 后续还会进行改动
 * 
 * http://c.hzdlsoft.com:7082/Wiki.jsp?page=Sp-pageFragment
 * templateId 现在没有模板 暂时写死传3
 */
describe('首页配置-活动', async function () {
    this.timeout(30000);
    /**配置信息 用于结束后还原*/
    let pageFragment = {}, _content;
    before('设置首页活动配置方案', async function () {
        await spAuth.staffLogin();

        try {
            pageFragment = await spConfig.findPageFragment({ homePageVersion: 0 }).then(res => res.result.data);
            pageFragment.content = JSON.parse(pageFragment.content);

            if (!pageFragment.id) {
                console.warn(`\t未读取到首页配置方案详情,跳过本用例集`);
                this.skip();
            }
        } catch (error) {
            this.skip();
        }

        _content = _.cloneDeep(pageFragment.content);
        _content.type = ['searchbar', 'swiper'].find(val => val != _content.type);
        await spConfig.savePageFragment({ typeId: 6, content: _content });
    });
    after('还原配置', async function () {
        if (pageFragment.id) {
            await spConfig.savePageFragment({ typeId: 6, content: pageFragment.content });
        }
    });
    it('查询首页配置方案详情 平台端', async function () {
        const detail = await spConfig.getPageFragmentById({ id: pageFragment.id }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(_content, JSON.parse(detail.content));
    });
    it('查询首页配置方案详情 买家端', async function () {
        await spReq.spClientLogin();
        const detail = await spConfig.findPageFragment({ homePageVersion: 0 }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(_content, JSON.parse(detail.content));
    });
});

function bannerJson(params) {
    const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];

    let json = Object.assign({
        name: `autoBanner${common.getRandomStr(5)}`,
        type: common.getRandomNum(0, 2), //使用场景1,首页,2上新
        coverDoc: docData.image.banner[0].docId, //封面图地址
        flag: 1,  //状态：0-无效 1-有效(不传，默认为有效)
        linkType: 1,  //链接类型，1代表外链，2代表内链
        jumpLinkMap: { url: 'https://spchk.hzecool.com/web/#/login' },
        remark: `banner${common.getRandomStr(5)}`
    }, params);
    return json;
};

function saveJson(params) {
    let json = Object.assign({
        itemType: 0,
        itemSeq: common.getRandomNum(5, 7),
        startDate: common.getDateString([0, 0, -10]),
        endDate: common.getDateString([0, 0, 2]),
        bannerKind: 0,
        carouselName: `auto轮播图${common.getRandomStr(3)}`
    }, params);
    return json;
};