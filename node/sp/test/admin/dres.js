const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const common = require('../../../lib/common');
const esSearchHelp = require('../../help/esSearchHelp');
const configParam = require('../../help/configParamManager');
const dresManage = require('../../help/dresManage');
const dresSpuUnusual = require('../../help/spAdmin/dresSpuUnusual');

describe('平台端商品管理', function () {
    this.timeout(30000);
    let sellerInfo;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = LOGINDATA;
    });

    describe('好店列表配置', function () {
        let setSpusRes, spusInfo;
        before('管理员给好店列表配置商品', async function () {
            await spReq.spClientLogin();
            let searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, orderBy: 'slhDate', orderByDesc: true, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            let spus = searchRes.slice(0, 5).map((res, index) => {
                return { spuId: res.id, spuTitle: res.title, spuDocId: JSON.parse(res.docHeader)[0].docId, showOrder: 10 - index };
            });
            await spAuth.staffLogin();
            setSpusRes = await spCommon.setGoodShopSpus({ sellerUnitId: sellerInfo.unitId, platSpus: spus });
        });

        it('管理员查询好店列表配置', async function () {
            spusInfo = await spCommon.getGoodShopSpus({ sellerUnitId: sellerInfo.unitId }).then(res => res.result.data.rows);
            common.isApproximatelyArrayAssert(setSpusRes.params.jsonParam.platSpus, spusInfo);
        });

        it('排序', async function () {
            esSearchHelp.orderAssert({ dataList: spusInfo, path: 'showOrder', orderByDesc: false });   //从小到大
        });
    });

    // 第一，列表新增原价、改价，原价显示商陆花中的价格。若运营进行改价，则在改价一列显示改后价格。
    // 第二，改价操作。点击修改弹出的页面新增原价与改价标签。原价不能修改，改价可以填写与修改。
    // 若运营进行改价操作，不会改变商陆花里的价格，。
    // 改价后，买家端显示改价后价格。运营点击恢复原价，则改价价格消失，又恢复原价 卖家修改价格不影响es
    describe('平台改价', async function () {
        let editCapacity, dres = dresManage.setupDres();
        before(async function () {
            await spAuth.staffLogin();

            // 货品是否可以由平台改价, 0、不可以，1、可以, 服务端默认不可以   关闭则修改无效
            editCapacity = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_spu_money_plat_edit_capacity' });
            // 若不可以,先改为可以,执行完后还原
            await editCapacity.updateParam({ val: 1 });

            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            dres.setByEs(dresList[0]);

            // 改价
            await dres.updateSpuClassByAdmin({ changeMoney: common.sub(dres.spu.pubPrice, 50) });
        });
        after(async function () {
            await editCapacity.returnOriginalParam();
        });
        it('全局商品搜索', async function () {
            this.retries(2);
            await common.delay(1000);
            const esDresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: [dres.id] } }).then(res => res.result.data.dresStyleResultList);
            common.isApproximatelyArrayAssert(dres.spu, esDresList[0]);
        });
        it('买家查看商品', async function () {
            const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
        it('卖家查看商品', async function () {
            await spReq.spSellerLogin();
            const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
        // 管理员修改商品的价格，只能改低，不能改高
        // web端控制 吴坚伟那现象比较奇怪 可以正常修改 spPlatPrice得到更新，但是根据价格取低价原则 不会影响到正常使用
        it.skip('管理员改高价格', async function () {
            await spAuth.staffLogin();
            const res = await spdresb.updateSpuClassByAdmin({ changeMoney: common.add(dres.spu.pubPrice, 100), _cid: dres.spu.clusterCode, unitId: dres.spu.unitId, tenantId: dres.spu.tenantId, id: dres.spu.id, check: false });
            expect(res.result, JSON.stringify(res)).to.includes({ "msgId": "the_change_money_can_not_less_than_slh" });
        });

        describe('重置改价为原价', async function () {
            before(async function () {
                await spAuth.staffLogin();
                await dres.updateSpuClassByAdmin({ resetPriceFlag: 1 });
            });
            it('全局商品搜索', async function () {
                const esDresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: [dres.id] } }).then(res => res.result.data.dresStyleResultList);
                common.isApproximatelyArrayAssert(dres.spu, esDresList.shift(), ['updatedDate']);
            });
            it('买家查看商品', async function () {
                const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
                common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            });
            it('卖家查看商品', async function () {
                await spReq.spSellerLogin();
                const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            });
        });
    });

    describe('平台修改划线价', async function () {
        const dres = dresManage.setupDres();
        before(async function () {
            await spAuth.staffLogin();

            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            dres.setByEs(dresList[0]);

            // 改价  markingPrice
            await dres.updateSpuClassByAdmin({ markingPrice: common.add(dres.spu.pubPrice, common.getRandomNum(50, 100)) });
        });
        it('全局商品搜索', async function () {
            this.retries(2);
            await common.delay(1000);
            const esDres = await dres.searchDresById();
            common.isApproximatelyArrayAssert(dres.spu, esDres);
        });
        it('买家查看商品', async function () {
            const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
        it('卖家查看商品', async function () {
            await spReq.spSellerLogin();
            const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
        describe('清空划线价', async function () {
            before(async function () {
                await spAuth.staffLogin();
                await dres.updateSpuClassByAdmin({ markingPrice: null });
            });
            it('全局商品搜索', async function () {
                const esDres = await dres.searchDresById();
                common.isApproximatelyArrayAssert(dres.spu, esDres, ['updatedDate']);
            });
            it('买家查看商品', async function () {
                const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
                common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            });
            it('卖家查看商品', async function () {
                await spReq.spSellerLogin();
                const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
                common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            });
        });
    });

    describe('平台修改商品类别', async function () {
        const dres = dresManage.setupDres();
        before(async function () {
            await spAuth.staffLogin();
            // 字典 季节
            const seasonList = await spugr.getDictList({ typeId: 2034, flag: 1 }).then((res) => res.result.data.rows);

            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            dres.setByEs(dresList[0]);
            await dres.updateSpuClassByAdmin({ name: `${dres.spu.name}edit`, platSeason: common.randomSort(seasonList)[0].codeValue });

            await common.delay(500);
        });
        it('全局商品搜索', async function () {
            const esDresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: [dres.id] } }).then(res => res.result.data.dresStyleResultList);
            common.isApproximatelyArrayAssert(dres.spu, esDresList[0]);
        });
        it('全局商品搜索-mutilVeluesKey-platSeanon', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, platSeanon: dres.spu.platSeason })
                .then(res => {
                    // console.log(`res=${JSON.stringify(res)}`);
                    return res.result.data.dresStyleResultList;
                });
            dresList.forEach(data => expect(data.platSeason).to.includes(String(dres.spu.platSeason)));
        });
        it('买家查看商品', async function () {
            const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
        it('卖家查看商品', async function () {
            await spReq.spSellerLogin();
            const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
        });
    });

    /**
     * 推送异常商品数据 -> 买家隐藏 hideFlag=2
     * 
     * 买家隐藏规则 好店APP、小程序等不再显示该商品，效果同商品下架，但不同步下架信息给卖家，该商品在卖家端还是显示为上架状态
     * 
     * 异常价格商品推送  人工审核流程已去除 只保留自动处理
     */
    describe('异常价格商品管理', async function () {
        const dresUnusual = new dresSpuUnusual.DresSpuUnusualManager(),
            dres1 = dresManage.setupDres();//, dres2 = dresManage.setupDres()
        before(async function () {
            await spReq.spClientLogin();
            // keyWords: { id: [71254] }
            const dresList = await spdresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId }).then(res => {
                // console.log(res);
                return res.result.data.dresStyleResultList;
            });
            dres1.setByEs(dresList[0]);
            const dresDetail = await dres1.getDetailForBuyer().then(res => res.result.data);
            if (dresDetail.spu.flag != 1) {
                throw new Error(`es搜索查询到非上架状态商品 spuId=${dres1.id} flag=${dresDetail.spu.flag}`);
            }

            await spAuth.staffLogin();
            dresUnusual.addDres(dresList.slice(0, 2));

            // dres2.setByEs(dresList[1]);
        });
        describe('自动处理', async function () {
            before(async function () {
                dresUnusual.changeDres({ spuId: dres1.id, pushFlag: 1 });
                console.log(`spuId=${dres1.id}`);
                await dresUnusual.saveUnusualPriceDres({ spuIds: [dres1.id] });
                await common.delay(500);
            });
            it('获取异常价格商品列表', async function () {
                const dresSpu = await dresUnusual.findSpuUnusualList({ spuId: dres1.id });
                // console.log(`dresSpu=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyArrayAssert(dresUnusual.spus.get(dres1.id), dresSpu);
            });
            it('es搜索商品', async function () {
                await spReq.spClientLogin();

                const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres1.id] } }).then(res => res.result.data.dresStyleResultList);
                expect(dresList.length, `es搜索还能搜索到异常价格商品 spuId=${dres1.id}`).to.equal(0);
            });
            it('查询商品详情', async function () {
                const dresDetail = await dres1.getDetailForBuyer().then(res => res.result.data);
                expect(dresDetail.spu).to.includes({ flag: 3, hideFlag: 2 });
            });
        });
        describe.skip('人工确认', async function () {
            before(async function () {
                await spAuth.staffLogin();

                dresUnusual.changeDres({ spuId: dres1.id, pushFlag: 0 });
                await dresUnusual.saveUnusualPriceDres({ spuIds: [dres2.id] });
            });
            it('获取异常价格商品列表', async function () {
                const dresSpu = await dresUnusual.findSpuUnusualList({ spuId: dres2.id });
                common.isApproximatelyArrayAssert(dresUnusual.spus.get(dres2.id), dresSpu);
            });
            it('es搜索商品', async function () {
                await spReq.spClientLogin();

                const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres1.id] } });
                console.log(`dresList=${JSON.stringify(dresList)}`);

            });
            it('查询商品详情', async function () {
                const dresDetail = await dres1.getDetailForBuyer().then(res => res.result.data);
                expect(dresDetail.spu).to.includes({ flag: 1, hideFlag: 0 });
            });
            describe('人工下架异常价格商品', async function () {
                before(async function () {
                    await spAuth.staffLogin();

                    await dresUnusual.offMarket({ spuIds: [dres2.id] });
                });
                it('获取异常价格商品列表', async function () {
                    const dresSpu = await dresUnusual.findSpuUnusualList({ spuId: dres2.id });
                    common.isApproximatelyArrayAssert(dresUnusual.spus.get(dres2.id), dresSpu);
                });
                it('es搜索商品', async function () {
                    await spReq.spClientLogin();

                    const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres1.id] } });
                    console.log(`dresList=${JSON.stringify(dresList)}`);
                });
                it('查询商品详情', async function () {
                    const dresDetail = await dres1.getDetailForBuyer().then(res => res.result.data);
                    expect(dresDetail.spu).to.includes({ flag: 1, hideFlag: 1 });
                });
            });
        });
    });

    // 目前 只能给flga=1的商品打分
    describe('商品打分', async function () {
        const spuIds = [], markSpuIds = [];
        before(async function () {
            await spAuth.staffLogin();

            // 选取商品用于打分
            const dresList = await spdresb.searchDres({ pageSize: 5, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                spuIds.push(dres.id);
                if (dres.flag == 1) markSpuIds.push(dres.id);
            });
        });
        describe('平台批量给商品打分', async function () {
            let platScore = common.getRandomNum(80, 100), dresList;
            before(async function () {
                await spdresb.batchMarkSpu({ tenantId: sellerInfo.tenantId, unitId: sellerInfo.unitId, platScore, spuIds });
                await common.delay(500);// 批量打分有延迟
            });
            // 使用keyWords.id 缩小搜索范围 否则商品过多，影响匹配
            it('es搜索-评分后，商品platScore校验', async function () {
                this.retries(2);
                dresList = await spdresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId, platScoreStart: platScore - 1, platScoreEnd: platScore + 1, keyWords: { id: spuIds } })
                    .then(res => res.result.data.dresStyleResultList);
                const actSpuIds = dresList.map(dres => dres.id);
                expect(actSpuIds).to.includes.members(markSpuIds);
            });
            it('es搜索-platScoreStart/platScoreEnd校验', async function () {
                dresList.forEach(dres => expect(dres.platScore).to.be.within(platScore - 1, platScore + 1));
            });
        });
    });

    // 动销数据推送存储 -> 买家隐藏 hideFlag=2
    describe('动销数据推送', async function () {
        let dresList;
        before(async function () {
            await spAuth.staffLogin();
            // 找3个上架时间最早的商品 2个立即下架，1个异常嫌疑
            dresList = await spdresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, pageNo: 1, pageSize: 2, orderBy: 'marketDate' })
                .then(res => res.result.data.dresStyleResultList.map(dres => {
                    return {
                        pushFlag: 1,
                        pushPhone: sellerInfo.mobile,
                        pushTenantId: sellerInfo.tenantId,
                        pushTenantName: dres.tenantName,
                        spuCode: dres.code,
                        spuId: dres.id,
                        spuName: dres.name,
                        sureFlag: 0,
                        typeId: 1
                    };
                }));
            if (dresList.length == 0) this.skip();
            dresList[0].sureFlag = 1;
            await spAdmin.offLineDynamicSell({ dresSpuUnusualList: dresList });
            // console.log(res); const res = 
            await common.delay(500);
        });
        it.skip('参数校验', async function () {

        });
        it('es商品状态校验', async function () {
            const dresListAfter = await spdresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, keyWords: { id: dresList.map(dres => dres.spuId) } })
                .then(res => res.result.data.dresStyleResultList);
            expect(dresListAfter).to.have.lengthOf(0);// 1
            // expect(dresListAfter[0].id).to.equal(dresList[0].spuId);
        });
        it('卖家校验商品状态', async function () {
            await spReq.spSellerLogin();
            for await (const dres of dresList) {
                const dresFull = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
                expect(dresFull.spu, `dres.spuId=${dres.spuId} pushFlag=${dres.pushFlag}`).to.includes({ flag: 1, hideFlag: dres.pushFlag == 0 ? 1 : 2, hideFrom: 1 });
            };
        });
    });

});