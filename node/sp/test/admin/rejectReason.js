const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spRejectReason = require('../../help/spAdmin/spRejectReason');

describe('驳回理由管理', async function () {
    this.timeout(TESTCASE.timeout);

    before(async function () {
        await spAuth.staffLogin();
    });

    it('驳回理由列表 排序校验', async function () {

    });

    describe('驳回理由维护', async function () {
        const rejectReason = spRejectReason.setupRejectReason();
        before('新增搜索词', async function () {
            await rejectReason.getRandom().save();
        });
        it('列表查询', async function () {
            const detail = await rejectReason.getFromList();
            common.isApproximatelyEqualAssert(rejectReason, detail);
        });
        describe('修改', async function () {
            before(async function () {
                await rejectReason.getRandom().save();
            });
            it('列表查询', async function () {
                const detail = await rejectReason.getFromList();
                common.isApproximatelyEqualAssert(rejectReason, detail);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await rejectReason.disable();
            });
            it('列表查询', async function () {
                const detail = await rejectReason.getFromList();
                common.isApproximatelyEqualAssert(rejectReason, detail);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await rejectReason.enable();
            });
            it('列表查询', async function () {
                const detail = await rejectReason.getFromList();
                common.isApproximatelyEqualAssert(rejectReason, detail);
            });
        });
        describe('删除', async function () {
            before(async function () {
                await rejectReason.deleteReason();
            });
            it('列表查询', async function () {
                it('列表查询', async function () {
                    const detail = await rejectReason.getFromList();
                    common.isApproximatelyEqualAssert(rejectReason, detail);
                });
            });
        });
    });


});