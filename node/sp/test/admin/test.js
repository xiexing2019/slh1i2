const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spReq = require('../../help/spReq');
const spCaps = require('../../../reqHandler/sp/spCaps');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

describe.skip('简单冒泡', function () {
    this.timeout(30000);

    it('获取买家城市', async () => {
        //买家登录
        await spReq.spClientLogin();
        const authRes = await spCommon.findShopAuditsByBuyer().then(res => res.result.data);
        // console.log(`authRes=${JSON.stringify(authRes)}`);
        //区域化配置
        let AreaConfigResArr = await spCommon.findCatConfig({ type: 5 }).then(res => res.result.data[5]);
        let cityCode;
        for (let i = 0; i < AreaConfigResArr.length; i++) {
            if (AreaConfigResArr[i].typeValue == authRes.cityCode || AreaConfigResArr[i].relateCityCodes.includes(authRes.cityCode)) {
                cityCode = AreaConfigResArr[i].typeValue;
                break;
            };
        };
        let cityConfigRes = await spCommon.findCatConfig({ type: 6 }).then(res => res.result.data[6].find(obj => obj.typeValue == authRes.masterClassId && obj.mainCityCode == cityCode));
        expect(cityConfigRes, '推荐城市查询查不到当前登录买家的相关城市').to.not.be.undefined;
        let exp = [{ [`${cityConfigRes.mainCityCode}`]: cityConfigRes.mainCityName }];
        cityConfigRes.relateCityCodes.forEach((ele, index) => {
            exp.push({ [`${ele}`]: cityConfigRes.relateCityNames[index] });
        });
        // console.log(`exp=${JSON.stringify(exp)}`);
        //获取买家城市及推荐城市
        let res = await spdresb.getRecommendCity();
        common.isApproximatelyEqualAssert(exp, res.result.data.rows);
    });

    it('订单数量统计', async () => {
        await spReq.spSellerLogin();
        let billCountForSeller = await spTrade.findBillsCountForSeller();
        // expect().to.
        console.log(`billCount=${JSON.stringify(billCount)}`);
        await spReq.spClientLogin();
        let billCountForCilent = await spTrade.findBillsCountForClient();
    });
    it('买家撤消退货退款申请', async () => {
        let purBillInfo, returnBill;
        //买家登录
        BASICDATA['2005'] = await spugr.getDictList({ typeId: '2005', flag: 1 })
            .then((res) => res.result.data.rows);
        await spReq.spClientLogin();
        const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'id', orderByDesc: true, statusType: 4 });

        //为了避免选择已经发生过退款操作的单子进行退款操作
        for (let i = 0; i < purBillList.result.data.rows.length; i++) {
            if (purBillList.result.data.rows[i].bill.backFlag == 0) {
                purBillInfo = purBillList.result.data.rows[i];
                break;
            };
        };
        returnBill = await spTrade.saveReturnBill(mockJsonParam.returnBillJson(purBillInfo));
        let findBillBackViewFullInfo = await spTrade.findBillBackViewFull({ id: returnBill.result.data.val });
        console.log(`findBillBackViewFullInfo=${JSON.stringify(findBillBackViewFullInfo)}`);

        let cancelReturnBill = await spTrade.cancelReturn({ id: returnBill.result.data.val });
        console.log(`cancelReturnBill=${JSON.stringify(cancelReturnBill)}`);


    });
    it('混合搜索名称查询买家认证店铺接口', async () => {
        //管理员登录
        await spAuth.staffLogin();
        let res = await spCommon.findShopLite({ searchToken: '买家店铺' });
        expect(res.result.data.rows.length, `平台端混合搜索名称查询买家认证店铺错误:${JSON.stringify(res)}`).to.above(0);
    });
    it('首页Banner查询', async () => {
        await spAuth.staffLogin();
        let findBannerByKindList = await spCommon.findBannerByKind({
            pageSize: 10, pageNo: 1, jsonParam: {
                flag: 1,
            },
        });
        expect(findBannerByKindList.result.data.rows.length, `首页Banner查询长度小于0:${JSON.stringify(findBannerByKindList)}`).to.above(0);
    });
    it('分类配置分页查询', async () => {
        let res = await spCommon.catConfigList({ pageNo: 1, pageSize: 2, type: 1 });
        expect(res.result.data.rows.length, `分类配置分页查询有误:${JSON.stringify(res)}`).to.equal(2);
    });
    it('查询商品类别配置列表', async () => {
        let res = await spdresb.dresClassConfigList({ pageNo: 1, pageSize: 2, });
        // console.log(`res.result.data.rows.length=${JSON.stringify(res.result.data.rows)}`);
        expect(res.result.data.rows.length).to.equal(2);
    });
    it.skip('资源位保存接口', async () => {
        let res = await spCommon.saveRecommend();
        console.log(`res=${JSON.stringify(res)}`);
    });
    it('获取推荐位列表接口', async () => {
        let res = await spCommon.getRecommendList({ pageNo: 1, pageSize: 2, });
        console.log(`res.result.data.rows.length=${JSON.stringify(res.result.data.rows)}`);
        expect(res.result.data.rows.length).to.equal(2);
    });
    it('买家提醒卖家上架商品', async () => {
        await spReq.spClientLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        let res = await spdresb.remindSeller({ buyerId: LOGINDATA.tenantId });
        // console.log(`res.result.data.rows.length=${JSON.stringify(res.result.data.rows)}`);
        // expect(res.result.data.rows.length).to.equal(2);

    });

    describe('清分', function () {
        before(async () => {
            await spReq.spSellerLogin();
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        });
        it('查询清分提现申请信息', async () => {
            let res = await spTrade.findLiquidation({ unitId: LOGINDATA.unitId });
            expect(res.result.data.rows.length, `查询清分提现申请信息长度小于0`).to.above(0);
        });
        it('处理清分提现申请', async () => {
            let res = await spTrade.dealLiquidation({ unitId: LOGINDATA.unitId });
            console.log(`res=${JSON.stringify(res)}`);
        });
        it.skip('重新清分提现申请', async () => {
            let res = await spTrade.againDealLiquidation({ unitId: LOGINDATA.unitId, tenantId: LOGINDATA.tenantId, billNos: '' });
            console.log(`res=${JSON.stringify(res)}`);
        });
    });

});