const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spReq = require('../../help/spReq');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const configParam = require('../../help/configParamManager');
const getExp = require('../../help/getExp');

describe('勋章管理', function () {
    this.timeout(30000);
    let sellerTenantId;
    before(async function () {
        //卖家登录
        await spReq.spSellerLogin();
        //获取卖家店铺id
        sellerTenantId = LOGINDATA.tenantId;
    });

    // 修复数据用 
    // 正常流程不包含
    it.skip('批量更新勋章信息到es', async function () {
        await spAuth.staffLogin();
        await spCommon.batcShopMedalListtoEs();
    });

    //手动授予勋章流程
    describe('新增勋章', function () {
        let sfMedal, medalListByAdmin, medalListBySeller;
        before(async () => {
            //管理员登录
            await spAuth.staffLogin();
            sfMedal = await spCommon.saveMedalFull(mockJsonParam.saveMedalJson());
            // console.log(`sfMedal=${JSON.stringify(sfMedal)}`);
            medalListByAdmin = await spCommon.getMedalList({ code: sfMedal.params.jsonParam.code });
            // console.log(`medalListByAdmin=${JSON.stringify(medalListByAdmin.result.data.rows[0])}`);
            //卖家登录，获取卖家勋章列表
            await spReq.spSellerLogin();
            medalListBySeller = await spCommon.getSellerMedalList({
                id: sellerTenantId
            });
            // console.log(`medalListBySeller=${JSON.stringify(medalListBySeller)}`);
        });
        after(async () => {
            await spAuth.staffLogin();
            let medal = await spCommon.getMedalList({ code: sfMedal.params.jsonParam.code }).then(res => res.result.data.rows.find(obj => obj.id = sfMedal.result.data.val));
            if (medal) {
                await spCommon.deleteMedal({ id: sfMedal.result.data.val });
                await spAuth.staffLogout();
            };

        });
        it('勋章列表验证', async function () {
            let exp = getExp.medalExp(sfMedal.params.jsonParam);
            exp.id = sfMedal.result.data.val;
            exp.flag = 1;
            exp.showFlag = 0;
            // console.log(`exp=${JSON.stringify(exp)}`);
            // console.log(`新增勋章列表查询=${JSON.stringify(medalListByAdmin.result.data.rows[0])}`);
            common.isApproximatelyEqualAssert(exp, medalListByAdmin.result.data.rows[0]);
        });
        it('卖家勋章列表验证', async function () {
            medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
            expect(medalListForNew.length, '卖家勋章列表中新增的勋章数不为1').to.equal(1);
            let exp = getExp.medalExp(sfMedal.params.jsonParam);
            exp.showFlag = 0;
            // console.log(`卖家exp=${JSON.stringify(exp)}`);
            // console.log(`新增勋章卖家列表查询=${JSON.stringify(medalListForNew)}`);
            common.isApproximatelyEqualAssert(exp, medalListForNew);
        });
        it('买家推荐下拉列表', async function () {
            await spReq.spClientLogin();
            const medalConfig = await spCommon.getFilterConfigData({ filterType: 1, }).then(res => {
                return res.result.data.rows.find(obj => obj.typeValue == 2);
            });
            const medalInfo = medalConfig.filterDatas.find(obj => obj.codeValue == sfMedal.params.jsonParam.code);
            expect(medalInfo, `新增勋章时默认showFlag为0，推荐下拉列表查询到新增的勋章`).to.be.undefined;
        });
        describe('授予勋章', function () {
            before(async () => {
                //授予勋章,管理员登录
                await spAuth.staffLogin();
                await spCommon.setMedal({ id: sellerTenantId, code: sfMedal.params.jsonParam.code, showFlag: 1, endDate: common.getDateString([0, 3, 0]) });
                //卖家登录，获取卖家勋章列表
                await spReq.spSellerLogin();
                medalListBySeller = await spCommon.getSellerMedalList({
                    id: sellerTenantId
                });
                // console.log(`授予勋章后卖家列表=${JSON.stringify(medalListBySeller)}`);
            });
            it('授予勋章后，卖家勋章列表查询', async function () {
                let medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
                expect(medalListForNew.length, '授予勋章后卖家勋章列表中勋章数不为1').to.equal(1);
                expect(medalListForNew[0].showFlag, '授予勋章后卖家勋章列表中勋章的showFlag不为1').to.equal(1);
            });
        });
        for (let i = 1; i < 4; i++) {
            describe('修改勋章', function () {
                let updateInfo;
                before(async () => {
                    await spAuth.staffLogin();
                    await common.delay(1000);
                    updateInfo = {
                        id: sfMedal.result.data.val,
                        code: sfMedal.params.jsonParam.code,//勋章code不允许修改
                        name: '修改' + common.getRandomChineseStr(4),
                        logoDoc: 'doc34670298',//测试环境
                        // ruleKind:,//规则类型(101准勋章；102快勋章；103新勋章；104多勋章)
                        triggerType: 1,
                        triggerPeriod: 0,//触发周期(0表示实时；1表示日；7表示周；30表示月)
                        // showOrder:,//显示顺序
                        showFlag: i,
                        rem: '修改后勋章备注' + common.getRandomStr(8),//
                        ver: i,//乐观锁
                    };
                    sfMedal = await spCommon.saveMedalFull(updateInfo);
                    medalListByAdmin = await spCommon.getMedalList({ code: sfMedal.params.jsonParam.code });
                    // console.log(`medalListByAdmin=${JSON.stringify(medalListByAdmin)}`);
                    //卖家登录，获取卖家勋章列表
                    await spReq.spSellerLogin();
                    medalListBySeller = await spCommon.getSellerMedalList({
                        id: sellerTenantId
                    });
                    // console.log(`medalListBySeller=${JSON.stringify(medalListBySeller)}`);
                });
                it('勋章列表验证', async function () {
                    let exp = getExp.medalExp(updateInfo);
                    exp.flag = 1;
                    exp.showFlag = i;
                    // console.log(`修改后列表查询Exp=${JSON.stringify(exp)}`);
                    // console.log(`修改后列表查询=${JSON.stringify(medalListByAdmin)}`);
                    common.isApproximatelyEqualAssert(exp, medalListByAdmin.result.data.rows[0], ['ver']);
                });
                it('卖家勋章列表验证', async function () {
                    medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == updateInfo.code);
                    expect(medalListForNew.length, '卖家勋章列表中新增的勋章数不为1').to.equal(1);
                    let exp = getExp.medalExp(updateInfo);
                    exp.showFlag = 1;
                    // console.log(`修改后卖家exp=${JSON.stringify(exp)}`);
                    // console.log(`修改后卖家列表=${JSON.stringify(medalListForNew)}`);
                    common.isApproximatelyEqualAssert(exp, medalListForNew);
                });

                it('买家上新推荐下拉列表', async function () {
                    await spReq.spClientLogin();
                    await common.delay(2000);
                    const medalConfig = await spCommon.getFilterConfigData({ filterType: 1, }).then(res => res.result.data.rows.find(obj => obj.typeValue == 2));
                    const medalInfo = medalConfig.filterDatas.find(obj => obj.codeValue == sfMedal.params.jsonParam.code);
                    // console.log(`\n买家medalInfo=${JSON.stringify(medalInfo)}`);
                    await common.delay(1000);
                    if (i == 1) {
                        expect(medalInfo, `修改勋章showFlag为1，买家推荐下拉列表查到该勋章`).to.be.undefined;
                    } else {
                        expect(medalInfo, `修改勋章showFlag为${i}，推荐下拉列表查不到该勋章`).to.not.be.undefined;
                        common.isApproximatelyEqualAssert({ codeName: updateInfo.name, codeValue: updateInfo.code }, medalInfo);
                    };
                });
                it.skip('修改勋章code-异常流', async function () {
                    this.test.isWarn = true;//测试环境已经加了限制，审核环境未发布
                    let res = await spCommon.saveMedalFull(mockJsonParam.saveMedalJson({ id: sfMedal.result.data.val, code: 'edit' + common.getRandomStr(6), check: false }));
                    expect(res.result, `允许修改勋章code${res}`).to.includes({ msgId: 'the_medal_code_not_edit', msg: '勋章编码不能修改' });
                });

            });
        };

        describe('解除授予', function () {
            before(async () => {
                //授予勋章,管理员登录
                await spAuth.staffLogin();
                await spCommon.setMedal({ id: sellerTenantId, code: sfMedal.params.jsonParam.code, showFlag: 0, endDate: common.getDateString([0, 3, 0]) });
                //卖家登录，获取卖家勋章列表
                await spReq.spSellerLogin();
                medalListBySeller = await spCommon.getSellerMedalList({
                    id: sellerTenantId
                });
                // console.log(`授予勋章后卖家列表=${JSON.stringify(medalListBySeller)}`);
            });
            it('解除授予勋章后，卖家勋章列表查询', async function () {
                let medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
                expect(medalListForNew.length, '解除授予勋章后卖家勋章列表中勋章数不为1').to.equal(1);
                expect(medalListForNew[0].showFlag, '解除授予勋章后，卖家勋章列表中勋章的showFlag不为0').to.equal(0);
            });
        })
        describe.skip('停用勋章', function () {
            before(async () => {
                //管理员登录
                await spAuth.staffLogin();
                let res = await spCommon.disableMedal({ id: sfMedal.result.data.val });
                console.log(`res=${JSON.stringify(res)}`);
                medalListByAdmin = await spCommon.getMedalList({ id: sfMedal.result.data.val });

                await spReq.spSellerLogin();
                medalListBySeller = await spCommon.getSellerMedalList({
                    id: sellerTenantId
                });
                console.log(`停用勋章后卖家列表=${JSON.stringify(medalListBySeller)}`);
            });
            it('停用勋章后,勋章列表查询', async function () {
                console.log(`medalListByAdmin=${JSON.stringify(medalListByAdmin)}`);
                expect(medalListByAdmin.result.data.rows[0].flag, '停用勋章后，勋章列表中勋章flag不为0').to.equal(0);
            });
            it('授予勋章后，停用勋章后,卖家勋章列表查询', async function () {
                let medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
                console.log(`medalListForNew=${JSON.stringify(medalListForNew)}`);
                expect(medalListForNew.length, '授予的勋章被停用后，卖家勋章列表中勋章showFlag不为0').to.equal(0);
            });
        });
        describe.skip('启用勋章', function () {
            before(async () => {
                //管理员登录
                await spAuth.staffLogin();
                await spCommon.enableMedal({ id: sfMedal.result.data.val });
                medalListByAdmin = await spCommon.getMedalList({ id: sfMedal.result.data.val });

                await spReq.spSellerLogin();
                medalListBySeller = await spCommon.getSellerMedalList({
                    id: sellerTenantId
                });
            });
            after(async () => {
                //重新授予勋章
                await spAuth.staffLogin();
                await spCommon.setMedal({ id: sellerTenantId, code: sfMedal.params.jsonParam.code, showFlag: 1 });
            });
            it('启用勋章后，勋章列表查询', async function () {
                expect(medalListByAdmin.result.data.rows[0].flag, '停用勋章后，勋章列表中勋章flag不为1').to.equal(1);
            });
            it('启用勋章后，卖家勋章列表查询', async function () {
                let medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
                expect(medalListForNew[0].showFlag, '启用勋章，卖家勋章列表中勋章showFlag不为0').to.equal(0);
            })
        });
        describe('删除勋章', function () {
            before(async () => {
                await spAuth.staffLogin();
                await spCommon.deleteMedal({ id: sfMedal.result.data.val });
                medalListByAdmin = await spCommon.getMedalList({ code: sfMedal.params.jsonParam.code });
                // console.log(`删除勋章勋章列表=${JSON.stringify(medalListByAdmin)}`);
                await spReq.spSellerLogin();
                medalListBySeller = await spCommon.getSellerMedalList({
                    id: sellerTenantId
                });
                // console.log(`删除勋章后卖家列表=${JSON.stringify(medalListBySeller)}`);
            });
            it('删除勋章后，勋章列表查询', async function () {
                expect(medalListByAdmin.result.data.rows.length, '删除勋章后，勋章列表仍然可以存在这个勋章').to.equal(0);
            });
            it('删除勋章后，卖家勋章列表查询', async function () {
                let medalListForNew = common.takeWhile(medalListBySeller.result.data.rows, obj => obj.code == sfMedal.params.jsonParam.code);
                // console.log(`medalListForNew=${JSON.stringify(medalListForNew)}`);
                expect(medalListForNew.length, '删除勋章后卖家勋章列表仍然可以查询到勋章').to.equal(0);
            });
            it('推荐下拉列表', async function () {
                await spReq.spClientLogin();
                const medalConfig = await spCommon.getFilterConfigData({ filterType: 1, }).then(res => res.result.data.rows.find(obj => obj.typeValue == 2));
                const medalInfo = medalConfig.filterDatas.find(obj => obj.codeValue == sfMedal.params.jsonParam.code);
                expect(medalInfo, `管理员删除勋章后，推荐下拉列表查询到删除的勋章${JSON.stringify(medalInfo)}`).to.be.undefined;
            });

        });
    });

    // 线上测试用 保证买家端与平台端一致即可
    describe('勋章查看-online', async function () {
        it('买家端勋章介绍界面数据与平台端一致', async function () {
            await spAuth.staffLogin();
            const medalListByAdmin = await spCommon.getMedalList().then(res => res.result.data.rows);

            await spReq.spClientLogin();
            const medalListByClient = await spCommon.getMedalList().then(res => res.result.data.rows);
            common.isApproximatelyEqualAssert(medalListByAdmin, medalListByClient);
        });
    });

    // 自动授予勋章
    // 准和新,快
    const taskNames = ['sp_dres_medal_task', 'sp_sales_medal_task'];
    describe.skip('勋章定时任务', function () {
        let spMedalSetExecte;
        before(async function () {
            await spAuth.staffLogin();
            spMedalSetExecte = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_medal_set_execte' });
            await spMedalSetExecte.updateParam({ val: 1 });

            await spReq.spSellerLogin();
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        });
        after(async function () {
            await spMedalSetExecte.returnOriginalParam();
        });
        for (let index = 0; index < taskNames.length; index++) {
            it.skip('获取卖家勋章列表', async function () {
                const list = await getShowMedalList();
                console.log(`list=${JSON.stringify(list)}`);
                console.log(list.length);
                console.log('\n\n');

            });
            it('授予勋章', async function () {
                await executeTask({ taskCode: taskNames[index] });
            });
            it('卖家勋章列表校验', async function () {
                const list = await getShowMedalList();
                console.log(`list=${JSON.stringify(list)}`);
                console.log(list.length);
                // expect(list.result).to
            });
        };
    });

});

async function executeTask({ taskCode }) {
    const urlDir = taskCode == 'sp_dres_medal_task' ? 'http://172.81.240.156:8081/spdresb' : 'http://172.81.241.15:8081/spsales';
    const res = await spReq.executeTask({ urlDir, taskCode });
    console.log(`res=${JSON.stringify(res)}`);
};

async function getShowMedalList() {
    const medalList = await spCommon.getSellerMedalList({ id: LOGINDATA.tenantId }).then(res => res.result.data.rows);
    console.log(`medalList=${JSON.stringify(medalList)}`);
    return common.takeWhile(medalList, data => data.showFlag == 1);
};