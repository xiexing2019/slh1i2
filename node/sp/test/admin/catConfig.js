const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
// const spReq = require('../../help/spReq');
// const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');

describe('分类配置', function () {
    this.timeout(30000);
    before(async function () {
        //管理员登录
        await spAuth.staffLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    describe('城市', function () {
        let city = new mockJsonParam.CatConfig();
        let json = {
            type: 1,
            typeId: 850,
            provCode: 320000,
            cityCode: 320500,
            typeValue: 320500,
            typeName: `苏州市`,
            showFlag: 3,
            showOrder: common.getRandomNum(10, 100),
            prop: { alias: '苏' }
        };
        // { "hobbyFlag": 1, "alias": "苏",   "typeValue": 320500, "typeName": "苏州市" }

        before('保存城市', async function () {
            await delCatConf(1, '苏州市');

            await city.saveCatConfig(json);
        });

        it('1.重复保存城市', async function () {
            json.check = false;
            const res = await spCommon.saveCatConfig(json);
            expect(res.result).to.includes({ "msgId": "comn_entity_duplicated" });
        });

        it('2.城市查询', async function () {
            const res = await spCommon.findCatConfig({ type: city.type });// typeParentId: '.001.002.'
            const actual = res.result.data['1'].find((element) => element.id == city.id);
            expect(actual, `分类配置查询 未查询到数据 type=${city.type} id=${city.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(city.getInfo(), actual);
        });

        it('3.删除城市', async function () {
            await spAuth.staffLogin();
            await spCommon.delCatConfig({ id: city.id });
            const res = await spCommon.findCatConfig({ type: city.type });
            const actual = res.result.data['1'].find((element) => element.id == city.id);
            expect(actual).to.be.undefined;
        });
    });

    describe('风格', function () {
        let saveRes;

        before(async function () {
            await delCatConf(2, '中国画风');
        });

        //中心区 confc
        it('1.保存风格', async function () {
            saveRes = await spCommon.saveCatConfig({
                type: 2,
                typeId: 2004,
                typeValue: 1,
                typeName: `中国画风`,
                showFlag: 3,
                showOrder: common.getRandomNum(10, 100)
            });
        });

        it('2.重复保存风格', async function () {
            saveRes.params.check = false;
            const res = await spCommon.saveCatConfig(saveRes.params);
            expect(res.result).to.includes({ "msgId": "comn_entity_duplicated" });
        });

        //reqFlag 是否按推荐市场排序，只在市场查询时加此有值的参数,还未验证  排序
        it('3.风格查询', async function () {
            const res = await spCommon.findCatConfig({ type: 2, reqFlag: 1, pageSize: 20, pageNo: 1 });// typeParentId: '.001.002.'
            const actual = res.result.data['2'].find((element) => element.id == saveRes.result.data.val);
            // console.log(`actual=${JSON.stringify(actual)}`);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, actual);
        });

        it('4.风格删除', async function () {
            await spCommon.delCatConfig({ id: saveRes.result.data.val });
            const res = await spCommon.findCatConfig({ type: 2, reqFlag: 1, pageSize: 20, pageNo: 1 });
            const actual = res.result.data['2'].find((element) => element.id == saveRes.result.data.val);
            expect(actual).to.be.undefined;
        });


    });

    //唯一索引  type+typeId+typeValue+typeAccode
    describe('市场', function () {
        let saveRes;

        before(async function () {
            await delCatConf(3, '银泰');
        });

        it('1.保存市场', async function () {
            saveRes = await spCommon.saveCatConfig({
                type: 3,
                typeId: 9,
                typeValue: '',
                typeName: '银泰',
                showFlag: 3,
                showOrder: common.getRandomNum(10, 100)
            });
        });

        it('2.重复保存市场', async function () {
            saveRes.params.check = false;
            const res = await spCommon.saveCatConfig(saveRes.params);
            expect(res.result).to.includes({ "msgId": "comn_entity_duplicated" });
        });
        it('3.市场查询', async function () {
            const res = await spCommon.findCatConfig({ type: 3, reqFlag: 1, pageSize: 20, pageNo: 1 });// typeParentId: '.001.002.'
            const actual = res.result.data['3'].find((element) => element.id == saveRes.result.data.val);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, actual);
        });

        it('4.市场删除', async function () {
            await spCommon.delCatConfig({ id: saveRes.result.data.val });
            const res = await spCommon.findCatConfig({ type: 3, reqFlag: 1, pageSize: 20, pageNo: 1 });
            const actual = res.result.data['3'].find((element) => element.id == saveRes.result.data.val);
            expect(actual).to.be.undefined;
        });
    });

    describe('类目', function () {
        let saveRes;

        before(async function () {
            await delCatConf(4, '小西装');
        });

        it('1.保存类目', async function () {
            saveRes = await spCommon.saveCatConfig({
                type: 4,
                typeId: 1057,
                typeValue: '',
                typeName: '小西装',
                showFlag: 3,
                showOrder: common.getRandomNum(10, 100)
            });
        });

        it('2.重复保存类目', async function () {
            saveRes.params.check = false;
            const res = await spCommon.saveCatConfig(saveRes.params);
            expect(res.result).to.includes({ "msgId": "comn_entity_duplicated" });
        })

        it('3.类目查询', async function () {
            const res = await spCommon.findCatConfig({ type: 4, reqFlag: 1, pageSize: 20, pageNo: 1 });// typeParentId: '.001.002.'
            const actual = res.result.data['4'].find((element) => element.id == saveRes.result.data.val);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, actual);
        });

        it('4.类目删除', async function () {
            await spCommon.delCatConfig({ id: saveRes.result.data.val });
            const res = await spCommon.findCatConfig({ type: 4, reqFlag: 1, pageSize: 20, pageNo: 1 });
            const actual = res.result.data['4'].find((element) => element.id == saveRes.result.data.val);
            expect(actual).to.be.undefined;
        });
    });

    /**
     * 类别标签商圈功能
     * http://jira.hzdlsoft.com:7082/browse/SP-4661
     * 
     * 分类配置查询 只显示关联市场的商圈
     */
    describe('商圈', async function () {
        const trade = new mockJsonParam.CatConfigTrade(),
            market = new mockJsonParam.CatConfig();;
        before(async function () {
            await trade.saveCatConfig({
                type: 9,
                typeId: 0,
                typeName: `商圈${common.getRandomStr(5)}`,
                showFlag: 3,
                showOrder: common.getRandomNum(10, 100)
            });
        });
        after(async function () {
            // await delCatConf(3, '银泰');
            await market.delCatConfig();
        });
        it('商圈列表查询', async function () {
            const tradeList = await spCommon.getCatConfigTradeList({ typeName: trade.typeName, showFlag: trade.showFlag, flag: trade.flag });
            const tradeInfo = tradeList.result.data.rows.shift();
            expect(tradeInfo, `商圈列表查询未找到新增的商圈信息 id=${trade.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(trade, tradeInfo);
        });
        describe('关联市场与商圈', async function () {
            before(async function () {
                await delCatConf(3, '银泰');
                await market.saveCatConfig({
                    type: 3,
                    typeId: 9,
                    typeName: `银泰`,
                    showFlag: 3,
                    showOrder: common.getRandomNum(10, 100)
                });

                //关联市场与商圈
                await trade.relateMarketAndTrade({ market: market.getInfo() });
            });
            it('查询商圈信息', async function () {
                const tradeList = await spCommon.getCatConfigTradeList({ typeName: trade.typeName, showFlag: trade.showFlag, flag: trade.flag });
                const tradeInfo = tradeList.result.data.rows.shift();
                expect(tradeInfo, `商圈列表查询未找到新增的商圈信息 id=${trade.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(trade.getInfo(), tradeInfo);
            });
            it('分类配置查询', async function () {
                const res = await spCommon.findCatConfig({ type: trade.type, orderBy: 'updatedDate', orderByDesc: true });
                const tradeInfo = res.result.data[trade.type].find(row => row.id == trade.id);
                common.isApproximatelyEqualAssert(trade.getInfo(), tradeInfo);
            });
            it('查询市场信息', async function () {
                const res = await spCommon.findCatConfig({ type: market.type, reqFlag: 1, pageSize: 20, pageNo: 1 });
                const actual = res.result.data[market.type].find((element) => element.id == market.id);
                expect(actual).to.includes({ tradeName: trade.typeName });
            });
        });
        //删除商圈会解除绑定市场
        describe('删除商圈', async function () {
            before(async function () {
                await trade.delCatConfig();
            });
            it('商圈列表', async function () {
                const tradeList = await spCommon.getCatConfigTradeList({ typeName: trade.typeName, showFlag: trade.showFlag, flag: trade.flag });
                const tradeInfo = tradeList.result.data.rows.shift();
                expect(tradeInfo, `商圈列表依然显示被删除数据 id=${trade.id}`).to.be.undefined;
            });
            it('市场信息', async function () {
                const res = await spCommon.findCatConfig({ type: market.type, reqFlag: 1, pageSize: 20, pageNo: 1 });
                const actual = res.result.data[market.type].find((element) => element.id == market.id);
                expect(actual).not.to.includes({ tradeName: trade.typeName });
            });
        });
    });

    describe('新主营类目', function () {
        const standardClass = new mockJsonParam.CatConfig();
        before('新增配置', async function () {
            await spAuth.staffLogin();
            const dresClassList = await spConfig.getDresClassList({ flag: 1, parentId: 1 })
                .then(res => res.result.data.rows);
            const dresClass = _.last(dresClassList);
            const json = { type: 13, typeId: dresClass.id, typeName: dresClass.name, showFlag: common.getRandomNum(0, 3), showOrder: common.getRandomNum(10, 100) };
            // const json = { type: 13, typeId: 1013, typeName: '女装', showFlag: 3, showOrder: 0 };
            await standardClass.saveCatConfig(json);
            // console.log(standardClass);
        });
        it('分类配置查询', async function () {
            const res = await spCommon.findCatConfig({ type: standardClass.type });
            const actual = res.result.data[standardClass.type].find((element) => element.id == standardClass.id);
            expect(actual, `分类配置查询 未查询到数据 type=${standardClass.type} id=${standardClass.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(standardClass.getInfo(), actual);
        });
        it('3.删除城市', async function () {
            await spCommon.delCatConfig({ id: standardClass.id });
            const res = await spCommon.findCatConfig({ type: standardClass.type });
            const actual = res.result.data[standardClass.type].find((element) => element.id == standardClass.id);
            expect(actual).to.be.undefined;
        });
    });

    describe('分类配置分页查询', function () {
        it('分类配置分页查询', async function () {
            let catConfigList, pageRes1, pageRes2;
            const type = 4, total = 40, pageSize = 20;
            [catConfigList, pageRes1, pageRes2] = await Promise.all([spCommon.findCatConfig({ type }), spCommon.getCatConfigList({ pageNo: 1, pageSize, type }), spCommon.getCatConfigList({ pageNo: 2, pageSize, type })]);
            let exp = catConfigList.result.data[type].slice(0, total);
            expect(exp.length, `分类配置查询结果为空:${catConfigList}`).to.above(0);
            let result = pageRes1.result.data.rows.concat(pageRes2.result.data.rows);
            expect(result.length, `分配配置分页查询结果为空`).to.above(0);
            common.isApproximatelyEqualAssert(exp, result);
        });

    });
});

/**
 * 删除分类配置
 * @param {number} type 分类的id
 * @param {string} name 分类的名称
 */
async function delCatConf(type, name) {
    const res = await spCommon.findCatConfig({ type, reqFlag: 1, pageSize: 20, pageNo: 1 });
    const info = res.result.data[type].find(res => res.typeName == name);

    if (info != undefined) {
        await spCommon.delCatConfig({ id: info.id });
    };
};