const innerStaffMsg = require('../../help/spConfg/innerStaffMsg');
const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spReq = require('../../help/spReq');
const spbi = require('../../../reqHandler/sp/global/spbi');
const mysql = require('../../../reqHandler/sp/mysql');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');

/**
 * 
 * http://zentao.hzdlsoft.com:6082/zentao/task-view-2228.html
 * 
 * 内部人员信息 spconfg.inner_staff_msg
 * 搜索历史明细表 spbig.sp_search_history_detail
 * 
 * 提供3个接口 搜索查询与统计 历史明细表 
 * 只统计商品数据,不统计店铺
 */
describe('内部人员信息', async function () {
    this.timeout(30000);

    before(async function () {
        await spAuth.staffLogin();
    });

    it('搜索数据准备', async function () {
        await spReq.spClientLogin({ code: '18268176451' });
        // await spdresb.searchDres({ searchToken: '上衣' });
        await spdresb.searchDres({ searchToken: 'z', pageNo: 1, pageSize: 20 });
        await spdresb.searchDres({ searchToken: 'z', pageNo: 2, pageSize: 20 });
        await spdresb.searchDres({ searchToken: 'zx' });
        await spdresb.searchDres({ searchToken: 'zxx' });

        await spReq.spClientLogin();
        await spdresb.searchDres({ searchToken: 'z', pageNo: 1, pageSize: 20 });
        await spdresb.searchDres({ searchToken: 'z', pageNo: 2, pageSize: 20 });
        await spdresb.searchDres({ searchToken: 'zx' });
        await spdresb.searchDres({ searchToken: 'zxx' });
    });

    describe('内部人员维护', async function () {
        const innerStaff = innerStaffMsg.setupInnerStaffMsg();
        before('新增', async function () {
            await spReq.spLogin({ code: common.getRandomMobile() });
            const clientInfo = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();
            await innerStaff.getRandom({ userInfo: clientInfo }).save();
        });
        after(async function () {
            if (innerStaff.id) {
                await innerStaff.disable();
            }
        });
        it('查询内部人员信息表', async function () {
            const innerStaffList = await spAuth.findInnerStaffMsg({ nameLike: innerStaff.name, mobileLike: innerStaff.mobile }).then(res => res.result.data.rows);
            const info = innerStaffList.find(data => data.id == innerStaff.id);
            expect(info).not.to.be.undefined;
            common.isApproximatelyEqualAssert(innerStaff, info);
        });
        describe('修改', async function () {
            before(async function () {
                await innerStaff.getRandom().save();
            });
            it('查询内部人员信息表', async function () {
                const innerStaffList = await spAuth.findInnerStaffMsg({ nameLike: innerStaff.name, mobileLike: innerStaff.mobile }).then(res => res.result.data.rows);
                const info = innerStaffList.find(data => data.id == innerStaff.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(innerStaff, info);
            });
        });
        describe('停用类型', async function () {
            before(async function () {
                await innerStaff.disable();
            });
            it('查询内部人员信息表', async function () {
                const innerStaffList = await spAuth.findInnerStaffMsg({ nameLike: innerStaff.name, mobileLike: innerStaff.mobile, flag: 0 }).then(res => res.result.data.rows);
                const info = innerStaffList.find(data => data.id == innerStaff.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(innerStaff, info);
            });
        });
        describe('启用类型', async function () {
            before(async function () {
                await innerStaff.enable();
            });
            it('查询内部人员信息表', async function () {
                const innerStaffList = await spAuth.findInnerStaffMsg({ nameLike: innerStaff.name, mobileLike: innerStaff.mobile, flag: 1 }).then(res => res.result.data.rows);
                const info = innerStaffList.find(data => data.id == innerStaff.id);
                expect(info).not.to.be.undefined;
                common.isApproximatelyEqualAssert(innerStaff, info);
            });
        });
    });

    describe('搜索历史明细查询', async function () {
        let spgSql, innerTenantIds = [], testTenantIds = [];
        before(async function () {
            spgSql = await mysql({ dbName: 'spgMysql' });

            // typeId 1代表测试人员，2代表内部人员
            testTenantIds = await spgSql.query(`SELECT tenant_id FROM spconfg.inner_staff_msg WHERE type_id=1`).then(res => res[0].map(data => data.tenant_id));
            innerTenantIds = await spgSql.query(`SELECT tenant_id FROM spconfg.inner_staff_msg WHERE type_id=2`).then(res => res[0].map(data => data.tenant_id));
            // console.log(`innerTenantIds=${JSON.stringify(innerTenantIds)}`);
            // console.log(`testTenantIds=${JSON.stringify(testTenantIds)}`);
        });
        after(async function () {
            await spgSql.end();
        });
        describe('搜索历史明细查询', async function () {
            it('排除内部人员', async function () {
                const list = await spbi.findSpSearchHistoryDetail({ removeInner: 1, removeTest: 0, pageNo: 1, pageSize: 50 }).then(res => res.result.data.rows);
                const queryCond = innerTenantIds.length > 0 ? `AND tenant_id NOT IN (${innerTenantIds.toString()})` : '';
                const [expList] = await spgSql.query(`SELECT*FROM spbig.sp_search_history_detail WHERE created_date>='${common.getCurrentDate()}' AND type_id=0 ${queryCond}`);
                // console.log(`expList=${JSON.stringify(expList)}`);
                // console.log(`\nlist=${JSON.stringify(list)}`);
            });
            it('排除测试人员', async function () {
                const list = await spbi.findSpSearchHistoryDetail({ removeInner: 0, removeTest: 1 }).then(res => res.result.data.rows);
                // console.log(`list=${JSON.stringify(list)}`);
            });
        });
        describe('搜索人+搜索词统计搜索历史明细', async function () {
            it('数据校验', async function () {
                const list = await spbi.findSpSearchHistoryDetailByTenantAndToken({ pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
                for (let index = 0; index < list.length; index++) {
                    const element = list[index];
                    const exp = await spgSql.query(`SELECT
                    FLOOR( AVG( search_num ) ) AS avgSearchNum,
                    MIN( search_num ) AS minSearchNum,
                    MAX( search_num ) AS maxSearchNum,
                    COUNT( id ) AS searchTimes,
                    FLOOR( AVG( page_no ) ) AS avgPageNo 
                FROM
                    spbig.sp_search_history_detail 
                WHERE
                    type_id = 0 
                    AND search_token = '${element.searchToken}'
                    AND tenant_id = ${element.tenantId}
                    AND created_date >= ${common.getCurrentDate()}`).then(res => res[0]);
                    common.isApproximatelyEqualAssert(exp, element);
                }
            });
        });
        describe('搜索词统计搜索历史明细', async function () {
            it('数据校验', async function () {
                const list = await spbi.findSpSearchHistoryDetailByToken({ pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
                for (let index = 0; index < list.length; index++) {
                    const element = list[index];
                    const exp = await spgSql.query(`SELECT
                    FLOOR(AVG( search_num )) AS avgSearchNum,
                    MIN( search_num ) AS minSearchNum,
                    MAX( search_num ) AS maxSearchNum,
                    COUNT( id ) AS searchTimes,
                    COUNT(DISTINCT tenant_id ) AS searchManNum,
                    FLOOR(AVG( page_no )) AS avgPageNo 
                FROM
                    spbig.sp_search_history_detail 
                WHERE
                    type_id = 0 
                    AND search_token = '${element.searchToken}' 
                    AND created_date >= ${common.getCurrentDate()}`).then(res => res[0]);
                    common.isApproximatelyEqualAssert(exp, element);
                }
            });

        });
    });

});