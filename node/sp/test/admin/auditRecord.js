
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const auditManage = require('../../help/spAdmin/auditManage');

// 审核流程 => 买家审核 卖家店铺审核
// 审核禁用 在visitor/ban.js 后面看情况合并整理 19-04-11
describe('平台审核-买家', async function () {
    this.timeout(30000);
    const myAudit = auditManage.setupSpAudit();
    before(async function () {
        // 注册用户 
        // 若开启了验证码校验 则需要先将手机号加入白名单
        // const registerRes = await spReq.userRegister();
        // 使用新用户登陆
        await spReq.spLogin({ code: common.getRandomMobile() });//registerRes.params.jsonParam.mobile
        myAudit.setUserInfo();

        const json = await myAudit.mockAuditJson();
        await myAudit.submitAuditRecord(json);
    });

    after('管理员登出,注销注册用户', async function () {
        await spAuth.staffLogout();

        try {
            await spReq.spLogin({ code: myAudit.mobile });
            await spugr.deleteUser();
        } catch (error) {
            console.warn(error);
        }
    });

    // 服务端 相同店铺名称可以提交审核
    describe('买家店铺审核--管理员审核流程', function () {
        it('修改买家店铺名称时,同步店铺名称与买家租户名,买家用户名一致', async function () {
            const res = await spugr.getUser();
            common.isFieldsEqualAssert(myAudit, res.result.data, 'shopName=nickName', '修改后名称不一致');
        });

        it('搜索查看买家店铺审核记录列表(管理员)', async function () {
            await spAuth.staffLogin();
            const findShopAuditsRes = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, flag: 0, nameLike: myAudit.auditName, submitDateS: common.getCurrentDate(), submitDateE: common.getCurrentDate(), platConfirm: 0, auditSrc: 0 });
            const info = findShopAuditsRes.result.data.rows.find(element => element.auditObjId == myAudit.auditObjId);
            expect(info, `买家店铺审核记录列表未找到审核记录 auditObjId:${myAudit.auditObjId}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(myAudit.getBuyerShopAuditsExp(), info);
        });

        it('买家获取店铺审核信息', async function () {
            await spReq.spLogin({ code: myAudit.mobile });
            const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
            common.isApproximatelyEqualAssert(myAudit, shopAuditsInfo.result.data);
        });

        it('新增价格区间和主营类目字段验证', async function () {
            await spReq.spLogin({ code: myAudit.mobile });
            const messageRes = await spCommon.verifyShopMessage();
            common.isApproximatelyEqualAssert({
                flag: 0,//1代表主营类目，价格区间信息均齐全,0代表信息不齐全
                priceRangeCompleteIs: 0,//1代表价格区间信息齐全,0代表价格区间信息不齐全1代表价格区间信息齐全,0代表价格区间信息不齐全
                masterClassIdCompleteIs: 1,//1代表主营类目信息齐全,0代表主营类目信息不齐全
            }, messageRes.result.data);
        });

        describe('驳回审核信息', async function () {
            before(async function () {
                await spAuth.staffLogin();
                await myAudit.changeAuditRecord({ flag: 9, auditRemark: '条件不符合申请条件，请认真填写申请' });
            });
            it('搜索查看买家店铺审核记录列表(管理员)', async function () {
                const findShopAuditsRes = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, flag: 9, nameLike: myAudit.auditName });
                const info = findShopAuditsRes.result.data.rows.find(element => element.auditObjId == myAudit.auditObjId);
                expect(info, `买家店铺审核记录列表未找到审核记录 auditObjId:${myAudit.auditObjId}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(myAudit.getBuyerShopAuditsExp(), info);
            });

            it('驳回后,买家查看店铺审核信息', async function () {
                await spReq.spLogin({ code: myAudit.mobile });
                const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                common.isApproximatelyEqualAssert(myAudit, shopAuditsInfo.result.data);
            });
        });

        // 前端限制不能重复审核通过,服务端没有限制
        describe('审核通过', async function () {
            before('重新提交审核,管理员审核通过', async function () {
                await common.delay(500);
                await spReq.spLogin({ code: myAudit.mobile });
                const json = myAudit.getBuyerShopAuditsExp();
                json.id = myAudit.id;
                json.priceRange = [{ price1: common.getRandomNum(10, 100), price2: common.getRandomNum(101, 500) }];
                await myAudit.submitAuditRecord(json);

                await common.delay(500);
                await spAuth.staffLogin();
                await myAudit.changeAuditRecord({ flag: 1, auditRemark: '条件符合审核条件，同意了' });
            });
            it('搜索查看买家店铺审核记录列表(管理员)', async function () {
                const findShopAuditsRes = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, flag: 1, nameLike: myAudit.auditName });
                const info = findShopAuditsRes.result.data.rows.find(element => element.auditObjId == myAudit.auditObjId);
                expect(info, `买家店铺审核记录列表未找到审核记录 auditObjId:${myAudit.auditObjId}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(myAudit.getBuyerShopAuditsExp(), info);
            });
            it('买家获取店铺审核信息', async function () {
                await spReq.spLogin({ code: myAudit.mobile });
                const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                common.isApproximatelyEqualAssert(myAudit, shopAuditsInfo.result.data);
            });
        });

        describe('买家维护价格区间和主营类目字段', function () {
            before(async function () {
                //买家登录
                await spReq.spLogin({ code: myAudit.mobile });
            });
            it('价格区间和主营类目验证', async function () {
                const messageRes = await spCommon.verifyShopMessage();
                common.isApproximatelyEqualAssert({
                    flag: 1,//1代表主营类目，价格区间信息均齐全,0代表信息不齐全
                    priceRangeCompleteIs: 1,//1代表价格区间信息齐全,0代表价格区间信息不齐全1代表价格区间信息齐全,0代表价格区间信息不齐全
                    masterClassIdCompleteIs: 1,//1代表主营类目信息齐全,0代表主营类目信息不齐全
                }, messageRes.result.data);
            });
            it('买家查看审批信息', async function () {
                const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                common.isApproximatelyEqualAssert(myAudit, shopAuditsInfo.result.data);
            });
        });
    });

    describe('买家店铺审核-有订单-强制审核', async function () {
        before('买家下单,平台驳回审核', async function () {
            await spReq.spSellerLogin();
            const sellerTenantId = LOGINDATA.tenantId;

            await spReq.spLogin({ code: myAudit.mobile });
            BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
            BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });
            const styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchDres.result.data.dresStyleResultList[0].detailUrl });
            const purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

            await spAuth.staffLogin();
        });
        it('平台驳回审核', async function () {
            const res = await myAudit.changeAuditRecord({ flag: 9, auditRemark: '条件不符合申请条件，请认真填写申请' });
            expect(res.result.data).to.includes({ flag: 0, auditFailReason: '当前买家有订单正在进行，是否强制驳回' });
        });
        it('平台强制驳回', async function () {
            const res = await myAudit.changeAuditRecord({ flag: 9, auditRemark: '条件不符合申请条件，请认真填写申请', rebutIs: 1 });
            expect(res.result.data).to.includes({ flag: 1 });
        });
        it('买家获取店铺审核信息', async function () {
            await spReq.spLogin({ code: myAudit.mobile });
            const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
            common.isApproximatelyEqualAssert(myAudit, shopAuditsInfo.result.data);
        });
    });

});


describe.skip('店铺审核--邀请码', function () {
    let submitAuditRes, registerRes, areaJson;
    before('注册一个用户，并且提交申请', async function () {
        registerRes = await spReq.userRegister();
        await spReq.spLogin({ code: registerRes.params.jsonParam.mobile });
        areaJson = await mockJsonParam.getAreaJson();
        let submitJson = await mockJsonParam.submitAuditJson({ areaJson });
        submitJson.inviteCode = 'autotest';
        submitAuditRes = await spCommon.submitAuditRecord(submitJson);
    });

    after('注销用户', async function () {
        await spReq.spLogin({ code: registerRes.params.jsonParam.mobile });
        await spugr.deleteUser();
    });

    it('买家查看审核信息', async function () {
        let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
        common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
        expect(shopAuditsInfo.result.data.auditFlag, '输入正确的邀请码后未成功开通店铺').to.be.equal(1);
    });

    it('管理员查看申请列表', async function () {
        await spAuth.staffLogin();
        const findShopAuditsRes = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, jsonParam: { inviteCodeIs: 1, nameLike: submitAuditRes.params.jsonParam.shopName, submitDateS: common.getCurrentDate(), submitDateE: common.getCurrentDate() } });
        const info = findShopAuditsRes.result.data.rows.find(element => element.auditObjId == submitAuditRes.result.data.id);
        submitAuditRes.params.jsonParam.auditSrc = 2;   //代表 bd认证   bd认证指的就是邀请码认证
        submitAuditRes.params.jsonParam.platConfirm = 0;     //平台未确认
        common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, info);
        expect(info.flag, '输入正确的邀请码后未成功开通店铺').to.be.equal(1);
    });

    it('用错误的邀请码申请', async function () {
        //管理员让申请再不通过
        await spCommon.changeAuditRecord({
            shopId: submitAuditRes.result.data.id,
            flag: 9,
        });

        await spReq.spLogin({ code: registerRes.params.jsonParam.mobile });
        let submitJson = await mockJsonParam.submitAuditJson({ areaJson });
        submitJson.inviteCode = common.getRandomNumStr(12);
        submitJson.check = false;
        let res = await spCommon.submitAuditRecord(submitJson);
        expect(res.result, '无效的邀请码也成功了').to.includes({ "msgId": "invite_code_worry" });
    });
});
