
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresg = require('../../../reqHandler/sp/global/spdresg');
const spconfg = require('../../../reqHandler/sp/global/spconfg');
const common = require('../../../lib/common');

/**
 * 频道商品同步
 * 内部使用 不作为常规测试
 */
describe.skip('saas主从同步', async function () {
    this.timeout(30000);

    before(async function () {
        await spAuth.staffLogin();

    });

    it('保存平台分发规则', async function () {
        const res = await spdresg.saveSpSaasDresSyncRule({ masterSaasId: 1, slaveSaasId: 87673, channelId: 1685 });
        console.log(`res=${JSON.stringify(res)}`);
    });

    it('手动触发同步saas分发商品', async function () {
        const res = await spdresg.manualSyncSaasDres({ saasSyncId: 17 });
        console.log(`res=${JSON.stringify(res)}`);
    });

    it('saas频道商品迁移', async function () {
        const res = await spdresg.channelDresCopy({ masterChannelId: 1789, slaveChannelId: 1981 });
        console.log(`res=${JSON.stringify(res)}`);
    });


});