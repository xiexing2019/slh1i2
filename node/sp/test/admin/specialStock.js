const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const configParamManager = require('../../help/configParamManager');

// http://zentao.hzdlsoft.com:6082/zentao/task-view-1404.html
//
describe('特殊库存', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, dres, spSpecialStockFlag;
    before(async function () {
        await spAuth.staffLogin();
        spSpecialStockFlag = await configParamManager.getParamInfo({ code: 'sp_special_stock_flag', domainKind: 'system' });
        await spSpecialStockFlag.updateParam({ val: 1 });

        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        const classId = 1013;
        const classInfo = await spdresb.findByClass({ classId });
        const arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));

        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };

        // 新增商品
        dres = await spdresb.saveFull(mockJsonParam.styleJson({ classId: 1013 })).then(res => res.result.data);
        await common.delay(1000);
    });
    after(async function () {
        await spReq.spSellerLogin();
        // 下架商品 防止影响其他用例
        await spdresb.offMarket({ ids: [dres.spuId] });

        await spAuth.staffLogin();
        await spSpecialStockFlag.returnOriginalParam();
    });

    describe('平台设置特殊库存', async function () {
        before(async function () {
            await spAuth.staffLogin();
            const skuJson = dres.skuIds.map(sku => { return { id: sku.skuId, spuId: dres.spuId, stockFlag: 1 } });
            await spdresb.setSpecialStockSkus({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId, jsonParam: skuJson });
            await common.delay(1000);
        });
        // after(async function () {

        // });
        it('根据spuId获取sku', async function () {
            const skus = await spdresb.findSkusBySpuIds({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId, type: 1 })
                .then(res => res.result.data.rows);
            skus.forEach(sku => expect(sku).to.includes({ stockFlag: 1 }));
        });
        it('全局商品搜索', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
            const dresDetail = dresList.find(data => data.id == dres.spuId);
            expect(dresDetail).to.includes({ specialFlag: 1 });
        });
        it('卖家查询商品详情', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
            dresDetail.skus.forEach(sku => expect(sku).to.includes({ stockFlag: 1 }));
        });
        it('买家查询商品详情', async function () {
            await spReq.spClientLogin();
            const dresDetail = await spdresb.getFullForBuyer({ spuId: dres.spuId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId }).then(res => res.result.data);
            dresDetail.skus.forEach(sku => expect(sku).to.includes({ stockFlag: 1 }));
        });
        it('买家下单', async function () {
            // await spdresb.cancelSpecialStockSkus({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId })
        });
    });

    describe('平台设置特殊颜色尺码', async function () {
        let jsonParam;
        before(async function () {
            await spAuth.staffLogin();
            jsonParam = [{
                spec1: common.randomSort(BASICDATA['605'])[0].codeValue,
                spec2: common.randomSort(BASICDATA['601'])[0].codeValue,
            }]
            await spdresb.setSpecialColorSkus({
                spuId: dres.spuId,
                unitId: sellerInfo.unitId,
                tenantId: sellerInfo.tenantId,
                jsonParam
            });
            await common.delay(1000);
        });
        it('根据spuId获取sku', async function () {
            const skus = await spdresb.findSkusBySpuIds({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId, type: 2 })
                .then(res => res.result.data.rows);
            common.isApproximatelyArrayAssert(jsonParam, skus);
        });
        it('全局商品搜索', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
            const dresDetail = dresList.find(data => data.id == dres.spuId);
            expect(dresDetail).to.includes({ specialFlag: 1 });
        });
        it('卖家查询商品详情', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
            common.isApproximatelyArrayAssert(jsonParam, dresDetail.skus);
        });
        // 需要重构 数据来源依赖商陆花同步
        it.skip('取消特殊尺码颜色', async function () {
            // await spdresb.cancelSpecialColorSkus({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId });
        });
        it('卖家查询商品详情', async function () {
            // await spReq.spSellerLogin();
            // const dresDetail = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
            // console.log(`dresDetail=${JSON.stringify(dresDetail)}`);

            // expect(dresDetail.spu).to.includes({ minSpuOrderNum: 5 });
        });
    });

    //http://zentao.hzdlsoft.com:6082/zentao/task-view-3141.html 
    describe('没有库存的待上架商品,设置特殊库存后,需要自动触发重新上架流程', async function () {
        describe('设置特殊库存', async function () {
            let dres;
            before('新增没有库存的商品并设置特殊库存', async function () {
                await spReq.spSellerLogin();
                const json = mockJsonParam.styleJson({ classId: 1013 });
                json.skus.forEach(sku => sku.num = 0);
                dres = await spdresb.saveFull(json).then(res => res.result.data);
                // console.log(dres);

                const skuJson = dres.skuIds.map(sku => { return { id: sku.skuId, spuId: dres.spuId, stockFlag: 1 } });
                await spdresb.setSpecialStockSkus({ spuId: dres.spuId, unitId: sellerInfo.unitId, tenantId: sellerInfo.tenantId, jsonParam: skuJson });
                await common.delay(1000);
            });
            it('查询商品状态', async function () {
                const dresFull = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
                expect(dresFull.spu).to.includes({ flag: 1 });
            });
        });
        describe('设置特殊颜色尺码', async function () {
            let dres;
            before('新增没有库存的商品并设置特殊颜色尺码', async function () {
                await spReq.spSellerLogin();
                const json = mockJsonParam.styleJson({ classId: 1013 });
                json.skus.forEach(sku => sku.num = 0);
                dres = await spdresb.saveFull(json).then(res => res.result.data);
                // console.log(dres);

                await spdresb.setSpecialColorSkus({
                    spuId: dres.spuId,
                    unitId: sellerInfo.unitId,
                    tenantId: sellerInfo.tenantId,
                    jsonParam: [{
                        spec1: common.randomSort(BASICDATA['605'])[0].codeValue,
                        spec2: common.randomSort(BASICDATA['601'])[0].codeValue,
                        stockFlag: 1
                    }]
                });
                await common.delay(1000);
            });
            it('查询商品状态', async function () {
                const dresFull = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
                expect(dresFull.spu).to.includes({ flag: 1 });
            });
        });
    });

});