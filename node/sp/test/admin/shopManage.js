
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spAccount = require('../../data/spAccount');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const paramManager = require('../../help/configParamManager');

//管理员 管理店铺的相关信息
describe('店铺管理', function () {
    this.timeout(TESTCASE.timeout);
    let sellerTenantId;
    before('数据准备', async function () {
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
    });

    describe('店铺启用停用', async function () {
        let saveRes, purRes, shopInfoBefore;

        before('新增租户，并且禁用', async function () {
            await spReq.spLogin({ code: spAccount.seller.mobile });
            //创建一个新的租户，createShop创建的是一个上线并且有效的店铺
            const saveTenantRes = await spReq.createShop({
                name: `企业${common.getRandomStr(5)}`
            });
            sellerTenantId = saveTenantRes.result.data.val;

            shopInfoBefore = await spugr.getShop({ id: sellerTenantId }).then(res => res.result.data);
            //门店禁用，注意：禁用同时会下线门店，但是启用不会同时上线门店的
            await spAuth.staffLogin();
            saveRes = await spugr.disableShopByAdmin({ id: sellerTenantId, unUseReason: '门店涉嫌违规操作' });
        });

        after('下线店铺', async function () {
            if (sellerTenantId) {
                await spAuth.staffLogin();
                await spugr.offlineShopById({ id: sellerTenantId });
            }
        });

        it('1.禁用后查询门店可登陆状态', async function () {
            await spReq.spLogin({ code: spAccount.seller.mobile });
            //注，此接口主要是为了让客户端知道门店的状态，目前服务端还是没有限制的
            const canLogin = await spugr.getShopLoginStatus({ id: sellerTenantId })
                .then(res => res.result.data);
            expect(canLogin.canLogin).to.be.false;
            expect(canLogin.unUsingReason, `禁用原因不一致，${canLogin.unUsingReason}`).to.be.equal(saveRes.params.jsonParam.unUseReason);
        });

        //usingFlag:禁用停用  1--启用 0---停用  flag：门店上下线标识 0---下线 1---上新
        /**
         * 修改
         * 1.门店信息其他字段也要校验 isAql简单验证即可
         */
        it('2.禁用后门店详情验证', async function () {
            await spReq.spLogin({ code: spAccount.seller.mobile });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            let shopInfo = await spugr.getShopDetail().then(res => res.result.data);
            // console.log(`shopInfo=${JSON.stringify(shopInfo)}`);
            shopInfoBefore.flag = 0;
            shopInfoBefore.usingFlag = 0;
            shopInfoBefore.unUnsingReason = saveRes.params.jsonParam.unUseReason;

            common.isApproximatelyEqualAssert(shopInfoBefore, shopInfo, ['updatedBy']);//updateBy  一次是卖家自己，另一次是管理员
        });

        it('禁用后尝试上线', async function () {
            await spAuth.staffLogin();
            let res = await spugr.onlineShopById({ id: sellerTenantId, check: false });
            expect(res.result).to.includes({ "msgId": "shop_can_not_online_with_unusing" });
        });

        it('3.门店启用验证', async function () {
            //管理员启用门店
            await spAuth.staffLogin();
            await spugr.enableShopByAdmin({ id: sellerTenantId });
            await spReq.spLogin({ code: spAccount.seller.mobile });
            const canLogin = await spugr.getShopLoginStatus({ id: sellerTenantId })
                .then(res => res.result.data);
            expect(canLogin.canLogin, '门店启用了，但是canLogin字段还是不显示为true').to.be.true;
        });

        //启用不会上线门店的
        it('4.启用后验证门店详情', async function () {
            await spReq.spLogin({ code: spAccount.seller.mobile });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            let shopInfo = await spugr.getShopDetail().then(res => res.result.data);

            expect(shopInfo.flag, '门店启用，将门店同时上线了').to.be.equal(0);
            expect(shopInfo.usingFlag, '门店没有启用成功').to.be.equal(1);
            await spAuth.staffLogin();
            await spugr.onlineShopById({ id: sellerTenantId });
        });

        describe('门店存在未完成的订单', function () {

            before('店铺存在未完成的订单，禁用店铺', async function () {
                await spReq.spLogin({ code: spAccount.seller.mobile });
                await spugr.changeTenantInSession({ tenantId: sellerTenantId });

                await spReq.saveDresFull();
                await spReq.spClientLogin();
                //买家下订单，为了让卖家有未完成的单子，从而保证卖家有未完成的单子   
                await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                await common.delay(3000);
                const dresList = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, tenantId: sellerTenantId, queryType: 4 })
                    .then(res => res.result.data.dresStyleResultList);
                // 商品新增同步到es可能比较慢 若查不到商品 则重试1次
                if (dresList.length == 0) {
                    this.retries(1);
                }

                const getFullForBuyerByUrlRes = await spReq.getFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl });
                purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: getFullForBuyerByUrlRes }));
                //管理员禁用店铺
                await spAuth.staffLogin();
                await spugr.disableShopByAdmin({ id: sellerTenantId, unUseReason: '门店涉嫌违规操作' });
            });

            it('1.查询店铺是否可登录', async function () {
                await spReq.spLogin({ code: spAccount.seller.mobile });
                const canLogin = await spugr.getShopLoginStatus({ id: sellerTenantId });
                let exp = { canLogin: true, spShopBillInfoDTO: { remainNums: 1, remainReasons: `订单编号为[${purRes.result.data.rows[0].billNo}]的订单正在进行中` } };
                //console.log(`canLogin=${JSON.stringify(canLogin)}`);

                common.isApproximatelyEqualAssert(exp, canLogin.result.data);
            });

            it('2.查询店铺详情', async function () {
                await spReq.spLogin({ code: spAccount.seller.mobile });
                await spugr.changeTenantInSession({ tenantId: sellerTenantId });
                let shopInfo = await spugr.getShopDetail().then(res => res.result.data);

                expect(shopInfo.flag, '门店停用，店铺没有同时下线').to.be.equal(0);
                expect(shopInfo.usingFlag, '禁用门店失败').to.be.equal(0);
            })

            //取消门店。登录状态为不可登录状态
            it('3.取消这个订单,检查门店的可登陆状态', async function () {
                await spReq.spClientLogin();
                await spTrade.cancelPurBill({ id: purRes.result.data.rows[0].billId, cancelKind: BASICDATA['2008'][0].codeValue, buyerRem: common.getRandomStr(4) });
                await spReq.spLogin({ code: spAccount.seller.mobile });
                const canLogin = await spugr.getShopLoginStatus({ id: sellerTenantId });
                expect(canLogin.result.data.canLogin).to.be.false;
            });
        });
    });

    describe('店铺上线/下线', async function () {
        before('店铺上线', async function () {
            if (!sellerTenantId) this.skip();

            await spAuth.staffLogin();
            await spugr.enableShopByAdmin({ id: sellerTenantId });
        });
        it('查看店铺详情 typeId=1', async function () {
            await spugr.onlineShopById({ id: sellerTenantId, shopTypeId: 1 });
            const shopInfo = await spugr.getShop({ id: sellerTenantId }).then(res => res.result.data);
            expect(shopInfo, `店铺种类与保存时不一致`).to.includes({ flag: 1, typeId: 1, usingFlag: 1 });
        });
        it('查看店铺详情 typeId=0', async function () {
            await spugr.onlineShopById({ id: sellerTenantId, shopTypeId: 0 });
            const shopInfo = await spugr.getShop({ id: sellerTenantId }).then(res => res.result.data);
            expect(shopInfo, `店铺种类与保存时不一致`).to.includes({ flag: 1, typeId: 0, usingFlag: 1 });
        });
        describe('店铺下线', async function () {
            before(async function () {
                if (!sellerTenantId) this.skip();
                await spugr.offlineShopById({ id: sellerTenantId });
            });
            it('查看店铺详情', async function () {
                const shopInfo = await spugr.getShop({ id: sellerTenantId }).then(res => res.result.data);
                expect(shopInfo, `店铺种类与保存时不一致`).to.includes({ flag: 0, typeId: 0, usingFlag: 1 });
            });
        });
    });

    // 执行也是为了确保自动化卖家账户有上新能力
    describe('门店启用/禁用上新能力', function () {
        let sellerInfo;

        before('门店状态初始化', async function () {
            await spReq.spSellerLogin();
            let shopDetail = await spugr.getShopDetail().then(res => res.result.data);
            let masterClassId = shopDetail.masterClassId;

            sellerInfo = LOGINDATA;
            await spReq.saveDresFull();
            //console.log(`\ns.params.jsonParam=${JSON.stringify(s.params.jsonParam)}`);
            await spReq.spClientLogin();
            await spCommon.updateBuyerShopMessage({ masterClassId });

            await spUp.addFavorShop({
                shopId: sellerInfo.tenantId,
                shopName: sellerInfo.shopName,
                check: false
            });
            await spAuth.staffLogin();
            await spugr.inUseNewShopById({ id: sellerInfo.tenantId, check: false });    //确保门店的初始状态为可上新状态

            //管理员设置好店， score >0  即设置好店，score =0  删除好店
            await spCommon.saveShopScore({ id: sellerInfo.tenantId, score: common.getRandomNum(1, 5) });
        });

        after('启用门店上新能力', async function () {
            await spAuth.staffLogin();
            await spugr.inUseNewShopById({ id: sellerInfo.tenantId, check: false });
        });

        describe('禁用门店上新能力', function () {

            before('禁用门店上新能力', async function () {
                await spugr.unUseNewShopById({ id: sellerInfo.tenantId });
                await common.delay(1000);
            });
            it('获取店铺详情，校验specialFlag', async function () {
                await spReq.spSellerLogin();
                const specialFlag = await spugr.getShopDetail().then(res => res.result.data.specialFlag);
                // console.log(specialFlag);
                expect(specialFlag, '禁用门店上新能力失败').to.be.equal(0);
            });

            it('推荐上新列表查看', async function () {
                await spReq.spClientLogin();
                let searchDres = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 2, tenantId: sellerInfo.tenantId });
                expect(searchDres.result.data.dresStyleResultList.length, '禁用了门店上新能力，但是推荐上新的款号没用清空').to.be.equal(0);
            });

            //门店禁用上新能力之后，商品的speialFlag 也会变化的
            it('查看商品列表', async function () {
                await spReq.spSellerLogin();
                let dresList = await spdresb.findSellerSpuList({ pageSize: 20, pageNo: 1, orderBy: 'marketDate', orderByDesc: true });
                dresList.result.data.rows.forEach(res => {
                    expect(res.specialFlag, `推荐上新能力禁用之后，商品的specialFlag没有变化,商品：${res.name},specialFlag:${res.specialFlag}`).to.be.oneOf([0, -1]);
                });
            });

            it('关注上新列表验证', async function () {
                await spReq.spClientLogin();
                let favorShopDres = await spdresb.searchDres({ pageSize: 30, pageNo: 1, queryType: 1 }).then(res => res.result.data.dresStyleResultList);
                let info = favorShopDres.find(res => res.tenantId == sellerInfo.tenantId);
                expect(info, '禁用门店上新功能之后，关注上新列表也没有此门店的数据了').to.not.be.undefined;
            });

            it('今日上新列表', async function () {
                let todayNewShopList = await spdresb.getTodayNewShop({ pageSize: 10, pageNo: 1, showSpus: 1, nameLike: spAccount.seller.shopName }).then(res => res.result.data.rows);
                let info = todayNewShopList.find(res => res.id == sellerInfo.tenantId);
                expect(info, '店铺上新能力禁用后，今日上新门店列表里面没有这个店铺了').to.not.be.undefined;
            });

            it('发现好店列表', async function () {
                let goodShopList = await spdresb.getGoodShopByBuyer({ pageSize: 0, pageNo: 1, showSpus: 1 }).then(res => res.result.data.rows);
                let info = goodShopList.find(res => res.tenantId == sellerInfo.tenantId);
                expect(info, '店铺上新能力禁用后，好店列表里面依然有这个店铺').to.be.undefined;
            });

            it('搜索门店', async function () {
                let shopList = await spugr.findAllShopByParams({ nameLike: spAccount.seller.shopName }).then(res => res.result.data.rows);
                let info = shopList.find(res => res.tenantId == sellerInfo.tenantId);
                expect(info, '禁用门店上新能力后，店铺搜索搜索不到这个店铺了').to.not.be.undefined;
            });

            it('今日上新（不包含热度款号）', async function () {
                let searchRes = await spdresb.searchDres({ pageSize: 20, pageNo: 1, tenantId: sellerInfo.tenantId, queryType: 6 }).then(res => res.result.data.dresStyleResultList);
                let info = searchRes.find(res => res.tenantId == sellerInfo.tenantId);

                expect(info, '店铺没有上新能力，今日上新搜索不到此店铺的商品').to.not.be.undefined;
            });

            it('es搜索', async function () {
                let searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tenantId: sellerInfo.tenantId, queryType: 4 });
                let info = searchRes.result.data.dresStyleResultList.find(res => res.tenantId == sellerInfo.tenantId);
                expect(info, '禁用门店上新能力后,es搜索不到这个门店的商品了').to.not.be.undefined;
            });
        });

        describe('启用门店上新能力', function () {
            let saveDresRes;
            before('启用门店上新能力', async function () {
                await spAuth.staffLogin();
                await spugr.inUseNewShopById({ id: sellerInfo.tenantId });
                await common.delay(500);
                await spReq.spSellerLogin();
                const specialFlag = await spugr.getShopDetail().then(res => res.result.data.specialFlag);
                // console.log(specialFlag);
                expect(specialFlag, '启用门店上新能力失败').to.be.equal(1);
                saveDresRes = await spReq.saveDresFull();
            });

            it('卖家新增一个商品,检查推荐上新', async function () {
                this.retries(2);
                await spReq.spClientLogin();
                await common.delay(2000); //为了脚本稳定性先设置3s
                let searchDres = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 2, tenantId: sellerInfo.tenantId })
                    .then(res => res.result.data.dresStyleResultList);

                let info = searchDres.find(res => res.id == saveDresRes.result.data.spuId);
                expect(info, `给门店赋予了上新能力，但是新增一个商品后，推荐上新还是没有展示此商品${JSON.stringify(saveDresRes.params.jsonParam)}`).to.not.be.undefined;
                expect(searchDres.every(res => res.specialFlag == 1 || res.specialFlag == 0), `门店有上新能力，推荐列表中展示了没有推荐属性的商品,${searchDres}`).to.be.true;
            });

            it('启用门店上新能力之后，添加的商品应该都有上新能力', async function () {
                await spReq.spSellerLogin();
                let dresList = await spdresb.findSellerSpuList({ nameLike: saveDresRes.params.jsonParam.spu.name, orderBy: 'marketDate', orderByDesc: true })
                    .then(res => res.result.data.rows);
                let info = dresList.find(res => res.id == saveDresRes.result.data.spuId);
                expect(info.specialFlag, '门店有上新能力，但是添加的商品状态为不推荐').to.be.equal(1);
            });

            it('商品列表检验', async function () {
                await spReq.spSellerLogin();
                let dresList = await spdresb.findSellerSpuList({ pageSize: 20, pageNo: 1, orderBy: 'marketDate', orderByDesc: true })
                    .then(res => res.result.data.rows);
                _.remove(dresList, res => {
                    return res.id == saveDresRes.result.data.spuId;
                });
                expect(dresList.every(res => res.specialFlag == -1 || res.specialFlag == 0), '门店启用上新能力之后，商品的推荐属性也变了').to.be.true;
            });

            //上新 + 主营类目 + 后台设置好店同时匹配才能显示在好店列表
            it('好店列表', async function () {
                await spReq.spClientLogin();
                let goodShopList = await spdresb.getGoodShopByBuyer({ pageSize: 0, pageNo: 1, showSpus: 1 }).then(res => res.result.data.rows);
                let info = goodShopList.find(res => res.tenantId == sellerInfo.tenantId);
                expect(info, '店铺上新能力启用后，好店列表依然没有找到这个门店').to.not.be.undefined;
            });

            describe('管理员下新商品', function () {
                before('管理员下新商品', async function () {
                    await spAuth.staffLogin();
                    await spugr.offDresByAdmin({
                        spus: [{
                            spuId: saveDresRes.result.data.spuId,
                            tenantId: sellerInfo.tenantId,
                            unitId: sellerInfo.unitId,
                            clusterCode: sellerInfo.clusterCode
                        }]
                    });
                });

                it('下新商品之后，检查商品状态', async function () {
                    this.retries(3);
                    await spReq.spSellerLogin();
                    await common.delay(2000);
                    let dresList = await spdresb.findSellerSpuList({ nameLike: saveDresRes.params.jsonParam.spu.name, orderBy: 'marketDate', orderByDesc: true })
                        .then(res => res.result.data.rows);
                    let info = dresList.find(res => res.id == saveDresRes.result.data.spuId);
                    expect(info.specialFlag, `商品下新之后，状态没变,商品的id是${saveDresRes.result.data.spuId}`).to.be.oneOf([0, -1]);
                });

                it('买家查看es条件过滤查询', async function () {
                    await spReq.spClientLogin();
                    let dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tenantId: sellerInfo.tenantId, keyWords: { specialFlag: [1] } });
                    let info = dresList.result.data.dresStyleResultList.find(res => res.id == saveDresRes.result.data.spuId);
                    expect(info, `商品已经下新了，但是商品依然可以查到`).to.be.undefined;
                });

                it('修改商品,检查商品状态', async function () {
                    await spReq.spSellerLogin();
                    let updateJson = saveDresRes.params.jsonParam;
                    updateJson.spu.id = saveDresRes.result.data.spuId;
                    updateJson.spu.rem = `备注${common.getRandomNum(4)}`;
                    await spdresb.saveFull(updateJson);
                    let dresList = await spdresb.findSellerSpuList({ nameLike: saveDresRes.params.jsonParam.spu.name, orderBy: 'marketDate', orderByDesc: true })
                        .then(res => res.result.data.rows);
                    let info = dresList.find(res => res.id == saveDresRes.result.data.spuId);
                    expect(info.specialFlag, '永久下新的商品，修改保存后又上新了').to.be.equal(0);
                });
            });
        });
    });

    describe('运营端配置推荐店铺', async function () {
        let suggestShopInfo;
        before('配置推荐店铺', async function () {
            await spReq.spSellerLogin();
            const shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);

            await spAuth.staffLogin();
            // 不能重复保存
            const data = await spCommon.findSpSuggFocus({ shopName: shopInfo.shopName, masterClassId: shopInfo.masterClassId })
                .then(res => res.result.data.rows.find(val => val.shopName == shopInfo.name));
            if (data) await spCommon.deleteSuggestShop({ id: data.id });

            const saveRes = await spCommon.saveSuggestShop({ shopName: shopInfo.name, shopId: shopInfo.id, masterClassId: shopInfo.masterClassId, showOrder: common.getRandomNum(5, 10) });
            suggestShopInfo = saveRes.params.jsonParam;
            suggestShopInfo.id = saveRes.result.data.val;
            suggestShopInfo.type = 2;
            suggestShopInfo.unitId = shopInfo.unitId;
            suggestShopInfo.shopAddr = shopInfo.ecCaption.provCode + shopInfo.ecCaption.cityCode + shopInfo.ecCaption.areaCode + shopInfo.detailAddr;
        });
        it('查询配置店铺', async function () {
            const res = await spCommon.findSpSuggFocus({ shopName: suggestShopInfo.shopName, masterClassId: suggestShopInfo.masterClassId });
            const data = res.result.data.rows.find(val => val.shopName == suggestShopInfo.shopName);
            common.isApproximatelyEqualAssert(suggestShopInfo, data);
        });
        it('查询配置店铺 默认排序校验', async function () {
            const dataList = await spCommon.findSpSuggFocus().then(res => res.result.data.rows);
            dataList.forEach((data, index) => {
                dataList[index - 1] && expect(data.showOrder).to.be.least(dataList[index - 1].showOrder);
            });
        });
        describe('删除配置店铺', async function () {
            before(async function () {
                await spCommon.deleteSuggestShop({ id: suggestShopInfo.id });
            });
            it('查询配置店铺', async function () {
                const res = await spCommon.findSpSuggFocus({ shopName: suggestShopInfo.shopName });
                const data = res.result.data.rows.find(val => val.shopName == suggestShopInfo.shopName);
                expect(data, `删除配置后 查询配置店铺列表中依然可以查到被删除的配置店铺 id:${suggestShopInfo.id}`).to.be.undefined;
            });
        });
    });

    describe('过年不发货提醒', async function () {
        let sellerInfo;
        before('保存租户的参数', async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
        });
        after('还原设置', async function () {
            let vacationStartDay = common.getDateString([0, 0, -3]), vacationEndDay = common.getDateString([0, 0, -2]), deliverDay = common.getDateString([0, 0, -1]);
            await spAuth.staffLogin();
            await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_shop_vacation', vacationStartDay, vacationEndDay });
            await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_delivery_day', deliverDay });
        });
        describe('放假时间内--第一天', async function () {
            let vacationStartDay = common.getDateString([0, 0, 0]), vacationEndDay = common.getDateString([0, 0, 1]), deliverDay = common.getDateString([0, 0, 2]);
            before(async function () {
                await spAuth.staffLogin();
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_shop_vacation', vacationStartDay, vacationEndDay });
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_delivery_day', deliverDay });

                await spReq.spSellerLogin();
            });
            it('校验买家下单时间是否在卖家放假时间内', async function () {
                const data = await spdresb.verifyProTime().then(res => res.result.data);
                expect(data).to.includes({ [LOGINDATA.tenantId]: getShopVacationMsg({ vacationStartDay, vacationEndDay, deliverDay }) });
            });
            it('买家下单校验提示信息', async function () {

            });
        });
        describe('放假时间内--最后一天', async function () {
            let vacationStartDay = common.getDateString([0, 0, -1]), vacationEndDay = common.getDateString([0, 0, 0]), deliverDay = common.getDateString([0, 0, 1]);
            before(async function () {
                await spAuth.staffLogin();
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_shop_vacation', vacationStartDay, vacationEndDay });
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_delivery_day', deliverDay });

                await spReq.spSellerLogin();
            });
            it('校验买家下单时间是否在卖家放假时间内', async function () {
                const data = await spdresb.verifyProTime().then(res => res.result.data);
                expect(data).to.includes({ [LOGINDATA.tenantId]: getShopVacationMsg({ vacationStartDay, vacationEndDay, deliverDay }) });
            });
            it('买家下单校验提示信息', async function () {

            });
        });
        describe('发货时间', async function () {
            let vacationStartDay = common.getDateString([0, 0, -2]), vacationEndDay = common.getDateString([0, 0, -1]), deliverDay = common.getDateString([0, 0, 0]);
            before(async function () {
                await spAuth.staffLogin();
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_shop_vacation', vacationStartDay, vacationEndDay });
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_delivery_day', deliverDay });

                await spReq.spSellerLogin();
            });
            it('校验买家下单时间是否在卖家放假时间内', async function () {
                const data = await spdresb.verifyProTime().then(res => res.result.data);
                expect(data).not.to.includes({ [LOGINDATA.tenantId]: getShopVacationMsg({ vacationStartDay, vacationEndDay, deliverDay }) });
            });
            it('买家下单校验提示信息', async function () {

            });
        });
        describe('放假时间外', async function () {
            let vacationStartDay = common.getDateString([0, 0, 1]), vacationEndDay = common.getDateString([0, 0, 2]), deliverDay = common.getDateString([0, 0, 3]);
            before(async function () {
                await spAuth.staffLogin();
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_shop_vacation', vacationStartDay, vacationEndDay });
                await spAuth.saveUnitParamVal({ tenantId: sellerInfo.tenantId, code: 'sp_delivery_day', deliverDay });

                await spReq.spSellerLogin();
            });
            it('校验买家下单时间是否在卖家放假时间内', async function () {
                const data = await spdresb.verifyProTime().then(res => res.result.data);
                expect(data).not.to.includes({ [LOGINDATA.tenantId]: getShopVacationMsg({ vacationStartDay, vacationEndDay, deliverDay }) });
            });
            it('买家下单校验提示信息', async function () {

            });
        });
    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-1481.html
    // 平台设置的发货地址在卖家信息的props字段中 不会影响原有的发货地址
    // 取消设置的店铺地址时，清空props中的地址
    // 仓储获取地址 优先获取平台设置的发货地址
    describe('平台设定发货地址', async function () {
        let sellerInfo, deliverAddrInfo, origDeliverAddrInfo;
        before(async function () {
            await spReq.spSellerLogin({ shopName: '中洲店' });
            sellerInfo = _.cloneDeep(LOGINDATA);

            await spAuth.staffLogin();
            // 获取原始地址
            const shopList = await spugr.findAllShopByParams({ nameLike: sellerInfo.shopName })
                .then(res => res.result.data.rows);
            origDeliverAddrInfo = format.dataFormat(shopList, 'deliverProvCode;deliverCityCode;deliverAreaCode;deliverDetailAddr');

            // 平台设置卖家发货地址
            const areaJson = await mockJsonParam.getAreaJson({ provinceLabel: '浙江省' });
            deliverAddrInfo = {
                deliverProvCode: areaJson.provinceValue,
                deliverCityCode: areaJson.cityValue,
                deliverAreaCode: areaJson.countyValue,
                deliverDetailAddr: `滨江星耀城16楼${common.getRandomStr(5)}`,
            };
            await spugr.updateShopAddr({
                id: sellerInfo.tenantId,
                ...deliverAddrInfo
            });
        });
        it('店铺多搜索值组合搜索（平台）', async function () {
            const shopList = await spugr.findAllShopByParams({ nameLike: sellerInfo.shopName })
                .then(res => res.result.data.rows);
            const shopInfo = shopList.find(shop => shop.tenantId == sellerInfo.tenantId);
            expect(shopInfo).not.to.includes(origDeliverAddrInfo);
        });
        it('卖家获取店铺详情', async function () {
            await spReq.spSellerLogin();
            const shopInfo = await spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
            // 扩展字段校验
            common.isApproximatelyEqualAssert(deliverAddrInfo, JSON.parse(shopInfo.props));
        });
        it.skip('仓储-收货/入库/出库列表', async function () {
            await wmsWarehouse.wmsStaffLogin();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

            const list = await wmsWarehouse.getMissionList({ orderType: 1, orderByDesc: true, orderBy: 'created_date' });
            // console.log(`list=${JSON.stringify(list)}`);
        });
        describe('取消店铺发货地址', async function () {
            before(async function () {
                await spAuth.staffLogin();
                await spugr.cancelShopAddrFlag({ id: sellerInfo.tenantId });
            });
            it('店铺多搜索值组合搜索（平台）', async function () {
                const shopList = await spugr.findAllShopByParams({ nameLike: sellerInfo.shopName })
                    .then(res => res.result.data.rows);
                const shopInfo = shopList.find(shop => shop.tenantId == sellerInfo.tenantId);
                expect(shopInfo).not.to.includes(origDeliverAddrInfo);
            });
            it('卖家获取店铺详情', async function () {
                await spReq.spSellerLogin();
                const shopInfo = await spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
                // 扩展字段校验 不返回 或者返回字段中不包含平台设置的地址
                if (shopInfo.hasOwnProperty('props')) {
                    expect(JSON.parse(shopInfo.props)).not.to.includes(deliverAddrInfo);
                }
            });
            it.skip('仓储-收货/入库/出库列表', async function () {
                await wmsWarehouse.wmsStaffLogin();

                const list = await wmsWarehouse.getMissionList({ orderType: 1, orderByDesc: true, orderBy: 'created_date' });
                // console.log(`list=${JSON.stringify(list)}`);
            });
        });
    });

    describe('批量设置退货标签', async function () {
        let sellerTenantId, returnLabel;
        before(async function () {
            await spReq.spSellerLogin();
            sellerTenantId = LOGINDATA.tenantId;

            await spAuth.staffLogin();
            returnLabel = await paramManager.getParamInfo({ domainKind: 'system', code: 'sp_return_label' });
        });
        it('不能同时选择48小时无理由和不支持退换', async function () {
            const labels = returnLabel.val.split('-').join(',');
            const res = await spugr.batchSetShopLebal({ check: false, jsonParam: [{ id: sellerTenantId, labels }] });
            expect(res.result).to.includes({ msgId: 'return_label_must_one' });
        });
        it('卖家获取店铺详情', async function () {
            const label = returnLabel.val.split('-')[0];
            await spugr.batchSetShopLebal({ jsonParam: [{ id: sellerTenantId, labels: label }] });

            await spReq.spSellerLogin();
            const shopInfo = await spugr.getShopDetail();
            expect(shopInfo.result.data.labels, `${shopInfo.params.apiKey},店铺标签显示错误`).to.have.members([Number(label)]);
        });

    });

});

function getShopVacationMsg({ vacationStartDay, vacationEndDay, deliverDay }) {
    const formatDateStr = (DateStr) => {
        const [year, mouth, day] = DateStr.split('-');
        return `${mouth}月${day}日`;
    }
    return `商家${formatDateStr(vacationStartDay)}~${formatDateStr(vacationEndDay)}放假,${formatDateStr(deliverDay)}起发货`;
};
