const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const frontCategory = require('../../help/spAdmin/frontCategory');
const common = require('../../../lib/common');

describe('前台类目', async function () {
    this.timeout(30000);
    before(async function () {
        await spAuth.staffLogin();
    });

    describe('前台类目', async function () {
        const frontCategoryAction = frontCategory.setupFrontCategoryAction();
        before('新增前台类目', async function () {
            await spAuth.staffLogin();
            await frontCategoryAction.getRandom().save();
        });
        after(async function () {
            if (frontCategoryAction.id) {
                await frontCategoryAction.disable();
            }
        });
        it('前台类目列表查询', async function () {
            const list = await spAuth.getSpFrontCategoryActionList({ nameLike: frontCategoryAction.name }).then(res => res.result.data.rows);
            const info = list.find(data => data.id == frontCategoryAction.id);
            expect(info, `未找到前台类目 id=${frontCategoryAction.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(frontCategoryAction, info);
        });
        it('通过id查询前台类目', async function () {
            const detail = await frontCategoryAction.getDetail().then(res => res.result.data);
            common.isApproximatelyEqualAssert(frontCategoryAction, detail);
        });
        describe('修改', async function () {
            before(async function () {
                await frontCategoryAction.getRandom().save();
            });
            it('前台类目列表查询', async function () {
                const list = await spAuth.getSpFrontCategoryActionList({ nameLike: frontCategoryAction.name }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == frontCategoryAction.id);
                expect(info, `未找到前台类目 id=${frontCategoryAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(frontCategoryAction, info);
            });
            it('通过id查询前台类目', async function () {
                const detail = await frontCategoryAction.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(frontCategoryAction, detail);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await frontCategoryAction.disable();
            });
            it('前台类目列表查询', async function () {
                const list = await spAuth.getSpFrontCategoryActionList({ nameLike: frontCategoryAction.name, flag: 0 }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == frontCategoryAction.id);
                expect(info, `未找到前台类目 id=${frontCategoryAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(frontCategoryAction, info);
            });
            it('通过id查询前台类目', async function () {
                const detail = await frontCategoryAction.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(frontCategoryAction, detail);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await frontCategoryAction.enable();
            });
            it('前台类目列表查询', async function () {
                const list = await spAuth.getSpFrontCategoryActionList({ nameLike: frontCategoryAction.name, flag: 1 }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == frontCategoryAction.id);
                expect(info, `未找到前台类目 id=${frontCategoryAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(frontCategoryAction, info);
            });
            it('通过id查询前台类目', async function () {
                const detail = await frontCategoryAction.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(frontCategoryAction, detail);
            });
        });
    });

    // 前提
    describe('前台类目项目', async function () {
        const frontCategoryAction = frontCategory.setupFrontCategoryAction(),
            detailAction = frontCategory.setupFrontCategoryDetailAction();
        before('新增前台类目', async function () {
            await spAuth.staffLogin();
            // 新增商品集
            await frontCategoryAction.getRandom().save();
            // 新增商品项目
            await detailAction.getRandom({ categoryId: frontCategoryAction.id });
            await detailAction.save();
            await common.delay(500);
            // console.log(frontCategoryAction);
            // console.log(detailAction);
        });
        after(async function () {
            if (frontCategoryAction.id) {
                await frontCategoryAction.disable();
            }
        });
        it('前台类目项目列表查询', async function () {
            const list = await spAuth.getSpFrontCategoryDetailActionList({ categoryId: frontCategoryAction.id, pageSize: 100, pageNo: 1 })
                .then(res => {
                    // console.log(`res=${JSON.stringify(res)}`);
                    return res.result.data.rows;
                });
            const info = list.find(data => data.id == detailAction.id);
            expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(detailAction, info);
        });
        it('前台类目项目树状查询', async function () {
            const list = await spAuth.getSpFrontCategoryDetailActionListByTree({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
            const info = list.find(data => data.id == detailAction.id);
            expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(detailAction, info);
        });
        it.skip('初始化前台类目项目信息', async function () {
            await detailAction.init();
        });
        describe('修改', async function () {
            before(async function () {
                await detailAction.getRandom({ categoryId: frontCategoryAction.id });
                await detailAction.update();
                await common.delay(500);
            });
            it('前台类目项目列表查询', async function () {
                this.retries(2);
                await common.delay(100);
                const list = await spAuth.getSpFrontCategoryDetailActionList({ categoryId: frontCategoryAction.id, nameLike: detailAction.name }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
            it('前台类目项目树状查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionListByTree({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await detailAction.disable();
                await common.delay(500);
            });
            it('前台类目项目列表查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionList({ categoryId: frontCategoryAction.id, flag: 0 }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
            it('前台类目项目树状查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionListByTree({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await detailAction.enable();
                await common.delay(500);
            });
            it('前台类目项目列表查询', async function () {
                this.retries(2);
                await common.delay(100);
                const list = await spAuth.getSpFrontCategoryDetailActionList({ categoryId: frontCategoryAction.id, flag: 1, pageSize: 100, pageNo: 1 }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
            it('前台类目项目树状查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionListByTree({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(detailAction, info);
            });
        });
        describe.skip('删除', async function () {
            before(async function () {
                await detailAction.delete();
            });
            it('前台类目项目列表查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionList({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).to.be.undefined;
            });
            it('前台类目项目树状查询', async function () {
                const list = await spAuth.getSpFrontCategoryDetailActionListByTree({ categoryId: frontCategoryAction.id }).then(res => res.result.data.rows);
                const info = list.find(data => data.id == detailAction.id);
                expect(info, `未找到前台类目 id=${detailAction.id}`).to.be.undefined;
            });
        });
    });

});
