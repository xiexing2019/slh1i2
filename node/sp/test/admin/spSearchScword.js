
const spSearchScword = require('../../help/spConfg/spSearchScword');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spconfg = require('../../../reqHandler/sp/global/spconfg');
const common = require('../../../lib/common');

/**
 * http://zentao.hzdlsoft.com:6082/zentao/task-view-2770.html
 * 
 * 由运营维护
 * 自动化根据搜索词 执行定时任务
 */
describe('自检搜索词', async function () {
    this.timeout(30000);

    before(async function () {
        await spAuth.staffLogin();
    });

    describe('搜索词维护', async function () {
        const searchScword = spSearchScword.setupSearchScword();
        before('新增搜索词', async function () {
            await searchScword.getRandom().save();
        });
        it('列表查询', async function () {
            const detail = await searchScword.getFromList();
            common.isApproximatelyEqualAssert(searchScword, detail);
        });
        it('明细校验', async function () {
            const detail = await searchScword.getDetailById().then(res => res.result.data);
            common.isApproximatelyEqualAssert(searchScword, detail);
        });
        describe('修改', async function () {
            before(async function () {
                await searchScword.getRandom().save();
            });
            it('列表查询', async function () {
                const detail = await searchScword.getFromList();
                common.isApproximatelyEqualAssert(searchScword, detail);
            });
            it('明细校验', async function () {
                const detail = await searchScword.getDetailById().then(res => res.result.data);
                common.isApproximatelyEqualAssert(searchScword, detail);
            });
        });
        describe('删除', async function () {
            before(async function () {
                await searchScword.deleteScwords();
            });
            it('列表', async function () {
                const list = await spconfg.getSpSearchScwordList({ scword: searchScword.scword }).then(res => res.result.data.rows);
                const data = list.find(row => row.id == searchScword.id);
                expect(data, `自检关键词列表中查询到被删除的关键词 id=${searchScword.id} scword=${searchScword.scword}`).to.be.undefined;
            });
            it('明细校验', async function () {
                const res = await searchScword.getDetailById({ check: false });
                expect(res.result).to.include({ msgId: `SpSearchScword不存在的编号:${searchScword.id}` });
            });
        });
    });

});