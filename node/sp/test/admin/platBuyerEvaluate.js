const common = require('../../../lib/common');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const mysql = require('../../../reqHandler/sp/mysql');
const spReq = require('../../help/spReq');


describe('买手说', async function () {
    this.timeout(30000);
    let spgSql;
    before(async function () {
        await spReq.spClientLogin();

        spgSql = await mysql({ dbName: 'spgMysql' });
    });
    after(async function () {
        await spgSql.end();
    });

    describe('获取买手说-店铺', async function () {
        it('获取买手说', async function () {
            const [expList] = await spgSql.query(`SELECT id,type,biz_id AS bizId,biz_name AS bizName,belong_id AS belongId,flag,action_flag AS actionFlag,show_flag AS showFlag FROM spadmin.sp_plat_buyer_evaluate WHERE type=1`);

            const evaluateList = await spAdmin.getPlatBuyerEvaluateList({ type: 1 }).then(res => res.result.data.rows);
            common.isArrayContainObjectAssert(expList, evaluateList);
        });
    });

    describe('获取买手说-商品', async function () {
        it('获取买手说', async function () {
            const [expList] = await spgSql.query(`SELECT id,type,biz_id AS bizId,biz_name AS bizName,belong_id AS belongId,flag,action_flag AS actionFlag,show_flag AS showFlag FROM spadmin.sp_plat_buyer_evaluate WHERE type=2`);

            const evaluateList = await spAdmin.getPlatBuyerEvaluateList({ type: 2 }).then(res => res.result.data.rows);
            common.isArrayContainObjectAssert(expList, evaluateList);
        });
    });

});