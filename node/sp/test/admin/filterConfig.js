const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const esSearchHelp = require('../../help/esSearchHelp');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');

describe('筛选配置', function () {
    this.timeout(30000);

    describe('筛选类别顺序验证', function () {
        let configList;
        before('登陆', async function () {
            await spAuth.staffLogin();
        });

        let filterType = [1, 2, 3, 4];
        //给已经配置了的 筛选配置 配置showOrder 让showOrder大小不一致，为了验证排序功能
        for (let i = 0; i < filterType.length; i++) {

            it('初始化筛选顺序', async function () {
                let res = await spCommon.getSpFilterConfigList({ filterType: filterType[i] }).then(res => res.result.data.rows);
                for (let i = 0; i < res.length; i++) {
                    res[i].showOrder = i + 1;
                    await spCommon.saveSpFilterConfig(res[i]);
                };
            });

            //管理员验证列表排序  按照 showOrder顺序排的
            it('验证顺序', async function () {
                configList = await spCommon.getSpFilterConfigList({ filterType: filterType[i] }).then(res => res.result.data.rows);
                esSearchHelp.orderAssert({ dataList: configList, path: 'showOrder', orderByDesc: false });
            });

            it('买家查询', async function () {
                let res = await spCommon.getFilterConfigData({ filterType: filterType[i] }).then(res => res.result.data.rows);
                expect(configList.length, '买家端筛选配置显示的数量和设置的不一致').to.be.equal(res.length);
                common.isApproximatelyEqualAssert(configList, res, `买家端筛选配置顺序错误`);
            });
        };
    });

    describe('修改后前后端一致性', function () {
        let filterList, saveRes, configInfo;
        const filterType = common.getRandomNum(1, 4);//从四种筛选对象中选择一个 1 推荐下拉， 2 商品搜索， 3 店铺搜索， 4 分类
        before('读取买家端筛选', async function () {
            await spReq.spClientLogin();
            filterList = await spCommon.getFilterConfigData({ filterType }).then(res => res.result.data.rows);
        });

        after('数据还原', async function () {
            if (saveRes != undefined) {
                await spCommon.delSpFilterConfig({ id: saveRes.result.data.val });
            };
            configInfo.showFlag = 2;
            await spCommon.saveCatConfig(configInfo);
        });

        it('验证内容筛选', async function () {
            let exp = await getExp(filterList);
            common.isApproximatelyEqualAssert(exp, filterList);
        });

        it('修改内容后，查看内容筛选', async function () {
            let info = filterList.find(val => val.typeValue == 5);
            if (info == undefined) {
                saveRes = await spCommon.saveSpFilterConfig({ filterType, typeValue: 5, typeName: '城市', showOrder: common.getRandomNum(5, 10) });
            };
            //取城市中第一个  设置为买家不可见
            let res = await spCommon.getFilterConfigData({ filterType }).then(res => res.result.data.rows);
            let typeName = res.find(val => val.typeValue == 5).filterDatas[0].codeName;
            configInfo = await spCommon.findCatConfig({ type: 1, typeName }).then(res => res.result.data[1][0]);
            configInfo.showFlag = 0;
            await spCommon.saveCatConfig(configInfo);
            await common.delay(1500);
            let resAfter = await spCommon.getFilterConfigData({ filterType }).then(res => res.result.data.rows);
            let infoAfter = resAfter.find(val => val.typeValue == 5).filterDatas.find(val => val.codeName == typeName);
            expect(infoAfter, `城市${typeName}设置成了买家不可见，但是查询${filterType}依然有这个城市`).to.be.undefined;
        });
    });

    describe('筛选配置增删改查', function () {
        const filterType = common.getRandomNum(1, 4);
        let filterInfo, saveRes;
        before('获取字典', async function () {
            //字典中随机取一条
            await spAuth.staffLogin();
            let dictList = await spugr.getDictList({ "flag": 1, "typeId": 2023 }).then(res => res.result.data.rows);
            let dictInfo = common.randomSort(dictList)[0];

            let filterList = await spCommon.getSpFilterConfigList({ filterType }).then(res => res.result.data.rows);
            filterInfo = filterList.find(val => val.typeValue == dictInfo.codeValue);

            if (filterInfo != undefined) {
                await spCommon.delSpFilterConfig({ id: filterInfo.id });
            };
            saveRes = await spCommon.saveSpFilterConfig({ filterType, typeValue: dictInfo.codeValue, typeName: dictInfo.codeName, showOrder: 1 });
        });

        after('数据还原', async function () {
            if (filterInfo != undefined) {
                await spCommon.saveSpFilterConfig({ filterType, typeValue: filterInfo.typeValue, typeName: filterInfo.typeName, showOrder: 1 });
            } else {
                await spCommon.delSpFilterConfig({ id: saveRes.result.data.val, check: false });
            };
        });

        it('查询', async function () {
            let filterList = await spCommon.getSpFilterConfigList({ filterType, }).then(res => res.result.data.rows);
            let info = filterList.find(val => val.id == saveRes.result.data.val);
            if (info == undefined) {
                throw new Error('新增的筛选配置列表中找不到');
            } else {
                common.isApproximatelyEqualAssert(info, saveRes.params.jsonParam);
            };
        });

        it('删除', async function () {
            await spCommon.delSpFilterConfig({ id: saveRes.result.data.val });
            let filterList = await spCommon.getSpFilterConfigList({ filterType, }).then(res => res.result.data.rows);
            let info = filterList.find(val => val.id == saveRes.result.data.val);
            expect(info, '删除后列表中还能找到').to.be.undefined;
        });
    });

});

async function getExp(list) {
    await spAuth.staffLogin();
    let exp = [];
    for (let i = 0; i < list.length; i++) {
        let filterDatas = await getData(list[i].typeValue);
        exp.push({ typeValue: list[i].typeValue, filterType: list[i].filterType, filterDatas });
    };
    return exp;
};

async function getData(typeValue) {
    let filterData = [];
    switch (typeValue) {
        // 标签   获取标签列表 showFlagBit:2 买家可见
        case 1:
            await spCommon.findCatConfig({ type: 8, showFlagBit: 2 }).then(res => res.result.data[8].forEach(val => {
                if (val.flag == 1) filterData.push({ codeValue: val.typeValue, codeName: val.typeName });
            }));
            break;
        case 2:    //勋章  获取勋章列表
            await spCommon.getMedalList({ flag: 1 }).then(res => res.result.data.rows.forEach(val => {
                filterData.push({ codeValue: val.code, codeName: val.name });
            }));
            break;
        case 3:    //风格
            await spCommon.findCatConfig({ type: 2, showFlagBit: 2 }).then(res => res.result.data[2].forEach(val => {
                if (val.flag == 1) filterData.push({ codeValue: val.typeValue, codeName: val.typeName });
            }));
            break;
        case 4: //主营类目
            await spCommon.findCatConfig({ type: 7, showFlagBit: 2 }).then(res => res.result.data[7].forEach(val => {
                if (val.flag == 1) filterData.push({ codeValue: val.typeValue, codeName: val.typeName });
            }));
            break;
        case 5:  //城市
            await spCommon.findCatConfig({ type: 1, showFlagBit: 2 }).then(res => res.result.data[1].forEach(val => {
                if (val.flag == 1) filterData.push({ codeValue: val.typeValue, codeName: val.typeName });
            }));
            break;
        case 6:  //市场
            await spCommon.findCatConfig({ type: 3, showFlagBit: 2, reqFlag: 1 }).then(res => res.result.data[3].forEach(val => {
                if (val.flag == 1) filterData.push({ codeValue: val.typeValue, codeName: val.typeName });
            }));
            break;
        default:
            break;
    };

    return filterData;
};