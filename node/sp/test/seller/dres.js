const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const esSearch = require('../../reqHandler/sp/biz_server/esSearch');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spExp = require('../../help/getExp');
const dresManage = require('../../help/dresManage');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];

describe('商品', function () {
	this.timeout(TESTCASE.timeout);
	let sellerInfo, tenantId, styleRes, classId = 1013;
	const dres = dresManage.setupDres();

	before('卖家登录后保存商品', async () => {
		await spReq.spSellerLogin();
		// console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
		tenantId = LOGINDATA.tenantId;
		sellerInfo = _.cloneDeep(LOGINDATA);
		//获取类别信息,目前不支持顶级，如果传了顶级的，那么会给你转一个该级别下面的类别
		const classInfo = await spdresb.findByClass({ classId });// classTree.result.data.options.shift().code
		//获取字典类别列表
		let arr = ['606', '850', '2001', '2002', '2003', '2004'];
		classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
		for (let index = 0; index < arr.length; index++) {
			const element = arr[index];
			if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
				.then((res) => res.result.data.rows);
		};
		//保存商品
		const json = mockJsonParam.styleJson({ classId });
		// console.log(`json=${JSON.stringify(json)}`);
		// styleRes = await spdresb.saveFull(json); //
		await dres.saveDres(json);
		// console.log(dres);
		console.log(`spuId=${dres.id}`);
	});

	//卖家查询
	it('查询租户商品信息-online', async function () {
		const dresDetail = await dres.getFullById().then(res => res.result.data);
		// console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
		// 更新商品信息 补充保存时jsonParam中没有涉及的字段
		const dresExp = _.cloneDeep(dres);
		dres.setByDetail(dresDetail);
		common.isApproximatelyEqualAssert(dresExp.getDetailExp(), dresDetail, ['namePy', 'showOrder']);
	});

	it('卖家查询商品列表-online', async function () {
		const dresDetail = await dres.findSellerSpuList({ classId: dres.spu.classId });
		common.isApproximatelyEqualAssert(dres.spu, dresDetail);
	});

	it('修改spu-online', async function () {
		// 防止修改冲突
		await common.delay(500);
		await dres.editDresSpu({
			isAllowReturn: 1,
			rem: `修改spu备注${common.getRandomStr(3)}`
		});
		const dresDetail = await dres.getFullById().then(res => res.result.data);
		common.isApproximatelyEqualAssert(dres, dresDetail);
	});

	//这里是将将所有的skus的库存都修改
	it.skip('增加skus库存-online', async function () {
		const skus = [...dres.skus.values()].map(sku => { return { id: sku.id, num: common.getRandomNum(10, 30) } });
		await dres.addDresSkuNum({ skus });

		const dresDetail = await dres.getFullById().then(res => res.result.data);
		common.isApproximatelyEqualAssert(dres, dresDetail);
	});

	it('es搜索验证-online', async function () {
		await spReq.spClientLogin();
		await common.delay(1000);
		const dresSpu = await dres.searchDresById();

		// 2.1.8 商品添加店铺市场城市 用于扩展搜索
		await spReq.spSellerLogin();
		const shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
		dres.spu.marketId = shopInfo.marketId;
		dres.spu.cityId = shopInfo.cityCode;

		common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['docHeader', 'labels']);
	});

	it('买家查询商品信息-online', async function () {
		await spReq.spClientLogin();
		const dresDetail = await dres.getDetailForBuyer().then(res => res.result.data);
		common.isApproximatelyEqualAssert(dres.getDetailExp(), dresDetail, ['labels']);//labels []与'' 需要处理
	});

	it('修改商品信息-online', async function () {
		await common.delay(1000);
		await spReq.spSellerLogin();
		const updateJson = mockJsonParam.styleJson({ classId });
		updateJson.id = updateJson.spu.id = dres.id;
		const _skus = [...dres.skus.values()];
		updateJson.skus.forEach((res, index) => {
			res.id = _skus[index].id;
			res.num = common.add(res.num, 1000);
		});

		await dres.saveDres(updateJson);

		const dresDetail = await dres.getFullById().then(res => res.result.data);
		// 更新商品信息 补充保存时jsonParam中没有涉及的字段
		const dresExp = _.cloneDeep(dres);
		dres.setByDetail(dresDetail);

		common.isApproximatelyEqualAssert(dresExp.getDetailExp(), dresDetail, ['namePy', 'showOrder']);
	});

	// 在修改商品信息中,使用保存商品接口 修改校验
	// 本用例集为验证单独的修改商品详情图接口
	describe('商品详情图', async function () {
		describe('清空商品详情图', async function () {
			before(async function () {
				await spReq.spSellerLogin();
				// 清空商品详情图
				await spdresb.updateDetailProps({ spuId: dres.id, pictureContent: [] });
			});
			it('卖家查询租户商品信息', async function () {
				const dresDetail = await dres.getFullById().then(res => res.result.data);
				common.isApproximatelyEqualAssert({ pictureContent: [] }, JSON.parse(dresDetail.spu.detailProps));
			});
			it('买家查询商品详情', async function () {
				await spReq.spClientLogin();
				const dresDetail = await dres.getDetailForBuyer().then(res => res.result.data);
				common.isApproximatelyEqualAssert({ pictureContent: [] }, JSON.parse(dresDetail.spu.detailProps));
			});
		});
		describe('修改商品详情图', async function () {
			const pictureContent = common.randomSort(_.cloneDeep(docData.image.dres)).slice(0, 3).map(res => { return { docId: res.docId, docUrl: res.docUrl } });
			before(async function () {
				await spReq.spSellerLogin();
				// 添加商品详情图
				await spdresb.updateDetailProps({ spuId: dres.id, pictureContent });
			});
			it('卖家查询租户商品信息', async function () {
				const dresDetail = await dres.getFullById().then(res => res.result.data);
				common.isApproximatelyArrayAssert(pictureContent, JSON.parse(dresDetail.spu.detailedPictureContent));
			});
			it('买家查询商品详情', async function () {
				await spReq.spClientLogin();
				const dresDetail = await dres.getDetailForBuyer().then(res => res.result.data);
				common.isApproximatelyArrayAssert(pictureContent, JSON.parse(dresDetail.spu.detailedPictureContent));
			});
		});
	});

	describe('平台修改商品类别', function () {
		let fabricList, editParams;
		before('修改商品类别', async function () {
			await spReq.spSellerLogin();
			const dresDetail = await dres.getFullById().then(res => res.result.data);
			dres.setBySeller(dresDetail);
			// console.log(dres);

			await spAuth.staffLogin();
			fabricList = await spugr.getDictList({ "flag": 1, "typeId": 637 }).then(res => res.result.data.rows);
			editParams = {
				classId: '1085',    //类别
				seasonLabel: '春',		//季节
				mainKindLabel: '男',	//男女童
				colorLabel: '紫色',		//颜色
				versionLabel: '中款',	//板式
				themeLabel: '古典风',	//风格
				fabric: fabricList[common.getRandomNum(0, fabricList.length - 1)].codeValue,
			};
			const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
			editParams.docContent = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
			editParams.docHeader = [_.head(editParams.docContent)];
			// console.log(`editParams=${JSON.stringify(editParams)}`);
			await dres.updateSpuClassByAdmin(editParams);
		});

		it.skip('es测试', async function () {
			const res = await spdresb.esBaseSearch({
				query: {
					term: {
						id: styleRes.result.data.spuId,
					}
				}
			});
			console.log(res);
		});

		it('查看商品类别', async function () {
			await spReq.spSellerLogin();
			const dresDetail = await dres.getFullById().then(res => res.result.data);
			common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
		});

		it('es搜索此商品', async function () {
			const dresSpu = await dres.searchDresById();
			common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['labels']);
		});

		it('买家查看商品详情', async function () {
			await spReq.spClientLogin();
			const dresDetail = await dres.getDetailForBuyer().then(res => res.result.data);
			common.isApproximatelyEqualAssert(dres, dresDetail, ['labels']);
		});

		//管理员修改了商品的 类别，材质，图片等  卖家修改商品信息，这几个字段的修改时无效的，还是管理员修改的那个
		it.skip('修改商品，检查字段', async function () {
			await spReq.spSellerLogin();
			let param = mockJsonParam.styleJson({ classId });
			param.id = param.spu.id = dres.id;

			param.skus.forEach((res) => res.num += 1000);
			let codeValues = fabricList.map(val => val.codeValue);
			_.remove(codeValues, res => res == editParams.fabric);
			param.spu.fabric = codeValues[0, codeValues.length - 1];
			let res = await spdresb.saveFull(param);

			const dresInfo = await spdresb.getFullById({ id: dres.id });
			common.isApproximatelyEqualAssert(dres.spu, dresInfo.result.data.spu, [], '管理员后台修改了商品的类别，卖家还可以修改此类别');
			common.isApproximatelyEqualAssert(res.params.jsonParam, dresInfo.result.data.spu, ['classId', 'fabric', 'id']);
		});
	});

	describe('商品改价-卖家', async function () {
		before(async function () {
			await spReq.spSellerLogin();
			const pubPrice = common.getRandomNum(100, 200);
			await spdresb.updateSpuForSeller({ id: dres.id, pubPrice });
			dres.spu.pubPrice = pubPrice;
		});
		it('买家查询商品信息', async function () {
			this.retries(2);
			await common.delay(500);
			await spReq.spClientLogin();
			const dresDetail = await dres.getDetailForBuyer().then(res => res.result.data);
			common.isApproximatelyEqualAssert(dres, dresDetail, ['labels']);
		});
	});

	describe('推荐上新-online', async function () {
		before('确保买家是关注卖家的', async function () {
			await spReq.spClientLogin();
			await spUp.addFavorShop({
				shopId: sellerInfo.tenantId,
				shopName: sellerInfo.shopName,
				check: false
			});

			await spReq.spSellerLogin();
			const masterClassId = await spugr.getShopDetail().then(res => res.result.data.masterClassId);

			// 重新更新商品信息 防止前面脚本出错影响
			const dresDetail = await dres.getFullById().then(res => res.result.data);
			dres.setBySeller(dresDetail);

			//买家设置和卖家一样的主营类目
			await spReq.spClientLogin();
			await spCommon.updateBuyerShopMessage({ masterClassId });
		});
		it('新增的商品，是否在推荐上新列表里', async function () {
			this.retries(2);
			await common.delay(500);
			const dresSpu = await dres.searchDresById({ queryType: 2, needWait: 1 });
			common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['docHeader', 'labels']);
		});
		it('新增的商品，是否在关注上新列表里', async function () {
			const dresSpu = await dres.searchDresById({ queryType: 1 });
			common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['docHeader', 'labels']);
		});
	});

	//商品信息中相应字段更新还未实现 暂时只进行接口的冒泡处理验证
	describe('商品增量-online', function () {
		let buyerViewRes, clientInfo, dresDetail;
		before('商品增量', async function () {
			await spReq.spClientLogin();
			clientInfo = LOGINDATA;

			//商品查看数自增 
			buyerViewRes = await spdresb.saveBuyerView({ spuId: dres.id, sellerId: dres.tenantId, sellerUnitId: dres.spu.unitId, type: 1 });

			//商品点赞数自增
			await spdresb.saveBuyerPraise({ spuId: dres.id, sellerId: dres.tenantId, sellerUnitId: dres.spu.unitId });

			await spReq.spSellerLogin();
			await common.delay(500);
			// 重新更新商品信息 防止前面脚本出错影响
			dresDetail = await dres.getFullById().then(res => res.result.data);
		});
		it('商品查看数自增(记录)', async function () {
			// console.log(`styleFullRes.result.data.spu=${JSON.stringify(styleFullRes.result.data.spu)}`);
			expect(Number(dresDetail.spu.readNum), `商品id:${dres.id}`).at.least(2);
		});
		it('商品点赞数自增(记录)', async function () {
			expect(Number(dresDetail.spu.praiseNum)).to.equal(1);
		});
		it('店铺日志流查询', async function () {
			const res = await spUp.getShopLogList({ flowType: 1 });
			const exp = spExp.shopLogListExp({ createdDate: buyerViewRes.opTime, flowType: 1, clientInfo });
			common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
			expect(res.result.data.rows[0].flowContent).to.includes(`查看了货品：${dres.spu.title}`);
		});
		it('商品点赞数自增-重复点赞', async function () {
			await spReq.spClientLogin();
			const repeactRes = await spdresb.saveBuyerPraise({ spuId: dres.id, sellerId: dres.tenantId, sellerUnitId: dres.spu.unitId, check: false });
			expect(repeactRes.result).to.includes({ code: -10, msgId: 'the_spu_praise_have_exist' });
		});
	});

	describe('商品下架', function () {
		describe('卖家商品下架-online', function () {
			let clientInfo, purRes;

			before('卖家下架商品', async function () {
				await spReq.spClientLogin();
				await common.delay(500);
				clientInfo = _.cloneDeep(LOGINDATA);
				//获取用户默认收货地址
				await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

				const dresDetail = await dres.getDetailForBuyer();
				// 买家下单 
				purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresDetail }));
				// 添加商品至购物车
				dresDetail.count = 0;
				await spTrade.emptyCart();
				await spTrade.saveCart(mockJsonParam.cartJson(dresDetail));

				await spReq.spSellerLogin();
				await dres.offMarket();
			});
			it('1.卖家查询商品详情', async function () {
				const dresDetail = await dres.getFullById().then(res => res.result.data);
				expect(dresDetail.spu.flag).to.equal(-4);
			});

			it('2.卖家查询商品列表', async function () {
				const spuList = await spdresb.findSellerSpuList({ pageSize: 20, nameLike: dres.spu.code, flag: -4 }).then(res => res.result.data.rows);
				const dresSpu = spuList.find(element => element.id == dres.id);
				expect(dresSpu.flag, '商品下架后列表中的flag未改变').to.be.equal(-4);
			});

			//卖家自己下架 spuFlag =-4，管理员下架 spuFlag=-2
			it('3.购物车列表查看', async function () {
				// console.log(`styleRes=${JSON.stringify(styleRes)}`);
				await spReq.spClientLogin();
				await common.delay(1000);
				await spTrade.showCartList().then(res => res.result.data.rows[0].carts.forEach(cart => expect(cart.spuFlag).to.be.equal(-4)));
			});

			it('4.es商品搜索商品', async function () {
				const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.id] } }).then(res => res.result.data.dresStyleResultList);
				const dresSpu = dresList.find(data => data.id == dres.id);
				expect(dresSpu, `全局商品搜索 查询到下架商品 spuId=${dres.id}`).to.be.undefined;
			});

			it('5.下架后，已经生成的采购单后续操作', async function () {
				await spReq.spClientLogin();
				let payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
				await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

				await spReq.spSellerLogin();
				await common.delay(500);
				const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
				const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);

				const qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
				const details = qfRes.result.data.skus.map((sku) => {
					return { salesDetailId: sku.id, num: sku.skuNum };
				});
				const logisList = await spconfb.findLogisList1({ cap: 1 });
				await spTrade.deliverSalesBill({
					main: {
						logisCompId: logisList.result.data.rows[0].id,
						waybillNo: common.getRandomNumStr(12),
						buyerId: clientInfo.tenantId,
						shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
						hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
					},
					details: details
				});
				await spReq.spClientLogin();
				await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
			});

			//重新保存商品是不能上架商品的
			it('卖家保存下架商品-校验是否会重新上架', async function () {
				await spReq.spSellerLogin();
				await common.delay(1000);
				const updateJson = mockJsonParam.styleJson({ classId });
				updateJson.id = updateJson.spu.id = dres.id;

				await dres.saveDres(updateJson);

				const dresDetail = await dres.getFullById().then(res => res.result.data);
				dres.setByDetail(dresDetail);
				expect(dresDetail.spu.flag, '重新保存下架的商品又重新上架了').to.be.equal(-4);
			});

			describe('卖家重新上架商品', async function () {
				before('上架商品', async function () {
					await dres.onMarket();
				});
				it('卖家查询商品状态', async function () {
					const dresDetail = await dres.getFullById().then(res => res.result.data);
					expect(dresDetail.spu.flag).to.equal(1);
				});
				it('es校验商品状态', async function () {
					this.retries(5);
					await common.delay(1000);
					await dres.searchDresById();
				});
			});
		});

		describe('管理员下架商品', function () {
			let clientInfo, purRes, styleRes,
				reqRem, info, checkRem;
			before('管理员下架商品', async function () {
				//console.log(`添加的货品是=${JSON.stringify(styleRes)}`);
				await spReq.spSellerLogin();
				styleRes = await spReq.saveDresFull();
				await spReq.spClientLogin();
				clientInfo = _.cloneDeep(LOGINDATA);
				//获取用户默认收货地址
				await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
				//这里es直接用商品id进行搜索，精确查询范围。
				await common.delay(1000);
				const searchRes = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, keyWords: { "id": [styleRes.result.data.spuId] }, tenantId: tenantId, queryType: 0 });
				//console.log(`searchRes=${JSON.stringify(searchRes)}`);

				const getFullForBuyerByUrlRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchRes.result.data.dresStyleResultList[0].detailUrl });
				purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: getFullForBuyerByUrlRes }));//mockStyleRes
				//console.log(`采购订单的是=${JSON.stringify(purRes)}`);
				getFullForBuyerByUrlRes.count = 0;
				await spTrade.emptyCart();
				await spTrade.saveCart(mockJsonParam.cartJson(getFullForBuyerByUrlRes));

				await spAuth.staffLogin();
				await spCommon.offMarket({
					offmarketReason: `spuId:${styleRes.result.data.spuId},商品信息有误`,
					spus: [{
						spuId: styleRes.result.data.spuId,
						tenantId: tenantId,
						unitId: sellerInfo.unitId,
						clusterCode: sellerInfo.clusterCode
					}]
				});
				await spReq.spSellerLogin();
			});

			//这里说加了新的逻辑导致同步变慢，所以延迟加大点
			it('1.卖家查询商品详情', async function () {
				this.retries(2);
				await common.delay(2000);
				//console.log(`=${JSON.stringify(styleRes.params.jsonParam.spu.title)}`);
				const res = await spdresb.getFullById({ id: styleRes.result.data.spuId });

				// console.log(`商品详细信息=${JSON.stringify(res)}`);
				expect(res.result.data.spu.flag, `下线后商品的flag还是没有变化，${styleRes.result.data.spuId}`).to.be.equal(-2);

				//expect(res.result.data.spu.offmarketReason, `商品spu.offmarketReason错误`).to.equal(`spuId:${styleRes.result.data.spuId},商品信息有误`);
			});

			it('2.卖家查询商品列表', async function () {
				const res = await spdresb.findSellerSpuList({ pageSize: 20, nameLike: styleRes.params.jsonParam.spu.code, flag: -2 });
				let info = res.result.data.rows.find(element => element.id == styleRes.result.data.spuId);
				expect(info.flag, '商品下架后列表中的flag未改变').to.be.equal(-2);
			});

			//这里 购物车  无效的商品flag都是 -4，为了兼容老版本的客户端
			it('3.购物车列表查看', async function () {
				await spReq.spClientLogin();
				let cartList = await spTrade.showCartList();
				cartList.result.data.rows[0].carts.forEach(res =>
					expect(res.spuFlag).to.be.equal(-4));
			});

			it('4.es商品搜索商品', async function () {
				//	console.log(`styleRes=${JSON.stringify(styleRes)}`);
				let flag = false;
				for (let i = 0; i < 10; i++) {
					await common.delay(1000);
					const searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', searchToken: styleRes.params.jsonParam.spu.name, tenantId: tenantId });
					let res = searchRes.result.data.dresStyleResultList.find(element =>
						element.name == styleRes.params.jsonParam.spu.name);
					if (res == undefined) {
						flag = true;
						break;
					};
				};
				expect(flag, '商品下架后es依然可以搜到此商品').to.be.true;
			});

			it('5.下架后，已经生成的采购单后续操作', async function () {
				await spReq.spClientLogin();
				let payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
				await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
				await spReq.spSellerLogin();
				await common.delay(500);

				const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
				const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
				const qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
				const details = qfRes.result.data.skus.map((sku) => {
					return { salesDetailId: sku.id, num: sku.skuNum };
				});
				const logisList = await spconfb.findLogisList1({ cap: 1 });
				await spTrade.deliverSalesBill({
					main: {
						logisCompId: logisList.result.data.rows[0].id,
						waybillNo: common.getRandomNumStr(12),
						buyerId: clientInfo.tenantId,
						shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
						hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
					},
					details: details
				});
				await spReq.spClientLogin();
				await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
			});

			it('6.卖家发起重新上架申请', async function () {
				await spReq.spSellerLogin();
				reqRem = '已整改，希望重新上架！'
				await spdresb.saveOnMarketReq({
					spuIds: styleRes.result.data.spuId, reqRem
				});
				//console.log(`onMarketReqId=${JSON.stringify(onMarketReqId)}`);
			});

			//状态(2 待审核，3 审核通过，99 审核不通过)
			it('7.卖家查看申请列表', async function () {
				await common.delay(500);
				//console.log(`=${JSON.stringify(styleRes.params.jsonParam.spu.title)}`);
				const reqList = await spdresb.findOnMarketReqList({ flag: 2, pageSize: 20, pageNo: 1, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
				//console.log(`=${JSON.stringify(reqList)}`);
				info = reqList.result.data.rows.find(res => res.spuTitle == styleRes.params.jsonParam.spu.title);
				if (info == undefined) {
					throw new Error('申请上架的列表中没有刚刚上架的商品,spuid=' + styleRes.result.data.spuId);
				} else {
					let exp = onMarketListExp({ id: info.id, styleRes, flag: 2, sellerInfo, reqRem });
					common.isApproximatelyEqualAssert(exp, info);
				};
			});

			it('8.卖家查看申请详情', async function () {
				const reqDetail = await spdresb.findOnMarketReqDetail({ id: info.id });
				let exp = onMarketDetailExp({ id: info.id, styleRes, flag: 2, reqRem, sellerInfo })
				common.isApproximatelyEqualAssert(exp, reqDetail);
			});

			it('9.管理员查看申请列表', async function () {
				await spAuth.staffLogin();
				const reqListByAdmin = await spCommon.findOnMarketReqListByAdmin({ flag: 2, pageSize: 20, pageNo: 1, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
				let spuInfo = reqListByAdmin.result.data.rows.find(res => res.id == info.id);
				if (spuInfo == undefined) {
					throw new Error('申请上架的列表中没有刚刚上架的商品,spuid=' + styleRes.result.data.spuId);
				} else {
					let exp = onMarketListExp({ id: info.id, styleRes, flag: 2, sellerInfo, reqRem });
					common.isApproximatelyEqualAssert(exp, spuInfo);
				};
			});

			it('10.管理员查看申请详情', async function () {
				const reqDetailByAdmin = await spCommon.findOnMarketReqDetailByAdmin({ id: info.id });
				let exp = onMarketDetailExp({ id: info.id, styleRes, flag: 2, reqRem, sellerInfo })
				common.isApproximatelyEqualAssert(exp, reqDetailByAdmin);
			});

			it('11.管理员审核', async function () {
				checkRem = '符合上传条件，审核通过';
				await spCommon.checkOnMarketReq({
					id: info.id,
					checkRem,
					flag: 3 //通过
				});
			});

			it('12.审核通过后管理员查看申请列表', async function () {
				await common.delay(2000);
				let reqListByAdmin = await spCommon.findOnMarketReqListByAdmin({ flag: 3, pageSize: 20, pageNo: 1, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
				let spuInfo = reqListByAdmin.result.data.rows.find(res => res.id == info.id);
				if (spuInfo == undefined) {
					throw new Error('申请上架的列表中没有刚刚上架的商品,spuid=' + styleRes.result.data.val);
				} else {
					let exp = onMarketListExp({ id: info.id, styleRes, flag: 3, sellerInfo, checkRem, reqRem });
					common.isApproximatelyEqualAssert(exp, spuInfo);
				};
			});

			it('13.管理员查看申请详情', async function () {
				const reqDetailByAdmin = await spCommon.findOnMarketReqDetailByAdmin({ id: info.id });
				let exp = onMarketDetailExp({ id: info.id, styleRes, flag: 3, checkRem, reqRem, sellerInfo })
				common.isApproximatelyEqualAssert(exp, reqDetailByAdmin);
			});

			it('14.审核通过后卖家查看列表', async function () {
				await spReq.spSellerLogin();
				const reqList = await spdresb.findOnMarketReqList({ flag: 3, pageSize: 20, pageNo: 1, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });  //{ proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate(), flag: 2 }
				let spuInfo = reqList.result.data.rows.find(res => res.id == info.id);
				if (spuInfo == undefined) {
					throw new Error('申请上架的列表中没有刚刚上架的商品,spuid=' + styleRes.result.data.spuId);
				} else {
					let exp = onMarketListExp({ id: info.id, styleRes, flag: 3, sellerInfo, checkRem, reqRem });
					common.isApproximatelyEqualAssert(exp, spuInfo);
				};
			});

			it('15.审核通过后卖家查看申请详情', async function () {
				const reqDetail = await spdresb.findOnMarketReqDetail({ id: info.id });
				let exp = onMarketDetailExp({ id: info.id, styleRes, flag: 3, checkRem, reqRem, sellerInfo })
				common.isApproximatelyEqualAssert(exp, reqDetail);
			});

			it('16.重新上架后es搜索', async function () {
				await spReq.spClientLogin();
				let flag = false;
				for (let i = 0; i < 10; i++) {
					await common.delay(1000);
					const searchRes = await spdresb.searchDres({ pageSize: 20, pageNo: 1, keyWords: { "id": [styleRes.result.data.spuId] }, queryType: 4 });
					if (searchRes.result.data.dresStyleResultList.length > 0);
					flag = true;
					break;
				};
				expect(flag, '重新上架之后es搜不到').to.be.true;
			});
		});
	});

	//新增商品 spu.pubPrice超过5000自动下架
	describe('商品自动下架-online', async function () {
		before(async function () {
			await spReq.spSellerLogin();
		});
		it('pubPrice>5000', async function () {
			let dresJson = mockJsonParam.styleJson({ classId });
			dresJson.spu.pubPrice = 6000;
			const styleRes = await spdresb.saveFull(dresJson);
			const res = await spdresb.getFullById({ id: styleRes.result.data.spuId });
			expect(res.result.data.spu.flag, `spuId=${styleRes.result.data.spuId} 价格超过5000,商品状态错误`).to.be.equal(-2);
		});
		it('pubPrice=5000', async function () {
			let dresJson = mockJsonParam.styleJson({ classId });
			dresJson.spu.pubPrice = 5000;
			dresJson.skus.forEach(sku => sku.pubPrice = 5000);
			const styleRes = await spdresb.saveFull(dresJson);
			const res = await spdresb.getFullById({ id: styleRes.result.data.spuId });
			expect(res.result.data.spu.flag, `spuId=${styleRes.result.data.spuId} 价格等于5000,商品状态错误`).to.be.equal(1);
		});
		it('pubPrice<5000', async function () {
			let dresJson = mockJsonParam.styleJson({ classId });
			dresJson.spu.pubPrice = 200;
			const styleRes = await spdresb.saveFull(dresJson);
			const res = await spdresb.getFullById({ id: styleRes.result.data.spuId });
			expect(res.result.data.spu.flag, `spuId=${styleRes.result.data.spuId} 价格小于5000,商品状态错误`).to.be.equal(1);
		});
		it('上架商品 库存=0', async function () {
			const dresJson = mockJsonParam.styleJson({ classId });
			dresJson.skus.forEach(sku => sku.num = 0);
			const styleRes = await spdresb.saveFull(dresJson);
			const dresInfo = await spdresb.getFullById({ id: styleRes.result.data.spuId }).then(res => res.result.data);
			expect(dresInfo.spu).to.includes({ marketFailure: '库存不足', flag: 2, markFlag: 0 });
		});
		it('调整库存变为0', async function () {
			const styleRes = await spdresb.saveFull(mockJsonParam.styleJson({ classId }));
			const skus = styleRes.result.data.skuIds.map((sku, index) => {
				return { id: sku.skuId, num: -styleRes.params.jsonParam.skus[index].num }
			});
			await spdresb.addDresSkuNum({ skus });

			const dresInfo = await spdresb.getFullById({ id: styleRes.result.data.spuId }).then(res => res.result.data);
			expect(dresInfo.spu).to.includes({ markFlag: 0, flag: 1 });
		});
	});

	// http://zentao.hzdlsoft.com:6082/zentao/task-view-1416.html
	// 新增商品->自动审核->[人工审核]->商品上架
	// 不符合自动审核规则 不可进行人工审核
	// 审核通过的商品 切换审核状态后 不会重新审核
	describe('商品上架前审核功能', async function () {
		let sellerTenantId;
		before(async function () {
			await spReq.spSellerLogin({ shopName: '中洲店' });
			sellerTenantId = LOGINDATA.tenantId;

			// 修改审核状态 人工审核
			await spAuth.staffLogin();
			await spugr.updateShopSpuCheckFlag({ id: sellerTenantId, checkFlag: 1 });
		});
		after(async function () {
			// 修改审核状态 自动审核 防止影响其他用例
			await spAuth.staffLogin();
			await spugr.updateShopSpuCheckFlag({ id: sellerTenantId, checkFlag: 0 });
		});
		describe('新增商品-符合自动审核条件', async function () {
			let dres;
			before(async function () {
				await spReq.spSellerLogin({ shopName: '中洲店' });

				const json = mockJsonParam.styleJson({ classId });
				dres = await spdresb.saveFull(json).then(res => res.result.data);
			});
			it('保存商品完整信息', async function () {
				expect(dres).to.includes({ spFlag: 2, marketFailure: '当前商品处于上架审核中' });
			});
			it('卖家查询商品列表', async function () {
				const dresList = await spdresb.findSellerSpuList({ flags: [2], orderBy: 'createdDate', orderByDesc: true, }).then(res => res.result.data.rows);
				const dresDetail = dresList.find(data => data.id == dres.spuId);
				expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${dres.spuId}`).not.to.be.undefined;
				expect(dresDetail).to.includes({ flag: 2, checkFlag: 2 });
			});
			it('全局商品搜索', async function () {
				const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
				const dresSpu = dresList.find(data => data.id == dres.spuId);
				expect(dresSpu, `全局商品搜索 查询到未审核通过的商品 spuId=${dres.spuId}`).to.be.undefined;
			});
			describe('设置商品审核状态-去除审核状态', async function () {
				before(async function () {
					await spdresb.updateSpuCheckFlag({
						tenantId: LOGINDATA.tenantId,
						unitId: LOGINDATA.unitId,
						id: dres.spuId,
						flag: 0
					});
				});
				it('卖家查询商品列表', async function () {
					const dresList = await spdresb.findSellerSpuList({ flags: [2], orderBy: 'createdDate', orderByDesc: true, }).then(res => res.result.data.rows);
					const dresDetail = dresList.find(data => data.id == dres.spuId);
					expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${dres.spuId}`).not.to.be.undefined;
					expect(dresDetail).to.includes({ flag: 2, checkFlag: 2 });
				});
				it('全局商品搜索', async function () {
					const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
					const dresSpu = dresList.find(data => data.id == dres.spuId);
					expect(dresSpu, `全局商品搜索 查询到未审核通过的商品 spuId=${dres.spuId}`).to.be.undefined;
				});
			});
			describe('设置商品审核状态-已审核', async function () {
				before(async function () {
					await spdresb.updateSpuCheckFlag({
						tenantId: LOGINDATA.tenantId,
						unitId: LOGINDATA.unitId,
						id: dres.spuId,
						flag: 1
					});
				});
				it('卖家查询商品列表', async function () {
					const dresList = await spdresb.findSellerSpuList({ flags: [1], orderBy: 'createdDate', orderByDesc: true, }).then(res => res.result.data.rows);
					const dresDetail = dresList.find(data => data.id == dres.spuId);
					expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${dres.spuId}`).not.to.be.undefined;
					expect(dresDetail).to.includes({ flag: 1, checkFlag: 1 });
				});
				it('全局商品搜索', async function () {
					this.retries(2);
					await common.delay(500);
					const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
					const dresSpu = dresList.find(data => data.id == dres.spuId);
					expect(dresSpu, `全局商品搜索 查询不到审核通过的商品 spuId=${dres.spuId}`).not.to.be.undefined;
				});
			});
		});
		describe('新增商品-不符合自动审核条件', async function () {
			let dres;
			before(async function () {
				await spReq.spSellerLogin({ shopName: '中洲店' });

				const json = mockJsonParam.styleJson({ classId });
				json.spu.pubPrice = 0;
				dres = await spdresb.saveFull(json).then(res => res.result.data);

				await spdresb.updateSpuCheckFlag({
					tenantId: LOGINDATA.tenantId,
					unitId: LOGINDATA.unitId,
					id: dres.spuId,
					flag: 1
				});
			});
			after(async function () {
				await spdresb.offMarket({ ids: [dres.spuId] });
			});
			it('保存商品完整信息', async function () {
				expect(dres).to.includes({ spFlag: 2, marketFailure: '发布价未大于0' });
			});
			it('卖家查询商品列表', async function () {
				const dresList = await spdresb.findSellerSpuList({ flags: [2], orderBy: 'createdDate', orderByDesc: true, }).then(res => res.result.data.rows);
				const dresDetail = dresList.find(data => data.id == dres.spuId);
				expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${dres.spuId}`).not.to.be.undefined;
				expect(dresDetail).to.includes({ flag: 2, checkFlag: 1 });
			});
			it('全局商品搜索', async function () {
				const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.spuId] } }).then(res => res.result.data.dresStyleResultList);
				const dresSpu = dresList.find(data => data.id == dres.spuId);
				expect(dresSpu, `全局商品搜索 查询到未审核通过的商品 spuId=${dres.spuId}`).to.be.undefined;
			});
		});
	});

});

//5分钟一次同步商品信息到es中
//需要先从日志库取最后同步时间
//select * from `slhlog`.`mylog` where `serverid` LIKE '%spdresb%' AND `classname` = 'com.hzecool.sp.biz.dres.service.RefreshDresTask'  order by `optime` desc
describe.skip('es同步', function () {
	it('', async function () {
		//styleRes.result.data.spuId
		// const res = await spdresb.searchDres({ pageSize: 10, pageNo: 1, jsonParam: { "keyWords": { "id": [8654] } } });
		// console.log(`res=${JSON.stringify(res)}`);
	});
});

function onMarketListExp(params) {
	let exp = {
		id: params.id,
		spuTitle: params.styleRes.params.jsonParam.spu.title,
		reqRem: params.reqRem,
		flag: params.flag,
		checkRem: params.checkRem ? params.checkRem : '',
		sellerId: params.sellerInfo.tenantId,
		sellerUnitId: params.sellerInfo.unitId
	};
	return exp;
};

function onMarketDetailExp(params) {
	let exp = {
		main: {
			flag: params.flag,
			checkRem: params.checkRem ? params.checkRem : '',
			sellerId: params.sellerInfo.tenantId,
			sellerUnitId: params.sellerInfo.unitId,
			reqRem: params.reqRem,
			spuTitle: params.styleRes.params.jsonParam.spu.title
		}, details:
			[{
				spuId: params.styleRes.result.data.spuId,
				spuTitle: params.styleRes.params.jsonParam.spu.title,
				reqId: params.id
			}]
	};
	return exp;
};
