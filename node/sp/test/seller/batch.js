const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const esSearch = require('../../reqHandler/sp/biz_server/esSearch');
// const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
// const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
// const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
// const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const configParam = require('../../help/configParamManager');
// const spExp = require('../../help/getExp');
// const format = require('../../../data/format');
// const dresManage = require('../../help/dresManage');
// const caps = require('../../../data/caps');
// const fs = require('fs');
// const path = require('path');

// http://zentao.hzdlsoft.com:6082/zentao/task-view-1407.html
//
// 1. 卖家设置混批由系统参数控制，系统参数code = “sp_maxbatch_flag”,查询
// apiKey = ec-config-pargetParamInfo,jsonParam = { “code”: “sp_maxbatch_flag”, “ownerKind”: 1, “domainKind”: “system” }
// 其中返回val字段 中val = 1代表允许卖家设置混批，val=0代表不允许
// 
// 2，卖家设置全局性混批设置，存储于租户级别系统参数，参数code = sp_max_batchnum，
// apiKey = ec-config-param-saveOwnerVal,
// 举例jsonParam =
// { “ownerKind”: 6, “data”: [{ “ownerId”: 82862, “code”: “sp_max_batchnum”, “domainKind”: “business”, “val”: { “type”: “1”, “mixNum”: “3” } }] }
// val存储格式为json,形如{“type”:”1”,”mixNum “:”2”}其中当type =1代表sku混批量，当type=2代表spu混批量，当type=3代表店铺级别混批量，mixNum代表混批量，若要清除混批直接传””
// 
// 3卖家设置单元性混批设置，即对特定spu,sku设置混批，ec-spdresb-dresSpu-setMinSpuOrSkuNum接口，
describe('混批', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, classInfo, spMaxbatchFlag;
    before(async function () {
        await spAuth.staffLogin();
        spMaxbatchFlag = await configParam.getParamInfo({ code: 'sp_maxbatch_flag', domainKind: 'system' });
        await spMaxbatchFlag.updateParam({ val: 1 });

        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        const classId = 1013;
        const classInfo = await spdresb.findByClass({ classId });
        const arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));

        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };
    });
    after(async function () {
        await spMaxbatchFlag.returnOriginalParam();
    });

    describe('混批设置', async function () {
        let dres;
        before(async function () {
            const res = await spdresb.saveFull(mockJsonParam.styleJson({ classId: 1013 }));
            dres = res.result.data;

            await common.delay(500);
            await spdresb.setMinSpuOrSkuNum({ type: 2, mixNum: 5, id: dres.spuId });
        });
        it('卖家查询商品详情', async function () {
            const dresDetail = await spdresb.getFullById({ id: dres.spuId }).then(res => res.result.data);
            expect(dresDetail.spu).to.includes({ minSpuOrderNum: 5 });
        });
        it('买家查询商品详情', async function () {
            await spReq.spClientLogin();
            const dresDetail = await spdresb.getFullForBuyer({ spuId: dres.spuId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId }).then(res => res.result.data);
            expect(dresDetail.spu).to.includes({ minSpuOrderNum: 5 });
        });
        it('买家下单', async function () {

        });
    });

    describe('批量设置sku混批', async function () {
        let spuId, setParam;
        before(async function () {
            await spReq.spSellerLogin();
            const data = await spdresb.saveFull(mockJsonParam.styleJson({ classId: 1013 })).then(res => res.result.data);
            spuId = data.spuId;
            setParam = data.skuIds.map(sku => {
                return { id: sku.skuId, mixNum: common.getRandomNum(1, 10) };
            });

            await spdresb.batchSetSkuMinNum(setParam);
        });
        it('卖家查询商品详情', async function () {
            const dresDetail = await spdresb.getFullById({ id: spuId }).then(res => res.result.data);
            dresDetail.skus.forEach(sku => {
                expect(sku.minSkuOrderNum).to.equal(setParam.find(_sku => _sku.id == sku.id).mixNum);
            });
        });
    });

});