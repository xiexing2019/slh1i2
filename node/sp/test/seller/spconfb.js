// const spugr = require('../../../reqHandler/sp/global/spugr');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const mockJsonParam = require('../../help/mockJsonParam');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spmdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');



describe('运费管理-online', function () {
    this.timeout(30000);
    let areaJson, sellerInfo;
    before('获取区域信息', async function () {
        areaJson = await mockJsonParam.getAreaJson();
    });

    const feeType = [{ type: 1, name: '运费按件数' }, { type: 2, name: '运费按重量' }];
    const freeType = [{ type: 1, name: '包邮按件数' }, { type: 2, name: '包邮按金额' }];
    for (let m = 0; m < feeType.length; m++) {
        for (let n = 0; n < freeType.length; n++) {
            describe('运费流程', function () {
                let saveRes, lastProv;
                before('清空运费模板', async function () {
                    await spReq.spSellerLogin();
                    sellerInfo = LOGINDATA;
                    await spReq.saveShipRule({ feeType: 0, freeType: 0 });  //设置卖家包邮，为了清空运费模板，做到初始化
                });

                it(`1.卖家设置规则-${feeType[m].name}-${freeType[n].name}`, async function () {
                    saveRes = await spReq.saveShipRule(saveRuleJson({ feeType: feeType[m].type, freeType: freeType[n].type, areaJson }));
                });

                it('2.卖家查询运费规则', async function () {
                    let shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, shipRuleList);
                });

                it('3.卖家修改运费规则', async function () {
                    let shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                    //将查询的列表结果作为添加运费规则的数据。
                    let updateJson = _.cloneDeep(shipRuleList);
                    let provIdsArr = updateJson.feeRules[0].provIds.split(',');   //provids 转为数组
                    lastProv = provIdsArr.pop();    //取最后的一个元素，作为删除掉的元素
                    updateJson.feeRules[0].provIds = provIdsArr.join();   //转为字符串，这里相当于将以前的省份最后一个省份去除
                    updateJson.feeRules[0].addNum += 10;
                    updateJson.feeRules[0].addFee += 10;

                    //修改运费规则,这里是去掉了运费规则中省份中的最后一个省份,递增数量和费用各自+10
                    let updateRes = await spReq.saveShipRule(updateJson);
                    let res = await spconfb.findShipRule().then(res => res.result.data);
                    //因为这里添加时候的参数没有将ecCaption去除，只是将provids最后一个省份去除了，所以这里忽略ecCaption
                    common.isApproximatelyEqualAssert(updateRes.params.jsonParam, res, ['ecCaption']);
                });

                it('4.卖家在原有的基础上添加一个运费规则', async function () {
                    let shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                    let updateJson = _.cloneDeep(shipRuleList);
                    let element = _.cloneDeep(updateJson.feeRules[0]);
                    delete element.id;
                    element.provIds = lastProv;
                    updateJson.feeRules.push(element);
                    let updateRes = await spReq.saveShipRule(updateJson);
                    let res = await spconfb.findShipRule().then(res => res.result.data);
                    common.isApproximatelyEqualAssert(updateRes.params.jsonParam, res, ['ecCaption', 'ecSeq']);
                });

                it('5.添加相同的运费', async function () {
                    let shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                    let updateJson = _.cloneDeep(shipRuleList);
                    updateJson.feeRules[1].provIds = updateJson.feeRules[0].provIds;
                    delete updateJson.feeRules[1].id;
                    updateJson.check = false;
                    let res = await spReq.saveShipRule(updateJson);
                    expect(res.result).to.includes({ "msgId": "provId_exist" });
                });

                describe('运费计算', function () {
                    let styleRes, purJson, shipRuleList, provinceCode;
                    before('获取运费计算相关数据', async function () {
                        await spReq.spClientLogin();
                        buyerTenantId = LOGINDATA.tenantId;

                        searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerInfo.tenantId });
                        styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchRes.result.data.dresStyleResultList[0].detailUrl });
                        //构造计算用费接口使用的数据
                        purJson = mockJsonParam.purJson({ styleInfo: styleRes });
                    });

                    it('1.卖家给商品设置运输属性，为了和运费规则统一', async function () {
                        await spReq.spSellerLogin();
                        await saveShipProps(styleRes, 0, feeType[m].type);
                    });

                    it('2.满足包邮时，fee检测', async function () {
                        await spReq.spSellerLogin();
                        sellerInfo = LOGINDATA;
                        shipRuleList = await spconfb.findShipRule().then(res => res.result.data);
                        provinceCode = shipRuleList.feeRules[0].provIds.split(',')[0];   //取第一个地址作为收货地址
                        await spReq.spClientLogin();
                        let orderSpus;
                        //如果包邮是满件数
                        if (shipRuleList.freeRules[0].freeType == 1) {
                            orderSpus = getOrderSpus(purJson, shipRuleList.freeRules[0].limitNum, 100);
                            let fee = await spconfb.evalShipFee({ provinceCode: provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus }] });
                            // console.log(`fee=${JSON.stringify(fee)}`);
                            expect(fee.result.data.fees[0].fee, `满足运费时运费不是0`).to.be.equal(0);
                        } else {   //包邮按金额  
                            orderSpus = getOrderSpus(purJson, 2, shipRuleList.freeRules[0].limitNum);
                            let fee = await spconfb.evalShipFee({ provinceCode: provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus }] });
                            //console.log(`\nfee=${JSON.stringify(fee.result.data.fees[0].fee)}`);
                            expect(fee.result.data.fees[0].fee, `满足运费时运费不是0`).to.be.equal(0);
                        };
                    });

                    it('3.不满足包邮时，检查运费', async function () {
                        await spReq.spSellerLogin();
                        let perNum = await spdresb.getShipProp({
                            objId: styleRes.result.data.spu.classId,
                            objType: 0
                        }).then(res => res.result.data.perNum);
                        await spReq.spClientLogin();
                        if (shipRuleList.freeRules[0].freeType == 1) {
                            let orderSpus = getOrderSpus(purJson, 1, 100);   //这一步是为了设置让订单不符合免运费
                            let fee = await spconfb.evalShipFee({ provinceCode: provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus }] });
                            let exp = getFree({ orderSpus, role: shipRuleList.feeRules[0], perNum });
                            expect(fee.result.data.fees[0].fee, `运费计算错误，${JSON.stringify(fee)},\n${JSON.stringify(shipRuleList)}`).to.be.equal(exp);
                        } else {
                            let orderSpus = getOrderSpus(purJson, 1, 1);  //这一步是为了设置让订单不符合免运费
                            let fee = await spconfb.evalShipFee({ provinceCode: provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus }] });
                            let exp = getFree({ orderSpus, role: shipRuleList.feeRules[0], perNum });
                            expect(fee.result.data.fees[0].fee, `运费计算错误，${JSON.stringify(fee)},\n${JSON.stringify(shipRuleList)}`).to.be.equal(exp);
                        };
                    });
                });
            });
        };
    };

    // task-2815 APP端商家包邮标签根据商家商陆花-商陆好店-邮费设置的卖家包邮
    // 目前自动设置 只会影响到店铺标签 不会影响到商品
    describe('包邮标签自动设置', async function () {
        let tenantId;
        describe('店铺设置包邮', async function () {
            before(async function () {
                await spReq.spSellerLogin();
                tenantId = LOGINDATA.tenantId;
                await spReq.saveShipRule({ feeType: 0, freeType: 0 });
            });
            it('获取店铺详情', async function () {
                await spReq.spClientLogin();
                const shopInfo = await spugr.getShopSpb({ id: tenantId }).then(res => res.result.data);
                expect(shopInfo.ecCaption.labels).to.includes('包邮');
            });
            it('搜索商品-查看标签', async function () {
                const dresList = await spdresb.searchDres({ queryType: 0, tenantId: tenantId, pageNo: 1, pageSize: 5 })
                    .then(res => res.result.data.dresStyleResultList);

                const dresDetail = await spReq.getFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);
                // console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
                expect(dresDetail.spu.ecCaption).to.includes({ shipFeeWayId: '包邮' });
            });
        });
        describe('店铺设置不包邮', async function () {
            before(async function () {
                await spReq.spSellerLogin();
                await spReq.saveShipRule(saveRuleJson({ feeType: 1, freeType: 1, areaJson }));
            });
            it('获取店铺详情', async function () {
                await spReq.spClientLogin();
                const shopInfo = await spugr.getShopSpb({ id: tenantId }).then(res => res.result.data);
                expect(shopInfo.ecCaption.labels).not.to.includes('包邮');
            });
            it.skip('搜索商品-查看标签', async function () {
                const dresList = await spdresb.searchDres({ queryType: 0, tenantId: tenantId, pageNo: 1, pageSize: 5 })
                    .then(res => res.result.data.dresStyleResultList);

                const dresDetail = await spReq.getFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);
                console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
                expect(dresDetail.spu.ecCaption).not.to.includes({ shipFeeWayId: '包邮' });
            });
        });
    });



});


function getFree(params) {
    const { orderSpus, role, perNum } = params;
    let totalNum = 0;
    orderSpus.forEach(element => totalNum += element.orderNum);
    if (role.feeType == 2) {
        totalNum = totalNum * perNum;
    };

    let fee = 0;
    if (totalNum <= role.startNum) {
        fee = role.startFee;
    } else {
        fee = role.startFee + Math.ceil((totalNum - role.startNum) / role.addNum) * role.addFee;//perNum
    };
    return fee;
};

function getJson(element) {
    let rules = [];
    for (let i = 0; i < element.length; i++) {
        info = {};
        info.id = element[i].id;
        info.feeType = element[i].feeType;
        info.provIds = element[i].provIds;
        info.startNum = element[i].startNum;
        info.startFee = element[i].startFee;
        info.addNum = element[i].addNum;
        info.addFee = element[i].addFee;
        rules.push(info);
    };
    return rules;
};

/**
 * 保存规则
 * @param {object} styleRes 保存商品json
 * @param {Integer} objType 对象类型 0-类别 1-款号
 * @param {feeType} 计费类型  1-计件 2-计重 3-计体积，默认计重2
*/
async function saveShipProps(styleRes, objType, feeType) {
    let objId, perNum;
    if (objType == 0) {
        objId = styleRes.result.data.spu.classId;
        perNum = common.getRandomNum(0.5, 1);
    } else {
        objId = styleRes.result.data.spu.id;
        perNum = common.getRandomNum(2, 8)
    }
    return await spdresb.saveShipProps({
        shipProps: [{
            objId: objId,//
            objType: objType,
            feeType: feeType,
            perNum: perNum
        }]
    });
};

/**
 * 保存运费、包邮规则
*/
function saveRuleJson(params) {
    let json = {
        feeType: params.feeType,
        feeRules: [{
            feeType: params.feeType,
            provIds: params.areaJson.proviceArr.slice(0, 5).join(),
            startNum: common.getRandomNum(5, 10),
            startFee: common.getRandomNum(5, 15),
            addNum: common.getRandomNum(2, 5),
            addFee: common.getRandomNum(5, 10),
        }],
        freeType: params.freeType,
        freeRules: [{
            freeType: params.freeType,
            provIds: params.areaJson.proviceArr.slice(0, 5).join(),
            limitNum: common.getRandomNum(20, 25),
        }],
    };
    return json;
};

/**
 * 估算运费中 orderSpus获取
 * @param {Object} purJson 采购订单的JSON
 * @param {orderNum} 订货的数量
 * @param {orderMoney} 订货的总金额 
*/
function getOrderSpus(purJson, orderNum, orderMoney) {
    let arr = purJson.orders[0].details.map((detail) => {
        return {
            spuId: detail.spuId,
            orderNum,   //detail.num订货数量
            orderMoney  //订货金额
        }
    });
    return arr;
}
