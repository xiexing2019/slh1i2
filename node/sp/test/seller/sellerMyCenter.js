const common = require('../../../lib/common');
// const spugr = require('../../../reqHandler/sp/global/spugr');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spReq = require('../../help/spReq');
// const spCaps = require('../../../reqHandler/sp/spCaps');
// const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
// const mockJsonParam = require('../../help/mockJsonParam');
// const spAuth = require('../../../reqHandler/sp/confc/spAuth');


describe('卖家个人中心-online', function () {
    this.timeout(30000);
    before(async () => {
        //卖家登录
        await spReq.spSellerLogin();
    });
    describe('卖家订单数量统计', function () {
        it('卖家订单数量统计', async () => {
            let countExp = {};
            //totalNum总订单数  toDelivelNum待发货订单数 toPayNum 待付款订单数  toReceiveNum 待收货订单
            const statusType = { 0: 'totalNum', 2: 'toDelivelNum', 1: 'toPayNum', 3: 'toReceiveNum', };
            for (const key of Object.keys(statusType)) {
                await spTrade.salesFindBills({ pageSize: 0, statusType: key }).then(res => countExp[statusType[key]] = res.result.data.total);
            };
            //backingNum 退货中 
            //backFlag 10--买家申请退款 11--买家申请退货退款
            let pageTotal = 0;
            const dataList = await spTrade.salesFindBills({ pageSize: 20, pageNo: 1, statusType: 5 }).then(res => {
                pageTotal = Math.ceil(res.result.data.total / 20);
                return res.result.data.rows;
            });
            for (let index = 2; index < pageTotal; index++) {
                await spTrade.salesFindBills({ pageSize: 20, pageNo: index, statusType: 5 }).then(res => dataList.push(...res.result.data.rows));
            }

            countExp.backingNum = common.takeWhile(dataList, (obj) => obj.bill.backFlag == 10 || obj.bill.backFlag == 11).length;
            const billCountForCilent = await spTrade.findBillsCountForSeller();
            common.isApproximatelyEqualAssert(countExp, billCountForCilent.result.data);
        });
    });

});