const spugr = require('../../../reqHandler/sp/global/spugr');
// const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
// const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const mockJsonParam = require('../../help/mockJsonParam');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
// const spmdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const format = require('../../../data/format');
const spBank = require('../../data/spBank');
const caps = require('../../../data/caps');

// 先审核商户，商户审核成功，flag=4，失败=0，成功后，在审核账户，成功1，
// mobile detailAddr  mchName contact 属于商户信息，其他属于账户信息
// 商户账户 是定时器定时提交到清分系统进行审核的，定时器是5分钟1次，这里暂时没管 flag为3 6 所以状态未覆盖
// spugr sp_merchant

// 需要商陆花帐套 等世国配置好
describe.skip('门店商户', function () {
    this.timeout(30000);
    //这里最后一步保证审核账户都不通过，为了数据还原操作。

    after('切回环境', async function () {
        caps.updateEnvByName({ name: caps.envName })
    });

    describe('主流程：提交审核---审核商户通过---商户账户都审核通过', function () {
        let saveRes, merchantInfoBefore, shopInfo;
        before('通过slh登录好店卖家并且修改商户信息', async function () {
            await spReq.loginGoodShopBySlh();
            shopInfo = await spugr.getShopDetail();
            merchantInfoBefore = await spugr.findMerchant().then(res => res.result.data);

            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 5 });
            await spReq.loginGoodShopBySlh();
            LOGINDATA.mobile = merchantInfoBefore.mobile;      //初始的手机号存到LOGINDATA
            LOGINDATA.userName = merchantInfoBefore.contact;   //初始的用户名存到LOGINDATA
            //更新门店的商户信息
            saveRes = await spugr.updateMerchant(mockJsonParam.merchantJson({ id: merchantInfoBefore.id }));
        });

        after('数据还原，审核不通过', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 5, check: false });
        });

        //待审核  flag=2 表示已经提交到本地 
        it('1.提交审核后状态检查', async function () {
            const res = await spugr.findMerchant().then(res => res.result.data);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, res);
            expect(res.flag, '提交后管理员未审核时flag错误').to.be.equal(2);
        });

        it('2.提交后管理员查看列表', async function () {
            await spAuth.staffLogin();
            const res = await spCommon.findMerchantByAdmin({ settleAcctNo: saveRes.params.jsonParam.settleAcctNo, shopName: shopInfo.result.data.name });
            let info = res.result.data.rows.find(element => element.id == saveRes.result.data.val);
            //这里字段不统一，提交的时候的detailAddr对应的是管理员列表的mcmAddr 字段的
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, info, ['detailAddr']);
            expect(saveRes.params.jsonParam.detailAddr).to.be.equal(info.mchAddr);
            expect(info.flag, '提交后管理员未审核时flag错误').to.be.equal(2);
        });

        it('3.提交后管理员查看详情', async function () {
            const info = await spCommon.getMerchantInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, info, ['detailAddr']);
            expect(saveRes.params.jsonParam.detailAddr).to.be.equal(info.mchAddr);
            expect(info.flag, '提交后管理员未审核时flag错误').to.be.equal(2);
        });

        //这里是flag =4 表示只通过商户审核，账户还未审核
        it('4.管理员审核清分商户通过（flag-4）', async function () {
            //管理员审核通过，flag=1. 注：flag=1之后，卖家就不能修改商户信息了。
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 4 });
        });

        it('5.管理员审核清分商户通过后卖家查询商户信息', async function () {
            await spReq.loginGoodShopBySlh();
            const res = await spugr.findMerchant().then(res => res.result.data);
            common.isApproximatelyEqualAssert(res, saveRes.params.jsonParam);
            expect(res.flag, '提交后管理员未审核时flag错误').to.be.equal(4);
        });

        it('6.审核商户通过后管理员查看列表', async function () {
            await spAuth.staffLogin();
            const res = await spCommon.findMerchantByAdmin({ settleAcctNo: saveRes.params.jsonParam.settleAcctNo });
            let info = res.result.data.rows.find(element => element.id == saveRes.result.data.val);
            expect(info.flag, '提交后管理员未审核时flag错误').to.be.equal(4);
        })

        it('7.审核商户通过后管理员查看详情', async function () {
            const info = await spCommon.getMerchantInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, info, ['detailAddr']);
            expect(saveRes.params.jsonParam.detailAddr).to.be.equal(info.mchAddr);
            expect(info.flag, '提交后管理员未审核时flag错误').to.be.equal(4);
        });

        //卖家修改商户信息之后，需要管理员审核，这里审核通过 。 flag =1 商户和账户都审核通过
        it('8.管理员审核通过商户信息', async function () {
            //管理员审核通过，flag=1. 注：flag=1之后，卖家就不能修改商户信息了。
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 1 });
        });

        it('9.审核通过后查询门店商户', async function () {
            await spReq.loginGoodShopBySlh();
            const merchantInfo = await spugr.findMerchant().then(res => res.result.data);
            expect(merchantInfo.flag, 'flag错误').to.be.equal(1);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, merchantInfo);
        });

        it('10.审核通过后管理员查看列表', async function () {
            await spAuth.staffLogin();
            const res = await spCommon.findMerchantByAdmin({ settleAcctNo: saveRes.params.jsonParam.settleAcctNo });
            let info = res.result.data.rows.find(element => element.id == saveRes.result.data.val);
            expect(info.flag, '审核通过后flag错误').to.be.equal(1);
        });

        it('11.审核后管理员查看详情', async function () {
            const res = await spCommon.getMerchantInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, res, ['settleAcctNo', 'detailAddr']);
            expect(res.mchAddr).to.be.equal(saveRes.params.jsonParam.detailAddr);
            let str = saveRes.params.jsonParam.settleAcctNo;
            let exp = `******${str.substring(str.length - 4)}`;  //审核通过后只显示后4位的
            expect(res.settleAcctNo, '卡号可见').to.be.equal(exp);
            expect(res.flag, '提交后管理员未审核时flag错误').to.be.equal(1);
        });

        //商户审核通过后，应该不能再进行修改商户信息
        it('12.商户已经审核通过后修改商户', async function () {
            await spReq.loginGoodShopBySlh();
            LOGINDATA.mobile = merchantInfoBefore.mobile;      //初始的手机号存到LOGINDATA
            LOGINDATA.userName = merchantInfoBefore.contact;   //初始的用户名存到LOGINDATA
            let res = await spugr.updateMerchant(mockJsonParam.merchantJson({ id: merchantInfoBefore.id, check: false }));
            expect(res.result, '商户审核成功后，依然可以修改商户').to.includes({ "msgId": "spugr_merchant_is_auditing_successed_already" });
        });

        //管理员将已经审核通过的商户 修改为审核不通过
        it('13.管理员修改商户信息为不通过', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 5 });
            await spReq.loginGoodShopBySlh();
            const merchantInfo = await spugr.findMerchant().then(res => res.result.data);
            expect(merchantInfo.flag, 'flag错误').to.be.equal(5);
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, merchantInfo);
        });

        //重置门店状态，5--2
        it('14.管理员重置门店状态', async function () {
            await spAuth.staffLogin();
            const res = await spCommon.resetMerchantFlag({ id: merchantInfoBefore.id });
            await spReq.loginGoodShopBySlh();
            const merchantInfo = await spugr.findMerchant().then(res => res.result.data);
            expect(merchantInfo.flag, '重置未将flag从5重置到2，重置到了' + merchantInfo.flag).to.be.equal(2);
        });

        it('15.审核通过', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 1 });
        });
    });

    describe('分支，商户审核通过---账户审核不通过', function () {
        let saveRes, merchantInfoBefore;
        before('通过slh登录好店卖家并且修改商户信息', async function () {
            await spReq.loginGoodShopBySlh();
            merchantInfoBefore = await spugr.findMerchant().then(res => res.result.data);
            LOGINDATA.mobile = merchantInfoBefore.mobile;      //初始的手机号存到LOGINDATA
            LOGINDATA.userName = merchantInfoBefore.contact;   //初始的用户名存到LOGINDATA
            //更新门店的商户信息
            saveRes = await spugr.updateMerchant(mockJsonParam.merchantJson({ id: merchantInfoBefore.id }));
        });

        //用例跑完一遍，保证最后是审核通过的状态
        after('商户账户都审核通过', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 1 });
        });

        //flag=0 商户通过，账户未通过
        it('1.管理员登录，商户审核通过，账户不通过', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 0 });
        });


        it('2.商户审核通过、账户不通过后,卖家查看商户信息', async function () {
            await spReq.loginGoodShopBySlh();
            const res = await spugr.findMerchant().then(res => res.result.data);
            common.isApproximatelyEqualAssert(res, saveRes.params.jsonParam);
            expect(res.flag, '提交后管理员未审核时flag错误').to.be.equal(0);
        });

        it('3.管理员修改卖家的商户信息', async function () {
            await spAuth.staffLogin();
            let res = await spCommon.getMerchantInfo({ id: merchantInfoBefore.id }).then(res => res.result.data);
            let updateParam = updateJson(res);
            let updateRes = await spCommon.updateMerchantByAdmin(updateParam);
            let merchantInfo = await spCommon.getMerchantInfo({ id: merchantInfoBefore.id });

            common.isApproximatelyEqualAssert(updateRes.params.jsonParam, merchantInfo.result.data, ['detailAddr']);
            expect(merchantInfo.result.data.mchAddr).to.be.equal(updateRes.params.jsonParam.detailAddr);
            expect(merchantInfo.result.data.flag).to.be.equal(4);
        });

        it('4.重新提交信息，查询商户', async function () {
            await spAuth.staffLogin();
            await spugr.updateSpMerchatFlagByPerson({ id: merchantInfoBefore.id, flag: 0 });
            await spReq.loginGoodShopBySlh();
            let merchantInfo = await spugr.findMerchant().then(res => res.result.data);
            let exp = {
                detailAddr: merchantInfo.detailAddr,
                contact: merchantInfo.contact,
                mchName: merchantInfo.mchName,
                mobile: merchantInfoBefore.mobile,
            };  //这四个属于商户信息。由于商户已经审核通过，所以再次提交后查询，商户信息还是不变的

            LOGINDATA.mobile = merchantInfo.mobile;      //初始的手机号存到LOGINDATA
            LOGINDATA.userName = merchantInfo.contact;   //初始的用户名存到LOGINDATA
            let json = mockJsonParam.merchantJson({ id: merchantInfo.id });
            json.mobile = '12' + common.getRandomNumStr(9);
            json.mchName = `商户${common.getRandomNumStr(4)}`;
            json.contact = `${common.getRandomChineseStr(3)}`;
            let res = await spugr.updateMerchant(json);
            let LastmerchantInfo = await spugr.findMerchant().then(res => res.result.data);
            common.isApproximatelyEqualAssert(exp, LastmerchantInfo);
            //这几个字段不变，所以不用比较
            common.isApproximatelyEqualAssert(res.params.jsonParam, LastmerchantInfo, ['mobile', 'detailAddr', 'mchName', 'contact']);
        });
    });
});


/**
 * 此方法用来生成修改商户信息的参数
 * @param merchantInfo 商户详情
 * 修改了银行卡号和账户名
*/
function updateJson(merchantInfo) {
    let mapfield = 'contact;settleAcctNo;mchName;mchAbbr;settleAcctName;detailAddr=mchAddr;mobile;settleAcctType;id;bankName;bankCode';
    const json = format.dataFormat(merchantInfo, mapfield);
    let bank = spBank.bank;
    let remove = { name: json.settleAcctName, settleAcctNo: json.settleAcctNo };
    _.remove(bank, remove);
    json.settleAcctName = bank[0].name;
    json.settleAcctNo = bank[0].settleAcctNo;
    json.cncbFlag = 1;
    return json;
};


