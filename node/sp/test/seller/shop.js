const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');

describe('卖家店铺', async function () {
    this.timeout(30000);
    let sellerInfo;

    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('设置退换货规则', async function () {
        const exchangeRuleContent = `7天无理由退换货${common.getRandomStr(5)}`;
        before(async function () {
            await spugr.setShopExchangeRule({ shopId: sellerInfo.shopId, exchangeRuleContent });
        });
        it('获取当前登录的店铺详情接口（包括认证信息）', async function () {
            const shopDetail = await spugr.getShopDetail().then(res => res.result.data);
            expect(JSON.parse(shopDetail.exchangeRule)).to.includes({ exchangeRuleContent: exchangeRuleContent });
        });
        it('店铺多搜索值组合搜索（平台）', async function () {
            const shopList = await spugr.findAllShopByParams({ mobile: sellerInfo.mobile, name: sellerInfo.shopName }).then(res => res.result.data.rows);
            const shopDetail = shopList.find(shop => shop.tenantId == sellerInfo.tenantId);
            expect(JSON.parse(shopDetail.exchangeRule)).to.includes({ exchangeRuleContent: exchangeRuleContent });
        });
        it('买家查询商品详情', async function () {
            await spReq.spClientLogin();

            const dresList = await spdresb.searchDres({ tenantId: sellerInfo.tenantId, queryType: 0, pageSize: 5, pageNo: 1 }).then(res => res.result.data.dresStyleResultList);
            const dresDetail = await spReq.getFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);
            expect(dresDetail.spu).to.includes({ exchangeRuleContent: exchangeRuleContent });
        });
    });


    /**
     *  http://zentao.hzdlsoft.com:6082/zentao/task-view-3355.html
     *  好店APP、微信小程序、好店H5中，如卖家名称有别名的，优先展示别名，没有别名展示真实的卖家名称。
     *  （搜索也要跟着变，有别名的情况下，仅能按别名搜索，不能按真实店铺名称搜索店铺）
     *  此修改不影响卖家端，卖家端仍然显示真实的店铺名称
     * 
     *  店铺名称 name   别名 alias
     *  买家查询时 若含有别名,name返回alias的值
     */
    describe('店铺名称优先显示别名', async function () {
        let alias, dres;
        before(async function () {
            await spReq.spSellerLogin();
        });
        it('卖家查询店铺信息', async function () {
            const shopInfo = await spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
            alias = shopInfo.alias || sellerInfo.shopName;
            expect(shopInfo).to.includes({ name: sellerInfo.shopName });
        });
        it('买家查询店铺信息', async function () {
            await spReq.spClientLogin();
            const shopInfo = await spugr.getShopSpb({ id: sellerInfo.tenantId }).then(res => res.result.data);
            expect(shopInfo).to.includes({ name: alias });
        });
        it('es商品信息中tenantName返回店铺别名', async function () {
            const dresList = await spdresb.searchDres({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            dres = dresList[0];
            expect(dres).to.includes({ tenantName: alias });
        });
        it.skip('买家使用别名搜索商品', async function () {
            // , tenantId: sellerInfo.tenantId
            const dresList = await spdresb.searchDres({ searchToken: alias });
            console.log(`dresList=${JSON.stringify(dresList)}`);
        });
        it('好店列表', async function () {
            const shopList = await spdresb.getGoodShopByBuyer({}).then(res => res.result.data.rows);
            shopList.forEach(shop => shop.alias && expect(shop.name, `名称未取别名 tenantId=${shop.tenantId},name=${shop.name},alias=${shop.alias}`).to.equal(shop.alias));
        });
        it('买家获取已购买过的店铺', async function () {
            const shopList = await spTrade.getRecentBuyShopList({}).then(res => res.result.data.rows);
            shopList.forEach(shop => shop.alias && expect(shop.name, `名称未取别名 tenantId=${shop.tenantId},name=${shop.name},alias=${shop.alias}`).to.equal(shop.alias));
        });
        it('市场查看商品', async function () {
            const shopList = await spdresb.findTradeAreaShop({ type: 1, id: dres.marketId }).then(res => res.result.data.rows);
            shopList.forEach(shop => shop.alias && expect(shop.name, `名称未取别名 tenantId=${shop.tenantId},name=${shop.name},alias=${shop.alias}`).to.equal(shop.alias));
        });
    });




});