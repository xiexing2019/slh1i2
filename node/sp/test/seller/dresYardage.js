const common = require('../../../lib/common');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');

// http://119.3.46.172:6082/zentao/task-view-288.html
describe('尺码表管理', async function () {
    this.timeout(30000);

    describe('尺码表(平台)', async function () {
        let yardage, dresInfo;
        before('新增尺码表', async function () {
            await spAuth.staffLogin();

            const dresClassList = await spDresb.findClassTreeList({ incDirectSub: true }).then(res => res.result.data.rows);
            expect(dresClassList, `全局区商品类别树表查询无数据`).to.have.lengthOf.above(0);

            const dresClassInfo = dresClassList.find(val => val.typeName == '男装') || dresClassList.shift();
            const dresClassId = dresClassInfo.id;

            yardage = await saveOrUpdateYardage({
                classId: dresClassId,
                name: `yardage${common.getRandomStr(6)}`,
                props: { fontSize: 14, lineHeight: 32, backgroundColors: ['#fff'], colors: ['#333'], borderColor: '#999', tableData: [['s', 'm', 'l'], ['30', '31', '32']], des: 'test' },
                docId: { docId: 'docx155002299537691202-docs2.png' }
            });
        });
        it('查询尺码表列表', async function () {
            const yardageList = await spAdmin.findDresYardageList({ name: yardage.name }).then(res => res.result.data.rows);
            const yardageInfo = yardageList.find(val => val.id == yardage.id);
            expect(yardageInfo, `尺码表列表未找到新增的尺码表 id=${yardage.id}`).not.to.be.undefined;
            yardage.isEqualAssert(yardageInfo);
        });
        it('获取单条记录详情', async function () {
            const detailInfo = await spAdmin.getYardageDetailInfo({ id: yardage.id }).then(res => res.result.data);
            yardage.isEqualAssert(detailInfo);
        });
        // 因为想验证删除尺码表后,不影响货品已设置的尺码表 因此直接插在本用例集中
        describe('卖家货品更新尺码图', async function () {
            before(async function () {
                const dresList = await spDresb.searchDres({ queryType: 4 }).then(res => res.result.data.dresStyleResultList);
                dresInfo = dresList.shift();
                await spAdmin.updateSpuYardage({
                    id: dresInfo.id,
                    unitId: dresInfo.unitId,
                    tenantId: dresInfo.tenantId,
                    _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode,
                    yardageProps: Object.assign({
                        name: yardage.name,
                        docId: yardage.docId.docId,
                        flag: 1
                    }, yardage.props)
                });
            });
            it('查询商品详情', async function () {
                const detailInfo = await spDresb.getFullForBuyer({ spuId: dresInfo.id, _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(yardage.props, detailInfo.spu.yardageProps);
            });
        });
        describe('删除单条记录详情', async function () {
            before(async function () {
                await yardage.deleteYardageById();
            });
            it('查询尺码表列表', async function () {
                const yardageList = await spAdmin.findDresYardageList({ name: yardage.name }).then(res => res.result.data.rows);
                const yardageInfo = yardageList.find(val => val.id == yardage.id);
                expect(yardageInfo, `尺码表列表显示已删除的尺码表记录 id=${yardage.id}`).to.be.undefined;
            });
            it('获取单条记录详情', async function () {
                const detailInfo = await spAdmin.getYardageDetailInfo({ id: yardage.id }).then(res => res.result.data);
                yardage.isEqualAssert(detailInfo);
            });
            it('查询商品详情', async function () {
                const detailInfo = await spDresb.getFullForBuyer({ spuId: dresInfo.id, _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(yardage.props, detailInfo.spu.yardageProps);
            });
        });
    });

    describe('尺码表(卖家)', async function () {
        let yardage, dresInfo;
        before('新增尺码表', async function () {
            await spReq.spSellerLogin();

            const dresClassList = await spDresb.findClassTreeList({ incDirectSub: true }).then(res => res.result.data.rows);
            expect(dresClassList, `全局区商品类别树表查询无数据`).to.have.lengthOf.above(0);

            const dresClassInfo = dresClassList.find(val => val.typeName == '男装') || dresClassList.shift();
            const dresClassId = dresClassInfo.id;

            yardage = await saveOrUpdateYardage({
                classId: dresClassId,
                name: `yardage${common.getRandomStr(6)}`,
                props: { fontSize: 14, lineHeight: 32, backgroundColors: ['#fff'], colors: ['#333'], borderColor: '#999', tableData: [['s', 'm', 'l'], ['30', '31', '32']], des: 'test' },
                docId: { docId: 'docx155002299537691202-docs2.png' }
            });
        });
        it('查询尺码表列表', async function () {
            const yardageList = await spDresb.findDresYardageList({ name: yardage.name }).then(res => res.result.data.rows);
            const yardageInfo = yardageList.find(val => val.id == yardage.id);
            expect(yardageInfo, `尺码表列表未找到新增的尺码表 id=${yardage.id}`).not.to.be.undefined;
            yardage.isEqualAssert(yardageInfo);
        });
        it('获取单条记录详情', async function () {
            const detailInfo = await spDresb.getYardageDetailInfo({ id: yardage.id }).then(res => res.result.data);
            yardage.isEqualAssert(detailInfo);
        });
        // 因为想验证删除尺码表后,不影响货品已设置的尺码表 因此直接插在本用例集中
        describe('卖家货品更新尺码图', async function () {
            before(async function () {
                const dresList = await spDresb.searchDres({ queryType: 0, tenantId: LOGINDATA.tenantId }).then(res => res.result.data.dresStyleResultList);
                dresInfo = dresList.shift();
                await spDresb.updateSpuYardage({
                    id: dresInfo.id,
                    unitId: dresInfo.unitId,
                    tenantId: dresInfo.tenantId,
                    _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode,
                    yardageProps: Object.assign({
                        name: yardage.name,
                        docId: yardage.docId.docId,
                        flag: 1
                    }, yardage.props)
                });
            });
            it('查询商品详情', async function () {
                const detailInfo = await spDresb.getFullForBuyer({ spuId: dresInfo.id, _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(yardage.props, detailInfo.spu.yardageProps);
            });
        });
        describe('删除单条记录详情', async function () {
            before(async function () {
                await yardage.deleteYardageById();
            });
            it('查询尺码表列表', async function () {
                const yardageList = await spDresb.findDresYardageList({ name: yardage.name }).then(res => res.result.data.rows);
                const yardageInfo = yardageList.find(val => val.id == yardage.id);
                expect(yardageInfo, `尺码表列表显示已删除的尺码表记录 id=${yardage.id}`).to.be.undefined;
            });
            it('获取单条记录详情', async function () {
                const detailInfo = await spDresb.getYardageDetailInfo({ id: yardage.id }).then(res => res.result.data);
                yardage.isEqualAssert(detailInfo);
            });
            it('查询商品详情', async function () {
                const detailInfo = await spDresb.getFullForBuyer({ spuId: dresInfo.id, _tid: dresInfo.tenantId, _cid: dresInfo.clusterCode }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(yardage.props, detailInfo.spu.yardageProps);
            });
        });
    });

});

class DresYardage {
    constructor(info) {
        /** 主键id */
        this.id = '';
        /** 类别 */
        this.classId = '';
        /** 名称 */
        this.name = '';
        /** 状态：1 有效，0 无效 */
        this.flag = 0;
        /** 扩展字段 */
        this.props = {};
        /** 文档ID */
        this.docId = {};

        common.update(this, info);
        this.props = JSON.parse(this.props);
        this.docId = JSON.parse(this.docId);
    }

    async deleteYardageById() {
        if (LOGINDATA.sessionId.includes('ctr-')) {
            await spAdmin.deleteYardageById({ id: this.id });
        } else {
            await spDresb.deleteYardageById({ id: this.id });
        }
        this.flag = 0;
    }

    isEqualAssert(actual) {
        if (typeof actual.props == 'string') actual.props = JSON.parse(actual.props);
        if (typeof actual.docId == 'string') actual.docId = JSON.parse(actual.docId);

        common.isApproximatelyEqualAssert(this, actual);
    }
};

async function saveOrUpdateYardage(params) {
    let saveRes;
    if (LOGINDATA.sessionId.includes('ctr-')) {
        saveRes = await spAdmin.saveOrUpdateYardage(params);
    } else {
        saveRes = await spDresb.saveOrUpdateYardage(params);
    }
    common.isApproximatelyEqualAssert(saveRes.params.jsonParam, saveRes.result.data);
    return new DresYardage(saveRes.result.data);
};