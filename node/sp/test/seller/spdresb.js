'use strict';
const common = require('../../../lib/common');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');
const dresManage = require('../../help/dresManage');

describe('商品', function () {
    this.timeout(30000);
    let sellerInfo;
    before(async function () {
        // const res = await spdresb.searchDres();
        // console.log(`res=${JSON.stringify(res)}`);

        await spReq.spSellerLogin({ shopName: '中洲店' });
        sellerInfo = _.cloneDeep(LOGINDATA);

    });

    // 商品过多时会影响性能, 因此改为用辅助用的店铺去校验
    // 不会修改sku中的价格
    // 更变价格为0时，则不同步价格 需要再次确认
    // 需要补充 flag!=1的商品价格验证
    describe.skip('卖家变更价格类型到发布价', function () {
        const dres = dresManage.setupDres();
        const priceType = common.randomSort([1, 2, 3, 4, 5]).shift();
        before(async function () {
            this.test.isWarn = true;
            const spuInfo = await spdresb.findSellerSpuList({ orderBy: 'createdDate', orderByDesc: true, flag: 1 })
                .then(res => res.result.data.rows.shift());
            const dresFull = await spdresb.getFullById({ id: spuInfo.id }).then(res => res.result.data);
            dres.setBySeller(dresFull);
        });
        for (let discountType = 0; discountType < 3; discountType++) {
            let changeFlag = false;
            it(`更变priceType为${priceType},discountType为${discountType}`, async function () {
                this.test.isWarn = true;
                await dres.changePubPriceByPriceType({ priceType, discount: common.getRandomNum(0.8, 1, 2), discountType });//
                changeFlag = true;// 若改价失败 es商品价格也不需要校验

                const dresFull = await spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(dres.spu, dresFull.spu);
            });
            it('es商品价格校验', async function () {
                if (!changeFlag) this.skip();
                this.test.isWarn = true;
                this.retries(3);
                await common.delay(1000);//es更新有延迟

                const dresFull = await spdresb.searchDres({ pageSize: 5, pageNo: 1, keyWords: { id: [dres.id] }, queryType: 4 })
                    .then(res => res.result.data.dresStyleResultList.find(dresInfo => dresInfo.id == dres.id));
                common.isApproximatelyEqualAssert(dres.spu, dresFull, ['docHeader', 'labels']);
            });
        };

    });

    describe('卖家设置常用类别', async function () {
        let likeClass = [];
        before(async function () {
            const classList = await spdresb.findShipPropsClassTree({}).then(res => res.result.data.rows);
            if (classList.length < 1) {
                console.warn(`类别树无数据,请检查`);
                this.skip();
            }
            likeClass = classList.slice(0, 3).map(val => val.id);

            await spdresb.saveLikeClass({ codes: likeClass.join(',') });
        });
        it('获取常用商品类别', async function () {
            const classList = await spdresb.findLikeClass({}).then(res => res.result.data.rows);
            common.isArrayContainObjectAssert(classList.map(val => val.id), likeClass);
        });
    });

    describe('作废店铺所有商品', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await spugr.cancelAllDresSpu({ id: sellerInfo.tenantId });
        });
        it('查询商品列表', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
            expect(dresList).to.have.lengthOf(0);
        });
        it('卖家查询商品信息-校验商品状态', async function () {
            await spReq.spSellerLogin({ shopName: '中洲店' });
            const dresList = await spdresb.findSellerSpuList({ pageSize: 20 }).then(res => res.result.data.rows);
            dresList.forEach(dres => expect(dres.flag).to.equal(-1));
        });
    });

});