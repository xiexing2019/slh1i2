'use strict'
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const wmsJson = require('../../help/wmsHelp/wmsJson');
const common = require('../../../lib/common');

describe.skip('仓管人员管理', function () {
    this.timeout(30000);;

    describe('骑手管理', function () {
        let user;
        before('新增骑手', async function () {
            await wmsWarehouse.wmsStaffLogin();
            user = new wmsJson.UserJson();
            await user.saveUser();
        });

        it('保存相同的骑手', async function () {
            let saveParam = _.cloneDeep(user.getUser());
            delete saveParam.id;
            saveParam.check = false;
            let res = await wmsWarehouse.saveUser(saveParam);
            expect(res.result).to.includes({ "msgId": "the_mobile_is_existed" });
        });

        it('查询骑手', async function () {
            let res = await wmsWarehouse.getUserList({ pageSize: 20, pageNo: 1, warehouseId: LOGINDATA.tenantId, flag: 1, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            let info = res.find(val => val.id == user.getUser().id);

            if (info == undefined) {
                throw new Error(`${user.getUser().name}新增失败，列表中没有找到新增的骑手`)
            } else {
                let exp = _.cloneDeep(user.getUser());
                exp.updatedBy = exp.createdBy = LOGINDATA.userId;
                common.isApproximatelyEqualAssert(user.getUser(), info);
            };
        });

        it('修改骑手', async function () {
            await user.updateUser({
                name: `取货员${common.getRandomStr(4)}`,
                mobile: `12${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`,
                idCardNo: `330327199${common.getCurrentDate('YY-MM-DD').replace(/-/g, '').slice(1)}${common.getRandomNumStr(4)}`
            });

            let res = await wmsWarehouse.getUserList({ pageSize: 20, pageNo: 1, flag: 1, warehouseId: LOGINDATA.tenantId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            let info = res.find(val => val.id == user.getUser().id);
            common.isApproximatelyEqualAssert(user.getUser(), info);
        });

        it('删除骑手', async function () {
            await wmsWarehouse.delUser({ id: user.getUser().id });
            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, orderBy: 'createdDate', orderByDesc: true, flag: 1 }).then(res => res.result.data.rows);
            let info = res.find(val => val.id == user.getUser().id);
            expect(info, `删除骑手之后，该骑手还显示在列表中${user.getUser().name}`).to.be.undefined;
        });

        it('查询骑手,flag=-1', async function () {
            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, orderBy: 'createdDate', orderByDesc: true, flag: -1 }).then(res => res.result.data.rows);
            let info = res.find(val => val.id == user.getUser().id);
            common.isApproximatelyEqualAssert(user.getUser(), info);
        });
    });

    describe('骑手分配市场', function () {
        let marketId, saveRes, user, user1;

        before('获取市场列表', async function () {
            await wmsWarehouse.wmsStaffLogin();
            user = new wmsJson.UserJson();
            await user.saveUser();
            let res = await wmsWarehouse.getWareHouseMarket({ whTenantId: LOGINDATA.tenantId, flag: 1 }).then(res => res.result.data.rows);
            marketId = res[common.getRandomNum(0, res.length - 1)].marketId;
            saveRes = await wmsWarehouse.saveUserMarket({ userId: user.getUser().id, marketId });
        });

        after('删除新增的拿货员', async function () {
            await wmsWarehouse.delUser({ id: user.getUser().id });
            await wmsWarehouse.delUser({ id: user1.getUser().id });
        });

        it('查看市场下骑手列表', async function () {
            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, marketIds: saveRes.params.jsonParam.marketId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);

            let info = res.find(val => val.id == saveRes.params.jsonParam.userId);
            if (info == undefined) {
                throw new Error(`给${user.getUser().name}设置了市场，但是该市场列表下面没有这个骑手`)
            } else {
                expect(info.marketId, `给骑手设置了市场，但是该骑手信息里没有显示此市场`).to.be.equal(saveRes.params.jsonParam.marketId);
            };
        });

        it('解绑', async function () {
            await wmsWarehouse.delUserMarket({ id: saveRes.result.data.val });
            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, marketIds: saveRes.params.jsonParam.marketId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);

            let info = res.find(val => val.id == saveRes.params.jsonParam.userId);
            expect(info, '解绑后骑手仍然显示在市场列表里面').to.be.undefined;
        });

        it('批量绑定', async function () {
            //再添加一个骑手
            user1 = new wmsJson.UserJson();
            await user1.saveUser();

            //将刚刚这两个新的骑手批量绑定到仓库
            await wmsWarehouse.setUserMarketInBatch([
                { userId: user.getUser().id, marketId, flag: 1 },
                { userId: user1.getUser().id, marketId, flag: 1 }
            ]);
        });

        it('查看市场下骑手列表', async function () {
            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, marketIds: marketId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            expect(res.map(val => val.id), '给市场批量设置了收货员，但是列表中找不到设置的收货员').to.include.members([user.getUser().id, user1.getUser().id]);
        });

        it('批量取消', async function () {
            await wmsWarehouse.setUserMarketInBatch([
                { userId: user.getUser().id, marketId, flag: -1 },
                { userId: user1.getUser().id, marketId, flag: -1 }
            ]);

            let res = await wmsWarehouse.getUserList({ pageSize: 10, pageNo: 1, warehouseId: LOGINDATA.tenantId, marketIds: marketId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            expect(res.every(val => [user.getUser().id, user1.getUser().id].indexOf(val) == -1), `骑手已经从市场移除了，还是显示在市场里面,,取消的骑手是${user.getUser().name},${user1.getUser().name}`).be.be.true;
        });
    });
});
