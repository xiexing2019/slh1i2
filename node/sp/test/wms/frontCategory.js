const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const spAccount = require('../../data/spAccount');
const frontCategoryManage = require('../../help/spAdmin/frontCategory');
const common = require('../../../lib/common');

// http://zentao.hzdlsoft.com:6082/zentao/task-view-1711.html
// 复用前台类目
// 1.增加仓库前台类目关系表
// 2.增加关系维护功能
// 3.增加根据仓库id获取类目集主键接口
//
// 前台类目状态变化不会影响关联关系 
describe('仓库前台类目', async function () {
    this.timeout(30000);
    const frontCategory = frontCategoryManage.setupFrontCategoryAction(),
        saasCategoryRef = frontCategoryManage.setupSaasCategoryRef();

    before('新建仓库前台类目关系', async function () {
        await spAuth.staffLogin();

        // 新增前台类目 (UNIQUE KEY `ux_saas_type_id` (`saas_id`,`category_id`,`warehouse_id`))
        await frontCategory.getRandom().save();

        const warehouse = await wmsWarehouse.getWareHouseList({ name: spAccount.wareHouse.name }).then(res => res.result.data.rows[0]);

        await saasCategoryRef.save({
            categoryId: frontCategory.id,
            categoryName: frontCategory.name,
            rem: common.getRandomStr(4),
            saasId: 1,// 暂时写死
            warehouseId: warehouse.id,
            warehouseName: warehouse.name,
        });
    });

    it('仓库类目列表', async function () {
        const detail = await saasCategoryRef.findDetailFromList();
        common.isApproximatelyEqualAssert(saasCategoryRef, detail);
    });
    describe('停用', async function () {
        before(async function () {
            await saasCategoryRef.changeFlag({ flag: 0 });
        });
        it('仓库类目列表', async function () {
            const detail = await saasCategoryRef.findDetailFromList();
            common.isApproximatelyEqualAssert(saasCategoryRef, detail);
        });
    });
    describe('启用', async function () {
        before(async function () {
            await saasCategoryRef.changeFlag({ flag: 1 });
        });
        it('仓库类目列表', async function () {
            const detail = await saasCategoryRef.findDetailFromList();
            common.isApproximatelyEqualAssert(saasCategoryRef, detail);
        });
    });
    describe('停用前台类目-校验关系', async function () {
        before(async function () {
            await frontCategory.disable();
        });
        it('仓库类目列表', async function () {
            const detail = await saasCategoryRef.findDetailFromList();
            common.isApproximatelyEqualAssert(saasCategoryRef, detail);
        });
    });
    describe('删除', async function () {
        before(async function () {
            await saasCategoryRef.changeFlag({ flag: -1 });
        });
        it('仓库类目列表', async function () {
            const detail = await saasCategoryRef.findDetailFromList();
            common.isApproximatelyEqualAssert(saasCategoryRef, detail);
        });
    });
});