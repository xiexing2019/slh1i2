const common = require('../../../lib/common');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const wmsCombine = require('../../../reqHandler/sp/wms/combine');
const wmsStorage = require('../../../reqHandler/sp/wms/storage');
const wmsJson = require('../../help/wmsHelp/wmsJson');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const billManage = require('../../help/billManage');
const cartManager = require('../../help/cartHelp');
const parcelBillManager = require('../../help/wmsHelp/parcelBill');

/**
 * 纯包裹业务流程校验
 * 合包单生成较慢 使用mock生成
 */
describe('包裹', async function () {
    this.timeout(30000);
    let sellerA, dresA1, dresA2;
    /** 骑手收货数统计 起始值 */
    let riderStatisticOrign = {};
    const combBillJson = parcelBillManager.mockCombBillJson(),
        parcelBill = parcelBillManager.setupParcelBill();
    before(async function () {
        await spReq.spSellerLogin();
        sellerA = _.cloneDeep(LOGINDATA);
        const shopInfo = await spugr.getShop({ id: sellerA.tenantId }).then(res => res.result.data);
        // console.log(`shopInfo=${JSON.stringify(shopInfo)}`);
        if (!shopInfo.contactPhone) shopInfo.contactPhone = sellerA.mobile;
        await combBillJson.setSeller(shopInfo);

        const dresAList = await spdresb.searchDres({ queryType: 0, tenantId: sellerA.tenantId }).then(res => res.result.data.dresStyleResultList);
        dresA1 = await spReq.getFullForBuyerByUrl({ detailUrl: dresAList[0].detailUrl }).then(res => res.result.data);
        // console.log(`dresA1=${JSON.stringify(dresA1)}`);

        await spReq.spClientLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await combBillJson.setBuyer(LOGINDATA);

        await wmsWarehouse.wmsStaffLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        combBillJson.setWarehouse(LOGINDATA);
        // console.log(combBillJson);

    });

    describe('创建包裹', async function () {
        const parcelBillJson = parcelBillManager.mockParcelBillJson();
        before(async function () {
            const combBillRes = await wmsCombine.newCombBillByMock(combBillJson.getJson());
            console.log(`\ncombBillRes=${JSON.stringify(combBillRes)}`);

            parcelBillJson.setWarehouse(LOGINDATA).setCombBoll({ combBillId: combBillRes.result.data.billId, billNo: combBillRes.params.jsonParam.combBillDetailList[0].billNo, salesBillId: combBillRes.params.jsonParam.combBillDetailList[0].salesBillId }).addDres(dresA1);
            const parcelBillRes = await wmsStorage.createParcelBillMock(parcelBillJson.getJson());
            console.log(`\nparcelBillRes=${JSON.stringify(parcelBillRes)}`);
            parcelBill.setByMock({ combBillRes, parcelBillRes });
            console.log(parcelBill);
        });
        it('查询包裹列表', async function () {
            const billDetail = await parcelBill.findBillFromList();
            expect(billDetail.parcelBill.flag).to.be.oneOf([0, 1, 10]);// 自动分配状态无法控制
            common.isApproximatelyEqualAssert(parcelBill.getListExp(), billDetail, ['flag', 'riderId', 'riderName']);
        });
        it('查询包裹详情', async function () {
            const billDetail = await parcelBill.getFull();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
    });

    describe('包裹分配骑手', async function () {
        before(async function () {
            const riderList = await wmsWarehouse.getUserList({ warehouseId: LOGINDATA.tenantId, flag: 1 }).then(res => res.result.data.rows);
            // console.log(`riderList=${JSON.stringify(riderList)}`);
            await parcelBill.allotRider(riderList[0]);
        });
        it('查询包裹列表', async function () {
            const billDetail = await parcelBill.findBillFromList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp(), billDetail);
        });
        it('查询包裹详情', async function () {
            const billDetail = await parcelBill.getFull();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
        it('骑手收货数统计-获取起始值', async function () {
            riderStatisticOrign = await wmsStorage.getParcelRiderStatistic({ unitId: LOGINDATA.unitId, riderId: parcelBill.riderId }).then(res => res.result.data);
        });
        it('骑手查询收货列表', async function () {
            const billDetail = await parcelBill.findBillFromRiderList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp().parcelBill, billDetail);
        });
        it('骑手查询包裹详情', async function () {
            const billDetail = await parcelBill.getFullByRider();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
    });

    describe('包裹确认取货', async function () {
        before(async function () {
            await parcelBill.confirmPick();
        });
        it('查询包裹列表', async function () {
            const billDetail = await parcelBill.findBillFromList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp(), billDetail);
        });
        it('查询包裹详情', async function () {
            const billDetail = await parcelBill.getFull();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
        it('骑手收货数统计', async function () {
            const riderStatistic = await wmsStorage.getParcelRiderStatistic({ unitId: LOGINDATA.unitId, riderId: parcelBill.riderId }).then(res => res.result.data)
            common.isApproximatelyEqualAssert({ numTotal: riderStatisticOrign.numTotal + 1, numToday: riderStatisticOrign.numToday + 1 }, riderStatistic);
        });
        it('骑手查询收货列表', async function () {
            const billDetail = await parcelBill.findBillFromRiderList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp().parcelBill, billDetail);
        });
        it('骑手查询包裹详情', async function () {
            const billDetail = await parcelBill.getFullByRider();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
    });

    describe('包裹入库', async function () {
        before(async function () {
            await parcelBill.consignee();
        });
        it('查询包裹列表', async function () {
            const billDetail = await parcelBill.findBillFromList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp(), billDetail);
        });
        it('查询包裹详情', async function () {
            const billDetail = await parcelBill.getFull();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
        it('骑手查询收货列表', async function () {
            const billDetail = await parcelBill.findBillFromRiderList();
            common.isApproximatelyEqualAssert(parcelBill.getListExp().parcelBill, billDetail);
        });
        it('骑手查询包裹详情', async function () {
            const billDetail = await parcelBill.getFullByRider();
            common.isApproximatelyEqualAssert(parcelBill.getFullExp(), billDetail);
        });
    });

    describe('取消包裹', async function () {

    });

    describe('删除包裹', async function () {

    });

});