'use strict';

const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const common = require('../../../lib/common');
const wmsJson = require('../../help/wmsHelp/wmsJson');

describe.skip('货架管理', async function () {
    this.timeout(30000);
    let saveRes, shelf, shelvesList;

    before('保存货架', async function () {
        shelf = new wmsJson.Shelf();
        await wmsWarehouse.wmsStaffLogin();

        saveRes = await shelf.saveShelf();
    });

    it('搜索条件验证', async function () {
        shelvesList = await wmsWarehouse.getShelvesList({ pageSize: 100, pageNo: 1, shelfNoPrefix: saveRes.params.shelfName });
        expect(shelvesList.result.data.rows.every(val => val.shelfNo.indexOf(shelvesList.params.jsonParam.shelfNoPrefix) > -1), `查询条件有误，查询条件是${shelvesList.params.jsonParam.shelfNoPrefix},搜出来的结果是${JSON.stringify(shelvesList.result.data.rows)}`).to.be.true;
    });

    it('查询条件', async function () {
        let shelfNames = shelvesList.result.data.rows.map(val => val.shelfNo);
        let exp = shelf.getShelfList();
        common.isApproximatelyEqualAssert(exp, shelfNames, [], `添加仓位后，查询列表的仓位和添加的不一致,预期的是${JSON.stringify(exp)},实际的是${shelfNames}`);
    });

    it('删除仓位', async function () {
        await shelf.delShelves();
        let res = await wmsWarehouse.getShelvesList({ pageSize: 100, pageNo: 1, shelfNoPrefix: saveRes.params.shelfName });
        let info = res.result.data.rows.find(val => val.shelfNo.indexOf(res.params.jsonParam.shelfNoPrefix) == -1);
        expect(info, `删除仓位之后，还搜索到了此仓位相关的仓位`).to.be.undefined;
    });
});
