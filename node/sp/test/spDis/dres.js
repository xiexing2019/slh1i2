const disReq = require('../../help/disHelp/disReq');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spReq = require('../../help/spReq');


describe.skip('商品--分销', async function () {
    this.timeout(30000);
    before(async function () {

    });

    it('查询资源管理列表', async function () {
        await spAuth.staffLogin();
        const res = await spugr.getSpSourceEntryList({ nameLike: '自动化', flag: 1 });
        // console.log(`res=${JSON.stringify(res)}`);

    });

    // es搜索 调整pubPrice
    // 买家商品详情 调整price 
    describe('商品加价校验', async function () {
        let disInfo;
        const dresMap = new Map(), expDresMap = new Map();
        before('渠道登录 查询商品', async function () {
            await spAuth.staffLogin();
            disInfo = await spugr.getSpDisList({ nameLike: disReq.disInfo.spDis.name })
                .then(res => res.result.data.rows.find(row => row.id == disReq.disInfo.spDis.id));
            if (!disInfo) {
                console.error(`未找到分销商信息`);
                this.skip();
            }

            await disReq.defMerchantLogin();
            await disReq.searchDres({ pageSize: 20, pageNo: 1 })
                .then(res => res.result.data.dresStyleResultList.forEach(dres => {
                    dresMap.set(dres.id, { pubPrice: dres.pubPrice, detailUrl: dres.detailUrl });
                }));

            // 普通登录，查询商品
            await spReq.spClientLogin();
            await spDresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 0, keyWords: { id: [...dresMap.keys()] } })
                .then(res => res.result.data.dresStyleResultList.forEach(dres => {
                    expDresMap.set(dres.id, { pubPrice: dres.pubPrice });
                }));
        });
        it('es搜索校验商品价格', async function () {
            for (const [spuId, dres] of dresMap.entries()) {
                // console.log(`spuId=${spuId} ${expDresMap.get(spuId).pubPrice},${dres.pubPrice},${getAdjustPrice({ origPrice: expDresMap.get(spuId).pubPrice, disInfo })}`);
                expect(dres.pubPrice).to.equal(getAdjustPrice({ origPrice: expDresMap.get(spuId).pubPrice, disInfo }));
            }
        });
        // 详情校验时 现阶段忽略sku的pubPrice
        // 目前价格以spu价格为准 新增商品时可以使spu与sku的价格不一致，修改时会将spu价格覆盖到sku
        it('买家查询商品详情-校验价格', async function () {
            await disReq.defMerchantLogin();

            for await (const [spuId, dres] of dresMap.entries()) {
                const expDres = expDresMap.get(spuId);
                const origPrice = expDres.pubPrice,
                    adjustPrice = getAdjustPrice({ origPrice: expDres.pubPrice, disInfo });

                const dresFull = await disReq.getDresFullForBuyerByUrl({ detailUrl: dres.detailUrl }).then(res => {
                    // if (res.result.data.id == '61055') console.log(res);
                    return res.result.data;
                });
                expect(dresFull.spu).to.include({ pubPrice: origPrice, price: adjustPrice });
                dresFull.skus.forEach(sku => expect(sku, JSON.stringify(dresFull)).to.include({ price: adjustPrice }));//pubPrice: origPrice,
            }
        });
    });

});

/**
 * 获取调整价格
 * @description 100 * (1 + 5%(平台）) + 100 * (1 + 5%(平台）) * (5%（渠道） + 10%（合伙人合计)) = 商品销售价
 * 
 * @param {object} param0 
 */
function getAdjustPrice({ origPrice, disInfo }) {
    const price = origPrice * (1 + disInfo.platProfit) + origPrice * (1 + disInfo.platProfit) * (disInfo.totalProfit - disInfo.platProfit);
    return Number(price.toFixed(2));
};