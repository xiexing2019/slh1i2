const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spAct = require('../../../reqHandler/sp/global/spAct');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');

/**
 * 频道 
 * 纯频道功能,服务端已废弃，现在为活动+频道的方式进行(活动流程中会校验)
 * wiki: http://c.hzdlsoft.com:7082/Wiki.jsp?page=平台活动频道
 */
describe('频道', async function () {
    this.timeout(TESTCASE.timeout);

    describe('创建频道', async function () {
        const channel = mockJsonParam.getChannelJson({ channelType: 2 });
        before('新增频道', async function () {
            await spAuth.staffLogin();
            // 
            await spReq.getDictList(['theme', 'masterClass']);

            channel.fullJson();
            const channelRes = await spdchg.saveChannelWithRule(channel);
            channel.update({ id: channelRes.result.data.val });
        });
        after('停用频道--数据还原', async function () {
            if (channel.id) await spdchg.stopChannel({ id: channel.id });
        });
        it('活动频道详情', async function () {
            const res = await spdchg.getChannelById({ id: channel.id });
            common.isApproximatelyEqualAssert(channel.fullExp(), res.result.data);
        });
        it('活动频道列表', async function () {
            const channelList = await spdchg.getChannelList({ name: channel.name, flag: channel.flag }).then(res => res.result.data.rows);
            const channelInfo = channelList.find(val => val.id == channel.id);
            expect(channelInfo, `列表中未找到新增的频道 id:${channel.id} name:${channel.name}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(channel.listExp(), channelInfo);
        });
        it('添加商品', async function () {
            await spReq.spSellerLogin();
            const sellerTenantId = LOGINDATA.tenantId;

            await spAuth.staffLogin();
            const spuId = await spDresb.searchDres({ tenantId: sellerTenantId, queryType: 4 })
                .then(res => res.result.data.dresStyleResultList[0].id);
            await spdchg.saveChannelDetail({ channelId: channel.id, spuIds: [spuId] });

            const dresList = await spAct.getActChannelSearchDres({ channelId: channel.id, spuId }).then(res => res.result.data.rows);
            expect(dresList.find(dres => dres.spuId == spuId), `活动频道商品中未找到商品 spuId=${spuId}`).not.to.be.undefined;
        });
        describe('编辑频道', async function () {
            before(async function () {
                await spdchg.updateChannelWithRule(channel.fullJson());
            });
            it('活动频道详情', async function () {
                const res = await spdchg.getChannelById({ id: channel.id });
                common.isApproximatelyEqualAssert(channel.fullExp(), res.result.data);
            });
            it('活动频道列表', async function () {
                const channelList = await spdchg.getChannelList({ name: channel.name, flag: channel.flag }).then(res => res.result.data.rows);
                const channelInfo = channelList.find(val => val.id == channel.id);
                expect(channelInfo, `列表中未找到新增的频道 id:${channel.id} name:${channel.name}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(channel.listExp(), channelInfo);
            });
        });
        describe('停用频道', async function () {
            before(async function () {
                await spdchg.stopChannel({ id: channel.id });
                channel.update({ flag: 0 });
            });
            it('活动频道详情', async function () {
                const res = await spdchg.getChannelById({ id: channel.id });
                common.isApproximatelyEqualAssert(channel.fullExp(), res.result.data);
            });
            it('活动频道列表', async function () {
                const channelList = await spdchg.getChannelList({ name: channel.name, flag: channel.flag }).then(res => res.result.data.rows);
                const channelInfo = channelList.find(val => val.id == channel.id);
                expect(channelInfo, `列表中未找到新增的频道 id:${channel.id} name:${channel.name}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(channel.listExp(), channelInfo);
            });
        });
        describe('启用频道', async function () {
            before(async function () {
                await spdchg.startChannel({ id: channel.id });
                channel.update({ flag: 1 });
            });
            it('活动频道详情', async function () {
                const res = await spdchg.getChannelById({ id: channel.id });
                common.isApproximatelyEqualAssert(channel.fullExp(), res.result.data);
            });
            it('活动频道列表', async function () {
                const channelList = await spdchg.getChannelList({ name: channel.name, flag: channel.flag }).then(res => res.result.data.rows);
                const channelInfo = channelList.find(val => val.id == channel.id);
                expect(channelInfo, `列表中未找到新增的频道 id:${channel.id} name:${channel.name}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(channel.listExp(), channelInfo);
            });
        });
    });

    describe('维护活动商品', async function () {

    });

});