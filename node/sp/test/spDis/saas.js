const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const mysql = require('../../../reqHandler/sp/mysql');

describe('saas相关', async function () {
    it('获取saas列表', async function () {
        const spgSql = await mysql({ dbName: 'spgMysql' });
        const [saasList] = await spgSql.query(`SELECT id,saas_cluster_id AS saasClusterId,saas_domain_type as saasDomainType FROM spugr.sp_saas `);
        await spgSql.end();

        const data = await spugr.getSaasList().then(res => res.result.data.rows);
        common.isApproximatelyArrayAssert(saasList, data);
    });
});