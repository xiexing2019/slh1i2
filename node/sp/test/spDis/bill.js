const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const disReq = require('../../help/disHelp/disReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spop = require('../../../reqHandler/sp/spop/spo');
const disManage = require('../../help/disHelp/disManage');
const mysql = require('../../../reqHandler/sp/mysql');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const disBillManage = require('../../help/disHelp/disBill');

describe('分销下单', async function () {
    this.timeout(30000);

    let disBill = disBillManage.spDisBillManage();
    before(async function () {
        await disReq.defMerchantLogin();
        console.log(`合伙人登录信息:${JSON.stringify(LOGINDATA)}`);

        await disManage.prepareRecAddress(LOGINDATA);

        disBill.setUser({ userInfo: LOGINDATA });
        await disBill.getRecAddressInfo();
        console.log(disBill.userInfo);

    });

    describe('创建订单', async function () {
        before(async function () {
            // 搜索商品
            const dresList = await disReq.searchDres({ pageSize: 2, pageNo: 1 }).then(res => res.result.data.dresStyleResultList);
            const dresFull = await disReq.getDresFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);

            // 下单
            await disBill.getPurJson({ dresFull }).saveDisPurBill();
            console.log(disBill);
        });
        it('', async function () {

        });
    });

    describe('订单支付', async function () {
        before(async function () {
            await disBill.createPay();
        });
        it('', async function () {

        });
    });

});