const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const disReq = require('../../help/disHelp/disReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spop = require('../../../reqHandler/sp/spop/spo');
const disManage = require('../../help/disHelp/disManage');
const mysql = require('../../../reqHandler/sp/mysql');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const disBillManage = require('../../help/disHelp/disBill');
const bizOrderDetail = require('../../../reqHandler/sp/ecAccount/bizOrderDetail');
const accBizOrderDetailManage = require('../../help/acct/accountBizOrderDetail');

describe('分销下单-仅退款', async function () {
    this.timeout(30000);

    let disBill = disBillManage.spDisBillManage(), returnBillId;
    before(async function () {
        await disReq.defMerchantLogin();
        console.log(`合伙人登录信息:${JSON.stringify(LOGINDATA)}`);

        await disManage.prepareRecAddress(LOGINDATA);

        disBill.setUser({ userInfo: LOGINDATA });
        await disBill.getRecAddressInfo();
        console.log(disBill.userInfo);

        // 搜索商品
        const dresList = await disReq.searchDres({ searchToken: '', pageSize: 2, pageNo: 1 }).then(res => {
            // console.log(res);
            return res.result.data.dresStyleResultList;
        });
        // console.log(`dresList=${JSON.stringify(dresList)}`);

        const dresFull = await disReq.getDresFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);

        // 下单
        await disBill.getPurJson({ dresFull }).saveDisPurBill();
        // 订单支付
        await disBill.createPay();

    });

    it('自测', async function () {

    });


    describe('创建退款申请', async function () {
        before(async function () {
            const purBillFull = await disBill.findPurBillFull();
            console.log(`\n采购单详情${JSON.stringify(purBillFull)}`);

            // 根据采购单详情 拼接退货单参数
            const returnJson = await disBill.getReturnJson({ typeId: 0, backFlag: 1, purBillFull });
            console.log(`\nreturnJson=${JSON.stringify(returnJson)}`);

            returnBillId = await spTrade.saveReturnBill(returnJson).then(res => {
                console.log(`res=${JSON.stringify(res)}`);
                return res.result.data.val;
            });
            console.log(`\nreturnBillId=${JSON.stringify(returnBillId)}`);

        });
        it('', async function () {

        });
    });

    describe('分销商登陆,同意退款', async function () {
        before(async function () {

            await disReq.staffLogin();
            console.log(`渠道商:${JSON.stringify(LOGINDATA)}`);
            await common.delay(500);


            // 保证退款单已生成
            for (let index = 0; index < 10; index++) {
                await common.delay(1000);
                const returnList = await spTrade.findSalesReturnBills({}).then(res => res.result.data.rows);
                // console.log(`index=${index}\nreturnList=${JSON.stringify(returnList)}`);
                const hasFinished = returnList.find(data => data.bill.id == returnBillId);
                if (hasFinished) {
                    console.log(`\n退款单:${JSON.stringify(hasFinished)}`);
                    break;
                }
                if (index == 9 && !hasFinished) {
                    console.log(`\n未找到退款单 当前时间:${common.getCurrentTime()} returnBillId=${returnBillId}`);
                }
            }

            // console.log(`returnList=${JSON.stringify(returnList)}`);

            // const toRefundPayList = await spTrade.getToRefundPayDetailNo({ returnBillId: returnBillId });
            // console.log(`\ntoRefundPayList=${JSON.stringify(toRefundPayList)}`);

            // 通过退款申请
            const res = await spTrade.checkReturnBill({ id: returnBillId, checkResult: 1 });
            console.log(`\nres=${JSON.stringify(res)}`);

            // 退款回调
            // const returnRes = await spTrade.receiveReturnResult({ mainId, amount });
            // console.log(`\nreturnRes=${JSON.stringify(returnRes)}`);

            await common.delay(5000);
        });
        it('查询支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].billNo });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询子订单支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].subBillNos });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询退款信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: returnBillId });
            console.log(`data=${JSON.stringify(data)}`);
        });
    });

    describe.skip('创建退款申请', async function () {
        before(async function () {
            const purBillFull = await disBill.findPurBillFull();
            console.log(`\n采购单详情${JSON.stringify(purBillFull)}`);

            // 根据采购单详情 拼接退货单参数
            const returnJson = await disBill.getReturnJson({ typeId: 0, backFlag: 2, purBillFull });
            console.log(`\nreturnJson=${JSON.stringify(returnJson)}`);

            returnBillId = await spTrade.saveReturnBill(returnJson).then(res => {
                console.log(`res=${JSON.stringify(res)}`);
                return res.result.data.val;
            });
            console.log(`\nreturnBillId=${JSON.stringify(returnBillId)}`);

        });
        it('', async function () {

        });
    });

    describe.skip('分销商登陆,同意退款', async function () {
        before(async function () {

            await disReq.staffLogin();
            console.log(`渠道商:${JSON.stringify(LOGINDATA)}`);
            await common.delay(500);


            // 保证退款单已生成
            for (let index = 0; index < 10; index++) {
                await common.delay(1000);
                const returnList = await spTrade.findSalesReturnBills({}).then(res => res.result.data.rows);
                // console.log(`index=${index}\nreturnList=${JSON.stringify(returnList)}`);
                const hasFinished = returnList.find(data => data.bill.id == returnBillId);
                if (hasFinished) {
                    console.log(`\n退款单:${JSON.stringify(hasFinished)}`);
                    break;
                }
                if (index == 9 && !hasFinished) {
                    console.log(`\n未找到退款单 当前时间:${common.getCurrentTime()} returnBillId=${returnBillId}`);
                }
            }

            // console.log(`returnList=${JSON.stringify(returnList)}`);

            // const toRefundPayList = await spTrade.getToRefundPayDetailNo({ returnBillId: returnBillId });
            // console.log(`\ntoRefundPayList=${JSON.stringify(toRefundPayList)}`);

            // 通过退款申请
            const res = await spTrade.checkReturnBill({ id: returnBillId, checkResult: 1 });
            console.log(`\nres=${JSON.stringify(res)}`);

            // 退款回调
            // const returnRes = await spTrade.receiveReturnResult({ mainId, amount });
            // console.log(`\nreturnRes=${JSON.stringify(returnRes)}`);

            await common.delay(5000);
        });
        it('查询支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].billNo });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询子订单支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].subBillNos });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询退款信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: returnBillId });
            console.log(`data=${JSON.stringify(data)}`);
        });
    });

    describe.skip('创建退款申请-2次退款', async function () {
        before(async function () {
            await common.delay(10000);
            await disReq.defMerchantLogin();
            const purBillFull = await disBill.findPurBillFull();
            console.log(`\n采购单详情${JSON.stringify(purBillFull)}`);

            // 根据采购单详情 拼接退货单参数
            const returnJson = await disBill.getReturnJson({ typeId: 0, purBillFull });
            console.log(`\nreturnJson2=${JSON.stringify(returnJson)}`);

            returnBillId = await spTrade.saveReturnBill(returnJson).then(res => {
                console.log(`res=${JSON.stringify(res)}`);
                return res.result.data.val;
            });
            console.log(`\nreturnBillId2=${JSON.stringify(returnBillId)}`);

        });
        it('', async function () {

        });
    });

    describe.skip('分销商登陆,同意退款', async function () {
        before(async function () {

            await disReq.staffLogin();
            console.log(`渠道商:${JSON.stringify(LOGINDATA)}`);
            await common.delay(500);


            // 保证退款单已生成
            for (let index = 0; index < 10; index++) {
                await common.delay(1000);
                const returnList = await spTrade.findSalesReturnBills({}).then(res => res.result.data.rows);
                // console.log(`index=${index}\nreturnList=${JSON.stringify(returnList)}`);
                const hasFinished = returnList.find(data => data.bill.id == returnBillId);
                if (hasFinished) {
                    console.log(`\n退款单:${JSON.stringify(hasFinished)}`);
                    break;
                }
                if (index == 9 && !hasFinished) {
                    console.log(`\n未找到退款单 当前时间:${common.getCurrentTime()} returnBillId=${returnBillId}`);
                }
            }

            // console.log(`returnList=${JSON.stringify(returnList)}`);

            // const toRefundPayList = await spTrade.getToRefundPayDetailNo({ returnBillId: returnBillId });
            // console.log(`\ntoRefundPayList=${JSON.stringify(toRefundPayList)}`);

            // 通过退款申请
            const res = await spTrade.checkReturnBill({ id: returnBillId, checkResult: 1 });
            console.log(`\nres=${JSON.stringify(res)}`);

            // 退款回调
            // const returnRes = await spTrade.receiveReturnResult({ mainId, amount });
            // console.log(`\nreturnRes=${JSON.stringify(returnRes)}`);

            await common.delay(5000);
        });
        it('查询支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].billNo });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询子订单支付信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: disBill.bills.rows[0].subBillNos });
            console.log(`\ndata=${JSON.stringify(data)}`);
        });
        it('查询退款信息', async function () {
            const data = await bizOrderDetail.getBizOrderDetailBySubBizOrderId({ bizSubOrderId: returnBillId });
            console.log(`data=${JSON.stringify(data)}`);
        });
    });




});