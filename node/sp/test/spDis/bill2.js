const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const disReq = require('../../help/disHelp/disReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spop = require('../../../reqHandler/sp/spop/spo');
const disManage = require('../../help/disHelp/disManage');
const mysql = require('../../../reqHandler/sp/mysql');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const disBillManage = require('../../help/disHelp/disBill');

describe.skip('分销下单', async function () {
    this.timeout(30000);

    const disBill = disBillManage.spDisBillManage();
    let seller1, seller2;
    before(async function () {
        await spReq.spSellerLogin();
        seller1 = _.cloneDeep(LOGINDATA);
        await spReq.spSellerLogin({ shopName: '中洲店' });
        seller2 = _.cloneDeep(LOGINDATA);

        await disReq.defMerchantLogin();
        console.log(`合伙人登录信息:${JSON.stringify(LOGINDATA)}`);

        await disManage.prepareRecAddress(LOGINDATA);

        disBill.setUser({ userInfo: LOGINDATA });
        await disBill.getRecAddressInfo();
        console.log(disBill.userInfo);

    });

    describe('创建订单', async function () {
        before(async function () {
            // 搜索商品
            const dresList = await disReq.searchDres({ pageSize: 2, pageNo: 1, tenantId: seller1.tenantId }).then(res => res.result.data.dresStyleResultList);
            const dresFull = await disReq.getDresFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl }).then(res => res.result.data);

            const dresList2 = await disReq.searchDres({ pageSize: 2, pageNo: 1, tenantId: seller2.tenantId }).then(res => res.result.data.dresStyleResultList);
            const dresFull2 = await disReq.getDresFullForBuyerByUrl({ detailUrl: dresList2[0].detailUrl }).then(res => res.result.data);

            // console.log(`dresFull=${JSON.stringify(dresFull)}`);
            // console.log(`dresFull2=${JSON.stringify(dresFull2)}`);

            disBill.addDres(dresFull);
            disBill.addDres(dresFull2);
            // 下单
            await disBill.getPurJson({ dresFull }).saveDisPurBill();
            // console.log(disBill);
        });
        it('', async function () {

        });
    });

    describe.skip('订单支付', async function () {
        before(async function () {
            await disBill.createPay();
        });
        it('', async function () {

        });
    });

});