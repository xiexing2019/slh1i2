const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const disReq = require('../../help/disHelp/disReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spop = require('../../../reqHandler/sp/spop/spo');
const disManage = require('../../help/disHelp/disManage');
const mysql = require('../../../reqHandler/sp/mysql');

// 分销渠道 
// 渠道商/合伙人/买家 注册登录/信息维护相关用例
// 买家绑定需要合伙人的tenantCode(uniCode)
describe('分销渠道', async function () {
    this.timeout(30000);
    before(async function () {
        await spAuth.staffLogin();
    });

    it.skip('自测', async function () {

        await spReq.spSellerLogin();
        // await disReq.defMerchantLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        const spDisMerchantInfo = mockJsonParam.merchantJson();
        spDisMerchantInfo.id = 1504;
        const saveRes = await spugr.saveChannelMerchant(spDisMerchantInfo);
        console.log(`saveRes=${JSON.stringify(saveRes)}`);

    });

    it.skip('自测', async function () {
        await disReq.staffLogin();
        const buyerTenantId = '306598';
        spgSql = await mysql({ dbName: 'spgMysql' });

        const list = await spugr.getSpBuyerSellerList({ buyerTenantId }).then(res => {
            console.log(res);
            return res.result.data.rows;
        });
        saasRelId = list.find(data => data.buyerTenantId == buyerTenantId).id;

        await spugr.changeSaasId({ id: saasRelId, saasId: 1, buyerTenantId });
    });

    // 分销商增改
    describe('分销商', async function () {
        let spDisInfo = new mockJsonParam.SpDisInfo(),
            disUserInfo = {};
        before('新增分销商', async function () {
            await spAuth.staffLogin();
            await spDisInfo.saveSpDis();
            // console.log(spDisInfo);
        });
        it('分销商查询', async function () {
            const spDisList = await spugr.getSpDisList({ nameLike: spDisInfo.name, channelId: spDisInfo.channelId }).then(res => res.result.data.rows);
            disUserInfo = spDisList.find(val => val.id == spDisInfo.id);
            expect(disUserInfo, `列表中未找到新增的分销商 id:${spDisInfo.id} name:${spDisInfo.name}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(spDisInfo, disUserInfo);
        });
        // http://zentao.hzdlsoft.com:6082/zentao/testtask-view-598.html
        it('获取店铺详情', async function () {
            const shopDetail = await spugr.getShop({ id: spDisInfo.id }).then(res => res.result.data);
            expect(shopDetail).to.include({ id: spDisInfo.id, saasId: spDisInfo.id, tenantId: spDisInfo.id })
        });
        describe.skip('修改分销商', async function () {
            before(async function () {
                spDisInfo = await spDisInfo.mockDisInfo();
                await spDisInfo.saveSpDis(spDisInfo);
            });
            it('分销商查询', async function () {
                const spDisList = await spugr.getSpDisList({ nameLike: spDisInfo.name, channelId: spDisInfo.channelId }).then(res => res.result.data.rows);
                const listInfo = spDisList.find(val => val.id == spDisInfo.id);
                expect(listInfo, `列表中未找到新增的分销商 id:${spDisInfo.id} name:${spDisInfo.name}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(spDisInfo, listInfo);
            });
        });
        describe('维护分销商商户信息', async function () {
            let saveRes;
            before(async function () {
                // spDisMerchantInfo = mockJsonParam.getSpDisMerchantJson(spDisInfo);
                const spDisMerchantInfo = mockJsonParam.merchantJson();
                spDisMerchantInfo.mobile = spDisInfo.mobile;
                spDisMerchantInfo.contact = spDisInfo.name;
                spDisMerchantInfo.unitId = disUserInfo.unitId;
                spDisMerchantInfo.shopId = disUserInfo.id;
                // console.log(`spDisMerchantInfo=${JSON.stringify(spDisMerchantInfo)}`);
                saveRes = await spugr.saveChannelMerchantForWeb(spDisMerchantInfo);
                // console.log(`saveRes=${JSON.stringify(saveRes)}`);

                // 手动审核通过  (一段时间后自动会通过) 
                // await spugr.updateSpMerchatFlagByPerson({ id: saveRes.result.data.id, flag: 1 });
            });
            it('分销商查询', async function () {
                const info = await spugr.getDisMerchant({ id: saveRes.result.data.id }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(saveRes.params.jsonParam, info, ['detailAddr']);
            });
        });
        // 创建分销商后会自动生成一个卖家应用
        // 第三方app用 现阶段不涉及
        describe.skip('卖家应用', async function () {
            before(async function () {
                await spAuth.staffLogin();
            });
            it('查询开放应用列表', async function () {
                const opAppList = await spop.getOpAppList({ orderBy: 'updated_date', orderByDesc: true, pageSize: 5 }).then(res => res.result.data.rows);
                expect(opAppList.find(val => val.tenantId == spDisInfo.id), `创建分销商后会未找到自动生成的卖家应用 tenantId=${spDisInfo.id}`).not.to.be.undefined;
            });
        });
    });

    // 使用渠道28 自动化测试的入口
    describe('合伙人', async function () {
        const disPartner = disManage.setupDisPartner();
        before('第一次绑定登录', async function () {
            await disPartner.login();
            // console.log(disPartner);
        });
        // 渠道员工登录 审核合伙人 
        describe('认证校验', async function () {
            before(async function () {
                await disReq.staffLogin();

                // 查询刚生成的合伙人
                const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                // console.log(channelMerchantList);

                const merchantInfo = channelMerchantList.find(data => data.mobile == disPartner.mobile);
                expect(merchantInfo, `获取渠道合伙人列表 未找到新注册的合伙人 mobile=${disPartner.mobile}`).not.to.be.undefined;

                disPartner.update(merchantInfo);
            });
            it('未审核前,合伙人状态校验', async function () {
                expect(disPartner).to.include({ auditFlag: 0, flag: 1 });
            });
            it('合伙人是否已认证校验', async function () {
                await disPartner.hasAuthenticatedForSourceCheck();
            });
            describe.skip('驳回审核', async function () {
                before(async function () {
                    await disPartner.authenticateForSource({ auditFlag: -1 });
                });
                it('合伙人审核状态校验', async function () {
                    const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                    const merchantInfo = channelMerchantList.find(data => data.mobile == disPartner.mobile);
                    common.isApproximatelyEqualAssert(disPartner, merchantInfo);
                });
                it('合伙人是否已认证校验', async function () {
                    await disPartner.hasAuthenticatedForSourceCheck();
                });
                it('合伙人登录校验', async function () {
                    await disPartner.login();
                });
            });
            describe('通过审核', async function () {
                before(async function () {
                    await disReq.staffLogin();

                    await disPartner.authenticateForSource({ auditFlag: 1 });
                });
                it('合伙人审核状态校验', async function () {
                    const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                    const merchantInfo = channelMerchantList.find(data => data.mobile == disPartner.mobile);
                    common.isApproximatelyEqualAssert(disPartner, merchantInfo);
                });
                it('合伙人是否已认证校验', async function () {
                    await disPartner.hasAuthenticatedForSourceCheck();
                });
                it('合伙人登录校验', async function () {
                    await disPartner.login();
                });
            });
        });
        describe.skip('渠道关系校验(saasId)', async function () {
            let saasRelId, spgSql;
            before('更新渠道关系', async function () {
                await disReq.staffLogin();
                await common.delay(500);
                spgSql = await mysql({ dbName: 'spgMysql' });

                const list = await spugr.getSpBuyerSellerList({ buyerTenantId: disPartner.buyerTenantId }).then(res => {
                    console.log(res);
                    return res.result.data.rows;
                });
                saasRelId = list.find(data => data.buyerTenantId == disPartner.buyerTenantId).id;

                await spugr.changeSaasId({ id: saasRelId, saasId: 1, buyerTenantId: disPartner.buyerTenantId });
            });
            after(async function () {
                await spgSql.end();
            });
            it('获取Saas列表', async function () {
                const res = await spugr.getSaasList({ tenantId: LOGINDATA.tenantId });
                console.log(res);

            });
            it('获取买家卖家关系列表', async function () {
                await spAuth.staffLogin();
                const list = await spugr.getSpBuyerSellerList({ buyerTenantId: disPartner.buyerTenantId }).then(res => res.result.data.rows);
                console.log(`list=${JSON.stringify(list)}`);
            });
            it('买家卖家关联表', async function () {
                const [spBuyerSellerLevel] = await spgSql.query(`select * from spugr.sp_buyer_seller_rel where buyer_tenant_id=${disPartner.buyerTenantId}`);
                console.log('sp_buyer_seller_rel');
                console.log(spBuyerSellerLevel);
            });
            it('买家卖家层级表', async function () {
                const [spBuyerSellerLevel] = await spgSql.query(`select * from spugr.sp_buyer_seller_level where buyer_tenant_id=${disPartner.buyerTenantId}`);
                console.log('spBuyerSellerLevel');
                console.log(spBuyerSellerLevel);
            });
            it('换绑后 用户登录', async function () {
                await disPartner.login();
                console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            });
            describe('还原渠道关系', async function () {
                before(async function () {
                    await disReq.staffLogin();
                    await spugr.changeSaasId({ id: saasRelId, saasId: disPartner.sellerTenantId, buyerTenantId: disPartner.buyerTenantId });
                });
                it('获取买家卖家关系列表', async function () {
                    const list = await spugr.getSpBuyerSellerList({ buyerTenantId: disPartner.buyerTenantId }).then(res => res.result.data.rows);
                    console.log(`list=${JSON.stringify(list)}`);
                });
                it('买家卖家层级表', async function () {
                    const [spBuyerSellerLevel] = await spgSql.query(`select * from spugr.sp_buyer_seller_level where buyer_tenant_id=${disPartner.buyerTenantId}`);
                    console.log('spBuyerSellerLevel');
                    console.log(spBuyerSellerLevel);
                });
                it('换绑后 用户登录', async function () {
                    await common.delay(5000);
                    await disPartner.login();
                    console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                });
            });
        });
        describe.skip('渠道分销商修改用户手机号', async function () {
            const oldMobile = disPartner.mobile;
            before(async function () {
                await disReq.staffLogin();

                await disPartner.changeMobileForSource({ mobile: common.add(disPartner.mobile, 1) });
                // console.log(disPartner);

            });
            it('合伙人列表 合伙人信息校验', async function () {
                const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                const merchantInfo = channelMerchantList.find(data => data.id == disPartner.id);
                common.isApproximatelyEqualAssert(disPartner, merchantInfo);
            });
            //等同用新手机号绑定注册
            it('合伙人用旧手机号登陆', async function () {
                const res = await disReq.spMerchantLogin({ code: oldMobile, check: false });
                expect(res.result).to.include({ msgId: 'dis_buyer_to_audit' });
            });
            it('合伙人用新手机号登陆', async function () {
                await disPartner.login();
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

            });
            // it.skip('获取店铺简要信息', async function () {
            //     const shopDetail = await spugr.getShopDetailByBuyerSeller({ buyerUnitId: LOGINDATA.unitId });
            //     console.log(`shopDetail=${JSON.stringify(shopDetail)}`);
            // });
        });
        describe('合伙人创建门店商户', async function () {
            let saveRes;
            before(async function () {
                await disPartner.login();

                const merchantJson = mockJsonParam.merchantJson();
                saveRes = await spugr.saveChannelMerchant(merchantJson);
                // console.log(`res=${JSON.stringify(saveRes)}`);

                await disReq.staffLogin();
            });
            after(async function () {
                // 强制转为正常 防止中信审核出问题影响后续流程
                // await spAuth.staffLogin();
                // await spugr.updateSpMerchatFlagByPerson({ id: saveRes.result.data.id, flag: 1 });
            });
            it('渠道用户账户列表校验', async function () {
                const channelMerchantList = await spugr.findChannelMerchant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                const merchantInfo = channelMerchantList.find(data => data.id == saveRes.result.data.id);
                expect(merchantInfo).not.to.be.undefined;
                common.isApproximatelyEqualAssert(saveRes.params.jsonParam, merchantInfo);
            });
            it('渠道根据买家id查找账号详细', async function () {
                const channelMerchantDetail = await spugr.getChannelMerchantDetail({ buyerId: disPartner.buyerTenantId, saasId: LOGINDATA.saasId });
                common.isApproximatelyEqualAssert(saveRes.params.jsonParam, channelMerchantDetail);
            });
            describe.skip('渠道员工修改门店商户信息', async function () {
                let updateJson;
                before(async function () {
                    await disReq.staffLogin();
                    updateJson = mockJsonParam.merchantJson({ mobile: disPartner.mobile });
                    updateJson.id = saveRes.result.data.id;
                    await spugr.updateChannelMerchantByAdmin(updateJson);
                });
                it('渠道用户账户列表校验', async function () {
                    const channelMerchantList = await spugr.findChannelMerchant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                    const merchantInfo = channelMerchantList.find(data => data.id == saveRes.result.data.id);
                    expect(merchantInfo).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(updateJson, merchantInfo);
                });
                it('账户详细校验', async function () {
                    const channelMerchantDetail = await spugr.getChannelMerchantDetail({ buyerId: disPartner.buyerTenantId });
                    common.isApproximatelyEqualAssert(updateJson, channelMerchantDetail);
                });
            });
            describe.skip('渠道员工修改门店商户信息', async function () {
                let updateJson;
                before(async function () {
                    // 将审核状态改为失败 失败状态才能修改
                    await spAuth.staffLogin();
                    await spugr.updateSpMerchatFlagByPerson({ id: saveRes.result.data.id, flag: 8 });
                    await common.delay(1000);

                    await disReq.staffLogin();
                    updateJson = mockJsonParam.merchantJson({ mobile: disPartner.mobile });
                    updateJson.id = saveRes.result.data.id;
                    await spugr.updateChannelMerchantByAdmin(updateJson)
                });
                it('渠道用户账户列表校验', async function () {
                    const channelMerchantList = await spugr.findChannelMerchant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                    const merchantInfo = channelMerchantList.find(data => data.id == saveRes.result.data.id);
                    expect(merchantInfo).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(updateJson, merchantInfo);
                });
                it('账户详细校验', async function () {
                    const channelMerchantDetail = await spugr.getChannelMerchantDetail({ buyerId: disPartner.buyerTenantId });
                    common.isApproximatelyEqualAssert(updateJson, channelMerchantDetail);
                });
            });
        });
        describe.skip('停用合伙人', async function () {
            before(async function () {
                await disReq.staffLogin();
                await disPartner.changeFlag({ flag: 0 });
            });
            it('合伙人列表 合伙人信息校验', async function () {
                const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                const merchantInfo = channelMerchantList.find(data => data.id == disPartner.id);
                common.isApproximatelyEqualAssert(disPartner, merchantInfo);
            });
            it('合伙人登陆', async function () {
                await disPartner.login();
            });
        });
        describe.skip('启用合伙人', async function () {
            before(async function () {
                await disReq.staffLogin();
                await disPartner.changeFlag({ flag: 1 });
            });
            it('合伙人列表 合伙人信息校验', async function () {
                const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                const merchantInfo = channelMerchantList.find(data => data.id == disPartner.id);
                common.isApproximatelyEqualAssert(disPartner, merchantInfo);
            });
            it('合伙人登陆', async function () {
                await disPartner.login();
            });
        });
        describe.skip('删除合伙人', async function () {
            before(async function () {
                await disReq.staffLogin();
                await disPartner.changeFlag({ flag: -1 });
            });
            it('合伙人列表 合伙人信息校验', async function () {
                const channelMerchantList = await spugr.findListUserByTenant({ mobile: disPartner.mobile }).then(res => res.result.data.rows);
                const merchantInfo = channelMerchantList.find(data => data.id == disPartner.id);
                common.isApproximatelyEqualAssert(disPartner, merchantInfo);
            });
            it('合伙人登陆', async function () {
                await disPartner.login();
            });
        });
    });

    describe.skip('买家', async function () {
        const disClient = disManage.setupDisClient();
        before(async function () {
            await disReq.defMerchantLogin();
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

            await disClient.login();
            console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);

        });
        it('', async function () {

        });
    });

});