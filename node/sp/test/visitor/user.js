const common = require('../../../lib/common');
const format = require('../../../data/format');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAccount = require('../../data/spAccount');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const userHobbyManage = require('../../help/spugr/userHobby');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');

// 修改nickName 不会改变shopName
// 修改shopName 会修改nickName
describe('注册/登录', function () {
    this.timeout(30000);
    const myMobile = spAccount.client.mobile, myCaptcha = '000000';
    let curMobile = common.getRandomMobile(), curPass;

    after('将注册的手机号从白名单去除', async function () {
        await spReq.delWhite();
    });

    it('1.验证码登录', async function () {
        await spugr.spLogin({
            code: myMobile,
            captcha: myCaptcha,
            authcType: 2
        });
        await spugr.spLogOut();
    });

    describe('用户注册/登录-基本流程', function () {
        let registerRes, updateUserRes, submitAuditRes;
        before(async function () {
            // 注册一个新用户-未审核
            // registerRes = await spReq.userRegister();
            // curMobile = registerRes.params.jsonParam.mobile;
            // curPass = registerRes.params.jsonParam.pass;

            //使用新注册用户登录
            await spReq.registerLogin({ code: curMobile });
        });

        after(async function () {
            //注销用户
            // await spReq.spLogin({ code: curMobile });
            // await spugr.deleteUser();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        });

        // by 德贵
        // im测试和审核是按需创建的，就是真正发起会话是（startConv）时才创建；
        // 线上的是注册时就创建的，因为老的app版本不兼容只能这么处理，所以线上不用考虑本用例
        it('获取焦点-无im信息', async function () {
            const res = await spugr.getSpStateFocus();
            expect(res.result).to.includes({ msg: '成功' });
        });

        // 接口已废弃 
        it.skip('相同手机号注册', async function () {
            let repeteRegisterParam = _.cloneDeep(registerRes.params.jsonParam);
            repeteRegisterParam.check = false;
            const res = await spugr.userRegister(repeteRegisterParam);
            expect(res.result).to.includes({
                msg: '该手机号码的用户已存在',
            });
        });

        // tenantFlag实时查询 可能会变
        it('校验并返回会话', async function () {
            const res = await spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId });
            common.isApproximatelyEqualAssert(LOGINDATA, res.result.data, ['tenantFlag', 'lastSessionTime', 'regTime']);
        });

        //以后用失效的sessionid 查询返回的是{}。现在返回的是gateway_sessionid_illegal,会话已失效返回的code为 -9
        it('退出登陆后校验并返回会话', async function () {
            const sessionId = LOGINDATA.sessionId;
            await spReq.spLogOut();
            const res = await spugr.fetchUserSessionLite({ sessionId, check: false });
            expect(res.result, '过期的会话查询返回错误').to.satisfy((data) => {
                return data.msgId && (data.msgId == 'gateway_sessionid_illegal' || data.msgId == 'sessionId参数不正确!');
            });
        });

        //根据注册时间 3天内允许
        //-1代表不允许试用  没有试用了
        it('检查是否允许买家试用', async function () {
            await spReq.spLogin({
                code: curMobile
            });
            const res = await spugr.verifyTryUse();
            expect(res.result.data).to.includes({ val: -1 });
        });

        it('修改用户信息', async function () {
            updateUserRes = await spugr.updateUser({
                userName: common.getRandomStr(5),
                nickName: common.getRandomStr(5),
                avatar: common.getRandomNumStr(5),
                birthday: common.getDateString([-5, 0, 0]),
                birthLunarFlag: 0,
                gender: 1
            });
            const userInfo = await spugr.getUser();
            await spReq.spLogin({
                code: curMobile
            });
            common.isApproximatelyEqualAssert(updateUserRes.params, userInfo.result.data);
        });

        it.skip('6.修改密码', async function () {
            await spugr.changePass({ newPass: '123456' });
            curPass = '123456';
            //旧密码
            const res = await spugr.spLogin({
                code: curMobile,
                pass: registerRes.params.jsonParam.pass,
                authcType: 1,
                check: false
            });
            expect(res.result).to.includes({
                msg: 'user_old_pwd_error'
            });

            //新密码
            await spugr.spLogin({
                code: registerRes.params.jsonParam.mobile,
                pass: curPass,
                authcType: 1,
            });
        });

        it.skip('7.密码重置', async function () {
            await spugr.resetPass();
            const oldPass = curPass;
            curPass = '000000';

            //旧密码
            const res = await spugr.spLogin({
                code: registerRes.params.jsonParam.mobile,
                pass: oldPass,
                authcType: 1,
                check: false
            });
            expect(res.result).to.includes({
                msg: 'user_old_pwd_error'
            });

            //新密码
            await spugr.spLogin({
                code: registerRes.params.jsonParam.mobile,
                pass: curPass,
                authcType: 1,
                // check: false
            });
        });

        //管理员
        it('买家门店认证列表-未认证', async function () {
            await spAuth.staffLogin();
            const auditInfo = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, jsonParam: { flag: 99, mobile: curMobile, summitDateGte: common.getCurrentDate() } }).then(res => res.result.data.rows.shift());
            expect(auditInfo, `mobile:${curMobile} 注册后,未在买家门店认证列表中找到其未认证信息`).not.to.be.undefined;
            // 和顾准新确认，查99返回的是用户数据，flag与查询的flag无关 为1 18-12-05 lxx
            expect(auditInfo, `mobile:${curMobile} 未审核信息有误`).to.includes({ flag: 1, mobile: curMobile });
        });

        it('平台审核', async function () {
            // 买家提交店铺审核
            await spReq.spLogin({ code: curMobile });
            const areaJson = await mockJsonParam.getAreaJson();
            submitAuditRes = await spCommon.submitAuditRecord(await mockJsonParam.submitAuditJson({ areaJson }));

            // 管理员审核通过
            await spAuth.staffLogin();
            await spCommon.changeAuditRecord({
                shopId: submitAuditRes.result.data.id,
                flag: 1,
                auditRemark: '条件符合审核条件，同意了'
            });
            await spAuth.staffLogout();
        });

        it('未提交调研信息,校验调研信息是否完善', async function () {
            await spReq.spLogin({ code: curMobile });
            const data = await spCommon.verifyBuyerSurvery().then(res => res.result.data);
            expect(data.val).to.equal(0);
        });

        describe('调研信息', async function () {
            let buyerSurveryInfo;
            before('买家提交调研信息', async function () {
                await spReq.getDictList(['age', 'theme', 'masterClass']);
                //marketId和goodsSourceCity需要从分类配置ec-spadmin-spCatConfig-findCatConfig中获取
                const marketId = await spCommon.findCatConfig({ type: 3 }).then(res => res.result.data['3'].find(val => val.flag == 1).id);
                const goodsSourceCity = await spCommon.findCatConfig({ type: 1 }).then(res => res.result.data['1'].find(val => val.flag == 1).id);
                const userInfo = await spCommon.findShopAuditsByBuyer({}).then(res => res.result.data);

                buyerSurveryInfo = {
                    customGender: common.getRandomNum(0, 2),
                    customAge: BASICDATA['2028'].slice(0, 2).map(val => val.id).join(','),
                    shopStyle: BASICDATA['2016'].slice(0, 2).map(val => val.id).join(','),
                    goodsSourceCity: goodsSourceCity,
                    marketId: marketId,
                    masterClassId: userInfo.masterClassId,
                };
                await spCommon.fillBuyerSurvery(buyerSurveryInfo);
            });
            it('买家验证调研信息是否完善', async function () {
                await common.delay(500);
                const data = await spCommon.verifyBuyerSurvery().then(res => res.result.data);
                // expect(data.val).to.equal(1);
            });
            // 调研信息一般只写一次，但没有限制次数
            it('重复提交调研信息', async function () {
                await spCommon.fillBuyerSurvery(buyerSurveryInfo);
            });
            it('管理员查看买家调研信息', async function () {
                await spAuth.staffLogin();
                const shopAudits = await spCommon.findBuyerShopAudits({ mobile: curMobile }).then(res => res.result.data.rows[0]);
                common.isApproximatelyEqualAssert(buyerSurveryInfo, shopAudits);
            });
        });

        describe('设置主营类目/副营类目/新主营类目', async function () {
            let masterClassId, viceMasterClassId, standardClassId;
            before(async function () {
                await spReq.spLogin({ code: curMobile });
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

                // 查询平台配置的推荐店铺
                await spCommon.findCatConfig({ type: 7 }).then(res => {
                    const data = res.result.data['7'];
                    masterClassId = data[0].typeValue;
                    viceMasterClassId = data.slice(1, 3).map(row => row.typeValue).join(',');
                });
                // 2.2.8暂不加
                // standardClassId = await spCommon.findCatConfig({ type: 13 }).then(res => res.result.data['13'].find(val => val.flag == 1).typeId);
                await spCommon.updateBuyerShopMessage({ masterClassId, viceMasterClassId, standardClassId });

            });
            it('买家查看审批信息', async function () {
                const userInfo = await spCommon.findShopAuditsByBuyer();
                // console.log(userInfo);
                expect(userInfo.result.data).to.includes({ masterClassId, viceMasterClassId });
            });
            describe('买家第一次点关注时弹出运营端配置店铺', async function () {
                let suggestShops = [], spSuggesFocusShopList = [];
                it('首次关注', async function () {
                    [suggestShops, spSuggesFocusShopList] = await Promise.all([spCommon.findSpSuggFocus({ masterClassId }).then(res => res.result.data.rows),
                    spUp.findSpSuggesFocusShop().then(res => {
                        return res.result.data.rows ? res.result.data.rows.map(val => { return { tenantId: val.tenantId, unitId: val.unitId } }) : [];
                    })]);
                    suggestShops.length > 0 ? common.isArrayContainArrayAssert(suggestShops, spSuggesFocusShopList) : expect(suggestShops).to.eql(spSuggesFocusShopList);
                });
                // 买家没有关注店铺时,同一会话再次请求 弹出运营端配置店铺 返回空由前端自己控制
                // 服务端只有在买家有关注店铺时，才返回空
                it('同一会话 再次关注', async function () {
                    const totalShop = await spUp.findSpSuggesFocusShop().then(res => res.result.data.total);
                    spSuggesFocusShopList.length > 0 ? expect(totalShop).to.be.equal(spSuggesFocusShopList.length) : expect(totalShop).to.be.below(1);
                });
            });
        });

        describe('买家标记', async function () {
            let tags, setTags;
            before('给买家店铺打标签', async function () {
                await spAuth.staffLogin();
                let res = await spugr.getDictList({ "flag": 1, "typeId": 2024 }).then(res => res.result.data.rows);
                tags = common.randomSort(res).slice(0, 2).map(val => val.codeValue);
                //设置标签
                setTags = await spCommon.setTagsForBuyer({ id: submitAuditRes.result.data.id, tags: tags.join() });
            });

            it('查询标记', async function () {
                await spReq.spLogin({
                    code: curMobile
                });
                let res = await spCommon.findShopAuditsByBuyer({});
                expect(res.result.data.tags).to.be.equal(setTags.params.jsonParam.tags);
            });

            //管理员
            it('管理员查询标记', async function () {
                await spAuth.staffLogin();
                let res = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, nameLike: submitAuditRes.params.jsonParam.shopName, submitDateS: common.getCurrentDate(), submitDateE: common.getCurrentDate(), tags: setTags.params.jsonParam.tags.split(',') });
                let info = res.result.data.rows.find(element => element.auditObjId == submitAuditRes.result.data.id);
                expect(info.tags.join()).to.be.equal(setTags.params.jsonParam.tags);
            });
            //管理员
            it('搜索条件', async function () {
                let res = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, tagsAll: setTags.params.jsonParam.tags.split(',').map(val => Number(val)) });
                res.result.data.rows.forEach(val => {
                    common.isArrayContainArrayAssert(res.params.jsonParam.tagsAll, val.tags, [], `${JSON.stringify(val)}\n标记不一致的也显示了,搜索条件是${res.params.jsonParam.tags}，搜索到的结果是${val.tags}`);
                });
            });
            it('管理员查看买家店铺列表', async function () {
                const shopInfo = await spCommon.findBuyerShops({ pageSize: 10, pageNo: 1, shopNameLike: submitAuditRes.params.jsonParam.shopName, tagsAll: tags }).then(res => res.result.data.rows.shift());
                expect(shopInfo, `买家店铺列表未找到店铺信息 shopName=${submitAuditRes.params.jsonParam.shopName}`).not.to.be.undefined;
                common.isArrayContainArrayAssert(tags, shopInfo.tags);
            });
        });

        describe('修改手机号', async function () {
            let oldLoginData;
            before('修改当前用户手机号', async function () {
                // 修改客户信息后,使用会话校验ec-spugr-spLogin-fetchSession返回的值 没有更新userName tenantName等
                // 等德贵修改 暂时直接重新登录
                await spugr.spLogin({
                    code: curMobile,
                    captcha: '0000',
                    authcType: 2
                });
                oldLoginData = _.cloneDeep(LOGINDATA);

                // if (updateUserRes.result.code == 0) oldLoginData.userName = updateUserRes.params.jsonParam.userName;
                const res = await spugr.changeMobile({ mobile: Number(curMobile) + 1 });
                curMobile = res.params.jsonParam.mobile;
            });
            it('管理员查看用户手机号', async function () {
                await spAuth.staffLogin();
                const shopAuditsList = await spCommon.findBuyerShopAudits({ mobile: curMobile }).then(res => res.result.data.rows);

                common.isApproximatelyEqualAssert({
                    buyerTenantId: oldLoginData.tenantId,
                    mobile: curMobile,
                }, shopAuditsList[0]);
            });
            it('使用新手机号登陆', async function () {
                await spReq.spLogin({ code: curMobile });
                common.isApproximatelyEqualAssert(oldLoginData, LOGINDATA, ['code', 'loginTime', 'sessionId', 'mobile', 'extProps']);
            });
        });

        describe('租户注册', function () {
            let entTenantRes, staffRes, updateRes;
            before('注册企业租户', async function () {
                await spReq.spLogin({
                    code: curMobile,
                });
                //新增企业租户
                entTenantRes = await spReq.createEntTenant({
                    mobile: curMobile,
                    name: `企业${common.getRandomStr(5)}`
                });

                await spugr.addShopByTenantId({ tenantId: LOGINDATA.tenantId });

                // console.log(`entTenantRes=${JSON.stringify(entTenantRes)}`);
                //数据维护 防止被认为是脏数据
                let shopInfo = await mockJsonParam.shopJson();
                shopInfo.id = entTenantRes.result.data.val;
                shopInfo.name = entTenantRes.params.jsonParam.name;
                shopInfo.contactName = entTenantRes.params.jsonParam.name;//联系人姓名
                shopInfo.contactPhone = curMobile;//联系人电话
                await spugr.updateShop(shopInfo);
            });

            it('注册信息校验', async function () {
                let tenantids = await spugr.findAvailableTenantIds();
                let expectJson = {};
                expectJson[entTenantRes.result.data.val] = entTenantRes.params.jsonParam.name;
                expect(tenantids.result.data, '注册失败').to.includes({});
            });

            it('获取店铺联系人信息', async function () {
                const res = await spugr.getShopContractMsg({ id: LOGINDATA.tenantId });
                // console.log(`res=${JSON.stringify(res)}`);
            });

            it('修改租户', async function () {
                let updateJson = {
                    id: entTenantRes.result.data.val,
                    name: `企业修改${common.getRandomStr(5)}`
                }
                updateRes = await spugr.updateTenant(updateJson);
                let tenantids = await spugr.findAvailableTenantIds();
                expect(tenantids.result.data[updateJson.id], '修改失败').to.be.equal(updateJson.name);
            });
            //这里还没打断言，目前没有好的方法来验证是否绑定用户成功
            it('租户添加员工', async function () {
                //注册一个新用户，用来充当店员
                staffRes = await spReq.registerNewUser();
                await spReq.spLogin({
                    code: curMobile
                });
                await spugr.changeTenantInSession({ tenantId: entTenantRes.result.data.val });
                await spugr.creatStaff({
                    userId: staffRes.result.data.userId,
                    code: '004',
                    rem: '店长'
                });
            });

            describe('注销店铺', function () {
                let shopInfo;
                before('注销店铺', async function () {
                    await spReq.spLogin({
                        code: curMobile,
                    });
                    await spugr.changeTenantInSession({ tenantId: entTenantRes.result.data.val });

                    //注销租户
                    await spugr.cancelShopById({ id: entTenantRes.result.data.val });
                });

                //这里只返回未注销的店铺，无法验证。
                it.skip('注销后名称检查', async function () {
                    await spReq.spLogin({
                        code: curMobile,
                    });
                    let name = await spugr.findAvailableTenantIds().then(res => res.result.data[entTenantRes.result.data.val]);
                    expect(name).to.be.equal(`${updateRes.params.jsonParam.name}(已注销)`)
                });

                it('注销后搜索店铺', async function () {
                    await spAuth.staffLogin();
                    let shopInfo = await spugr.findAllShopByParams({ pageSize: 10, pageNo: 1, shadowFlag: 0, nameLike: entTenantRes.params.jsonParam.name, orderBy: 'updatedDate', orderByDesc: 'true' });

                    let info = shopInfo.result.data.rows.find(val => val.id == entTenantRes.result.data.val);
                    let exp = { name: `${entTenantRes.params.jsonParam.name}(已注销)`, usingFlag: 0, unUnsingReason: '门店注销' };
                    common.isApproximatelyEqualAssert(exp, info);
                });

                it('注销后查看店铺详情', async function () {
                    let shopInfo = await spugr.getShop({ id: entTenantRes.result.data.val, check: false });

                    expect(shopInfo.result).to.includes({ "msgId": "shop_haved_cancel_by_user" });
                });

                it('注销后店铺上线', async function () {
                    await spAuth.staffLogin();
                    let res = await spugr.offlineShopById({ id: entTenantRes.result.data.val, check: false });
                    expect(res.result).to.includes({ "msgId": "shop_haved_cancel_by_user" });
                });
            });
        });
    });

    describe('用户偏好', async function () {
        before(async function () {
            await spReq.spLogin({ code: curMobile });
        });
        it('未设置过用户偏好的用户,判断用户偏好信息是否完善', async function () {
            const isPerfect = await spugr.verifySpUserHobbyPerfect().then(res => res.result.data.val);
            expect(isPerfect).to.be.equal(0);
        });
        describe('设置用户偏好(已完善)', async function () {
            let userHobby;
            before(async function () {
                const json = await userHobbyManage.getRandomUserHobby();
                userHobby = userHobbyManage.setUpUserHobby();
                await userHobby.saveOrUpdate(json);
            });
            it('数据校验', async function () {
                await userHobby.checkInfoBySql();
            });
            it('判断用户偏好信息是否完善', async function () {
                const isPerfect = await spugr.verifySpUserHobbyPerfect().then(res => res.result.data.val);
                expect(isPerfect).to.be.equal(1);
            });
        });
    });

    describe('店铺管理', function () {
        //企业租户,企业租户id,店铺信息
        let tenantRes, tenantId, shopInfo, saveRes;
        before('登录+注册新企业租户', async () => {
            await spReq.spLogin({
                code: myMobile
            });

            //新增企业租户 保存租户id
            tenantRes = await spReq.createEntTenant({
                mobile: myMobile,
                name: `企业${common.getRandomStr(5)}`
            });
            await common.delay(500);


            await spugr.spLogin({
                code: myMobile,
                authcType: 2,
                captcha: '000000'
            });
            tenantId = tenantRes.result.data.val;//租户的id就是门店的id
            changeRes = await spugr.changeTenantInSession({ tenantId });
            let json = mockJsonParam.merchantJson();
            json.name = tenantRes.params.jsonParam.name;
            json.contact = tenantRes.params.jsonParam.name;
            json.mobile = myMobile;
            let res = await spugr.createMerchat(json);
            await spAuth.staffLogin();
            await common.delay(1000);
            await spugr.updateSpMerchatFlagByPerson({ id: res.result.data.id, flag: 1 });

            //店铺上线，也就是店铺通过审核，要进行这一步操作，不然搜索查不到
            await spugr.onlineShopById({ id: tenantId });
            await spugr.spLogin({
                code: myMobile,
                authcType: 2,
                captcha: '000000'
            });
            changeRes = await spugr.changeTenantInSession({ tenantId });

        });

        after('注销卖家', async function () {
            await spReq.spLogin({
                code: myMobile
            });
            await spugr.changeTenantInSession({ tenantId });
            await spugr.cancelShopById({ id: tenantId, check: false });
        });

        // 接口已废弃
        // it('1.同租户新增其他店铺', async () => {
        //     const res = await spugr.addShopByTenantId({
        //         tenantId, authCode: '0000', check: false
        //     });
        //     expect(res.result, `${JSON.stringify(res)}`).to.includes({ msgId: 'spugr_shop_name_same', msg: '已存在同名店铺' });
        // });

        it('2.店铺认证信息保存', async function () {
            let saveParam = await mockJsonParam.shopJson();
            // let areaJson = await mockJsonParam.getAreaJson();
            saveRes = await spugr.authcShop(saveParam);
            expect(saveRes.result.data.val, '门店不一致').to.be.equal(tenantId);
            const shopDetail = await spugr.getShopDetail();
            common.isApproximatelyEqualAssert(saveParam, shopDetail.result.data);
        });

        it('3.获取当前登录的店铺详情', async function () {
            const shopDetail = await spugr.getShopDetail();
            const shopInfo = await spugr.getShop({ id: tenantId });
            common.isApproximatelyEqualAssert(shopDetail.result.data, shopInfo.result.data);
        });

        it('4.添加名称相同的店铺', async () => {
            let shopDetail = await spugr.getShopDetail();
            const res = await spReq.createEntTenant({
                mobile: myMobile,
                name: shopDetail.result.data.name,
                check: false
            });
            expect(res.result, '添加相同名称的店铺成功').to.includes({ msg: '已存在同名租户' });
        });

        it('5.修改店铺信息', async () => {
            let updateJson = _.cloneDeep(saveRes.params.jsonParam);
            updateJson.name = updateJson.name.replace('企业', '门店');
            updateJson.id = tenantId;
            const editRes = await spugr.updateShop(updateJson);
            shopInfo = await spugr.getShop({ id: tenantId });
            common.isApproximatelyEqualAssert(editRes.params.jsonParam, shopInfo.result.data, []);
        });

        it('6.手机号精准查询店铺', async () => {
            const searchList = await spugr.findShopByMobile({ mobile: myMobile, pageSize: 10, pageNo: 1, orderByDesc: 'true', orderBy: 'createdDate' }).then(res => res.result.data.rows);
            const searchInfo = searchList.find(val => val.tenantId = shopInfo.result.data.tenantId);
            common.isApproximatelyEqualAssert(shopInfo.result.data, searchInfo, ['freeType']);
        });

        it('7.名字模糊查找店铺', async () => {
            const res = await spugr.findShopByNameLike({ jsonParam: { nameLike: '门店' }, pageSize: 10, orderBy: 'createdDate', orderByDesc: true });
            res.result.data.rows.forEach((data) => expect(data.name).to.includes('门店'));
            common.isArrayContainArrayAssert([shopInfo.result.data], res.result.data.rows);
        });

        it('8.根据地址编码查找店铺', async () => {
            const {
                cityCode, marketId, floor
            } = shopInfo.result.data;
            const res = await spugr.findShopByAddr({ cityCode, marketId, floor, pageSize: 10 });
            common.isArrayContainArrayAssert([shopInfo.result.data], res.result.data.rows);
        });

        it('9.混合搜索名称+手机号+档口', async function () {
            const searchList = await spugr.findShopBySearchToken({ searchToken: shopInfo.result.data.name, pageSize: 10, orderByDesc: 'true', orderBy: 'createdDate' }).then(res => res.result.data.rows);
            const searchInfo = searchList.find(val => val.tenantId = shopInfo.result.data.tenantId);
            common.isApproximatelyEqualAssert(shopInfo.result.data, searchInfo, ['freeType']);
        });

        it('10.店铺多搜索值组合搜索（买家游客）', async function () {
            const searchList = await spugr.findShopByParams({ nameLike: '门店', mobile: `${myMobile}`, pageSize: 10, pageSize: 10, orderByDesc: 'true', orderBy: 'createdDate' }).then(res => res.result.data.rows);
            const searchInfo = searchList.find(val => val.tenantId = shopInfo.result.data.tenantId);
            common.isApproximatelyEqualAssert(shopInfo.result.data, searchInfo, ['freeType']);
        });

        it('11.店铺多搜索值组合搜索（平台）', async function () {
            const searchList = await spugr.findAllShopByParams({ nameLike: '门店', mobile: `${myMobile}`, pageSize: 10, orderByDesc: 'true', orderBy: 'createdDate' }).then(res => res.result.data.rows);
            const searchInfo = searchList.find(val => val.tenantId = shopInfo.result.data.tenantId);
            common.isApproximatelyEqualAssert(shopInfo.result.data, searchInfo, ['freeType']);
        });

        // 目前这里没有限制重复点赞，可以一直点  店家可以给自己店铺点赞吗？目前是可以的
        it('12.点赞-获取点赞数', async function () {
            let likeNum = await spugr.getShopLikeNum({ id: tenantId });
            await spugr.likeShop({ id: tenantId });
            let finalLikeNum = await spugr.getShopLikeNum({ id: tenantId });
            expect(finalLikeNum.result.data.val, '点赞失败').to.be.equal(common.add(likeNum.result.data.val, 1))
        });

        it('13.店铺下线', async function () {
            await spAuth.staffLogin();
            await spugr.offlineShopById({ id: tenantRes.result.data.val });
            const res = await spugr.findShopByNameLike({ pageSize: 10, orderBy: 'createdDate', orderByDesc: true, jsonParam: { nameLike: tenantRes.params.jsonParam.name } });
            let info = res.result.data.rows.find(res => res.name == tenantRes.params.jsonParam.name);
            expect(info, '下线后依然能搜到该店铺').to.be.equal(undefined);
        });

        // http://zentao.hzdlsoft.com:6082/zentao/task-view-2203.html
        describe('营业执照和信用代码', async function () {
            let auditMsg = { licence: common.getRandomStr(5), socialCreditCode: `${Date.now()}${common.getRandomNum(100, 999)}` };
            before(async function () {
                await spugr.spLogin({ code: myMobile, authcType: 2, captcha: '000000' });
                await spugr.changeTenantInSession({ tenantId });

                await spugr.submitAuditMsg({ id: tenantId, ...auditMsg });
                auditMsg.auditFlag = 2;
            });
            it('获取店铺详情', async function () {
                const shopDetail = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
                expect(shopDetail).to.includes(auditMsg);
            });
            it('店铺多搜索值组合搜索（平台）', async function () {
                const shopList = await spugr.findAllShopByParams({ auditFlag: 2, socialCreditCode: auditMsg.socialCreditCode }).then(res => res.result.data.rows);
                const shopDetail = shopList.find(shop => shop.tenantId == tenantId);
                expect(shopDetail).to.includes(auditMsg);
            });
            describe('平台审核卖家社会信用代码相关信息--驳回', async function () {
                before(async function () {
                    await spAuth.staffLogin();

                    await spugr.auditSubmitAuditMsg({ id: tenantId, auditFlag: 0, auditAdvice: '驳回' });
                    auditMsg.auditFlag = 0;
                });
                it('获取店铺详情', async function () {
                    const shopDetail = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
                    expect(shopDetail).to.includes(auditMsg);
                });
                it('店铺多搜索值组合搜索（平台）', async function () {
                    this.retries(2);
                    await common.delay(200);
                    const shopList = await spugr.findAllShopByParams({ auditFlag: 0, socialCreditCode: auditMsg.socialCreditCode }).then(res => {
                        // console.log(`tenantId=${tenantId}`);
                        // console.log(res);
                        return res.result.data.rows;
                    });
                    const shopDetail = shopList.find(shop => shop.tenantId == tenantId);
                    expect(shopDetail).to.includes(auditMsg);
                });
            });
            describe('平台审核卖家社会信用代码相关信息--通过', async function () {
                before(async function () {
                    await spAuth.staffLogin();

                    await spugr.auditSubmitAuditMsg({ id: tenantId, auditFlag: 1, auditAdvice: '通过' });
                    auditMsg.auditFlag = 1;
                });
                it('获取店铺详情', async function () {
                    const shopDetail = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
                    expect(shopDetail).to.includes(auditMsg);
                });
                it('店铺多搜索值组合搜索（平台）', async function () {
                    this.retries(2);
                    await common.delay(200);
                    const shopList = await spugr.findAllShopByParams({ auditFlag: 1, socialCreditCode: auditMsg.socialCreditCode }).then(res => res.result.data.rows);
                    const shopDetail = shopList.find(shop => shop.tenantId == tenantId);
                    expect(shopDetail).to.includes(auditMsg);
                });
            });
        });

        describe('管理员修改店铺', async function () {
            let editInfo = {};
            before(async function () {
                await spAuth.staffLogin();

                const shopInfo = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
                editInfo = _.cloneDeep(saveRes.params.jsonParam);
                editInfo.id = tenantId;
                editInfo.logisticsMarketId = shopInfo.marketId;
                await spCommon.updateShopByAdmin(editInfo);
            });
            it('店铺多搜索值组合搜索（平台）', async function () {
                const shopInfo = await spugr.findAllShopByParams({ mobile: myMobile, logisticsMarketId: editInfo.logisticsMarketId }).then(res => res.result.data.rows.find(val => val.id == tenantId));
                common.isApproximatelyEqualAssert(editInfo, shopInfo);
            });
            it('卖家查看店铺信息', async function () {
                await spReq.spLogin({ code: myMobile });
                await spugr.changeTenantInSession({ tenantId });

                const shopInfo = await spugr.getShop({ id: tenantId }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(editInfo, shopInfo);
            });
        });
    });

    // 卖家分享依然需要提交审核信息
    describe('卖家分享邀请买家注册', async function () {
        let sellerInfo, sellerShopDetail, buyerMobile;
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);

            sellerShopDetail = await spugr.getShopDetail().then(res => res.result.data);

            buyerMobile = common.getRandomMobile();
            await spugr.spLogin({
                srcType: 3,//卖家分享
                srcId: sellerInfo.tenantId,//srcType=3时 卖家租户id
                code: buyerMobile,
                captcha: myCaptcha,
                authcType: 2
            });
        });
        it('买家用户信息校验', async function () {
            const userInfo = await spugr.getUser();
            const exp = format.dataFormat(LOGINDATA, 'mobile;userName;tenantId');
            exp.flag = 1;
            common.isApproximatelyEqualAssert(exp, userInfo.result.data);
        });
        it('买家购物', async function () {

        });
        it('管理员查看买家店铺审核信息', async function () {
            await spAuth.staffLogin();
            const shopInfo = await spCommon.findBuyerShopAudits({ mobile: buyerMobile });
            const exp = Object.assign(format.dataFormat(LOGINDATA, 'buyerTenantId=tenantId;mobile;buyerUnitId=unitId'), format.dataFormat(sellerShopDetail, 'masterClassId'));
            exp.auditFlag = 1;
            common.isApproximatelyEqualAssert(exp, shopInfo.result.data);
        });
    });

    describe('手机号格式', function () {
        const errorMsg = '手机号码格式错误';
        it('1.手机号非纯数字', async function () {
            const res = await spReq.userRegister({ mobile: `158${common.getRandomNumStr(7)}a`, check: false });
            expect(res.result, `${JSON.stringify(res)}`).to.includes({ msg: errorMsg });
        });
        it('2.手机号长度不足11位', async function () {
            const res = await spReq.userRegister({ mobile: `158${common.getRandomNumStr(7)}`, check: false });
            expect(res.result, `${JSON.stringify(res)}`).to.includes({ msg: errorMsg });
        });
        it('3.手机号长度超过11位', async function () {
            const res = await spReq.userRegister({ mobile: `158${common.getRandomNumStr(9)}`, check: false });
            expect(res.result, `${JSON.stringify(res)}`).to.includes({ msg: errorMsg });
        });
        it('4.国家码+手机号码', async function () {
            const res = await spReq.userRegister({ mobile: `86158${common.getRandomNumStr(8)}`, check: false });
            expect(res.result, `${JSON.stringify(res)}`).to.includes({ msg: errorMsg });
        });
    });
});

