const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');

// http://c.hzdlsoft.com:7082/Wiki.jsp?page=Sp invite buyer sms
// 同一天只能操作一次，默认上限是3次，可后台扩展，手机号有校验格式，注册过的不加入
// 此功能已移除
describe.skip('客户通知', function () {
    this.timeout(30000);
    let lastTaskInfo, curTaskInfo;

    before(async function () {
        await spReq.spSellerLogin();

        // 客户通知查询
        curTaskInfo = await spugr.getInviteTaskMsg().then(res => res.result.data);
        console.log(`curTaskInfo=${JSON.stringify(curTaskInfo)}`);
    });
    after(async function () {
    });

    it('保存邀请买家开通任务', async function () {
        //一天之内只能一次
        if (curTaskInfo.operationDate.includes(common.getCurrentDate())) return;

        const sfRes = await spugr.saveInviteTask(inviteTaskJson());
        console.log(`sfRes=${JSON.stringify(sfRes)}`);

        const taskInfo = await spugr.getInviteTaskMsg().then(res => res.result.data);
        [lastTaskInfo, curTaskInfo] = [curTaskInfo, taskInfo];
        lastTaskInfo.operationDate = common.getCurrentTime();
        lastTaskInfo.totalNums = common.add(lastTaskInfo.totalNums, sfRes.params.jsonParam.details.length);
        lastTaskInfo.sendTimes++;
        common.isApproximatelyEqualAssert(lastTaskInfo, curTaskInfo);

        const qlRes = await spugr.getInviteTaskList({ shopName: LOGINDATA.shopName });
        // // console.log(`qlRes=${JSON.stringify(qlRes)}`);
    });

    it('重复保存邀请买家开通任务', async function () {
        let json = inviteTaskJson({ check: false });
        json.check = false;
        const sfRes = await spugr.saveInviteTask(json);
        expect(sfRes.result).to.includes({ msgId: 'send_only_once_one_day' });
    });

    it('允许客户再次通知', async function () {
        const qlRes = await spugr.getInviteTaskList({ shopName: LOGINDATA.shopName });
        console.log(`qlRes=${JSON.stringify(qlRes)}`);

        await spugr.restartInviteTask({ id: 10 });

        curTaskInfo = await spugr.getInviteTaskMsg().then(res => res.result.data);
        console.log(`curTaskInfo=${JSON.stringify(curTaskInfo)}`);

        const qlRes2 = await spugr.getInviteTaskList({ shopName: LOGINDATA.shopName });
        console.log(`qlRes=${JSON.stringify(qlRes2)}`);
    });

    it('关闭客户通知', async function () {
        await spugr.stopInviteTask({ id: 10 });

        curTaskInfo = await spugr.getInviteTaskMsg().then(res => res.result.data);
        console.log(`curTaskInfo=${JSON.stringify(curTaskInfo)}`);

        const qlRes = await spugr.getInviteTaskList({ shopName: LOGINDATA.shopName });
        console.log(`qlRes=${JSON.stringify(qlRes)}`);
    });

    it('', async function () {

    });

});




function inviteTaskJson() {
    let json = {
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        rem: 'autotest',
        inviteContent: `${LOGINDATA.shopName}邀请你加入商陆好店，一手货源，轻松拿货，点击http://t.cn/E76G19a下载`,
        // inviteModelId: '短信内容的模板id',
        details: [],
    };
    json.details = new Array(2).fill({}).map((detail) => {
        return { mobile: `12${common.getRandomNumStr(9)}`, customerName: common.getRandomChineseStr(3) };
    });
    return json;
};