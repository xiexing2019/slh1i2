const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const auditManage = require('../../help/spAdmin/auditManage');
const billManage = require('../../help/billManage');
const dresManage = require('../../help/dresManage');

// http://zentao.hzdlsoft.com:6082/zentao/task-view-1688.html
// 买家提交审核 -> 管理员禁用 flag=4 
// 禁用后 买家可以下单 不能再次提交审核 (经涂辉确认)
// 管理员修改审核状态后 权限更变为对应状态
describe('买家审核-禁用', async function () {
    this.timeout(30000);
    const mobile = common.getRandomMobile(),
        shopAudit = auditManage.setupSpAudit();

    before(async function () {
        // 买家登录 未认证
        await spReq.spClientLogin({ code: mobile });
        shopAudit.setUserInfo();

        // 买家提交审核信息
        const submitAuditJson = await shopAudit.mockAuditJson();
        await shopAudit.submitAuditRecord(submitAuditJson);

        // 平台驳回
        await spAuth.staffLogin();
        await shopAudit.changeAuditRecord({ flag: 4 });
    });

    it('搜索查看买家店铺审核记录列表（管理员）', async function () {
        const auditList = await spCommon.findBuyerShopAudits({ flag: 4, tenantId: shopAudit.tenantId }).then(res => res.result.data.rows);
        const auditDetail = auditList.find(data => data.buyerTenantId == shopAudit.tenantId);
        expect(auditDetail, `店铺审核记录列表 未找到审核记录 auditObjId=${shopAudit.auditObjId}`).not.to.be.undefined;
        common.isApproximatelyEqualAssert(shopAudit.getBuyerShopAuditsExp(), auditDetail);
    });

    it('校验买家登录信息-tenantFlag', async function () {
        await spReq.spClientLogin({ code: mobile });
        expect(LOGINDATA).to.includes({ tenantFlag: 4 });
    });

    it('校验并返回会话-tenantFlag', async function () {
        const data = await spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId }).then(res => res.result.data);
        expect(data).to.includes({ tenantFlag: 4 });
    });

    it('买家查看审批信息', async function () {
        const auditDetail = await spCommon.findShopAuditsByBuyer();
        common.isApproximatelyEqualAssert(shopAudit.getBuyerShopAuditsExp(), auditDetail);
    });

    it('买家再次提交审核信息', async function () {
        const submitAuditJson = await shopAudit.mockAuditJson();
        submitAuditJson.check = false;
        const submitAuditRes = await spCommon.submitAuditRecord(submitAuditJson);
        expect(submitAuditRes.result).to.includes({ msgId: 'shop_already_forbid' });
    });

    it('买家下单', async function () {
        await spReq.spSellerLogin();
        const sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin({ code: mobile });
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        const json = mockJsonParam.addAddrJson({
            provinceCode: 110000,
            cityCode: 110100,
            countyCode: 110101,
        });
        json.recInfo.isDefault = 1;
        await spMdm.saveUserRecInfo(json);
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

        const dresList = await spDresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
        const dres = dresManage.setupDres().setByEs(dresList[0]);
        await dres.searchAndSetByDetail();

        // 买家下单
        const purBill = billManage.setUpPurBill().addDres(dres).mockPurParam();
        const res = await spTrade.savePurBill({ orders: purBill.orders, check: false });
        expect(res.result).to.includes({ msgId: 'tenant_buyer_forbid' });
    });

});