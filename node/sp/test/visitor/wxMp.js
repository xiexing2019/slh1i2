const common = require('../../../lib/common');
const spAccount = require('../../data/spAccount');
const spugr = require('../../../reqHandler/sp/global/spugr');

// mysql spugr.sp_nim_account
describe('微信-online', async function () {
    this.timeout(30000);
    let userInfo;
    before(async function () {
        await spugr.userLoginWithBindOpenId({ code: spAccount.client.mobile });
        userInfo = _.cloneDeep(LOGINDATA);
        // console.log(`userInfo=${JSON.stringify(userInfo)}`);
    });
    it('修改状态账号设备在线状态 客户端隐藏至后台', async function () {
        await spugr.updateOnlineFlag({ onlineFlag: 2 });
    });
    it('修改状态账号设备在线状态 离线', async function () {
        await spugr.updateOnlineFlag({ onlineFlag: 0 });
    });
    it('修改状态账号设备在线状态 在线', async function () {
        await spugr.updateOnlineFlag({ onlineFlag: 1 });
    });

    describe('获取会话', async function () {
        afterEach(async function () {
            await spugr.spLogOut();
        });
        it.skip('sessionId+openId', async function () {
            await spugr.wxFetchSessionWithLogin();
            common.isApproximatelyEqualAssert(userInfo, LOGINDATA, ['loginTime', 'lastSessionTime', 'credId']);
        });
        it('openId', async function () {
            await spugr.wxFetchSessionWithLogin({ openId: userInfo.extProps.wxOpenId });
            common.isApproximatelyEqualAssert(userInfo, LOGINDATA, ['loginTime', 'lastSessionTime', 'sessionId', 'credId']);
        });
    });

});