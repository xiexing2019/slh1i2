const common = require('../../../../lib/common');
const spReq = require('../../../help/spReq');
const spugr = require('../../../../reqHandler/sp/global/spugr');
const spAccount = require('../../../data/spAccount');
const spConfig = require('../../../../reqHandler/sp/global/spConfig');
const billManage = require('../../../help/billManage');
const dresManage = require('../../../help/dresManage');
const mockJsonParam = require('../../../help/mockJsonParam');
const spMdm = require('../../../../reqHandler/sp/biz_server/spmdm');
const spDresb = require('../../../../reqHandler/sp/biz_server/spdresb');
// const phoneApi = require('../../../../reqHandler/publicApis/phone');

describe('未认证用户', async function () {
    this.timeout(30000);

    const users = [{ phone: '15050290000', city: '苏州市', postcode: 320500 }, { phone: '15821120000', city: '上海市', postcode: 310100 }, { phone: '13989850000', city: '杭州市', postcode: 330100 }];
    for (let index = 0; index < users.length; index++) {
        const user = users[index];
        it('归属地校验', async function () {
            await spReq.spLogin({ code: user.phone });
            expect(LOGINDATA, `手机号${user.phone} tenantFlag!=3 请检查`).to.includes({ tenantFlag: 3 });

            const citys = await spDresb.getRecommendCity().then(res => res.result.data.rows);
            expect(citys).to.eql([{ [user.postcode]: user.city }]);
        });

        it('查询未认证买家部分信息', async function () {
            await spReq.spLogin({ code: user.phone });

            const rows = await spugr.findSubmitBuyer({ mobile: user.phone, auditFlag: 3 }).then(res => res.result.data.rows);
            expect(rows[0]).to.includes({ code: LOGINDATA.uniCode, id: LOGINDATA.tenantId, mobile: user.phone });
        });
    }


});