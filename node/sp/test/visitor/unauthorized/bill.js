const common = require('../../../../lib/common');
const spReq = require('../../../help/spReq');
const spugr = require('../../../../reqHandler/sp/global/spugr');
const spAccount = require('../../../data/spAccount');
const spConfig = require('../../../../reqHandler/sp/global/spConfig');
const billManage = require('../../../help/billManage');
const dresManage = require('../../../help/dresManage');
const mockJsonParam = require('../../../help/mockJsonParam');
const spMdm = require('../../../../reqHandler/sp/biz_server/spmdm');
const spDresb = require('../../../../reqHandler/sp/biz_server/spdresb');


// 1、测试环境用未认证买家登陆获取sessionId
// 2、添加任意商品调下单接口，下单时注意如果商品加价部分未完成的话(跟郑潮确认)，手动将price和money取得比一般的高一些
// 3、模拟支付
// 4、通过3返回的payDetailId查询ec-liquidation-bizOrderDetail-getBizOrderDetailListByPayId接口，返回入账列表
// 5、通过http://xxx/spb/api.do?apiKey=ec-config-param-getParamVal接口查询code为sp_combine_mch_tenant_id的参数，拿到平台入账分成的tenant_id
// 6、在4的返回结果中取5返回的tenant_id，并与总量比较，是否正确。
describe('未认证用户下单', async function () {
    this.timeout(30000);
    const dres = dresManage.setupDres(),
        purBill = billManage.setUpPurBill(),
        priceRate = 1.1;
    before(async function () {
        await spReq.spSellerLogin();
        const sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin({ code: '12903270001' });
        // 校验 认证状态-未认证
        if (LOGINDATA.tenantFlag != 3) {
            console.warn(`${LOGINDATA.mobile} tenantFlag=${LOGINDATA.tenantFlag}!=3,非未认证状态,请检查`);
            this.skip();
        }
        // 设置用户默认收货地址
        await purBill.getRecAddressInfo();

        const dresList = await spDresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
        dres.setByEs(dresList[0]);
        await dres.searchAndSetByDetail();

        // 手动加价 等郑潮商品加价完成后 再去除
        dres.spu.pubPrice = common.mul(dres.spu.pubPrice, priceRate);

        // 买家下单+支付
        purBill.addDres(dres).mockPurParam();
        await purBill.savePurBill();
        await purBill.createPay();

        // 入账操作较慢
        await common.delay(1000);
        // console.log(purBill);
    });

    it('根据支付ID查入账记录', async function () {
        this.retries(5);
        await common.delay(1000);
        const tenantId = await spConfig.getParamVal({
            code: 'sp_combine_mch_tenant_id',
            domainKind: 'system',
            ownerKind: 6,
            // ownerId: 306850,
        }).then(res => res.result.data.val);

        const bizOrderDetail = await purBill.getBizOrderDetailBySubBizOrderId();
        purBill.checkBizOrderDetail({ bizOrderDetail, tenantId, priceRate })

    });


});