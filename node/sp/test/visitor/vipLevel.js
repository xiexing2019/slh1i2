const common = require('../../../lib/common');
const format = require('../../../data/format');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAccount = require('../../data/spAccount');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const saasLevelManage = require('../../help/spugr/saasLevel');

// vip 默认等级 1游客 2登陆 未认证 3认证
// spugr.sp_vip_level  会员等级表
// spugr.sp_saas_level
describe('会员等级', async function () {
    this.timeout(30000);
    const clientMobile = common.getRandomMobile();
    let clientInfo;

    before(async function () {
        // 买家登录 未认证
        await spReq.spClientLogin({ code: clientMobile });
        clientInfo = _.cloneDeep(LOGINDATA);
    });

    it('获取会员等级列表-未认证', async function () {
        const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId }).then(res => res.result.data.rows);
        const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
        expect(vipData).to.includes({ vipLevel: 2 });
    });

    it('获取会员等级列表-认证', async function () {
        // 买家提交店铺审核
        await spReq.spLogin({ code: clientMobile });
        const areaJson = await mockJsonParam.getAreaJson();
        const submitAuditJson = await mockJsonParam.submitAuditJson({ areaJson });
        const submitAuditRes = await spCommon.submitAuditRecord(submitAuditJson);

        // 管理员审核通过
        await spAuth.staffLogin();
        await spCommon.changeAuditRecord({
            shopId: submitAuditRes.result.data.id,
            flag: 1,
            auditRemark: '条件符合审核条件，同意了'
        });
        await common.delay(500);

        const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId }).then(res => res.result.data.rows);
        const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
        expect(vipData).to.includes({ vipLevel: 3 });
    });

    describe('修改Vip等级', async function () {
        let id;
        before(async function () {
            await spAuth.staffLogin();

            const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId }).then(res => res.result.data.rows);
            const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
            expect(vipData).not.to.be.undefined;
            id = vipData.id;
        });
        it('vip1', async function () {
            await spugr.changeVipLevel({ id, tenantId: clientInfo.tenantId, vipLevel: 1 });

            const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId, vipLevel: 1 }).then(res => res.result.data.rows);
            const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
            expect(vipData).not.to.be.undefined;
        });
        it('vip2', async function () {
            await spugr.changeVipLevel({ id, tenantId: clientInfo.tenantId, vipLevel: 2 });

            const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId, vipLevel: 2 }).then(res => res.result.data.rows);
            const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
            expect(vipData).not.to.be.undefined;
        });
        it('vip3', async function () {
            await spugr.changeVipLevel({ id, tenantId: clientInfo.tenantId, vipLevel: 3 });

            const vipList = await spugr.findVipList({ tenantId: clientInfo.tenantId, vipLevel: 3 }).then(res => res.result.data.rows);
            const vipData = vipList.find(data => data.saasId == 1 && data.tenantId == clientInfo.tenantId);
            expect(vipData).not.to.be.undefined;
        });
    });


});

// 
describe('spSaasLevel', async function () {
    this.timeout(30000);
    const saasLevel = saasLevelManage.setupSpSaasLevel();
    before(async function () {
        await spAuth.staffLogin();
        await saasLevel.getRandom().save();
    });
    it('用户级别划分列表', async function () {
        const saasLevelList = await spugr.findSaasLevelList({ saasId: saasLevel.saasId }).then(res => res.result.data.rows);
        const saasLevelDetail = saasLevelList.find(data => data.levelCode == saasLevel.levelCode);
        expect(saasLevel, `未找到 id=${saasLevel.id}`).not.to.be.undefined;
        common.isApproximatelyEqualAssert(saasLevel, saasLevelDetail);
    });
    describe('停用', async function () {
        before(async function () {
            await saasLevel.changeFlag({ flag: 0 });
        });
        it('用户级别划分列表', async function () {
            const saasLevelList = await spugr.findSaasLevelList({ saasId: saasLevel.saasId }).then(res => res.result.data.rows);
            const saasLevelDetail = saasLevelList.find(data => data.levelCode == saasLevel.levelCode);
            expect(saasLevelDetail, `未找到 id=${saasLevel.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(saasLevel, saasLevelDetail);
        });
    });
    describe('启用', async function () {
        before(async function () {
            await saasLevel.changeFlag({ flag: 1 });
        });
        it('用户级别划分列表', async function () {
            const saasLevelList = await spugr.findSaasLevelList({ saasId: saasLevel.saasId }).then(res => res.result.data.rows);
            const saasLevelDetail = saasLevelList.find(data => data.levelCode == saasLevel.levelCode);
            expect(saasLevelDetail, `未找到 id=${saasLevel.id}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(saasLevel, saasLevelDetail);
        });
    });
    describe('删除', async function () {
        before(async function () {
            await saasLevel.changeFlag({ flag: -1 });
        });
        it('用户级别划分列表', async function () {
            const saasLevelList = await spugr.findSaasLevelList({ saasId: saasLevel.saasId }).then(res => res.result.data.rows);
            const saasLevelDetail = saasLevelList.find(data => data.levelCode == saasLevel.levelCode);
            expect(saasLevelDetail, `可找到删除的 id=${saasLevel.id}`).to.be.undefined;
        });
    });
});
