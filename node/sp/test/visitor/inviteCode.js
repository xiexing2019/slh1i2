const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
// const spAccount = require('../../data/spAccount');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spbi = require('../../../reqHandler/sp/global/spbi');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const configParam = require('../../help/configParamManager');

// 邀请码 oa中设置  由她销售-邀请码管理
// 若crmAnalysisByBd统计列表中找不到业务员,可能为服务端忘记推送,oa中点击初始化同步
describe('邀请码注册', async function () {
    this.timeout(30000);
    let sellerTenantId, clusterCode;
    before(async function () {
        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;
        clusterCode = LOGINDATA.clusterCode;
    });

    describe('一类邀请码', async function () {
        let bdId, inviteCountBef, auditCountBef, clientMobile = 12 + common.getRandomNumStr(9), submitAuditRes;
        // console.log(`clientMobile=${JSON.stringify(clientMobile)}`);
        before(async function () {
            await spAuth.staffLogin();
            const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
            bdId = crmAnalysisInfo.id;
            inviteCountBef = crmAnalysisInfo.inviteCount;
            auditCountBef = crmAnalysisInfo.auditCount;
            await spReq.spLogin({ code: clientMobile });
        });

        it('审核列表', async function () {
            await spAuth.staffLogin();
            let shopAuditList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows);
            expect(shopAuditList.length).to.equal(0);
        });
        it('地推统计-业务统计', async function () {
            const crmAnalysisInfo = await spbi.crmAnalysisByBd({ wrap: true, bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
            // console.log(`crmAnalysisInfo=${JSON.stringify(crmAnalysisInfo)}`);
            common.isApproximatelyEqualAssert({ inviteCount: inviteCountBef, auditCount: auditCountBef, }, crmAnalysisInfo);
        });
        it('地推统计-业务统计-查看客户', async function () {
            const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, pageNo: 1, bdId, timeKind: 0 }).then(res => res.result.data.invalidCust.rows);
            let info = custAnalysisInfo.find(val => val.mobile == clientMobile);
            // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
            expect(info).to.be.undefined;
        });

        describe('直接审核通过--平台未确认', async function () {
            let custAnalysisInfo;
            before(async function () {
                await spReq.spLogin({ code: clientMobile });
                const areaJson = await mockJsonParam.getAreaJson();
                let auditjson = await mockJsonParam.submitAuditJson({ areaJson });
                auditjson.inviteCode = 'autotest1';
                submitAuditRes = await spCommon.submitAuditRecord(auditjson);
                // console.log(`submitAuditRes=${JSON.stringify(submitAuditRes)}`);

            });
            it('买家查看审核信息', async function () {
                let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
                expect(shopAuditsInfo.result.data.auditFlag, '一类邀请码+提交认证信息后未成功开通店铺').to.be.equal(1);
            });
            it('审核列表', async function () {
                await spAuth.staffLogin();
                let shopAuditList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                // console.log(`shopAuditList[0]=${JSON.stringify(shopAuditList)}`);
                expect(shopAuditList, `审核列表查不到手机号--${clientMobile}--的审核信息`).to.not.be.undefined;
                expect(shopAuditList.flag, `使用一类邀请码提交店铺认证，没有直接审核通过，审核列表flag不是1：${shopAuditList}`).to.equal(1);
            });
            it('业务统计', async function () {
                this.retries(2);
                const crmAnalysisInfo = await spbi.crmAnalysisByBd({ wrap: true, bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                // console.log(`crmAnalysisInfo=${JSON.stringify(crmAnalysisInfo)}`);
                common.isApproximatelyEqualAssert({ inviteCount: common.add(inviteCountBef, 1), auditCount: common.add(auditCountBef, 1) }, crmAnalysisInfo);
            });

            //注册用户和认证用户列表里面都应该有此用户
            it('业务统计-查看客户-注册用户', async function () {
                await common.delay(2000);
                custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0, now: common.getCurrentDate() }).then(res => res.result.data);
                let info = custAnalysisInfo.inviteCust.rows.find(val => val.mobile == clientMobile);
                // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
                let exp = _.cloneDeep(submitAuditRes.params.jsonParam);
                exp.mobile = clientMobile;
                exp.auditDate = common.getCurrentDate();
                common.isApproximatelyEqualAssert(exp, info);
            });

            it('业务统计-查看客户-认证用户', async function () {
                let info = custAnalysisInfo.auditCust.rows.find(val => val.mobile == clientMobile);
                let exp = _.cloneDeep(submitAuditRes.params.jsonParam);
                exp.mobile = clientMobile;
                exp.auditDate = common.getCurrentDate();
                common.isApproximatelyEqualAssert(exp, info);
            });
            describe('平台未确认时，买家修改店铺信息', async function () {
                let updateShop;
                before(async function () {
                    //买家修改店铺信息
                    // console.log(`修改前submitAuditRes=${JSON.stringify(submitAuditRes)}`);
                    await spReq.spLogin({ code: clientMobile });
                    const areaJson = await mockJsonParam.getAreaJson();
                    let json = await mockJsonParam.submitAuditJson({ areaJson });
                    updateShop = await spCommon.updateBuyerShopAuditMessage(json);
                    submitAuditRes.params.jsonParam = updateShop.params.jsonParam;
                });
                it('买家修改店铺信息后，买家查看审核信息', async function () {
                    let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                    common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
                    expect(shopAuditsInfo.result.data.auditFlag, '提交认证信息+一类邀请码后未成功开通店铺').to.be.equal(1);
                });
                it('平台端审核列表查看买家用户', async function () {
                    await spAuth.staffLogin();
                    const shopAuditsList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    // console.log(`平台shopAuditsList=${JSON.stringify(shopAuditsList)}`);
                    expect(shopAuditsList, `审核列表查不到该买家用户信息`).to.not.be.undefined;
                    common.isApproximatelyEqualAssert(updateShop.params.jsonParam, shopAuditsList);
                });
                it('查看买家店铺列表', async function () {
                    const buyerShopListInfo = await spCommon.findBuyerShops({ shopNameLike: updateShop.params.jsonParam.shopName }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    expect(buyerShopListInfo, `查看买家店铺列表查不到该客户信息`).to.not.be.undefined;
                    // console.log(`查看买家店铺列表buyerShopListInfo=${JSON.stringify(buyerShopListInfo)}`);
                    common.isApproximatelyEqualAssert(updateShop.params.jsonParam, buyerShopListInfo, ['remark']);
                });
                it('查看买家', async function () {
                    await spAuth.staffLogin();
                    const buyerShopListInfo = await spugr.getBuyerList({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    expect(buyerShopListInfo, `查看买家查不到该客户信息`).to.not.be.undefined;
                    // console.log(`查看买家buyerListInfo=${JSON.stringify(buyerShopListInfo)}`);
                    common.isApproximatelyEqualAssert(updateShop.params.jsonParam, buyerShopListInfo, ['remark']);
                });
            });


        });
        describe('驳回', async function () {
            let custAnalysisInfo;
            before(async function () {
                //驳回
                await spAuth.staffLogin();
                await spCommon.changeAuditRecord({
                    shopId: submitAuditRes.result.data.id,
                    flag: 9,
                    auditRemark: bdId
                });
            });
            it('驳回后买家查看审核信息', async function () {
                await spReq.spLogin({ code: clientMobile });
                const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                // console.log(`驳回后shopAuditsInfo=${JSON.stringify(shopAuditsInfo)}`);
                common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
                expect(shopAuditsInfo.result.data.auditFlag, '驳回后买家查看审核信息auditFlag不为0').to.be.equal(0);
            });
            it('审核列表', async function () {
                await spAuth.staffLogin();
                const shopAuditList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                // console.log(`shopAuditList[0]=${JSON.stringify(shopAuditList)}`);
                expect(shopAuditList, `审核列表查不到手机号：${clientMobile}审核信息`).to.not.be.undefined;
                expect(shopAuditList.flag, `使用一类邀请码提交店铺认证，驳回审核后，审核列表flag不是9：${shopAuditList}`).to.equal(9);
            });

            it('业务统计', async function () {
                const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                // console.log(`驳回后crmAnalysisInfo=${JSON.stringify(crmAnalysisInfo)}`);
                common.isApproximatelyEqualAssert({ inviteCount: common.add(inviteCountBef, 1), auditCount: auditCountBef }, crmAnalysisInfo);
            });

            it('业务统计-查看客户-注册用户', async function () {
                custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data);
                const info = custAnalysisInfo.inviteCust.rows.find(val => val.mobile == clientMobile);
                expect(info, `驳回买家审核后,注册用户里面也没有该买家了`).to.not.be.undefined;
            });

            it('业务统计-查看客户-认证用户', async function () {
                const info = custAnalysisInfo.auditCust.rows.find(val => val.mobile == clientMobile);
                expect(info, `驳回买家审核后,认证用户列表里面依然有这个客户`).to.be.undefined;
            });

            it('驳回后买家修改店铺信息', async function () {
                await spReq.spLogin({ code: clientMobile });
                const areaJson = await mockJsonParam.getAreaJson();
                let json = await mockJsonParam.submitAuditJson({ areaJson });
                json.check = false;
                let updateShop = await spCommon.updateBuyerShopAuditMessage(json);
                // console.log(`updateShop=${JSON.stringify(updateShop)}`);
                expect(updateShop.result).to.includes({ msg: "还未审核通过,请重新提交认证", msgId: "resumbit_audit" });
            });

        });
        describe('再次审核通过', async function () {
            before(async function () {
                //邀请码已失效，必须要审核通过
                await spAuth.staffLogin();
                await spCommon.changeAuditRecord({
                    shopId: submitAuditRes.result.data.id,
                    flag: 1,
                    auditRemark: '条件符合审核条件，同意了'
                });
                await common.delay(500);

            });
            it('再次审核通过后买家查看审核信息', async function () {
                await spReq.spLogin({ code: clientMobile });
                const shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                // console.log(`再次审核通过买家查看审核信息shopAuditsInfo=${JSON.stringify(shopAuditsInfo)}`);
                common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
                expect(shopAuditsInfo.result.data.auditFlag, '驳回后买家查看审核信息auditFlag不为9').to.be.equal(1);
            });
            it('审核列表', async function () {
                await spAuth.staffLogin();
                let shopAuditList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                // console.log(`shopAuditList[0]=${JSON.stringify(shopAuditList)}`);
                expect(shopAuditList, `审核列表查不到手机号：${clientMobile}审核信息`).to.not.be.undefined;
                expect(shopAuditList.flag, `使用一类邀请码提交店铺认证，没有直接审核通过，审核列表flag不是1：${shopAuditList}`).to.equal(1);
            });
            it('业务统计', async function () {
                const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                // console.log(`再次邀请crmAnalysisInfo.inviteCount=${JSON.stringify(crmAnalysisInfo)}`);
                common.isApproximatelyEqualAssert({ inviteCount: common.add(inviteCountBef, 1), auditCount: common.add(auditCountBef, 1) }, crmAnalysisInfo);
            });

            it('业务统计-查看客户-认证用户', async function () {
                const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, timeKind: 0, bdId }).then(res => res.result.data);
                let info = custAnalysisInfo.auditCust.rows.find(val => val.mobile == clientMobile);
                // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
                expect(info, '重新审核通过后,认证用户列表还是没有这个用户').to.not.be.undefined;
            });

            it('再次审核通过后买家修改店铺信息', async function () {
                await spReq.spLogin({ code: clientMobile });
                const areaJson = await mockJsonParam.getAreaJson();
                let json = await mockJsonParam.submitAuditJson({ areaJson });
                json.check = false;
                let updateShop = await spCommon.updateBuyerShopAuditMessage(json);
                // console.log(`updateShop=${JSON.stringify(updateShop)}`);
                expect(updateShop.result).to.includes({ msg: "平台已确认认证信息,请勿重复提交", msgId: "plat_confirm_already" });
            });

        });

        //0 小于达标金额 1 等于达标金额 2 大于达标金额
        for (let i = 0; i < 3; i++) {
            describe.skip('买家下单', async function () {
                let bdId, targetAmount, billCustNumBef, invalidCustNumBef, salesRes, billCountBef, billMoneyBef;
                before(async function () {
                    //先审核通过
                    //获取系统参数   地推下单达标金额
                    targetAmount = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_crm_mark_money' });
                    const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                    bdId = crmAnalysisInfo.id;
                    billCustNumBef = crmAnalysisInfo.billCustNum;
                    invalidCustNumBef = crmAnalysisInfo.invalidCustNum;
                    const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.billCust.rows.find(val => val.mobile == clientMobile));
                    // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
                    // billCountBef = custAnalysisInfo.billCount;
                    billMoneyBef = custAnalysisInfo.money;
                    //开单
                    salesRes = await getSalesBill({ clientMobile, sellerTenantId });
                    // console.log(`salesRes=${JSON.stringify(salesRes)}`);
                    await common.delay(500);
                });

                it('业务统计', async function () {
                    const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                    common.isApproximatelyEqualAssert({ billCustNum: i == 2 ? billCustNumBef : common.add(billCustNumBef, 1), invalidCustNum: i == 1 ? common.sub(invalidCustNumBef, 1) : invalidCustNumBef }, crmAnalysisInfo);
                });
                it('业务统计-查看客户', async function () {
                    const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.billCust.rows.find(val => val.mobile == clientMobile));
                    common.isApproximatelyEqualAssert({ billMoney: common.add(billMoneyBef, salesRes.result.data.totalMoney), billCount: common.add(billCountBef, 1), }, custAnalysisInfo);
                });
                describe('买家退货', async function () {
                    let returnBill;
                    before(async function () {
                        returnBill = await getReturnBill({ clientMobile, targetAmount: targetAmount.val, returnMoney: i == 2 ? -2 : i });
                        // console.log(`returnBill=${JSON.stringify(returnBill.params)}`);
                        await common.delay(500);
                    });
                    it('业务统计', async function () {
                        const crmAnalysisInfo = await spbi.crmAnalysisByBd({ wrap: true, bdNameLike: '林子祥', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '林子祥'));
                        let invalidCustNum;
                        if (i == 0) {
                            invalidCustNum = common.add(invalidCustNumBef, 1);
                        } else if (i == 1) {
                            invalidCustNum = common.sub(invalidCustNumBef, 1);
                        } else {
                            invalidCustNum = invalidCustNumBef;
                        };
                        common.isApproximatelyEqualAssert({ billCustNum: i == 1 ? common.add(billCustNumBef, 1) : billCustNumBef, invalidCustNum }, crmAnalysisInfo);
                    });
                    it('业务统计-查看客户', async function () {
                        const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.billCust.rows.find(val => val.mobile == clientMobile));
                        common.isApproximatelyEqualAssert({ billMoney: i == 0 ? billMoneyBef : billMoneyBef + salesRes.params.jsonParam.orders[0].main.money - returnBill.params.jsonParam.main.totalMoney, billCount: i == 0 ? billCountBef : common.add(billCountBef, 1), }, custAnalysisInfo);
                    });
                });
            });
        };
    });

    describe('二类邀请码', async function () {
        let bdId, addressList, inviteCountBef, auditCountBef, clientMobile = 12 + common.getRandomNumStr(9), submitAuditRes;
        before(async function () {
            await spAuth.staffLogin();
            const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '徐世国', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '徐世国'));
            bdId = crmAnalysisInfo.id;
            inviteCountBef = crmAnalysisInfo.inviteCount;
            auditCountBef = crmAnalysisInfo.auditCount;
            // 注册买家,使用邀请码,直接通过审核
            // console.log(`clientMobile=${JSON.stringify(clientMobile)}`);
            await spReq.spLogin({ code: clientMobile });
            submitAuditRes = await spCommon.submitAuditRecord({ inviteCode: 'autotest' });
            //获取收货地址
            addressList = await spMdm.getUserRecInfoList({ typeId: 1 });
            const areaJson = await mockJsonParam.getAreaJson();
            const saveUserRecInfoRes = await spMdm.saveUserRecInfo(await mockJsonParam.addAddrJson(areaJson));
            //设置默认收货地址
            await spMdm.setDefaultUserRecInfo({
                id: saveUserRecInfoRes.result.data.val
            });
        });
        it('买家查看收货地址', async function () {
            expect(addressList.result.data.rows.length, `认证时只输入二类邀请码，买家查看收货地址不为空${addressList}`).to.equal(0);
        });
        it('买家查看审核信息', async function () {
            let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
            common.isApproximatelyArrayAssert(submitAuditRes.params.jsonParam, shopAuditsInfo.result.data);
            expect(shopAuditsInfo.result.data.auditFlag, '提交认证时填写二类邀请码未成功开通店铺').to.be.equal(1);
        });
        it('审核列表', async function () {
            await spAuth.staffLogin();
            let shopAuditList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
            // console.log(`shopAuditList[0]=${JSON.stringify(shopAuditList[0])}`);
            expect(shopAuditList, `审核列表查不到手机号：${clientMobile}审核信息`).to.not.be.undefined;
            expect(shopAuditList.flag, `使用二类邀请码注册登录，审核列表flag不是1`).to.equal(1);
        });
        it('地推统计-业务统计', async function () {
            this.retries(2);
            const crmAnalysisInfo = await spbi.crmAnalysisByBd({ wrap: true, bdNameLike: '徐世国', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '徐世国'));
            // console.log(`crmAnalysisInfo=${JSON.stringify(crmAnalysisInfo)}`);
            common.isApproximatelyEqualAssert({ inviteCount: common.add(inviteCountBef, 1), auditCount: common.add(auditCountBef, 1) }, crmAnalysisInfo);
        });
        it('地推统计-业务统计-查看客户-注册用户', async function () {
            const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.inviteCust.rows.find(val => val.mobile == clientMobile));
            // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
            expect(custAnalysisInfo, `使用二类邀请码注册登录，审核通过，业务统计-查看客户中没有该客户`).to.not.be.undefined;
        });
        it('地推统计-业务统计-查看客户-认证用户', async function () {
            const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0, now: common.getCurrentTime() }).then(res => res.result.data.auditCust.rows.find(val => val.mobile == clientMobile));
            // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
            expect(custAnalysisInfo, `使用二类邀请码注册登录，审核通过，业务统计-查看客户中没有该客户`).to.not.be.undefined;
        });
        describe('修改买家店铺信息', async function () {
            let updateShop;
            before(async function () {
                //修改买家信息
                await spReq.spLogin({ code: clientMobile });
                const areaJson = await mockJsonParam.getAreaJson();
                let json = await mockJsonParam.submitAuditJson({ areaJson });
                updateShop = await spCommon.updateBuyerShopAuditMessage(json);
                // console.log(`\nupdateShop=${JSON.stringify(updateShop)}`);
            });
            it('买家修改店铺信息后，买家查看审核信息', async function () {
                let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                common.isApproximatelyArrayAssert(updateShop.params.jsonParam, shopAuditsInfo.result.data);
                expect(shopAuditsInfo.result.data.auditFlag, '提交认证信息+一类邀请码后未成功开通店铺').to.be.equal(1);
            });
            it('查看买家', async function () {
                await spAuth.staffLogin();
                const buyerShopListInfo = await spugr.getBuyerList({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                expect(buyerShopListInfo, `查看买家查不到该客户信息`).to.not.be.undefined;
                // console.log(`查看买家buyerListInfo=${JSON.stringify(buyerShopListInfo)}`);
                common.isApproximatelyEqualAssert(updateShop.params.jsonParam, buyerShopListInfo, ['remark']);
            });
            it('查看买家店铺列表', async function () {
                const buyerShopListInfo = await spCommon.findBuyerShops({ shopNameLike: updateShop.params.jsonParam.shopName }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                expect(buyerShopListInfo, `查看买家店铺列表查不到该客户信息`).to.not.be.undefined;
                // console.log(`查看买家店铺列表buyerShopListInfo=${JSON.stringify(buyerShopListInfo)}`);
                common.isApproximatelyEqualAssert(updateShop.params.jsonParam, buyerShopListInfo, ['remark']);
            });
            it('平台端审核列表查看买家用户', async function () {
                await spAuth.staffLogin();
                const shopAuditsList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                // console.log(`平台shopAuditsList=${JSON.stringify(shopAuditsList)}`);
                expect(shopAuditsList, `审核列表查不到该买家用户信息`).to.not.be.undefined;
                common.isApproximatelyEqualAssert(updateShop.params.jsonParam, shopAuditsList);
            });
            describe('再次修改店铺认证信息', async function () {
                let againUpdateShop;
                before(async function () {
                    await spReq.spLogin({ code: clientMobile });
                    const areaJson = await mockJsonParam.getAreaJson();
                    const json = await mockJsonParam.submitAuditJson({ areaJson });
                    againUpdateShop = await spCommon.updateBuyerShopAuditMessage(json);
                    // console.log(`\nagainUpdateShop=${JSON.stringify(againUpdateShop)}`);
                });
                it('买家再次修改店铺信息后，买家查看审核信息', async function () {
                    let shopAuditsInfo = await spCommon.findShopAuditsByBuyer();
                    common.isApproximatelyArrayAssert(againUpdateShop.params.jsonParam, shopAuditsInfo.result.data);
                    expect(shopAuditsInfo.result.data.auditFlag, '提交认证信息+一类邀请码后未成功开通店铺').to.be.equal(1);
                });
                it('查看买家', async function () {
                    await spAuth.staffLogin();
                    const buyerShopListInfo = await spugr.getBuyerList({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    expect(buyerShopListInfo, `查看买家查不到该客户信息`).to.not.be.undefined;
                    // console.log(`查看买家buyerListInfo=${JSON.stringify(buyerShopListInfo)}`);
                    common.isApproximatelyEqualAssert(againUpdateShop.params.jsonParam, buyerShopListInfo, ['remark']);
                });
                it('查看买家店铺列表', async function () {
                    const buyerShopListInfo = await spCommon.findBuyerShops({ shopNameLike: againUpdateShop.params.jsonParam.shopName }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    expect(buyerShopListInfo, `查看买家店铺列表查不到该客户信息`).to.not.be.undefined;
                    // console.log(`查看买家店铺列表buyerShopListInfo=${JSON.stringify(buyerShopListInfo)}`);
                    common.isApproximatelyEqualAssert(againUpdateShop.params.jsonParam, buyerShopListInfo, ['remark']);
                });
                it('平台端审核列表查看买家用户', async function () {
                    await spAuth.staffLogin();
                    const shopAuditsList = await spCommon.findBuyerShopAudits({ mobile: clientMobile }).then(res => res.result.data.rows.find(obj => obj.mobile = clientMobile));
                    // console.log(`平台shopAuditsList=${JSON.stringify(shopAuditsList)}`);
                    expect(shopAuditsList, `审核列表查不到该买家用户信息`).to.not.be.undefined;
                    common.isApproximatelyEqualAssert(againUpdateShop.params.jsonParam, shopAuditsList);
                });
                it('平台确认后,买家修改认证信息', async function () {
                    await spAuth.staffLogin();
                    await spCommon.changeAuditRecord({
                        shopId: submitAuditRes.result.data.id,
                        flag: 1,
                        auditRemark: bdId
                    });
                    await spReq.spLogin({ code: clientMobile });
                    const areaJson = await mockJsonParam.getAreaJson();
                    const json = await mockJsonParam.submitAuditJson({ areaJson });
                    json.check = false;
                    let res = await spCommon.updateBuyerShopAuditMessage(json);
                    // console.log(`res=${JSON.stringify(res)}`);
                    expect(res.result).to.includes({ msg: "平台已确认认证信息,请勿重复提交", msgId: "plat_confirm_already" });
                });
            });

        });
        //验证下单数
        //0 小于达标金额 1 等于达标金额 2 大于达标金额
        for (let i = 0; i < 3; i++) {
            describe.skip('买家下单', async function () {
                let bdId, targetAmount, billCustNumBef, invalidCustNumBef, salesRes, billCountBef, billMoneyBef;
                before(async function () {
                    //先审核通过
                    //获取系统参数   地推下单达标金额
                    targetAmount = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_crm_mark_money' });
                    const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '徐世国', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '徐世国'));
                    bdId = crmAnalysisInfo.id;
                    billCustNumBef = crmAnalysisInfo.billCustNum;
                    invalidCustNumBef = crmAnalysisInfo.invalidCustNum;
                    const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.invalidCust.rows.find(val => val.mobile == clientMobile));
                    // console.log(`custAnalysisInfo=${JSON.stringify(custAnalysisInfo)}`);
                    billCountBef = custAnalysisInfo.billCount;
                    billMoneyBef = custAnalysisInfo.billMoney;
                    //开单

                    salesRes = await getSalesBill({ clientMobile, sellerTenantId });
                    // console.log(`salesRes=${JSON.stringify(salesRes)}`);
                    await common.delay(500);
                });

                it('业务统计', async function () {
                    const crmAnalysisInfo = await spbi.crmAnalysisByBd({ bdNameLike: '徐世国', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '徐世国'));
                    common.isApproximatelyEqualAssert({ billCustNum: i == 2 ? billCustNumBef : common.add(billCustNumBef, 1), invalidCustNum: i == 1 ? common.sub(invalidCustNumBef, 1) : invalidCustNumBef }, crmAnalysisInfo);
                });
                it('业务统计-查看客户', async function () {
                    const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.invalidCust.rows.find(val => val.mobile == clientMobile));
                    common.isApproximatelyEqualAssert({ billMoney: common.add(billMoneyBef, salesRes.result.data.totalMoney), billCount: common.add(billCountBef, 1), }, custAnalysisInfo);
                });
                describe('买家退货', async function () {
                    let returnBill;
                    before(async function () {
                        returnBill = await getReturnBill({ clientMobile, targetAmount: targetAmount.val, returnMoney: i == 2 ? -2 : i });
                        // console.log(`returnBill=${JSON.stringify(returnBill.params)}`);
                        await common.delay(500);
                    });
                    it('业务统计', async function () {
                        const crmAnalysisInfo = await spbi.crmAnalysisByBd({ wrap: true, bdNameLike: '徐世国', timeKind: 0 }).then(res => res.result.data.rows.find(val => val.bdName == '徐世国'));
                        let invalidCustNum;
                        if (i == 0) {
                            invalidCustNum = common.add(invalidCustNumBef, 1);
                        } else if (i == 1) {
                            invalidCustNum = common.sub(invalidCustNumBef, 1);
                        } else {
                            invalidCustNum = invalidCustNumBef;
                        };
                        common.isApproximatelyEqualAssert({ billCustNum: i == 1 ? common.add(billCustNumBef, 1) : billCustNumBef, invalidCustNum }, crmAnalysisInfo);
                    });
                    it('业务统计-查看客户', async function () {
                        const custAnalysisInfo = await spbi.custAnalysisByBd({ pageSize: 200, bdId, timeKind: 0 }).then(res => res.result.data.invalidCust.rows.find(val => val.mobile == clientMobile));
                        common.isApproximatelyEqualAssert({ billMoney: i == 0 ? billMoneyBef : billMoneyBef + salesRes.params.jsonParam.orders[0].main.money - returnBill.params.jsonParam.main.totalMoney, billCount: i == 0 ? billCountBef : common.add(billCountBef, 1), }, custAnalysisInfo);
                    });
                });
            });
        };
    });

    describe('异常流', async function () {
        it('必填字段校验', async function () {
            await spReq.spLogin({ code: 12 + common.getRandomNumStr(9) });
            const areaJson = await mockJsonParam.getAreaJson();
            let submitJson = await mockJsonParam.submitAuditJson({ areaJson });
            // console.log(`submitJson=${JSON.stringify(submitJson)}`);
            let mustProperty = {
                '店铺详细地址': { shopAddr: '', },
                '店铺名称': { shopName: '', },
                '店铺地址': { provCode: '', },
                '必须选两种照片上传': { headPic: '', contentPic: '', certPic: '', },
            };
            for (const key of Object.keys(mustProperty)) {
                let value = mustProperty[key];
                if (key == '必须选两种照片上传') {
                    let picture = common.randomSort(Object.keys(value))[0];
                    delete value[picture];
                };
                let json = Object.assign({ check: false }, submitJson, value);

                let submitAuditRes = await spCommon.submitAuditRecord(json);
                // console.log(`submitAuditRes=${JSON.stringify(submitAuditRes.result)}`);
                expect(submitAuditRes.result, `${key}必填项未填，保存成功${JSON.stringify(json)}`).to.includes({ msg: key == '必须选两种照片上传' ? key : `${key}必填;` });
            };
        });
        it('一类邀请码+无认证信息', async function () {
            await spReq.spLogin({ code: 12 + common.getRandomNumStr(9) });
            let json = { inviteCode: 'autotest1', check: false };
            let submitAuditRes = await spCommon.submitAuditRecord(json);
            expect(submitAuditRes.result, `一类邀请码+无认证信息也通过了${JSON.stringify(submitAuditRes)}`).to.includes({ msg: '店铺地址必填;店铺名称必填;店铺详细地址必填;必须选两种照片上传' });

        });
        it('无效的邀请码', async function () {
            await spReq.spLogin({ code: 12 + common.getRandomNumStr(9) });
            const areaJson = await mockJsonParam.getAreaJson();
            let json = await mockJsonParam.submitAuditJson({ areaJson });
            json.inviteCode = common.getRandomNumStr(12);
            json.check = false;
            let submitAuditRes = await spCommon.submitAuditRecord(json);
            // console.log(`submitAuditRes=${JSON.stringify(submitAuditRes)}`);
            expect(submitAuditRes.result, '无效的邀请码也成功了').to.includes({ "msgId": "invite_code_worry" });
        });
    });
});


/**
 * 开单
 * @param {object} param
 * @param {object} param.sellerTenantId 卖家tenantId
 * @param {object} param.mobile 买家手机号
 */
async function getSalesBill({ clientMobile, sellerTenantId }) {
    //买家登录
    await spReq.spClientLogin({ code: clientMobile });
    clientInfo = _.cloneDeep(LOGINDATA);
    // console.log(`开单clientInfo=${JSON.stringify(clientInfo)}`);
    await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
    const searchDres = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });//searchToken: 'shangpinVyFvW'
    // console.log(`searchDres=${JSON.stringify(searchDres)}`);
    const style = searchDres.result.data.dresStyleResultList.shift();
    styleRes = await spReq.getFullForBuyerByUrl({
        detailUrl: style.detailUrl,// mockDetailUrl
    });
    //买家下单
    purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
    payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
    await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
    return purRes;
};

/**
 * 退货流程
 * @param {Object} param
 * @param {Object} param.clientMobile
 * @param {Object} param.returnMoney  0小于达标金额， 1 等于于达标金额  -1 大于达标金额
 *  
 */
async function getReturnBill({ clientMobile, targetAmount, returnMoney }) {
    BASICDATA['2005'] = await spugr.getDictList({ typeId: '2005', flag: 1 })
        .then((res) => res.result.data.rows);
    //仅退款
    BASICDATA['2012'] = await spugr.getDictList({ typeId: '2012', flag: 1 })
        .then((res) => res.result.data.rows);
    returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];
    await spReq.spClientLogin({ code: clientMobile });
    const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });

    //为了避免选择已经发生过退款操作的单子进行退款操作
    purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0);
    expect(purBillInfo, `未找到可退货退款的单子`).not.to.be.undefined;

    let json = mockJsonParam.returnBillJson(purBillInfo);

    Object.assign(json.main, { typeId: 0, totalNum: 0, returnReason: returnReason.codeValue, returnReasonName: returnReason.codeName })
    json.details.map(ele => ele.num = 0);
    if (returnMoney == 0) {
        json.main.totalMoney = common.sub(purBillInfo.bill.totalMoney, targetAmount) + 1;
    } else if (returnMoney == 1) {
        json.main.totalMoney = common.sub(purBillInfo.bill.totalMoney, targetAmount);
    } else { //使总额大于达标金额
        json.main.totalMoney = common.sub(purBillInfo.bill.totalMoney, targetAmount) - 1;
    };

    let returnBill = await spTrade.saveReturnBill(json);
    //卖家审核
    await spReq.spSellerLogin();
    await spTrade.checkReturnBill({
        id: returnBill.result.data.val,
        checkResult: 1, //同意
    });
    return returnBill;
};



/**
 * 可能用的到
 * @param {object} params 
 * @param {object} params.unitId 业务员的买家ids
 * @param {object} params.sp_crm_mark_money 地推下单达标金额 最大订单金额-退货金额的客户
 */
async function getAuditExp(params) {
    //获取业务员的注册用户数
    const spgMysql = await mysql({ dbName: 'spgMysql' });
    //提交
    const [invite] = await spgMysql.query(`SELECT buyer_tenant_id as tenantId from sp_buyer_shop where invite_code='autotest' `);
    // const inviteNum=inviteNum.length;
    //认证通过
    const [audit] = await spgMysql.query(`SELECT buyer_tenant_id as tenantId from sp_buyer_shop where invite_code='autotest' and audit_flag=1`);
    spgMysql.end();
    return { invite, audit, inviteNum: invite.length, auditNum: audit.length };
};