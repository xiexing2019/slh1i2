'use strict';
const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const esSearch = require('../../../reqHandler/sp/biz_server/esSearch');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const { URLSearchParams } = require('url');
const spReq = require('../../help/spReq');

describe('不登录-online', function () {
    this.timeout(30000);
    const errorMsg = '缺少sessionId参数';
    let tenantId, sessionId;
    before(async function () {
        //登出 若登录
        await spReq.spSellerLogin();
        tenantId = LOGINDATA.tenantId;
        sessionId = LOGINDATA.sessionId;
        await spugr.spLogOut();
    });

    it('获取焦点-无会话', async function () {
        const res = await spugr.getSpStateFocus({ sessionId, check: false });
        expect(res.result).to.includes({ msg: '会话已失效，请重新登录' });
    });

    it('查询全局区商品类别树列表', async function () {
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const res = await spdresb.findClassTreeList({ parentId: 0, incDirectSub: true });//, name: '衣服'
        expect(res.result.data.rows.length, `查询列表结果为空`).to.above(0);
        res.result.data.rows.forEach(element => {
            expect(element.parentId).to.equal(0);
        });
    });

    it('查看购物车', async function () {
        const res = await spTrade.showCartList({ check: false });
        expect(res.result).to.includes({ msg: errorMsg });
    });

    it('查询租户商品信息', async function () {
        const res = await spdresb.getFullById({ id: 26, check: false });
        expect(res.result).to.includes({ msg: errorMsg });
    });

    it.skip('添加货品到购物车', async function () {
        let saveJson = mockJsonParam.cartJson();
        saveJson.check = false;
        let saveResult = await spTrade.saveCart(saveJson);
        expect(saveResult.result).to.includes({ msg: '缺少sessionId参数' });
    });

    it.skip('买家查询商品详情', async function () {
        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, "orderByDesc": true, "orderBy": 'marketDate', tenantId });//线上环境tenantid：117  测试  55390
        expect(searchDres.result.data.dresStyleResultList.length, `${JSON.stringify(searchDres)}\n\t查询条目数与返回结果条目数不匹配`).to.not.be.above(searchDres.params.pageSize);
        // console.log(`searchDres=${JSON.stringify(searchDres)}`);
        const strParams = _.last(searchDres.result.data.dresStyleResultList.shift().detailUrl.split('?'));
        const urlParams = new URLSearchParams(strParams);
        const res = await spdresb.getFullForBuyer({
            spuId: urlParams.get('spuId'),
            _cid: urlParams.get('_cid'),
            _tid: urlParams.get('_tid'),
        });
        expect(String(res.result.data.id), `${JSON.stringify(res)}\n\t查询款号与结果不匹配`).to.equal(String(res.params.spuId));
    });

    it('不登录查看市场列表', async function () {
        const marketList = await spCommon.getMarket();
        expect(marketList.result.data.rows.length, '查询列表数据错误').to.not.equal(0);
    });

    it('导出所有列表', async function () {
        const res = await spConfig.exportDistrictRegion();
        expect(res.result.data.rows.length, '查询列表数据错误').to.not.equal(0);
    });

});