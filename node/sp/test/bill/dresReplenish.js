const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');

/**
 * 一键补货
 * 库存>订单数量时 返回采购单sku数量
 * 库存<订单数量时 返回库存
 * 库存=0时 返回0
 * 非正常上线商品 则返回错误提示
 * 
 * 现在没有限制单据状态 都可返回结果
 */
describe('补货-online', async function () {
    this.timeout(30000);
    let sellerInfo, clientInfo, dresRes, purRes;

    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        // 新增商品 库存20
        const styleRes = await spReq.saveDresFull(dresJson => dresJson.skus.forEach(sku => sku.num = 20));
        // console.log(`styleRes=${JSON.stringify(styleRes.result)}`);

        await spReq.spClientLogin();
        clientInfo = _.cloneDeep(LOGINDATA);
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        await common.delay(1000);
        const searchDres = await spdresb.searchDres({ queryType: 0, needWait: 1, pageSize: 10, pageNo: 1, keyWords: { id: [styleRes.result.data.spuId] }, tenantId: sellerInfo.tenantId });

        const style = searchDres.result.data.dresStyleResultList.find(val => val.id == styleRes.result.data.spuId);

        dresRes = await spReq.getFullForBuyerByUrl({ detailUrl: style.detailUrl });

        purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresRes }));
        // console.log(`purRes=${JSON.stringify(purRes)}`);
    });

    it('一键补货-库存充足', async function () {
        const res = await spdresb.replenishForBuyer({ _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, spuId: dresRes.result.data.id, billNo: purRes.result.data.rows[0].billNo });
        // console.log(`\nres=${JSON.stringify(res)}`);
        const dresExp = await getReplenishExp({ spuId: dresRes.result.data.id, sellerInfo, purDetails: purRes.params.jsonParam.orders[0].details });
        // console.log(`\ndresExp=${JSON.stringify(dresExp)}`);
        common.isApproximatelyArrayAssert(dresExp, res.result.data.rows);
    });
    it('一键补货-库存不足', async function () {
        let json = mockJsonParam.purJson({ styleInfo: dresRes });
        json.orders[0].main.totalNum = 0;
        json.orders[0].main.money = 0;
        json.orders[0].details.forEach(ele => {
            ele.num = 10;
            ele.money = common.mul(ele.num, ele.price);
            json.orders[0].main.totalNum += ele.num;
            json.orders[0].main.money += ele.money;
        });
        json.orders[0].main.totalMoney = json.orders[0].main.money - json.orders[0].main.shopCoupsMoney + json.orders[0].main.shipFeeMoney;
        let purcharseRes = await spTrade.savePurBill(json);
        let replenishForBuyerRes = await spdresb.replenishForBuyer({ _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, spuId: dresRes.result.data.id, billNo: purcharseRes.result.data.rows[0].billNo });
        const dresExp = await getReplenishExp({ spuId: dresRes.result.data.id, sellerInfo, purDetails: purcharseRes.params.jsonParam.orders[0].details });
        // console.log(`\ndresExp=${JSON.stringify(dresExp)}`);
        common.isApproximatelyArrayAssert(dresExp, replenishForBuyerRes.result.data.rows);

    });
    it('下线商品后-一键补货', async function () {
        await spReq.spSellerLogin();
        //商品下架
        await spdresb.offMarket({
            ids: [dresRes.result.data.id]
        });
        await spReq.spClientLogin();
        const replenishForBuyerRes = await spdresb.replenishForBuyer({ _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, spuId: dresRes.result.data.id, billNo: purRes.result.data.rows[0].billNo, check: false });
        expect(replenishForBuyerRes.result, `已下架的商品仍然可以一键补货`).to.include({
            // msg: '商品未上架或商品已被下架',
            msgId: 'dres_spu_not_ongroup'
        });
    });
});

async function getDresRes({ sellerTenantId, spuId }) {
    const esRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tenantId: sellerTenantId, keyWords: { id: [spuId] } });
    const dresSpu = esRes.result.data.dresStyleResultList.find(val => val.id == spuId);
    return spReq.getFullForBuyerByUrl({ detailUrl: dresSpu.detailUrl });
};

async function getReplenishExp({ spuId, sellerInfo, purDetails }) {
    let exp = {};
    //获取款号库存
    let styleRes = await getDresRes({ sellerTenantId: sellerInfo.tenantId, spuId });
    purDetails.forEach(ele => {
        let skuInfo = styleRes.result.data.skus.find(obj => obj.id == ele.skuId);
        exp[ele.skuId] = { skuId: ele.skuId, skuNum: skuInfo.num > ele.num ? ele.num : skuInfo.num, };
    });
    return Object.values(exp);
};