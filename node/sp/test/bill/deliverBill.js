const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const configParam = require('../../help/configParamManager');

// 设置0也要1小时后定时任务才能跑到 yyj 
// 自动化暂不校验
describe.skip('卖家发货', function () {
    this.timeout(30000);
    let sellerTenantId;
    before(async function () {
        //卖家登录
        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;

        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
    });

    describe('卖家发货提醒', async function () {
        let purRes;
        let paramInfo;
        before(async function () {
            await spAuth.staffLogin();
            paramInfo = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_remind_deliver_first' });
            await paramInfo.updateParam({ val: 0 });
            // sp_remind_deliver_second

            await spReq.spClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);
            //获取用户默认收货地址
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });
            const style = searchDres.result.data.dresStyleResultList.shift();
            const styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,
            });

            //开单
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));

            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.skip();
            };

            //买家支付
            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
        });
        // sp_remind_deliver_first
        describe('第一次发货提醒', async function () {

            before(async function () {

            });
            after(async function () {
                if (paramInfo) {
                    await spAuth.staffLogin();
                    await paramInfo.updateParam({ val: 24 });
                }
            });
            it('', async function () {
                await spReq.spSellerLogin();
                const msgList = await spMdm.pullMessagesList({});
                console.log(`msgList=${JSON.stringify(msgList)}`);

            });
        });

    });

});