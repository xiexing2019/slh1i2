const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const common = require('../../../lib/common');
const spExp = require('../../help/getExp');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const caps = require('../../../data/caps');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAccount = require('../../data/spAccount');
const format = require('../../../data/format');
const dresManage = require('../../help/dresManage');

// 订单联动商品库存变动
// http://zentao.hzdlsoft.com:6082/zentao/doc-view-99.html
// http://zentao.hzdlsoft.com:6082/zentao/task-view-1894.html
// 
describe('开单-onlineMain', function () {
    this.timeout(TESTCASE.timeout);
    const dres = dresManage.setupDres();
    let sellerTenantId, clusterCode, clientInfo, purRes, clientData, addData, sellerInfo;
    before(async function () {
        // 等待店铺上线 重新上架商品
        // if (caps.name == 'sp_online') { await common.delay(10000) };
        // 卖家登录
        await spReq.spSellerLogin();
        // 新增商品 保证数据正常
        await spReq.saveDresFull();
        sellerTenantId = LOGINDATA.tenantId;
        clusterCode = LOGINDATA.clusterCode;//租户集群id
        sellerInfo = LOGINDATA;
    });

    describe('买家开单', function () {
        before(async function () {
            // 买家登录
            await spReq.spClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);
            // console.log(clientInfo);

            //获取买家数据统计
            clientData = await spTrade.getPurStat().then(res => res.result.data);
            //查询商品信息
            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 0 })
                .then(res => res.result.data.dresStyleResultList);
            if (dresList.length == 0) {
                console.warn(`es搜索未找到商品`);
                this.parent ? this.parent.skip() : this.skip();
            };
            dres.setByEs(dresList[0]);
            const dresDetail = await dres.getDetailForBuyer();

            await spReq.spSellerLogin();
            const dresFull = await dres.getFullById().then(res => res.result.data);
            dres.setByDetail(dresFull);

            await spReq.spClientLogin();
            //获取用户默认收货地址
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //开单
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresDetail }));
            console.log(`purRes=${JSON.stringify(purRes)}`);
            // 下单 增加锁定库存
            dres.changeStockByBill({ purDetails: purRes.params.jsonParam.orders[0].details, payFlag: 0 });
            // 买家数据统计 增量
            addData = { totalPurTimes: 1, totalPurNum: purRes.params.jsonParam.orders[0].main.totalNum };
            // await common.delay(1000);
            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.parent ? this.parent.skip() : this.skip();
            };

            await spReq.spSellerLogin();
            await common.delay(500);
            // console.log(dres);

        });
        it('卖家查询消息列表', async function () {
            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: '71', text: '您收到了新的订单', title: '新订单提醒' });
        });
        it('卖家店铺日志流查询', async function () {
            const res = await spUp.getShopLogList({ flowType: 3 });
            const exp = spExp.shopLogListExp({ createdDate: purRes.opTime, flowType: 3, clientInfo });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('购买了货品'); //ipad日志都是货品，没有商品
        });
        it('卖家查询销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows);
            const salesBillInfo = salesList.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.bill.payFlag).to.equal(0);
        });
        it('卖家查询销售单详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.bill.payFlag).to.equal(0);
        });
        it('卖家查询商品详情', async function () {
            this.retries(2);
            await common.delay(500);
            const dresFull = await dres.getFullById().then(res => res.result.data);
            common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['ver', 'channelIds', 'actHistoryNum']);
        });
        //付款之后拿货次数、金额、数量才会计算的，下订单后付款前应该不变的
        it('未支付前查看买家数据统计', async function () {
            await spReq.spClientLogin();
            const purStat = await spTrade.getPurStat().then(res => res.result.data);
            common.isApproximatelyEqualAssert(clientData, purStat, ['viewSpuNum', 'avgDeliverElapsed']);
        });
        it('买家查询采购单列表', async function () {

        });
        it('买家查询采购单详情', async function () {

        });
        it('平台端查看订单详情', async function () {
            //管理员登录
            await spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerTenantId, _cid: clusterCode });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 1, payFlag: 0, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });

    // favor_money
    describe.skip('修改订单明细金额', async function () {
        before(async function () {
            await spReq.spSellerLogin();

            const selesBillDetail = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);

            const json = {
                id: selesBillDetail.bill.id,
                shipFeeMoney: common.getRandomNum(5, 10),
                details: selesBillDetail.skus.map(sku => { return { id: sku.id, newMoney: common.sub(common.mul(sku.skuNum, sku.skuPrice), common.getRandomNum(10, 20)) } })
            };
            const res = await spTrade.changeBillDetailMoney(json);
            console.log(`res=${JSON.stringify(res)}`);
            // purRes.params.jsonParam.orders[0].main.money = res.result.data.totalMoney;
            const newTotalMoney = json.details.reduce((preVal, curVal) => {
                return { newMoney: common.add(preVal.newMoney, curVal.newMoney) };
            }, { newMoney: 0 });

            purRes.params.jsonParam.orders[0].main.shipFeeMoney = json.shipFeeMoney;
            purRes.params.jsonParam.orders[0].main.favorMoney = common.sub(purRes.params.jsonParam.orders[0].main.totalMoney, newTotalMoney.newMoney);
        });
        it('卖家查询销售单详情', async function () {
            const selesBillDetail = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            console.log(`\nselesBillDetail=${JSON.stringify(selesBillDetail)}`);
        });
        it('查询改价订单详情', async function () {
            const res = await spTrade.getChangeMoneyBill({ id: purRes.result.data.rows[0].salesBillId });
            // console.log(`\nres=${JSON.stringify(res)}`);
        });
        it('买家查询采购单详情', async function () {
            await spReq.spClientLogin();
            const purBillDetail = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            // console.log(`\npurBillDetail=${JSON.stringify(purBillDetail)}`);
        });
    });

    describe('买家支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await spReq.spClientLogin();
            const purBillDetail = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            addData.totalPurMoney = purBillDetail.bill.totalMoney;
            addData.monthTotalPurMoney = purBillDetail.bill.totalMoney;

            payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purBillDetail.bill.totalMoney });
            // console.log(payRes);

            const res = await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            console.log(`\n支付回调:${JSON.stringify(res)}`);
            await common.delay(500);

            // 支付后 减少库存和锁定的库存
            dres.changeStockByBill({ purDetails: purRes.params.jsonParam.orders[0].details, payFlag: 1 });
        });
        after(async function () {
            await spReq.spSellerLogin();
        })
        it('支付查询', async function () {
            const res = await spTrade.checkQueryPayResult({ id: payRes.result.data.payDetailId });
            expect(res.result.data).to.includes({ val: true });
        });
        it('买家数据统计验证', async function () {
            let clientDataLast = await spTrade.getPurStat().then(res => res.result.data);
            let exp = common.addObject(clientData, addData);
            common.isApproximatelyEqualAssert(exp, clientDataLast, ['avgDeliverElapsed', 'viewSpuNum']);
        });
        it('订单是否可直接退货', async function () {
            await spReq.spClientLogin();
            // console.log(`purRes.result.data.rows[0].billId=${JSON.stringify(purRes.result.data.rows[0].billId)}`);
            let billIsReturn = await spTrade.getReturnDirect({ id: purRes.result.data.rows[0].billId });
            console.log(`billIsReturn=${JSON.stringify(billIsReturn)}`);
            expect(billIsReturn.result.data.val, `x小时内未发货订单是否可直接退货不为flase:\n\t${JSON.stringify(billIsReturn)}`).to.be.false;
        })
        it('买家查询采购单列表', async function () {

        });
        it('买家查询采购单详情', async function () {

        });
        it('卖家消息中心', async function () {
            //卖家登录
            await spReq.spSellerLogin();

            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 72, text: '买家已经付款', title: '买家已付款' });
        });
        it('卖家店铺日志流查询', async function () {
            const res = await spUp.getShopLogList({ flowType: 4 });
            const exp = spExp.shopLogListExp({ createdDate: purRes.opTime, flowType: 4, clientInfo });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('购买了货品');
        });
        it('卖家查询销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows);//purRes.result.data.rows[0].billId
            const salesBillInfo = salesList.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.bill).to.includes({
                payFlag: 1,//已付款
                frontFlag: 2,//未发货
            });
        });
        it('卖家查询销售单详情', async function () {
            salesBillInfo = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.bill).to.includes({
                payFlag: 1,//已付款
                frontFlag: 2,//未发货
            });
        });
        // 2.1.0 spu的salesNum没有更新 暂时只校验sku的salesNum 2019-04-12
        it('卖家查询商品详情', async function () {
            this.retries(2);
            await common.delay(500);
            const dresFull = await dres.getFullById().then(res => res.result.data);
            common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['ver', 'channelIds', 'actHistoryNum']);
        });
        it('买家提醒发货', async function () {
            await spReq.spClientLogin();
            await spTrade.remindBillDeliver({ billId: purRes.result.data.rows[0].billId });
        });
        it('卖家消息中心-校验买家提醒', async function () {
            //卖家登录
            await spReq.spSellerLogin();
            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 78, text: '请尽快发货', title: '买家提醒' });
        });
        it('买家评论', async function () {
            await spReq.spClientLogin();
            let commentRes = await spTrade.addComment({
                id: purRes.result.data.rows[0].billId,
                rem: `单据id=${purRes.result.data.rows[0].billId}的单子我已收到货！`,
                val: common.getRandomNum(1, 5),
                logisScore: common.getRandomNum(1, 5),
                sendFlowerIs: common.getRandomNum(0, 1),
                check: false
            });
            expect(commentRes.result, '订单未收货，就能评论了').to.includes({ "msgId": "bill_not_confirmed" });
        });
        it('买家查看已购买过的店铺', async function () {
            await spReq.spClientLogin();
            recentBuyShopList = await spTrade.getRecentBuyShopList({ pageSize: 10, pageNo: 1 }).then(res => res.result.data.rows);
            const recentShop = recentBuyShopList.find(element => element.sellerId == sellerTenantId);
            if (recentShop == undefined) {
                throw new Error(`买家在${sellerTenantId}购买过商品，但是列表中没有这个店铺`);
            };

            const shopList = await spugr.findShopBySearchToken({ searchToken: sellerInfo.shopName });
            const shopInfo = shopList.result.data.rows.find(res => res.id == sellerInfo.tenantId);
            const exp = format.dataFormat(shopInfo, 'provCode;cityCode;shopName=name;detailAddr=shopAddr;masterClassId;marketId;marketName;sellerId=tenantId;logoPic');
            exp.sellerId = sellerInfo.tenantId;
            exp.maxProTime = salesBillInfo.bill.proTime;
            exp.linkFlag = 1;//1非免租店铺，0免租店铺
            common.isApproximatelyEqualAssert(exp, recentShop, ['detailAddr']);
            // 返回字段隐藏了档口号，替换为空串 
            expect(recentShop.detailAddr.trim()).to.equal(exp.detailAddr);

            const expMedals = JSON.parse(shopInfo.medals);
            const actualMedals = JSON.parse(recentShop.medals);
            common.isApproximatelyEqualAssert(expMedals, actualMedals);
        });
        //应该按照 maxProTime 倒序排序
        it('买家购买过的店铺排序', async function () {
            for (let i = 0; i < recentBuyShopList.length - 1; i++) {
                let flag = recentBuyShopList[i].maxProTime >= recentBuyShopList[i + 1].maxProTime;
                expect(flag, `当前店铺：${recentBuyShopList[i].shopName},time:${recentBuyShopList[i].maxProTime}，下一个:${recentBuyShopList[i + 1].shopName},time:${recentBuyShopList[i + 1].maxProTime}`).to.be.true;
            };
        });
        it('平台端查看订单详情', async function () {
            //管理员登录
            await spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerTenantId, _cid: clusterCode });
            // console.log(`spPurInfo=${JSON.stringify(spPurInfo)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 2, payFlag: 1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });

    describe('卖家发货', function () {
        before(async function () {
            await spReq.spSellerLogin();
            // 查询销售单详情
            const salesBillInfo = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            const details = salesBillInfo.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });
            // 查询物流商
            const logisList = await spconfb.findLogisList1({ cap: 1 });
            //发货
            await spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[0].id,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: clientInfo.tenantId,
                    shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            });
        });
        it('卖家查询销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows);//purRes.result.data.rows[0].billId
            const salesBillInfo = salesList.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesBillInfo.bill.frontFlag).to.equal(3);
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.bill.frontFlag).to.equal(3);
        });
        it('卖家查询商品详情-库存', async function () {
            const dresFull = await dres.getFullById().then(res => res.result.data);
            common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
            // common.isApproximatelyEqualAssert({ stockNum: common.sub(styleInfoBefore.spu.stockNum, purRes.params.jsonParam.orders[0].main.totalNum) }, styleInfoAfter.result.data.spu);
        });
        it('买家查询采购单列表', async function () {

        });
        it('买家查询采购单详情', async function () {

        });
        it('买家消息中心', async function () {
            await spReq.spClientLogin();
            // console.log('\n买家消息中心\n');
            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 90, text: `${purRes.params.jsonParam.orders[0].details[0].spuTitle}`, title: '订单已发货' });
        });
        it('买家评论', async function () {
            await spReq.spClientLogin();
            let commentRes = await spTrade.addComment({
                id: purRes.result.data.rows[0].billId,
                rem: `单据id=${purRes.result.data.rows[0].billId}的单子我已收到货！`,
                val: common.getRandomNum(1, 5),
                logisScore: common.getRandomNum(1, 5),
                sendFlowerIs: common.getRandomNum(0, 1),
                check: false
            });
            expect(commentRes.result, '订单未收货，就能评论了').to.includes({ "msgId": "bill_not_confirmed" });
        });
        it('平台端查看订单详情', async function () {
            //管理员登录
            await spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerTenantId, _cid: clusterCode });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({ flag: 6, frontFlag: 3, payFlag: 1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);

        });
    });

    describe('买家收货', function () {
        before(async function () {
            await spReq.spClientLogin();
            //买家确认收货
            await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
            // console.log(`确认收货：=${JSON.stringify(res)}`);
        });
        it('确认收货后-延迟收货', async function () {
            const res = await spTrade.extendedReturn({ id: purRes.result.data.rows[0].billId, check: false });
            expect(res.result).to.includes({ msgId: 'spbillflag_after_confirm' });
        });
        it('买家查询采购单列表', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });

            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag).to.equal(4);
        });
        it('买家查询采购单详情', async function () {
            const purInfo = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam, purInfo.result.data);
            expect(purInfo.result.data.bill.frontFlag).to.equal(4);
        })
        it('卖家消息中心', async function () {
            //卖家登录
            await spReq.spSellerLogin();
            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 77, text: '买家已经确认收货', title: '收货提醒' });
        });
        it('卖家查询销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            const salesBillInfo = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesBillInfo.bill.frontFlag).to.equal(4);
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId });//
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(4);
        });
        it('平台端查看订单详情', async function () {
            //管理员登录
            await spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerTenantId, _cid: clusterCode });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({ flag: 8, frontFlag: 4, payFlag: 1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
        describe('买家评论', async function () {
            let commentRes;
            before('买家评论', async function () {
                await spReq.spClientLogin();
                commentRes = await spTrade.addComment({
                    id: purRes.result.data.rows[0].billId,
                    rem: `单据id=${purRes.result.data.rows[0].billId}的单子我已收到货！`,
                    val: common.getRandomNum(3, 5),
                    logisScore: common.getRandomNum(3, 5),
                    sendFlowerIs: common.getRandomNum(0, 1)
                });
            });

            it('买家查看评论', async function () {
                let commentInfo = await spTrade.getCommentById({ billId: purRes.result.data.rows[0].billId });
                let exp = _.cloneDeep(commentRes.params.jsonParam);
                exp.updatedDate = exp.createdDate = commentInfo.opTime;
                exp.createdBy = exp.updatedBy = LOGINDATA.userId;

                common.isApproximatelyEqualAssert(exp, commentInfo.result.data, ['id']);
            });

            it.skip('管理员查看', async function () {
                this.retries(2);
                await spAuth.staffLogin();
                await common.delay(2000);   //这里等1.5s 因为这里是走MQ的，所以存在延迟，所以不能保证消息的及时性。
                let res = await spTrade.searchSalesBills({ pageSize: 20, pageNo: 1, sellerSearchToken: spAccount.seller.shopName, keyWords: { flag: [9], billNo: [purRes.result.data.rows[0].billNo] } });   //flag 9 已评价
                // console.log(`res=${JSON.stringify(res)}`);
                let info = res.result.data.rows.find(element => element.id == purRes.result.data.rows[0].billNo);
                if (info == undefined) {
                    throw new Error(`管理员查看找不到这个评论的单子,billNo:${purRes.result.data.rows[0].billNo}`)
                } else {
                    let exp = _.cloneDeep(commentRes.params.jsonParam);
                    exp.billNo = exp.id = purRes.result.data.rows[0].billNo;
                    exp.flag = 9;
                    exp.payFlag = 1;
                    Object.assign(exp, format.dataFormat(clientInfo, 'buyerId=tenantId;buyerName=nickName;buyerUnitId=unitId;clusterCode'))
                    Object.assign(exp, format.dataFormat(sellerInfo, 'sellerName=shopName;sellerUnitId=unitId;sellerId=tenantId'))

                    Object.assign(exp, format.dataFormat(purRes.params.jsonParam.orders[0].details[0], 'spuId;spuCode;spuDocId;spuTitle;skuNum=num;spec1Name;spec2Name;spec3Name'));
                    exp.totalMoney = purRes.params.jsonParam.orders[0].main.totalMoney;
                    exp.money = purRes.params.jsonParam.orders[0].main.money;
                    common.isApproximatelyEqualAssert(exp, info);
                    common.isFieldsEqualAssert(exp, info, 'rem;logisScore;val');
                };
            });
        });
    });

    describe.skip('卖家修改订单金额和设置卖家备注', function () {
        let purchaseRes, sellerRem, salesListResult, styleBefore, styleBeforePay, totalMoney;
        const favorMoney = 50, shipFeeMoney = 100;
        before(async () => {
            //买家登录
            await spReq.spClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);
            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });//searchToken: 'shangpinVyFvW'
            // console.log(`searchDres=${JSON.stringify(searchDres)}`);
            const style = searchDres.result.data.dresStyleResultList.shift();
            let styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,// mockDetailUrl
            });
            //卖家登录，查商品
            await spReq.spSellerLogin();
            styleBefore = await spdresb.getFullById({ id: mockJsonParam.purJson({ styleInfo: styleRes }).orders[0].details[0].spuId });
            // console.log(`styleBefore=${JSON.stringify(styleBefore)}`);

            //买家登录
            await spReq.spClientLogin();
            //获取用户默认收货地址
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            // mockDetailUrl = `sp_domain_url/spb/api.do?apiKey=ec-spdresb-dresSpu-getFullForBuyer&_cid=cs3s1&_tid=14&spuId=38&buyerId=${LOGINDATA.tenantId}`;
            //买家下单
            purchaseRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
            // console.log(`purchaseRes=${JSON.stringify(purchaseRes)}`);
            //卖家登录
            await spReq.spSellerLogin();
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListResult = salesList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
            //卖家修改订单金额
            totalMoney = common.sub(common.add(purchaseRes.params.jsonParam.orders[0].main.totalMoney, shipFeeMoney), common.add(purchaseRes.params.jsonParam.orders[0].main.shopCoupsMoney, favorMoney));
            // console.log(`totalMoney=${JSON.stringify(totalMoney)}`);
            await spTrade.changeBillMoney({
                id: salesListResult.bill.id,
                shipFeeMoney,
                favorMoney,
                totalMoney: totalMoney,
                ver: 0//乐观锁，手动改，后续再改
            });
            sellerRem = '卖家备注' + common.getRandomStr(8);
            const res = await spTrade.saveSellerRem({ id: salesListResult.bill.id, sellerRem });
            // console.log(`设置卖家备注=${JSON.stringify(res)}`);
            //买家支付前，卖家货品详情
            styleBeforePay = await spdresb.getFullById({ id: purchaseRes.params.jsonParam.orders[0].details[0].spuId });

            //买家支付
            //  await spReq.spClientLogin();
            // let payRes = await spTrade.createPay({ orderIds: [purchaseRes.result.data.rows[0].billId], payMoney: totalMoney });
            // await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: totalMoney });

        });
        it('卖家销售单列表', async function () {
            //卖家登录
            await spReq.spSellerLogin();
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListResult = salesList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
            //  console.log(`卖家销售单列表=${JSON.stringify(salesListResult)}`);
            common.isApproximatelyEqualAssert({ totalMoney, sellerRem }, salesListResult.bill);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListResult.bill.id });//
            //  console.log(`卖家销售单详情=${JSON.stringify(salesBillInfo)}`);
            common.isApproximatelyEqualAssert({ totalMoney, sellerRem }, salesBillInfo.result.data.bill);
        });
        it('买家支付前，卖家商品详情', async function () {
            //这里没有支付，skus 里面的salesNum就已经统计了，bug功能组已经报了，等开发修复
            common.isApproximatelyEqualAssert(styleBefore.result.data, styleBeforePay.result.data, ['readNum', 'lockNum']);
        });
        it('买家登录消息提醒', async function () {
            //买家登录
            await spReq.spClientLogin();
            await messageCenter({ billNo: purchaseRes.result.data.rows[0].billNo, tagOneIn: '94', text: `${purchaseRes.params.jsonParam.orders[0].details[0].spuTitle}`, title: '订单金额已变更' });
        });
        it('买家采购单金额验证', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
            //  console.log(`买家采购单=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.totalMoney).to.equal(totalMoney);
        });
        it('卖家修改金额后平台端查看订单详情', async function () {
            //管理员登录
            await spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purchaseRes.result.data.rows[0].billNo, _tid: sellerTenantId, _cid: clusterCode });
            common.isApproximatelyEqualAssert(purchaseRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill, ['totalMoney', 'shipFeeMoney', 'favorMoney']);
            common.isApproximatelyEqualAssert(purchaseRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 1, payFlag: 0, totalMoney, shipFeeMoney, favorMoney, sellerRem, billNo: purchaseRes.result.data.rows[0].billNo, purBillId: purchaseRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);

        });
        describe('买家支付', function () {
            let payRes;
            before(async () => {
                //买家支付
                await spReq.spClientLogin();
                payRes = await spTrade.createPay({ orderIds: [purchaseRes.result.data.rows[0].billId], payMoney: totalMoney });
                await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: totalMoney });
            });
            it('支付查询', async function () {
                const res = await spTrade.checkQueryPayResult({ id: payRes.result.data.payDetailId });
                expect(res.result.data).to.includes({ val: true });
            });
            it('卖家消息中心', async function () {
                //卖家登录
                await spReq.spSellerLogin();

                await messageCenter({ billNo: purchaseRes.result.data.rows[0].billNo, tagOneIn: 72, text: '买家已经付款', title: '买家已付款' });
            });
            it('卖家销售单列表', async function () {
                const salesList = await spTrade.salesFindBills({ pageSize: 20, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
                salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
                // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.payFlag).to.equal(1);//已付款
                expect(salesListRes.bill.frontFlag).to.equal(2);//未发货
            });
            it('卖家商品详情', async function () {
                await common.delay(200);
                let styleAfter = await spdresb.getFullById({ id: purchaseRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                let exp = common.addObject({
                    stockNum: -purchaseRes.params.jsonParam.orders[0].main.totalNum,
                    // salesNum: purchaseRes.params.jsonParam.orders[0].main.totalNum,
                    // salesMoney: purchaseRes.params.jsonParam.orders[0].main.totalMoney,//common.sub(totalMoney, 100),//减去运费
                }, styleBefore.result.data.spu);
                common.isApproximatelyEqualAssert(exp, styleAfter.spu, ['sessionId', 'readNum', 'salesNum', 'salesMoney', 'ver']);
            })
            it('卖家销售单详情', async function () {
                const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
                expect(salesBillInfo.result.data.bill.frontFlag).to.equal(2);
            });
            it('提醒发货', async function () {
                await spReq.spClientLogin();
                await spTrade.remindBillDeliver({ billId: purchaseRes.result.data.rows[0].billId });
            });
            it('卖家消息中心', async function () {
                //卖家登录
                await spReq.spSellerLogin();
                await messageCenter({ billNo: purchaseRes.result.data.rows[0].billNo, tagOneIn: 78, text: '请尽快发货', title: '买家提醒' });
            });
        });
        describe('卖家发货', function () {
            let qfRes;
            before('', async () => {
                await spReq.spSellerLogin();
                //列表skus只取单据sku中的一条数据。。
                const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
                // console.log(`qlRes=${JSON.stringify(qlRes)}`);
                salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
                //查询单据详情
                qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
                //发货
                const details = qfRes.result.data.skus.map((sku) => {
                    return { salesDetailId: sku.id, num: sku.skuNum };
                });
                const logisList = await spconfb.findLogisList1({ cap: 1 });
                const res = await spTrade.deliverSalesBill({
                    main: {
                        logisCompId: logisList.result.data.rows[0].id,
                        waybillNo: common.getRandomNumStr(12),
                        buyerId: clientInfo.tenantId,
                        shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                        hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                    },
                    details: details
                });
                // console.log(`卖家发货:${JSON.stringify(res)}`);
            });
            it('卖家销售单列表', async function () {
                const salesList = await spTrade.salesFindBills({ pageSize: 20, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
                salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
                // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.frontFlag).to.equal(3);
            });
            it('卖家销售单详情', async function () {
                const salesBillInfo = await spTrade.salesFindBillFull({ id: purchaseRes.result.data.rows[0].salesBillId });//
                // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
                expect(salesBillInfo.result.data.bill.frontFlag).to.equal(3);
            });
            it('卖家商品详情-库存', async function () {
                let styleInfoAfter = await spdresb.getFullById({ id: purchaseRes.params.jsonParam.orders[0].details[0].spuId });
                // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                common.isApproximatelyEqualAssert({ stockNum: common.sub(styleBefore.result.data.spu.stockNum, purchaseRes.params.jsonParam.orders[0].main.totalNum) }, styleInfoAfter.result.data.spu);
            });
            it('买家消息中心', async function () {
                await spReq.spClientLogin();
                // console.log('\n买家消息中心\n');
                await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 90, text: `${purchaseRes.params.jsonParam.orders[0].details[0].spuTitle}`, title: '订单已发货' });
            });
        });
        describe('买家收货', function () {
            before(async () => {
                await spReq.spClientLogin();
                //买家确认收货
                const res = await spTrade.confirmReceipt({ purBillIds: [purchaseRes.result.data.rows[0].billId] });
                // console.log(`确认收货：=${JSON.stringify(res)}`);
            });
            it('买家采购单列表', async function () {
                const purBillList = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });
                // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
                let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
                // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
                expect(purBillInfo.bill.frontFlag).to.equal(4);
            });
            it('卖家消息中心', async function () {
                //卖家登录
                await spReq.spSellerLogin();
                await messageCenter({ billNo: purchaseRes.result.data.rows[0].billNo, tagOneIn: 77, text: '买家已经确认收货', title: '收货提醒' });
            });
            it('卖家销售单列表', async function () {
                const salesList = await spTrade.salesFindBills({ pageSize: 20, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
                salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purchaseRes.result.data.rows[0].billNo);
                // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.frontFlag).to.equal(4);
            });
            it('卖家销售单详情', async function () {
                const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
                expect(salesBillInfo.result.data.bill.frontFlag).to.equal(4);
            });
        });

    });
});


/**
 * 消息中心验证(断言)
 * @description 暂时去掉消息中心的text字段验证(提示信息会变动) lxx 18-09-04
 * @param {object} params
 */
async function messageCenter({ billNo, tagOneIn, title }) {
    //获取消息列表-未读
    // pageSize需要限制条目数，数量太大会拉爆服务器
    let messageList = await spMdm.pullMessagesList({ unread: 1, tagOneIn, pageNo: 1, pageSize: 20 });
    // console.log(`messageList=${JSON.stringify(messageList)}`);
    let msgInfo = messageList.result.data.dataList.find(message => message.body.billNo = billNo);
    // console.log(`msgInfo=${JSON.stringify(msgInfo)}`);
    expect(msgInfo.body, `消息列表中未读的消息没有单据:${JSON.stringify(billNo)}的消息`).to.includes({ title });//msgJson[tagOneIn] text,
    //获取未读消息数
    let unReadCount = await spMdm.getPullUnreadCount({ tagOneIn });
    // console.log(`unReadCount.result.data.val=${JSON.stringify(unReadCount.result.data.val)}`);
    expect(unReadCount.result.data.val, `类型:${JSON.stringify(tagOneIn)}未读消息数和消息列表中该类型的未读的消息总数不一致`).to.equal(messageList.result.data.count);
    //更新消息为已读
    await spMdm.updateReadStatus({ msgIds: msgInfo.id });
    let unReadCount2 = await spMdm.getPullUnreadCount({ tagOneIn });
    expect(common.sub(unReadCount.result.data.val, 1), `消息已读后，未读消息数没有减1`).to.equal(unReadCount2.result.data.val);
    //获取消息列表
    messageList = await spMdm.pullMessagesList({ tagOneIn });
    msgInfo = messageList.result.data.dataList.find((message) => message.body.billNo == billNo);
    //unread 未读状态 0表示已读，1表示未读
    expect(msgInfo.unread, `消息列表中单据:${JSON.stringify(billNo)}已读,但是未读状态不是0`).to.equal(0);
};

async function getDetailStyleInv(details) {
    let invResult = {}, spuResult = {};
    for (let index = 0; index < details.length; index++) {
        if (spuResult.hasOwnProperty(`${details[index].spuId}`)) { continue; };
        await spdresb.getFullById({ id: details[index].spuId }).then(res => {
            spuResult[details[index].spuId] = res.result.data.spu;
            res.result.data.skus.forEach(data => invResult[data.id] = data.num)
        });

    };
    return { invResult, spuResult };
}


