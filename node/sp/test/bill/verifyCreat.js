
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const dresManage = require('../../help/dresManage');

describe('创建订单校验', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo = {};

    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin();
    });

    describe('正常商品', async function () {
        let dres = dresManage.setupDres();
        before(async function () {
            const dresList = await spdresb.searchDres({ tenantId: sellerInfo.tenantId })
                .then(res => res.result.data.dresStyleResultList);
            await dres.setByEs(dresList[0]).searchAndSetByDetail();
            // console.log(dres);
        });
        it('创建订单综合校验', async function () {
            await dres.verifyCreateOrder([{ currentOrderNum: 10, skuId: [...dres.skus.keys()][0] }]);
        });
    });

    describe('', async function () {

    });



});