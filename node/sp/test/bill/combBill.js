const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const common = require('../../../lib/common');
const spExp = require('../../help/getExp');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const caps = require('../../../data/caps');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAccount = require('../../data/spAccount');
const format = require('../../../data/format');
const billManage = require('../../help/billManage');
const cartManager = require('../../help/cartHelp');
const combine = require('../../../reqHandler/sp/wms/combine');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const wmsStorage = require('../../../reqHandler/sp/wms/storage');

// 
describe('合包', async function () {
    this.timeout(30000);

    describe('合包主流程', async function () {
        let sellerA, sellerB, myCart;
        const combBill = billManage.combBillManage();
        before(async function () {
            await spReq.spSellerLogin();
            sellerA = _.cloneDeep(LOGINDATA);

            await spReq.spSellerLogin({ shopName: '中洲店' });
            sellerB = _.cloneDeep(LOGINDATA);
            // 新增商品 保证数据正常 
            await spReq.saveDresFull();

            //买家登录 清空购物车,将店铺A与店铺B的商品加入购物车
            await spReq.spClientLogin();
            await spTrade.emptyCart({ check: false });

            const dresAList = await spdresb.searchDres({ queryType: 0, tenantId: sellerA.tenantId }).then(res => res.result.data.dresStyleResultList);
            const dresA = await spReq.getFullForBuyerByUrl({ detailUrl: dresAList[0].detailUrl });
            await spTrade.saveCart(mockJsonParam.cartJson(dresA));

            const dresBList = await spdresb.searchDres({ queryType: 0, tenantId: sellerB.tenantId }).then(res => res.result.data.dresStyleResultList);
            const dresB = await spReq.getFullForBuyerByUrl({ detailUrl: dresBList[0].detailUrl });
            await spTrade.saveCart(mockJsonParam.cartJson(dresB));

            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            myCart = cartManager.setUpCart(cartList);

            combBill.setUpByCart(myCart);
        });
        it('运费合包计算', async function () {
            const shopFee = await combBill.defevalShipFee();
            // console.log(`shopFee=${JSON.stringify(shopFee)}`);
        });
        it('估算商品运费', async function () {
            const shopFee = await combBill.evalShipFee();
            // console.log(`shopFee=${JSON.stringify(shopFee)}`);
        });
        describe('买家下单', async function () {
            before(async function () {
                await spReq.spClientLogin();
                await combBill.savePurBill();
            });
            it('买家查看采购单', async function () {
                const purBillDetail = await spTrade.findPurBillFull({ id: combBill.bills.rows[0].billId });
                console.log(`purBillDetail=${JSON.stringify(purBillDetail)}`);
            });
            it('店铺A查看销售单', async function () {

            });
            it('店铺B查看销售单', async function () {

            });
        });
        describe('买家支付', async function () {
            before(async function () {
                await combBill.createPay();
                console.log(combBill);
                console.log(combBill.bills);
            });
            it('根据订单号查询入账明细', async function () {

            });
        });

        describe.skip('卖家通知取货', async function () {
            before(async function () {
                await common.delay(5000);

                await spReq.spSellerLogin();
                await spTrade.pickUpGood({ id: combBill.bills.rows.find(bill => bill.sellerId == LOGINDATA.tenantId).salesBillId });

                await spReq.spSellerLogin({ shopName: '中洲店' });
                await spTrade.pickUpGood({ id: combBill.bills.rows.find(bill => bill.sellerId == LOGINDATA.tenantId).salesBillId });
            });
            it('仓管员登录', async function () {
            });
        });
        // 消息同步过慢
        describe.skip('确认取货', async function () {
            before(async function () {
                await common.delay(3000);
                await wmsWarehouse.wmsStaffLogin();
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

                const missionList = await wmsStorage.getManageMissionList({ orderType: 1, unitId: LOGINDATA.unitId, warehouseId: LOGINDATA.tenantId }).then(res => res.result.data.rows);
                const targetList = common.takeWhile(missionList, (info) => info.billNo == combBill.bills.rows[0].billNo || info.billNo == combBill.bills.rows[1].billNo);
                // console.log(`targetList=${JSON.stringify(targetList)}`);
                if (targetList.length < 2) {
                    this.skip();
                }
                // console.log(`targetList=${JSON.stringify(targetList)}`);
                const res = await wmsStorage.getGoods({ id: targetList[0].missionId, unitId: targetList[0].unitId, consigneeId: targetList[0].consigneeId });
                // console.log(`res=${JSON.stringify(res)}`);
                await wmsStorage.getGoods({ id: targetList[1].missionId, unitId: targetList[1].unitId, consigneeId: targetList[1].consigneeId });

            });
            it('买家查看采购单', async function () {

            });
        });
        describe.skip('', async function () {

        });

    });


});