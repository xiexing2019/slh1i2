const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');

describe('订单取消-online', function () {
    this.timeout(30000);
    let purRes, sellerTenantId, salesListRes;

    before(async function () {
        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;
        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
    });

    describe('卖家取消订单', function () {
        before('取消订单', async function () {
            await spReq.spClientLogin();
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });//searchToken: 'shangpinVyFvW'
            const style = searchDres.result.data.dresStyleResultList.shift();
            styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,// mockDetailUrl
            });

            //开单
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
            // await common.delay(1000);
            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.skip();
            };

            await spReq.spSellerLogin();

            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            const res = await spTrade.cancelSalseBill({ id: salesListRes.bill.id, cancelKind: BASICDATA['2009'][0].codeValue });
            // console.log(`取消订单:${JSON.stringify(res)}`);
        });

        it('卖家销售单列表', async function () {
            //销售单列表 
            const salesList = await spTrade.salesFindBills({ orderBy: 'proTime', orderByDesc: true, statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            expect(salesListRes.bill.frontFlag).to.equal(5);
        });

        it('卖家销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            // console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(5);
        });
        //该功能未上线，后续加了再打开
        it.skip('买家消息中心', async function () {
            await spReq.spClientLogin();
            await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 92, text: `${purRes.params.jsonParam.orders[0].details[0].spuTitle}`, title: '卖家已取消订单' })
        });

        it('买家采购单列表', async function () {
            await spReq.spClientLogin();
            const purBillList = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'id', orderByDesc: true, statusType: 0 });
            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag).to.equal(5);
        });
    });

    describe('买家取消订单', function () {
        let buyerRem = common.getRandomChineseStr(6);

        before('取消订单', async function () {
            await spReq.spClientLogin();
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });//searchToken: 'shangpinVyFvW'
            const style = searchDres.result.data.dresStyleResultList.shift();
            styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,// mockDetailUrl
            });

            //开单
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
            // console.log(`\npurRes=${JSON.stringify(purRes)}`);
            // await common.delay(1000);
            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.skip();
            };
            await spTrade.cancelPurBill({ id: purRes.result.data.rows[0].billId, cancelKind: BASICDATA['2008'][0].codeValue, buyerRem, check: false });
        });

        it('买家查询采购单', async function () {
            const qlRes = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'id', orderByDesc: true });
            const billInfo = qlRes.result.data.rows.find((data) => data.bill.id == purRes.result.data.rows[0].billId);
            expect(billInfo.bill).to.includes({ frontFlagName: '交易关闭' });
        });

        it('卖家消息查询', async function () {
            await spReq.spSellerLogin();

            const res = await spMdm.pullMessagesList({ tagOneIn: 73 });
            const msgInfo = res.result.data.dataList.find((message) => message.body.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`msgInfo=${JSON.stringify(msgInfo)}`);
            expect(msgInfo.body).to.includes({ text: '买家已经取消订单', title: '买家取消订单', reason: BASICDATA['2008'][0].codeName });
        });

        it('卖家销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ orderBy: 'proTime', orderByDesc: true, statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            //console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag).to.equal(5);
        });

        it('卖家销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(5);
        });
    });


    describe.skip('待付款时取消订单', function () {
        it('', async () => {

        });
    });
});

async function messageCenter({ billNo, tagOneIn, title }) {
    //获取消息列表-未读
    let messageList = await spMdm.pullMessagesList({ unread: 1, tagOneIn, pageNo: 1, pageSize: 0 });
    // console.log(`messageList=${JSON.stringify(messageList)}`);
    let msgInfo = messageList.result.data.dataList.find(message => message.body.billNo = billNo);
    // console.log(`msgInfo=${JSON.stringify(msgInfo)}`);
    expect(msgInfo.body).to.includes({ title });//msgJson[tagOneIn] text,
    //获取未读消息数
    let unReadCount = await spMdm.getPullUnreadCount({ tagOneIn });
    // console.log(`unReadCount.result.data.val=${JSON.stringify(unReadCount.result.data.val)}`);
    expect(unReadCount.result.data.val).to.equal(messageList.result.data.count);
    //更新消息为已读
    await spMdm.updateReadStatus({ msgIds: msgInfo.id });
    let unReadCount2 = await spMdm.getPullUnreadCount({ tagOneIn });
    expect(common.sub(unReadCount.result.data.val, 1)).to.equal(unReadCount2.result.data.val);
    //获取消息列表
    messageList = await spMdm.pullMessagesList({ tagOneIn });
    msgInfo = messageList.result.data.dataList.find((message) => message.body.billNo == billNo);
    expect(msgInfo.unread).to.equal(0);
};