const common = require('../../../lib/common');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');

// http://zentao.hzdlsoft.com:6082/zentao/story-view-786.html
// 退货时，pickedNum，pickFlag会根据退货数减少 在退货流程中覆盖
describe('按款配货', async function () {
    this.timeout(TESTCASE.timeout);

    let purRes;
    before('买家下单', async function () {
        await spReq.spSellerLogin();
        const sellerTenantId = LOGINDATA.tenantId;

        await spReq.spClientLogin();
        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });
        const styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchDres.result.data.dresStyleResultList.shift().detailUrl });

        // 买家下单并支付
        purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
        const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
    });

    it('卖家配货-超过应配数量', async function () {
        await spReq.spSellerLogin();
        const billDetail = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
        const pickDetailJson = getPickDetailJson({ salesDetail: billDetail, type: 2 });
        pickDetailJson.check = false;
        const pickRes = await spTrade.pickDetail(pickDetailJson);
        expect(pickRes.result).to.include({ msgId: 'sales_detail_pick_num_error' });
    });

    describe('卖家配货-不配齐', function () {
        let pickDetailJson;
        before(async function () {
            await spReq.spSellerLogin();
            const billDetail = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            pickDetailJson = getPickDetailJson({ salesDetail: billDetail, type: 0 });
            await spTrade.pickDetail(pickDetailJson);
        });
        it('查询销售单', async function () {
            const salesBillFull = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            pickDetailCheck({ billDetail: salesBillFull, salesDetailPickList: pickDetailJson.salesDetailPickList });
        });
        it('查询采购单', async function () {
            await spReq.spClientLogin();
            const purBillFull = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            pickDetailCheck({ billDetail: purBillFull, salesDetailPickList: pickDetailJson.salesDetailPickList });
        });
    });

    describe('卖家配货-配齐', function () {
        let pickDetailJson;
        before(async function () {
            await spReq.spSellerLogin();
            const billDetail = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            pickDetailJson = getPickDetailJson({ salesDetail: billDetail, type: 1 });
            await spTrade.pickDetail(pickDetailJson);
        });
        it('查询销售单', async function () {
            const salesBillFull = await spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            pickDetailCheck({ billDetail: salesBillFull, salesDetailPickList: pickDetailJson.salesDetailPickList });
        });
        it('查询采购单', async function () {
            await spReq.spClientLogin();
            const purBillFull = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            pickDetailCheck({ billDetail: purBillFull, salesDetailPickList: pickDetailJson.salesDetailPickList });
        });
    });

});

/**
 * 拼接卖家按款配货参数
 * @param {object} param
 * @param {object} param.salesDetail 销售单明细
 * @param {string} param.type 0未配齐 1配齐 2超过应配数量
 */
function getPickDetailJson({ salesDetail, type }) {
    // 下单的sku的数量，生成时一定大于0
    const num = type == 0 ? -1 : type == 2 ? 1 : 0;
    const salesDetailPickList = salesDetail.skus.map(sku => {
        return {
            skuId: sku.skuId,
            salesDetailId: sku.id,
            pickedNum: common.add(sku.skuNum, num),
            pickFlag: type == 0 ? 0 : 1,//是否配齐，1：配齐 0：未配齐
        }
    });
    return {
        billId: salesDetail.bill.id,
        salesDetailPickList
    };
};

/**
 * 按款配货校验
 * @param {object} param
 * @param {object} param.billDetail 单据明显
 * @param {object} param.pickDetailList 配货明细期望值
 */
function pickDetailCheck({ billDetail, salesDetailPickList }) {
    salesDetailPickList.forEach(pickInfo => {
        const sku = billDetail.skus.find(data => data.skuId == pickInfo.skuId);
        expect(sku).to.include({ pickedNum: pickInfo.pickedNum, pickFlag: pickInfo.pickFlag });
    });
};