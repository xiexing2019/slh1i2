const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');

describe('智能进货车', async function () {
    this.timeout(TESTCASE.timeout);
    before(async function () {
        await spReq.spClientLogin();
    });

    it('调用智能进货单接口后，会内部重置一次智能进货单', async function () {
        const spuIds1 = await spdresb.intelligentCart().then(res => res.result.data.spuList.map(spu => spu.spuId));
        await common.delay(500);
        const spuIds2 = await spdresb.intelligentCart().then(res => res.result.data.spuList.map(spu => spu.spuId));
        expect(spuIds1, `${spuIds1}\n${spuIds2}`).not.to.eql(spuIds2);
    });
});