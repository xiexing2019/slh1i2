'use strict'
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const logisHelp = require('../../help/wmsHelp/logisHelp');
const configParam = require('../../help/configParamManager');
const format = require('../../../data/format');

//需要重构  接口返回加了其他新字段。

describe('物流商发货流程-online', function () {
    this.timeout(30000);
    let sellerInfo, styleRes, purRes, clientInfo, logisInfo, deliverBillRes, logisBill;

    before('买家下单', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin();
        clientInfo = _.cloneDeep(LOGINDATA);

        let provCode;
        await spMdm.getUserDefaultRecInfo().then((res) => {
            LOGINDATA.defAddressId = res.result.data.recInfo.id;
            provCode = res.result.data.address.provinceCode;
        });

        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerInfo.tenantId });
        const style = searchDres.result.data.dresStyleResultList.shift();
        styleRes = await spReq.getFullForBuyerByUrl({
            detailUrl: style.detailUrl,
        });
        //买家下单，并且支付
        const purJson = mockJsonParam.purJson({ styleInfo: styleRes });
        // console.log(`purJson=${JSON.stringify(purJson)}`);
        const orderSpus = purJson.orders[0].details.map((detail) => {
            return {
                spuId: detail.spuId,
                orderNum: 100//detail.num
            }
        });
        // console.log(`orderSups=${JSON.stringify(orderSpus)}`);
        const fee = await spconfb.evalShipFee({
            buyerId: clientInfo.tenantId,
            provinceCode: provCode,
            orders: [{
                sellerId: sellerInfo.tenantId,
                orderSpus: orderSpus,
            }]
        });
        //console.log(`fee=${JSON.stringify(fee)}`);
        purJson.orders[0].main.shipFeeMoney = fee.result.data.fees[0].fee;
        purJson.orders[0].main.totalMoney += fee.result.data.fees[0].fee;
        purJson.orders[0].main.payKind = 1;
        purRes = await spTrade.savePurBill(purJson);

        const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.totalMoney });
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
        // console.log(Date.now());
    });

    describe('卖家发货', async function () {
        before(async function () {
            await spReq.spSellerLogin();
            const salesBillList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows);
            const salesBill = salesBillList.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);

            const qfRes = await spTrade.salesFindBillFull({ id: salesBill.bill.id });
            const details = qfRes.result.data.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });

            // 获取物流商信息
            const logisList = await spconfb.findLogisList1({ cap: 1 });
            logisInfo = logisList.result.data.rows.find(val => val.name == '申通快递');
            // console.log(`logisInfo=${JSON.stringify(logisInfo)}`);

            // 发货
            deliverBillRes = await spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisInfo.id,
                    waybillNo: `${Date.now()}${common.getRandomNumStr(3)}`,
                    buyerId: clientInfo.tenantId,
                    shipFeeMoney: 0,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            });

            logisBill = logisHelp.setUpLogisBill({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo });
        });

        // 现阶段，本功能只做在销售单，即卖家和平台查看  买家端不体现
        describe.skip('订单12小时物流信息触发状态变化', async function () {
            let paramInfo;
            before(async function () {
                await spAuth.staffLogin();
                paramInfo = await configParam.getParamInfo({ domainKind: 'system', code: 'bill_deliver_no_update_time_out' });
            });
            describe('错误运单号', async function () {
                it('时间范围内', async function () {
                    await paramInfo.updateParam({ val: 0 });

                    const billFull = await spTrade.findBillFullByAdmin({ id: purRes.result.data.rows[0].billNo, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                    console.log(`billFull=${JSON.stringify(billFull)}`);

                });
                it('时间范围外', async function () {
                    await paramInfo.updateParam({ val: 12 });

                    await spReq.spClientLogin();
                    const billFull = await spTrade.findBillFullByAdmin({ id: purRes.result.data.rows[0].billNo, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                    console.log(`billFull=${JSON.stringify(billFull)}`);
                });
            });
            describe('正确运单号', async function () {
                before('卖家发货物流更新', async function () {
                    await logisBill.changeLogisStatus({ status: -1 });

                    await spTrade.updateDeliverBill({
                        deliverId: deliverBillRes.result.data.billId,
                        waybillNo: logisInfo.getLogisJson().number,
                        logisCompId: logisInfo.id,
                        logisCompName: logisInfo.name,
                    });
                });
                it('时间范围内', async function () {
                    await paramInfo.updateParam({ val: 0 });

                    const billFull = await spTrade.findBillFullByAdmin({ id: purRes.result.data.rows[0].billNo, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                    console.log(`billFull=${JSON.stringify(billFull)}`);

                });
                it('时间范围外', async function () {
                    await paramInfo.updateParam({ val: 12 });

                    await spReq.spClientLogin();
                    const billFull = await spTrade.findBillFullByAdmin({ id: purRes.result.data.rows[0].billNo, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                    console.log(`billFull=${JSON.stringify(billFull)}`);
                });
            });
        });
    });

    describe('初始status=1', function () {
        let upRes;
        before('修改物流状态', async function () {
            upRes = await logisBill.changeLogisStatus({ status: 1 });
        });

        it('买家登录查询物流轨迹', async function () {
            await spReq.spClientLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });

        it('卖家查询物流轨迹', async function () {
            await spReq.spSellerLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });
    });

    describe('status=0', function () {
        let upRes;
        before('修改物流状态', async function () {
            upRes = await logisBill.changeLogisStatus({ status: 0 });
        });

        it('买家登录查询物流轨迹', async function () {
            await spReq.spClientLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });

        it('卖家查询物流轨迹', async function () {
            await spReq.spSellerLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });
    });

    describe('status=5', function () {
        let upRes;
        before('修改物流状态', async function () {
            upRes = await logisBill.changeLogisStatus({ status: 5 });
        });

        it('买家登录查询物流轨迹', async function () {
            await spReq.spClientLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });

            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });

        it('卖家查询物流轨迹', async function () {
            await spReq.spSellerLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        })
    });

    describe('status=3', function () {
        let upRes;
        before('修改物流状态', async function () {
            upRes = await logisBill.changeLogisStatus({ status: 3 });
            await common.delay(1000);
        });

        it('买家登录查询物流轨迹', async function () {
            await spReq.spClientLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });
        // 单据状态为已签收
        it('买家校验采购单状态--已签收', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            const purBill = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            expect(purBill.bill.flag).to.be.oneOf([6, 7]);
        });
        it('卖家查询物流轨迹', async function () {
            await spReq.spSellerLogin();
            let logisTraceRes = await spconfb.getLogisTrace({ waybillNo: deliverBillRes.params.jsonParam.main.waybillNo, logisCompId: deliverBillRes.params.jsonParam.main.logisCompId, newFlag: 1 });
            let exp = logisTraExp(deliverBillRes, upRes);
            common.isApproximatelyEqualAssert(exp, logisTraceRes.result.data);
        });
        // 单据状态为已签收
        it('卖家校验销售单状态--已签收', async function () {
            const salesBillList = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows);
            const salesBill = salesBillList.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            expect(salesBill.bill).to.include({ flag: 7 });
        });
    });

});

function logisTraExp(deliverRes, logisRes) {
    let exp = {
        waybillNo: deliverRes.params.jsonParam.main.waybillNo,
        logisCompName: '申通快递',
        status: logisRes.params.jsonParam.status,

    };
    let traces = [];
    logisRes.params.jsonParam.track.forEach(val => {
        let info = format.dataFormat(val, 'acceptStation=context;acceptTime=ftime');
        traces.push(info);
    });
    traces.reverse();
    exp.traces = exp.Traces = traces;
    return exp;
};