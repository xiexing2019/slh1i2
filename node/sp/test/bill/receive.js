const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');

describe('买家收货-online', function () {
    this.timeout(30000);
    let sellerTenantId;
    before(async function () {
        //卖家登录
        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;

        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
    });
    // 采购订单延迟收货，先仅允许延迟收货一次。默认一次延迟收货天数2天
    // 仅当订单处于 6 代表订单已发货状态 以及 7代表 订单签收 状态 允许 订单延迟收货
    // 确认收货后-延迟收货 放到bill开单流程中
    describe('延迟收货', function () {
        let clientInfo, purRes;
        before(async function () {
            //买家登录
            await spReq.spClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);
            //获取用户默认收货地址
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });
            const style = searchDres.result.data.dresStyleResultList.shift();
            const styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,
            });

            //开单
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));

            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.skip();
            };

            //买家支付
            const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            //延迟收货
            const res = await spTrade.extendedReturn({ id: purRes.result.data.rows[0].billId, check: false });
            expect(res.result).to.includes({ msgId: 'spbillflag_before_deliver' });
            await common.delay(500);
        });

        it('发货状态-延迟收货', async function () {
            //卖家登录
            await spReq.spSellerLogin();
            const salesBill = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            //查询单据详情
            const salesFullRes = await spTrade.salesFindBillFull({ id: salesBill.bill.id });
            const details = salesFullRes.result.data.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });
            const logisList = await spconfb.findLogisList1({ cap: 1 });
            //卖家发货
            await spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[0].id,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: clientInfo.tenantId,
                    shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            });

            //买家登录
            await spReq.spClientLogin();
            await common.delay(500);
            await spTrade.extendedReturn({ id: purRes.result.data.rows[0].billId });
        });
        it('重复延迟收货', async function () {
            const res = await spTrade.extendedReturn({ id: purRes.result.data.rows[0].billId, check: false });
            expect(res.result).to.includes({ msgId: 'extendedreturnnum_only_one' });
        });
    });

});