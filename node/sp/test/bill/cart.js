'use strict';
// const spugr = require('../../../reqHandler/sp/global/spugr');
const mockJsonParam = require('../../help/mockJsonParam');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const common = require('../../../lib/common');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const cartHelp = require('../../help/cartHelp');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const dresManage = require('../../help/dresManage');
const configParamManager = require('../../help/configParamManager');

describe('购物车模块-online', function () {
    this.timeout(30000);
    let sellerTenantId, dresFullRes;

    before(async function () {
        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;

        await spReq.spClientLogin();
        const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerTenantId }).then(res => res.result.data.dresStyleResultList);
        dresFullRes = await spReq.getFullForBuyerByUrl({ detailUrl: dresList[0].detailUrl });
        // console.log(`dresFullRes=${JSON.stringify(dresFullRes)}`);
    });

    // 购物车相关用例。包括新增购物车，查看购物车，修改购物车，删除购物车操作
    // 用例 http://zentao.hzdlsoft.com:6082/zentao/testcase-view-2048-2.html
    describe('购物车管理', function () {
        let carts;
        before('买家登录,清空购物车', async function () {
            await spReq.spClientLogin();
            await spTrade.emptyCart({ check: false });

            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            carts = cartHelp.setUpCart(cartList);
        });

        describe.skip('新增购物车', async function () {
            let cartList;
            before(async function () {
                await carts.saveCart(mockJsonParam.cartJson(dresFullRes));
            });
            it('查看购物车', async function () {
                cartList = await spTrade.showCartList().then(res => res.result.data.rows);
                carts.checkCartList(cartList);
            });
            it('查询购物车商品总数', async function () {
                const cartNum = await spTrade.showCartNum().then(res => res.result.data);
                let totalNum = 0;
                cartList.forEach(ele => ele.carts.forEach(cart => totalNum += cart.skuNum));
                expect(cartNum.val).to.equal(totalNum);
            });
        });

        describe.skip('修改购物车', async function () {
            before(async function () {
                const updateJson = mockJsonParam.updateCart(saveCartRes.params.jsonParam);
                updateJson.id = saveCartRes.result.data.val;
                let updateCartResult = await spTrade.updateCart(updateJson);
            });
            it('', async function () {
                cartHelp.updateCartExpect(cartListRes.result.data.rows, saveCartRes, updateCartResult);

                let lastCartListRes = await spTrade.showCartList({ pageSize: 20 });
                common.isApproximatelyEqualAssert(cartListRes.result.data.rows, lastCartListRes.result.data.rows);
            });
            it('查询购物车商品总数', async function () {
                const cartNum = await spTrade.showCartNum().then(res => res.result.data);
                let totalNum = 0;
                cartList.forEach(ele => ele.carts.forEach(cart => totalNum += cart.skuNum));
                expect(cartNum.val).to.equal(totalNum);
            });
        });

        describe.skip('删除购物车', async function () {
            before(async function () {

            });
            it('', async function () {

            });
            it('查询购物车商品总数', async function () {
                const cartNum = await spTrade.showCartNum().then(res => res.result.data);
                let totalNum = 0;
                cartList.forEach(ele => ele.carts.forEach(cart => totalNum += cart.skuNum));
                expect(cartNum.val).to.equal(totalNum);
            });
        });

        describe.skip('清空购物车', async function () {
            before(async function () {
                await spTrade.emptyCart();
            });
            it('查看购物车', async function () {
                const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
                expect(cartList).to.includes({ total: 0, count: 0, rows: [] });
            });
            it('查询购物车商品总数', async function () {
                const cartNum = await spTrade.showCartNum().then(res => res.result.data);
                expect(cartNum.val).to.equal(0);
            });
        });

        it('删除购物车验证', async function () {
            await spTrade.saveCart(mockJsonParam.cartJson(dresFullRes));
            let firstCartListRes = await spTrade.showCartList({ pageSize: 20 });
            let saveCartRes = await spTrade.saveCart(mockJsonParam.cartJson(dresFullRes));
            let saveCartparam = saveCartRes.params.jsonParam;
            await spTrade.deleteCart({ id: saveCartRes.result.data.val });
            cartHelp.deleteCartExpect(firstCartListRes.result.data.rows, saveCartparam);
            let lastCartListRes = await spTrade.showCartList({ pageSize: 20 });
            if (firstCartListRes.result.data.rows[0].carts.length == 0) {
                firstCartListRes.result.data.rows.length = 0;
            };
            common.isApproximatelyArrayAssert(firstCartListRes.result.data.rows, lastCartListRes.result.data.rows);
        });

        it('更新购物车数量传0验证', async function () {
            let saveRes = await spTrade.saveCart(mockJsonParam.cartJson(dresFullRes)); //先保存一条数据
            let updateJson = saveRes.params.jsonParam;
            updateJson.skuNum = 0;
            updateJson.id = saveRes.result.data.val;
            updateJson.check = false;
            let updateResult = await spTrade.updateCart(updateJson);
            expect(updateResult.result, '更新购物车未限制0或者小于0的情况').to.includes({ "msg": "加入购物车数量必须大于0" });
        });

        it('批量添加购物车', async function () {
            await spTrade.saveCart(mockJsonParam.cartJson(dresFullRes));
            let cartListRes = await spTrade.showCartList({ pageSize: 20 });
            dresFullRes.count = 2;
            let saveCartInBatchsRes = await spTrade.saveCartInBatchs(mockJsonParam.cartJson(dresFullRes));

            cartHelp.expectResult(cartListRes.result.data.rows, saveCartInBatchsRes.params.jsonParam);
            let lastCartListRes = await spTrade.showCartList({ pageSize: 20 });
            common.isApproximatelyArrayAssert(cartListRes.result.data.rows[0].carts, lastCartListRes.result.data.rows[0].carts);
        });

        it('批量删除购物车', async function () {
            await spTrade.saveCartInBatchs(mockJsonParam.cartJson(dresFullRes));
            let cartList = await spTrade.showCartList({ pageSize: 20 });

            let ids = [];
            for (let i = 0; i < 2; i++) {
                ids[i] = cartList.result.data.rows[0].carts[i].id;
            };
            let deleteRes = await spTrade.deleteCartInBatch({ ids: ids });
            cartList.result.data.rows[0].carts.splice(0, 2);
            let lastCartListRes = await spTrade.showCartList({ pageSize: 20 });

            common.isApproximatelyEqualAssert(cartList.result, lastCartListRes.result, ["total", "count"]);
        });

    });

    describe.skip('混批校验', async function () {
        it('', async function () {
            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            const carts = cartHelp.setUpCart(cartList);
            await carts.saveCart(mockJsonParam.cartJson(dresFullRes));

            const cartList2 = await spTrade.showCartList().then(res => res.result.data.rows);
            console.log(`cartList=${JSON.stringify(cartList2)}`);

            // const jsonParam = cartList.map(cart => {

            // });
            // const verifyOrderLimit = await spTrade.verifyOrderLimit(jsonParam);
            // console.log(`verifyOrderLimit=${JSON.stringify(verifyOrderLimit)}`);


        });
    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2209.html
    // 为购物车内部调用，查询商品详情获取价格与库存等信息
    describe('购物车获取商品详情', async function () {
        const dres = dresManage.setupDres();
        let spSpecialStocknumToCar;
        before(async function () {
            spSpecialStocknumToCar = await configParamManager.getParamInfo({ code: 'sp_special_stocknum_to_car', domainKind: 'system' });

            const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerTenantId }).then(res => res.result.data.dresStyleResultList);
            dres.setByEs(dresList[0]);
            await dres.searchAndSetByDetail();
        });
        it('findSpuFullByCarts', async function () {
            const dresFull = await spdresb.findSpuFullByCarts([{ spuIds: [dres.id], unitId: dres.spu.unitId, tenantId: dres.tenantId }]).then(res => res.result.data.rows[0]);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            dresFull.skus.forEach(sku => common.isApproximatelyArrayAssert(dres.skus.get(sku.id), sku));
            expect(dresFull).to.includes({ id: dres.id, stockFlag: dres.spu.stockFlag, stockNum: dres.spu.stockFlag == 0 ? 0 : spSpecialStocknumToCar.val });
        });
        it('findSpuFullBySpuIds', async function () {
            const dresFull = await spdresb.findSpuFullBySpuIds({ spuIds: dres.id, unitId: dres.spu.unitId, tenantId: dres.tenantId }).then(res => res.result.data.rows[0]);
            common.isApproximatelyArrayAssert(dres.spu, dresFull.spu);
            dresFull.skus.forEach(sku => common.isApproximatelyArrayAssert(dres.skus.get(sku.id), sku));
            expect(dresFull).to.includes({ id: dres.id, stockFlag: dres.spu.stockFlag, stockNum: dres.spu.stockFlag == 0 ? 0 : spSpecialStocknumToCar.val });
        });
    });

    // 用例 http://zentao.hzdlsoft.com:6082/zentao/testcase-view-2068-1.html
    describe('购物车下单', async function () {
        before('批量添加数据到购物车', async function () {
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            //购物车批量添加2条货品
            dresFullRes.count = 2;
            await spTrade.saveCartInBatchs(mockJsonParam.cartJson(dresFullRes));
        });

        it('开单操作', async function () {
            const cartList = await spTrade.showCartList();
            const purRes = await spTrade.savePurBill(mockJsonParam.purJsonByCart(cartList));
            if (purRes.result.data.rows[0].isSuccess != 1) {
                throw new Error(`开单失败:${purRes.result.data.rows[0].failedMsg}`);
            };
            await common.delay(500);
            const cartList1 = await spTrade.showCartList();
            // console.log(`=${JSON.stringify(cartList1)}`);
            expect(cartList1.result.data.rows.length, '购物车添加到订单后款号还在购物车存在').to.be.equal(0);
        });
    });

    describe('异常操作', async function () {
        before(async function () {
            await spReq.spClientLogin();
        });
        it('删除购物车--参数类型', async function () {
            const deleteCartRes = await spTrade.deleteCart({ 'id': '', 'check': false });
            expect(deleteCartRes.result).to.includes({ "msgId": 'id参数不正确!' });
        });

        it.skip('删除非当前用户的购物车', async function () {
            const deleteCartRes = await spTrade.deleteCart({ 'id': 99999999, 'check': false });
            expect(deleteCartRes.result).to.includes({ "msg": "当前购物车信息不属于当前登录用户" });
        });

        it.skip('添加负数量的货品到购物车', async function () {
            const cartParam = mockJsonParam.cartJson(dresFullRes);
            cartParam.cart.skuNum = -10;
            cartParam.check = false;
            const saveRes = await spTrade.saveCart(cartParam);
            expect(saveRes.result, '加入购物车未限制0或者小于0的情况').to.includes({ msg: '加入购物车数量必须大于0', msgId: 'cart_error_skunum' });
        });

        it('批量删除购物车--不选择明细直接删除', async function () {
            const deleteRes = await spTrade.deleteCartInBatch({ 'ids': [], 'check': false });
            expect(deleteRes.result, '没有选择删除明细直接删除，没有提示').to.includes({ "msg": "参数[ids]必填" })
        });
    });

});

/**
 * 获取添加到购物车 skuid对应的库存
*/
async function getInvNum(params) {
    await spReq.spSellerLogin();
    let skuInfo = await spdresb.getFullById({ id: params.cart.spuId }).then(res => res.result.data.skus);
    let info = skuInfo.find(res => res.id == params.cart.skuId);
    params.cart.invNum = info.num;
};