const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const configParam = require('../../help/configParamManager');
const dresManage = require('../../help/dresManage');
// const billManage = require('../../help/billManage');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

// http://c.hzdlsoft.com:7082/Wiki.jsp?page=Sp-bill-return
// 退货流程需要重构,4~7的状态需要高梦忠提供退款回调才能进行判断
describe('退款退货', function () {
    this.timeout(30000);
    let purBillInfo, returnBill, saveDeliverRes, sellerTenantId, clusterCode, returnReason, billConfirmTimeout, receiveInfo;

    before(async function () {
        await spReq.spClientLogin();
        // 买家收货地址 
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);

        //退货原因（字典类别2005）
        BASICDATA['2005'] = await spugr.getDictList({ typeId: '2005', flag: 1 })
            .then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['2012'] = await spugr.getDictList({ typeId: '2012', flag: 1 })
            .then((res) => res.result.data.rows);
        returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await spReq.spSellerLogin();
        sellerTenantId = LOGINDATA.tenantId;
        clusterCode = LOGINDATA.clusterCode;//租户集群id

        await spAuth.staffLogin();
        //获取参数：SP订单未发货多久可以直接退款 
        billConfirmTimeout = await configParam.getParamInfo({ domainKind: 'system', code: 'bill_confirm_time_out' });
    });

    describe('退货并且退款', function () {
        describe('买家创建退款退货申请单-退货退款', function () {
            before('创建退款申请', async function () {
                await spReq.spClientLogin();
                const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });

                //为了避免选择已经发生过退款操作的单子进行退款操作
                purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0);
                expect(purBillInfo, `未找到可退货退款的单子`).not.to.be.undefined;
                // console.log(`purBillInfo=${JSON.stringify(purBillInfo)}`);
                returnBill = await spTrade.saveReturnBill(mockJsonParam.returnBillJson(purBillInfo));
                returnBill.params.jsonParam.returnDate = billConfirmTimeout.val;
                // console.log(`\nreturnBill=${JSON.stringify(returnBill)}`);
            });

            it('买家查看采购订单的售后列表', async function () {
                const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
                // console.log(`\nres.result.data.rows[0]=${JSON.stringify(res.result.data.rows[0])}`);
                let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1 });  //要查发起申请后的预期结果，所以typeId传6.
                // console.log(`\nexp=${JSON.stringify(exp)}`);
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            });
            it('买家申请退货退款后平台端退货列表查询', async function () {
                this.retries(2);//平台端退货列表更新flag慢
                await common.delay(500);

                //管理员登录
                await spAuth.staffLogin();
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`spReturnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `买家申请退货退款后平台端退货列表查询不到退货的单据 billNo:${purBillInfo.bill.billNo}`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
                // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
            });
            it('买家申请退货退款后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });

                // console.log(`\n买家申请退货spPurInfo=${JSON.stringify(spPurInfo)}`);
                let adminPurExp = adminReturnInfoExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1, receiveInfo
                });

                // console.log(`\nadminPurExp=${JSON.stringify(adminPurExp)}`);
                common.isApproximatelyEqualAssert(adminPurExp, spPurInfo.result.data);
            });
        });

        describe('卖家审核退货申请', function () {
            before('卖家查看退货列表', async function () {
                await spReq.spSellerLogin();
                let res = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            });

            it('审核前卖家查看退货单详情', async function () {
                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });
                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1
                });
                common.isApproximatelyEqualAssert(exp, res.result.data);
            });

            it('卖家审核', async function () {
                //卖家审核
                const checkReturnRes = await spTrade.checkReturnBill({
                    id: returnBill.result.data.val,
                    checkResult: 1, //同意
                });
            });

            it('审核后卖家查看退货列表', async function () {
                const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ purBillInfo, returnBill, flag: 4, returnType: 1 });
                common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
            })

            it('审核后卖家查看退货详情', async function () {
                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });
                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1,
                });
                common.isApproximatelyEqualAssert(exp, res.result.data);
            });
            it('卖家审核后平台端退货列表查询', async function () {
                //管理员登录
                this.retries(2);//平台端退货列表更新flag慢
                await common.delay(1000);
                await spAuth.staffLogin();
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`spReturnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 4, returnType: 1 });
                // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
            });
            it('卖家审核后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });
                // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
                let sellerExp = adminReturnInfoExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1, receiveInfo
                });
                // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
                common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['backFlag']);
            });
        });

        describe('买家发货', async function () {
            before('买家登录', async function () {
                await spReq.spClientLogin();
            });

            it('发货前买家查看售后列表', async function () {
                const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
                let exp = getBuyerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1
                });
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            });

            it('买家发货', async function () {
                let logisList = await spconfb.findLogisList1({ cap: 1 });
                saveDeliverRes = await spTrade.saveReturnDeliver({
                    id: returnBill.result.data.val,
                    logisCompId: logisList.result.data.rows[0].id,
                    logisCompName: logisList.result.data.rows[0].name,
                    waybillNo: common.getRandomNumStr(10),
                    deliverRem: '退货'
                });
            });

            it('发货后买家查看售后列表', async function () {
                const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
                let exp = getBuyerExp({
                    purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 4, returnType: 1
                });
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            });
            it('买家发货后平台端退货列表查询', async function () {
                this.retries(2);//平台端退货列表更新flag慢
                await common.delay(500);
                //管理员登录
                await spAuth.staffLogin();
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `买家发货后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 5, returnType: 1, saveDeliverRes });
                // console.log(`\nspReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
            });
            it('买家发货后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });
                // console.log(`买家发货spPurInfo=${JSON.stringify(spPurInfo)}`);
                let sellerExp = adminReturnInfoExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 4, returnType: 1, saveDeliverRes, receiveInfo
                });
                // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
                common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data);

            });
        });

        describe('卖家确认收货', async function () {
            before('卖家登录，切换门店', async function () {
                await spReq.spSellerLogin();
            });

            it('确认收货前查看退货列表', async function () {
                const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ returnBill, purBillInfo, flag: 5, returnType: 1, saveDeliverRes });
                common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0]);
            });

            it('确认收货前查看退货详情', async function () {
                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });
                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 4, returnType: 1, saveDeliverRes
                });
                common.isApproximatelyEqualAssert(exp, res.result.data);
            });

            it('卖家确认收货', async function () {
                const confirmReceiptRes = await spTrade.confirmReturnBillReceipt({ id: returnBill.result.data.val });
            });

            it('确认收货后查看退货列表', async function () {
                await common.delay(1500);
                const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ returnBill, purBillInfo, flag: 6, returnType: 1 });
                common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['flag', 'frontFlag']);
            });

            it.skip('确认收货后查看退货详情', async function () {

                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });
                // let exp = getSellerExp({
                //     purBillInfo, saveReturnBillRes: returnBill, typeId: 5, returnType: 1, saveDeliverRes
                // });
                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 1, saveDeliverRes
                });
                // console.log(`确认收货后查看退货详情res=${JSON.stringify(res)}`);
                // console.log(`\nexp=${JSON.stringify(exp)}`);
                common.isApproximatelyEqualAssert(exp, res.result.data, ['flag', 'frontFlag']);
            });

            it('买家登录查看售后流程', async function () {
                await spReq.spClientLogin();

                const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
                // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
                expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
                let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 1 });
                common.isApproximatelyEqualAssert(exp, purReturnListInfo);

            });
            it('卖家确认收货后平台端退货列表查询', async function () {
                //管理员登录
                this.retries(2);//平台端退货列表更新flag慢
                await common.delay(1500);
                await spAuth.staffLogin();
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`卖家确认收货后spReturnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `卖家确认收货后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 6, returnType: 1 });
                // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['flag']);
            });
            it('卖家确认收货后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });
                // console.log(`卖家确认收货后spPurInfo=${JSON.stringify(spPurInfo)}`);
                let sellerExp = adminReturnInfoExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 1, saveDeliverRes, receiveInfo
                });
                // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
                common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data);
            });
        });
    });

    //待付款，待发货，退货订单是不能进行退款退货的
    describe('不能退货退款状态的单据进行退货退款', function () {
        // const dres = dresManage.setupDres();
        let purRes;
        before('登录', async function () {
            await spReq.spClientLogin();
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            //查询商品信息
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });
            // console.log(`searchDres=${JSON.stringify(searchDres)}`);
            const style = searchDres.result.data.dresStyleResultList.shift();
            // console.log(`style=${JSON.stringify(style)}`);
            // mockDetailUrl = `sp_domain_url/spb/api.do?apiKey=ec-spdresb-dresSpu-getFullForBuyer&_cid=cs3s1&_tid=14&spuId=38&buyerId=${LOGINDATA.tenantId}`;
            styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,// mockDetailUrl
            });

            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
            addData = { totalPurTimes: 1, totalPurNum: purRes.params.jsonParam.orders[0].main.totalNum, totalPurMoney: purRes.params.jsonParam.orders[0].main.totalMoney };
            // await common.delay(1000);
            //开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.skip();
            };
            await common.delay(500);
        });

        it('未付款的单子退货退款', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 1 });   //查询代付款的单子
            let saveParams = mockJsonParam.returnBillJson(purBillList.result.data.rows[0])
            saveParams.check = false;
            const res = await spTrade.saveReturnBill(saveParams);
            expect(res.result, '未付款的单据也能退款退货了').to.includes({ "msgId": "return_bill_create_error" });
        });

        //需要完善一下
        it('待发货的单子退货退款', async function () {
            await spReq.spClientLogin();
            let payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

            await common.delay(500);
            const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });   //查询待发货的单子
            let saveParams = mockJsonParam.returnBillJson(purBillList.result.data.rows[0]);
            saveParams.main.typeId = 1;
            saveParams.check = false;
            const res = await spTrade.saveReturnBill(saveParams);
            // console.log(`res=${JSON.stringify(res)}`);
            expect(res.result, '未发货的单据也能退款退货了').to.includes({ "msgId": "return_bill_create_error" });
        });

        it.skip('退货订单的单子退货退款', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 5 });   //退货退款的单子
            let saveParams = mockJsonParam.returnBillJson(purBillList.result.data.rows[0])
            saveParams.check = false;
            const res = await spTrade.saveReturnBill(saveParams);

            expect(res.result, '正在退货的单子可以再次退货了').to.includes({ "msgId": "return_bill_create_error" });
        });
    });

    describe('仅退款', async function () {
        let totalNum;
        describe('买家创建退款申请单-退货退款', function () {
            before('创建退款申请', async function () {
                await spReq.spClientLogin();
                const purBillList = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });
                //为了避免选择已经发生过退款操作的单子进行退款操作
                for (let i = 0; i < purBillList.result.data.rows.length; i++) {
                    if (purBillList.result.data.rows[i].bill.backFlag == 0) {
                        purBillInfo = purBillList.result.data.rows[i];
                        break;
                    };
                };
                let params = mockJsonParam.returnBillJson(purBillInfo);
                totalNum = params.main.totalNum;
                params.main.typeId = 0;
                params.main.totalNum = 0;
                params.main.returnReason = returnReason.codeValue;
                params.main.returnReasonName = returnReason.codeName;
                params.details.map(res => res.num = 0);
                returnBill = await spTrade.saveReturnBill(params);
                returnBill.params.jsonParam.returnDate = billConfirmTimeout.val;
            });

            it('买家查看采购订单的售后列表', async function () {
                const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
                // console.log(`returnBill=${JSON.stringify(returnBill)}`);

                let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0 });
                // exp.bill.totalNum = totalNum;
                // console.log(`\nexp=${JSON.stringify(exp)}`);
                // console.log(`\nres.result.data.rows[0]=${JSON.stringify(res.result.data.rows[0])}`);
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            });
            it('买家申请退款后平台端退货列表查询', async function () {
                //管理员登录
                this.retries(2);//平台端退货列表更新flag慢
                await spAuth.staffLogin();
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `买家申请退款后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
                // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
            });
            it('买家申请退款后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });
                // console.log(`买家申请退货spPurInfo=${JSON.stringify(spPurInfo)}`);
                let sellerExp = adminReturnInfoExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0, receiveInfo
                });
                // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
                common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['rem', 'skuNum']);
            });
        });

        describe('卖家审核退货申请', function () {
            before('卖家查看退货列表', async function () {
                await spReq.spSellerLogin();
                let res = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
                common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['rem', 'skuNum']);
            });

            it('审核前卖家查看退货单详情', async function () {
                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });
                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0
                });
                common.isApproximatelyEqualAssert(exp, res.result.data, ['rem', 'skuNum']);

            });

            it('卖家审核', async function () {
                //卖家审核
                await spTrade.checkReturnBill({
                    id: returnBill.result.data.val,
                    checkResult: 1, //同意
                });

            });


            it('审核后卖家查看退货列表', async function () {
                await common.delay(1000);
                const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBill.result.data.val });
                let exp = getSellerListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
                common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['rem', 'flag', 'frontFlag', 'frontFlagName', 'backFlag', 'backMoney']);
            })

            it('审核后卖家查看退货详情', async function () {
                const res = await spTrade.getReturnBillDetail({ id: returnBill.result.data.val });

                let exp = getSellerExp({
                    purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0
                });
                common.isApproximatelyEqualAssert(exp, res.result.data, ['rem', 'flag', 'frontFlag', 'frontFlagName', 'backFlag', 'backMoney', 'skuNum']);
            });

            it.skip('买家登录查看售后流程', async function () {

                await spReq.spClientLogin();
                const qlRes = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
                let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0 });
                exp.bill.totalNum = totalNum;
                common.isApproximatelyArrayAssert(exp.flows, qlRes.result.data.rows[0].flows)
            });
            it('卖家审核后平台端退货列表查询', async function () {
                //管理员登录
                this.retries(2);//平台端退货列表查询flag更新慢
                await spAuth.staffLogin();
                await common.delay(1000);
                let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
                // console.log(`卖家审核后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
                expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
                let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
                // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
                common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['flag', 'backFlag', 'backMoney']);
            });
            it('卖家审核后平台端查看退货详情', async function () {
                //管理员登录
                await spAuth.staffLogin();
                let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerTenantId, _cid: clusterCode });
                // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
                let sellerExp = adminReturnInfoExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0, receiveInfo });
                // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
                common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['rem', 'backFlag', 'backMoney', 'skuNum']);
            });
        });
    });

    //代付款的单子是不能进行退款的
    describe('不能退款状态的的单据仅退款', function () {
        before('登录', async function () {
            await spReq.spClientLogin();
        });

        it.skip('未付款的单子退货退款', async function () {
            const purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 1 });   //查询代付款的单子
            let saveParams = mockJsonParam.returnBillJson(purBillList.result.data.rows[0])
            saveParams.check = false;
            saveParams.main.typeId = 0;
            saveParams.main.totalNum = 0;
            saveParams.details.map(res => res.num = 0);
            const res = await spTrade.saveReturnBill(saveParams);
            expect(res.result, '未付款的单据也能退款退货了').to.includes({ "msgId": "return_bill_create_error" });
        });
    });

    describe('订单是否直接退货', function () {
        let param;
        before(async function () {
            //管理员登录
            await spAuth.staffLogin();
            //获取参数：SP订单未发货多久可以直接退款 
            param = await configParam.getParamInfo({ domainKind: 'system', code: 'spBillNotDeliverCanReturnHour' });
            // console.log(`param=${JSON.stringify(param)}`);
        });
        //没有查到未发货的单子就新增一张未发货的单子
        it.skip('新增未发货的单子', async function () {
            await spReq.spSellerLogin();
            let sellerTenantId = LOGINDATA.tenantId;
            await spReq.spClientLogin();
            //获取用户默认收货地址
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId });
            const style = searchDres.result.data.dresStyleResultList.shift();
            const styleRes = await spReq.getFullForBuyerByUrl({
                detailUrl: style.detailUrl,// mockDetailUrl
            });
            //开单
            let purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));//mockStyleRes
            //买家支付
            let payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
        });
        it('时间外的订单可以退货', async function () {
            await spReq.spClientLogin();
            let purBillList = await spTrade.purFindBills({ pageSize: 200, statusType: 2 })
                .then(res => res.result.data.rows.find(obj => (common.stringToDate(common.getCurrentTime()) - common.stringToDate(obj.bill.payTime)) / 3600000 > param.val));
            let billIsReturn = await spTrade.getReturnDirect({ id: purBillList.bill.id });
            // console.log(`billIsReturn=${JSON.stringify(billIsReturn)}`);
            expect(billIsReturn.result.data.val, `x小时前未发货的单据是否可以直接退货退款不为true:\n\t${JSON.stringify(billIsReturn)}`).to.be.true;
        });
    });

    describe.skip('分支流程', function () {
        let clientInfo;
        describe('买家申请退款后,卖家不能进行发货了', function () {
            before('创建退款申请', async function () {
                await spReq.spClientLogin();
                clientInfo = _.cloneDeep(LOGINDATA);
                const purBillList = await spTrade.purFindBills({ pageSize: 100, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
                //为了避免选择已经发生过退款操作的单子进行退款操作
                for (let i = 0; i < purBillList.result.data.rows.length; i++) {
                    if (purBillList.result.data.rows[i].bill.backFlag == 0) {
                        purBillInfo = purBillList.result.data.rows[i];
                        break;
                    };
                };
                let params = mockJsonParam.returnBillJson(purBillInfo);
                params.main.typeId = 0;
                params.main.totalNum = 0;
                params.details.map(res => res.num = 0);
                returnBill = await spTrade.saveReturnBill(params);
            });

            it('卖家尝试发货', async function () {
                await spReq.spSellerLogin();

                let salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 2, searchToken: purBillInfo.bill.billNo });
                let qfRes = await spTrade.salesFindBillFull({ id: salesList.result.data.rows[0].bill.id });
                let details = qfRes.result.data.skus.map((sku) => {
                    return { salesDetailId: sku.id, num: sku.skuNum };
                });
                let logisList = await spconfb.findLogisList1({ cap: 1 });
                logisInfo = logisList.result.data.rows[0];
                let deliverSalesRes = await spTrade.deliverSalesBill({
                    main: {
                        buyerId: clientInfo.unitId,
                        hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                        logisCompId: logisInfo.id,
                        logisCompName: logisInfo.name,
                        waybillNo: common.getRandomNumStr(10)
                    },
                    details: details,
                    check: false
                });
            });
        });
    });

});


/** 
 * 此方法主要用来获取买家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} typeId：需要得到买家哪一步的预期结果 1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  saveDeliverRes:买家发货返回的JSON
*/
function getBuyerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };
    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[5]);
                break;
            case 3:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[4]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '退货完成';
                break;
        };
    } else {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(0)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退款';
                break;
            case 1:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 3:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '交易完成';
                // flows = getBuyerExpJson(params.saveReturnBillRes, 0)[6].concat(getBuyerExpJson(params.saveReturnBillRes, 0)[1]);
                // bill.flag = 4;   //线上7  测试4
                // bill.frontFlag = 4;
                // bill.frontFlagName = '交易完成';
                break;
        };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    const skus = [];
    params.purBillInfo.skus.forEach((sku) => {
        skus.push(format.dataFormat(sku, 'actId;favorMoney;originalPrice;skuId;rem;skuNum;skuPrice;spec1;spec2;spec3;spuCode;spuDocId;spuId;spuTitle;tagId;tagKind'));
    });
    return { bill, flows, skus };

};

/**
 * 获取买家所有状态的JSON
*/
function getBuyerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            // flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }, {
            // flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            // flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            // flowName: '等待买家寄出货品',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            // flowName: '等待卖家确认收货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            // flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }, {
            // flowName: '卖家已退款，3-5天内到账',
            oldFlag: 5,
            newFlag: 6,
            typeId: 5,
        }],
        3: [{
            // flowName: '退款成功',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            // flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            backFlag: 3,// 0 未退货，1 全部退货，2 部分退货 3仅退款
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }, {
            // flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            // flowName: '卖家已同意，退款中',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }, {
            // flowName: '卖家已退款，3-5天内到账',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        3: [{
            // flowName: '退款成功',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/** 
 * 此方法主要用来获取卖家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.saveDeliverRes:买家发货返回的JSON
*/
function getSellerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
        returnReason: params.saveReturnBillRes.params.jsonParam.main.returnReasonName,
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        // totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };

    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getSellerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5]);
                bill.flag = 6;
                bill.frontFlag = 6;
                bill.frontFlagName = '卖家已确认收货';
                break;
            case 3:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5], getSellerExpJson(1)[3]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '卖家已确认收货';
                break;
        };
    } else {
        // switch (params.typeId) {
        //     case 6:
        //         flows = getSellerExpJson(0)[6];
        //         bill.flag = 3;
        //         bill.frontFlag = 3;
        //         bill.frontFlagName = '买家申请退款';
        //         break;
        //     case 3:
        //         flows = getSellerExpJson(0)[6].concat(getSellerExpJson(0)[1], getSellerExpJson(0)[3]);
        //         bill.flag = 7
        //         bill.frontFlag = 7;
        //         bill.frontFlagName = '退款完成';
        //         break;
        // };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, flows, skus: params.purBillInfo.skus };
};

/**
 * 获取卖家所有状态的JSON
*/
function getSellerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            // flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }],
        1: [{
            // flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            // flowName: '等待买家退货',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            // flowName: '买家已发货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            // flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }],
        3: [{
            // flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            // flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }],
        1: [{
            // flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }],
        3: [{
            // flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/**
 * 获取卖家列表的预期结果
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function getSellerListExp(params) {
    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.returnBill.params._tid,
        id: params.returnBill.result.data.val,
        purBillId: params.returnBill.params.jsonParam.main.purBillId,
        salesBillId: params.purBillInfo.bill.salesBillId,
        totalMoney: params.returnBill.params.jsonParam.main.totalMoney,
        totalNum: params.returnBill.params.jsonParam.main.totalNum
    };

    // switch (params.flag) {
    //     case 3:
    //         bill.flag = 3;
    //         bill.frontFlag = 3;
    //         params.returnType == 1 ? bill.frontFlagName = '买家申请退货退款' : bill.frontFlagName = '买家申请退款';
    //         break;
    //     case 4:
    //         bill.flag = 4;
    //         bill.frontFlag = 4;
    //         bill.frontFlagName = '买家退货中';
    //         break;
    //     case 5:
    //         bill.flag = 5;
    //         bill.frontFlag = 5;
    //         bill.frontFlagName = '买家已发货';
    //         break;
    //     case 6:
    //         bill.flag = 6;
    //         bill.frontFlag = 6;
    //         bill.frontFlagName = '卖家已确认收货';
    //     bill.flag = 7;
    //     bill.frontFlag = 7;
    //     params.returnType == 1 ? bill.frontFlagName = '退货完成' : bill.frontFlagName = '退款完成';
    // };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, skus: params.purBillInfo.skus }
};

/**
 * 管理员查看退货详情
 * @param {object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.returnType
 */
function adminReturnInfoExp(params) {
    const mapField = 'billNo;buyerRem;confirmTime;logisCompid;money;payKind;purBillId=id;payTime;proTime;procFlag;salesBillId;shipFeeMoney;totalMoney;totalNum;waybillNo';
    let exp = getSellerExp(params);
    let bill = Object.assign(format.dataFormat(params.purBillInfo.bill, mapField), {
        id: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        favourtotalMoney: common.add(params.purBillInfo.bill.favorMoney, params.purBillInfo.bill.shopCoupsMoney),//优惠总金额
    });

    exp.bill = bill;
    exp.bill.receiveAddress = params.receiveInfo.ecCaption.addrId;
    exp.bill.receiveName = params.receiveInfo.linkMan;
    exp.bill.receivePhone = params.receiveInfo.telephone;
    return exp;
};

/**
 * 获取平台端退货列表查询期望值
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function adminReturnListExp(params) {
    const mapField = 'clusterCode;sellerId=tenantId;sellerName=tenantName;sellerUnitId=unitId';
    let sellerList = getSellerListExp(params);
    let exp = Object.assign(sellerList.skus[0], sellerList.bill, format.dataFormat(params.purBillInfo.trader, mapField));
    exp.typeId = params.returnBill.params.jsonParam.main.typeId;
    exp.rem = params.returnBill.params.jsonParam.main.rem
    exp.proTime = params.returnBill.opTime;
    return exp;
};