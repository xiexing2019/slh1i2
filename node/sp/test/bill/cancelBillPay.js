const common = require('../../../lib/common');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

describe('取消支付', function () {
    this.timeout(TESTCASE.timeout);
    // let mockDetailUrl, mockStyleRes;
    let clientInfo, styleRes;
    before(async function () {
        await spReq.spSellerLogin();
        const sellerTenantId = LOGINDATA.tenantId;

        await spReq.spClientLogin();
        BASICDATA['2009'] = await spugr.getDictList({ typeId: '2009', flag: 1 }).then((res) => res.result.data.rows);
        BASICDATA['2008'] = await spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        clientInfo = _.cloneDeep(LOGINDATA);

        const searchDres = await spdresb.searchDres({ pageSize: 10, pageNo: 1, orderByDesc: true, orderBy: 'marketDate', tenantId: sellerTenantId, queryType: 4 });
        styleRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchDres.result.data.dresStyleResultList.shift().detailUrl });

    });

    describe('买家开单-买家取消支付-online', function () {
        let purRes;
        before(async function () {
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await spTrade.cancelBillPayStatus({ id: purRes.result.data.rows[0].billId });
        });
        it('查询销售单列表', async function () {
            const salesBillList = await spTrade.searchSalesBills({ keyWords: { billNo: [purRes.result.data.rows[0].billNo] } }).then(res => res.result.data.rows);
            const salesBill = salesBillList.find(bill => bill.billNo == purRes.result.data.rows[0].billNo);
            expect(salesBill, `取消支付后,销售单列表找不到销售单 billNo:${purRes.result.data.rows[0].billNo}`).not.to.be.undefined;
            expect(salesBill).to.includes({ payFlag: 0, flag: 3 });
        });
    });

    describe('买家开单-平台取消支付', async function () {
        let purRes;
        before(async function () {
            await spReq.spClientLogin();
            LOGINDATA = clientInfo;
            purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: styleRes }));
            await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });

            await spAuth.staffLogin();
            await spTrade.cancelBillPayStatus({ id: purRes.result.data.rows[0].billId, billNo: purRes.result.data.rows[0].billNo, tenantId: clientInfo.tenantId, unitId: clientInfo.unitId, cancelFrom: 1 });
            await common.delay(500);
        });
        it('查询销售单列表', async function () {
            const salesBillList = await spTrade.searchSalesBills({ keyWords: { billNo: [purRes.result.data.rows[0].billNo] } }).then(res => res.result.data.rows);
            const salesBill = salesBillList.find(bill => bill.billNo == purRes.result.data.rows[0].billNo);
            expect(salesBill, `取消支付后,销售单列表找不到销售单 billNo:${purRes.result.data.rows[0].billNo}`).not.to.be.undefined;
            expect(salesBill).to.includes({ payFlag: 0, flag: 3 });
        });
    });

});