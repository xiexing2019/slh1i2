const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCoupon = require('../../../reqHandler/sp/global/spCoupon');
const spCouponManager = require('../../help/couponManager');
const esSearchHelp = require('../../help/esSearchHelp');

describe('领券中心', async function () {
    this.timeout(30000);

    before(async function () {

    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-1861.html
    // 卡券类型判断 以effectShopId区分，-10是平台券，其余是店铺券
    describe('领券中心排序', async function () {
        before(async function () {
            // await spAuth.staffLogin();
            await spReq.spClientLogin();
        });
        it('可领取卡券列表', async function () {
            await spReq.spSellerLogin();
            const res = await spCoupon.findRecvableCoupons({ shopId: LOGINDATA.tenantId, searchList: 20 });
            // console.log(`res=${JSON.stringify(res)}`);

        });
        describe('领券中心卡券列表-coupType=1(平台券)', async function () {
            let couponList = [];
            before(async function () {
                await spReq.spClientLogin();
                couponList = await spCoupon.findCouponsFromMyCoupons({ coupType: 1 }).then(res => {
                    // console.log(`res=${JSON.stringify(res)}`);
                    return res.result.data.rows;
                });
                if (couponList.length == 0) {
                    console.warn(`无卡券记录`);
                    this.skip();
                }
            });
            it('卡券类型校验', async function () {
                couponList.forEach((coupon) => expect(coupon.effectShopId == -10));
            });
            it('排序校验', async function () {
                esSearchHelp.orderAssert({ dataList: couponList, path: 'execNum', orderByDesc: true });
            });
        });
        describe('领券中心卡券列表-coupType=2(店铺券)', async function () {
            let couponList = [];
            before(async function () {
                couponList = await spCoupon.findCouponsFromMyCoupons({ coupType: 2 }).then(res => {
                    // console.log(`res=${JSON.stringify(res)}`);
                    return res.result.data.rows;
                });
                if (couponList.length == 0) {
                    console.warn(`无卡券记录`);
                    this.skip();
                }
            });
            it('卡券类型校验', async function () {
                couponList.forEach((coupon) => expect(coupon.effectShopId != -10));
            });
            it('排序校验', async function () {
                esSearchHelp.orderAssert({ dataList: couponList, path: 'execNum', orderByDesc: true });
            });
        });

    });


});