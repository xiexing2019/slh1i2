const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCoupon = require('../../../reqHandler/sp/global/spCoupon');
const spCouponManager = require('../../help/couponManager');
// const billManage = require('../../help/billManage');

describe('退款退券', function () {
    this.timeout(30000);
    let mineCoupon;
    const coupon = spCouponManager.setUpCardCoupon();
    before(async function () {
        await spReq.spSellerLogin();
        const sellerInfo = _.cloneDeep(LOGINDATA);

        //管理员登录 新增运费券
        await spAuth.staffLogin();
        await coupon.getRandomJson({ cardType: 3 });
        await coupon.saveCardCoupon();


        await spReq.spClientLogin();
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        // 买家领用卡券
        [mineCoupon] = await spCouponManager.receiveMyCoupons({ coupons: [coupon] });
        // console.log(mineCoupon);

        // 买家下单+支付
        const dresList = await spdresb.searchDres({ tenantId: sellerInfo.tenantId, queryType: 0 })
            .then(res => res.result.data.dresStyleResultList);
        const dresDetail = await spReq.getFullForBuyerByUrl({
            detailUrl: dresList[0].detailUrl,// mockDetailUrl
        });
        const purJson = mockJsonParam.purJson({ styleInfo: dresDetail });
        purJson.platCoupons = {
            platCoupsMoney: mineCoupon.execNum,//平台券抵扣金额
            minePlatCouponsIds: mineCoupon.mineCouponId,
            platCouponsIds: mineCoupon.couponId,
        };
        const purRes = await spTrade.savePurBill(purJson);
        const payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });

        // 买家退款
        // 退货原因（字典类别2005）
        BASICDATA['2005'] = await spugr.getDictList({ typeId: '2005', flag: 1 })
            .then((res) => res.result.data.rows);
        // 仅退货原因
        BASICDATA['2012'] = await spugr.getDictList({ typeId: '2012', flag: 1 })
            .then((res) => res.result.data.rows);
        const returnReason = BASICDATA['2012'][0];
        const purBillList = await spTrade.purFindBills({ orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        const purBillInfo = purBillList.result.data.rows.find(row => row.bill.billNo == purRes.result.data.rows[0].billNo);
        const returnJson = mockJsonParam.returnBillJson(purBillInfo);
        const returnBill = await spTrade.saveReturnBill(returnJson);

        // 买家同意退款
        await spReq.spSellerLogin();
        await spTrade.checkReturnBill({
            id: returnBill.result.data.val,
            checkResult: 1, //同意
        });
    });
    it('我的卡券查询', async function () {
        await spReq.spClientLogin();
        const myCouponsList = await spCoupon.findMyCoupons({ cardType: coupon.cardType, orderBy: 'receiveDate', orderByDesc: true }).then(res => res.result.data.rows);
        const myCoupon = myCouponsList.find(coupon => coupon.id == mineCoupon.mineCouponId);
        expect(myCoupon).not.to.be.undefined;
        expect(myCoupon).to.includes({ flag: 1 });
    });

});