const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spugr = require('../../../reqHandler/sp/global/spugr');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const esSearchHelp = require('../../help/esSearchHelp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');

// queryType=6
// http://zentao.hzdlsoft.com:6082/zentao/task-view-1131.html
// 按类别+最高显示价格的方式，优化今日新款(不是上新推荐频道)展示的商品，
// 某类别下的商品，只有低于该类别对应的最高显示价格，才展示在今日新款这里。
// 否则显示到高端区活动（运营这边会新建一个高端区的活动，常设活动不删除，只是可能不展示）
describe('今日上新', async function () {
    this.timeout(30000);
    let classId;
    before(async function () {
        await spAuth.staffLogin();

        // 添加类目最高价
        const dresClass = await spCommon.findCatConfig({ type: 4 }).then(res => res.result.data[4][0]);
        console.log(`dresClass=${JSON.stringify(dresClass)}`);

        // if (dresClass.hasOwnProperty('prop')) dresClass.prop = JSON.parse(dresClass.prop);
        // dresClass.prop.classHighestPrice = 300;
        classId = dresClass.typeId;

        // await spAdmin.saveSpTopPriceConfig({ confType: 1, classId, topPrice: 300, flag: 1 });

        // await spCommon.saveCatConfig(dresClass);
        // console.log(`\nres=${JSON.stringify(res)}`);  const res =

        await spReq.spSellerLogin();
        const classInfo = await spdresb.findByClass({ classId: classId }).then(res => res.result.data);
        const arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.spuProps.forEach(element => arr.push(element.dictTypeId));
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };

    });

    it.skip('', async function () {
        const dresList = await spdresb.searchDres({ queryType: 6, keyWords: { id: 68314 } });
        console.log(`dresList=${JSON.stringify(dresList)}`);


        const dresList2 = await spdresb.searchDres({ queryType: 6, keyWords: { id: 68318 } });
        console.log(`\ndresList=${JSON.stringify(dresList2)}`);
    });

    describe('新增商品-价格低于类目最高价', async function () {
        let homePageData, spuId;
        before(async function () {
            // 首页数据统计起始值
            homePageData = await spdresb.findHomePageData().then(res => res.result.data);
            console.log(`homePageData=${JSON.stringify(homePageData)}`);

            //保存商品
            const json = mockJsonParam.styleJson({ classId });
            const styleRes = await spdresb.saveFull(json);
            spuId = styleRes.result.data.spuId;
            console.log(spuId);
            await common.delay(500);
        });
        it('全局商品搜索-今日上新', async function () {
            const dresList = await spdresb.searchDres({ queryType: 6, keyWords: { id: spuId } });
            console.log(`dresList=${JSON.stringify(dresList)}`);

        });
        it('首页数据统计', async function () {
            // 首页数据统计起始值
            const homePageData = await spdresb.findHomePageData().then(res => res.result.data);
            console.log(`homePageData=${JSON.stringify(homePageData)}`);
        });
    });
    describe('新增商品-价格高于类目最高价', async function () {
        let homePageData, spuId;
        before(async function () {
            // 首页数据统计起始值
            homePageData = await spdresb.findHomePageData().then(res => res.result.data);
            // console.log(`\n\nhomePageData=${JSON.stringify(homePageData)}`);

            //保存商品
            const json = mockJsonParam.styleJson({ classId });
            json.spu.pubPrice = 400;
            const styleRes = await spdresb.saveFull(json);
            spuId = styleRes.result.data.spuId;
            console.log(`spuId=${spuId}`);
            await common.delay(500);
        });
        it('全局商品搜索-今日上新', async function () {
            const dresList = await spdresb.searchDres({ queryType: 6, keyWords: { id: spuId } }).then(res => res.result.data.dresStyleResultList);
            expect(dresList).have.lengthOf(0);
        });
        it('首页数据统计', async function () {
            // 首页数据统计起始值
            const homePageData2 = await spdresb.findHomePageData().then(res => res.result.data);
            common.isApproximatelyEqualAssert(homePageData, homePageData2);
        });
        // 商品价格高于类目最高价 商品设置为搜索隐藏
        // http://zentao.hzdlsoft.com:6082/zentao/task-view-2818.html
        it('全局商品搜索-普通搜索', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: spuId } }).then(res => res.result.data.dresStyleResultList);
            expect(dresList).have.lengthOf(0);
        });
        it('校验商品隐藏状态', async function () {
            const dresList = await spdresb.findSellerSpuList({ classId, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            const dresDetail = dresList.find(data => data.spuId == spuId);
            expect(dresDetail).to.includes({ flag: 1, hideFlag: 1 });
        });
    });

});