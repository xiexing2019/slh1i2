const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const docManager = require('../../../reqHandler/sp/doc_server/doc');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');


describe('图片相关-online', async function () {
    this.timeout(30000);
    let docContent = [];

    before('搜索商品', async function () {
        await spReq.spClientLogin();
        const dresList = await spdresb.searchDres({ queryType: 0, shadowFlag: 0, pageSize: 5 }).then(res => res.result.data.dresStyleResultList);
        docContent = JSON.parse(dresList[0].docContent);
        // console.log(docContent);
    });

    // 加载较慢输出警告 不报错
    it('加载图片', async function () {
        for (let index = 0; index < docContent.length; index++) {
            const ele = docContent[index];
            if (!ele.docUrl) continue;
            const res = await docManager.getDocByDocUrl({ docUrl: ele.docUrl });
            common.logSlowMsg(res);
            if (ele.docCdnUrl) {
                for (const thumbType in ele.docCdnUrl) {
                    const res2 = await docManager.getDocByDocUrl({ docUrl: ele.docCdnUrl[thumbType] });
                    common.logSlowMsg(res2);
                }
            }
        }
    });

});

