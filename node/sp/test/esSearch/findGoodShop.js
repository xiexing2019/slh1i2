
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAccount = require('../../data/spAccount');
// const caps = require('../../../data/caps');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const format = require('../../../data/format');

describe('好店列表', function () {
    this.timeout(30000);
    let sellerInfo;

    describe('根据主营类目匹配好店', async function () {
        let masterClassId;
        //数据还原
        after('还原卖家主营类目', async function () {
            await spReq.spClientLogin();
            await spCommon.updateBuyerShopMessage({ masterClassId });
        });

        describe('买家设置了主营类目,并且存在此主营类目的门店-online', function () {
            let shopDetail, goodShopList, shopInfo;

            before('获取卖家的主营类目,并且设置买家主营类目', async function () {
                await spReq.spSellerLogin();

                sellerInfo = LOGINDATA;
                await spAuth.staffLogin();
                //管理员设置好店， score >0  即设置好店，score =0  删除好店
                saveShopScore = await spCommon.saveShopScore({ id: sellerInfo.tenantId, score: common.getRandomNum(1, 5) });
                //买家设置主营类目
                await spReq.spSellerLogin();

                shopDetail = await spugr.getShopDetail().then(res => res.result.data);

                masterClassId = shopDetail.masterClassId;
                await spReq.spClientLogin();
                await spCommon.updateBuyerShopMessage({ masterClassId });
            });

            it('列表中查看卖家店铺', async function () {
                goodShopList = await spdresb.getGoodShopByBuyer({ pageSize: 0, pageNo: 1, showSpus: 1 }).then(res => res.result.data.rows);
                shopInfo = goodShopList.find(res => res.tenantId == sellerInfo.tenantId);

                if (shopInfo == undefined) {
                    throw new Error('发现好店列表中没有找到自己的门店');
                } else {
                    await spReq.spClientLogin();
                    await common.delay(2000);
                    //获取好店需要展示的9个商品
                    let exp = await newShopExp(sellerInfo);
                    //买家获取好店列表
                    common.isApproximatelyEqualAssert(shopDetail, shopInfo, ['ver', 'freeType', 'labels', 'labelJsons']);

                    common.isApproximatelyEqualAssert(exp, shopInfo.spus, [], '好店列表中店铺下面的商品和预期的不一致');
                    expect(shopInfo.spus.length).to.be.equal(exp.length);
                };
            });

            //验证好店列表中每个门店下面的商品列表显示的是自己门店的商品
            it('列表中其他店铺spus验证', async function () {
                let shopList = _.cloneDeep(goodShopList);
                _.remove(shopList, val => {
                    return val.tenantId == sellerInfo.tenantId;
                });
                if (shopList.length == 0) return;

                let exp = await newShopExp({ tenantId: shopList[0].tenantId, unitId: shopList[0].unitId });
                common.isApproximatelyEqualAssert(exp, shopList[0].spus, '其他门店下面的spus显示错误');
            });

            it('好店列表中商品列表的唯一性校验', async function () {
                let spus = shopInfo.spus.map(val => {
                    return val.spuId;
                });
                let exp = common.dedupe(spus);
                expect(shopInfo.spus.length, `好店列表下面的商品有重复显示了,${exp}`).to.be.equal(exp.length);
            });

            it('买家查询好店列表', async function () {
                goodShopList.forEach(res => {
                    //这里为了排序做准备，score为0的要排在后面                    
                    if (res.score == 0) {
                        res.score = 99999;
                    };
                    expect(res.masterClassId, `不是同一个主营类目的店铺也显示了，店铺是${res.name},类目是${res.masterClassId}`).to.be.equal(masterClassId);
                    expect(res.flag, `不是有效的店铺也展示在了列表里面，店铺是${res.name}`).to.be.equal(1);
                });
            });

            //好店先在前面，非好店（score）在后面  
            it('当主营类目匹配时，排序', async function () {
                for (let i = 0; i < goodShopList.length - 1; i++) {
                    //前一条记录的score 小于等于后一条的记录
                    expect(goodShopList[i].score, `当前score${goodShopList[i].score},下一个score${goodShopList[i + 1].score}`).to.be.at.most(goodShopList[i + 1].score);
                };
            });

            it('唯一性校验', async function () {
                let ids = goodShopList.map(res => { return res.id });
                let exp = common.dedupe(ids).length;
                expect(goodShopList.length).to.be.equal(exp);
            });
        });

        //买家如果没有主营类目，那么好店数据就是后台设置的好店，排序score顺序
        describe('买家没有设置主营类目，查看好店列表排序', async function () {
            let findGoodShopByAdmin, totalNum, randomGoodShopList;

            before('设置买家没有设置主营类目', async function () {
                await spReq.spClientLogin();
                //masterClassId: 0 表示清空  和开发确认过了  
                await spCommon.updateBuyerShopMessage({ masterClassId: 0 });
            });

            it('管理员查看好店列表排序', async function () {
                await spAuth.staffLogin();
                findGoodShopByAdmin = await spCommon.findSpGoodShops({ pageSize: 50, pageNo: 1 })
                    .then(res => res.result.data.rows);
                for (let i = 0; i < findGoodShopByAdmin.length - 1; i++) {
                    expect(findGoodShopByAdmin[i].score, `当前score:${findGoodShopByAdmin[i].score},下一个score：${findGoodShopByAdmin[i + 1].score}`).to.be.at.most(findGoodShopByAdmin[i + 1].score);
                    expect(findGoodShopByAdmin[i].score, `score为0的门店也显示在了好店列表里面,记录为：${JSON.stringify(findGoodShopByAdmin[i])}`).to.be.above(0);
                };
            });

            it('买家查看好店顺序', async function () {
                await spReq.spClientLogin();
                let goodShopList = await spdresb.getGoodShopByBuyer({ pageSize: 50, pageNo: 1 })
                    .then(res => res.result.data.rows);
                for (let i = 0; i < goodShopList.length - 1; i++) {
                    //前一条记录的score 小于等于后一条的记录
                    expect(goodShopList[i].score, `当前score${goodShopList[i].score},下一个score${goodShopList[i + 1].score}`).to.be.at.most(goodShopList[i + 1].score);
                };
            });

            it('买家端唯一性验证', async function () {
                let findGoodShopByClient = await spdresb.getGoodShopByBuyer({ pageSize: 0, pageNo: 1 })
                    .then(res => res.result.data.rows);
                let ids = findGoodShopByClient.map(res => {
                    return res.tenantId;
                });
                let exp = (common.dedupe(ids)).length;
                expect(findGoodShopByClient.length, `好店列表中有重复的店铺显示了.${findGoodShopByClient.map(res => { return res.tenantId })}`).to.be.equal(exp);
            });

            it.skip('买家端随机填充店铺', async function () {
                totalNum = await spCommon.findSpGoodShops({ pageSize: 0, pageNo: 1 })
                    .then(res => res.result.data.count);

                randomGoodShopList = await spdresb.getGoodShopByBuyer({ pageSize: totalNum + 3, pageNo: 1, random: 1 });
                expect(randomGoodShopList.result.data.rows.length).to.be.equal(randomGoodShopList.params.pageSize);
                randomGoodShopList.result.data.rows.forEach(res => {
                    expect(res.flag, `不是有效的店铺也填充上来了,店铺${res.name},门店状态flag${res.flag}`).to.be.equal(1);
                });
                let ids = randomGoodShopList.result.data.rows.map(res => {
                    return res.tenantId;
                });

                let exp = (common.dedupe(ids)).length;
                expect(randomGoodShopList.result.data.rows.length, `随机填充店铺后,好店列表中有重复的店铺显示了.${randomGoodShopList.result.data.rows.map(res => { return res.tenantId })}`).to.be.equal(exp);
            });

            it.skip('随机填充店铺后，整个列表的排序', async function () {
                let res = await spdresb.getGoodShopByBuyer({ pageSize: totalNum }).then(res => res.result.data.rows);
                randomGoodShopList.result.data.rows.slice(0, totalNum - 1);
                common.isApproximatelyEqualAssert(randomGoodShopList.result.data.rows, res, [], '随机填充店铺时，随机填充的店铺填充到了前面');
            });

            it('查询条件--不显示spus', async function () {
                let res = await spdresb.getGoodShopByBuyer({ pageSize: 10, showSpus: 0 }).then(res => res.result.data.rows);
                expect(res[0]).not.to.property('spus');
            });
        });
    });

    describe('管理员设置好店相关', async function () {
        let shopDetail;
        before('设置好店', async function () {
            await spAuth.staffLogin();
            //管理员设置好店， score >0  即设置好店，score =0  删除好店
            saveShopScore = await spCommon.saveShopScore({ id: sellerInfo.tenantId, score: common.getRandomNum(1, 5) });
            await spReq.spSellerLogin();
            shopDetail = await spugr.getShopDetail();
        });

        it('管理员查看好店', async function () {
            await spAuth.staffLogin();
            let findGoodShopByAdmin = await spCommon.findSpGoodShops({ nameLike: spAccount.seller.shopName });
            let info = findGoodShopByAdmin.result.data.rows.find(res => res.name == spAccount.seller.shopName);
            if (info == undefined) {
                throw new Error('设置了门店为好店，但门店列表未查到此门店!');
            } else {
                shopDetail.result.data.score = saveShopScore.params.jsonParam.score;
                common.isApproximatelyEqualAssert(shopDetail.result.data, info, ['dresShopLabels']);
            };
        });

        it('管理员修改score', async function () {
            let saveRes = await spCommon.saveShopScore({ id: sellerInfo.tenantId, score: saveShopScore.params.jsonParam.score + 5 });
            let res = await spCommon.findSpGoodShops({ nameLike: spAccount.seller.shopName });
            let info = res.result.data.rows.find(res => res.name == spAccount.seller.shopName);
            shopDetail.result.data.score = saveRes.params.jsonParam.score;
            common.isApproximatelyEqualAssert(shopDetail.result.data, info, ['ver', 'dresShopLabels']);
        });


        it('管理员删除好店后管理员查看好店', async function () {
            await spCommon.saveShopScore({ id: sellerInfo.tenantId, score: 0 });
            let goodShopByAdmin = await spCommon.findSpGoodShops({ nameLike: spAccount.seller.shopName });
            let info = goodShopByAdmin.result.data.rows.find(res => res.name == spAccount.seller.shopName);
            expect(info, '删除好店后好店里面里面还存在此店铺').to.be.equal(undefined);
        });
    });

    describe('获取买家城市及推荐城市', function () {
        it('获取买家城市', async function () {
            //买家登录
            await spReq.spClientLogin();
            const shopAudits = await spCommon.findShopAuditsByBuyer().then(res => res.result.data);
            // console.log(`shopAudits=${JSON.stringify(shopAudits)}`);
            //区域化配置
            const areaConfigArr = await spCommon.findCatConfig({ type: 5 }).then(res => res.result.data[5]);
            const areaConfig = areaConfigArr.find(areaInfo => {
                return areaInfo.typeValue == shopAudits.cityCode || areaInfo.relateCityCodes && areaInfo.relateCityCodes.includes(shopAudits.cityCode);
            });
            if (areaConfig) {
                const cityCode = areaConfig.typeValue;

                const cityConfigInfo = await spCommon.findCatConfig({ type: 6 }).then(res => res.result.data[6].find(obj => obj.typeValue == shopAudits.masterClassId && obj.mainCityCode == cityCode));
                expect(cityConfigInfo, '推荐城市查询查不到当前登录买家的相关城市').to.not.be.undefined;

                let exp = [{ [`${cityConfigInfo.mainCityCode}`]: cityConfigInfo.mainCityName }];
                cityConfigInfo.relateCityCodes.forEach((ele, index) => {
                    exp.push({ [`${ele}`]: cityConfigInfo.relateCityNames[index] });
                });
                // console.log(`exp=${JSON.stringify(exp)}`);
                //获取买家城市及推荐城市
                let res = await spdresb.getRecommendCity();
                common.isApproximatelyEqualAssert(exp, res.result.data.rows);
            } else {
                // 默认返回手机号归属地城市
                const cityList = await spdresb.getRecommendCity({ masterClassId: shopAudits.masterClassId }).then(res => res.result.data.rows);
                expect(cityList).to.eql([{ '330100': '杭州市' }]);
            }

        });
    });
});

/**
 * 现在好店列表下面的款号顺序是 今日上新的款号倒序
 * @param {Object}  发现好店列表下面的商品数组
 * @param {Object}   es搜到的今天上新的商品 
*/

async function newShopExp(sellerInfo) {
    let setDres = [];
    //  获取后台设置的商品
    let res = await spCommon.getGoodShopSpus({ sellerUnitId: sellerInfo.unitId }).then(res => res.result.data.rows);
    res.forEach(val => {
        setDres.push(format.dataFormat(val, 'spuId;docId=spuDocId'));
    });
    if (setDres.length >= 15) {
        return setDres;
    } else {
        let dresList = await spdresb.searchDres({ needWait: 1, orderBy: 'slhDate', orderByDesc: true, pageSize: 30, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId })
            .then(res => res.result.data.dresStyleResultList);
        //去除掉已经设置的spu
        setDres.forEach(val => {
            _.remove(dresList, res => {
                return res.id == val.spuId
            });
        });
        let expArr = dresList.slice(0, 15 - setDres.length).map(res => {
            return {
                spuId: res.id,
                code: res.code,
                name: res.name,
                price: res.pubPrice,
                docId: (JSON.parse(res.docHeader))[0].docId,
                detailUrl: `${res.detailUrl}${LOGINDATA.tenantId}`
            };
        });
        return setDres.concat(expArr);
    };
};