const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const esSearchHelp = require('../../help/esSearchHelp');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
// 今日新款/上新--关注/推荐
describe('上新-online', function () {
    this.timeout(TESTCASE.timeout);
    let masterClassId;
    describe('推荐上新', function () {
        let shopDetail;

        describe('上新--推荐上新,买家设置主营类目', function () {
            let dresList, ids = [], sortArr = [];

            before('买家设置和卖家相同的主营类目', async function () {
                await spReq.spSellerLogin();
                sellerTenantId = LOGINDATA.tenantId;
                shopDetail = await spugr.getShopDetail().then(res => res.result.data);
                masterClassId = shopDetail.masterClassId;

                //买家设置和卖家一样的主营类目
                await spReq.spClientLogin();
                await spCommon.updateBuyerShopMessage({ masterClassId });
            });

            it('1.查询推荐上新,非随机商品必须符合买家类目', async function () {
                dresList = await spdresb.searchDres({ pageSize: 30, pageNo: 1, tenantId: sellerTenantId, queryType: 2 }).then(res => res.result.data.dresStyleResultList);

                dresList.forEach(dresInfo => {
                    ids.push(dresInfo.id);
                    if (dresInfo.showCaption == 0) {
                        sortArr.push({ id: dresInfo.id, marketDate: dresInfo.marketDate });
                        expect(dresInfo.masterClassId, `非随机商品的主营类目和买家的主营类目不一致，商品：${dresInfo.name}，主营类目：${dresInfo.masterClassId}`).to.be.equal(masterClassId); //正常搜索出来的主营类目应该和买家的主营类目一致
                    };
                });
            });

            it('2.查询推荐上新，唯一性校验', async function () {
                let exp = common.dedupe(ids);
                expect(ids.length, `推荐上新前50个记录中，有重复的商品，商品列表是${JSON.stringify(ids)}`).to.be.equal(exp.length);
            });

            it.skip('3.非随机商品排序验证', async function () {
                for (let i = 0; i < sortArr.length - 1; i++) {
                    let flag = sortArr[i].marketDate >= sortArr[i + 1].marketDate;
                    expect(flag, `推荐上新类别中，非随机的商品排序错误，当前：${JSON.stringify(sortArr[i])}，下一个${JSON.stringify(sortArr[i + 1])}`).to.be.true;
                };
            });
            //改到在dres验证，节约脚本执行时间
            it.skip('4.新增一个商品,检查推荐上新门店列表', async function () {
                await spReq.spSellerLogin();
                saveDres = await spReq.saveDresFull();
                let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });

                await spReq.spClientLogin();
                let searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 2, needWait: 1 }).then(res => res.result.data.dresStyleResultList);

                let info = searchRes.find(res => res.id == saveDres.result.data.spuId);

                if (info == undefined) {
                    throw new Error('新上架的商品1分钟后，今日上新门店列表里面还没有查询到')
                } else {
                    common.isApproximatelyEqualAssert(dresInfo, info);
                };
            });

            it('5.城市搜索条件', async function () {
                let cityCode = 330100; //杭州市
                //把卖家的市改为要查询的市，保证查询到的列表不为空
                await spReq.spSellerLogin();
                let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
                shopInfo.provCode = 330000;
                shopInfo.cityCode = cityCode;
                shopInfo.areaCode = '';
                await spugr.updateShop(shopInfo);

                await spReq.spClientLogin();
                let searchDresByCity = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 2, "keyWords": { "cityId": [cityCode] } }).then(res => res.result.data.dresStyleResultList);

                searchDresByCity.forEach(res => {
                    if (res.showCaption == 0) {
                        expect(res.tenantAddr.indexOf('杭州市'), `城市搜索出来的非随机店铺和推荐城市不一致${res.tenantAddr}`).to.be.above(-1);
                    };
                });
            });

            it('下架商品查看推荐上新列表', async function () {
                await spReq.spSellerLogin();
                await spdresb.offMarket({ ids: [dresList[0].id] });

                await common.delay(500);
                await spReq.spClientLogin();
                let searchList = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 2, keyWords: { id: [dresList[0].id] } }).then(res => res.result.data.dresStyleResultList);
                expect(searchList, `下线后商品还显示在门店上新列表,商品是${dresList[0].title}`).to.have.lengthOf(0);
            });
        });

        describe('上新--推荐上新,买家主营类目匹配不到卖家', function () {
            before('买家匹配不到相同主营类目的卖家', async function () {
                await spReq.spClientLogin();
                await spCommon.updateBuyerShopMessage({ masterClassId: 0 });
            });

            after('数据还原', async function () {
                await spCommon.updateBuyerShopMessage({ masterClassId });
            });

            it('查询', async function () {
                let dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 2 }).then(res => res.result.data.dresStyleResultList);
                expect(dresList.length, '当买家主营类目匹配不到卖家时，列表里没有展示商品').to.be.above(0);
            });
        });
    });

    describe('关注上新', function () {
        //这里是取了关注上新列表中的前50个记录进行测试验证的，由于数据过大，所以无法对所有的记录进行验证
        let sellerLoginData, favorShopDres;
        before('关注上新查询', async function () {
            await spReq.spSellerLogin();
            sellerLoginData = LOGINDATA;

            await spReq.spClientLogin();
            //尝试关注卖家的店铺，确保买家关注列表里面有数据
            await spUp.addFavorShop({
                shopId: sellerLoginData.tenantId,
                shopName: sellerLoginData.shopName,
                check: false
            });
            //queryType=1 关注上新，显示关注的门店的所有的商品，默认按时间倒序排。
            await common.delay(500);
            favorShopDres = await spdresb.searchDres({ pageSize: 50, pageNo: 1, queryType: 1 }).then(res => res.result.data.dresStyleResultList);
        });

        it('显示的门店都应该是已关注的', async function () {
            favorShopDres.forEach(res => {
                expect(res.favorFlag, '没关注的店铺也显示了').to.be.equal(1);
            });
        });

        it('唯一性校验', async function () {
            let dresList = await spdresb.searchDres({ pageSize: 60, pageNo: 1, queryType: 1 }).then(res => res.result.data.dresStyleResultList);
            let arr = dresList.map(res => res.id);
            let exp = common.dedupe(arr).length;
            expect(arr.length, '商品重复显示了').to.be.equal(exp);
        });

        //顺序是按照时间的倒序进行排的,现在服务端不返回marketDate字段了
        it.skip('顺序验证', async function () {
            esSearchHelp.orderAssert({ dataList: favorShopDres, path: 'marketDate', orderByDesc: false });
        });

        //移除到dres,节约执行时间
        it.skip('新上架一个商品，查看关注门店上新列表', async function () {
            await spReq.spSellerLogin();
            saveDres = await spReq.saveDresFull();
            let dresInfo = await spdresb.getFullById({ id: saveDres.result.data.spuId });

            await spReq.spClientLogin();
            let dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 1, needWait: 1 }).then(res => res.result.data.dresStyleResultList);


            let info = dresList.find(res => res.id == saveDres.result.data.spuId);
            if (info == undefined) {
                throw new Error(`新上线的货品等待1分钟后查询，商品没有显示到关注上新列表里,上架的商品是${saveDres.result.data.spuId},名称${saveDres.params.jsonParam.spu.name}`)
            } else {
                common.isApproximatelyEqualAssert(dresInfo, info);
            };
        });

        it('下架商品查看门店上新列表', async function () {
            await spReq.spSellerLogin();
            await spdresb.offMarket({ ids: [favorShopDres[0].id] });
            await common.delay(200);
            await spReq.spClientLogin();
            let dresList = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 1, keyWords: { id: [favorShopDres[0].id] } }).then(res => res.result.data.dresStyleResultList);
            expect(dresList.length, `下线后商品还显示在门店上新列表${favorShopDres[0].title}`).to.be.equal(0);
        });
    });

    describe('今日新款（不掺杂热度）', async function () {
        let searchRes, saveDresRes;

        before('今日新款', async function () {
            await spReq.spSellerLogin();
            saveDresRes = await spReq.saveDresFull();

            await spReq.spClientLogin();
            searchRes = await spdresb.searchDres({ pageSize: 300, pageNo: 1, queryType: 6 }).then(res => res.result.data.dresStyleResultList);
        });

        //现在会打乱顺序的
        it.skip('排序验证', async function () {
            esSearchHelp.sortAssert(searchRes, 'marketDate', true);
        });

        it('唯一性验证', async function () {
            let ids = searchRes.map(res => res.id);
            let exp = common.dedupe(ids);
            expect(searchRes.length, `今日新款有重复显示了${JSON.stringify(ids)}`).to.be.equal(exp.length);
        });

        it('新增一个商品，检查今日新款列表', async function () {
            this.retries(2);
            await spReq.spClientLogin();
            await common.delay(1000);
            let res = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 6, keyWords: { id: [saveDresRes.result.data.spuId] } }).then(res => res.result.data.dresStyleResultList);
            let hasDres = res.find(val => val.id == saveDresRes.result.data.spuId);
            expect(hasDres, '店铺上架了一个新的商品，但是今日新款列表没有找到这个款,有可能是同步延迟造成的').to.not.be.undefined;
        });

        it('列表里面的每一个款号都应该是今日的', async function () {
            let exp = common.getCurrentDate();
            searchRes.forEach(res => {
                expect(res.marketDate.substring(0, 10), `不是今天的商品也显示在了今日新款列表里：商品：${res.name}, 日期：${res.marketDate}`).to.be.equal(exp);
            });
        });
    });
});






/**
 * 获取关注上新的前50个记录的预期结果
 * 
 */
async function newDresExp() {
    // 获取买家已关注店铺 isOnline 0 下线 1 上线

    lists = _.flatten(lists);

    let favorShopIds = [];
    await spUp.getFavorShopList().then(res => res.result.data.rows.forEach(shopInfo => shopInfo.isOnline == 1 && favorShopIds.push(shopInfo.shopId)));

    const promises = favorShopIds.map((id) => spdresb.searchDres({ pageSize: 50, pageNo: 1, tenantId: id, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows));
    let lists = await Promise.all(promises);

    // let newDresArr = [];
    // //这里是用es搜索买家关注的所有卖家的商品（时间倒序排）  各查50个，然后放到一个数组，再数组按照时间倒序进行排序 ，最前面的50条既为预期结果
    // for (let i = 0; i < favorList.length; i++) {
    //     let res = await spdresb.searchDres({ pageSize: 50, pageNo: 1, tenantId: favorList[i].shopId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.dresStyleResultList);
    //     newDresArr = res.concat(newDresArr);
    //     await common.delay(100);
    // };

    // //按照时间倒序进行排序，构造预期结果 
    // newDresArr.sort(function (obj1, obj2) {
    //     let val1 = common.stringToDate(obj1.marketDate);
    //     let val2 = common.stringToDate(obj2.marketDate);;
    //     return val2 - val1;
    // });
    // //这里截取数组的前50个
    // return newDresArr.slice(0, 50);
};



/**
 * 查看两次es搜索的数组是否包含相同的商品
 * @returns true:两个数组不包括相同的元素  false：两个数组中有相同的元素
*/
function hasSameDres(obj1, obj2) {
    let idsArr1 = obj1.map(res => {
        return res.id;
    });

    let idsArr2 = obj2.map(res => {
        return res.id;
    });
    let idsArr = idsArr1.concat(idsArr2);   //两个数组连接起来
    // console.log(`idsArr = ${ JSON.stringify(idsArr) }`);

    if ((common.dedupe(idsArr)).length == common.add(idsArr1.length, idsArr2.length)) {
        return true;
    } else {
        return false;
    };
};
