const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAccount = require('../../data/spAccount');

describe('店铺相关搜索', async function () {
    this.timeout(30000);

    // 数据维护脚本，在上线店铺2时，会设置为免租店铺
    describe('免租商户搜索', async function () {
        let shopName = spAccount.seller2.shopName, sellerTenantId;
        before(async function () {
            await spReq.spSellerLogin({ shopName });
            sellerTenantId = LOGINDATA.tenantId;

            await spReq.spClientLogin();
        });
        it('es商品搜索', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, shadowFlag: 2, tenantId: sellerTenantId })
                .then(res => res.result.data.dresStyleResultList);
            expect(dresList).to.have.length.above(0);
        });
        it('混合搜索名称+手机号+档口查询店铺接口(买家游客)', async function () {
            const shopList = await spugr.findShopBySearchToken({ searchToken: shopName }).then(res => res.result.data.rows);
            const shop = shopList.find(data => data.name == shopName);
            expect(shop).not.to.be.undefined;
        });
    });

});