const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const dresManage = require('../../help/dresManage');
const mockJsonParam = require('../../help/mockJsonParam');
const configParam = require('../../help/configParamManager');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');

/**
 *  http://zentao.hzdlsoft.com:6082/zentao/task-view-1420.html
 * 
 *  1）搜索隐藏，该状态下，好店APP买家端将不能够搜索到该商品，但店铺、活动等页面还是能够看到该商品。
 *  购物车 spuFlag=1  收藏列表 hideFlag=1 validFlag=1
 *  2）买家隐藏-店铺不可见，该状态下，好店APP、小程序等不再显示该商品，效果同商品下架，但不同步下架信息给卖家，该商品在卖家端还是显示为上架状态
 *  购物车 spuFlag=-4  收藏列表 hideFlag=2 validFlag=0
 *  3）买家隐藏-店铺可见，其店铺内可见，进货车内不失效，我的收藏，我的足迹可见，非下架
 *  购物车 spuFlag=1  收藏列表 hideFlag=3 validFlag=1
 * 
 *  我的分享不受影响
 * 
 *  测试点
 * 【1】针对商品隐藏增加控制选项；
 * 【2】选择搜索隐藏，该状态下，好店APP买家端将不能够搜索到该商品，但店铺、活动等页面还是能够看到该商品。，切换到买家隐藏，再次查询校验；
 * 【3】买家隐藏，该状态下，好店APP、小程序等不再显示该商品，效果同商品下架，但不同步下架信息给卖家，
 * 
 */
describe('隐藏商品', async function () {
    this.timeout(30000);
    const dres = dresManage.setupDres();
    let sellerInfo, spuHideFlag;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        spuHideFlag = await configParam.getParamInfo({ domainKind: 'system', code: 'sp_spu_hide_flag' });
        await spuHideFlag.updateParam({ val: 1 });

        const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList);
        dres.setByEs(dresList[0]);
        console.log(`隐藏商品 spuId=${dres.id}`);
    });
    after(async function () {
        await spReq.spClientLogin();
        await spTrade.emptyCart();

        await spAuth.staffLogin();
        await spuHideFlag.returnOriginalParam();
    });

    it('数据准备', async function () {
        await spReq.spClientLogin();
        await spTrade.emptyCart();
        // 添加商品至购物车
        const dresDetailRes = await dres.getDetailForBuyer();
        dresDetailRes.count = 2;
        const cartJson = mockJsonParam.cartJson(dresDetailRes);
        cartJson.trader.traderName = sellerInfo.shopName;
        const res = await spTrade.saveCartInBatchs(cartJson);
        // console.log(`res=${JSON.stringify(res)}`);

        // 收藏商品
        const addFavorRes = await spUp.addFavorSpu({
            spuId: dres.id,
            spuTitle: dres.spu.title,
            shopId: sellerInfo.shopId,
            shopName: sellerInfo.shopName,
            rem: `备注${common.getRandomStr(5)}`,
            check: false,
        });
        // console.log(`addFavorRes=${JSON.stringify(addFavorRes)}`);
    });

    describe('商品隐藏状态-1搜索隐藏', async function () {
        before(async function () {
            await dres.hideSpuShow({ typeId: 1 });
            await common.delay(1000);
        });
        it('全局商品搜索queryType=0', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `隐藏商品后 全局商品搜索可以查询到商品 spuId=${dres.id}`).to.be.undefined;
        });
        it('全局商品搜索queryType=4', async function () {
            const dresDetail = await dres.searchDresById({ queryType: 4 });
            expect(dresDetail).to.includes({ hideFlag: 1 });
        });
        it('卖家查询商品列表', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await dres.findSellerSpuList();
            expect(dresDetail).to.includes({ flag: dres.spu.flag, hideFlag: dres.spu.hideFlag });
        });
        it('买家查询购物车列表-隐藏商品状态校验', async function () {
            await spReq.spClientLogin();
            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            cartList.filter(data => data.trader.tenantId == sellerInfo.tenantId)
                .forEach(data => data.carts.forEach(cart => cart.spuId == dres.id && expect(cart).to.includes({ spuFlag: 1 })));
        });
        it('买家查询商品收藏列表-隐藏商品状态校验', async function () {
            const favorDresStyleResultList = await spdresb.getMyFavorDres({ searchToken: dres.spu.title }).then(res => res.result.data.favorDresStyleResultList);
            const favorDres = favorDresStyleResultList.find(data => data.id == dres.id);
            expect(favorDres, `收藏列表未查询到商品 spuId=${dres.id}`).not.to.be.undefined;
            expect(favorDres).to.includes({ hideFlag: 1, validFlag: 1 });
        });
    });

    describe('商品隐藏状态-2买家隐藏-店铺内不可见', async function () {
        before(async function () {
            await spReq.spClientLogin();
            await dres.hideSpuShow({ typeId: 2 });
            await common.delay(1000);

            await spAuth.staffLogin();
        });
        it('全局商品搜索queryType=0', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `隐藏商品后 全局商品搜索可以查询到商品 spuId=${dres.id}`).to.be.undefined;
        });
        it('全局商品搜索queryType=4', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `隐藏商品后 全局商品搜索可以查询到商品 spuId=${dres.id}`).to.be.undefined;
        });
        it('卖家查询商品列表', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await dres.findSellerSpuList();
            expect(dresDetail).to.includes({ flag: dres.spu.flag, hideFlag: dres.spu.hideFlag });
        });
        it('买家查询购物车列表-隐藏商品状态校验', async function () {
            await spReq.spClientLogin();
            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            cartList.filter(data => data.trader.tenantId == sellerInfo.tenantId)
                .forEach(data => data.carts.forEach(cart => cart.spuId == dres.id && expect(cart).to.includes({ spuFlag: -4 })));
        });
        it('买家查询商品收藏列表-隐藏商品状态校验', async function () {
            const favorDresStyleResultList = await spdresb.getMyFavorDres({ searchToken: dres.spu.title }).then(res => res.result.data.favorDresStyleResultList);
            const favorDres = favorDresStyleResultList.find(data => data.id == dres.id);
            expect(favorDres, `收藏列表未查询到商品 spuId=${dres.id}`).not.to.be.undefined;
            expect(favorDres).to.includes({ hideFlag: 2, validFlag: 0 });
        });
        it('买家查询店铺商品列表', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `店铺内查询到hideFlag=2的商品 spuId=${dres.id}`).to.be.undefined;
        });
    });

    describe('商品隐藏状态-3买家隐藏-店铺内可见', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await dres.hideSpuShow({ typeId: 3 });
            await common.delay(1000);
        });
        it('全局商品搜索queryType=0', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `隐藏商品后 全局商品搜索可以查询到商品 spuId=${dres.id}`).to.be.undefined;
        });
        it('全局商品搜索queryType=4', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `隐藏商品后 全局商品搜索可以查询到商品 spuId=${dres.id}`).to.be.undefined;
        });
        it('卖家查询商品列表', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await dres.findSellerSpuList();
            expect(dresDetail).to.includes({ flag: dres.spu.flag, hideFlag: dres.spu.hideFlag });
        });
        it('买家查询购物车列表-隐藏商品状态校验', async function () {
            await spReq.spClientLogin();
            const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
            cartList.filter(data => data.trader.tenantId == sellerInfo.tenantId)
                .forEach(data => data.carts.forEach(cart => cart.spuId == dres.id && expect(cart).to.includes({ spuFlag: 1 })));
        });
        it('买家查询商品收藏列表-隐藏商品状态校验', async function () {
            const favorDresStyleResultList = await spdresb.getMyFavorDres({ searchToken: dres.spu.title }).then(res => res.result.data.favorDresStyleResultList);
            const favorDres = favorDresStyleResultList.find(data => data.id == dres.id);
            expect(favorDres, `收藏列表未查询到商品 spuId=${dres.id}`).not.to.be.undefined;
            expect(favorDres).to.includes({ hideFlag: 3, validFlag: 1 });
        });
        it('买家查询店铺商品列表', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, keyWords: { id: [dres.id] } })
                .then(res => res.result.data.dresStyleResultList);
            const dresSpu = dresList.find(data => data.id == dres.id);
            expect(dresSpu, `店铺内查询不到hideFlag=3的商品 spuId=${dres.id}`).not.to.be.undefined;
        });
    });

    describe('商品隐藏状态-0无隐藏', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await dres.hideSpuShow({ typeId: 0 });
            await common.delay(500);
        });
        it('全局商品搜索queryType=0', async function () {
            this.retries(2);
            await common.delay(500);
            const dresDetail = await dres.searchDresById({ queryType: 0 });
            expect(dresDetail).to.includes({ hideFlag: 0 });
        });
        it('全局商品搜索queryType=4', async function () {
            this.retries(2);
            await common.delay(200);
            const dresDetail = await dres.searchDresById({ queryType: 4 });
            expect(dresDetail).to.includes({ hideFlag: 0 });
        });
        it('卖家查询商品列表', async function () {
            await spReq.spSellerLogin();
            const dresDetail = await dres.findSellerSpuList();
            expect(dresDetail).to.includes({ flag: dres.spu.flag, hideFlag: dres.spu.hideFlag });
        });
    });

    describe('批量设置隐藏商品', async function () {

    });

    // 自测一次 不日常测试  商品数量级大时，还原隐藏状态很慢
    describe.skip('隐藏店铺内所有商品', async function () {
        before(async function () {
            await spAuth.staffLogin();
            const res = await spdresb.hideAllDresSpu({ id: sellerInfo.tenantId, hideFlag: 0 });
            console.log(res);

            // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
        });
        it('', async function () {

        });
    });

});

