const spSql = require('../../../reqHandler/sp/mysql');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

describe('商品热度表', async function () {
    this.timeout(30000);
    let sellerInfo;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });
    it('下架后,商品热度表校验', async function () {
        const spgSql = await spSql({ dbName: 'spgMysql' });
        const [dresList] = await spgSql.query(`select * from spbig.dres_fervor_pool where unit_id=${sellerInfo.unitId}`);
        const dres = dresList.shift();
        if (!dres) {
            await spgSql.end();
            console.log(`dres_fervor_pool中未找到unit_id=${sellerInfo.unitId}的数据，请检查`);
            this.skip();
        }

        await spAuth.staffLogin();
        await spCommon.offMarket({
            offmarketReason: `spuId:${dres.id},商品信息有误`,
            spus: [{
                spuId: dres.id,
                tenantId: sellerInfo.tenantId,
                unitId: sellerInfo.unitId,
                clusterCode: sellerInfo.clusterCode
            }]
        });
        await common.delay(1000);

        const [dresList2] = await spgSql.query(`select * from spbig.dres_fervor_pool where unit_id=${sellerInfo.unitId} and id=${dres.id}`);
        spgSql.end();

        expect(dresList2, `${JSON.stringify(dresList2)} \n商品下架后,商品热度表中依然显示下架商品`).to.have.lengthOf(0);
    });
});