const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const configParamManager = require('../../help/configParamManager');
const mysql = require('../../../reqHandler/sp/mysql');

// http://zentao.hzdlsoft.com:6082/zentao/task-view-1852.html
// 批量同步商陆花销量与虚拟销量 校验库存变化
// 2.1.12 添加15天销量 30天销量
describe('销量同步', async function () {
    this.timeout(30000);
    let spSalesRanking, sellerInfo, spuIds, params, dresStat;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 系统参数 销量排序
        spSalesRanking = await configParamManager.getParamInfo({ code: 'sp_sales_ranking', domainKind: 'system' });
        // console.log(spSalesRanking);
        await spSalesRanking.updateParam({ val: 0 });

        spuIds = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, pageSize: 3, pageNo: 1 })
            .then(res => res.result.data.dresStyleResultList.map(dres => dres.id));
        // console.log(spuIds);

        // 同步销量
        params = [{ tenantId: sellerInfo.tenantId, dresSpuSalesDTOS: spuIds.map(spuId => { return { spuId, slhSalesNums: common.getRandomNum(200, 300), virtualSalesNum: common.getRandomNum(200, 300), slhSalesNums15: common.getRandomNum(200, 300), slhSalesNums30: common.getRandomNum(200, 300) } }) }];
        // console.log(`params=${JSON.stringify(params)}`);
        await spugr.batchUpdateSaleNum(params);
        // console.log(res); const res =
        await common.delay(500);

        const spbSql = await mysql({ dbName: 'spbMysql' });
        dresStat = await spbSql.query(`SELECT spu_id as spuId,virtual_sales_num as virtualSalesNum FROM spdresb001.dres_stat WHERE spu_id IN(${spuIds.toString()})`).then(res => res[0]);
        await spbSql.end();

    });
    after(async function () {
        await spSalesRanking.returnOriginalParam();
    });
    it('查询商品信息', async function () {
        const dresList = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, keyWords: { id: spuIds } })
            .then(res => res.result.data.dresStyleResultList);
        // console.log(`dresList=${JSON.stringify(dresList)}`);

        const expList = params[0].dresSpuSalesDTOS;
        for (let index = 0; index < dresList.length; index++) {
            const dres = dresList[index];
            const dresFull = await spReq.getFullForBuyerByUrl({ detailUrl: dres.detailUrl }).then(res => res.result.data);
            // console.log(`\n\ndresFull=${JSON.stringify(dresFull)}`);

            const exp = expList[index];
            expect(dresFull.spu).to.includes({ spuId: exp.spuId, slhSalesNums: exp.slhSalesNums, slhSalesNums15: exp.slhSalesNums15, slhSalesNums30: exp.slhSalesNums30 });
        }
    });

    it('虚拟销量校验', async function () {
        const expList = params[0].dresSpuSalesDTOS;
        expList.forEach(exp => expect(Number(dresStat.find(dres => dres.spuId == exp.spuId).virtualSalesNum)).to.equal(exp.virtualSalesNum));
    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2700.html
    describe('商陆好店详情列表展示方式', async function () {
        let spDresSpuListShowType;
        before(async function () {
            await spAuth.staffLogin();
            spDresSpuListShowType = await configParamManager.getParamInfo({ code: 'sp_dres_spu_list_show_type', domainKind: 'system' });
        });
        after(async function () {
            await spAuth.staffLogin();
            await spDresSpuListShowType.returnOriginalParam();
        });
        it('买家查询店铺详情-线下销量', async function () {
            await spDresSpuListShowType.updateParam({ val: 0 });

            await spReq.spClientLogin();
            const shopDetail = await spugr.getShopSpb({ id: sellerInfo.tenantId }).then(res => res.result.data);
            expect(shopDetail.dresBi).not.to.have.property('sellNumNew');
        });
        it('买家查询店铺详情-线上销量', async function () {
            await spAuth.staffLogin();
            await spDresSpuListShowType.updateParam({ val: 1 });

            await spReq.spClientLogin();
            const shopDetail = await spugr.getShopSpb({ id: sellerInfo.tenantId }).then(res => res.result.data);
            expect(shopDetail.dresBi).to.have.property('sellNumNew');
        });
    });

});