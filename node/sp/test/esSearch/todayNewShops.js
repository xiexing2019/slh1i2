const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spugr = require('../../../reqHandler/sp/global/spugr');
//const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAccount = require('../../data/spAccount');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const esSearchHelp = require('../../help/esSearchHelp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
/**
 * 今日上新门店
 */
describe('今日上新门店', function () {
    this.timeout(TESTCASE.timeout);
    // before(async function () {
    //     await spReq.spClientLogin();
    // });

    //需求已改 已去除主营类目和价格区间查询
    describe.skip('好店列表(买家游客)', function () {
        let masterClassIds;
        before(async function () {
            // 获取主营类目id 取部分
            masterClassIds = await spugr.getDictList({ typeId: '2010', flag: 1 })
                .then((res) => common.randomSort(res.result.data.rows.map(val => val.codeValue)).slice(0, 3));
        });
        it('主营类目搜索', async function () {
            const res = await spdresb.getGoodShopByBuyer({ pageSize: 50, pageNo: 1, showSpus: 0, masterClassId: masterClassIds.join(','), random: 1 });
            // const masterClassIdList=
            const extraIdx = _.findIndex(res.result.data.rows, val => !masterClassIds.includes(val.masterClassId));
            if (extraIdx > -1 && extraIdx < res.result.data.rows.length) {
                const index = _.findIndex(res.result.data.rows, val => masterClassIds.includes(val.masterClassId), extraIdx);
                if (index > -1) throw new Error(`主营类目排序错误:\n   ${JSON.stringify(res.params)}\n   主营类目查询结果:${res.result.data.rows.map(val => val.masterClassId)}`);
            };
        });
    });

    //这里指的不是新开通的门店，而是今天上架过新商品的门店
    describe('今日上新门店列表', function () {

        describe('门店上架新商品后查看上新门店列表-online', function () {
            let sellerTenantId, idsArr = [], todayNewShopList;

            before('新增商品', async function () {

                await spReq.spSellerLogin();
                sellerTenantId = LOGINDATA.tenantId;
                styleRes = await spReq.saveDresFull();
            });

            it('showSpus=1时，门店都应该spus', async function () {
                await spReq.spClientLogin();
                todayNewShopList = await spdresb.getTodayNewShop({ pageSize: 30, pageNo: 1, showSpus: 1 }).then(res => res.result.data.rows);
                todayNewShopList.forEach(res => {
                    idsArr.push(res.id);
                    expect(res.spus.length, `${res.name}门店下面没有展示商品列表`).to.be.above(0);
                });
            });

            /**
             * 实现逻辑： 查询上新门店列表，然后取每一个门店的下面款号列表第一个款号进行es搜索，从而获得各个门店最新的
             * 货品上新时间，然后结合shadowFlag进行顺序验证,
             * 'name' of undefined  完善
             * */
            it.skip('今日上新门店列表顺序验证', async function () {
                let shopArr = [];
                //查询好店列表，结果放到shopArr
                await common.delay(500);
                await spdresb.getTodayNewShop({ pageSize: 10, pageNo: 1, showSpus: 1 }).then(res => res.result.data.rows.forEach(shopInfo => shopArr.push({ tenantId: shopInfo.tenantId, shadowFlag: shopInfo.shadowFlag, name: shopInfo.spus[0].name })));
                //用今日上新门店列表的前10个门店中商品列表的第一个商品进行es搜索，为了取到marketDate
                let promise = shopArr.map((res) => spdresb.searchDres({ startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), pageSize: 10, pageNo: 1, orderBy: 'marketDate', orderByDesc: false, tenantId: res.tenantId, queryType: 4 }).then(element => {
                    return { tenantId: element.result.data.dresStyleResultList[0].tenantId, shadowFlag: res.shadowFlag, marketDate: element.result.data.dresStyleResultList[0].marketDate, tenantName: element.result.data.dresStyleResultList[0].tenantName }
                }));
                let list = await Promise.all(promise);

                //这里是把shadowFlag 和market拼接一下，为了同时验证时间和shadowFlag字段的排序
                list.map(res => {
                    //shadowFlag 0变为1,1变为0  这样验证的时候就能体现0的排序在1的前面
                    if (res.shadowFlag == 0) {
                        res.shadowFlag = 9999;
                    };
                });
                //marketDate进行排序。
                for (let index = 0; index < list.length - 1; index++) {
                    const flag = list[index].marketDate >= list[index + 1].marketDate && list[index].shadowFlag >= list[index + 1].shadowFlag;
                    if (!flag) {
                        throw new Error(`今日上新门店顺序错误:\n   current:{ id:${list[index].tenantName},marketDate:${list[index].marketDate} }\n   next:{ id:${list[index + 1].tenantName},marketDate:${list[index + 1].marketDate} }`);
                    };
                };
            });

            it('今日上新门店唯一性校验', async function () {
                let exp = common.dedupe(idsArr).length;
                expect(todayNewShopList.length, `今日上新门店列表界面有重复的店铺店铺列表为：${JSON.stringify(idsArr)}`).to.be.equal(exp);
            });

            //后期还要改，目前是验证了列表中的前9个款号   优化
            it('门店存在新品后查看今日上新门店列表--显示商品', async function () {
                await spReq.spSellerLogin();
                let shopInfo = await spugr.getShopDetail();

                await spReq.spClientLogin();
                let newShopList = await spdresb.getTodayNewShop({ nameLike: spAccount.seller.shopName, showSpus: 1 });
                let newShopInfo = newShopList.result.data.rows.find(res => res.name == spAccount.seller.shopName);

                if (newShopInfo == undefined) {
                    throw new Error('门店有上新商品，但是今日上新门店列表里面没有这个门店');
                } else {
                    common.isApproximatelyEqualAssert(shopInfo.result.data, newShopInfo);
                    await common.delay(500);
                    let dresList = await spdresb.searchDres({ needWait: 1, pageSize: 100, pageNo: 1, queryType: 4, tenantId: sellerTenantId, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() })
                        .then(res => res.result.data.dresStyleResultList);
                    newShopExp(newShopInfo.spus, dresList);
                };
            });


            it('门店存在新品后查看今日上新门店列表--不显示商品', async function () {
                let newShopList = await spdresb.getTodayNewShop({ nameLike: spAccount.seller.shopName, showSpus: 0 });
                let newShopInfo = newShopList.result.data.rows[0];
                let flag = newShopInfo.hasOwnProperty('spus');
                expect(flag, '不展示款号的情况下也展示了款号').not.to.be.equal(true);
            });

            it('商品下架之后在查看今日门店列表', async function () {
                await spReq.spSellerLogin();
                await spdresb.offMarket({ ids: [styleRes.result.data.spuId] });
                await spReq.spClientLogin();
                await common.delay(500);
                let newShopList = await spdresb.getTodayNewShop({ nameLike: spAccount.seller.shopName, showSpus: 1 });
                let newShopInfo = newShopList.result.data.rows.find(res => res.name == spAccount.seller.shopName);
                expect(newShopInfo.spus.every(res => res.spuId != styleRes.result.data.spuId), '商品下架了，还显示在今日上新门店列表下面').to.be.true;
            });
        });

        describe('首页统计数据--online', async function () {
            let homePageData, backgroundPic;
            before('登录', async function () {
                await spReq.spClientLogin();
                // 首页数据统计
                homePageData = await spdresb.findHomePageData({ productVersion: '2.0.0' });
                // 背景图片查询
                backgroundPic = await spCommon.getBackGroundPhotoList({ productVersion: '2.0.0' });
            });

            it('今日上新门店验证', async function () {
                const newShopList = await spdresb.getTodayNewShop({ pageSize: 10, pageNo: 1, showSpus: 1 }).then(res => res.result.data);
                const exp = { newShopNum: newShopList.count };
                const newShopInfo = backgroundPic.result.data.rows.find(res => res.type == 1);
                if (newShopInfo) exp.newShopDocId = newShopInfo.url;
                common.isApproximatelyEqualAssert(exp, homePageData.result.data, ['newShopDocId']);
            });

            it('今日新款验证', async function () {
                const newSpuList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 6 }).then(res => res.result.data);
                const exp = { newSpuNum: newSpuList.count };
                const newSpuInfo = backgroundPic.result.data.rows.find(res => res.type == 3);
                if (newSpuInfo) exp.newSpuDocId = newSpuInfo.url;
                common.isApproximatelyEqualAssert(exp, homePageData.result.data, ['newShopDocId', 'newSpuDocId']);
            });

            it.skip('爆款验证', async function () {
                let hotDresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 3 });
                let hotSpuNum = hotDresList.result.data.count;
                let hotDresInfo = backgroundPic.result.data.rows.find(res => res.type == 2);
                common.isApproximatelyEqualAssert({ hotSpuNum, hotSpuDocId: hotDresInfo.url }, homePageData.result.data)
            });

            it('包邮区验证', async function () {
                const shippingList = await spdresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 7 }).then(res => res.result.data);
                const exp = { shippingNum: shippingList.count };
                const shipInfo = backgroundPic.result.data.rows.find(res => res.type == 4);
                if (shipInfo) exp.shippingDocId = shipInfo.url;
                common.isApproximatelyEqualAssert(exp, homePageData.result.data, ['shippingDocId']);
            });
        });

        describe.skip('首页背景图片管理', function () {
            it('获取首页图片', async function () {
                const res = await spCommon.getBackGroundPhotoList({ pageSize: 20, pageNo: 1 });
            });
            it('', async function () {
                const res = await spCommon.saveBackGroundPhoto({ name: 'newSpuDocId', url: 'doc35376690', type: 1 });
                // console.log(`res=${JSON.stringify(res)}`);
            });
            it('修改', async function () {
                await spAuth.staffLogin();
                //{"createdBy":1,"createdDate":"2018-09-26 11:10:45","flag":1,"id":13,"name":"newShopDocId","remark":"","type":1,"updatedBy":1,
                // "updatedDate":"2018-09-27 12:06:12","url":"docx15371762691825-spdoc01.png"}
                const res = await spCommon.updateBackGroundPhoto({ name: 'newShopDocId', url: 'docx15371762691825-spdoc01.png', type: 4, id: 30 });
                // console.log(`res=${JSON.stringify(res)}`);
            });
            it('获取首页图片', async function () {
                const res = await spCommon.getBackGroundPhotoList({ pageSize: 20, pageNo: 1 });
                // console.log(`res=${JSON.stringify(res)}`);
            });
        });

        describe('首页背景图-版本号', function () {
            let maxVersion, newShopBackGrounds;
            before('管理员查询背景图片列表', async function () {
                await spAuth.staffLogin();
                let res = await spCommon.getBackGroundPhotoList({ pageSize: 50, pageNo: 1 }).then(res => res.result.data.rows);
                newShopBackGrounds = [];
                //找到符合条件的背景图片，这里就针对type=1（今日上新门店）的背景图进行操作，筛选出有效的
                res.forEach(val => {
                    if (val.type == 1 && val.flag == 1) {
                        newShopBackGrounds.push(val);
                    };;
                });

                maxVersion = esSearchHelp.maxVersion(newShopBackGrounds);
            });

            it('版本号检查-相同版本号', async function () {

                let res = await spdresb.findHomePageData({ productVersion: maxVersion.productVer });
                expect(res.result.data.newShopDocId).to.be.equal(maxVersion.url);
            });

            it('版本号检查-大于最大的版本号', async function () {
                let productVer = maxVersion.productVer.split('.');
                let firstVer = parseInt(productVer[0]) + 2;
                productVer[0] = firstVer;
                let productVersion = productVer.join('.');

                let res = await spdresb.findHomePageData({ productVersion });
                expect(res.result.data.newShopDocId).to.be.equal(maxVersion.url);
            })
        });
    });
});



/**
 * @param {Object} spus 上新门店列表接口返回的spus
 * @param {Object} dresList 当前门店当天所有的上新款号的列表
*/

function newShopExp(spus, dresList) {

    if (dresList.length < 15) {
        expect(spus.length, '今日上新门店列表下面的款号数量不正确').to.be.equal(dresList.length);
    } else {
        expect(spus.length, '今日上新的商品多于15个，但是今日上新门店列表显示的商品不是15个').to.be.equal(15);
    };
    spus.forEach(res => {
        let isTodayNewDres = dresList.find(element => element.id == res.spuId);
        expect(isTodayNewDres, 'spus下面展示了不属于今天上新的商品').to.not.be.undefined;
        exp = {
            spuId: isTodayNewDres.id,
            code: isTodayNewDres.code,
            name: isTodayNewDres.name,
            price: isTodayNewDres.pubPrice,
            docId: (JSON.parse(isTodayNewDres.docHeader))[0].docId,
            detailUrl: isTodayNewDres.detailUrl
        };
        exp.detailUrl = `${exp.detailUrl}${LOGINDATA.tenantId}`;
        common.isApproximatelyEqualAssert(res, exp, [], '列表中款号信息错误');
    });
};

