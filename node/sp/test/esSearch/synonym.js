const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const common = require('../../../lib/common');
const spSearchSynonym = require('../../help/spConfg/spSearchSynonym');

describe('同义词', async function () {
    this.timeout(30000);
    before(async function () {
        await spAuth.staffLogin();
    });

    describe('同义词维护', async function () {
        const synonym = spSearchSynonym.setupSynonym();
        before(async function () {
            await synonym.getRandom().save();
        });
        after(async function () {
            if (synonym.id) {
                await synonym.disable();
            }
        });
        it('查看搜索同义词', async function () {
            const synonymDetail = await synonym.getDetail().then(res => res.result.data);
            common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
        });
        it('查看搜索同义词列表', async function () {
            const synonymList = await spCommon.getSpSearchSynonymList({ searchName: synonym.searchName }).then(res => res.result.data.rows);
            const synonymDetail = synonymList.find(data => data.id == synonym.id);
            expect(synonymDetail).not.to.be.undefined;
            common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
        });
        it('searchName唯一性校验', async function () {
            const res = await spCommon.saveSpSearchSynonym({ searchName: synonym.searchName, stdName: synonym.stdName, synonymList: synonym.synonymList, check: false });
            expect(res.result).to.includes({ msgId: 'spsearchsynonym_is_repeat' });
        });
        describe('修改', async function () {
            before(async function () {
                await synonym.getRandom().save();
            });
            it('查看搜索同义词', async function () {
                const synonymDetail = await synonym.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
            it('查看搜索同义词列表', async function () {
                const synonymList = await spCommon.getSpSearchSynonymList({ searchName: synonym.searchName }).then(res => res.result.data.rows);
                const synonymDetail = synonymList.find(data => data.id == synonym.id);
                expect(synonymDetail).not.to.be.undefined;
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
        });
        describe('停用', async function () {
            before(async function () {
                await synonym.enable();
            });
            it('查看搜索同义词', async function () {
                const synonymDetail = await synonym.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
            it('查看搜索同义词列表', async function () {
                const synonymList = await spCommon.getSpSearchSynonymList({ searchName: synonym.searchName, flag: synonym.flag }).then(res => res.result.data.rows);
                const synonymDetail = synonymList.find(data => data.id == synonym.id);
                expect(synonymDetail).not.to.be.undefined;
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
        });
        describe('启用', async function () {
            before(async function () {
                await synonym.disable();
            });
            it('查看搜索同义词', async function () {
                const synonymDetail = await synonym.getDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
            it('查看搜索同义词列表', async function () {
                const synonymList = await spCommon.getSpSearchSynonymList({ searchName: synonym.searchName, flag: synonym.flag }).then(res => res.result.data.rows);
                const synonymDetail = synonymList.find(data => data.id == synonym.id);
                expect(synonymDetail).not.to.be.undefined;
                common.isApproximatelyEqualAssert(synonym.getDetailExp(), synonymDetail);
            });
        });
    });

});