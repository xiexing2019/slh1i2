const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const mockJsonParam = require('../../help/mockJsonParam');
const common = require('../../../lib/common');

/**
 * 商品排序
 * http://119.3.46.172:6082/zentao/story-view-1457.html
 * 
 * 1、A(1,n)：新品，3张图；要求这个规则下的商品，其中来自一批店铺的占比为60%，二批店铺的占比为40%；
 * 在此基础上，再以下面的分值进行排序；
 * 1）上新率，10分；建款后越快上架到好店分值越高，最高10分；
 * 2）小视频，5分；有小视频5分，没有小视频0分；
 * 3）颜色尺码，10分；有多个颜色尺码的10分，只有1个颜色尺码的0分；
 * 
 * 2、B(1,n)：线上/线下价格一致，未展示过的商品优先展示；
 * 在此基础上，再以下面的分值进行排序；
 * 1）小视频，5分；有小视频5分，没有小视频0分；
 * 2）颜色尺码，10分；有多个颜色尺码的10分，只有1个颜色尺码的0分；
 * 3）加入收藏夹，5分；加入收藏夹次数越多的分值越高，最高5分；
 * 4）加入购物车，5分；加入购物车次数越多的分值越高，最高5分；
 * 5）IM咨询，5分；IM咨询次数越多的分值越高，最高5分；
 * 6）点击量，5分；点击查看详情次数越多的分值越高，最高5分；
 * 7）线上销量，10分；APP线上销量越高的分值越高，最高10分；
 * 
 * 3、C(1,n)：线下销售数据（一周）；
 * 
 * 4、D(1,n)：50元以下，未展示过的商品优先展示；
 * 在此基础上，再以和第2点相同的规则进行排序；
 */
describe.skip('商品排序', async function () {
    this.timeout(30000);
    const classId = 1013;
    before(async function () {
        await spReq.spSellerLogin();
        const classInfo = await spdresb.findByClass({ classId });
        const arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };

        await spReq.spClientLogin();
    });

    it('图片数量', async function () {
        await spReq.spSellerLogin();
        const dresJson = mockJsonParam.styleJson({ classId });
        dresJson.spu.docContent.shift();
        const afterSpuId = await spdresb.saveFull(dresJson).then(res => res.result.data.spuId);
        const frontSpuId = await spdresb.saveFull(mockJsonParam.styleJson({ classId })).then(res => res.result.data.spuId);

        await spReq.spClientLogin();
        await sortCheck({ frontSpuId, afterSpuId });
    });

    it('小视频', async function () {

    });

    it('颜色尺码', async function () {
        await spReq.spSellerLogin();
        const dresJson = mockJsonParam.styleJson({ classId });
        dresJson.spu.docContent.shift();
        const afterSpuId = await spdresb.saveFull(dresJson).then(res => res.result.data.spuId);
        const frontSpuId = await spdresb.saveFull(mockJsonParam.styleJson({ classId })).then(res => res.result.data.spuId);

        await spReq.spClientLogin();
        await sortCheck({ frontSpuId, afterSpuId });
    });

    it('加入收藏夹', async function () {

    });

    it('加入购物车', async function () {

    });


});

/**
 * 排序校验
 * @description frontSpuId出现在afterSpuId前即可
 */
async function sortCheck({ frontSpuId, afterSpuId, pageNo = 1 }) {
    if (pageNo > 10) {
        throw new Error(`未找到商品 spuId=${frontSpuId}`);
    }
    const dresList = await spdresb.searchDres({ queryType: 0, pageNo, pageSize: 20 }).then(res => res.result.data.dresStyleResultList);
    const frontIndex = _.findIndex(dresList, (dres) => dres.spuId == frontSpuId);
    const afterIndex = _.findIndex(dresList, (dres) => dres.spuId == afterSpuId);
    // 找到商品1
    if (frontIndex > -1) {
        if (afterIndex > -1 && afterIndex < frontIndex) {
            throw new Error(`排序错误`);
        }
    } else {
        pageNo++;
        await sortCheck({ frontSpuId, afterSpuId, pageNo })
    }
};

