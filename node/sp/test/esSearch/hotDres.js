const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const mockJsonParam = require('../../help/mockJsonParam');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAccount = require('../../data/spAccount');
// const caps = require('../../../data/caps');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const esSearchHelp = require('../../help/esSearchHelp');


describe('爆款频道', function () {
    this.timeout(TESTCASE.timeout);
    describe('手动设置爆款-online', function () {
        let saveDresHotRes, spuInfo, sellerTenantId, masterClassId, hotDresIds;

        before('设置爆款', async function () {
            await spReq.spSellerLogin();
            sellerTenantId = LOGINDATA.tenantId;
            let dresHotInfo = await spdresb.getDresHotConfig();
            const styleList = await spdresb.findSellerSpuList({ pageSize: 20, pageNo: 1, flag: 1, orderBy: 'marketDate', orderByDesc: true });

            spuInfo = styleList.result.data.rows.map(res => {
                return { spuId: res.id, title: res.title, docUrl: res.ecCaption.docHeader[0].docId }//, docUrl: (JSON.parse(res.docContent))[0].docId 
            });
            let saveDresHotParam = mockJsonParam.saveDresHotConfigJson(1);
            if (dresHotInfo.result.hasOwnProperty('data')) {
                saveDresHotParam.config.id = dresHotInfo.result.data.config.id;
            };
            saveDresHotParam.configDetails = getConfigDetails(spuInfo.slice(0, 4), 7);
            //console.log(`saveDres=${JSON.stringify(saveDresHotParam)}`);
            saveDresHotRes = await spdresb.saveDresHotConfig(saveDresHotParam);

            let shopDetail = await spugr.getShopDetail().then(res => res.result.data);
            masterClassId = shopDetail.masterClassId;
            await spReq.spClientLogin();
            await spCommon.updateBuyerShopMessage({ masterClassId });
        });

        it('查看手动设置的爆款信息', async function () {
            await spReq.spSellerLogin();
            const dresHotInfo = await spdresb.getDresHotConfig();

            let exp = getExp(saveDresHotRes.params.jsonParam, spuInfo.slice(0, 4));
            common.isApproximatelyEqualAssert(exp, dresHotInfo.result.data, [], '卖家查看爆款，和设置时的不一样');
        });

        it.skip('爆款主营类目验证', async function () {
            await spReq.spClientLogin();
            hotDresList = await spdresb.searchDres({ pageSize: 200, pageNo: 1, queryType: 3 });

            hotDresList.result.data.dresStyleResultList.forEach(element => {
                ids.push(element.id);
                expect(element.masterClassId, `不是该主营类目的商品，也显示在了爆款列表里面,${element.name},masterClassId:${element.masterClassId}`).to.be.equal(masterClassId);
            });
        });

        it.skip('爆款唯一性验证', async function () {
            let exp = common.dedupe(ids).length;
            expect(hotDresList.result.data.dresStyleResultList.length, '爆款列表中存在相同的商品').to.be.equal(exp);
        });

        it.skip('买家查看爆款列表', async function () {
            await spReq.spClientLogin();
            //买家设置主营类目
            let res = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 3, tenantId: sellerTenantId }); //queryType 3 爆款
            // console.log(`res=${JSON.stringify(res)}`);
            expect(res.result.data.dresStyleResultList.length, '用户列表没有显示爆款').not.to.be.equal(0);
            let day = new Date().getDay();
            if (day == 0) {
                day = 7;
            };
            //  console.log(`day=${JSON.stringify(day)}`);
            let configDetail = saveDresHotRes.params.jsonParam.configDetails.find(res => res.showDay == day);
            //  console.log(`saveDresHotRes=${JSON.stringify(saveDresHotRes)}`);
            let spuIds = configDetail.spus;
            let exp = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tenantId: sellerTenantId, keyWords: { "id": spuIds } });
            //  console.log(`exp=${JSON.stringify(exp)}`);

            common.isApproximatelyArrayAssert(exp.result.data.dresStyleResultList, res.result.data.dresStyleResultList);
        });

        it('卖家店铺内搜索爆款', async function () {
            let day = new Date().getDay();
            if (day == 0) {
                day = 7;
            };
            await spReq.spClientLogin();
            await common.delay(2000);
            let hotList = await spdresb.searchDres({ needWait: 1, pageSize: 10, pageNo: 1, queryType: 3, tenantId: sellerTenantId }); //queryType 3 爆款
            hotDresIds = hotList.result.data.dresStyleResultList.map(val => val.id);

            let exp = saveDresHotRes.params.jsonParam.configDetails.find(res => res.showDay == day).spus;
            common.isApproximatelyArrayAssert(exp, hotDresIds, [], `卖家店铺显示的爆款和设置的爆款不一致，设置的是${hotDresIds}，实际的是${exp}`);
        });

        it('其他返回字段验证', async function () {
            let searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, keyWords: { id: hotDresIds } }).then(res => res.result.data.dresStyleResultList);

            searchRes.forEach(val => {

                expect(val.hotFlag, `搜索是爆款的商品，但是返回的hotFlag不是true`).to.be.true;

                //这里暂时备注，因为服务单是加了能力位，客户端没有对接，害怕客户端出问题，所以先没放出来,等客户端对接之后放出来之后打开即可

                // let sc = new esSearchHelp.ShowCaption();
                // sc.setElement(val.specialFlag, val.hotFlag, val.newDresFlag);
                // let exp = sc.getElement();

                // expect(val.showCaption).to.be.at.least(exp);
                // expect(val.showCaption).to.be.below(common.add(exp, 3));
            });
        });
    });
});


function getConfigDetails(spus, period) {
    let configDetails = [];
    for (let i = 1; i < period + 1; i++) {
        //let spuId = spus[common.getRandomNum(0, spus.length - 1)].spuId;
        configDetails.push({
            showDay: i,
            spus: spus.map(res => res.spuId),//[spuId] 
        });
    };
    return configDetails;
};


function getExp(saveDresHotParam, spuInfo) {

    let saveJson = _.cloneDeep(saveDresHotParam);
    let configDetails = {};

    for (let i = 0; i < saveJson.configDetails.length; i++) {
        let arr = [];
        for (let j = 0; j < saveJson.configDetails[i].spus.length; j++) {
            let info = spuInfo.find(res => res.spuId == saveJson.configDetails[i].spus[j]);
            arr.push(info);
        };
        configDetails[i + 1] = arr;
    };
    return configDetails;
};
