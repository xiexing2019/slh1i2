const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spAccount = require('../../data/spAccount');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const format = require('../../../data/format');
const esSearchHelp = require('../../help/esSearchHelp');
const configParam = require('../../help/configParamManager');
const mysql = require('../../../reqHandler/sp/mysql');
const moment = require('moment');

describe('es基本搜索', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spClientLogin();
        clientInfo = _.cloneDeep(LOGINDATA);
    });

    // 暂时直接查询 后面需要功能组提供含有上次价数据的店铺 进行价格校验
    // 列表界面使用bestPubprice  详情界面使用price
    it('商品列表/详情界面 价格一致性验证-online', async function () {
        const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 0, showFlag: 2 }).then(res => res.result.data.dresStyleResultList);
        for (let index = 0; index < dresList.length; index++) {
            const dresSpu = dresList[index];
            const dresDetail = await spReq.getFullForBuyerByUrl({ detailUrl: dresSpu.detailUrl }).then(res => res.result.data);
            expect(dresSpu.bestPubprice).to.equal(dresDetail.spu.price);
        }
    });

    // 服务端2.0.8添加 queryType为0时有效
    describe('videoFlag搜索校验', async function () {
        it('videoFlag=0', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, videoFlag: 0 }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                let videoNums = 0;
                JSON.parse(dres.docContent).forEach(data => {
                    if (data.typeId == 3) videoNums++;
                });
                expect(videoNums).to.equal(0);
            });
        });
        it('videoFlag=1', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, videoFlag: 1 }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                let videoNums = 0;
                JSON.parse(dres.docContent).forEach(data => {
                    if (data.typeId == 3) videoNums++;
                });
                expect(videoNums).to.above(0);
                expect(dres.videoNums).to.equal(videoNums);
            });
        });
    });

    describe('labelItems', async function () {
        let labelItemIds = [];
        before(async function () {
            const dresList = await spdresb.searchDres({ queryType: 0 }).then(res => res.result.data.dresStyleResultList);
            labelItemIds = dresList[0].labelItemIds.split(/\s/);
            // console.log('labelItemIds:%j', labelItemIds);
        });
        it('labelItemsOr', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, labelItemsOr: labelItemIds.join(',') }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                const _labelItemIds = dres.labelItemIds.split(/\s/);
                expect(labelItemIds, `labelItemsOr=${labelItemIds}  spuId=${dres.spuId},labelItemIds=${dres.labelItemIds}`).
                    to.satisfy((itemIds) => itemIds.some((itemId) => _labelItemIds.includes(itemId)));
            });
        });
        it('labelItemsAnd', async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, labelItemsAnd: labelItemIds.join(',') }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => expect(dres.labelItemIds.split(/\s/)).to.include.members(labelItemIds))
        });
    });

    describe('es搜索条件验证', function () {
        let shopInfo, queryParams, queryTypes;
        before(async () => {
            await spAuth.staffLogin();
            shopInfo = await spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
            expect(shopInfo, `未找到店铺信息 tenantId=${sellerInfo.tenantId}`).not.to.be.undefined;
            // console.log(`shopInfo=${JSON.stringify(shopInfo)}`);

            queryParams = {
                masterClassId: shopInfo.masterClassId,
                marketId: shopInfo.marketId,
                tagsAll: shopInfo.tagsList,
                labelsAll: shopInfo.labels,
                cityCodes: shopInfo.cityCode,
            };
            let keys = ['masterClassId', 'marketId', 'tagsAll', 'labelsAll', 'cityCodes'];
            queryTypes = common.randomSort(keys).slice(0, 2);//['marketId', 'medalsCodeList']//
            // console.log(`queryTypes=${JSON.stringify(queryParams['medalsCodeList'])}`);
        });

        //价格区间
        it('es搜索条件--价格区间', async function () {
            const pubPriceStart = 100, pubPriceEnd = 200;
            const dresList = await spdresb.searchDres({ pageSize: 50, pageNo: 1, pubPriceStart, pubPriceEnd }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(res => {
                expect(res.pubPrice).to.be.within(pubPriceStart, pubPriceEnd);//搜出来的每个货品的发布价都应该再搜索价格之内
                expect(res.pubPrice).not.equal(pubPriceEnd); //这里100--200，不应该搜到200的货品
            });
        });
        // 李果那实现
        it('searchToken--市场', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, searchToken: shopInfo.marketName, queryType: 0 }).then(res => res.result.data.dresStyleResultList);
            expect(dresList, `searchToken=${shopInfo.marketName}商品搜索无结果`).to.have.lengthOf.above(0);
            dresList.forEach(val => expect(val).to.satisfy((dres) => dres.tenantAddr.includes(shopInfo.marketName) || dres.title.includes(shopInfo.marketName)));
        });

        it('searchToken--城市', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, searchToken: '杭州', queryType: 0 }).then(res => res.result.data.dresStyleResultList);
            expect(dresList, `searchToken=杭州 商品搜索无结果`).to.have.lengthOf.above(0);
            dresList.forEach(val => expect(val.city).to.includes('杭州'));
        });

        it('keyWords--masterClassId', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, keyWords: { masterClassId: [queryParams.masterClassId] }, queryType: 0 }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(val => expect(val.masterClassId).to.equal(queryParams.masterClassId));
        });
        // 商圈搜索 需要使用es中已经配置了的商圈 否则无效
        // 因此商圈直接写死 
        describe('商圈搜索', async function () {
            let tradeList = [];
            before(async function () {
                tradeList = await spCommon.getCatConfigTradeList({ typeName: '广州沙河商圈', flag: 1 }).then(res => res.result.data.rows);
                expect(tradeList, `商圈列表数据不足`).to.have.lengthOf.above(0);
            });
            it('tradeId--商圈id', async function () {
                const trade = tradeList[0];
                const marketIds = trade.marketList.map(market => market.typeId);
                const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tradeId: trade.id, queryType: 0 }).then(res => res.result.data.dresStyleResultList);
                dresList.forEach(dres => {
                    expect(dres.marketId).to.be.oneOf(marketIds);
                });
            });
            it('tradeIds--商圈ids', async function () {
                const trades = tradeList.slice(0, 2);
                const marketIds = _.flatten(trades.map(trade => trade.marketList.map(market => market.typeId)));
                const dresList = await spdresb.searchDres({ pageSize: 10, pageNo: 1, tradeIds: trades.map(trade => trade.id).join(','), queryType: 0 }).then(res => res.result.data.dresStyleResultList);
                dresList.forEach(dres => {
                    expect(dres.marketId).to.be.oneOf(marketIds);
                });
            });
        });

        //以下的排序  j 代表降序升序  i为排序字段  k为查询条件
        // 排序验证（按销量排序）、浏览量、点赞数、收藏数
        // 收藏数、 spuFavorNum, es 存储的为 favorNum（搜索orderBy传递favorNum）
        const arr = ['favorNum=spuFavorNum', 'pubPrice', 'sellNum', 'viewNum', 'praiseNum'];
        const orderByDesc = common.randomSort([true, false]);
        for (let j = 0; j < 1; j++) {//2控制升序还是降序
            for (let k = 0; k < 2; k++) {//控制几种查询条件 对应上面的queryTypes
                for (let i = 0; i < arr.length; i++) {
                    it.skip(`全局商品搜索条件-orderBy: ${arr[i]}排序验证,orderByDesc:${orderByDesc[j]}`, async function () {
                        const [orderBy, path = orderBy] = arr[i].split('=');
                        await spReq.spClientLogin();
                        const searchList = await spdresb.searchDres({ pageSize: 50, pageNo: 1, keyWords: { [`${queryTypes[k]}`]: queryParams[queryTypes[k]] }, orderBy: orderBy, orderByDesc: orderByDesc[j] }).then(res => res.result.data.dresStyleResultList);
                        esSearchHelp.orderAssert({ dataList: searchList, path: path, orderByDesc: orderByDesc[j] });
                    });
                };
            };
        };

        // 门店内排序校验 'showOrder'
        const arr2 = ['sellNum', 'pubPrice', 'marketDate'];
        for (let j = 0; j < 1; j++) {//降序还是升序
            for (let i = 0; i < arr2.length; i++) {
                it(`门店内排序校验, orderBy: ${arr2[i]}, orderByDesc:${orderByDesc[j]}`, async function () {
                    const searchList = await spdresb.searchDres({ pageSize: 50, pageNo: 1, queryType: 0, tenantId: sellerInfo.tenantId, orderBy: arr2[i], orderByDesc: orderByDesc[j] }).then(res => res.result.data.dresStyleResultList);
                    esSearchHelp.orderAssert({ dataList: searchList, path: arr2[i], orderByDesc: orderByDesc[j] });
                });
            };
        };

        //销量，上新数，'salesNum', 'marketNum',关注数  只有关注数
        const arr4 = ['salesNum', 'concernNum', 'likeNum'];  //'marketNum',

        for (let k = 0; k < 2; k++) {//随机两种查询条件queryTypes
            for (let i = 0; i < arr4.length; i++) {
                it(`混合搜索名称 + 手机号查询有效店铺接口（买家游客）按查询条件查询,orderBy:${arr4[i]},orderByDesc:${orderByDesc[0]}`, async function () {
                    //买家登录
                    await spReq.spClientLogin();
                    const searchList = await spugr.findShopBySearchToken(Object.assign({ pageSize: 20, pageNo: 1, orderBy: arr4[i], orderByDesc: orderByDesc[0] }, { [`${queryTypes[k]}`]: queryParams[queryTypes[k]] })).then(res => res.result.data.rows);
                    await orderAndQueryCheck({ searchList, orderBy: arr4[i], orderByDesc: orderByDesc[0] });
                });
            };
        };

    });

    // 首页商品门店聚合搜索 
    describe('es店铺商品搜索', function () {
        let searchToken = '商品', searchRes;
        before('搜索', async function () {
            await spReq.spClientLogin();
            searchRes = await spdresb.searchDresAndShop({ pageSize: 20, pageNo: 1, searchToken }).then(res => res.result.data);
        });

        it('验证结果中的商品信息', async function () {
            //console.log(`searchRes=${JSON.stringify(searchRes)}`);
            let res = await spdresb.searchDres({ pageSize: 20, pageNo: 1, searchToken }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(res.dresStyleResultList, searchRes.dres.dresStyleResultList);
        });

        it('验证结果中的店铺信息', async function () {
            let res = await spugr.findShopBySearchToken({ pageSize: 20, pageNo: 1, searchToken }).then(res => res.result.data);
            //console.log(`searchRes.shop.shopList=${JSON.stringify(searchRes.shop.shopList)}`);
            common.isApproximatelyEqualAssert(res.rows, searchRes.shop.shopList);
        });
    });

    describe('ES其他搜索', function () {
        let tenantId;
        before('获取卖家信息', async function () {
            await spReq.spSellerLogin();
            tenantId = LOGINDATA.tenantId;
        });

        it.skip('累计上新', async function () {
            await spReq.spClientLogin();
            let actualNum = 0;
            let exp;
            for (let i = 1; i < 10; i++) {
                let res = await spdresb.searchDres({ pageSize: 200, pageNo: i, tenantId, queryType: 0 });
                actualNum += res.result.data.dresStyleResultList.length;

                if (res.result.data.dresStyleResultList.length < 200) {
                    exp = res.result.data.count;
                    break;
                }; //一页查不满，就跳出循环了
            };
            expect(actualNum, '累计上新数量和实际数组的长度不一致').to.be.equal(exp);
        });

        describe('es搜索条件-online', function () {
            let searchByDresFlag, dres = [];
            before('登录', async function () {
                await spReq.spClientLogin();
            });

            it('es搜索条件 newDresFlag', async function () {
                await spReq.spClientLogin();
                //换成 4搜索，0不返回 marketDate了
                searchByDresFlag = await spdresb.searchDres({ pageSize: 50, pageNo: 1, queryType: 1, newDresFlag: true, tenantId }).then(res => res.result.data.dresStyleResultList);

                searchByDresFlag.forEach(val => {
                    expect(val.newDresFlag, `商品 ${val.name}不是商家一个月之内的商品，也搜索出来的`).to.be.true;
                    expect(val.tenantId, `商品${val.name}不属于租户${tenantId}，但是也搜索出来了`).to.be.equal(tenantId);
                    dres.push({ name: val.name, marketDate: val.marketDate });
                });
            });

            it('搜索出来的商品，都应该是一个月之内的', async function () {
                let startDate = moment(common.getCurrentTime(), 'YYYY-MM-DD HH:mm:ss').subtract(30, 'day').format('YYYY-MM-DD HH:mm:ss');
                dres.forEach(val => {
                    let flag = val.marketDate >= startDate;
                    expect(flag, `${val.name}不是近30天的商品，也显示在了列表里面${val.marketDate}`).to.be.true;
                });
            });

            it('es搜索条件，店铺内推荐商品', async function () {
                let searchRes = await spdresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 0, tenantId, keyWords: { specialFlag: [1] } }).then(res => res.result.data.dresStyleResultList);
                searchRes.forEach(val => {
                    expect(val.specialFlag).to.be.equal(1);    //推荐标志1
                    expect(val.pubPrice).to.be.below(6000);     //价格大于6000
                    expect(val.name.length).to.be.above(5);     //标题长度大于5
                    expect(JSON.parse(val.docContent).length).to.be.at.least(3);   //图片数量大于等于三
                    expect(val.tenantId).to.be.equal(tenantId);
                });
            });
        });
    });

    describe('mutilRangeValues-online', async function () {
        before(async function () {
            await spReq.spClientLogin();
        });
        it('invNum-库存', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 20, pageNo: 1, mutilRangeValues: { invNum: [{ valueStart: 50, valueEnd: 200 }] } }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => expect(dres.invNum).to.be.within(50, 200));
        });
        it('score-店铺评分', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 20, pageNo: 1, mutilRangeValues: { score: [{ valueStart: 1, valueEnd: 5 }] } }).then(res => res.result.data.dresStyleResultList);
            const dres = dresList.map(val => {
                return { id: val.id, tenantId: val.tenantId }
            });
            const tenantIds = _.uniqBy(dres, 'tenantId').slice(0, 5); //去除重复的门店

            for (let i = 0; i < tenantIds.length; i++) {
                let score = await spugr.getShop({ id: tenantIds[i].tenantId }).then(res => res.result.data.score);
                expect(score).to.be.within(1, 5);
            };
        });

        it('picNums-图片数量', async function () {
            const dresList = await spdresb.searchDres({ pageSize: 20, pageNo: 1, mutilRangeValues: { picNums: [{ valueStart: 3, valueEnd: 6 }] } }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => expect(_.takeWhile(JSON.parse(dres.docContent), (val) => val.typeId == 1).length, `图片数量有误 spuId:${dres.id}`).to.be.within(3, 6));
        });
    });

    describe('关联词列表-online', async function () {
        let assoWords;
        before(async function () {
            assoWords = await spdresb.searchAssoWord({ searchToken: '商品' }).then(res => res.result.data.rows);
        });
        // 不指定门店时查 spg.spbig.dres_search_word
        it('查询搜索关联词列表', async function () {
            expect(assoWords.every(word => word.includes('商品')), `查询结果: ${assoWords}, \n不包含token: 商品`).to.be.true;
        });
        it('唯一性校验', async function () {
            expect(assoWords.length, `查询结果重复: ${JSON.stringify(assoWords)}`).to.equal(common.dedupe(assoWords).length);
        });
        // 查dres_search_word,updated_date为今天的
        // 后面有时间再补数据校验
        it('搜索关联词列表--今日上新', async function () {
            await spdresb.searchAssoWord({ searchToken: '商品', isNewToday: 1 }).then(res => res.result.data.rows);
        });
        // 指定店铺,查的是dres_spu,根据店铺id得到unitId,然后再根据unitId得到name
        // 如取20条关联词列表数据时，为dres_spu中该门店有效的最近20条数据
        it('搜索关联词列表--门店搜索', async function () {
            await spReq.spSellerLogin();
            const spuNames = [];
            const [assoWordsByShop] = await Promise.all([spdresb.searchAssoWord({ searchToken: '商品', tenantId: LOGINDATA.tenantId }).then(res => res.result.data.rows),
            spdresb.findSellerSpuList({ flags: 1, nameLike: '商品' }).then(res => res.result.data.rows.forEach(val => {
                if (val.name.includes('商品')) spuNames.push(val.name);
            }))]);
            common.isApproximatelyEqualAssert(spuNames, assoWordsByShop.slice(0, spuNames.length));
        });
    });

    describe('买家搜索反馈', async function () {
        let clientInfo, saveRes;
        before('买家提交反馈', async function () {
            await spAuth.staffLogin();
            let dictList = await spConfig.findDictList({ typeId: 2021, flag: 1 });
            let typeIds = common.randomSort(dictList.result.data.rows.map(val => val.codeValue)).slice(0, 3);
            await spReq.spClientLogin();
            clientInfo = LOGINDATA;

            const keyWord = `商品${common.getRandomStr(5)}`;
            saveRes = await spUp.saveFeeBacks({ typeIds: typeIds.join(), rem: `输入关键词:${keyWord}，无法准确的搜索到商品`, searchKeyWords: keyWord });
        });

        it('管理员查看', async function () {
            await common.delay(500);
            await spAuth.staffLogin();
            let res = await spCommon.getFeeBacks({ pageSize: 50, pageNo: 1, orderBy: 'createdDate', orderByDesc: true, buyerName: clientInfo.userName });
            let info = res.result.data.rows.find(val => val.id == saveRes.result.data.val);
            let exp = saveRes.params.jsonParam;
            Object.assign(exp, format.dataFormat(clientInfo, 'updatedBy=userId;buyerId=tenantId;buyerName=tenantName;buyerUnitId=unitId'));
            exp.createdDate = updatedDate = saveRes.opTime;
            common.isApproximatelyEqualAssert(exp, info, [], '管理员显示的搜索反馈和买家提交的不一致')
        });
    });

    describe('平台端搜索店铺', function () {

        it('平台端搜索-masterClassId', async function () {
            await spReq.spSellerLogin();
            let tenantId = LOGINDATA.tenantId;
            let masterClassId = await spugr.getShopDetail().then(res => res.result.data.masterClassId);

            await spAuth.staffLogin();
            let searchRes = await spugr.findAllShopByParams({ pageSize: 50, pageNo: 1, masterClassId });
            searchRes.result.data.rows.forEach(val => {
                expect(val.masterClassId).to.be.equal(searchRes.params.jsonParam.masterClassId);
            });
        });

        //根据score区间查询，搜索结果是包括scoreGte和scorLte边界值的
        it('平台端搜索--scoreGte和scorLte', async function () {
            let searchRes = await spugr.findAllShopByParams({ pageSize: 50, pageNo: 1, scoreGte: 1, scoreLte: common.getRandomNum(10, 50) });
            searchRes.result.data.rows.forEach(val => {
                expect(val.score).to.be.within(searchRes.params.jsonParam.scoreGte, searchRes.params.jsonParam.scoreLte);
            });
        });
    });

    // marketDate 大部分去掉不返回了
    describe.skip('发布时间', async function () {
        let usingParam, configHour;
        before(async function () {
            await spAuth.staffLogin();
            const paramInfo = await configParam.getParamInfo({ domainKind: 'system', nameOrCode: 'sp_market_date_rule' });
            //形如A-B，A代表是否采用该规则,A为1代表启用改规则，为0则代表停用该规则。B代表时间，单位小时
            [usingParam, configHour] = paramInfo.val.split('-');

            if (usingParam == 0) this.skip();
        });
        it('发布时间校验', async function () {
            const dresList = await spdresb.searchDres({ queryType: 4 }).then(res => res.result.data.dresStyleResultList);

        });
    });

});

//获取门店销量和上新数
async function getSummary({ unitIds, orderBy }) {
    let sqlRes, cond = unitIds.length > 0 ? `unit_id in (${unitIds})` : '';
    // console.log(cond);
    const spbMysql = await mysql({ dbName: 'spbMysql' });
    try {
        if (orderBy == 'salesNum') {
            cond = cond ? `where ${cond}` : cond;
            //获取销量  
            [sqlRes] = await spbMysql.query(`select unit_id as unitId,sum(sales_num) as num from spdresb001.dres_stat ${cond} group by unit_id`);
            // console.log(`salesNum.length=${JSON.stringify(salesNum)}`);
        } else {
            cond = cond ? `and ${cond}` : cond;
            //获取上新数
            [sqlRes] = await spbMysql.query(`select unit_id as unitId ,count(*) as num  from spdresb001.dres_spu where flag=1 ${cond} group by unit_id `);
        };
    } catch (error) {
        console.warn(`sql查询出错:${error}`);
    } finally {
        await spbMysql.end();
    };

    return { sqlRes };
};

/**
 * 混合搜索名称 + 手机号查询有效店铺接口（买家游客) 查询排序
 * @param {Object} param
 * @param {Object} param.searchList
 * @param {Object} param.orderBy 排序字段 salesNum  marketNum
 * @param {Boolean} param.orderByDesc 是否降序 true 为降序
 */
async function orderAndQueryCheck({ searchList, orderBy, orderByDesc }) {

    if (['salesNum', 'marketNum'].includes(orderBy)) {
        let unitIds = searchList.map(ele => ele.unitId);
        // console.log(`unitIds=${JSON.stringify(unitIds)}`);
        let sum = await getSummary({ unitIds, orderBy });
        // console.log(`sum.sqlRes=${JSON.stringify(sum.sqlRes)}`);
        let orderInfo = {};
        sum.sqlRes.forEach(ele => orderInfo[ele.unitId] = ele.num);
        let actual = searchList.map(ele => orderInfo.hasOwnProperty(ele.unitId) ? orderInfo[ele.unitId] : 0);
        if (orderByDesc) {
            //降序
            let exp = _.cloneDeep(actual).sort((a, b) => b - a);
            common.isApproximatelyEqualAssert(exp, actual, [], `${orderBy}排序错误:unitIds=[${unitIds}]\nexpected:[${exp}]\nactual:[${actual}]`);
        } else {
            let exp = _.cloneDeep(actual).sort((a, b) => a - b);
            common.isApproximatelyEqualAssert(exp, actual, [], `${orderBy}排序错误:unitIds=[${unitIds}]\nexpected:[${exp}]\nactual:[${actual}]`);
        };
    } else {
        esSearchHelp.orderAssert({ dataList: searchList, path: orderBy, orderByDesc });
    };
};

