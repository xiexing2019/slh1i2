

const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const esSearchHelp = require('../../help/esSearchHelp');
const common = require('../../../lib/common');

describe('包邮区-online', function () {
    this.timeout(TESTCASE.timeout);
    let searchRes = [];
    before('查询包邮区', async function () {
        await spReq.spClientLogin();

        for (let i = 1; i < 20; i++) {
            let res = await spdresb.searchDres({ pageSize: 200, pageNo: i, queryType: 7 }).then(res => res.result.data.dresStyleResultList);
            searchRes = searchRes.concat(res);   //把数组连起来
            if (res.length < 200) {
                break;
            }; //一页查不满，就跳出循环了
        };
    });

    it('唯一性校验', async function () {
        let ids = searchRes.map(res => res.id);
        expect(searchRes.length).to.be.equal(ids.length);
    });

    it('排序', async function () {
        esSearchHelp.orderAssert({ dataList: searchRes, path: 'marketDate', orderByDesc: true });
    });

    it('检测门店包邮', async function () {

        //过滤出tenantIds 和tenantName
        let tenantInfo = searchRes.map(val => {
            return { tenantId: val.tenantId, tenantName: val.tenantName }
        });

        //去除重复的
        let tenantIds = _.uniqBy(tenantInfo, 'tenantId').slice(0, 5);

        for (let i = 0; i < tenantIds.length; i++) {
            let shipFeeWayId = await spugr.getShop({ id: tenantIds[i].tenantId }).then(res => res.result.data.shipFeeWayId);
            expect(shipFeeWayId, `店铺${tenantIds[i].tenantName}本身不是包邮的，也显示在了包邮区`).to.be.equal(1);
        };
    });
});