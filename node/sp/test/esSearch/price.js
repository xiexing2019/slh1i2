const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spAccount = require('../../data/spAccount');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const format = require('../../../data/format');
const esSearchHelp = require('../../help/esSearchHelp');
const configParam = require('../../help/configParamManager');
const mysql = require('../../../reqHandler/sp/mysql');

describe('es-价格相关校验', async function () {
    this.timeout(30000);


    /**
     * besePubPrice 上次价/活动价/最优价的最小值
     * 本用例集只校验besePubPrice取值规则是否正确  使用抽样查询模式校验 
     * 上次价/活动价/最优价 值的准确性由各自的模块校验
     * 
     * es列表 pubPrice不会改变  商品详情 price会变为besePubPrice
     * 
     * 2.2.0只做了数据的初始化 排序校验到2.2.4实现
     */
    describe('besePubPrice', async function () {
        let dresList;
        before(async function () {
            await spReq.spClientLogin();

            dresList = await spdresb.searchDres({ queryType: 0, pageSize: 10, pageNo: 1 })
                .then(res => res.result.data.dresStyleResultList.map(dres => format.dataFormat(dres, 'id;bestPubprice;pubPrice;priceWithlastPrice=priceWithlastPrice=999999999;detailUrl')));
            // console.log(dresList);
        });
        it('数据校验', async function () {
            for (let index = 0; index < dresList.length; index++) {
                const element = dresList[index];
                // const dresDetail = await spReq.getFullForBuyerByUrl({ detailUrl: element.detailUrl }).then(res => res.result.data);

                expect(element.bestPubprice).to.equal([element.pubPrice, element.priceWithlastPrice].reduce((a, b) => Math.min(a, b)));
            }
        });
    });

});

