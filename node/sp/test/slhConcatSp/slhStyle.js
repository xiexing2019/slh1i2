const common = require('../../../lib/common');
const format = require('../../../data/format');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const mockJsonParam = require('../../help/mockJsonParam');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const reqHandler = require('../../../slh1/help/reqHandlerHelp');
const basiceJsonparam = require('../../../slh1/help/basiceJsonparam');
const basicInfoReqHandler = require('../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const mainReqHandler = require('../../../slh1/help/basicInfoHelp/mainDataReqHandler');

describe.skip('平台同步slh货品', function () {
    this.timeout('30000');
    let saveStyle, sellerInfo, clientInfo, colorIds = [], sizeIds = [], classInfo = {}, supplierInfo = {};
    before(async () => {
        //颜色 尺码 
        //slh登录
        await spReq.slhStaffLogin();
        // LOGINDATA.sessionid = '0CD86CA4-7E2F-446A-0E85-C0524F78AB9B';
        console.log(`slhLOGINDATA=${JSON.stringify(LOGINDATA)}`);
        let typeId = [601, 605];
        for (let i = 0; i < typeId.length; i++) {
            let dictList = await basicInfoReqHandler.getDictList({ typeid: typeId[i], })
                .then(res => res.result.dataList);
            let index = typeId[i] == 601 ? Math.min(dictList.length, 5) : 5;
            for (let j = 0; j < index; j++) {
                if (typeId[i] == 601) {
                    if (dictList[j].name == '均色') continue;
                    colorIds.push(dictList[j].sid);
                } else {
                    sizeIds.push(dictList[j].sid);
                };
            };
        };
        //获取类别标准库
        // classInfo = await common.apiDo({
        //     apiKey: 'ec-config-spuStdClass-tree',
        //     includeSubs: true,
        // }).then(res => res.result.data.options[1].items[0].items[0]);
        // console.log(`classInfo=${JSON.stringify(classInfo)}`);
        let supplierList = await mainReqHandler.getSupplierList().then(res => res.result.dataList);
        if (supplierList.length == 1) {
            supplierInfo.id = await mainReqHandler.saveSupplier({ nameshort: '厂商A' }).then(res => res.result.val);
        } else {
            supplierInfo = supplierList[0];
        };

        let classList = await basicInfoReqHandler.getClassList().then(res => res.result.dataList);
        let ids = [];
        classList.forEach(val => { if (val.level == 3) { ids.push(val.id) } });
        classInfo = ids[common.getRandomNum(0, ids.length - 1)];
    });

    describe('平台同步slh新增的货品', function () {
        let styleInfo, styleListInfo;
        before(async () => {
            //平台下单需要有库存
            let purinjson = [];
            colorIds.forEach((colorid) =>
                sizeIds.forEach((sizeid) => {
                    purinjson.push({ colorid, sizeid, num: '100' });
                }));
            let dresInfo = common.getRandomStr(6);
            let json = basiceJsonparam.addGoodJson({
                code: `code${dresInfo}`,
                name: `商品${dresInfo}`,
                purinjson,
                dwid: supplierInfo.id,
                colorids: colorIds.toString(),
                sizeids: sizeIds.toString(),
                classid: classInfo,
            });
            // console.log(`json=${JSON.stringify(json)}`);

            //新增款号 颜色 尺码  类别 价格  图片
            saveStyle = await basicInfoReqHandler.editStyle({ jsonparam: json });
            console.log(`\nsaveStyle=${JSON.stringify(saveStyle)}`);
            //sn=='1400221154483'为adev1环境 否则为app1环境的图片,暂时只有两个环境的图片
            let fileid = LOGINDATA.sn == '1400221154483' ? '34367825,34367829,34367833' : '34364949,34364953,34364957,34364961,34364965,34364969,34364973,34364977,34364981';

            // //上传图片
            savePicture = await reqHandler.csIFCHandler({
                interfaceid: 'cs-dres-update-video',
                pk: saveStyle.result.val,
                fileid: common.randomSort(fileid.split(',')).join(),
                epid: LOGINDATA.epid,
            });
            styleInfo = await common.fetchMatInfo(saveStyle.result.val);
            // // expect(styleInfo.fileid).to.equal(savePicture.params.fileid);
            // console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
            let stockNum = 0;//库存
            json.purinjson.forEach(ele => {
                stockNum = common.add(stockNum, ele.num);
            });
            styleInfo.stockNum = stockNum;
            // //slh切换到平台
            await spugr.slhStaffLoginSp()
            sellerInfo = _.cloneDeep(LOGINDATA);
            // tenantId
            // // // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            await common.delay(1000);
            // //查询刚刚新增的商品
            styleListInfo = await spdresb.findSellerSpuList({ nameLike: styleInfo.code, flags: 1 }).then(res => res.result.data.rows.find(obj => obj.slhId == saveStyle.result.val));
            console.log(`\n卖家商品列表查询res=${JSON.stringify(styleListInfo)}`);
            expect(styleListInfo, `卖家商品列表查询查不到商陆花新增的款号`).not.to.be.undefined;
        });
        it('切换到平台查看上架商品列表', async () => {
            expect(styleListInfo.flag, `slh新增有库存款号，没有自动上架`).to.equal(1);
            let exp = spStyleListExp(styleInfo);
            // console.log(`卖家商品列表查询exp=${JSON.stringify(exp)}`);
            common.isApproximatelyEqualAssert(exp, styleListInfo);
        });
        it('切换到平台查看上架商品详情', async () => {
            let styleFullRes = await spdresb.getFullById({ id: styleListInfo.id });
            let sku = [];
            let skuCommonExp = format.dataFormat(styleListInfo, 'pubPrice;price1;updatedDate;flag');
            saveStyle.params.purinjson.forEach(ele => {
                sku.push(Object.assign({
                    spec2: ele.colorid,
                    spec1: ele.sizeid,
                    num: ele.num,
                    salesNum: 0,
                    backNum: 0,
                }, skuCommonExp));
            });
            let exp = { sku, spu: styleListInfo };
            console.log(`\n上架商品详情exp=${JSON.stringify(exp)}`);
            common.isApproximatelyEqualAssert({ sku, spu: styleListInfo }, styleFullRes.result.data.spu);
        });
        describe.skip('买家登录es搜索+下单', function () {
            let searchRes, dresRes;
            before(async () => {
                await common.delay(2000);
                await spReq.spClientLogin();//keyWords: { id: styleListInfo.id },
                searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList.find(obj => obj.id == styleListInfo.id));
                console.log(`\nes搜索searchRes=${JSON.stringify(searchRes)}`);
                expect(searchRes, `slh新增款号，平台端es搜索查不到该款号`).not.to.be.undefined;
                dresRes = await spReq.getFullForBuyerByUrl({ detailUrl: searchRes.detailUrl });
            });
            it('es搜索', async () => {
                let exp = Object.assign({
                    invNum: styleListInfo.stockNum,
                    sellNum: styleListInfo.salesNum,
                    tenantId: sellerInfo.tenantId,
                }, styleListInfo);
                console.log(`\nes搜索exp=${JSON.stringify(exp)}`);

                common.isApproximatelyEqualAssert(exp, searchRes, ['docHeader', 'favorNum']);
            });
            it('es搜索商品详情', async () => {
                //买家查询刚刚新增的款号详情
                // console.log(`\ndresRes=${JSON.stringify(dresRes)}`);
                common.isApproximatelyEqualAssert(searchRes, dresRes.result.data);
            });

            describe('买家下单', function () {
                let purRes, purListInfo;
                before(async () => {
                    await spReq.spClientLogin();
                    clientInfo = _.cloneDeep(LOGINDATA);
                    //获取用户默认收货地址
                    await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                    purRes = await spTrade.savePurBill(mockJsonParam.purJson({ styleInfo: dresRes }));//mockStyleRes
                    console.log(`\n买家下单purRes=${JSON.stringify(purRes)}`);
                });
                it('买家采购单查询', async () => {
                    purListInfo = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'id', orderByDesc: true, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    console.log(`\npurListInfo=${JSON.stringify(purListInfo)}`);
                    expect(purListInfo, `slh新增的款号，买家下单后查询不到采购单`).not.to.undefined;
                    let exp = {
                        bill: purRes.params.jsonParam.orders[0].main,
                        skus: purRes.params.jsonParam.orders[0].details,
                        trader: {}
                    };
                    exp.skus.forEach(ele => {
                        ele.skuNum = ele.num;
                    });
                    console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    exp.trader = format.dataFormat(sellerInfo, 'tenantId;unitId;clusterCode;');
                    console.log(`\n采购单查询exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, purListInfo);
                });
                it('买家采购单详情', async () => {
                    const purBillInfo = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId });
                    console.log(`\n买家采购单详情purBillInfo=${JSON.stringify(purBillInfo)}`);
                    common.isApproximatelyEqualAssert(purListInfo, purBillInfo.result.data);
                });
                it('卖家销售单查询', async () => {
                    //slh平台登录
                    await spReq.loginGoodShopBySlh();
                    const salesListInfo = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    console.log(`\n卖家销售单查询salesListInfo=${JSON.stringify(salesListInfo)}`);
                    expect(salesListInfo, `slh新增的款号，买家下单后查询不到销售单`).not.to.undefined;
                    let exp = _.cloneDeep(purListInfo);
                    exp.bill.purBillId = exp.bill.id;
                    exp.bill.id = exp.bill.salesBillId;
                    exp.skus = exp.skus.shift();
                    console.log(`\n卖家销售单查询exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, salesListInfo);
                });
                it('卖家销售单详情', async () => {
                    const salesBillInfo = await spTrade.salesFindBillFull({ id: purListInfo.bill.salesBillId });
                    console.log(`\n卖家销售单详情salesBillInfo=${JSON.stringify(salesBillInfo)}`);
                    let exp = {
                        bill: purRes.params.jsonParam.orders[0].main,
                        skus: purRes.params.jsonParam.orders[0].details,
                        trader: {}
                    };
                    exp.skus.forEach(ele => {
                        ele.skuNum = ele.num;
                    });
                    exp.trader = format.dataFormat(clientInfo, 'tenantId;unitId;clusterCode;');
                    console.log(`\n卖家销售单详情exp=${JSON.stringify(exp)}`);
                    common.isApproximatelyEqualAssert(exp, salesBillInfo);
                });
            });
        });
    });
});

function spStyleListExp(slhStyleInfo) {
    //spec1Names=show_sizeids;spec2Names=show_colorids;
    let exp = format.dataFormat(slhStyleInfo, 'discount;slhId=pk;name;code;slhDate=optime;unit=show_unit;stockNum;price1=stdprice1;pubPrice=stdprice1;special');
    exp.title = `${exp.name}_${slhStyleInfo.show_classid}`;
    exp.salesNum = 0;//新增的款号销售数为0
    exp.salesMoney = 0;
    exp.spec1IdList = slhStyleInfo.sizeids.split(',').sort();
    exp.spec2IdList = slhStyleInfo.colorids.split(',').sort();
    return exp;
};