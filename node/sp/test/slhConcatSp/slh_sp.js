const spugr = require('../../../reqHandler/sp/global/spugr');
const mockJsonParam = require('../../help/mockJsonParam');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');


//需要优化一下
describe.skip('slh相关用户、老板注册平台', function () {
    this.timeout(30000);
    describe('slh相关接口冒泡', function () {
        let registerResult, tenantId;
        before('slh老板注册', async function () {
            let slhBossRegisterJson = mockJsonParam.slhBossRegisterJson();
            registerResult = await spugr.slhBossRegister(slhBossRegisterJson);
            expect(registerResult.result.msg, '注册失败').to.be.equal('成功');
        });

        it('slh老板登录', async function () {
            //用注册的老板登录,token这里还没好
            let loginJson = {
                token: registerResult.params.jsonParam.token,
                mobile: registerResult.params.jsonParam.mobile,
                slhSessionId: registerResult.params.jsonParam.slhSessionId,
                tenantId: registerResult.result.data.tenantId
            };
            let loginResult = await spugr.slhBossLogin(loginJson);
            expect(registerResult.result.data.tenantId, '商陆花老板注册的登录失败').to.be.equal(loginResult.result.data.tenantId);
            tenantId = loginResult.result.data.tenantId;
        });

        //这里的逻辑有所改动，以前是重复注册有提示，现在重复注册没有提示，返回的还是第一次注册的用户信息
        it('重复注册', async function () {
            //获取第一次注册的信息，并且让post方法里面不需要断言--slhBossRegisterJson.check = false;
            let slhBossRegisterJson = registerResult.params.jsonParam;
            slhBossRegisterJson.check = false;
            let registerResult1 = await spugr.slhBossRegister(slhBossRegisterJson);
            common.isApproximatelyArrayAssert(registerResult, registerResult1, ['loginTime']);
        });


        it.skip('slh员工登陆', async function () {
            let slhUserLoginRes = await spugr.slhUserLogin({
                token: common.getRandomStr(5),
                code: '000',
                name: 'slh员工' + common.getRandomNumStr(4),
                slhSessionId: common.getRandomStr(10),
                tenantId: tenantId
            });
            expect(slhUserLoginRes.result.data.userName).to.be.equal(slhUserLoginRes.params.jsonParam.code);
        });
    })
    //需要调一下，审核环境有问题
    describe('slh好店', async function () {

        describe('好店绑定修改', function () {
            let cqShopId, zzShopId, tenantId;
            it('从常青店切到中州店', async function () {
                await spReq.loginGoodShopBySlh();
                shopInfo = await spugr.getShopDetail().then(res => res.result.data);
                tenantId = LOGINDATA.tenantId;
                await spReq.slhStaffLogin({ logid: '200' });
                zzShopId = LOGINDATA.invid;
                await spReq.slhStaffLogin();
                cqShopId = LOGINDATA.invid;
                //将门店和仓库修改
                await spugr.updateGoodShopStoreInv({ spTenantId: tenantId, slhShopId: zzShopId, storeSyncInvIds: zzShopId });
            });

            it('常青店登录应该不成功', async function () {
                await spReq.slhStaffLogin();
                let info = await spugr.slhStaffLoginSp({ check: false });
                expect(info.result, '切换门店后，原来的门店还是可以登录').to.includes({ "msgId": "sp_tenant_not_open" });
                let isOpen = await spugr.isOpen({ slhShopId: LOGINDATA.invid });
                expect(isOpen.result.data.isOpen).to.be.equal(false);
            });

            it('中州店应该可以正常登录', async function () {
                await spReq.slhStaffLogin({ logid: '200' });
                let shopName = LOGINDATA.invname;
                let isOpen = await spugr.isOpen({ slhShopId: LOGINDATA.invid });
                expect(isOpen.result.data.isOpen).to.be.true;
                await spugr.slhStaffLoginSp();

                let shopInfo = await spugr.getShop({ id: tenantId }).then(res => res.result.data);

                expect(shopInfo.slhShopName, '好店绑定的slh门店名称错误').to.be.equal(shopName);
                expect(shopInfo.storeInvNames, '好店绑定的仓库错误').to.be.equal(shopName);
            });

            after('还原，好店绑定到常青店', async function () {
                await spReq.slhStaffLogin({ logid: '200' });
                await spugr.updateGoodShopStoreInv({ spTenantId: tenantId, slhShopId: cqShopId, storeSyncInvIds: cqShopId });
            });
        });

        describe.skip('门店商户信息完善', function () {
            let merchantInfo, shopInfo;
            before('通过常青店登录好店', async function () {
                await spReq.loginGoodShopBySlh();
                shopInfo = await spugr.getShopDetail().then(res => res.result.data);
                merchantInfo = await spugr.findMerchant().then(res => res.result.data);
                await spAuth.staffLogin();
                await spugr.updateSpMerchatFlagByPerson({ id: merchantInfo.id, flag: 5 });
                await spReq.loginGoodShopBySlh();

            });

            //注，这里是直接提交到清分系统的，会调用第三方接口进行相关信息审核的
            it('修改', async function () {
                let json = mockJsonParam.merchantJson();
                json.contact = shopInfo.name;
                json.mobile = shopInfo.mobile;
                if (Object.keys(merchantInfo).length != 0) {
                    json.id = merchantInfo.id;
                };
                await spugr.createMerchat(json);
            });

            it('查看账户信息', async function () {
                const res = await spugr.findMerchant().then(res => res.result.data);
                //console.log(`res=${JSON.stringify(res)}`);
            });
        });
    });
});































