const common = require('../../../lib/common');
const spAssist = require('../../../reqHandler/sp/spAssistant/spAssist');
const spReq = require('../../help/spReq');
const spIM = require('../../../reqHandler/sp/biz_server/spIM');
const spSql = require('../../../reqHandler/sp/mysql');
const caps = require('../../../data/caps');
const mockJsonParam = require('../../help/mockJsonParam');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
// http://c.hzdlsoft.com:7082/Wiki.jsp?page=好店助手登录用例
// 暂时只能在审核环境运行
// 关于IM聊天功能,只进行简单的校验,服务端不报错和关键字段的返回
describe.skip('好店助手', function () {
    this.timeout(300000);
    let staffInfo;
    // 员工角色
    before(async function () {
        // await spugr.slhUserLogin({});
        // staffMobile = '12577233335';

        await spReq.slhStaffLogin({ env: 'app1' });
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        // 用户管理-查询列表
        staffInfo = await common.callInterface2({ interfaceid: 'ql-1110', depid: LOGINDATA.depid }).then(res => {
            // console.log(`res=${JSON.stringify(res)}`);
            return res.result.dataList.find(val => val.code == '004');
        });
    });

    after('切回环境', async function () {
        caps.updateEnvByName({ name: caps.envName })
    });

    it('店员开通好店助手', async function () {
        if (staffInfo.id) {
            // 用户管理-获取修改页面数据
            const qfRes = await common.callInterface2({ interfaceid: 'qf-1110-2', pk: staffInfo.id });

            let jsonparam = qfRes.result;
            if (jsonparam.spAssistantFlag != 1) {
                jsonparam.spAssistantFlag = 1;
                jsonparam.action = 'edit';
                jsonparam.epid = LOGINDATA.epid;
                jsonparam.depid = LOGINDATA.depid;
                jsonparam.pk = staffInfo.id;
                // 用户管理-新增/修改保存
                await common.callInterface2({ interfaceid: 'sf-1110-2', pk: staffInfo.id, jsonparam });
                await common.delay(500);
            };
        };
    });

    describe('好店助手', async function () {
        let sellerInfo;
        before('登陆', async function () {
            await spReq.slhStaffLogin({ logid: staffInfo.code, env: 'app1' });

            const spStaffInfo = await spAssist.getSpStaffForLogin({ mobile: staffInfo.mobile });

            await spAssist.loginForSpServiceStaff({ mobile: staffInfo.mobile, spStaffInfo: spStaffInfo.result.data });
            sellerInfo = _.cloneDeep(LOGINDATA);
        });
        after('助手登出并校验会话失效', async function () {
            if (sellerInfo) {
                LOGINDATA = sellerInfo;
                await spAssist.invalidSpAssistSession({ mobile: LOGINDATA.mobile });
                const res = await spAssist.checkSpSession({ mobile: LOGINDATA.mobile, check: false });
                expect(res.result.code, `登出后会话code错误`).to.equal(-100);
            }
        });
        //一次 
        it.skip('注册并更新消息桥端点', async function () {
            await spAssist.registerMessageCid();
        });
        it('获取验证码', async function () {
            const res = await spAssist.getValidateCode({ mobile: sellerInfo.mobile });
            expect(res.result.data).to.includes({ val: '查询成功!' });
        });
        it('拉取im登陆信息', async function () {
            const res = await spIM.getNimLoginInfo();
            expect(res.result.data).to.includes({ imToken: sellerInfo.imToken, imAccid: sellerInfo.imAccid });
        });
    });

    // 1.4.8开始 与好店助手无关  认证来源从李果那获取
    describe.skip('好店助手手机号注册买家查看是否批发商', function () {
        let submitAuditRes;
        before('用好店助手手机号注册好店买家', async function () {
            await spReq.spLogin({ code: staffInfo.mobile });
            let areaJson = await mockJsonParam.getAreaJson();
            let json = await mockJsonParam.submitAuditJson({ areaJson });
            json.shopName = '助手买家';
            submitAuditRes = await spCommon.submitAuditRecord(json);
            // console.log(`submitAuditRes=${JSON.stringify(submitAuditRes)}`);

            await spAuth.staffLogin();
            await spCommon.changeAuditRecord({
                shopId: submitAuditRes.result.data.id,
                flag: 1,
                auditRemark: '条件符合审核条件，同意了'
            });
        });

        //执行最后需要驳回，为了做到手机号循环利用
        after('驳回，数据初始化', async function () {
            await spCommon.changeAuditRecord({
                shopId: submitAuditRes.result.data.id,
                flag: 9,
                auditRemark: '条件不符合申请条件，请认真填写申请'
            });
        });

        it('管理员查看审核列表', async function () {
            let clientInfo = await spCommon.findBuyerShopAudits({ pageSize: 10, pageNo: 1, nameLike: submitAuditRes.params.jsonParam.shopName, submitDateS: common.getCurrentDate(), submitDateE: common.getCurrentDate() });
            const info = clientInfo.result.data.rows.find(element => element.auditObjId == submitAuditRes.result.data.id);
            expect(info.wholesalerIs, `用好店助手注册的买家，管理员查看显示的不是批发商`).to.be.equal(1);
        });

    });

});

describe.skip('客服系统', async function () {
    this.timeout(30000);
    let sellerInfo, converse;
    before('买家发起会话', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        await spReq.spClientLogin();
        converse = await spIM.startConv({ _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId, buyerId: LOGINDATA.userId, salesUnitId: sellerInfo.unitId });
    });
    it('', async function () {
        console.log(`converse=${JSON.stringify(converse)}`);

    });
    // it('会话校验', async function () {
    //     expect(converse.result.data).to.includes({ sallerAccId: sellerInfo.imAccid, buyerAccId: LOGINDATA.extProps.im.imAccid });
    // });
    // it('模拟聊天消息', async function () {
    //     // spugr sp_nim_account
    //     const data = await spIM.testTransferMsg({ body: { body: 'test' }, mobileFrom: LOGINDATA.mobile, mobilesTo: '15291685193' }).then(res => res.result.data);
    //     expect(data.body).to.includes({ body: 'test' });
    // });
    // it('买家重新开始聊天', async function () {
    //     //一个客户对一个卖家的team_id唯一
    //     // const spgSql = await spSql({ dbName: 'spbMysql' });
    //     // const [teamIds] = await spgSql.query(`select team_id from spcsb001.sp_nim_team where name=${LOGINDATA.unitName} `);//and unit_id=${sellerInfo.unitId}
    //     // console.log(`teamId=${teamIds.shift().team_id}`);
    //     // spgSql.end();
    //     const res = await spIM.reStartConv({ _cid: sellerInfo.spClusterCode, _tid: sellerInfo.spTenantId, salesUnitId: sellerInfo.spUnitId, teamId: converse.result.data.tid });
    //     expect(res.result).to.includes({ msg: '成功' });
    // });
    // it('卖家重新开始聊天', async function () {
    //     LOGINDATA = _.cloneDeep(sellerInfo);
    //     const res = await spIM.reStartConv({ _cid: sellerInfo.spClusterCode, _tid: sellerInfo.spTenantId, salesUnitId: sellerInfo.spUnitId, teamId: converse.result.data.tid, salesCustSvcerId: LOGINDATA.id, sessionId: sellerInfo.spSessionId });
    //     expect(res.result).to.includes({ msg: '成功' });
    // });
});

// 未激活/已注册没有覆盖
describe('买家咨询失败情况统计', async function () {
    this.timeout(30000);
    let consultFailInfo;
    before(async function () {
        await spReq.spSellerLogin({ shopName: '中洲店' });
        consultFailInfo = getConsultFailInfo({ sellerInfo: LOGINDATA });

        await spAuth.staffLogin();
        const dataList = await spCommon.findSpConsultFailCount({ sellerShopName: '中洲店' }).then(res => res.result.data.rows);
        const failInfo = dataList.find(val => val.tenantId == consultFailInfo.tenantId);
        expect(failInfo, '未找到中洲店的失败统计').not.to.be.undefined;
        consultFailInfo.update(failInfo);
    });
    describe('im未安装', async function () {
        it('校验失败信息', async function () {
            await spReq.spClientLogin();
            await consultFailInfo.startConv();

            await spAuth.staffLogin();
            const dataList = await spCommon.findSpConsultFailCount({ sellerShopName: '中洲店' }).then(res => res.result.data.rows);
            const data = dataList.find(val => val.tenantId == consultFailInfo.tenantId);
            common.isApproximatelyEqualAssert(consultFailInfo, data);
        });
    });
});

function getConsultFailInfo({ sellerInfo }) {
    const ConsultFailInfo = class {
        constructor({ sellerInfo }) {
            this.id = '';
            this.sellerShopName = sellerInfo.shopName;
            this.clusterCode = sellerInfo.clusterCode;
            this.tenantId = sellerInfo.tenantId;
            this.unitId = sellerInfo.unitId;
            this.flag = 0;
            this.consultCount = 0;
            this.createdBy = '';
            this.createdDate = '';
            this.imFlag = 0;
            this.remark = '';
            this.updatedBy = '';
            this.latestConsultTime = '';
        }
        update(failInfo) {
            common.update(this, failInfo);
            return this;
        }
        /**
         * 买家发起沟通
         */
        async startConv() {
            const converseRes = await spIM.startConv({ _cid: this.clusterCode, _tid: this.tenantId, buyerId: LOGINDATA.userId, salesUnitId: this.unitId, check: false })
                .then(res => res.result);
            if (converseRes.code < 0 && converseRes.msgId == 'has_no_custsvcer') {
                this.consultCount++;
                this.latestConsultTime = common.getCurrentTime();
            }
        }
    };
    return new ConsultFailInfo({ sellerInfo });
};