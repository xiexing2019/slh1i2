const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spActManager = require('../../help/spActManager');
const spReq = require('../../help/spReq');
const esSearchHelp = require('../../help/esSearchHelp');

describe('活动搜索', async function () {
    this.timeout(30000);

    before(async function () {
        // await spReq.spClientLogin();
    });

    describe('批量查询商品最优活动', async function () {
        // 正在进行中的活动列表 活动对应的频道ids  最优活动查询结果
        let actList = [], channelIds = [], dresOptimalActList = [];
        before(async function () {
            await spAuth.staffLogin();
            // 查询进行中的活动 并保存活动频道id
            actList = await spAct.getActList({ flag: 4 }).then(res => {
                const list = res.result.data.rows;
                expect(list, `目前没有进行中的活动 跳过验证`).to.have.lengthOf.above(0);
                channelIds = list.map(act => act.channelId);
                return list;
            });

            await spReq.spClientLogin();
            const spuIds = await spAct.getActChannelSearchDres({ channelId: actList[0].channelId, flag: 1, pageNo: 1, pageSize: 3 }).then(res => res.result.data.rows.map(row => row.spuId));

            const dresList = await spDresb.searchDres({ queryType: 0, keyWords: { id: spuIds } }).then(res => {
                return res.result.data.dresStyleResultList;
            });
            // console.log(`dresList=${JSON.stringify(dresList)}`);

            const params = dresList.map(dres => { return { spuId: dres.id, spuPrice: dres.pubPrice, channelIds: dres.channelIds.split(/\s/) } });//.replace(/\s/g, ',')
            dresOptimalActList = await spAct.findDresOptimalAct(params).then(res => res.result.data.rows);
            // console.log(`res=${JSON.stringify(dresOptimalActList)}`);
        });
        it('活动校验', async function () {
            for await (const actDres of dresOptimalActList) {
                const effectChannelIds = common.takeWhile(actDres.channelIds, (channelId) => channelIds.includes(channelId));
                // 没有参与活动
                if (effectChannelIds.length == 0) {
                    expect(data).not.to.have.property('optimalAct');
                } else {
                    let optimalAct = {}, minPrice = 0;
                    for (const effectChannelId of effectChannelIds) {
                        const dresInfo = await spAct.getActChannelSearchDres({ channelId: effectChannelId, spuId: actDres.spuId }).then(res => res.result.data.rows[0]);
                        if (minPrice > dresInfo.pubPrice || minPrice == 0) {
                            minPrice = dresInfo.pubPrice;
                            optimalAct = dresInfo;
                        }
                    }
                    // console.log(`\noptimalAct=${JSON.stringify(optimalAct)}`);
                    // console.log(`\nactDres=${JSON.stringify(actDres)}`);
                    expect(actDres).to.includes({ optimalPrice: optimalAct.pubPrice, spuId: actDres.spuId });
                    if (minPrice < actDres.spuPrice) {
                        expect(actDres).to.includes({ tagId: optimalAct.actId });
                        common.isApproximatelyArrayAssert(optimalAct, actDres.optimalAct);
                    }
                }
            }
        });
    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2322.html
    // 暂时以批量查询校验 等限时活动脚本维护好后，改为正常流程校验  2.1.8  19-04-25
    describe('商品详情中增加最优活动信息和对应的活动价格', async function () {
        let dresList = [], dresOptimalActList = [];
        before(async function () {
            await spReq.spClientLogin();
            const actList = await spAct.getActList({ flag: 4 }).then(res => {
                const list = res.result.data.rows;
                expect(list, `目前没有进行中的活动 跳过验证`).to.have.lengthOf.above(0);
                return list;
            });
            const spuIds = await spAct.getActChannelSearchDres({ channelId: actList[0].channelId, flag: 1, pageNo: 1, pageSize: 10 }).then(res => res.result.data.rows.map(row => row.spuId));

            dresList = await spDresb.searchDres({ queryType: 0, keyWords: { id: spuIds } }).then(res => {
                return res.result.data.dresStyleResultList;
            });
            // console.log(`dresList=${JSON.stringify(dresList)}`);

            const params = dresList.map(dres => { return { spuId: dres.id, spuPrice: dres.pubPrice, channelIds: dres.channelIds.split(/\s/) } });//.replace(/\s/g, ',')
            dresOptimalActList = await spAct.findDresOptimalAct(params).then(res => res.result.data.rows);
            // console.log(`res=${JSON.stringify(dresOptimalActList)}`);
        });
        it('查询商品详情', async function () {
            for (let index = 0; index < dresList.length; index++) {
                const dresSpu = dresList[index];
                const dresFull = await spReq.getFullForBuyerByUrl({ detailUrl: dresSpu.detailUrl }).then(res => res.result.data);
                if (dresOptimalActList[index].hasOwnProperty('optimalAct')) {
                    const optimalAct = dresOptimalActList[index].optimalAct;
                    // console.log(`\n\noptimalAct=${JSON.stringify(optimalAct)}`);
                    expect(dresFull.spu.bestAcct).to.includes({ haveAcctFlag: true, tagId: optimalAct.id, accName: optimalAct.actName });//tagKind:optimalAct.,
                } else {
                    expect(dresFull.spu.bestAcct).to.includes({ haveAcctFlag: false });
                }
            }
        });
    });

    describe.skip('首页活动top3查询', async function () {
        before(async function () {
            await spAuth.staffLogin();
        });
        it('19-普通活动(商品集)', async function () {
            const act = await spAct.getActList({ actSort: 1 }).then(res => res.result.data.rows[0]);

            const top3List = await spAct.findHomeShowDres([{ type: 19, typeObjId: act.id }]).then(res => res.result.data.rows[0].details);
            const dresList = await spAct.getActChannelSearchDres({ channelId: act.channelId, flag: 1, pageNo: 1, pageSize: 3 })
                .then(res => res.result.data.rows.map(dres => { return { name: dres.name, title: dres.title, pubPrice: dres.pubPrice } }));
            common.isApproximatelyArrayAssert(dresList, top3List, [], ``);
        });
        it('13-今日新款', async function () {
            await spReq.spClientLogin();

            const dresList = await spDresb.searchDres({ queryType: 2, pageNo: 1, pageSize: 3 })
                .then(res => res.result.data.dresStyleResultList.map(dres => { return { name: dres.name, title: dres.title, pubPrice: dres.pubPrice } }));
            console.log(`dresList=${JSON.stringify(dresList)}`);
            const top3List = await spAct.findHomeShowDres([{ type: 13, typeObjId: LOGINDATA.tenantId }]).then(res => res.result.data.rows[0].details);
            console.log(`\ntop3List=${JSON.stringify(top3List)}`);
            common.isApproximatelyArrayAssert(dresList, top3List);
        });
    });



});