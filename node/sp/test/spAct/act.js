const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spActManager = require('../../help/spActManager');
const spReq = require('../../help/spReq');
const esSearchHelp = require('../../help/esSearchHelp');

/**
 * 活动  
 * spg spactg.act_info
 * http://c.hzdlsoft.com:7082/Wiki.jsp?page=Event
 * 
 * 需要门店 常青店，中洲店，文一店 (审核/测试环境已维护)
 * 
 * tips
 * 1.新增活动会同时新增一个频道
 * 2.免租店铺 分润比例初始化时为0
 */
describe('活动-平台组织', async function () {
    this.timeout(30000);
    let sellerInfo;
    const myAct = new spActManager();
    before('创建活动', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 新增频道时需要 风格 主营类目
        // await spReq.getDictList(['theme', 'masterClass']);

        await myAct.getSignUpRuleMap();
        myAct.mockRandomAct().setSignUpRule('avgDresPicCount > 2');//feeType == 1;

        // 新增活动
        await myAct.saveAct();
        // console.log(myAct);
    });

    describe('活动暂未开始', async function () {
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
        it('校验活动状态-买家端', async function () {
            const res = await spAct.checkActStatus({ actId: myAct.id });
            common.isApproximatelyEqualAssert(myAct.getStatus(), res.result.data);
        });
    });

    describe('更新活动信息', async function () {
        before(async function () {
            await myAct.mockRandomAct().saveAct();
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
    });

    describe('活动频道商品', async function () {
        let spuIds = [];
        before('频道商品批量上架', async function () {
            await spAuth.staffLogin();
            const dresList = await spDresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, pageSize: 10, pageNo: 1 }).then(res => res.result.data.dresStyleResultList);
            expect(dresList, `es商品搜索未找到tenantId=${sellerInfo.tenantId}的商品，请检查`).to.have.lengthOf.above(0);
            myAct.addSpus(dresList);
            spuIds = [...myAct.spus.keys()];
            await spdchg.saveChannelDetail({ channelId: myAct.channelId, spuIds });
        });
        it('查看频道信息', async function () {
            const channelInfo = await spdchg.getChannelById({ id: myAct.channelId }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.channelInfo.fullExp(), channelInfo);
        });
        it('查询活动频道商品', async function () {
            const spuId = spuIds.shift();
            const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, spuId }).then(res => res.result.data.rows);
            expect(dresList.find(dres => dres.spuId == spuId), `活动频道商品中未找到商品 spuId=${spuId}`).not.to.be.undefined;
        });
        it('批量查询活动商家分成比例', async function () {
            const dresInfo = myAct.spus.get(spuIds.shift());
            const tenantShareRatio = await spAct.findTenantShareRatio({ actTenants: [{ actId: myAct.id, spuId: dresInfo.id, tenantId: dresInfo.tenantId }] }).then(res => res.result.data.rows);
            expect(tenantShareRatio.shift()).to.includes({ actId: myAct.id, tenantId: dresInfo.tenantId, profitRate: myAct.profitRate });
        });
        describe('排序校验', async function () {
            it('view_num 倒序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'view_num', orderByDesc: true }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'viewNum', true);
            });
            it('view_num 升序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'view_num', orderByDesc: false }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'viewNum', false);
            });
            it('pur_price 倒序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'pur_price', orderByDesc: true }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'pubPrice', true);
            });
            it('pur_price 升序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'pur_price', orderByDesc: false }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'pubPrice', false);
            });
            it('praise_num 倒序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'praise_num', orderByDesc: true }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'praiseNum', true);
            });
            it('praise_num 升序', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, orderBy: 'praise_num', orderByDesc: false }).then(res => res.result.data.rows);
                esSearchHelp.sortAssert(dresList, 'praiseNum', false);
            });
        });
        describe('修改活动商品状态-待审核', async function () {
            let spuId;
            before(async function () {
                spuId = spuIds[0];
                await myAct.changeActDresAuditFlag({ spuId, flagToChange: 2 });
            });
            it('查询活动商品信息', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: 2, spuId }).then(res => res.result.data.rows);
                const dresInfo = dresList.find(dres => dres.spuId == spuId);
                common.isApproximatelyEqualAssert(myAct.spus.get(spuId).getChannelDresInfo(), dresInfo);
            });
            it('查询es商品信息', async function () {
                const dresList = await spDresb.searchDres({ queryType: 4, keyWords: { id: [spuIds[0]] } }).then(res => res.result.data.dresStyleResultList);
                const dresInfo = dresList.find(dres => dres.id == spuIds[0]);
                expect(dresInfo.validFlag, `修改活动频道的商品时,影响到了正常商品的状态`).to.equal(1);
            });
        });
        describe('修改活动商品状态-手动下架', async function () {
            before(async function () {
                await myAct.changeActDresAuditFlag({ spuId: spuIds[0], flagToChange: -1 });
            });
            it('查询活动商品信息', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: -1, spuId: spuIds[0] }).then(res => res.result.data.rows);
                const dresInfo = dresList.find(dres => dres.spuId == spuIds[0]);
                common.isApproximatelyEqualAssert(myAct.spus.get(spuIds[0]).getChannelDresInfo(), dresInfo);
            });
            it('查询es商品信息', async function () {
                const dresList = await spDresb.searchDres({ queryType: 4, keyWords: { id: [spuIds[0]] } }).then(res => res.result.data.dresStyleResultList);
                const dresInfo = dresList.find(dres => dres.id == spuIds[0]);
                expect(dresInfo.validFlag, `修改活动频道的商品时,影响到了正常商品的状态`).to.equal(1);
            });
        });
        describe('修改活动商品状态-正常/上架', async function () {
            before(async function () {
                await myAct.changeActDresAuditFlag({ spuId: spuIds[0], flagToChange: 1 });
            });
            it('查询活动商品信息', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: 1, spuId: spuIds[0] }).then(res => res.result.data.rows);
                const dresInfo = dresList.find(dres => dres.spuId == spuIds[0]);
                common.isApproximatelyEqualAssert(myAct.spus.get(spuIds[0]).getChannelDresInfo(), dresInfo);
            });
            it('查询es商品信息', async function () {
                const dresList = await spDresb.searchDres({ queryType: 4, keyWords: { id: [spuIds[0]] } }).then(res => res.result.data.dresStyleResultList);
                const dresInfo = dresList.find(dres => dres.id == spuIds[0]);
                expect(dresInfo.validFlag, `修改活动频道的商品时,影响到了正常商品的状态`).to.equal(1);
            });
        });
        describe('修改活动商品状态-下架', async function () {
            before(async function () {
                await myAct.changeActDresAuditFlag({ spuId: spuIds[0], flagToChange: 0 });
            });
            it('查询活动商品信息', async function () {
                const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: 0, spuId: spuIds[0] }).then(res => res.result.data.rows);
                const dresInfo = dresList.find(dres => dres.spuId == spuIds[0]);
                common.isApproximatelyEqualAssert(myAct.spus.get(spuIds[0]).getChannelDresInfo(), dresInfo);
            });
            it('查询es商品信息', async function () {
                const dresList = await spDresb.searchDres({ queryType: 4, keyWords: { id: [spuIds[0]] } }).then(res => res.result.data.dresStyleResultList);
                const dresInfo = dresList.find(dres => dres.id == spuIds[0]);
                expect(dresInfo.validFlag, `修改活动频道的商品时,影响到了正常商品的状态`).to.equal(1);
            });
        });
    });

    describe.skip('分润比率', async function () {
        let actShopId;
        before(async function () {
            const shopList = await spAct.getListByActId({ actId: myAct.id }).then(res => res.result.data.rows);
            expect(shopList, `根据活动id获取门店列表 actId=${myAct.id} 无数据返回 请检查`).to.have.lengthOf.above(0);
            actShopId = shopList[0].id;
            await spAct.updateProfitRate({ id: actShopId, actId: myAct.id, profitRate: myAct.profitRate });
        });
        it('根据活动id获取门店列表', async function () {
            const shopList = await spAct.getListByActId({ actId: myAct.id }).then(res => res.result.data.rows);
            expect(shopList.find(val => val.id == actShopId).profitRate).to.equal(myAct.profitRate);
        });
    });

    describe('更新活动状态-活动进行中', async function () {
        before('修改活动状态', async function () {
            await spAuth.staffLogin();
            await myAct.updateActFlag(4);
            // console.log(myAct);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
        it('校验活动状态-买家端', async function () {
            const res = await spAct.checkActStatus({ actId: myAct.id });
            common.isApproximatelyEqualAssert(myAct.getStatus(), res.result.data);
        });
        it('查询活动频道商品', async function () {
            const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId });
            // console.log(`dresList=${JSON.stringify(dresList)}`);
        });
    });

    describe('更新活动状态-活动结束', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await myAct.updateActFlag(5);
            // console.log(myAct);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
        it('校验活动状态-买家端', async function () {
            const res = await spAct.checkActStatus({ actId: myAct.id });
            common.isApproximatelyEqualAssert(myAct.getStatus(), res.result.data);
        });
    });

    describe('更新活动状态-删除', async function () {
        before(async function () {
            await myAct.updateActFlag(-1);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
            // expect(actInfo, `活动列表依然显示被删除的活动 actId=${myAct.id}`).to.be.undefined;
        });
        it('校验活动状态-买家端', async function () {
            const res = await spAct.checkActStatus({ actId: myAct.id });
            common.isApproximatelyEqualAssert(myAct.getStatus(), res.result.data);
        });
        it('商品channelIds校验', async function () {
            const spuIds = [...myAct.spus.keys()];
            const dresList = await spDresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, keyWords: { id: spuIds } }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => {
                // console.log(`dres=${JSON.stringify(dres)}`);
                expect(dres.channelIds).not.to.includes(myAct.channelId);
            });
        });
    });
});

describe.skip('活动-商家报名', async function () {
    this.timeout(30000);
    const myAct = new spActManager();
    let sellerInfo, sellerInfoB, sellerInfoC;

    before('创建活动', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spReq.spSellerLogin({ shopName: '中洲店' });
        sellerInfoB = _.cloneDeep(LOGINDATA);

        await spReq.spSellerLogin({ shopName: '文一店' });
        sellerInfoC = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        await spReq.getDictList(['theme', 'masterClass']);

        await myAct.getSignUpRuleMap();
        myAct.mockRandomAct().setSignUpRule('avgDresPicCount > 2');//feeType == 1;
        myAct.orgType = 2;

        // 新增活动
        await myAct.saveAct();
        // 调试用
        // const actDetail = await spAct.getActDetail({ id: 22 }).then(res => res.result.data);
        // myAct.updateActInfo(actDetail);
        console.log(myAct);
    });
    after(async function () {
        if (myAct.id) await myAct.updateActFlag(-1);
    });

    it('查看活动详情', async function () {
        const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
    });

    it('活动列表-平台端', async function () {
        const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
        const actInfo = actList.find(val => val.id == myAct.id);
        common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
    });

    describe('更新活动信息', async function () {
        before(async function () {
            await myAct.mockRandomAct().saveAct();
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
    });

    //活动门店列表接口需要等服务端新增
    //ec-actg-actInfo-checkEntryLimit 返回结果有误 等小邱排查
    describe('卖家门店报名活动', async function () {
        before(async function () {
            //组织形式为商家报名 才走这个流程
            if (myAct.orgType != 2) this.test.skip();

            await spReq.spSellerLogin();
            await spAct.sellerShopSignUpAct({ actId: myAct.id, shopId: sellerInfo.tenantId });

            await spAuth.staffLogin();
        });
        it('活动门店报名规则检查', async function () {
            const res = await spAct.checkEntryLimit({ id: myAct.id, sallerShopId: sellerInfo.tenantId });
            console.log(`res=${JSON.stringify(res)}`);

        });
        it('检查活动门店', async function () {
            const shopList = await spAct.getActShopList({ actId: myAct.id });
            console.log(`shopList=${JSON.stringify(shopList)}`);
        });
    });

    // 缺少校验接口
    describe.skip('绑定活动门店', async function () {
        let sellerInfoB, sellerInfoC;
        before(async function () {
            await spAuth.staffLogin();
        });
        after(async function () {
            const shopList = await spAct.getActShopList({ actId: myAct.id });
            console.log(`shopList=${JSON.stringify(shopList)}`);
        });
        it('绑定门店', async function () {
            await myAct.rebindShops({ shopId: `${sellerInfoB.tenantId},${sellerInfoC.tenantId}` });
            const shopList = await spAct.getActShopList({ actId: myAct.id, flag: 2 });
            console.log(`\nshopList=${JSON.stringify(shopList)}`);

            const shopList2 = await spAct.getActShopList({ actId: myAct.id, flag: 1 });
            console.log(`\nshopList2=${JSON.stringify(shopList2)}`);

        });
        it('解绑门店', async function () {
            await myAct.rebindShops({ shopId: `${sellerInfoB.tenantId}` });

            const shopList = await spAct.getActShopList({ actId: myAct.id, flag: 2 });
            console.log(`\nshopList=${JSON.stringify(shopList)}`);

            const shopList2 = await spAct.getActShopList({ actId: myAct.id, flag: 1 });
            console.log(`\nshopList2=${JSON.stringify(shopList2)}`);

            const shopList3 = await spAct.getActShopList({ actId: myAct.id, flag: -1 });
            console.log(`\nshopList3=${JSON.stringify(shopList3)}`);

            const shopList4 = await spAct.getActShopList({ actId: myAct.id, flag: 0 });
            console.log(`\nshopList4=${JSON.stringify(shopList4)}`);
        });
    });

    describe('活动频道商品', async function () {
        before('', async function () {
            await spAuth.staffLogin();

            const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId });
            console.log(`dresList=${JSON.stringify(dresList)}`);
        });
        it('修改活动商品状态', async function () {
            // const dresList = await spAct.changeActDresAuditFlag({ channelId: myAct.channelId, orderBy: 'marketDate', orderByDesc: true });
            // console.log(`dresList=${JSON.stringify(dresList)}`);
        });
    });

    describe('更新活动状态-活动进行中', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await myAct.updateActFlag(4);
            // console.log(myAct);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
    });

    describe('更新活动状态-活动结束', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await myAct.updateActFlag(5);
            // console.log(myAct);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
        it('活动列表-平台端', async function () {
            const actList = await spAct.getActList({ actNameLike: myAct.actName, orgType: myAct.orgType, flag: myAct.flag }).then(res => res.result.data.rows);
            const actInfo = actList.find(val => val.id == myAct.id);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actInfo);
        });
        it.skip('查询频道状态', async function () {
            // const 
        });
    });

});

