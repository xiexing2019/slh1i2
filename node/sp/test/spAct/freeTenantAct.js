const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
// const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spReq = require('../../help/spReq');
const spActManager = require('../../help/spActManager');
const shopManager = require('../../help/shopManager');
// const billManage = require('../../help/billManage');
// const dresManage = require('../../help/dresManage');

// http://c.hzdlsoft.com:7082/Wiki.jsp?page=FreeTenantAndLowPriceAct
// 免租卖家 + 活动 
// 免租店铺 es搜索受限

describe('低价区活动-免租商户', async function () {
    this.timeout(30000);
    const freeTenant = new shopManager(), myAct = new spActManager();

    before(async function () {
        await spReq.spSellerLogin();// {shopName:'中洲店'}
        await freeTenant.getShopInfo({ tenantId: LOGINDATA.tenantId });
        if (freeTenant.platCommissionRatio == 0 || freeTenant.typeId != 1) {
            await spugr.onlineShopById({ id: freeTenant.id, shopTypeId: 1, shopPlatRatio: '5.5', forceFlag: true, autoTestToken: 'autotest' });
            freeTenant.typeId = 1;
            freeTenant.platCommissionRatio = 0.055;
        };

        await spAuth.staffLogin();
        await myAct.getSignUpRuleMap();
        myAct.mockRandomAct().setSignUpRule('avgDresPicCount > 2');
        myAct.filterShopType = 1;// 店铺类型(0:年租店铺 1:免租店铺)

        // 新增活动
        await myAct.saveAct();
        // console.log(myAct);
    });

    after('数据还原', async function () {
        // 将常青店改回年租
        await spugr.onlineShopById({ id: freeTenant.id, shopTypeId: 0, shopPlatRatio: 0, forceFlag: true, autoTestToken: 'autotest' });
    });

    it('免租商户信息校验', async function () {
        const shopInfo = await spugr.getShop({ id: freeTenant.id });
        common.isApproximatelyEqualAssert(freeTenant, shopInfo);
    });

    it('查看活动详情', async function () {
        const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
    });

    describe('免租店铺上架商品', async function () {
        let spuId;
        before(async function () {
            await spReq.spSellerLogin();
            spuId = await spReq.saveDresFull().then(res => res.result.data.spuId);

            await spReq.spClientLogin();
        });
        it('全局商品搜索', async function () {
            const dresList = await spDresb.searchDres({ queryType: 0, keyWords: { id: spuId } }).then(res => res.result.data.dresStyleResultList);
            expect(dresList, `全局商品搜索不能查询到免租店铺的商品 现在可以查到 spuId=${spuId}`).to.have.length(0);
        });
        it.skip('店铺搜索', async function () {
            const shopList = await spugr.findShopBySearchToken({ searchToken: freeTenant.name }).then(res => res.result.data.rows);
            expect(shopList, `店铺搜索不能搜索到免租店铺 现在可以查到 tenantId=${freeTenant.id}`).to.have.length(0);
        });
        it('活动商品频道', async function () {
            const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId });
            // console.log(`dresList=${JSON.stringify(dresList)}`);
        });
    });

    describe('买家下单', async function () {
        before(async function () {
            await spReq.spClientLogin();

        });
    });

    describe('活动结束', async function () {
        before(async function () {
            await spAuth.staffLogin();
            await myAct.updateActFlag(5);
        });
        it('查看活动详情', async function () {
            const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
        });
    });

});
