const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spActManager = require('../../help/spActManager');
const spReq = require('../../help/spReq');
const esSearchHelp = require('../../help/esSearchHelp');

describe('活动-15天内有线下销量', async function () {
    this.timeout(30000);
    let sellerInfo;
    const myAct = new spActManager();
    before('创建活动', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 新增频道时需要 风格 主营类目
        // await spReq.getDictList(['theme', 'masterClass']);

        await myAct.getSignUpRuleMap();
        myAct.mockRandomAct({ filterSlhSalesNums15: 1, filterShopIds: sellerInfo.tenantId }).setSignUpRule('avgDresPicCount > 2');//feeType == 1;

        // 新增活动
        await myAct.saveAct();
        console.log(myAct.actName);

        // 等待频道商品初始化
        await common.delay(10000);
    });

    after('删除活动', async function () {
        await myAct.updateActFlag(-1);
    });

    it('查询活动频道商品', async function () {
        const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, spuFlag: 1 }).then(res => res.result.data.rows);
        esSearchHelp.orderAssert({ dataList: dresList, path: 'slhSalesNums15', orderByDesc: true })
    });

});


describe('活动-30天内有线下销量', async function () {
    this.timeout(30000);
    let sellerInfo;
    const myAct = new spActManager();
    before('创建活动', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 新增频道时需要 风格 主营类目
        // await spReq.getDictList(['theme', 'masterClass']);

        await myAct.getSignUpRuleMap();
        myAct.mockRandomAct({ filterSlhSalesNums30: 1, filterShopIds: sellerInfo.tenantId }).setSignUpRule('avgDresPicCount > 2');//feeType == 1;

        // 新增活动
        await myAct.saveAct();
        console.log(myAct.actName);

        // 等待频道商品初始化
        await common.delay(10000);
    });

    after('删除活动', async function () {
        await myAct.updateActFlag(-1);
    });

    it('查询活动频道商品', async function () {
        const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, spuFlag: 1 }).then(res => res.result.data.rows);
        esSearchHelp.orderAssert({ dataList: dresList, path: 'slhSalesNums15', orderByDesc: true })
    });

});