const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spbi = require('../../../reqHandler/sp/global/spbi');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spActManager = require('../../help/spActManager');
const spReq = require('../../help/spReq');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const esSearchHelp = require('../../help/esSearchHelp');

/**
 * 限时活动  
 */
describe('限时活动-平台组织', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [];
    const myAct = new spActManager();
    before('创建活动', async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 新增频道时需要 风格 主营类目
        // await spReq.getDictList(['theme', 'masterClass']);

        // 新增活动
        await myAct.mockRandomAct({ typeId: 2, filterShopIds: sellerInfo.tenantId, filterMarketDateBeign: common.getDateString([0, -1, 0]) })
            .saveAct({ purLimitType: 2, purLimitNum: 5 });
        // console.log(myAct);
        //活动频道商品初始化较慢
        await common.delay(1000);
    });

    it('获取限时购列表', async function () {
        await myAct.getActFlashSaleList();
        // console.log(`res=${JSON.stringify(res)}`);
    });

    it('活动详情', async function () {
        const actDetail = await spAct.getActDetail({ id: myAct.id }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(myAct.getDetail(), actDetail);
    });

    it('后台查询活动频道商品', async function () {
        this.retries(10);
        await common.delay(2000);

        dresList = await spdchg.getActChannelDresList({ channelId: myAct.channelId, flag: 1, spuFlag: 1, shadowAll: true, pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
        expect(dresList, `未查询到活动商品`).to.have.lengthOf.above(0);
    });

    it('添加商品至活动', async function () {
        if (!dresList || dresList.length == 0) {
            dresList = await spDresb.searchDres({ queryType: 0, tenantId: sellerInfo.tenantId, pageNo: 1, pageSize: 5 }).then(res => res.result.data.dresStyleResultList);
            await spdchg.saveBatchActDetail({ actIds: [myAct.id], spuIds: dresList.map(dres => dres.spuId) });
            await common.delay(500);

            dresList = await spdchg.getActChannelDresList({ channelId: myAct.channelId, flag: 1, spuFlag: 1, shadowAll: true, pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
        }
    });

    it('添加商品至购物车', async function () {
        // console.log(`dresList[0]=${JSON.stringify(dresList[0])}`);
        await spReq.spClientLogin();
        await spTrade.emptyCart({ check: false });
        const json = mockJsonParam.cartJsonByAct({ dres: dresList[0], count: 2 });
        await spTrade.saveCartInBatchs(json);
    });

    it('限时活动商品配置', async function () {
        await spAuth.staffLogin();
        // console.log(`dresList=${JSON.stringify(dresList[0])}`);
        for (let index = 0; index < dresList.length; index++) {
            const spu = dresList[index];
            console.log(`spuId=${spu.spuId}  title=${spu.title}`);

            const params = {
                spuId: spu.spuId,
                spuTenantId: spu.tenantId,
                channelId: myAct.channelId,
                jsonParam: myAct.flashList.map((data, index) => { return { flashSaleId: data.id, actId: myAct.id, spuId: spu.spuId, showOrder: 1, limitNum: 100, fakeOrderNum: 1, specPrice: common.sub(spu.pubPrice, 10), announce: 1, flag: 1 } }),
            };
            console.log('%j', params.jsonParam);
            await myAct.saveActFlashSaleDetail(params);
        }
    });

    it('购物车查询商品', async function () {
        await spReq.spClientLogin();
        const cartList = await spTrade.showCartList().then(res => res.result.data.rows);
        // console.log('%j', cartList);
        cartList.forEach(val => {
            val.carts.forEach(cart => {
                const spu = myAct.flashList.find(flash => flash.spuId == cart.spuid);
                // spu && expect(cart).to.includes({ skuPrice: spu.specPrice });
            })
        });
    });

    it('查询商品已配置限时抢购信息', async function () {
        await spAuth.staffLogin();
        const _dresList = await spdchg.getActChannelDresList({ channelId: myAct.channelId, flag: 1, spuFlag: 1, shadowAll: true, pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
        // console.log(`dresList[0]=${JSON.stringify(_dresList[0])}`);
    });

    it('活动最优价校验', async function () {
        await common.delay(10000);
        const _dresList = await spDresb.searchDres({ queryType: 0, keyWords: { id: dresList.map(dres => dres.spuId) } }).then(res => res.result.data.dresStyleResultList);
        // console.log(`_dresList=${JSON.stringify(_dresList)}`);

    });

    it('活动商品修改优先值', async function () {
        const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: 1, shadowAll: true }).then(res => res.result.data.rows);
        const dres = dresList[0];
        const res = await spdchg.updateChannelDetail({ id: dres.id, channelId: myAct.channelId, purLimitNum: 10 });
        console.log(`res=${JSON.stringify(res)}`);

    });

    it('在秒杀时间内-判断限时秒杀支付是否在有效时间内', async function () {
        const actId = myAct.typeId == 2 ? myAct.flashList[0].id : myAct.id;
        const params = dresList.map(dres => { return { spuId: dres.spuId, actId, actKind: myAct.typeId } });
        const data = await spAct.checkPayIntime(params).then(res => res.result.data);
        expect(data).to.includes({ success: true });
    });

    it('在秒杀时间外-判断限时秒杀支付是否在有效时间内', async function () {
        const actId = myAct.typeId == 2 ? myAct.flashList[1].id : myAct.id;
        const params = dresList.map(dres => { return { spuId: dres.spuId, actId, actKind: myAct.typeId } });
        const data = await spAct.checkPayIntime(params).then(res => res.result.data);
        expect(data).to.includes({ success: false, errorCode: 'act_flash_time_is_not_start' });
    });
    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2633.html
    it('查询活动频道商品-排序校验', async function () {
        const dresList = await spAct.getActChannelSearchDres({ channelId: myAct.channelId, flag: 1, shadowAll: true }).then(res => res.result.data.rows);
        // console.log(`dresList=${JSON.stringify(dresList)}`);
    });

    it.skip('活动概览', async function () {
        const res = await spbi.getActOverview({ tagKind: myAct.typeId, typeId: myAct.flashList[0].id });
        console.log(`res=${JSON.stringify(res)}`);
    });

    it.skip('获取活动订单明细表', async function () {
        const res = await spbi.getActPurBillDetail({ tagKind: myAct.typeId, typeId: myAct.flashList[0].id });
        console.log(`res=${JSON.stringify(res)}`);
    });

    describe('活动商品设置限购', async function () {
        before(async function () {

        });
        it('', async function () {

        });
    });

    describe('更新活动状态-停用', async function () {
        before(async function () {
            await myAct.updateActFlag(0);
        });
        it('在秒杀时间内-判断限时秒杀支付是否在有效时间内', async function () {
            const actId = myAct.typeId == 2 ? myAct.flashList[0].id : myAct.id;
            const params = dresList.map(dres => { return { spuId: dres.spuId, actId, actKind: myAct.typeId } });
            const data = await spAct.checkPayIntime(params).then(res => res.result.data);
            expect(data).to.includes({ success: true });
        });
    });

    describe('更新活动状态-结束', async function () {
        before(async function () {
            await myAct.updateActFlag(5);
        });
        it('在秒杀时间内-判断限时秒杀支付是否在有效时间内', async function () {
            const actId = myAct.typeId == 2 ? myAct.flashList[0].id : myAct.id;
            const params = dresList.map(dres => { return { spuId: dres.spuId, actId, actKind: myAct.typeId } });
            const data = await spAct.checkPayIntime(params).then(res => res.result.data);
            expect(data).to.includes({ success: true });
        });
    });

});