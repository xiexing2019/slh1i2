const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
// const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spbi = require('../../../reqHandler/sp/global/spbi');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spActManager = require('../../help/spActManager');
const spReq = require('../../help/spReq');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const esSearchHelp = require('../../help/esSearchHelp');

/**
 * http://zentao.hzdlsoft.com:6082/zentao/story-view-2154.html  
 */
describe('今日降价专区', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [];
    const myAct = new spActManager();
    before('创建活动', async function () {
        // await spReq.spSellerLogin();
        // sellerInfo = _.cloneDeep(LOGINDATA);

        await spAuth.staffLogin();
        // 新增频道时需要 风格 主营类目
        // await spReq.getDictList(['theme', 'masterClass']);

        // 新增活动
        await myAct.mockRandomAct({ typeId: 1, filterReducePriceFlag: 1, filterMarketDateBeign: common.getDateString([0, -1, 0]) })
            .saveAct({ purLimitType: 2, purLimitNum: 5 });
        // console.log(myAct);
        //活动频道商品初始化较慢
        await common.delay(1000);
    });

    after(async function () {
        await myAct.updateActFlag(5);
    });

    it('后台查询活动频道商品', async function () {
        this.retries(10);
        await common.delay(2000);

        dresList = await spdchg.getActChannelDresList({ channelId: myAct.channelId, flag: 1, spuFlag: 1, shadowAll: true, pageNo: 1, pageSize: 5 }).then(res => res.result.data.rows);
        expect(dresList, `未查询到活动商品`).to.have.lengthOf.above(0);
        // console.log(`dresList[0]=${JSON.stringify(dresList[0])}`);
    });



});