const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spAct = require('../../../reqHandler/sp/global/spAct');
const spdchg = require('../../../reqHandler/sp/global/spdchg');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');


describe('活动商品', async function () {
    this.timeout(TESTCASE.timeout);

    before(async function () {
        await spAuth.staffLogin();
    });

    // http://zentao.hzdlsoft.com:6082/zentao/task-view-2985.html
    describe('货品批量加入多个活动', async function () {
        let actList = [], spuIds = [];
        before(async function () {
            actList = await spAct.getActList({ pageNo: 1, pageSize: 3 }).then(res => res.result.data.rows.map(act => { return { actId: act.id, channelId: act.channelId } }));
            console.log('%s:%j', '活动列表', actList);

            spuIds = await spDresb.searchDres({ pageNo: 1, pageSize: 3 }).then(res => res.result.data.dresStyleResultList.map(dres => dres.spuId));
            console.log('%s:%j', 'spuIds', spuIds);

            await spdchg.saveBatchActDetail({ actIds: actList.map(act => act.actId), spuIds: spuIds });
        });
        it('全局商品搜索-校验扩展字段channelIds', async function () {
            this.retries(2);
            await common.delay(500);
            const dresList = await spDresb.searchDres({ keyWords: { id: spuIds } }).then(res => res.result.data.dresStyleResultList);
            const channelIds = actList.map(act => act.channelId.toString());
            dresList.forEach(dres => {
                expect(dres.channelIds.split(/\s/)).to.include.members(channelIds);
            });
        });
        it('查询活动频道商品', async function () {
            this.retries(2);
            await common.delay(500);
            for (let index = 0; index < actList.length; index++) {
                const act = actList[index];
                const dresList = await spAct.getActChannelSearchDres({ spuId: spuIds[0], channelId: act.channelId, flag: 1, shadowAll: true }).then(res => res.result.data.rows);
                expect(dresList).to.have.length.above(0);
            }
        });
    });

});