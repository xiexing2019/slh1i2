const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
// const esSearch = require('../../reqHandler/sp/biz_server/esSearch');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spExp = require('../../help/getExp');
const dresManage = require('../../help/dresManage');

describe('同步', async function () {
    this.timeout(TESTCASE.timeout);
    before(async function () {
        await spReq.spSellerLogin();
    });

    describe('最低价状态同步', async function () {
        before(async function () {
            const dresList = await spdresb.searchDres({ queryType: 0, tenantId: LOGINDATA.tenantId, pageNo: 1, pageSize: 20 })
                .then(res => res.result.data.dresStyleResultList.map(dres => {
                    return {
                        unitId: dres.unitId,
                        spuId: dres.spuId,
                        reducePriceDate: common.getCurrentTime(),
                        reducePriceFlag: 1,
                    }
                }));
            console.log(dresList);
            await spdresb.syncReducePrice(dresList);
        });
        it('', async function () {
            // spu.props
            // {"markFlag": 1, "slhPrice": 200, "channelIds": "0,14,1106,1154,1198,1206,1202,1210,1454,1458,1462,1466,1517,1693,1701,1705,1697,1689,1709,1713,1777,1773,1925,1837,1869,1949", "oriClassId": 1085, "platSeason": "", "actHistoryNum": 20, "constructPrice": 200, "reducePriceDate": "2019-05-24 17:25:40", "reducePriceFlag": 1, "oriPropChangedSet": 1, "spPubPriceShowFlag": 1}
        });
    });


});