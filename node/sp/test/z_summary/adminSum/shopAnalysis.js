
const spbi = require('../../../../reqHandler/sp/global/spbi');
const spReq = require('../../../help/spReq');
const common = require('../../../../lib/common');
const caps = require('../../../../data/caps');
const spAccount = require('../../../data/spAccount');
const esSearchHelp = require('../../../help/esSearchHelp');

describe('店铺数据统计', function () {
    this.timeout(30000);
    let saveRes, shopInfo;
    before('管理员登录', async function () {
        if (!spAccount.slhConSp) {
            this.skip();
        }

        await spReq.loginGoodShopBySlh();
        shopInfo = LOGINDATA;
        caps.updateEnvByName({ name: caps.envName });
        await spReq.spClientLogin();
        saveRes = await spbi.saveSpShopData([saveJson(shopInfo)]);
    });

    it('查询店铺分析', async function () {
        let res = await spbi.getSpShopData({ name: shopInfo.shopName });
        let info = res.result.data.rows.find(val => val.tenantId == shopInfo.tenantId);
        common.isApproximatelyEqualAssert(saveRes.params.jsonParam[0], info);
    });

    it('排序校验', async function () {
        let orders = ['avgPrice', 'newRate', 'totalNum', 'overMaxPriceNum', 'lessMinPriceNum', 'normalPriceNum',
            'premiumRate', 'raiseRate'];
        let orderByDesc = false;
        if (common.getRandomNum(0, 1) == 1) {
            orderByDesc = true
        };
        let res = await spbi.getSpShopData({ pageSize: 20, pageNo: 1, orderBy: orders[common.getRandomNum(0, orders.length - 1)], orderByDesc });
        esSearchHelp.orderAssert({ dataList: res.result.data.rows, path: res.params.jsonParam.orderBy, orderByDesc });
    });
});


function saveJson(shopInfo) {
    let json = {
        avgPrice: common.getRandomNum(200, 400),
        epId: shopInfo.epid,
        name: shopInfo.shopName,
        newRate: Math.random().toFixed(2),
        totalNum: common.getRandomNum(20, 30),
        overMaxPriceNum: common.getRandomNum(10, 20),
        lessMinPriceNum: common.getRandomNum(10, 20),
        normalPriceNum: common.getRandomNum(10, 20),
        premiumRate: Math.random().toFixed(2),
        raiseRate: Math.random().toFixed(2),
        tenantId: shopInfo.tenantId,
        useSpPrice: common.getRandomNum(0, 1),//0,非好店价，1 好店价
        spPriceRaiseRate: common.getRandomNum(0, 0.2, 2),//好店价加价率
    };
    return json;
};