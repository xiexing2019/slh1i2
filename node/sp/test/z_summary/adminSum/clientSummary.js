const spAuth = require('../../../../reqHandler/sp/confc/spAuth');
const common = require('../../../../lib/common');
const format = require('../../../../data/format');
const mockJsonParam = require('../../../help/mockJsonParam');
const spCommon = require('../../../../reqHandler/sp/global/spCommon');
const spbi = require('../../../../reqHandler/sp/global/spbi');
const spTrade = require('../../../../reqHandler/sp/biz_server/spTrade');
const spAccount = require('../../../data/spAccount');
const spbiHelp = require('../../../help/spbi/spbiHelp');
const spReq = require('../../../help/spReq');
const mysql = require('../../../../reqHandler/sp/mysql');
const spdresb = require('../../../../reqHandler/sp/biz_server/spdresb');
const wmsWarehouse = require('../../../../reqHandler/sp/wms/wmsWarehouse');

describe('统计分析', function () {
    this.timeout(30000);
    before(async function () {
        //管理员登录
        await spAuth.staffLogin();
        // LOGINDATA.sessionId = 'ctr-55210A70-5772-3946-0201-F016D8496176';
    });

    describe('买家数据统计', function () {
        let custAddExp;
        let proDateGte = common.getDateString([0, 0, -3]);
        let proDateLte = common.getCurrentDate();
        before(async () => {
            //统计面板和买家数据查询-按地区，查询条件是一周
            custAddExp = await getSpgRegInfo({ queryDate: proDateGte });
            // console.log(`custAddExp=${JSON.stringify(custAddExp)}`);

        });
        it('统计面板--自定义一周统计', async function () {
            //自定义 一周
            this.test.isWarn = true;
            let clientSummary = await spbi.getBuyersAnalysis({ timeKind: 5, proDateGte, proDateLte });

            common.isApproximatelyEqualAssert({ custAdd: custAddExp.passNum, totalCustAdd: custAddExp.totalPassNum }, clientSummary.result.data);
            // common.isFieldsEqualAssert({ custAdd: custAddExp.passNum, totalCustAdd: custAddExp.totalPassNum }, clientSummary.result.data, 'custAdd;totalCustAdd');
        });
        it.skip('统计面板--自定义-', async function () {
            //管理员登录
            await spAuth.staffLogin();
            //获取按周
            let clientSummary = await spbi.getBuyersAnalysis({ timeKind: 1, buyerNameLike: spAccount.client.mobile });
            console.log(`clientSummary.result.data=${JSON.stringify(clientSummary.result.data)}`);
            await spReq.spClientLogin();
            let purBillList = await spTrade.purFindBills({ pageSize: 0, pageNo: 1, orderBy: 'id', orderByDesc: true });
            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let expList = spbiHelp.getDataListByDate({ dataList: purBillList.result.data.rows, keyPath: 'bill.payTime', proDateGte: common.getCurrentDate(), proDateLte: common.getDateString([0, 0, 1]) });
            // console.log(`exp=${JSON.stringify(exp)}`);
            // console.log(`exp[0]=${JSON.stringify(exp[0])}`);
            // "totalMoney": "合计拿货金额",
            //  "totalNum": "合计拿货件数",
            //   "totalCount": "合计拿货次数"
            let exp = {
                totalMoney: 0,
                totalNum: 0,
                totalCount: expList.length
            };
            expList.forEach((ele) => {
                exp.totalMoney = common.add(exp.totalMoney, ele.bill.totalMoney);
                exp.totalNum = common.add(exp.totalNum, ele.bill.totalNum);
            });
            console.log(`exp=${JSON.stringify(exp)}`);
            common.isApproximatelyEqualAssert(exp, clientSummary.result.data);

        });
        it('买家数据查询--按地区', async function () {
            this.test.isWarn = true;
            let clientSummaryByRegion = await spbi.buyerAnalysisByArea({ timeKind: 5, proDateGte, proDateLte });
            //查询日期内邀请认证审核通过的买家
            // console.log(`clientSummaryByRegion=${JSON.stringify(clientSummaryByRegion.result.data)}`);
            // console.log(`custAddExp.provCodeNum=${JSON.stringify(custAddExp.provCodeNum)}`);
            common.isApproximatelyArrayAssert(custAddExp.provCodeNum, clientSummaryByRegion.result.data.rows);
        });
        it('统计面板', async function () {
            this.test.isWarn = true;
            //管理员登录
            await spAuth.staffLogin();
            //买家数据查询-按时间
            let clientSummaryByTime = await spbi.purAnalysisByTimeSlot({ timeKind: 1, buyerNameLike: spAccount.client.mobile });
            // console.log(`clientSummaryByTime=${JSON.stringify(clientSummaryByTime)}`);
            expect(clientSummaryByTime.result.data.rows.length).to.above(0);
            //买家数据查询
            let clientQuery = await spbi.conQueryByBuyer({ dataType: 2, orderType: 1, searchType: 1, buyerNameLike: spAccount.client.mobile, month: '11' });
            // console.log(`clientQuery=${JSON.stringify(clientQuery)}`);
            expect(clientQuery.result.data.rows.length).to.above(0);
            //买家数据查询按消费
            let clientByConsumption = await spbi.conDataAnalysisByBuyer({ timeKind: 1, buyerNameLike: spAccount.client.mobile });
            // console.log(`clientByConsumption=${JSON.stringify(clientByConsumption)}`);
            expect(clientByConsumption.result.data.rows.length).to.above(0);
            //买家数据查询按地区
            let clientByArea = await spbi.buyerAnalysisByArea({ timeKind: 1 });
            // console.log(`clientByArea=${JSON.stringify(clientByArea)}`);
            expect(clientByArea.result.data.rows.length).to.above(0);
        });
    });

    // http://c.hzdlsoft.com:7082/Wiki.jsp?page=Sp-static
    describe('整体分析/转化率', () => {
        let buyerAnalysisExp;
        before(async () => {
            buyerAnalysisExp = await getBuyerAnalysisExp();
            // console.log(`buyerAnalysisExp=${JSON.stringify(buyerAnalysisExp)}`);
        });
        it.skip('注册买家', async function () {
            // 注册买家,使用邀请码,直接通过审核
            await spReq.spLogin({ code: `12${common.getRandomNumStr(9)}` });
            const areaJson = await mockJsonParam.getAreaJson();
            let auditjson = mockJsonParam.submitAuditJson({ areaJson });
            auditjson.inviteCode = 'autotest';
            submitAuditRes = await spCommon.submitAuditRecord(auditjson);
        });
        it('统计面板', async function () {
            this.test.isWarn = true;
            const analysisOverall = await spbi.buyerAnalysisOverall({ timeKind: 1 }).then(res => res.result.data);
            // console.log(`\nanalysisOverall=${JSON.stringify(analysisOverall)}`);
            common.isApproximatelyEqualAssert(buyerAnalysisExp, analysisOverall, ['totalRegNum', 'totalApplyAuditNum', 'totalPassNum', 'totalHaveOrderNum', 'totalRegConverRate', 'totalApplyAuditConverRate', 'totalPassConverRate']);
            // common.isFieldsEqualAssert(buyerAnalysisExp, analysisOverall, 'regNum;totalRegNum;applyAuditNum;totalApplyAuditNum;passNum;totalPassNum;firstCreateOrderNum;totalHaveOrderNum;regConverRate;totalRegConverRate;applyAuditConverRate;totalApplyAuditConverRate;passConverRate;totalPassConverRate;haveOrderConverRate');
        });
        it('列表', async function () {
            this.test.isWarn = true;
            const analysisDaily = await spbi.buyerAnalysisDaily({ timeKind: 1 });
            // console.log(`列表\nanalysisDaily=${JSON.stringify(analysisDaily)}`);
            let analysisDailyListExp = format.dataFormat(buyerAnalysisExp, 'applyAuditConverRate;regConverRate;createOrderConverRate=haveOrderConverRate');
            // console.log(`analysisDailyListExp=${JSON.stringify(analysisDailyListExp)}`);
            analysisDailyListExp.passConverRate = !buyerAnalysisExp.firstCreateOrderNum || !buyerAnalysisExp.passNum ? 0 : common.div(buyerAnalysisExp.firstCreateOrderNum, buyerAnalysisExp.passNum).toFixed(4);
            // common.isFieldsEqualAssert(analysisDailyListExp, analysisDaily.result.data.rows[0], ';;;');
            common.isApproximatelyEqualAssert(analysisDailyListExp, analysisDaily.result.data.rows[0]);
        });

    });

    describe('商品动态', function () {
        let newDresData;
        before('获取今日上新统计', async function () {
            await spReq.spClientLogin();
            newDresData = await spdresb.getTodayNewDresData({ shadowFlag: 0 });

        });

        //一直报错就找祝朝阳，先观察几天
        it('验证数据', async function () {
            this.test.isWarn = true;
            common.isApproximatelyEqualAssert(newDresData.result.data, await getTodayNewDresData());
        });
    });

    describe('特定认证来源统计', async function () {
        const summitDateGte = common.getDateString([0, 0, -4]), summitDateLte = common.getCurrentDate();
        it('查询特定认证来源的买家店铺统计数据', async function () {
            const res = await spCommon.getQRAuditBuyerStatistic({ summitDateGte, summitDateLte });

            const spgMysql = await mysql({ dbName: 'spgMysql' });
            const totalCount = await spgMysql.query(`select count(id) from spadmin.sp_audit where audit_src in(4,5)`).then(res => res[0][0]['count(id)']);
            const selectDayCount = await spgMysql.query(`select count(id) from spadmin.sp_audit where audit_src in(4,5) and created_date >= '${summitDateGte}'`).then(res => res[0][0]['count(id)']);
            await spgMysql.end();

            expect(res.result.data, `统计数据与数据库中的数据不一致`).to.eql({ totalCount, selectDayCount });
        });
        describe('查询特定认证来源的买家店铺列表', async function () {
            let obj = { 0: '已提交', 1: '已通过', 9: '驳回', 99: '未认证' };
            for (const flag in obj) {
                it(`${flag}-${obj[flag]}`, async function () {
                    const auditSrcs = common.randomSort([0, 1, 2, 3, 4, 5]).slice(0, 3);
                    const res = await spCommon.findAuditSrcBuyerList({ flag, summitDateGte, summitDateLte, auditSrcs });
                    const auditList = await getSpAuditListFromDB({ sqlLimit: `where flag=${flag} and created_date>='${summitDateGte}' and audit_src in(${auditSrcs.join(',')})` });
                    common.isApproximatelyEqualAssert({ total: auditList.length, count: auditList.length, rows: auditList }, res.result.data);
                });
            }
        });
    });

    describe('仓库绑定市场列表', async function () {
        it('logisticsRelShopCount校验', async function () {
            const spgMysql = await mysql({ dbName: 'spgMysql' });
            const markets = await spgMysql.query(`select id,flag, market_id as marketId, wh_tenant_id as whTenantId, wh_unit_id as whUnitId from spugr.wms_wh_market where flag=1 order by id asc `).then(res => res[0].slice(0, 10));
            for (let index = 0; index < markets.length; index++) {
                const element = markets[index];
                element.logisticsRelShopCount = await spgMysql.query(`select count(id) from spugr.sp_shop where logistics_market_id =${element.marketId}  and flag=1`).then(res => res[0][0]['count(id)']);
            }
            await spgMysql.end();

            const wareHouseMarketList = await wmsWarehouse.getWareHouseMarket({ flag: 1, pageSize: 20 }).then(res => res.result.data.rows);
            markets.forEach(market => {
                const actual = wareHouseMarketList.find(val => val.id == market.id);
                expect(actual, `仓库绑定市场列表中未找到:${JSON.stringify(market)}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(market, actual);
            });
        });
    });

});

/**
 * 获取审核表信息
 * @param {*} params 
 */
async function getSpAuditListFromDB({ sqlLimit = '' }) {
    const spgMysql = await mysql({ dbName: 'spgMysql' });
    // shopName ->sp_buyer_shop
    const [reg] = await spgMysql.query(`select id as auditId,audit_obj_id as auditObjId,audit_src as auditSrc,
    audit_src_id as auditSrcId,created_date as createdDate,flag from spadmin.sp_audit ${sqlLimit} order by created_date desc`);
    await spgMysql.end();
    return reg;
};

/**
 * 获取注册人数
 * 
 */
async function getSpgRegInfo(params = { queryDate: common.getCurrentDate() }) {
    const spgMysql = await mysql({ dbName: 'spgMysql' });
    // 获取x时间段内注册人数
    const [reg] = await spgMysql.query(`select id from spugr.sp_tenant where type_id=1 and created_date>='${params.queryDate}'`);
    const regNum = reg.length;

    // 获取总注册人数
    const [totalReg] = await spgMysql.query(`select id from spugr.sp_tenant where type_id=1`);
    const totalRegNum = totalReg.length;

    // 当前申请认证用户数
    const [applyAudit] = await spgMysql.query(`select * from spadmin.sp_audit where created_date>='${params.queryDate}'`);
    const applyAuditNum = applyAudit.length;

    // 累计申请认证用户数
    const [totalApplyAudit] = await spgMysql.query(`select * from spadmin.sp_audit`);
    const totalApplyAuditNum = totalApplyAudit.length;

    // 当前认证通过用户数
    const [passAudit] = await spgMysql.query(`select * from spadmin.sp_audit where flag=1 and updated_date>='${params.queryDate}'`);//common.getCurrentDate()
    const passNum = passAudit.length;

    // 累计认证通过用户数
    const [totalPassAudit] = await spgMysql.query(`select * from spadmin.sp_audit where flag=1`);
    const totalPassNum = totalPassAudit.length;

    //按地区查询认证通过的用户数
    const [provCodeNum] = await spgMysql.query(`SELECT prov_code as provCode,COUNT(*)AS totalCount FROM spadmin.sp_buyer_shop t1 ,spadmin.sp_audit t2 where t1.id=t2.audit_obj_id and t1.audit_flag=1 and t2.finish_date>='${params.queryDate}'  GROUP BY prov_code  ORDER BY totalCount DESC`);

    // 关闭链接
    spgMysql.end();
    return { regNum, totalRegNum, applyAuditNum, totalApplyAuditNum, passNum, totalPassNum, provCodeNum };
};

/**
 * 获取下单用户数
 */
async function getPurBillList(params = { queryDate: common.getCurrentDate() }) {
    const spbMysql = await mysql({ dbName: 'spbMysql' });
    // 获取x时间段内首次下单用户数
    const [firstCreateOrder] = await spbMysql.query(`select buyer_id from spsales001.sp_sales_bill  GROUP BY buyer_id 
    HAVING DATE_FORMAT(min(created_date),'%Y-%m-%d') >= DATE_FORMAT('${params.queryDate}','%Y-%m-%d')`);
    const firstCreateOrderNum = firstCreateOrder.length;

    // 付款成功数
    const [firstPayOrder] = await spbMysql.query(`select buyer_id from spsales001.sp_sales_bill where pay_flag = 1 and pay_time is not null GROUP BY buyer_id 
    HAVING DATE_FORMAT(min(pay_time),'%Y-%m-%d') >=DATE_FORMAT('${params.queryDate}','%Y-%m-%d')`);
    const firstPayNum = firstPayOrder.length;

    // 获取累计创建订单用户数
    const [totalHaveOrder] = await spbMysql.query(`select distinct buyer_id from spsales001.sp_sales_bill`);
    const totalHaveOrderNum = totalHaveOrder.length;
    // 获取累计下单用户数（支付过的用户）
    const [totalPayOrder] = await spbMysql.query(`select distinct buyer_id from spsales001.sp_sales_bill where pay_flag=1 and pay_time is not null`);
    const totalPayNum = totalPayOrder.length;

    // 关闭链接
    spbMysql.end();
    return { firstCreateOrderNum, totalHaveOrderNum, firstPayNum, totalPayNum };
};

/**
 * 拼接整体分析期望值
 * @param {object} params 
 */
async function getBuyerAnalysisExp(params) {
    // spg买家注册信息,spb采购单列表
    const [spgRegInfo, purBillList] = await Promise.all([getSpgRegInfo(), getPurBillList()]);

    let exp = {
        regNum: spgRegInfo.regNum, //当前注册用户数
        totalRegNum: spgRegInfo.totalRegNum, // 累计注册用户数
        applyAuditNum: spgRegInfo.applyAuditNum, // 当前申请认证用户数
        totalApplyAuditNum: spgRegInfo.totalApplyAuditNum, // 累计申请认证用户数
        passNum: spgRegInfo.passNum, // 当前认证通过用户数
        totalPassNum: spgRegInfo.totalPassNum, // 累计认证通过用户数
        firstCreateOrderNum: purBillList.firstPayNum, // 首次下单用户数
        totalHaveOrderNum: purBillList.totalPayNum, // 累计下单用户数
    };

    // 1、注册转化率 = 申请认证数 ÷ 注册数，注册日期限定在选定日期内，申请认证日期不限。
    // 当前注册转化率
    exp.regConverRate = !exp.applyAuditNum || !exp.regNum ? 0 : common.div(exp.applyAuditNum, exp.regNum).toFixed(4);
    // 累计注册转化率
    exp.totalRegConverRate = !exp.totalApplyAuditNum || !exp.totalRegNum ? 0 : common.div(exp.totalApplyAuditNum, exp.totalRegNum).toFixed(4);
    // 2、申请认证转化率 = 认证通过数 ÷ 申请认证数，申请认证日期限定在选定日期内，认证通过日期不限。
    // 当前申请认证转化率
    exp.applyAuditConverRate = !exp.passNum || !exp.applyAuditNum ? 0 : common.div(exp.passNum, exp.applyAuditNum).toFixed(4);
    // 累计申请认证转化率
    exp.totalApplyAuditConverRate = !exp.totalPassNum || !exp.totalApplyAuditNum ? 0 : common.div(exp.totalPassNum, exp.totalApplyAuditNum).toFixed(4);
    // 3、认证通过转化率 = 今天认证通过并且发起订单 数÷  认证通过数，认证通过日期限定在选定日期内，发起订单日期不限。
    // 当前认证通过转化率
    exp.passConverRate = !purBillList.firstCreateOrderNum || !exp.passNum ? 0 : common.div(purBillList.firstCreateOrderNum, exp.passNum).toFixed(4);
    // 累计认证通过转化率
    exp.totalPassConverRate = !purBillList.totalHaveOrderNum || !exp.totalPassNum ? 0 : common.div(purBillList.totalHaveOrderNum, exp.totalPassNum).toFixed(4);
    // 4、下单转化率 = 付款成功数 ÷ 发起订单数，发起订单日期限定在选定日期内，付款成功日期不限。
    // 当前下单转化率  
    exp.haveOrderConverRate = !purBillList.firstPayNum || !purBillList.firstCreateOrderNum ? 0 : common.div(purBillList.firstPayNum, purBillList.firstCreateOrderNum).toFixed(4);
    // 累计下单转化率
    exp.totalHaveOrderConverRate = !purBillList.totalPayNum || !purBillList.totalHaveOrderNum ? 0 : common.div(purBillList.totalPayNum, purBillList.totalHaveOrderNum).toFixed(4);

    return exp;
};

async function getTodayNewDresData() {
    const spgMysql = await mysql({ dbName: 'spbMysql' });

    const [res] = await spgMysql.query(`select * from spdresb001.dres_spu where market_date>='${common.getCurrentDate()}'`);
    const todayNewTotalCount = res.length;
    const [res1] = await spgMysql.query(`select * from spdresb001.dres_spu where market_date>='${common.getCurrentDate()}' and flag=1`);
    const todayNewOnMarketCount = res1.length;
    //断开连接
    spgMysql.end();
    return { todayNewTotalCount, todayNewOnMarketCount, todayNewOffMarketCount: todayNewTotalCount - todayNewOnMarketCount };
}