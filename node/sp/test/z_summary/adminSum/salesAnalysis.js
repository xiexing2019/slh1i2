const spbi = require('../../../../reqHandler/sp/global/spbi');
const spAuth = require('../../../../reqHandler/sp/confc/spAuth');
const spAccount = require('../../../data/spAccount');


describe('订单统计', function () {
    this.timeout(30000);
    before('管理员登录', async function () {
        await spAuth.staffLogin();
    });

    it('订单统计--统计面板', async () => {
        let res = await spbi.salesAnalysisOverall({ timeKind: 1, sellerNameLike: spAccount.seller.shopName });
        // console.log(`订单统计--统计面板res=${JSON.stringify(res)}`);
        expect(res.result.data).not.to.be.undefined;

    });
    it('订单统计--按时段', async function () {
        this.test.isWarn = true;
        let res = await spbi.salesAnalysisByTimeSlot({ timeKind: 1, sellerNameLike: spAccount.seller.shopName });
        expect(res.result.data.rows.length).to.above(0);
        // await spReq.spSellerLogin();
        // let salesList = await spTrade.salesFindBills({ pageSize: 100, pageNo: 1, orderBy: 'payTime', orderByDesc: true, statusType: 0 });
        // let dataList = salesList.result.data.rows;
        // let exp = spbiHelp.getDataListByDate({ dataList, keyPath: 'bill.payTime', proDateGte: common.getCurrentDate(), proDateLte: common.getDateString([0, 0, 1]) });
    });

    it('订单统计--按销售', async function () {
        this.test.isWarn = true;
        let res = await spbi.salesAnalysisBySeller({ timeKind: 1, sellerNameLike: spAccount.seller.shopName });
        // console.log(`\n订单统计--按销售res=${JSON.stringify(res)}`);
        expect(res.result.data.rows.length).to.above(0);
    });
});
