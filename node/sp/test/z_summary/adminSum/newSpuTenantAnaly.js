const spAuth = require('../../../../reqHandler/sp/confc/spAuth');
const common = require('../../../../lib/common');
const spbi = require('../../../../reqHandler/sp/global/spbi');
const mysql = require('../../../../reqHandler/sp/mysql');

// 上新货品 统计所有符合条件的货品，与是否上架无关
describe('上新统计', async function () {
    this.timeout(30000);

    // 当前时间段 -> 校验当天的统计数据是否正确 
    const date = common.getCurrentDate();
    let spgMysql;

    before(async function () {
        await spAuth.staffLogin();
        spgMysql = await mysql({ dbName: 'spgMysql' });
    });
    after(async function () {
        await spgMysql.end();
    });


    describe('上新门店统计', async function () {
        it('当天数据校验', async function () {
            const shopList = await spbi.newSpuTenantAnalys({ firstMarketDateGte: date, firstMarketDateLte: date }).then(res => res.result.data);

            const [expArr] = await spgMysql.query(`SELECT DISTINCT shop_unit_id as shopUnitId FROM spbig.dres_spu_set WHERE first_market_date >= '${date} 00:00:00'`);
            common.isApproximatelyArrayAssert(expArr, shopList.rows);
            expect(shopList.total).to.equal(expArr.length);
        });
    });

    describe('上新货品列表', async function () {
        let newShopList;
        before(async function () {
            // 上新门店
            newShopList = await spbi.newSpuTenantAnalys({ firstMarketDateGte: date, firstMarketDateLte: date }).then(res => res.result.data.rows);
            if (newShopList.length == 0) {
                console.log(`无今日上新门店,跳过上新货品列表校验`);
                this.skip();
            }
        });
        // 
        it('款号校验', async function () {
            for (let index = 0; index < newShopList.length; index++) {
                const newShop = newShopList[index];

                let totalPageNo = 0;
                const spuIds = await spbi.tenantSpuList({ firstMarketDateGte: date, firstMarketDateLte: date, shopUnitId: newShop.shopUnitId, pageSize: 20, pageNo: 1 })
                    .then(res => {
                        const data = res.result.data;
                        totalPageNo = Math.ceil(data.total / 20);
                        return data.rows.map(spu => spu.id);
                    });
                for (let pageNo = 2; pageNo <= totalPageNo; pageNo++) {
                    await spbi.tenantSpuList({ firstMarketDateGte: date, firstMarketDateLte: date, shopUnitId: newShop.shopUnitId, pageSize: 20, pageNo })
                        .then(res => res.result.data.rows.forEach(spu => spuIds.push(spu.id)));
                }

                const expSpuIds = await spgMysql.query(`SELECT id FROM spbig.dres_spu_set WHERE shop_unit_id = ${newShop.shopUnitId} AND first_market_date >= '${date} 00:00:00'`)
                    .then(res => res[0].map(data => data.id));
                common.isApproximatelyArrayAssert(expSpuIds, spuIds);
            }
        });
    });

    describe('查询卖家店铺数据分析', async function () {
        it('数据校验', async function () {
            const analyseOverAll = await spbi.newSpuTenantAnalysOverall({ firstMarketDateGte: date, firstMarketDateLte: date }).then(res => res.result.data);

            // 上新店铺数（时间段内） 
            const newTenantCount = await spgMysql.query(`SELECT COUNT(DISTINCT shop_unit_id) as num FROM spbig.dres_spu_set WHERE first_market_date >= '${date} 00:00:00'`).then(res => res[0][0].num);
            // 上新数（时间段内）
            const newSpuCount = await spgMysql.query(`SELECT COUNT(DISTINCT id) as num FROM spbig.dres_spu_set WHERE first_market_date >= '${date} 00:00:00'`).then(res => res[0][0].num);
            // 总共上新店铺数
            const totalNewTenantCount = await spgMysql.query(`SELECT COUNT(DISTINCT shop_unit_id) as num FROM spbig.dres_spu_set`).then(res => res[0][0].num);
            // 总共上新数
            const totalNewSpuCount = await spgMysql.query(`SELECT COUNT(DISTINCT id) as num FROM spbig.dres_spu_set`).then(res => res[0][0].num);

            expect(analyseOverAll).to.include({
                newTenantCount,
                newSpuCount,
                totalNewSpuCount,
                totalNewTenantCount
            });
        });
    });

});