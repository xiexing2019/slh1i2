const spAuth = require('../../../../reqHandler/sp/confc/spAuth');
const common = require('../../../../lib/common');
const spbi = require('../../../../reqHandler/sp/global/spbi');
const mysql = require('../../../../reqHandler/sp/mysql');

// 上新货品 统计所有符合条件的货品，与是否上架无关
describe('商品统计快照', async function () {
    this.timeout(30000);
    let spgMysql;

    before(async function () {
        await spAuth.staffLogin();
        spgMysql = await mysql({ dbName: 'spgMysql' });
    });
    after(async function () {
        await spgMysql.end();
    });

    const arr = [{ name: '商品数', key: 'total_num', dataType: 1 },
    { name: '新增商品数', key: 'add_num', dataType: 2 },
    { name: '被查看商品数', key: 'view_num', dataType: 3 },
    { name: '被收藏商品数', key: 'favor_num', dataType: 4 },
    { name: '分享商品数', key: 'share_num', dataType: 5 },
    { name: '加购商品数', key: 'add_cart_num', dataType: 6 },
    { name: '动销商品数', key: 'sales_num', dataType: 7 }];

    describe('商品统计快照', async function () {
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            it(element.name, async function () {
                const data = await spbi.findSpuSnapshot({ dateType: 1, dataType: element.dataType, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() }).then(res => res.result.data);
                const sum = await spgMysql.query(`SELECT SUM(${element.key}) as totalNum FROM spbig.sp_spu_snapshot WHERE date='${common.getCurrentDate()}'`).then(res => res[0]);
                expect(data.rows[0].value).to.equal(Number(sum[0].totalNum));
            });
        }
    });

    describe('商品标签组统计快照', async function () {
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            it(element.name, async function () {
                const data = await spbi.findSpuSnapshot({ dateType: 1, dataType: element.dataType, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() }).then(res => res.result.data);
                const sum = await spgMysql.query(`SELECT SUM(${element.key}) as totalNum FROM spbig.sp_spu_labels_snapshot WHERE date='${common.getCurrentDate()}'`).then(res => res[0]);
                expect(data.rows[0].value).to.equal(Number(sum[0].totalNum));
            });
        }
    });

});