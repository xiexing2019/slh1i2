const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const common = require('../../../lib/common');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const caps = require('../../../data/caps');
const clientManager = require('../../help/clientManager');
const fs = require('fs');
const path = require('path');

const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')));

//买家分享
describe('商品门店分享-online', async function () {
    this.timeout(TESTCASE.timeout);

    let sellerInfo, clientInfo;
    before(async function () {
        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        await spReq.spClientLogin();
        clientInfo = _.cloneDeep(LOGINDATA);
    });

    describe('门店分享', async function () {
        it('通过门店分享信息生成口令+通过口令获取门店分享信息', async function () {
            const shareRes = await spdresb.getSharePasswordByParam({ type: 102, tenantId: sellerInfo.tenantId, cid: 0, id: 0 });
            // console.log(`shareRes=${JSON.stringify(shareRes)}`);

            const detailInfo = await spdresb.getShareDtoByPassword({ password: shareRes.result.data.val });
            common.isApproximatelyEqualAssert(shareRes.params.jsonParam, detailInfo.result.data, [], `通过口令获取的门店分享信息与生成时的门店分享信息不一致`);
        });
        it('通过门店分享信息生成口令二维码', async function () {
            await spdresb.getShareQrCodeByParam({ type: 102, tenantId: sellerInfo.tenantId, cid: 0, id: 0 });
        });
        it('获取门店分享短信内容', async function () {
            const res = await spdresb.getShopShareSMS({ shopId: sellerInfo.tenantId });
            expect(res.result.data.val).to.have.string(`【商陆好店】我在商陆好店app发现了一个优质店铺[${sellerInfo.shopName}]，分享给你哦~戳http://t.cn/`);
        });
        //extProps 扩展字段 用来二维码识别之后的url拼接的参数
        it('生成商品门店小程序分享二维码的BASE64字符串', async function () {
            const res = await spdresb.getShareQrCodeBase64ForMiniProgram({ page: 'shop', tenantId: sellerInfo.tenantId, extProps: {} });
            expect(res.result.data.val).to.includes('data:image/png;base64');
        });
        // 等待祝朝阳提供链接接口
        // 暂直接新增分享记录。验证后续接口
        describe('分享访问店铺', async function () {
            before(async function () {
                await spReq.spClientLogin();
                await spdresb.createShareIn({ srcType: 2, sellerTenantId: sellerInfo.tenantId });
            });
            it('通过分享访问过的店铺', async function () {
                const shareList = await spdresb.getShareInShopList().then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(sellerInfo, shareList.find(val => val.shopId == sellerInfo.tenantId));
            });
            describe('停用分享记录', async function () {
                before(async function () {
                    await spdresb.disableShareIn({ shopId: sellerInfo.tenantId });
                });
                it('通过分享访问过的店铺', async function () {
                    this.retries(2);
                    await common.delay(500);
                    const shareList = await spdresb.getShareInShopList().then(res => res.result.data.rows);
                    expect(shareList.find(val => val.shopId == sellerInfo.tenantId), `停用后依然显示分享记录 shopId=${sellerInfo.tenantId}`).to.be.undefined;
                });
            });
        });
    });

    // 用例 http://119.3.46.172:6082/zentao/testcase-view-1902-1.html
    describe('商品分享', async function () {
        let dresInfo;
        before(async function () {
            const dres = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId }).then(res => res.result.data.dresStyleResultList.shift());
            dresInfo = await spReq.getFullForBuyerByUrl({ detailUrl: dres.detailUrl }).then(res => res.result.data);
            // console.log(`dresInfo=${JSON.stringify(dresInfo)}`);
        });
        it('通过商品分享信息生成口令+通过口令获取商品分享信息', async function () {
            const shareRes = await spdresb.getSharePasswordByParam({ type: 101, tenantId: sellerInfo.tenantId, cid: sellerInfo.clusterCode, id: dresInfo.id });

            const detailInfo = await spdresb.getShareDtoByPassword({ password: shareRes.result.data.val });
            common.isApproximatelyEqualAssert(shareRes.params.jsonParam, detailInfo.result.data, [], `通过口令获取的商品分享信息与生成时的商品分享信息不一致`);
        });
        it('通过商品分享信息生成口令二维码', async function () {
            await spdresb.getShareQrCodeByParam({ type: 101, tenantId: sellerInfo.tenantId, cid: sellerInfo.clusterCode, id: dresInfo.id });
        });
        it('生成分享商品二维码的BASE64字符串', async function () {
            const res = await spdresb.getDresShareQrCodeBase64({ _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, spuId: dresInfo.id });
            expect(res.result.data.val).to.includes('data:image/png;base64');
        });
        it('分销分享', async function () {
            // 偶发问题。添加日志排查问题
            if (!docData[caps.name]) {
                console.warn(`caps.name=${caps.name}\n${docData[caps.name]}`);
            }
            // const qrCode = await spdresb.getShareQrCodeByParam({ type: 101, tenantId: sellerInfo.tenantId, cid: sellerInfo.clusterCode, id: dresInfo.id });
            // console.log(`qrCode=${JSON.stringify(qrCode)}`);

            //buyerLogoDocId 需要生成二维码,然后上传文档服务器获取docId
            const res = await spdresb.getDresShareQrCodeBase64({
                _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode,
                spuId: dresInfo.id,
                shopName: sellerInfo.shopName,
                pubPrice: dresInfo.spu.pubPrice,
                title: dresInfo.spu.title.split('(')[0],
                docHeader: dresInfo.spu.docHeader,
                buyerLogoDocId: docData[caps.name].image.qrCode.shift().docId,
                extProps: {
                    shareType: 2,//分销分享
                    originalPrice: common.sub(dresInfo.spu.pubPrice, 10),
                    distributionPrice: common.sub(dresInfo.spu.pubPrice, 20),
                }
            });

            await common.delay(500);
            const shareList = await spdresb.searchDres({ queryType: 13, orderBy: 'createdDate', orderByDesc: true }).then(res => res.result.data.rows);
            const shareInfo = shareList[0];
            expect(shareInfo, `我的商品分享列表 未找到分享分销记录`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(res.params, shareInfo);
        });
        it('生成商品门店小程序分享二维码的BASE64字符串', async function () {
            const res = await spdresb.getShareQrCodeBase64ForMiniProgram({ page: 'goods', tenantId: sellerInfo.tenantId, spuId: dresInfo.id, extProps: {} });
            expect(res.result.data.val).to.includes('data:image/png;base64');
        });
    });

    describe('买家反馈', async function () {
        let feedBack;
        for (let functionType = 1; functionType < 4; functionType++) {
            it('买家反馈列表查看反馈信息', async function () {
                await spReq.spClientLogin();
                feedBack = new clientManager.BuyerFeedback();
                feedBack.functionType = functionType;
                feedBack.itemValue = common.getRandomNum(0, 1);
                await feedBack.save();

                await spAuth.staffLogin();
                const feeBacksList = await spUp.getBuyerFeedbacksList({ orderBy: 'createdDate', orderByDesc: true, wrapper: true }).then((res) => res.result.data.rows);
                const feeBackInfo = feeBacksList.find(val => val.id == feedBack.id);
                expect(feeBackInfo, `买家反馈列表未找到反馈数据 id=${feedBack.id}`).not.to.be.undefined;
                common.isApproximatelyEqualAssert(feedBack.getInfo(), feeBackInfo);
            });
        };
    });

});

