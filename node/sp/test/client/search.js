const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spAuth = require('../../../reqHandler/sp/confc/spAuth');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spExp = require('../../help/getExp');
const spAccount = require('../../data/spAccount');
const mysql = require('../../../reqHandler/sp/mysql');
// const nodejieba = require('nodejieba');

describe('search', function () {
    this.timeout(30000);
    let sellerInfo, clientInfo;
    before(async function () {
        //卖家登录，
        await spReq.spSellerLogin();
        sellerInfo = LOGINDATA;

        //买家登录
        await spReq.spClientLogin();
        clientInfo = LOGINDATA;
    });

    describe('买家我的数据统计-online', function () {
        let mineStat;
        before(async function () {
            await spReq.spClientLogin();
            mineStat = await spTrade.getPurStat().then(res => res.result.data);
        });

        it('关注店铺数', async function () {
            const favorShopNum = await spUp.getFavorShopList().then(res => res.result.data.count);
            expect(mineStat.favorShopNum, `关注店铺数与已关注列表结果不一致`).to.equal(favorShopNum);
        });

        it('拿货店铺数', async function () {
            const buyedShopNum = await spTrade.getRecentBuyShopList().then(res => res.result.data.count);
            expect(mineStat.buyedShopNum, `拿货店铺数与已购买过的店铺列表结果不一致`).to.equal(buyedShopNum);
        });

        it('分享商品数', async function () {
            const shareDresNum = await spdresb.searchDres({ queryType: 13 }).then(res => res.result.data.count);
            expect(mineStat.shareDresNum, `分享商品数与我的商品分享列表结果不一致`).to.equal(shareDresNum);
        });

        it('收藏商品数', async function () {
            const favorSpuNum = await spdresb.getMyFavorDres().then(res => res.result.data.favorDresStyleResultList.length);
            expect(mineStat.favorSpuNum, `收藏商品数与买家收藏列表结果不一致`).to.equal(favorSpuNum);
        });

        it('足迹数', async function () {
            if (caps.name == 'sp_online') this.skip();
            const spbSql = await mysql({ dbName: 'spbMysql' });
            const trackNum = await spbSql.query(`select count(distinct spu_id) from up001.up_buyer_view where unit_id=${LOGINDATA.unitId} and flag=1`).then(res => res[0][0]['count(distinct spu_id)']);
            await spbSql.end();
            expect(mineStat.trackNum, `足迹数与up_buyer_view中的数据不一致`).to.equal(trackNum);
        });

        describe('同一商品点赞、收藏，只能算一个看款数', function () {
            let styleRes, mineStatInfo;
            before('获取初始的买家数据', async function () {
                await spReq.spSellerLogin();
                styleRes = await spReq.saveDresFull();

                await spReq.spClientLogin();
                mineStatInfo = await spTrade.getPurStat().then(res => res.result.data);
            });

            after('下架商品', async function () {
                await spReq.spSellerLogin();
                await spdresb.offMarket({ ids: [styleRes.result.data.spuId] });
            });

            it('点赞后查看买家数据统计', async function () {
                await spReq.spClientLogin();

                //点赞 ，并且查看数+1
                await spdresb.saveBuyerPraise({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId });
                await spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 3 });

                //点赞之后应该viewSpuNum +1
                const exp = _.cloneDeep(mineStatInfo);
                exp.viewSpuNum++;
                exp.trackNum++;

                mineStatInfo = await spTrade.getPurStat().then(res => res.result.data);
                common.isApproximatelyEqualAssert(exp, mineStatInfo);
            });

            it('收藏后查看数据统计', async function () {
                await spUp.addFavorSpu({
                    spuId: styleRes.result.data.spuId,
                    spuTitle: styleRes.params.jsonParam.spu.title,
                    shopId: sellerInfo.shopId,
                    shopName: sellerInfo.shopName,
                    spuDocId: styleRes.params.jsonParam.spu.docHeader.shift(),
                });
                await spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 4 });

                const exp = _.cloneDeep(mineStatInfo);
                exp.favorSpuNum++;

                mineStatInfo = await spTrade.getPurStat().then(res => res.result.data);
                common.isApproximatelyEqualAssert(exp, mineStatInfo);
            });

            it('购买商品后查看数据统计', async function () {
                await spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 1 });
                const exp = _.cloneDeep(mineStatInfo);

                mineStatInfo = await spTrade.getPurStat().then(res => res.result.data);
                common.isApproximatelyEqualAssert(exp, mineStatInfo);
            });

            it('查看大图后查看数据统计', async function () {
                await spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 2 });
                const exp = _.cloneDeep(mineStatInfo);

                mineStatInfo = await spTrade.getPurStat().then(res => res.result.data);
                common.isApproximatelyEqualAssert(exp, mineStatInfo);
            });

            it('平均发货时间验证', async function () {
                await spReq.spClientLogin();
                let res = await spTrade.getPurStat().then(res => res.result.data);
                expect(res, '没有返回平均发货时间字段').to.include.keys('avgDeliverElapsed');
                expect(res.avgDeliverElapsed).to.be.above(0); //平均发货时间大于0
            });
        });

    });

    describe('买家查看店铺信息-online', function () {
        let qfRes, shopInfo;
        before(async function () {
            await spReq.spClientLogin();
            shopInfo = await spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
        });
        it('混合搜索店铺-市场', async function () {
            const shopList = await spugr.findShopBySearchToken({ searchToken: shopInfo.marketName }).then(res => res.result.data.rows);
            shopList.forEach(shop => expect(shop.marketName).to.includes(shopInfo.marketName));
        });

        it('买家查看卖家店铺信息', async function () {
            qfRes = await spugr.getShopSpb({ id: sellerInfo.tenantId });
        });

        it('店铺日志流查询', async function () {
            await spReq.spSellerLogin();
            const res = await spUp.getShopLogList({ flowType: 5 });
            const exp = spExp.shopLogListExp({ createdDate: qfRes.opTime, flowType: 5, clientInfo: clientInfo });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('访问了您的店铺');
        });
    });

    describe('自定义标签-online', async function () {
        let sellerInfo;
        before(async function () {
            await spReq.spSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
        });
        // 需要确认是否在线上跑  自定义标签查询需要设置，至少要跑一次
        describe('平台设置标签', async function () {
            let codeValues;
            before(async function () {
                const labels = await spugr.getDictList({ typeId: '2020', flag: 1 }).then((res) => res.result.data.rows);
                codeValues = common.randomSort(labels).slice(0, 2).map(res => { return res.codeValue });
                // console.log(`codeValues=${JSON.stringify(codeValues)}`);
            });
            it('设置自定义标签', async function () {
                await spAuth.staffLogin();
                await spugr.setShopLebal({ id: sellerInfo.tenantId, labels: codeValues.join() });

                await spReq.spSellerLogin();
                const shopInfo = await spugr.getShopDetail();
                // console.log(shopInfo);
                expect(shopInfo.result.data.labels, `${shopInfo.params.apiKey},店铺标签显示错误`).to.include.members(codeValues);
            });
        });

        describe('自定义标签查询', async function () {
            let shopLabels;
            before(async function () {
                const shopInfo = await spugr.getShopDetail().then(res => res.result.data);
                shopLabels = shopInfo.labels;

                await spReq.spClientLogin();
            });
            //查询出来记录的labels包括查询条件即可
            it('门店自定义标签搜索校验', async function () {
                let searchRes = await spugr.findShopBySearchToken({ labelsOne: shopLabels, searchToken: spAccount.seller.mobile });
                const shopInfo = searchRes.result.data.rows.find(res => res.id == sellerInfo.tenantId);
                //验证刚刚设置了labels门店的labels是否正确
                common.isApproximatelyArrayAssert(shopLabels, shopInfo.labels);
            });

            //查到的列表中所有记录的labels都应该包括查询时传的labels
            it('labeslOne查询', async function () {
                let searchRes = await spugr.findShopBySearchToken({ labelsOne: shopLabels, pageSize: 200, pageNo: 1 });

                searchRes.result.data.rows.forEach(val => {
                    expect(shopLabels.some(shopLabel => val.labels.includes(shopLabel)), `spuId:${val.id},labels:${val.labels} 不包含标签 ${shopLabels}`).to.be.true;
                });
            });

            //查询出来的记录的labels必须和查询条件一致
            it('labelsAll查询', async function () {
                const qlRes = await spugr.findShopBySearchToken({ labelsAll: shopLabels });

                //查询出来的列表中的记录，labels必须和查询时的一致
                qlRes.result.data.rows.forEach(res => {
                    // common.isApproximatelyArrayAssert(qlRes.params.jsonParam.labelsAll, res.labels, 'labelsAll查询，查询出来的记录中的labels和查询的不一致');
                    expect(res.labels, `${JSON.stringify(res)}`).to.include.members(shopLabels);
                });
            });

            //同时查询，是按照最低标准进行匹配的，也就是按照labelsOne进行匹配
            it('labelsAll,labelsOne同时查询', async function () {
                const qlRes = await spugr.findShopBySearchToken({ labelsAll: shopLabels, labelsOne: [shopLabels[0]] });
                qlRes.result.data.rows.forEach(res => {
                    expect(res.labels, 'labelsone和labelsAll同时查询，没有按照labelsone匹配').to.include.members(shopLabels);
                });
            });
        });
    });

    /**
     * http://jira.hzdlsoft.com:7082/browse/SP-5114
     */
    describe('我的足迹-online', async function () {
        let buyerViews, date = [], spuIds = [];
        before(async function () {
            await spReq.spClientLogin();

            buyerViews = await spdresb.searchDres({ queryType: 8 }).then(res => res.result.data.dresStyleResultList);
            buyerViews.forEach(val => {
                date.push(val.updatedDate.split(' ')[0]);
                spuIds.push(val.id);
            });
        });
        it('查询我的足迹', async function () {
            const dataList = await spdresb.searchDres({ queryType: 8, dateGte: date[0], dateLt: date[0] }).then(res => res.result.data.dresStyleResultList);
            dataList.forEach(val => expect(val.updatedDate).to.includes(date[0]));
        });
        it('查询足迹日期', async function () {
            const viewDate = await spUp.selectBuyerViewDate().then(res => res.result.data.rows);
            common.dedupe(date).forEach(val => expect(viewDate).to.includes(val));
        });
        describe('删除足迹', async function () {
            let delSpus = [];
            before(async function () {
                delSpus = spuIds.slice(0, 2);
                await spUp.deleteBuyerView({ ids: delSpus });
            });
            it('查询我的足迹', async function () {
                const dataList = await spdresb.searchDres({ queryType: 8 }).then(res => res.result.data.dresStyleResultList);
                dataList.forEach(val => expect(delSpus, `我的足迹中显示被删除的记录`).not.to.includes(val.id));
            });
        });
        describe('新增足迹', async function () {
            let dataList = [];
            before(async function () {
                //商品查看自增 新增记录
                await spdresb.saveBuyerView({ spuId: buyerViews[0].id, sellerId: buyerViews[0].tenantId, sellerUnitId: buyerViews[0].unitId, type: 1 });
                await spdresb.saveBuyerView({ spuId: buyerViews[1].id, sellerId: buyerViews[1].tenantId, sellerUnitId: buyerViews[1].unitId, type: 4 });
                await common.delay(1000);

                dataList = await spdresb.searchDres({ queryType: 8, dateGte: common.getCurrentDate(), dateLt: common.getCurrentDate() }).then(res => res.result.data.dresStyleResultList);
            });
            it('查询我的足迹-查看商品详情', async function () {
                const dresInfo = dataList.find(val => val.id == buyerViews[0].id);
                expect(dresInfo, `我的足迹中未找到新增的足迹 ${buyerViews[0].detailUrl}`).not.to.be.undefined;
            });
            it('查询我的足迹-商品收藏', async function () {
                const dresInfo = dataList.find(val => val.id == buyerViews[1].id);
                expect(dresInfo, `我的足迹中未找到新增的足迹 ${buyerViews[1].detailUrl}`).not.to.be.undefined;
            });
            it('查询足迹日期', async function () {
                const viewDate = await spUp.selectBuyerViewDate().then(res => res.result.data.rows);
                expect(viewDate).to.include(common.getCurrentDate());
            });
            it('商品唯一性校验', async function () {
                const dresSpuIds = dataList.map(data => data.id);
                expect(dresSpuIds.length).to.equal(common.dedupe(dresSpuIds).length);
            });
        });

    });

});
