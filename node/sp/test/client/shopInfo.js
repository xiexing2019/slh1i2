const common = require('../../../lib/common');
const spReq = require('../../help/spReq');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAdmin = require('../../../reqHandler/sp/global/spAdmin');
const spDresb = require('../../../reqHandler/sp/biz_server/spdresb');

describe('买家店铺信息', function () {
    this.timeout(30000);

    before(async function () {
        await spReq.spClientLogin();
    });

    it('获取焦点-有im信息', async function () {
        const res = await spugr.getSpStateFocus();
        common.isApproximatelyEqualAssert({ msg: '成功', data: { im: LOGINDATA.extProps.im } }, res.result);
    });

    describe('修改买家用户店铺信息', function () {
        let masterClass, standardClass;
        before(async function () {
            masterClass = await spCommon.findCatConfig({ type: 7 }).then(res => res.result.data['7'].find(val => val.flag == 1));
            // console.log(`masterClass=${JSON.stringify(masterClass)}`);
            standardClass = await spCommon.findCatConfig({ type: 13 }).then(res => res.result.data['13'].find(val => val.flag == 1));
            // console.log(`standardClass=${JSON.stringify(standardClass)}`);
        });
        for (let i = 1; i >= 0; i--) {
            let priceRange = i == 0 ? [] : [{ price1: common.getRandomNum(10, 100), price2: common.getRandomNum(101, 500) }];
            it('价格区间校验', async function () {
                await spCommon.updateBuyerShopMessage({ priceRange, masterClassId: masterClass.typeValue, standardClassId: standardClass.typeId });
                const messageRes = await spCommon.verifyShopMessage();
                // console.log(`messageRes=${JSON.stringify(messageRes)}`);
                common.isApproximatelyEqualAssert({
                    flag: i,//1代表主营类目，价格区间信息均齐全,0代表信息不齐全
                    priceRangeCompleteIs: i,//1代表价格区间信息齐全,0代表价格区间信息不齐全1代表价格区间信息齐全,0代表价格区间信息不齐全
                    masterClassIdCompleteIs: 1,//1代表主营类目信息齐全,0代表主营类目信息不齐全
                    standardClassIdCompleteIs: 1,
                }, messageRes.result.data);
            });
            it('买家查看店铺信息', async function () {
                const res = await spCommon.findShopAuditsByBuyer();
                console.log(`res=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert({ priceRange, masterClassId: masterClass.typeValue, standardClassId: standardClass.typeId }, res.result.data);
            });
        };
        //服务端暂时没做限制
        it.skip('价格区间异常校验--price1>price2', async function () {
            await spCommon.updateBuyerShopMessage({ priceRange: [{ price1: 200, price2: 100 }], masterClassId: masterClass.typeValue });
        });
    });

    describe('副营类目', async function () {
        let masterClassId, viceMasterClassId;
        before(async function () {
            await spReq.spClientLogin();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

            await spCommon.findCatConfig({ type: 7 }).then(res => {
                // console.log(`res=${JSON.stringify(res)}`);
                const data = res.result.data['7'];
                masterClassId = data[0].typeValue;
                viceMasterClassId = data.slice(1, 3).map(row => row.typeValue).join(',');
            });
            // console.log(`masterClassId=${masterClassId}  viceMasterClassId=${viceMasterClassId}`);
            await spCommon.updateBuyerShopMessage({ masterClassId, viceMasterClassId });
        });
        it('获取标准类目关系列表', async function () {
            const res = await spAdmin.getSpMainStandardClassRefList();
            // console.log(`res=${JSON.stringify(res)}`);

        });
        it('买家查看审批信息', async function () {
            const userInfo = await spCommon.findShopAuditsByBuyer();
            // console.log(`userInfo=${JSON.stringify(userInfo)}`);
            expect(userInfo.result.data).to.includes({ masterClassId, viceMasterClassId });
        });
        it('es商品搜索', async function () {
            const exp = [masterClassId, ...viceMasterClassId.split(',')];
            const dresList = await spDresb.searchDres({ queryType: 11, pageNo: 1, pageSize: 20 }).then(res => res.result.data.dresStyleResultList);
            dresList.forEach(dres => expect(exp).to.includes(dres.masterClassId));
        });
    });

});