'use strict'
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spCommon = require('../../../reqHandler/sp/global/spCommon');

describe('倾向-online', function () {
    this.timeout(30000);
    describe('用户倾向管理', function () {
        let saveTendencyRes, updateRes;
        before('保存用户倾向', async function () {

            await spReq.spClientLogin();
            saveTendencyRes = await spUp.saveUserTendency(saveTendencyJson());
        });

        it('查询倾向列表', async function () {
            let tendencyList = await spUp.getUserTendencyList({ tendencyType: saveTendencyRes.params.jsonParam.tendencyType, typeId: 0 });
            let info = tendencyList.result.data.rows.find(res => res.id == saveTendencyRes.result.data.val);
            if (info == undefined) {
                throw new Error(`用户保存了用户倾向${saveTendencyRes.params.jsonParam}.但是列表中找不到这个记录`)
            } else {
                common.isApproximatelyEqualAssert(info, saveTendencyRes.params.jsonParam);
            }
        });

        it('修改', async function () {
            updateRes = await spUp.saveUserTendency(saveTendencyJson({ id: saveTendencyRes.result.data.val }));
            let res = await spUp.getUserTendencyById({ id: saveTendencyRes.result.data.val });
            common.isApproximatelyEqualAssert(res.result.data, updateRes.params.jsonParam);
        });

        it('查询倾向详情', async function () {
            let tendencyInfo = await spUp.getUserTendencyById({ id: saveTendencyRes.result.data.val });
            common.isApproximatelyEqualAssert(tendencyInfo.result.data, updateRes.params.jsonParam);
        });

        it('删除用户倾向', async function () {
            await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val });
            let tendencyList = await spUp.getUserTendencyList({ tendencyType: updateRes.params.jsonParam.tendencyType, typeId: 0, flag: 1 });
            let info = tendencyList.result.data.rows.find(val => val.id == saveTendencyRes.result.data.val);
            expect(info, '删除用户倾向之后，再列表里面还可以查询到').to.be.undefined;

            let res = await spUp.getUserTendencyList({ tendencyType: updateRes.params.jsonParam.tendencyType, typeId: 0, flag: -1 });
            let delInfo = res.result.data.rows.find(val => val.id == saveTendencyRes.result.data.val);
            if (delInfo == undefined) {
                throw new Error('删除了用户倾向，删除列表里面没有找到这条记录')
            } else {
                common.isApproximatelyEqualAssert(delInfo, updateRes.params.jsonParam);
            };
        });
    });

    //这里暂且实现了不喜欢商品、类别的验证
    describe('设置不喜欢后es搜索', async function () {
        let tenantId;
        before('卖家买家主营类目设置一致', async function () {
            await spReq.spSellerLogin();
            tenantId = LOGINDATA.tenantId;
            let masterClassId = await spugr.getShopDetail().then(res => res.result.data.masterClassId);

            await spReq.spClientLogin();
            await spCommon.updateBuyerShopMessage({ masterClassId });
        });

        describe('不喜欢商品', function () {
            let saveTendencyRes;
            before('不喜欢商品', async function () {
                let res = await spdresb.searchDres({ pageSize: 20, pageNo: 1, queryType: 2, tenantId }).then(res => res.result.data.dresStyleResultList);
                if (res.length == 0) {
                    throw new Error('推荐上新列表里面没有商品，无法验证买家不喜欢');
                    this.skip();
                };
                saveTendencyRes = await spUp.saveUserTendency(saveTendencyJson({ tendencyType: 1, tendencyId: res[0].id }));
            });

            after('数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val, check: false });
            });

            it('不喜欢商品后查询列表', async function () {
                let res = await spdresb.searchDres({ queryType: 2, keyWords: { id: [saveTendencyRes.params.jsonParam.tendencyId] } });
                expect(res.result.data.dresStyleResultList.length, `将商品${saveTendencyRes.params.jsonParam.tendencyId}设置为不喜欢之后，此商品依然显示在列表`).to.be.equal(0);
            });

            it('删除倾向，数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val });
                let res = await spdresb.searchDres({ queryType: 2, keyWords: { id: [saveTendencyRes.params.jsonParam.tendencyId] } });
                expect(res.result.data.dresStyleResultList.length, `将不喜欢商品倾向删除后，商品依然不在es里面展示`).to.be.above(0);

            });
        });

        describe('不喜欢类别', function () {
            let saveTendencyRes;
            before('不喜欢类别', async function () {
                let searchDres = await spdresb.searchDres({ queryType: 2, pageSize: 20, pageNo: 1 }).then(res => res.result.data.dresStyleResultList);
                //设置此类别不喜欢
                saveTendencyRes = await spUp.saveUserTendency(saveTendencyJson({ tendencyType: 2, tendencyId: searchDres[0].classId }));
            });

            after('数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val, check: false });
            });

            it('设置不喜欢后查询', async function () {
                let res = await spdresb.searchDres({ queryType: 2, keyWords: { classId: [saveTendencyRes.params.jsonParam.tendencyId] } }).then(res => res.result.data.dresStyleResultList);
                _.remove(res, val => val.showCaption == 1);   //把随机的删掉
                expect(res.length, `将类别${saveTendencyRes.params.jsonParam.tendencyId}设置为不喜欢之后，此类别的商品依然显示在列表中`).to.be.equal(0);
            });

            it('删除倾向，做到数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val });

                let searchRes = await spdresb.searchDres({ queryType: 2, keyWords: { classId: [saveTendencyRes.params.jsonParam.tendencyId] } }).then(res => res.result.data.dresStyleResultList);
                _.remove(searchRes, val => val.showCaption == 1);   //把随机的删掉
                expect(searchRes.length, `删除倾向后，该类别的商品没有显示`).to.be.above(0);
            });
        });

        describe('不喜欢店铺', function () {
            let saveTendencyRes;
            before('不喜欢类别', async function () {
                saveTendencyRes = await spUp.saveUserTendency(saveTendencyJson({ tendencyType: 5, tendencyId: tenantId }));
            });

            after('数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val, check: false });
            });

            it('设置不喜欢后查询', async function () {
                let res = await spdresb.searchDres({ queryType: 2, tenantId }).then(res => res.result.data.dresStyleResultList);
                _.remove(res, val => val.showCaption == 1);   //把随机的删掉
                expect(res.length, `把${saveTendencyRes.params.jsonParam.tendencyId}设置为不喜欢之后，该店铺的商品依然显示在推荐上新里面`).to.be.equal(0);
            });

            it('删除倾向，做到数据还原', async function () {
                await spUp.delUserTendencyById({ id: saveTendencyRes.result.data.val });
                let searchRes = await spdresb.searchDres({ queryType: 2, tenantId }).then(res => res.result.data.dresStyleResultList);
                _.remove(searchRes, val => val.showCaption == 1);   //把随机的删掉
                expect(searchRes.length, `删除倾向后，该店铺的商品没有正常显示`).to.be.above(0);
            });
        });
    });
});



function saveTendencyJson(params) {
    let json = {
        typeId: 0,
        tendencyType: 1,
        tendencyId: 48647,
        tendencyStr: '不喜欢'
    };
    return Object.assign(json, params);
}
