const common = require('../../../lib/common');
const format = require('../../../data/format');
const spReq = require('../../help/spReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spExp = require('../../help/getExp');
const esSearchHelp = require('../../help/esSearchHelp');

//本用例集下的子用例集都以买家登录状态开始
describe('收藏关注--online', function () {
    this.timeout(30000);
    //卖家登录信息,卖家tenantId，买家登录信息
    let sellerLoginData, tenantId, clientLogindata;
    before(async function () {
        //卖家登录，
        await spReq.spSellerLogin();
        sellerLoginData = LOGINDATA;
        tenantId = LOGINDATA.tenantId;

        //买家登录
        await spReq.spClientLogin();
        clientLogindata = LOGINDATA;
    });

    describe('关注店铺', function () {
        let addFavorShopRes;
        before('买家关注店铺', async function () {
            await spUp.cancelFavorShop({ shopId: tenantId, check: false });
            addFavorShopRes = await spUp.addFavorShop({
                shopId: tenantId,
                shopName: sellerLoginData.shopName,
                rem: '诚心关注'
            });
        });

        after('尝试再次取消关注，数据还原', async function () {
            await spReq.spClientLogin();
            await spUp.cancelFavorShop({ shopId: tenantId, check: false });
        });

        it('重复关注', async function () {
            let param = _.cloneDeep(addFavorShopRes.params.jsonParam);
            param.check = false;
            const res = await spUp.addFavorShop(param);
            expect(res.result).to.includes({ msg: '您已关注该店铺。', msgId: 'up_favor_haved_concern' });
        });

        it('买家查看关注店铺列表', async function () {
            const favorShopList = await spUp.getFavorShopList();
            const info = favorShopList.result.data.rows.find(res => res.shopId == tenantId);
            common.isApproximatelyEqualAssert(addFavorShopRes.params.jsonParam, info);
        });

        // 卖家的列表 不显示买家关注的备注
        it('卖家查看关注者列表', async function () {
            await spReq.spSellerLogin();

            const shopFavorerList = await spUp.getShopFavorerList();
            const info = shopFavorerList.result.data.rows.find(res => res.favorerId == clientLogindata.tenantId);
            expect(info.favorerName, '关注者username错误').to.be.equal(clientLogindata.userName);
        });

        it('卖家店铺日志流查询', async function () {
            const res = await spUp.getShopLogList({ flowType: 6 });
            const exp = spExp.shopLogListExp({ createdDate: addFavorShopRes.opTime, flowType: 6, clientInfo: clientLogindata });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('关注了您的店铺');
        });

        it('买家取消关注', async function () {
            await spReq.spClientLogin();
            await spUp.cancelFavorShop({ shopId: tenantId });

            // 买家查看关注的店铺列表
            const getFavorShopList = await spUp.getFavorShopList();
            const info = getFavorShopList.result.data.rows.find(element => element.shopId == tenantId);
            expect(info, '取消关注失败,买家关注的店铺列表依然显示取消关注的店铺').to.be.equal(undefined);
        });

        it('卖家查看关注者的列表', async function () {
            await spReq.spSellerLogin();
            const getShopFavorerList = await spUp.getShopFavorerList();
            const info = getShopFavorerList.result.data.rows.find(element => element.shopId == tenantId);
            expect(info, '取消关注失败,卖家关注的店铺列表依然显示取消关注的店铺').to.be.equal(undefined);
        });
    });

    // 用例 http://119.3.46.172:6082/zentao/testcase-view-1941-2.html
    // 为了校验列表数据分页唯一性,需要在列表中保留20条以上数据 lxx 18-11-19
    describe('收藏商品', function () {
        // 已收藏的商品id
        let favorSpuIds = new Set(), dresInfo, addFavorRes, createdDate, updatedDate, spuIds, myFavorDres;
        before('买家收藏商品', async function () {
            await spReq.spClientLogin();
            const favorSpuList = await spUp.getFavorSpuList().then(res => res.result.data.rows);
            favorSpuList.forEach(val => { favorSpuIds.add(val.spuId) });
            // await spUp.cancelFavorShopInBatch({ spuIds, check: false });

            // 找一个不在收藏列表中的商品
            const searchDres = await spdresb.searchDres({ pageSize: 30, pageNo: 1, tenantId });
            const dresDetailUrl = searchDres.result.data.dresStyleResultList.find(val => !favorSpuIds.has(val.id)).detailUrl;
            dresInfo = await spReq.getFullForBuyerByUrl({ detailUrl: dresDetailUrl }).then((res) => res.result.data);

            addFavorRes = await spUp.addFavorSpu({
                spuId: dresInfo.id,
                spuTitle: dresInfo.spu.title,
                shopId: sellerLoginData.shopId,
                shopName: sellerLoginData.shopName,
                rem: `备注${common.getRandomStr(5)}`
            });
            createdDate = updatedDate = addFavorRes.opTime;
            favorSpuIds.add(dresInfo.id);
        });

        after('保持收藏商品不超过20个', async function () {
            const chunkSpuIds = _.chunk([...favorSpuIds], 20);
            if (chunkSpuIds.length < 2) return;

            await spReq.spClientLogin();
            await spUp.cancelFavorShopInBatch({ spuIds: chunkSpuIds[1], check: false });
        });

        it('买家已关注商品列表', async function () {
            const favorList = await spUp.getFavorSpuList({ spuTitleLike: dresInfo.spu.title });
            const favorInfo = favorList.result.data.rows.find(res => res.spuId == dresInfo.id);
            const exp = Object.assign({ createdDate, updatedDate, flag: 1 }, format.dataFormat(clientLogindata, 'createdBy=userId;updatedBy=userId;unitId'), format.dataFormat(addFavorRes.params.jsonParam, 'spuId;spuTitle;shopId;shopName;rem'));
            common.isApproximatelyEqualAssert(exp, favorInfo);
        });

        it('买家我的收藏搜索', async function () {
            myFavorDres = await spdresb.getMyFavorDres({ pageSize: 30, pageNo: 1 }).then(res => res.result.data.favorDresStyleResultList);
            const myFavorSpuIds = myFavorDres.map(res => res.id);
            const dres = myFavorDres.find(res => res.id == dresInfo.id);
            common.isAqualOptimeAssert(dres.favorDate, createdDate);
            common.isApproximatelyEqualAssert(favorSpuIds.values(), myFavorSpuIds, [], '买家搜索收藏和买家收藏列表的商品不一致');
        });
        // 废弃使用 
        it.skip('es搜索queryType=5', async function () {
            let res = await spdresb.searchDres({ pageSize: 30, pageNo: 1, queryType: 5 }).then(res => res.result.data.dresStyleResultList);
            common.isApproximatelyEqualAssert(myFavorDres, res, ['channelIds']);
        });

        it('买家查看收藏商品详情', async function () {
            await spReq.spClientLogin();
            const favorDresFull = await spdresb.searchFavorDresById({ id: dresInfo.id, _cid: sellerLoginData.clusterCode, _tid: sellerLoginData.tenantId }).then(res => res.result.data);
            const exp = await spdresb.searchDres({ pageSize: 20, pageNo: 1, jsonParam: { keyWords: { id: [dresInfo.id] } } });
            common.isApproximatelyEqualAssert(exp.result.data.dresStyleResultList[0], favorDresFull);
        });

        it('卖家商品收藏者列表', async function () {
            await spReq.spSellerLogin();
            const favorerList = await spUp.getSpuFavorerList({ spuId: dresInfo.id, favorerNameLike: clientLogindata.userName });
            const info = favorerList.result.data.rows.shift();
            expect(info, `收藏商品后，卖家查看商品收藏者列表，未找到收藏者信息`).not.to.be.undefined;
            const exp = Object.assign({ createdDate, updatedDate, flag: 1 }, format.dataFormat(clientLogindata, 'favorerId=tenantId;favorerName=userName'), format.dataFormat(addFavorRes.params.jsonParam, 'spuId;spuTitle;shopId;rem'));
            common.isApproximatelyEqualAssert(exp, info);
        });

        it('卖家店铺日志流查询', async function () {
            await spReq.spSellerLogin();
            const res = await spUp.getShopLogList({ flowType: 7 });
            const exp = spExp.shopLogListExp({ createdDate, flowType: 7, clientInfo: clientLogindata });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('收藏了货品');
        });

        it('重复收藏', async function () {
            await spReq.spClientLogin();
            const param = _.cloneDeep(addFavorRes.params.jsonParam);
            param.check = false;
            const res = await spUp.addFavorSpu(param);
            expect(res.result).to.includes({ msgId: 'up_spu_has_favored' });
        });

        it('买家我的收藏-默认排序校验', async function () {
            const favorList = await spdresb.getMyFavorDres({ pageSize: 30, pageNo: 1 }).then(res => res.result.data.favorDresStyleResultList);
            if (favorList.length > 1) {
                esSearchHelp.sortAssert(favorList, 'favorDate', true);
            };
        });

        describe('买家取消收藏', async function () {
            before(async function () {
                await spReq.spClientLogin();
                await spUp.cancelFavorSpu({ spuId: dresInfo.id });
            });
            it('买家已关注商品列表', async function () {
                const favorList = await spUp.getFavorSpuList();
                const info = favorList.result.data.rows.find(res => res.spuId == dresInfo.id);
                expect(info, '取消收藏失败,买家收藏的商品列表依然显示取消收藏的商品').to.be.undefined;
            });
            it('卖家查看关注者的列表', async function () {
                await spReq.spSellerLogin();
                const favorerList = await spUp.getSpuFavorerList({ spuId: dresInfo.id, favorerNameLike: clientLogindata.userName });
                expect(favorerList.result.data.count, '取消收藏失败,卖家查看关注者的列表依然显示取消收藏的商品').to.be.equal(0);
                expect(favorerList.result.data.rows.length, '取消收藏失败,卖家查看关注者的列表依然显示取消收藏的商品').to.be.equal(0);
            });
        });

        describe('批量取消关注', async function () {
            it('买家批量取消关注', async function () {
                await spReq.spClientLogin();
                const searchRes = await spdresb.searchDres({ pageSize: 10, pageNo: 1, jsonParam: { tenantId: tenantId } });
                //收藏3个商品，批量取消2个 保留一个作为排序等校验,after-hock中会保持数据不超过20个
                let dresList = searchRes.result.data.dresStyleResultList.slice(0, 3);

                for (let i = 0; i < dresList.length - 1; i++) {
                    const addRes = await spUp.addFavorSpu({
                        spuId: dresList[i].id,
                        spuTitle: dresList[i].title,
                        shopId: sellerLoginData.shopId,
                        shopName: sellerLoginData.shopName,
                        check: false
                    });
                    if (addRes.result.code == 0) favorSpuIds.add(dresList[i].id);
                };

                //批量取消收藏
                const spuIds = [...favorSpuIds].slice(0, 2);
                await spUp.cancelFavorShopInBatch({ spuIds });
                spuIds.forEach(spuId => favorSpuIds.delete(spuId));

                const favorList = await spUp.getFavorSpuList().then(res => res.result.data.rows);
                favorList.every(favorInfo => !spuIds.includes(favorInfo.spuId));
            });
        });
    });
});
