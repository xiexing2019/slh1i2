const spugr = require('../../../reqHandler/sp/global/spugr');
const spReq = require('../../help/spReq');
const common = require('../../../lib/common');

describe.skip('邀请slh店铺', function () {
    this.timeout(30000);
    let inviteRes;
    before('添加店铺到邀请', async function () {
        await spReq.slhStaffLogin({ epid: '16057' });   //app1 autotest_Invite帐套 此帐套未开通好店

        shopInfo = LOGINDATA;

        await spReq.spClientLogin();
        inviteRes = await spugr.saveInviteShop([inviteJson(shopInfo)]);
    });

    it('查询', async function () {
        let res = await spugr.getInviteShopList({ nameLike: inviteRes.params.jsonParam[0].name });
        let info = res.result.data.rows.find(val => val.id == inviteRes.result.data.rows[0]);
        if (info == undefined) {
            throw new Error('找不到店铺');
        } else {
            common.isApproximatelyEqualAssert(inviteRes.params.jsonParam[0], info);
        };
    });
});

function inviteJson(shopInfo) {
    let json = {
        slhSn: shopInfo.sn,
        slhId: shopInfo.epid,
        slhShopId: shopInfo.invid,
        name: shopInfo.invname,
        address: '滨江星耀城',
    };
    return json;
}