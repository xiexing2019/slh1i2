const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const mockJsonParam = require('../../help/mockJsonParam');
const spReq = require('../../help/spReq');
const mysql = require('../../../reqHandler/sp/mysql');

describe('商品识别标签', async function () {
    this.timeout(30000);
    let spgSql, sellerInfo, styleRes;
    // const dres = dresManage.setupDres();
    before(async function () {
        spgSql = await mysql({ dbName: 'spgMysql' });

        await spReq.spSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        const classInfo = await spdresb.findByClass({ classId: 1013 });// classTree.result.data.options.shift().code
        //获取字典类别列表
        let arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };
        //保存商品
        const json = mockJsonParam.styleJson({ classId: 1013 });
        styleRes = await spdresb.saveFull(json);
        // console.log(`styleRes=${JSON.stringify(styleRes)}`);

        // const dresList = await spdresb.searchDres({ tenantId: LOGINDATA.tenantId, keyWords: { id: styleRes.result.data.spuId } }).then(res => res.result.data.dresStyleResultList);
        // dres.setByEs(dresList[0]);
        // console.log(dres);
        await common.delay(500);
    });
    after(async function () {
        await spgSql.end();
    });

    it('dres_spu_anno查询记录', async function () {
        const data = await spgSql.query(`SELECT * FROM spbig.dres_spu_anno WHERE id=${styleRes.result.data.spuId} AND spu_flag=1`).then(res => res[0][0]);
        expect(data).to.includes({ id: styleRes.result.data.spuId, spu_unit_id: sellerInfo.unitId, spu_flag: 1 });
        // , img_urls: [{ docUrl: styleRes.params.jsonParam.spu.docContent[0].docUrl, googleUrl: '' }]
        expect(data.img_urls[0].docUrl).to.includes(styleRes.params.jsonParam.spu.docContent[0].docUrl.split('id=')[1]);
    });


});