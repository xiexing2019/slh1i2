'use strict';
const httpRequest = require('../lib/httpRequest.js');
const superagent = require('superagent');
const caps = require('../data/caps');
const format = require('../data/format');
const common = require('../lib/common');
const basicReqHandler = require('../slh1/help/basicInfoHelp/basicInfoReqHandler');
/**
 * 获取测试中常用的基本信息
 * @module getData
 */
let getData = module.exports = {};

//常用id
const basicDataArr = [{
	interfaceid: 'cs-getpk-style',
	key: 'styleidAgc001',
	code: 'agc001',
}, {
	interfaceid: 'cs-getpk-style',
	key: 'styleidAgc002', //show_styleidAgc002:'agc002' id
	code: 'agc002',
}, {
	interfaceid: 'cs-getpk-style',
	key: 'styleid3035', //均色均码
	code: '3035',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'coloridBaiSe',
	name: '白色',
	typeid: '601',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'coloridHongSe',
	name: '红色',
	typeid: '601',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'coloridTuoSe',
	name: '驼色',
	typeid: '601',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'coloridJunSe',
	name: '均色',
	typeid: '601',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'sizeidM',
	name: 'M',
	typeid: '605',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'sizeidL',
	name: 'L',
	typeid: '605',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'sizeidXL',
	name: 'XL',
	typeid: '605',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'sizeidJunMa',
	name: '均码',
	typeid: '605',
}, {
	interfaceid: 'cs-getpk-dict',
	key: 'seasonidSpring',
	name: '春季',
	typeid: '613',
}, {
	interfaceid: 'cs-getpk-dwxx',
	key: 'dwidXw',
	name: '小王',
}, {
	interfaceid: 'cs-getpk-dwxx',
	key: 'dwidLs',
	name: '李四',
}, {
	interfaceid: 'cs-getpk-dwxx',
	key: 'dwidVell',
	name: 'Vell',
}, {
	interfaceid: 'cs-getpk-dwxx',
	key: 'dwidRt',
	name: 'Rt',
}, {
	interfaceid: 'cs-getpk-dwxx',
	key: 'dwidSFKD',
	name: '顺丰快递',
}, {
	interfaceid: 'cs-getpk-shop',
	key: 'shopidCqd',
	name: '常青店',
}, {
	interfaceid: 'cs-getpk-shop',
	key: 'shopidZzd',
	name: '中洲店',
}, {
	interfaceid: 'cs-getpk-shop',
	key: 'shopidCkd',
	name: '仓库店',
}, {
	interfaceid: 'cs-getpk-shop',
	key: 'shopidWyd',
	name: '文一店',
}, {
	interfaceid: 'cs-getpk-staff',
	key: 'staffid000', //常青店总经理
	code: '000',
}, {
	interfaceid: 'cs-getpk-staff',
	key: 'staffid001', //常青店财务员
	code: '001',
}, {
	interfaceid: 'cs-getpk-staff',
	key: 'staffid004', //常青店店长
	code: '004',
}, {
	interfaceid: 'cs-getpk-staff',
	key: 'staffid204', //中洲店店长
	code: '204',
}];

let bankDataArr = function (data) {
	return [{
		interfaceid: 'cs-getpk-account',
		key: `cashAccount${data.shopidCqd}`, //常青店
		name: '常青店-现金账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `bankAccount${data.shopidCqd}`, //常青店
		name: '常青店-银行账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `agencyAccount${data.shopidCqd}`, //常青店
		name: '常青店-代收账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `cashAccount${data.shopidZzd}`, //中洲店
		name: '中洲店-现金账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `bankAccount${data.shopidZzd}`, //中洲店
		name: '中洲店-银行账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `agencyAccount${data.shopidZzd}`, //中洲店
		name: '中洲店-代收账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `cashAccount${data.shopidWyd}`, //文一店
		name: '文一店-现金账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `bankAccount${data.shopidWyd}`, //文一店
		name: '文一店-银行账户',
	}, {
		interfaceid: 'cs-getpk-account',
		key: `agencyAccount${data.shopidWyd}`, //文一店
		name: '文一店-代收账户',
	}];
}

//二代适配
// function formatDataForSLH2() {
// if(USEECINTERFACE == '2') {
// basicDataArr.forEach((obj) => {
//季节typeid 一代为613 二代为608
// if(obj.key.includes('seasonid')) obj.typeid = 608;
// });
// 	};
// };

/**
 * getDictIds - 获取id等信息
 * @alias module:getData
 * @async
 * @param {array} [arr=basicDataArr] 请求数组
 * @param {boolean} [onlyId=true] 是否只需要id
 * @return {object}
 */
getData.getDictIds = async function (arr = basicDataArr, onlyId = true) {
	let json = {};
	// let promises = arr.map((obj) => httpRequest.callInterface(obj.interfaceid, obj));
	// let datas = await Promise.all(promises);
	// // console.log(`datas = ${JSON.stringify(datas)}`);
	// datas.forEach((data, index) => {
	// 	if(data.hasOwnProperty('error')) {
	// 		console.log(`获取基本数据出错: ${JSON.stringify(arr[index])}\nerrorMsg: ${JSON.stringify(data)}`);
	// 	} else {
	// 		json[arr[index].key] = onlyId ? data.id : data;
	// 	};
	// });

	for (let i = 0; i < arr.length; i++) {
		const params = arr[i];
		if (params.interfaceid) {
			const res = await httpRequest.callInterface2(params);
			json[params.key] = onlyId ? res.result.id : res.result;
		} else {
			const res = await httpRequest.apiDo(params);
			if (res.result.data.rows.length == 0) {
				// console.log(`未获取到基本信息:${JSON.stringify(params)}`);
				continue;
			};
			json[params.key] = onlyId ? res.result.data.rows[0].id : res.result.data.rows[0];
		};
	};

	return json;
};


/**
 * getNameById - 根据BASICDATA中的id获取相应的name
 * @param {string} id
 * @param {string} type 类型
 * @return {string} name
 */
getData.getNameById = (id, type) => {
	let key = Object.keys(BASICDATA).find(key => BASICDATA[key] == id);
	let arr = basicDataArr.find((obj) => obj.key == key);
	return arr.name || arr.code;
};

/**
 * getBasicID - 获取基本信息的id
 * @alias module:getData
 * @async
 * @return {object} BASICDATA
 */
getData.getBasicID = async function () {
	//相同账套不再重复获取 18-09-13 lxx
	if (BASICDATA.epid === LOGINDATA.epid) return BASICDATA;
	BASICDATA.epid = LOGINDATA.epid;
	// formatDataForSLH2();
	let classIds = {};
	await basicReqHandler.getClassList().then(res => {
		const classNames = ['登山服', '鞋'];
		for (let index = 0; index < res.result.dataList.length; index++) {
			if (classNames.includes(res.result.dataList[index].name))
				classIds[res.result.dataList[index].name] = res.result.dataList[index].id;
		};
	});
	let basicData1 = await getData.getDictIds(basicDataArr, true);
	let basicData2 = await getData.getDictIds(bankDataArr(basicData1), true); //银行账户信息
	let specGoodsList = await getData.getSpecGoodsList(); //特殊货品列表
	specGoodsList.dataList.forEach((obj) => BASICDATA[`styleid${obj.code}`] = obj.id);
	return Object.assign(BASICDATA, basicData1, basicData2, {
		specGoodsList: specGoodsList.dataList,
		classIds,
	});
};

/*
 * 常用货品/客户/厂商的详细信息
 * 需要先获取id (getBasicID)
 */
getData.constDataArr = [{
	interfaceid: 'qf-1511',
	key: 'styleAgc001',
	action: 'edit',
	pk: BASICDATA.styleidAgc001,
}, {
	interfaceid: 'qf-1401',
	key: 'dwXw',
	action: 'edit',
	pk: BASICDATA.dwidXw,
}];

const bankAccountTypes = {
	'现': 'cash',
	'刷': 'card',
	'汇': 'remit',
	'银': 'bank',
	'代': 'agency',
	'微': 'weixin',
	'支': 'ali',
};
getData.getBankAccountIds = async function () {
	if (BASICDATA[`cashAccount${LOGINDATA.invid}`]) return;

	let res = await httpRequest.callInterface('cs-getAccountData', {
		invid: LOGINDATA.invid,
	});
	res.dataList.forEach((obj) => {
		BASICDATA[`${bankAccountTypes[obj.name]}Account${obj.invid}`] = obj.id;
	});
};

/**
 * getSpecGoodsList - 获取特殊货品列表
 * @param {object} [params={}] params
 * @return {object}
 * {dataList:[{id,profitcount:'核算到利润',ratio:'最高比例(%)',name,special:'核算到积分',negative:'是否负值',code}]}
 */
getData.getSpecGoodsList = async function (params = {}) {
	let dataList = [],
		errorMsg;
	if (USEECINTERFACE == 1) {
		//code name flag
		let qlRes = await httpRequest.callInterface('ql-1518', format.qlParamsFormat({
			flag: 2, //是否停用 1是 2否
		}));
		dataList = qlRes.dataList;
	} else {
		const mapfield = 'id;profitcount=ecCaption.profitcount;ratio;name;special=ecCaption.scoreCount;negative=ecCaption.negative;code;flag';
		params = format.paramsFormat(Object.assign({
			sessionId: LOGINDATA.sessionid,
			pageSize: 15,
			pageNo: 1,
			wrapper: true,
			// jsonParam: {
			// 	name: '抹零',
			// },
		}, params));
		await superagent.post(`${caps.url}/slh/api.do?apiKey=ec-dres-speicalSpu-list`)
			.type('form')
			.send(params)
			.then((data) => {
				const text = JSON.parse(data.text);
				dataList = text.data.rows.map((obj) => format.dataFormat(obj, mapfield));
			});
	};
	return {
		dataList: dataList,
		errorMsg,
	};
};

const typeIds = {
	colorGroup: 631,
	color: 601,
	sizeGroup: 604,
	size: 605,
	brand: 606,
};

/**
 * getDictListInfo - 获取字典列表信息
 * @alias module:getData
 * @async
 * @param {object} params
 * @param {string} params.typeName 类型名称 用来得到typeid
 * @return {object} {params,result}
 */
getData.getDictListInfo = async function (params) {
	params = Object.assign({
		typeid: typeIds[params.typeName],
		delflag: 0, //是否停用 0:启用 1:停用
	}, params);
	delete params.typeName;
	let result = await httpRequest.callInterface('cs-dict-list', params);
	expect(result, `查询字典列表cs-dict-list失败:${result}`).not.to.have.property('error');
	return {
		params,
		result,
	};
};


/**
 * 将值保存到BASICDATA中
 * @param {} key
 */
getData.setCache = async function (key, cb) {
	if (typeof cb == 'function') {
		BASICDATA.key = await cb();
	} else {
		BASICDATA.key = cb;
	};
};
