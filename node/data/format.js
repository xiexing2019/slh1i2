'use strict';
const caps = require('./caps.js');
const moment = require('moment');
const calculate = require('../lib/calculate.js');
const mapData = require('./mapData');
/**
 * 数据拼接
 * @module format
 */
let format = module.exports = {};

/**
 * jsonparamFormat - 组装开单(sf接口)的jsonparam
 * 需要含有sf接口的interfaceid
 * sf-14211-1销售开单 默认为结余balance 选择找零则需设置cashchange
 * @alias module:format
 * @param {object}    json          jsonparam
 * @param {boolean} [needLog=false] 是否需要导出拼接后的jsonparam
 * @return {object} 拼接后的jsonparam
 */
format.jsonparamFormat = (json, needLog = false) => {
	//销售订货-按订单配货
	if (json.interfaceid == 'cs-saveOrderDetDistribute') {
		return json;
	};
	//通用部分
	//是否挂单 转正式单传1,其他都传0 (二代)
	if (USEECINTERFACE == 2 && !json.fromPend) json.fromPend = 0;
	//支付 还未提供获取银行id的接口
	if (json.cash && !json.hasOwnProperty('cashaccountid')) json.cashaccountid = BASICDATA[`cashAccount${LOGINDATA.invid}`];
	//后台 银行账户设为通用  刷卡汇款默认使用银行账户
	if (json.card && !json.hasOwnProperty('cardaccountid')) json.cardaccountid = BASICDATA[`bankAccount${LOGINDATA.invid}`];
	if (json.remit && !json.hasOwnProperty('remitaccountid')) json.remitaccountid = BASICDATA[`bankAccount${LOGINDATA.invid}`];
	if (json.agency && !json.hasOwnProperty('agencyaccountid')) json.agencyaccountid = BASICDATA[`agencyAccount${LOGINDATA.invid}`];
	//唯一码hashkey
	json.hashkey = moment().format('YYYY-MM-DD HH:mm:ss:SSS');
	//基本信息
	if (!json.action) json.action = json.pk ? 'edit' : 'add';
	if (!json.invid) json.invid = LOGINDATA.invid; //登陆门店id
	if (!json.invalidflag) json.invalidflag = "0"; //状态 0正常 1作废 9挂单
	if (!json.prodate) json.prodate = moment().format('YYYY-MM-DD'); //发生日期
	// if(json.action == 'edit' && !json.pk) json.pk = vars.get('pk') || vars.getObject('billInfo').pk;
	let [totalsum, totalnum] = [0, 0];
	//特殊货品id
	const specialStyleids = BASICDATA.specGoodsList.map((obj) => Number(obj.id));;

	//整单折扣
	const mainDiscount = json.maindiscount ? Number(json.maindiscount) : 1;
	//明细details处理
	for (let i = 0; i < json.details.length; i++) {
		if (json.details[i].price == undefined) json.details[i].price = 100; //进货价
		//折扣一般只会生效一个 有整单折扣时取整单折扣
		if (mainDiscount != 1) {
			json.details[i].discount = mainDiscount;
		};
		let discount = json.details[i].discount ? Number(json.details[i].discount) : 1;
		json.details[i].total = calculate.mul(calculate.mul(json.details[i].num || 0, json.details[i].price), discount);
		// json.details[i].realprice = calculate.mul(json.details[i].price, discount); //折后价 服务端自行计算

		totalsum += json.details[i].total;

		//需要先获取常用数据信息
		if (!json.details[i].styleid) {
			json.details[i].styleid = BASICDATA[`styleid${json.details[i].matCode}`];
		};
		delete json.details[i].matCode;

		if (specialStyleids.includes(Number(json.details[i].styleid))) {
			json.details[i].flag = 2; //特殊货品
		} else {
			json.details[i].flag = 0; //普通货品
			totalnum += Number(json.details[i].num || 0); //总数量 不计算特殊货品
		};

		json.details[i].colorid = BASICDATA[`colorid${json.details[i].colorid}`] || json.details[i].colorid; //传入BaiSe，输出coloridBaiSe
		json.details[i].sizeid = BASICDATA[`sizeid${json.details[i].sizeid}`] || json.details[i].sizeid;
		if (!json.details[i].rowid) json.details[i].rowid = i;

		//价格类型 采购0 其他默认1
		if (USEECINTERFACE == 1 && !json.details[i].stdpricetype) json.details[i].stdpricetype = ['sf-14212-1', 'sf-22401-1', 'sf-22101-1'].includes(json.interfaceid) ? 0 : 1;
	};

	json.totalnum = totalnum; //总数量
	json.totalsum = totalsum; //总金额
	let actualPay = Number(json.cash || 0) + Number(json.card || 0) + Number(json.remit || 0) + Number(json.agency || 0);
	let shortOf = (actualPay - totalsum).toFixed(3);

	//处理接口号之间的差异字段
	switch (json.interfaceid) {
		case "sf-14212-1": //采购入库单
			json.actid = 10; //操作类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.type = 10; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			// json.typeid = 10; //单据类型,服务端自行处理
			json.inoutflag = 1; //出入类型 1为入库,2为出库,必填
			// json.orderversion //创建为0，每改一次加1
			// if(json.orderversion && json.orderversion > 0 && !json.billid) json.billid = vars.getObject('billInfo').pk;
			if (!json.dwid) json.dwid = BASICDATA.dwidVell; //厂商Vell
			if (!json.shopid) json.shopid = LOGINDATA.invid; //登陆门店
			if (!json.deliver) json.deliver = LOGINDATA.id; //登陆角色
			//结余or欠款
			json.balance = Number(json.finpayVerifysum || 0) + Number(shortOf);
			//总金额
			json.totalmoney = json.totalsum;
			delete json.totalsum;
			break;

		case 'sf-14401-1': //销售订单
			json.actid = 39;
			if (!json.shopid) json.shopid = LOGINDATA.invid;
			if (!json.sellerid) json.sellerid = LOGINDATA.id;
			if (json.clientid == undefined) json.clientid = BASICDATA.dwidXw;
			//总金额 二代必填
			json.totalmoney = json.totalsum;
			break;

		case 'sf-14211-1': //新销售开单接口
		case 'sf-1421-1': //老销售开单
		case 'sf-1427-saleout_dres'://按配货开单-开单保存
			if (!json.shopid) json.shopid = LOGINDATA.invid;
			if (!json.deliver) json.deliver = LOGINDATA.id;
			if (json.dwid == undefined) json.dwid = BASICDATA.dwidXw; //客户id值 非必填
			json.actid = 21; //操作类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.type = 21;
			// json.typeid = 21;//服务端自行处理
			json.inoutflag = 2;
			//差额
			// if(json.cash > 0 && json.cash == actualPay && shortOf > 0) {
			// 	//找零 只有现金支付且有结余时，点选的功能
			// 	//e.g. 现金填了500 实际的传值是cash:400(应付)，cashchange:100(找零)
			// 	json.cashchange = shortOf;
			// 	json.cash = Number(json.cash - json.cashchange || 0);
			// } else {
			json.balance = Number(json.finpayVerifysum || 0) + Number(shortOf); //结余=核销金额+差额
			// };
			//总金额
			json.totalmoney = json.totalsum;
			delete json.totalsum;
			break;

		case 'sf-1862-1': //调拨出库
			json.totalmoney = json.totalsum;
			delete json.totalsum;
			json.type = 25; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			if (!json.deliver) json.deliver = LOGINDATA.id; //送货人
			if (!json.shopid) json.shopid = LOGINDATA.invid; //调出门店
			if (!json.invidref) { //调入门店
				let shopidZzd = BASICDATA.shopidZzd;
				json.invidref = json.shopid == shopidZzd ? BASICDATA.shopidCqd : shopidZzd;
			}
			break;

		case 'sf-22101-1': //采购订单
			if (!json.dwid) json.dwid = BASICDATA.dwidVell;
			if (!json.respopid) json.respopid = LOGINDATA.id; //默认当前登录角色
			if (!json.shopid) json.shopid = LOGINDATA.invid; //二代
			if (json.rem) json.remark = json.rem; //二代
			break;

		case 'sf-22401-1': //批量入库
			if (!json.deliver) json.deliver = LOGINDATA.id;
			if (!json.invid) json.invid = LOGINDATA.invid;
			// if(!json.opid) json.opid = LOGINDATA.id;
			json.type = 10; //类型,21为销售,10为采购,25为调拨出库,16为调拨入库,必填
			json.inoutflag = 1; //出入类型 1为入库,2为出库,必填
			for (let i = 0; i < json.details.length; i++) {
				json.details[i].mainDwid = BASICDATA[`dwid${json.details[i].mainDwid}`] || json.details[i].mainDwid;
			};
			break;

		case 'sf-1924-1': //盘点单
			delete json.totalsum;
			if (!json.deliver) json.deliver = LOGINDATA.id;
			json.shopid = LOGINDATA.invid; // if(!json.shopid) 登陆门店id
			json.inoutflag = 1; //出入类型 1为入库,2为出库,必填
			json.typeid = 17;
			json.type = 17;
			json.actid = 17;
			break;

		case 'sf-1355-1': //收入单
		case 'sf-1356-1': //支出单
			delete json.totalnum;
			if (!json.accountid) json.accountid = BASICDATA[`cashAccount${LOGINDATA.invid}`]; //当前登录门店的现金账户，accountid必填

			for (let i = 0; i < json.details.length; i++) {
				totalsum += json.details[i].money;
			}
			json.totalsum = totalsum;
			if (!json.deliver) json.deliver = LOGINDATA.id;
			json.shopid = LOGINDATA.invid;
			if (USEECINTERFACE == 2) {
				json.payType = 1;
			}
			break;
		case 'sf-2324-1'://按要货调拨
			json.totalmoney = json.totalsum;
		default:
			break;
	};


	// delete json.interfaceid; // common.editBilling需要使用
	if (needLog) console.log(`\njsonformat:\n${JSON.stringify(json)}`);
	return json;
};

/**
 * updateHashkey - 更新jsonparam中的hashkey
 * 一些用例只需要更新hashkey重复开单即可
 * @alias module:format
 * @param {object} jsonparam
 * @return {object} jsonparam
 */
format.updateHashkey = (jsonparam) => {
	let time = moment().format('YYYY-MM-DD HH:mm:ss:SSS');
	jsonparam.hashkey = `${caps.deviceNo}-${time}`;
	return jsonparam;
};

/**
 * verifyJsonFormat - 组装物流商核销的jsonparam
 * @alias module:format
 * @param {object}    json                jsonparam
 * @param {boolean} [isNeedSpecial=false] 是否需要特殊货品(true时会添加抹零和打包费)
 * 核销加上特殊外，还另外使用了多种支付方式
 * @return {object} jsonparam
 */
format.verifyJsonFormat = function (json, isNeedSpecial = false) {
	if (isNeedSpecial) {
		json.details = [{
			'num': "-1",
			'price': "3",
			'rem': "",
			'styleid': BASICDATA.styleid00000,
		}, {
			'num': "1",
			'price': "18",
			'rem': "",
			'styleid': BASICDATA.styleid00001,
		}]
		json.totalmoney = Number(json.details[0].price) * Number(json.details[0].num) + Number(json.details[1].price) * Number(json.details[1].num);
		json.cash = json.logisVerifysum + json.totalmoney - (json.card || 0) - (json.remit || 0);
	}

	if (json.cash && !json.cashaccountid) json.cashaccountid = BASICDATA[`cashAccount${LOGINDATA.invid}`];
	if (json.card && !json.cardaccountid) json.cardaccountid = BASICDATA[`bankAccount${LOGINDATA.invid}`];
	if (json.remit && !json.remitaccountid) json.remitaccountid = BASICDATA[`agencyAccount${LOGINDATA.invid}`];
	if (!json.prodate) json.prodate = moment().format('YYYY-MM-DD'); //发生日期
	if (!json.shopid) json.shopid = LOGINDATA.invid;
	if (!json.deliver) json.deliver = LOGINDATA.id; //业务员ID值
	json.action = 'add';

	return json;
};

/* 通用查询参数 */
const defParams = {
	curpageno: 1,
	pagesize: 15, //默认15 0则返回全部
	search_list: 1,
	search_count: 1,
	search_sum: 1,
};

/**
 * qlParamsFormat - 拼接查询ql接口的params
 * @alias module:format
 * @param {object}  [params={}]     ql接口的param
 * @param {boolean} [today=false]   是否当天
 * @param {boolean} [needLog=false] 是否需要导出拼接后的params
 * @return {object} params
 */
format.qlParamsFormat = function (params = {}, today = false, needLog = false) {
	if (today) {
		params.prodate1 = moment().format('YYYY-MM-DD');
		params.prodate2 = moment().format('YYYY-MM-DD');
	};
	params = Object.assign(_.cloneDeep(defParams), params);

	if (needLog) console.log(`\nsearchParams:\n${JSON.stringify(params)}`);
	return params;
};

format.paramsFormat = function (params) {
	//二代接口参数使用小驼峰,一代为全小写
	if (USEECINTERFACE == 1) {
		for (let key in params) {
			if (typeof (params[key]) == 'object' && !params[key].length) params[key] = format.paramsFormat(params[key]);
			params[key.toLowerCase()] = JSON.stringify(params[key]); //
			delete params[key];
		};
	};
	return params;
};

/**
 * params->params.jsonParam
 * @param {object} [params]
 * @param {object[]} [excludeKeys] 排除字段
 */
format.packJsonParam = function (params = {}, excludeKeys = []) {
	let json = {};

	Array.from(new Set(['check', 'pageSize', 'pageNo', ...excludeKeys])).forEach((key) => {
		if (params.hasOwnProperty(key)) {
			json[key] = params[key];
			delete params[key];
		};
	});

	if (!params.jsonParam) {
		json.jsonParam = params;
	} else {
		json.jsonParam = params.jsonParam;
	};
	return json;
};

/*
 * 仅返回汇总列表
 */
const onlySumDefParams = {
	search_list: 0,
	search_count: 0,
	search_sum: 1,
};

/**
 * qlOnlySumFormat - 拼接查询ql接口的params 只获取汇总值
 * @alias module:format
 * @param {object}  [params={}]     ql接口的param
 * @param {boolean} [today=false]   是否当天
 * @param {boolean} [needLog=false] 是否需要导出拼接后的params
 * @return {object} params
 */
format.qlOnlySumFormat = function (params = {}, today = false, needLog = false) {
	if (today) {
		params.prodate1 = moment().format('YYYY-MM-DD');
		params.prodate2 = moment().format('YYYY-MM-DD');
	};
	params = Object.assign(_.cloneDeep(onlySumDefParams), params);

	if (needLog) console.log(`\nsearchParams:\n${JSON.stringify(params)}`);
	return params;
};


/**
 * dataFormat - 数据拼接
 * @param {object} obj      Description
 * @param {String} mapfield 拼接规则(key=path=defaultValue),分号分割 e.g. 'id;profitcount=ecCaption.profitcount=0'
 * @return {object} 拼接后的结果
 */
format.dataFormat = function (obj, mapfield) {
	let exp = {};
	const fields = mapfield.split(';');
	for (let i = 0, length = fields.length; i < length; i++) {
		const [key, path = key, defaultValue = ''] = fields[i].split('=');
		exp[key] = _.get(obj, path, defaultValue);
	};
	return exp;
};

/**
 * numberFormat - 数字格式化
 * @param {Number|String} number
 * @param {Number|String} decimals      保留几位小数
 * @param {String} dec     小数点符号
 * @param {String} sep 千分位符号
 * @return {String}
 */
format.numberFormat = function (number, decimals, dec = '.', sep = ',') {
	number = String(number).replace(/[^0-9+-Ee.]/g, '');
	let n = !isFinite(+number) ? 0 : +number,
		prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
		s = '',
		toFixedFix = function (n, prec) {
			let k = Math.pow(10, prec);
			return '' + Math.floor(n * k) / k;
		};
	s = (prec ? toFixedFix(n, prec) : '' + Math.floor(n)).split('.');
	let re = /(-?\d+)(\d{3})/;
	while (re.test(s[0])) {
		s[0] = s[0].replace(re, `$1${sep}$2`);
	};

	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	};
	return s.join(dec);
};

/**
 * mapData - 拼接ql接口的期望值
 * @alias module:format
 * @see module:mapData
 */
format.mapData = mapData;
