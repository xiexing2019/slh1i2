'use strict';

let format = module.exports = {};

// '='前面的是目标字段，'='后面的是服务端qf返回的字段
let mapfieldMain = {
	'ql-22301': 'id=pk;shopname=show_shopid;paysum=actualpay;verifysum=finpayVerifysum;purmoney=totalmoney;dwname=show_dwid;seller=show_deliver;weixinpay=finpayWeixinpay;',
};

let mapfieldDetail = {};

// ';'分隔开，对应的依次是'分隔主题;分隔标致;取分隔后的第几个元素'，
// 如下: 分隔主题.split('分隔标致')[取分隔后的第几个元素]
//      seller.split(',')[1]
let splitParams = {
	'ql-22301': ['seller;,;1'],
};

/**
 * 拼接 采购入库-按批次查查询界面结果期望值
 * interfaceid = ql-22301
 * @param {object} qfResult - qf-14212-1 采购入库单据详情
 * @return {object} 期望值
 */
format.mapData_ql_22301 = (qfResult, interfaceid) => {
	/*
	let exp = {
		optime: qfResult.optime,
		remark: qfResult.remark,
		remit: qfResult.remit,
		shopname: qfResult.show_shopid,
		verifysum: qfResult.finpayVerifysum,
		id: qfResult.pk,
		finpayAlipay: qfResult.finpayAlipay,
		balance: qfResult.balance,
		finpayWeixinpay: qfResult.finpayWeixinpay,
		modelClass: qfResult.modelClass,
		purmoney: qfResult.totalmoney,
		card: qfResult.card,
		totalmoney: qfResult.totalmoney,
		dwname: qfResult.show_dwid,
		agency: qfResult.agency,
		totalnum: qfResult.totalnum,
		seller: qfResult.show_deliver.split(',')[1], //e.g. 000,总经理
		prodate: qfResult.prodate,
		opname: qfResult.opname,
		invalidflag: qfResult.invalidflag,
		weixinpay: qfResult.finpayWeixinpay,
		alipay: qfResult.finpayAlipay,
		// totalRoll: '',//总匹数
		cash: qfResult.cash,
		puredmaincode: '0', //订货号 没有默认为0
		paysum: qfResult.actualpay, //付款
		billno: qfResult.billno,
		outsum: 0, //进货额
		backsum: 0, //退货额
	};
	*/
	let exp = _.cloneDeep(qfResult);

	// 处理mapfieldMain部分
	let mfMainArray = mapfieldMain[interfaceid] != undefined ? mapfieldMain[interfaceid].split(';') : [];
	for(let i = 0; i < mfMainArray.length; i++) {
		let fields = mfMainArray[i].split('=');
		exp[fields[0]] = exp[fields[1]];
		delete exp[fields[1]];
	}

	// 处理split
	let splitArray = splitParams[interfaceid];
	for(let i = 0; i < splitArray.length; i++) {
		let params = splitArray[i].split(';');
		exp[`${params[0]}`] = exp[`${params[0]}`].split(`${params[1]}`)[`${params[2]}`];
	}

	// 处理mapfieldDetail部分
	let mfDetailArray = mapfieldDetail[interfaceid] != undefined ? mapfieldDetail[interfaceid].split(';') : [];
	if(mfDetailArray.length > 0) {
		for(let i = 0; i < exp.details.length; i++) {
			for(let j = 0; j < mfDetailArray.length; i++) {
				let fields = mfDetailArray[j].split(',');
				exp.details[i][fields[0]] = exp.details[i][fields[1]];
				delete exp.details[i][fields[1]];
			}
		}
	}


	qfResult.details.map((obj) => {
		obj.num < 0 ? exp.backsum -= Number(obj.total) : exp.outsum += Number(obj.total);
	});
	return exp;
};
