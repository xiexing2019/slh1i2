'use strict';
const common = require('../lib/common.js');
const format = require('./format.js');
const getBasicData = require('./getBasicData.js');
const basiceJsonparam = require('../slh1/help/basiceJsonparam.js');
const shopInOutReqHandler = require('../slh1/help/shopInOutHelp/shopInOutRequestHandler');

/**
 * 门店设置
 * @module storeBinding
 */
let storeBinding = module.exports = {};


/**
 * foundShopInfo - 找到门店具体信息(含有断言)
 * @alias module:storeBinding
 * @async
 * @param {string} [shopName=文一店] 门店名称
 * @return {object} 门店信息
 */
async function foundShopInfo(shopName = '文一店') {
	let shopList = await common.callInterface('ql-1501', format.qlParamsFormat({
		'name': shopName
	}));
	expect(shopList.dataList.length > 0, `foundShopInfo没有查询结果:${JSON.stringify(shopList)}`).to.be.true;

	let shopInfo = await common.callInterface('qf-1501', {
		'pk': shopList.dataList[0].id
	});
	expect(shopInfo, `foundShopInfo查询明细有误:${JSON.stringify(shopInfo)}`).to.not.have.property('error');
	return shopInfo;
}

/**
 * bindingWarehouse - 绑定仓库->仓库店 (含断言-是否成功)
 * @alias module:storeBinding
 * @async
 * @param {string} [shopName=文一店] 门店名称
 */
storeBinding.bindingWarehouse = async function (shopName = '文一店') {
	let shopInfo = await foundShopInfo(shopName);
	await shopInOutReqHandler.shopInAllOnroadBilling(shopInfo.pk); //将某个门店所有的在途调拨单全部调入

	shopInfo.bindinvid = BASICDATA['shopidCkd']; //绑定仓库 仓库店
	shopInfo.action = 'edit';
	delete shopInfo.cityid;
	delete shopInfo.logoUrl;
	let save = await common.callInterface('sf-1501', {
		'jsonparam': shopInfo
	});
	// console.log(`save : ${JSON.stringify(save)}`);
	expect(save, `${shopName}绑定仓库店失败:${JSON.stringify(save)}`).to.not.have.property('error');
};

/**
 * unboundWarehouse - 解除仓库店的绑定 (含断言-是否成功)
 * @alias module:storeBinding
 * @async
 * @param {string} [shopName=文一店] 门店名称
 */
storeBinding.unboundWarehouse = async function (shopName = '文一店') {
	let shopInfo = await foundShopInfo(shopName);
	await shopInOutReqHandler.shopInAllOnroadBilling(shopInfo.pk); //将某个门店所有的在途调拨单全部调入
	shopInfo.bindinvid = '';
	shopInfo.show_bindinvid = '';
	delete shopInfo.cityid;
	delete shopInfo.logoUrl;
	shopInfo.action = 'edit';
	let save = await common.callInterface('sf-1501', {
		'jsonparam': shopInfo
	});
	expect(save, `${shopName}解绑仓库店失败:${JSON.stringify(save)}`).to.not.have.property('error');
};
