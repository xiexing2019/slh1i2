const purchase = require('./mapDataHelp/purchase');

const format = Object.assign(purchase);

const mapfieldMain = {
	//采购入库-按批次查
	'ql-22301': 'id=pk;shopname=show_shopid;paysum=actualpay;verifysum=finpayVerifysum;purmoney=totalmoney;dwname=show_dwid;seller=show_deliver;weixinpay=finpayWeixinpay;',
	//盘点管理-按批次查 flag,processtime,rem
	'ql-1923': 'id=pk;staffName=opstaffName;invName=show_invid',
};

const mapData = function (interfaceid, data) {
	let exp = _.cloneDeep(data);

	const mfMainArray = mapfieldMain[interfaceid] != undefined ? mapfieldMain[interfaceid].split(';') : [];
	for(let i = 0, length = mfMainArray.length; i < length; i++) {
		const fields = mfMainArray[i].split('=');
		exp[fields[0]] = exp[fields[1]];
		delete exp[fields[1]];
	};

	switch(interfaceid) {
	case 'ql-22301': //采购入库-按批次查
		exp.seller = data.show_deliver.split(',')[1]; //e.g. 000,总经理
        [exp.backsum, exp.outsum] = [0, 0];
		exp.details.map((obj) => {
			obj.num < 0 ? exp.backsum -= Number(obj.total) : exp.outsum += Number(obj.total);
		});
		delete exp.details;
		break;
	case 'ql-1923': //盘点管理-按批次查
		exp.staffName = data.show_deliver.split(',')[1];
		break;
	default:
		// console.log(`mapData interfaceid=${interfaceid}尚未适配`);
		break;
	};
	return exp;
};

module.exports = mapData;
