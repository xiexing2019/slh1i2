

//全局参数

//常用第三方库
global.assert = require('assert');
global.expect = require('chai').expect;
global._ = require('lodash');

//登录信息
global.LOGINDATA = {};
//店铺信息
global.SHOPDATA = {};
//常用信息
global.BASICDATA = {};

//确定使用1代还是2代接口 1:一代, 2:二代
global.USEECINTERFACE = '';
//测试用例
global.TESTCASE = { timeout: 90000 };

