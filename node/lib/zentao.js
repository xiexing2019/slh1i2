const superagent = require('superagent');

const zentaoUrl = 'http://zentao.hzdlsoft.com:6082/zentao';

async function baseRequest(urlPath, params) {
    let res = {};
    await superagent.get(`${zentaoUrl}${urlPath}`)
        .type('from')
        .query(params)
        .then((response) => {
            const text = JSON.parse(response.text);
            if (text.status == 'success') {
                res.data = JSON.parse(text.data);
            }
        });
    return res;
};

class ZenTao {
    constructor({ id }) {
        this.id = id;
        this.suit = {};
    }

    /**
     * 查看用例详情
     * @param {object} params 
     * @param {string} params.id 用例id 
     */
    async getTestCaseDetail(params) {
        const data = await baseRequest(`/testcase-view-${params.id}.json`, params).then(res => res.data);
        return { title: data.title, case: data.case };
    }

    /**
     * 执行用例
     */
    async runTestCase() {
        // await superagent.post(`/testtask-runCase-0-60-2.json`)

    }




}

module.exports = new ZenTao();