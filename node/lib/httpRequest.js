'use strict';
const caps = require('../data/caps');
const format = require('../data/format.js');
const superagent = require('superagent');
const moment = require('moment');
// const async_hooks = require('async_hooks');

/**  */
const commonParams = [];

/**
 * 请求类方法
 * 合并到lib/common中
 * @module httpRequest
 */
let httpRequest = module.exports = {};

/**
 * 生成cliReqId 拱服务端查询日志
 */
function getCliReqId() {
	return `auto${Date.now()}${_.random(100000, 999999)}`;
};

/**
 * loginDo - 登陆login.do
 * 获取sessionId
 * @alias module:httpRequest
 * @async
 * @param {object}  [params={}] {logid,pass}
 * @param {boolean} [flag=true] false时,强制登陆
 */
httpRequest.loginDo = async function (params = {}, flag = true) {
	params = Object.assign({
		logid: '000',
		pass: '000000',
		deviceno: caps.deviceNo,
		dlProductCode: caps.dlProductCode,
		dlProductVersion: caps.slhVersion,
		epid: caps.epid,
		language: 'zh-Hans-CN',
		slh_version: caps.slhVersion,
	}, params);
	//同一个帐套+工号时不重新登录
	if (flag && params.logid === LOGINDATA.code && params.epid === LOGINDATA.epid) return;

	await superagent.post(`${caps.url}/slh/login.do`)
		.send(params)
		.type('form')
		.then((res) => {
			// console.log(`res = ${JSON.stringify(res)}`);
			LOGINDATA = JSON.parse(res.text);
			expect(LOGINDATA, `工号${params.logid}登陆失败\n${JSON.stringify(res)}`).to.not.have.property('error');
			USEECINTERFACE = LOGINDATA.serverCode && LOGINDATA.serverCode == 'G2' ? 2 : 1;
		}).catch((err) => {
			throw new Error(`login.do请求失败:${err.message}`);
		});
	// console.log(`LOGINDATA = ${JSON.stringify(LOGINDATA)};`);
};

/**
 * callInterface - /slh/callInterface.do
 * @alias module:httpRequest
 * @async
 * @param {string} interfaceid 接口id
 * @param {Object} params      接口参数
 * @param {Object} params.check   是否需要断言 默认否
 * @return {Object} 请求结果
 */
httpRequest.callInterface = async function (interfaceid, params) {
	params.interfaceid = interfaceid;
	const res = await httpRequest.callInterface2(params);
	return res.result;
};

httpRequest.callInterface2 = async function (params) {
	params = Object.assign({
		'epid': caps.epid,
		'slh_version': caps.slhVersion,
		'testTaskId': caps.testTaskId,
	}, params);
	params.check = params.hasOwnProperty('check') ? params.check : false;
	return httpRequest.post('/slh/callInterface.do', params);
};

/**
 * slhApiDo - /slh/api.do
 * @alias module:httpRequest
 * @async
 * @param {object} params
 * @param {string} params.apiKey
 * @param {object} [params.jsonParam]
 * @return {object} {params,result}
 */
httpRequest.apiDo = async function (params) {
	return httpRequest.post('/slh/api.do', params);
};

/**
 * smallcallBackDo - /slh/small/callBack.do
 * @alias module:httpRequest
 * @async
 * @param {object} params
 * @param {string} params.apiKey
 * @return {object} {params,result}
 */
httpRequest.callBackDo = async function (params) {
	return httpRequest.post('/slh/small/callBack.do', params);
};

function setUrlSearchParams(url, search) {
	const myURL = new URL(url);
	// cliReqId 拱服务端查询请求内容
	const cliReqId = getCliReqId();
	BASICDATA.cliReqId = cliReqId;
	myURL.searchParams.set('cliReqId', cliReqId);
	// myURL.searchParams.set('testTaskId', caps.testTaskId); // 目前2代框架不支持 等赖哥处理

	if (search.hasOwnProperty('_tid')) {
		myURL.searchParams.set('_tid', search._tid);
		// delete search._tid;
	}
	if (search.hasOwnProperty('_cid')) {
		myURL.searchParams.set('_cid', search._cid);
		// delete search._cid;
	}
	if (search.hasOwnProperty('_master')) {
		myURL.searchParams.set('_master', search._master);
		delete search._master;
	}

	//服务端底层限制，登录时不能传sessionId
	if (search.hasOwnProperty('sessionid') || search.hasOwnProperty('sessionId')) {

	} else if (search.interfaceid) {
		search.sessionid = LOGINDATA.sessionId || LOGINDATA.sessionid;
	} else if (search.apiKey != 'ec-spugr-spLogin-userLogin') {
		search.sessionId = LOGINDATA.sessionId || LOGINDATA.sessionid || '';
	};
	// console.log(myURL.href);
	return { url: myURL, search };
};

httpRequest.post = async function (url, search) {
	let params = _.cloneDeep(search);
	let res = {}, body = {}, reqUrl = '',
		check = params.hasOwnProperty('check') ? params.check : true;
	delete params.check;
	if (!url.includes('http')) {
		url = `${caps.url}${url}`;
	};
	const req = setUrlSearchParams(url, params);
	for (let key in params) {
		if (typeof (params[key]) == 'object') {
			body[key] = JSON.stringify(params[key]);
		} else {
			body[key] = params[key];
		};
	};
	reqUrl = decodeURI(req.url);
	// console.log(reqUrl);

	const time = Date.now();
	// const t = setTimeout(() => {
	// 	console.log(`\nopTime:${moment(time).format('YYYY-MM-DD HH:mm:ss')}\n请求响应慢:${reqUrl}`);
	// }, 10000);
	// console.log(`time=${moment().format('YYYY-MM-DD HH:mm:ss:SSS')}`);
	await superagent.post(req.url.href)
		.timeout(0)
		.type('form')
		.send(body)
		.then((response) => {
			// response.status
			res = JSON.parse(JSON.stringify(response)); //
		}).catch((err) => {
			// err.message, err.response
			// console.log(`err=${JSON.stringify(err)}`);
			logReqErrMsg({ err, reqUrl });
		});
	// clearTimeout(t);
	const duration = Date.now() - time,
		result = JSON.parse(res.text);
	res.header.date = moment().utc(res.header.date).format('YYYY-MM-DD HH:mm:ss');
	if (check && (result.code < 0 || result.hasOwnProperty('error'))) {
		// console.log();
		// httpRequest.post2('http://172.81.237.145:8081/conf/tomcat/threadList.do');
		// console.log();
		// httpRequest.post2('http://172.81.236.216:8081/conf/tomcat/threadList.do');
		// res.req.url = decodeURI(res.req.url);
		// throw new Error(`opTime:${res.header.date}\n\turl:${reqUrl}\n\tresult:${res.text}`);
		throw new Error(`opTime:${res.header.date}\n\turl:${reqUrl}\n\tbody:${JSON.stringify(body)}\n\tresult:${res.text}`);
	};

	BASICDATA.url = reqUrl;
	return {
		reqUrl,
		params,
		result,
		opTime: res.header.date,
		duration
	};
};
httpRequest.logSlowMsg = function ({ reqUrl, duration }) {
	// if (duration > 7000) {
	// 	console.log(`请求响应较慢:${reqUrl}\n持续时间:${duration}`);
	// }
};

httpRequest.post2 = async function (url, params = {}) {
	await superagent.post(url)
		.timeout(0)
		.type('form')
		.then((response) => {
			console.log(response.text);
		}).catch((err) => {
			// err.message, err.response
			throw new Error(`请求失败:\n${JSON.stringify(err)}`);
		});
};

httpRequest.get = async function (url, search) {
	let params = _.cloneDeep(search);
	let res = {}, body = {}, reqUrl = '',
		check = params.hasOwnProperty('check') ? params.check : true;
	delete params.check;
	if (!url.includes('http')) {
		url = `${caps.url}${url}`;
	};
	const req = setUrlSearchParams(url, params);
	for (let key in params) {
		if (typeof (params[key]) == 'object') {
			body[key] = JSON.stringify(params[key]);
		} else {
			body[key] = params[key];
		};
	};
	reqUrl = decodeURI(req.url);

	const time = Date.now();
	// const t = setTimeout(() => {
	// 	console.log(`\nopTime:${moment(time).format('YYYY-MM-DD HH:mm:ss')}\n请求响应慢:${reqUrl}`);
	// }, 10000);
	await superagent.get(req.url.href)
		.timeout(0)
		.type('form')
		.query(body)
		.then((response) => {
			// if (params.apiKey == 'ec-spchb-dresShopShare-getDresShareQrCodeBase64') {
			// 	console.log(decodeURI(res.req.url));
			// };
			res = JSON.parse(JSON.stringify(response)); //
		}).catch((err) => {
			// err.message, err.response
			// err.req.url = decodeURI(err.req.url);
			logReqErrMsg({ err, reqUrl });
		});
	// clearTimeout(t);
	const duration = Date.now() - time,
		result = JSON.parse(res.text);
	res.header.date = moment().utc(res.header.date).format('YYYY-MM-DD HH:mm:ss');
	if (check && (result.code < 0 || result.hasOwnProperty('error'))) {
		// throw new Error(`opTime:${res.header.date}\n\turl:${reqUrl}\n\tresult:${res.text}`);
		throw new Error(`opTime:${res.header.date}\n\turl:${reqUrl}\n\tbody:${JSON.stringify(body)}\n\tresult:${res.text}`);
	};
	httpRequest.logSlowMsg({ reqUrl, duration });
	return {
		reqUrl,
		params,
		result,
		opTime: res.header.date,
		duration
	};
};

function decodeURI(encodedUrl) {
	return decodeURIComponent(encodedUrl);//.replace(/\+/g, ' ')
};

function logReqErrMsg({ err, reqUrl }) {
	if (!err.response) {
		throw new Error(`请求失败:\n\turl:${reqUrl}\n\tresult:${JSON.stringify(err)}`);
	}
	const response = JSON.parse(JSON.stringify(err)).response;
	// \n\t text: ${response.text || ''}
	throw new Error(`请求失败:\n\t opTime:${moment().utc(response.header.date).format('YYYY-MM-DD HH:mm:ss')}\n\t url:${response.req.url}\n\t data:${JSON.stringify(response.req.data || '')}\n\t status: ${response.status}`);
};

httpRequest.superagent = superagent;

/**
 * setGlobalParam - 修改单个全局参数
 * @alias module:httpRequest
 * @async
 * @param {string}    code         参数代码
 * @param {string}    val          参数值
 * @param {boolean} [check=true] 断言 参数修改是否成功
 * @return {object} check=false时,返回请求结果 {val:ok}
 */
httpRequest.setGlobalParam = async function (code, val, check = true) {
	let paramReturn = await httpRequest.callInterface('cs-param', {
		'code': code,
		'val': val
	});
	if (check) {
		expect(paramReturn, `修改系统参数${code}:${val}失败,reason:${JSON.stringify(paramReturn)}`).to.includes({
			val: 'ok'
		});
	} else {
		return paramReturn;
	};
};

/**
 * setGlobalParams - 批量保存系统参数
 * @alias module:httpRequest
 * @async
 * @param {array} paramsArr [{code,val},{code,val}]
 */
httpRequest.setGlobalParams = async function (paramsArr) {
	for (let i = 0, length = paramsArr.length; i < length; i++) {
		await httpRequest.setGlobalParam(paramsArr[i].code, paramsArr[i].val);
	};
};

//mdm 角色列表
httpRequest.getRoleList = async function () {
	if (!BASICDATA.roleList) {
		BASICDATA.roleList = await httpRequest.apiDo({
			apiKey: 'ec-om-role-list',
			jsonParam: {
				flagGte: 0
			}
		});
	};
	return BASICDATA.roleList;
};

/**
 * setRoleParam - 设置角色参数
 * @description 修改后,重新登录才能生效
 * @alias module:httpRequest
 * @async
 * @param {object} param {ipadcode,code,val}
 * @param {string} param.ipadcode  角色编码 gm:总经理,tally:财务员,inventory:仓管,aa:采购员,shopmanager:店长,boss:开单员,
 *  							seller:营业员,invdistributor:配货员
 * @param {string} param.code
 * @param {string} param.val
 */
httpRequest.setRoleParam = async function (param) {
	let res;
	if (USEECINTERFACE == 1) {
		res = await httpRequest.callInterface('cs-updateRoleParam', param);
		expect(res, `设置角色参数cs-updateRoleParam失败\nparam:${JSON.stringify(param)}\nresult:${JSON.stringify(res)}`).to.includes({
			val: 'ok'
		});
	} else {
		//获取角色id
		let ownerId = await httpRequest.getRoleList().then((res) => {
			let id;
			for (let i = 0, length = res.result.data.rows.length; i < length; i++) {
				if (res.result.data.rows[i].code == param.ipadcode) {
					id = res.result.data.rows[i].id;
					break;
				};
			};
			return id;
		});

		//配置子系统接口文档 保存参数值
		res = await httpRequest.apiDo({
			apiKey: 'ec-config-param-saveOwnerVal',
			jsonParam: {
				ownerKind: 11,
				data: [{
					domainKind: 'role',
					ownerId: ownerId,
					code: param.code,
					val: param.val,
				},],
			},
		});
		if (res.result.error) throw new Error(`修改角色参数失败:${res}`);
	};
	return res;
};

/**
 * setDataPrivilege - 设置角色数据权限 (角色记录权限)
 * @description 二代替换为 ec-config-priv-roleRow-saveInBatch
 * @alias module:httpRequest
 * @async
 * @param {string} role      角色编码
 * gm:总经理, tally:财务员, inventory:仓管, aa:采购员, shopmanager:店长,
 * boss:开单员, seller:营业员, invdistributor:配货员
 * @param {string} powerType 权限类型 -1:所有部门 -2:本部门即下级部门 -3:仅本人创建 -4:仅本人拥有
 */
httpRequest.setDataPrivilege = async function (role, powerType) {
	let res;
	if (USEECINTERFACE == 1) {
		let params = {
			'roleCode': role,
			'powerType': powerType
		};
		res = await httpRequest.callInterface('cs-setDataPrivilegeByRoleCode', params);
		expect(res, `cs-setDataPrivilegeByRoleCode,设置角色记录权限失败\nparams:${JSON.stringify(params)}\nresult:${JSON.stringify(res)}`).not.to.have.property('error');
	} else {
		let roleId = await httpRequest.getRoleList().then((res) => {
			let id;
			for (let i = 0, length = res.result.data.rows.length; i < length; i++) {
				if (res.result.data.rows[i].code == role) {
					id = res.result.data.rows[i].id;
					break;
				};
			};
			return id;
		});

		const value = ['', '2,0', '2,-1', '11,-1', '13,-1'][Math.abs(powerType)].split(',');

		res = await httpRequest.apiDo({
			apiKey: 'ec-config-priv-roleRow-saveInBatch',
			jsonParam: {
				roleId: roleId,
				funcId: '',
				items: [{
					dim: value[0],
					scope: value[1],
				}],
			},
		});
	};
	return res;
};

/**
 * setToggleRoleColumn - 设置角色敏感字段
 * @alias module:httpRequest
 * @async
 * @param {object} param
 * @param {string} param.rolecode 角色编码
 * 			gm:总经理, tally:财务员, inventory:仓管, aa:采购员, shopmanager:店长,
 * 			boss:开单员, seller:营业员, invdistributor:配货员
 * @param {string} param.columncode 敏感字段编码 {@link http://c.hzdlsoft.com:7082/Wiki.jsp?page=商陆花敏感字段}
 * @param {string} param.off 1:关闭 0:启用
 */
httpRequest.setToggleRoleColumn = async function (param) {
	let res;
	if (USEECINTERFACE == 1) {
		res = await httpRequest.callInterface('cs-toggleRoleColumn', param);
		expect(res, `设置角色敏感字段失败:${JSON.stringify(res)}`).to.includes({
			'val': 'ok'
		});
	} else {
		let roleId = await httpRequest.getRoleList().then((res) => {
			let id;
			for (let i = 0, length = res.result.data.rows.length; i < length; i++) {
				if (res.result.data.rows[i].code == param.rolecode) {
					id = res.result.data.rows[i].id;
					break;
				};
			};
			return id;
		});
		res = await httpRequest.apiDo({
			apiKey: 'ec-config-priv-roleCol-saveInBatch',
			jsonParam: {
				roleId: roleId,
				funcId: 0,
				items: [{
					colAlias: param.columncode,
					mode: param.off == 1 ? 0 : 31,
				}],
			},
		});
	};
	return res;
};

/**
 * getResults - 获取接口返回结果
 * @alias module:httpRequest
 * @async
 * @param {object} obj {interfaceid:param}
 * @return {object} {interfaceid:result}
 */
httpRequest.getResults = async function (obj) {
	let json = {};
	let promises = _.mapValues(obj, (value, key) => {
		let today = value.today == true ? true : false;
		delete value.today;
		let param = key.includes('ql-') ? format.qlParamsFormat(value, today) : value;
		// if(key == 'ql-1209') console.log(`param = ${JSON.stringify(param)}`);
		return httpRequest.callInterface(key, param);
	});
	let keys = Object.keys(promises);
	let results = await Promise.all(Object.values(promises));
	keys.forEach((key, index) => {
		if (results[index].error) {
			obj[key].interfaceid = key;
			throw new Error(`接口${key}请求失败:\n\tparams:${JSON.stringify(obj[key])}\n\tresult:${JSON.stringify(results[index])}`);
		};
		json[key] = results[index];
	});
	return json;
};


/**
 * editBilling - 新增单据
 * @alias module:httpRequest
 * @async
 * @param {object}    jsonparam    sf接口的jsonparam 需要含interfaceid
 * @param {boolean} [check=true] 是否需要断言
 * @return {object} {params:jsonparam,result:结果,duration:持续时间(ms)}
 */
httpRequest.editBilling = async function (jsonparam, check = true) {
	jsonparam = format.jsonparamFormat(_.cloneDeep(jsonparam));
	let time = Date.now();
	let result = await httpRequest.callInterface(jsonparam.interfaceid, { jsonparam, check });
	let duration = Date.now() - time;

	//开单成功后添加延迟,等待qf,ql接口的数据同步
	if (USEECINTERFACE == 2) {
		await new Promise((resolve) => setTimeout(resolve, 500));
		if (jsonparam.interfaceid == 'sf-1924-1') result.orderno = result.billno;
	};

	return {
		params: jsonparam,
		result,
		duration,
	};
};

/**
 * fetchMatInfo - 获取货品信息
 * @description 2代都使用tenantSpuId 18-05-08
 * @alias module:httpRequest
 * @async
 * @param {string} styleid 款号id
 * @return {object} 货品信息
 */
httpRequest.fetchMatInfo = async function (styleid) {
	let param = {
		'pk': styleid,
	};
	// if(USEECINTERFACE == 2) {
	// 	// 新增sf-1511返回 {val:款号在当前用户门店的id,tenantSpuId:租户级的款号ID}
	// 	// 获取款号信息qf-1511需要款号在当前用户门店的id
	// 	const res = await httpRequest.callInterface('cs-getpk-style', {
	// 		tenantSpuId: styleid, //租户款号id
	// 		shopId: LOGINDATA.invid, //本门店
	// 	});
	// 	//sf-1511返回的val为款号在当前用户门店的id,则查询不到,不替换
	// 	if(res.id) param.pk = res.id; //款号在当前用户门店的id
	// };
	const matInfo = await httpRequest.callInterface('qf-1511', param);
	expect(matInfo, `qf-1511获取货品信息失败:params:${JSON.stringify(param)},result:${JSON.stringify(matInfo)}`).to.not.have.property('error');
	return matInfo;
};

/**
 * switchCluster - 快速切换集群并设置可用账套
 * @param {object} params
 * @param {string} params.sn 企业编号
 * @param {string} params.mac MAC地址
 * @param {string} params.msgUrl 消息地址
 * @param {string|Number} params.deviceType 设备类型 ipad:0,iphone:9,'店员助手':11,Android:12,'仓库助手ipad版本':13
 */
httpRequest.switchCluster = async function (params) {
	await superagent.post(`http://${params.url}/slh/switchCluster.do`)
		.send({
			sn: params.sn,
			mac: params.deviceno,
			msgUrl: params.socketURL,
			deviceType: params.deviceType || 0,
		})
		.type('form')
		.then((res) => {
			// console.log(`data = ${JSON.stringify(data)}`);
			expect(JSON.parse(res.text)).not.to.have.property('error');
			caps.url = params.url;
		}).catch((err) => {
			throw new Error();
		});
};

/**
 *  根据sn获取客户服务器
 *  @description sn取后8位
 *  http://115.231.110.253:7080/mycrm/showInterface.do?interfaceid=ql-getServerURLbySN
 *  @return {object} { slhurl_master:'线路1',slhurl_slave:'线路2',slhurl_ssl_master:'ssl线路1',slhurl_ssl_slave:'ssl线路2',socketurl:'socketurl服务器'}
 */
httpRequest.getServerURLbySN = async function () {
	let serverURL;
	await superagent.post(`http://hzdlsoft.com:7080/mycrm/callInterface.do`)
		.query(`interfaceid=ql-getServerURLbySN`)
		.query(`sn=${caps.sn.slice(-8)}`)
		.then((res) => {
			serverURL = JSON.parse(res.text);
		}).catch((err) => {
			throw new Error(`请求失败:${err.message}`);
		});
	return serverURL;
};
