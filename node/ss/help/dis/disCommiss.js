const DisPartner = require('./disBase');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp')
const ssReq = require('../ssReq');
const common = require('../../../lib/common');
const ssAccount = require('../../data/ssAccount');
const ssConfigParam = require('../../help/configParamManager');
const basicJson = require('../basicJson');
const dresManage = require('../dresManage');


/**佣金 */
class Commission {
    constructor(params) {
        /**分销商id */
        this.id = '';
        /**推广商使用状态 */
        this.flag = '';
        /**推广官标签 */
        this.tag = '';
        /**锁标志 */
        this.lockFlag = 0;
        /**分组id */
        this.groupId = '';
        /**分组名称 */
        this.groupName = '';
        /**佣金比例 */
        this.brokerageRate = 1;
        /**推广金额 */
        this.totalBillMoney = 0;
        /**推广订单数 */
        this.disOrders = 0;
        /**累计佣金 */
        this.totalAmount = 0;
        /**打款佣金 */
        this.withdrawedAmount = 0;
        /**销售数 */
        this.salesNum = 0;
        /**等级 */
        this.level = {};
        /**库存类型 */
        this.invType = -1;
        /**商品id */
        this.spuId = '';
        /**推广员个数 */
        this.disCount = 0;
        /**商品id列表 */
        this.spuIds = [];
        /**推广等级列表 */
        this.levelV1 = {};
        /**分组列表数据 */
        this.groupIdList = [];
        /**sku列表 */
        this.skuList;
        /**等级个数 */
        this.levelCount = 0;
        /**累计客户 */
        this.custCount = 0;
        /**商品map */
        this.disDresMap = new Map();
        /**分组map */
        this.groupMap = new Map();
        /**单商品map */
        this.ownDresInfo = new Map();
        /**多对单/多 map */
        this.disMultdresMap = new Map();
        /**等级map */
        this.disLevelMap = new Map();
        /**新增商品map */
        this.newlyDresMap = new Map();
        /**单商品锁map */
        this.dresLockMap = new Map();
    }


    /**申请一条龙 */
    async applyForPromotion() {
        let newMobile = common.getRandomMobile(), newRealName = common.getRandomChineseStr(4);
        //卖家登录
        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        const sellerInfo = _.cloneDeep(LOGINDATA);
        const ssDistributeFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_distribute_flag', ownerId: sellerInfo.tenantId });
        const ssBrokerageRate = await ssConfigParam.getSpbParamInfo({ code: 'ss_brokerage_rate', ownerId: sellerInfo.tenantId });
        const visitorLogin = await ssConfigParam.getSpbParamInfo({ code: 'ss_shop_visitor_login_in_or_not', ownerId: LOGINDATA.tenantId });
        const whoCanViewMyShop = await ssConfigParam.getSpbParamInfo({ code: 'who_can_view_my_shop', ownerId: LOGINDATA.tenantId });
        await whoCanViewMyShop.updateParam({ val: 1 });
        await ssDistributeFlag.updateParam({ val: 1 });
        await ssBrokerageRate.updateParam({ val: 1 });
        await visitorLogin.updateParam({ val: 1 });
        await whoCanViewMyShop.updateParam({ val: 0 });
        // this.parameTers();

        await common.delay(1000);
        //买家登录
        // await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
        await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
        const userInfo = _.cloneDeep(LOGINDATA);
        //申请成为推广官
        try {
            let i = 1, disApplyMsg = '', res;
            // const errMsg = '您还不是该店会员，请稍后重试', errMsg2 = '会员信息未绑定，请重新扫码关联店铺';
            const msg = '成功'
            while (disApplyMsg != msg) {
                if (i > 20) throw new Error(`判断申请状态状态已超过20次`);
                res = await ss.dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: newRealName, mobile: userInfo.mobile, openId: userInfo.wxOpenId });
                disApplyMsg = res.result.msg;
                console.log(`\n 第${i}次：disApplyMsg=${JSON.stringify(disApplyMsg)}`);
                await common.delay(500);
                i++;
            }

            this.id = res.result.data.disPartnerId;
            this.realName = res.params.realName;
            this.mobile = res.params.mobile;
            this.nickName = userInfo.nickName;

            await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
            //卖家 查看推广商列表
            await ss.dis.listDisPartner({ auditFlag: 0 });
            //同意申请
            const disApply = await ss.dis.verifyDisApply({ disId: this.id, auditFlag: 1 });
            this.updateDisBaseInfo(disApply.params)

            return this.id;
        } catch (err) {
            throw new Error(`推广申请失败:${err}`);
        } finally {
            await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        }


    };

    /**
     * 更新推广官信息
     */
    updateDisBaseInfo(params) {
        if (params.auditFlag == 1) {
            this.flag = 1;
            this.auditTime = common.getCurrentTime();
            this.auditFlag = 1;
            this.validFlag = 1;
            this.tag = '';
            this.lockFlag = 0;
            this.totalBillMoney = 0;
            this.disOrders = 0;
            this.totalAmount = 0;
            this.custCount = 0;
            this.withdrawedAmount = 0;
        }
    }

    /**
   * 卖家 修改推广员对应商品信息（支持批量）
   * @param {object} params
   * @param {object} params.disSpus
   * @param {object} params.disSpus.disPartnerId	
   * @param {object} params.disSpus.brokerageRate
   * @param {object} params.disSpus.lockFlag
   * @param {object} params.disSpus.spuId
   */
    async updateDisSpu(params = {}) {
        this.updateDresLock(params)
        const disSpu = [];
        params.dresList.forEach(dres => {
            let res = this.getDresLockExp(dres.spuId);
            if (res.lockFlag == 1) {
                const check = false;
                let data = this.getDisListExp(dres.spuId, check);
                if (!data) {
                    data = this.getNewlyDresExp(dres.spuId, check)
                    if (!data) {
                        throw new Error(`没有找到该spu：${dres.spuId}`)
                    }
                }
                else if (params.lockFlag == 0) {
                    disSpu.push({
                        disPartnerId: this.id,
                        brokerageRate: params.brokerageRate,
                        lockFlag: params.lockFlag,
                        spuId: dres.spuId
                    });
                    this.updateDisDresAll(params);
                } else if (params.lockFlag == 1) {
                    const data = this.getDisListExp(dres.spuId);
                    disSpu.push({
                        disPartnerId: this.id,
                        brokerageRate: data.brokerageRate,
                        lockFlag: params.lockFlag,
                        spuId: dres.spuId
                    })
                    //TODO 这里params的比例应该为data.brokerageRate
                    const paramsFix = _.cloneDeep(params);
                    paramsFix.brokerageRate = data.brokerageRate;
                    this.updateDisDresAll(paramsFix);
                }
            } else if (res.lockFlag == 0) {
                disSpu.push({
                    disPartnerId: this.id,
                    brokerageRate: params.brokerageRate,
                    lockFlag: params.lockFlag,
                    spuId: dres.spuId
                });
                this.updateDisDresAll(params);
            }
        });

        const update = await ss.dis.updateDisSpu({
            disSpus: disSpu
        }).then(res => res);
        // this.updateDresLock(params)

        return update;
    }

    /**商品锁更新 */
    updateDresLock(params) {
        params.dresList.forEach(dres => {
            const res = this.getDresLockExp(dres.spuId)

            if (res == undefined) {
                const spuId = dres.spuId;
                // const spu = this.dresLockMap.has(spuId) ? this.dresLockMap.get(spuId) : new DresLockMap();
                const spu = new DresLockMap();
                spu.spuId = spuId;
                spu.lockFlag = params.lockFlag

                this.dresLockMap.set(spuId, spu);
            } else if (res.lockFlag == 0) {
                const spuId = dres.spuId;
                const spu = this.dresLockMap.get(spuId);
                spu.spuId = spuId;
                spu.lockFlag = params.lockFlag;

                this.dresLockMap.set(spuId, spu);
            }
            else if (res.lockFlag == 1) {
                const spuId = dres.spuId;
                const spu = new DresLockMap();
                spu.spuId = spuId;
                if (params.lockFlag != 0) {
                    spu.lockFlag = 1;
                } else { spu.lockFlag = 0 };


                this.dresLockMap.set(spuId, spu);
            }

        });
    };

    /**商品锁期望值 */
    getDresLockExp(spuId) {
        const res = this.dresLockMap.get(spuId);
        return res;
    };

    /**单人多商品更新 */
    updateDisDresAll(params = {}) {
        params.dresList.forEach(dres => {
            const spuId = dres.spuId;
            const spu = this.disDresMap.has(spuId) ? this.disDresMap.get(spuId) : new DisdresMap();
            spu.disPartnerId = this.id;
            spu.spuId = spuId;
            spu.title = dres.title;
            spu.pubPrice = dres.pubPrice;
            spu.brokerageRate = params.brokerageRate;
            spu.marketDate = dres.marketDate;
            spu.lockFlag = params.lockFlag || 0;
            spu.invNum = dres.invNum;

            this.disDresMap.set(spuId, spu);
        })
    };


    /**卖家 获取推广商品列表
     * @param {object} params
     * @param {object} params.disPartnerId 不传返回全部等级对应佣金比例，传则返回当前推广官对应的等级以及佣金比例
     */
    async findDisPartnerSpu(params = {}) {
        const res = await ss.dis.findDisPartnerSpu({ orderBy: 'marketDate', orderByDesc: true, ...params });
        this.spuIds = res.result.data.rows.map(arr => arr.spuId)
        return res;
    }

    /**单人 分销商品列表\详情期望值 */
    getDisListExp(spuId, check = true) {
        if (typeof (spuId) == 'object') {
            const spuList = [];
            for (let i = 0; i < spuId.length; i++) {
                const res = this.disDresMap.get(spuId[i]);
                spuList.push(res);
            }
            return spuList

        }
        const res = this.disDresMap.get(spuId);
        if (check && res == undefined) {
            throw new Error('没有在disDresMap找到该spu的数据')
        }
        return res;
    }

    /**
     * 卖家 查询推广商品详情
     * @param {object} params
     * @param {string} params.spuId 商品spuid
     * @param {string} params.disId 分销商id
     */
    async disPartnerSpuDetail(params = {}) {
        const res = await ss.dis.disPartnerSpuDetail({ ...params });
        return res;
    }


    /**
     * 修改商品各等级佣金比例（支持批量）
     * @param {object} params
     * @param {object} params.spuIds
     * @param {object} params.levels
     * @param {object} params.levels.id
     * @param {object} params.levels.brokerageRate
     */
    async updateSpuLevelRate(params = {}) {
        // if (params.AllFlag == true) {
        // let levelData = { spuIds: [], levels: [{ id: this.levelV1.id, brokerageRate: this.levelV1.brokerageRate }] };
        let levelData = { 'spuIds': [], 'levels': [] };
        const levelName = params.levelName;
        const spuList = params.spus

        spuList.forEach(obj => levelData.spuIds.push(obj.spuId));
        this.updateOwnDisDres(spuList);
        const levels = params.levels;
        levels.forEach(ele => levelData.levels.push({ id: ele.id, brokerageRate: ele.brokerageRate }));

        const res = await ss.dis.updateSpuLevelRate(levelData);
        // console.log(res.params.jsonParam.levels);

        this.updateMultDisDres(levelData, levelName);
        return res;

    }


    /**更新单商品 */
    updateOwnDisDres(spuList) {
        spuList.forEach(obj => {
            const spus = this.ownDresInfo.has(obj.spuId) ? this.ownDresInfo.get(obj.spuId) : new OwnDresInfo();
            spus.spuId = obj.spuId;
            spus.title = obj.title;
            spus.pubPrice = obj.pubPrice;
            spus.lockFlag = obj.lockFlag;

            this.ownDresInfo.set(obj.spuId, spus);
        })
    }

    /**单商品期望值 */
    getDisOwnDresInfoExp(spuId) {
        const res = this.ownDresInfo.get(spuId);
        if (res == undefined) {
            throw new Error('没有在ownDresInfo找到该spu的数据')
        }
        return res;
    }



    /**更新多人单/多商品 */
    updateMultDisDres(levelData, levelName) {
        levelData.levels.forEach(obj => {
            const levels = []
            const level = this.disMultdresMap.has(levelName) ? this.disMultdresMap.get(levelName) : new DisMultdresMap();
            // level.id = obj.id;
            // level.brokerageRate = obj.brokerageRate;
            level.disPartnerId = this.id;
            // level.spuList = levelData.spus;
            levels.push({ id: obj.id, brokerageRate: obj.brokerageRate })
            level.levels = levels

            this.disMultdresMap.set(levelName, level);
        })
    }

    /**开启等级推广商列表佣金比例期望值 */
    getDisPartnerSpuExp(levelName) {
        const res = this.disMultdresMap.get(levelName);
        // if (res.spuList.length == 1) {
        //     res.spuList = res.spuList[0]
        // }
        if (res == undefined) {
            throw new Error('没有找到spu的数据')
        }
        return res;
    }

    /**
     * 新增商品
     * 
     */
    async saveNewDres() {
        await dresManage.prePrepare();
        const json = basicJson.styleJson();
        console.log(`json=${JSON.stringify(json)}`);

        const newDres = await sp.spdresb.saveFull(json);
        this.spuId = newDres.result.data.spuId;
        const spuId = newDres.result.data.spuId;

        this.updateDres(newDres, json);
        return spuId
    };

    /**新增商品更新 */
    updateDres(newDres, json) {
        const spuId = newDres.result.data.spuId;
        const spu = this.newlyDresMap.has(spuId) ? this.newlyDresMap.get(spuId) : new NewlyDresMap();
        spu.spuId = spuId;
        spu.title = json.spu.title;
        spu.pubPrice = json.spu.pubPrice;
        let cont = json.skus.map(sku => sku.num);
        spu.invNum = eval(cont.join('+'));
        spu.marketDate = common.getCurrentDate();

        this.newlyDresMap.set(spuId, spu);
    };

    getNewlyDresExp(spuId, check = true) {
        if (typeof (spuId) == 'object') {
            const spuList = [];
            for (let i = 0; i < spuId.length; i++) {
                const res = this.newlyDresMap.get(spuId[i]);
                spuList.push(res);
            }
            return spuList

        }
        const res = this.newlyDresMap.get(spuId);
        if (check && res == undefined) {
            throw new Error('没有在newlyDresMap找到该spu的数据')
        }
        return res;
    }

    /**
     * 卖家 查询分销商详情
     * @param {object} params
     * @param {string} params.disId 分销商id
     */
    async disPartnerDetail(params) {
        const res = await ss.dis.disPartnerDetail(params);
        return res;
    };

    /**
     * 卖家 新增/编辑分组信息
     * @param {object} params
     * @param {number} params.groupId
     * @param {string} params.groupName 分组名称
     * @param {list} params.disIds 分销商id列表
     */
    async saveNewgroup(params = {}) {
        const res = await ss.dis.saveDisPartnerGroup({ ...params });
        if (params.id == undefined) {
            this.groupId = res.result.data.groupId;
            this.groupName = params.groupName;
            const groupList = [{
                groupId: this.groupId,
                groupName: this.groupName,
                disCount: 0,
                usefulFlag: 1
            }];
            this.updateDisGroup(groupList);
            // console.log(res);
            return res;
        } else {
            const groupList = [{
                groupId: params.id,
                groupName: params.groupName,
                disIds: params.disIds,
                disCount: params.disIds.length,
            }];
            this.updateDisGroup(groupList);
            return res;
        }

    };

    /**
     * 获取分组列表
     * @param {object} params
     * @param {number} params.isRelateLevel 
     */
    async findDisPartnerGroup(params = {}) {
        const res = await ss.dis.findDisPartnerGroup({ ...params }).then(res => res);
        const groupList = res.result.data.rows;
        this.groupIdList = res.result.data.rows.map(obj => obj.groupId);
        this.updateDisGroup(groupList)
        return res;
    };

    /**分组更新方法 */
    updateDisGroup(groupList) {
        groupList.forEach(obj => {
            const groupId = obj.groupId || this.groupId;
            const group = this.groupMap.has(groupId) ? this.groupMap.get(groupId) : new GroupMap();
            group.groupId = groupId;
            group.groupName = obj.groupName;
            group.disCount = obj.disCount;
            group.usefulFlag = obj.usefulFlag ? obj.usefulFlag : 1;

            this.groupMap.set(groupId, group);
        })

    }

    /**分组列表期望值 */
    getFindDisPartnerGroupExp(groupId) {
        const res = this.groupMap.get(groupId);
        if (res == undefined) {
            throw new Error('没有找到该组的数据')
        }
        return res;
    }

    /**
     * 获取分组详情
     * @param {object} params
     * @param {number} params.groupId
     */
    async disGroupDetail(params = {}) {
        const res = await ss.dis.disGroupDetail({ groupId: params.groupId });
        return res;
    };

    /**
     * 删除分组
     * @param {object} params
     * @param {object} params.groupId
     */
    async delDisPartnerGroup(params = {}) {
        const res = await ss.dis.delDisPartnerGroup({ ...params });
        return res;
    }

    /**
     * 获取推广员等级列表
     * @param {object} params
     */
    async findDisPartnerLevel(params = {}) {
        const res = await ss.dis.findDisPartnerLevel();
        this.levelV1 = res.result.data.rows[0];

        return res;
    };

    /**
     * 卖家 修改分销商信息
     * @param {object} params
     */
    async updateDisInfo(params = {}) {
        const res = await ss.dis.updateDisInfo({
            id: params.id || this.id,
            flag: params.flag || 1,
            realName: params.realName || this.realName,
            mobile: params.mobile || this.mobile,
            tag: params.tag || this.tag,
            groupName: params.groupName || this.groupName,
            groupId: params.groupId || this.groupId
            , ...params
        });
        params.groupId ? this.groupId = params.groupId : this.groupId = this.groupId;

        const disList = this.getFindDisPartnerGroupExp(params.groupId).disList;
        const groupList = [{
            groupId: params.groupId || this.groupId,
            groupName: params.groupName || this.groupName,
            disCount: this.getFindDisPartnerGroupExp(params.groupId).disCount + 1,
            disList: disList.push({ realName: params.realName || this.realName }),
        }];
        this.updateDisGroup(groupList)
        return res;
    };

    /**
     * 保存推广等级
     * @param {object} params
     * @param {object} params.disLevel
     * @param {object} params.levelName
     * @param {object} params.groupName
     * @param {object} params.groupId
     * @param {object} params.brokerageRate
     */
    async saveDisPartnerLevel(params = {}) {
        //必须存在V1等级
        const disLevel = [{
            id: this.levelV1.id,
            levelName: this.levelV1.levelName,
            groupName: this.levelV1.groupName,
            groupId: this.levelV1.groupId,
            brokerageRate: this.levelV1.brokerageRate || 1,
        }];

        if (params.disLevel) {
            params.disLevel.forEach(obj => {
                const choiceType = obj.choiceType;
                switch (choiceType) {
                    case 'newly':
                        disLevel.push({
                            levelName: obj.levelName,
                            groupName: obj.groupName,
                            groupId: obj.groupId,
                            brokerageRate: obj.brokerageRate || 1
                        })
                        break;
                    case 'fix':
                        disLevel.push({
                            id: obj.id,
                            levelName: obj.levelName,
                            groupName: obj.groupName,
                            groupId: obj.groupId,
                            brokerageRate: obj.brokerageRate || 1
                        })
                        break;
                    default:
                        break;
                };

            });
        }
        const res = await ss.dis.saveDisPartnerLevel({ disLevel });
        this.levelCount = disLevel.length
        this.updateLevel(disLevel);
        return res;

    }

    /**更新等级 */
    updateLevel(disLevel) {
        disLevel.forEach(obj => {
            const level = this.disLevelMap.has(obj.levelName) ? this.disLevelMap.get(obj.levelName) : new DisLevelMap();
            if (level.id) {
                level.id = obj.id;
            }
            level.levelName = obj.levelName;
            level.groupId = obj.groupId;
            level.groupName = obj.groupName;
            level.brokerageRate = obj.brokerageRate;
            this.disLevelMap.set(obj.levelName, level);
        })

    }

    /**等级列表总数期望值 */
    getDisLevelCountExp() {
        return {
            total: this.levelCount,
            count: this.levelCount,
        }
    }

    /**等级列表数据期望值 */
    getDisLevelDataExp(levelName) {
        const res = this.disLevelMap.get(levelName);
        if (res == undefined) {
            throw new Error('没有找到该等级的数据');
        }
        return res
    }

    /**
     * 卖家 查询商品列表
     */
    async dresSpuList(params = {}) {
        const spuList = await ss.spchb.getDresSpuList({ pageNo: 1, pageSize: 10, ...params });
        // this.spuIds = spuList.result.data.rows.map(spuId => spuId.id)
        return spuList;

    };

    /**
     * 获取商品库存信息
     * @param {object} params
     * @param {object} params.spuId
     */
    async findDisSkus() {
        const res = await ss.dis.findDisSkus({ spuId: this.spuId });
        this.skuList = res.result.data.invList
        this.customId = this.skuList[0].id
        return res;
    }



    /**
     * 保存推广自定义库存
     * @param {object} params
     * @param {object} params.spuId
     * @param {object} params.skus
     * @param {object} params.skus.id
     * @param {object} params.skus.skuId
     * @param {object} params.skus.invNum
     */
    async saveDisSkus(params = {}) {
        const res = await ss.dis.saveDisSkus({
            spuId: this.spuId,
            skus: [{ skuId: this.skuList[0].skuId, invNum: this.skuList[0].skuNum }],
            ...params
        });
        return res;
    }

    /**
     * 删除分销商
     * @param {object} params
     * @param {object} params.id 推广商id
     */
    async delDisPartner(params) {
        const res = await ss.dis.delDisPartner(params).then(res => res);
        return res;
    }

    /**设置全局佣金参数
     * 
     */
    async saveOwnerVal(params) {
        await ss.config.saveOwnerVal(params);
    }

    /**无等级推广商列表期望值 */
    getNoLevelDisSpuExp() {
        return {
            title: this.title,
            pubPrice: this.pubPrice,
            marketDate: this.marketDate,
            invNum: this.invNum,
            salesNum: this.salesNum,
            brokerageRate: 1,
            lockFlag: this.lockFlag,
            levels: this.level,
        }
    }

    /**无等级推广商详情期望值 */
    getNoLevelDisSpuDetailExp() {
        return {
            title: this.title,
            pubPrice: this.pubPrice,
            invNum: this.invNum,
            brokerageRate: 1,
            lockFlag: this.lockFlag,
            levels: this.level,
        }
    }
    /**推广商详情期望值 */
    getDisPartnerDetailExp() {
        return {
            id: this.id,
            flag: this.flag,
            auditFlag: this.auditFlag,
            auditTime: this.auditTime,
            realName: this.realName,
            mobile: this.mobile,
            nickName: this.nickName,
            lockFlag: this.lockFlag,
            tag: this.tag,
            groupName: this.groupName,
            groupId: this.groupId,
            totalBillMoney: this.totalBillMoney,
            disOrders: this.disOrders,
            totalAmount: this.totalAmount,
            custCount: this.custCount
        }
    }



    /**分组人数期望值 */
    getFindDisGroupDetailCountExp() {
        return {
            id: this.groupId,
            disCount: this.disCount,
            groupName: this.groupName
        }
    }




};


const commission = module.exports = {};

commission.setupDis = function () {
    return new Commission();
};


/**新增商品map */
function NewlyDresMap() {
    /**商品id */
    this.spuId = '';
    /**商品名称 */
    this.title = '';
    /**发布价 */
    this.pubPrice = 0;
    /**上架时间(添加时间) */
    this.marketDate = 0;
    /**库存 */
    this.invNum = 0;
}


/**单对单/多 map */
function DisdresMap() {
    /**分销商id */
    this.disPartnerId = '';
    /**佣金比例 */
    this.brokerageRate = 1;
    /**是否锁定 1锁定 0不锁定 */
    this.lockFlag = 0;
    /**商品id */
    this.spuId = '';
    /**商品名称 */
    this.title = '';
    /**发布价 */
    this.pubPrice = 0;
    /**上架时间(添加时间) */
    this.marketDate = 0;
    /**库存 */
    this.invNum = 0;
    /**销量 */
    // this.salesNum = 0;
    /**等级信息 */
    // this.levels = {};
}


/**多对多/单，有等级佣金比例 map */
function DisMultdresMap() {
    /**分销商id */
    this.disPartnerId = '';
    /**等级佣金比例 */
    this.levels = [];
}


/**单商品的 map */
function OwnDresInfo() {
    /**商品id */
    this.spuId = '';
    /**商品标题 */
    this.title = '';
    /**商品发布价 */
    this.pubPrice = '';
    /**是否锁定 1锁定 0不锁定 */
    this.lockFlag = '';
}


/**分组map */
function GroupMap() {
    /**分组id */
    this.groupId = '';
    /**分组名 */
    this.groupName = '';
    /**推广员个数 */
    this.disCount = 0;
    /**是否可选 1可选 0不可选 */
    this.usefulFlag = 1;
    /**分组列表数据 */
    this.disList = [];
}


/**等级map */
function DisLevelMap() {
    /**等级id */
    // this.id = ''
    /**等级名称 */
    this.levelName = '';
    /**分组id */
    this.groupId = '';
    /**关联分组名称 */
    this.groupName = '';
    /**佣金比例 */
    this.brokerageRate = 0;
}

/**商品锁定map */
function DresLockMap() {
    /**商品id */
    this.spuId = '';
    /**锁定状态 */
    this.lockFlag = '';
}