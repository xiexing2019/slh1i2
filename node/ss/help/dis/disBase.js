const ss = require('../../../reqHandler/ss');
const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ssAccount = require('../../data/ssAccount');

class DisPartner {
    constructor(params) {
        /**  主键 */
        this.id = '';
        /**  关联会员id */
        this.memberId = '';
        /**  门店名称 */
        this.shopName = '';
        /**  门店logo */
        this.shopLogo = '';
        /**  门店描述 */
        this.shopDesc = '';
        /**   状态 0：停用，-1：删除，1，正常 */
        this.flag = 1;
        /**  分销等级 */
        this.level = 0;
        /**  上级分销 */
        this.parentId = '';
        /**  上上级分销 */
        this.grandpaId = '';
        /**  扩展字段 */
        this.extProps = {};
        /**  单元id */
        this.unitId = '';
        // /**  审核通过时间 */
        // this.auditTime = '';
        /**  真实姓名 */
        this.realName = '';
        /**  微信名字 */
        this.nickName = '';
        /**  电话号码 */
        this.mobile = '';
        /**  标签 */
        this.tag = '';
        /**  审核状态 状态 0：未审核，1：审核通过（启用）3：(审核不通过)拒绝 */
        this.auditFlag = 0;
        /** 申请时间 */
        this.auditTime = '';
        /**  分销价 */
        // this.disPrice=0;
        /**  订单佣金比例 */
        this.brokerageRate = 0;
        /**  微信openId */
        this.openId = '';

        /**分组名 */
        this.groupName
        /**分组id */
        this.groupId = '';
        /**推广金额 */
        this.totalBillMoney = 0;
        /**分销订单数 */
        this.disOrders = 0;
        /**累计客户 */
        this.myCustCount = 1;
        /**累计佣金 */
        this.totalBrokerage = 0;

        /**已提现金额 */
        this.withdrawedAmount = 0;
        /**可提现金额 */
        this.withdrawedAmount = 0;
        //提现明细
        this.withDrawCount = '';

        /** 申请状态 ： 0 不存在，1 审核通过（启用）,2 申请中 , 3 拒绝 */
        this.validFlag = 0;
        /**分销商 数据中心*/
        this.disDataMap = new Map();
    };

    /**
     * 获取卖家信息
     */
    // async getSellerInfo(params = {}) {
    //     this.sellerInfo = params
    // };


    /**
     * 申请成为分销商
     * @param {object} params
     * @param {string} params.sellerTenantId 卖家租户id
     * @param {string} [params.realName] 姓名，新用户申请时必传
     * @param {string} [params.mobile] 电话，新用户申请时必传
     * @param {string} [params.invCode] 新用户申请时不传，通过邀请码申请的必传
     */
    async applyDistribute(params = {}) {
        const applyDis = await ss.dis.applyDistribute({ ...params });
        if (params.check == false) {
            return applyDis;
        };
        this.id = applyDis.result.data.disPartnerId;
        common.update(this, params);
        this.auditTime = common.getCurrentDate();
        this.validFlag = 2;
        this.auditFlag = 0;
        this.nickName = LOGINDATA.nickName;

        // this.shopName = this.sellerInfo.shopName;
        // this.shopDesc = this.sellerInfo.shopDesc;
    };

    /** 
     * 查询当前角色申请状态
     * @description
     */
    async disPartnerCustFlag(params = {}) {

        const res = await ss.dis.getDisPartnerCustFlag({ sellerTenantId: params.sellerTenantId, ...params }).then(res => res.result.data);

        return res;
    };

    getDisPartnerFlagExp() {
        // TODO 确认未申请时的返回值
        return {
            id: this.id,
            validFlag: this.validFlag,
            realName: this.realName,
            mobile: this.mobile,
        };
    };

    /** 
     * 审核申请
     * @param {object} params
     * @param {string} params.auditFlag 审核状态 状态 0：未审核，1：审核通过（启用）3：(审核不通过)拒绝
     */
    async verifyDisApply(params = {}) {
        await ss.dis.verifyDisApply({ disId: this.id, ...params });
        this.auditFlag = params.auditFlag;
        this.validFlag = params.auditFlag;
        this.shopName = params.shopName;
        return this;
    };

    /**
     * 卖家 查询推广商列表
     * @param {object} params
     * @param {string} params.auditFlag 审核状态 状态 0：未审核，1：审核通过（启用）3：(审核不通过)拒绝
     */
    async listDisPartner(params = {}) {
        const disList = await ss.dis.listDisPartner({ ...params }).then(res => res.result.data.rows);
        // console.log(`${JSON.stringify(disList)}`);

        const res = disList.find(obj => obj.mobile == this.mobile)

        return disList;
    };

    /**卖家 查看推广商详情 */
    async disPartnerDetail(params = {}) {
        let disInfo = await ss.dis.disPartnerDetail({ disId: this.id, ...params }).then(res => res.result.data);
        return disInfo;
    };

    /**买家 切换分销商 */
    async switchDisPartner(params = {}) {
        await ssReq.switchDisPartner({ ...params });
        switchInfo = _.cloneDeep(LOGINDATA);
        return switchInfo
    };

    /**
     * 卖家 生成邀请码
     * @param {object} params
     */
    async invitationCodeBuilder() {
        let invCode = await ss.dis.invitationCodeBuilder().then(res => res.result.data.val);
        return invCode
    };

    /**
     * 获取推广商二维码
     * @param {object} params
     */
    async appCodeForShopSeller(params = {}) {
        // let userId;
        if (params.type == 2) {
            let userId = this.id
            const res = await ss.spmdm.getAppCodeForShopSeller({ userId: userId, ...params }).then(res => res.result.data);
            return res;
        }

    };

    /**
     * 查询推广中心首页数据
     * @param {*} params 
     */
    async disStatisticsData(params = {}) {
        let data = await ss.dis.getDisStatisticsData({ disId: this.id, ...params }).then(res => res.result.data);
        return data
    };

    /**
     * 我的客户列表
     * @param {*} params 
     */
    async myCustList(params = {}) {
        let data = await ss.dis.getMyCustList({ disPartnerId: this.id, lastVisitTime: true, ...params }).then(res => res.result.data);
        return data;
    };

    /**
     * 查看订单列表
     * @param {*} params 
     */
    async findDisOrders(params = {}) {
        let data = await ss.dis.findDisOrders({ disPartnerId: this.id, ...params }).then(res => res.result.data);
        return data;
    };

    /**
     * 获取推广总价
     * @param {*} params 
     */
    async totalBrokerage(params = {}) {
        let data = await ss.dis.getTotalBrokerage({ disPartnerId: this.id, ...params }).then(res => res.result.data);
        return data;
    };

    /**
    * 卖家 修改分销商信息
    * @param {object} params
    */
    async updateDisInfo(params = {}) {
        await ss.dis.updateDisInfo({ ...params }).then(res => res);
        this.flag = params.flag;
        this.realName = params.realName;
        this.mobile = params.mobile;
        this.tag = params.tag || this.tag;
        this.groupName = params.groupName || this.groupName;
        this.groupId = params.groupId || this.groupId;


    };

    /**
     * 删除分销商
     * @param {object} params
     * @param {object} params.id 推广商id
     */
    async delDisPartner(params = {}) {
        await ss.dis.delDisPartner(params).then(res => res)
    };


    /**
     * 分销商详情信息
     */
    getDisPartnerInfoExp() {
        return {
            id: this.id,
            flag: this.flag,
            auditFlag: this.auditFlag,
            auditTime: this.auditTime,
            realName: this.realName,
            mobile: this.mobile,
            nickName: this.nickName,
            tag: this.tag,
            totaldAmount: this.totalBillMoney,
            disOrders: this.disOrders,
            custCount: this.myCustCount,
        };
    };


    /**
     * 分销中心信息
     */
    getDisStatisticsDataExp() {
        return {
            shopName: this.shopName,
            withdrawedAmount: this.withdrawedAmount,
            withdrawableAmount: this.withdrawedAmount,
            totalBrokerage: this.totalBrokerage,
            disOrders: this.disOrders,
            withDrawCount: this.withDrawCount,
            myCustCount: this.myCustCount,
            disPartnerId: this.id
        }
    }

    /**
     * 客户列表信息
     */
    // getMyCustList() {
    //     return{
    //         custTenantId:this.,
    //         custUserId:,
    //         nickName:
    //     }
    // }



    updateDisData(params = {}) {
        const disData = this.disDataMap.has(this.id) ? this.disDataMap.get(this.id) : new DisDataMap();
        this.disDataMap.set(this.id, disData);
    }


};

module.exports = DisPartner;
// const disPartner = module.exports = {};

// disPartner.setupDis = function () {
//     return new DisPartner();
// };

/**分销商首页数据 */
function DisDataMap() {
    /**已提现金额 */
    this.withdrawedAmount = 0,
        /**可提现金额 */
        this.withdrawedAmount = 0,
        /**分销佣金 */
        this.totalBrokerage = 0,
        /**分销订单数 */
        this.disOrders = 0,
        /**提现明细 */
        this.withDrawCount = '',
        /**累计客户*/
        this.myCustCount = 0
};