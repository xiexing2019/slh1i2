const common = require('../../../lib/common');
const spCaps = require('../../../reqHandler/sp/spCaps');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const pinyin = require("pinyin4js");


class PriceTypes {
    constructor() {
        this.pricedec = '';
        this.pricedecOptions = [];
        this.priceTypes = new Map();
        this.selectItems = new Map();
    }

    async updatePricedec(shop, val) {
        const code = 'custom_rounding_method';
        await shop.getConfigParam({ code: code, ownerId: LOGINDATA.tenantId }, 'spg');
        await shop.updateConfigParam({ code: code, val: val });
        this.pricedec = val;
    }

    /**
     * 
     * @param {object} dres 
     */
    getExpForPrice(dres) {
        // console.log(dres);
        //[{ id: 0, name: "保留两位小数" }, { id: 1, name: "四舍五入取整" }, { id: 2, name: "向上取整" }, { id: 3, name: "向下取整" }];
        switch (this.pricedec) {
            case 0:
                for (let value = 1; value <= 5; value++) {
                    let price = `price${value}`;
                    const basicPriceType = this.priceTypes.get(value);
                    // let price = value == 0 ? 'pubPrice' : `price${value}`;
                    dres.skus.forEach(sku => { if (sku[price]) sku[price] = (sku.pubPrice * basicPriceType.percentage + basicPriceType.addition).toFixed(2) });
                    // if (!sku[price]) sku[price] = (sku.pubPrice * basicPriceType.percentage + basicPriceType.addition).toFixed(2);
                    dres.spu[price] = (dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition).toFixed(2);
                }
                break;
            case 1:
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = this.priceTypes.get(value);
                    console.log(basicPriceType);
                    let price = `price${value}`;
                    dres.skus.forEach(sku => { if (sku[price]) sku[price] = Math.round(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition) });
                    dres.spu[price] = Math.round(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
            case 2:
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = this.priceTypes.get(value);
                    let price = `price${value}`;
                    dres.skus.forEach(sku => { if (sku[price]) sku[price] = Math.ceil(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition) });
                    dres.spu[price] = Math.ceil(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
            case 3:
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = this.priceTypes.get(value);
                    let price = `price${value}`;
                    dres.skus.forEach(sku => { if (sku[price]) sku[price] = Math.floor(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition) });
                    dres.spu[price] = Math.floor(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
        }
    }

    /**
     * 获取价格下拉选项值list（app）
     */
    async getPriceTypeSelectItemInApp(params) {
        const res = await ss.spdresb.getPriceTypeSelectItemInApp(params);
        console.log(`\n 价格下拉选项值list=${JSON.stringify(res)}`);
        return res.result.data.rows;
    }

    /**
     * 获取价格list（app）
     */
    async getPriceTypeListInApp(params) {
        const res = await ss.spdresb.getPriceTypeListInApp(params);
        console.log(`\n 价格list=${JSON.stringify(res)}`);
        return res.result.data;
    }

    /**
     * 获取价格字典值(新增)
     * @description 返回启用的价格
     */
    async getPriceDict(params) {
        const res = await ss.spdresb.getPriceDict(params);
        console.log(`\n 启用价格list=${JSON.stringify(res)}`);
        return res.result.data.prices;
    }

    /**
     * 获取已存在价格类型列表
     */
    async getExistPriceList() {
        const data = await this.getPriceTypeListInApp();
        this.pricedec = data.pricedec;
        this.pricedecOptions = data.pricedecOptions;
        this.updataPriceTypes(data.priceTypes);
        const items = await this.getPriceTypeSelectItemInApp();
        this.updataSelectItems(items);
    }

    /**
     * 更新价格下拉选项值
     * @param {*} selectItems 
     */
    updataSelectItems(selectItems) {
        selectItems.forEach(item => this.selectItems.set(item.id, item));
    }

    /**
     * 更新价格
     * @param {*} priceTypes 
     */
    updataPriceTypes(priceTypes) {
        if (!priceTypes.length) priceTypes = [priceTypes];
        priceTypes.forEach(price => {
            const priceType = new PriceType();
            price.codeValue = price.localPriceTypeId;
            price.codeName = price.localPriceTypeName;
            common.update(priceType, price);
            this.priceTypes.set(priceType.localPriceTypeId, priceType);
        });
    }
}
/**
 * 价格类型
 * typeId=402
 * spb.sc_dict
 */
class PriceType {
    constructor(params) {
        /**  加价比 */
        this.addition = 0;
        /**  百分比，100 % 对应值1 */
        this.percentage = 1;
        /**  当前类型id */
        this.localPriceTypeId = '';
        /**  当前类型名 */
        this.localPriceTypeName = '';
        /**  外部价格类型id */
        this.outPriceTypeId = 1;
        /**  外部价格类型名 */
        this.outPriceTypeName = '默认价';
        /**  默认标识 */
        this.defaultFlag = '';
        /**  字典值 */
        this.codeValue = '';
        /**  中文名称 */
        this.codeName = '';
        /**  字典定义id */
        this.typeId = 402;
        /**  状态 */
        this.flag = 0;
    }

    /**
     * 获取价格list（app）
     */
    async getPriceTypeListInApp(params) {
        const res = await ss.spdresb.getPriceTypeListInApp(params);
        console.log(`\n 价格list=${JSON.stringify(res)}`);
        return res.result.data.priceTypes;
    }

    /**
     * 获取价格字典值(新增)
     * @description 返回启用的价格
     */
    async getPriceDict(params) {
        const res = await ss.spdresb.getPriceDict(params);
        console.log(`\n 启用价格list=${JSON.stringify(res)}`);
        return res.result.data.prices;
    }

    /**
     * 价格列表校验:获取价格list（app）
     */
    async priceTypeListAssert() {
        const priceTypes = await this.getPriceTypeListInApp();
        const priceType = priceTypes.find(obj => obj.localPriceTypeId == this.localPriceTypeId);
        console.log(`\n 价格类型实际值=${JSON.stringify(priceType)}`);
        common.isApproximatelyEqualAssert(this, priceType, [], `价格类型localPriceTypeId:${this.localPriceTypeId}`);
    }

    /**
     * 价格列表校验:getPriceDict
     * @param {*} params 
     * @param {*} params.state true:false:作废
     */
    async priceListAssert(params = { state: true }) {
        const priceList = await this.getPriceDict();
        const priceType = priceList.find(obj => obj.codeValue == this.localPriceTypeId);
        console.log(`\n 价格类型实际值=${JSON.stringify(priceType)}`);
        if (params.state) {
            common.isApproximatelyEqualAssert(this, priceType, [], `价格类型localPriceTypeId:${this.localPriceTypeId}`);
        } else {
            expect(priceType, `getPriceDict找到了停用的价格${JSON.stringify(priceType)}`).to.be.undefined;
        }
    }

    /**
     * 修改价格
     * @param {object} params jsonParam 
     * @param {string} [params.codeName] 名称
     * @param {string} [params.props] typeId为402时传递， key: addition 值: 加价值 key: percentage，100 % 对应值1, 值：百分比 key: outPriceTypeId 值：参考的价格类型 清空传‘{ } ’
     * @param {object} selectItems  
     */
    async updatePriceTypeDict(params, selectItems) {
        const res = await ss.config.saveDict({ typeId: 402, codeValue: this.localPriceTypeId, ...params });
        console.log(`\n 修改价格=${JSON.stringify(res)}`);
        params.codeName && (params.localPriceTypeName = params.codeName);
        common.update(this, params);
        if (JSON.stringify(params.props) == '{}') common.update(this, { addition: '', percentage: '', outPriceTypeId: '' });
        if (params.props.outPriceTypeId != undefined) params.props.outPriceTypeName = selectItems.get(params.props.outPriceTypeId).name;
        common.update(this, params.props);
    }

    /**
     * 不启用价格
     * @param {object} params jsonParam 
     */
    async stopPriceTypeDict(params) {
        let props = {};
        this.localPriceTypeId && (props = { addition: this.addition, percentage: this.percentage, outPriceTypeId: this.outPriceTypeId });
        const res = await ss.config.saveDict({ typeId: 402, codeValue: this.localPriceTypeId, flag: 0, props: props });
        console.log(`\n 不启用价格=${JSON.stringify(res)}`);
        this.flag = 0;
    }

    /**
     * 启用价格
     * @param {object} params jsonParam 
     */
    async startPriceTypeDict(params) {
        let props = {};
        this.localPriceTypeId && (props = { addition: this.addition, percentage: this.percentage, outPriceTypeId: this.outPriceTypeId });
        const res = await ss.config.saveDict({ typeId: 402, codeValue: this.localPriceTypeId, flag: 1, props: props });
        console.log(`\n 启用价格=${JSON.stringify(res)}`);
        this.flag = 1;
    }

}


const priceTypeManage = module.exports = {};

/**
 * 初始化
 * @param {*} params 
 */
priceTypeManage.setupPriceTypes = function (params) {
    return new PriceTypes(params);
};