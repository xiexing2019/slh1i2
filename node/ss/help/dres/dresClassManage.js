const common = require('../../../lib/common');
const spCaps = require('../../../reqHandler/sp/spCaps');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const pinyin = require("pinyin4js");


/**
 * 商品分类
 * @description
 * 1. 默认会有一个‘其他’分类，并不会真实关联(没有priClassId),在商品没有分类的情况下,会变成'其他'分类，
 */
class DresClass {
    constructor(params = {}) {
        /**  主键id */
        this.id = '';
        /**  类别名称 */
        this.name = '';
        /**  名称拼音 */
        this.namePy = '';
        /**  父类别 */
        this.parentId = '';
        /**  排序 */
        this.showOrder = 9999;
        /**  层级 */
        this.level = 0;
        /**  访问编码 */
        this.accode = '';
        /**  状态 */
        this.flag = 1;
        /**  有效款数 */
        this.validNum = 0;
        /**  备注 */
        this.rem = '';
        /**  商陆花id */
        this.slhId = '';
        /**  单元id */
        this.unitId = '';
        /**  商陆花类别id */
        this.fullName = '';
        // /**  saasId */
        // this.saasId = '';
        /**  分类图片docId */
        this.classDocId = '';
        /**  分类下商品数 计算比较费时,卖家查询时会返回数值,买家不处理返回0 */
        this.classSpuNum = '';
        /**其直属分类数 */
        this.sonClassNums = 0;
        /**其直属分类商品数 */
        this.sonSpuNums = 0;
        /**  隐藏标识 0 不隐藏 1隐藏 */
        this.hideFlag = 0;

        /** 类别绑定商品 */
        this.spuIds = new Set();
        /** 子类别 */
        this.subDresClass = new Set();
    }

    /**
     * 卖家分类列表树（app）
     * @param {string} [params.nameLike] 名字模糊，没有默认查询全部
     * @param {string} [params.parentId] 上次类别id,没有默认0，就是第一级
     * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
     * @param {string} [params.hideFlag] 不传看全部，0 看非隐藏 1 看隐藏的
     */
    async findDresSpuClassTree(params) {
        const res = await ss.spdresb.findDresSpuClassTree(params);
        console.log(`\n 卖家分类列表树=${JSON.stringify(res)}`);
        return res.result.data.rows;
    }

    /**
     * 卖家分类列表树校验
     * @param {object} params
     * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
     */
    async dresSpuClassTreeAssert(params = {}) {
        const res = await this.findDresSpuClassTree({ parentId: this.parentId, nameLike: this.name, ...params });
        const actualdresClass = res.find(obj => obj.id == this.id);
        console.log(`\n 分类实际值=${JSON.stringify(actualdresClass)}`);
        this.setClassSpuNum();
        this.setClassNum();
        common.isApproximatelyEqualAssert(this, actualdresClass, [], `商品分类id:${this.id}`);
    }

    /**
     * 买家分类列表树(小程序)
     * @param {object} params jsonParam
     * @param {string} params.sellerId 卖家id
     * @param {string} params.sellerUnitId 卖家单元unitId
     * @param {string} [params.nameLike] 名字模糊，没有默认查询全部
     * @param {string} [params.parentId] 上次类别id,没有默认0，就是第一级
     * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
     */
    async findDresSpuClassTreeByBuyer(params) {
        const res = await ss.spdresb.findDresSpuClassTreeByBuyer({ sellerId: this.sellerId, sellerUnitId: this.unitId, ...params });
        console.log(`\n 买家分类列表树=${JSON.stringify(res)}`);
        return res.result.data.rows;
    }

    /**
     * 买家分类列表树校验
     * @param {object} params
     * @param {string} [params.flag] 是否有效 0 无效 1 有效，没有默认有效
     */
    async findDresSpuClassTreeByBuyerAssert(params = {}) {
        const rows = await this.findDresSpuClassTreeByBuyer({ parentId: this.parentId, nameLike: this.name, sellerId: this.sellerId, sellerUnitId: this.unitId, ...params });
        const actualdresClass = rows.find(obj => obj.id == this.id);
        console.log(`\n 分类实际值=${JSON.stringify(actualdresClass)}`);
        // 不返回隐藏的分类， 类目下商品数量为0时买家自动隐藏
        if (this.hideFlag == 1 || this.classSpuNum == 0) {
            expect(actualdresClass, `买家分类列表树 查询到隐藏的分类 id=${this.id}，hideFlag=${this.hideFlag},类目下商品数量=${this.classSpuNum}\n${JSON.stringify(actualdresClass)}`).to.be.undefined;
        } else {
            // 买家classSpuNum没有意义,直接返回数据库的值为0
            common.isApproximatelyEqualAssert(this, actualdresClass, ['classSpuNum'], `商品分类id:${this.id}`);
        }
    }

    /**
     * 新增类目（app）
     * @param {object} params
     * @param {string} params.name 分类名字
     * @param {string} [params.classDocId] 分类图片
     * @param {string} params.parentId 上次类别id，一级类别传0，二级传上级的(共3级)
     * @param {string} params.level 当前层级，一级类别传1，二级传2
     * @param {string} [params.spuIds] 需要关联的spuIds，多个以逗号隔开，没有就不传
     * @param {string} [params.showOrder] 排序号，预留，新增的分类，默认9999
     * @param {string} opt.parentDresClass 分类名字
     */
    async saveFullClass(params, opt = {}) {
        const res = await ss.spdresb.saveFullClass(params);
        console.log(`\n 新增类目=${JSON.stringify(res)}`);

        this.id = res.result.data.val;
        common.update(this, params);
        this.setOwnerInfo();
        this.setNamePy();
        let parentFullName = '', parentAccode = '.';
        if (opt.parentDresClass) {
            parentFullName = `${opt.parentDresClass.fullName}.`;
            parentAccode = opt.parentDresClass.accode;
        }
        this.fullName = `${parentFullName}${this.name}`;
        this.accode = `${parentAccode}${this.id}.`;
    }

    /**
     * 编辑类目（app）
     * desc
     * @param {string} params.id 分类id
     * @param {string} params.name 分类名字
     * @param {string} [params.classDocId] 分类图片，不传不修改
     * @param {string} [params.showOrder] 排序号，预留，不传不修改
     */
    async updateFullClass(params, opt = {}) {
        const res = await ss.spdresb.updateFullClass(params);
        console.log(`\n 编辑类目=${JSON.stringify(res)}`);
        common.update(this, params);
        if (params.hasOwnProperty('spuIds')) {
            this.spuIds = new Set(params.spuIds ? params.spuIds.split(',') : []);
            this.validNum = this.spuIds.size;
            this.sonSpuNums = this.spuIds.size;
            // this.setSonClassSpuNum();
        };
        console.log(this.spuIds);
        this.setNamePy();
        this.setClassSpuNum();
        this.setClassNum();

        // fullName不变，郑潮:目前fullName就是装饰。。。
    }

    /**
     * 判断类目相关状态 app
     * @description 返回值:0不可以（type为3跳商品列表） 1可以（type为3跳分类列表） 2预留
     * @param {Object} params
     * @param {string} params.typeId 1 代表能否新增， 2 代表能否删除(有子类不能删除，需先删除子类) 3 代表下属跳转 4 能否关联货品
     */
    async judgeFullClass(params) {
        const res = await ss.spdresb.judgeFullClass({ id: this.id, ...params });
        console.log(`\n 判断类目相关状态=${JSON.stringify(res)}`);
        return res.result.data;
    }

    /**
     * 判断类目相关状态 断言
     * @param {Object} params
     * @param {string} params.typeId 1 代表能否新增， 2 代表能否删除(有子类不能删除，需先删除子类) 3 代表下属跳转 4 能否关联货品
     */
    async judgeFullClassAssert(params) {
        const data = await this.judgeFullClass(params);

        const typeId = params.typeId;
        let expFlag = 0;
        switch (params.typeId) {
            case 1:
                // 4级待确认
                // 能否新增(有商品不能新增)
                expFlag = this.spuIds.size == 0 ? 1 : 0;
                break;
            case 2:
                // 能否删除(有子类不能删除，需先删除子类)
                expFlag = this.subDresClass.size == 0 ? 1 : 0;
                break;
            case 3:
                // 下属跳转(有商品不能新增)
                expFlag = this.subDresClass.size == 0 ? 0 : 1;
                break;
            case 4:
                // 能否关联货品(有子节点不能关联)
                expFlag = this.subDresClass.size == 0 ? 1 : 0;
                break;
            default:
                throw new Error(`判断类目相关状态错误: 未知状态'${params.typeId}',请检查请求参数`);
                break;
        }
        common.isApproximatelyEqualAssert({ val: expFlag }, data);
    }

    /**
     * 删除作废类目 app接口
     * @param {Object} params
     */
    async deleteFullClass(params = {}) {
        const res = await ss.spdresb.deleteFullClass({ id: this.id, ...params });
        console.log(`\n 删除作废类目=${JSON.stringify(res)}`);
        this.flag = 0;
        return res;
    }

    /**
     * 编辑类目顺序 app
     * @param {Object} params
     * @param {Object[]} params.showOrders 分类名字(list 结构)
     * @param {string} params.showOrders[].id 分类id
     * @param {string} params.showOrders[].showOrder 分类顺序值
     */
    async updateClassShowOrder(params = {}) {
        const res = await ss.spdresb.updateClassShowOrder(params);
        console.log(`\n 编辑类目顺序=${JSON.stringify(res)}`);
        common.update(this, params.showOrders.find(obj => obj.id == this.id));
    }

    /**
     * 新增分类到多个商品中（app）
     * @param {Object} params
     * @param {string} params.spuIds
     */
    async addSpuForClass(params) {
        const { spuIds } = params;
        await ss.spdresb.addSpuForClass({ classId: this.id, dresSpuIds: spuIds.join(',') });
        spuIds.forEach(spuId => this.spuIds.add(spuId));
        this.validNum = params.spuIds.length;
    }

    /**
     * 新增多个商品到分类中（app）
     * @param {Object} params
     * @param {string} params.spuIds
     * @param {string} params.typeId  0 或者不传 增量增加，1 增量减少
     */
    async addSpuToClass(params) {
        const spuIds = params.spuIds.join(',');
        await ss.spdresb.addSpuToClass({ classId: this.id, spuIds: spuIds, typeId: params.typeId });
        params.spuIds.forEach(spuId => this.spuIds.add(spuId));
        this.validNum = params.spuIds.length;
    }

    /**
     * 新增多个分类到商品中（app）
     * @param {Object} params
     * @param {string} params.spuId
     */
    async addClassForSpu(params) {
        const { spuId } = params;
        await ss.spdresb.addClassForSpu({ classIds: this.id, spuId: spuId, ...params });
        this.spuIds.add(spuId);
        this.validNum = params.spuId.length;
    }

    /**
     * 对买家隐藏分类类目
     * @description 隐藏父级,不会影响子类目hideFlag
     * @param {object} params
     * @param {string} params.hideFlag 隐藏状态位 0 不隐藏 1隐藏
     */
    async hideDresClass(params) {
        const { hideFlag } = params;
        await ss.spdresb.hideDresClass({ classId: this.id, hideFlag });

        this.hideFlag = hideFlag;
        // this.subDresClass.forEach(subClass => subClass.hideFlag = hideFlag);
    }

    /**
     * 设置类别所属者信息
     * @param {object} [ownerInfo=LOGINDATA] 所属者信息(默认当前角色)
     */
    setOwnerInfo(ownerInfo = LOGINDATA) {
        this.unitId = ownerInfo.unitId;
        this.sellerId = ownerInfo.tenantId;
        return this;
    }

    /**
     * 设置名称拼音
     */
    setNamePy() {
        this.namePy = pinyin.convertToPinyinString(this.name, '', pinyin.WITHOUT_TONE);
    }

    /**
     * 设置直属分类数
     */
    setClassNum() {
        this.sonClassNums = this.subDresClass.size;
        console.log('打印分类商品数', this.sonClassNums);

        return this;
    };

    /**设置直属商品数
     * @param {*} params.num 商品数（如果是1级，就传3级的数量，如果是3级就传自己的数量）
     */
    async setSonClassSpuNum(num) {
        this.sonSpuNums = common.add(this.spuIds.size, num);
        return this;
    };

    /**
     * 设置分类下商品数
     * @description 父类别为子类别的总商品数
     */
    setClassSpuNum() {
        console.log('\n\nthis.subDresClass.values()');
        console.log(this.subDresClass.values());
        console.log(`this.subDresClass.size=${this.subDresClass.size}`);

        this.classSpuNum = this.subDresClass.size == 0 ? this.spuIds.size :
            [...this.subDresClass].reduce((a, b) => common.add(a.spuIds.size, b.spuIds.size), { spuIds: new Set() });
        console.log('打印子级商品数', this.classSpuNum);

        return this;
    }
}

const dresClassManage = module.exports = {};

/**
 * 初始化
 * @param {*} params
 */
dresClassManage.setupDresClass = function (params) {
    return new DresClass(params);
};

/**
 * 买家分类列表树 排序
 * @description
 * 1. 在查看2级的时候，会把没有三级的二级放上面，有三级的二级放下面
 * 2. http://119.3.46.172:6082/zentao/testtask-view-1660.html
 */
dresClassManage.sortSpuClassTreeByBuyerAssert = function (classTree) {
    const classIds = [], list1 = [], list2 = [];
    classTree.forEach(ele => {
        classIds.push(ele.id);
        if (ele.subItems.length === 0) {
            list1.push({ id: ele.id, showOrder: ele.showOrder });
        } else {
            list2.push({ id: ele.id, showOrder: ele.showOrder });
        }
    });
    const classIdsExp = _.orderBy(list1, ['showOrder'], ['asc'])
        .concat(_.orderBy(list2, ['showOrder'], ['asc']))
        .map(ele => ele.id);
    expect(classIds, `买家分类列表树 二级类目排序错误\n${JSON.stringify(classTree)}`).to.eql(classIdsExp);
};