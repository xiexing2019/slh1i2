const spMdm = require('../../reqHandler/sp/biz_server/spmdm');
const common = require('../../lib/common');

let messageManage = module.exports = {};


/**
 * 消息中心验证(断言)
 * @description 暂时去掉消息中心的text字段验证(提示信息会变动) lxx 18-09-04
 * @param {object} params
 */
messageManage.messageCenter = async function ({ billNo, tagOneIn, title }) {
    return;
    //获取消息列表-未读
    let messageList = await spMdm.pullMessagesList({ unread: 1, tagOneIn, pageNo: 1, pageSize: 20 });
    expect(messageList.result.data.dataList.length, `消息列表为空,${JSON.stringify(messageList)}`).to.not.equal(0);
    // console.log(`messageList=${JSON.stringify(messageList)}`);
    let msgInfo = messageList.result.data.dataList.find(message => message.body.billNo = billNo);
    // console.log(`msgInfo=${JSON.stringify(msgInfo)}`);
    expect(msgInfo, `找不到最新的消息`).to.not.be.undefined;
    expect(msgInfo.body, `消息列表中未读的消息没有单据:${JSON.stringify(billNo)}的消息`).to.includes({ title });//msgJson[tagOneIn] text,
    //获取未读消息数
    let unReadCount = await spMdm.getPullUnreadCount({ tagOneIn });
    // console.log(`unReadCount.result.data.val=${JSON.stringify(unReadCount.result.data.val)}`);
    expect(unReadCount.result.data.val, `类型:${JSON.stringify(tagOneIn)}未读消息数和消息列表中该类型的未读的消息总数不一致`).to.equal(messageList.result.data.count);
    //更新消息为已读
    await spMdm.updateReadStatus({ msgIds: msgInfo.id });
    let unReadCount2 = await spMdm.getPullUnreadCount({ tagOneIn });
    expect(common.sub(unReadCount.result.data.val, 1), `消息已读后，未读消息数没有减1`).to.equal(unReadCount2.result.data.val);
    //获取消息列表
    messageList = await spMdm.pullMessagesList({ tagOneIn, pageNo: 1, pageSize: 20 });
    msgInfo = messageList.result.data.dataList.find((message) => message.body.billNo == billNo);
    // console.log(msgInfo);
    //unread 未读状态 0表示已读，1表示未读
    expect(msgInfo, `消息不存在`).to.not.be.undefined;
    expect(msgInfo.unread, `消息列表中单据:${JSON.stringify(billNo)}已读,但是未读状态不是0`).to.equal(0);
};

/**
 * 消息中心验证(断言)
 * @description 暂时去掉消息中心的text字段验证(提示信息会变动) lxx 18-09-04
 * @param {object} params
 */
messageManage.messageSystem = async function ({ msgId, tagOneIn, title }) {
    return;
    //获取消息列表-未读
    let messageList = await spMdm.pullMessagesList({ unread: 1, tagOneIn, pageNo: 1, pageSize: 20 });
    expect(messageList.result.data.dataList.length, `消息列表为空,${JSON.stringify(messageList)}`).to.not.equal(0);
    // console.log(`messageList=${JSON.stringify(messageList)}`);
    let msgInfo = messageList.result.data.dataList[0];
    // console.log(`msgInfo=${JSON.stringify(msgInfo)}`);
    expect(msgInfo, `找不到最新的消息`).to.not.be.undefined;
    expect(msgInfo.id, `消息列表中未读的消息的第一条居然不为新消息，而是:${msgId}`).to.not.equal(msgId);
    expect(msgInfo.body, `消息列表中未读的消息没有单据:${title}的消息`).to.includes({ title });//msgJson[tagOneIn] text,
    //获取未读消息数
    let unReadCount = await spMdm.getPullUnreadCount({ tagOneIn });
    // console.log(`unReadCount.result.data.val=${JSON.stringify(unReadCount.result.data.val)}`);
    expect(unReadCount.result.data.val, `类型:${JSON.stringify(tagOneIn)}未读消息数和消息列表中该类型的未读的消息总数不一致`).to.equal(messageList.result.data.count);
    //更新消息为已读
    await spMdm.updateReadStatus({ msgIds: msgInfo.id });
    let unReadCount2 = await spMdm.getPullUnreadCount({ tagOneIn });
    expect(common.sub(unReadCount.result.data.val, 1), `消息已读后，未读消息数没有减1`).to.equal(unReadCount2.result.data.val);
    //获取消息列表
    messageList = await spMdm.pullMessagesList({ tagOneIn, pageNo: 1, pageSize: 20 });
    msgInfo = messageList.result.data.dataList[0];
    // console.log(msgInfo);
    //unread 未读状态 0表示已读，1表示未读
    expect(msgInfo, `消息列表第一条不存在`).to.not.be.undefined;
    expect(msgInfo.unread, `消息列表中单据:${msgInfo.id}已读,但是未读状态不是0`).to.equal(0);
};