const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');
const fs = require('fs');
const path = require('path');
const LogAnalysis = require('./logAnalysis');
const ssReq = require('../../help/ssReq');
const basicJson = require('../../help/basicJson');
const billManage = require('../../help/billManage');
const dresManage = require('../../help/dresManage');

class DataCenter {


    /**
     *
     * @param {object} params
     * @param {string} params.startDate  开始日期
     * @param {string} params.endDate  结束日期
     */
    async salesTrend(params) {
        const con = await ss.creatSqlPool({})
    }

    /**
     * 场景推送统计
     * @param {object} params
     * @param {string} params.startDate  开始日期
     * @param {string} params.endDate  结束日期
     */
    async pushStatistics(params = {}) {
        const con = await ss.creatSqlPool({ dbName: 'spgMysql' });
        const queryRes = await con.query(`
        SELECT
            pro_date,
            seller_tenant_id,
            scene_id,
            count(*) as cnt,
            sum(case when is_success = 1 then 1 else 0 end) as cnt_success,
            sum(case when is_success = 0 then 1 else 0 end) as cnt_fail,
            sum(case when is_fail_sub = 1 then 1 else 0 end) as cnt_fail_sub
        FROM
            sp_mp_send_detail
        WHERE
            pro_date = ${params.startDate}
        group by
            scene_id
        ORDER BY
            cnt_success DESC`);
        await con.end();
        return queryRes;
    }


}