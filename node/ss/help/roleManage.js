const common = require('../../lib/common');
const spCaps = require('../../reqHandler/sp/spCaps');
const sp = require('../../reqHandler/sp');
const ss = require('../../reqHandler/ss');


class Role {
    constructor(roleInfo = {}) {
        //  ID
        this.id = '';
        // //  单元id
        // this.unitId = '';
        //  角色编码
        this.code = '';
        //  角色名
        this.name = '';
        // //  级别
        // this.level = 0;
        // //  状态
        // this.flag = 0;
        //  备注
        this.rem = '';
        //  功能
        this.funcIds = [];
        // //  创建人
        // this.createdBy = '';
        // //  创建时间
        // this.createdDate = '';
        // //  修改人
        // this.updatedBy = '';
        // //  修改时间
        // this.updatedDate = '';
        common.update(this, roleInfo);
    }

    /**
     * 保存角色权限
     * @param {object} params 
     * @param {string} params.id 角色id，更新时必传
     * @param {string} params.name 角色名称，添加时必传
     * @param {string} params.rem 备注
     * @param {object} params.funcIds 为空时传[]
     */
    async newOrEditRole(params) {
        const role = await ss.spauth.newRole(params).then(res => res.result.data);
        this.id = role.id;
        this.name = role.name;
        this.rem = role.rem;
        // this.funcIds = params.funcIds;
        await this.getAuthListByRoleId();
    }

    /**
     * 查询角色菜单权限
     * @param {object} params 
     */
    async getAuthListByRoleId() {
        const authList = await ss.spauth.getAuthListByRoleId({ roleId: this.id }).then(res => res.result.data.rows);
        let funcIds = [];
        authList.forEach(auth => {
            funcIds.push(auth.funcId);
        });
        this.funcIds = funcIds;
    }

    /**
     * 查询角色列表
     * @param {object} params 
     */
    async getRoleListAtShop() {
        return ss.spauth.getRoleListAtShop({ flag: 1 }).then(res => res.result.data.rows);
    }

    /**
     * 删除角色
     * @param {object} params 
     */
    async deleteRole(params) {
        return ss.spauth.deleteRole({ roleId: this.id, ...params });
    }

    /**
     * 树状获取所有权限
     * @param {object} params 
     */
    async getlistByTree() {
        return ss.spauth.getlistByTree().then(res => res.result.data.rows);
    }

    /**
     * 获取角色详情
     */
    getRole() {
        return {
            id: this.id,
            code: this.code,
            name: this.name,
            rem: this.rem,
            funcIds: this.funcIds,
        }
    }
};

/**
 * 获取角色详情
 * @param {object} id 角色id
 */
const getRoleInfo = async function (id) {
    const roleInfo = await ss.spauth.getRoleListAtShop().then(res => res.result.data.rows.find(obj => obj.id == id))
    const authList = await ss.spauth.getAuthListByRoleId({ roleId: id }).then(res => res.result.data.rows);
    roleInfo.funcIds = [];
    authList.forEach(auth => {
        roleInfo.funcIds.push(auth.funcId);
    });
    const role = new Role(roleInfo);
    return role;
};
const roleManage = module.exports = {
    Role, getRoleInfo
};

roleManage.setupRole = function () {
    return new Role();
};


