
const ssAccount = require('../../data/ssAccount');
const myShop = require('./myShop');


let getShop = module.exports = {};
const shopData = new Map();

getShop.getSellerShop = ({ seller = 'seller6' } = {}) => {
    getShopData({ code: ssAccount[seller].mobile, shopName: ssAccount[seller].shopName });
    return shopData.get(ssAccount[seller].mobile);
}

/**
 * 获取卖家缓存信息
 */
function getShopData({ code = ssAccount.seller6.mobile, shopName = ssAccount.seller6.shopName } = {}) {
    if (!shopData.get(code)) {
        shopData.set(code, new myShop());
        return;
    }
    const sellerInfo = shopData.get(code).seller.get(code);
    if (sellerInfo && sellerInfo.shopMap.has(shopName)) return;
    return shopData;
};

