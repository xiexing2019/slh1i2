const common = require('../../../../lib/common');
const format = require('../../../../data/format');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const moment = require('moment');
const ShipRule = require('../dto/shipRule');
const basicJson = require('../../basicJson');


class ShipFee {
    constructor(params) {
        // this.shipFeeWayId = 0; //1:包邮、2:自定义、3:到付
        // this.deliverProvCode = 0;
        // this.deliverDetailAddr = '';
        // this.feeType = 0;
        // this.freeType = 0;
        this.defaultTemplateId = ''; //默认模板id
        this.defaultTemplate = new Map(); //包邮，到付。
        this.templateMap = new Map(); //自定义
        // this.shipRule = { feeRules: new Map(), freeRules: new Map() };      //运费
    }

    /**
     * 修改默认模板标识
     * @param {number} templateId 
     */
    async updateFeeDefaultTemplate(templateId, type = 2) {
        const res = await ss.spconfg.updateFeeDefaultTemplate({ templateId: templateId });
        console.log(`\n 修改默认模板标识=${JSON.stringify(res)}`);
        if (this.defaultTemplateId) {
            if (this.defaultTemplateId == this.defaultTemplate.get(-1).pId) {
                this.defaultTemplate.get(-1).defaultTemplateFlag = 0;
            } else if (this.defaultTemplateId == this.defaultTemplate.get(-3).pId) {
                this.defaultTemplate.get(-3).defaultTemplateFlag = 0;
            } else {
                this.templateMap.get(this.defaultTemplateId).defaultTemplateFlag = 0
            }
        }

        if (templateId == this.defaultTemplate.get(-1).pId) {
            this.defaultTemplate.get(-1).defaultTemplateFlag = 1;
        } else if (templateId == this.defaultTemplate.get(-3).pId) {
            this.defaultTemplate.get(-3).defaultTemplateFlag = 1;
        } else {
            this.templateMap.get(templateId).defaultTemplateFlag = 1;
        }
        this.defaultTemplateId = templateId;
    }


    /**
     * 删除运费模板
     * @param {number} templateId 
     */
    async deleteTemplate(templateId) {
        const res = await ss.spconfg.deleteTemplate({ templateId: templateId });
        console.log(`\n 删除运费模板=${JSON.stringify(res)}`);
        this.updateTemplateCount(this.defaultTemplate.get(-1).pId, this.templateMap.get(templateId).count);
        this.templateMap.delete(templateId);
        if (templateId == this.defaultTemplateId) this.defaultTemplateId = this.defaultTemplate.get(-1).pId;
    }

    /**
     * 保存运费模板
     * @param {*} templateFeeRuleJson 
     * @param {*} oldRules 需要删除的规则
     */
    async saveTemplateFeeRule(templateFeeRuleJson, oldRules = { spFeeRules: [], spShipFeeRules: [] }) {
        const newTemplate = await ss.spconfg.saveTemplateFeeRule(templateFeeRuleJson);
        console.log(`\n保存运费模板=${JSON.stringify(newTemplate)}`);
        templateFeeRuleJson.id = newTemplate.result.data.val;
        if (templateFeeRuleJson.state == 0) {
            console.log(`\n关闭了自定义包邮`);
            templateFeeRuleJson.spShipFeeRules = [];
            if (this.templateMap.get(templateFeeRuleJson.id)) this.templateMap.get(templateFeeRuleJson.id).spShipFeeRules = new Map();
        }
        this.deleteRule(templateFeeRuleJson.id, oldRules);
        const ruleTypes = ['spFeeRules', 'spShipFeeRules'];
        ruleTypes.forEach(type => {
            if (templateFeeRuleJson[type]) {
                templateFeeRuleJson[type].forEach((rule, index) => {
                    if (rule.deleteFlag == 1) templateFeeRuleJson[type].splice(index, 1);
                });
            }
        });
        this.updateTemplate(templateFeeRuleJson);
        return newTemplate.result.data.val;
    }

    /**
     * 运费模板详情
     * @param {number} templateId 
     */
    async templateFeeRuleDetails(templateId) {
        const templateFeeRuleDetails = await ss.spconfg.newTemplateFeeRuleDetails({ templateId: templateId }).then(res => res.result.data);
        console.log(`\n 运费模板详情=${JSON.stringify(templateFeeRuleDetails)}`);
        this.updateRuleId(templateFeeRuleDetails);
        return templateFeeRuleDetails;
    }

    async getTemplateList() {
        const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data);
        console.log(`\n运费规则列表=${JSON.stringify(templateFeeRules)}`);
        return templateFeeRules;
    }

    /**
     * 运费规则列表
     */
    async getTemplateFeeRules(templateId) {
        const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
        console.log(`\n运费规则列表=${JSON.stringify(templateFeeRules)}`);
        expect(templateFeeRules.find(obj => obj.id == templateId), `运费模板列表不存在模板${templateId}`).not.to.be.undefined;
        this.updateRuleId(templateFeeRules.find(obj => obj.id == templateId));
        return templateFeeRules;
    }

    /**
     * 批量更新商品运费模板
     * @param {Object} params
     * @param {Object[]} params.spuIds 商品id 多个逗号分隔
     * @param {string} params.templateId 模板id
     */
    async batchUpdateFreeTemplate(params) {
        let templateIds = [];
        for (const spuId of params.spuIds) {
            const dresFull = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
            templateIds.push(dresFull.spu.templateId);
        }
        const res = await ss.spdresb.batchUpdateFreeTemplate({ spuIds: params.spuIds.toString(), templateId: params.templateId });
        console.log(`\n一键更新商品运费模板=${JSON.stringify(res)}`);
        this.updateTemplateCount(params.templateId, params.spuIds.length);
        for (const templateId of templateIds) this.updateTemplateCount(templateId, -1);
    }

    /**
     * 一键更新商品运费模板
     * @param {number} templateId 
     */
    async quantityUpdateFreeTemplate(templateId) {
        const res = await ss.spdresb.quantityUpdateFreeTemplate({ templateId: templateId });
        console.log(`\n一键更新商品运费模板=${JSON.stringify(res)}`);
        let dresTotal = 0;
        for (const flag of [1, 2, -4]) {
            dresTotal += await sp.spdresb.findSellerSpuList({ flag: flag }).then(res => res.result.data.total);
        }
        console.log(`所有有效商品总数：`, dresTotal);

        if (templateId == this.defaultTemplate.get(-1).pId) {
            this.updateTemplateCount(templateId, dresTotal - this.defaultTemplate.get(-1).count);
            this.updateTemplateCount(this.defaultTemplate.get(-3).pId, -this.defaultTemplate.get(-3).count);
            // shopInfo.shipFee.updateTemplate({ id: -1, count: dresTotal });
        } else if (templateId == this.defaultTemplate.get(-3).pId) {
            this.updateTemplateCount(templateId, dresTotal - this.defaultTemplate.get(-3).count);
            this.updateTemplateCount(this.defaultTemplate.get(-1).pId, -this.defaultTemplate.get(-1).count);
        } else {
            this.updateTemplateCount(templateId, dresTotal - this.templateMap.get(templateId).count);
        }
        for (const template of Object.values(this.templateMap)) {
            console.log(template);
            if (template.id == templateId) continue;
            this.updateTemplateCount(template.id, -this.templateMap.get(template.id).count);
        }
    }

    /**
     * 删除已存在的运费规则
     * @param {*} oldRules 
     */
    deleteRule(shipId, oldRules = { spFeeRules: [], spShipFeeRules: [] }) {
        // console.log(oldRules);
        if (oldRules.spFeeRules) {
            oldRules.spFeeRules.forEach(rule => {
                rule.provIds.split(',').forEach(provId => {
                    this.templateMap.get(shipId).spFeeRules.delete(provId);
                });
            });
        }
        if (oldRules.spShipFeeRules) {
            oldRules.spShipFeeRules.forEach(rule => {
                rule.shipProvIds.split(',').forEach(provId => {
                    this.templateMap.get(shipId).spShipFeeRules.delete(provId);
                });
            });
        }
    }

    /**
     * 获取已存在的规则
     * @param {boolean} isFee 默认计费
     */
    getRandomRule(templateId, isFee = true) {
        const ruleType = isFee ? 'spFeeRules' : 'spShipFeeRules';
        const rules = [...this.templateMap.get(templateId)[ruleType].values()];
        return rules[common.getRandomNum(0, rules.length - 1)];
    }

    /**
     * 获取已存在的模板
     */
    getRandomTemplate(exceptId) {
        const templates = [...this.templateMap.values()];
        let template = templates.find(template => template.id != exceptId);
        if (!template) throw new Error(`当前店铺不存在其他(${exceptId}以外)自定义运费模板。\n${JSON.stringify(templates)}`);
        return template;
    }

    /**
     * 修改模板的使用商品数量
     * @param {*} templateId 
     * @param {*} modifyCount 修改数，减少传-
     */
    updateTemplateCount(templateId, modifyCount) {
        console.log('修改模板:', templateId, ',修改数:', modifyCount);

        let template;
        if (templateId == this.defaultTemplate.get(-1).pId) {
            template = this.defaultTemplate.get(-1);
        } else if (templateId == this.defaultTemplate.get(-3).pId) {
            template = this.defaultTemplate.get(-3);
        } else {
            template = this.templateMap.get(templateId);
        }
        common.update(template, { count: template.count + modifyCount });
        // this.templateMap.set(templateId, template);
    }

    /**
     * 更新规则id
     * @param {*} templateDetail 
     */
    updateRuleId(templateDetail) {
        if (!this.templateMap.has(templateDetail.id)) throw new Error(`不存在该模板。\n模板:${JSON.stringify(templateDetail)}`);
        const template = this.templateMap.get(templateDetail.id);
        // console.log(template);
        if (templateDetail.spFeeRules) {
            templateDetail.spFeeRules.forEach(feeRule => {
                feeRule.provIds.split(',').forEach(provId => {
                    if (!template.spFeeRules.has(provId)) throw new Error(`模板${templateDetail.id}不存在包含省份${provId}的计费规则。\n模板:${JSON.stringify(templateDetail)}`);
                    const rule = template.spFeeRules.get(provId);
                    common.update(rule, { id: feeRule.id });
                    template.spFeeRules.set(provId, rule);
                });
            });
        }
        if (templateDetail.spShipFeeRules) {
            templateDetail.spShipFeeRules.forEach(freeRule => {
                freeRule.shipProvIds.split(',').forEach(provId => {
                    if (!template.spShipFeeRules.has(provId)) throw new Error(`模板${templateDetail.id}不存在包含省份${provId}的包邮规则。\n模板:${JSON.stringify(templateDetail)}`);
                    const rule = template.spShipFeeRules.get(provId);
                    common.update(rule, { id: freeRule.id });
                    template.spShipFeeRules.set(provId, rule);
                });
            });
        }
    }
    /**
     * 更新模板
     * @param {Array} templates 
     */
    updateTemplate(templates) {
        if (!templates.length) templates = [templates];
        templates.forEach(temp => {
            if (temp.id != -1 && temp.id != -3) {
                const template = this.templateMap.has(temp.id) ? this.templateMap.get(temp.id) : new Template(temp);
                if (temp.defaultTemplateFlag) this.defaultTemplateId = temp.id;
                if (temp.temlateName) common.update(template, { temlateName: temp.temlateName });
                if (temp.defaultTemplateFlag) common.update(template, { defaultTemplateFlag: temp.defaultTemplateFlag });
                if (temp.state == undefined) temp.state = 1;
                common.update(template, { state: temp.state });
                this.templateMap.set(temp.id, template);
                if (temp.spFeeRules) {
                    temp.spFeeRules.forEach(feeRule => {
                        const provIds = feeRule.provIds.split(',');
                        provIds.forEach(provId => {
                            const rule = this.templateMap.get(temp.id).spFeeRules.has(provId) ? this.templateMap.get(temp.id).spFeeRules.get(provId) : new FeeRule();
                            // delete rule.provIds;
                            common.update(rule, feeRule);
                            this.templateMap.get(temp.id).spFeeRules.set(provId, rule);
                        });
                    });
                }
                if (temp.spShipFeeRules) {
                    temp.spShipFeeRules.forEach(freeRule => {
                        const provIds = freeRule.shipProvIds.split(',');
                        provIds.forEach(provId => {
                            const rule = this.templateMap.get(temp.id).spShipFeeRules.has(provId) ? this.templateMap.get(temp.id).spShipFeeRules.get(provId) : new FreeRule();
                            // delete rule.shipProvIds;
                            common.update(rule, freeRule);
                            this.templateMap.get(temp.id).spShipFeeRules.set(provId, rule);
                        });
                    });
                }
                // console.log(this.templateMap.get(temp.id));
            } else {
                const template = this.defaultTemplate.get(temp.id);
                if (temp.defaultTemplateFlag) this.defaultTemplateId = temp.pId;
                common.update(template, temp);
            }
        });
    }

    /**
     * 初始化默认模板
     */
    initDefaultTemplate() {
        const defaultTemps = [
            { id: -1, pId: '', temlateName: '包邮', defaultTemplateFlag: 0, count: 0, type: 50 },
            { id: -3, pId: '', temlateName: '到付', defaultTemplateFlag: 0, count: 0, type: 30 }
        ];
        defaultTemps.forEach(defaultTemp => {
            this.defaultTemplate.set(defaultTemp.id, defaultTemp);
        });
    }

    /**
     * 获取已存在的运费模板
     */
    async getExistShipRule() {
        let templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => {
            console.log(`\ntemplateFeeRules=${JSON.stringify(res)}`);
            return res.result.data.rows
        });
        if (!templateFeeRules.length) return;
        this.updateTemplate(templateFeeRules);
        if (!this.templateMap.size) return;
        for (const id of Object.keys(this.templateMap)) {
            const templateFeeRuleDetails = await ss.spconfg.newTemplateFeeRuleDetails({ templateId: id }).then(res => res.result.data);
            // console.log(`\ntemplateFeeRuleDetails=${JSON.stringify(templateFeeRuleDetails)}`);
            this.updateTemplate(templateFeeRuleDetails);
        }
    }

    /**
     * mockFeeRuleJson
     * @param {*} opts {id:,count: }
     */
    async mockFeeRuleJson(templateId, opts = {}) {
        opts = Object.assign({ count: 1 }, opts)
        const spFeeRules = [];

        // TODO 是否直接初始化 这里去除 改成同步方法
        const areaJson = await basicJson.getAreaJson();
        // console.log(areaJson);
        areaJson.proviceArr = common.arrRemoveSameEle(areaJson.proviceArr, [...this.templateMap.get(templateId).spFeeRules.keys()]);
        // console.log(areaJson);
        let proFlag = 0;
        for (let index = 0; index < opts.count; index++) {
            const proTemp = proFlag + common.getRandomNum(1, 3);
            const rule = new FeeRule(Object.assign({ provIds: areaJson.proviceArr.slice(proFlag, proTemp).join() }, opts));
            spFeeRules.push(rule);
            proFlag = proTemp;
        }
        return spFeeRules;
    }

    /**
     * mockFreeRuleJson
     * @param {*} opts {id:,count: }
     */
    async mockFreeRuleJson(templateId, opts = {}) {
        opts = Object.assign({ count: 1 }, opts)
        const spFreeRules = [];
        // console.log(`\nopts=${JSON.stringify(opts)}`);
        const areaJson = await basicJson.getAreaJson();
        // console.log(areaJson);
        areaJson.proviceArr = common.arrRemoveSameEle(areaJson.proviceArr, [...this.templateMap.get(templateId).spShipFeeRules.keys()]);
        // console.log(areaJson);
        let proFlag = 0;
        for (let index = 0; index < opts.count; index++) {
            const proTemp = proFlag + common.getRandomNum(1, 3);
            const rule = new FreeRule(Object.assign({ shipProvIds: areaJson.proviceArr.slice(proFlag, proTemp).join() }, opts));
            spFreeRules.push(rule);
            proFlag = proTemp;
        }
        return spFreeRules;
    }

    /**
     * 运费模板断言
     * @param {*} exp 期望值: mockjson
     * @param {*} templateFeeRule 
     */
    templateAssert(exp, templateFeeRule) {
        console.log(`\n实际值=${JSON.stringify(templateFeeRule)}`);
        common.isApproximatelyEqualAssert(exp, templateFeeRule, ['id', 'spFeeRules', 'spShipFeeRules']);
        exp.spFeeRules.forEach(rule => {
            common.isApproximatelyEqualAssert(rule, templateFeeRule.spFeeRules.find(obj => obj.provIds == rule.provIds), ['id']);
        });
        exp.spShipFeeRules.forEach(rule => {
            common.isApproximatelyEqualAssert(rule, templateFeeRule.spShipFeeRules.find(obj => obj.shipProvIds == rule.shipProvIds), ['id']);
        });
    }

    /**
     * 默认运费模板断言（包邮、到付）
     * @param {*} templateDetail 
     */
    defaultTemplateAssert(templateDetail) {
        console.log(`\n实际值=${JSON.stringify(templateDetail)}`);
        common.isApproximatelyEqualAssert(this.defaultTemplate.get(templateDetail.id), templateDetail);
    }

    /**
     * 运费模板断言
     * @param {*} templateDetail 模板详情
     * @param {*} type 0：模板列表单个，1：模板详情
     */
    shipTemplateAssert(templateDetail, type = 1) {
        //模板id,
        if ([-1, -3].includes(templateDetail.id)) {
            this.defaultTemplateAssert(templateDetail);
            return;
        }
        console.log(`\n实际值=${JSON.stringify(templateDetail)}`);
        common.isApproximatelyEqualAssert(this.templateMap.get(templateDetail.id), templateDetail, ['id', 'spFeeRules', 'spShipFeeRules']);
        const ruleNum = { feeRule: 0, freeRule: 0 };
        if (templateDetail.spFeeRules) {
            templateDetail.spFeeRules.forEach(feeRule => {
                feeRule.provIds.split(',').forEach(provId => {
                    const ruleExp = this.templateMap.get(templateDetail.id).spFeeRules.get(provId);
                    common.isApproximatelyEqualAssert(ruleExp, feeRule);
                    ruleNum.feeRule += 1;
                });
            });
        }
        if (templateDetail.spShipFeeRules) {
            templateDetail.spShipFeeRules.forEach(freeRule => {
                freeRule.shipProvIds.split(',').forEach(provId => {
                    const ruleExp = this.templateMap.get(templateDetail.id).spShipFeeRules.get(provId);
                    common.isApproximatelyEqualAssert(ruleExp, freeRule);
                    ruleNum.freeRule += 1;
                });
            });
        }
        if (type == 0) {
            expect(templateDetail.spFeeRules.length, '列表返回了超过1条的计费规则').to.most(1);
            if (this.templateMap.get(templateDetail.id).spShipFeeRules.size) expect(templateDetail.spShipFeeRules.length, '列表返回了超过1条的包邮规则').to.most(1);
        } else if (type == 1) {
            expect(this.templateMap.get(templateDetail.id).spFeeRules.size, `计费规则数:${ruleNum.feeRule}与预期不符`).to.equal(ruleNum.feeRule);
            expect(this.templateMap.get(templateDetail.id).spShipFeeRules.size, `包邮规则数:${ruleNum.freeRule}与预期不符`).to.equal(ruleNum.freeRule);
        }
    }

    /**
     * 获取订单运费
     * @param {object} [opts={}] 
     * @param {string} [opts.provinceCode] 
     * @param {object[]} [opts.orderSpus=[]] 
     * @param {number} [opts.orderSpus.spuId] 
     * @param {number} [opts.orderSpus.orderNum] 
     * @param {number} [opts.orderSpus.orderMoney] 
     * @param {object[]} [opts.orderSpus.skuDTOS] 
     * @param {number} [opts.orderSpus.skuDTOS.skuId] 
     * @param {number} [opts.orderSpus.skuDTOS.num] 
     */
    async getOrderFee(opts) {
        let totalFee = 0, type = 0;
        const group = await this.groupByTemplateId(opts.orderSpus);
        console.log(`\n group=${JSON.stringify(group)}`);
        const templateIds = Object.keys(group);
        if (templateIds.length == 1 && templateIds[0] == this.defaultTemplate.get(-1).pId) {
            type = this.defaultTemplate.get(-1).type;
        } else if (templateIds.length == 1 && templateIds[0] == this.defaultTemplate.get(-3).pId) {
            type = this.defaultTemplate.get(-3).type;
        }
        // console.log(this.templateMap);
        for (const templateId in group) {
            if (templateId == this.defaultTemplate.get(-1).pId || templateId == this.defaultTemplate.get(-3).pId) {
                console.log(`商品${JSON.stringify(group[templateId])}的运费模板为默认(包邮到付)模板${templateId}`);
                continue;
            }
            const template = this.templateMap.get(Number(templateId));
            console.log(template);
            if (!template) throw new Error(`当前店铺不存在该运费模板:${templateId}`);
            const fee = await this.getDresListFee(opts.provinceCode, template, group[templateId]);
            totalFee += fee;
            console.log(fee, totalFee);
        }
        // const expFee = { type: type, val: totalFee };
        // console.log(expFee);
        return { type: type, val: totalFee };
    }

    /**
     * 商品按照运费模板分组
     * @param {*} orderSpus 
     */
    async groupByTemplateId(orderSpus) {
        const group = {}; //模板id : dresInfo
        for (const orderSpu of orderSpus) {
            const spu = _.cloneDeep(orderSpu);
            const dresFull = await sp.spdresb.getFullById({ id: spu.spuId }).then(res => res.result.data);
            if (!group.hasOwnProperty(dresFull.spu.templateId)) group[dresFull.spu.templateId] = [];
            //根据sku计算重量
            let weight = 0;
            if (!orderSpu.skuDTOS) {
                console.log('按照spu计算重量');
                weight = dresFull.spu.weight * spu.orderNum;
            } else {
                console.log('按照sku计算重量');
                orderSpu.skuDTOS.forEach(sku => {
                    const dresSku = dresFull.skus.find(obj => obj.id == sku.skuId);
                    weight += dresSku.weight * sku.num;
                });
            }
            group[dresFull.spu.templateId].push(Object.assign(spu, { templateId: dresFull.spu.templateId, weight: weight }));
        }
        return group;
    }

    /**
     * 获取相同模板下商品运费
     * @param {string} provinceCode
     * @param {object} [dresList=[]] 
     */
    async getDresListFee(provinceCode, template, dresList) {
        provinceCode = provinceCode + '';
        let dresListInfo = { weight: 0, orderNum: 0, orderMoney: 0 };
        for (const dresFull of dresList) {
            if (!dresFull.weight) throw new Error(`该商品重量不存在或为0:${JSON.stringify(dresFull)}`);
            dresListInfo.weight += dresFull.weight;
            dresListInfo.orderNum += dresFull.orderNum;
            dresListInfo.orderMoney += dresFull.orderMoney;
        }
        //判断是否满足包邮条件
        const isFree = this.judgeFree(template, provinceCode, dresListInfo);
        console.log('是否满足包邮条件', isFree);
        console.log('dresListInfo', dresListInfo);
        if (isFree) return 0;
        //计费
        return this.calculateFee(template, provinceCode, dresListInfo);
    }

    /**
     * 根据规则计算运费
     * @param {object} template
     * @param {string} provinceCode
     * @param {object} [dresListInfo={}] 
     * @param {number} [orderSpu.spuId] 
     * @param {number} [orderSpu.orderNum] 
     * @param {number} [orderSpu.orderMoney] 
     * @param {number} [orderSpu.weight] 
     */
    calculateFee(template, provinceCode, dresListInfo) {
        let feeRule = template.spFeeRules.get(provinceCode);
        console.log(feeRule);
        if (!feeRule) {
            //使用默认计费规则
            feeRule = [...this.templateMap.get(template.id).spFeeRules.values()].find(obj => obj.defaultFlag = 1);
            console.log(`${provinceCode}省使用默认计费规则`);
            console.log(feeRule);
        }
        if (feeRule.feeType == 1) { //计件
            let addFee = feeRule.addFee * Math.ceil((dresListInfo.orderNum - feeRule.startNum) / feeRule.addNum);
            if (addFee < 0 || isNaN(addFee)) addFee = 0;
            return feeRule.startFee + addFee;
        } else if (feeRule.feeType == 2) { //计重
            let addFee = feeRule.addFee * Math.ceil((dresListInfo.weight - feeRule.startNum) / feeRule.addNum);
            if (addFee < 0 || isNaN(addFee)) addFee = 0;
            return feeRule.startFee + addFee;
        }
    }

    /**
     * 判断是否满足包邮
     * @param {object} template
     * @param {string} provinceCode
     * @param {object} [dresListInfo={}] 
     * @param {number} [orderSpu.spuId] 
     * @param {number} [orderSpu.orderNum] 
     * @param {number} [orderSpu.orderMoney] 
     */
    judgeFree(template, provinceCode, dresListInfo) {
        const freeRule = template.spShipFeeRules.get(provinceCode);
        if (!freeRule) return false;
        if (freeRule.shipFreeType == 1) { //件数
            if (dresListInfo.orderNum < freeRule.limitNum) {
                return false;
            } else {
                return true;
            }
        } else if (freeRule.shipFreeType == 2) { //金额
            if (dresListInfo.orderMoney < freeRule.limitNum) {
                return false;
            } else {
                return true;
            }
        }
    }
};

Template = function (template = {}) {
    this.id = template.id || '';	//Long	否	主键id —修改必传
    this.temlateName = template.temlateName || '';
    this.state = template.state;
    this.count = template.count || 0;
    this.defaultTemplateFlag = template.defaultTemplateFlag || 0;
    this.type = template.type || 0;
    this.spFeeRules = new Map();
    this.spShipFeeRules = new Map();
}

FeeRule = function (rule = {}) {
    this.id = rule.id || '';//Long	否	主键id —修改必传
    this.startProvCode = rule.startProvCode || 0;//String	否	始发省代码
    this.feeType = rule.feeType || 1;//	Integer	是	计费类型，1-计件 2-计重
    this.provIds = rule.provIds || 330000;	//String	否	省区列表，系统字典中地区类型（850）的code_value，逗号分隔。若为空代表默认运费规则
    this.startNum = rule.startNum || common.getRandomNum(2, 5);	//Decimal	是	起始数量
    this.startFee = rule.startFee || common.getRandomNum(8, 20);	//Decimal	是	起始运费
    this.addNum = rule.addNum || common.getRandomNum(1, 5);//	Decimal	是	每次递增数量
    this.addFee = rule.addFee || common.getRandomNum(1, 5);//	是	每次递增运费
    this.defaultFlag = rule.defaultFlag || 0;//Integer	是	是否默认模板 1是 0 不是
    this.deleteFlag = rule.deleteFlag || 0;//Integer	否	1代表删除 0代表更新 —模板修改必传
}

FreeRule = function (rule = {}) {
    this.id = rule.id || '';	//Long	否	主键id —修改必传
    this.shipFreeType = rule.shipFreeType || 1;//Long	是	包邮类型 1-满件数 2-满金额
    this.shipStartProvCode = rule.shipStartProvCode || 0;//String	否	始发省代码
    this.shipProvIds = rule.shipProvIds || 330000;	//String	否	省份集合代码
    this.limitNum = rule.limitNum || common.getRandomNum(5, 10);	//decimal	是	包邮满数量
    this.deleteFlag = rule.deleteFlag || 0;	//Integer	否	1代表删除 0代表更新 —模板修改必传
}

// module.exports = ShipFee;
const shipFee = module.exports = {};

/**
 * 初始化运费
 */
shipFee.setupShipFee = function (params) {
    return new ShipFee(params);
};

/**
 * 模板json
 * @param {*} params {id:,temlateName:,feeRule:{count:,type:,...},freeRule:{count:,type:,...}}
 */
shipFee.mockTemplateFeeRuleJson = async function (opts = {}) {
    opts = Object.assign({ feeRule: { count: 1 }, freeRule: { count: 1 } }, opts)
    // console.log(`\nopts=${JSON.stringify(opts)}`);

    const template = common.update({
        id: '',
        temlateName: `运费模板${common.getRandomStr(5)}`,
        state: 1,
        spFeeRules: [],
        spShipFeeRules: []
    }, opts);

    const areaJson = await basicJson.getAreaJson();
    // console.log(areaJson);
    for (const key in opts) {
        // console.log(key);
        let proFlag = 0;
        for (let index = 0; index < opts[key].count; index++) {
            const proTemp = proFlag + common.getRandomNum(1, 3);
            if (key == 'feeRule') {
                if (index == 0) opts[key].defaultFlag = 1;
                const rule = new FeeRule(Object.assign({ provIds: areaJson.proviceArr.slice(proFlag, proTemp).join() }, opts[key]));
                opts[key].defaultFlag = 0;
                template.spFeeRules.push(rule);
            } else if (key == 'freeRule') {
                const rule = new FreeRule(Object.assign({ shipProvIds: areaJson.proviceArr.slice(proFlag, proTemp).join() }, opts[key]));
                template.spShipFeeRules.push(rule);
            }
            proFlag = proTemp;
        }
    }
    return template;
}


