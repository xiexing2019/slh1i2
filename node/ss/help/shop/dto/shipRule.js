const common = require('../../../../lib/common');
const format = require('../../../../data/format');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const moment = require('moment');


class ShipRule {
    constructor() {
        /**  编号 */
        this.id = '';
        /**  计费类型 */
        this.feeType = 0;
        /**  省区列表 */
        this.provIds = '';
        /**  起始数量 */
        this.startNum = 0;
        /**  起始运费 */
        this.startFee = 0;
        /**  每次递增数量 */
        this.addNum = 0;
        /**  每次递增费用 */
        this.addFee = 0;
        /**  单元id */
        this.unitId = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        /**  始发省代码 */
        this.startProvCode = '';
        /**  是否从仓库发1是 */
        this.fromWarehouse = 0;
        /**  其他类型 */
        this.otherType = 0;
        /**  运费模板id */
        this.templateId = '';
        /**  是否默认运费 1-是  0-不是 */
        this.defaultFlag = 0;

        // this.ecCaption = {
        //     provIds: '北京市,天津市,河北省,山西省',
        //     feeType: '计重',
        //     startProvCode: ""
        // };
    }

};

module.exports = ShipRule;


