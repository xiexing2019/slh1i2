const common = require('../../../../lib/common');
const format = require('../../../../data/format');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const moment = require('moment');


class Shop {
    constructor(sellerInfo) {
        /**  编号 */
        this.id = sellerInfo.tenantId;
        /**  平台商户号（sn或自定义） */
        this.mchntNo = '';
        /**  子商户全称 */
        this.mchntName = sellerInfo.shopName;
        // /**  简称 */
        // this.mchntAbbr = '';
        /**  办公地址 */
        this.mchntAddr = '';
        /**  联系人 */
        this.contactPerson = '';
        /**  联系电话 */
        this.contactPhone = '';
        /**  邮箱 */
        this.contactEmail = '';
        /**  状态(初始0已提交:1已审核:2) */
        this.status = 2;
        // /**  是否删除 */
        // this.deleteFlag = 0;
        /**  清分ID */
        this.liquidationId = '';
        /**  单元id(租户) */
        this.unitId = sellerInfo.unitId;
        /**  租户id */
        this.tenantId = params.tenantId;

        this.endData = '';

        // this.deliverProvCode = 0;
        // this.deliverDetailAddr = '';
        // this.totalShopManageNum = 0;
        // this.existedShopManageNum = 0;

        // this.priceRule = new Map();     //价格体系
        // this.ParamRule = new Map();     //个性化参数
        // this.roleRule = new Map();      //角色
        // this.employeeRule = new Map();  //员工
    }

};

module.exports = Shop;
