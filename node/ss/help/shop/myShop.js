const ssReq = require('../../help/ssReq');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const shipFee = require('./manage/shipFee');
const Seller = require('../user/seller');
const ssConfigParam = require('../configParamManager');
const priceTypeManage = require('../../help/dres/priceType');


class MyShop {
    constructor(options) {
        options = options || {};
        this.options = options;
        this.seller = new Map();
        this.shipFee = shipFee.setupShipFee();
        this.configParam = new Map();
        this.buyer = new Map();
        this.priceType = priceTypeManage.setupPriceTypes();
    }

    async getConfigParam(params, area = 'spg') {
        const configParam = this.configParam.get(params.code);
        if (configParam) return configParam;

        let ssParam;
        if (area == 'spg') {
            ssParam = await ssConfigParam.getSpgParamInfo(params);
        } else if (area == 'spb') {
            ssParam = await ssConfigParam.getSpbParamInfo(params);
        } else {
            throw new Error(`不存在${area}`)
        }
        this.configParam.set(params.code, ssParam);
        return ssParam;
    }

    /**
     * 更新参数
     * @param {object} params 
     * @param {string} params.code 
     * @param {string} params.val 更新值(与原值相同时不更新)
     */
    async updateConfigParam(params) {
        const configParam = this.configParam.get(params.code);
        if (configParam.val == params.val) return;
        await configParam.updateParam({ val: params.val });
        // 等待参数生效 目前看起来效果不明显
        await configParam.waitParamChange({ expVal: params.val });
    }

    async getShipFee() {
        this.shipFee.initDefaultTemplate();
        await this.shipFee.getExistShipRule();
        return this.shipFee;
    }

    async getPriceType() {
        await this.priceType.getExistPriceList();
        return this.priceType;
    }

    /**
     * 切换卖家
     * @description 登录，保存卖家信息
     * @param {string} code
     * @param {string} shopName
     */
    async switchSeller({ code = ssAccount.seller6.mobile, shopName = ssAccount.seller6.shopName } = {}) {
        const sellerInfo = this.seller.get(code);
        if (sellerInfo && sellerInfo.shopMap && sellerInfo.shopMap.has(shopName)) {
            LOGINDATA = _.cloneDeep(sellerInfo.shopMap.get(shopName));
            const sessionLite = await sp.spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId, check: false });
            if (sessionLite.result.code == 0) {
                // 更新登录信息
                Object.assign(LOGINDATA, sessionLite.result.data);
                const sellerLoginInfo = _.cloneDeep(LOGINDATA);
                sellerInfo.shopMap.set(shopName, sellerLoginInfo);
                return sellerInfo.shopMap.get(shopName);
            }
        }
        await ssReq.ssLogin({ code });
        const seller = new Seller(LOGINDATA);
        await seller.changeUserShop(shopName);
        await seller.setShopDetail(shopName);
        seller.appKey = ssAccount.seller6.appKey;
        seller.appSecret = ssAccount.seller6.appSecret;
        if (seller.appKey && seller.appSecret) {
            await seller.refreshToken(shopName);
        }
        this.seller.set(code, seller);
        return this.seller.get(code).shopMap.get(shopName);
    }

    /**
     * 获取卖家信息
     * @param {string} code
     * @param {string} shopName
     */
    getSellerInfo({ code = ssAccount.seller6.mobile, shopName = ssAccount.seller6.shopName } = {}) {
        return this.seller.get(code).shopMap.get(shopName);
    }

    /**
     * 买家登录
     * @param {Object} params
     * @param {string} params.tenantId 卖家tid
     * @param {string} params.sellerId
     * @param {string} params.mobile
     * @param {string} params.appId
     */
    async switchBuyer(params) {
        const sellerInfo = this.getSellerInfo({ code: LOGINDATA.mobile, shopName: LOGINDATA.shopName });
        params = {
            productCode: 'slhMallWxApplet',
            appId: ssAccount.seller6.appId,
            openId: ssAccount.client.openId,
            mobile: ssAccount.client.mobile || '',
            tenantId: sellerInfo.tenantId,
            ...params
        };
        await ssReq.ssClientGuestLogin(params);
    }
}


module.exports = MyShop;
