const common = require('../../lib/common');
const ssConfig = require('../../reqHandler/ss/spg/config');

/**
 * 参数配置 confc.sc_param
 * 所属域参数值 spugr.sc_param_owner_val
 */
class SsConfigParam {
    constructor(paramInfo = {}) {

        /** 所属者类型。1 全局, 5 集群, 6 租户, 7 单元等 */
        this.ownerKind = '';
        /** 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID */
        this.ownerId = 0;

        /**  ID */
        this.id = '';
        /**  参数代码 */
        this.code = '';
        /**  配置域。system、business、role等值 */
        this.domainKind = '';
        /**  参数名称 */
        this.name = '';
        /**  参数值 */
        this.val = '';
        /**  类别 */
        this.catId = '';
        /**  可设置级别 */
        this.ownerLevel = 0;
        /**   顺序号 */
        this.showOrder = 0;
        /**  是否只读 */
        this.readOnly = 0;
        /**  修改是否需要授权 */
        this.needAuth = 0;
        /**  录入类型 */
        this.inputType = 0;
        /**  是否组合参数 */
        this.isComp = 0;
        /**  流量切分比例 */
        this.splitRadio = '';
        /**  参数版本 */
        this.codeVersion = 0;
        /**  备注 */
        this.remark = '';
        /**  可用业务域 */
        this.bdomains = '';
        /**  指定包含或排除产品 */
        this.productIncExc = 0;
        /**  适用产品类型 */
        this.productTypes = '';
        /**  参数修改警示语 */
        this.warnningTxt = '';

        /** 原始值 方便还原 */
        this.originVal = '';
        /** URL路径 spg,spb */
        this.urlPath = 'spb';

        common.update(this, paramInfo);
        if (!paramInfo.remark) this.remark = this.name;
    }

    /**
     * 修改参数基本信息
     * @param {object} params 
     * @param {string} params.val 
     */
    async updateParam(params) {
        // 防止缓存中的参数值没有更新,暂强行修改
        // if (this.val == params.val) return;

        const res = await ssConfig.saveOwnerVal({ ownerKind: this.ownerKind, data: [{ code: this.code, val: params.val, domainKind: this.domainKind, ownerId: this.ownerId }] });
        console.log(`\n res=${JSON.stringify(res)}`);
        this.val = params.val;
    }

    /**
     * 还原参数
     */
    async returnOriginalParam() {
        if (this.originVal && (this.val == this.originVal)) return;

        await ssConfig.saveOwnerVal({ ownerKind: this.ownerKind, data: [{ code: this.code, val: this.originVal, domainKind: this.domainKind, ownerId: this.ownerId }] });
    }

    /**
     * 获取参数信息
     */
    async getParamInfo(params = {}) {
        if (this.urlPath == 'spg') {
            return ssConfig.getParamInfo({ ownerId: this.ownerId, code: this.code, ownerKind: this.ownerKind, domainKind: this.domainKind, ...params }).then(res => res.result.data);
        }

        if (this.urlPath == 'spb') {
            const res = await ssConfig.findProductParams({ ownerId: this.ownerId, codes: this.code, ownerKind: this.ownerKind, domainKind: this.domainKind, ...params });
            return res.result.data.rows.find(val => val.code == this.code);
        }
    }

    /**
     * 等待参数生效
     * @description 
     * 1. 修改参数后不会立即生效 马上校验业务流程时会受到缓存的影响
     * 2. 本方法适用场景 卖家修改参数后调用 查询当前参数值与期望值,不一致时进行轮询等待
     * 3. 目前没有强制修改缓存中参数的方法
     * @param {object} param
     * @param {string} param.expVal 期望参数值 
     * @param {string} [param.count] 轮询次数 
     * @param {string} [param.delayTime] 期望参数值 
     */
    async waitParamChange({ expVal, count = 5, delayTime = 500 }) {
        for (let index = 1; index <= count; index++) {
            const ssParam = await this.getParamInfo();
            if (ssParam.val == expVal) return;
            console.log(`参数确认${configParam.code},第${index}次查询,期望=${expVal},实际=${ssParam.val},参数不一致,${delayTime}ms后重新查询`);
            await common.delay(delayTime);
        }
    }

}


/**
 * 获取全局区参数详细信息
 * @description 全局区使用时 ownerKind1,3,5没有区别 可以写死
 * @param {object} params
 * @param {string} [params.domainKind='business'] 配置域* business 业务参数 system 系统参数
 * @param {string} params.code 编码
 * @param {string} [params.ownerId] 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID
 */
const getSpgParamInfo = async function (params) {
    // 获取不到会抛异常 这里不再多进行校验
    const res = await ssConfig.getParamInfo({ ownerKind: 6, domainKind: 'business', ...params });

    const configParam = new SsConfigParam(res.result.data);
    configParam.urlPath = 'spg';
    // 保存原始值 用例完成后 看情况还原
    configParam.originVal = configParam.val;
    configParam.ownerKind = res.params.jsonParam.ownerKind;
    configParam.ownerId = [1, 5].includes(configParam.ownerKind) ? 0 : params.ownerId;
    return configParam;
};

/**
 * 获取个性化参数详细信息
 * @description 单元区
 * @param {object} params
 * @param {string} [params.domainKind='business'] 配置域* business 业务参数 system 系统参数
 * @param {string} params.code 编码
 * @param {string} [params.ownerId] 所属者。ownerKind=是1或5时，设置为0。ownerKind=6，设置为租户ID。ownerKind=7，设置为单元ID
 */
const getSpbParamInfo = async function (params) {
    // 获取不到会抛异常 这里不再多进行校验
    const res = await ssConfig.findProductParams({ ownerKind: 6, codes: params.code, domainKind: 'business', ...params });
    console.log(`res=${JSON.stringify(res)}`);

    const _paramInfo = res.result.data.rows.find(val => val.code == params.code);
    expect(_paramInfo, `查询不到参数:${params.code}\n url:${res.reqUrl}`).not.to.be.undefined;

    const configParam = new SsConfigParam(_paramInfo);
    configParam.urlPath = 'spb';
    // 保存原始值 用例完成后 看情况还原
    configParam.originVal = configParam.val;
    configParam.ownerKind = res.params.jsonParam.ownerKind;
    configParam.ownerId = [1, 5].includes(configParam.ownerKind) ? 0 : params.ownerId;
    return configParam;
};


module.exports = {
    getSpgParamInfo,
    getSpbParamInfo
};