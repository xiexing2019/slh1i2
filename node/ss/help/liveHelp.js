const format = require('../../data/format');
const ss = require('../../reqHandler/ss/');
const caps = require('../../data/caps');
const fs = require('fs');
const path = require('path');
const common = require('../../lib/common')
const moment = require('moment');
const basicJson = require('./basicJson');

const liveHelp = module.exports = {};


/**
 * 保存直播任务
 * @param {Object} params
 * @param {number} params.liveRoomId 直播间id
 * @param {number} params.timGroupId 聊天室id
 * @param {string} params.spus 商品id列表，逗号隔开
 */
liveHelp.saveLiveTask = async function (params = {}) {
    let liveJson = Object.assign(basicJson.saveLiveTaskJson(), params);
    return ss.live.saveLiveTask(liveJson);
};
