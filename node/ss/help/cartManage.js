const common = require('../../lib/common');
const spCaps = require('../../reqHandler/sp/spCaps');
const sp = require('../../reqHandler/sp');
const ss = require('../../reqHandler/ss');

class ShoppingCart {
    constructor(params) {
        //卖家
        this.unitId = params.unitId;
        this.name = params.tenantName;
        this.tenantId = params.tenantId;
        this.loginUrl = params.shopLogo;
        this.clusterCode = params.clusterCode;
        this.linkFlag = 1;

        /** cart */
        this.spuCarts = new Map(); //以spuId为key
        // this.spu = {};
        // this.carts = new Map();
    }

    /**
     * 清空购物车
     */
    async emptyCart() {
        await sp.spTrade.emptyCart();
        this.spuCarts = new Map();
    }

    /**
     * 根据skuIds批量查询卖家sku的实时信息
     */
    async getSkusInfoInBatch() {
        const spus = [];
        [...this.spuCarts.keys()].forEach(spuId => {
            spus.push({ spuId: spuId, skuIds: [...this.spuCarts.get(spuId).carts.keys()] });
        });
        const skuInfoInBatch = await ss.spdresb.getSkusInfoInBatch({ buyerId: LOGINDATA.tenantId, spus: spus, _cid: this.clusterCode, _tid: this.tenantId }).then(res => res.result.data.rows);
        // console.log(`skuInfoInBatch=${JSON.stringify(skuInfoInBatch)}`);
        return skuInfoInBatch;
    }

    async skusInfoInBatchAssert() {
        const skusInfoInBatch = await this.getSkusInfoInBatch();
        for (const info of skusInfoInBatch) {
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: info.spu.id, _tid: this.tenantId, _cid: this.clusterCode }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(dresFull.spu, info.spu);
            info.skus.forEach(sku => {
                common.isApproximatelyEqualAssert(dresFull.skus.find(obj => obj.id == sku.id), sku);
            });
        }
    }

    getAllCarts() {
        const carts = new Map();
        this.spuCarts.forEach(spuCart => {
            spuCart.carts.forEach(skuCart => {
                carts.set(skuCart.skuId, skuCart);
            });
        });
        return carts;
    }

    /**
     * 批量删除
     * @param {*} skuIds
     */
    async deleteCartInBatch(skuIds = []) {
        const carts = this.getAllCarts();
        if (!skuIds.length) skuIds = [...carts.keys()];
        const ids = [];
        console.log(skuIds);

        skuIds.forEach(skuId => ids.push(carts.get(skuId).id));
        await sp.spTrade.deleteCartInBatch({ ids: ids, shopId: this.tenantId });
        skuIds.forEach(skuId => this.spuCarts.get(carts.get(skuId).spuId).carts.delete(skuId));

        this.correctSpuCartsCount();
    }

    /**
     * 修正spuCarts
     */
    correctSpuCartsCount() {
        console.log(this.spuCarts);

        this.spuCarts.forEach(spuCart => {
            if (!spuCart.carts.size) {
                this.spuCarts.delete(spuCart.spu.spuId);
            };
        });

        console.log(this.spuCarts);
    }

    /**
     * 修正商品信息 { spuId: cartJson.spu.spuId, title: `商品${common.getRandomStr(5)}`, skuPrice: 1000 }
     */
    correctSpu(updateSpuJson) {
        this.updateSpuCart({ spu: updateSpuJson });
        const spuCart = this.spuCarts.get(updateSpuJson.spuId);
        if (updateSpuJson.spuFlag == -4) {
            spuCart.spu
        }
        spuCart.carts.forEach(cart => {
            this.updateSkuCart(cart.spuId, Object.assign(cart, updateSpuJson));
        });
    }

    /**
     * 删除
     * @param {*} skuId 
     */
    async deleteCart(spuId, skuId) {
        const id = this.spuCarts.get(spuId).carts.get(skuId).id;
        await sp.spTrade.deleteCart({ id: id, shopId: this.tenantId });
        this.spuCarts.get(spuId).carts.delete(skuId);

        this.correctSpuCartsCount();
        return id;
    }

    /**
     * 删除购物车后,获取购物车列表数据校验
     * @param {*} spuId 
     * @param {*} skuId 
     */
    async deleteCartAssert(spuId, skuId) {
        const id = await this.deleteCart(spuId, skuId);
        const cartList = await this.getCartList();
        expect(cartList[0].carts.find(obj => obj.id == id), `删除未生效`).to.be.undefined;
    }

    /**
     * 获取购物车列表
     */
    async getCartList() {
        const cartList = await ss.spcart.getShoppingCartList({ shopId: this.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
        return cartList;
    }

    /**
     * 购物车列表
     * @param {*} ignore 
     */
    async cartListAssert(ignore = []) {
        const cartList = await this.getCartList();
        console.log(`\n cartList=${JSON.stringify(cartList)}`);
        // console.log(this.spuCarts);

        if (this.spuCarts.size) {
            const spuCarts = [...this.spuCarts.values()]; // this.getAllCarts();
            console.log(spuCarts);
            expect(spuCarts.length, `购物车总数${cartList.length}与预期${spuCarts.length}不符`).to.equal(cartList.length);
            spuCarts.forEach(spuCart => {
                console.log('spuCart:%j', spuCart);
                console.log(spuCarts);
                // console.log(cartList[0].carts.find(obj => obj.id == cart.id));
                const cartListSpu = cartList.find(obj => obj.spu.spuId == spuCart.spu.spuId);
                common.isApproximatelyEqualAssert(spuCart, cartListSpu, ignore, `购物车spuCart与预期不符`);
                spuCart.carts.forEach(cart => {
                    console.log(cart);
                    common.isApproximatelyEqualAssert(cart, cartListSpu.carts.find(obj => obj.skuId == cart.skuId), ignore, `购物车Cart与预期不符`);
                });
            });
        } else {
            expect(cartList, `购物车列表${JSON.stringify(cartList)}不为空`).to.be.empty;
        }
    }

    /**
     * 购物车列表
     * @param {*} ignore 
     */
    async cartListAssertBySkuId() {
        const cartList = await this.getCartList();
        // console.log(`\n cartList=${JSON.stringify(cartList)}`);
        // console.log(this.spuCarts);

        if (this.spuCarts.size) {
            const carts = this.getAllCarts();
            console.log(carts);
            expect(carts.size, `购物车总数${cartList[0].carts.length}与预期${carts.size}不符`).to.equal(cartList[0].carts.length);
            [...carts.values()].forEach(cart => {
                // console.log(cart);
                // console.log(cartList[0].carts.find(obj => obj.id == cart.id));
                common.isApproximatelyEqualAssert(cart, cartList[0].carts.find(obj => obj.skuId == cart.skuId), ['id', 'spec1Name', 'spec2Name', 'spec3Name', 'tagId', 'tagKind', 'rem'], `购物车cart与预期不符`);
            });
        } else {
            expect(cartList, `购物车列表${JSON.stringify(cartList)}不为空`).to.be.empty;
        }
    }
    /**
     * 购物车按spu备注
     * @param {number} spuId 
     * @param {string} rem 
     */
    async updateSpuRem(spuId, rem) {
        await ss.spcart.updateSpuRem({ spuId: spuId, rem: rem });
        this.spuCarts.get(spuId).carts.forEach(cart => {
            this.updateSkuCart(spuId, { skuId: cart.skuId, rem: rem });
        });
    }

    /**
     * 更新购物车
     * @param {number} skuId 
     * @param {object} [opts={}] 
     * @param {number} [opts.spuId] 
     * @param {number} [opts.skuNum] 
     * @param {string} [opts.rem] 
     */
    async updateShoppingCart(spuId, skuId, opts) {
        await sp.spTrade.updateCart({
            id: this.spuCarts.get(spuId).carts.get(skuId).id,
            spec1: this.spuCarts.get(spuId).carts.get(skuId).spec1,
            spec2: this.spuCarts.get(spuId).carts.get(skuId).spec2,
            spec3: this.spuCarts.get(spuId).carts.get(skuId).spec3,
            spec1Name: this.spuCarts.get(spuId).carts.get(skuId).spec1Name,
            spec2Name: this.spuCarts.get(spuId).carts.get(skuId).spec2Name,
            spec3Name: this.spuCarts.get(spuId).carts.get(skuId).spec3Name,
            skuId: this.spuCarts.get(spuId).carts.get(skuId).skuId,
            skuNum: opts.skuNum,
            skuPrice: this.spuCarts.get(spuId).carts.get(skuId).skuPrice,
            rem: opts.rem || this.spuCarts.get(spuId).carts.get(skuId).rem
        });
        this.updateSkuCart(spuId, Object.assign({ skuId: skuId }, opts));
        if (opts.rem) {
            this.spuCarts.get(spuId).carts.forEach(cart => {
                this.updateSkuCart(spuId, Object.assign({ skuId: cart.skuId }, { rem: opts.rem }));
            });
        }
    }

    /**
     * 添加购物车
     * @param {object} addCartJson 
     */
    async saveCartInBatchs(addCartJson) {
        const saveCart = await sp.spTrade.saveCartInBatchs(addCartJson);
        console.log(`\n 加入购物车=${JSON.stringify(saveCart)}`);
        if (this.spuCarts.get(addCartJson.spu.spuId)) {
            addCartJson.carts.forEach(cartInfo => {
                this.spuCarts.get(addCartJson.spu.spuId).carts.forEach(cart => {
                    if (cartInfo.skuId == cart.skuId) {
                        // console.log(cart);
                        cartInfo.skuNum += cart.skuNum;
                        cartInfo.rem += `,${cart.rem}`;
                    };
                });
            });
        };
        console.log(`\n加入购物车Json=${JSON.stringify(addCartJson)}`);
        this.updateSpuCart(addCartJson);
        saveCart.result.data.rows.forEach(cart => {
            this.updateSkuCart(cart.spuId, cart)
        });
    }

    /**
     * 保存卖家信息
     * @param {*} sellerInfo 
     */
    saveCartSellerInfo(sellerInfo) {
        this.unitId = sellerInfo.unitId;
        this.name = sellerInfo.tenantName;
        this.tenantId = sellerInfo.tenantId;
        this.loginUrl = sellerInfo.shopLogo;
        this.clusterCode = sellerInfo.clusterCode;
    }

    /**
     * 更新cart信息
     * @param {*} spuCartInfo {spu:{},carts:[{},{}]}
     */
    updateSpuCart(spuCartInfo) {
        if (this.spuCarts.has(spuCartInfo.spu.spuId)) {
            const cartSpu = this.spuCarts.get(spuCartInfo.spu.spuId);
            common.update(cartSpu.spu, spuCartInfo.spu);
            this.spuCarts.set(cartSpu.spu.spuId, cartSpu);
            if (spuCartInfo.carts) {
                spuCartInfo.carts.forEach(cartInfo => {
                    this.updateSkuCart(spuCartInfo.spu.spuId, Object.assign(cartInfo, spuCartInfo.spu));
                });
            }
        } else {
            const cartSpu = new CartSpu();
            common.update(cartSpu.spu, spuCartInfo.spu);
            this.spuCarts.set(cartSpu.spu.spuId, cartSpu);
            if (spuCartInfo.carts) {
                spuCartInfo.carts.forEach(cartInfo => {
                    this.updateSkuCart(spuCartInfo.spu.spuId, Object.assign(cartInfo, spuCartInfo.spu));
                });
            }
        }
        return this;
    }

    /**
     * 更新cart信息
     * @param {*} cartInfo
     */
    updateSkuCart(spuId, cartInfo) {
        // console.log(`this.spuCarts.get(spuId)=${JSON.stringify(this.spuCarts.get(spuId))}`);
        if (this.spuCarts.get(spuId).carts.has(cartInfo.skuId)) {
            const cart = this.spuCarts.get(spuId).carts.get(cartInfo.skuId);
            common.update(cart, cartInfo);
            this.spuCarts.get(spuId).carts.set(cart.skuId, cart);
        } else {
            const cart = new CartSku();
            common.update(cart, cartInfo);
            this.spuCarts.get(spuId).carts.set(cart.skuId, cart);
        }
        return this;
    }

    getShop() {
        return {
            unitId: this.unitId,
            traderName: this.name,
            tenantId: this.tenantId,
            logoUrl: this.loginUrl,
            clusterCode: this.clusterCode,
            linkFlag: this.linkFlag
        };
    };

    // getCart() {
    //     return Object.assign(
    //         this.getShop(), {
    //             cartsNum: this.carts.size,
    //             carts: [...this.carts.values()]
    //         }
    //     )
    // };

    /**
     * 拼接购物车json
     * @description 
     * 1. 指定sku的id和数量:skus:[{skuId:000,num:1},...]
     * 2. 指定sku的件数count和每个sku的数量num: {count：1，num：2}
     * @param {object} dres 商品
     * @param {object} [opts={}] 配置
     * @param {number} [opts.count=2] 加入单据sku件数
     * @param {string} [opts.num=1] 加入单据单个sku数量
     * @param {string} [opts.enableAct=true] 
     */
    makeCartJson(dres, opts = { type: 2, count: 1 }) {
        if (!dres.spu.ecCaption.docHeader[0]) dres.spu.ecCaption.docHeader[0].docId = '';
        const cartJson = {
            trader: {
                tenantId: this.tenantId,
                traderName: this.name,
                unitId: this.unitId
            },
            spu: {
                spuName: dres.spu.name,
                classId: dres.spu.classId,
                spuId: dres.spu.id,
                title: dres.spu.title,
                spuCode: dres.spu.code,
                spuPic: dres.spu.ecCaption.docHeader[0].docId
            },
            carts: []
        };

        // console.log(opts.type);
        if (opts.type == 1) {
            opts.skus.forEach(skuInfo => {
                const sku = dres.skus.find(sku => sku.id == skuInfo.skuId);
                // console.log(`sku=${JSON.stringify(sku)}`);
                cartJson.carts.push(cart(sku, skuInfo.num));
            });
        } else if (opts.type == 2) {
            const count = Math.min(opts.count, dres.skus.length);
            for (let index = 0; index < count; index++) {
                cartJson.carts.push(cart(dres.skus[index], opts.num));
            }
        }

        function cart(sku, num = common.getRandomNum(1, 10)) {
            let skus = {
                spec1: sku.spec1,
                spec2: sku.spec2,
                spec3: sku.spec3,
                spec1Name: sku.ecCaption.spec1 || '',
                spec2Name: sku.ecCaption.spec2 || '',
                spec3Name: sku.ecCaption.spec3 || '',
                skuId: sku.id,
                skuNum: num,
                skuPrice: sku.pubPrice,
                rem: `lxx(不准改)`//common.getRandomStr(5)
            };
            if (dres.activity && JSON.stringify(dres.activity) != '{}') Object.assign(skus, { actId: dres.activity.actId, })
            return skus;
        }
        return cartJson;
    }
}

function CartSpu() {
    this.spu = new Spu();
    this.carts = new Map();
}

function Spu() {
    // this.isProtected = '';
    // this.spuFlag = 1;
    this.spuId = '';
    this.spuName = '';
    this.spuPic = '';
    this.title = '';
    this.spuCode = '';
    this.classId = '';
};

function CartSku() {
    /**  ID */
    this.id = '';
    // /**  单元ID */
    // this.unitId = '';
    // /**  卖家id */
    // this.sellerTenantId = '';
    // /**  卖家名称 */
    // this.sellerName = '';
    // /**  卖家单元id */
    // this.sellerUnitId = '';
    /**  卖家的商品sku */
    this.skuId = '';
    /**  卖家的商品spu */
    this.spuId = '';
    /**  spu名称 */
    this.spuName = '';
    /**  spu描述 */
    this.title = '';
    /**  spu图片，目前单个，多个以逗号分隔 */
    this.spuPic = '';
    /**  规格1 */
    this.spec1 = 0;
    /**  规格1名称 */
    this.spec1Name = '';
    /**  规格2 */
    this.spec2 = 0;
    /**  规格2名称 */
    this.spec2Name = '';
    /**  规格3 */
    this.spec3 = 0;
    /**  规格3名称 */
    this.spec3Name = '';
    /**  商品数量 */
    this.skuNum = 0;
    /**  价格(加入购物车时价格) */
    this.price = 0;
    /**  状态(1-有效(默认) 0-无效 卖家下线商品时应自动将该购物车记录置为无效) */
    this.flag = 1;
    // /**  业务域代码（购物车比较通用，可以支持不同业务域） */
    // this.bdomainCode = '';
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    // /**  sku的编码 */
    // this.skuCode = '';
    // /**  原始价格 */
    // this.originalPrice = 0;
    // /**  卖家的商品spu的origId */
    // this.spuOrigid = '';
    /**  标签id:活动id */
    this.tagId = '';
    // /**  扩展属性字段 */
    // this.extProps = {};
    /**  标签种类活动种类 */
    this.tagKind = 0;
    /**  备注 */
    this.rem = '';
    /**  活动id */
    this.actId = '';

    this.skuPrice = '';
    this.isProtected = false;
    this.spuFlag = 1;
};

const cartManage = module.exports = {};

/**
 * 初始化购物车
 * @param {*} params 
 */
cartManage.setupShoppingCart = function (params) {
    return new ShoppingCart(params);
};