const common = require('../../lib/common');
const spUp = require('../../reqHandler/sp/biz_server/spUp');

let clientManager = module.exports = {};


const functionType = {
    '1': '买家商品分享',
    '2': '买家上新反馈',
    '3': '我的设置-反馈'
}, itemType = {
    '1': '是否希望我们加入购买功能'
}, itemValue = {
    '0': '否',
    '1': '是',
};
/**
 * 买家反馈
 */
clientManager.BuyerFeedback = class {
    constructor(clientInfo = LOGINDATA) {
        this.id = '';
        this.buyerTenantId = clientInfo.tenantId;
        this.buyerName = clientInfo.userName;
        this.buyerUnitId = clientInfo.unitId;
        /**反馈的功能类型，1-买家商品分享反馈 2-买家上新反馈 3-我的设置-反馈 */
        this.functionType = 1;
        /**反馈项类型，买家商品分享：1-是否希望我们加入购买功能；买家意见反馈：该项不用填 */
        this.itemType = 1;
        /**反馈项的值，买家商品分享：1-是，0-否；买家意见反馈：该项不用填 */
        this.itemValue = 1;
        this.rem = common.getRandomStr(10);
    }

    async save() {
        const res = await spUp.saveBuyerFeedback({ functionType: this.functionType, itemType: this.itemType, itemValue: this.itemValue, rem: this.rem });
        this.id = res.result.data.val;
        this.flag = 1;
    }

    getInfo() {
        const info = this;
        info.ecCaption = { functionType: functionType[this.functionType] };
        if (this.functionType == 1) {
            info.ecCaption.itemType = itemType[this.itemType];
            info.ecCaption.itemValue = itemValue[this.itemValue];
        }
        return info;
    }
};