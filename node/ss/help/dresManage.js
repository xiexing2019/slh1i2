const common = require('../../lib/common');
const spCaps = require('../../reqHandler/sp/spCaps');
const sp = require('../../reqHandler/sp');
const ss = require('../../reqHandler/ss');
const pinyin = require("pinyin4js");

// const mockJsonParam = require('./mockJsonParam');

/**
 * 商品
 */
class Dres {
    constructor() {
        /** 商品spuId */
        this.id = '';
        /** 自定义商品详情 */
        this.goodsDetail = { goodsFabricStatus: 1, goodsBrandStatus: 1, goodsInvStatus: 1, goodsSeasonStatus: 1 };
        /**  集群id _cid */
        this.clusterCode = '';
        /**  租户id _tid */
        this.tenantId = '';
        /**  原价 */
        this.originalPrices = {};
        /** spu */
        this.spu = new DresSpu();
        /** skus */
        this.skus = new Map();
        /** 拆色拆码 */
        this.splitSkus = new Map();
    }

    /**
    * 保存自定义商品详情
    * @param {object} params 状态
    */
    async saveGoodsDetail(params) {
        await ss.spdresup.saveGoodsDetail(params);
        common.update(this.goodsDetail, params);
    }

    /**
     * 保存商品
     * @param {object} jsonParam
     */
    async saveDres(jsonParam) {
        const data = await sp.spdresb.saveFull(jsonParam).then(res => res.result.data);
        // console.log(`\ndata=${JSON.stringify(data)}`);

        !this.id && (this.id = data.spuId);
        !this.clusterCode && (this.clusterCode = LOGINDATA.clusterCode);
        !this.tenantId && (this.tenantId = LOGINDATA.tenantId);
        // 更新spu信息
        this.updateSpu(Object.assign({ discount: 1, specialFlag: 0 }, jsonParam.spu));
        // 新增时 需要初始化的字段
        if (!this.spu.id) {
            this.spu.showOrder = 999;
            this.spu.marketDate = common.getCurrentTime();
        }
        this.setNamePy();
        this.spu.id = data.spuId;
        this.spu.unitId = LOGINDATA.unitId;

        // 更新sku信息
        this.spu.stockNum = 0;
        jsonParam.skus.forEach((sku, index) => {
            sku.id = data.skuIds[index].skuId;
            this.updateSku(sku);
            //
            this.spu.stockNum += Number(sku.num);
        });
    }

    /**
     * 卖家商品spu信息修改
     * @param {object} params spuInfo
     */
    async editDresSpu(params) {
        await sp.spdresb.editDresSpu({ id: this.id, ...params });
        this.updateSpu(params);
    }

    /**
     * 卖家商品sku库存添加
     * @param {object} params
     * @param {object} params.skus
     */
    async updateDresSkuNum(params) {
        await ss.op.updateFullDresSkuNum(params);

        params.skus.forEach(skuInfo => {
            const sku = this.skus.get(skuInfo.code);
            sku.num = Number(skuInfo.num);
            this.skus.set(sku.id, sku);
        });

        this.spu.stockNum = 0;
        [...this.skus.values()].forEach(sku => this.spu.stockNum += Number(sku.num));
    }

    /**
     * 根据es搜索结果设值
     * @description 只有spu相关信息
     * @param {object} dresInfo
     */
    setByEs(dresInfo) {
        expect(dresInfo, `es未查找到商品`).not.to.be.undefined;
        this.id = dresInfo.id;
        this.clusterCode = dresInfo.clusterCode;
        this.tenantId = dresInfo.tenantId;
        common.update(this.spu, dresInfo);
        // dresInfo.forEach()
        return this;
    }

    /**
     * 搜索商品详情并更新商品信息
     */
    async searchAndSetByDetail() {
        const dresDetail = await this.getDetailForBuyer();
        this.setByDetail(dresDetail.result.data);
        return this;
    }

    /**
     * 根据卖家搜索设置
     * @param {object} dresInfo
     */
    setBySeller(dresInfo) {
        this.setByDetail(dresInfo);
        this.clusterCode = LOGINDATA.clusterCode;
        this.tenantId = LOGINDATA.tenantId;
        return this;
    }

    /**
     * 根据商品明细设置
     * @description {id,spu,skus}结构
     * @param {object} dresInfo
     */
    setByDetail(dresInfo) {
        this.id = dresInfo.id;
        this.originalPrices = dresInfo.originalPrices;
        dresInfo.goodsDetail && (this.goodsDetail = dresInfo.goodsDetail);
        this.updateSpu(dresInfo.spu);
        dresInfo.skus.forEach(sku => this.updateSku(sku));
        return this;
    }

    /**
     * 更新商品spu信息
     * @param {*} spuInfo
     */
    updateSpu(spuInfo) {
        common.update(this.spu, spuInfo);
        // 更新商品运费模板相关字段
        // 传参时覆盖更新 其他流程中不传则不校验 (初始化中已去除)
        spuInfo.hasOwnProperty('templateId') && (this.spu.templateId = spuInfo.templateId);
        spuInfo.hasOwnProperty('templateName') && (this.spu.templateName = spuInfo.templateName);
        return this;
    }

    /**
     * 更新商品sku信息
     * @param {*} skuInfo
     */
    updateSku(skuInfo) {
        if (this.skus.has(skuInfo.id)) {
            const sku = this.skus.get(skuInfo.id);
            common.update(sku, skuInfo);
            this.skus.set(sku.id, sku);
        } else {
            const sku = new DresSku();
            common.update(sku, skuInfo);
            sku.spuId = this.spu.id;
            sku.unitId = this.spu.unitId;
            // sku.ssNum = skuInfo.ssNum;   // ssNum和num不一样，ssNum是商城库存，num是saas同步过来的库存
            this.skus.set(sku.id, sku);
        }
        return this;
    }

    /**
     * 卖家查询商品列表
     * @param {object} params
     */
    async findSellerSpuList(params = {}) {
        params = Object.assign({
            nameLike: this.spu.title,
        }, params);
        const dresList = await sp.spdresb.findSellerSpuList(params).then(res => {
            console.log(`\ndresList=${JSON.stringify(res.result.data)}`);
            return res.result.data.rows;
        });

        const dresDetail = dresList.find(data => data.id == this.id);
        expect(dresDetail, `卖家查询商品列表 未查询到商品 spuId=${this.id}`).not.to.be.undefined;
        return dresDetail;
    }

    /**
     * 查询租户商品信息
     * @description 卖家
     * @return {object} {id,spu,skus}
     */
    async getFullById() {
        return sp.spdresb.getFullById({ id: this.id }).then(res => {
            console.log(`\n查询租户商品信息=${JSON.stringify(res)}`);
            return res.result.data;
        });
    }

    /**
     * 买家查询全部商品列表(查询本商品)
     * @description 仅获取本商品所在店铺所有商品
     * @param {object} params
     * @return {object} spu
     */
    async findBuyerSpuList(params = {}) {
        const qlRes = await ss.spchb.getDresSpuList({ tenantId: this.tenantId, orderBy: 'marketDate', orderByDesc: true, ...params });
        const dresSpu = qlRes.result.data.rows.find(data => data.id == this.id);
        expect(dresSpu, `买家获取全部商品列表 未查询到商品 spuId=${this.id}\n\t${qlRes.reqUrl}`).not.to.be.undefined;
        return dresSpu;
    }

    /**
     * 买家查询商品详情
     * @description 需要先es搜索 获取卖家_cid _tid
     */
    async getDetailForBuyer() {
        if (!this.clusterCode || !this.tenantId) {
            throw new Error(`商品 clusterCode=${this.clusterCode},tenantId=${this.tenantId},值有误`);
        }
        return sp.spdresb.getFullForBuyer({
            spuId: this.id,
            buyerId: LOGINDATA.tenantId,
            _cid: this.clusterCode,
            _tid: this.tenantId
        });
    }

    /**
     * 更新sku售罄状态接口
     * @param {Object} params
     * @param {string} params.validIds 售罄启用的sku的id 例如 “21,22,23”
     * @param {string} params.inValidIds 不启用的sku的id 例如 “21,22,23”
     */
    async updateSkuSoldOutFlag(params) {
        const validIds = _.cloneDeep(params.validIds);
        const inValidIds = _.cloneDeep(params.inValidIds);
        console.log(validIds);
        const res = await ss.spdresb.updateSkuSoldOutFlag({ validIds: params.validIds.toString(), inValidIds: params.inValidIds.toString() });
        console.log(`\n更新sku售罄状态接口=${JSON.stringify(res)}`);
        console.log(validIds);
        validIds.forEach(id => {
            const sku = this.skus.get(id);
            this.skus.set(sku.id, common.update(sku, { soldOutFlag: 1 }));
        });
        inValidIds.forEach(id => {
            const sku = this.skus.get(id);
            this.skus.set(sku.id, common.update(sku, { soldOutFlag: 0 }));
        });
    }

    /**
     * 修改卖家商品价格
     * @param {object} params
     */
    async updateSpuPrice(addPrice) {
        const prices = { pubPrice: this.spu.pubPrice + addPrice, price1: this.spu.price1 + addPrice, price2: this.spu.price2 + addPrice, price3: this.spu.price3 + addPrice, price4: this.spu.price4 + addPrice, price5: this.spu.price5 + addPrice }
        await this.updateSpuForSeller(Object.assign({ id: this.id }, prices));
        this.skus.forEach(sku => this.updateSku(Object.assign(sku, { pubPrice: this.spu.pubPrice, price1: this.spu.price1, price2: this.spu.price2, price3: this.spu.price3, price4: this.spu.price4, price5: this.spu.price5 })));
    }

    /**
     * 修改卖家商品
     * @param {object} params
     * @param {object} opt
     */
    async updateSpuForSeller(params, opt = {}) {
        const res = await sp.spdresb.updateSpuForSeller({ id: this.id, ...params });
        console.log(`\nres=${JSON.stringify(res)}`);
        this.updateSpu(params);

        // 拆色拆码
        if (params.hasOwnProperty('colorFlag') || params.hasOwnProperty('sizeFlag')) {
            this.splitSpuAndSkus(params);
        }
        (params.skus) && (this.skus.forEach(sku => this.updateSku(params.skus)));

        // opt.priceType && (await opt.priceType.getExpForPrice(this));
        // this.calculatePrice(this, opt.priceType);
    }

    /**
     * 更新商品运费模板
     * @param {object} params
     * @param {*} params.templateId
     */
    async updateFreeTemplate(params) {
        const res = await ss.spdresb.updateFreeTemplate({ id: this.id, ...params });
        console.log(`\n更新商品运费模板=${JSON.stringify(res)}`);
        this.updateSpu(params);
    }

    splitSpuAndSkus(params) {
        this.splitSkus = [];
        let spec1s = [];
        let spec2s;
        let spec1Ids;
        let spec2Ids;

        if (params.sizeFlag == 1) {
            this.spu.sizeSplitFlag = 2;
            this.spu.splitSpec1 = params.sizeSplit;
            spec1Ids = params.sizeSplit.split(',').map(ele => BASICDATA['605'].find(data => data.codeName == ele).codeValue);
            spec1s = spec1Ids.map(ele => { return Object.assign({}, { "spec1Id": ele, "spec1Name": BASICDATA['605'].find(data => data.codeValue == ele).codeName }) })
        }

        if (params.sizeFlag == 0) {
            this.spu.sizeSplitFlag = 0;
            this.spu.splitSpec1 = '';
            spec1Ids = Object.keys(this.spu.spec1Ids)
            spec1s = spec1Ids.map(ele => { return Object.assign({}, { "spec1Id": ele, "spec1Name": BASICDATA['605'].find(data => data.codeValue == ele).codeName }) })
        }

        if (params.colorFlag == 1) {
            this.spu.colorSplitFlag = 2;
            this.spu.splitSpec2 = params.colorSplit;
            spec2Ids = params.colorSplit.split(',').map(ele => BASICDATA['601'].find(data => data.codeName == ele).codeValue);
            spec2s = spec2Ids.map(ele => { return Object.assign({}, { "spec2Id": ele, "spec2Name": BASICDATA['601'].find(data => data.codeValue == ele).codeName }) })

        }

        if (params.colorFlag == 0) {
            this.spu.colorSplitFlag = 0;
            this.spu.splitSpec2 = '';
            spec2Ids = Object.keys(this.spu.spec2Ids)
            spec2s = spec2Ids.map(ele => { return Object.assign({}, { "spec2Id": ele, "spec2Name": BASICDATA['601'].find(data => data.codeValue == ele).codeName }) })
        }

        const splitSku = [...this.skus.values()][0]

        spec1s.forEach((spec1) => {
            spec2s.forEach((spec2) => this.splitSkus.push(Object.assign({}, splitSku, { strSpec1: spec1.spec1Name, strSpec2: spec2.spec2Name })));
        });
    }

    /**
     * 管理员修改商品类别
     * @param {object} params
     */
    async updateSpuClassByAdmin(params) {
        Object.assign(params, { _cid: this.clusterCode, unitId: this.spu.unitId, tenantId: this.tenantId, id: this.id });
        await sp.spdresb.updateSpuClassByAdmin(params);
        this.updateSpu(params);

        // 平台改价 resetPriceFlag 1:重置改价为原价 0:不重置(默认)
        if (params.resetPriceFlag) {
            this.spu.pubPrice = this.spu.esProps.originalPrice || this.spu.pubPrice;
        } else {
            if (params.hasOwnProperty('changeMoney')) {
                if (!this.spu.esProps) this.spu.esProps = {};
                this.spu.esProps.originalPrice = this.spu.pubPrice;
                this.spu.pubPrice = params.changeMoney;
            }
        }

        // 划线价 不传或者传null 清空
        if (!params.markingPrice) {
            this.spu.markingPrice = 0;
        }

        // 标签  web端给商品打标签,拱es筛选过滤用
        ['seasonLabel', 'mainKindLabel', 'colorLabel', 'versionLabel', 'themeLabel', 'fabric'].forEach(key => {
            if (params.hasOwnProperty(key)) {
                this.spu[key] = params[key];
            }
        });
    }

    /**
     * 卖家更变价格类型到发布价
     * @param {object} params jsonParam
     * @param {number} params.priceType 价格类型(取值1~5，分别对应价格1到价格5)
     * @param {string} [params.discount=1] 折扣 默认1
     * @param {string} [params.discountType=1] 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认 1
     */
    async changePubPriceByPriceType(params) {
        const res = await sp.spdresb.changePubPriceByPriceType(params);

        // 更新折扣
        // discountType 0:不乘折扣 1:客户默认折扣 2:产品折扣 默认1
        const discountType = params.hasOwnProperty('discountType') ? params.discountType : 1;
        const discount = params.hasOwnProperty('discount') ? params.discount : 1;
        this.spu.discount = [1, discount, this.spu.discount][discountType];

        // 更新价格
        // slhPrice为slh推送过来的价格  constructPrice为推送过来的最高价
        this.spu.pubPrice = Math.round(common.mul(this.spu[`price${params.priceType}`], this.spu.discount));

        return res;
    }

    /**
     * 卖家上架商品
     */
    async onMarket() {
        await sp.spdresb.onMarket({ ids: [this.id] });
        this.spu.marketFailure = '';
        this.spu.flag = 1;
    }

    /**
     * 卖家下架商品
     */
    async offMarket() {
        await sp.spdresb.offMarket({ ids: [this.id] });
        this.spu.flag = -4;
    }

    /**
     * 设置商品隐藏状态
     * @param {object} params
     * @param {string} params.typeId 0无隐藏 1搜索隐藏 2买家隐藏-店铺不可见 3买家隐藏-店铺可见
     */
    async hideSpuShow(params) {
        await sp.spdresb.hideSpuShow({ id: this.id, tenantId: this.tenantId, unitId: this.spu.unitId, typeId: params.typeId });
        this.spu.hideFlag = params.typeId;
    }

    /**
     * 寻找相似商品
     * @param {object} params
     */
    async searchSimilarDres(params = {}) {
        const similarList = await sp.spdresb.searchSimilarDres({ spuId: this.id, tenantId: this.tenantId, unitId: this.spu.unitId, ...params });
        return similarList;
    }

    /**
     * 获取库存期望值
     * @description
     * 1. 下单库存校验 num lockNum
     * 2. 下单 num不变 lockNum增加  支付 num增加 lockNum减少 取消 num不变 lockNum还原
     * @param {object} params
     * @param {array} params.purDetails 采购单明细
     * @param {string} params.payFlag 支付状态 0未支付 1已支付
     */
    changeStockByBill(params) {
        params.purDetails.forEach(detail => {
            if (this.skus.has(detail.skuId)) {
                const sku = this.skus.get(detail.skuId);
                if (params.payFlag == 0) {
                    sku.lockNum = common.add(sku.lockNum, detail.num);
                } else {
                    sku.num = common.sub(sku.num, detail.num);
                    sku.lockNum = common.sub(sku.lockNum, detail.num);
                    this.spu.stockNum = common.sub(this.spu.stockNum, detail.num);
                }
                this.skus.set(sku.id, sku);
            }
        });
        return this;
    }

    /**
     * 创建订单综合校验
     * @param {object[]} params
     * @param {string} params.tagId
     * @param {string} params.tagKind
     * @param {string} params.currentOrderNum
     * @param {string} [params.skuId]
     */
    async verifyCreateOrder(params) {
        const defParam = { tenantId: this.tenantId, spuId: this.id, spuName: this.spu.name, tagId: 0, tagKind: 0 };
        params = params.map(param => Object.assign({}, defParam, param));
        return sp.spTrade.verifyCreateOrder(params);
    }

    setNamePy() {
        this.spu.namePy = pinyin.convertToPinyinString(this.spu.name, '', pinyin.FIRST_LETTER);
    }

    /**
     * 获取商品详情期望值
     */
    getDetailExp() {
        const skus = _.cloneDeep([...this.skus.values()]);
        return {
            id: this.id,
            originalPrices: this.originalPrices,
            goodsDetail: this.goodsDetail,
            // 针对于独立部署app 返回该货品详情链接 微信小程序链接
            spuDetailUrl: `pages/index/index?page=goods&tenantId=${this.tenantId}&spuId=${this.id}`,
            spu: this.spu,
            skus: skus.map(sku => {
                // delete sku.weight;
                delete sku.spec1;
                delete sku.spec2;
                sku.soldOutFlag && (sku.num = 0);
                return sku;
            }),// sku.weight暂时无法校验 独立app会更新,saas商品为0
            splitSkus: this.splitSkus,
        }
    }
    /**
     * 获取泛行业商品重量区间
     */
    getWeightRange() {
        const skus = [...this.skus.values()];
        let weightArray = skus.map(sku => sku.weight);

        const weightLow = Math.min.apply(null, weightArray);
        const weightTop = Math.max.apply(null, weightArray);
        // return `${weightLow}-${weightTop}kg`;
        let weightStr = new Object();
        weightStr.weightLow = weightLow;
        weightStr.weightTop = weightTop;
        return weightStr;
    }

    /**
     * 买家获取商品详情期望值
     */
    getBuyerDetailExp(params = { zeroCanOn: 1 }) {
        const skus = _.cloneDeep([...this.skus.values()]);
        return {
            id: this.id,
            originalPrices: this.originalPrices,
            goodsDetail: this.goodsDetail,
            // 针对于独立部署app 返回该货品详情链接 微信小程序链接
            spuDetailUrl: `pages/index/index?page=goods&tenantId=${this.tenantId}&spuId=${this.id}`,
            spu: this.spu,
            skus: skus.map(sku => {
                delete sku.weight;
                delete sku.spec1;
                delete sku.spec2;
                // params.zeroCanOn && (sku.num = 9999);
                sku.soldOutFlag && (sku.num = 0);
                //根据参数
                return sku;
            }),// sku.weight暂时无法校验 独立app会更新,saas商品为0
            splitSkus: this.splitSkus,
        }
    }

    /**
     * 分享商品海报
     * @param {*} params
     */
    // async shareSku(params = {}) {
    //     const res = await ss.up.shareSku({ type: 3, shopId: this.tenantId, spuId: this.id, spuPrice: this.spu.pubPrice, spuOrgPrice: this.spu.pubPrice, shareScene: 0, ...params });
    //     console.log(`res=${JSON.stringify(res)}`);
    //     expect(res.result.data.val).not.to.be.null;
    // }

    //独立app都是以默认价计算其他价格，同步saas暂不考虑
    /**
     * 计算价格
     * @param {*} dres dres.jsonParam
     * @param {*} priceType
     */
    calculatePrice(dres, priceType) {
        switch (priceType.pricedec) {
            case 0:
                console.log('取整方式', { id: 0, name: "保留两位小数" });
                for (let value = 1; value <= 5; value++) {
                    // const outPriceTypeId = priceType.priceTypes.find(priceType => priceType.localPriceTypeId == value).outPriceTypeId;
                    // if (!outPriceTypeId) outPriceTypeId = 0;
                    // const selectItem = priceType.selectItems.find(obj => obj.id = outPriceTypeId);
                    const basicPriceType = priceType.priceTypes.get(value);
                    console.log(basicPriceType);
                    let price = `price${value}`;
                    dres.skus.map(sku => {
                        if (!sku[price]) sku[price] = (sku.pubPrice * basicPriceType.percentage + basicPriceType.addition).toFixed(2);
                    });
                    dres.spu[price] = (dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition).toFixed(2);
                }
                break;
            case 1:
                console.log('取整方式', { id: 1, name: "四舍五入取整" });
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = priceType.priceTypes.get(value);
                    let price = `price${value}`;
                    dres.skus.map(sku => {
                        if (!sku[price]) sku[price] = Math.round(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                    });
                    dres.spu[price] = Math.round(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
            case 2:
                console.log('取整方式', { id: 2, name: "向上取整" });
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = priceType.priceTypes.get(value);
                    let price = `price${value}`;
                    dres.skus.map(sku => {
                        if (!sku[price]) sku[price] = Math.ceil(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                    });
                    dres.spu[price] = Math.ceil(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
            case 3:
                console.log('取整方式', { id: 3, name: "向下取整" });
                for (let value = 1; value <= 5; value++) {
                    const basicPriceType = priceType.priceTypes.get(value);
                    let price = `price${value}`;
                    dres.skus.map(sku => {
                        if (!sku[price]) sku[price] = Math.floor(sku.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                    });
                    dres.spu[price] = Math.floor(dres.spu.pubPrice * basicPriceType.percentage + basicPriceType.addition);
                }
                break;
        }
        return dres;
    }

    /**
     * 更新取整类型
     * @param {*} priceType
     * @param {*} sellerShop
     * @param {*} val 取整类型
     */
    async updatePricedec(priceType, sellerShop, val) {
        await priceType.updatePricedec(sellerShop, val);
        await priceType.getExpForPrice(this);
    }

    /**
     * 商品视频转码优化
     * @param {object} params
     * @param {string} params.spuId
     * @param {string} params.docHeader
     */
    async upgradeDressVideo(params = {}) {
        const video = [this.spu.docHeader.find(obj => obj.typeId == 3)];
        await ss.spdresb.translateCode({ spuId: this.id, docHeader: video, ...params });
    }
};

/**
 * 商品(独立app)
 */
class DresByApp extends Dres {
    constructor() {
        super();

        this.spu.classId = 9999;
        this.spu.isAppStatus = 1;
    }

    /**
     * 保存商品(独立app)
     * @param {object} jsonParam
     * @param {object} opt
     * @param {object} opt.priceType
     */
    async saveDresByApp(jsonParam, opt = {}) {
        const data = await ss.spdresb.saveFullByApp(jsonParam).then(res => res.result.data);
        console.log(`\ndata=${JSON.stringify(data)}`);

        !this.id && (this.id = data.val);
        !this.clusterCode && (this.clusterCode = LOGINDATA.clusterCode);
        !this.tenantId && (this.tenantId = LOGINDATA.tenantId);
        // 更新spu信息
        if (opt.priceType) jsonParam = this.calculatePrice(jsonParam, opt.priceType);
        console.log(`=${JSON.stringify(jsonParam)}`);
        this.updateSpu(Object.assign({ discount: 1, specialFlag: 0 }, jsonParam.spu));

        // 新增时 需要初始化的字段
        if (!this.spu.id) {
            this.spu.showOrder = 999;
            this.spu.marketDate = common.getCurrentTime();
        }
        this.spu.id = this.id;
        // this.spu.pubPriceLow = jsonParam.spu.pubPrice;
        // this.spu.pubPriceTop = jsonParam.spu.priceTop.pubPrice;
        this.spu.unitId = LOGINDATA.unitId;
        this.setNamePy();
        await this.setDictId(jsonParam.spu);

        // 更新sku信息
        const dresFull = await this.getFullById();
        console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
        this.spu.stockNum = 0;
        jsonParam.skus.forEach((sku, index) => {
            // console.log(`\ndresFull.skus[index]=${JSON.stringify(dresFull.skus[index])}`);
            sku.id = dresFull.skus[index].id;
            this.updateSku(sku);
            this.spu.stockNum += Number(sku.num);
        });
        if (jsonParam.spu.addSpuType == 1) {
            const weightStr = this.getWeightRange();
            if (weightStr.weightLow == weightStr.weightTop) {
                this.spu.weightStr = `${weightStr.weightLow}kg`;
            } else {
                this.spu.weightStr = `${weightStr.weightLow}-${weightStr.weightTop}kg`;
            }
        } else {
            this.spu.weightStr = `${jsonParam.spu.weight}kg`;
        };

        // if (weightStr.split('-')[0] == weightStr.split('-')[1].split('k')[0]) {
        //     this.spu.weightStr = weightStr.split('-')[1];
        // } else {
        //     this.spu.weightStr = weightStr;
        // };
    };
    /**
     * 根据名称设置商品属性中的字典id
     * @param {*} params
     */
    async setDictId(params) {
        if (params.fabricName) {
            //面料
            const dictList = await ss.config.getSpbDictList({ typeId: 637, flag: 1 }).then(res => res.result.data.rows);
            // console.log(`637dictList=${JSON.stringify(dictList)}`);
            const dict = dictList.find(ele => ele.codeName == params.fabricName);
            dict && (this.spu.fabric = dict.codeValue);
        }

        if (params.seasonName) {
            //季节
            const dictList = await ss.config.getSpbDictList({ typeId: 613, flag: 1 }).then(res => res.result.data.rows);
            // console.log(`613dictList=${JSON.stringify(dictList)}`);
            const dict = dictList.find(ele => ele.codeName == params.seasonName);
            dict && (this.spu.season = dict.codeValue);
        }

        if (params.themeName) {
            //风格 2004 商品风格 2016
            const dictList = await ss.config.getSpbDictList({ typeId: 2016, flag: 1 }).then(res => res.result.data.rows);
            // console.log(`2016dictList=${JSON.stringify(dictList)}`);
            const dict = dictList.find(ele => ele.codeName == params.themeName);
            dict && (this.spu.theme = dict.codeValue);
        }

        if (params.brandName) {
            const dictList = await ss.config.getSpbDictList({ typeId: 606, flag: 1 }).then(res => res.result.data.rows);
            // console.log(`606dictList=${JSON.stringify(dictList)}`);
            const dict = dictList.find(ele => ele.codeName == params.brandName);
            dict && (this.spu.brandId = dict.codeValue);
        }

        if (params.slhClassName) {
            const classList = await ss.config.findDressClass().then(res => res.result.data.rows);
            // console.log(`classList=${JSON.stringify(classList)}`);
            const slhClass = classList.find(ele => ele.name == params.slhClassName);
            slhClass && (this.spu.priClassId = slhClass.id);
        }
    }

};

/**
 * 商品spu
 */
function DresSpu() {
    /**  主键id */
    this.id = '';
    /**  单元id */
    this.unitId = '';
    /**  款号 */
    this.code = '';
    /**  名称 */
    this.name = '';
    /**  标题 */
    this.title = '';
    // /**  名称拼音 */
    this.namePy = '';
    /**  类别 */
    this.classId = '';
    /**  规格1范围 */
    this.spec1Ids = {};
    /**  规格2范围 */
    this.spec2Ids = {};
    /**  规格3范围 */
    this.spec3Ids = {};
    /**  品牌 */
    this.brandId = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  划线价 */
    this.markingPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    // /**  最低价 */
    // this.pubPriceLow = 0;
    // /**  最高价 */
    // this.pubPriceTop = 0;
    /**  计量单位 */
    this.unit = '';
    /**  上架日期 */
    this.marketDate = '';
    /**  季节：1-6 春夏 秋 冬 春 夏 秋冬 。 对应字典2034 */
    this.platSeason = '';
    /**  产地 */
    this.origin = '';
    /**  发货地 */
    this.shipOriginId = 0;
    /**  运费方式 */
    this.shipFeeWayId = 0;
    /**  商品标题区图片 */
    this.docHeader = {};
    /**  商品内容区图片 */
    this.docContent = {};
    /**  是否特价商品 */
    this.special = 0;
    /**  是否允许退货 */
    this.isAllowReturn = 0;
    /**  发布范围 */
    this.pubScope = 0;
    /**  标签 */
    this.labels = {};
    /**  详情页面URL */
    this.url = '';
    /**  状态 -5 解绑saas -4 卖家下架 -3 商品申请上架 -2 平台下架 -1 商品删除 0 商品下架(对应slh停用) 1商品上架(校验通过) 2商品待上架(校验不通过) 3买家隐藏-店内不可见(hideFlag=2)*/
    this.flag = 1;
    /**  备注 */
    this.rem = '';
    // /**  商品属性 */
    // this.props = {};
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  操作版本号 */
    this.opVer = 0;
    // /**  乐观锁 */
    // this.ver = 0;
    /**  商陆花商品ID */
    this.slhId = '';
    /**  数据Hash值 */
    this.hashKey = '';
    /**  被平台下架原因 */
    this.offmarketReason = '';
    /**  平台下架时间 */
    this.offmarketTime = '';
    /**  上架失败原因 */
    this.marketFailure = '';
    /**  折扣 */
    this.discount = 0;
    /**  商品排序 */
    this.showOrder = 0;
    /**  第三方同步时间 */
    this.slhDate = '';
    /**  商品上新能力位 0 无能力 。1 上新推荐能力 */
    this.specialFlag = 0;
    /**  上架商品不上新原因 */
    this.unRemmondReason = '';
    /**  商陆花停用标志，0 商陆花未停用  1 商陆花已经停用 */
    this.slhUsingFlag = 0;
    /**  平台停用标志，0 平台未停用 1 平台已经停用 */
    this.spPlatUsingFlag = 0;
    // /** 尺码表信息 */
    // this.yardage_props = '';
    /** 重量(kg) */
    this.weight = 0.3;
    /** 图文描述 */
    this.graphicText = '';
    /** 私有类别id */
    this.priClassId = '';
    /** 厂商id */
    this.dresManuFacturerId = '';
    /** 是否放入待上架，0 不放入 1放入 */
    this.isBeOnlineStatus = 0;
    /** 是否是独立app新增 0 不是 1 是 */
    this.isAppStatus = 0;
    /**  泛行业规格对应关系字段 */
    this.specProps = {};
    /**  是否是虚拟商品 0 不是 1是 默认0 不是 */
    this.virtualFlag = 0;
    /**  运费模板id 不初始化 在updateSpu时 根据传参更新值 */
    // this.templateId = 1;
    // this.templateName = '包邮'; //模板名称

    /** 起售数 */
    this.pricesStart = {};
    /** 按手购买设置 */
    this.pricesInHand = {};

    this.fabric = 0;
    this.season = 0;
    this.theme = 0;

};

/**
 * SKU定义
 */
function DresSku() {
    /**  ID */
    this.id = '';
    /**  单元ID */
    this.unitId = '';
    /**  款号 */
    this.spuId = '';
    /**  规格1 */
    this.spec1 = 0;
    /**  规格2 */
    this.spec2 = 0;
    /**  规格3 */
    this.spec3 = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  数量 */
    this.num = 0;
    /** 商城商品数量 */
    this.ssNum = 0;
    /**  状态  好店商家下架-4 卖家申请上架针对平台下架 平台下架-2 删除-1 下架0 上架1 待上架2*/
    this.flag = 1;
    /**  备注 */
    this.rem = '';
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
    /**  商陆花SKU ID */
    this.slhId = '';
    /**  sku的编码 */
    this.code = '';
    /**  占用库存 */
    this.occupyNum = 0;
    /**  微商城库存 */
    this.ssNum = 0;
    /**  sku的重量 */
    this.weight = 0.3;
    /**  saasId */
    this.saasId = LOGINDATA.saasId;
    /**  售罄标识 */
    this.soldOutFlag = 0;
};

const dresManage = module.exports = {
    Dres
};

/**
 * 初始化商品
 * @param {object} params
 * @param {string} [params.type] 类型 app:独立app
 */
dresManage.setupDres = function (params = {}) {
    if (params.type == 'app') {
        return new DresByApp();
    }
    return new Dres();
};

/**
 * 保存商品前置准备
 * @description
 * 1. 获取类别
 * 2. 获取颜色尺码等保存商品会用到的字典值至BASICDATA(cache)
 * 601颜色 605尺码 613季节 637面料 850发货地 2001运费方式 2003发布范围 2004风格
 * @param {object} params
 * @param {string} params.dictTypeId
 */
dresManage.prePrepare = async function (params = {}) {
    const dictIds = new Set(['601', '605', '606', '613', '850', '2001', '2002', '2003', '2004']);

    for (const typeId of dictIds) {
        // 优先从缓存获取
        if (!BASICDATA[typeId]) {
            if (['601', '605'].includes(typeId)) {
                BASICDATA[typeId] = await ss.config.getSpgDictList({ typeId: typeId, flag: 1 })
                    .then((res) => res.result.data.rows.map(ele => { return { codeName: ele.codeName, codeValue: ele.codeValue } }));
            } else if (['606', '613'].includes(typeId)) {
                BASICDATA[typeId] = await ss.config.getSpbDictList({ typeId: typeId, flag: 1 })
                    .then((res) => res.result.data.rows.map(ele => { return { codeName: ele.codeName, codeValue: ele.codeValue } }));
            } else {
                BASICDATA[typeId] = await sp.spugr.getDictList({ typeId: typeId, flag: 1 })
                    .then((res) => res.result.data.rows.map(ele => { return { codeName: ele.codeName, codeValue: ele.codeValue } }));

            }
        }
    }

    params.dictTypeId && (BASICDATA[params.dictTypeId] = await ss.config.getSpbDictList({ typeId: params.dictTypeId, flag: 1 })
        .then((res) => res.result.data.rows.map(ele => { return { codeName: ele.codeName, codeValue: ele.codeValue } })));
};

/**
 * 初始化规格 (外观，存储容量)
 */
dresManage.setupSpec = async function () {
    let typeList = [1], specObj = { 'outward': '外观', 'capacity': '存储容量' }, dictTypeId;
    for (let spec of Object.keys(specObj)) {
        for (let typeId of typeList) {
            let specListRes = await ss.spdresb.findSpecsList({ typeId: typeId }).then(res => res.result.data.rows.map(obj => obj.propCaption));
            if (!specListRes.includes(specObj[spec])) {
                const specRes = await ss.spdresb.saveSpecsFull({ propsCode: spec, propCaption: specObj[spec], typeId: typeId }).then(res => res.result.data);
                console.log(`新增规格成功=${JSON.stringify(specRes)}`);
            }
        }
    }
    let rows = await ss.spdresb.findSpecsList({ typeId: 2, propsCode: 'space' }).then(res => res.result.data.rows);
    if (rows.length == 0) {
        const specRes = await ss.spdresb.saveSpecsFull({ propsCode: 'space', propCaption: '产地', typeId: 2 }).then(res => res.result.data);
        console.log(`新增属性成功=${JSON.stringify(specRes)}`);
        dictTypeId = specRes.dictTypeId;
        await ss.config.saveDict({ codeName: '中国', typeId: specRes.dictTypeId });
    } else {
        dictTypeId = rows[0].dictTypeId;
        await ss.config.saveDict({ codeName: '中国', typeId: rows[0].dictTypeId });
    }
    return dictTypeId;
};
