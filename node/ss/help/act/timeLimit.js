const ActBase = require('./base');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const moment = require('moment');

/**
 * 限时折扣
 * ss_market_base
 */
class TimeLimit extends ActBase {
    constructor(params) {
        super();

        this.actType = 1;

        // this.actClass = 2;
    }

    /**
     * 新增限时折扣
     * @param {object}  
     */
    async saveFull(params, dresList = []) {
        // 设置了重复周期时,开始时间和结束时间的时分秒不传
        if (params.dimPeriod && params.dimPeriod.dayPeroidType != 0) {
            params.startDate = moment(params.startDate).format('YYYY-MM-DD');
            params.endDate = moment(params.endDate).format('YYYY-MM-DD');
        }

        const result = await ss.act.timeLimit.saveFull(params).then(res => res.result);
        console.log(`\n 保存限时活动=${JSON.stringify(result)}`);
        common.update(this, params);
        !this.id && (this.id = result.data.val);
        this.setFlag();
        // 设置商品信息
        this.updateSpu(dresList);

        return this;
    }

    /**
     * 限时折扣列表查询
     * @param {object}  
     */
    async getActList(params) {
        const info = await ss.act.timeLimit.getMarketList({ flag: this.flag, ...params }).then(res => res.result.data.rows.find(obj => obj.id == this.id));
        expect(info, `限时折扣列表查询不到活动id=${this.id}`).not.to.be.undefined;
        return info;
    }

    /**
     * 失效活动
     */
    async disableRule() {
        await ss.act.timeLimit.disableRule({ id: this.id });
        this.flag = -2;
        this.spuMap.forEach((value) => value.flag = -1);
        return this;
    }

    /**
     * 删除活动
     */
    async deleteRule() {
        await ss.act.timeLimit.deleteRule({ id: this.id });
        this.flag = -1;
        this.spuMap.forEach((value) => value.flag = -1);
        return this;
    }

    /**
     * 小程序专题活动获取活动列表 查找本活动
     * @param {*} params 
     */
    async findMarketForActivity(params) {
        const res = await ss.act.timeLimit.findMarketForActivity(params);
        console.log(`res=${JSON.stringify(res)}`);
        const data = res.result.data.rows.find(row => row.id == this.id);
        // expect(data, `小程序专题活动获取活动列表`).not.to.be.undefined;
        return data;
    }

    /**
     * 小程序专题活动获取活动列表 断言
     * @param {*} params 
     */
    async findMarketForActivityAssert(params) {
        const data = await this.findMarketForActivity(params);
        if (this.flag == 3) {
            expect(data, `小程序专题活动获取活动列表 未查询到活动id=${this.id} flag=${this.flag}`).not.to.be.undefined;
            common.isApproximatelyEqualAssert(this.getActBase(), data);
        } else {
            expect(data, `小程序专题活动获取活动列表 查询到活动id=${this.id} flag=${this.flag}`).to.be.undefined;
        }
    }

};

function SkuInv() {
    /**  编号  */
    this.id = '';
    /**  活动编号，指向ss_market_base  */
    this.actId = '';
    /**  sku编号  */
    this.skuId = '';
    /**  spu编号  */
    this.spuId = '';
    /**  库存数量  */
    this.invNum = 0;
    /**  已拍数量  */
    this.buyNum = 0;
    /**  锁定库存 */
    this.lockNum = 0;
    /**  单元id  */
    this.unitId = '';
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
};

const timeLimitManage = module.exports = {};

/**
 * 初始化限时活动
 * @param {*} params 
 */
timeLimitManage.setupTimeLimit = function (params) {
    return new TimeLimit();
};

/**
 * 拼接限时活动json
 */
timeLimitManage.mockActJson = function (dresList = [], opts = {}) {
    const json = common.update({
        actName: `限时活动${common.getRandomStr(5)}`,// 活动名称
        actLabels: ``,// 活动标签
        startDate: `${common.getCurrentDate()} 00:00:00`,//common.getCurrentTime(),// 开始时间
        endDate: `${common.getCurrentDate()} 23:59:59`,// 结束时间
        coupFlag: 1,// 可叠加适用优惠券 1-可以 0-不可以
        levelPrice: [0, 1, 2, 3, 4, 5]
    }, opts);

    /**
     * 按周期重复时，周期时间
     * 单独校验,基本流程中不加
     * dayPeroidType 周期类型,0=无周期, 1=每天,2=每月,3=每周
     */
    // json.dimPeriod = {};

    /** 
     * 限购规则
     * limitType 是	0=不限购; 1=每种货品限购; 2=每种货品前若干件享受折扣
     * limitNum  否	对应的限购值
     */
    json.limitVal = { limitType: opts.limitType || 0 };
    opts.hasOwnProperty('limitNum') && (json.limitVal.limitNum = opts.limitNum);

    /** 优惠策略 */
    const execVal = common.update({
        execType: 1,// 优惠类型1=折扣 2=定价(特价) 5=免减
        execNum: 1,// 优惠类型对应的值 1时传（1-9表示对应折扣），2时直接传价钱， 5时传减免额度
        execExtraKind: 4 // 额外附加的条件类型 4=抹零,抹去角和分  8=抹零,抹去分
    }, opts.execVal);
    json.execVal = execVal;

    // 活动商品信息
    json.spuList = dresList.map(dres => { return { spuId: dres.id, execVal } });
    return json;
};

/**
 * 设置活动重复周期
 * @description
 * 1. startDate,endDate为整个活动的时间
 * 2. dimPeriod为活动时间内的重复周期
 * 3. 买家调商品详情的时候只是前端显示的时候会变更他的活动状态，这个状态并没有在数据库中维护的(即查询活动详情的时候不做校验)
 * @param {*} params 
 */
timeLimitManage.setPeriod = function (params) {
    const { actJson, type, isContains } = params;
    if (type == 0) return;

    // 设置了活动重复周期时,活动开启/截止时间传参格式为年月日 
    // 写入数据库时会自动填充时分秒,导致开始与截止时间相同
    // 因此修改截止时间,保证2个时间不同
    actJson.endDate = moment(actJson.endDate).add(1, 'd');

    // 额外值 不包含时直接当天+1
    const extraVal = isContains ? 0 : 1;
    actJson.dimPeriod = {
        // 周期类型,0=无周期, 1=每天,2=每月,3=每周
        dayPeroidType: type,
        // 周期类型=1时为空;周期类型=2时,[1,31]代表每月几号;周期类型=3时, [1,7] 代表每周星期几
        dayValue: [['', '', moment().date(), moment().day()][type]],
        // 时间段, 格式= [{“begin”: “HH:mm”, “end”: “HH:mm”}]
        timeAreas: [{ begin: `${moment().hour() + extraVal}:00:00`, end: `${moment().hour() + 2}:00:00` }],
    };
    return;
};

/**
 * 删除所有活动
 * @description 初始化
 */
timeLimitManage.deleteOrDisableAllAct = async function () {
    for (const flag of [1, 2, 3]) {
        const secKillList = await ss.act.secKill.getSecKillList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`secKillList.length=${JSON.stringify(secKillList.length)}`);
        for (const secKill of secKillList) {
            const res = await ss.act.timeLimit.deleteRule({ id: secKill.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
        if (flag == 2) continue;
        const timeLimitList = await ss.act.timeLimit.getMarketList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`timeLimitList.length=${JSON.stringify(timeLimitList.length)}`);
        for (const act of timeLimitList) {
            const res = await ss.act.timeLimit.disableRule({ id: act.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
        const groupBuyList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`actList.length=${JSON.stringify(groupBuyList.length)}`);
        for (const act of groupBuyList) {
            const res = await ss.act.groupBuy.marketGroupBuyInvalid({ actId: act.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
    };
};
