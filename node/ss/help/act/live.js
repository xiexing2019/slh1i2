const common = require('../../../lib/common');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const liveHelp = require('../liveHelp');
const ActBase = require('./base');
const format = require('../../../data/format');

class Live extends ActBase {
    constructor(params) {
        super();
        /** =直播活动扩展表; */
        /**  单元id */
        this.unitId = '';
        /**  分支id */
        // this.branchId = '';
        /** 直播类型 */
        this.actType = 4;
        /**  直播间id */
        this.liveRoomId = '';
        /**  云通信群组id */
        this.timGroupId = '';
        /**  标题 */
        this.title = '';
        this.actLabel = '';
        /**  描述 */
        this.description = '';
        /**  封面url */
        this.coverUrl = '';
        /**  直播类型(0 默认 1 自定义第三方推流) */
        this.liveType = 0;
        /** 点赞数 */
        this.likeNum = 0;
        /**  直播实际开始时间 */
        this.factStartTime = '';
        /**  直播实际结束时间 */
        this.factEndTime = '';
        /**  机器人数量 */
        this.robotNum = 0;
        /**  结束类型，0 正常结束 1异常断开 */
        this.closeFlag = 0;
        /** 直播商品id */
        this.spus = '';
        /** liveSpus */
        this.liveSpus = [];
        // this.txSecret = '';
        // this.txTime = '';
        /** 优惠策略 */
        this.execVal = '';
        this.coupFlag = 1;
        /** 限购数量 */
        this.limitNum = 0;
        /** 推流rul */
        this.pushUrl = '';
        /** 拉流url */
        this.pullUrl = '';
        /**录制id */
        this.recordId = '';
        /** 直播商品状态 */
        this.spuList = [];
        /** 云通信群组信息 */
        this.timGroupMap = new Map();
    }


    /**
     * 获取直播功能适用情况
     */
    async getLiveShowEnableStatus() {
        let status = await ss.live.getLiveShowEnableStatus();
        return status;
    }

    /**
     * 获取直播间信息
     * @param {object}
     */
    async getLiveRoomInfo() {
        const liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows[0]);
        console.log(`直播间信息 = ${JSON.stringify(liveRoomInfo)}`);
        this.liveRoomId = liveRoomInfo.id;
        this.timGroupId = liveRoomInfo.timGroupId;
        this.likeNum == 0 && (this.likeNum = liveRoomInfo.likeNum);
        return this;
    }


    /**
     * 保存直播任务
     * @param {object}
     * @param {string} params.spus 商品id列表，逗号隔开
     */
    async saveLiveTask(params = {}, dresList = []) {
        const liveTaskRes = await liveHelp.saveLiveTask({ liveRoomId: this.liveRoomId, timGroupId: this.timGroupId, robotNum: 500, ...params });
        console.log(`保存直播任务结果= ${JSON.stringify(liveTaskRes)}`);
        common.update(this, liveTaskRes.result.data);
        common.update(this, liveTaskRes.params.jsonParam);
        this.addSpus(dresList);
        return this;
    }

    /**
     * 获取直播任务
     * @param {object} params
     * @param {string} params.sellerUnitId 卖家单元id
     * @param {number} params.spuFlag  默认不返回，1 获取商品列表
     */
    async getLastLiveTask(params = {}) {
        const liveInfo = await ss.live.getLastLiveTask(params).then(res => res.result.data);
        return liveInfo;
    }

    /**
     * 操作直播
     * @param {object} params
     * @param {object} params.opType 	操作类型，对应直播任务flag，3 ：开始，4： 暂停 0： 结束， -1： 删除
     */
    async operateLiveTask(params = {}) {
        const operateRes = await ss.live.operateLiveTask({ id: this.id, ...params });
        if ([0, -1].includes(params.opType)) {
            this.flag = 0;
        } else {
            this.flag = params.opType;
        }
        return operateRes;
    }

    /**
     * 直播弹商品
     * @param {object} params
     * @param {string} params.spuIds  商品id列表，逗号隔开
     */
    async popSpus(params = {}) {
        await ss.live.popSpus({ taskId: this.id, ...params });
    }


    /**
    * 新增直播活动商品
    * @param {object} params
    * @param {object} params.execVal 否 优惠策略
    * @param {number} params.coupOverFlag 否 叠加优惠券
    */
    async addLiveSpus(dres = {}, params = {}) {
        let dresJson = Object.assign({
            actId: this.id,
            execVal: { execType: 5, execNum: 10 },
            coupOverFlag: 1, // 叠加优惠券
            limitNum: 0,  // 限购数量
            invType: -1, // -1= 保持现有库存 1=使用自定义库存， 默认 -1
            spuId: dres.spu.id,
            skuInvList: { skuId: dres.skus[0].id, invNum: dres.skus[0].num }  //活动商品sku信息
        }, params);
        const addRes = await ss.live.addLiveTaskSpus(dresJson);
        if (params.check == false) {
            return addRes;
        } else {
            this.addSpu(dres, dresJson);
            return addRes;
        }
    }

    /**
    * 直播商品列表
    * @param {object} params
    * @param {string} params.sellerUnitId 否 卖家租户id ，买家调用时需要传
    * @param {string} params.showInv 否  传1代表显示库存
    */
    async liveSpusList(params = {}) {
        const liveSpusList = await ss.live.getLiveTaskSpus({ taskId: this.id, ...params })
            .then(res => res.result.data);
        this.updateSpu(liveSpusList.rows);
        return liveSpusList;
    }

    /**
     * 查询店铺关注情况
     * @param {object} params
     * @param {string}  params.shopId 店铺id
     */
    async checkShopFavorState(params) {
        const status = await ss.live.checkShopFavorState(params).then(res => res.result);
        return status;
    }

    /**
     * 点赞直播间
     * @param {object} params
     * @param {string} params.timUserName 是 聊天室用户名称
     * @param {number} params.likeNum 是  用户点赞数
     */
    async likeLiveRoom(params) {
        const likeNum = common.getRandomNum(5, 10);
        const likes = await ss.live.likeLiveRoom({ roomId: this.liveRoomId, likeNum: likeNum, ...params }).then(res => res.result);
        this.likeNum += likeNum;
        return likes;
    }

    /**
     * 获取直播结束统计信息
     */
    async getLiveTaskStats() {
        const liveStats = await ss.live.getLiveTaskStats({ taskId: this.id }).then(res => res.result.data);
        return liveStats;
    }

    /**
     * 开始录制
     * @param {object} params
     * @param {object} params.taskId
     * @param {object} params.spuId
     */
    async startRecord(params) {
        const recordId = await ss.live.startRecord(params).then(res => res);
        this.recordId = recordId;
        return recordId;
    }

    /**
    * 结束录制
    * @param {object} params
    * @param {number} params.recordId
    */
    async endRecord(params) {
        await ss.live.endRecord({ recordId: this.recordId, ...params });
    };

    /**
    * 取消录制
    * @param {object} params
    * @param {number} params.recordId 录制任务id
    */
    async cancelRecord(params) {
        await ss.live.cancelRecord({ recordId: this.recordId, ...params });
    };

    /**
    * 获取历史直播任务列表
    * @param {object} params
    * @param {number} params.jsonParam
    * @param {number} params.jsonParam.sellerUnitId
    * @param {number} params.jsonParam.nameLike
    * @param {number} params.jsonParam.searchStartTime
    * @param {number} params.jsonParam.searchEndTime
    */
    async findHistoryLiveRecord(params) {
        await ss.live.findHistoryLiveRecord(params);
    };

    /**
    * 录播操作
    * @param {object} params
    * @param {number} params.liveTaskIId
    * @param {number} params.recordIds
    * @param {number} params.operateType
    */
    async operateLiveTaskRecord(params) {
        await ss.live.operateLiveTaskRecord(params);
    };

    /**
    * 获取录播详情
    * @param {object} params
    * @param {number} params.sellerUnitId
    * @param {number} params.liveTaskId
    */
    async getLiveRecordFullInfo(params) {
        await ss.live.getLiveRecordFullInfo();
    };


    //----------------更新数据
    /**
     * 更新直播活动商品列表
     * @param {Array} dresList
     */
    addSpus(dresList = []) {
        this.spuList = [];
        dresList.forEach((obj, index) => {
            const spuId = obj.id || obj.spuId;
            // const spu = new LiveSpuList();
            const spu = this.getLiveSpuList();
            common.update(spu, obj.spu || dres);
            spu.spuId = spuId;
            spu.limitNum = 0;
            spu.execVal = this.execVal;
            let stockNum = 0;
            for (let stock of obj.skus) { stockNum += stock.num };
            spu.invNum = stockNum;
            spu.showOrder = index + 1;
            spu.pubPrice = obj.spu.pubPrice;
            spu.title = obj.spu.title;
            spu.code = obj.spu.code;
            spu.coverUrl = JSON.parse(obj.spu.docContent)[0].docId;
            if (this.execVal.execType == 5) {
                spu.afterDiscountPrice = common.sub(obj.spu.pubPrice, this.execVal.execNum);
            }
            spu.spus = [{
                docContent: obj.spu.docContent,
                id: obj.spu.id,
                name: obj.spu.name,
                price: obj.spu.pubPrice,
                stockNum: stockNum,
                title: obj.spu.title
            }];
            this.spuList.push(spu);
            // console.log(this.spuMap.get(spuId).spus);
        });
        return this;
    }

    /**
     * 更新直播活动商品列表
     * @param {Array} dresList
     */
    addSpu(obj, params) {
        if (!params.id) {
            const spuId = obj.id || obj.spuId;
            // const spu = new LiveSpuList();
            const spu = this.getLiveSpuList();
            common.update(spu, obj.spu || dres);
            spu.spuId = spuId;
            spu.limitNum = 0;
            spu.execVal = params.execVal || { execType: 5, execNum: 10 };
            let stockNum = 0;
            for (let stock of obj.skus) { stockNum += stock.num };
            spu.invNum = stockNum;
            spu.showOrder = 0;
            spu.pubPrice = obj.spu.pubPrice;
            spu.title = obj.spu.title;
            spu.code = obj.spu.code;
            spu.coverUrl = JSON.parse(obj.spu.docContent)[0].docId;
            if (spu.execVal.execType == 5) {
                spu.afterDiscountPrice = common.sub(obj.spu.pubPrice, this.execVal.execNum);
            }
            spu.spus = [{
                docContent: obj.spu.docContent,
                id: obj.spu.id,
                name: obj.spu.name,
                price: obj.spu.pubPrice,
                stockNum: stockNum,
                title: obj.spu.title
            }];
            // this.spuList.forEach(obj => obj.showOrder += 1);
            this.spuList.push(spu);
            // console.log(this.spuMap.get(spuId).spus);
            return this;
        } else {
            this.spuList.forEach(data => {
                if (data.id == params.id) {
                    data.execVal == params.execVal;
                    if (params.execVal.execType == 5) {
                        data.afterDiscountPrice = common.sub(obj.spu.pubPrice, params.execVal.execNum);
                    }
                }
            })
        }

    }

    /**
     * 查询直播商品列表后，更新spuList-id
     */
    updateSpu(params = []) {
        params.forEach(obj => {
            this.spuList.forEach(data => {
                if (obj.spuId == data.spuId) {
                    data.id = obj.id
                }
            })
        })
    }

    //--------------- 获取期望值
    /**
     * 获取直播任务期望值
     */
    getLastLiveTaskExp() {
        return {
            // txSecret: this.txSecret,
            flag: this.flag,
            // txTime: this.txTime,
            liveType: this.liveType,
            liveRoomId: this.liveRoomId,
            description: this.description,
            // onlineNum: this.onlineNum,
            title: this.title,
            pushUrl: this.pushUrl,
            unitId: this.unitId,
            robotNum: this.robotNum,
            id: this.id,
            timGroupId: this.timGroupId,
            pullUrl: this.pullUrl,
            coverUrl: this.coverUrl,
            liveSpus: this.liveSpus
        }
    }

    /**
     * 拼接商品详情的活动信息(一个商品)
     * @description dres.activity
     * @param {object} params
     * @param {string} params.spuId
     */
    getDresActExp(params) {
        if (!this.spuList.find(obj => obj.spuId == params)) {
            throw new Error(`actId=${this.id},不包含商品${param.spuId}，请检查`)
        }
        // 活动基础信息
        const mapFile = 'actName=title;actLabels=actLabel;actFlag=flag;actId=id;actType;startDate;endDate;actImgs=coverUrl;coupOverFlag=coupFlag;limitNum;execVal';
        const actBase = format.dataFormat(this, mapFile);

        const spu = this.spuList.find(obj => obj.spuId == params);
        const info = format.dataFormat(spu, 'showOrder;pubPrice;extFlag;afterDiscountPrice');

        // const execNum = spu.pubPrice - this.activityPrice;
        // info.execVal = `{"execNum": ${execNum}, "execType": 5}`;
        // info.execVal = `{"execNum": ${this.activityPrice}, "execType": 2}`;

        return Object.assign(info, actBase);
    }

    getLiveSpuList() {
        return {
            /** 折扣后价格 */
            afterDiscountPrice: '',
            /** 款号 */
            code: '',
            /** 商品id */
            spuId: '',
            /** 限购数量 */
            limitNum: '',
            /** spu名称 */
            title: '',
            /** 价格 */
            pubPrice: '',
            /** 排序 */
            showOrder: '',
            /** 弹出标志 */
            extFlag: 0,
            /** 封面url */
            coverUrl: '',
            /** 剩余库存 */
            invNum: '',
            /** 已销数量 */
            totalBuyNum: 0,
        }
    }


}

const liveManage = module.exports = {};

/**
 * 初始化直播
 */
liveManage.setupLive = function () {
    return new Live();
};


function LiveSpuList() {
    /** 折扣后价格 */
    this.afterDiscountPrice = '';
    /** 款号 */
    this.code = '';
    /** 商品id */
    this.spuId = '';
    /** 限购数量 */
    this.limitNum = '';
    /** spu名称 */
    this.title = '';
    /** 价格 */
    this.pubPrice = '';
    /** 排序 */
    this.showOrder = '';
    /** 弹出标志 */
    this.extFlag = 0;
    /** 封面url */
    this.coverUrl = '';
    /** 剩余库存 */
    this.invNum = '';
    /** 已销数量 */
    this.totalBuyNum = 0;
};

function TimGroup() {
    /**  编号 */
    this.id = '';
    /**  云通信群组id */
    this.timGroupId = '';
    /**  昵称 */
    this.nickName = '';
    /**  云通信账号 */
    this.userId = '';
    /**  头像url */
    this.avatar = '';
    /**  单元id */
    this.unitId = '';
    /**  分支id */
    this.branchId = '';
    /**  状态 */
    this.flag = 0;
    /**  创建时间 */
    this.createdDate = '';
    /**  修改时间 */
    this.updatedDate = '';
};
