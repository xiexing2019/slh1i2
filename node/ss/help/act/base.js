const common = require('../../../lib/common');
const format = require('../../../data/format');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const moment = require('moment');

/**
 * 营销活动基本信息表
 * ss_market_base
 *
 * 微商城营销体系设计
 * http://192.168.0.251:8080/index.php/apps/onlyoffice/11828?filePath=%2Fec-doc%2F05.项目组%2F04.衣科商城%2F10.设计%2F应用组%2F微商城营销体系设计.docx
 *
 *
 */
class ActBase {
    constructor() {
        /**  编号 */
        this.id = '';
        // /**  规则类型 */
        // this.ruleType = 0;
        /**  活动名称 */
        this.actName = '';
        /**  活动类型（限时-1 ，秒杀-2 ，拼团-3 ，直播-4, 群接龙-6）*/
        this.actType = 0;
        /**  活动标签 */
        this.actLabels = '';
        /**  活动图片 */
        this.actImgs = '';
        /**  周期性限时维度 */
        this.dimPeriod = {};
        /**  会员维度 */
        this.dimCustomer = {};
        /**  优惠网关 */
        this.execVal = {};
        /**  限购规则 */
        this.limitVal = {};
        /**  活动状态 -1:无效-被取消 0:无效-已结束 1:有效-未开始  2:有效-预热中  3:有效-进行中 4:有效-暂停*/
        this.flag = 1;
        /**  开始日期 */
        this.startDate = '';
        /**  截止日期 */
        this.endDate = '';
        /**  固有权重 */
        this.weights = 0;
        /**  同权重时优先级 */
        this.priority = 0;
        // /**  所属单元 */
        // this.unitId = '';
        /**   是否允许叠加优惠券 0-不行 1- 可以 */
        this.coupFlag = 0;
        // /**  创建人 */
        // this.createdBy = '';
        // /**  修改人 */
        // this.updatedBy = '';
        // /**  修改时间 */
        // this.updatedDate = '';
        // /**  创建时间 */
        // this.createdDate = '';

        this.spuMap = new Map();
    }

    /**
     * 限时/秒杀活动详情
     * @param {object} params
     * @param {string} params.id 活动id
     */
    async getById(params) {
        const actInfo = await ss.act.marketBase.getById({ id: this.id, ...params }).then(res => res.result.data);
        return actInfo;
    }

    /**
     * 限时/秒杀批量添加商品
     * @param {object} params
     * @param {Array} params.dresList 商品列表
     */
    async batchAddSpu(params) {
        const res = await ss.act.marketBase.batchAddSpu({ actId: this.id, spuIds: params.dresList.map(spu => spu.id) }).then(res => res.result);
        console.log(`\n res=${JSON.stringify(res)}`);
        let spuList = [];
        params.dresList.forEach(dres => {
            spuList.push(Object.assign(dres, {
                execVal: this.execVal,
                coupOverFlag: this.coupFlag,
                invType: -1
            }));
        });
        this.updateSpu(spuList);
    }

    /**
     * 限时/秒杀批量删除商品
     * @param {object} params
     * @param {string} params.actId 活动id
     * @param {Array} params.spuIds 商品id列表, 例: [1111,22222,3333]
     */
    async batchDeleteSpu(params) {
        const res = await ss.act.marketBase.batchDeleteSpu({ actId: this.id, spuIds: params.spuIds.map(spu => spu.id) }).then(res => res.result);
        params.spuIds.forEach(spu => this.spuMap.delete(spu.id));
    }

    /**
     * 查看单个活动商品信息
     * @param {object} params
     * @param {string} params.spuId 商品id
     */
    async findFullMarketSpu(params) {
        const spuFull = await ss.act.marketBase.findFullMarketSpu({ actId: this.id, spuId: params.spuId }).then(res => res.result.data);
        expect(spuFull).not.to.be.undefined;
        return spuFull;
    }

    /**
     * 修改活动商品信息
     * @param {object} params jsonParam
     * @param {string} params.id 活动商品主键id （注：非商品spuid）
     */
    async updateMarketSpu(params) {
        await ss.act.marketBase.updateMarketSpu(params).then(res => res.result.data);
        this.updateSpu([params]);
    }

    /**
     * 更新商品spu信息
     * @param {*} skuInfo
     */
    updateSpu(spuList) {
        console.log(`spuList=${JSON.stringify(spuList)}`);
        if (!spuList.hasOwnProperty('length')) throw new Error(`活动更新商品信息参数类型错误 ${typeof spuList}`);

        // 更新活动商品信息
        spuList.forEach(dres => {
            const spuId = dres.id || dres.spuId;
            const spu = this.spuMap.has(spuId) ? this.spuMap.get(spuId) : new MarketSpu();
            common.update(spu, dres.spu || dres);
            spu.id = '';// 待处理
            spu.spuId = spuId;
            spu.execVal = this.execVal;
            spu.coupOverFlag = this.coupFlag;

            this.spuMap.set(spuId, spu);
        });

        // 更新自定义库存数量(秒杀活动)
        [...this.spuMap.values()].forEach(spu => {
            spu.invNum = 0;
            spu.skuInvList.forEach(skuInv => {
                spu.invNum += skuInv.invNum;
            });
        });
        return this;
    }

    /**
     * 批量商品参与活动信息
     */
    async translateSpuPartAct(params) {
        const res = await ss.act.marketBase.translateSpuPartAct({ spuKey: 'spuId', setField: 'actInfos', jsonParam: [...this.spuMap.keys()].map(spuId => { return spuId }), ...params });
        console.log(res);
        return res;
    }

    /**
     * base活动
     */
    getActBase() {
        return {
            id: this.id,
            // ruleType: this.ruleType,
            actName: this.actName,
            actType: this.actType,
            actLabels: this.actLabels,
            actImgs: this.actImgs,
            dimPeriod: this.dimPeriod,
            dimCustomer: this.dimCustomer,
            execVal: this.execVal,
            limitVal: this.limitVal,
            flag: this.flag,
            startDate: this.startDate,
            endDate: this.endDate,
            weights: this.weights,
            priority: this.priority,
            // unitId: this.unitId,
            spuNum: this.spuMap.size,
            spuList: [...this.spuMap.values()]
        }
    };

    /**
     * 获取商品列表
     * @param {number} listNum 期望商品数量
     */
    async getDresList(listNum) {
        let dresList = [];
        const len = Math.floor(listNum / 20);
        // console.log(len);
        for (let index = 1; index <= len; index++) {
            const dresSpuList = await sp.spdresb.findSellerSpuList({ pageNo: index, pageSize: 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
            for (const dresSpu of dresSpuList) {
                const dresFull = await sp.spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
                const dres = dresManage.setupDres();
                dres.setBySeller(dresFull);
                // await dres.searchAndSetByDetail();
                dresList.push(dres.getDetailExp());
            }
            // console.log(dresList.length);
        }
        // console.log(dresList.length);
        if (listNum % 20 > 0) {
            // const dresSpuList = await ss.spchb.getDresSpuList({ pageNo: len, pageSize: (listNum - len), tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            const dresSpuList = await sp.spdresb.findSellerSpuList({ pageNo: len + 1, pageSize: listNum % 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
            for (const dresSpu of dresSpuList) {
                const dresFull = await sp.spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
                const dres = dresManage.setupDres();
                dres.setBySeller(dresFull);
                // await dres.searchAndSetByDetail();
                // console.log(dres.getDetailExp());
                dresList.push(dres.getDetailExp());
            }
            // console.log(this.dresList.length);
        }
        // console.log(this.dresList.length);
        return dresList;
    }

    /**
     * 设置活动状态
     * @description
     * 1. 根据本地时间判断 可能存在误差
     * 2. flag <=0都是无效的，>0都是有效的
     */
    setFlag() {
        // 无效或者暂停时都不处理
        if (this.flag <= 0 || this.flag == 4) return this;

        const curTime = common.getCurrentTime();
        if (moment(curTime).isBefore(this.startDate)) {
            this.flag = 1;
            return this;
        }

        if (moment(curTime).isAfter(this.endDate)) {
            this.flag = 0;
            return this;
        }
        // 进行中
        this.flag = 3;
        return this;
    }

    /** 设置商品的活动状态 */
    setDresFlag() {
        this.flag = moment(common.getCurrentTime()).isBetween(this.startDate, this.endDate, null, []) ? 1 : 0;
        return this;
    }

    /**
     * 拼接商品详情的活动信息(一个商品)
     * @description
     * 1. dres.activity
     * 2. 买家查询 getFullForBuyer
     * @param params
     * @param params.spuId
     */
    getDresActInfo(params) {
        if (!this.spuMap.has(params.spuId)) {
            throw new Error(`actId=${this.id},不包含商品${param.spuId}，请检查`)
        }

        // 活动基础信息
        const mapFile = 'actName;actFlag=flag;actId=id;actType;startDate;endDate;actImgs;actLabels';
        const actBase = format.dataFormat(this, mapFile);
        // 限时活动 活动商品根据重复周期 重设开始/结束/活动状态
        if (this.actType == 1) {
            resetActDate({ activity: actBase, dimPeriod: this.dimPeriod });
            // 重设actFlag
            this.setFlag.call(actBase);
            actBase.actFlag = actBase.flag;
            delete actBase.flag;
        }

        // 目前是写死返回对应的活动名称
        actBase.actLabels = ['', '限时折扣', '秒杀', '拼团'][this.actType];

        const spu = this.spuMap.get(params.spuId);
        const info = format.dataFormat(spu, 'spuId;pubPrice;invType;execVal;coupOverFlag=coupOverFlag=0');

        info.discountAmount = evalDiscountAmount(info.pubPrice, info.execVal);
        info.afterDiscountPrice = common.sub(info.pubPrice, info.discountAmount);

        return Object.assign(info, actBase);
    }
};

/**
 * 计算折扣价格
 * @description
 * 1. execType	优惠类型1=折扣,2=定价(特价),5=免减
 * 2. execNum	优惠类型对应的值 1时传（1-9表示对应折扣），2时直接传价钱， 5时传减免额度
 * 3. execExtraKind  额外附加的条件类型4=抹零,抹去角和分8=抹零,抹去分
 */
function evalDiscountAmount(pubPrice, execVal) {
    const { execType, execNum, execExtraKind } = execVal;
    let discountAmount = 0;
    switch (execType) {
        case 1:
            discountAmount = common.mul(pubPrice, (10 - execNum) / 10);
            break;
        case 2:
            discountAmount = common.sub(pubPrice, execNum);
            break;
        case 5:
            discountAmount = execNum;
            break;
        default:
            break;
    };
    return discountAmount.toFixed(execExtraKind == 4 ? 1 : 0);
};

/**
 * 重置活动商品中的活动开始/结束时间
 * @description 卖家查看不影响
 * @param {*} params
 */
function resetActDate(params) {
    const { activity, dimPeriod } = params;
    // 周期类型,0=无周期, 1=每天,2=每月,3=每周
    const dayPeroidType = dimPeriod.dayPeroidType;
    if (dayPeroidType > 0) {
        const dayValue = dimPeriod.dayValue[0];
        const day = ['', '', moment().date(dayValue).format('YYYY-MM-DD'), moment().day(dayValue).format('YYYY-MM-DD')][dayPeroidType];

        const timeArea = dimPeriod.timeAreas[0];

        activity.startDate = `${day} ${timeArea.begin}`.trim();
        activity.endDate = `${day} ${timeArea.end}`.trim();
    }
    return;
};

/**
 * 商品
 */
function MarketSpu() {
    /**  活动商品主键id （注：非商品spuid）s */
    this.id = 0;
    /**  spu id */
    this.spuId = '';
    /**  款号 */
    this.code = '';
    /**  名称 */
    this.name = '';
    /**  标题 */
    this.title = '';
    /**  类别 */
    this.classId = '';
    /**  品牌 */
    this.brandId = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  划线价 */
    this.markingPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  标签 */
    this.labels = {};
    /**  详情页面URL */
    this.url = '';
    // /**  状态  -4 卖家下架 -3 商品申请上架 -2 平台下架 -1 商品删除 0 商品下架(对应slh停用) 1商品上架(校验通过) 2商品待上架(校验不通过) 3买家隐藏-店内不可见(hideFlag=2)*/
    // this.flag = 1;
    /**  备注 */
    this.rem = '';
    this.slhId = '';

    /**  自定义库存总数 */
    this.invNum = 0;
    this.execVal = this.execVal;
    this.coupOverFlag = this.coupFlag;
    this.invType = -1;
    this.skuInvList = [];
};

module.exports = ActBase;
