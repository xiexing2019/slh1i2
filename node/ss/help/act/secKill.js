const ActBase = require('./base');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const moment = require('moment');

/**
 * 秒杀
 * ss_market_sec_kill
 */
class SecKill extends ActBase {
    constructor(params) {
        super();
        /**   是否开启预热 0-不行 1-可以 */
        this.preHeatFlag = 0;
        /**   秒杀样式 */
        this.styleKind = 0;
        /**  所属分支 */
        this.branchId = '';
        this.actType = 2
        // this.spuMap = new Map();
    }

    /**
     * 删除活动
     */
    async deleteAct() {
        const res = await ss.act.timeLimit.deleteRule({ id: this.id });
        console.log(`res=${JSON.stringify(res)}`);
        this.flag = -1;
    }

    /**
     * 新增秒杀
     * @param {object}  
     */
    async saveFullSecKill(params, dresList = []) {
        const saveFull = await ss.act.secKill.saveFull(params).then(res => res.result);
        console.log(`\n saveFull=${JSON.stringify(saveFull)}`);
        [...this.spuMap.values()].forEach(spu => {
            // spu.invNum = 0;
            spu.execVal = params.execVal;
            spu.coupOverFlag = params.coupFlag;
        });
        common.update(this, params);
        this.setFlag();
        !this.id && (this.id = saveFull.data.val);
        // !this.actType && (this.actType = 2);
        if (params.spuList.length) this.updateSpu(params.spuList);
        if (dresList.length) this.updateSpu(dresList);
        return saveFull;
    }

    setInformation(params) {
        common.update(this, params);
    }

    /**
     * 秒杀列表查询
     * @param {object}  
     */
    async getSecKillList(params) {
        const secKillInfo = await ss.act.secKill.getSecKillList({ flag: this.flag, ...params }).then(res => {
            console.log(`\n getSecKillList=${JSON.stringify(res)}`);
            return res.result.data.rows.find(obj => obj.id == this.id);
        });
        expect(secKillInfo).not.to.be.undefined;
        return secKillInfo;
    }

    /**
     * 买家端-秒杀活动标题列表
     * @param {object}  
     */
    async listForBuyer() {
        const secKillInfo = await ss.act.secKill.listForBuyer().then(res => res.result.data.rows.find(obj => obj.id == this.id));
        expect(secKillInfo).not.to.be.undefined;
        return secKillInfo;
    }

    /**
     * 买家端-秒杀活动商品列表查询
     * @param {object}  
     */
    async getSpuListForBuyer() {
        const secKillInfo = await ss.act.secKill.getSpuListForBuyer().then(res => res.result.data.rows.find(obj => obj.id == this.id));
        expect(secKillInfo).not.to.be.undefined;
        return secKillInfo;
    }

    /**
     * 设置活动状态
     * @description
     * 1. 根据本地时间判断 可能存在误差
     * 2. flag <=0都是无效的，>0都是有效的
     */
    setFlag() {
        //flag 1: 未开始 2:预热中 3: 进行中 0:已结束
        // preHeatFlag: 是否开启预热 1-开启 0-不开启
        const curTime = common.getCurrentTime();
        if (moment(curTime).isAfter(this.endDate)) {
            this.flag = 0;
        } else if (moment(curTime).isBefore(this.startDate) && this.preHeatFlag == 1 && moment(curTime).isAfter(moment().subtract(24, 'hours').format('YYYY-MM-DD HH:mm:ss'))) {
            this.flag = 2;
        } else if (moment(curTime).isBefore(this.startDate)) {// && this.preHeatFlag == 0
            this.flag = 1;
        } else {
            this.flag = 3;
        };
    }

    /**
     * 获取秒杀
     */
    getSecKill() {
        return Object.assign(
            this.getActBase(),
            {
                coupFlag: this.coupFlag,
                preHeatFlag: this.preHeatFlag,
                styleKind: this.styleKind,
                branchId: this.branchId,
            });
    };
};

function SkuInv() {
    /**  编号  */
    this.id = '';
    /**  活动编号，指向ss_market_base  */
    this.actId = '';
    /**  sku编号  */
    this.skuId = '';
    /**  spu编号  */
    this.spuId = '';
    /**  库存数量  */
    this.invNum = 0;
    /**  已拍数量  */
    this.buyNum = 0;
    /**  锁定库存 */
    this.lockNum = 0;
    /**  单元id  */
    this.unitId = '';
    // /**  创建人 */
    // this.createdBy = '';
    // /**  创建时间 */
    // this.createdDate = '';
    // /**  修改人 */
    // this.updatedBy = '';
    // /**  修改时间 */
    // this.updatedDate = '';
};

const secKillManage = module.exports = {};

/**
 * 初始化秒杀活动
 * @param {*} params 
 */
secKillManage.setupSecKill = function (params) {
    return new SecKill();
};

secKillManage.deleteOrDisableAllAct = async function () {
    for (const flag of [1, 2, 3]) {
        const secKillList = await ss.act.secKill.getSecKillList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`secKillList.length=${JSON.stringify(secKillList.length)}`);
        for (const secKill of secKillList) {
            const res = await ss.act.timeLimit.deleteRule({ id: secKill.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
        if (flag == 2) continue;
        const timeLimitList = await ss.act.timeLimit.getMarketList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`timeLimitList.length=${JSON.stringify(timeLimitList.length)}`);
        for (const act of timeLimitList) {
            const res = await ss.act.timeLimit.disableRule({ id: act.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
        const groupBuyList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows);
        console.log(`actList.length=${JSON.stringify(groupBuyList.length)}`);
        for (const act of groupBuyList) {
            const res = await ss.act.groupBuy.marketGroupBuyInvalid({ actId: act.id });
            console.log(`\n res=${JSON.stringify(res)}`);
        };
    };
};

/**
 * 拼接新建秒杀活动json
 * @param {object} params 
 * @param {string} params.startDate
 * @param {string} params.endDate
 * @param {Array} dresList
 * @param {string} params.num 每个sku的库存
 * @param {string} params.execVal 优惠策略json
 * @param {string} params.limitVal 限购规则
 * @param {string} params.invType -1= 保持现有库存 1=使用自定义库存
 */
secKillManage.getSecKillJson = function (dresList = [], params = {}) {
    const secKillJson = common.update({
        actName: `秒杀${common.getRandomStr(5)}`,//string	是	活动名称
        actLabels: ``,//string	是	活动标签
        startDate: common.getCurrentTime(),//string	是	开始时间
        endDate: `${common.getCurrentDate()} 23:59:59`,//string	是	结束时间
        coupFlag: 0, //int	是	可叠加适用优惠券 1-可以 0-不可以
        preHeatFlag: 0,//int	是	是否开启预热 1-开启 0-不开启
        // limitVal: limitVal,// { limitType: params.limitType || 0 },//json	是 限购规则  limitType： 0=不限购; 1=每种货品限购; (2=每种货品前若干件享受折扣)秒杀没有; limitNum： 对应的限购值；格式={"limitType":0,"limitNum":5}
        // execVal: execVal,//优惠策略
        spuList: []
    }, params);
    const execVal = common.update({	//json	是	优惠策略json
        execType: 1,//int	是	优惠类型1=折扣 2=定价(特价（秒杀没有）) 5=免减
        execNum: 1,//BigDecimal	是	优惠类型对应的值
        execExtraKind: 4 //Integer	是	额外附加的条件类型 4=抹零,抹去角和分  8=抹零,抹去分
    }, params.execVal);
    if (params.limitVal && params.limitVal.limitType && !params.limitVal.limitNum) Object.assign(params.limitVal, { limitNum: 2 });
    const limitVal = Object.assign({ limitType: 0 }, params.limitVal);//json	是 限购规则  limitType： 0=不限购; 1=每种货品限购; (2=每种货品前若干件享受折扣)秒杀没有; limitNum： 对应的限购值；格式={"limitType":0,"limitNum":5}
    Object.assign(secKillJson, { execVal: execVal, limitVal: limitVal });
    const hasNum = params.hasOwnProperty('num');
    dresList.forEach(dres => {
        // console.log(`\n dres=${JSON.stringify(dres)}`);
        const spuInfo = {	//list	是	活动商品信息
            spuId: dres.spu.id,//long	是	商品id
            execVal: execVal,//优惠策略
            coupOverFlag: secKillJson.coupFlag,//int	是	可叠加适用优惠券 1-可以 0-不可以
            // limitNum: params.limitNum || 2,//int	是	限购数量
            invType: -1,//int	是	-1= 保持现有库存 1=使用自定义库存
            // skuInvList: []	//list	是	sku秒杀库存 invType为1时必填
        };

        if (params.invType) Object.assign(spuInfo, { invType: params.invType });

        if (spuInfo.invType == 1) {
            Object.assign(spuInfo, { skuInvList: [] });
            dres.skus.forEach(sku => {
                const skuInfo = { skuId: sku.id, invNum: hasNum ? params.num : 2 };  //skuId,  num:库存
                spuInfo.skuInvList.push(skuInfo);
            });
        };
        secKillJson.spuList.push(spuInfo);
    });
    return secKillJson;
}