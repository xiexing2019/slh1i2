const ActBase = require('./base');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const common = require('../../../lib/common');
const moment = require('moment');
const format = require('../../../data/format');
const ssReq = require('../../help/ssReq');
const billManage = require('../../help/billManage');
const fs = require('fs');
const path = require('path');
const caps = require('../../../data/caps');
const basicInfoReqHandler = require('../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const basicJsonparam = require('../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../data/getBasicData');
const ssAccount = require('../../data/ssAccount');

//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];

const groupShareManager = module.exports = {};

class GroupShare extends ActBase {
    constructor() {
        super();
        this.actType = 6;
        /** '活动类别 1全局 2 活动 3 权益' */
        this.actClass = 1;
    }

    /**
     *  保存群接龙活动
     * @param {Array} dresList
     * @param {object} params
     * @param {object} params.actJson
     */
    async saveGroupShare(params = {}, dresList = []) {
        const result = await ss.act.groupShare.saveGroupShareAct(params).then(res => res.result);
        console.log(`保存群接龙活动Res= ${JSON.stringify(result)}`);
        common.update(this, params);
        !this.id && (this.id = result.data.val);
        this.setFlag();
        this.updateSpu(dresList);
        for (let spu of params.spuList) {
            common.update([...this.spuMap.values()][0], spu.actProps)
        }
        return this;
    }

    /**
     * 预览群接龙活动
     * @param {object} params
     * @param {string} params.slhSpuIds
     */
    async getActPrepareInfo(params = {}) {
        // const slhSpuIds = [...this.spuMap.values()].map(spu => { return spu.slhId });
        const data = await ss.act.groupShare.getActPrepareInfo({ slhSpuIds: params.slhSpuIds }).then(res => res.result.data)
        return data;
    }

    /**
     * 查询历史群接龙活动
     * @param {object} params
     */
    async findGroupShareList(params = {}) {
        const info = await ss.act.groupShare.findGroupShareList({ ...params }).then(res => res.result.data.rows.find(obj => obj.actId == this.id));
        expect(info, `群接龙列表查询不到活动id=${this.id}`).not.to.be.undefined;
        return info;
    }

    /**
     * 查询群接龙活动详情
     * @param {object} params
     * @param {string} params.sellerUnitId
     */
    async getGroupShareFullInfo(params = {}) {
        const result = await ss.act.groupShare.getGroupShareFullInfo({ actId: this.id, ...params }).then(res => res.result.data);
        expect(result).not.to.be.undefined;
        return result;
    }

    /**
     * 生成群接龙下单参数
     */
    async getPurJson(sellerInfo = {}) {
        await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId, openId: ssAccount.saas.openId, });
        const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...this.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
        common.update(dresDetail.spu, [...this.spuMap.values()][0]);
        dresDetail.skus.forEach(ele => { ele.pubPrice = [...this.spuMap.values()][0].pubPrice }); // 群接龙只会修改spu里的pubPrice,手动将sku里面的价格改了方便后面计算订单价格
        // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        let purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
        purJson.orders[0].main.sellerId = sellerInfo.tenantId;
        // purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
        Object.assign(purJson.orders[0].details[0], { actIds: this.id, "actName": '群接龙', "actType": 6 });
        console.log(`\npurJson=${JSON.stringify(purJson)}`);
        return purJson;
    }


    /**
     * 更新商品spu信息
     * @param {*} skuInfo
     */
    updateSpu(spuList) {
        console.log(`spuList=${JSON.stringify(spuList)}`);
        if (!spuList.hasOwnProperty('length')) throw new Error(`活动更新商品信息参数类型错误 ${typeof spuList}`);

        // 更新活动商品信息
        spuList.forEach(dres => {
            const spuId = dres.id || dres.spuId;
            const spuInfo = dres.spu;
            dres.skus.forEach(sku => sku.num = Number(sku.num) < 0 ? 0 : sku.num);
            spuInfo.skus = dres.skus;
            switch (this.execVal.execType) {
                case 1:
                    spuInfo.pubPrice = (spuInfo.pubPrice / 10) * this.execVal.execNum;
                    break;
                case 2:
                    spuInfo.pubPrice = this.execVal.execNum;
                    break;
                case 5:
                    spuInfo.pubPrice -= this.execVal.execNum;
                    break;
                default:
                    break;
            }
            this.spuMap.set(spuId, spuInfo);
        });

        // 更新自定义库存数量(秒杀活动)
        // [...this.spuMap.values()].forEach(spu => {
        //     spu.invNum = 0;
        //     spu.skuInvList.forEach(skuInv => {
        //         spu.invNum += skuInv.invNum;
        //     });
        // });
        return this;
    }

    /**
     * 下单后更新sku数量
     * @param {object} purJson
     */
    updateSku(purJson = {}) {
        [...this.spuMap.values()][0].skus.forEach(sku => {
            if (sku.id == purJson.orders[0].details[0].skuId) {
                sku.num -= purJson.orders[0].details[0].num
                sku.ssNum -= purJson.orders[0].details[0].num
            }
        })
    }

    //---------------------------------------------断言方法----------------------------------

    /**
     * 群接龙预览(默认填充信息)期望值
     * @param {object} dressFull  商品详情
     * @param {object} saasDressFull  saas商品详情
     */
    getActPrepareInfoExp(dressFull = {}, saasDressFull = {}) {
        let actDesc = '';
        let spec1NamesList = dressFull.spu.spec1Names.split(',');
        let spec2NamesList = dressFull.spu.spec2Names.split(',');
        for (let spec1 of spec1NamesList) {
            for (let spec2 of spec2NamesList) {
                actDesc += (spec2 + spec1 + ';');
            }
        }
        dressFull.spu.skus = dressFull.skus;
        dressFull.spu.skus.forEach(sku => sku.num = Number(sku.num) < 0 ? 0 : sku.num);
        dressFull.spu.pubPrice = dressFull.spu.pubPriceLow = dressFull.spu.pubPriceTop = saasDressFull.stdprice1;
        dressFull.spu.skus.forEach(sku => sku.pubPrice = saasDressFull.stdprice1);
        return {
            actName: dressFull.spu.name,
            spuInfo: [dressFull.spu],
            actDesc: actDesc.toString()
        }
    }

    /**
     * 获取群接龙活动列表期望值
     */
    getGroupShareListExp(params) {
        let json = {
            id: this.id,
            // ruleType: this.ruleType,
            actName: this.actName,
            actType: this.actType,
            actLabels: this.actLabels,
            actImgs: this.actImgs,
            startDate: this.startDate,
            endDate: this.endDate,
            spuInfo: [...this.spuMap.values()],
            dimPeriod: this.dimPeriod,
            // dimCustomer: this.dimCustomer,
            execVal: this.execVal,
            limitVal: this.limitVal,
            flag: this.flag,
            weights: this.weights,
            priority: this.priority,
            coupFlag: this.coupFlag,
            execDto: this.execVal,
        }
        if (params) {
            json.shareBills = [{
                actIds: this.id,
                details: [{
                    num: params.orders[0].details[0].num,
                    skuId: params.orders[0].details[0].skuId,
                    spuId: params.orders[0].details[0].spuId
                }]
            }]
        }
        return json;
    };
}

groupShareManager.setupGroupShare = function () {
    return new GroupShare();
};

/**
 * 拼接群接龙json
 */
groupShareManager.getGroupShareJson = function (dresList = [], opts = {}) {
    const json = Object.assign({
        actName: `群接龙${common.getRandomStr(5)}`,// 活动名称
        actLabels: `群接龙`,// 活动标签
        startDate: `${common.getCurrentDate()} 00:00:00`,//common.getCurrentTime(),// 开始时间
        endDate: `${common.getCurrentDate()} 23:59:59`,// 结束时间
        coupFlag: 0,// 可叠加适用优惠券 1-可以 0-不可以
    }, opts);
    /**
     * 限购规则
     * limitType 是	0=不限购; 1=每种货品限购; 2=每种货品前若干件享受折扣
     * limitNum  否	对应的限购值
     */
    json.limitVal = { limitType: opts.limitType || 0 };
    opts.hasOwnProperty('limitNum') && (json.limitVal.limitNum = opts.limitNum);
    /**
     * 优惠策略
     */
    const execVal = common.update({
        execType: 2, // 优惠类型1=折扣,2=定价(特价),5=免减
        execNum: common.getRandomNum(1, 20), // 优惠类型对应的值 1时传（1-9表示对应折扣），2时直接传价钱， 5时传减免额度
        // execExtraKind: 4, // 额外附加的条件类型 4=抹零,抹去角和分  8=抹零,抹去分
    }, opts.execVal);
    json.execVal = execVal;
    // 活动商品信息
    json.spuList = dresList.map(dres => {
        return {
            spuId: dres.id,
            execVal: execVal,
            actProps: {
                name: `name${common.getRandomStr(5)}`,
                code: `code${common.getRandomStr(5)}`,
                className: `类别${common.getRandomStr(5)}`,
            }
        }
    });
    // 活动图片
    let video = docData.video.dres[0];
    video.typeId = 3;
    const images = common.randomSort(docData.image.dres).slice(0, 2).map(obj => { return { docId: obj.docId, typeId: 1 } });
    const actImgs = [video, ...images];
    json.actImgs = actImgs;
    return json;
};

groupShareManager.saasLogin = async function () {
    await caps.updateEnvByName({ name: ssAccount.saas.ename });
    caps.epid = ssAccount.saas.epid;
    await common.loginDo({ epid: ssAccount.saas.epid });

    await getBasticData.getBasicID();
};

/**
 * 新增款号和库存
 */
groupShareManager.addNewStyle = async function () {
    await caps.updateEnvByName({ name: ssAccount.saas.ename });
    caps.epid = ssAccount.saas.epid;
    await common.loginDo({ epid: ssAccount.saas.epid });
    await getBasticData.getBasicID();

    styleJson = basicJsonparam.addGoodJson({ fileid: ssAccount.saas.fileid });
    newStyle = await basicInfoReqHandler.editStyle({ jsonparam: styleJson });

    await common.loginDo({ epid: ssAccount.saas.epid });
    let json = basicJsonparam.purchaseJson();
    json.details.forEach(ele => {
        ele.styleid = newStyle.result.val;
        ele.matCode = styleJson.code;
    });
    await common.editBilling(json);

    return newStyle;
}