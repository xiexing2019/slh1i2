const ActBase = require('./base');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const common = require('../../../lib/common');
const moment = require('moment');
const format = require('../../../data/format');
/**
 * 拼团
 * ss_market_group_buy
 */
class GroupBuy extends ActBase {
    constructor(params) {
        super();
        /**  编号，和ss_market_base主键一对一 */
        this.actType = 3;
        this.actLabel = '';
        /**  拼团人数 */
        this.groupPersonNum = 0;
        /**  有效期(单位:小时) */
        this.effectiveTime = 0;
        /**  凑团开关（0: 关闭 1：开启） */
        this.clusterFlag = 0;
        /**  自助拼团开关（0：关闭，1开启） */
        this.autoClusterFlag = 0;
        /**  预热开关（0：关闭，1开启） */
        this.preHeatFlag = 0;
        /**  同步到分销商（0：关闭，1开启） */
        this.syncDistributorFlag = 0;
        /**  优惠券叠加开关（0：关闭，1开启） */
        this.couponOverFlag = 0;
        /**  所属单元 */
        // this.unitId = '';
        /**  限购数量 */
        this.limitedNum = '';
        /**  活动价格 */
        this.activityPrice = '';
        /** 拼团活动数据展示 */
        this.groupBuyStatisticsMap = new Map();
        /** 买家开团信息 */
        this.groupOrderMap = new Map();
    }

    /**
     * 拼团活动查询期望
     */
    getGroupBuyExp() {
        return {
            id: this.id,
            actName: this.actName,
            startDate: this.startDate,
            endDate: this.endDate,
            flag: this.flag,
            // dresSpuId: '',    // spuId
            activityPrice: this.activityPrice,
            effectiveTime: this.effectiveTime,
            groupPersonNum: this.groupPersonNum,
            clusterFlag: this.clusterFlag,
            autoClusterFlag: this.autoClusterFlag,
            preHeatFlag: this.preHeatFlag,
            syncDistributorFlag: this.syncDistributorFlag,
            couponOverFlag: this.couponOverFlag,  //	优惠券叠加开关（0：关闭，1开启） 未返回
            limitVal: this.limitedNum ? { limitedNum: this.limitedNum, limitType: 1 } : { limitType: 0 },
            execVal: { execNum: [...this.spuMap.values()][0].pubPrice - this.activityPrice, execType: 5 },
            dresSpuId: [...this.spuMap.values()][0].spuId,
            spus: [...this.spuMap.values()][0].spus,
        }
    }

    /**
     * 买家查询拼团中订单列表期望值
     * @param {object} params 创建拼团活动的json
     */
    getGroupBuyOrderResultListExp(params = {}) {
        return {
            clusterFlag: params.clusterFlag,
            coupFlag: params.couponOverFlag,
            preHeatFlag: params.preHeatFlag,
            buySuccessNum: [...this.groupBuyStatisticsMap.values()][0].totalFailSalesNum,
            joinGroupBuyPersonNum: [...this.groupBuyStatisticsMap.values()][0].totalCustNum + [...this.groupBuyStatisticsMap.values()][0].totalNewCustNum,
            // joinGroupBuyPersonNum: [...this.groupBuyStatisticsMap.values()][0].totalCustNum,
            groupBuyOrderResultList: {
                actId: this.id,
                // startDate: this.startDate,
                // endDate: moment(this.startDate).add(this.effectiveTime, 'h').format('YYYY-MM-DD HH:mm:ss'),
                // endDate: this.endDate,
                flag: 0,
                groupJoinNum: 1,
                groupPersonNum: params.groupPersonNum,
                items: [{
                    actId: this.id,
                    flag: 0,
                    leaderFlag: 1,
                }]
            }
        }
    }

    /**
     * 买家查询拼团中订单详情期望值
     * @param {*} params
     */
    getGroupBuyOrderResultDetailExp() {
        return {
            id: [...this.groupOrderMap.values()][0].id,
            flag: [...this.groupOrderMap.values()][0].flag,
            startDate: [...this.groupOrderMap.values()][0].startDate,
            endDate: [...this.groupOrderMap.values()][0].endDate,
            actId: this.id,
            coupFlag: [...this.groupOrderMap.values()][0].coupFlag,
            actFlag: this.flag,
            groupJoinNum: [...this.groupOrderMap.values()][0].groupJoinNum,
            dresSpuDTO: {
                id: [...this.spuMap.values()][0].id,
                actPrice: this.activityPrice,
                code: [...this.spuMap.values()][0].code,
                name: [...this.spuMap.values()][0].name,
                oldPrice: [...this.spuMap.values()][0].spus[0].price,
                docContent: [...this.spuMap.values()][0].spus[0].docContent
            },
            items: [...this.groupOrderMap.values()][0].groupBuyOrderResultList[0].items
        }
    }


    /**
     * 更新拼团活动数据
     * @param {object} params
     * @param {string} params.static 是否新客户 new
     * @param {ocject} params.orderRes 下单返回
     */
    updateGroupBuyStatistics1(params = {}) {
        const statistics = this.groupBuyStatisticsMap.has(this.id) ? this.groupBuyStatisticsMap.get(this.id) : new GroupBuyStatistics();
        if (params.static == 'new') {
            statistics.totalNewCustNum += 1;
        } else {
            statistics.totalCustNum += 1;
        }
        statistics.totalFailOrderNum += 1;
        statistics.totalFailNum += 1;
        statistics.totalTurnOver += params.params.jsonParam.orders[0].main.money;
        statistics.totalFailSalesNum += params.params.jsonParam.orders[0].main.totalNum;
        statistics.actId = this.id;
        this.groupBuyStatisticsMap.set(this.id, statistics);
        return this;
    }
    /**
     * 拼团成功后 更新拼团活动数据
     */
    updateGroupBuyStatistics2() {
        const statistics = this.groupBuyStatisticsMap.has(this.id) ? this.groupBuyStatisticsMap.get(this.id) : new GroupBuyStatistics();
        statistics.totalSuccessSalesMoney = statistics.totalTurnOver;
        statistics.totalSuccessNum = statistics.totalFailNum;
        statistics.totalSuccessSalesNum = statistics.totalFailSalesNum;
        statistics.totalSuccessOrderNum = statistics.totalFailOrderNum;
        statistics.totalFailNum -= statistics.totalFailNum;
        statistics.totalFailSalesNum -= statistics.totalFailSalesNum;
        statistics.totalFailOrderNum -= statistics.totalFailOrderNum;
        this.groupBuyStatisticsMap.set(this.id, statistics);
        return this;
    }

    /**
     * B用户下单拼团成功后 更新拼团活动数据
     * @param {object} params
     * @param {string} params.static 是否新客户 new
     * @param {ocject} params.orderRes 下单返回
     */
    updateGroupBuyStatisticsB(params = {}) {
        const statistics = this.groupBuyStatisticsMap.has(this.id) ? this.groupBuyStatisticsMap.get(this.id) : new GroupBuyStatistics();
        statistics.totalNewCustNum += 1;
        statistics.totalFailOrderNum += 1;
        // statistics.totalFailNum += 1;
        statistics.totalTurnOver += params.params.jsonParam.orders[0].main.money;
        statistics.totalFailSalesNum += params.params.jsonParam.orders[0].main.totalNum;
        // statistics.actId = this.id;

        statistics.totalSuccessSalesMoney = statistics.totalTurnOver;
        statistics.totalSuccessNum = statistics.totalFailNum;
        statistics.totalSuccessSalesNum = statistics.totalFailSalesNum;
        statistics.totalSuccessOrderNum = statistics.totalFailOrderNum;

        statistics.totalFailNum -= statistics.totalFailNum;
        statistics.totalFailSalesNum -= statistics.totalFailSalesNum;
        statistics.totalFailOrderNum -= statistics.totalFailOrderNum;

        this.groupBuyStatisticsMap.set(this.id, statistics);
        return this;
    }

    /**
     * 更新 拼团订单信息
     * @param {object} orderRunListRes
     */
    updateGroupBuyOrder(orderRunListRes = {}) {
        const groupBuyOrderId = orderRunListRes.data.id;
        let groupBuyOrdDetail = this.groupOrderMap.has(groupBuyOrderId) ? this.groupOrderMap.get(groupBuyOrderId) : new GroupOrder();
        common.update(groupBuyOrdDetail, orderRunListRes.data);
        common.update(groupBuyOrdDetail, orderRunListRes.data.groupBuyOrderResultList[0]);
        // common.update(groupBuyOrdDetail, orderRunListRes.data.groupBuyOrderResultList[0].items[0], [id]);
        this.groupOrderMap.set(groupBuyOrderId, groupBuyOrdDetail);
        return this;
    }

    /**
     * 更新spu信息
     */
    updateSpu(dres = {}) {
        const spuId = dres.id || dres.spuId;
        const spu = this.spuMap.has(spuId) ? this.spuMap.get(spuId) : new MarketSpu();
        common.update(spu, dres.spu || dres);
        spu.id = spuId;
        spu.spuId = spuId;
        spu.limitNum = this.limitedNum;
        spu.execVal = { execNum: dres.spu.pubPrice - this.activityPrice, execType: 5 };
        let stockNum = 0;
        for (let obj of dres.skus) { stockNum += obj.ssNum };
        spu.spus = [{
            docContent: dres.spu.docContent,
            id: dres.spu.id,
            name: dres.spu.name,
            price: dres.spu.pubPrice,
            stockNum: stockNum,
            title: dres.spu.title
        }];
        this.spuMap.set(spuId, spu);
        // console.log(this.spuMap.get(spuId).spus);
        return this;
    }

    /**
     * 拼接商品详情的活动信息(一个商品)
     * @description dres.activity
     * @param {object} params
     * @param {string} params.spuId
     */
    getDresActInfo(params) {
        if (!this.spuMap.has(params.spuId)) {
            throw new Error(`actId=${this.id},不包含商品${param.spuId}，请检查`)
        }
        // 活动基础信息
        const mapFile = 'actName;actLabels=actLabel;actFlag=flag;actId=id;actType;afterDiscountPrice=activityPrice;startDate;endDate;actImgs;coupOverFlag=couponOverFlag;limitNum=limitedNum';
        const actBase = format.dataFormat(this, mapFile);

        const spu = this.spuMap.get(params.spuId);
        const info = format.dataFormat(spu, 'spuId;pubPrice;invType');

        // const execNum = spu.pubPrice - this.activityPrice;
        // info.execVal = `{"execNum": ${execNum}, "execType": 5}`;
        info.execVal = `{"execNum": ${this.activityPrice}, "execType": 2}`;

        return Object.assign(info, actBase);
    }

    /**
     * 新增拼团
     * @param {object}
     */
    async addOrUpdateMarketGroupBuy(params, dres = {}) {
        const result = await ss.act.groupBuy.addOrUpdateMarketGroupBuy(params).then(res => res.result);
        console.log(`\n保存拼团活动=${JSON.stringify(result)}`);
        common.update(this, params);
        // await common.delay(3000);
        !this.id && (this.id = result.data.val);
        console.log(`this.id = ${this.id}`);

        this.setFlag();
        // 设置商品信息
        this.updateSpu(dres);
    }

    /**
     * 查询拼团活动列表
     * @param {object}
     */
    async marketGroupBuyList(params) {
        const info = await ss.act.groupBuy.marketGroupBuyList({ flag: this.flag, ...params })
            .then(res => res.result.data.rows.find(obj => obj.id == this.id));
        expect(info).not.to.be.undefined;
        return info;
    }

    /**
     * 查询拼团活动详情
     * @param {object}
     */
    async marketGroupBuyDetail() {
        const detail = await ss.act.groupBuy.marketGroupBuyDetail({ actId: this.id }).then(res => res.result.data);
        expect(detail).not.to.be.undefined;
        return detail;
    }

    /**
     * 拼团失效
     * @param {object} params
     * @param {object} params.status
     */
    async marketGroupBuyInvalid(params = {}) {
        await ss.act.groupBuy.marketGroupBuyInvalid({ actId: this.id });
        this.flag = -2;
        this.spuMap.forEach((value) => value.flag = -2);
        if (params.status == 'fail') {
            return this;
        }
        this.updateGroupBuyStatistics2();
        return this;
    }

    /**
     * 拼团删除
     * @param {object}
     */
    async deleteMarketGroupBuyById() {
        // console.log(this.id);
        await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: this.id });
        this.flag = -1;
        this.spuMap.forEach((value) => value.flag = -1);
        return this;
    }

    /**
     * 卖家查询拼团订单列表
     * @param {object} params
     * @param {number} params.flag  0 拼团中 1拼团成功 2拼团失败
     */
    async marketGroupBuyOrderList(params) {
        const qlRes = await ss.act.groupBuy.marketGroupBuyOrderList({ actId: this.id, ...params }).then(res => res.result);
        return qlRes;
    }

    /**
     * 拼团活动数据展示
     * @param {object}
     */
    async marketGroupBuyStatistics() {
        const data = await ss.act.groupBuy.marketGroupBuyStatistics({ actId: this.id }).then(res => res.result);
        return data;
    }

    /**
     * 新建或者加入团购下单
     * @param {object} params
     * @param {object} params.groupBuyOrder 拼团活动数据
     * @param {object} params.platCoupons 平台券
     * @param {list} params.orders 订货数据。list结构
     */
    async groupBuyOrderItem(params = {}) {
        const purRes = await ss.act.groupBuy.groupBuyOrderItem(params);
        // console.log(`创建采购单:${JSON.stringify(purRes.result.data.rows[0])}`);
        console.log(`创建采购单:${JSON.stringify(purRes.result)}`);
        let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
        while (checkPurBillFlag.purBillFlag != 3) {
            if (i > 20) throw new Error(`判断订单状态已超过20次`);
            await common.delay(1000);
            checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
            console.log(`\n 第${i}次：checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
            i++;
            if (checkPurBillFlag.failedMsg && (checkPurBillFlag.failedMsg.indexOf('超出了最大限购数') != -1)) break;
        }
        Object.assign(purRes.result.data.rows[0], checkPurBillFlag);
        return purRes;
    }

    /**
     * 创建团购支付
     * @param {object} params
     */
    async creatPay(params = {}) {
        const payRes = await sp.spTrade.createPay(params.payParams);
        this.updateGroupBuyStatistics1(params.orderRes);
        return payRes;
    }

    /**
     * 买家查询拼团中订单列表
     * @param {Object} params
     * @param {string} params.sellerId 卖家id
     */
    async findGroupBuyOrderRunningList(params = {}) {
        const data = await ss.act.groupBuy.findGroupBuyOrderRunningList({ actId: this.id, ...params })
            .then(res => {
                console.log(`查询订单参数 = ${JSON.stringify(res)}`);
                return res.result
            });
        return data;
    }

    /**
     * 买家查询拼团中订单详情
     * @param {object} params
     * @param {string} params.actOrderId 拼团团购id
     * @param {string} params.sellerId 卖家id
     */
    async findGroupBuyOrderDetail(params = {}) {
        const data = await ss.act.groupBuy.findGroupBuyOrderDetail(params).then(res => res.result);
        return data;
    }

};


const groupBuyManager = module.exports = {};

/**
 * 初始化拼团活动
 * @param {object} params
 */
groupBuyManager.setupGroupBuy = function (params) {
    return new GroupBuy(params);
};

/**
 * 拼接拼团活动json
 * @param {object} opts
 * @param {number} opts.dresSpuId // 货品id (必传)
 * @param {number} opts.id  // 活动id 修改时上传
 * @param {number} opts.limitedNum // 限购数量 0代表不限购  默认 0
 * @param {number} opts.groupPersonNum // 拼团人数 默认10
 * @param {number} opts.clusterFlag  // 凑团开关（0: 关闭 1：开启 默认 1）
 * @param {number} opts.autoClusterFlag // 自助拼团开关（0：关闭，1开启 默认 1）
 * @param {number} opts.preHeatFlag // 预热开关（0：关闭，1开启 默认 1）
 */
groupBuyManager.getGroupBuyJson = function (dres = {}, opts = {}) {
    const groupBuyJson = Object.assign({
        actName: `拼团${common.getRandomStr(5)}`, // string    是  活动名称
        actLabel: `拼团`, // string   否  标签
        startDate: moment().subtract(10, 'minutes').format('YYYY-MM-DD HH:mm:ss'), // string    是  活动开始时间
        // startDate: common.getCurrentTime(), // string    是  活动开始时间
        endDate: `${common.getCurrentDate()} 23:59:59`, // string    是  活动结束时间
        effectiveTime: 1,     // int   是  有效期（小时）
        activityPrice: 9.99,   // decimal   是   活动价格
        limitedNum: 0,   // int   是   限购数量 0代表不限购
        groupPersonNum: 2, // int   是    拼团人数
        clusterFlag: 1,  // int    是  	凑团开关（0: 关闭 1：开启）
        autoClusterFlag: 1, // int    是  自助拼团开关（0：关闭，1开启）
        preHeatFlag: 1, // int    是  预热开关（0：关闭，1开启）
        syncDistributorFlag: 0, // int    是  	同步到分销商（0：关闭，1开启)
        couponOverFlag: 0, // int  是  	优惠券叠加开关（0：关闭，1开启）
    }, opts);
    groupBuyJson.dresSpuId = dres.id;
    return groupBuyJson;
};

/**
 * 拼接拼团活动json
 * @param {object} params
 * @param {number} params.dresSpuId // 货品id (必传)
 * @param {number} params.id  // 活动id 修改时上传
 * @param {number} params.limitedNum // 限购数量 0代表不限购  默认 0
 * @param {number} params.groupPersonNum // 拼团人数 默认10
 * @param {number} params.clusterFlag  // 凑团开关（0: 关闭 1：开启 默认 1）
 * @param {number} params.autoClusterFlag // 自助拼团开关（0：关闭，1开启 默认 1）
 * @param {number} params.preHeatFlag // 预热开关（0：关闭，1开启 默认 1）
 */
groupBuyManager.updateGroupBuyJson = function (params = {}, dres = {}) {
    const updateGroupBuyJson = Object.assign({
        actName: `拼团${common.getRandomStr(5)}`, // string    是  活动名称
        actLabel: `拼团`, // string   否  标签
        // startDate: common.getCurrentTime(), // string    是  活动开始时间
        startDate: moment().subtract(1, 'minutes').format('YYYY-MM-DD HH:mm:ss'), // string    是  活动开始时间
        endDate: `${common.getCurrentDate()} 23:59:59`, // string    是  活动结束时间
        effectiveTime: 1,     // int   是  有效期（小时）
        activityPrice: 1.99,   // decimal   是   活动价格
        limitedNum: 1,   // int   是   限购数量 0代表不限购
        groupPersonNum: 5, // int   是    拼团人数
        clusterFlag: 0,  // int    是  	凑团开关（0: 关闭 1：开启）
        autoClusterFlag: 0, // int    是  自助拼团开关（0：关闭，1开启）
        preHeatFlag: 0, // int    是  预热开关（0：关闭，1开启）
        syncDistributorFlag: 1, // int    是  	同步到分销商（0：关闭，1开启)
        couponOverFlag: 1, // int  是  	优惠券叠加开关（0：关闭，1开启）
    }, params);
    updateGroupBuyJson.dresSpuId = dres.id;
    return updateGroupBuyJson;
};


/**
 * 商品
 */
function MarketSpu() {
    /**  活动商品主键id （注：非商品spuid）s */
    this.id = '';
    /**  spu id */
    this.spuId = '';
    /**  款号 */
    this.code = '';
    /**  名称 */
    this.name = '';
    /**  标题 */
    this.title = '';
    /**  类别 */
    this.classId = '';
    /**  品牌 */
    this.brandId = 0;
    /**  吊牌价 */
    this.tagPrice = 0;
    /**  发布价 */
    this.pubPrice = 0;
    /**  划线价 */
    this.markingPrice = 0;
    /**  价格1 */
    this.price1 = 0;
    /**  价格2 */
    this.price2 = 0;
    /**  价格3 */
    this.price3 = 0;
    /**  价格4 */
    this.price4 = 0;
    /**  价格5 */
    this.price5 = 0;
    /**  标签 */
    this.labels = {};
    /**  详情页面URL */
    this.url = '';
    // /**  状态  -4 卖家下架 -3 商品申请上架 -2 平台下架 -1 商品删除 0 商品下架(对应slh停用) 1商品上架(校验通过) 2商品待上架(校验不通过) 3买家隐藏-店内不可见(hideFlag=2)*/
    // this.flag = 1;
    /**  备注 */
    this.rem = '';
};

/**
 * 买家开团信息（小团信息）
 */
function GroupOrder() {
    /** =拼团团购记录表; */
    /**  主键id */
    this.id = '';
    /**  拼团活动id */
    this.actId = '';
    /**  团购id */
    this.actOrderId = '';
    /**  采购订单id */
    this.purBillId = '';
    /**  销售订单id  */
    this.salesBillId = '';
    /**  团购状态 -1进行中 0拼团中 1拼团成功 2拼团失败 */
    this.flag = 0;
    /**  团长标识 0：普通用户 1 团长 */
    this.leaderFlag = 0;
    /**  用户id */
    this.userId = '';
    /**  用户名称 */
    this.userName = '';
    /**  拼团人数 */
    this.groupPersonNum = 0;
    /**  已加入人数 */
    this.groupJoinNum = 0;
    /**  拼团开始时间 */
    this.startDate = '';
    /**  拼团结束时间 */
    this.endDate = '';
    /** 优惠券叠加开关 */
    this.coupFlag = '';
    /** groupBuyOrderResultList */
    this.groupBuyOrderResultList = '';
};

/**
 * 拼团活动数据展示
 */
function GroupBuyStatistics() {
    /** 参团老客户 */
    this.totalCustNum = 0;
    /** 成团数 */
    this.totalSuccessNum = 0;
    /** 成团金额 */
    this.totalSuccessSalesMoney = 0;
    /** 未成团买家数 */
    this.totalFailOrderNum = 0;
    /** 成团商品数 */
    this.totalSuccessSalesNum = 0;
    /** 未成团数 */
    this.totalFailNum = 0;
    /** 支付金额 */
    this.totalTurnOver = 0;
    /** 未成团商品数 */
    this.totalFailSalesNum = 0;
    /** 拼团id */
    this.actId = 0;
    /** 参团新客户 */
    this.totalNewCustNum = 0;
    /** 成团买家数 */
    this.totalSuccessOrderNum = 0;
};