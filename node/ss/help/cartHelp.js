let cartHelp = module.exports = {};
let basicJson = require('./basicJson');
let common = require('../../lib/common');


/**批量新增购物车的预期结果构造实现
 * @param {Array} cartListRows 初始值中的 rows数组
 * @param {Array} saveCartInBatchsParam 批量添加购物车传的参数
 * **/

cartHelp.expectResult = (cartListRows, saveCartInBatchsParam) => {
    let hasTenantId = cartListRows.find(element => element.trader.tenantId == saveCartInBatchsParam.trader.tenantId);
    if (hasTenantId == undefined) {
        cartListRows.push(basicJson.inBatchJsonFormat(saveCartInBatchsParam));   //要把添加的都push进去
    } else {
        for (let i = 0; i < saveCartInBatchsParam.carts.length; i++) {
            let hasSkuId = hasTenantId.carts.find(element => element.skuId == saveCartInBatchsParam.carts[i].skuId)
            if (hasSkuId == undefined) {
                hasTenantId.carts.push(saveCartInBatchsParam.carts[i]);
            } else {
                hasSkuId.skuNum = common.add(hasSkuId.skuNum, saveCartInBatchsParam.carts[i].skuNum);
                hasSkuId.skuPrice = saveCartInBatchsParam.carts[i].skuPrice;
            };
        };
    };
};
/****
 * 添加货品的预期结果的方法
 * @param cartListRows 初始值的rows数组
 * @param params 添加购物车传递的参数
*/


cartHelp.addCartexpectResult = (cartListRows, params) => {
    if (cartListRows.length == 0) {
        let rowsDat = {};
        let catArr = [];
        catArr[0] = params.cart;
        rowsDat.carts = catArr;
        rowsDat.trader = params.trader;
        cartListRows[0] = rowsDat
    } else {
        let hasTenantId = cartListRows.find(element => element.trader.tenantId == params.trader.tenantId);
        if (hasTenantId == undefined) {
            cartListRows.push(params);   //要把添加的都push进去
        } else {
            let hasSkuId = hasTenantId.carts.find(element => element.skuId == params.cart.skuId);
            if (hasSkuId == undefined) {
                hasTenantId.carts.push(params.cart);
            } else {
                hasSkuId.skuNum = common.add(hasSkuId.skuNum, params.cart.skuNum);
                hasSkuId.skuPrice = params.cart.skuPrice;
                hasSkuId.invNum = params.cart.invNum;
            };
        };
    };
};
/***
 * 删除货品的预期结果
 * @param cartListRows 原始数据rows数组
 * @param params 新增购物车传的json
 * **/

cartHelp.deleteCartExpect = (cartListRows, params) => {
    let index = _.findIndex(cartListRows, element => element.trader.tenantId == params.trader.tenantId);
    if (index > -1) {
        _.remove(cartListRows[index].carts, element => element.skuId == params.cart.skuId);
    }
    return cartListRows;
};


/**修改购物车断言
 * @param cartListRows 初始查询集合中的rows数组
 * @param saveResult 添加购物车返回的结果
 * @param updateResult 修改购物车返回的json
 * 
 * **/
cartHelp.updateCartExpect = (cartListRows, saveResult, updateResult) => {
    let tenantId = cartListRows.find(element => element.trader.tenantId == saveResult.params.jsonParam.trader.tenantId);
    if (tenantId == undefined) {

    } else {
        let rows = tenantId.carts.find(element => element.id == saveResult.result.data.val);
        if (rows == undefined) {

        } else {
            rows.skuNum = updateResult.params.jsonParam.skuNum;
            rows.skuPrice = updateResult.params.jsonParam.skuPrice;
        };
    }
};
