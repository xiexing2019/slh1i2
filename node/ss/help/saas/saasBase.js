const common = require('../../../lib/common');
const ssReq = require('../ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const slh1 = require('../../../reqHandler/slh1');
const ssAccount = require('../../data/ssAccount');
const getBasticData = require('../../../data/getBasicData');
const caps = require('../../../data/caps');
const format = require('../../../data/format');
const ssConfigParam = require('../configParamManager');
const dresManage = require('../dresManage');
const slh1BasicJsonparam = require('../../../slh1/help/basiceJsonparam');

class saasBase {
    constructor() {
        this.saasdress = {};
        this.dress = {};
        this.sellerInfo = {};
    }

    /**
     * saas登录
     */
    async slh1Login() {
        await caps.updateEnvByName({ name: ssAccount.saas.ename });
        caps.epid = ssAccount.saas.epid;
        await common.loginDo({ epid: ssAccount.saas.epid });
    };

    /**
     * 新增商品同步到商城,并且给商品添加库存
     * @param {object} params
     * @param {string} params.pk  否  slhId
     * @param {bool} params.isJunSe 否  true 是否均色均码
     */
    async addNewDress(params = {}) {
        await getBasticData.getBasicID();
        let styleJson = slh1BasicJsonparam.addGoodJson({ fileid: ssAccount.saas.fileid });
        params.isJunSe && Object.assign(styleJson, { colorids: `${BASICDATA.coloridJunSe}`, sizeids: `${BASICDATA.sizeidJunMa}` });
        params.pk && (styleJson.pk = params.pk);
        const newStyle = await slh1.slh1MetManager.saveGoodFull({ jsonparam: styleJson });
        this.saasdress = newStyle;
        // 增加库存
        params.isJunSe && this.purchase(newStyle, slh1BasicJsonparam.junPurchaseJson());
        //判断同步是否成功
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        let i = 0, qlRes;
        while (qlRes == undefined) {
            await common.delay(3000);
            qlRes = await sp.spdresb.findSellerSpuList({ nameLike: newStyle.param.code })
                .then(res => res.result.data.rows.find(data => data.slhId == newStyle.result.val));
            i += 1;
            console.log(`商品列表=`, JSON.stringify(qlRes));
            if (i > 10) throw new Error('新增商品30秒还未同步到商城成功....');
        }
        const dressInfo = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result.data);
        if (params.pk) {
            this.dress.spu.name = this.dress.spu.namePy = this.dress.spu.title = styleJson.name;
            this.dress.spu.code = styleJson.code;
        } else {
            this.dress = dressInfo;
        }
        console.log(`saas新增商品详情=`, JSON.stringify(this.dress));
    }

    /**
     * saas 均色均码做采购入库，增加库存
     * @param {object} params saas新增货品返回值
     */
    async purchase(params = {}, json) {
        json = json || slh1BasicJsonparam.purchaseJson();
        json.details.forEach(ele => {
            ele.styleid = params.result.val;
            ele.matCode = params.param.code;
        });

        await common.editBilling(json);
    }

    /**
     * saas销售开单 线下订单
     * @param {object} params 销售开单json
     */
    async addSalesOrder(salesJson) {
        let billRes = await common.editBilling(salesJson);
        let salesListRes, i = 0;
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId });
        while (salesListRes == undefined) {
            i += 1;
            await common.delay(2000);
            const salesList = await sp.spTrade.purFindBills({ orderBy: 'proTime', orderByDesc: true, statusType: 0, srcType: 4 }).then(res => res.result.data.rows);
            salesListRes = salesList.find(obj => JSON.parse(obj.bill.extProps).slhBatchNum == billRes.result.billno);
            if (i > 10) throw new Error('线下订单同步失败。。。');
        }
        return salesListRes;
    }

    /**
     * saas作废销售单
     * @param {object} params
     * @param {string} params.pk saas销售单id
     * @param {boolean} succeed 是否判断作废成功 默认true
     */
    async cancelSaleoutBill(params = {}, succeed = true) {
        let res = await common.callInterface('cs-cancel-saleout-bill', { 'action': 'edit', 'pk': params.pk, });
        succeed && expect(res, `作废单据失败,reason:${JSON.stringify(res)}`).to.includes({ 'val': 'ok', });
        return res;
    };


    /**
     * 价格管理
     * @param {object} params
     * @param {string} params.shopId slh店铺id
     */
    async getPriceSetting(params = {}) {
        let priceList = await slh1.slh1SSManager.getPriceSetting({ shopId: ssAccount.saas.saasShopId, ...params });
        return priceList.result.data;
    }


    /**
     * 开启或关闭个性化参数
     * @param {object} params
     * @param {stirng} params.code 个性化参数code
     * @param {string} params.val 1:开启 0:关闭
     * @param {string} params.region 作用域 spg/spb
     */
    async setConfigParam(params = {}) {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        if (params.region == 'spg') {
            const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: this.sellerInfo.tenantId, code: params.code });
            const res = await ssParam.updateParam({ val: params.val });
        } else {
            const ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: this.sellerInfo.tenantId, code: params.code });
            const res = await ssParam.updateParam({ val: params.val });
        }
    }

    /**
     * 拆色拆码
     */
    async updateSpuForSeller() {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        await dresManage.prePrepare();
        await sp.spdresb.updateSpuForSeller({
            id: this.dress.spu.id,
            sizeFlag: 1, sizeSplit: 'M,L,XL',
            colorFlag: 1, colorSplit: '白色,黑色'
        });
        this.dress.spu.colorSplitFlag = this.dress.spu.sizeSplitFlag = 2; // 尺码拆分标志 0-不可 1-可拆 2-拆过
        this.dress.spu.specSplitFlag = 1; // 拆分标志
        const dressInfo = await sp.spdresb.getFullById({ id: this.dress.spu.id }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(this.dress, dressInfo, ['ver'], '拆色拆码后，不能修改原商品属性');
        this.dress = dressInfo;
    }

    /**
     * 设置拆色拆码库存
     * @param {object} params
     * @param {string} params.stockNum top:库存超出，low：库存不等于总库存
     */
    async setSplitSkuStockNum(params = {}) {
        let numList = [0, 5, 10, 10];
        (params.stockNum == 'top') && (numList[common.getRandomNum(0, 3)] += 1);
        (params.stockNum == 'low') && (numList[common.getRandomNum(0, 3)] -= 1);
        const json = this.dress.splitSkus.map((obj, index) => { return { id: obj.id, strSpec1: obj.strSpec1, strSpec2: obj.strSpec2, stockNum: numList[index] } });
        let res = await sp.spdresb.setSplitSkuStockNum({ skus: json, check: false });
        if (params.stockNum == 'top') {
            expect(res.result.msgId).to.be.include('bizfdn_params_required');
        } else if (params.stockNum == 'low') {
            expect(res.result.msgId).to.be.include('bizfdn_params_required');
        } else {
            console.log(JSON.stringify(res));
            json.forEach(ele => {
                this.dress.splitSkus.forEach(obj => {
                    (ele.id == obj.id) && (Object.assign(ele, obj));
                });
            });
        }
    }

    /**
     * 设置拆色拆码售罄标识
     * @param {object} params
     * @param {stirng} params.soldOutFlag 0非售罄 1售罄
     */
    async setSplitSoldFlag(params = {}) {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        const json = this.dress.splitSkus.map(obj => { return { id: obj.id, strSpec1: obj.strSpec1, strSpec2: obj.strSpec2, soldOutFlag: params.soldOutFlag } });
        let res = await sp.spdresb.setSplitSoldFlag({ skus: json });
        // console.log(res);
        this.dress.splitSkus.forEach(obj => obj.soldOutFlag = params.soldOutFlag);

    }

    //-----------------------------------------以下是saas商品通用断言-------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * 卖家查询商品列表断言
     */
    async sellerDressListAssert() {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        let qlRes = await sp.spdresb.findSellerSpuList({ nameLike: this.saasdress.param.code })
            .then(res => res.result.data.rows.find(data => data.slhId == this.saasdress.result.val));
        let exp = format.dataFormat(this.saasdress.param, 'discount;name;namePy=name;title=name;code;pubPrice=stdprice1;isAllowReturn=isallowreturn;spec1IdList=sizeids;price1=stdprice1;price2=stdprice2;price3=stdprice3;price4=stdprice4');
        exp.spec1IdList = exp.spec1IdList.split(',');
        common.isApproximatelyEqualAssert(exp, qlRes);
    }

    /**
     * 卖家查询商品详情断言
     * @param {object} params
     * @param {stirng} params.detail  true:是否断言商品所有信息
     */
    async sellerDressFullAssert(params = {}) {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        const qfRes = await sp.spdresb.getFullById({ id: this.dress.spu.id }).then(res => res.result);
        console.log(`\n商品详情= `, JSON.stringify(qfRes));
        if (params.detail) {
            common.isApproximatelyEqualAssert(this.dress, qfRes);
        } else {
            let exp = format.dataFormat(this.saasdress.param, 'discount;name;namePy=name;title=name;code;pubPrice=stdprice1;isAllowReturn=isallowreturn;spec1IdList=sizeids;price1=stdprice1;price2=stdprice2;price3=stdprice3;price4=stdprice4');
            exp.spec1IdList = exp.spec1IdList.split(',');
            common.isApproximatelyEqualAssert(exp, qfRes.data.spu);
        }
    }


}

const saasBaseManger = module.exports = {
    saasBase
};

/**
 * 初始化saasBase
 */
saasBaseManger.setupSaas = function () {
    return new saasBase();
}

/**
 * saas登录
 * @param {object} params
 * @param {string} params.ename 1代帐套名称
 * @param {string} params.epid 1代帐套id
 */
saasBaseManger.slh1Login = async function (params = {}) {
    await caps.updateEnvByName({ name: ssAccount.saas.ename });
    caps.epid = ssAccount.saas.epid;
    await common.loginDo({ epid: ssAccount.saas.epid });
};
