const common = require('../../../lib/common');
const ssReq = require('../ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const slh1 = require('../../../reqHandler/slh1');
const ssAccount = require('../../data/ssAccount');
const getBasticData = require('../../../data/getBasicData');
const caps = require('../../../data/caps');
const format = require('../../../data/format');
const ssConfigParam = require('../configParamManager');
const dresManage = require('../dresManage');
const slh1BasicJsonparam = require('../../../slh1/help/basiceJsonparam');
const saasBase = require('./saasBase').saasBase;

class saasBatchSku extends saasBase {
    constructor() {
        super();
        this.dresBefore = {};
    }

    /**
     * 新增商品同步到商城,并且给商品添加库存
     * @param {object} params
     * @param {string} params.pk  否  slhId
     * @param {bool} params.isJunSe 否 是否均色均码
     */
    async addNewDress(params = {}) {
        await getBasticData.getBasicID();
        let styleJson = slh1BasicJsonparam.addGoodJson({ fileid: ssAccount.saas.fileid });
        params.isJunSe && Object.assign(styleJson, { colorids: `${BASICDATA.coloridJunSe}`, sizeids: `${BASICDATA.sizeidJunMa}` });
        params.pk && (styleJson.pk = params.pk);
        const newStyle = await slh1.slh1MetManager.saveGoodFull({ jsonparam: styleJson });

        // 新增库存
        !params.pk && await this.purchase(newStyle);

        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        let i = 0, qlRes;
        while (qlRes == undefined) {
            await common.delay(3000);
            qlRes = await sp.spdresb.findSellerSpuList({ nameLike: newStyle.param.code })
                .then(res => res.result.data.rows.find(data => data.slhId == newStyle.result.val));
            i += 1;
            console.log(`修改sku价格前商品列表=`, JSON.stringify(qlRes));
            if (i > 10) throw new Error('新增商品15秒还未同步到商城成功....');
        }
        const dresInfo = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result.data);

        if (params.pk) {
            this.dres.spu.name = this.dres.spu.namePy = this.dres.spu.title = styleJson.name;
            this.dres.spu.code = styleJson.code;
            this.dresBefore.spu.name = this.dresBefore.spu.namePy = this.dresBefore.spu.title = styleJson.name;
            this.dresBefore.spu.code = styleJson.code;
        } else {
            this.dres = dresInfo;
            this.dresBefore = _.cloneDeep(dresInfo);
        }
        console.log(`修改sku价格前商品详情=`, JSON.stringify(this.dres));
    }

    /**
     * 获取sku价格高级设置参数
     */
    getSkuPriceJson(params) {
        const skuList = this.dres.skus.map(sku => {
            return {
                id: sku.id,
                pubPrice: common.getRandomNum(50, 100),
                price1: common.getRandomNum(50, 100),
                price2: common.getRandomNum(50, 100),
                price3: common.getRandomNum(50, 100),
                price4: common.getRandomNum(50, 100),
                price5: common.getRandomNum(50, 100),
            }
        });
        return { skuSpecialFlag: 1, spuId: this.dres.spu.id, skus: skuList };
    }

    /**
     * sku设置高级价格后，更新缓存sku信息
     * @param {*} params sku价格高级设置参数
     */
    _updateDres(params = []) {
        this.dres.spu.pubPriceTop = 0;
        this.dres.spu.skuSpecialFlag = 1;
        params.forEach(sku => {
            this.dres.skus.forEach(dresSku => {
                if (sku.id == dresSku.id) {
                    Object.assign(dresSku, sku);
                    (sku.pubPrice < this.dres.spu.pubPrice) && (this.dres.spu.pubPriceLow = this.dres.spu.price = sku.pubPrice);
                    (sku.pubPrice > this.dres.spu.pubPriceTop) && (this.dres.spu.pubPriceTop = sku.pubPrice);
                    for (let key in sku) {
                        // (this.dres.spu[key] > sku[key]) && (this.dres.spu[key] = sku[key]);
                        if (this.dres.spu[key] > sku[key]) {
                            this.dres.spu[key] = sku[key]
                        }
                    }
                }
            })
        })
        let obj = common.update(params[0], this.dres.spu);
        delete obj.id;
        const array = Object.values(obj).sort();
        console.log(array);
        this.dres.spu.lowPrice = array[0];
        this.dres.spu.topPrice = array[array.length - 1];
    }

    /**
     * 批量更新sku价格
     * @description saas同步到商城的商品支持：sku设置不同的价格，设置好后买家端展示的是价格区间，选好sku后才有具体价格
     */
    async updateSkuPrice() {
        const json = this.getSkuPriceJson();
        await sp.spdresb.updateSkuPrice(json);
        this._updateDres(json.skus);
    }

    /**
     * 开启或关闭个性化参数
     * @param {object} params
     * @param {stirng} params.code 个性化参数code
     * @param {string} params.val 1:开启 0:关闭
     * @param {string} params.region 作用域 spg/spb
     */
    async setConfigParam(params = {}) {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        if (params.region == 'spg') {
            const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: this.sellerInfo.tenantId, code: params.code });
            const res = await ssParam.updateParam({ val: params.val });
        } else {
            const ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: this.sellerInfo.tenantId, code: params.code });
            const res = await ssParam.updateParam({ val: params.val });
        }
    }

    /**
     * 更新购物车信息
     * @param {object} cartSpu
     */
    async updateShopingCart(cartSpu, flag = 0) {
        let dres = flag == 0 ? this.dres : this.dresBefore;
        cartSpu.get(dres.spu.id).spu.spuName = cartSpu.get(dres.spu.id).spu.title = dres.spu.name;
        cartSpu.get(dres.spu.id).spu.spuCode = dres.spu.code;
        cartSpu.get(dres.spu.id).carts.forEach((cart, skuId) => {
            dres.skus.forEach(sku => {
                if (cart.skuId == sku.id) {
                    cart.skuPrice = sku.pubPrice;
                    // cart.spuName = dres.spu.name
                }
            })
        })
    }

    //------------------------------------以下是batchSkuPrice用例断言-------------------------------------------------------
    /**
     *  卖家查询商品列表断言
     */
    async sellerDressListAssert(params = {}) {
        const qlRes = await sp.spdresb.findSellerSpuList({ nameLike: this.dres.spu.name });
        const dresSpu = qlRes.result.data.rows.find(data => data.id == this.dres.spu.id);
        console.log(`商品设置高级价格后卖家查询商品列表=`, JSON.stringify(dresSpu));
        console.log(`商品设置高级价格后卖家查询商品列表exp=`, JSON.stringify(this.dres.spu));
        expect(dresSpu, `买家获取全部商品列表 未查询到商品 spuId=${this.dres.spu.id}\n\t${qlRes.reqUrl}`).not.to.be.undefined;
        common.isApproximatelyEqualAssert(params.spu || this.dres.spu, dresSpu, ['ver']);
    }

    /**
     * 卖家查询商品详情断言
     */
    async sellerDressFullAssert(params) {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        const qfRes = await sp.spdresb.getFullById({ id: this.dres.spu.id }).then(res => {
            console.log(`\nres=${JSON.stringify(res)}`);
            return res.result.data;
        });
        common.isApproximatelyEqualAssert(params || this.dres, qfRes, ['ver']);
    }

    /**
     * 买家查询商品列表断言
     */
    async buyerDressListAssert(params = {}) {
        const qlRes = await ss.spchb.getDresSpuList({ tenantId: ssAccount.saas.tenantId, searchToken: this.dres.spu.name });
        const dresSpu = qlRes.result.data.rows.find(data => data.id == this.dres.spu.id);
        // console.log(`商品设置高级价格后买家查询商品列表=`, JSON.stringify(dresSpu));
        expect(dresSpu, `买家获取全部商品列表 未查询到商品 spuId=${this.dres.spu.id}\n\t${qlRes.reqUrl}`).not.to.be.undefined;
        common.isApproximatelyEqualAssert(params.spu || this.dres.spu, dresSpu, ['ver']);
    }

    /**
     * 买家查询商品详情断言
     */
    async buyerDressfullAssert(params) {
        const qfRes = await sp.spdresb.getFullForBuyer({ spuId: this.dres.spu.id, buyerId: LOGINDATA.tenantId, _cid: this.sellerInfo.clusterCode, _tid: ssAccount.saas.tenantId }).then(res => {
            console.log(`\n买家查询商品详情Res=${JSON.stringify(res)}`);
            return res.result.data;
        });
        common.isApproximatelyEqualAssert(params || this.dres, qfRes, ['ver']);
    }


}

const saasBatchSkuManager = module.exports = {};

/**
 * 初始化slh1
 */
saasBatchSkuManager.setupSaas = function () {
    return new saasBatchSku();
}
