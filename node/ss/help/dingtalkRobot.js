let common = require('../../lib/common');

let dingtalkRobot = module.exports = {};

const access_token = '9c55f9ae730c6e4af95455122b938e07e98328a82e946ca94813025d6da88e24';

dingtalkRobot.sendMsg = async function (params) {
    return common.superagent.post(`https://oapi.dingtalk.com/robot/send?access_token=${access_token}`)
        .send(params)
        .then((response) => {
            // console.log(`response=${JSON.stringify(response)}`);
        }).catch((err) => {
            console.warn(`请求失败:\n${JSON.stringify(err)}`);
        });
};