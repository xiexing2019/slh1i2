const common = require('../../lib/common');
const basicJson = require('./basicJson');
const format = require('../../data/format');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const spMdm = require('../../reqHandler/sp/biz_server/spmdm');

let couponHelp = module.exports = {};

/**
 * setBit转换成showInCenter， useTogether
 * @param {object} coupon
 */
couponHelp.setBitTranslate = function (coupon) {
    let showInCenter, useTogether;
    switch (coupon.setBit) {
        case 0:
            showInCenter = false;
            useTogether = false;
            break;
        case 1:
            showInCenter = false;
            useTogether = true;
            break;
        case 2:
            showInCenter = true;
            useTogether = false;
            break;
        case 3:
            showInCenter = true;
            useTogether = true;
            break;
    }
    coupon = Object.assign(coupon, { showInCenter: showInCenter, useTogether: useTogether });
    return coupon;
};

/**
 * 新建优惠券下单后拼接效果数据
 * @param {object} params
 * @param {object} params.couponInfo 优惠券信息 
 * @param {object} params.billInfo 订单信息 
 */
couponHelp.effectionJson = function (params = {}) {
    const data = {
        effection: {
            billCountWithCoupons: 1, //用券成交订单数
            billMoneyWithCoupons: params.billInfo.orders[0].main.totalMoney, //用券成交额
            costBenefitRatio: (params.billInfo.orders[0].main.shopCoupsMoney / params.billInfo.orders[0].main.totalMoney).toFixed(4), //费效比
            couponId: params.couponInfo.couponId,
            moneyPerBill: params.billInfo.orders[0].main.totalMoney, //用券单笔价
            newCustomerNum: 1, //用券新客数
            pct: params.billInfo.orders[0].main.totalMoney, // 客单价
            regularCustomerNum: 0, //用券老客数
            totalBuyNum: params.billInfo.orders[0].main.totalNum, //购买件数
            totalDeductMoney: params.billInfo.orders[0].main.shopCoupsMoney, //优惠总金额
            totalPayMoney: params.billInfo.orders[0].main.totalMoney, //支付金额
            unitId: LOGINDATA.unitId
        },
        quantity: {
            couponId: params.couponInfo.couponId,
            inventory: params.couponInfo.totalQuantity - 1, //库存
            totalQuantity: params.couponInfo.totalQuantity, //发行数量
            unitId: LOGINDATA.unitId,
            usedQuantity: 1 //已使用数量
        }
    }
    return data;
};
