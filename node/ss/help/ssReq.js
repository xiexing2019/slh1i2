'use strict';
const common = require('../../lib/common');
// const format = require('../../data/format');
const spugr = require('../../reqHandler/sp/global/spugr');
const ssugr = require('../../reqHandler/ss/spg/spugr');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const docManager = require('../../reqHandler/sp/doc_server/doc');
const { URLSearchParams } = require('url');
const ssAccount = require('../data/ssAccount');
const spAuth = require('../../reqHandler/ss/confc/spAuth');
const caps = require('../../data/caps');
const ssCaps = require('../../reqHandler/ss/ssCaps');
const basicJson = require('./basicJson');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const spconfb = require('../../reqHandler/sp/biz_server/spconfb');
const ugr = require('../../reqHandler/ss/spg/ugr');
const spmdm = require('../../reqHandler/ss/spb/spmdm');
const dis = require('../../reqHandler/ss/spb/dis');
const Seller = require('./user/seller');
const dresManage = require('./dresManage');
// 现在根据productCode判断要不要踢会话，
// slhMallH5和slhMallWxApplet不踢会话（代表买家端）；
// 其他productCode都踢会话（含卖家端）
//
// 商陆花微商城买家端测试代码 slhMallTest
// 商陆花微商城卖家端测试代码 slhMallAssistTest (暂时不用)
//
// ec-ugr-wxApp-loginV2目前写死传slhMallTest(底层写死)

const dictTypeId = {
    size: 605,
    fabric: 637, //面料
    color: 601,
    season: 613,
    goodsSourceCity: 850,
    masterClass: 2010, //主营类目
    theme: 2016, //风格
    age: 2028,

};

let ssReq = module.exports = {
    captcha: '000000',
    provCode: '330000', //省份代码，省份所对应的编号，省份如浙江 330000
    cityCode: '330100', //地市代码，市所对应的编号，市如杭州市 330100
};


/**
 * 登录用户
 * {code:LOGINDATA}
 */
const spLoginUsers = {};

/** web端账号 */
const webUsers = {};

/** 白名单列表 线上测试完成后需要清空 id */
const whiteSet = new Set();

/**
 * sp平台登录
 * @param {object} params
 * @param {string} params.code 登录名：手机号或邮箱等*
 */
ssReq.ssLogin = async function (params = {}) {
    // 已登录用户不重新登录
    // 校验会话，更新信息
    if (spLoginUsers.hasOwnProperty(params.code)) {
        LOGINDATA = _.cloneDeep(spLoginUsers[params.code]);
        const sessionLite = await spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId, check: false });
        if (sessionLite.result.code == 0) {
            // 更新登录信息
            Object.assign(LOGINDATA, sessionLite.result.data);
            return;
        };
    };
    params = Object.assign({
        code: '13750850013',
        // pass: '000000',
        // authcType: 1
        captcha: this.captcha,
        authcType: 2,
        dlProductCode: 'slhMallAssistIOS'
    }, params);
    // 登录并更新登录信息
    const res = await spugr.spLogin(params);
    spLoginUsers[params.code] = _.cloneDeep(LOGINDATA);
    return res;
};

/**
 * 卖家登录并切换门店
 * @description 切换门店sessionId不变
 * @param {object} params
 * @param {string} params.code 登录名：手机号或邮箱等*
 * @param {string} params.shopName 门店名称 唯一
 */
ssReq.ssSellerLogin = async function ({ code = ssAccount.seller6.mobile, shopName = ssAccount.seller6.shopName, appKey = ssAccount.seller6.appKey, appSecret = ssAccount.seller6.appSecret } = {}) {
    await ssReq.ssLogin({ code });

    const seller = new Seller(LOGINDATA);
    await seller.changeUserShop(shopName);

    if (appKey && appSecret) {
        seller.appKey = appKey;
        seller.appSecret = appSecret;
        await seller.refreshToken(shopName);
    }

    spLoginUsers[code] = _.cloneDeep(LOGINDATA);
    return seller;
};

/**
 * 买家登录
 * @description 游客登录使用
 * @param {Object} params
 * @param {string} params.tenantId 卖家tid
 * @param {string} params.sellerId
 * @param {string} params.mobile
 * @param {string} params.appId
 * @param {string} params.scene 门店类型（0临时商城，1正式商城，2分销商城）
 */
ssReq.ssClientGuestLogin = async function (params = {}) {
    delete LOGINDATA.sessionId;
    const res = await ugr.wxAppLogin({
        openId: `${params.mobile}openId`,
        productCode: 'slhMallWxApplet',
        appId: ssAccount.seller6.appId,
        shopId: params.tenantId,
        ...params
    });
    // console.log(`\nres=${JSON.stringify(res)}`);
    LOGINDATA = _.cloneDeep(res.result.data);
};

/**
 * 买家登录
 * @param {Object} params
 * @param {string} params.tenantId 卖家tid
 * @param {string} params.sellerId
 * @param {string} params.mobile
 * @param {string} params.appId
 * @param {string} params.scene 门店类型（0临时商城，1正式商城，2分销商城）
 */
ssReq.ssClientLogin = async function (params = {}) {
    await ssReq.ssClientGuestLogin(params);

    // 保存客户信息最大重试次数
    let retryCount = 1;
    // 游客绑定手机号转变为正式用户
    if (LOGINDATA.guestFlag == 1) {
        retryCount = 10;

        // 线上需要先加白名单
        if (caps.name.includes('online')) {
            const _userInfo = _.cloneDeep(LOGINDATA);
            await spAuth.staffLogin();
            const res = await spAuth.saveWhiteList({ mobile: params.mobile, check: false });
            console.log(`添加白名单:${JSON.stringify(res)}`);
            // 删除名单需要传id
            whiteSet.add(res.result.data.val);

            LOGINDATA = _userInfo;
        }
        const bindData = await ssugr.buyerBindMobile({
            mobile: params.mobile,
            captcha: this.captcha,
        }).then(res => res.result.data);
        Object.assign(LOGINDATA, bindData, { guestFlag: 0 });
    }
    // console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);

    // 保存客户信息
    // 新客户初始化时间可能比较久,需要等待一段时间 可能需要5S左右
    for (let index = 0; index < retryCount; index++) {
        await ssugr.getShopFromBuyer({ id: LOGINDATA.shopId })
            .then(res => index = retryCount)// 成功则跳出循环
            .catch(async err => {
                if (index < retryCount && err.message.includes('初始化')) {
                    await common.delay(500);
                    return;
                }
                throw new Error(err);// 非初始化问题 直接抛出异常
            });
    }
};

/**
 * 买家切换分销商
 * @param {Object} params
 * @param {string} params.tenantId
 * @param {string} params.mobile
 * @param {string} params.appId
 */
ssReq.switchDisPartner = async function (params = {}) {
    delete LOGINDATA.sessionId;
    await ssReq.ssClientLogin(params);
    // console.log(LOGINDATA.sellerTid);
    const res = await dis.switchDistributionSeller({
        sellerTenantId: params.tenantId,
        buyerSessionId: LOGINDATA.sessionId
    });
    // console.log(`res=${JSON.stringify(res)}`);
    await spmdm.saveMemberInfo({
        tenantId: LOGINDATA.shopId,
        type: 2,
        userId: LOGINDATA.userId
    });
    LOGINDATA = _.cloneDeep(res.result.data);
};

/**
 * 微信登录小程序
 * @param {Object} params
 * @param {string} [params.appId] 小程序appId
 * @param {string} [params.tenantId] 卖家店铺
 */
ssReq.userLoginWithWx = async function (params = {}) {
    params = {
        productCode: 'slhMallWxApplet',
        appId: ssAccount.seller6.appId,
        openId: ssAccount.client.openId,
        mobile: ssAccount.client.mobile || '',
        tenantId: ssAccount.seller6.tenantId,
        ...params
    };
    await this.ssClientGuestLogin(params);

    // console.log(LOGINDATA);
    await common.delay(500);
    console.log(`当前买家的shopId=${LOGINDATA.shopId}`);

    // 保存客户信息
    await ssugr.getShopFromBuyer({ id: LOGINDATA.shopId });
};

/**
 * 退出登录
*/
ssReq.ssLogOut = async function () {
    delete spLoginUsers[LOGINDATA.code];
    await spugr.spLogOut();
};

/**
 * 卖家用小程序登录后切换到卖家身份
 * @param {object} params
 * @param {string} params.mobile
 * @param {string} params.tenantId 卖家id
 */
ssReq.wxAppSwitchSeller = async function (params) {
    delete LOGINDATA.sessionId;
    await this.ssClientLogin(params);
    let res = await ugr.wxAppSwitchSeller({ sellerTenantId: params.tenantId });
    LOGINDATA = _.cloneDeep(res.result.data);
};

/**
 * @description 切换到命令行输入的环境
*/
ssReq.changeToCommonEnv = async function () {
    let argvInfo = process.argv;
    let envInfo = argvInfo.find(res => res.includes('--env='))
    let envName = _.last(envInfo.split("="));
    caps.updateEnvByName({ name: envName });
};

/**
 * 注册用户
 */
ssReq.userRegister = async function (params) {
    params = Object.assign({
        //手机号使用12开头,与服务端约定,不发送短信通知
        mobile: common.getRandomMobile(),
        pass: '000000',
        userName: 'luxx',
        provCode: this.provCode,
        cityCode: this.cityCode,
        captcha: this.captcha,
    }, params);
    await spAuth.staffLogin();
    await spAuth.saveWhiteList({ mobile: params.mobile }); //加入白名单
    return spugr.userRegister(params);
};

/**
 * 使用新注册用户登录
 * @description spugr.sp_unit.flag=1表示创建成功
 */
ssReq.registerLogin = async function (params) {
    await ssReq.spLogin(params);

    const spgSql = await mysql({ dbName: 'spgMysql' });
    let flag = await spgSql.query(`SELECT flag FROM spugr.sp_unit WHERE tenant_id=${LOGINDATA.tenantId}`).then(res => res[0][0].flag);
    if (flag != 1) {
        await common.delay(10000);
        flag = await spgSql.query(`SELECT flag FROM spugr.sp_unit WHERE tenant_id=${LOGINDATA.tenantId}`).then(res => res[0][0].flag);
    }
    await spgSql.end();

    if (flag != 1) {
        throw new Error(`创建单元${LOGINDATA.tenantId}失败 请重试`);
    }
};

/**
 * 注册企业租户
 * @description 需要先用户注册-再企业注册
 * @description 新增租户，会自动切换到企业租户
 * @param {Object} params jsonParam
 */
ssReq.createEntTenant = async function (params) {
    params = Object.assign({
        name: common.getRandomStr(5),
        capability: 1, //能力位，从右向左各位代表能力，1 服装批发，2 服装零售*
        provCode: this.provCode,
        cityCode: this.cityCode,
        captcha: this.captcha, //验证码
    }, params);
    const createRes = await spugr.createEntTenant(params);
    return createRes;
};

/**
 * 白名单删除
 * @description 内部使用
 */
ssReq.delWhite = async function () {
    await spAuth.staffLogin();
    // 获取白名单列表
    const whiteList = await spAuth.getWhiteList().then(res => res.result.data.rows);
    // console.log(`whiteList=${JSON.stringify(whiteList)}`);
    // 排除常用手机号
    const usualMobiles = new Set(),
        deleteMobiles = new Set();
    Object.values(ssAccount).forEach(data => {
        if (data.hasOwnProperty('mobile')) {
            usualMobiles.add(data.mobile);
        }
    });
    whiteList.forEach(data => {
        if (!usualMobiles.has(data.mobile)) {
            deleteMobiles.add(data.id);
        };
    });
    for await (const val of deleteMobiles) {
        await spAuth.deleteWhiteList({ id: val, check: false });
        await common.delay(100);
    }
    // 登出
    await spAuth.staffLogout();
};

/**
 * 买家查询商品信息
 * @description 前置条件:全局搜索商品->获取商品detailUrl
 * @param {object} params
 * @param {string} params.detailUrl 商品详情url
 */
ssReq.getFullForBuyerByUrl = async function (params) {
    const strParams = _.last(params.detailUrl.split('?'));
    const urlParams = new URLSearchParams(strParams);
    return spdresb.getFullForBuyer({
        spuId: urlParams.get('spuId'),
        _cid: urlParams.get('_cid'),
        _tid: urlParams.get('_tid'),
        buyerId: LOGINDATA.tenantId
    });
};

/**
 * 前提需要卖家先登录
 * 新建一个商品，并且返回新建商品的信息
 * @param {function} cb 修改新增商品的jsonParam
 */
ssReq.saveDresFull = async function (cb) {
    const classId = [1013, 1085, 1093][common.getRandomNum(0, 2)];
    let classInfo = await spdresb.findByClass({ classId });
    let arr = ['606', '850', '2001', '2002', '2003', '2004'];
    classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
            .then((res) => res.result.data.rows);
    };
    const json = basicJson.styleJson({ classId: classId });
    if (typeof cb == 'function') {
        cb(json);
    };
    let styleRes = await spdresb.saveFull(json);
    await common.delay(500);

    return styleRes;
};

/**
 * 新增运费，并且同时修改店铺的运费方式属性
*/
ssReq.saveShipRule = async function (params) {
    let saveRes = await spconfb.saveShipRule(params);
    let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
    let shipFeeWayId;   //1 包邮，2自定义运费
    params.feeType == 0 ? shipFeeWayId = 1 : shipFeeWayId = 2;
    shopInfo.shipFeeWayId = shipFeeWayId;
    await spugr.updateShop(shopInfo);
    return saveRes;
};

/**
 * 获取字典列表
 */
ssReq.getDictList = async function (dictArr) {
    if (!dictArr.length) return;
    for (let index = 0; index < dictArr.length; index++) {
        const typeId = dictTypeId[dictArr[index]];
        if (!typeId) {
            console.warn(`未找到${dictArr[index]}的typeId,请确认~`);
            continue;
        };
        if (!BASICDATA[typeId]) BASICDATA[typeId] = await spugr.getDictList({ typeId, flag: 1 })
            .then((res) => res.result.data.rows);
    };
};

/**
 * 获取商品列表
 * @param {number} listNum 期望商品数量
 * @return {Object} Dres
 */
ssReq.getDresList = async function (listNum) {
    let dresList = [];
    const len = Math.floor(listNum / 20);
    // console.log(len);
    for (let index = 1; index <= len; index++) {
        const dresSpuList = await spdresb.findSellerSpuList({ pageNo: index, pageSize: 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
        for (const dresSpu of dresSpuList) {
            const dresFull = await spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
            const dres = dresManage.setupDres();
            dres.setBySeller(dresFull);
            // await dres.searchAndSetByDetail();
            dresList.push(dres);
        }
        // console.log(dresList.length);
    }
    // console.log(dresList.length);
    if (listNum % 20 > 0) {
        // const dresSpuList = await ss.spchb.getDresSpuList({ pageNo: len, pageSize: (listNum - len), tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        const dresSpuList = await spdresb.findSellerSpuList({ pageNo: len + 1, pageSize: listNum % 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
        for (const dresSpu of dresSpuList) {
            const dresFull = await spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
            const dres = dresManage.setupDres();
            dres.setBySeller(dresFull);
            // await dres.searchAndSetByDetail();
            // console.log(dres.getDetailExp());
            dresList.push(dres);
        }
        // console.log(this.dresList.length);
    }
    // console.log(this.dresList.length);
    return dresList;
};

/**
 * 删除缓存中的白名单数据
 */
ssReq.delWhiteByCache = async function () {
    await spAuth.staffLogin();
    for await (const val of whiteSet) {
        await spAuth.deleteWhiteList({ id: val, check: false });
        await common.delay(100);
    }
};