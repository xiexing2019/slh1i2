let common = require('../../lib/common');

let spbiHelp = module.exports = {};

/**
 * 截取日期范围内的数据
 * @param {array} dataList
 * @param {string} keyPath
 * @param {string} proDateGte 时间从
 * @param {string} proDateLte 时间到
 */
spbiHelp.getDataListByDate = function ({ dataList, keyPath, proDateGte, proDateLte }) {
    let [startIdx, endIdx] = [0, 0];

    proDateLte = common.stringToDate(proDateLte);
    endIdx = _.findIndex(dataList, data => common.stringToDate(_.get(data, keyPath)) < proDateLte);

    proDateGte = common.stringToDate(proDateGte);
    startIdx = _.findIndex(dataList, data => common.stringToDate(_.get(data, keyPath)) < proDateGte);
    console.log(`startIdx=${startIdx}  endIdx=${endIdx}`);
    if (startIdx == -1 && endIdx != -1) return dataList.slice(endIdx);
    if (startIdx != -1 && endIdx == -1) return dataList.slice(startIdx);
    if (startIdx > endIdx) [startIdx, endIdx] = [endIdx, startIdx];
    return dataList.slice(startIdx, endIdx);
};