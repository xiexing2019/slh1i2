const common = require('../../lib/common');
const spConfig = require('../../reqHandler/sp/global/spConfig');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const format = require('../../data/format');
const caps = require('../../data/caps');
const spugr = require('../../reqHandler/sp/global/spugr');
// const spCoupon = require('../../reqHandler/sp/global/spCoupon');
const spdchg = require('../../reqHandler/sp/global/spdchg');
const spconfb = require('../../reqHandler/sp/biz_server/spconfb');
const ss = require('../../reqHandler/ss');
const spBank = require('../data/spBank');
const fs = require('fs');
const path = require('path');
const numUtils = require('number-precision');
const moment = require('moment');


//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/doc.json')))[caps.name];

let basicJson = module.exports = {};


basicJson.cartJson2 = (params = {}) => {
    // let s = common.getRandomNum(0, params.result.data.skus.length - 1);
    let count = params.count || 1;
    let json = {
        "trader": {
            tenantId: LOGINDATA.shopId,
            traderName: params.result.data.spu.shopName,
            unitId: params.result.data.spu.unitId
        }
    };
    let docId;
    if (typeof (params.result.data.spu.ecCaption.docHeader[0]) == "undefined") {
        docId = '';
    } else {
        docId = params.result.data.spu.ecCaption.docHeader[0].docId;
    }
    let spu = {
        spuName: params.result.data.spu.name,
        classId: params.result.data.spu.classId,
        spuId: params.result.data.spu.id,
        title: params.result.data.spu.title,
        spuPic: docId,
    };
    function sku(count) {
        let skus = {
            spec1: params.result.data.skus[count].spec1,
            spec2: params.result.data.skus[count].spec2,
            spec3: params.result.data.skus[count].spec3,
            spec1Name: params.result.data.skus[count].ecCaption.spec1 || '',
            spec2Name: params.result.data.skus[count].ecCaption.spec2 || '',
            spec3Name: params.result.data.skus[count].ecCaption.spec3 || '',
            skuId: params.result.data.skus[count].id,
            skuNum: params.num || common.getRandomNum(1, 10),//common.getRandomNum(1, params.result.data.skus[s].num - 1),
            skuPrice: params.result.data.skus[count].pubPrice
        };
        if (params.result.data.activity && JSON.stringify(params.result.data.activity) != '{}') {
            Object.assign(skus, {
                // skuPrice: params.result.data.activity.afterDiscountPrice,
                actId: params.result.data.activity.actId,
                // tagId: params.result.data.activity.actId,
                // tagKind: params.result.data.activity.actType,
                // tagName: params.result.data.activity.actName
            })
        }

        return skus;
    };
    let cartJson = {};
    if (count == 0) {
        console.warn(`count不可为0`);
    } else {
        cartJson.trader = json.trader;
        cartJson.spu = spu;
        let arr = [];
        if (params.result.data.skus.length < count) {
            count = params.result.data.skus.length;
        };
        for (let i = 0; i < count; i++) {
            arr[i] = sku(i);
        };
        cartJson.carts = arr;
    };
    // console.log(params.result.data.spu.ecCaption.docHeader[0].docId);

    return cartJson;
};

basicJson.cartJson = (params = {}) => {
    // let s = common.getRandomNum(0, params.result.data.skus.length - 1);
    let count = params.count || 0;
    let json = {
        "trader": {
            tenantId: params.sellerTenantId,
            traderName: params.result.data.spu.shopName,
            unitId: params.result.data.spu.unitId
        }
    };
    let spu = {
        spuName: params.result.data.spu.name,
        classId: params.result.data.spu.classId,
        spuId: params.result.data.spu.id,
        title: params.result.data.spu.title
    };
    function sku(count) {
        let skus = {
            spec1: params.result.data.skus[count].spec1,
            spec2: params.result.data.skus[count].spec2,
            spec3: params.result.data.skus[count].spec3,
            spec1Name: params.result.data.skus[count].ecCaption.spec1 || '',
            spec2Name: params.result.data.skus[count].ecCaption.spec2 || '',
            spec3Name: params.result.data.skus[count].ecCaption.spec3 || '',
            skuId: params.result.data.skus[count].id,
            skuNum: common.getRandomNum(1, 15),//common.getRandomNum(1, params.result.data.skus[s].num - 1),
            skuPrice: params.result.data.skus[count].pubPrice
        };
        return skus;
    };
    let cartJson = {};
    let docId;
    if (typeof (params.result.data.spu.ecCaption.docHeader[0]) == "undefined") {
        docId = '';
    } else {
        docId = params.result.data.spu.ecCaption.docContent[0].docId;
    }
    if (count == 0) {
        cartJson.trader = json.trader;
        let skus = sku(count);
        skus.classId = spu.classId;
        skus.spuId = spu.spuId;
        skus.spuName = spu.spuName;
        skus.title = params.result.data.spu.title;
        skus.spuPic = docId
        cartJson.cart = skus;
    } else {
        cartJson.trader = json.trader;
        cartJson.spu = spu;
        let arr = [];
        if (params.result.data.skus.length < count) {
            count = params.result.data.skus.length;
        };
        for (let i = 0; i < count; i++) {
            arr[i] = sku(i);
        };
        cartJson.carts = arr;
    };
    // console.log(params.result.data.spu.ecCaption.docHeader[0].docId);

    return cartJson;
};

basicJson.updateCart = (params) => {

    let updateJson = {
        spec1: params.spec1,
        spec2: params.spec2,
        spec3: params.spec3,
        spec1Name: params.spec1Name,
        spec2Name: params.spec2Name,
        spec3Name: params.spec3Name,
        skuId: params.skuId,
        skuNum: common.getRandomNum(1, 99),
        skuPrice: common.getRandomNum(88, 299)
    };
    return updateJson;
};

basicJson.inBatchJsonFormat = (params) => {
    let spu = params.spu;
    let formatJson = {};
    let carts = [];
    formatJson.trader = params.trader;
    for (let i = 0; i < params.carts.length; i++) {
        let cart = params.carts[i];
        cart.spuId = params.spu.spuId;
        cart.spuName = params.spu.spuName;
        carts.push(cart);
    };
    formatJson.carts = carts;
    return formatJson;
};

basicJson.slhBossRegisterJson = () => {
    let json = {
        token: common.getRandomStr(5),
        mobile: common.getRandomMobile(),
        name: 'slh老板' + common.getRandomNumStr(4),
        slhSessionId: common.getRandomStr(10),
        provCode: '330000',
        cityCode: '330100'
    };
    return json;
};

basicJson.addAddrJson = (params) => {
    let json = {
        recInfo: {
            linkMan: common.getRandomChineseStr(3),
            telephone: common.getRandomMobile(),
            postcode: common.getRandomNumStr(6),  //邮编，6位
            type: 1
        },
        address: {
            provinceCode: params.provinceValue,
            cityCode: params.cityValue,
            countyCode: params.countyValue,
            detailAddr: `和平大厦${common.getRandomNumStr(5)}号`
        },
    };
    return json;
};

//运费json
basicJson.saveFeeRuleJson = async (provCode, count) => {
    let proviceArray = (await basicJson.getAreaJson()).proviceArr;
    _.remove(proviceArray, (element) => element == provCode);

    let rules = [];
    for (let i = 0; i <= count; i++) {
        rules.push(getJson(i))
    };
    function getJson(count) {
        return {
            feeType: 2,
            provIds: proviceArray[count],
            startNum: common.getRandomNum(20, 30), //起始数量，随机生成20-30之间的数作为起始数量
            startFee: common.getRandomNum(10, 15),  //起始费用，随机生成10-15之间的数作为起始费用
            addNum: 10,
            addFee: 1
        }
    };
    rules[rules.length - 1].provIds = provCode;
    return { rules: rules };
};

/**
 * 店铺认证信息保存
 * @param {Object} params
*/
basicJson.shopJson = async () => {
    const areaJson = await basicJson.getAreaJson();
    const getMarket = await spCommon.getMarket({});
    const tagList = await spCommon.findCatConfig({ type: 2, showFlagBit: 1 });
    const masterClass = await spugr.getDictList({ "flag": 1, "typeId": 2010 }); //主营类目，2010字典里拿的
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    const json = {
        name: `平台企业${common.getRandomNumStr(6)}`,
        logoPic: docData.image.shopLogo[0].docId,//存储LOGO图片fileid，单张
        //存储门头照片文件id，多张，逗号分隔
        //客户端只能上传一张，并且没有做多张处理,因此自动化这里也只传一张
        headPic: _.take(docIdArr, 1).join(','),
        contentPic: _.takeRight(docIdArr, 3).join(','),//存储店铺内景照片，多张，逗号分隔
        certPic: _.takeRight(docIdArr, 2).join(','),//营业执照和身份证照（证书照片）id，多张，逗号分隔
        // videoId: common.getRandomNumStr(6),
        // videoUrl: common.getRandomStr(8),
        // coverUrl: common.getRandomStr(8),
        provCode: areaJson.provinceValue,
        cityCode: areaJson.cityValue,
        areaCode: areaJson.countyValue,
        tagsList: [tagList.result.data['2'][0].typeValue], //这里要用typeValue的 别用id
        masterClassId: masterClass.result.data.rows[0].codeValue,
        marketId: getMarket.result.data.rows[0].id,
        marketName: getMarket.result.data.rows[0].fullName,
        floor: common.getRandomNum(1, 99),
        doorNo: common.getRandomNumStr(3),
        detailAddr: '滨江星耀城',
        shipFeeWayId: 1,
        deliverProvCode: areaJson.provinceValue,
        deliverCityCode: areaJson.cityValue,
        deliverAreaCode: areaJson.countyValue,
        deliverDetailAddr: '滨江星耀城16楼',
    };
    return json;
};

basicJson.tenantJson = async function () {
    const areaJson = await basicJson.getAreaJson();
    // 获取主营类目
    const masterClassIds = await ss.config.getSpgDictList({ typeId: '2010', flag: 1 })
        .then(res => res.result.data.rows.slice(0, 3).map(ele => ele.codeValue));
    // 可用套餐列表
    const availableSetList = await ss.spugr.findAvailableSetList().then(res => res.result.data.rows);
    console.log(`\n availableSetList=${JSON.stringify(availableSetList)}`);
    const name = `店铺${common.getRandomStr(5)}`;
    return {
        name: name,
        typeId: 2,
        logoPic: docData.image.shopBackground[0].docId,
        address: `${areaJson.provinceCode}${areaJson.cityCode}${areaJson.countyCode}${common.getRandomStr(5)}`,
        provCode: areaJson.provinceValue,
        cityCode: areaJson.cityValue,
        areaCode: areaJson.countyValue,
        bizCategories: masterClassIds.join(','),
        bizModelId: 1,
        mobile: LOGINDATA.mobile,
        ssSetId: availableSetList[0].id,
        headPic: await ss.spugr.getRandomShopOutImg({ shopName: name }).then(res => res.result.data.val),
        contentPic: await ss.spugr.getRandomShopInImg().then(res => res.result.data.val),
        // certPic: `${docData.idCard.frontImage.docId},${docData.idCard.backImage.docId}`
    };
};

/**
 * 拼接商品保存参数
 * @param {object} params
 * @param {object} params.classId 类别id
 * @param {object} params.spec1IdsNum
 * @param {object} params.spec2IdsNum
 */
basicJson.styleJson = (params = {}, cb) => {
    const randomStr = common.getRandomStr(5);

    const price = {
        tagPrice: 300,
        pubPrice: 200,
        price1: 190,
        price2: 180,
        price3: 170,
        price4: 160,
        price5: 150,
    };

    let spu = Object.assign({
        code: `款号${randomStr}`,
        name: `商品n${randomStr}`,
        classId: params.classId || 1013,
        title: `商品t${randomStr}`,
        // namePy:'',//自动生成
        shipOriginId: BASICDATA['850'][0].codeValue,//	发货地（国家行政区划，字典850）
        shipFeeWayId: BASICDATA['2001'][0].codeValue,//	运费方式（买家承担、包邮等，字典2001）
        brandId: params.brandId || '',//品牌（字典606）
        pubScope: BASICDATA['2003'].find((data) => data.codeName == '全部').codeValue || 1,//发布范围
        unit: '件',
        special: 0,
        isAllowReturn: 1,
        origin: '杭州',//产地（用户手工输入）
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        // slhClassName: `奇装异服`,   //类别名
    }, price);
    if (params.hasOwnProperty('season')) {
        spu.season = params.season;
        spu.seasonName = BASICDATA['613'].find(data => data.codeValue == params.season).codeName || `春季`; //季节名
    }
    const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
    spu.docContent = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
    const vodeo = { typeId: 3, ...docData.video.dres[0] };
    spu.docHeader = [vodeo, _.head(spu.docContent)];//
    // spu.docContent.push(vodeo);

    //spec1Ids 规格1范围。
    //格式：{“规格值”:{“属性1名称”:”属性1值”},…}。
    //每个规格值可以定义一些附加属性。
    //目前支持“salesCaption”（销售标题）属性，以指定该款号的规格值在销售页面显示给买家的文本。
    //如“服装”类别的款号，包含颜色(spec1）和尺码(spec2）两种规格属性。
    //有一个款号可选颜色范围为2（红色）、6（灰色），其中红色显示给顾客的文本需要改为“2018升级版红色”。
    //spec1_ids可设置为{“2”:{“salesCaption”:”2018升级版红色”, “6”:{}}。

    const spec1IdsNum = params.spec1IdsNum || 3;
    const spec2IdsNum = params.spec2IdsNum || 3;
    // console.log(spec1IdsNum, spec2IdsNum);
    spu.spec1Ids = {};
    common.randomSort(BASICDATA['605']).slice(0, spec1IdsNum).forEach(element => spu.spec1Ids[element.codeValue] = { salesCaption: element.codeName });
    spu.spec2Ids = {};
    common.randomSort(BASICDATA['601']).slice(0, spec2IdsNum).forEach(element => spu.spec2Ids[element.codeValue] = { salesCaption: element.codeName });
    spu.spec3Ids = {};

    let skus = [];
    const sku = { id: '', spec1: '', spec2: '', spec3: '', num: '10000', ssNum: '10000', rem: '备注', ...price },
        spec1Ids = Object.keys(spu.spec1Ids),
        spec2Ids = Object.keys(spu.spec2Ids);

    spec1Ids.forEach((spec1Id) => {
        spec2Ids.forEach((spec2Id) => skus.push(Object.assign({}, sku, { spec1: spec1Id, spec2: spec2Id })));
    });

    if (typeof (cb) == 'function') {
        cb(spu, skus);
    }

    return {
        id: '',
        spu,
        skus,
    };
};

basicJson.styleJsonByApp = (params = {}, cb) => {
    const randomStr = common.getRandomStr(5);

    const price = {
        tagPrice: 300,
        pubPrice: 200,
        price1: 190,
        price2: 180,
        price3: 170,
        price4: 160,
        price5: 150,
    };

    let spu = Object.assign({
        code: `款号${randomStr}`,
        name: `商品n${randomStr}`,
        slhClassName: 'T恤',
        title: `商品t${randomStr}`,
        // namePy:'',//自动生成
        shipOriginId: BASICDATA['850'][0].codeValue,//	发货地（国家行政区划，字典850）
        shipFeeWayId: BASICDATA['2001'][0].codeValue,//	运费方式（买家承担、包邮等，字典2001）
        brandId: '',//品牌（字典606）
        pubScope: BASICDATA['2003'].find((data) => data.codeName == '全部').codeValue || 1,//发布范围
        unit: '件',
        special: 0,
        isAllowReturn: 1,
        origin: '杭州',//产地（用户手工输入）
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        brandName: `测试品牌`,      //品牌名
        themeName: `简约`,      //风格名
        seasonName: `春季`,     //季节名
        fabricName: `真丝`,     //面料名
        // slhClassName: `星星装`,   //类别名
        // templateId: 1,// 包邮
        weight: 0.3
    }, price);
    const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
    spu.docContent = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
    const vodeo = { typeId: 3, ...docData.video.dres[0] };
    spu.docHeader = [vodeo, _.head(spu.docContent)];//
    // spu.docContent.push(vodeo);

    // 尺码
    spu.spec1Names = params.spec1Names || 'X,XL,XXL';
    // 颜色
    spu.spec2Names = params.spec2Names || '白色,黑色,灰色';

    const skus = [];
    const sku = { num: 10000, ssNum: 10000, rem: '备注', ...price },
        spec1Names = spu.spec1Names.split(','),
        spec2Names = spu.spec2Names.split(',');

    spec1Names.forEach((spec1Name) => {
        spec2Names.forEach((spec2Name) => skus.push(Object.assign({}, sku, { spec1Name, spec2Name })));
    });

    if (typeof (cb) == 'function') {
        cb(spu, skus);
    }

    return {
        id: '',
        spu,
        skus,
    };
};

basicJson.wideStyleJsonByApp = (params = {}, cb) => {
    const randomStr = common.getRandomStr(5);

    const price = {
        tagPrice: 300,
        pubPrice: 200,
        price1: 190,
        price2: 180,
        price3: 170,
        price4: 160,
        price5: 150,
    };

    let spu = Object.assign({
        addSpuType: 1, // 0 不考虑泛行业的独立app商品处理 1 泛行业的独立app商品处理
        spec1FiledName: 'outward',  // 外观
        spec2FiledName: 'capacity', // 存储容量
        calPriceType: 0, // 计价类型，0 按件 1按重 不传默认0
        virtualFlag: 0, // 是否是虚拟商品 0 不是 1是 不传默认0
        code: `泛款号${randomStr}`,
        name: `泛商品n${randomStr}`,
        slhClassName: 'T恤',
        title: `泛商品t${randomStr}`,
        // namePy:'',//自动生成
        shipOriginId: BASICDATA['850'][0].codeValue,//	发货地（国家行政区划，字典850）
        shipFeeWayId: BASICDATA['2001'][0].codeValue,//	运费方式（买家承担、包邮等，字典2001）
        brandId: '',//品牌（字典606）
        pubScope: BASICDATA['2003'].find((data) => data.codeName == '全部').codeValue || 1,//发布范围
        unit: '件',
        special: 0,
        isAllowReturn: 1,
        origin: '杭州',//产地（用户手工输入）
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        brandName: `测试品牌`,      //品牌名
        themeName: `简约`,      //风格名
        seasonName: `春季`,     //季节名
        fabricName: `真丝`,     //面料名
        // slhClassName: `星星装`,   //类别名
        // templateId: 1,// 包邮
        // weight: 0.3,
        priceTop: {}  // 接口不需要的字段，拼期望值使用
    }, price);

    // 产地（泛行业新增属性)
    params.dictTypeId && (spu.propFiledList = [{
        propFiledDictTypeId: params.dictTypeId,
        propFiledName: 'space',
        propFiledNameCaption: '产地',
        propFiledValue: BASICDATA[params.dictTypeId].codeValue,
        propFiledValueCaption: '中国'
    }]);

    const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
    spu.docContent = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
    const vodeo = { typeId: 3, ...docData.video.dres[0] };
    spu.docHeader = [vodeo, _.head(spu.docContent)];//
    // spu.docContent.push(vodeo);

    // 规格1
    spu.spec1Names = params.spec1Names || '深空灰色,暗夜绿色,银色,金色';
    // 规格2
    spu.spec2Names = params.spec2Names || '64GB,256GB,512GB';

    const skus = [];
    const sku = { num: 10000, ssNum: 10000, rem: '备注' },
        spec1Names = spu.spec1Names.split(','),
        spec2Names = spu.spec2Names.split(',');

    spec1Names.forEach((spec1Name) => {
        spec2Names.forEach((spec2Name) => {
            const skuPrice = {
                tagPrice: 300,
                pubPrice: common.getRandomNum(190, 200),
                price1: common.getRandomNum(190, 200),
                price2: common.getRandomNum(180, 190),
                price3: common.getRandomNum(170, 180),
                price4: common.getRandomNum(160, 170),
                price5: common.getRandomNum(150, 160),
                weight: common.getRandomNum(1, 100),
            };
            skus.push(Object.assign(skuPrice, sku, { spec1Name, spec2Name }))
        })
    });

    if (typeof (cb) == 'function') {
        cb(spu, skus);
    }
    let spuPrice = {}, priceTop = {};
    for (let p of Object.keys(price)) {
        let tempArray = skus.map(ele => ele[p]);
        spuPrice[p] = Math.min.apply(null, tempArray);
        priceTop[p] = Math.max.apply(null, tempArray);
    }
    Object.assign(spu, spuPrice);
    Object.assign(spu.priceTop, priceTop);

    return {
        id: '',
        spu,
        skus,
    };
};

/**
 * 修改单据商品价格
 * @description 线上环境需要控制单据总价
 * @param {Number} price
 */
basicJson.changeBillSkuPrice = function (price) {
    return caps.name.includes('online') && Number(price) >= 10 ? common.div(price, 100) : price;
};

/**
 * 拼接采购订单参数
 * @description ec-sppur-purBill-createFull
 * @param {object} params
 * @param {object} params.styleInfo 款号信息
 * @param {string} [params.shipPayKind=0] 0-快递(默认) 1-预先支付（未启用） 2-客户自提
 * @param {string} [params.payKind=1] 1 预付，2 货到付款，3 线下支付
 * @param {number} [param.count=2] 添加到单据sku数量
 */
basicJson.purJson = (params) => {
    const [styleInfo, trader] = [params.styleInfo.result.data, params.styleInfo.params];
    let docId;
    if (typeof (styleInfo.spu.ecCaption.docHeader[0]) == 'undefined') {
        docId = '';
    } else {
        docId = styleInfo.spu.ecCaption.docHeader[0].docId;
    }

    const detailSpu = {
        spuId: styleInfo.spu.id,
        spuTitle: styleInfo.spu.title,
        spuCode: styleInfo.spu.code,
        spuDocId: docId  //这里要传，不然导致下订单之后订单款号没有图片
    };

    // 泛行业扩展字段
    // if (styleInfo.spu.addSpuType == 1) {
    detailSpu.specPropsJson = {
        spec1: styleInfo.spu.spec1FiledNameCaption,// 不传默认尺码
        spec2: styleInfo.spu.spec2FiledNameCaption,// 不传默认颜色
    }
    // }

    let main = {
        sellerId: trader._tid,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: 0,//运费。若为到付，则为0
        shipPayKind: params.shipPayKind || 0, //shipPayKind  int  是
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: params.payKind || 1,//2,//付款方式。 1 预付，2 货到付款，3 线下支付
        couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: LOGINDATA.defAddressId,//收货地址ID 取当前用户默认地址 需要登录时获取
    }, details = [];
    common.randomSort(styleInfo.skus).slice(0, params.hasOwnProperty('count') ? params.count : 2).forEach((sku) => {
        const detail = Object.assign(new basicJson.GetDetail(sku), detailSpu);
        //console.log(detail);
        main.originalMoney = common.add(main.originalMoney, detail.originalPrice);
        main.money = common.add(main.money, detail.money);
        main.totalNum += detail.num;
        details.push(detail);
        //console.log(details);
    });
    main.totalMoney = [main.money, -main.shopCoupsMoney, main.shipFeeMoney].reduce((a, b) => common.add(a, b), 0);
    return { orders: [{ main, details }] };
};

basicJson.GetDetail = function (sku) {
    console.log(sku);

    this.skuId = sku.id;
    this.spec1 = sku.spec1;
    this.spec1Name = sku.ecCaption.spec1 || '';
    this.spec2 = sku.spec2;
    this.spec2Name = sku.ecCaption.spec2 || '';
    this.spec3 = sku.spec3;
    this.spec3Name = sku.ecCaption.spec3 || '';
    this.num = common.getRandomNum(1, 10);
    /** 原始价格，即pubPrice */
    this.originalPrice = basicJson.changeBillSkuPrice(sku.pubPrice);
    /** 买家适用价格 */
    this.price = this.originalPrice;
    this.money = common.mul(this.num, this.price);
    /** 活动优惠金额, 活动订单时，填充为（原价-下单价) * 数量 */
    this.marketFavorMoney = common.mul(common.sub(this.originalPrice, this.price), this.num);
};

/**
 * 拼接购物车 转采购订单的参数
 * @param {object} params 购物车查询列表的json(包括参数和返回)
 * @returns {object} 采购订单需要的JSON
*/
basicJson.purJsonByCart = (params) => {
    let details = [];
    let main = {
        sellerId: params.result.data.rows[0].trader.tenantId,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: 0,//店家卡券抵扣金额
        shipFeeMoney: 0,//运费。若为到付，则为0
        shipPayKind: params.shipPayKind || 0, //shipPayKind  int  是
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: params.payKind || 1,//付款方式。1 预付，2 货到付款。
        couponsIds: '',//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: LOGINDATA.defAddressId//收货地址ID 取当前用户默认地址 需要登录时获取
    };

    for (let i = 0; i < params.result.data.rows[0].carts.length; i++) {
        let info = format.dataFormat(params.result.data.rows[0].carts[i], "cartId=id;skuId;spuId;spuTitle=title;spec1;spec1Name;spec2;spec2Name;spec3;spec3Name;num=skuNum;originalPrice=skuPrice");
        info.money = common.mul(info.num, info.originalPrice);
        info.price = info.originalPrice;
        info.spuCode = info.spuTitle.replace('商品', '款号');
        main.originalMoney += info.originalPrice;
        main.money += info.money;
        main.totalNum += info.num;
        details.push(info);
    };
    //实付金额=商品总额-优惠卡券+运费
    main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;

    return { orders: [{ main, details }] };
};

/**
 * 拼接购物车 转采购订单的参数
 * @param {object} params 购物车查询列表的json(包括参数和返回)
 * @param {object} params.styleInfo 款号信息
 * @param {object} params.couponInfo 优惠券信息
 * @param {object} params.evalShipFeeInfo 运费信息
 * @param {object} params.addressInfo 地址信息
 * @param {string} [params.shipPayKind=0] 0-快递(默认) 1-预先支付（未启用） 2-客户自提
 * @param {string} [params.payKind=1] 1 预付，2 货到付款，3 线下支付
 */
basicJson.purJsonByCart2 = (params) => {
    const styleInfo = params.styleInfo.result.data;
    let details = [];
    let main = {
        sellerId: LOGINDATA.shopId,
        money: 0,//成交金额。按买家适用价格计算得到的金额合计
        shopCoupsMoney: params.couponInfo.discountSum,//店家卡券抵扣金额TODO:
        shipFeeMoney: params.evalShipFeeInfo || 0,//运费。若为到付，则为0TODO:
        shipPayKind: params.shipPayKind || 0, //shipPayKind  int  是
        originalMoney: 0,//订单原金额， 按pubPrice计算金额
        // totalMoney:0,//总应收金额。totalMoney = money - shopCoupsMoney + shipFeeMoney
        totalNum: 0,
        payKind: params.payKind || 1,//付款方式。1 预付，2 货到付款，3 线下支付
        couponsIds: params.couponInfo.couponId,//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        mineCouponsIds: params.couponInfo.mineCouponId,//使用的券领用号ids。该订单使用的优惠券领用编号列表，多个逗号分隔
        buyerRem: 'savePurBill' + common.getRandomStr(5),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        addressId: params.addressInfo.result.data.recInfo.id//收货地址ID 取当前用户默认地址 需要登录时获取
    };
    // console.log(`\n styleInfo=${JSON.stringify(styleInfo)}`);
    for (let i = 0; i < styleInfo.rows[0].carts.length; i++) {
        // console.log(`\n styleInfo.rows[0].carts[i]=${JSON.stringify(styleInfo.rows[0].carts[i])}`);
        let info = format.dataFormat(styleInfo.rows[0].carts[i], "cartId=id;skuId;spuId;spec1;spec1Name;spec2;spec2Name;spec3;spec3Name;num=skuNum;originalPrice=skuPrice;title=title");
        console.log(`\n styleInfo=${JSON.stringify(styleInfo)}`);
        console.log(`\n info=${JSON.stringify(info)}`);
        info.spuTitle = info.title;
        info.originalPrice = basicJson.changeBillSkuPrice(info.originalPrice);
        info.money = common.mul(info.num, info.originalPrice);
        info.price = info.originalPrice;
        info.spuCode = info.spuTitle.replace('商品', '款号');
        // main.originalMoney += info.originalPrice;
        main.money += info.money;
        main.totalNum += info.num;
        details.push(info);
    };
    //实付金额=商品总额-优惠卡券+运费
    main.money = numUtils.strip(main.money);
    main.totalMoney = main.money - main.shopCoupsMoney + main.shipFeeMoney;

    return { orders: [{ main, details }] };
};

/**
 * 拼接退货声请单参数
 * @description ec-sppur-billReturn-createFull
 * @param {object} params 采购单信息
 */
basicJson.returnBillJson = (params) => {

    let returnReason = BASICDATA['2005'][common.getRandomNum(0, BASICDATA['2005'].length - 1)];
    const main = {
        typeId: 1,//0-仅退款 1-退款退货 2-48小时极速退款
        purBillId: params.bill.id,
        totalNum: 0,
        totalMoney: 0,
        rem: common.getRandomStr(6),
        // score: 0,//应退积分
        returnReason: returnReason.codeValue,//退货原因（字典类别2005）
        returnReasonName: returnReason.codeName,
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
    }, details = [];

    const mapField = 'purDetailId=id;num=skuNum;price=skuPrice';
    params.skus.forEach((sku) => {
        const detail = format.dataFormat(sku, mapField);
        detail.money = common.mul(detail.num, detail.price);
        details.push(detail);
        main.totalNum += detail.num;
        main.totalMoney += detail.money;
    });
    //实退金额=商品总额-优惠金额-优惠券金额
    main.totalMoney = main.totalMoney - params.bill.favorMoney - params.bill.shopCoupsMoney;
    return { main, details };
};

/**
 * 拼接退货声请单参数
 * @description ec-sppur-billReturn-createFull
 * @param {object} params 采购单信息
 * @param {object} params.typeId //0-仅退款 1-退款退货 2-48小时极速退款 默认1
 * @param {object} params.returnNum //仅当退款退货时生效， 默认全部
 */
basicJson.returnBillJson2 = async (params) => {

    if (typeof (params.typeId) == "undefined") {
        params.typeId = 1;
    }
    let reasonData;
    if (params.typeId == 0) {
        //仅退款
        reasonData = await spugr.getDictList({ "typeId": 3003, "flag": 1 }).then((res) => res.result.data.rows);
    } else if (params.typeId == 1) {
        //退货原因
        reasonData = await spugr.getDictList({ "typeId": 3003, "flag": 1 }).then((res) => res.result.data.rows);
    }
    const random = common.getRandomNum(0, reasonData.length - 1);
    const main = {
        typeId: params.typeId,//0-仅退款 1-退款退货 2-48小时极速退款
        purBillId: params.purBillInfo.bill.id,
        totalNum: 0,
        totalMoney: 0,
        rem: common.getRandomStr(6),
        // score: 0,//应退积分
        returnReason: reasonData[random].codeValue,//退货原因（字典类别3003）
        returnReasonName: reasonData[random].codeName,
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
    }, details = [];
    if (typeof (params.returnNum) == "undefined") {
        params.returnNum = params.purBillInfo.skus.length;
    }
    // console.log(params.returnNum, params.typeId);

    const mapField = 'purDetailId=id;num=skuNum;price=skuPrice';
    if (params.typeId == 0) {  //仅退款
        params.purBillInfo.skus.forEach((sku) => {
            const detail = format.dataFormat(sku, mapField);
            detail.money = common.mul(detail.num, detail.price);
            main.totalMoney = common.add(main.totalMoney, detail.money);
        });
        //实退金额=商品总额-优惠金额-优惠券金额
        main.totalMoney = [main.totalMoney, -params.purBillInfo.bill.favorMoney, -params.purBillInfo.bill.shopCoupsMoney].reduce((a, b) => common.add(a, b), 0);
    } else if (params.typeId == 1) {
        if (params.returnNum == params.purBillInfo.skus.length) { // 退全部
            params.purBillInfo.skus.forEach((sku) => {
                const detail = format.dataFormat(sku, mapField);
                detail.money = common.mul(detail.num, detail.price);
                details.push(detail);
                main.totalNum = common.add(main.totalNum, detail.num);
                main.totalMoney = common.add(main.totalMoney, detail.money);
            });
            //实退金额=商品总额-优惠金额-优惠券金额
            main.totalMoney = [main.totalMoney, -params.purBillInfo.bill.favorMoney, -params.purBillInfo.bill.shopCoupsMoney].reduce((a, b) => common.add(a, b), 0);
        } else {
            //退货退款退returnNum款
            for (let i = 0; i < params.returnNum; i++) {
                const detail = format.dataFormat(params.purBillInfo.skus[i], mapField);
                detail.money = common.mul(detail.num, detail.price);
                main.totalNum += detail.num;
                // main.totalMoney += (detail.money).toFixed(2);
                // endMoney += (detail.money - ((detail.money / params.bill.money) * params.bill.shopCoupsMoney).toFixed(2) - ((detail.money / params.bill.money).toFixed(2) * params.bill.favorMoney).toFixed(2)).toFixed(2);
                let ratio = common.div(detail.price, params.purBillInfo.bill.money);
                let avg = Math.round((detail.price - common.mul(ratio, params.purBillInfo.bill.shopCoupsMoney) - common.mul(ratio, params.purBillInfo.bill.favorMoney)) * Math.pow(10, 2)) / Math.pow(10, 2);
                // console.log(ratio, avg, params.purBillInfo.bill.shopCoupsMoney);
                detail.skuAvgPrice = parseFloat(avg);
                if (detail.skuAvgPrice < 0.01) {
                    detail.skuAvgPrice = 0.01;
                }
                details.push(detail);
                main.totalMoney += parseFloat((detail.skuAvgPrice * detail.num).toFixed(2));
            }
        }
    };
    // console.log({ main, details });
    return { main, details };
};

/**
 * 改单
 * @param {object} params
 * @param {object} params.billInfo
 * @param {object} params.edit 要修改的部分
 */
basicJson.editBillJson = (params) => {
    params = Object.assign({ partDeliver: false, edit: { spu: false, skuNum: false, skuSum: false, price: false, difPrice: false } }, params);
    console.log(`\nparams=${JSON.stringify(params)}`);

    const bill = {
        id: params.billInfo.bill.id,
        totalNum: 0,
        originalMoney: 0,
        totalMoney: 0,
        money: params.billInfo.bill.money,
    }, details = [];

    let mapField = 'id=id;spuId=spuId;skuId=skuId;spuTitle=spuTitle;spec1=spec1;spec2=spec2;spec3=spec3;spec1Name=spec1Name;spec2Name=spec2Name;spec3Name=spec3Name;num=skuNum;oroginalPrice=originalPrice;price=skuPrice;spuDocUrl=spuDocUrl';
    if (params.partDeliver) mapField = 'id=id;spuId=spuId;skuId=skuId;spuTitle=spuTitle;spec1=spec1;spec2=spec2;spec3=spec3;spec1Name=spec1Name;spec2Name=spec2Name;spec3Name=spec3Name;num=num;oroginalPrice=originalPrice;price=price;spuDocUrl=spuDocUrl';

    params.billInfo.skus.forEach((sku, index) => {
        const detail = format.dataFormat(sku, mapField);
        console.log('\n deliverNum:', sku.deliverNum);
        detail.deliverNum = sku.deliverNum ? sku.deliverNum : 0;
        params.edit.price && (detail.price = 250);
        params.edit.difPrice && (detail.price = common.getRandomNum(100, 300));
        params.edit.skuNum && (detail.num = common.getRandomNum(detail.deliverNum + 1, 10));
        detail.money = common.mul(detail.num, detail.price);
        if (params.edit.skuSum && index == 0) {
            console.log('\n跳过第一条');
        } else {
            details.push(detail);
            bill.totalNum += detail.num;
            bill.totalMoney += detail.money;
            bill.originalMoney += common.mul(detail.num, detail.oroginalPrice);
        }

    });
    bill.totalMoney = bill.totalMoney - params.billInfo.bill.favorMoney - params.billInfo.bill.shopCoupsMoney;
    return { bill, detail: details };

}

/**
 * 缺货终结
 * @description
 * @param {object} params
 * @param {object} params.payKind 0:终结退款, 2:仅缺货终结
 * @param {object} params.part false
 * @param {object} params.billInfo
 * @param {object} params.syncDeliverJson 同步发货json
 */
basicJson.stockoutEndBillJson = (params) => {
    params = Object.assign({ payKind: 2, part: false }, params);
    const main = {
        payKind: params.payKind,
        billId: params.billInfo.bill.id,
        totalNum: 0,
        totalMoney: 0,
        // score: 0,
        rem: common.getRandomStr(6),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
    }, details = [];
    const mapField = 'onlineid=id;num=skuNum;price=skuPrice';
    params.billInfo.skus.forEach((sku) => {
        const detail = format.dataFormat(sku, mapField);
        detail.money = common.mul(detail.num, detail.price);
        details.push(detail);
        main.totalNum += detail.num;
        main.totalMoney += detail.money;
    });
    //实退金额=商品总额-优惠金额-优惠券金额
    main.totalMoney = main.totalMoney - params.billInfo.bill.favorMoney - params.billInfo.bill.shopCoupsMoney;
    let stockoutEndBillJson = { main, details };
    // console.log(stockoutEndBillJson);
    if (params.part) {
        stockoutEndBillJson.main.totalNum = 0;
        params.syncDeliverJson.details.forEach((detail, index) => {
            stockoutEndBillJson.details[index].num -= detail.num;
            stockoutEndBillJson.details[index].money = common.mul(stockoutEndBillJson.details[index].price, stockoutEndBillJson.details[index].num);
            stockoutEndBillJson.main.totalNum += stockoutEndBillJson.details[index].num;
        });
    }
    // console.log(stockoutEndBillJson);
    return stockoutEndBillJson;
};

/**
 * 同步发货
 * @description
 * @param {object} params
 * @param {object} params.part false
 * @param {object} params.billInfo
 */
basicJson.syncDeliverJson = async (params) => {
    params = Object.assign({ part: false }, params);
    const logisList = await spconfb.findLogisList1({ cap: 1 });
    const main = {
        shipFeeMoney: 0,
        logisCompId: logisList.result.data.rows[0].id,
        logisCompName: logisList.result.data.rows[0].name,
        waybillNo: common.getRandomNumStr(12),
        rem: common.getRandomStr(6),
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        // ver: 0
    }, details = [];
    const mapField = 'salesDetailId=id;num=skuNum';
    params.billInfo.skus.forEach((sku) => {
        const detail = format.dataFormat(sku, mapField);
        // console.log(detail);
        details.push(detail);
    });
    // console.log({ main, details });
    if (params.part) {
        details[0].num = 1;
        if (details.length >= 3) {
            details[2].num = 0;
        }
    }
    // console.log({ main, details });
    return { main, details };
};

/**
 * slh开通商陆好店Json
*/
basicJson.createTetantJson = (params) => {
    // console.log(`params.bank=${JSON.stringify(params.bank)}`);
    let json = {
        slhShopId: LOGINDATA.invid,
        name: '言匠',
        mchAbbr: '言匠',
        contact: 'lxx',
        mobile: '15988413448',
        provCode: params.areaJson.provinceValue,
        cityCode: params.areaJson.cityValue,
        areaCode: params.areaJson.countyValue,
        detailAddr: '星耀城1期16楼',
        settleAcctNo: common.getRandomNumStr(16),
        settleAcctName: params.bank.lName,
        settleAcctType: 1,   //1个人账户 2企业账户
        bankName: params.bank.lName,
        bankCode: params.bank.bankCode,
        authCode: params.authCode,
        cncbFlag: 2,
    };
    return json;
};

/**
 *
*/
basicJson.createTetantBySlhJson = async () => {
    const areaJson = await basicJson.getAreaJson();
    const getMarket = await spCommon.getMarket({});
    const tagList = await spCommon.findCatConfig({ type: 2, showFlagBit: 1 });  //店铺风格
    const masterClass = await spugr.getDictList({ "flag": 1, "typeId": 2010 }); //主营类目，2010字典里拿的
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    let json = {
        name: '花木牛仔屋',
        mobile: '15988413448',
        provCode: areaJson.provinceValue,
        cityCode: areaJson.cityValue,
        areaCode: areaJson.countyValue,
        detailAddr: '滨江星耀城',
        logoPic: docData.image.shopLogo[0].docId,
        tagsList: [tagList.result.data['2'][0].typeValue],  //店铺风格
        marketId: getMarket.result.data.rows[0].id, //市场标签
        floor: common.getRandomNum(1, 50),
        doorNo: common.getRandomNumStr(3),
        headPic: _.take(docIdArr, 1).join(','),
        masterClassId: masterClass.result.data.rows[0].codeValue //主营类目
    };
    return json;
}

/**
 * 获取区域
 * @description 港澳台地区没有cityCode 实际传值时传任意值 不做判断
 */
basicJson.getAreaJson = async function () {
    const areaList = await spConfig.exportDistrictRegion().then(res => res.result.data.rows);
    //随机取一个省份信息
    const areaInfo = areaList[common.getRandomNum(0, areaList.length - 1)];

    let area = {
        provinceCode: areaInfo.label,
        provinceValue: areaInfo.value,
        cityCode: '',
        cityValue: 0,
        countyCode: '',
        countyValue: 0
    };

    if (areaInfo.hasOwnProperty('children')) {
        const cityInfo = areaInfo.children[common.getRandomNum(0, areaInfo.children.length - 1)];
        area.cityCode = cityInfo.label;
        area.cityValue = cityInfo.value;
        if (cityInfo.hasOwnProperty('children')) {
            const countyInfo = cityInfo.children[common.getRandomNum(0, cityInfo.children.length - 1)];
            area.countyCode = countyInfo.label;
            area.countyValue = countyInfo.value;
        };
    };
    //所有省份的provinceValue
    area.proviceArr = areaList.map(val => val.value);
    return area;
};

//保存勋章json
basicJson.saveMedalJson = function (params = {}) {
    let json = {
        code: `xunC${common.getRandomStr(6)}`,
        name: `勋章${common.getRandomChineseStr(4)}`,
        logoDoc: docData.image.medal[0].docId,
        // ruleKind:,//规则类型(101准勋章；102快勋章；103新勋章；104多勋章)
        triggerType: 1,//触发类型(0自动；1手动)
        triggerPeriod: 0,//触发周期(0表示实时；1表示日；7表示周；30表示月)
        // showOrder:,//显示顺序
        // showFlag: 0,//显示标志，0-不可见 1-卖家可见 2-买家可见 3-卖家买家都可见  控制上新-推荐下拉筛选条件中勋章是否可见
        rem: '勋章备注' + common.getRandomStr(8),//
        // ver:,//乐观锁
        // flag:,//状态：(0-禁用 1-启用)
    };
    json = Object.assign(json, params);
    return json;
};

/**
 * 卖家设置爆款json
 * strategy 爆款设置策略  0-自动   1-手动
*/
basicJson.saveDresHotConfigJson = function (strategy) {
    let json = {
        period: 7  //7天循环/7天内销售最好的款号
    };
    if (strategy == 1) {
        json.strategy = 1;
    } else {
        json.strategy = 0;
        json.hotNum = 5;//效率最好的5个款号
    };
    return { config: json };
};

/**
 * 买家认证信息
 * @description 填写inviteCode直接通过审核
 */
basicJson.submitAuditJson = async function (params) {
    const docIdArr = common.randomSort(_.cloneDeep(docData.image.shopBackground)).map((data) => data.docId);
    // 主营类目需要从分类配置获取,从字典获取的无效
    const masterClassId = await spCommon.findCatConfig({ type: 7 }).then(res => {
        const catConfig = res.result.data['7'];
        // console.log(`catConfig=${JSON.stringify(catConfig)}`);
        return catConfig.find(val => val.flag == 1).typeValue;
    });

    let json = {
        shopName: `买家店铺${common.getRandomNumStr(5)}`,
        shopAddr: `滨江区${common.getRandomChineseStr(3)}路${common.getRandomNumStr(3)}号`,
        provCode: params.areaJson.provinceValue,
        cityCode: params.areaJson.cityValue,
        areaCode: params.areaJson.countyValue,
        headPic: docData.image.shopLogo[0].docId,
        contentPic: _.takeRight(docIdArr, 3).join(','),
        certPic: _.takeRight(docIdArr, 2).join(','),
        // inviteCode: 'autotest',//绑定业务员 徐世国
        remark: '信息属实，请审核！',
        masterClassId  //主营类目，现在是必填项了
    };
    return json;
};


basicJson.merchantJson = function (params = {}) {
    let bankInfo = spBank.bank[common.getRandomNum(0, spBank.bank.length - 1)];
    let json = Object.assign({
        mobile: LOGINDATA.mobile,
        settleAcctNo: bankInfo.settleAcctNo,//账户号（银行卡号，必选）
        settleAcctName: bankInfo.name,//账户名（必选）
        settleAcctType: common.getRandomNum(1, 2),//账户类型（1-个人账户;2-企业账户）（必选）
        BankName: '中信银行', //"银行名称（非中信账户必选，中信账户选填）",
        BankCode: 10469565,//"银行编码（选填，若非中信账户不填银行编码，置商户状态为人工审核状态）"
        detailAddr: `杭州市滨江区${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`,//商户地址（必选）
        // "mchName": "商户全称（不传则用原有商户全称）",
        contact: LOGINDATA.userName, //"申请人姓名",
        cncbFlag: 1,//"若为1代表为中信银行，若为2代表为非中信银行（必选）"
    }, params);
    return json;
};


/**
 * 卡券
 */
basicJson.cardCouponsJson = function () {
    // let json = {
    //     cardType,//	是	卡券大类：1优惠券、2礼品券、3运费券；取值调用ec-cardCoupons-dcodes&domain=coupTypeDomain
    //     quantity,//	是	库存数量
    //     color,//	是	卡券颜色；取值调用ec-cardCoupons - dcodes & domain=cardColorDomain
    //     shareBits,//	是	分享能力位：0没有能力 1可分享
    //     effectiveDate,//	是	生效时间(大于等于该时间) ，’yyyy-MM - dd’格式
    //     expiresDate,//	是	到期时间（小于等于该时间），’yyyy-MM - dd’格式
    //     receiveEffectPeriod,//	是	领取后有效时间，0表示无限制 其他表示具体数值
    //     receiveEffectPeriodUnit,//	否	领取后有效时间的单位，默认天：0年, 1月, 2天, 3时
    //     scopeType: 0,//卡券可见范围类型: 0普通券 1内部券
    //     getLimit: 1,	//领券限制，每个用户领券上限，如不填，则默认为1
    //     notice,	//	说明
    //     shareNotice,	//	分享说明
    //     fulfilValue,	//	是	达成金额
    //     execNum,	//	是	优惠金额
    //     domainKind,//	否	规则类型，先不传，后台自动填充
    //     // execType,//	否	先不传，后台自动填充；优惠类型:5满额减（cardType=1/3），9满额赠（cardType=2）
    //     execOther,//	是	cardType=2时代表赠送的物品id,格式如[{“id”:”赠送商品id”,num”:”赠送商品的数量”,”caption”:”赠送商品的名称”}]
    //     dims: [{// array	是	维度数组：每个成员代表一个维度，每个维度有下列三个元素；平台暂时只有一个成员；例子：分类：{“dimName”:”dimSpu”, “dimLevel”:”level_cat”, “dimValue”:”分类id”}，商品：{“dimName”:”dimSpu”, “dimLevel”:”level_spu”, “dimValue”:”商品id”}，门店：{“dimName”:”dimShop”, “dimLevel”:”level_shop”, “dimValue”:”店铺id”}
    //         dimName,//	是	维度名称：由ec-rule - get返回的数据，dimSpu
    //         dimValue,//	是	维度值：用户选择ec-dimension - codes & viewId=dimSpu返回的数据中的value
    //         dimValueCaption,//	是	维度值的中文说明：如款号名称、分类名称、门店名称
    //         dimLevel,//	是	维度层次：选择到dimValue时，有对应的levelName(level_spu/ level_cat)}],
    //     }]
    // };

    // return json;
};


// 推送消息长度没有限制
// 推送url格式也没有校验
/**
 * @param {Object} params
 * @param {Object} params.methodType 1 通知 2 命令
 * @param {Object} params.sendScopeType 1 全部买家，2 全部卖家 3 部分买家 4 部分卖家
 */
basicJson.spBroadcastTaskJson = function (params) {
    let json = {
        sendScope: params.sendScopeType == 1 ? common.getRandomNum(1, 2) : common.getRandomNum(3, 4),//类别 1 全部买家，2 全部卖家 3 部分买家 4 部分卖家
        hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`,
        msgContent: 'msgContent' + common.getRandomStr(10),
        msgTitle: 'msgTitle' + common.getRandomStr(10),
        msgDocId: docData.image.dres[0].docId,
        msgOutUrl: 'https://www.google.com',
        tenantIdList: [1],
        rem: 'autotest',//服务端处理,不推送自动化生成的测试消息
    };
    json.methodType = params.methodType || common.getRandomNum(1, 2);
    json.linkType = common.getRandomNum(0, 2);
    if (json.methodType == 2) {
        json.typeId = 4;//推送日志
    } else {
        json.typeId = json.linkType == 2 ? [2, 3, 5, 6][common.getRandomNum(0, 3)] : 1;
    }
    if ([3, 4].includes(json.typeId)) json.tenantIdList = [1, 2];
    if (json.typeId == 3) json.couponIdList = [1, 2];
    return json;
};

/**
 * 首页banner
 * @param {object} params
 * @param {string} params.linkType 链接类型，1代表外链，2代表内链
 * @param {string} params.tenantId
 */
basicJson.bannerJson = function (params = {}) {
    let json = {
        itemId: docData.image.banner[common.getRandomNum(0, docData.image.banner.length - 1)].docId,
        itemUrl: '',
        itemType: '1',
        itemSeq: common.getRandomNum(3, 10),
        bannerKind: '0',
        startDate: common.getCurrentDate(),
        endDate: common.getDateString([0, 0, 1]),
        linkType: params.linkType || common.getRandomNum(1, 2),
    };
    // 外链
    if (json.linkType == 1) {
        json.jumpLink = { url: 'http://www.hzecool.com' };
    };
    // 内链
    if (json.linkType == 2) {
        json.contentType = common.getRandomNum(1, 3);
        json.jumpLink = json.contentType != 3 ? { 'key': '' } : { 'tenantId': params.tenantId };
    };

    return json;
};

basicJson.spLabelItemJson = function (params) {
    return Object.assign({
        typeId: common.getRandomNum(1, 5),//类别id（1 季节 2 男女童等大类属性 3 颜色 4 板式 5 风格 待补充）
        labelName: common.getRandomStr(6),
        rem: `autotest${common.getRandomStr(3)}`,
        showOrder: common.getRandomNum(5, 20),
    }, params);
};

class BaseJson {
    /**
     * 更新参数
     * @param {object} data 更新字段
     */
    update(data) {
        return Object.assign(this, data)
    }
};

/**
 * 频道
 * @description name与flag必填 需要再加任意一个条件。无条件会报服务端错误
 */
class ChannelJson extends BaseJson {
    constructor() {
        super();
        this.id = '';
        this.name = `频道${common.getRandomStr(5)}`;
        /**是否启用（0：停用，1：启用） */
        this.flag = 1;
        /**频道类型 1.活动频道 2.分销商频道 3.第三方频道 */
        this.channelType = 2;
        /** 是否自动审核（0：否，1：是） */
        this.autoAudit = 1;
        /**商品加载类型 (0.动态加载 1.静态加载) */
        this.loadType = 0;
    }

    fullJson() {
        this.filterClassIds = '1,1013,1085,1093';//类别ids
        this.filterStyleIds = BASICDATA['2016'].slice(0, 5).map(val => val.codeValue).join(',');//风格ids
        this.filterMasterClassIds = BASICDATA['2010'].slice(0, 5).map(val => val.codeValue).join(',');//主营类目ids
        this.filterShopIds = '';//店铺ids
        this.filterPriceBegin = common.getRandomNum(50, 100);//价格区间从
        this.filterPriceEnd = common.getRandomNum(300, 500);//价格区间到
        this.filterStoreBeign = common.getRandomNum(50, 100);//库存从
        this.filterStoreEnd = common.getRandomNum(90000, 100000);//库存到
        this.filterShopScoreBegin = common.getRandomNum(1, 3);//铺评分从
        this.filterShopScoreEnd = common.getRandomNum(3, 5);//店铺评分到
        this.priceRatio = common.getRandomNum(0.5, 1.5, 1);//加价乘系数
        this.priceAddition = common.getRandomNum(50, 100);//系数后额外再加
        this.initStartTime = common.getDateString([0, 0, -7]);//商品上架时间
        return this;
    }

    /**
     * 详情期望值
     */
    fullExp() {
        let exp = { ruleParams: {} };
        Object.keys(this).forEach((key) => {
            if (['id', 'name', 'flag', 'initStartTime'].includes(key)) {
                exp[key] = this[key];
            } else {
                exp.ruleParams[key] = this[key];
            }
        });
        return exp;
    }

    /**
     * 列表期望值
     */
    listExp() {
        return {
            id: this.id,
            name: this.name,
            flag: this.flag,
            ruleParams: this
        };
    }
};
basicJson.getChannelJson = () => new ChannelJson();

/**
 * 分销商
 */
basicJson.SpDisInfo = class extends BaseJson {
    constructor() {
        super();
        this.name = '';//分销商名称
        this.provCode = '';//省
        this.cityCode = '';//市
        this.areaCode = '';//县区
        this.detailAddr = '';//详细地址
        this.channelId = '';//频道
        this.mobile = ``;//联系电话
    }

    /**
     * 随机生成一个分销商信息
     */
    async randomInit() {
        const areaInfo = await basicJson.getAreaJson();
        const channelInfo = await spdchg.getChannelList({ flag: 1 }).then(res => res.result.data.rows.shift());
        this.name = `分销商${common.getRandomStr(5)}`;
        this.provCode = areaInfo.provinceValue;
        this.cityCode = areaInfo.cityValue;
        this.areaCode = areaInfo.countyValue;
        this.detailAddr = `${common.getRandomChineseStr(2)}路${common.getRandomNumStr(3)}号`;
        this.channelId = channelInfo.id;
        this.mobile = common.getRandomMobile();
        this.profitPercent = common.getRandomNum(0, 1, 2);
        this.refundPercent = common.getRandomNum(0, 1, 2);
        return this;
    }

    /**
     * 分销商保存
     * @param {object} params
     */
    async saveSpDis(params) {
        if (!params) params = await this.randomInit();
        const saveRes = await spdchg.saveSpDis(params);
        this.id = saveRes.result.data.val;
        this.flag = 1;
    }

};

/**
 * 分销商商户
 */
class SpDisMerchantInfo extends BaseJson {
    constructor(spDisInfo) {
        super();
        const bankInfo = spBank.bank[common.getRandomNum(0, spBank.bank.length - 1)];
        this.mobile = spDisInfo.mobile;//手机号(必选)，不传就用当前登录用户的手机号；也作为结算账户的联系电话*
        this.detailAddr = spDisInfo.detailAddr;//详细地址，也作商户地址
        this.mchAbbr = `销${spDisInfo.name.slice(-5)}`;//商户简称，不填用全称，全称是name
        this.contact = common.getRandomChineseStr(3);//结算账户的联系人*
        this.settleAcctNo = bankInfo.settleAcctNo;//结算账户号：如银行卡号*
        this.settleAcctName = bankInfo.name;//结算账户名称*
        this.settleAcctType = 1;//账户类型。1-个人账户;2-企业账户
        // this.bankName = `中信银行${spDisInfo.detailAddr.slice(0, 2)}分行`;//结算账户银行名称，账户类型>2非中信账户必填,中信账户选填
        // this.bankCode = '';//结算账户银行编码，账户类型>2选填
        this.cncbFlag = 1;//若为1代表为中信银行，若为2代表为非中信银行
        this.disId = spDisInfo.id;//分销商id*
    }

};
basicJson.getSpDisMerchantJson = (spDisInfo) => new SpDisMerchantInfo(spDisInfo);

/**
 * 分类配置
 */
basicJson.CatConfig = class extends BaseJson {
    constructor() {
        super();
        this.id = '';
    }

    /**
     * 保存分类配置
     */
    async saveCatConfig(params) {
        const saveRes = await spCommon.saveCatConfig(params);
        this.id = saveRes.result.data.val;
        this.update(params);
        if (!this.hasOwnProperty('flag')) this.flag = 1;
    }

    /**
     * 删除分类配置
     */
    async delCatConfig() {
        await spCommon.delCatConfig({ id: this.id });
    }

    /**
     * 获取配置信息
     */
    getInfo() {
        if (this.hasOwnProperty('prop')) this.extProp = this.prop;
        return format.dataFormat(this, Object.keys(this).join(';'));
    }

};

/**
 * 分类配置-商圈
 */
basicJson.CatConfigTrade = class extends basicJson.CatConfig {
    constructor() {
        super();
        this.id = '';
        this.marketList = [];
    }

    /**
    * 关联市场与商圈
    */
    async relateMarketAndTrade({ market }) {
        await spCommon.relateMarketAndTrade({ marketId: market.id, tradeId: this.id });
        this.marketList.push(market);
        this.typeAccode = `.${this.id}.`;
        this.typeValue = 0;
    }
};

basicJson.saveLiveTaskJson = () => {
    return {
        liveType: 0,
        title: `直播主题${common.getRandomStr(5)}`,
        actLabel: `直播`,
        description: `直播描述${common.getRandomStr(5)}`,
        coverUrl: docData.image.shopBackground[0].docUrl,
        template_flag: 0,
        startDate: moment().subtract(5, 'm').format('Y-MM-DD HH:mm:ss'),
        endDate: moment().add(5, 'hours').format('Y-MM-DD HH:mm:ss'),
        robotNum: common.getRandomNum(0, 1000),
        spus: ''
    };
};
