const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');

class DresSpuGroup {
    constructor() {
        /**  主键id */
        this.id = '';
        /**  分组名称 */
        this.name = '';
        /**  展示位 1货品详情 2购物车 3我的 */
        this.showPlace = 0;
        /**  是否为系统默认 1默认 */
        this.defaultGroup = 0;
        /**  状态，0：删除，1：正常 */
        this.flag = 1;
        /**  父分组id */
        this.parentId = '';
        /**  备注，描述 */
        this.rem = '';
        /**  单元id */
        this.unitId = '';
        // this.tenantId
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';

        /** 货品id集合 */
        this.dresSpuIds = new Set();
    }

    /**
     * 添加商品分组
     * @param {object} params 
     */
    async addGroup(params) {
        if (this.id) params.id = this.id;
        const res = await ss.spdresb.addGroup(params);
        // console.log(`res=${JSON.stringify(res)}`);

        common.update(this, res.result.data);
        if (params.dresSpuIds) {
            this.dresSpuIds.clear();
            params.dresSpuIds.split(',').forEach(spuId => this.dresSpuIds.add(spuId));
        }
        return this;
    }

    /**
     * 删除商品分组
     */
    async deleteGroup() {
        await ss.spdresb.deleteGroup({ groupId: this.id });
        this.flag = 0;
        this.dresSpuIds.clear();
        return this;
    }

    /**
     * 获取分组详情
     */
    async findGroupInfo() {
        if (!this.id) throw new Error(`未初始化货品分组id,请检查`);

        const res = await ss.spdresb.findDresSpuGroupInfo({ id: this.id });
        return res.result.data;
    }

    /**
     * 从列表获取本分组信息
     */
    async findGroupInfoFromList() {
        if (!this.id) throw new Error(`未初始化货品分组id,请检查`);
        const res = await ss.spdresb.listGroup();
        const groupInfo = res.result.data.rows.find(ele => ele.id == this.id);
        expect(groupInfo, `查询分组列表未找到分组信息 id=${this.id}`).not.to.be.undefined;
        return groupInfo;
    }

    /**
     * 更新分组展示位
     * @param {object} params 
     * @param {string} params.showPlace 
     */
    async updateShowPlace(params) {
        await ss.spdresb.updateDresSpuGroupShowPlace({ id: this.id, showPlace: params.showPlace });
        this.showPlace = params.showPlace;
        return this;
    }

    getListExp() {
        return { createdBy: this.createdBy, createdDate: this.createdDate, defaultGroup: this.defaultGroup, flag: this.flag, id: this.id, name: this.name, parentId: this.parentId, rem: this.rem, showPlace: this.showPlace, unitId: this.unitId, updatedBy: this.updatedBy, updatedDate: this.updatedDate };
    }
    /**
     * 获取明细期望值
     */
    getDetailExp() {
        const dresSpuIds = [...this.dresSpuIds];
        return { dresSpuIds: dresSpuIds.join(','), flag: this.flag, name: this.name, dresSpuCount: dresSpuIds.length, rem: this.rem, showPlace: this.showPlace, parentId: this.parentId };
    }
};

const spuGroup = module.exports = {};

spuGroup.setupDresSpuGroup = function (params) {
    return new DresSpuGroup();
};



