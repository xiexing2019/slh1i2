
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');

const dresSpecs = module.exports = {};


/**
 * 用户类别属性
 */
class Specs {
    constructor() {
        /**  ID */
        this.id = '';
        /**  所属单元 */
        this.unitId = '';
        /**  款号类别，0 指定适用于所有类别的属性（功能预留） */
        this.classId = '';
        /**  访问编码（预留） */
        this.accode = '';
        /**  类型，1-SPU规格属性，2-SPU扩展属性(json属性里)，3-SKU规格(允许差异，没有设置跟spu等同) 4-SPU字段属性属性(直接在spu表中有个字段) */
        this.typeId = 0;
        /**  字段名 */
        this.fieldName = '';
        /**  属性名(前后端交互) */
        this.name = '';
        /**  标题(前端显示) */
        this.caption = '';
        /**  录入类型 1 输入，2 单选，3 多选 */
        this.inputType = 2;
        /**  数据类型 1 String，2 Integer 3 Long 4 BigDecimal，5 Date */
        this.dataType = 2;
        /**  字典类型 */
        this.dictTypeId = '';
        /**  显示顺序 */
        this.showOrder = 0;
        /**  ES属性名 */
        this.esName = '';
        /**  是否为ES搜索属性，0 不是 1 是 */
        this.esSearchFlag = 0;
        /**  备注 */
        this.rem = '';
        /**  状态 0 无效 1 有效 -1 删除 */
        this.flag = 0;
        /**  同步状态 0-自定义 1 标准类目属性数据初始化 */
        this.defaultFlag = 0;
    }

    /**
     * 货品规格属性新增
     * @param {*} params 
     */
    async saveFull(params) {
        const res = await ss.spdresb.saveSpecsFull(params);
        // console.log(`res=${JSON.stringify(res)}`);

        //  更新属性值
        const data = res.result.data;
        common.update(this, data);
        this.unitId = LOGINDATA.unitId;
        this.caption = params.propCaption;
        this.fieldName = this.name = data.propsCode;
        this.defaultFlag = data.isDefaultFlag;
        return this;
    }

    /**
     * 启用规格属性
     */
    async ableSpecs() {
        await ss.spdresb.ableSpecsProps({ id: this.id });
        this.flag = 1;
    }

    /**
     * 禁用规格属性
     */
    async disableSpecs() {
        await ss.spdresb.disableSpecsProps({ id: this.id });
        this.flag = 0;
    }

    /**
     * 货品规格属性列表
     */
    async getFullFromList(params = {}) {
        const res = await ss.spdresb.findSpecsList({ propCaption: this.caption, typeId: this.typeId, ...params });
        // console.log(`res=${JSON.stringify(res)}`);
        const data = res.result.data.rows.find(row => row.id == this.id);
        expect(data).not.to.be.undefined;
        return data;
    }

    /**
     * 生成期望值
     */
    getExp() {
        return { dictTypeId: this.dictTypeId, flag: this.flag, id: this.id, isDefaultFlag: this.defaultFlag, propCaption: this.caption, propsCode: this.name, typeId: this.typeId };
    }

};

dresSpecs.setupDresSpecs = function () {
    return new Specs();
};


