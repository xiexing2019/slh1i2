const common = require('../../lib/common');
const moment = require('moment');

let esSearchHelp = module.exports = {};

/**
 * @description 数组排序验证
 * @param {Object} targetArr 目标数组
 * @param {String} key 需要排序的字段
 * @param {Boolean} orderByDesc 是否降序排，如果降序传true，升序传false
 */
esSearchHelp.sortAssert = (targetArr, key, orderByDesc) => {
    if (orderByDesc) {
        for (let i = 0; i < targetArr.length - 1; i++) {
            let flag = targetArr[i][key] >= targetArr[i + 1][key];
            if (!flag) {
                throw new Error(`排序错误:\n   current:{ id:${targetArr[i].id},name:${targetArr[i].name},marketDate:${targetArr[i].marketDate} }\n   next:{ id:${targetArr[i + 1].id},name:${targetArr[i + 1].name},marketDate:${targetArr[i + 1].marketDate} }`);
            };
        }
    } else {
        for (let i = 0; i < targetArr.length - 1; i++) {
            let flag = targetArr[i][key] <= targetArr[i + 1][key];
            if (!flag) {
                throw new Error(`排序错误:\n   current:{ id:${targetArr[i].id},name:${targetArr[i].name},marketDate:${targetArr[i].marketDate} }\n   next:{ id:${targetArr[i + 1].id},name:${targetArr[i + 1].name},marketDate:${targetArr[i + 1].marketDate} }`);
            };
        };
    };
};

/**
 * 搜索接口排序校验
 * @param {Object} params
 * @param {array} params.dataList 
 * @param {string} params.path 列表字段路径
 * @param {Boolean} orderByDesc 是否降序排，如果降序传true，升序传false
 * @param {Function} params.orderRule 排序规则 orderRule会覆盖orderByDesc
 */
esSearchHelp.orderAssert = (params) => {
    let eleArr = params.dataList.map(data => _.get(data, params.path, ''));
    if (common.stringToDate(eleArr[0]) != 'Invalid Date') {
        eleArr = eleArr.map(ele => common.stringToDate(ele));
    }
    let sortArr = _.cloneDeep(eleArr).sort((a, b) => {
        if (typeof params.orderRule == 'function') return params.orderRule(a, b);
        if (params.orderByDesc) return b - a;
        return a - b;
    });
    // expect(eleArr).to.eql(sortArr, `排序错误:\nexpected:[${sortArr}]\nactual:[${eleArr}]\n`);
    common.isApproximatelyEqualAssert(sortArr, eleArr, [], `排序错误:\nexpected:[${sortArr}]\nactual:[${eleArr}]\n`);
};

esSearchHelp.ShowCaption = class {

    /**
     * 
     * @param {Number} specialFlag  
     * @param {Boolean} hotFlag 
     * @param {Boolean} newDresFlag 
     */
    setElement(specialFlag, hotFlag, newDresFlag) {
        this.specialFlag = specialFlag;
        this.hotFlag = hotFlag == true ? 1 : 0;
        this.newDresFlag = newDresFlag == true ? 1 : 0;
    }

    getElement() {
        return this.calculate();
    };

    calculate() {
        let specialNum = common.mul(this.specialFlag, 8);
        let hotNum = common.mul(this.hotFlag, 4);
        let newDresNum = common.mul(this.newDresFlag, 16);
        return specialNum + hotNum + newDresNum;
    }
};

/**
 * 首页背景图方法列表中找出最大的版本号
 */
esSearchHelp.maxVersion = function (arr) {
    let max = arr[0];
    for (let i = 0; i < arr.length - 1; i++) {
        max = getMax(max, arr[i + 1]);
    };
    return max;
};

/**
 * 获取两个版本号中大的那个版本号
 */
function getMax(num1, num2) {
    let arr1 = num1.productVer.split('.');
    let arr2 = num2.productVer.split('.');
    let max;
    let minLen = Math.min(arr1.length, arr2.length);

    for (let i = 0; i < minLen; i++) {
        if (parseInt(arr1[i]) < parseInt(arr2[i])) {
            max = num2;
            break;
        } else if (parseInt(arr1[i]) > parseInt(arr2[i])) {
            max = num1;
            break;
        };
    };
    return max;
};




