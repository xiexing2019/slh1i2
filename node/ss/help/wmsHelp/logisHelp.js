
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const common = require('../../../lib/common');

class LogisHelp {
    constructor() {


        this.upJson = {
            number: `test${Date.now()}${common.getRandomNum(100, 999)}`,
            company: 'shentong',
        };
    };

    async upLogisStatus(params) {
        this.getUpdateJson(params);
        let res = await spconfb.changeLogisStatus(this.upJson);
        return res;
    };

    getUpdateJson(params) {
        this.upJson.monitorStatus = this.getMonitorStatus(params.status);
        this.upJson.status = params.status;
        this.upJson.track = this.getTrackByStatus(params.status)
    };

    getLogisJson() {
        return this.upJson;
    };

    getMonitorStatus(status) {
        if ([-1, 0, 1, 2, 4, 5, 6].indexOf(status) > -1) {
            return 'polling';
        } else {
            return 'shutdown';
        };
    };

    getTrackByStatus(status) {
        let traces = [
            {
                time: '2018-10-27 19:20:13',
                ftime: '2018-10-27 19:22:13',
                context: '【杭州市】 快件已在 【杭州下沙区】 签收, 签收人: 本人，前台，同事, 如有疑问请电联:15267466799 / 0571-87111787, 您的快递已经妥投, 如果您对我们的服务感到满意, 请给个五星好评, 鼓励一下我们【请在评价快递员处帮忙点亮五颗星星哦~】',
            }, {
                time: '2018-10-27 09:54:54',
                ftime: '2018-10-27 09:54:54',
                context: '【杭州市】 【杭州下沙区】 的财通中心（15267466799） 正在第1次派件, 请保持电话畅通,并耐心等待',
            }, {
                time: '2018-10-27 09:54:54',
                ftime: '2018-10-27 09:54:54',
                context: '【嘉兴市】 快件到达 【杭州中转部】',
            }, {
                time: '2018-10-26 12:23:14',
                ftime: '2018-10-26 12:23:14',
                context: '【北京市】 快件到达 【北京】',
            }, {
                time: '2018-10-26 08:22:31',
                ftime: '2018-10-26 08:22:31',
                context: '【北京市】 快件离开 【北京市场二部】 发往 【北京】',
            }, {
                time: '2018-10-25 21:31:31',
                ftime: '2018-10-25 21:31:31',
                context: '【北京市】【北京市场二部】(010 - 56998340、010 - 56998360、010 - 56998367）的互动（13593510454） 已揽收',
            }, {
                time: '2018-10-25 15:33:46',
                ftime: '2018-10-25 15:33:46',
                context: '快递已打包，等待揽件',
            }];
        switch (status) {
            case -1:
                return [];
            case 1:
                return traces.splice(-2);
            case 0:
                return traces.splice(-5);
            case 5:
                return traces.splice(-6);
            case 3:
                return traces.splice(-7);
        };
    };
};

module.exports = { LogisHelp };
