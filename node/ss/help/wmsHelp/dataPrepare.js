const spCommon = require('../../../reqHandler/sp/global/spCommon');
const ssAccount = require('../../data/ssAccount');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const common = require('../../../lib/common');
const wmsWarehouse = require('../../../reqHandler/sp/wms/wmsWarehouse');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const format = require('../../../data/format');
const ssReq = require('../ssReq');
const spugr = require('../../../reqHandler/sp/global/spugr');

class PrepareData {

    constructor() {
        this.marketId = '';
        this.catConfigId = '';
        this.wareHouseId = '';
        this.receiveProv = '';
    }

    /**
     * 检测中心区是否有市场auto，如果没有，则添加
     */
    async checkMarket() {
        let res = await spConfig.getMarketList({ shortNameLike: ssAccount.seller.market });
        let info = res.result.data.rows.find(val => val.fullName == ssAccount.seller.market);
        if (info == undefined) {
            let res = await spConfig.saveMarket(this.addMarketJson());
            this.marketId = res.result.data.val;
        } else {
            this.marketId = info.id;
        };
        await spConfig.enableMarket({ id: this.marketId, check: false });   //强行启用市场 
    };

    addMarketJson() {
        let json = {
            fullName: ssAccount.seller.market,
            shortName: ssAccount.seller.market,
            floorsOverGround: 5,
            floorsUnderGround: 2,
            provCode: 330000,
            cityCode: 330100,
            areaCode: 330104,
        };
        return json;
    };

    /**
     * 检测分类配置中的市场
     */
    async checkConfigMarket() {
        let res = await spCommon.getCatConfigList({ type: 3, typeName: ssAccount.seller.market }).then(res => res.result.data.rows);
        let info = res.find(val => val.typeId == this.marketId); //分类配置里面找 auto市场
        if (info == undefined) {
            let res = await spCommon.saveCatConfig({
                typeName: ssAccount.seller.market,
                typeId: this.marketId,
                showFlag: 3,//买家卖家可见
                showOrder: common.getRandomNum(10, 20),
                seqOrder: common.getRandomNum(10, 20),
                type: 3,
                prop: { "tradeId": 14163 },   //商圈暂且写死
            });
            this.catConfigId = res.result.data.val;
        } else {
            this.catConfigId = info.id;
        };
    };
    //检查仓库，根据name和id判断
    async checkWareHouse() {
        let res = await wmsWarehouse.getWareHouseList({ pageSize: 50 }).then(res => res.result.data.rows);   //找auto仓库这个仓库
        let info = res.find(val => val.name == ssAccount.wareHouse.name);
        if (info == undefined) {
            let wareHouseInfo = res.find(val => val.id == ssAccount.wareHouse.id);
            if (wareHouseInfo == undefined) {
                await wmsWarehouse.saveWareHouse(this.wareHouseJson());
            } else {
                let json = this.wareHouseJson();
                json.id = ssAccount.wareHouse.id;
                await wmsWarehouse.updateWareHouse(json);
            };
        };
        this.wareHouseId = ssAccount.wareHouse.id;
    };

    wareHouseJson() {
        let json = {
            name: ssAccount.wareHouse.name,
            mobile: '12989898989',
            provCode: '330000',
            cityCode: '330100',
            areaCode: '330104',
            address: '杭海路88号四季青服装市场',
            streetCode: '杭海路88',
            insteadDeliverFlag: '1'
        };
        return json;
    };
    /**
     * 市场绑定仓库，确保auto市场绑定 auto仓库
     */
    async checkMarketWh() {
        let res = await wmsWarehouse.getWareHouseMarket({ marketName: ssAccount.seller.market, flag: 1 }).then(res => res.result.data.rows);
        let info = res.find(val => val.marketId == this.marketId);
        if (info == undefined) {
            await wmsWarehouse.bindMarketToWh({ whTenantId: this.wareHouseId, marketId: this.marketId });
        } else {
            await wmsWarehouse.updateWareHouseMarket({ id: info.id, whTenantId: this.wareHouseId, marketId: this.marketId })
        };
    };

    /**
     * 市场运费设置
     */
    async marketFee() {
        let res = await spCommon.findFeeRuleList({ feeType: 2, fromWarehouse: 1, startProvCode: this.wareHouseId }).then(res => res.result.data.rows);
        let info = res.find(val => val.startProvCode == this.wareHouseId);
        if (info == undefined) {
            await spCommon.saveFeeRule(this.feeJson());
        } else {
            await spCommon.deleteFeeRule({ feeType: 2, startProvCode: this.wareHouseId });
            await spCommon.saveFeeRule(this.feeJson());
        };
        this.receiveProv = this.feeJson().rules[0].provIds;
    };

    feeJson() {
        let json = {
            feeType: 2,
            startProvCode: this.wareHouseId,
            rules: [{
                feeType: 2,
                provIds: 110000,
                startNum: common.getRandomNum(2, 3),
                startFee: common.getRandomNum(3, 5),
                addNum: 1,
                addFee: 1,
                fromWarehouse: 1
            }]
        };
        return json
    };

    /**
     * 卖家运费设置
     */
    async sellerFee() {
        let res = await spCommon.findFeeRuleInfo({ feeType: 2, startProvCode: this.wareHouseId }).then(res => res.result.data.rows);
        let feeJson = format.dataFormat(res[0], 'feeType;provIds;startNum;startFee;addNum;addFee')
        feeJson.addFee += 2;
        let addFeeJson = {
            feeType: 2,
            feeRules: [feeJson],
            freeType: 0,
            freeRules: []
        };
        await ssReq.spSellerLogin();
        addFeeJson.startProvCode = await spugr.getShopDetail().then(res => res.result.data.provCode);

        await spconfb.saveShipRule(addFeeJson);
        await ssReq.spSellerLogin({ shopName: '中洲店' });
        await spconfb.saveShipRule(addFeeJson);
    };

    async dataPrepare() {
        await this.checkMarket();
        await this.checkConfigMarket();
        await this.checkWareHouse();
        await this.checkMarketWh();
        await this.marketFee();
        await this.sellerFee();
    };
};



module.exports = { PrepareData };