const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const EventEmitter = require('events').EventEmitter;

class Seller {
    constructor(params) {
        this.mobile = params.mobile;
        /** 集群编码 */
        this.clusterCode = params.clusterCode || '';
        /** 租户id */
        this.tenantId = params.tenantId || '';
        /** 单元id */
        this.unitId = '';
        /** 单元id */
        this.userId = params.userId || '';
        this.appKey = '';
        this.appSecret = '';
        /** 门店列表 */
        this.shopMap = new Map();
    }

    async setShopDetail(shopName) {
        const shopDetail = await ss.spugr.getShopDetail().then(res => res.result.data);

        if (this.shopMap.has(shopName)) {
            const sellerInfo = _.cloneDeep(LOGINDATA);
            const shop = Object.assign(this.shopMap.get(shopName), sellerInfo, shopDetail);
            this.shopMap.set(shopName, shop);
        } else {
            throw new Error(`列表未找到'${shopName}',请检查`);
        }

        return this;
    }

    async getShopList() {
        const shopList = await ss.spugr.userShopList({ userId: LOGINDATA.userId }).then(res => res.result.data.rows);
        // console.log(shopList);

        shopList.forEach((shop) => this.shopMap.set(shop.ecCaption.shopTenantId, shop));

        return this;
    }

    /** 切换当前门店 */
    async changeUserShop(shopName) {
        if (this.shopMap.size == 0) {
            await this.getShopList();
        }
        // console.log(this.shopMap);

        if (this.shopMap.has(shopName)) {
            const unitId = this.shopMap.get(shopName).unitId;
            if (unitId == LOGINDATA.unitId) return this;
            await ss.spugr.changeUserShop({ mobile: this.mobile, newUnitId: unitId });
        } else {
            throw new Error(`卖家登录,切换企业租户失败. 用户关联的用户列表未找到'${shopName}',请检查`);
        }
        return this;
    }

    async getAppKeyAndAppSecret() {
        this.appKey = '';
        this.appSecret = '';
    }

    /** 
     * 获取accessToken
     * @description 更新值至LOGINDATA
     */
    async refreshToken() {
        await ss.op.refreshToken({ appKey: this.appKey, appSecret: this.appSecret });
    }




}

class SellerShop extends EventEmitter {
    constructor(params) {
        super();
        this.on('changeSS', (params) => {

        });
    }


}

module.exports = Seller;