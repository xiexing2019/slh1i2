const common = require('../../../lib/common');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const basicJson = require('../basicJson');

/** 消息推送任务 */
class SpBroadcastTask {
    constructor(params) {
        /**  租户ID */
        this.id = '';
        /**  消息类型 */
        this.typeId = 0;
        /**  开始推送时间 */
        this.startTime = '';
        /**  推送范围 */
        this.sendScope = 0;
        /**  推送租户ids */
        this.tenantIds = {};
        /**  推送的消息文本 */
        this.msgContent = '';
        /**  推送的图片id */
        this.msgDocId = '';
        /**  推送的内部链接 */
        this.msgInnerUrl = '';
        /**  推送的外部链接 */
        this.msgOutUrl = '';
        /**  优惠券id列表 */
        this.couponIds = {};
        /**  需推送数 */
        this.needSendNum = '';
        /**  已推送数 */
        this.haveSendNum = '';
        /**  备注 */
        this.rem = '';
        /**  状态 */
        this.flag = 0;
        /**  防重复提交 */
        this.hashKey = '';
        /**  创建人 */
        this.createdBy = '';
        /**  创建时间 */
        this.createdDate = '';
        /**  修改人 */
        this.updatedBy = '';
        /**  修改时间 */
        this.updatedDate = '';
        /**  推送的消息标题 */
        this.msgTitle = '';
        /**  通知方式 1 通知 2 命令 3 弹窗*/
        this.methodType = 0;
        /**  链接方式 0 无消息 1 外链 2 内链 */
        this.linkType = 0;
        /**  扩展属性 */
        this.props = {};
    }

    async saveTask(params) {
        // methodType=3时不能修改 流程中单独校验
        if (this.id && this.methodType == 3) {
            return;
        }

        const res = await spCommon.saveSpBroadcastTask(params);
        common.update(this, params);
        this.id = res.result.data.val;
    }

    async stopTask() {
        await spCommon.stopSpBroadcastTask({ id: this.id });
        // this.flag=
    }

}

module.exports = SpBroadcastTask;