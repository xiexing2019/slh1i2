const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const uuid = require('uuid');

class SmsTemplate {
    constructor() {
        this.id = '';
        /**  短信模板名称，用户自定义，可不填 */
        this.templateName = '';
        /**  短信模板内容 */
        this.templateContent = '';
        /**  模板业务类型：1、未到店；2、生日当日；3、生日当月首日 */
        this.bizType = 0;
        /**  定义类型：1、系统模板；2、用户模板 */
        this.defType = 0;
        /**  系统的时候为0，用户时为用户公司的GUID */
        this.guid = '';
        /**  状态0:未启用，1：已启用 */
        this.status = 0;
        // /**  创建时间 */
        // this.createdDate = '';
        // /**  创建人 */
        // this.createdBy = '';
        // /**  更新时间 */
        // this.updatedDate = '';
        // /**  更新人 */
        // this.updatedBy = '';
        /**  短信平台模板ID */
        this.platformTemplateId = '';
        /**  短信平台代码 */
        this.platformCode = '';
        /**  模板编号 */
        this.templateCode = '';
        /**  产品code，用于区分商城， 一代，二代 */
        this.productCode = '';

        /** 状态 -1:删除 为物理删除 */
        this.flag = 1;
    }

    /**
     * 短信模版添加/修改
     * @param {*} params 
     */
    async saveTemplate(params) {
        const res = await ss.smsTemplateDefinition.saveTemplate({ id: this.id, ...params });
        console.log(res);
        await common.delay(200);
        common.update(this, params);
        this.id = res.result.data.val;
        return this;
    }

    /**
     * 短信模版查询
     * @param {*} params 
     */
    async getFromList(params = {}) {
        const res = await ss.smsTemplateDefinition.findTemplateList({ bizType: this.bizType, productCode: this.productCode, guid: this.guid, templateCode: this.templateCode, statusGte: 0, ...params });
        console.log(`getFromList=${JSON.stringify(res)}`);
        return res.result.data.rows.find(ele => ele.id == this.id);
    }

    /**
     * 短信模版查询校验 断言
     * @param {*} params 
     */
    async getFromListAssert(params = {}) {
        const data = await this.getFromList(params);
        // 删除为物理删除 
        if (this.flag == -1) {
            expect(data, `短信模版查询 查询到被删除的模板 id=${this.id}`).to.be.undefined;
            return;
        }

        expect(data, `短信模版查询 未查询到模板id=${this.id}`).not.to.be.undefined;
        common.isApproximatelyEqualAssert(this, data, ['templateContent', 'platformTemplateId']);
        expect(data.templateContent).to.match(/[this.templateContent]/);
    }

    /**
     * 删除模板
     */
    async deleteTemplate() {
        await ss.smsTemplateDefinition.deleteTemplate({ id: this.id });
        this.flag = -1;
    }
};

const smsManage = {};

/**
 * 初始化短信模板
 */
smsManage.setupSmsTemplate = () => new SmsTemplate;

/**
 * 短信充值
 */
smsManage.saveFinAcctFlowInfo = async function (userInfo) {
    return ss.fin.saveFinAcctFlowInfo({
        unitId: userInfo.unitId,
        tenantId: userInfo.tenantId,
        acctType: 1302,// 短信余额
        name: '购买短信包充值',
        amount: 50,
        bizType: 1204,
        bizOrderId: common.getRandomNumStr(5),
        ioFlag: 0,
        tradeType: 200,
        hashKey: `${Date.now()}${uuid.v4()}`
    });
};

/**
 * 群发短信
 * @description 
 * @param {object} params
 * @param {object} params.smsTemplate SmsTemplate
 */
smsManage.sendGroupSms = async function (params) {
    const res = await ss.spugr.sendGroupSms({ mobiles: ['13750850013', '18829897169'], templateCode: params.smsTemplate.templateCode, param: { code: LOGINDATA.shopName } });
    console.log(`res=${JSON.stringify(res)}`);

};

module.exports = smsManage;