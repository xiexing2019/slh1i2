const common = require('../../lib/common');
const spCaps = require('../../reqHandler/sp/spCaps');
const sp = require('../../reqHandler/sp');
const ss = require('../../reqHandler/ss');


class Employee {
    constructor() {
        // /**  ID  */
        // this.id = '';
        // /**  单元id  */
        // this.unitId = '';
        /**  用户  */
        this.userId = '';
        /**  姓名  */
        this.name = '';
        /**  手机  */
        this.phone = '';
        /**  角色  */
        this.roleId = '';
        // /**  创建人  */
        // this.createdBy = '';
        // /**  创建时间  */
        // this.createdDate = '';
        /**  角色编码  */
        // this.roleCode = '';
        // /**  删除标志  */
        // this.flag = 0;
        /**  备注  */
        this.rem = '';
        /**  转让  */
        this.transferable = 0;
        /**  移交  */
        this.deliverable = 0;
        /**  关联客户列表  */
        this.clientList = new Map();
        /**  关联客户人数  */
        this.clientCount = 0;
    }

    /**
     * 新建员工
     * @param {object} params 
     */
    async newOrEditStaff(params) {
        const newOrEditStaff = await ss.spmdm.newOrEditStaff(params);
        console.log(`打印编辑请求=${JSON.stringify(newOrEditStaff)}`);
        this.name = newOrEditStaff.params.userName;
        this.phone = newOrEditStaff.params.mobile;
        this.rem = newOrEditStaff.params.rem;
        this.roleId = newOrEditStaff.params.roleId;
        !this.userId && (this.userId = newOrEditStaff.result.data.userId);
        await common.delay(1000);
    }

    /**
     * 查询员工列表
     * @param {object} params 
     */
    async getStaffList() {
        const staff = await ss.spmdm.getStaffList().then(res => res.result.data.generalStaff.find(obj => obj.phone == this.phone));
        return staff;
    }

    /**
     * 查询员工
     * @param {object} params 
     */
    async getStaffInfo() {
        const staffInfo = await ss.spmdm.getStaffInfo({ userId: this.userId, roleId: this.roleId }).then(res => res.result.data);
        this.transferable = staffInfo.transferable;
        this.deliverable = staffInfo.deliverable;
        return staffInfo;
    }

    /**
     * 移交
     * @param {object} params 
     * @param {object} params.userId 转移用户（客户增加者） 
     */
    async deliverStaff(params) {
        await this.getStaffInfo();
        if (this.deliverable) {
            await ss.spmdm.deliverStaff({ deliverUserId: this.userId, ...params });
            this.clientList.forEach(client => this.clientList.delete(client.id));
        }
    }

    /**
     * 管理员转让
     * @param {object} params 
     * @param {object} params.userId 用户id 
     */
    async transferStaff(params) {
        await this.getStaffInfo();
        if (this.transferable) {
            await ss.spmdm.transferStaff({ transferStaff: this.userId, userId: params.userId });
        }
    }

    /**
     * 删除
     * @param {object} params 
     */
    async deleteStaff(params) {
        return ss.spmdm.deleteStaff({ userId: this.userId, roleId: this.roleId, ...params });
    }

    /**
     * 关联客户列表
     * @param {object} params 
     */
    async getClientListForEmployee() {
        const clientListForEmployee = await ss.spmdm.getClientListForEmployee({ userId: this.userId }).then(res => res.result.data.rows);
        // expect(clientListForEmployee, '关联客户列表为空').to.not.be.undefined;
        // console.log(`\n clientListForEmployee=${JSON.stringify(clientListForEmployee)}`);
        if (clientListForEmployee) {
            clientListForEmployee.forEach((client, index) => {
                client.id = clientListForEmployee[index].id;
                this.updateClient(client);
            });
        }
        return clientListForEmployee;
    }

    /**
     * 修改客户信息
     * @param {object} params
     * @param {object} params.
     * @param {object} params.nickName "昵称",
     * @param {object} params.avatar "头像id",
     * @param {object} params.birthday": "生日",
     * @param {object} params.birthLunarFlag": "农历生日标志；1表示birthday是农历生日",
     * @param {object} params.gender "性别；1男2女0其他"
     */
    async updateUser(params) {
        const defaultJson = {
            nickName: common.getRandomStr(5),
            avatar: "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLoNkBtbatvibCZpyZjolZZdqU80leVADU04SXRmOibYliagoyjpR1Iaer60M5icQ3xfiaf9IW0NF6iafjw/132",
            brithday: '1970-1-1',
            birthLunarFlag: 1,
            gender: 1,
        }
        const updateParams = Object.assign(defaultJson, params);
        await sp.spugr.updateUser(updateParams);
        console.log(`defaultJson=${JSON.stringify(updateParams)}`);

        // const clientSupplement = {
        //     id: params.id,
        //     nickName: params.nickName || defaultJson.nickName,
        //     avatar: params.avatar || defaultJson.avatar,
        //     // nickName: params.nickName || this.clientList.get(params.id).nickName,
        //     // avatar: params.avatar || this.clientList.get(params.id).avatar,
        //     birthday: params.birthday || defaultJson.brithday,
        //     birthLunarFlag: params.birthLunarFlag || defaultJson.birthLunarFlag,
        //     gender: params.gender || defaultJson.gender
        // }
        this.updateClient(updateParams);
        return this;
    };

    /**
     * 关联客户人数
     * @param {object} params 
     */
    async getClientCountForEmployee() {
        const clientCountForEmployee = await ss.spmdm.getClientCountForEmployee({ userId: this.userId }).then(res => res.result.data.val);
        this.clientCount = clientCountForEmployee;
        // clientCountForEmployee && (this.clientCount = clientCountForEmployee);

    }

    /**
     * 客户关联客户期望值
     */
    getClientListExp(clientInfo) {
        const client = this.clientList.get(clientInfo.userId);
        if (clientInfo.userName == '') {
            const client1 = _.cloneDeep(client);
            delete client1.userName;
            return client1;
        };
        return client;
    };

    /**
     * 更新客户信息
     * @param {*} clientInfo 
     */
    updateClient(clientInfo) {
        if (this.clientList.has(clientInfo.id)) {
            const client = this.clientList.get(clientInfo.id);
            common.update(client, clientInfo);
            this.clientList.set(client.id, client);
        } else {
            const client = new Client();
            common.update(client, clientInfo);
            this.clientList.set(client.id, client);
        }
        return this;
    }

    /**
     * 获取角色详情
     */
    getEmployee() {
        return {
            userId: this.userId,
            name: this.name,
            phone: this.phone,
            roleId: this.roleId,
            // roleCode: this.roleCode,
            rem: this.rem,
            transferable: this.transferable,
            deliverable: this.deliverable,
            clientList: [...this.clientList.values()],
            clientCount: this.clientCount,
        }
    }
};

/**
 * 客户
 */
function Client() {
    //  用户id
    this.id = '';
    //  手机号码
    this.mobile = '';
    //  用户名称
    this.userName = '';
    //  名称拼音
    this.namePy = '';
    //  昵称
    this.nickName = '';
    //  用户类型
    this.typeId = 0;
    //  租户id
    this.tenantId = '';
    //  头像id
    this.avatar = '';
    //  性别
    this.gender = 0;
    // //  生日
    // this.birthday = '';
    //  生日月日
    this.birthMd = '';
    //  农历生日标记
    this.birthLunarFlag = 0;
    // //  下次生日日期
    // this.nextBirthday = '';
    //  地址id
    this.addrId = 0;
    //  状态
    // this.flag = 0;
    //  二维码
    this.qrCode = '';
    //  备注
    this.rem = '';
    // //  创建人
    // this.createdBy = '';
    // //  创建时间
    // this.createdDate = '';
    // //  更新人
    // this.updatedBy = '';
    // //  修改时间
    // this.updatedDate = '';
    //  通讯识别号
    this.embGuid = 0;
    //  商陆花ID
    this.slhId = 0;
    //  对应的slh用户ID
    this.slhUserId = 0;
};

const employeeManage = module.exports = {
    Employee
};

employeeManage.setupEmployee = function () {
    return new Employee();
};


