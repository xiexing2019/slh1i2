const common = require('../../lib/common');
const ss = require('../../reqHandler/ss');
const sp = require('../../reqHandler/sp');
const MyShop = require('../help/shop/myShop');

describe('价格管理', async function () {
    this.timeout(TESTCASE.timeout);
    const myShop = new MyShop();
    let dictListlet, sellerInfo, clientInfo;;
    before('获取价格列表', async function () {
        await myShop.switchSeller();
        // console.log(myShop);
        dictList = await ss.config.getSpbDictList({ typeId: 402 }).then(res => res.result.data.rows);
        console.log(`\ndictList=${JSON.stringify(dictList)}`);
    });
    after('启用所有价格', async function () {
        for (const dict of dictList) {
            dict.flag = 1;
            await ss.config.saveDict(dict);
        };
    });
    describe('修改非默认价', async function () {
        let priceType;
        before('修改非默认价', async function () {
            let codeValue = index;
            priceType = dictList.find(obj => obj.codeValue == codeValue);
            priceType.codeName = `价格${codeValue}_${common.getRandomStr(3)}`;
            priceType.flag = 0;
            console.log(priceType);
            await ss.config.saveDict(priceType);
        });
        it('价格列表', async function () {
            const priceList = await ss.config.getSpbDictList({ typeId: 402 }).then(res => res.result.data.rows);
            // console.log(`priceList=${JSON.stringify(priceList.find(price => price.id == priceType.id))}`);
            common.isApproximatelyEqualAssert(priceType, priceList.find(price => price.id == priceType.id));
        });
        it('获取价格字典值(新增)', async function () {
            const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
            // console.log(`\npriceList=${JSON.stringify(priceList.find(price => price.id == priceType.id))}`);
            expect(priceList.find(price => price.id == priceType.id), 'priceList找到了停用的价格').to.be.undefined;
        });
        //TODO: 目前自动核算的是没有生效的，等生效后需要补充货品价格验证
        //价格取整 key: autoCountSalesPrice 值 四舍五入取整 isRound ,价格取整 向上取整 isCeil,向下取整 isFloor 自动核算传‘{}’
        // const autoCountSalesPrices = [{ autoCountSalesPrice: 'isRound' }, { autoCountSalesPrice: 'isCeil' }, { autoCountSalesPrice: 'isFloor' }, {}];
        // for (const autoCountSalesPrice of autoCountSalesPrices) {
        //     describe(`价格核算:${JSON.stringify(autoCountSalesPrice)}`, async function () {
        //         before(`设置自动核算:${JSON.stringify(autoCountSalesPrice)}`, async function () {
        //             priceType.props = autoCountSalesPrice;
        //             await ss.config.saveDict(priceType);
        //         });
        //         it('价格列表', async function () {
        //             const priceList = await ss.config.getSpbDictList({ typeId: 402 }).then(res => res.result.data.rows);
        //             console.log(`priceList=${JSON.stringify(priceList.find(price => price.id == priceType.id))}`);
        //             common.isApproximatelyEqualAssert(priceType, priceList.find(price => price.id == priceType.id));
        //         });
        //     });
        // }

    });

    //禅道地址：  http://zentao.hzdlsoft.com:6082/zentao/bug-view-13231.html
    describe.skip('为客户添加适用价', async function () {
        let priceList;
        before('查询列表', async function () {
            priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
        });
        after('启用会员价2', async function () {
            const res = await ss.config.saveDict({ typeId: 402, codeValue: 2, codeName: '会员价2', flag: 1, props: { addition: 9, percentage: 0.50, outPriceTypeId: 0 } }).then(res => res);
            expect(res.result.msg, '更新失败').to.be.equal('更新成功!');
        });
        describe('设置客户适用价', async function () {
            before('买家访问店铺,卖家设置会员价2', async function () {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: ssAccount.clientAlternate.mobile, openId: ssAccount.clientAlternate.openId, shopId: sellerInfo.shopId });
                clientInfo = _.cloneDeep(LOGINDATA);
                const validPrice = await ss.spmdm.updateMemberValidPrice({ custTenantId: clientInfo.tenantId, validPrice: 2 });
                updateVaild(validPrice);
            });

            it('查询客户适用价', async function () {
                const res = await ss.spmdm.getMemberList();
                // console.log(res.result.data.rows);
                common.isApproximatelyEqualAssert(getVaildPriceExp(clientInfo.tenantId), res.result.data.rows);
            });
        });
        describe('关闭客户适用价', async function () {
            before('停用会员价2', async function () {
                const res = await ss.config.saveDict({ typeId: 402, codeValue: 2, codeName: '会员价2', flag: 0, props: { addition: 9, percentage: 0.50, outPriceTypeId: 0 } }).then(res => res);
                expect(res.result.msg, '更新失败').to.be.equal('更新成功!');
                if (res.params.flag == 0) {
                    const validPrice = await ss.spmdm.updateMemberValidPrice({ custTenantId: clientInfo.tenantId, validPrice: 0 });
                    updateVaild(validPrice);
                }
            });
            it('查询客户适用价', async function () {
                const res = await ss.spmdm.getMemberList();
                // console.log(res.result.data.rows);
                common.isApproximatelyEqualAssert(getVaildPriceExp(clientInfo.tenantId), res.result.data.rows);
            });
        });
    });

});

/**客户适用价期望值 */
function getVaildPriceExp(tenantId) {
    const vaild = vaildMap.get(tenantId);
    if (vaild == undefined) {
        throw new Error('未在vaildMap找到该用户的适用价')
    }
    return vaild;
};

/**更新客户适用价 */
function updateVaild(validPrice) {
    console.log(validPrice);

    const id = validPrice.params.custTenantId
    const vaild = vaildMap.has(id) ? vaildMap.get(id) : new VaildMap();
    vaild.tenantId = id;
    vaild.validPrice = validPrice.params.validPrice;

    vaildMap.set(id, vaild);
};

/**客户适用价map */
function VaildMap() {
    /**租户id */
    this.tenantId = '';
    /**适用价 */
    this.validPrice = '';
};

const vaildMap = new Map();