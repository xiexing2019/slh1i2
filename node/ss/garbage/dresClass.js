const common = require('../../lib/common');
const ssReq = require('../help/ssReq');
const ss = require('../../reqHandler/ss');
const basicJson = require('../help/basicJson');
const dresManage = require('../help/dresManage');

// 用例放入垃圾箱
describe.skip('同步作废商城的商品分类', function () {
    this.timeout(TESTCASE.timeout);
    let dresJson, className = `类别${Date.now()}${common.getRandomNum(100, 999)}`;
    const dres = dresManage.setupDres({ type: 'app' });
    before('卖家登录后保存商品', async () => {
        await ssReq.ssSellerLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await dresManage.prePrepare();
        //保存商品
        dresJson = basicJson.styleJsonByApp();
        dresJson.spu.slhClassName = className;
        console.log(`json=${JSON.stringify(dresJson)}`);
        // await dres.saveDres(json);
        await dres.saveDresByApp(dresJson);
        // console.log(dres.spu);
    });
    it('商品类别', async function () {
        const classList = await ss.config.findDressClass().then(res => res.result.data.rows);
        // console.log(`classList=${JSON.stringify(classList)}`);
        const slhClass = classList.find(ele => ele.name == className);
        console.log(slhClass);
        expect(className, '商品类别未找到').to.equal(slhClass.name);
    });
    describe('同步作废商城的商品分类', async function () {
        before(async function () {
            const res = await ss.op.syncDeleteDresSpuClass({ slhId: 0, name: className });
            console.log(`res=${JSON.stringify(res)}`);
        });
        it('商品类别', async function () {
            const classList = await ss.config.findDressClass().then(res => res.result.data.rows);
            // console.log(`classList=${JSON.stringify(classList)}`);
            const slhClass = classList.find(ele => ele.name == className);
            console.log(slhClass);
            expect(slhClass, '商品类别找到了').to.be.undefined;
        });
        it('卖家查询商品列表', async function () {
            const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
            // console.log(`\ndresFull=${JSON.stringify(dresSpu)}`);
            expect(className, '商品类别与预期不一致').to.equal(dresSpu.ecCaption.classId);
            // common.isApproximatelyEqualAssert(dres.spu, dresSpu);
        });
        it('卖家查询租户商品信息', async function () {
            const dresFull = await dres.getFullById();
            // console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
            expect(className, '商品类别与预期不一致').to.equal(dresFull.spu.ecCaption.classId);
            // common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
        });
        it('买家查询商品详情', async function () {
            await ssReq.userLoginWithWx();
            const dresFull = await dres.getDetailForBuyer();
            // console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
            expect(className, '商品类别与预期不一致').to.equal(dresFull.result.data.spu.ecCaption.classId);
            // common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull.result.data);
        });
        describe('再次使用该类别创建商品', async function () {
            const dres2 = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await ssReq.ssSellerLogin();
                await dresManage.prePrepare();
                //保存商品
                dresJson = basicJson.styleJsonByApp();
                dresJson.spu.slhClassName = className;
                console.log(`json=${JSON.stringify(dresJson)}`);
                await dres2.saveDresByApp(dresJson);
            });
            it('商品类别', async function () {
                const classList = await ss.config.findDressClass().then(res => res.result.data.rows);
                // console.log(`classList=${JSON.stringify(classList)}`);
                const slhClass = classList.find(ele => ele.name == className);
                console.log(slhClass);
                expect(slhClass, '商品类别找到了').to.be.undefined;
            });
        });
        describe('同步商品分类到商城', async function () {
            before(async function () {
                const res = await ss.op.syncDresSpuClass({ slhId: 0, slhParentId: 0, name: className, flag: 1 });
                console.log(`res=${JSON.stringify(res)}`);
            });
            it('商品类别', async function () {
                const classList = await ss.config.findDressClass().then(res => res.result.data.rows);
                // console.log(`classList=${JSON.stringify(classList)}`);
                const slhClass = classList.find(ele => ele.name == className);
                console.log(slhClass);
                expect(className, '商品类别未找到').to.equal(slhClass.name);
            });
        });
    });
});