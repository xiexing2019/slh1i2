const common = require('../../lib/common');
const ssReq = require('../help/ssReq');
const sp = require('../../reqHandler/sp');
const ss = require('../../reqHandler/ss');
const caps = require('../../data/caps');
const ssAccount = require('../data/ssAccount');
const basicJson = require('../help/basicJson');
const fs = require('fs');
const path = require('path');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/doc.json')))[caps.name];
const mysql = require('../../reqHandler/sp/mysql');
const myShop = require('../help/shop/myShop');
const ssCaps = require('../../reqHandler/ss/ssCaps');



async function newShop(params) {
    const user = {
        code: common.getRandomMobile(),
        // code: 13857137831,
        authcType: 2,
        captcha: 0000,
    };
    let crmId; // 增家crmId后续使用crmId去数据库查询购买套餐可用门店数量 提测单 1514
    await ss.spugr.userLogin(user);
    console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    await sp.spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId });
    const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
    const setRes = await ss.spugr.findSetList({ flag: 1 });
    const setDetail = setRes.result.data.rows[0];
    expect(setDetail, `未查询到可用套餐,\n${setRes.reqUrl}`).not.to.be.undefined;

    const billRes = await ss.spugr.saveValueAddService({ code: setDetail.crmId, num: 10 });
    console.log(`res=${JSON.stringify(billRes)}`);

    const payBackRes = await ss.spugr.backCallValueAddService({ billId: billRes.result.data.id });
    console.log(`payBackRes=${JSON.stringify(payBackRes)}`);

    await ss.spugr.userLogin(user);
    const tenantJson = await basicJson.tenantJson();
    // console.log(tenantJson);
    user.shopName = tenantJson.name;
    shop = await ss.spugr.appCreateTenant(tenantJson).then(res => res.result.data);
    console.log(`\nshop=${JSON.stringify(shop)}`);

    // 切换用户门店
    const res = await ss.spugr.changeUserShop({ newUnitId: shop.unitId, mobile: shop.mobile });
    console.log(`\nres=${JSON.stringify(res)}`);
    const merchat = await sp.spugr.createMerchat({
        mobile: shop.mobile,
        settleAcctNo: "6217730700880774",
        settleAcctName: "在市谋",
        contact: "在市谋",
        settleAcctType: "2",
        bankName: "中信银行",
        bankCode: "302100011000", // 102100099996 工商银行 卡号6212261202022206396
        cncbFlag: 1,
        idCard: docData.idCard.cardMsg,
        idCardFront: docData.idCard.frontImage.docId,//是	身份证正面照, docId
        idCardReverse: docData.idCard.backImage.docId,//是	身份证背面照, docId id_card_reverse
    });
    console.log(`\n保存商户信息=${JSON.stringify(merchat)}`);
    await ss.spugr.updateOpenFlow({ shopId: shop.id, openFlowFlag: 99 });
}


newShop();