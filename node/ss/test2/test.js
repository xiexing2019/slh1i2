'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const ssReq = require('../help/ssReq');
const ss = require('../../reqHandler/ss');
const sp = require('../../reqHandler/sp');
const fs = require('fs');
const path = require('path');
const child_process = require('child_process');
const moment = require('moment');
const ssAccount = require('../data/ssAccount');
const configParam = require('../../ss/help/configParamManager');
const Seller = require('../help/user/seller');
const basicJson = require('../help/basicJson');
const dresManage = require('../help/dresManage');
const QrCode = require('qrcode-reader');
// const zentao = require('../../lib/zentao');
const ssCaps = require('../../reqHandler/ss/ssCaps');
const ecPlant = require('../../reqHandler/ecoolPlant');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/doc.json')))[caps.name];
const MyShop = require('../help/shop/myShop');
const cartManage = require('../help/cartManage');
const timeLimitManage = require('../help/act/timeLimit');
const shipFee = require('../help/shop/manage/shipFee');
// import { assuming } from '../../lib/assume';
const assume = require('../../lib/assume');

describe('自测', function () {
    this.timeout(300000);
    it.only('1', async function () {
        await ssReq.ssSellerLogin();
        const seller = _.cloneDeep(LOGINDATA);

        await ssReq.ssClientLogin({ mobile: 12908209999, tenantId: seller.tenantId, });
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const res = await ss.config.findProductParams({ ownerId: LOGINDATA.tenantId, domainKind: 'business', sellerId: seller.tenantId, codes: ',show_inventory_in_ssmall,allow_shelves_when_inventory_less_than_zero,open_after_sale,goods_pick_up_by_self' });
        console.log(`res=${JSON.stringify(res)}`);

    });

    assume.assuming(false, '3')('2', async function () {
        console.log('2-2');
    });

    it.skip('运费', async function () {
        const shopInfo = new MyShop();
        // await shopInfo.switchSeller({ code: 12909298653, shopName: '店铺Uw9en' });
        await shopInfo.switchSeller({ code: 12909296069, shopName: '店铺bbLei' });

        const ship = await shopInfo.getShipFee();
        console.log(shopInfo.shipFee.templateMap);
        const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 3, feeType: common.getRandomNum(1, 2) }, freeRule: { count: 2, shipFreeType: common.getRandomNum(1, 2) } });
        // const templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
        const templateId = 1063;

        // await shopInfo.shipFee.templateFeeRuleDetails(templateId);
        // const feeRuleJson = await shopInfo.shipFee.mockFeeRuleJson(templateId, { id: '', provIds: 'DEFAULT', deleteFlag: 1 });
        // console.log(feeRuleJson);
        // await shopInfo.shipFee.templateFeeRuleDetails(templateId);
        // await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spFeeRules: feeRuleJson });
        // await shopInfo.shipFee.templateFeeRuleDetails(templateId);


        await shopInfo.shipFee.templateFeeRuleDetails(templateId);
        const freeRuleJson = await shopInfo.shipFee.mockFreeRuleJson(templateId, { id: '', shipProvIds: '110000', deleteFlag: 1 });
        console.log(freeRuleJson);
        await shopInfo.shipFee.templateFeeRuleDetails(templateId);
        await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spShipFeeRules: freeRuleJson });
        await shopInfo.shipFee.templateFeeRuleDetails(templateId);

        // await shopInfo.shipFee.deleteTemplate(templateId);
    });

    it.skip('', async function () {
        const myShop = new MyShop();
        await myShop.switchSeller();
        const lll = await myShop.getConfigParam({ code: 'ss_ugr_merchant_enble', domainKind: 'system', ownerId: LOGINDATA.tenantId }, 'spg');
        console.log(lll);
        await myShop.updateConfigParam({ code: 'ss_ugr_merchant_enble', val: 1 });
    });

    it.skip('', async function () {
        // 
        await ssReq.ssSellerLogin();
        const sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        const act = timeLimitManage.setupTimeLimit();
        const dresList = await act.getDresList(300);


        await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        LOGINDATA.tenantId = 561;
        LOGINDATA.sessionId = 'ssg10d-18-7B0B20AF-DC13-CCF8-4189-DB1CCA3E1445';


        const shoppingCart = cartManage.setupShoppingCart(sellerInfo);
        await shoppingCart.emptyCart();

        for (let index = 0; index < dresList.length; index++) {
            console.log(index);
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList[index].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            const cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 1, num: 1 });
            // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
            await shoppingCart.saveCartInBatchs(cartJson);
        }


    });

    it.skip('', async function () {
        await ssReq.ssSellerLogin();
        const res = await ss.spugr.idCardDetect({ frontUrl: docData.idCard.frontImage.docUrl, backUrl: docData.idCard.backImage.docUrl });
        console.log(`res=${JSON.stringify(res)}`);
        // const res = await ss.spugr.idCardDetect({ frontUrl: 'http://119.3.73.24:8081/doc/v2/content.do?id=docx156818880133347893-docg1.jpg', backUrl: 'http://119.3.73.24:8081/doc/v2/content.do?id=docx156818962833347897-docg1.jpg' });
        // console.log(`res=${JSON.stringify(res)}`);
    });

    it.skip('', async function () {
        await ssReq.ssSellerLogin();
        const res = await ss.spdresb.quantityUpdateFreeTemplate({ templateId: 1 });
        console.log(`res=${JSON.stringify(res)}`);
    });

    it.skip('', async function () {
        await ssReq.ssSellerLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);


        await ssReq.userLoginWithWx();
        console.log(`\n\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);


    });

    it.skip('', async function () {
        await ssReq.ssSellerLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        await ssReq.userLoginWithWx();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    it.skip('', async function () {
        const qr = new QrCode();
        qr.callback = function (error, result) {
            if (error) {
                console.log(this);
                console.log(error)
                return;
            }
            console.log(result)
        }
        qr.decode('image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHQAAAB0CAYAAABUmhYnAAAAAklEQVR4AewaftIAAAKsSURBVO3BQW7kQAwEwSxC//9yro88NSBIM2sTjIg/WGMUa5RijVKsUYo1SrFGKdYoxRqlWKMUa5RijVKsUYo1SrFGKdYoxRrl4qEkfJNKl4QTlS4JncpJEr5J5YlijVKsUYo1ysXLVN6UhDtUuiS8SeVNSXhTsUYp1ijFGuXiw5Jwh8oTSehUuiS8KQl3qHxSsUYp1ijFGuXij1M5ScJJEjqVv6xYoxRrlGKNcvHHJeFE5SQJkxRrlGKNUqxRLj5M5ZNU7kjCm1R+k2KNUqxRijXKxcuS8E1J6FS6JHQqXRLuSMJvVqxRijVKsUaJPxgkCZ1Kl4QTlb+sWKMUa5RijXLxUBI6lZMk/E8qXRK6JHQqJ0noVLok3KHyRLFGKdYoxRrl4j9TeSIJncoTKl0STlTuUPmkYo1SrFGKNcrFQypdEt6UhCeS0Kl0KneonCThjiR0Kk8Ua5RijVKsUeIPXpSEE5WTJHQqJ0l4QqVLQqdykoROpUvCicqbijVKsUYp1igXH6bSJeFEpUtCp9KpdEk4UXkiCSdJ6FS6JHxSsUYp1ijFGiX+4A9LQqdyRxI6lTuS0Kl0SbhD5YlijVKsUYo1ysVDSfgmlZMknKjckYQ7knCi8knFGqVYoxRrlIuXqbwpCU+odEk4ScI3JaFTeaJYoxRrlGKNcvFhSbhD5X9SOUlCp9IloVPpktCpvKlYoxRrlGKNcjGMSpeEE5UuCScqJyonKp9UrFGKNUqxRrn441S6JNyRhBOVkyR0Kl0S7lB5olijFGuUYo1y8WEq36TSJeEOlS4JnUqn8psUa5RijVKsUS5eloRvSkKn0qm8KQknKp3KNxVrlGKNUqxR4g/WGMUapVijFGuUYo1SrFGKNUqxRinWKMUapVijFGuUYo1SrFGKNUqxRvkHv7QE9MppxbsAAAAASUVORK5CYII=');
    });

    it.skip('生成商品-刷数据', async function () {
        await ssReq.ssSellerLogin();
        const dres = dresManage.setupDres({ type: 'app' });

        await dresManage.prePrepare();
        for (let index = 0; index < 100; index++) {
            console.log(index + 1);
            const json = basicJson.styleJsonByApp();
            await dres.saveDresByApp(json);
        }
    });

    it.skip('发送mq消息', async function () {
        await ssReq.ssSellerLogin();
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);


        const lqd = await ss.creatSqlPool({ dbName: 'lqdMysql' });
        const orderDetail = await lqd.query(`SELECT * FROM liquidation.eb_biz_order_detail WHERE cluster_code LIKE '%${LOGINDATA.clusterCode}%' AND tenant_id=${LOGINDATA.tenantId} AND status=4 ORDER BY created_date DESC`).then(res => {
            // console.log(res);
            return res[0][0];
        });
        await lqd.end();

        console.log(`\norderDetail=${JSON.stringify(orderDetail)}`);

        const res = await sp.spTest.sendMq({
            type: 0,
            bizKeys: common.getRandomNumStr(5),
            cid: LOGINDATA.clusterCode,
            tags: 'finReceiveBizOrder',
            className: 'com.hzecool.eb.dto.BizOrderDetailDTO',
            topic: 'slhPlat',
            jsonParam: [{
                mchNo: orderDetail.mch_no,
                payDate: orderDetail.pay_date,
                bizOrderId: orderDetail.biz_order_id,
                bizSubOrderId: orderDetail.biz_sub_order_id,
                newBizOrderId: orderDetail.biz_order_id,
                bizType: 1240,
                payId: orderDetail.pay_id,
                channelOrderId: orderDetail.channel_order_id,
                payType: orderDetail.pay_type,
                orderType: orderDetail.order_type,
                orderAmount: orderDetail.order_amount,
                payAmount: orderDetail.pay_amount,
                discountAmount: orderDetail.discount_amount,
                feeAmount: orderDetail.fee_amount,
                shareAmount: orderDetail.share_amount,
                payWayType: orderDetail.pay_way_type,
                description: orderDetail.description,
                unitId: orderDetail.unit_id,
                tenantId: orderDetail.tenant_id,
                saasClusterId: orderDetail.saas_cluster_id,
                saasId: orderDetail.saas_id,
                extra: orderDetail.extra,
                attch: orderDetail.attch,
            }]
        });
        console.log(res);

    });

});
