const mocha = require('mocha');
const child_process = require('child_process');
const path = require('path');

const args = [path.join(__dirname, './test.js')];
process.argv.slice(2).forEach(arg => args.push(arg));
console.log(args);

const proc = child_process.fork(mocha, args, {
    stdio: 'inherit'
});

proc.on('exit', (code, signal) => {
    process.on('exit', () => {
        if (signal) {
            process.kill(process.pid, signal);
        } else {
            process.exit(code);
        }
    });
});