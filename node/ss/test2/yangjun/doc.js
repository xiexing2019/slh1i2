const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const doc = require('../../../reqHandler/ss/confc/doc');
const fs = require('fs');
const path = require('path');
// const docData = fs.readFileSync(path.join(__dirname, '../timg094.jpg'));

describe.skip('doc图片上传', function () {
    this.timeout(TESTCASE.timeout);
    before('登录', async function () {
        await ssReq.ssSellerLogin();
    })
    it('zip图片上传', async function () {
        let filePath = path.join(__dirname, '../../../../../../Downloads/img.zip');
        let res = await sp.doc.uploadWithZip({ path: filePath });
        console.log(JSON.stringify(res));
    });
    it('图片上传', async function () {
        let filePath = path.join(__dirname, '../../../../../../Downloads/img/timg (1).jpeg');
        let res = await sp.doc.upload({ path: filePath });
        console.log(res);
    });
})