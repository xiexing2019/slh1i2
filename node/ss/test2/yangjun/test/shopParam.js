const shopParamDf = require('../shopParamDf');
const ssReq = require('../../../help/ssReq');
const ssCaps = require('../../../../reqHandler/ss/ssCaps');
const common = require('../../../../lib/common');
const data_yunfeiSZ = require('../dataProvide/data_yunfeiSZ');

describe('运费设置', async function () {
    let shopId, shopDetail;
    let tid = ssCaps.spbDefParams()._tid;
    let sendAdressInitial = _.cloneDeep(data_yunfeiSZ.sendAdress);
    this.timeout(300000);

    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('发货地址设置', async function () {
        before(async function () {
            shopDetail = await shopParamDf.getShopDetail({ unitId: sellerInfo.unitId, _tid: tid }).then(res => res.result.data)
            if (shopDetail.id == null) {
                "店铺未开通，请先开通店铺"
            } else {
                let updatedShopId = await shopParamDf.updateShop({ id: shopDetail.id, ...sendAdressInitial }).then(res => res.result.data.val)
                sendAdressInitial.id = updatedShopId;
            }
        });
        it('查看店铺详情-获取发货地址', async function () {
            shopDetail = await shopParamDf.getShopDetail().then(res => res.result.data)
            common.isApproximatelyEqualAssert(shopDetail, sendAdressInitial)

        });
    });

    describe('退货地址设置', async function () {
        let backAdress = _.cloneDeep(data_yunfeiSZ.sendAdress);
        let addressJson = {
            address:
            {
                provinceCode: backAdress.deliverProvCode,
                cityCode: backAdress.deliverCityCode,
                countyCode: backAdress.deliverAreaCode,
                detailAddr: backAdress.deliverDetailAddr,
                townCode: ''
            }
        };
        describe('默认发货地址', async function () {
            let userRecInfo = {};
            userRecInfo.address = backAdress;
            let recInfoId;
            before(async function () {
                let userBackAdressInfoExist = await shopParamDf.getUserRecInfo({ type: 2 }).then(res => res.result.data.rows.find(data => data.recInfo.type == 2));
                if (false) {
                    console.log('不存在用户退货地址，创建为默认发货地址');
                    recInfoId = await shopParamDf.saveUserRecInfo(Object.assign({ actionType: 0 }, addressJson)).then(res => res.result.data.val);
                }
                else {
                    console.log('已存在用户退货地址，修改为默认发货地址');
                    recInfoId = await shopParamDf.saveUserRecInfo({ actionType: 0, recInfo: { id: userBackAdressInfoExist.recInfo.id }, address: Object.assign({ id: userBackAdressInfoExist.address.id }, addressJson.address) }).then(res => res.result.data.val);

                };
                userRecInfo.recInfo = { id: recInfoId };

            });
            it('查询退货地址', async function () {
                UserRecInfoExpect = await shopParamDf.getUserRecInfo({ type: 2 }).then(res => res.result.data.rows.find(data => data.recInfo.id == recInfoId));
                common.isApproximatelyEqualAssert(UserRecInfoExpect, userRecInfo)

            });

        });
        describe('修改发货地址后，退货地址是默认发货地址的配置，查看退货地址是否跟着改变', async function () {

        });
        describe('自定义', async function () {

        });


    });
    describe('运费计价方式', async function () {
        describe('到付', async function () {

        });
        describe('自定义-按件数，自定义包邮-关闭', async function () {

        });
        describe('自定义-按重量，自定义包邮-关闭', async function () {

        });
        describe('自定义-按件数，自定义包邮-按件数', async function () {

        });
        describe('自定义-按件数，自定义包邮-按金额', async function () {

        });
        describe('自定义-按重量，自定义包邮-按件数', async function () {

        });
        describe('自定义-按重量，自定义包邮-按金额', async function () {

        });

    });


});