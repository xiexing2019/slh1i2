const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');

/**
 * 颜色开关标识colorSplitFlag：0-不可 1-可拆 2-拆过
 * sizeSplitFlag：0-不可 1-可拆 2-拆过
 * splitSpec1拆分过的尺码信息（逗号隔开的string）
 * splitSpec2拆分过的颜色信息（逗号隔开的string）
 * strSpec1尺码文本信息
 * strSpec2颜色文本信息
 * 
 * 前端限制只能拆均色或均码    服务端不做限制
 * 
 * sizeSplitFlag  初始为0 拆分2 还原后为1
 * 数据存在spu.props中
 * 获取详情时 在spu中显示
 */
describe('拆色拆码', async function () {
    this.timeout(300000);
    const dres = dresManage.setupDres();

    before('新增商品并拆色拆码', async function () {
        await ssReq.ssSellerLogin();
        await dresManage.prePrepare();

        // const freeColor = BASICDATA['601'].find(val => val.codeName == '均色'),
        //     freeSize = BASICDATA['605'].find(val => val.codeName == '均码');

        // 保存商品 均色均码
        const json = basicJson.styleJson();
        // json.spu.spec1Ids = { [freeColor.codeValue]: { 'salesCaption': freeColor.codeName } };
        // json.spu.spec2Ids = { [freeSize.codeValue]: { 'salesCaption': freeSize.codeName } };
        // json.skus = json.skus.slice(0, 1).map(sku => {
        //     sku.spec1 = freeColor.codeValue;
        //     sku.spec2 = freeSize.codeValue;
        //     return sku;
        // });
        // console.log(`\njson=${JSON.stringify(json)}`);
        await dres.saveDres(json);

        // 拆色拆码  colorFlag: 1, colorSplit: BASICDATA['601'].slice(-5).map(ele => ele.codeValue).join(','),
        await dres.updateSpuForSeller({ sizeFlag: 1, sizeSplit: BASICDATA['605'].slice(-5).map(ele => ele.codeName).join(','), colorFlag: 1, colorSplit: BASICDATA['601'].slice(-3).map(ele => ele.codeName).join(',') });
    });

    it('卖家查询商品列表', async function () {
        const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
        common.isApproximatelyEqualAssert(dres.spu, dresSpu);
    });

    it('卖家查询租户商品信息', async function () {
        const dresFull = await dres.getFullById();

        common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
    });

    it('买家查询商品列表', async function () {
        await ssReq.userLoginWithWx();
        const dresSpu = await dres.findBuyerSpuList();
        common.isApproximatelyEqualAssert(dres.spu, dresSpu);
    });

    it('买家查询商品详情', async function () {
        const dresFull = await dres.getDetailForBuyer();
        common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull.result.data);
    });





});


