const ssReq = require('../../../../../node/ss/help/ssReq');
const price = require('../price');
const common = require('../../../../lib/common');
describe('货品批量调价', async function () {
    let spuIds, spuList;
    this.timeout(300000);
    before(async function () {
        await ssReq.ssSellerLogin();
        spuList = await price.findSellerSpuList({ flags: 1, orderBy: 'pub_price', orderByDesc: 'true' }).then(res => res.result.data.rows);
        if (spuList.length == 0) {
            console.log('没有已上架的货品，请先上架货品');
            return false;
        } else {
            spuList = spuList.slice(0, 2)
            spuIds = spuList.map(data => data.id)

        }

    });
    describe('价格类型-默认价，调价方式-金额，选择方式-涨', async function () {
        let spuDetailOld;
        before(async function () {
            const x = -common.getRandomNum(1, 10)
            console.log(`id 是 ${spuIds}，原价分别是${spuList.map(data => data.pubPrice)},现在涨价${-x}`);
            spuDetailOld = await price.batchGetFullById(spuIds);
            await price.batchPreferentialPrice({ spuIds: spuIds.join(','), codeValue: 0, preferentialMode: 1, preferentialPrice: x })

            spuDetailOld = price.updatePrice(spuDetailOld, 0, 1, x);

        });

        it('商品详情', async function () {
            const spuDetailNew = await price.batchGetFullById(spuIds);
            common.isApproximatelyEqualAssert(spuDetailOld, spuDetailNew, ['updatedBy', 'updatedDate', 'ver', 'priceChangedType', 'priceChangedSet']);
        });

    });

    describe('价格类型-默认价，调价方式-金额，选择方式-降，输入金额-小于商品默认价', async function () {
        let spuDetailOld;
        before(async function () {
            spuDetailOld = await price.batchGetFullById(spuIds);
            const minPrice = price.getSmallest(spuDetailOld.map(data => data.spu.pubPrice))
            const x = common.getRandomNum(common.div(minPrice, 2), minPrice)

            console.log(`id 是 ${spuIds}，原价分别是${spuDetailOld.map(data => data.spu.pubPrice)},现在降价${x}`);
            await price.batchPreferentialPrice({ spuIds: spuIds.join(','), codeValue: 0, preferentialMode: 1, preferentialPrice: x });
            spuDetailOld = price.updatePrice(spuDetailOld, 0, 1, x);

        });

        it('商品详情', async function () {
            const spuDetailNew = await price.batchGetFullById(spuIds);
            common.isApproximatelyEqualAssert(spuDetailOld, spuDetailNew, ['updatedBy', 'updatedDate', 'ver', 'priceChangedType', 'priceChangedSet']);
        });

    });
    describe('异常用例：价格类型-默认价，调价方式-金额，选择方式-降，输入金额-大于商品默认价', async function () {
        let spuDetailOld;
        before(async function () {
            spuDetailOld = await price.batchGetFullById(spuIds);
            const maxPrice = price.getBigest(spuDetailOld.map(data => data.spu.pubPrice))
            const x = common.getRandomNum(maxPrice, maxPrice + 100)

            console.log(`id 是 ${spuIds}，原价分别是${spuDetailOld.map(data => data.spu.pubPrice)},现在降价${x}`);
            await price.batchPreferentialPrice({ spuIds: spuIds.join(','), codeValue: 0, preferentialMode: 1, preferentialPrice: x });
            spuDetailOld = price.updatePrice(spuDetailOld, 0, 1, x);

        });

        it('商品详情', async function () {
            const spuDetailNew = await price.batchGetFullById(spuIds);
            common.isApproximatelyEqualAssert(spuDetailOld, spuDetailNew, ['updatedBy', 'updatedDate', 'ver', 'priceChangedType', 'priceChangedSet']);
        });



    });



    describe('价格类型-默认价，调价方式-百分比', async function () {
        let spuDetailOld;
        before(async function () {
            const x = (common.getRandomNum(0, 120) / 100).toFixed(3)
            spuDetailOld = await price.batchGetFullById(spuIds);
            console.log(`id 是 ${spuIds}，原价分别是${spuDetailOld.map(data => data.spu.pubPrice)},现在折扣${x}`);
            let res = await price.batchPreferentialPrice({ spuIds: spuIds.join(','), codeValue: 0, preferentialMode: 0, preferentialPrice: x }).then(res => res.result);
            spuDetailOld = price.updatePrice(spuDetailOld, 0, 0, x);

        });

        it('商品详情', async function () {
            const spuDetailNew = await price.batchGetFullById(spuIds);
            common.isApproximatelyEqualAssert(spuDetailOld, spuDetailNew, ['updatedBy', 'updatedDate', 'ver', 'priceChangedType', 'priceChangedSet']);
        });

    });




});