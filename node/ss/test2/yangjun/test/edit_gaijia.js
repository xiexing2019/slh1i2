const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const spdresb = require('../../../../reqHandler/sp/biz_server/spdresb');


describe('改价', function () {
    this.timeout(300000000);
    let spuIds = [], dresList = [], priceValues = [];
    let sellerInfo;

    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
        priceList.forEach(price => priceValues.push(price.codeValue));
        const spuList = await sp.spdresb.findSellerSpuList({ flag: 1, orderBy: 'pub_price', orderByDesc: true }).then(res => res.result.data.rows);
        for (const i in spuList.slice(0, 3)) {
            spuIds.push(spuList[i].id);
            const dresFull = await sp.spdresb.getFullById({ id: spuList[i].id }).then(res => res.result.data);
            const dres = dresManage.setupDres();
            dres.setByDetail(dresFull);
            dresList[i] = dres;
        }
    });

    //商品创建时，如价格为0，则原价永远为0，调详情的接口也不会修改
    describe('单个商品改价', async function () {
        let dresFull;
        before(async function () {
            await dresList[0].updateSpuPrice(common.getRandomNum(1, 10));
            dresFull = await dresList[0].getFullById();
        });
        it('商品详情', async function () {
            common.isApproximatelyEqualAssert(dresFull, dresList[0].getDetailExp(), ['ssNum']);
        });

        it('商品详情,原价', async function () {
            common.isApproximatelyEqualAssert(dresFull.originalPrices, dresList[0].originalPrices);
        });
    });

    describe('批量改价', async function () {

        describe('批量改价:折扣价', async function () {
            let discount;
            const dresFullOld = [];
            before(async function () {
                for (let i in spuIds) {
                    dresFullOld.push(await dresList[i].getFullById());
                }
                discount = common.getRandomNum(1, 9) / 10;
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 0, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (let i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);

                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.ssClientLogin()
                for (let i in spuIds) {
                    const dresFull = await spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })

                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);

                }

            });
        });
        describe('批量改价:优惠价', async function () {
            let discount;
            const dresFullOld = [];
            let sellerInfo;
            before(async function () {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);

                for (const i in spuIds) {
                    dresFullOld.push(await dresList[i].getFullById());
                }

                discount = common.getRandomNum(-20, 20);
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 1, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (const i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 1);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);

                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.ssClientLogin()
                for (let i in spuIds) {
                    const dresFull = await spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);

                }

            });
        });
    });
});

/**
 * 
 * @description 
 * @param {object} params
 * @param {object} params.codeValues 
 * @param {object} params.dresFull 
 * @param {object} params.discount 
 * @param {object} params.preferentialMode 0 折扣价 1 优惠价
 */
function modifyPrice(codeValues, dresFull, discount, preferentialMode) {
    let exp = {};
    for (let value of codeValues) {
        let price = value == 0 ? 'pubPrice' : `price${value}`;
        if (preferentialMode == 0) {

            dresFull.skus.map(data => data[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3));
        }
        else if (preferentialMode == 1) {
            dresFull.skus.map(data => data[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3));

        } else {
            console.warn(`不存在preferentialMode为${preferentialMode}`);
        }
    }

    return dresFull;
};

