const format = require('../../../data/format');
const ssCaps = require('../../../reqHandler/ss/ssCaps');
const common = require('../../../lib/common');

let price = module.exports = {

};



price.findSellerSpuList = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), ssCaps.spbDefParams());
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-findSellerSpuList', ...params });

}


price.batchPreferentialPrice = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams())
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-batchPreferentialPrice', ...params })

}

price.getFullById = async function (params = {}) {
    params = Object.assign(params, ssCaps.spbDefParams())
    return common.post(ssCaps.spbUrl, { apiKey: 'ec-spdresb-dresSpu-getFullById', ...params })

}
price.batchGetFullById = async function (spuIdlist) {
    let spuInfoList = [];
    for (let i = 0; i < spuIdlist.length; i++) {
        const spuId = spuIdlist[i];
        let spuInfo = await price.getFullById({ id: spuId }).then(res => res.result.data);
        spuInfoList.push(spuInfo)

    }
    return spuInfoList;

}

function sub(oldPrice, num, n = 3) {
    return (oldPrice - num).toFixed(n)
}
function mul(oldPrice, discount, n = 3) {
    return (oldPrice * discount).toFixed(n)
}

price.getSmallest = function (array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] < array[i + 1]) {
            tmp = array[i];
            array[i] = array[i + 1]
            array[i + 1] = tmp
        }
    }
    return array[array.length - 1]
}
price.getBigest = function (array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i] > array[i + 1]) {
            tmp = array[i];
            array[i] = array[i + 1]
            array[i + 1] = tmp
        }
    }
    return array[array.length - 1]
}

price.updatePrice = function (spus, codeValue, preferentialMode, preferentialPrice) {
    if (preferentialMode == 0) {
        spus.map(spu => {
            let price = mul(spu.spu.pubPrice, preferentialPrice);
            spu.spu.pubPrice = price;
            spu.skus.map(data => data.pubPrice = price)
        })

    }
    else if (preferentialMode == 1) {
        spus.map(spu => {
            let price = sub(spu.spu.pubPrice, preferentialPrice);
            spu.spu.pubPrice = price;
            spu.skus.map(data => data.pubPrice = price)
        })

    }
    return spus;
}

