const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssCaps = require('../../../reqHandler/ss/ssCaps');


let shopParamDf = module.exports = {
    spbUrl: ssCaps.spbUrl,
    spgUrl: ssCaps.spgUrl
};

shopParamDf.updateShop = async function (params = {}) {
    params = format.packJsonParam(params);
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-updateShop', ...params })
}

shopParamDf.getShopDetail = async function (params = {}) {
    return common.post(this.spgUrl, { apiKey: 'ec-spugr-spShop-getDetail', ...params })
}

shopParamDf.saveUserRecInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-spmdm-spUserManage-saveUserRecInfo', ...params })
}

shopParamDf.getUserRecInfo = async function (params = {}) {
    params = Object.assign(format.packJsonParam(params), ssCaps.spbDefParams());
    return common.post(this.spbUrl, { apiKey: 'ec-spmdm-spUserManage-getUserRecInfoList', ...params })

}


