const common = require('../../../lib/common');
const format = require('../../../data/format');
const ssReq = require('../../help/ssReq');
const spCaps = require('../../../reqHandler/sp/spCaps');



function dressInData() {
    let color = ['红色', '灰色', '白色']
    let size = ['M', 'L', 'XL']
    skus = []
    for (i = 0; i < color.length; i++) {
        for (j = 0; j < size.length; j++) {
            sku = {}
            sku["spec2Name"] = color[i]
            sku["spec1Name"] = size[j]
            skus.push(sku)
        }
    }
    return {
        spu: {
            code: `kh_${common.getRandomStr(3)}`,
            title: `测试商品_${common.getRandomStr(3)}`,
            name: `测试商品_${common.getRandomStr(3)}`,
            spec1Names: size.toString(),
            spec2Names: color.toString(),
            pubPrice: 20,
            price1: 21,
            price2: 22,
            price3: 23,
            price4: 24,
            price5: 25,
            weight: 0.3,
            isBeOnlineFlag: 0,
            docHeader: [],
            docContent: [],
            groupIds: 32954,
            brandName: "",
            themeName: "",
            seasonName: "",
            fabricName: "",
            pricesStart: {
                priceTypes: "",
                buyNums: 2
            },
            pricesInHand: {
                priceTypes: ""
            }
        },
        skus

    }

}


async function createDress(params = {}) {
    params = Object.assign(format.packJsonParam(params), spCaps.spbDefParams())
    return common.post('https://sst.hzecool.com/spb/api.do', { apiKey: 'ec-spdresb-dresSpu-saveFullByApp', ...params })
}

async function getDressInfo(params = {}) {
    params = Object.assign(format.packJsonParam(params, ['orderBy', 'orderByDesc']), spCaps.spbDefParams());
    return common.get('https://sst.hzecool.com/spb/api.do', { apiKey: 'ec-spdresb-dresSpu-findSellerSpuList', ...params });
}
describe('货品创建', function () {
    this.timeout(300000);
    let dresInfo;
    let spuId;
    let dresData = dressInData()
    before(async () => {
        await ssReq.ssSellerLogin();
        dresInfo = await createDress(dresData).then(res => res.result.data);
        spuId = dresInfo.val;
        dresData.id = spuId;
        dresData.spu.id = spuId;

    })
    it('货品信息查询', async () => {
        lastDressInfo = await getDressInfo({ outSpuId: dresInfo.val }).then(res => res.result.data.rows.find(data => data.id == dresInfo.val));
        console.log(lastDressInfo);

        expect(lastDressInfo).not.to.be.undefined;
        common.isApproximatelyEqualAssert(dresData, lastDressInfo);
    })
})

