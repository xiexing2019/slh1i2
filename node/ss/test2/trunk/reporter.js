const mocha = require('mocha');
const moment = require('moment');
const dingtalkRobot = require('../../../lib/dingtalkRobot');

// 微商城服务监控群
const accessToken = 'e7fa21089cfbb092dd68c505f8728db6c5d9be6cbf072ff7c71231574bcd17cc';

class Reporter extends mocha.reporters.Base {
    constructor(runner, options) {
        super(runner, options);

        let passCaseNum = 0, failCaseNum = 0, warnCaseNum = 0;

        runner.on('start', function (params) {
            console.log(`测试开始 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')}`);
        });

        runner.on('pass', function (test) {
            passCaseNum++;
        });

        runner.on('fail', async function (test, err) {
            failCaseNum++;

            const errorMsg = `${test.titlePath().join('-').replace(/-trunk/g, '')}\nError: ${err.message}`;
            console.log(errorMsg);
            await dingtalkRobot.sendMsg(accessToken, {
                msgtype: 'text',
                text: { content: errorMsg }
            });
        });

        runner.on('end', async function () {
            const total = passCaseNum + failCaseNum + warnCaseNum;
            // 输出拱金工判断 参数名称写死
            console.log(`\n测试结束 ${moment().format('YYYY-MM-DD HH:mm:ss:SSS')} \n   total=${total}, passes=${passCaseNum}, failures=${failCaseNum}, warns=${warnCaseNum}`);

            if (failCaseNum > 0) {
                await dingtalkRobot.sendMsg(accessToken, {
                    msgtype: 'text',
                    text: { content: `执行完毕,用例数:${total},通过数:${passCaseNum},失败数:${failCaseNum}` },
                    at: { isAtAll: true },
                });
            }
            process.exit(0);
        });
    }
}


module.exports = Reporter;