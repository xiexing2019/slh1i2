const ss = require('../../../../reqHandler/ss');
const ssAccount = require('../../../data/ssAccount');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const dresManage = require('../../../help/dresManage');
const billManage = require('../../../help/billManage');
const common = require('../../../../lib/common');


/**
 * 线上30分钟定时执行
 * 目前覆盖买家登录后登录首页
 *
 * 后期需要单独建一个店铺进行测试
 */
describe('买家主流程-trunk', async function () {
    this.timeout(TESTCASE.timeout);
    const sellerInfo = ssAccount.trunkSeller;
    Object.assign(sellerInfo, { clusterCode: 'ssb12', unitId: sellerInfo.tenantId });
    let clientInfo = ssAccount.trunkClient, dresList = [];

    before('微信登录小程序', async function () {
        await ssReq.userLoginWithWx({ mobile: clientInfo.mobile, openId: clientInfo.openId, tenantId: sellerInfo.tenantId });
        Object.assign(clientInfo, LOGINDATA);
        // console.log(`\n clientInfo=${JSON.stringify(clientInfo)}`);
        // console.log(`\n sellerInfo=${JSON.stringify(sellerInfo)}`);
    });

    it('查询是当前openId是否关注公众号', async function () {
        const res = await ss.ugr.getSubscribeMpStatus({ openId: clientInfo.openId, sellerTenantId: sellerInfo.tenantId });
        // console.log(`res=${JSON.stringify(res)}`);
    });

    it('分享进入门店数据保存', async function () {
        const res = await ss.ugr.shareToUp({ openId: clientInfo.openId, shopId: sellerInfo.tenantId, srcType: 3, srcId: sellerInfo.tenantId });
    });

    it('切换门店刷新会话', async function () {
        const res = await ss.ugr.switchShop({ sellerTenantId: sellerInfo.tenantId });
    });

    it('修改当前用户信息', async function () {
        const res = await sp.spugr.updateUser({ _cid: clientInfo.clusterCode, _tid: clientInfo.tenantId, nickName: "星尘", avatar: "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLoNkBtbatvibCZpyZjolZZdqU80leVADU04SXRmOibYliagoyjpR1Iaer60M5icQ3xfiaf9IW0NF6iafjw/132" });
    });

    it('修改状态账号设备在线状态', async function () {
        const res = await sp.spugr.updateOnlineFlag({ _cid: clientInfo.clusterCode, _tid: clientInfo.tenantId, onlineFlag: 2 });
    });

    it.skip('买家首页获取店铺详情', async function () {
        const res = await ss.spugr.getShopForBuyerInSeller({ id: sellerInfo.tenantId });
    });

    it('获取个性化参数列表', async function () {
        const res = await ss.config.findProductParams({ ownerId: clientInfo.tenantId, domainKind: 'business', sellerId: sellerInfo.tenantId, codes: ',show_inventory_in_ssmall,allow_shelves_when_inventory_less_than_zero,open_after_sale,goods_pick_up_by_self' });
    });

    it('装修预览', async function () {
        const res = await ss.spdresup.getPageFragment({ buyerId: clientInfo.tenantId, unitId: sellerInfo.unitId });
    });

    it('IM开始沟通', async function () {
        const res = await sp.spIM.startConv({ buyerId: clientInfo.userId, salesUnitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
    });

    it('查询商品分类', async function () {
        const res = await ss.spdresb.findDressClassByBuyer({ sellerUnitId: sellerInfo.unitId });
    });

    it('获取字典类别列表613', async function () {
        const res = await ss.config.getSpbDictList({ typeId: 613, flag: 1, sellerUnitId: sellerInfo.unitId });
    });

    it('获取字典类别列表606', async function () {
        const res = await ss.config.getSpbDictList({ typeId: 606, flag: 1, sellerUnitId: sellerInfo.unitId });
    });

    it('查看购物车', async function () {
        const res = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 });
    });

    it('保存客户信息', async function () {
        const res = await ss.spmdm.saveMemberInfo({ tenantId: clientInfo.shopId, nickName: "星尘", avatar: "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTLoNkBtbatvibCZpyZjolZZdqU80leVADU04SXRmOibYliagoyjpR1Iaer60M5icQ3xfiaf9IW0NF6iafjw/132" });
    });

    it('通过分享访问过的店铺', async function () {
        const res = await sp.spdresb.getShareInShopList({ pageSize: 1000 });
    });

    it('根据商品分组id获取商品列表', async function () {
        const res = await ss.spdresb.getSpuListByGroupId({ groupId: 98332, buyerId: clientInfo.tenantId, tenantId: sellerInfo.tenantId });
    });

    it('获取店铺详情', async function () {
        await sp.spugr.getShop({ id: sellerInfo.tenantId });
    });

    it('获取全部商品列表', async function () {
        const res = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        dresList = res.result.data.rows;
        expect(dresList, `商品列表无商品\n${res.reqUrl}\n${JSON.stringify(res.result)}`).to.have.lengthOf.above(0);
    });

    describe('查询商品', async function () {
        const dres = dresManage.setupDres();
        before(async function () {
            if (dresList.length < 1) this.skip();
            dres.setByEs(dresList[0]);
            dres.clusterCode = sellerInfo.clusterCode;
            dres.tenantId = sellerInfo.tenantId;
        });
        it('获取商品可用优惠券列表', async function () {
            await ss.sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: dres.id });
        });

        it('买家查询商品详情', async function () {
            await dres.getDetailForBuyer();
        });

        it.skip('根据skuIds批量查询卖家sku的实时信息', async function () {
            const res = await ss.spdresb.getSkusInfoInBatch({ buyerId: clientInfo.tenantId, spus: { spuId: dres.id, skuIds: dres.skus.keys() } });
            // console.log(`res=${JSON.stringify(res)}`);
        });

        it('商品查看数自增(记录)', async function () {
            await sp.spdresb.saveBuyerView({ spuId: dres.id, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 2 });
        });
    });

    describe.skip('订单', async function () {
        let purRes;
        before('买家下单', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.trunkSeller.mobile, shopName: ssAccount.trunkSeller.shopName });
            console.log(LOGINDATA);

            const dresList = await ssReq.getDresList(1);
            await ssReq.userLoginWithWx({ mobile: clientInfo.mobile, openId: clientInfo.openId, tenantId: sellerInfo.tenantId });
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);

            console.log(dresList[0]);

            const purJson = await billManage.mockPurParam(dresList[0]);
            purRes = await billManage.createPurBill(purJson);
        });
        it('', async function () {

        });
        describe('买家支付', async function () {
            before('买家支付', async function () {
                const payRes = await sp.spTrade.createPay({
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [purRes.result.data.rows[0].billId],
                    payMoney: purRes.params.jsonParam.orders[0].main.money,
                    appId: 'wxc09fcf8155edcecd'
                });
                await sp.spTrade.receivePayResult({
                    mainId: payRes.result.data.payDetailId,
                    amount: payRes.params.jsonParam.payMoney,
                    purBillId: purRes.result.data.rows[0].billId
                });
            });
            it('', async function () {

            });
            describe('卖家发货', async function () {
                before('卖家发货', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.trunkSeller.mobile, shopName: ssAccount.trunkSeller.shopName });
                    const salesFindBillFull = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId });
                    console.log(`\nsalesFindBillFull=${JSON.stringify(salesFindBillFull)}`);
                    const details = salesFindBillFull.result.data.skus.map((sku) => {
                        return {
                            salesDetailId: sku.id,
                            num: sku.skuNum
                        };
                    });
                    const logisList = await sp.spconfb.findLogisList1({ cap: 1 });
                    const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
                    const deliverSalesBill = await sp.spTrade.deliverSalesBill({
                        main: {
                            logisCompId: logisList.result.data.rows[randonNum].id,
                            logisCompName: logisList.result.data.rows[randonNum].name,
                            waybillNo: common.getRandomNumStr(12),
                            buyerId: salesFindBillFull.result.data.bill.buyerId,
                            // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                            hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                        },
                        details: details
                    }).then(res => res.result.data.billId);
                    console.log(`deliverSalesBill=${JSON.stringify(deliverSalesBill)}`);
                });
                it('', async function () {

                });
                describe('买家确认收货', async function () {
                    before('买家确认收货', async function () {
                        await ssReq.userLoginWithWx({ mobile: clientInfo.mobile, openId: clientInfo.openId, tenantId: sellerInfo.tenantId });
                        //买家确认收货
                        await sp.spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
                    });
                    it('买家采购单列表', async function () {
                        const purBillList = await sp.spTrade.purFindBills({
                            pageSize: 20,
                            pageNo: 1,
                            orderBy: 'proTime',
                            orderByDesc: true,
                            statusType: 4
                        });
                    });
                    it('买家采购单详情', async function () {
                        purInfo = await sp.spTrade.findPurBillFull({
                            id: purRes.result.data.rows[0].billId
                        });
                        console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);
                    });
                    it('卖家销售单列表', async function () {
                        await ssReq.ssSellerLogin({ code: ssAccount.trunkSeller.mobile, shopName: ssAccount.trunkSeller.shopName });
                        const salesFindBills = await sp.spTrade.salesFindBills();
                        console.log(`salesFindBills=${JSON.stringify(salesFindBills)}`);
                    });
                    it('卖家销售单详情', async function () {
                        const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId });
                        console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
                    });
                });
            });
        });
    });

});