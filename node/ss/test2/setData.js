'use strict';
const caps = require('../../data/caps');
const common = require('../../lib/common');
const ssReq = require('../help/ssReq');
const basicJson = require('../help/basicJson');
const spDoc = require('../../reqHandler/sp/doc_server/doc');
const spugr = require('../../reqHandler/sp/global/spugr');
const spmdm = require('../../reqHandler/sp/biz_server/spmdm');
const spAuth = require('../../reqHandler/sp/confc/spAuth');
const spCommon = require('../../reqHandler/sp/global/spCommon');
const ssAccount = require('../data/ssAccount');
const spTrade = require('../../reqHandler/sp/biz_server/spTrade');
const spdresb = require('../../reqHandler/sp/biz_server/spdresb');
const dataPrepare = require('../help/wmsHelp/dataPrepare');
const fs = require('fs');
const path = require('path');

describe('数据维护', function () {
    this.timeout(300000);
    //let sellerMobile = ssAccount.seller.mobile,      //等到配置文件好了之后，从配置文件里面拿
    // clientMobile = ssAccount.client.mobile;

    // before('添加白名单', async function () {
    //     await spAuth.staffLogin();
    //     await spAuth.saveWhiteList(sellerMobile);
    //     await spAuth.saveWhiteList(clientMobile);
    // });

    describe('文档数据维护', function () {
        let localPath, docData;
        before(async function () {
            localPath = path.join(__dirname, '../data/doc.json');

            docData = JSON.parse(fs.readFileSync(localPath));
            docData[caps.name] = { image: {}, card: {}, idCard: {} };
            // console.log('%j',docData);

            // docData[caps.name].image.shopLogo = images;
        });
        after(async function () {
            fs.writeFile(localPath, JSON.stringify(docData), (err) => {
                if (err) console.error(err);
            });
        });
        it('shopBackground', async function () {
            let images = [];
            for (let index = 1; index < 6; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/门店背景图/background${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            // console.log(images);
            docData[caps.name].image.shopBackground = images;
            // console.log(docData);
        });
        it('model', async function () {
            let images = [];
            for (let index = 1; index < 11; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/模特图片/model${index}.jpg`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.model = images;
        });
        it('dres', async function () {
            let images = [];
            for (let index = 1; index < 6; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/商品图片/门店商品/dres${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.dres = images;
        });
        it('medal', async function () {
            let images = [];
            const docPath = path.join(__dirname, `../doc/首页换图/勋章/medal.png`);
            await spDoc.upload({ path: docPath }).then(res => images.push(res));
            docData[caps.name].image.medal = images;
        });
        it('banner', async function () {
            let images = [];
            for (let index = 1; index < 5; index++) {
                const docPath = path.join(__dirname, `../doc/首页换图/商品图片/首页/banner${index}.png`);
                await spDoc.upload({ path: docPath }).then(res => images.push(res));
            };
            docData[caps.name].image.banner = images;
        });
        it('card', async function () {
            let card = {};
            const docPath = path.join(__dirname, `../doc/card.jpg`);
            await spDoc.upload({ path: docPath }).then(res => card = res);
            // console.log(images);
            docData[caps.name].card.cardImage = card;
            docData[caps.name].card.cardNum = '6226928888888888';
            // console.log(docData);
        });
        it('idCard', async function () {
            let frontIdCard = {}, backIdCard = {};
            const frontDocPath = path.join(__dirname, `../doc/idCard/frontIdCard.jpg`);
            const backDocPath = path.join(__dirname, `../doc/idCard/backIdCard.jpg`);
            await spDoc.upload({ path: frontDocPath }).then(res => frontIdCard = res);
            await spDoc.upload({ path: backDocPath }).then(res => backIdCard = res);
            // console.log(images);
            docData[caps.name].idCard.frontImage = frontIdCard;
            docData[caps.name].idCard.backImage = backIdCard;
            docData[caps.name].idCard.cardMsg = {
                address: "浙江省慈溪市新浦镇荣誉村阮家丁",
                nation: "汉",
                validDate: "2012.01.18-2022.01.18",
                authority: "慈溪市公安局",
                sex: "男",
                name: "高孟忠",
                birth: "1991/8/19",
                id: "330282199108194998"
            };
            // console.log(docData);
        });
    });

    describe.skip('web端维护', async function () {
        before(async function () {
            await spAuth.staffLogin();
        });
        it('保存筛选配置-推荐下拉', async function () {
            // const configList = await spCommon.getSpFilterConfigList({ filterType: 1 });
            // console.log(`configList=${JSON.stringify(configList)}`);

            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 1, typeName: '标签', showOrder: 1 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 2, typeName: '勋章', showOrder: 2 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 3, typeName: '风格', showOrder: 3 });
            await spCommon.saveSpFilterConfig({ filterType: 1, typeValue: 4, typeName: '价格', showOrder: 4 });
        });
        it('公司信息管理', async function () {
            // spAdmin 公司信息管理
        });
    });

    describe('卖家基本信息维护', function () {

        it('注册卖家，开通店铺', async function () {
            //需要修改为门店名称
            let name = ssAccount.seller.shopName;
            await ssReq.spLogin({ code: sellerMobile });
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            //创建门店
            await ssReq.createEntTenant({
                name: name,
            });

            //切换门店
            const sellerTenantId = await spugr.findAvailableTenantIds().then((res) => {
                console.log(`res=${JSON.stringify(res)}`);

                return _.findKey(res.result.data, (val) => val == name);
            });
            await spugr.changeTenantInSession({ tenantId: sellerTenantId });
            console.log(`sellerTenantId=${JSON.stringify(sellerTenantId)}`);

            let saveParam = await basicJson.shopJson();
            saveParam.id = sellerTenantId;
            saveParam.name = name;
            const saveRes = await spugr.updateShop(saveParam);
            await spAuth.staffLogin();
            await spugr.onlineShopById({ id: sellerTenantId, forceFlag: true, autoTestToken: 'autotest' });
        });

        it('卖家注册消息', async function () {
            await ssReq.spSellerLogin();
            let s = await spugr.findAvailableTenantIds();
            console.log(`s=${JSON.stringify(s)}`);
            await spmdm.registerUserMessageCid();
        });

        it('卖家店铺新增商品', async function () {
            await ssReq.spSellerLogin();
            for (let i = 0; i < 5; i++) {
                const res = await ssReq.saveDresFull();
                console.log(`res=${JSON.stringify(res.result)}`);

                // await common.delay(100);
            };
        });

        it.skip('店铺下线', async function () {
            await ssReq.spSellerLogin();
            let tenantId = LOGINDATA.tenantId;
            await spAuth.staffLogin();
            await spugr.offlineShopById({ id: tenantId });
        });
    });


    describe('slh注册平台', function () {
        let tenantId;
        const name = '花木牛仔屋'   //需要开通的好店的名称
        it('slh登录', async function () {
            await ssReq.slhStaffLogin();

            let createJson = await basicJson.createTetantBySlhJson();
            createJson.name = name;
            createJson.slhShopId = LOGINDATA.invid;
            createJson.storeInvIds = LOGINDATA.invid;
            createRes = await spugr.createGoodShopBySlh(createJson);
        });

        it('门店基本信息维护', async function () {
            await ssReq.loginGoodShopBySlh();
            let saveParam = await basicJson.shopJson();
            saveParam.id = LOGINDATA.tenantId;
            delete saveParam.name;
            await spugr.updateShop(saveParam);
        });

        it('门店商户信息维护', async function () {
            let json = basicJson.merchantJson({ mobile: '15988413448', contact: 'auto' });
            await spugr.createMerchat(json);
        });

        it('门店上线', async function () {
            await spAuth.staffLogin();
            await spugr.onlineShopById({ id: tenantId });
        });
    });

    describe('脚本执行完毕后检查', function () {

        it('脚本执行完检查最近购买的店铺列表', async function () {
            await ssReq.spClientLogin();
            let recentBuyShopList = await spTrade.getRecentBuyShopList({ pageSize: 10, pageNo: 1 }).then(res => res.result.data.rows);
            let info = recentBuyShopList.find(res => res.shopName == ssAccount.seller.shopName);
            if (info == undefined) {
                throw new Error('店铺下线之后，最近购买列表里面此门店找不到了，应该依然存在');
            } else {
                expect(info.shopFlag, '店铺下线之后，但是状态字段还不是下线状态').to.be.equal(0);
            }
        });
    });

    describe('清理数据脚本', function () {

        it('清理banner', async function () {
            await spAuth.staffLogin();
            const info = await spCommon.findBanner({ pageSize: 100, pageNo: 1, jsonParam: { flag: 0 } });
            //剩下3个
            for (let i = 0; i < info.result.data.rows.length - 3; i++) {
                await spCommon.deleteBanner({ id: info.result.data.rows[i].id });
                console.log(`删除了${info.result.data.rows[i].id}`);
            };
        });
    });

    describe.skip('影子系统', function () {
        let clientInfo, sellerInfo;
        describe('卖家影子系统', function () {

            before('给卖家设置影子系统', async function () {
                await ssReq.spSellerLogin();
                sellerInfo = LOGINDATA;

                await spAuth.staffLogin();
                //去店铺影子列表里面查卖家
                let shadowList = await spugr.findAllShopByParams({ name: sellerInfo.shopName, shadowFlag: 1 });
                let info = shadowList.result.data.rows.find(shop => shop.id == sellerInfo.tenantId);
                if (info == undefined) {
                    console.log('进入到设置影子分支了');
                    //如果列表里面没有找到卖家店铺，那么就将卖家店铺设置为影子店铺
                    await spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 1, tenantType: 1 });
                };
            });

            it('检验', async function () {
                await common.delay(3000);
                let shadowList = await spugr.findAllShopByParams({ name: sellerInfo.shopName, shadowFlag: 0 });
                let info = shadowList.result.data.rows.find(shop => shop.id == sellerInfo.tenantId);
                expect(info, `给店铺设置了影子，但是正常店铺还是可以搜索到此门店${JSON.stringify(info)}`).to.be.undefined;
            });

            it('es搜索检验', async function () {

                let searchDres = await spdresb.searchDres({ queryType: 4, tenantId: sellerInfo.tenantId, shadowFlag: 0 });
                let dresInfo = searchDres.result.data.dresStyleResultList;
                expect(dresInfo.length, `店铺添加到白名单了，但是还是可以搜索到商品`).to.be.equal(0);
            });
        });

        describe('买家', function () {
            let tenantIds;
            before('', async function () {
                await ssReq.spClientLogin();
                clientInfo = LOGINDATA;

                await spAuth.staffLogin();
                let whiteList = await spugr.getBuyerList({ nameLike: LOGINDATA.nickName, mobile: clientInfo.code, shadowFlag: 1 });
                let buyer = whiteList.result.data.rows.find(val => val.buyerTenantId == clientInfo.tenantId);

                if (buyer == undefined) {
                    console.log('进入添加白名单');
                    await spugr.setShadowFlag({ id: clientInfo.tenantId, shadowFlag: 1, tenantType: 0 });
                };
            });

            // it('验证', async function () {
            //     await common.delay(2000);
            //     await spReq.spClientLogin();
            //     let res = await spugr.findShopBySearchToken({ pageSize: 30, pageNo: 1, shadowFlag: 1 });
            //     let ids = res.result.data.rows.map(val => {
            //         return { name: val.name, shadowFlag: val.shadowFlag };
            //     });
            //     console.log(`ids=${JSON.stringify(ids)}`);
            // });

            it('es', async function () {
                await common.delay(2000);
                await ssReq.spClientLogin();
                let res = await spdresb.searchDres({ pageSize: 40, pageNo: 1, tenantId: sellerInfo.tenantId, queryType: 0 }).then(res => res.result.data.dresStyleResultList);
                expect(res.length, '买家和卖家都在白名单，但是买家搜不到卖家的商品').to.be.above(0);
            });

            it('es搜索', async function () {
                await spAuth.staffLogin();
                //查询白名单里面的店铺
                let shadowList = await spugr.findAllShopByParams({ pageSize: 40, pageNo: 1, shadowFlag: 1 });
                tenantIds = shadowList.result.data.rows.map(val => val.id);
                await ssReq.spClientLogin();
                let ids;
                for (let i = 0; i < 8; i++) {
                    let res = await spdresb.searchDres({ pageSize: 40, pageNo: 1, queryType: i }).then(res => res.result.data.dresStyleResultList);
                    ids = common.dedupe(res.map(val => val.tenantId));
                    expect(tenantIds, `白名单里面的租户${tenantIds} ,es搜出来的租户${ids},不是白名单店铺的商品也可以搜索出来`).to.include.members(ids);
                };
            });

            it('查询店铺', async function () {
                let shop = await spugr.findShopBySearchToken({ pageSize: 40, pageNo: 1 }).then(res => res.result.data.rows);
                let ids = shop.map(val => val.tenantId);
                expect(tenantIds, `白名单里面的租户${tenantIds} ,es搜出来的租户${ids},不是白名单店铺的商品也可以搜索出来`).to.include.members(ids);
            });
        });

        describe.skip('移除', function () {

            it('移除卖家', async function () {
                await ssReq.spSellerLogin();
                sellerInfo = LOGINDATA;

                await spAuth.staffLogin();
                await spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 0, tenantType: 1 });
            });

            it('移除买家', async function () {
                await ssReq.spClientLogin();
                clientInfo = LOGINDATA;

                await spAuth.staffLogin();
                await spugr.setShadowFlag({ id: clientInfo.tenantId, shadowFlag: 0, tenantType: 0 });
            });
        });
    });

    describe('仓储数据准备脚本', function () {
        let dp;
        it('准备市场', async function () {
            await spAuth.staffLogin();
            dp = new dataPrepare.PrepareData();
            await dp.dataPrepare();
            // console.log(dp);
        });

        it('修改两个店铺的市场', async function () {
            await ssReq.spSellerLogin();
            let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
            shopInfo.marketId = dp.marketId;
            await spugr.updateShop(shopInfo);
        });

        it('修改中洲店的市场', async function () {
            await ssReq.spSellerLogin({ shopName: '中洲店' });
            let shopInfo = await spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
            shopInfo.marketId = dp.marketId;
            await spugr.updateShop(shopInfo);
        });
    });
});
