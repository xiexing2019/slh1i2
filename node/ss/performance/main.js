const common = require('../../lib/common');
const ss = require('../../reqHandler/ss');
const fs = require('fs');
const readline = require('readline');
const child_process = require('child_process');

const args = process.argv.slice(2);

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const testCase = [
    '1:注册登录',
    '2:进入首页',
    '3:查看货品列表',
    '4:查询商品详情'
];

rl.setPrompt(`输入测试场景编号\n${testCase.join('\n')}\n\nclose:退出\n\n>`);
rl.prompt();

rl.on('line', async function (line) {
    switch (line) {
        case '1':
            console.log(`开始测试场景${line}`);
            child_process.fork('node/ss/test2/performance/test/login', args, { stdio: 'inherit' });
            rl.prompt();
            break;
        case '2':
            console.log(`开始测试场景${line}`);
            rl.prompt();
            break;
        case '3':
            console.log(`开始测试场景${line}`);
            rl.prompt();
            break;
        case '4':
            console.log(`开始测试场景${line}`);
            child_process.fork('node/ss/test2/performance/test/getFullForBuyer', args, { stdio: 'inherit' });
            rl.prompt();
            break;
        case 'close':
            rl.close();
            break;
        default:
            rl.setPrompt(`场景编号'${line}'错误,请重新输入:`);
            rl.prompt();
            break;
    }
});

rl.on('close', function () {
    console.log('测试结束');
    process.exit(0);
});