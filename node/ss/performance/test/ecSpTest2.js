const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../help/testCase');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

// const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../data/loginData/${[caps.name]}.json`)));
// delete clientJson.lastMobile;
// const clientData = Object.values(clientJson);

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    testCase.logAnalysis.start();
    for (let count = 0; count < totalNum; count++) {
        await common.delay(1000);
        const promises = new Array(Number(testNum)).fill({}).map((ele, index) => {
            // console.log(common.getCurrentPreciseTime());
            LOGINDATA.sessionId = 'ssg10d-18-35D8F309-B4CA-8E71-A549-9D2D0A8FCA71';//clientData[index].sessionId;
            return ss.test.baseTest({ _cid: 'spb11d' }).then(res => {
                console.log(res.result);
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
        });
        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));
    }
    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    console.log(testCase.logAnalysis.getInfo());
    process.exit(0);
});