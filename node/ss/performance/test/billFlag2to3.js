const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../help/testCase');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');
const billManage = require('../../help/billManage');

const bill = async function () {
    const billPath = path.join(__dirname, `../data/bill/8-5-5-300.json`);
    const bill = JSON.parse(fs.readFileSync(billPath));
    // console.log(bill);
    // console.log(Math.min.apply([], bill.billIds));
    // console.log(Math.max.apply([], bill.billIds));
    let result, i = 0;
    const con = await ss.creatSqlPool({ dbName: 'spbMysql' });
    while (true) {
        if ((!result) || result[0].flag == 2) {
            result = await con.query(`SELECT flag, id, updated_date, created_date FROM spb001.sp_pur_bill WHERE unit_id = 13542 AND
            flag in (2, 3) AND id >= ${Math.min.apply([], bill.billIds)} AND id <= ${Math.max.apply([], bill.billIds)} ORDER BY flag`).then(res => res[0]);
            i += 1
            console.log(i);

            // console.log(`result=${JSON.stringify(result)}`);
        } else {
            break;
        }
    }
    let total = 0
    result.forEach(res => {
        total += new Date(res.updated_date) - new Date(res.created_date);
    });
    console.log(`${result.length}个订单:最小${Math.min.apply([], bill.billIds)},最大${Math.max.apply([], bill.billIds)}的更新时间-创建时间的平均时间为${total / result.length}`);

    await con.end();
};

bill();