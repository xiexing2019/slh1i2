const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量,循环数,sku数: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1, count = 1] = line.trim().split(',');
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);
    console.log(`count:`, count);

    if (typeof Number(testNum) != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
        const dresList = await testCase.getDresSpuList().then(res => res.result.data.rows);
        // console.log(`dresList=${JSON.stringify(dresList)}`);
        await testCase.getFullForBuyer(dresList[0].id);
    }

    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);
    testCase.logAnalysis.start();

    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientData.slice(0, testNum).map((ele, index) => {
            // console.log(common.getCurrentPreciseTime());
            LOGINDATA.clusterCode = clientData[index].clusterCode;
            LOGINDATA.tenantId = clientData[index].tenantId;
            LOGINDATA.sessionId = clientData[index].sessionId;
            //购物车json
            testCase.getCartJson(testCase.dresFull, count);
            const cartJson = testCase.cartJson;
            return sp.spTrade.saveCartInBatchs(cartJson)
                .then((res) => {
                    // console.log(JSON.stringify(res));
                    // 统计
                    testCase.logAnalysis.add(res);
                }).catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));
    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});