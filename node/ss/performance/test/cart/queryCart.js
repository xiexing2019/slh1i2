const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const ssAccount = require('../../../data/ssAccount');

const testCase = new TestCase();

/** 卖家信息 */
const sellerInfo = ssAccount.sellerPerformance;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof Number(testNum) != 'number') {
        return;
    }

    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);
    testCase.logAnalysis.start();


    for (let count = 0; count < totalNum; count++) {
        await common.delay(1000);
        const promises = clientData.slice(0, testNum).map((ele, index) => {
            // console.log(common.getCurrentPreciseTime());
            LOGINDATA.clusterCode = clientData[index].clusterCode;
            LOGINDATA.tenantId = clientData[index].tenantId;
            LOGINDATA.sessionId = clientData[index].sessionId;
            return ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 })
                .then((res) => {
                    console.log(`${res.opTime},${res.duration}`);
                    // console.log(JSON.stringify(res));
                    // 统计
                    testCase.logAnalysis.add(res);
                }).catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));
    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});