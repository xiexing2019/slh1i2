const common = require('../../../lib/common');
const sp = require('../../../reqHandler/sp')
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];
console.log(docData);

/**
 * 修改商品图片
 */
async function updateSpuPicture() {
    await ssReq.ssSellerLogin({ code: '13400001234', shopName: '零售版' });
    const sellInfo = _.cloneDeep(LOGINDATA);
    // console.log(sellInfo);
    let qlRes = await spdresb.findSellerSpuList({ flags: 2, pageSize: 0 }).then(res => res.result.data.rows).then(res => res.map(data => data.id));
    console.log(qlRes);
    for (let i = 0; i < qlRes.length; i++) {
        BASICDATA['2002'] = await sp.spugr.getDictList({ typeId: '2002', flag: 1 })
            .then((res) => res.result.data.rows.map(ele => { return { codeName: ele.codeName, codeValue: ele.codeValue } }));
        const docTypeId = BASICDATA['2002'].find((data) => data.codeName == '图片').codeValue;
        let picture = common.randomSort(_.cloneDeep(docData.image.model)).slice(0, 3).map(res => { return { typeId: docTypeId, ...res } });
        console.log(picture);
        let a = await spdresb.updateSpuForSeller({ id: qlRes[i], docHeader: picture, docContent: picture });
        console.log(a);
    }
}

// updateSpuPicture()