const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量，totalNum数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }
    await testCase.sellerLogin();
    await testCase.changeBillPayFlag(testNum * totalNum);
    console.log(`从数据库修改了${testCase.waitPayBills.length}条数据`);

    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);

    testCase.logAnalysis.start();
    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = new Array(testNum).fill({}).map((ele, index) => {
            const No = i * testNum + index;
            // console.log(common.getCurrentPreciseTime());
            // console.log(No);
            const client = clientData.find(obj => obj.unitId == testCase.waitPayBills[No].unit_id);
            // console.log(`\n client=${JSON.stringify(client)}`);

            LOGINDATA.clusterCode = client.clusterCode;
            LOGINDATA.tenantId = client.tenantId;
            LOGINDATA.sessionId = client.sessionId;
            return sp.spTrade.createPay({ orderIds: [testCase.waitPayBills[No].id], payMoney: testCase.waitPayBills[No].total_money, payType: 5, payMethod: 2, payerOpenId: -10000, appId: 'wx4bcc55bd3ce7e912' })
                .then((res) => {
                    // console.log(JSON.stringify(res));
                    // 统计
                    testCase.logAnalysis.add(res);
                }).catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));
    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});