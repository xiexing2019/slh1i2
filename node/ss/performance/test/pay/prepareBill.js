const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const billManage = require('../../../help/billManage');
const basicJson = require('../../../help/basicJson');

(async function () {
    const testCase = new TestCase();
    await testCase.sellerLogin();
    await testCase.getdresListforbill(1);

    await testCase.clientLogin();
    // const areaJson = await basicJson.getAreaJson();
    // const json = await basicJson.addAddrJson(areaJson);
    // json.recInfo.isDefault = 1;
    // const val = await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);
    // console.log(val);
    const billDataPath = path.join(__dirname, `../../data/billData/${caps.name}.json`);
    let billData = [];
    for (let index = 0; index < 2; index++) {
        await common.delay(500);
        const purJson = billManage.mockPurParam(testCase.dresList.slice(0, 1), { count: 1 });
        purJson.productVersion = '2.9.0';
        purJson.orders[0].main.addressId = 8470;
        await sp.spTrade.savePurBill(purJson)
            .then(res => {
                console.log(res.result);
                billData.push({
                    billId: res.result.data.rows[0].billId,
                    payMoney: res.params.jsonParam.orders[0].main.money,
                    tenantId: testCase.client.tenantId,
                    clusterCode: testCase.client.clusterCode,
                    unitId: testCase.client.unitId,
                    saasId: testCase.client.saasId,
                    sessionId: testCase.client.sessionId
                });
            });
    }
    fs.writeFile(billDataPath, JSON.stringify(billData), function (err) {
        if (err) console.log(err);
    });
})();


