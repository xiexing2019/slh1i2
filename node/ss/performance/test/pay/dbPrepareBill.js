const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const billManage = require('../../../help/billManage');
const basicJson = require('../../../help/basicJson');

(async function () {
    const testCase = new TestCase();
    await testCase.sellerLogin();
    await testCase.clientLogin();

    let billNum = 1;
    const con = await ss.creatSqlPool({ dbName: 'spbMysql' });

    const selectRes = await con.query(`
        SELECT
            pur.id,
            pur.total_money,
            pur.unit_id
        FROM
            spb001.sp_pur_bill AS pur
        INNER JOIN spb002.sp_sales_bill AS sales ON pur.bill_no = sales.bill_no
        WHERE
            pur.seller_unit_id = ${testCase.seller.unitId}
        AND pur.flag != 2
        ORDER BY
            pur.bill_no DESC
        LIMIT ${billNum}`).then(res => res[0]);
    console.log(`selectRes=${JSON.stringify(selectRes)}`);

    const updateRes = await con.execute(`
        UPDATE spb001.sp_pur_bill AS pur
        INNER JOIN spb002.sp_sales_bill AS sales ON pur.bill_no = sales.bill_no
        SET pur.pay_flag = 0,
            pur.flag = 3,
            pur.pay_time = NULL,
            sales.pay_flag = 0,
            sales.flag = 3,
            sales.pay_time = NULL
        WHERE
            pur.id in (${selectRes.map(ele => ele.id)})`).then(res => res[0]);
    console.log(`updateRes=${JSON.stringify(updateRes)}`);

    await con.end();
    // fs.writeFile(billDataPath, JSON.stringify(billData), function (err) {
    //     if (err) console.log(err);
    // });
})();


