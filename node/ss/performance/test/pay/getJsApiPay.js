const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');

// 获取用户登录信息
const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
delete clientJson.lastMobile;
const clientData = Object.values(clientJson);

// 获取微信openId (内部环境 需要真实数据)
const openIds = String(fs.readFileSync(path.join(__dirname, `../../data/payData/${caps.name == 'ss_online' ? 'openIds' : 'openId'}.txt`))).split('\n');

const channelCode = ['ss_test', 'ss_chk'].includes(caps.name) ? '20971521543484004692' : '20971521553236802954';

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量,循环次数(默认1): `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
    }

    //
    testCase.logAnalysis.start();
    let i = 0, openIdLength = openIds.length;
    for (let index = 0; index < totalNum; index++) {
        await common.delay(1000);
        const promises = clientData.slice(0, testNum).map((ele, idx) => {
            LOGINDATA.clusterCode = ele.clusterCode;
            LOGINDATA.tenantId = ele.tenantId;
            LOGINDATA.sessionId = ele.sessionId;
            i++;
            const openId = openIds[i % openIdLength];

            return sp.spTrade.getJsApiPay({
                bizType: 1250,
                description: "商陆花平台交易",
                srvHost__: "pay.hzdlsoft.com",
                charEncode__: "UTF-8",
                reqsn: null,
                totalAmount: 31800.00,
                payType: 1048576,
                bizOrderId: `lxx${uuid.v4()}`,
                globalId__: null,
                customerId: null,
                unitId: null,
                cliIp__: "152.136.38.180",
                srvCtxUrl__: "https://pay.hzdlsoft.com/pay",
                callbackUrl: "https://ss.hzecool.com/spb/api.do?_cid=ssb12&_tid=1646&apiKey=ec-sppur-payBill-receivePayResult&_unitId=1646",
                payerOpenId: openId,
                shopId: null,
                srvScheme__: "https",
                channelCode: channelCode
            }).then((res) => {
                console.log(JSON.stringify(res));
                // 统计
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));

    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});