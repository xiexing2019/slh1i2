const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const billManage = require('../../../help/billManage');

const testCase = new TestCase();
// (async function () {

// })();

// 获取登录信息
const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
delete clientJson.lastMobile;
const clientData = Object.values(clientJson);

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入用户数量,spu数量，sku数量，totalNum数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, spuNum, skuNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));

    console.log(`testNum:`, testNum);
    console.log(`spuNum:`, spuNum);
    console.log(`skuNum:`, skuNum);
    console.log(`totalNum:`, totalNum);

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.getdresListforbill(20);
        console.log(`初始化商品数量${testCase.dresList.length}`);
        await testCase.clientLogin();
    }
    // const billPath = path.join(__dirname, `../../data/bill/${testNum}-${spuNum}-${skuNum}-${totalNum}.json`);
    // const bills = { billIds: [] };

    // const billDataPath = path.join(__dirname, `../data/billData/${caps.name}.json`);
    // let billData = {};
    // if (testNum != 0) {
    //     billData = JSON.parse(fs.readFileSync(billDataPath));
    // }

    testCase.logAnalysis.start();
    const totalClientNum = 500;
    for (let count = 0; count < totalNum; count++) {
        await common.delay(1000);
        const promises = new Array(testNum).fill({}).map((ele, index) => {
            testCase.dresList = common.randomSort(testCase.dresList);
            const purJson = billManage.mockPurParam(testCase.dresList.slice(0, spuNum), { count: skuNum });
            purJson.productVersion = '2.9.0';
            // console.log(`\n purJson=${JSON.stringify(purJson)}`);
            // console.log(`订单明细数量${purJson.orders[0].details.length}`);
            const clientNo = (count * 10 + index) % totalClientNum;
            console.log(`clientNo=${clientNo} tid=${clientData[clientNo].tenantId}`);
            LOGINDATA.clusterCode = clientData[clientNo].clusterCode;
            LOGINDATA.tenantId = clientData[clientNo].tenantId;
            LOGINDATA.sessionId = clientData[clientNo].sessionId;
            purJson.orders[0].main.addressId = clientData[clientNo].addId;
            purJson.orders[0].main.hashKey = `${Date.now()}${common.getRandomNum(100, 999999)}`;
            purJson.orders[0].main.partnerId = 0;
            return sp.spTrade.savePurBill(purJson)
                .then((res) => {
                    console.log(JSON.stringify(res));
                    // console.log(res.duration);
                    // 统计
                    testCase.logAnalysis.add(res);
                    // bills.billIds.push(res.result.data.rows[0].billId);

                }).catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
                // fs.writeFile(billPath, JSON.stringify(bills), function (err) {
                //     if (err) console.log(err);
                // });
            })
            .catch(err => console.log(err));
    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});