const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const fs = require('fs');
const path = require('path');
const caps = require('../../../../data/caps');


const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量,循环次数: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof Number(testNum) != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
        const dresList = await testCase.getDresSpuList().then(res => res.result.data.rows);
        // console.log(`dresList=${JSON.stringify(dresList)}`);
        testCase.addDresToDresMap(dresList);
    }

    const dresList = [...testCase.dresMap.values()];
    const dresNum = testCase.dresMap.size;
    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);
    // console.log(dresList);
    testCase.logAnalysis.start();

    for (let count = 0; count < totalNum; count++) {
        await common.delay(1000);
        const promises = clientData.slice(0, testNum).map((ele, index) => {
            // console.log(common.getCurrentPreciseTime());
            LOGINDATA.clusterCode = clientData[index].clusterCode;
            LOGINDATA.tenantId = clientData[index].tenantId;
            LOGINDATA.sessionId = clientData[index].sessionId;
            const _dres = dresList[(index + 1) % dresNum];
            return sp.spdresb.getFullForBuyer({
                spuId: _dres.spuId,
                buyerId: LOGINDATA.tenantId,
                _cid: _dres._cid,
                _tid: _dres._tid,
                cacheFlag: 1,
            }).then((res) => {
                // console.log(JSON.stringify(res));
                // console.log(`${res.opTime},${res.duration}`);

                // 统计
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`${count}执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));
    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});
