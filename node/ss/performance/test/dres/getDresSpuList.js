const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

// 查询商品列表

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,循环次数: ');
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
    }
    testCase.logAnalysis.start();
    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientList.slice(0, testNum).map((values, index) => {
            LOGINDATA.clusterCode = clientList[index].clusterCode;
            LOGINDATA.tenantId = clientList[index].tenantId;
            LOGINDATA.sessionId = clientList[index].sessionId;
            return ss.spchb.getDresSpuList({ tenantId: testCase.seller.tenantId })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    // console.log(`${res.opTime},${res.duration}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });
        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});