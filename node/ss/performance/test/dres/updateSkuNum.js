const basicJson = require('../../../help/basicJson');
const common = require('../../../../lib/common');
const ssRes = require('../../../help/ssReq');
const dresManager = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const path = require('path');
const caps = require('../../../../data/caps');
const fs = require('fs');

/**
 *  保存商品
 *  op同步商品至微商城
 *  批量同步->一个个新建商品
 */

const testCase = new TestCase();
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,商品数量: ');
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.getdresListforbill(20);
    }

    testCase.logAnalysis.start();

    const skus = await sp.spdresb.getFullById({ id: 65595 }).then(res => res.result.data.skus.map(sku => sku.id));

    for (let i = 0; i < totalNum; i++) {
        const json = _.cloneDeep(skus).map(sku => { return { code: sku, num: common.getRandomNum(1000, 2000) } });
        await ss.op.updateFullDresSkuNum({ skus: json })
            .then(res => {
                testCase.logAnalysis.add(res);
                console.log(`result=${JSON.stringify(res.result)},duration=${res.duration}`);
            })
            .catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
        console.log(testCase.logAnalysis.getInfo());
        // await common.delay(1000);
        // const promises = new Array(testNum).fill({}).map((values, index) => {
        //     return ss.op.saveDresSpuFull(basicJson.styleJson())
        //         .then(res => {
        //             testCase.logAnalysis.add(res);
        //             console.log(`result=${JSON.stringify(res.result)},duration=${res.duration}`);
        //         })
        //         .catch(err => {
        //             console.log(err);
        //             testCase.logAnalysis.addFail(err);
        //         });
        // });
        // Promise.all(promises)
        //     .then(res => {
        //         console.log('执行完成');
        //         console.log(testCase.logAnalysis.getInfo());
        //         rl.prompt();
        //     })
        //     .catch(err => {
        //         console.log(err);
        //     });
    };

});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});