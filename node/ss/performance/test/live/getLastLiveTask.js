const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const liveHelp = require('../../../help/liveHelp');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,循环次数(默认 1): ');
rl.prompt();
rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(value => Number(value));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        // 获取直播间信息
        const liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
        // 获取直播任务
        let lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
        // console.log(lastLive);
        if (!lastLive.data.id) {
            let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 1000 }).then(res => res.result.data.rows.map(data => data.id));
            await liveHelp.saveLiveTask({ liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
        } else if (lastLive.data.liveSpus.length < 1000) {
            let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 1000 }).then(res => res.result.data.rows.map(data => data.id));
            await liveHelp.saveLiveTask({ id: lastLive.data.id, liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
        }
        console.log(`直播创建成功，活动商品列表长度为: ${lastLive.data.liveSpus.length}`);
        await testCase.clientLogin();
    }

    testCase.logAnalysis.start();

    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientList.slice(0, testNum).map((values, index) => {
            const clientNo = (i * testNum + index) % (clientList.length - 1);
            LOGINDATA.clusterCode = clientList[clientNo].clusterCode;
            LOGINDATA.tenantId = clientList[clientNo].tenantId;
            LOGINDATA.sessionId = clientList[clientNo].sessionId;
            return ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    // console.log(`${res.opTime},${res.duration},listLen:${res.result.data.liveSpus.length},clientNo:${clientNo}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});