const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const liveHelp = require('../../../help/liveHelp');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

// 查询活动商品信息

// let lastLive;
// (async () => {
//     await testCase.sellerLogin();
//     // 卖家查询spulist
//     let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 200 }).then(res => res.result.data.rows.map(data => data.id));
//     console.log(`买家商品数量为: ${dresSpuList.length}`);
//     // 获取直播间信息
//     const liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
//     // 获取直播任务
//     lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
//     if (!lastLive.data.id) {
//         // let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 250 }).then(res => res.result.data.rows.map(data => data.id));
//         await liveHelp.saveLiveTask({ liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
//         lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
//     } else if (!lastLive.data.liveSpus) {
//         // let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 250 }).then(res => res.result.data.rows.map(data => data.id));
//         await liveHelp.saveLiveTask({ id: lastLive.data.id, liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
//         lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
//     }
//     console.log(`直播创建成功，活动商品列表长度为: ${lastLive.data.liveSpus.length}`);
// })()




const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,循环数量(默认1): ');
rl.prompt();

rl.on('line', async function (line) {
    let lastLive;
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(value => Number(value));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        // 卖家查询spulist
        let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 200 }).then(res => res.result.data.rows.map(data => data.id));
        console.log(`买家商品数量为: ${dresSpuList.length}`);
        // 获取直播间信息
        const liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
        // 获取直播任务
        lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
        if (!lastLive.data.id) {
            // let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 250 }).then(res => res.result.data.rows.map(data => data.id));
            await liveHelp.saveLiveTask({ liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
            lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
        } else if (!lastLive.data.liveSpus) {
            // let dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 250 }).then(res => res.result.data.rows.map(data => data.id));
            await liveHelp.saveLiveTask({ id: lastLive.data.id, liveRoomId: liveRoomInfo.id, timGroupId: liveRoomInfo.timGroupId, spus: dresSpuList.toString() });
            lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result);
        }
        console.log(`直播创建成功，活动商品列表长度为: ${lastLive.data.liveSpus.length}`);
        await testCase.clientLogin();
    }
    testCase.logAnalysis.start();

    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientList.slice(0, testNum).map((values, index) => {
            LOGINDATA.clusterCode = clientList[index].clusterCode;
            LOGINDATA.tenantId = clientList[index].tenantId;
            LOGINDATA.sessionId = clientList[index].sessionId;
            return ss.act.marketBase.getAllSpuInfoByAct({ actId: lastLive.data.id || liveTaskRes.result.data.id, sellerUnitId: testCase.seller.unitId, pageNo: common.getRandomNum(1, 9) })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    console.log(`${res.opTime},${res.duration},listLength:${res.result.data.rows.length}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                // rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});