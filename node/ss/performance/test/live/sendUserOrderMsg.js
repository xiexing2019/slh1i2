const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

// 发送下单消息到聊天室

// let liveRoomInfo, dresSpuList;
// (async function () {
//     await testCase.sellerLogin();
//     // 获取直播间信息
//     liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
//     dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 5 }).then(res => res.result.data.rows.map(data => data.id));
// })()

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,循环次数(默认1): ');
rl.prompt();

rl.on('line', async function (line) {
    let liveRoomInfo, dresSpuList;
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(value => Number(value));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        // 获取直播间信息
        liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
        dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 5 }).then(res => res.result.data.rows.map(data => data.id));
        await testCase.clientLogin();
    }
    testCase.logAnalysis.start();
    for (let i = 0; i < totalNum; i++) {
        const promises = clientList.slice(0, testNum).map((values, index) => {
            LOGINDATA.clusterCode = clientList[index].clusterCode;
            LOGINDATA.tenantId = clientList[index].tenantId;
            LOGINDATA.sessionId = clientList[index].sessionId;
            return ss.live.sendUserOrderMsg({ groupId: liveRoomInfo.timGroupId, nickName: common.getRandomStr(), spuIds: dresSpuList[0], orderType: 0 })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    console.log(`${res.opTime},${res.duration}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });
        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});