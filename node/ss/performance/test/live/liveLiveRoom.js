const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

// 点赞直播间
let liveRoomInfo;
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
rl.setPrompt('请输入并发数量,循环数量(默认1): ');
rl.prompt();
rl.on('line', async function (line) {

    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);


    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        // 获取直播间信息
        liveRoomInfo = lastLive = await ss.live.getLastLiveTask({ sellerUnitId: testCase.seller.unitId }).then(res => res.result.data);
        // console.log(liveRoomInfo);
        await testCase.clientLogin();
    }
    testCase.logAnalysis.start();

    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientList.slice(0, testNum).map((values, index) => {
            LOGINDATA.clusterCode = clientList[index].clusterCode;
            LOGINDATA.tenantId = clientList[index].tenantId;
            LOGINDATA.sessionId = clientList[index].sessionId;
            return ss.live.likeLiveRoom({ roomId: liveRoomInfo.liveRoomId, likeNum: common.getRandomNum(10, 100), timUserName: `昵称${common.getRandomStr(5)}` })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    // console.log(`${res.opTime},${res.duration}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);
});