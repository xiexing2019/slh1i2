const fs = require('fs');
const path = require('path');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const liveHelp = require('../../../help/liveHelp');
const common = require('../../../../lib/common');

const testCase = new TestCase();
const jsonPath = path.join(__dirname, `../../data/loginData/${[caps.name]}.json`);
const clientList = Object.values(JSON.parse(fs.readFileSync(jsonPath)));

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// 获取云通信登录账号与签名

rl.setPrompt('请输入并发数量,循环次数(默认1): ');
rl.prompt();
rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
    }

    testCase.logAnalysis.start();

    for (let i = 0; i < totalNum; i++) {
        await common.delay(1000);
        const promises = clientList.slice(0, testNum).map((values, index) => {
            const clientNo = (i * testNum + index) % (clientList.length - 1);
            LOGINDATA.clusterCode = clientList[clientNo].clusterCode;
            LOGINDATA.tenantId = clientList[clientNo].tenantId;
            LOGINDATA.sessionId = clientList[clientNo].sessionId;
            return ss.live.getTimUserSig({ userType: 0 })
                .then(res => {
                    testCase.logAnalysis.add(res);
                    // console.log(`${res.opTime},${res.duration},listLen:${res.result.data.liveSpus.length},clientNo:${clientNo}`);
                })
                .catch(err => {
                    console.log(err);
                    testCase.logAnalysis.addFail(err);
                });
        });

        Promise.all(promises)
            .then(res => {
                console.log('执行完成');
                console.log(testCase.logAnalysis.getInfo());
                rl.prompt();
            })
            .catch(err => {
                console.log(err);
            });
    }
});
rl.on('close', () => {
    console.log('执行完成');
    process.exit(0);

});