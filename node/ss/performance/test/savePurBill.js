const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../help/testCase');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const testNum = Number(line);
    console.log(`testNum:`, testNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        await testCase.clientLogin();
        // const defAddressId = { ss_chk: 3879, ss_test: 2306, ss_online: 7244 };
        // LOGINDATA.defAddressId = defAddressId[caps.name];
        const dresList = await testCase.getDresSpuList().then(res => res.result.data.rows);
        // console.log(`dresList=${JSON.stringify(dresList)}`);
        await testCase.getPurJson(dresList[0].id);
    }

    //purJson
    const purJson = testCase.purJson;
    purJson.productVersion = '2.9.0';
    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);
    const billPath = path.join(__dirname, `../data/billData/${caps.name}.json`);
    const bills = JSON.parse(fs.readFileSync(billPath));
    testCase.logAnalysis.start();

    const promises = clientData.slice(0, testNum).map((ele, index) => {
        // console.log(common.getCurrentPreciseTime());
        LOGINDATA.clusterCode = clientData[index].clusterCode;
        LOGINDATA.tenantId = clientData[index].tenantId;
        LOGINDATA.sessionId = clientData[index].sessionId;
        purJson.orders[0].main.addressId = clientData[index].addId;
        purJson.orders[0].main.hashKey = `${Date.now()}${common.getRandomNum(100, 999)}`;
        return sp.spTrade.savePurBill(purJson)
            .then((res) => {
                // console.log(JSON.stringify(res));
                // 统计
                testCase.logAnalysis.add(res);
                bills[res.result.data.rows[0].billId] = {
                    billId: res.result.data.rows[0].billId,
                    payMoney: res.params.jsonParam.orders[0].main.money,
                    tenantId: clientData[index].tenantId,
                    clusterCode: clientData[index].clusterCode,
                    unitId: clientData[index].unitId,
                    saasId: clientData[index].saasId,
                    sessionId: clientData[index].sessionId
                };
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
    });

    return Promise.all(promises)
        .then(res => {
            console.log(`执行完成`);
            console.log(testCase.logAnalysis.getInfo());
            fs.writeFile(billPath, JSON.stringify(bills), function (err) {
                if (err) console.log(err);
            });
        })
        .catch(err => console.log(err));

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});