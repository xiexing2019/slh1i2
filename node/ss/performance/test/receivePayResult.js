const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../help/testCase');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const testNum = Number(line);
    console.log(`testNum:`, testNum);

    if (typeof testNum != 'number') {
        return;
    }

    const billJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../data/payData/${[caps.name]}.json`)));
    // delete clientJson.lastMobile;
    const clientData = Object.values(billJson);
    testCase.logAnalysis.start();

    const promises = clientData.slice(0, testNum).map((ele, index) => {
        // console.log(common.getCurrentPreciseTime());
        LOGINDATA.clusterCode = clientData[index].clusterCode;
        LOGINDATA.tenantId = clientData[index].tenantId;
        LOGINDATA.sessionId = clientData[index].sessionId;
        LOGINDATA.unitId = clientData[index].unitId;
        LOGINDATA.saasId = clientData[index].saasId;
        return sp.spTrade.receivePayResult({ mainId: clientData[index].payDetailId, amount: clientData[index].payMoney })
            .then((res) => {
                // console.log(JSON.stringify(res));
                // 统计
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            });
    });

    return Promise.all(promises)
        .then(res => {
            console.log(`执行完成`);
            console.log(testCase.logAnalysis.getInfo());
        })
        .catch(err => console.log(err));

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});