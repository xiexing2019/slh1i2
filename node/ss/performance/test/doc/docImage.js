const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../../help/testCase');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');

const docIds = String(fs.readFileSync(path.join(__dirname, `../../data/docId.txt`))).split('\n');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量,循环次数(默认1): `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }

    const [testNum, totalNum = 1] = line.trim().split(',').map(ele => Number(ele));
    console.log(`testNum:`, testNum);
    console.log(`totalNum:`, totalNum);

    if (typeof testNum != 'number') {
        return;
    }

    testCase.logAnalysis.start();

    const docIdsLen = docIds.length;
    for (let count = 0; count < totalNum; count++) {
        await common.delay(1000);
        const promises = new Array(testNum).fill({}).map((ele, index) => {
            const no = (count * testNum + index) % docIdsLen;
            // console.log(`no=${JSON.stringify(no)}`);
            return sp.doc.getDocStream({
                id: docIds[no],
                thumbFlag: 0,
                // thumbFlag: 1,
                // thumbType: 'h240'
            }).then((res) => {
                // console.log(`\nres=${JSON.stringify(res)}`);
                // 统计
                testCase.logAnalysis.add2(res);
            }).catch(err => {
                console.log(`${err}`);
                testCase.logAnalysis.addFail(err);
            });
        });

        Promise.all(promises)
            .then(res => {
                console.log(`执行完成`);
                console.log(testCase.logAnalysis.getInfo());
            })
            .catch(err => console.log(err));

    }

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});