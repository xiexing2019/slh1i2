const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const readline = require('readline');
const TestCase = require('../help/testCase');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');
const billManage = require('../../help/billManage');

const testCase = new TestCase();

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.setPrompt(`请输入并发数量: `);
rl.prompt();

rl.on('line', async function (line) {
    if (line == 'close') {
        rl.close();
    }
    const testNum = Number(line);
    console.log(`testNum:`, testNum);

    if (typeof testNum != 'number') {
        return;
    }

    if (!testCase.seller.tenantId) {
        await testCase.sellerLogin();
        // await testCase.clientLogin();
    }

    // const purJson = billManage.mockPurParam(testCase.dresList.slice(0, spuNum), { count: skuNum });
    // console.log(`订单明细数量${purJson.orders[0].details.length}`);
    const clientJson = JSON.parse(fs.readFileSync(path.join(__dirname, `../data/loginData/${[caps.name]}.json`)));
    delete clientJson.lastMobile;
    const clientData = Object.values(clientJson);
    testCase.logAnalysis.start();
    const promise = clientData.slice(0, testNum).map((ele, index) => {
        // console.log(common.getCurrentPreciseTime());
        LOGINDATA.clusterCode = clientData[index].clusterCode;
        LOGINDATA.tenantId = clientData[index].tenantId;
        LOGINDATA.sessionId = clientData[index].sessionId;
        LOGINDATA.openId = `${clientData[index].code}openId`;
        return [ss.ugr.getSubscribeMpStatus({ openId: `${clientData[index].code}openId`, sellerTenantId: clientData[index].tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.ugr.shareToUp({ openId: `${clientData[index].code}openId`, shopId: clientData[index].tenantId, srcType: 3, srcId: clientData[index].tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.ugr.switchShop({ sellerTenantId: clientData[index].tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        sp.spugr.updateUser({ _cid: clientData[index].clusterCode, _tid: clientData[index].tenantId, nickName: "垃圾" })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        sp.spugr.updateOnlineFlag({ _cid: clientData[index].clusterCode, _tid: clientData[index].tenantId, onlineFlag: 2 })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spugr.getShopForBuyerInSeller({ id: testCase.seller.tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.config.findProductParams({ ownerId: clientData[index].tenantId, sellerId: testCase.seller.tenantId, codes: ',show_inventory_in_ssmall,allow_shelves_when_inventory_less_than_zero,open_after_sale,goods_pick_up_by_self' })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spdresup.getPageFragment({ buyerId: clientData[index].tenantId, unitId: testCase.seller.unitId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spdresb.findDressClassByBuyer({ sellerUnitId: testCase.seller.unitId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.config.getSpbDictList({ typeId: 613, flag: 1, sellerUnitId: testCase.seller.unitId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.config.getSpbDictList({ typeId: 606, flag: 1, sellerUnitId: testCase.seller.unitId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spcart.getShoppingCartList({ shopId: testCase.seller.tenantId, pageSize: 0 })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spmdm.saveMemberInfo({ tenantId: testCase.seller.tenantId, nickName: "垃圾" })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        sp.spdresb.getShareInShopList({ pageSize: 1000 })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        sp.spugr.getShop({ id: testCase.seller.tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            }),
        ss.spchb.getDresSpuList({ tenantId: testCase.seller.tenantId })
            .then((res) => {
                // console.log(JSON.stringify(res));
                testCase.logAnalysis.add(res);
            }).catch(err => {
                console.log(err);
                testCase.logAnalysis.addFail(err);
            })
        ];
    });
    let promises = [];
    for (const pro of promise) {
        promises = promises.concat(pro);
    }
    return Promise.all(promises)
        .then(res => {
            console.log(`执行完成`);
            console.log(testCase.logAnalysis.getInfo());
        })
        .catch(err => console.log(err));

    // rl.prompt();
});

rl.on('close', () => {
    console.log(`测试完成`);
    process.exit(0);
});