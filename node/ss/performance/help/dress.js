const basicJson = require('../../help/basicJson');
const sp = require('../../../reqHandler/sp');
const fs = require('fs');
const path = require('path');
const envName = require('../../../data/caps').name;

//
// let json = await basicJson.addAddrJson(areaJson);
// json.recInfo.isDefault = 1;
// json.address.provinceCode = 110000;   //这里省市区写死，为了保证此地有运费规则可以覆盖到
// json.address.cityCode = 110100;
// json.address.countyCode = 110101;
// await sp.spmdm.saveUserRecInfo(json);
const userPath = path.join(__dirname, `../data/loginData/${envName}.json`);
const curClients = JSON.parse(fs.readFileSync(userPath).toString());
const lastMobile = Number(curClients.lastMobile) || 12700000000;
delete curClients.lastMobile;

(async function (params) {

    const list = Object.values(curClients);
    console.log(`list=${JSON.stringify(list)}`);

    for (let index = 0; index < list.length; index++) {
        console.log(index);

        const ele = list[index];
        LOGINDATA = ele;
        const areaJson = await basicJson.getAreaJson();
        const json = await basicJson.addAddrJson(areaJson);
        json.recInfo.isDefault = 1;
        const val = await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);
        curClients[ele.code].addId = val;

    }

    curClients.lastMobile = lastMobile;
    fs.writeFile(userPath, JSON.stringify(curClients), function (err) {
        if (err) console.log(err);
    });
})();