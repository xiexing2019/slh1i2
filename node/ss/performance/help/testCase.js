const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');
const readline = require('readline');
const fs = require('fs');
const path = require('path');
const LogAnalysis = require('./logAnalysis');
const ssReq = require('../../help/ssReq');
const basicJson = require('../../help/basicJson');
const billManage = require('../../help/billManage');
const dresManage = require('../../help/dresManage');


class TestCase {
    constructor() {
        this.logAnalysis = new LogAnalysis();
        this.seller = {};
        this.client = {};
        this.dresMap = new Map();
        this.cartJson = {};
        this.purJson = {};
        this.dresList = [];
        this.dresFull = {};
        this.waitPayBills = [];
    }

    async sellerLogin() {
        const user = ssAccount.sellerPerformance;
        await ssReq.ssSellerLogin({ code: user.mobile, shopName: user.shopName });
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        this.seller = _.cloneDeep(LOGINDATA);
    }

    async clientLogin() {
        // await ssReq.ssClientLogin({ tenantId: this.seller.tenantId, mobile: 12800000000 });
        await ss.ugr.wxUserLoginWithRegister({
            openId: `${12800000000}openId`,
            productCode: 'slhMallWxApplet',
            appId: ssAccount.seller6.appId,
            tenantId: this.seller.tenantId,
        });
        // const areaJson = await basicJson.getAreaJson();
        // const json = await basicJson.addAddrJson(areaJson);
        // json.recInfo.isDefault = 1;
        // const val = await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);
        // console.log(val);

        // await ss.spugr.buyerBindMobile({ mobile: 12800000000 });
        // await sp.spugr.changeMobile({ mobile: 12800000000, tentantId: this.seller.tenantId });

        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        this.client = _.cloneDeep(LOGINDATA);
    }

    /**
     * 切换当前角色
     * @param {string} user seller卖家 client买家
     */
    changeUser(user) {
        LOGINDATA = this[user];
        return this;
    }

    async getDresSpuList() {
        return ss.spchb.getDresSpuList({ tenantId: this.seller.tenantId });
    }

    async getFullForBuyer(spuId) {
        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: this.seller.tenantId, _cid: this.seller.clusterCode });
        this.dresFull = dresFull;
    }

    getCartJson(dresFull, count = 1) {
        // const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: this.seller.tenantId, _cid: this.seller.clusterCode });
        this.cartJson = basicJson.cartJson2(Object.assign(dresFull, { count: count }));
        return this;
    }

    async getPurJson(spuId, count = 1) {
        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: this.seller.tenantId, _cid: this.seller.clusterCode });
        this.purJson = basicJson.purJson({ styleInfo: dresFull });
        return this;
    }

    async getdresListforbill(listNum) {
        this.dresList = [];
        const len = Math.floor(listNum / 20);
        console.log(len);
        for (let index = 1; index <= len; index++) {
            const dresSpuList = await sp.spdresb.findSellerSpuList({ pageNo: index, pageSize: 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
            for (const dresSpu of dresSpuList) {
                const dresFull = await sp.spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
                const dres = dresManage.setupDres();
                dres.setBySeller(dresFull);
                await dres.searchAndSetByDetail();
                this.dresList.push(dres);
            }
            console.log(index);
            console.log(this.dresList.length);
        }
        console.log(this.dresList.length);
        if (listNum % 20 > 0) {
            // const dresSpuList = await ss.spchb.getDresSpuList({ pageNo: len, pageSize: (listNum - len), tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            const dresSpuList = await sp.spdresb.findSellerSpuList({ pageNo: len + 1, pageSize: listNum % 20, flag: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
            // console.log(`dresSpuList=${JSON.stringify(dresSpuList)}`);
            for (const dresSpu of dresSpuList) {
                const dresFull = await sp.spdresb.getFullById({ id: dresSpu.id }).then(res => res.result.data);
                const dres = dresManage.setupDres();
                dres.setBySeller(dresFull);
                await dres.searchAndSetByDetail();
                this.dresList.push(dres);
            }
            // console.log(this.dresList.length);
        }
        // console.log(this.dresList.length);

        return this;
    }

    addDresToDresMap(dresList) {
        dresList.forEach(dres => this.dresMap.set(dres.id, { spuId: dres.id, _cid: this.seller.clusterCode, _tid: this.seller.tenantId }))
        return this;
    }

    async changeBillPayFlag(billNum = 500) {
        const con = await ss.creatSqlPool({ dbName: 'spbMysql' });
        const selectRes = await con.query(`
            SELECT
                pur.id,
                pur.total_money,
                pur.unit_id
            FROM
                sspur001.sp_pur_bill AS pur
            INNER JOIN spb002.sp_sales_bill AS sales ON pur.bill_no = sales.bill_no
            WHERE
                pur.seller_unit_id = ${this.seller.unitId}
            AND pur.flag != 2
            ORDER BY
                pur.bill_no DESC
            LIMIT ${billNum}`).then(res => res[0]);
        // console.log(`\n selectRes=${JSON.stringify(selectRes)}`);
        if (selectRes.length < billNum) console.warn(`数据不足,只有${selectRes.length}条数据~~~`);

        const updateRes = await con.execute(`
            UPDATE sspur001.sp_pur_bill AS pur
            INNER JOIN spb002.sp_sales_bill AS sales ON pur.bill_no = sales.bill_no
            SET pur.pay_flag = 0,
                pur.flag = 3,
                pur.pay_time = NULL,
                sales.pay_flag = 0,
                sales.flag = 3,
                sales.pay_time = NULL
            WHERE
                pur.id in (${selectRes.map(ele => ele.id)})`).then(res => res[0]);
        console.log(`\n updateRes=${JSON.stringify(updateRes)}`);
        if (updateRes.affectedRows < billNum) console.warn(`更新的数据不足,只有${updateRes.affectedRows}条数据~~~`);

        await con.end();
        this.waitPayBills = selectRes;
    }
};

module.exports = TestCase;