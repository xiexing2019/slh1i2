const basicJson = require('../../help/basicJson');
const sp = require('../../../reqHandler/sp');
const fs = require('fs');
const path = require('path');
const envName = require('../../../data/caps').name;

const userPath = path.join(__dirname, `../data/loginData/${envName}.json`);
const curClients = JSON.parse(fs.readFileSync(userPath).toString());
const lastMobile = Number(curClients.lastMobile) || 12700000000;
delete curClients.lastMobile;

(async function (params) {

    const list = Object.values(curClients);
    // console.log(`list=${JSON.stringify(list)}`);

    for (let index = 0; index < list.length; index++) {
        console.log(index);
        const ele = list[index];
        LOGINDATA = ele;
        // console.log(`ele=${JSON.stringify(ele)}`);
        const res = await sp.spugr.fetchUserSessionLite({ sessionId: ele.sessionId }).then(res => res.result.data);
        // console.log(`res=${JSON.stringify(res)}`);
        curClients[ele.code].unitId = res.unitId;
        curClients[ele.code].saasId = res.saasId;
        curClients[ele.code].userId = res.userId;
    }

    curClients.lastMobile = lastMobile;
    fs.writeFile(userPath, JSON.stringify(curClients), function (err) {
        if (err) console.log(err);
    });
})();