const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');
const readline = require('readline');
const fs = require('fs');
const path = require('path');
const LogAnalysis = require('./logAnalysis');
const ssReq = require('../../help/ssReq');
const envName = require('../../../data/caps').name;

/** 卖家信息 */
const sellerInfo = ssAccount.sellerPerformance;

const userPath = path.join(__dirname, `../data/loginData/${envName}.json`);
const curClients = JSON.parse(fs.readFileSync(userPath).toString());
let lastMobile = Number(curClients.lastMobile) || 12700000000;
delete curClients.lastMobile;

const clientNum = 1000;
const clientList = Object.values(curClients);
console.log(`需要客户数量:${clientNum},当前客户数量:${clientList.length}`);

(async function () {
    if (clientList.length < clientNum) {
        clientList.push(...new Array(clientNum - clientList.length).fill(new Client()));
    }

    for (let index = 0; index < clientList.length; index++) {
        console.log(index);
        const client = clientList[index];
        if (curClients[client.code]) {
            LOGINDATA.clusterCode = client.clusterCode;
            LOGINDATA.tenantId = client.tenantId;
            LOGINDATA.sessionId = client.sessionId;
            await sp.spugr.changeMobile({ check: false, mobile: client.code, tentantId: sellerInfo.tenantId });
            continue;
        }

        try {
            const code = ++lastMobile;
            console.log(code);
            const data = await ss.ugr.wxUserLoginWithRegister({
                productCode: 'slhMallWxApplet',
                appId: sellerInfo.appId,
                openId: `${code}openId`,
                mobile: code,
                tenantId: sellerInfo.tenantId,
            }).then(res => res.result.data);
            curClients[code] = { code, sessionId: data.sessionId, tenantId: data.tenantId, clusterCode: data.clusterCode };
        } catch (error) {
            console.log(error);
        } finally {
            curClients.lastMobile = lastMobile;
        }

    };
    // console.log(curClients);
    fs.writeFile(userPath, JSON.stringify(curClients), function (err) {
        if (err) console.log(err);
    });

})();



function Client(params) {
    this.code = '';
};