const common = require('../../../lib/common');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssReq = require('../../help/ssReq');
const groupHelper = require('../../../reqHandler/ss/spb/spmdm');

describe('买家个人中心-账户信息同步', function () {
    this.timeout(30000);
    let sellerLoginData, userId;
    before(async () => {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerLoginData = _.cloneDeep(LOGINDATA);
        await ssReq.userLoginWithWx();
        userId = LOGINDATA.userId;
        console.log(LOGINDATA);
    });

    // 基本校验不报错即可
    it('获取验证码', async function () {
        await sp.spugr.sendCode({ mobile: LOGINDATA.mobile });
    });

    it('查询是当前openId是否关注公众号', async function () {
        const res = await ss.ugr.getSubscribeMpStatus({ openId: LOGINDATA.wxOpenId, sellerTenantId: sellerLoginData.tenantId });
        console.log(`res=${JSON.stringify(res)}`);
    });

    describe('修改手机号-offline', function () {
        //^1[3-9]\d{9}$
        // const mobile = '1' + common.getRandomNum(3, 9).toString() + common.getRandomNumStr(9);
        const mobile = common.getRandomMobile();
        before('设置手机号', async function () {
            await ssReq.userLoginWithWx();
            await sp.spugr.changeMobile({ mobile: mobile, tentantId: sellerLoginData.tenantId });
        });

        it('买家登录获取信息', async function () {
            await ssReq.userLoginWithWx();
            expect(LOGINDATA.mobile, '买家手机号与设置不一致').to.equal(mobile);
        });

        it('卖家获取此客户信息', async function () {
            await ssReq.ssSellerLogin();
            const getMemberByUserId = await groupHelper.getMemberByUserId({ userId: userId });
            expect(getMemberByUserId.result.data.phone, '卖家获取此客户信息,买家手机号与设置不一致').to.equal(mobile);
        });
    });

    describe('修改基本资料', function () {
        let user;
        before('修改资料', async function () {
            // 姓名、性别、生日:012未知，男,女
            await ssReq.userLoginWithWx();
            let gender = [0, 1, 2];
            gender.splice(LOGINDATA.gender, 1);
            user = {
                userName: common.getRandomChineseStr(3),
                // nickName: common.getRandomStr(5),
                // avatar: common.getRandomNumStr(5),
                birthday: common.getDateString([common.getRandomNum(-50, 0), 0, 0]),
                gender: common.randomSort(gender).shift()
            }
            await sp.spugr.updateUser(user);
        });

        it('买家登录获取信息', async function () {
            await ssReq.userLoginWithWx();
            common.isApproximatelyEqualAssert(LOGINDATA, user);
        });

        it('卖家获取此客户信息', async function () {
            await ssReq.ssSellerLogin();
            const getMemberByUserId = await groupHelper.getMemberByUserId({ userId: userId });
            // console.log(`getMemberByUserId=${JSON.stringify(getMemberByUserId.result.data)}`);
            user.name = user.userName;
            delete user.userName;
            // console.log(`user=${JSON.stringify(user)}`);
            common.isApproximatelyEqualAssert(getMemberByUserId.result.data, user);
        });
    });

});

