const ssReq = require("../../help/ssReq");
const sp = require("../../../reqHandler/sp");
const common = require("../../../lib/common");
const ss = require("../../../reqHandler/ss");
const ssAccount = require("../../data/ssAccount");
const spugr = require("../../../reqHandler/ss/spg/spugr");
const caps = require('../../../data/caps');
const ssConfigParam = require('../../help/configParamManager')

/**
 * ec-spugr-spShop-getShopFromBuyerV2 添加足迹新接口,
 * ec-ugr-wxApp-shareToUp 添加足迹老接口
 * 老接口小程序后续不再使用，仍然能使用
 * 
 * up_share_in_shop 店铺足迹表
 */
describe("店铺足迹", async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, shopName, cutName, whoCanViewMyShop;

    before("登录", async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        whoCanViewMyShop = await ssConfigParam.getSpbParamInfo({ code: 'who_can_view_my_shop', ownerId: sellerInfo.tenantId });
        await whoCanViewMyShop.updateParam({ val: 0 });

        await ssReq.userLoginWithWx({ shopId: sellerInfo.shopId });
        clientInfo = _.cloneDeep(LOGINDATA);

        shopName = sellerInfo.shopName;

        if (shopName.length < 1) {
            throw new Error('店铺名不存在');

        };
        cutName = shopName.substring(0, 1);
        console.log(cutName);
    });

    describe("查询店铺足迹列表", async function () {
        let findresult, res, addShare;
        //买家端直接调用增加店铺足迹
        describe('店铺足迹查询', async function () {
            before('添加店铺足迹', async function () {
                const res = await ss.spugr.getShopFromBuyer({ id: clientInfo.shopId }).then(res => res)
                addShare = shopShareExp(res);
                // await ss.ugr.shareToUp({ srcType: 3, srcId: sellerInfo.tenantId, shopId: sellerInfo.shopId, openId: clientInfo.extProps.wxOpenId })
            })
            it('足迹查询', async function () {
                defShare = await sp.spdresb.getShareInShopList().then(res => res.result.data.rows);
                defShare.forEach(re => {
                    //0是一种特殊类型的店铺，传任何类型都会返回0类型的店
                    //bizModelId与saasId一一对应
                    expect(re.bizModelId, '未找到足迹').to.be.oneOf([sellerInfo.saasId, 0]);
                });
            });
            it('查询足迹列表', async function () {
                const findresult = await sp.spdresb.getShareInShopList().then(res => res.result.data.rows);

                const a = findresult.find(obj => obj.shopId == clientInfo.shopId)
                common.isApproximatelyEqualAssert(addShare, a);
            });
            it('已访问的店铺查询', async function () {
                visit = await sp.spdresb.getShareInShopList({ shopNameLike: shopName }).then(res => res.result.data.rows.find(obj =>
                    obj.shopId == sellerInfo.shopId
                ));
                allVisit = await sp.spdresb.getShareInShopList().then(res => res.result.data.rows);
                if (visit.length == 0) {
                    throw new Error('不存在该店铺');
                };
                common.isArrayContainObjectAssert(allVisit, visit);

            });
            it("模糊搜索", async function () {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                await ssReq.userLoginWithWx({ shopId: sellerInfo.shopId });
                // findresult = await sp.spdresb.getShareInShopList({ bizModelId: randModel, shopNameLike: cutName }).then(res => res.params)
                findresult = await sp.spdresb.getShareInShopList({ shopNameLike: cutName }).then(res => res.result.data.rows);

                if (findresult.length == 0) {
                    throw new Error('搜索无结果');
                };
                findresult.forEach(re => {
                    // console.log(re);
                    expect(re.shopName).to.include(cutName);
                });
            });
            it('切换店铺类型足迹查询', async function () {
                //卖家登录（saasid=2）
                if (caps.name != 'ss_test') this.skip();

                let sellerInfo2
                await ssReq.ssSellerLogin({ code: ssAccount.seller10.mobile, shopName: ssAccount.seller10.shopName, appKey: ssAccount.seller10.appKey, appSecret: ssAccount.seller10.appSecret });
                sellerInfo2 = _.cloneDeep(LOGINDATA);
                await ssReq.userLoginWithWx({ appId: ssAccount.seller10.appId, shopId: sellerInfo2.shopId });
                // await ss.ugr.shareToUp({ srcType: 3, srcId: sellerInfo2.tenantId, shopId: sellerInfo2.shopId, openId: clientInfo.extProps.wxOpenId })
                await ss.spugr.getShopFromBuyer({ id: clientInfo.shopId }).then(res => res)
                let switchVisit = await sp.spdresb.getShareInShopList().then(res => res.result.data.rows);
                // console.log('test', switchVisit);
                switchVisit.forEach(obj => {
                    expect(obj.bizModelId, '未找到足迹').to.be.oneOf([sellerInfo2.saasId, 0]);
                });
            });
        });
    });

    // bug链接：http://zentao.hzdlsoft.com:6082/zentao/bug-view-11602.html
    describe("绑定saas的店铺优先显示线上店铺名-offline", async function () {
        before('访问saas店铺', async () => {
            await ssReq.userLoginWithWx({ tenantId: ssAccount.saas.tenantId });
            clientInfo = _.cloneDeep(LOGINDATA);
            await ss.ugr.shareToUp({ srcType: 3, srcId: ssAccount.saas.tenantId, shopId: ssAccount.saas.tenantId, openId: clientInfo.extProps.wxOpenId })
        });
        it("查询店铺足迹", async function () {
            const shopRes = await sp.spdresb.getShareInShopList().then(res => res.result.data.rows.find(data => data.tenantId == ssAccount.saas.tenantId));
            expect(shopRes, `店铺足迹里店铺名称显示不正确`).to.be.include({ shopName: ssAccount.saas.shopName });
        });
    });

    describe.skip('上次访问的店铺', async function () {
        let clientInfoEz, clientInfoEz2;
        before('买家访问一个店铺', async function () {
            await ssReq.ssSellerLogin();
            await ssReq.userLoginWithWx({ shopId: sellerInfo.shopId });
        });
        describe('saas1的店铺', async function () {
            before('买家登录', async function () {
                clientInfoEz = await ss.ugr.wxAppLogin({ productCode: 'slhMallTest', openId: ssAccount.client.openId, appId: ssAccount.seller6.appId });
            })
            it('上次访问的店铺', async function () {
                expect(clientInfoEz.result.data.shopId).to.be.not.equal(clientInfo.shopId);
            });
        });
        describe.skip('saas2的店铺', async function () {
            before('买家登录', async function () {
                if (caps.name != 'ss_test') this.skip();
                clientInfoEz2 = await ss.ugr.wxAppLogin({ productCode: 'slhMallTest', openId: ssAccount.client2.openId, appId: ssAccount.seller6.appId });
            })
            it('上次访问的店铺', async function () {
                expect(clientInfoEz.result.data.shopId).to.be.not.equal(clientInfo.shopId);
            });
        });

    });
});
function shopShareExp(res) {
    return {
        shopId: res.result.data.id,
        shopName: res.result.data.name,
        bizModelId: res.result.data.bizModelId
    }
};