const common = require('../../../lib/common');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');
const ssReq = require('../../help/ssReq');
const decode = require('../../../ss/help/decodeManager');

describe('微信', async function () {
    this.timeout(TESTCASE.timeout);
    const decodeManager = new decode();
    before(async function () {
        await ssReq.userLoginWithWx();
    });

    it('公众号查询电子小票列表', async function () {
        const res = await ss.sppur.getPurReceipt({ saasShopId: 0, timeType: 1 });
        // console.log(`res=${JSON.stringify(res)}`);
    });
    it.skip('解密微信小程序用户信息', async function () {
        const decodeRes = await ss.spugr.decodeUserInfo({ iv: decodeManager.iv, encryptedData: decodeManager.encryptedData }).then(res => res.result.data);
        // console.log(JSON.stringify(decodeRes));
        common.isApproximatelyEqualAssert(decodeManager.getDecodeExp(), decodeRes);
    });

});
