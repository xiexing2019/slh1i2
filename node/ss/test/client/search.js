const caps = require('../../../data/caps');
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const spExp = require('../../help/getExp');
const ssAccount = require('../../data/ssAccount');
const mysql = require('../../../reqHandler/sp/mysql');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const searchHelp = require('../../../sp/help/esSearchHelp');
const dresManage = require('../../help/dresManage');
const basicJson = require('../../help/basicJson');

describe('search', function () {
    this.timeout(30000);
    let sellerInfo, clientInfo;
    before(async function () {
        //卖家登录，
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        //买家登录
        await ssReq.userLoginWithWx();
        clientInfo = LOGINDATA;
    });

    describe('查询商品类别', async function () {
        // http://zentao.hzdlsoft.com:6082/zentao/testtask-view-833.html
        it('默认以validNum降序排序', async function () {
            const dresClassList = await ss.spdresb.findDressClassByBuyer({ sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId }).then(res => res.result.data.rows);
            searchHelp.orderAssert({ dataList: dresClassList, path: 'validNum', orderByDesc: true });
        });
    });

    // dres_file
    describe('商品小视频', async function () {
        const dres = dresManage.setupDres();
        before(async function () {
            await ssReq.ssSellerLogin();

            const classIdArr = [1013, 1085, 1093];
            classId = classIdArr[common.getRandomNum(0, 2)];
            const classInfo = await sp.spdresb.findByClass({ classId });// classTree.result.data.options.shift().code
            //获取字典类别列表
            let arr = ['606', '850', '2001', '2002', '2003', '2004'];
            classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
            for (let index = 0; index < arr.length; index++) {
                const element = arr[index];
                if (!BASICDATA[element]) BASICDATA[element] = await sp.spugr.getDictList({ typeId: element, flag: 1 })
                    .then((res) => res.result.data.rows);
            };

            //保存商品
            const json = basicJson.styleJson({ classId: classId });
            // console.log(`json=${JSON.stringify(json)}`);
            await dres.saveDres(json);

            await ssReq.userLoginWithWx();
        });
        describe('获取全部数据_myPraiseFlag=0', async function () {
            let dresFileList = [];
            before(async function () {
                dresFileList = await ss.spdresb.findDresFileList({ sellerTenantId: sellerInfo.tenantId, pageSize: 20, pageNo: 1, myPraiseFlag: 0 }).then(res => res.result.data.rows);
            });
            it('新增带有视频的商品,列表中校验', async function () {
                const dresFile = dresFileList.find(ele => ele.spuId == dres.id);
                // expect(dresFile).not.to.be.undefined;
                if (!dresFile) this.skip();
                common.isApproximatelyEqualAssert(dres.spu, dresFile, ['id']);
            });
            it('数据校验', async function () {
                dresFileList.forEach((dresFile) => expect(dresFile).to.include({ shopName: sellerInfo.shopName, type: 2 }));
            });
            it('myPraiseFlag', async function () {
                const dresFileList2 = await ss.spdresb.findDresFileList({ sellerTenantId: sellerInfo.tenantId, pageSize: 20, pageNo: 1, myPraiseFlag: 1 }).then(res => res.result.data.rows);
                const dresFile = dresFileList2.find(ele => ele.spuId == dres.id);
                expect(dresFile).to.be.undefined;
            });
        });
        describe('获取我点赞的视频列表_myPraiseFlag=1', async function () {
            let dresFileList = [];
            before(async function () {
                await sp.spdresb.saveBuyerPraise({ spuId: dres.id, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId });
                await common.delay(500);

                dresFileList = await ss.spdresb.findDresFileList({ sellerTenantId: sellerInfo.tenantId, pageSize: 20, pageNo: 1, myPraiseFlag: 1 }).then(res => res.result.data.rows);
            });
            it('点赞商品,列表中校验', async function () {
                const dresFile = dresFileList.find(ele => ele.spuId == dres.id);
                // console.log(`\n dresFile=${JSON.stringify(dresFile)}`);
                // expect(dresFile).not.to.be.undefined;
                if (!dresFile) this.skip();
                expect(dresFile.dresFileDto || dresFile).to.include({ myPraiseFlag: 1, praiseNum: 1 });
            });
            it('数据校验', async function () {
                // console.log(`\n dresFileList=${JSON.stringify(dresFileList)}`);
                dresFileList.forEach((dresFile) => expect(dresFile.dresFileDto || dresFile).to.include({ shopName: sellerInfo.shopName, type: 2, myPraiseFlag: 1 }));
            });
        });
    });

    describe.skip('买家我的数据统计-test', function () {
        let mineStat;
        before(async function () {
            await ssReq.userLoginWithWx();
            mineStat = await sp.spTrade.getPurStat().then(res => res.result.data);
        });

        it.skip('关注店铺数', async function () {
            const favorShopNum = await sp.spUp.getFavorShopList().then(res => res.result.data.count);
            expect(mineStat.favorShopNum, `关注店铺数与已关注列表结果不一致`).to.equal(favorShopNum);
        });

        it.skip('拿货店铺数', async function () {
            const buyedShopNum = await sp.spTrade.getRecentBuyShopList().then(res => res.result.data.count);
            expect(mineStat.buyedShopNum, `拿货店铺数与已购买过的店铺列表结果不一致`).to.equal(buyedShopNum);
        });

        it.skip('分享商品数', async function () {
            const shareDresNum = await sp.spdresb.getDresShareList().then(res => res.result.data.count);
            expect(mineStat.shareDresNum, `分享商品数与我的商品分享列表结果不一致`).to.equal(shareDresNum);
        });

        it.skip('收藏商品数', async function () {
            const favorSpuNum = await sp.spdresb.getMyFavorDres().then(res => res.result.data.favorDresStyleResultList.length);
            expect(mineStat.favorSpuNum, `收藏商品数与买家收藏列表结果不一致`).to.equal(favorSpuNum);
        });

        it.skip('足迹数', async function () {
            if (caps.name == 'sp_test') this.skip();
            const spbSql = await mysql({ dbName: 'spbMysql' });
            const trackNum = await spbSql.query(`select count(distinct spu_id) from up001.up_buyer_view where unit_id=${LOGINDATA.unitId} and flag=1`).then(res => res[0][0]['count(distinct spu_id)']);
            await spbSql.end();
            expect(mineStat.trackNum, `足迹数与up_buyer_view中的数据不一致`).to.equal(trackNum);
        });

        describe('商品收藏，看款数 +1', async function () {
            let myData, myDataAfterFavor, myDataAfterBuy;
            before('获取买家数据统计', async function () {
                await ssReq.userLoginWithWx();
                myData = await sp.spTrade.getPurStat().then(res => res.result.data);
            });


            it('购买商品后查看买家数据统计', async function () {
                await sp.spugr.shopManagerLogin({ mobile: ssAccount.seller1.mobile });
                let styleRes = await ssReq.saveDresFull();
                await sp.spugr.userLoginWithBindOpenId({ code: ssAccount.client.mobile });
                await sp.spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 1 });
                myDataAfterBuy = await sp.spTrade.getPurStat().then(res => res.result.data);

                let exp = _.cloneDeep(myDataAfterFavor);
                exp.viewSpuNum++;
                exp.trackNum++;
                common.isApproximatelyEqualAssert(exp, myDataAfterBuy);

                await sp.spugr.shopManagerLogin();
                await sp.spdresb.offMarket({ ids: [styleRes.result.data.spuId] });
            });

            it('查看大图后查看买家数据统计', async function () {
                await sp.spugr.shopManagerLogin({ mobile: ssAccount.seller1.mobile });
                let styleRes = await ssReq.saveDresFull();
                await sp.spugr.userLoginWithBindOpenId({ code: ssAccount.client.mobile });
                await sp.spdresb.saveBuyerView({ spuId: styleRes.result.data.spuId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 2 });
                res = await sp.spTrade.getPurStat().then(res => res.result.data);

                let exp = _.cloneDeep(myDataAfterBuy);
                exp.viewSpuNum++;
                exp.trackNum++;
                common.isApproximatelyEqualAssert(exp, res);

                await sp.spugr.shopManagerLogin({ mobile: ssAccount.seller1.mobile });
                await sp.spdresb.offMarket({ ids: [styleRes.result.data.spuId] });
            });

            it('平均发货时间验证', async function () {
                await sp.spugr.userLoginWithBindOpenId({ code: ssAccount.client.mobile });
                let res = await sp.spTrade.getPurStat().then(res => res.result.data);
                expect(res, '没有返回平均发货时间字段').to.include.keys('avgDeliverElapsed');
                expect(res.avgDeliverElapsed).to.be.above(0); //平均发货时间大于0
            });
        });
    });

    describe.skip('买家查看店铺信息-test', function () {
        let qfRes, shopInfo;
        before(async function () {
            shopInfo = await sp.spugr.getShop({ id: sellerInfo.tenantId }).then(res => res.result.data);
        });

        it('买家查看卖家店铺信息', async function () {
            qfRes = await sp.spugr.getShopSpb({ id: sellerInfo.tenantId });
        });

        it.skip('店铺日志流查询', async function () {
            await sp.spugr.shopManagerLogin({ mobile: ssAccount.seller1.mobile });
            const res = await sp.spUp.getShopLogList({ flowType: 5 });
            const exp = spExp.shopLogListExp({ createdDate: qfRes.opTime, flowType: 5, clientInfo: clientInfo });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
            expect(res.result.data.rows[0].flowContent).to.includes('访问了您的店铺');
        });
    });
});
