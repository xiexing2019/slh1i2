const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const caps = require('../../../data/caps');
const ssAccount = require('../../data/ssAccount');
const basicJson = require('../../help/basicJson');
const fs = require('fs');
const path = require('path');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];
const slhCustomerManager = require('../../../reqHandler/slh1/customerManager');
const authList = require('../../data/menuAuthList');
const roleManage = require('../../help/roleManage');

/**
 * 创建独立app门店的时候，传入saas绑定id，在开通门店之前就将saas绑定
 * 1、版本3.10.0之前的走老的开通后绑定saas
 */
describe('卖家注册独立app-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let bindData;
    const user = {
        code: common.getRandomMobile(),
        authcType: 2,
        captcha: 0000,
    };

    before(async function () {
        await ss.spugr.userLogin(user);
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    it('saas获取绑定二维码', async function () {
        bindData = await ss.ssugr.getBindCodeUrl({ saasType: 1, saasShopId: ssAccount.saas.saasShopIdA, saasShopName: ssAccount.saas.saasShopNameA, saasUnitId: ssAccount.saas.epid, saasSn: ssAccount.saas.saasSn }).then(res => res.result.data);
        expect(bindData.codeUrl).to.not.be.null;
        expect(bindData.bindCode).to.not.be.null;
    });

    it('获取登录人会话', async function () {
        const res = await sp.spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId });
        common.isApproximatelyEqualAssert(LOGINDATA, res.result.data, ['lastSessionTime', 'avatar']);
        expect(LOGINDATA.avatar).to.include(res.result.data.avatar);
    });

    it('获取店铺开通信息', async function () {
        const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
        expect(shopInfo).not.to.have.property('cred');
    });

    describe('套餐', async function () {
        before('获取帐套列表并提交订单', async function () {
            const setRes = await ss.spugr.findSetList({ flag: 1 });
            const setDetail = setRes.result.data.rows[0];
            expect(setDetail, `未查询到可用套餐,\n${setRes.reqUrl}`).not.to.be.undefined;

            const billRes = await ss.spugr.saveValueAddService({ code: setDetail.crmId, num: 10 });
            console.log(`res=${JSON.stringify(billRes)}`);

            const payBackRes = await ss.spugr.backCallValueAddService({ billId: billRes.result.data.id });
            console.log(`payBackRes=${JSON.stringify(payBackRes)}`);
        });
        it.skip('获取店铺开通信息', async function () {
            const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
            // expect(shopInfo).to.have.property('cred');
        });
        it('当前用户可用套餐列表', async function () {
            const availableSetList = await ss.spugr.findAvailableSetList();
            console.log(`availableSetList=${JSON.stringify(availableSetList)}`);
            // expect(availableSetList.result.data.total).
        });
    });

    // 新建的店铺是无效的，绑定银行卡维护完基本信息后才会变成有效
    describe('店铺', async function () {
        let shop, merchat, merchantInfo;
        before('保存商户信息', async function () {
            await ss.spugr.userLogin(user);
            let tenantJson = await basicJson.tenantJson();
            tenantJson.bindCode = bindData.bindCode;
            tenantJson.dlProductVersion = '3.10.0';
            // console.log(tenantJson);
            shop = await ss.spugr.appCreateTenant(tenantJson)
                .then(res => {
                    console.log(`\nshop=${JSON.stringify(res)}`);
                    return res.result.data;
                });

            // 切换用户门店
            const res = await ss.spugr.changeUserShop({ newUnitId: shop.unitId, mobile: shop.mobile });
            console.log(`\nres=${JSON.stringify(res)}`);
            merchat = await sp.spugr.createMerchat({
                mobile: shop.mobile,
                settleAcctNo: "6217730700880774",
                settleAcctName: "在市谋",
                contact: "在市谋",
                settleAcctType: "2",
                bankName: "中信银行",
                bankCode: "302100011000",
                cncbFlag: 1,
                idCard: docData.idCard.cardMsg,
                idCardFront: docData.idCard.frontImage.docId,//是	身份证正面照, docId
                idCardReverse: docData.idCard.backImage.docId,//是	身份证背面照, docId id_card_reverse
            });
            console.log(`\n保存商户信息=${JSON.stringify(merchat)}`);
            await ss.spugr.updateOpenFlow({ shopId: shop.id, openFlowFlag: 99 });
        });
        after('解绑saas门店', async () => {
            await ssReq.ssSellerLogin({ code: shop.mobile, shopName: shop.name });
            const unBindRes = await ss.ssugr.unBindShop();
            console.log(`unBindRes = `, unBindRes);
            expect(unBindRes.result, `解绑saas门店失败`).to.be.include({ "msg": "成功" });
        });
        it('获取店铺开通信息', async function () {
            const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
            expect(shopInfo).to.have.property('cred');
        });
        it('门店商户查询', async function () {
            merchantInfo = await sp.spugr.findMerchant().then(res => res.result.data);
            console.log(merchantInfo);
            common.isApproximatelyEqualAssert(merchat.params.jsonParam, merchantInfo);
        });
        it('当前用户可用套餐列表', async function () {
            await ss.spugr.findAvailableSetList();
        });
        it('saas获取门店绑定关联', async function () {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid, logid: 'A000' }); //saas不同环境 门店A的管理员登录账号统一设置为A000
            let res = await slhCustomerManager.getShopBind();
            expect(res.result.data.shopName, `saas绑定失败`).to.be.equal('门店A');
        });

        describe('银行卡换绑', function () {
            let merBankInfo;
            before('卖家登录', async function () {
                await ssReq.ssSellerLogin({ code: shop.mobile, shopName: shop.name });
                merchantInfo = await sp.spugr.findMerchant().then(res => res.result.data);
                let i = 1;
                while (merchantInfo.flag != 1) {
                    if (i > 30) throw new Error('90s银行卡还没审核通过...')
                    await common.delay(3000);
                    merchantInfo = await sp.spugr.findMerchant().then(res => res.result.data);
                    i += 1;
                }
            });
            it('换卡预览', async function () {
                merBankInfo = await ss.spugr.getPrepareBindingData({
                    merchantId: merchantInfo.mchId,
                    bankPhone: shop.mobile,
                    bankAccountNo: common.getRandomNumStr(16),
                    bankAccountName: "在市谋",
                    bankType: "1",
                    bankName: "中信银行",
                    bankCode: "302100011000",
                }).then(res => res.result.data);
                console.log(merBankInfo);
            });
            it('银行卡换绑提交', async function () {
                let res = await ss.spugr.executeBankCardBinding({ merchantId: merchantInfo.mchId, merBankId: merBankInfo.merBankId, verifyCode: '000000' });
                console.log(res);
            });
        });

        describe('店铺开通途中获取切换店铺列表', function () {
            let shop2;
            before(`新增第二个门店`, async () => {
                await ss.spugr.userLogin(user);
                const tenantJson = await basicJson.tenantJson();
                shop2 = await ss.spugr.appCreateTenant(tenantJson).then(res => res.result.data);
            });
            it('获取切换店铺列表', async function () {
                const otherShopListRes = await ss.spugr.findOtherShopList().then(res => res.result.data.rows);
                console.log(`获取切换店铺列表结果=`, JSON.stringify(otherShopListRes));
                expect(otherShopListRes.find(obj => obj.id == shop2.id), `获取可以切换的店铺列表没有过滤未开通完成的店铺`).to.be.undefined;
                common.isApproximatelyEqualAssert(shop, otherShopListRes.find(obj => obj.id == shop.id));
            });
            describe('修改店铺基本信息(开通中)', function () {
                let editJson;
                before('修改店铺基本信息(开通中)', async function () {
                    editJson = await basicJson.tenantJson();
                    editJson.id = shop2.id;
                    editJson.certPic = `${docData.idCard.frontImage.docId},${docData.idCard.backImage.docId}`
                    const updateRes = await ss.spugr.updateShopWhenOpen(editJson);
                    // console.log(`修改店铺基本信息res = `, updateRes);
                });
                it('查询店铺详情', async function () {
                    await ss.spugr.changeUserShop({ newUnitId: shop2.unitId, mobile: shop2.mobile });
                    const shopDetail = await ss.spugr.getShopDetail().then(res => res.result.data);
                    console.log(`\n店铺详情=${JSON.stringify(shopDetail)}`);
                    common.isApproximatelyEqualAssert(editJson, shopDetail, ['typeId']);
                });
            });
        });
    });

    // bug: http://zentao.hzdlsoft.com:6082/zentao/bug-view-12573.html
    describe('职位', async function () {
        const role = roleManage.setupRole();
        let menuAuthList;
        before('登录,验证套餐权限', async function () {
            await ss.spugr.userLogin(user);
            menuAuthList = await role.getlistByTree();

            console.log(`\n menuAuthList=${JSON.stringify(menuAuthList)}`);
            console.log(`\n authList=${JSON.stringify(authList)}`);
            expect(authList.length, `权限长度不一致,期望值${authList.length}`).to.equal(menuAuthList.length);
            authList.forEach(auth => {
                const menuAuth = menuAuthList.find(obj => obj.name == auth.name);
                common.isApproximatelyEqualAssert(auth, menuAuth, ['subItems']);
                expect(auth.subItems.length, `${auth.name}子权限长度不一致,期望值${auth.subItems.length}`).to.equal(menuAuth.subItems.length);
                // console.log(menuAuth);
                auth.subItems.forEach(item => {
                    common.isApproximatelyEqualAssert(item, menuAuth.subItems.find(obj => obj.name == item.name));
                });
            });
        });

        it('拼团/限时折扣默认开启', async function () {
            let saleManagement = menuAuthList.find(menuAuth => menuAuth.code == 'saleManagement');
            let roleList = saleManagement.role.split(',');
            let shopRoleList = await role.getRoleListAtShop();
            let roleIds = roleList.map(role => {
                shopRoleList.map(data => {
                    if (role == data.code) {
                        return data.id
                    }
                })
            });
            for (roleId of roleIds) {
                for (subItem of saleManagement.subItems) {
                    let res = await ss.spauth.getAuthListByRoleId({ roleId: roleId }).then(res => res.result.data.rows.find(ele => ele.funcId == subItem.id));
                    expect(res, `活动权限没有默认开启`).not.to.be.undefined;
                }
            }
        });


    });

    describe('店铺默认设置', function () {
        let status;
        before('卖家登录', async function () {
            await ss.spugr.userLogin(user);
            status = { goodsFabricStatus: 1, goodsBrandStatus: 1, goodsInvStatus: 0, goodsSeasonStatus: 1, goodsSalesVolumeStatus: 0, goodsWeightStatus: 1 }
        });
        it('自定义商品默认设置查询', async function () {
            const goodsDetail = await ss.spdresup.getGoodsDetail().then(res => res.result.data);
            console.log(goodsDetail);
            common.isApproximatelyEqualAssert(status, goodsDetail);
        });
    });
});