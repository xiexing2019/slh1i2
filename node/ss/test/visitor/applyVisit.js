const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const ssConfigParam = require('../../help/configParamManager');
const messageManage = require('../../help/messageManage');
const ssAccount = require('../../data/ssAccount');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');


describe('买家认证可见', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, whoCanViewMyShop;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        // 关闭则所有人可见，开启则需认证
        whoCanViewMyShop = await ssConfigParam.getSpbParamInfo({ code: 'who_can_view_my_shop', ownerId: LOGINDATA.tenantId });
        await whoCanViewMyShop.updateParam({ val: 1 });
    });
    after(async function () {
        await whoCanViewMyShop.updateParam({ val: 0 });
    });
    describe('买家申请访问', async function () {
        let mobile = common.getRandomMobile();
        for (let index = 1; index <= 3; index++) {
            describe(`买家第${index}次申请访问`, async function () {
                let msgId;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    let messageList = await spMdm.pullMessagesList({ unread: 1, tagOneIn: 504, pageNo: 1, pageSize: 20 });
                    msgId = messageList.result.data.count > 0 ? messageList.result.data.dataList[0].id : 0;

                    await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                });
                it(`买家第${index}次申请访问`, async function () {
                    const applyForVisit = await ss.spmdm.applyForVisit({ check: false, userId: LOGINDATA.userId, tenantId: sellerInfo.tenantId });
                    expect(applyForVisit.result.code, `买家第${index}次申请访问失败`).to.equal(0);
                });

                it('卖家系统消息', async function () {
                    await ssReq.ssSellerLogin();
                    await messageManage.messageSystem({ msgId: msgId, tagOneIn: 504, text: '正在向您申请店铺访问权限,是否向他/她发放', title: '申请访问' });
                });
            });
        }
        describe('买家第四次申请访问', async function () {
            before(async function () {
                await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            });
            it('买家第4次申请访问', async function () {
                const applyForVisit = await ss.spmdm.applyForVisit({ check: false, userId: LOGINDATA.userId, tenantId: sellerInfo.tenantId });
                expect(applyForVisit.result, `买家第4次申请访问居然成功了`).to.includes({ msgId: 'visit_apply_too_much' });
            });
        });
    });

    // http://zentao.hzdlsoft.com:6082/zentao/file-read-11432.png
    describe.skip('批量修改门店访客审核状态', async function () {
        describe('开启访客认证-校验批量修改', async function () {
            let oldDataList;
            before('开启访客认证', async function () {
                await ssReq.ssSellerLogin();
                oldDataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);

                await whoCanViewMyShop.updateParam({ val: 1 });
            });
            it('不更新状态', async function () {
                await ss.spmdm.updateAuditFlagInBatch({ type: 0 });
                const dataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(oldDataList, dataList);
            });
            it('更新状态', async function () {
                await ss.spmdm.updateAuditFlagInBatch({ type: 1 });
                const dataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);
                dataList.forEach(data => expect(data.mallMember.flag).to.equal(0));
            });
        });
        describe('关闭访客认证-校验批量修改', async function () {
            let oldDataList;
            before('关闭访客认证', async function () {
                // await ssReq.ssSellerLogin();
                oldDataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);

                await whoCanViewMyShop.updateParam({ val: 0 });
            });
            it('不更新状态', async function () {
                await ss.spmdm.updateAuditFlagInBatch({ type: 0 });
                const dataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(oldDataList, dataList);
            });
            it('更新状态', async function () {
                await ss.spmdm.updateAuditFlagInBatch({ type: 1 });
                const dataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);
                dataList.forEach(data => expect(data.mallMember.flag).to.equal(1));
            });
        });
    });

    describe('修改访客权限为禁止访问', async function () {
        let mobile = common.getRandomMobile(), clientInfo;
        before('关闭访客认证', async function () {
            await ssReq.ssSellerLogin();

            oldDataList = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows);
            //确保店铺访客认证为关闭
            await whoCanViewMyShop.updateParam({ val: 0 });
            //买家登录
            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
        });
        describe('卖家设置买家为不可访问', async function () {
            //卖家设置买家的的flag为不可访问
            let client;
            before('将买家状态设置为不可访问', async function () {
                await ssReq.ssSellerLogin();
                await ss.spmdm.auditMemberByUserId({ userId: clientInfo.userId, flag: 0 });
                // TODO 查询列表后 需要校验列表数据
                client = await ss.spmdm.getMemberList({ custUserId: clientInfo.userId }).then(res => res.result.data.rows.find(obj => obj.custUserId == clientInfo.userId));
                if (client == undefined) {
                    throw new Error('客户列表为空');
                };
            });
            after('将买家状态设置为可以访问', async function () {
                await ssReq.ssSellerLogin();
                await ss.spmdm.auditMemberByUserId({ userId: clientInfo.userId, flag: 1 });
            });
            //买家查询商品列表
            it('买家首页获取店铺详情', async function () {
                await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                const storeDetail = await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId }).then(res => res.result.data);
                console.log(storeDetail);

                expect(storeDetail.accessFlag, '店铺状态未更新').to.be.equal(0);
            });
            //卖家批量修改客户状态
            describe('卖家批量修改客户状态', async function () {
                before('批量修改客户状态', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ memberId: client.id, flag: 0 });

                    await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                });
                //买家首页获取店铺详情
                it('买家首页获取店铺详情', async function () {
                    const storeDetail = await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId }).then(res => res.result.data);
                    expect(storeDetail.accessFlag, '店铺状态未更新').to.be.equal(0);
                });
            });
            //卖家联合更新客户信息
            describe('卖家联合更新客户信息', async function () {
                before('联合更新客户信息', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.spmdm.updateMemberUnion({ memberId: client.id, flag: 0 });

                    await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                });
                //买家首页获取店铺详情
                it('买家首页获取店铺详情', async function () {
                    const storeDetail = await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId }).then(res => res.result.data);
                    expect(storeDetail.accessFlag, '店铺状态未更新').to.be.equal(0);
                });
            });
        });
    });
    describe('个性化参数-游客是否允许访问', async function () {
        let mobile = common.getRandomMobile(), visitorLogin;
        before('卖家设置游客必须登录', async function () {
            await ssReq.ssSellerLogin();
            visitorLogin = await ssConfigParam.getSpbParamInfo({ code: 'ss_shop_visitor_login_in_or_not', ownerId: LOGINDATA.tenantId });
        });
        after('游客无须登录', async function () {
            await ssReq.ssSellerLogin({});
            await visitorLogin.updateParam({ val: 0 });
        });
        describe('游客登录参数-开启', async function () {
            before('修改个性化参数，允许游客访问', async function () {
                await ssReq.ssSellerLogin();
                await visitorLogin.updateParam({ val: 0 });
            })
            it('卖家个性化参数', async function () {
                const paramInfo = await visitorLogin.getParamInfo();
                common.isApproximatelyEqualAssert(visitorLogin, paramInfo);
            });
            it('游客访问店铺', async function () {
                this.retries(2);
                await common.delay(1000);
                await ssReq.ssClientGuestLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
                expect(LOGINDATA.visitLogin, '强制游客登录参数未生效').to.be.equal(0);
            })
        })
        describe('游客登录参数-关闭', async function () {
            before('修改个性化参数，不允许游客访问', async function () {
                await ssReq.ssSellerLogin();
                await visitorLogin.updateParam({ val: 1 });
            });
            it('卖家个性化参数', async function () {
                const paramInfo = await visitorLogin.getParamInfo();
                common.isApproximatelyEqualAssert(visitorLogin, paramInfo);
            });
            it('游客访问店铺', async function () {
                this.retries(2);
                await common.delay(1000);
                await ssReq.ssClientGuestLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
                expect(LOGINDATA.visitLogin, '强制游客登录参数未生效').to.be.equal(1);
            });
        });
    });
});