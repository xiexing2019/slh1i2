const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const ssConfigParam = require('../../help/configParamManager');
const messageManage = require('../../help/messageManage');
const ssAccount = require('../../data/ssAccount');

describe.skip('买家注册登录', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
        /** 关闭则所有人可见，开启则需认证 */
        const whoCanViewMyShop = await ssConfigParam.getSpbParamInfo({ code: 'who_can_view_my_shop', ownerId: LOGINDATA.tenantId });
        await whoCanViewMyShop.updateParam({ val: 0 });
    });
    describe('买家访问', async function () {
        let mobile = common.getRandomMobile();
        before(async function () {
            await ss.ugr.wxAppLogin({
                tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId, openId: `${mobile}openId`,
                productCode: 'slhMallWxApplet',
            });
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log(`\n clientInfo=${JSON.stringify(clientInfo)}`);
        });
        it('游客标识', async function () {
            expect(clientInfo, `买家游客标识不为1`).to.includes({ guestFlag: 1 });
        });
        it(`买家绑定手机号,游客标识`, async function () {
            const res = await ss.spugr.buyerBindMobile({ mobile: mobile }).then(res => res.result.data);
            console.log(`\n res=${JSON.stringify(res)}`);
            expect(res.extProps, `买家游客标识不为0`).to.includes({ guestFlag: 0 });
        });
    });
});