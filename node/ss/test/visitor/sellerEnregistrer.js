const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const caps = require('../../../data/caps');
const ssAccount = require('../../data/ssAccount');
const basicJson = require('../../help/basicJson');
const fs = require('fs');
const path = require('path');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];
const mysql = require('../../../reqHandler/sp/mysql');
const myShop = require('../../help/shop/myShop');
const ssCaps = require('../../../reqHandler/ss/ssCaps');

describe('卖家注册独立app-offline', async function () {
    this.timeout(TESTCASE.timeout);
    const user = {
        code: common.getRandomMobile(),
        // code: 13857137831,
        authcType: 2,
        captcha: 0000,
    };
    let crmId; // 增家crmId后续使用crmId去数据库查询购买套餐可用门店数量 提测单 1514

    before(async function () {
        await ss.spugr.userLogin(user);
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    it('查询用户信息', async function () {

    });

    it('获取登录人会话', async function () {
        const res = await sp.spugr.fetchUserSessionLite({ sessionId: LOGINDATA.sessionId });
        common.isApproximatelyEqualAssert(LOGINDATA, res.result.data, ['lastSessionTime', 'avatar']);
        expect(LOGINDATA.avatar).to.include(res.result.data.avatar);
    });

    it('获取店铺开通信息', async function () {
        const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
        expect(shopInfo).not.to.have.property('cred');
    });

    describe('套餐', async function () {
        before('获取帐套列表并提交订单', async function () {
            const setRes = await ss.spugr.findSetList({ flag: 1 });
            const setDetail = setRes.result.data.rows[0];
            expect(setDetail, `未查询到可用套餐,\n${setRes.reqUrl}`).not.to.be.undefined;

            const billRes = await ss.spugr.saveValueAddService({ code: setDetail.crmId, num: 10 });
            console.log(`res=${JSON.stringify(billRes)}`);

            const payBackRes = await ss.spugr.backCallValueAddService({ billId: billRes.result.data.id });
            console.log(`payBackRes=${JSON.stringify(payBackRes)}`);
        });
        it('获取店铺开通信息', async function () {
            const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
            // expect(shopInfo).to.have.property('cred');
        });
        it('当前用户可用套餐列表', async function () {
            const availableSetList = await ss.spugr.findAvailableSetList();
            crmId = availableSetList.result.data.rows[0].crmId;
            console.log(`availableSetList=${JSON.stringify(availableSetList)}`);
            expect(availableSetList.result.data.total, `购买套餐成功后，获取用户可用套餐列表为空`).to.be.equal(1);
        });
        it('查询套餐可用门店数', async function () {
            const spgSql = await mysql({ dbName: 'spgMysql' });
            const extJson = await spgSql.query(`select ext_json from spg.sp_company where name = ${user.code}`).then(res => res[0][0].ext_json);
            await spgSql.end();
            common.isApproximatelyEqualAssert(extJson[crmId], { "opened": 0, "canOpen": 10 });
        });
    });

    describe('用户申请开店', async function () {
        let name = `姓名${common.getRandomStr(5)}`, startTime, endTime;
        before('用户申请开店', async function () {
            startTime = common.getCurrentDate();
            console.log(common.getCurrentTime());
            await ss.spugr.saveUpShopLog({ name: name, phone: user.code });
            endTime = common.getCurrentDate();
        });
        it('查询开店记录:按手机号', async function () {
            await ss.spAuth.staffLogin();
            const shopLogs = await ss.spugr.getUpShopLogs({ phone: user.code });
            console.log(`\n开店记录:按手机号=${JSON.stringify(shopLogs)}`);
            common.isApproximatelyEqualAssert({ phone: user.code }, shopLogs.result.data.rows[0]);
        });
        it('查询开店记录:按日期', async function () {
            const shopLogs = await ss.spugr.getUpShopLogs({ startDate: startTime, endDate: endTime });
            console.log(`\n开店记录:按日期=${JSON.stringify(shopLogs)}`);
            common.isApproximatelyEqualAssert({ phone: user.code }, shopLogs.result.data.rows[0]);
        });
        //TODO: 1、目前第二次申请开店的记录不会新增一条，也不会覆盖第一条（跨天的也不会）；2、name字段无效；需确认
        describe('用户再次申请开店', async function () {
            before('用户申请开店', async function () {
                await ss.spugr.userLogin(user);
                startTime = common.getCurrentDate();
                console.log(common.getCurrentTime());
                await ss.spugr.saveUpShopLog({ name: name, phone: user.code });
                endTime = common.getCurrentDate();
            });
            it('查询开店记录:按手机号', async function () {
                await ss.spAuth.staffLogin();
                const shopLogs = await ss.spugr.getUpShopLogs({ phone: user.code });
                console.log(`\n开店记录:按手机号=${JSON.stringify(shopLogs)}`);
                common.isApproximatelyEqualAssert({ phone: user.code }, shopLogs.result.data.rows[0]);
            });
            it('查询开店记录:按日期', async function () {
                const shopLogs = await ss.spugr.getUpShopLogs({ startDate: startTime, endDate: endTime });
                console.log(`\n开店记录:按日期=${JSON.stringify(shopLogs)}`);
                common.isApproximatelyEqualAssert({ phone: user.code }, shopLogs.result.data.rows[0]);
            });
        });
    });

    describe('店铺名称限制20位(3.11.0版本以上)', function () {
        let shop;
        before('保存店铺信息', async function () {
            await ss.spugr.userLogin(user);
            let tenantJson = await basicJson.tenantJson();
            tenantJson.dlProductVersion = ssCaps.productVersion;
            tenantJson.name = `店铺${common.getRandomStr(20)}`;
            tenantJson.check = false;
            shop = await ss.spugr.appCreateTenant(tenantJson).then(res => res.result);
        });
        it('名称不能超过20个字', async function () {
            expect(shop, `店铺名称不能超过20位`).to.be.include({ msgId: 'shop_name_length_limit_exceed' });
        });
    });

    // 新建的店铺是无效的，绑定银行卡维护完基本信息后才会变成有效
    describe('店铺', async function () {
        let shop, merchat;
        before('保存商户信息', async function () {
            await ss.spugr.userLogin(user);
            const tenantJson = await basicJson.tenantJson();
            // console.log(tenantJson);
            user.shopName = tenantJson.name;
            shop = await ss.spugr.appCreateTenant(tenantJson).then(res => res.result.data);
            console.log(`\nshop=${JSON.stringify(shop)}`);

            // 切换用户门店
            const res = await ss.spugr.changeUserShop({ newUnitId: shop.unitId, mobile: shop.mobile });
            console.log(`\nres=${JSON.stringify(res)}`);

            merchat = await sp.spugr.createMerchat({
                mobile: shop.mobile,
                settleAcctNo: "6217730700880774",
                settleAcctName: "在市谋",
                contact: "在市谋",
                settleAcctType: "2",
                bankName: "中信银行",
                bankCode: "302100011000", // 102100099996 工商银行 卡号6212261202022206396
                cncbFlag: 1,
                idCard: docData.idCard.cardMsg,
                idCardFront: docData.idCard.frontImage.docId,//是	身份证正面照, docId
                idCardReverse: docData.idCard.backImage.docId,//是	身份证背面照, docId id_card_reverse
            });
            console.log(`\n保存商户信息=${JSON.stringify(merchat)}`);
            await ss.spugr.updateOpenFlow({ shopId: shop.id, openFlowFlag: 99 });
        });
        it('获取店铺开通信息', async function () {
            const shopInfo = await ss.spugr.fetchShops().then(res => res.result.data);
            expect(shopInfo).to.have.property('cred');
        });
        it('门店商户查询', async function () {
            const merchantInfo = await sp.spugr.findMerchant().then(res => res.result.data);
            // console.log(`merchant=${JSON.stringify(merchantInfo)}`);
            common.isApproximatelyEqualAssert(merchat.params.jsonParam, merchantInfo);
        });
        it('当前用户可用套餐列表', async function () {
            const availableSetList = await ss.spugr.findAvailableSetList();
            // console.log(`availableSetList=${JSON.stringify(availableSetList)}`);
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            expect(availableSetList.result.data.total, `购买套餐成功后，获取用户可用套餐列表为空`).to.be.equal(1);
        });
        it('查询套餐可用门店数', async function () {
            const spgSql = await mysql({ dbName: 'spgMysql' });
            const extJson = await spgSql.query(`select ext_json from spg.sp_company where name = ${user.code}`).then(res => res[0][0].ext_json);
            await spgSql.end();
            common.isApproximatelyEqualAssert(extJson[crmId], { "opened": 1, "canOpen": 10 });
        });
        // wxAppChannelCode与wxMpChannelCode改为非必填
        // 用于更新sp_shop表中wx_app_channel_code和wx_mp_channel_code 后续流程暂无校验
        it('店铺开通独立公众号', async function () {
            await ss.spugr.openIndependentWxMp({ shopId: LOGINDATA.tenantId, wxMpAppId: common.getRandomStr(6), wxAppChannelCode: common.getRandomNumStr(6), wxMpChannelCode: common.getRandomNumStr(6) });
        });
        describe('运费', async function () {
            const shopInfo = new myShop();
            let templateList;
            before('运费模板列表', async function () {
                await common.delay(10000);
                await shopInfo.switchSeller({ code: user.code, shopName: user.shopName });
                await shopInfo.getShipFee();
                console.log(shopInfo.shipFee);
                templateList = await shopInfo.shipFee.getTemplateList();
                common.isApproximatelyEqualAssert({ total: 2 }, templateList, []);
            });
            it('存在包邮模板', async function () {
                const template = templateList.rows.find(obj => obj.id == -1);
                expect(template, `列表不存在包邮模板\n${JSON.stringify(templateList)}`).not.to.be.undefined;
                shopInfo.shipFee.shipTemplateAssert(template);
            });
            it('存在到付模板', async function () {
                const template = templateList.rows.find(obj => obj.id == -3);
                expect(template, `列表不存在到付模板\n${JSON.stringify(templateList)}`).not.to.be.undefined;
                shopInfo.shipFee.shipTemplateAssert(template);
            });
        });
        describe('修改商户信息', async function () {
            before(async function () {
                await ss.spugr.updateMerchantAndBank({
                    check: false,
                    id: merchat.result.data.id,
                    mobile: shop.mobile,
                    // settleAcctNo: "6217730700880774",
                    // settleAcctName: "在市谋",
                    // contact: "在市谋",
                    settleAcctType: "2",
                    bankName: "中信银行",
                    // bankCode: "302100011000",
                    cncbFlag: 2,
                    // idCard: docData.idCard.cardMsg,
                    // idCardFront: docData.idCard.frontImage.docUrl,//是	身份证正面照, docId
                    // idCardReverse: docData.idCard.backImage.docUrl,//是	身份证背面照, docId id_card_reverse
                });
            });
            it('查询商户信息', async function () {
                const resss = await ss.spugr.getShopDetail();
                console.log(`\n店铺详情=${JSON.stringify(resss)}`);
            });
        });
        describe('店铺公众号配置保存', async function () {
            let mpConfigExp;
            before('保存店铺公众号配置', async function () {
                const getMpConfig = await ss.spugr.getMpConfig({ shopId: LOGINDATA.tenantId }).then(res => res.result.data);
                // 增加maTemplateList:小程序消息模版  群接龙订单支付成功后发送小程序模板消息
                mpConfigExp = { shopId: LOGINDATA.tenantId, wxAppAppId: common.getRandomStr(5), wxAppAppSecret: common.getRandomStr(5), wxMpAppId: common.getRandomStr(5), templateList: [{ sceneId: common.getRandomNum(0, 5), templateId: common.getRandomStr(5) }], maTemplateList: [{ sceneId: common.getRandomNum(0, 5), templateId: common.getRandomStr(5) }] };
                await ss.spugr.saveMpConfig(mpConfigExp);
            });
            it('获取店铺公众号配置', async function () {
                const mpConfig = await ss.spugr.getMpConfig({ shopId: LOGINDATA.tenantId }).then(res => res.result.data);
                console.log(`\nmpConfig=${JSON.stringify(mpConfig)}`);
                common.isApproximatelyEqualAssert(mpConfigExp, mpConfig, ['templateList', 'maTemplateList']);
                mpConfigExp.templateList.forEach(templateExp => {
                    const template = mpConfig.templateList.find(obj => obj.templateId == templateExp.templateId);
                    expect(template, 'template不存在').not.to.be.undefined;
                    common.isApproximatelyEqualAssert(templateExp, template);
                });
                mpConfigExp.maTemplateList.forEach(templateExp => {
                    const template = mpConfig.maTemplateList.find(obj => obj.templateId == templateExp.templateId);
                    expect(template, 'template不存在').not.to.be.undefined;
                    common.isApproximatelyEqualAssert(templateExp, template);
                });
            });
        });
        describe('修改店铺信息', async function () {
            let content = {};
            before('修改店铺信息', async function () {
                content = {
                    id: LOGINDATA.tenantId,
                    headPic: await ss.spugr.getRandomShopOutImg({ shopName: LOGINDATA.shopName }).then(res => res.result.data.val),
                    contentPic: await ss.spugr.getRandomShopInImg().then(res => res.result.data.val),
                    certPic: `${docData.idCard.frontImage.docId},${docData.idCard.backImage.docId}`
                }
                const res = await ss.spugr.updateShop(content);
                console.log(`\n修改店铺信息=${JSON.stringify(res)}`);
            });
            it('查询店铺详情', async function () {
                const shopDetail = await ss.spugr.getShopDetail().then(res => res.result.data);
                console.log(`\n店铺详情=${JSON.stringify(shopDetail)}`);
                common.isApproximatelyEqualAssert(content, shopDetail);
            });
        });
    });
});