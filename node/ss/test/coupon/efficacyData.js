const common = require('../../../lib/common');
const sscoupb = require('../../../reqHandler/ss/spb/sscoupb');
const format = require('../../../data/format');
const spConfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const ssReq = require('../../help/ssReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const couponHelp = require('../../help/couponHelp');
const dresManage = require('../../help/dresManage');
const spugr = require('../../../reqHandler/sp/global/spugr');
const sp = require('../../../reqHandler/sp');
const ss = require('../../../reqHandler/ss');
const basicJson = require('../../help/basicJson');
const billManage = require('../../help/billManage');


describe('效果数据', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, couponId, couponData, purJson, savePurBill;
    const dres = dresManage.setupDres();
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        //创建商品
        const classIdArr = [1013, 1085, 1093];
        classId = classIdArr[common.getRandomNum(0, 2)];
        const classInfo = await spdresb.findByClass({ classId });// classTree.result.data.options.shift().code
        //获取字典类别列表
        let arr = ['606', '850', '2001', '2002', '2003', '2004'];
        classInfo.result.data.spuProps.forEach(element => arr.push(element.dictTypeId));
        for (let index = 0; index < arr.length; index++) {
            const element = arr[index];
            if (!BASICDATA[element]) BASICDATA[element] = await spugr.getDictList({ typeId: element, flag: 1 })
                .then((res) => res.result.data.rows);
        };
        //保存商品
        const json = basicJson.styleJson({ classId: classId });
        // console.log(`json=${JSON.stringify(json)}`);
        await dres.saveDres(json);
        // console.log(dres);

        //创建优惠券
        const setCouponData = await sscoupb.setCouponData({ cardType: 0 });
        couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
        // 领取优惠券
        await ssReq.userLoginWithWx();
        await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: dres.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        const spuPrice = getFullForBuyer.result.data.spu.pubPrice;
        const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: spuPrice, applicableSpus: [{ spuId: dres.id, spuPrice: spuPrice }] });
        const findUseableCouponsByShop = await sscoupb.findUseableCouponsByShop({ jsonParam: billInfo.jsonParam });
        const couponInfo = findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId == couponId);

        const getUserAddress = await sp.spmdm.getUserDefaultRecInfo().then(res => res.result.data);
        LOGINDATA.defAddressId = getUserAddress.recInfo.id;
        const evalShipFee = await sp.spconfb.evalShipFee({ provinceCode: getUserAddress.address.provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus: [{ spuId: dres.id, orderNum: 1, orderMoney: spuPrice }] }] })

        purJson = await billManage.mockPurParam(getFullForBuyer.result.data, { count: 1, num: 1, couponInfo: couponInfo, shipFeeMoney: evalShipFee.result.data.fees[0].fee });
        purJson.orders[0].main.sellerId = sellerInfo.tenantId;

        couponData = Object.assign(setCouponData, couponInfo);
    });

    it('开单', async function () {
        // savePurBill = await spTrade.savePurBill(purJson);
        savePurBill = await billManage.createPurBill(purJson);
        // console.log(`\n savePurBill=${JSON.stringify(savePurBill)}`);
        const purFindBills = await spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 });
        // console.log(`\n purFindBills=${JSON.stringify(purFindBills)}`);
        expect(purFindBills.result.data.rows[0].bill.id, '待付款订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billId);
    });

    it('支付', async function () {
        const createPay = await spTrade.createPay({ orderIds: [savePurBill.result.data.rows[0].billId], payMoney: savePurBill.params.jsonParam.orders[0].main.totalMoney });
        await spTrade.receivePayResult({ mainId: createPay.result.data.payDetailId, amount: parseFloat(createPay.params.jsonParam.payMoney).toFixed(2), purBillId: savePurBill.result.data.rows[0].billId });
        const purFindBills = await spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 2 });
        expect(purFindBills.result.data.rows[0].bill.id, '待发货订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billId);
    });

    it('支付后查询优惠券效果数据', async function () {
        this.retries(2);
        await ssReq.ssSellerLogin();
        common.delay(1000);
        const getFullEffection = await sscoupb.getFullEffection({ couponId: couponId }).then(res => res.result.data);
        const effectionJson = couponHelp.effectionJson({ couponInfo: couponData, billInfo: purJson });
        common.isApproximatelyEqualAssert(getFullEffection, effectionJson);
    });

    it('查询优惠券购买商品列表', async function () {
        const getFullSpusSales = await sscoupb.getFullSpusSales({ couponId: couponId }).then(res => res.result.data.rows);
        expect(getFullSpusSales[0].id, '优惠券购买商品列表未找到购买商品').to.equal(purJson.orders[0].details[0].spuId);
    });
});