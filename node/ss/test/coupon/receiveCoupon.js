const common = require('../../../lib/common');
const sscoupb = require('../../../reqHandler/ss/spb/sscoupb');
const ssReq = require('../../help/ssReq');
const spchb = require('../../../reqHandler/ss/spb/spchb');
const ss = require('../../../reqHandler/ss');
const ssAccount = require('../../data/ssAccount');
const couponHelp = require('../../help/couponHelp');

describe('买家领取优惠券', function () {
    this.timeout(50000);
    let sellerInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // await ssReq.userLoginWithWx();
    });
    describe('领券中心-领取优惠券', function () {
        let setCouponData, couponId, couponsCountBefore;
        before(async function () {
            //创建优惠券
            setCouponData = await sscoupb.setCouponData({ cardType: 0 });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            await ssReq.userLoginWithWx();
            couponsCountBefore = await sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.coupons);
            // await ssReq.userLoginWithWx();
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
        });
        it('查询领券中心列表', async function () {
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        it('买家领取优惠券', async function () {
            await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
        });
        it('领取后查询可用优惠券数量', async function () {
            const myCouponCount = await sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
            expect(myCouponCount.result.data.coupons, '领取后我的卡券数量没有+1').to.equal(couponsCountBefore + 1);
        });
        it('领取后查询有效优惠券', async function () {
            const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.not.be.undefined;
        });
        it('查看我的优惠券详情', async function () {
            const CouponDetails = await sscoupb.getCouponsDetails({ couponId: couponId, tenantId: sellerInfo.tenantId });
            console.log(`CouponDetails=${JSON.stringify(CouponDetails)}`);
            common.isApproximatelyEqualAssert(setCouponData, CouponDetails.result.data, ['unitId']);
        });
        it('领取后查询领券中心该优惠券库存', async function () {
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            const findCoupon = findShopCoupons.result.data.rows.find(obj => obj.id == couponId);
            expect(findCoupon.inventory, '领取后查询领券中心此优惠券库存不为1').to.equal(findCoupon.totalQuantity - 1);
        });
    });

    describe('商品可用优惠券列表-领取优惠券', function () {
        let setCouponData, couponId, couponsCountBefore, getListDresSpu;
        before(async function () {
            //创建优惠券
            setCouponData = await sscoupb.setCouponData({ cardType: 1 });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            await ssReq.userLoginWithWx();
            couponsCountBefore = await sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.coupons);
            getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
        });
        it('查询商品可用优惠券列表', async function () {
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            console.log(`\nfindValidCoupons=${JSON.stringify(findValidCoupons)}`);

            common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        it('买家领取优惠券', async function () {
            await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
        });
        it('领取后查询可用优惠券数量', async function () {
            const myCouponCount = await sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
            expect(myCouponCount.result.data.coupons, '领取后我的卡券数量没有+1').to.equal(couponsCountBefore + 1);
        });
        it('领取后查询有效优惠券', async function () {
            const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 1, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.not.be.undefined;
        });
        it('查看我的优惠券详情', async function () {
            const CouponDetails = await sscoupb.getCouponsDetails({ couponId: couponId, tenantId: sellerInfo.tenantId });
            console.log(`CouponDetails=${JSON.stringify(CouponDetails)}`);
            common.isApproximatelyEqualAssert(setCouponData, CouponDetails.result.data, ['unitId']);
        });
        it('领取后查询商品可用优惠券列表该优惠券库存', async function () {
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            const findCoupon = findValidCoupons.result.data.rows.find(obj => obj.id == couponId);
            expect(findCoupon.inventory, '领取后查询领券中心此优惠券库存-1').to.equal(findCoupon.totalQuantity - 1);
        });
    });

    describe('不公开优惠券', async function () {
        let setCouponData, couponId;
        before('创建不公开优惠券', async function () {
            await ssReq.ssSellerLogin();
            setCouponData = await sscoupb.setCouponData({ showInCenter: false });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
        });
        it('卖家查看优惠券列表', async function () {
            const getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId });
            // console.log(`getCouponList=${JSON.stringify(getCouponList.result.data.rows)}`);
            common.isApproximatelyEqualAssert(setCouponData, getCouponList.result.data.rows.find(obj => obj.id == couponId));
        });
        it('卖家查看优惠券详情', async function () {
            const couponDetails = await sscoupb.couponDetails({ couponId: couponId });
            const coupon = couponHelp.setBitTranslate(couponDetails.result.data.coupon);
            common.isApproximatelyEqualAssert(setCouponData, coupon);
        });
        it('买家查询领券中心列表', async function () {
            await ssReq.userLoginWithWx();
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId), `应不存在该优惠券${couponId}`).to.be.undefined;
        });
        it('买家查询商品可用优惠券列表', async function () {
            const getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            console.log(`\nfindValidCoupons=${JSON.stringify(findValidCoupons)}`);
            expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId), `应不存在该优惠券${couponId}`).to.be.undefined;
        });
    });

    describe('指定领取人类型:非默认价', async function () {
        let priceCodeValue, couponId, setCouponData, getListDresSpu;
        before(async function () {
            await ssReq.ssSellerLogin();
            //创建优惠券
            // receiverLimitType: 0, validPrice: ''
            const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
            console.log(`\n priceList=${JSON.stringify(priceList)}`);
            priceList.forEach(price => {
                if (price.codeValue != 0) {
                    priceCodeValue = price.codeValue;
                    return;
                }
            });
            if (priceCodeValue == undefined) {
                throw new Error(`没有除默认价以外的其他价格类型:\n${JSON.stringify(priceList)}`);
            };
            setCouponData = await sscoupb.setCouponData({ cardType: 0, receiverLimitType: 1, validPrice: priceCodeValue });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            console.log(couponId);
            await ssReq.userLoginWithWx();
            getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
        });
        it('查询领券中心列表', async function () {
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            // common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查询商品可用优惠券列表', async function () {
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            console.log(`findValidCoupons=${JSON.stringify(findValidCoupons)}`);
            expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            // common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
    });

    describe('指定领取人类型：默认价,批量', async function () {
        let priceCodeValue, couponId, setCouponData, getListDresSpu, clientInfo, mobile = `${common.getRandomMobile()}`;
        before(async function () {
            await ssReq.ssSellerLogin();
            //创建优惠券
            // receiverLimitType: 0, validPrice: ''
            const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
            console.log(`\n priceList=${JSON.stringify(priceList)}`);
            priceList.forEach(price => {
                if (price.codeValue == 0) {
                    priceCodeValue = price.codeValue;
                    return;
                }
            });
            if (priceCodeValue == undefined) {
                throw new Error(`没有默认价:\n${JSON.stringify(priceList)}`);
            };
            setCouponData = await sscoupb.setCouponData({ cardType: 0, receiverLimitType: 1, validPrice: priceCodeValue });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            console.log(couponId);

            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
            // await ssReq.userLoginWithWx();
            getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
        });
        it('查询领券中心列表', async function () {
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            // expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查询商品可用优惠券列表', async function () {
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            console.log(`findValidCoupons=${JSON.stringify(findValidCoupons)}`);
            // expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        describe('领取优惠券', async function () {
            before(async function () {
                // if (!couponId) this.skip();
                //领取优惠券
                await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
            });
            it('查询已领取优惠券列表', async function () {
                const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
                expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').not.to.be.undefined;
            });
            describe('修改客户适用价格', async function () {
                before(async function () {
                    //修改为非默认价
                    await ssReq.ssSellerLogin();
                    const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
                    console.log(`\n priceList=${JSON.stringify(priceList)}`);
                    for (const price of priceList) {
                        if (price.codeValue != 0) {
                            priceCodeValue = price.codeValue;
                            break;
                        }
                    }
                    const res = await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: priceCodeValue })
                    console.log(`\n res=${JSON.stringify(res)}`);
                    await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                });
                it('查询领券中心列表', async function () {
                    const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
                    expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
                    // common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
                });
                it('查询商品可用优惠券列表', async function () {
                    const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
                    // console.log(`findValidCoupons=${JSON.stringify(findValidCoupons)}`);
                    expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
                    // common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
                });
                it('查询已领取优惠券列表', async function () {
                    const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
                    expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券居然存在此优惠券').to.be.undefined;
                });
            });
        });
    });

    describe('指定领取人类型：默认价', async function () {
        let priceCodeValue, couponId, setCouponData, getListDresSpu, clientInfo, mobile = `${common.getRandomMobile()}`;
        before(async function () {
            await ssReq.ssSellerLogin();
            //创建优惠券
            // receiverLimitType: 0, validPrice: ''
            const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
            console.log(`\n priceList=${JSON.stringify(priceList)}`);
            priceList.forEach(price => {
                if (price.codeValue == 0) {
                    priceCodeValue = price.codeValue;
                    return;
                }
            });
            if (priceCodeValue == undefined) {
                throw new Error(`没有默认价:\n${JSON.stringify(priceList)}`);
            };
            setCouponData = await sscoupb.setCouponData({ cardType: 0, receiverLimitType: 1, validPrice: priceCodeValue });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
            // await ssReq.userLoginWithWx();
            getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
        });
        it('查询领券中心列表', async function () {
            const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
            // expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查询商品可用优惠券列表', async function () {
            const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
            console.log(`findValidCoupons=${JSON.stringify(findValidCoupons)}`);
            // expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
        });
        describe('领取优惠券', async function () {
            before(async function () {
                //领取优惠券
                await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
            });
            it('查询已领取优惠券列表', async function () {
                const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
                expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').not.to.be.undefined;
            });
            describe('修改客户适用价格', async function () {
                before(async function () {
                    //修改为非默认价
                    await ssReq.ssSellerLogin();
                    const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
                    console.log(`\n priceList=${JSON.stringify(priceList)}`);
                    for (const price of priceList) {
                        if (price.codeValue != 0) {
                            priceCodeValue = price.codeValue;
                            break;
                        }
                    }
                    const res = await ss.spmdm.updateMemberValidPrice({ custTenantId: clientInfo.tenantId, validPrice: priceCodeValue })
                    console.log(`\n res=${JSON.stringify(res)}`);
                    await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                });
                it('查询领券中心列表', async function () {
                    const findShopCoupons = await sscoupb.findShopCoupons({ tenantId: sellerInfo.tenantId });
                    expect(findShopCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
                    // common.isApproximatelyEqualAssert(setCouponData, findShopCoupons.result.data.rows.find(obj => obj.id == couponId));
                });
                it('查询商品可用优惠券列表', async function () {
                    const findValidCoupons = await sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getListDresSpu[0].id });
                    // console.log(`findValidCoupons=${JSON.stringify(findValidCoupons)}`);
                    expect(findValidCoupons.result.data.rows.find(obj => obj.id == couponId)).to.be.undefined;
                    // common.isApproximatelyEqualAssert(setCouponData, findValidCoupons.result.data.rows.find(obj => obj.id == couponId));
                });
                it('查询已领取优惠券列表', async function () {
                    const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
                    expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '领取后查询有效优惠券居然存在此优惠券').to.be.undefined;
                });
            });
        });
    });
});
