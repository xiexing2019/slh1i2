const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const billManage = require('../../help/billManage');
const cartManage = require('../../help/cartManage');

describe('买家使用优惠券', function () {
    // this.retries(2);
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        //买家取消订单
        BASICDATA['3005'] = await spugr.getDictList({ "typeId": 3005, "flag": 1 }).then((res) => res.result.data.rows);
    });

    describe('取消订单', function () {
        let couponId, couponsCountBefore, purJson, savePurBill;
        before(async function () {
            //创建优惠券
            couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
            // 领取优惠券
            await ssReq.userLoginWithWx();
            await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
            couponsCountBefore = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.coupons);

            const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
            const spuId = getListDresSpu.result.data.rows[0].id;
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            const spuPrice = getFullForBuyer.result.data.spu.pubPrice;
            const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: spuPrice, applicableSpus: [{ spuId: spuId, spuPrice: spuPrice }] });
            const findUseableCouponsByShop = await ss.sscoupb.findUseableCouponsByShop({ jsonParam: billInfo.jsonParam });
            const couponInfo = findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId == couponId);
            const getUserAddress = await sp.spmdm.getUserDefaultRecInfo().then(res => res.result.data);
            LOGINDATA.defAddressId = getUserAddress.recInfo.id;
            const evalShipFee = await sp.spconfb.evalShipFee({ provinceCode: getUserAddress.address.provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus: [{ spuId: spuId, orderNum: 1, orderMoney: spuPrice }] }] })

            purJson = await billManage.mockPurParam(getFullForBuyer.result.data, { count: 1, num: 1, couponInfo: couponInfo, shipFeeMoney: evalShipFee.result.data.fees[0].fee });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
        });
        it('开单前查询有效优惠券', async function () {
            const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '开单前查询有效优惠券不存在此优惠券').to.not.be.undefined;
        });
        describe('开单', async function () {
            before('开单', async function () {
                // savePurBill = await sp.spTrade.savePurBill(purJson);
                savePurBill = await billManage.createPurBill(purJson);

                console.log(`savePurBill=${JSON.stringify(savePurBill)}`);
                const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 });
                console.log(`purFindBills=${JSON.stringify(purFindBills)}`);
                expect(Number(purFindBills.result.data.rows[0].bill.billNo), '待付款订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo);
            });

            it('开单后查询可用优惠券数量', async function () {
                const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
                console.log(myCouponCount.result.data.coupons);
                expect(myCouponCount.result.data.coupons, '开单后我的卡券数量没有-1').to.equal(couponsCountBefore - 1);
            });
            it('开单后查询支付中优惠券', async function () {
                const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 2, orderByDesc: true, orderBy: "consumeDate" });
                expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '开单后查询支付中优惠券不存在此优惠券').to.not.be.undefined;
            });
            describe('取消订单', async function () {
                before('取消订单', async function () {
                    const random = common.getRandomNum(0, BASICDATA['3005'].length - 1);
                    await sp.spTrade.cancelPurBill({ id: savePurBill.result.data.rows[0].billId, buyerRem: BASICDATA['3005'][random].codeName, cancelKind: BASICDATA['3005'][random].codeValue });
                    // 未付款订单
                    const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 });
                    expect(purFindBills.result.data.rows.find(obj => obj.billNo == savePurBill.result.data.rows[0].billNo), '待付款订单找到了该订单').to.be.undefined;
                });
                it('取消订单后查询可用优惠券数量', async function () {
                    const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
                    console.log(myCouponCount.result.data.coupons);
                    expect(myCouponCount.result.data.coupons, '取消订单后我的卡券数量与下单前不一致').to.equal(couponsCountBefore);
                });

                it('取消订单后查询有效优惠券', async function () {
                    const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
                    expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '取消订单后查询有效优惠券不存在此优惠券').to.not.be.undefined;
                });
            });
        });
    });

    describe('立即购买', function () {
        let couponId, couponsCountBefore, purJson, savePurBill;
        before(async function () {
            //创建优惠券
            await ssReq.ssSellerLogin();
            couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
            // 领取优惠券
            await ssReq.userLoginWithWx();
            await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
            couponsCountBefore = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.coupons);

            const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
            const spuId = getListDresSpu.result.data.rows[0].id;
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            const spuPrice = getFullForBuyer.result.data.spu.pubPrice;
            const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: spuPrice, applicableSpus: [{ spuId: spuId, spuPrice: spuPrice }] });
            const findUseableCouponsByShop = await ss.sscoupb.findUseableCouponsByShop({ jsonParam: billInfo.jsonParam });
            const couponInfo = findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId == couponId);

            const getUserAddress = await sp.spmdm.getUserDefaultRecInfo().then(res => res.result.data);
            LOGINDATA.defAddressId = getUserAddress.recInfo.id;
            const evalShipFee = await sp.spconfb.evalShipFee({ provinceCode: getUserAddress.address.provinceCode, orders: [{ sellerId: sellerInfo.tenantId, orderSpus: [{ spuId: spuId, orderNum: 1, orderMoney: spuPrice }] }] })

            purJson = await billManage.mockPurParam(getFullForBuyer.result.data, { count: 1, num: 1, couponInfo: couponInfo, shipFeeMoney: evalShipFee.result.data.fees[0].fee });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
        });
        it('开单前查询有效优惠券', async function () {
            const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '开单前查询有效优惠券不存在此优惠券').to.not.be.undefined;
        });
        describe('下单', async function () {
            before('开单', async function () {
                // savePurBill = await sp.spTrade.savePurBill(purJson);
                savePurBill = await billManage.createPurBill(purJson);
                const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 });
                // console.log(`\npurFindBills=${JSON.stringify(purFindBills.result.data.rows[0])}`);
                expect(Number(purFindBills.result.data.rows[0].bill.billNo), '待付款订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo);
            });

            it('开单后查询可用优惠券数量', async function () {
                const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
                expect(myCouponCount.result.data.coupons, '开单后我的卡券数量没有-1').to.equal(couponsCountBefore - 1);
            });

            it('开单后查询支付中优惠券', async function () {
                const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 2, orderByDesc: true, orderBy: "consumeDate" });
                expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '开单后查询支付中优惠券不存在此优惠券').to.not.be.undefined;
            });
            describe('支付', async function () {
                before('支付', async function () {
                    const createPay = await sp.spTrade.createPay({ orderIds: [savePurBill.result.data.rows[0].billId], payMoney: savePurBill.params.jsonParam.orders[0].main.totalMoney });
                    await sp.spTrade.receivePayResult({ mainId: createPay.result.data.payDetailId, amount: parseFloat(createPay.params.jsonParam.payMoney).toFixed(2), purBillId: savePurBill.result.data.rows[0].billId });
                    const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 2 });
                    expect(Number(purFindBills.result.data.rows[0].bill.billNo), '待发货订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo);
                });

                it('支付后查询可用优惠券数量', async function () {
                    const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
                    expect(myCouponCount.result.data.coupons, '支付后我的卡券数量没有-1').to.equal(couponsCountBefore - 1);
                });

                it('支付后查询已消费优惠券', async function () {
                    const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 3, orderByDesc: true, orderBy: "consumeDate" });
                    expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '支付后查询已消费优惠券不存在此优惠券').to.not.be.undefined;
                });
            });
        });
    });

    describe.skip('购物车购买-退货退款-offline', function () {
        let couponId, couponsCountBefore, purJsonByCart, savePurBill;
        before(async function () {
            //创建优惠券
            await ssReq.ssSellerLogin();
            couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
            // 领取优惠券
            await ssReq.userLoginWithWx();
            await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
            couponsCountBefore = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.coupons);

            const shoppingCart = cartManage.setupShoppingCart(sellerInfo);
            const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            await shoppingCart.emptyCart();
            for (const dres of dresList.slice(0, 2)) {
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dres.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                const cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 2, num: 3 });
                // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
                await shoppingCart.saveCartInBatchs(cartJson);
            };
            let showCartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 });
            console.log(`showCartList=${JSON.stringify(showCartList)}`);
            const cartList = showCartList.result.data.rows;
            const orders = [...cartList.values()].map(order => {
                // console.log(`\n order=${JSON.stringify(order.trader)}`);
                const orderSpus = [...order.carts.values()].map((cart) => {
                    return {
                        spuId: cart.spuId,
                        orderNum: cart.skuNum,
                        orderMoney: cart.skuPrice * cart.skuNum
                    };
                });
                return {
                    sellerId: sellerInfo.tenantId,
                    orderSpus
                }
            });
            const findConpon = [...cartList.values()].map(order => {
                const applicableSpus = [...order.carts.values()].map((cart) => {
                    return {
                        spuId: cart.spuId,
                        spuPrice: cart.skuPrice * cart.skuNum
                    };
                });
                return {
                    sellerTenantId: sellerInfo.tenantId,
                    totalSum: 0,
                    applicableSpus
                }
            });
            let totalSum = 0;
            console.log(`\ncartList=${JSON.stringify(cartList)}`);
            console.log(`\nfindConpon=${JSON.stringify(findConpon)}`);

            for (let spu of findConpon[0].applicableSpus) {
                totalSum += spu.spuPrice;
            }
            findConpon[0].totalSum = totalSum;
            const findUseableCouponsByShop = await ss.sscoupb.findUseableCouponsByShop({ jsonParam: findConpon[0] });
            const couponInfo = findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId == couponId);
            const getUserAddress = await sp.spmdm.getUserDefaultRecInfo();
            const evalShipFee = await sp.spconfb.evalShipFee({ provinceCode: getUserAddress.result.data.provinceCode, orders })
            console.log(`\n showCartList=${JSON.stringify(showCartList)}`);
            purJsonByCart = basicJson.purJsonByCart2({
                styleInfo: showCartList,
                couponInfo: couponInfo,
                evalShipFeeInfo: evalShipFee.result.data.fees[0].fee,
                addressInfo: getUserAddress
            });
            // console.log(`purJsonByCart=${JSON.stringify(purJsonByCart)}`);
            // savePurBill = await sp.spTrade.savePurBill(purJsonByCart);
            savePurBill = await billManage.createPurBill(purJsonByCart);
        });

        it('开单后查询待付款列表', async function () {
            const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 });
            // console.log(`\npurFindBills=${JSON.stringify(purFindBills.result.data.rows[0])}`);
            expect(Number(purFindBills.result.data.rows[0].bill.billNo), '待付款订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo);
        });

        it('开单后查询可用优惠券数量', async function () {
            const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
            expect(myCouponCount.result.data.coupons, '开单后我的卡券数量没有-1').to.equal(couponsCountBefore - 1);
        });

        it('开单后查询支付中优惠券', async function () {
            const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 2, orderByDesc: true, orderBy: "consumeDate" });
            expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '开单后查询支付中优惠券不存在此优惠券').to.not.be.undefined;
        });

        describe('买家支付', function () {
            before('支付', async function () {
                const createPay = await sp.spTrade.createPay({ orderIds: [savePurBill.result.data.rows[0].billId], payMoney: savePurBill.params.jsonParam.orders[0].main.totalMoney });
                await sp.spTrade.receivePayResult({ mainId: createPay.result.data.payDetailId, amount: parseFloat(createPay.params.jsonParam.payMoney).toFixed(2), purBillId: savePurBill.result.data.rows[0].billId });
                const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 2 });
                expect(Number(purFindBills.result.data.rows[0].bill.billNo), '待发货订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo);
            });

            it('支付后查询可用优惠券数量', async function () {
                const myCouponCount = await ss.sscoupb.myCouponCount({ tenantId: sellerInfo.tenantId });
                expect(myCouponCount.result.data.coupons, '支付后我的卡券数量没有-1').to.equal(couponsCountBefore - 1);
            });

            it('支付后查询已消费优惠券', async function () {
                const myCouponList = await ss.sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 3, orderByDesc: true, orderBy: "consumeDate" });
                expect(myCouponList.result.data.rows.find(obj => obj.couponId == couponId), '支付后查询已消费优惠券不存在此优惠券').to.not.be.undefined;
            });
        });

        describe('卖家发货', function () {
            before('发货', async function () {
                await ssReq.ssSellerLogin();
                //订单状态类型，0 返回全部订单， 1 返回待付款订单，2 返回待发货订单， 3 返回已发货订单， 4 返回已完成订单， 5 退货退款， 6 线下订单
                const salesFindBills = await sp.spTrade.salesFindBills({ pageSize: 0, searchToken: savePurBill.result.data.rows[0].billNo, statusType: 2 });
                expect(salesFindBills.result.data.rows[0].bill.billNo, '待发货订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo.toString());
                const salesFindBillFull = await sp.spTrade.salesFindBillFull({ id: salesFindBills.result.data.rows[0].bill.id });
                const findLogisList = await sp.spconfb.findLogisList1({ cap: 1 });
                //发货
                const randonNum = common.getRandomNum(1, findLogisList.result.data.rows.length - 1);
                const details = salesFindBillFull.result.data.skus.map((sku) => {
                    return { salesDetailId: sku.id, num: sku.skuNum };
                });
                await sp.spTrade.deliverSalesBill({
                    main: {
                        logisCompId: findLogisList.result.data.rows[randonNum].id,
                        logisCompName: findLogisList.result.data.rows[randonNum].name,
                        waybillNo: common.getRandomNumStr(12),
                        buyerId: salesFindBills.result.data.rows[0].bill.buyerId,
                        // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                        hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                    },
                    details: details
                });
            });

            it('查询已发货订单', async function () {
                const salesFindBills = await sp.spTrade.salesFindBills({ pageSize: 0, searchToken: savePurBill.result.data.rows[0].billNo, statusType: 3 });
                expect(salesFindBills.result.data.rows[0].bill.billNo, '已发货订单未找到该订单').to.equal(savePurBill.result.data.rows[0].billNo.toString());
            });
        });

        describe('部分退货退款', function () {
            let purBillInfo, findBillBackViewFull;
            before(async function () {
                await ssReq.userLoginWithWx();
                const purFindBills = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 3 });
                purBillInfo = purFindBills.result.data.rows.find(val => val.bill.backFlag == 0);
                // console.log(`purFindBills=${JSON.stringify(purFindBills)}`);
                console.log(`\npurBillInfo=${JSON.stringify(purBillInfo)}`);
                findBillBackViewFull = await sp.spTrade.findBillBackViewFull({ id: purBillInfo.bill.id });
            });

            it('部分退货退款', async function () {
                // 优惠后金额：
                const returnBillJson = await basicJson.returnBillJson2({ purBillInfo, returnNum: 1 });
                console.log(`\nreturnBillJson=${JSON.stringify(returnBillJson)}`);
                console.log(`\nfindBillBackViewFull=${JSON.stringify(findBillBackViewFull.result.data.skus[0])}`);
                // expect(returnBillJson.details[0].skuAvgPrice, '退货第一个sku的优惠后金额不为预期值').to.equal(findBillBackViewFull.result.data.skus[0].skuAvgPrice);
                expect(findBillBackViewFull.result.data.skus[0].skuAvgPrice, '退货第一个sku的优惠后金额不为预期值').to.within(Number((returnBillJson.details[0].skuAvgPrice - 0.01).toFixed(2)), Number((returnBillJson.details[0].skuAvgPrice + 0.01).toFixed(2)));
            });

        });
    });
});