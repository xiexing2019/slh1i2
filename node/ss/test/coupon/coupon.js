const common = require('../../../lib/common');
const sscoupb = require('../../../reqHandler/ss/spb/sscoupb');
const ssReq = require('../../help/ssReq');
const couponHelp = require('../../help/couponHelp');
const moment = require('moment');

describe('优惠券', function () {
    this.timeout(30000);
    // this.retries(2);
    let sellerInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });
    describe('创建未开始优惠券', function () {
        let setCouponData, couponId;
        before('创建', async function () {
            // 新增 满减，固定日期明天，全部商品可用，不限次数，不显示在领券中心，不与其他优惠同享，订单金额不限
            setCouponData = await sscoupb.setCouponData({ cardType: 1, effectPeriodType: 0, applyDresType: 0, showInCenter: false }, fulfilValue = 0.01, running = false);
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
        });
        it('查看优惠券列表', async function () {
            getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '0' });
            // console.log(`getCouponList=${JSON.stringify(getCouponList.result.data.rows)}`);
            common.isApproximatelyEqualAssert(setCouponData, getCouponList.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查看优惠券详情', async function () {
            const couponDetails = await sscoupb.couponDetails({ couponId: couponId });
            const coupon = couponHelp.setBitTranslate(couponDetails.result.data.coupon);
            common.isApproximatelyEqualAssert(setCouponData, coupon);
        });
        it('查询', async function () {
            const getCouponList = await sscoupb.getCouponList({ titleLike: setCouponData.title, tenantId: LOGINDATA.tenantId, flag: '0' }).then(res => res.result.data.rows);
            expect(getCouponList.length, '模糊查询结果没有数据').to.not.equal(0);
            getCouponList.forEach(coupon => {
                expect(coupon.title, '模糊查询失败').to.include(setCouponData.title);
            });
        });
        it('删除', async function () {
            await sscoupb.deleteCoupon({ couponId: couponId });
            const getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '0' }).then(res => res.result.data.rows);
            expect(getCouponList.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.be.undefined;
        });
    });

    describe('创建进行中优惠券', function () {
        let setCouponData, couponId;
        before('创建', async function () {
            // 新增 满折，当天起，部分商品可用，不限次数，不显示在领券中心，不与其他优惠同享，订单金额不限
            setCouponData = await sscoupb.setCouponData({ cardType: 0, effectPeriodType: 1, applyDresType: 1, showInCenter: false });
            console.log(`\n setCouponData=${JSON.stringify(setCouponData)}`);

            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
        });
        it('查看优惠券列表', async function () {
            getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' });
            // console.log(`getCouponList=${JSON.stringify(getCouponList.result.data.rows)}`);
            common.isApproximatelyEqualAssert(setCouponData, getCouponList.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查看优惠券详情', async function () {
            const couponDetails = await sscoupb.couponDetails({ couponId: couponId });
            const coupon = couponHelp.setBitTranslate(couponDetails.result.data.coupon);
            common.isApproximatelyEqualAssert(setCouponData, coupon);
        });
        it('查询', async function () {
            const getCouponList = await sscoupb.getCouponList({ titleLike: setCouponData.title, tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows);
            expect(getCouponList.length, '模糊查询结果没有数据').to.not.equal(0);
            getCouponList.forEach(coupon => {
                expect(coupon.title, '模糊查询失败').to.include(setCouponData.title);
            });
        });
        it('买家领取优惠券', async function () {
            await ssReq.userLoginWithWx();
            await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
        });
        it('领取后查询有效优惠券', async function () {
            const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            const coupon = myCouponList.result.data.rows.find(obj => obj.couponId == couponId);
            console.log(`\n coupon=${JSON.stringify(coupon)}`);
            expect(coupon, '领取后查询有效优惠券不存在此优惠券').to.not.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, coupon, ['unitId']);
            // 结束日期要顺延一天，因为开始时间是从半天的时候开始的，结束则必须是23:59:59
            expect(coupon, '优惠券时间不对').to.include({
                beginDate: `${moment(coupon.receiveDate).add(0, 'days').format('YYYY-MM-DD HH:mm:ss')}`,
                endDate: `${moment(coupon.receiveDate).add(setCouponData.receiveEffectPeriod, 'days').format('YYYY-MM-DD')} 23:59:59`
            });
        });
        it('删除', async function () {
            await ssReq.ssSellerLogin();
            await sscoupb.deleteCoupon({ couponId: couponId });
            const getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows);
            expect(getCouponList.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.be.undefined;
        });
    });

    describe('创建次日生效优惠券', function () {
        let setCouponData, couponId;
        before('创建', async function () {
            // 新增 满折，当天起，部分商品可用，不限次数，不显示在领券中心，不与其他优惠同享，订单金额不限
            setCouponData = await sscoupb.setCouponData({ cardType: 0, effectPeriodType: 2, applyDresType: 1, showInCenter: false });
            console.log(`\n setCouponData=${JSON.stringify(setCouponData)}`);

            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
        });
        it('查看优惠券列表', async function () {
            getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' });
            // console.log(`getCouponList=${JSON.stringify(getCouponList.result.data.rows)}`);
            common.isApproximatelyEqualAssert(setCouponData, getCouponList.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查看优惠券详情', async function () {
            const couponDetails = await sscoupb.couponDetails({ couponId: couponId });
            // console.log(`couponDetails=${JSON.stringify(couponDetails)}`);
            const coupon = couponHelp.setBitTranslate(couponDetails.result.data.coupon);
            common.isApproximatelyEqualAssert(setCouponData, coupon);
        });
        it('查询', async function () {
            const getCouponList = await sscoupb.getCouponList({ titleLike: setCouponData.title, tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows);
            expect(getCouponList.length, '模糊查询结果没有数据').to.not.equal(0);
            getCouponList.forEach(coupon => {
                expect(coupon.title, '模糊查询失败').to.include(setCouponData.title);
            });
        });
        it('买家领取优惠券', async function () {
            await ssReq.userLoginWithWx();
            await sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
        });
        it('领取后查询有效优惠券', async function () {
            const myCouponList = await sscoupb.myCouponList({ sellerTenantId: sellerInfo.tenantId, cardType: 0, flags: 1, orderByDesc: true, orderBy: "receiveDate" });
            const coupon = myCouponList.result.data.rows.find(obj => obj.couponId == couponId);
            console.log(`\n coupon=${JSON.stringify(coupon)}`);
            expect(coupon, '领取后查询有效优惠券不存在此优惠券').to.not.be.undefined;
            common.isApproximatelyEqualAssert(setCouponData, coupon, ['unitId']);
            expect(coupon, '优惠券时间不对').to.include({
                beginDate: `${moment(coupon.receiveDate).add(1, 'days').format('YYYY-MM-DD')} 00:00:00`,
                endDate: `${moment(coupon.receiveDate).add(setCouponData.receiveEffectPeriod, 'days').format('YYYY-MM-DD')} 23:59:59`
            });
        });
        it('删除', async function () {
            await ssReq.ssSellerLogin();
            await sscoupb.deleteCoupon({ couponId: couponId });
            const getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows);
            expect(getCouponList.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.be.undefined;
        });
    });

    // 已结束：1.优惠券过期；2.进行中选择结束
    describe('已结束优惠券', function () {
        let setCouponData, couponId;
        before('创建进行中优惠券，结束', async function () {
            // 新增 满折，当天起，全部商品可用，不限次数，不显示在领券中心，不与其他优惠同享，订单金额不限
            setCouponData = await sscoupb.setCouponData({ cardType: 0, effectPeriodType: 1, showInCenter: false });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            await sscoupb.endCoupon({ couponId: couponId });
        });
        it('查看优惠券列表', async function () {
            getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '2' });
            // console.log(`getCouponList=${JSON.stringify(getCouponList.result.data.rows)}`);
            common.isApproximatelyEqualAssert(setCouponData, getCouponList.result.data.rows.find(obj => obj.id == couponId));
        });
        it('查看优惠券详情', async function () {
            const couponDetails = await sscoupb.couponDetails({ couponId: couponId });
            const coupon = couponHelp.setBitTranslate(couponDetails.result.data.coupon);
            common.isApproximatelyEqualAssert(setCouponData, coupon);
        });
        it('查询', async function () {
            const getCouponList = await sscoupb.getCouponList({ titleLike: setCouponData.title, tenantId: LOGINDATA.tenantId, flag: '2' }).then(res => res.result.data.rows);
            expect(getCouponList.length, '模糊查询结果没有数据').to.not.equal(0);
            getCouponList.forEach(coupon => {
                expect(coupon.title, '模糊查询失败').to.include(setCouponData.title);
            });
        });
        it('删除', async function () {
            await sscoupb.deleteCoupon({ couponId: couponId });
            const getCouponList = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '2' }).then(res => res.result.data.rows);
            expect(getCouponList.find(obj => obj.couponId == couponId), '领取后查询有效优惠券不存在此优惠券').to.be.undefined;
        });
    });

    describe('优惠券修改发放总量大于等于领取数', function () {
        let couponId, totalQuantity;
        before('创建', async function () {
            const setCouponData = await sscoupb.setCouponData({ cardType: 1 });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            const max = setCouponData.totalQuantity < 5 ? setCouponData.totalQuantity : 5;
            const res = await sscoupb.sendToCustomer({ couponId: couponId, custs: await sscoupb.getCustomerCusts({ number: common.getRandomNum(1, max) }) });
            // console.log(`res=${JSON.stringify(res)}`);
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            const receiveNum = coupon.totalQuantity - coupon.inventory;
            totalQuantity = common.getRandomNum(receiveNum, receiveNum + 10);
            await sscoupb.updateCoupon({ couponId: couponId, totalQuantity: totalQuantity });
        });
        it('查看优惠券列表', async function () {
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            expect(totalQuantity, '发放总量修改失败').to.equal(coupon.totalQuantity);
        });
    });

    describe('优惠券修改发放总量小于领取数', function () {
        let couponId, setCouponData;
        before('创建', async function () {
            setCouponData = await sscoupb.setCouponData({ cardType: 1 });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            const max = setCouponData.totalQuantity < 5 ? setCouponData.totalQuantity : 5;
            await sscoupb.sendToCustomer({ couponId: couponId, custs: await sscoupb.getCustomerCusts({ number: common.getRandomNum(1, max) }) });
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            const receiveNum = coupon.totalQuantity - coupon.inventory;
            const totalQuantity = common.getRandomNum(1, receiveNum - 1);
            const updateCoupon = await sscoupb.updateCoupon({ check: false, couponId: couponId, totalQuantity: totalQuantity });
            console.log(`\n updateCoupon=${JSON.stringify(updateCoupon)}`);
            // expect(updateCoupon.result.msgId, '优惠券修改发放总量小于领取数居然成功了').to.equal("coup_total_less_than_published");
            expect(updateCoupon.result, '优惠券修改发放总量小于领取数居然成功了').to.includes({ msgId: 'coup_total_less_than_published' });

        });
        it('查看优惠券列表', async function () {
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            expect(setCouponData.totalQuantity, '优惠券修改发放总量小于领取数居然成功了').to.equal(coupon.totalQuantity);
        });
    });

    describe('优惠券修改发放总量为0', function () {
        let couponId, setCouponData;
        before('创建', async function () {
            setCouponData = await sscoupb.setCouponData({ cardType: 1 });
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            const max = setCouponData.totalQuantity < 5 ? setCouponData.totalQuantity : 5;
            await sscoupb.sendToCustomer({ couponId: couponId, custs: await sscoupb.getCustomerCusts({ number: common.getRandomNum(1, max) }) });
            const updateCoupon = await sscoupb.updateCoupon({ check: false, couponId: couponId, totalQuantity: 0 });
            expect(updateCoupon.result, '优惠券修改发放总量为0居然成功了').to.includes({ msgId: 'coup_total_less_than_published' });
        });

        it('查看优惠券列表', async function () {
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            expect(setCouponData.totalQuantity, '优惠券修改发放总量为0居然成功了').to.equal(coupon.totalQuantity);
        });
    });

    describe('派发', function () {
        let couponId, getCustomerCusts;
        before('派发', async function () {
            const setCouponData = await sscoupb.setCouponData({ cardType: 1 });
            // console.log(`setCouponData=${JSON.stringify(setCouponData)}`);
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            const max = setCouponData.totalQuantity < 5 ? setCouponData.totalQuantity : 5;
            getCustomerCusts = await sscoupb.getCustomerCusts({ number: common.getRandomNum(1, max) });
            await sscoupb.sendToCustomer({ couponId: couponId, custs: getCustomerCusts });
        });
        it('查看优惠券列表', async function () {
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            expect(getCustomerCusts.length, '派发失败').to.equal(coupon.totalQuantity - coupon.inventory);
        });
    });

    describe('派发:领取上限', function () {
        let couponId, getCustomerCusts;
        before('派发', async function () {
            const setCouponData = await sscoupb.setCouponData({ cardType: 1, getLimit: 1 });
            // console.log(`setCouponData=${JSON.stringify(setCouponData)}`);
            couponId = await sscoupb.createCoupon(setCouponData).then(res => res.result.data.val);
            getCustomerCusts = await sscoupb.getCustomerCusts({ number: 1 });
            await sscoupb.sendToCustomer({ couponId: couponId, custs: getCustomerCusts });
        });
        it('查看优惠券列表', async function () {
            const coupon = await sscoupb.getCouponList({ tenantId: LOGINDATA.tenantId, flag: '1' }).then(res => res.result.data.rows.find(obj => obj.id == couponId));
            expect(getCustomerCusts.length, '派发失败').to.equal(coupon.totalQuantity - coupon.inventory);
        });
        it('再次派发', async function () {
            const sendToCustomer = await sscoupb.sendToCustomer({ check: false, couponId: couponId, custs: getCustomerCusts });
            expect(sendToCustomer.result).to.includes({ msg: `买家[${getCustomerCusts[0].custUserId}]因超出优惠券限领数而不会收到优惠券` }, { msgId: 'can_not_send_coupon_to_buyer_because_get_limit' });
        });
    });
});
