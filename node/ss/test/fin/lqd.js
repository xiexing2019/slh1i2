const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');


describe.skip('清分-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let con;
    before(async function () {
        con = await ss.creatSqlPool({ dbName: 'lqdMysql' });
    });
    after(async function () {
        await con.end();
    });
    describe('订单', async function () {
        let sellerInfo, purRes, money, rate = 0.006, orderDetail;
        before(async function () {
            await ssReq.ssSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
            await ssReq.userLoginWithWx();
            const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: getListDresSpu.result.data.rows[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
            // purRes = await sp.spTrade.savePurBill(purJson);
            purRes = await billManage.createPurBill(purJson);
            // console.log(`purRes=${JSON.stringify(purRes)}`);
            const payRes = await sp.spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
            // console.log(`\n支付回调:${JSON.stringify(res)}`);
            money = parseFloat(common.mul(purRes.params.jsonParam.orders[0].main.totalMoney, (1 - rate)).toFixed(2));
            console.log(money);
        });
        it('查询', async function () {
            console.log(purRes.result.data.rows[0].salesBillId);
            let i = 1;
            while (!orderDetail) {
                if (i > 20) throw new Error(`查询数据库liquidation.eb_biz_order_detail已超过20次`);
                await common.delay(5000);
                orderDetail = await con.query(`SELECT * FROM liquidation.eb_biz_order_detail WHERE biz_order_id = '${purRes.result.data.rows[0].salesBillId}'`).then(res => {
                    // console.log(res);
                    return res[0][0];
                });
                console.log(`\norderDetail = ${JSON.stringify(orderDetail)}`);
                i++;
            };
            expect(orderDetail, 'orderDetail不为undefined').to.not.be.undefined;
        });
        it('更新', async function () {
            if (!orderDetail) this.skip();
            console.log(purRes.result.data.rows[0].salesBillId);
            const orderDetailRes = await con.execute(`UPDATE liquidation.eb_biz_order_detail SET status = 2 WHERE id = '${orderDetail.id}'`).then(res => {
                console.log(res);
                return res[0];
            });
            expect(orderDetailRes.affectedRows, 'affectedRows不为1').to.equal(1);
        });
        it('查询', async function () {
            if (!orderDetail) this.skip();
            let i = 1, orderDetailRes;
            while (!orderDetailRes) {
                if (i > 20) throw new Error(`查询数据库liquidation.eb_biz_order_detail已超过20次`);
                await common.delay(5000);
                orderDetailRes = await con.query(`SELECT * FROM liquidation.eb_biz_order_detail WHERE id = '${orderDetail.id}' AND status = 4`).then(res => {
                    console.log(res);
                    return res[0][0];
                });
                console.log(`\norderDetailRes = ${JSON.stringify(orderDetailRes)}`);
                i++;
            };
            expect(orderDetailRes, 'orderDetailRes不为undefined').to.not.be.undefined;
        });
        it('aaaaaaaaaaaa', async function () {
            //result=0&tenantId=380541&unitId=380541&transCode=226347"
            await ssReq.ssSellerLogin();
            const res = await ss.accounting.receiveRecordedResult({ result: 0, transCode: orderDetail.trans_code, unitId: sellerInfo.unitId });
            console.log(`res=${JSON.stringify(res)}`);
        });
    });

    describe('提现', async function () {
        let withdrawDetail, finCashId;
        before(async function () {
            await ssReq.ssSellerLogin();
            // 获取钱包首页初始值
            const homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(`homePageMoney=${JSON.stringify(homePageMoney)}`);
            const finCashMoney = homePageMoney.withdrawableMoney < 1 ? homePageMoney.withdrawableMoney : 1;
            console.log(`finCashMoney=${JSON.stringify(finCashMoney)}`);
            finCashId = await ss.fin.createFinCashBill({ acctId: homePageMoney.useAcctId, money: finCashMoney }).then(res => {
                // console.log(res);
                return res.result.data.val;
            });
        });
        it('查询', async function () {
            let i = 1;
            while (!withdrawDetail) {
                if (i > 20) throw new Error(`查询数据库liquidation.eb_withdraw_detail已超过20次`);
                await common.delay(5000);
                withdrawDetail = await con.query(`SELECT * FROM liquidation.eb_withdraw_detail WHERE detail_id = '${finCashId}'`).then(res => {
                    // console.log(res);
                    return res[0][0];
                });
                console.log(`\nwithdrawDetail = ${JSON.stringify(withdrawDetail)}`);
                i++;
            };
            expect(withdrawDetail, 'withdrawDetail不为undefined').to.not.be.undefined;
        });
        it('更新', async function () {
            if (!withdrawDetail) this.skip();
            const withdrawDetailUpdate = await con.execute(`UPDATE liquidation.eb_withdraw_detail SET status = 5, reason='5' WHERE id = '${withdrawDetail.id}'`).then(res => res[0]);
            expect(withdrawDetailUpdate.affectedRows, 'affectedRows不为1').to.equal(1);
        });
        it('查询', async function () {
            let i = 1, withdrawDetailAfter;
            while (!withdrawDetailAfter) {
                if (i > 20) throw new Error(`查询数据库liquidation.eb_withdraw_detail已超过20次`);
                await common.delay(1000);
                withdrawDetailAfter = await con.query(`SELECT * FROM liquidation.eb_withdraw_detail WHERE id = '${withdrawDetail.id}' AND status = 5`).then(res => {
                    // console.log(res);
                    return res[0][0];
                });
                console.log(`\nwithdrawDetailAfter = ${JSON.stringify(withdrawDetailAfter)}`);
                i++;
            };
            expect(withdrawDetailAfter, 'withdrawDetailAfter不为undefined').to.not.be.undefined;
        });
    });
});