const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const basicJson = require('../../help/basicJson');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const ss = require('../../../reqHandler/ss');
const ssConfigParam = require('../../help/configParamManager');
const messageManage = require('../../help/messageManage');
const billManage = require('../../help/billManage');


describe('线下订单', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, purRes, salesListRes, styleInfoBefore;
    before(async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = LOGINDATA;
        const openOfflinePayment = await ssConfigParam.getSpbParamInfo({ code: 'open_offline_payment', ownerId: LOGINDATA.tenantId });
        console.log(openOfflinePayment);
        await openOfflinePayment.updateParam({ val: 1 });

        await ssReq.ssSellerLogin();
    });

    describe('买家下单', function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
            const spuId = getListDresSpu.result.data.rows[0].id; //52363
            const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, payKind: 3 });
            // console.log(purJson);

            await ssReq.ssSellerLogin();
            await common.delay(500);
            //卖家商品详情
            styleInfoBefore = await spdresb.getFullById({ id: purJson.orders[0].details[0].spuId });
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore.result.data)}`);
            await ssReq.userLoginWithWx();
            await common.delay(500);
            // purRes = await spTrade.savePurBill(purJson);
            purRes = await billManage.createPurBill(purJson);
            // console.log(`\n purRes=${JSON.stringify(purRes)}`);

            // 开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.parent ? this.parent.skip() : this.skip();
            };

            await ssReq.ssSellerLogin();
            await common.delay(500);
        });

        it('查询消息列表', async function () {
            await messageManage.messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: '71', text: '您收到了新的订单', title: '新订单提醒' });
        });
        it('查询销售单列表', async function () {
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesList)}`);
            expect(salesListRes.bill.payFlag).to.equal(0);
        });
        it('查询单据详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家商品详情', async function () {
            let styleInfoAfter = await spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            common.isApproximatelyEqualAssert(styleInfoBefore.result.data, styleInfoAfter.result.data, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'occupyNum', 'stockNum']);
        });
    });

    describe('买家选择线下支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await ssReq.userLoginWithWx();
            //payType:-99、线下支付
            payRes = await spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await ssReq.ssSellerLogin();
        });
        after(async function () {
            await ssReq.ssSellerLogin();
        })

        it('卖家销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(-1);//未付款
            //订单前端状态值: 1. 待付款,2. 待发货, 3. 待收货, 4. 交易完成, 5. 交易已关闭, 6. 处理中
            expect(salesListRes.bill.frontFlag).to.equal(8);//未发货
        });
        it('卖家销售单详情', async function () {
            salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });

            expect(salesBillInfo.result.data.bill.payFlag).to.equal(-1);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(8);
        });

        it('买家选择线下支付后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`\n spPurInfo=${JSON.stringify(spPurInfo.result.data.bill)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['specPropsJson']);
            //frontFlag:1. 待付款,2. 待发货, 3. 待收货, 4. 交易完成, 5. 交易已关闭, 6. 处理中 8. 线下支付待审核
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 8, payFlag: -1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });

    describe('卖家审核订单', function () {
        let payRes, salesBillInfo, recentBuyShopList, findSellerPayWay;
        before(async function () {
            await ssReq.ssSellerLogin();
            common.delay(500);
            //payType:-99、线下支付
            findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
            // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
            const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: salesListRes.bill.id, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });
            // console.log(`verifyOfflineBill=${JSON.stringify(verifyOfflineBill)}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
        })

        it('卖家销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(5);//
            expect(salesListRes.bill.frontFlag).to.equal(2);//未发货
        });
        it('卖家商品详情-销售数量和销售金额', async function () {
            this.retries(2);
            await common.delay(500);
            // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
            let styleInfoAfter = await spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            let exp = common.addObject({
                stockNum: -purRes.params.jsonParam.orders[0].main.totalNum,
                salesNum: purRes.params.jsonParam.orders[0].main.totalNum,
                salesMoney: purRes.params.jsonParam.orders[0].main.totalMoney
            }, styleInfoBefore.result.data.spu);
            common.isApproximatelyEqualAssert(exp, styleInfoAfter.result.data.spu, ['sessionId', 'ver', 'channelIds']);
        });
        it('卖家销售单详情', async function () {
            salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });

            expect(salesBillInfo.result.data.bill.payFlag).to.equal(5);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(2);
        });

        it('买家支付后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`\n spPurInfo=${JSON.stringify(spPurInfo.result.data.bill)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['specPropsJson']);
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 2, payFlag: 5, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });

    describe('卖家发货', function () {
        let qfRes;
        before(async function () {
            await ssReq.ssSellerLogin();
            //列表skus只取单据sku中的一条数据。。
            const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
            // console.log(`qlRes=${JSON.stringify(qlRes)}`);
            salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            //查询单据详情
            qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
            const details = qfRes.result.data.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });
            const logisList = await spconfb.findLogisList1({ cap: 1 });
            const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
            await spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[randonNum].id,
                    logisCompName: logisList.result.data.rows[randonNum].name,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: qlRes.result.data.rows[0].bill.buyerId,
                    // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            });
        });
        it('卖家销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == qfRes.result.data.bill.billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag).to.equal(3);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: qfRes.result.data.bill.id });//
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(3);
        });
        it('卖家商品详情-库存', async function () {
            let styleInfoAfter = await spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // console.log(`purRes=${JSON.stringify(purRes)}`);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            common.isApproximatelyEqualAssert({ stockNum: common.sub(styleInfoBefore.result.data.spu.stockNum, purRes.params.jsonParam.orders[0].main.totalNum) }, styleInfoAfter.result.data.spu);
        });
        it('卖家发货后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['specPropsJson']);
            common.isApproximatelyEqualAssert({ flag: 6, frontFlag: 3, payFlag: 5, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);

        });
    });

    describe('买家收货', function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            //买家确认收货
            await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
            // console.log(`确认收货：=${JSON.stringify(res)}`);
        });
        it('确认收货后-延迟收货', async function () {
            const res = await spTrade.extendedReturn({ id: purRes.result.data.rows[0].billId, check: false });
            expect(res.result).to.includes({ msgId: 'spbillflag_after_confirm' });
        });
        it('买家采购单列表', async function () {

            const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });

            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag).to.equal(4);
        });

        it('买家采购单详情', async function () {
            const purInfo = await spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam, purInfo.result.data);
            expect(purInfo.result.data.bill.frontFlag).to.equal(4);
        })
        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();
            await messageManage.messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 77, text: '买家已经确认收货', title: '收货提醒' });
        });
        it('卖家销售单列表', async function () {
            const salesList = await spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag).to.equal(4);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(4);
        });

        it('买家收货后管理端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`\n spPurInfo=${JSON.stringify(spPurInfo.result.data.bill)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['specPropsJson']);
            common.isApproximatelyEqualAssert({ flag: 8, frontFlag: 4, payFlag: 5, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });
});
