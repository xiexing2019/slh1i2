const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const billManage = require('../../help/billManage');


describe('取消订单', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, purRes, salesListRes, styleInfoBefore;
    before('下单', async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await ssReq.userLoginWithWx();
        const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true });
        const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: getListDresSpu.result.data.rows[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        // const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        const purJson = await billManage.mockPurParam(getFullForBuyer.result.data, { count: 1, num: 1 });
        purJson.orders[0].main.sellerId = sellerInfo.tenantId;
        await ssReq.ssSellerLogin();
        //卖家商品详情
        styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
        console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);

        await ssReq.userLoginWithWx();
        purRes = await billManage.createPurBill(purJson);
        console.log(`purRes=${JSON.stringify(purRes)}`);
        // 开单失败则结束当前脚本
        if (purRes.result.data.rows[0].isSuccess != 1) {
            console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
            this.parent ? this.parent.skip() : this.skip();
        };
        await ssReq.ssSellerLogin();
    });
    it('查询销售单列表', async function () {
        const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });
        salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
        // console.log(`销售单列表:${JSON.stringify(salesList)}`);
        expect(salesListRes.bill.payFlag).to.equal(0);
    });
    it('查询单据详情', async function () {
        //销售单id 非采购单
        const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id }); //
        // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
        expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
    });
    it('卖家商品详情', async function () {
        let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
        // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter.result.data)}`);
        const styleInfo = _.cloneDeep(styleInfoBefore);
        console.log(`styleInfo=${JSON.stringify(styleInfo)}`);
        // styleInfo.skus[0].occupyNum += 1;
        // styleInfo.spu.stockNum -= 1;
        common.isApproximatelyEqualAssert(styleInfo, styleInfoAfter.result.data, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'splitSkus']);
    });
    describe('取消订单', async function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            BASICDATA['2008'] = await sp.spugr.getDictList({ typeId: '2008', flag: 1 }).then((res) => res.result.data.rows);
            await sp.spTrade.cancelPurBill({ id: purRes.result.data.rows[0].billId, cancelKind: BASICDATA['2008'][0].codeValue, buyerRem: common.getRandomStr(4) });
            await ssReq.ssSellerLogin();
        });
        it('查询销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(0);
        });
        it('查询单据详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id }); //
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter.result.data)}`);
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter.result.data, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
    });
});
