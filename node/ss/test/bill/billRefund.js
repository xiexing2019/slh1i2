const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const basicJson = require('../../help/basicJson');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const ss = require('../../../reqHandler/ss');
const billManage = require('../../help/billManage');

describe('仅退款-offline', function () {
    this.timeout(30000);
    let sellerInfo, purBillInfo, returnBill, returnBillSeller, saveDeliverRes, returnBillDate, receiveInfo, toBeRefundNum, totalNum;

    before('新建已付款订单', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        const spuId = dresSpuList.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // const purRes = await spTrade.savePurBill(purJson);
        const purRes = await billManage.createPurBill(purJson);

        // console.log(purRes.result.data);
        payRes = await spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

        const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        purBillInfo = purBillList.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
        // expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;

        // 开单失败则结束当前脚本
        if (typeof purBillInfo == 'undefined') {
            console.warn(`待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别3003）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
        toBeRefundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);

    });

    describe('买家创建退款申请单仅退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            const params = await basicJson.returnBillJson2({ purBillInfo, typeId: 0 });
            returnBill = await spTrade.saveReturnBill(params);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            totalNum = params.main.totalNum;
        });

        it('买家查看采购订单的售后列表', async function () {
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0 });
            res.result.data.rows[0].bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('买家申请退款后平台端退货列表查询-offline', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `买家申请退款后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id']);
        });

        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应加一').to.equal(toBeRefundNum + 1);
        });
    });

    describe('卖家审核退货申请', function () {
        before('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(500);
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            // console.log(res.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
            // console.log(returnBillSeller.bill.id);
        });

        it('审核前卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);

        });

        it('卖家审核', async function () {
            // 卖家审核
            const checkReturnRes = await spTrade.checkReturnBill({
                id: returnBillSeller.bill.id,
                checkResult: 1, //同意
            });
            console.log(`\ncheckReturnRes.result.data=${JSON.stringify(checkReturnRes)}`);

            // const arr = checkReturnRes.result.data.payWebUrl.split('=')[1].split('_');
            let params = {
                bizType: 1260,
                mainId: returnBillSeller.bill.id
            }
            // console.log(params);

            // const receivePayResult = await ss.spsales.returnReceivePayResult(params);
            // // console.log(receivePayResult);
            // expect(checkReturnRes.result.code).to.equal(0);
        });

        it('审核后卖家查看退货列表', async function () {
            await common.delay(1000);
            const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBillSeller.bill.id });
            let exp = getSellerListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id', 'rem']);
        })

        it('审核后卖家查看退货详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);
        });

        it('买家登录查看售后流程', async function () {
            await ssReq.userLoginWithWx();;
            const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
            // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
            expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 0 });
            purReturnListInfo.bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, purReturnListInfo, ['rem']);
        });

        it('卖家审核后平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表查询flag更新慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`卖家审核后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id', 'flag']);
        });

        it('卖家审核后平台端查看退货详情-offline', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBillSeller.bill.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0, receiveInfo });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['id', 'rem', 'procFlag']); //procFlag=0 未付款，procFlag=2 付款中
        });
        it('卖家首页统计待退款数-offline', async function () {  // 线上卖家钱包没有钱，退款失败
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应不变').to.equal(toBeRefundNum);
        });
    });

    describe.skip('买家删除退货退款', function () {
        let purBillList;
        before(async function () {
            await ssReq.userLoginWithWx();;
            //买家删除
            // spTrade.purFindBills
            purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            // console.log(`=${JSON.stringify(purBillList.result.data.rows[0])}`);
            await ss.spsales.displayPurBill({ purBillIds: [purBillInfo.bill.id] });
        });
        it('买家采购单列表', async function () {
            const purBillListAfter = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            expect(purBillList.result.data.total).to.equal(purBillListAfter.result.data.total + 1);
        });
    });

    describe.skip('卖家删除退货退款', function () {
        let findSalesReturnBills;
        before(async function () {
            await ssReq.ssSellerLogin();
            //卖家删除
            findSalesReturnBills = await spTrade.findSalesReturnBills();
            // console.log(JSON.stringify(returnBillSeller));
            await ss.spsales.hideReturnBills({ returnBillIds: [returnBillSeller.bill.id] });
        });
        it('卖家退货退款列表', async function () {
            findSalesReturnBillsAfter = await spTrade.findSalesReturnBills();
            expect(findSalesReturnBills.result.data.total).to.equal(findSalesReturnBillsAfter.result.data.total + 1);
        });
    });
});

describe('仅退款:部分退款-offline', function () {
    this.timeout(30000);
    let sellerInfo, purBillInfo, returnBill, returnBillSeller, saveDeliverRes, returnBillDate, receiveInfo, toBeRefundNum, totalNum;

    before('新建已付款订单', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true });
        const spuId = dresSpuList.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // const purRes = await spTrade.savePurBill(purJson);
        const purRes = await billManage.createPurBill(purJson);

        // console.log(purRes.result.data);
        payRes = await spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

        const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        purBillInfo = purBillList.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
        // expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;

        // 开单失败则结束当前脚本
        if (typeof purBillInfo == 'undefined') {
            console.warn(`待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别3003）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
        toBeRefundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);

    });

    describe('买家创建退款申请单仅退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            console.log(`\n purBillInfo=${JSON.stringify(purBillInfo)}`);
            const params = await basicJson.returnBillJson2({ purBillInfo, typeId: 0 });
            console.log(`\n params=${JSON.stringify(params)}`);
            if (params.main.totalMoney > 1) params.main.totalMoney = common.getRandomNum(1, params.main.totalMoney);
            console.log(`\n params=${JSON.stringify(params)}`);
            returnBill = await spTrade.saveReturnBill(params);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            totalNum = params.main.totalNum;
        });

        it('买家查看采购订单的售后列表', async function () {
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0 });
            res.result.data.rows[0].bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('买家申请退款后平台端退货列表查询-offline', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `买家申请退款后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id']);
        });

        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应加一').to.equal(toBeRefundNum + 1);
        });
    });

    describe('卖家审核退货申请', function () {
        before('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(500);
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            // console.log(res.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
            // console.log(returnBillSeller.bill.id);
        });

        it('审核前卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);

        });

        it('卖家审核', async function () {
            // 卖家审核
            const checkReturnRes = await spTrade.checkReturnBill({
                id: returnBillSeller.bill.id,
                checkResult: 1, //同意
            });
            // console.log(`checkReturnRes.result.data=${JSON.stringify(checkReturnRes.result.data)}`);

            // const arr = checkReturnRes.result.data.payWebUrl.split('=')[1].split('_');
            let params = {
                bizType: 1260,
                mainId: returnBillSeller.bill.id
            }
            // console.log(params);
            // const receivePayResult = await ss.spsales.returnReceivePayResult(params);
            // console.log(receivePayResult);
            expect(checkReturnRes.result.code).to.equal(0);
        });

        it('审核后卖家查看退货列表', async function () {
            await common.delay(1000);
            const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBillSeller.bill.id });
            let exp = getSellerListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id', 'rem']);
        })

        it('审核后卖家查看退货详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);
        });

        it('买家登录查看售后流程', async function () {
            await ssReq.userLoginWithWx();;
            const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
            // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
            expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 0 });
            purReturnListInfo.bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, purReturnListInfo, ['rem']);
        });

        it('卖家审核后平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表查询flag更新慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`卖家审核后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id', 'flag']);
        });

        it('卖家审核后平台端查看退货详情-offline', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBillSeller.bill.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0, receiveInfo });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['id', 'rem', 'procFlag']);
        });
        it('卖家首页统计待退款数-offline', async function () {  // 线上卖家钱包没有钱 退款失败
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应不变').to.equal(toBeRefundNum);
        });
    });

});

describe('仅退款:卖家不同意-offline', function () {
    this.timeout(30000);
    let sellerInfo, purBillInfo, returnBill, returnBillSeller, saveDeliverRes, returnBillDate, receiveInfo, toBeRefundNum, totalNum;

    before('新建已付款订单', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        const spuId = dresSpuList.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // const purRes = await spTrade.savePurBill(purJson);
        const purRes = await billManage.createPurBill(purJson);
        // console.log(purRes.result.data);
        payRes = await spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

        const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        purBillInfo = purBillList.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
        // expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;

        // 开单失败则结束当前脚本
        if (typeof purBillInfo == 'undefined') {
            console.warn(`待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别3003）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
        toBeRefundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);

    });

    describe('买家创建退款申请单仅退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            const params = await basicJson.returnBillJson2({ purBillInfo, typeId: 0 });
            console.log(`\n params=${JSON.stringify(params)}`);
            returnBill = await spTrade.saveReturnBill(params);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            totalNum = params.main.totalNum;
        });

        it('买家查看采购订单的售后列表', async function () {
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0 });
            res.result.data.rows[0].bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('买家申请退款后平台端退货列表查询-offline', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `买家申请退款后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id']);
        });

        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应加一').to.equal(toBeRefundNum + 1);
        });
    });

    describe('卖家审核退货申请', function () {
        before('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(500);
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            // console.log(res.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
            // console.log(returnBillSeller.bill.id);
        });

        it('审核前卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);

        });

        it('卖家审核', async function () {
            const dictList = await ss.config.getSpgDictList({ typeId: 3004, flag: 1 }).then(res => res.result.data.rows);
            console.log(`\n dictList=${JSON.stringify(dictList)}`);
            // 卖家审核
            const checkReturnRes = await spTrade.checkReturnBill({
                id: returnBillSeller.bill.id,
                checkResult: 0, //不同意
                rejectReason: dictList[common.getRandomNum(0, dictList.length - 1)].id
            });
            console.log(`checkReturnRes=${JSON.stringify(checkReturnRes)}`);
        });

        it('审核后卖家查看退货列表', async function () {
            await common.delay(1000);
            const qlRes = await spTrade.findSalesReturnBills({ orderBy: 'proTime', orderByDesc: 'true', searchToken: returnBillSeller.bill.id });
            let exp = getSellerListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id', 'rem']);
        })

        it('审核后卖家查看退货详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);
        });

        it('买家登录查看售后流程', async function () {
            await ssReq.userLoginWithWx();;
            const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
            // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
            expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 0 });
            purReturnListInfo.bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, purReturnListInfo, ['rem']);
        });

        it('卖家审核后平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表查询flag更新慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`卖家审核后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id', 'flag']);
        });

        it('卖家审核后平台端查看退货详情-offline', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBillSeller.bill.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0, receiveInfo });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['id', 'rem', 'procFlag']);
        });
        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应不变').to.equal(toBeRefundNum);
        });
    });
});

describe('仅退款:买家撤销-offline', function () {
    this.timeout(30000);
    let sellerInfo, purBillInfo, returnBill, returnBillSeller, saveDeliverRes, returnBillDate, receiveInfo, toBeRefundNum, totalNum;

    before('新建已付款订单', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        const spuId = dresSpuList.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // const purRes = await spTrade.savePurBill(purJson);
        const purRes = await billManage.createPurBill(purJson);
        // console.log(purRes.result.data);
        payRes = await spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

        const purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        purBillInfo = purBillList.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
        // expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;

        // 开单失败则结束当前脚本
        if (typeof purBillInfo == 'undefined') {
            console.warn(`待发货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别3003）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
        toBeRefundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);

    });

    describe('买家创建退款申请单仅退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            const params = await basicJson.returnBillJson2({ purBillInfo, typeId: 0 });
            console.log(`\n params=${JSON.stringify(params)}`);
            returnBill = await spTrade.saveReturnBill(params);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            totalNum = params.main.totalNum;
        });

        it('买家查看采购订单的售后列表', async function () {
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0 });
            res.result.data.rows[0].bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('买家申请退款后平台端退货列表查询-offline', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `买家申请退款后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 3, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id']);
        });

        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应加一').to.equal(toBeRefundNum + 1);
        });
        it('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(500);
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            // console.log(res.result.data.rows[0]);
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
            // console.log(returnBillSeller.bill.id);
        });
        it('卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 0
            });
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'rem']);
        });
    });

    describe('买家撤销退款申请', function () {
        before('买家撤销退款申请', async function () {
            await ssReq.userLoginWithWx();
            let res = await spTrade.cancelReturn({ id: returnBill.result.data.val });
            console.log(`\n res=${JSON.stringify(res)}`);
        });

        it('买家登录查看售后流程', async function () {
            await ssReq.userLoginWithWx();;
            const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
            // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
            expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 0 });
            purReturnListInfo.bill.totalNum = totalNum;
            common.isApproximatelyEqualAssert(exp, purReturnListInfo, ['rem']);
        });

        it('平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表查询flag更新慢
            await ss.spAuth.staffLogin();
            await common.delay(1000);
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.billNo == purBillInfo.bill.billNo));
            // console.log(`卖家审核后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ purBillInfo, returnBill, flag: 6, returnType: 0 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['id', 'flag']);
        });

        it('平台端查看退货详情-offline', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBillSeller.bill.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 0, receiveInfo });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data, ['id', 'rem']);
        });

        it('卖家首页统计待退款数', async function () {
            await ssReq.ssSellerLogin();
            const refundNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeRefundNum);
            expect(refundNum, '卖家首页统计待退款数应不变').to.equal(toBeRefundNum);
        });
    });
});

/**
 * 此方法主要用来获取买家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} typeId：需要得到买家哪一步的预期结果 1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  saveDeliverRes:买家发货返回的JSON
*/
function getBuyerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };
    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[5]);
                break;
            case 3:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[4]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '退货完成';
                break;
        };
    } else {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(0)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退款';
                break;
            case 1:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 3:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '交易完成';
                // flows = getBuyerExpJson(params.saveReturnBillRes, 0)[6].concat(getBuyerExpJson(params.saveReturnBillRes, 0)[1]);
                // bill.flag = 4;   //线上7  测试4
                // bill.frontFlag = 4;
                // bill.frontFlagName = '交易完成';
                break;
        };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, flows, skus: params.purBillInfo.skus };

};

/**
 * 获取买家所有状态的JSON
*/
function getBuyerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }, {
            flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            flowName: '等待买家寄出货品',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            flowName: '等待卖家确认收货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }, {
            flowName: '卖家已退款，3-5天内到账',
            oldFlag: 5,
            newFlag: 6,
            typeId: 5,
        }],
        3: [{
            flowName: '退款成功',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }, {
            flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            flowName: '卖家已同意，退款中',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }, {
            flowName: '卖家已退款，3-5天内到账',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        3: [{
            flowName: '退款成功',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/**
 * 此方法主要用来获取卖家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.saveDeliverRes:买家发货返回的JSON
*/
function getSellerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
        returnReason: params.saveReturnBillRes.params.jsonParam.main.returnReasonName,
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        // totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };

    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getSellerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5]);
                bill.flag = 6;
                bill.frontFlag = 6;
                bill.frontFlagName = '卖家已确认收货';
                break;
            case 3:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5], getSellerExpJson(1)[3]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '退货完成';
                break;
        };
    } else {
        // switch (params.typeId) {
        //     case 6:
        //         flows = getSellerExpJson(0)[6];
        //         bill.flag = 3;
        //         bill.frontFlag = 3;
        //         bill.frontFlagName = '买家申请退款';
        //         break;
        //     case 3:
        //         flows = getSellerExpJson(0)[6].concat(getSellerExpJson(0)[1], getSellerExpJson(0)[3]);
        //         bill.flag = 7
        //         bill.frontFlag = 7;
        //         bill.frontFlagName = '退款完成';
        //         break;
        // };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, flows, skus: params.purBillInfo.skus };
};

/**
 * 获取卖家所有状态的JSON
*/
function getSellerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            flowName: '等待买家退货',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            flowName: '买家已发货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }],
        3: [{
            flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }],
        3: [{
            flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/**
 * 获取卖家列表的预期结果
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function getSellerListExp(params) {
    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.returnBill.params._tid,
        id: params.returnBill.result.data.val,
        purBillId: params.returnBill.params.jsonParam.main.purBillId,
        salesBillId: params.purBillInfo.bill.salesBillId,
        totalMoney: params.returnBill.params.jsonParam.main.totalMoney,
        totalNum: params.returnBill.params.jsonParam.main.totalNum
    };

    // switch (params.flag) {
    //     case 3:
    //         bill.flag = 3;
    //         bill.frontFlag = 3;
    //         params.returnType == 1 ? bill.frontFlagName = '买家申请退货退款' : bill.frontFlagName = '买家申请退款';
    //         break;
    //     case 4:
    //         bill.flag = 4;
    //         bill.frontFlag = 4;
    //         bill.frontFlagName = '买家退货中';
    //         break;
    //     case 5:
    //         bill.flag = 5;
    //         bill.frontFlag = 5;
    //         bill.frontFlagName = '买家已发货';
    //         break;
    //     case 6:
    //         bill.flag = 6;
    //         bill.frontFlag = 6;
    //         bill.frontFlagName = '卖家已确认收货';
    //     bill.flag = 7;
    //     bill.frontFlag = 7;
    //     params.returnType == 1 ? bill.frontFlagName = '退货完成' : bill.frontFlagName = '退款完成';
    // };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, skus: params.purBillInfo.skus }
};

/**
 * 管理员查看退货详情
 * @param {object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.returnType
 */
function adminReturnInfoExp(params) {
    const mapField = 'billNo;buyerRem;confirmTime;logisCompid;money;payKind;purBillId=id;payTime;proTime;procFlag;salesBillId;shipFeeMoney;totalMoney;totalNum;waybillNo';
    let exp = getSellerExp(params);
    let bill = Object.assign(format.dataFormat(params.purBillInfo.bill, mapField), {
        id: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        favourtotalMoney: common.add(params.purBillInfo.bill.favorMoney, params.purBillInfo.bill.shopCoupsMoney),//优惠总金额
    });

    exp.bill = bill;
    exp.bill.receiveAddress = params.receiveInfo.ecCaption.addrId;
    exp.bill.receiveName = params.receiveInfo.linkMan;
    exp.bill.receivePhone = params.receiveInfo.telephone;
    return exp;
};

/**
 * 获取平台端退货列表查询期望值
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function adminReturnListExp(params) {
    const mapField = 'sellerInfo.clusterCode;sellerId=tenantId;sellerName=tenantName;sellerUnitId=unitId';
    let sellerList = getSellerListExp(params);
    let exp = Object.assign(sellerList.skus[0], sellerList.bill, format.dataFormat(params.purBillInfo.trader, mapField));
    exp.typeId = params.returnBill.params.jsonParam.main.typeId;
    exp.rem = params.returnBill.params.jsonParam.main.rem
    exp.proTime = params.returnBill.opTime;
    return exp;
};