
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const messageManage = require('../../help/messageManage');
// const employeeManage = require('../../help/employeeManage');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');
const dresManage = require('../../help/dresManage');
const MyShop = require('../../help/shop/myShop');

describe('订单流程', function () {
    this.timeout(TESTCASE.timeout);
    const myShop = new MyShop();
    let sellerInfo, clientInfo, purRes, getFullForBuyer, salesListRes, styleInfoBefore, toBeDeliverNum, salesOverview, rate = 0.003,
        money, sales, logisList, deliverSalesBill;
    /** 钱包首页数据 */
    let homePagedata = {};
    /** 商品 */
    const dres = dresManage.setupDres({ type: 'app' });
    before(async function () {
        await ssReq.userLoginWithWx();
        clientInfo = _.cloneDeep(LOGINDATA);
        //卖家登录
        await myShop.switchSeller();
        await myShop.getConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', ownerId: LOGINDATA.tenantId }, 'spb');
        await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
        await dresManage.prePrepare();
        await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu, skus) => {
            skus.forEach((sku) => {
                sku.num = 0;
                sku.ssNum = 0;
            });
        }));
        sellerInfo = _.cloneDeep(myShop.getSellerInfo());
        console.log(`\n LOGINDATA=${JSON.stringify(LOGINDATA)}`);

    });

    describe('买家下单', function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dres.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            const purJson = await billManage.mockPurParam(dresFull);
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;

            purRes = await billManage.createPurBill(purJson);
            console.log(`purRes=${JSON.stringify(purRes)}`);
            money = parseFloat(common.mul(purRes.params.jsonParam.orders[0].main.totalMoney, (1 - rate)).toFixed(2));
            console.log(money);
        });
        describe('再来一单', async function () {
            describe('无库存允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    purRes.params.jsonParam.orders[0].details.forEach(detail => {
                        common.isApproximatelyEqualAssert({
                            // pubPrice: common.div(detail.money, detail.num),
                            skuId: detail.skuId,
                            skuNum: detail.num
                        }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                    });
                });
            });
            describe('无库存不允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                after('库存小于0允许上架', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    expect(cartList).to.be.empty;
                });
            });
        });
    });

    describe('买家支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await myShop.switchSeller();
            await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
            await common.delay(15000);
            await ssReq.userLoginWithWx();
            payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            console.log(`payRes=${JSON.stringify(payRes)}`);
            // await sp.spTrade.receivePayResult({
            //     mainId: payRes.result.data.payDetailId,
            //     amount: payRes.params.jsonParam.payMoney
            // });
            // console.log(`\n支付回调:${JSON.stringify(res)}`);

            await myShop.switchSeller();
            const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
            // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
            const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

        });

        describe('再来一单', async function () {
            describe('无库存允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    const detail = purRes.params.jsonParam.orders[0].details[0];
                    common.isApproximatelyEqualAssert({
                        // pubPrice: common.div(detail.money, detail.num),
                        skuId: detail.skuId,
                        skuNum: detail.num
                    }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                });
            });
            describe('无库存不允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                after('库存小于0允许上架', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    expect(cartList).to.be.empty;
                });
            });
        });
    });

    describe('卖家发货', function () {
        let qfRes;
        before(async function () {
            await myShop.switchSeller();
            //查询单据详情
            qfRes = await sp.spTrade.salesFindBillFull({
                id: purRes.result.data.rows[0].salesBillId
            });
            const details = qfRes.result.data.skus.map((sku) => {
                return {
                    salesDetailId: sku.id,
                    num: sku.skuNum
                };
            });
            logisList = await sp.spconfb.findLogisList1({
                cap: 1
            });
            const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
            deliverSalesBill = await sp.spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[randonNum].id,
                    logisCompName: logisList.result.data.rows[randonNum].name,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: clientInfo.tenantId,
                    // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            }).then(res => res.result.data.billId);
            console.log(`deliverSalesBill=${JSON.stringify(deliverSalesBill)}`);

        });

        describe('再来一单', async function () {
            describe('无库存允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                    await common.delay(5000);

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    const detail = purRes.params.jsonParam.orders[0].details[0];
                    common.isApproximatelyEqualAssert({
                        // pubPrice: common.div(detail.money, detail.num),
                        skuId: detail.skuId,
                        skuNum: detail.num
                    }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                });
            });
            describe('无库存不允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });
                    await common.delay(5000);

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                after('库存小于0允许上架', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    expect(cartList).to.be.empty;
                });
            });
        });
    });

    describe('买家收货', function () {
        let purInfo;
        before(async function () {
            await ssReq.userLoginWithWx();
            // await common.delay(3000);
            //买家确认收货
            await sp.spTrade.confirmReceipt({
                purBillIds: [purRes.result.data.rows[0].billId]
            });
            // console.log(`确认收货：=${JSON.stringify(res)}`);
        });

        describe('再来一单', async function () {
            describe('无库存允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                    await common.delay(5000);

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    const detail = purRes.params.jsonParam.orders[0].details[0];
                    common.isApproximatelyEqualAssert({
                        // pubPrice: common.div(detail.money, detail.num),
                        skuId: detail.skuId,
                        skuNum: detail.num
                    }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                });
            });
            describe('无库存不允许下单', async function () {
                before('再来一单', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });

                    await ssReq.userLoginWithWx();
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                after('库存小于0允许上架', async function () {
                    await myShop.switchSeller();
                    await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    expect(cartList).to.be.empty;
                });
            });
        });
    });
});
