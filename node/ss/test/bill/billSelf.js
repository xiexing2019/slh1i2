const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const billManage = require('../../help/billManage');
const ssConfigParam = require('../../help/configParamManager');

describe('自提订单', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, purRes, salesListRes, styleInfoBefore;
    const dres = dresManage.setupDres();
    // bill = billManage.setUpPurBill();

    before(async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
        const goodsPickUpBySelf = await ssConfigParam.getSpbParamInfo({ code: 'goods_pick_up_by_self', ownerId: LOGINDATA.tenantId });
        await goodsPickUpBySelf.updateParam({ val: 1 });

        await ssReq.ssSellerLogin();
    });

    describe('买家下单', function () {
        before(async function () {
            const spuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
            const dresFull = await sp.spdresb.getFullById({ id: spuList[0].id }).then(res => res.result.data);
            dres.setBySeller(dresFull);
            await dres.searchAndSetByDetail();

            await ssReq.userLoginWithWx();
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            clientInfo = _.cloneDeep(LOGINDATA);
            // 自提
            const purJson = billManage.mockPurParam(dres, { shipPayKind: 2 });
            // console.log(`purJson=${JSON.stringify(purJson)}`);

            await ssReq.ssSellerLogin();
            await common.delay(500);
            //卖家商品详情
            styleInfoBefore = await dres.getFullById();
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore.result.data)}`);
            await ssReq.userLoginWithWx();
            await common.delay(500);
            // purRes = await sp.spTrade.savePurBill(purJson);
            purRes = await billManage.createPurBill(purJson);
            // console.log(`purRes=${JSON.stringify(purRes.result.data)}`);

            // 开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.parent ? this.parent.skip() : this.skip();
                process.exit(0);
            };

            await ssReq.ssSellerLogin();
            await common.delay(500);
        });
        it('查询销售单列表', async function () {
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesList)}`);
            expect(salesListRes.bill.payFlag).to.equal(0);
        });
        it('查询单据详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter.result.data, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'occupyNum', 'stockNum']);
        });
    });

    describe('买家支付', function () {
        let payRes, salesBillInfo;
        before(async function () {
            await ssReq.userLoginWithWx();
            payRes = await sp.spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
            // console.log(`\n支付回调:${JSON.stringify(res)}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
        });

        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();

            //await messageCenter({ billNo: purRes.result.data.rows[0].billNo, tagOneIn: 72, text: '买家已经付款', title: '买家已付款' });
        });
        it('卖家销售单列表', async function () {
            this.retries(2);
            await common.delay(100);
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill).to.includes({ payFlag: 1, frontFlag: 2 });//已付款 未发货
        });
        it('卖家商品详情-销售数量和销售金额', async function () {
            this.retries(2);
            await common.delay(500);
            let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            let exp = common.addObject({
                stockNum: -purRes.params.jsonParam.orders[0].main.totalNum,
                salesNum: purRes.params.jsonParam.orders[0].main.totalNum,
                salesMoney: purRes.params.jsonParam.orders[0].main.totalMoney
            }, styleInfoBefore.spu);
            common.isApproximatelyEqualAssert(exp, styleInfoAfter.result.data.spu, ['sessionId', 'ver', 'channelIds']);
        });
        it('卖家销售单详情', async function () {
            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });

            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(2);
        });
        it('买家支付后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await sp.spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['spuDocId', 'spec1', 'spec2', 'spec3']);
            common.isApproximatelyEqualAssert({ flag: 3, frontFlag: 2, payFlag: 1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);
        });
    });

    describe('卖家确认取货', function () {
        let qfRes;
        before(async function () {
            await ssReq.ssSellerLogin();
            //列表skus只取单据sku中的一条数据。。
            const qlRes = await sp.spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
            // console.log(`qlRes=${JSON.stringify(qlRes)}`);
            salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            //查询单据详情
            qfRes = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });
            const details = qfRes.result.data.skus.map((sku) => {
                return { salesDetailId: sku.id, num: sku.skuNum };
            });
            const logisList = await sp.spconfb.findLogisList1({ cap: 1 });
            const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
            await ss.spsales.confirmPickedUp({ saleBillIds: [salesListRes.bill.id] });

        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == qfRes.result.data.bill.billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag).to.equal(4);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: qfRes.result.data.bill.id });//
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag).to.equal(4);
        });
        it('卖家商品详情-库存', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId });
            common.isApproximatelyEqualAssert({ stockNum: common.sub(styleInfoBefore.spu.stockNum, purRes.params.jsonParam.orders[0].main.totalNum) }, styleInfoAfter.result.data.spu);
        });
        it('卖家确认提货后,平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await sp.spTrade.findBillFullByAdmin({ billNo: purRes.result.data.rows[0].billNo, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`spPurInfo=${JSON.stringify(spPurInfo)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus, ['spuDocId', 'spec1', 'spec2', 'spec3']);
            common.isApproximatelyEqualAssert({ flag: 11, frontFlag: 4, payFlag: 1, billNo: purRes.result.data.rows[0].billNo, purBillId: purRes.result.data.rows[0].billId }, spPurInfo.result.data.bill);

        });
        it('买家查询销售单列表', async function () {
            await ssReq.userLoginWithWx();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            const purBillList = await sp.spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 4 });
            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag).to.equal(4);
        });
    });
});