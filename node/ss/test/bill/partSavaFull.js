const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const messageManage = require('../../help/messageManage');
// const employeeManage = require('../../help/employeeManage');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');
const ssConfigParam = require('../../help/configParamManager');
const ssCaps = require('../../../reqHandler/ss/ssCaps');

describe('部分发货', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, purRes, checkPurBillFlag, getFullForBuyer, salesListRes, styleInfoBefore, toBeDeliverNum, salesOverview, rate = 0.006,
        money, sales, spuTitle, logisList, deliverSalesBill;
    before(async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await ssReq.ssSellerLogin();
        const dresList = await ssReq.getDresList(1);

        await ssReq.userLoginWithWx();
        clientInfo = _.cloneDeep(LOGINDATA);
        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = await billManage.mockPurParam(dresList, { count: 3, num: 3 });

        await ssReq.userLoginWithWx();
        purRes = await billManage.createPurBill(purJson);
        // purRes = await sp.spTrade.savePurBill(purJson);
        console.log(`purRes=${JSON.stringify(purRes)}`);
        // 开单失败则结束当前脚本
        if (purRes.result.data.rows[0].isSuccess != 1) {
            console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
            this.parent ? this.parent.skip() : this.skip();
        };
        await ssReq.ssSellerLogin();
    });
    it('卖家首页统计数据', async function () {
        toBeDeliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
        expect(toBeDeliverNum, '获取卖家首页统计数据失败').to.not.be.undefined;
    });
    describe('买家支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await ssReq.userLoginWithWx();
            // console.log(LOGINDATA);
            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: LOGINDATA.wxOpenId,
                orderIds: [purRes.result.data.rows[0].billId],
                payMoney: purRes.params.jsonParam.orders[0].main.money
            });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            await sp.spTrade.receivePayResult({
                mainId: payRes.result.data.payDetailId,
                amount: purRes.params.jsonParam.orders[0].main.money,
                purBillId: purRes.result.data.rows[0].billId
            });
        });
        it('查询销售单列表', async function () {
            await ssReq.ssSellerLogin();
            const salesList = await sp.spTrade.salesFindBills({ orderByDesc: true, orderBy: 'proTime', statusType: 0 }); //purRes.result.data.rows[0].billId
            const salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            expect(salesListRes.bill.payFlag).to.equal(1);
        });
    });
    describe('卖家发货', async function () {
        let unfilledDetailsExp, billInfoExp;
        before('卖家发货', async function () {
            await ssReq.ssSellerLogin();
            const unfilledDetails = await ss.spsales.getUnfilledDetails({ salesBillId: purRes.result.data.rows[0].salesBillId });
            console.log(`\n查询可发货订单明细=${JSON.stringify(unfilledDetails)}`);
            unfilledDetailsExp = _.cloneDeep(unfilledDetails.result.data);
            unfilledDetailsExp.toDoSpus[0].todoDetails = [];
            const details = unfilledDetails.result.data.toDoSpus[0].todoDetails.map((detail, index) => {
                return { salesDetailId: detail.id, num: detail.num - index };
            });
            console.log(details);
            details.forEach(detail => {
                const unfilledDetail = _.cloneDeep(unfilledDetails.result.data.toDoSpus[0].todoDetails.find(obj => obj.id == detail.salesDetailId));
                if (unfilledDetail) {
                    unfilledDetail.deliverNum = detail.num;
                    unfilledDetail.num -= detail.num;
                }
                unfilledDetail.num && (unfilledDetailsExp.toDoSpus[0].todoDetails.push(unfilledDetail));
            });
            logisList = await sp.spconfb.findLogisList1({ cap: 1 });
            const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
            await common.delay(1000);
            deliverSalesBill = await sp.spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[randonNum].id,
                    logisCompName: logisList.result.data.rows[randonNum].name,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: clientInfo.tenantId,
                    // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            });
            console.log(`deliverSalesBill=${JSON.stringify(deliverSalesBill)}`);

            billInfoExp = {
                deliverId: deliverSalesBill.result.data.billId,
                deliverNum: details.map(detail => detail.num).reduce((a, b) => common.add(a, b), 0),
                unDeliverNum: unfilledDetailsExp.toDoSpus[0].todoDetails.map(detail => detail.num).reduce((a, b) => common.add(a, b), 0),
                flag: 81,
                frontFlag: 2,
                frontFlagName: "部分发货"
            }
        });
        it('查询可发货订单明细', async function () {
            await ssReq.ssSellerLogin();
            const unfilledDetailsAfter = await ss.spsales.getUnfilledDetails({ salesBillId: purRes.result.data.rows[0].salesBillId });
            console.log(`\n查询可发货订单明细=${JSON.stringify(unfilledDetailsAfter.result.data)}`);
            console.log(`\nunfilledDetailsExp=${JSON.stringify(unfilledDetailsExp)}`);
            common.isApproximatelyArrayAssert(unfilledDetailsExp.toDoSpus, unfilledDetailsAfter.result.data.toDoSpus, ['updatedBy', 'updatedDate']);
        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({ orderByDesc: true, orderBy: 'proTime', statusType: 0 });
            const salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            common.isApproximatelyArrayAssert(billInfoExp, salesListRes.bill);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId });
            console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            common.isApproximatelyArrayAssert(billInfoExp, salesBillInfo.result.data.bill);
        });
        it('卖家首页统计待发货数', async function () {
            if (!toBeDeliverNum) this.skip();
            await ssReq.ssSellerLogin();
            const deliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
            expect(deliverNum, '卖家首页统计待发货数应=最开始+1').to.equal(toBeDeliverNum + 1);
        });
        it('买家采购单列表', async function () {
            await ssReq.userLoginWithWx();
            const purBillList = await sp.spTrade.purFindBills({ orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            console.log(`\n 买家采购单列表=${JSON.stringify(purBillInfo)}`);
            common.isApproximatelyArrayAssert(billInfoExp, purBillInfo.bill);
        });
        it('买家采购单详情', async function () {
            purInfo = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId });
            console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);
            common.isApproximatelyArrayAssert(billInfoExp, purInfo.result.data.bill);
        })
    });
});