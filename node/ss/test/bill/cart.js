const basicJson = require('../../help/basicJson');
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const billManage = require('../../help/billManage');
const cartManage = require('../../help/cartManage');
const dresManage = require('../../help/dresManage');
const ssAccount = require('../../data/ssAccount');

describe.skip('购物车模块', function () {
    this.timeout(TESTCASE.timeout);
    const dres = dresManage.setupDres();
    let sellerInfo;
    before('卖家登录', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('购物车操作', async function () {
        let cartJson, shoppingCart;
        before(async function () {
            await ssReq.userLoginWithWx();
            shoppingCart = cartManage.setupShoppingCart(sellerInfo);
            const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList.result.data.rows[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);

            await shoppingCart.emptyCart();
            cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 2, num: 3 });
            // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
            await shoppingCart.saveCartInBatchs(cartJson);
            // console.log(shoppingCart);
        });
        it('查询购物车', async function () {
            this.retries(2);
            await common.delay(1000);
            await shoppingCart.cartListAssert();
        });
        it('根据skuIds批量查询卖家sku的实时信息', async function () {
            await shoppingCart.skusInfoInBatchAssert();
        });

        describe('重复添加购物车', async function () {
            before(async function () {
                await shoppingCart.saveCartInBatchs(cartJson);
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert();
            });
        });

        describe('更新购物车', async function () {
            before(async function () {
                await shoppingCart.updateShoppingCart(cartJson.spu.spuId, cartJson.carts[0].skuId, { skuNum: 5 });//, rem: common.getRandomStr(5) 
                // console.log(shoppingCart);
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert();
            });
        });

        describe('购物车按spu备注', async function () {
            before(async function () {
                await shoppingCart.updateSpuRem(cartJson.spu.spuId, common.getRandomStr(5));
                // console.log(shoppingCart);
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert();
            });
        });

        describe('删除购物车', async function () {
            before(async function () {
                // console.log(shoppingCart);
                await shoppingCart.deleteCart(cartJson.spu.spuId, cartJson.carts[0].skuId);
                // console.log(shoppingCart);
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert();
            });
        });

        describe('批量删除购物车', async function () {
            before(async function () {
                // console.log(shoppingCart);
                await shoppingCart.saveCartInBatchs(cartJson);
                await shoppingCart.deleteCartInBatch();
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert();
            });
        });

        describe.skip('买家切换appId', async function () {
            let cartJson, shoppingCartB;
            before('买家切换appId', async function () {
                await ssReq.userLoginWithWx();
                console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
                await ssReq.userLoginWithWx({ appId: 'wx95c02f84bbc6fbae' });
                console.log(`\nLOGINDATA=${JSON.stringify(LOGINDATA)}`);
                shoppingCartB = cartManage.setupShoppingCart(sellerInfo);
                const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList.result.data.rows[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                await shoppingCartB.emptyCart();
                cartJson = shoppingCartB.makeCartJson(dresFull, { type: 2, count: 2, num: 3 });
                // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
                await shoppingCartB.saveCartInBatchs(cartJson);
                // console.log(shoppingCartB);
            });

            it('查询购物车B', async function () {
                await shoppingCartB.cartListAssert();
            });
            //等待服务端实现saas隔离后方法
            it('查询购物车', async function () {
                await ssReq.userLoginWithWx();
                await shoppingCart.cartListAssert();
            });
        });
    });

    describe('更新商品信息', async function () {
        let cartJson, shoppingCart, clientInfo, mobile = `${common.getRandomMobile()}`;
        before(async function () {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            clientInfo = _.cloneDeep(LOGINDATA);
            shoppingCart = cartManage.setupShoppingCart(sellerInfo);
            const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList.result.data.rows[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            await shoppingCart.emptyCart();
            cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 2, num: 3 });
            // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
            await shoppingCart.saveCartInBatchs(cartJson);
            // console.log(shoppingCart);
        });
        it('查询购物车', async function () {
            await shoppingCart.cartListAssert();
        });

        describe('修改商品详情', async function () {
            let updateSpuJson;
            before(async function () {
                await ssReq.ssSellerLogin();
                updateSpuJson = { id: cartJson.spu.spuId, title: `商品${common.getRandomStr(5)}`, pubPrice: common.getRandomNum(100, 1000) };
                await sp.spdresb.updateSpuForSeller(updateSpuJson);
                // await sp.spdresb.getFullById({ id: updateSpuJson.id }).then(res => res.result.data);
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
                shoppingCart.correctSpu({ spuId: updateSpuJson.id, title: updateSpuJson.title, skuPrice: updateSpuJson.pubPrice });
                // console.log(shoppingCart);
            });
            it('查询购物车', async function () {
                // this.retries(2);
                // await common.delay(1000);
                await shoppingCart.cartListAssert(['title']);
            });
            it('根据skuIds批量查询卖家sku的实时信息', async function () {
                await shoppingCart.skusInfoInBatchAssert();
            });
        });

        describe('商品上下架', async function () {
            before('下架', async function () {
                await ssReq.ssSellerLogin();
                await sp.spdresb.offMarket({ ids: [cartJson.spu.spuId] });
                shoppingCart.correctSpu({ spuId: cartJson.spu.spuId, spuFlag: -4 }); //-4卖家下架,flag: 0, 

                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert(['title']);
            });
            it('根据skuIds批量查询卖家sku的实时信息', async function () {
                await shoppingCart.skusInfoInBatchAssert();
            });
            describe('上架', async function () {
                before(async function () {
                    await ssReq.ssSellerLogin();
                    await sp.spdresb.onMarket({ ids: [cartJson.spu.spuId] });
                    shoppingCart.correctSpu({ spuId: cartJson.spu.spuId, spuFlag: 1 }); //-4卖家下架,flag: 0, 
                    await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
                });
                it('查询购物车', async function () {
                    await shoppingCart.cartListAssert(['title']);
                });
                it('根据skuIds批量查询卖家sku的实时信息', async function () {
                    await shoppingCart.skusInfoInBatchAssert();
                });
            });
        });

        describe('修改客户适用价格', async function () {
            before(async function () {
                //修改为非默认价
                await ssReq.ssSellerLogin();
                const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
                // console.log(`\n priceList=${JSON.stringify(priceList)}`);
                let flag = true;
                for (const price of priceList) {
                    if (price.codeValue != 0) {
                        flag = false;
                        await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: price.codeValue })
                        const dresFull = await sp.spdresb.getFullById({ id: cartJson.spu.spuId }).then(res => res.result.data.spu);
                        if (!dresFull[`price${price.codeValue}`]) throw new Error(`商品${cartJson.spu.spuId}不存在:price${price.codeValue}\n商品:${JSON.stringify(dresFull)}`);
                        shoppingCart.correctSpu({ spuId: cartJson.spu.spuId, skuPrice: dresFull[`price${price.codeValue}`] });
                        break;
                    }
                };
                if (flag) throw new Error(`当前店铺不存在codeValue不为0的价格类型:\n${JSON.stringify(priceList)}`);
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            });
            it('查询购物车', async function () {
                await shoppingCart.cartListAssert(['title']);
            });
        });

    });

    describe('购物车下单', function () {
        let cartJson, shoppingCart;
        before('加入购物车', async function () {
            await ssReq.userLoginWithWx();
            shoppingCart = cartManage.setupShoppingCart(sellerInfo);
            const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            await shoppingCart.emptyCart();
            for (const dres of dresList.slice(0, 2)) {
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dres.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                const cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 2, num: 3 });
                // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
                await shoppingCart.saveCartInBatchs(cartJson);
            };
        });
        it('查询购物车', async function () {
            await shoppingCart.cartListAssert();
        });
        describe('下单', async function () {
            let savePurBill;
            before(async function () {
                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = billManage.mockPurParam(shoppingCart);
                // console.log(`purJson=${JSON.stringify(purJson)}`);
                savePurBill = await billManage.createPurBill(purJson);
            });
            it('开单后查询待付款列表', async function () {
                // console.log(`\n savePurBill=${JSON.stringify(savePurBill)}`);
                const purBillList = await sp.spTrade.purFindBills({ searchToken: savePurBill.result.data.rows[0].billNo, statusType: 1 }).then(res => res.result.data.rows);
                const purBill = purBillList.find(ele => ele.bill.billNo == savePurBill.result.data.rows[0].billNo);
                // console.log(`\n purBill=${JSON.stringify(purBill)}`);
                expect(purBill, '待付款订单未找到该订单').not.to.be.undefined;
            });

            describe('再来一单', async function () {
                before('再来一单', async function () {
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: savePurBill.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    await shoppingCart.cartListAssertBySkuId();
                });
            });
        });
    });
});
