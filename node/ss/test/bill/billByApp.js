
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const messageManage = require('../../help/messageManage');
// const employeeManage = require('../../help/employeeManage');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');
const dresManage = require('../../help/dresManage');

// TODO 流程整理
describe('泛行业订单流程', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, purRes, checkPurBillFlag, dresDetail, salesListRes, styleInfoBefore, toBeDeliverNum, salesOverview, rate = 0.006,
        money, sales, spuTitle, logisList, deliverSalesBill;

    const dres = dresManage.setupDres({ type: 'app' });

    before(async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        await dresManage.setupSpec();
        await dresManage.prePrepare();
        //保存商品
        const json = basicJson.wideStyleJsonByApp();
        await dres.saveDresByApp(json);
    });
    it('买家和店员关联', async function () {
        // 买家登录
        await ssReq.userLoginWithWx();
        let clientInfo = _.cloneDeep(LOGINDATA);


        // 查询买家信息 是否关联
        await ssReq.ssSellerLogin();
        let member = await ss.spmdm.getMemberByUserId({
            userId: clientInfo.userId
        });
        // console.log(`\n member=${JSON.stringify(member)}`);

        if (member.result.data.sellerId == '' || member.result.data.sellerId == '0') {
            await ssReq.userLoginWithWx();
            await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId, sellerId: sellerInfo.userId });
        } else if (member.result.data.sellerId != sellerInfo.userId) {
            await ss.spmdm.deliverStaff({
                deliverStaff: member.result.data.sellerId,
                userId: sellerInfo.userId
            })
        };
    });
    it('查询销量汇总和概览', async function () {
        await ssReq.ssSellerLogin();
        sales = await ss.spsales.getSalesForEmployee({
            userId: sellerInfo.userId
        }).then(res => res.result.data);
        salesOverview = await ss.spsales.getSalesOverviewForEmployee({
            userId: sellerInfo.userId
        }).then(res => res.result.data);
        console.log(`原来的salesOverview：${salesOverview}`);

        delete sales.rows;
        delete salesOverview.rows;
    });
    it('卖家首页统计数据', async function () {
        toBeDeliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
        expect(toBeDeliverNum, '获取卖家首页统计数据失败').to.not.be.undefined;
    });

    describe('买家下单', function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            const getListDresSpu = await ss.spchb.getDresSpuList({
                tenantId: sellerInfo.tenantId,
                orderBy: 'marketDate',
                orderByDesc: true
            });
            const spuId = getListDresSpu.result.data.rows[0].id;
            spuTitle = getListDresSpu.result.data.rows[0].title;
            dresDetail = await sp.spdresb.getFullForBuyer({
                spuId: spuId,
                _tid: sellerInfo.tenantId,
                _cid: sellerInfo.clusterCode
            });
            console.log(`商品详情=${JSON.stringify(dresDetail)}`);

            await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({
                styleInfo: dresDetail
            });

            await ssReq.ssSellerLogin();
            await common.delay(500);
            //卖家商品详情
            styleInfoBefore = await sp.spdresb.getFullById({
                id: purJson.orders[0].details[0].spuId
            });
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore.result.data)}`);

            await ssReq.userLoginWithWx();

            purRes = await billManage.createPurBill(purJson);
            // purRes = await sp.spTrade.savePurBill(purJson);
            console.log(`purRes=${JSON.stringify(purRes)}`);
            money = parseFloat(common.mul(purRes.params.jsonParam.orders[0].main.totalMoney, (1 - rate)).toFixed(2));
            console.log(money);
            // 开单失败则结束当前脚本
            if (purRes.result.data.rows[0].isSuccess != 1) {
                console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
                this.parent ? this.parent.skip() : this.skip();
            };

            await ssReq.ssSellerLogin();
            await common.delay(500);
        });

        it('查询消息列表', async function () {
            await messageManage.messageCenter({
                billNo: purRes.result.data.rows[0].billNo,
                tagOneIn: '71',
                text: '您收到了新的订单',
                title: '新订单提醒'
            });
        });
        it('查询销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`销售单列表:${JSON.stringify(salesList)}`);
            expect(salesListRes.bill.payFlag).to.equal(0);
        });
        it('查询单据详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            }); //
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({
                id: purRes.params.jsonParam.orders[0].details[0].spuId
            });
            // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter.result.data)}`);
            common.isApproximatelyEqualAssert(styleInfoBefore.result.data, styleInfoAfter.result.data, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'occupyNum', 'stockNum']);
        });
        it('卖家首页统计待发货数', async function () {
            if (!toBeDeliverNum) this.skip();
            const deliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
            expect(deliverNum, '卖家首页统计待发货数变了，应不变').to.equal(toBeDeliverNum);
        });
        it.skip('查询员工关联客户人数本门店销售统计-未变化', async function () {
            let salesBill = await ss.spsales.getSalesForEmployee({
                userId: sellerInfo.userId,
                pageSize: 0
            }).then(res => res.result.data);
            delete salesBill.rows;
            common.isApproximatelyEqualAssert(sales, salesBill);
        });
        it.skip('员工关联客户人数本门店销售统计概览-未变化', async function () {
            let salesOverviewBill = await ss.spsales.getSalesOverviewForEmployee({
                userId: sellerInfo.userId
            }).then(res => res.result.data);
            delete salesOverviewBill.rows;
            common.isApproximatelyEqualAssert(salesOverview, salesOverviewBill);
        });
        it('买家采购单详情', async function () {
            await ssReq.userLoginWithWx();
            const purInfo = await sp.spTrade.findPurBillFull({
                id: purRes.result.data.rows[0].billId
            });
            console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam, purInfo.result.data);
            expect(purInfo.result.data.bill, '单据信息错误').to.includes({
                flag: 3,
                frontFlag: 1,
                payFlag: 0,
            });
            expect(purInfo.result.data.bill, `未返回 待支付倒计时 字段`).to.satisfy((bill) => bill.unpaidCountDown && bill.unpaidCountDown.payDeadline && bill.unpaidCountDown.serverTime);
        })
    });

    describe('买家支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await ssReq.userLoginWithWx();
            // console.log(LOGINDATA);
            // {"payType":5,"payMethod":2,"hashKey":"20190709103046468961","payerOpenId":"oJM315T183V7yCCzQsuGYKTDyDYg","payMoney":585,"orderIds":[209019]}
            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: LOGINDATA.wxOpenId,
                orderIds: [purRes.result.data.rows[0].billId],
                payMoney: purRes.params.jsonParam.orders[0].main.money
            });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            await sp.spTrade.receivePayResult({
                mainId: payRes.result.data.payDetailId,
                amount: payRes.params.jsonParam.payMoney,
                purBillId: purRes.result.data.rows[0].billId
            });
            // console.log(`\n支付回调:${JSON.stringify(res)}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
        });

        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();
            await messageManage.messageCenter({
                billNo: purRes.result.data.rows[0].billNo,
                tagOneIn: 72,
                text: '买家已经付款',
                title: '买家已付款'
            });
        });

        it('卖家销售单列表', async function () {
            await common.delay(1000);
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(1); //已付款
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(2); //未发货
        });
        it('卖家商品详情-销售数量和销售金额', async function () {
            this.retries(2);
            await common.delay(500);
            // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
            let styleInfoAfter = await sp.spdresb.getFullById({
                id: purRes.params.jsonParam.orders[0].details[0].spuId
            });
            let exp = common.addObject({
                stockNum: -purRes.params.jsonParam.orders[0].main.totalNum,
                salesNum: purRes.params.jsonParam.orders[0].main.totalNum,
                salesMoney: purRes.params.jsonParam.orders[0].main.totalMoney
            }, styleInfoBefore.result.data.spu);
            common.isApproximatelyEqualAssert(exp, styleInfoAfter.result.data.spu, ['sessionId', 'ver', 'channelIds', 'updatedDate']);
        });
        it('卖家销售单详情', async function () {
            salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            });
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(2);
        });
        it('买家支付后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await sp.spTrade.findBillFullByAdmin({
                billNo: purRes.result.data.rows[0].billNo,
                _tid: sellerInfo.tenantId,
                _cid: sellerInfo.clusterCode
            });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({
                flag: 3,
                frontFlag: 2,
                payFlag: 1,
                billNo: purRes.result.data.rows[0].billNo,
                purBillId: purRes.result.data.rows[0].billId
            }, spPurInfo.result.data.bill);
        });
        it('卖家首页统计待发货数', async function () {
            if (!toBeDeliverNum) this.skip();
            await ssReq.ssSellerLogin();
            const deliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
            expect(deliverNum, '卖家首页统计待发货数应加一').to.equal(toBeDeliverNum + 1);
        });
        it('查询员工关联客户人数本门店销售统计', async function () {
            // if (sales.total != 20) {
            //     sales.total = Number(sales.total) + 1;
            //     sales.count = Number(sales.count) + 1;
            // };
            sales.total = Number(sales.total) + 1;
            sales.count = Number(sales.count) + 1;
            let salesPay = await ss.spsales.getSalesForEmployee({
                userId: sellerInfo.userId
            }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, salesPay.rows[0]);
            common.isApproximatelyEqualAssert(sales, salesPay);
        });
        it('员工关联客户人数本门店销售统计概览', async function () {
            if (!salesOverview.totalMoney) this.skip();
            console.log(salesOverview.totalMoney, payRes.params.jsonParam.payMoney);
            salesOverview.totalMoney = common.add(salesOverview.totalMoney, payRes.params.jsonParam.payMoney);
            salesOverview.money = common.add(salesOverview.money, payRes.params.jsonParam.payMoney);
            salesOverview.totalNum = common.add(salesOverview.totalNum, purRes.params.jsonParam.orders[0].main.totalNum);
            let salesOverviewPay = await ss.spsales.getSalesOverviewForEmployee({
                userId: sellerInfo.userId
            }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(salesOverview, salesOverviewPay);
        });
    });

    describe('卖家发货', function () {
        let qfRes;
        before(async function () {
            await ssReq.ssSellerLogin();
            //列表skus只取单据sku中的一条数据。。
            const qlRes = await sp.spTrade.salesFindBills({
                pageSize: 10,
                pageNo: 1,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 2
            });
            // console.log(`qlRes=${JSON.stringify(qlRes)}`);
            salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            //查询单据详情
            qfRes = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            });
            const details = qfRes.result.data.skus.map((sku) => {
                return {
                    salesDetailId: sku.id,
                    num: sku.skuNum
                };
            });
            logisList = await sp.spconfb.findLogisList1({
                cap: 1
            });
            const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
            deliverSalesBill = await sp.spTrade.deliverSalesBill({
                main: {
                    logisCompId: logisList.result.data.rows[randonNum].id,
                    logisCompName: logisList.result.data.rows[randonNum].name,
                    waybillNo: common.getRandomNumStr(12),
                    buyerId: qlRes.result.data.rows[0].bill.buyerId,
                    // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                    hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                },
                details: details
            }).then(res => res.result.data.billId);
            console.log(`deliverSalesBill=${JSON.stringify(deliverSalesBill)}`);

        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == qfRes.result.data.bill.billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(3);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: qfRes.result.data.bill.id
            }); //
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(3);
        });
        it('卖家商品详情-库存', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({
                id: purRes.params.jsonParam.orders[0].details[0].spuId
            });
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // console.log(`purRes=${JSON.stringify(purRes)}`);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            common.isApproximatelyEqualAssert({
                stockNum: common.sub(styleInfoBefore.result.data.spu.stockNum, purRes.params.jsonParam.orders[0].main.totalNum)
            }, styleInfoAfter.result.data.spu);
        });
        it('卖家发货后平台端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await sp.spTrade.findBillFullByAdmin({
                billNo: purRes.result.data.rows[0].billNo,
                _tid: sellerInfo.tenantId,
                _cid: sellerInfo.clusterCode
            });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({
                flag: 6,
                frontFlag: 3,
                payFlag: 1,
                billNo: purRes.result.data.rows[0].billNo,
                purBillId: purRes.result.data.rows[0].billId
            }, spPurInfo.result.data.bill);
        });
        it('卖家首页统计待发货数', async function () {
            if (!toBeDeliverNum) this.skip();
            await ssReq.ssSellerLogin();
            const deliverNum = await ss.spsales.findStatistic().then(res => res.result.data.toBeDeliverNum);
            expect(deliverNum, '卖家首页统计待发货数应与最开始一致').to.equal(toBeDeliverNum);
        });
    });

    describe('买家收货', function () {
        let purInfo;
        before(async function () {
            await ssReq.userLoginWithWx();
            // await common.delay(3000);
            //买家确认收货
            await sp.spTrade.confirmReceipt({
                purBillIds: [purRes.result.data.rows[0].billId]
            });
            // console.log(`确认收货：=${JSON.stringify(res)}`);
        });
        it('确认收货后-延迟收货', async function () {
            const res = await sp.spTrade.extendedReturn({
                id: purRes.result.data.rows[0].billId,
                check: false
            });
            expect(res.result).to.includes({
                msgId: 'spbillflag_after_confirm'
            });
        });
        it('根据货品查询订单', async function () {
            const purBillList = await sp.spTrade.purFindBills({
                searchToken: spuTitle,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 4
            });
            console.log(`\n purBillList.result.data.rows=${JSON.stringify(purBillList)}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            console.log(`\n putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('买家采购单列表', async function () {
            const purBillList = await sp.spTrade.purFindBills({
                pageSize: 20,
                pageNo: 1,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 4
            });
            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('买家采购单详情', async function () {
            purInfo = await sp.spTrade.findPurBillFull({
                id: purRes.result.data.rows[0].billId
            });
            console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);

            common.isApproximatelyEqualAssert(purRes.params.jsonParam, purInfo.result.data);
            expect(purInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(4);
        })
        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();
            await messageManage.messageCenter({
                billNo: purRes.result.data.rows[0].billNo,
                tagOneIn: 77,
                text: '买家已经确认收货',
                title: '收货提醒'
            });
        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            }); //
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('买家收货后管理端查看订单详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await sp.spTrade.findBillFullByAdmin({
                billNo: purRes.result.data.rows[0].billNo,
                _tid: sellerInfo.tenantId,
                _cid: sellerInfo.clusterCode
            });
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, spPurInfo.result.data.bill);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details, spPurInfo.result.data.skus);
            common.isApproximatelyEqualAssert({
                flag: 8,
                frontFlag: 4,
                payFlag: 1,
                billNo: purRes.result.data.rows[0].billNo,
                purBillId: purRes.result.data.rows[0].billId
            }, spPurInfo.result.data.bill);
        });
        it.skip('快速补货列表', async function () {
            await ssReq.userLoginWithWx();
            const recentBillList = await ss.sppur.findRecentBillSpu({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            console.log(`\n recentBillList=${JSON.stringify(recentBillList)}`);
            const recentBill = purInfo.result.data.skus.map(sku => {
                const detailBill = recentBillList.find(obj => obj.purDetailId == sku.purDetailId);
                if (detailBill) {
                    console.log(detailBill);
                    dresDetail.result.data.spu.name = dresDetail.result.data.spu.title;//recentBill的name属性为dres的title
                    dresDetail.result.data.spu.pubPrice = sku.skuPrice; // 泛行业sku价格不同 添加修改
                    common.isApproximatelyEqualAssert(dresDetail.result.data.spu, detailBill, ['unitId', 'marketDays']);
                    expect(detailBill).to.include({ id: sku.spuId, purDetailId: sku.purDetailId });
                    return detailBill;
                };
            });
            expect(recentBill, '补货列表中不存在本次下单的货品').not.to.be.undefined;
        });
    });

});
