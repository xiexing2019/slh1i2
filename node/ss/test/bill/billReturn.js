const spTrade = require('../../../reqHandler/sp/biz_server/spTrade');
const basicJson = require('../../help/basicJson');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const spugr = require('../../../reqHandler/sp/global/spugr');
const common = require('../../../lib/common');
const format = require('../../../data/format');
const spconfb = require('../../../reqHandler/sp/biz_server/spconfb');
const spchb = require('../../../reqHandler/ss/spb/spchb');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const billManage = require('../../help/billManage');

describe('退款退货-offline', function () {
    this.timeout(TESTCASE.timeout);
    let purBillInfo, returnBill, payRes, returnBillSeller, saveDeliverRes, returnBillDate, arr, receiveInfo, receivePayResult, payResult, sales, salesOverview, purRes, selesCopy;

    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        const spuId = getListDresSpu.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // purRes = await spTrade.savePurBill(purJson);
        purRes = await billManage.createPurBill(purJson);
        console.log(`创建采购订单:${JSON.stringify(purRes)}`);
        payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money, payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, });
        // payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        console.log(`\n创建支付=${JSON.stringify(payRes)}`);
        payResult = await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
        console.log(`\n支付回调=${JSON.stringify(payResult)}`);
        await common.delay(1000);
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        let i = 1, purBillList;
        while (!purBillInfo) {
            if (i > 10) throw new Error(`采购单列表为undefined已超过10次`);
            purBillList = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
            purBillInfo = purBillList.result.data.rows.find(val => val.bill.billNo == purRes.result.data.rows[0].billNo);
            await common.delay(500);
            console.log(`\n 第${i}次查询列表`);
            i++;
        };

        expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billNo}的订单`).to.not.be.undefined;

        // 卖家发货
        await ssReq.ssSellerLogin();
        //列表skus只取单据sku中的一条数据。。
        const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        // console.log(`qlRes=${JSON.stringify(qlRes)}`);
        const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
        //查询单据详情
        qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
        const details = qfRes.result.data.skus.map((sku) => {
            return { salesDetailId: sku.id, num: sku.skuNum };
        });
        const logisList = await spconfb.findLogisList1({ cap: 1 });
        const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
        await spTrade.deliverSalesBill({
            main: {
                logisCompId: logisList.result.data.rows[randonNum].id,
                logisCompName: logisList.result.data.rows[randonNum].name,
                waybillNo: common.getRandomNumStr(12),
                buyerId: qlRes.result.data.rows[0].bill.buyerId,
                // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
            },
            details: details
        });
        await ssReq.userLoginWithWx();
        const purBillList1 = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 3 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        // console.log(`purBillList1=${JSON.stringify(purBillList1.result.data.rows)}`);
        purBillInfo = purBillList1.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
        expect(purBillInfo, `待收货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;
        //买家确认收货
        // await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
        // 开单失败则结束当前脚本
        if (typeof (purBillInfo) == 'undefined') {
            console.warn(`待收货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别2005）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
    });
    it('买家和店员关联', async function () {
        // 买家登录
        await ssReq.userLoginWithWx();
        let clientInfo = _.cloneDeep(LOGINDATA);
        // 查询买家信息 是否关联
        await ssReq.ssSellerLogin();
        let member = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId });
        if (member.result.data.sellerId == '' || member.result.data.sellerId == '0') {
            await ssReq.userLoginWithWx();
            await ss.spmdm.saveMemberInfo({ tenantId: sellerInfo.tenantId, sellerId: sellerInfo.userId });
        } else if (member.result.data.sellerId != sellerInfo.userId) {
            await ss.spmdm.deliverStaff({ deliverStaff: member.result.data.sellerId, userId: sellerInfo.userId })
        };
        // 查询销量汇总和概览
        await ssReq.ssSellerLogin();
        sales = await ss.spsales.getSalesForEmployee({ userId: sellerInfo.userId, pageSize: 0 }).then(res => res.result.data);
        salesOverview = await ss.spsales.getSalesOverviewForEmployee({ userId: sellerInfo.userId }).then(res => res.result.data);
        console.log(salesOverview);

        selesCopy = _.cloneDeep(sales);
        delete sales.rows;
        delete salesOverview.rows;
    });
    describe('买家创建退款退货申请单-退货退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            const returnBillJson = await basicJson.returnBillJson2({ purBillInfo });
            returnBill = await spTrade.saveReturnBill(returnBillJson);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            // console.log(`returnBill=${JSON.stringify(returnBill)}`);
            console.log(`\n退款退货申请单:${JSON.stringify(returnBill)}`);
            await ssReq.ssSellerLogin();
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
        });

        it('买家查看采购订单的售后列表', async function () {
            await ssReq.userLoginWithWx();
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            const exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1 });  //要查发起申请后的预期结果，所以typeId传6.
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            const exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, returnBillSeller, ['id']);
        });
        it('卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            const exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1
            });
            console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['billId', 'id']);
        });
    });

    describe('卖家审核退货申请', function () {
        before(async function () {
            await ssReq.ssSellerLogin();
            //卖家审核
            const checkReturnRes = await spTrade.checkReturnBill({
                id: returnBillSeller.bill.id,
                checkResult: 1, //同意
            });
            // console.log(`checkReturnRes=${JSON.stringify(checkReturnRes)}`);
            // arr = checkReturnRes.result.data.payWebUrl.split('=')[1].split('_');
            // console.log(arr);
            // receivePayResult = await ss.spsales.returnReceivePayResult({
            //     bizType: parseInt(arr[0]),
            //     mainId: parseInt(arr[1])
            // });
            // console.log(`receivePayResult=${JSON.stringify(receivePayResult)}`);

        });
        it('审核后卖家查看退货列表', async function () {
            const qlRes = await spTrade.findSalesReturnBills({ searchToken: returnBillSeller.bill.id });
            const exp = getSellerListExp({ purBillInfo, returnBill, flag: 4, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id']);
        })
        // 线上卖家钱包余额不足加-offline
        it('审核后卖家查看退货详情-offline', async function () {
            //单据状态，-1 已删除，0 已取消，1 正常（不使用）。添加状态：-2 彻底删除，2 处理中（暂不使用），3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            const exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1,
            });
            // console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'billId']);
        });
        // 线上卖家钱包余额不足加-offline
        it('发货前买家查看售后列表-offline', async function () {
            await ssReq.userLoginWithWx();
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1
            });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });
        it.skip('卖家审核后平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await common.delay(1000);
            await ss.spAuth.staffLogin();
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
            // console.log(`spReturnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家审核后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 4, returnType: 1 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
        });
        it.skip('卖家审核后平台端查看退货详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家审核spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1, receiveInfo
            });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data);
        });
    });

    // 线上卖家钱包余额不足,审核通过不了加-offline
    describe('买家发货-offline', async function () {
        before('买家发货', async function () {
            await ssReq.userLoginWithWx();
            let logisList = await spconfb.findLogisList1({ cap: 1 });
            saveDeliverRes = await spTrade.saveReturnDeliver({
                id: returnBill.result.data.val,
                logisCompId: logisList.result.data.rows[0].id,
                logisCompName: logisList.result.data.rows[0].name,
                waybillNo: common.getRandomNumStr(10),
                deliverRem: '退货'
            });
            await common.delay(500);
        });

        it('发货后买家查看售后列表', async function () {
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({
                purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 4, returnType: 1
            });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            const qlRes = await spTrade.findSalesReturnBills({ searchToken: returnBillSeller.bill.id });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 5, returnType: 1, saveDeliverRes });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id']);
        });

        it('查看退货详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 4, returnType: 1, saveDeliverRes
            });
            // console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'billId']);
        });
        it.skip('买家发货后平台端退货列表查询', async function () {
            this.retries(2);//平台端退货列表更新flag慢
            await common.delay(500);
            //管理员登录
            await ss.spAuth.staffLogin();
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
            // console.log(`买家发货后平台端退货列表查询spRet/urnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `买家发货后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 5, returnType: 1, saveDeliverRes });
            // console.log(`\nspReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo);
        });
        it('查询员工关联客户人数本门店销售统计-未变化', async function () {
            await ssReq.ssSellerLogin();
            let salesRes = await ss.spsales.getSalesForEmployee({ userId: sellerInfo.userId }).then(res => res.result.data);
            // console.log(purRes.params.jsonParam.orders[0].main, salesRes.rows[0]);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].main, salesRes.rows[0]);
            common.isApproximatelyEqualAssert(sales, salesRes);
        });
        it('员工关联客户人数本门店销售统计概览-未变化', async function () {
            await ssReq.ssSellerLogin();
            let salesOverviewRes = await ss.spsales.getSalesOverviewForEmployee({ userId: sellerInfo.userId }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(salesOverview, salesOverviewRes);
        });
    });
    // 线上卖家钱包余额不足,审核通过不了加-offline
    describe('卖家确认收货-offline', async function () {
        before('卖家确认收货', async function () {
            await ssReq.ssSellerLogin();
            const confirmReceiptRes = await spTrade.confirmReturnBillReceipt({ id: returnBillSeller.bill.id });
        });

        it('确认收货后查看退货列表', async function () {
            await common.delay(1500);
            const qlRes = await spTrade.findSalesReturnBills({ searchToken: returnBillSeller.bill.id });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 6, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id']);
        });

        it('确认收货后查看退货详情', async function () {
            this.retries(5);
            await common.delay(1000);
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            // let exp = getSellerExp({
            //     purBillInfo, saveReturnBillRes: returnBill, typeId: 5, returnType: 1, saveDeliverRes
            // });
            let exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 1, saveDeliverRes
            });
            exp.bill.frontFlagName = '退货退款完成';
            exp.flows[5].newFlagName = '退货退款完成';
            console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'billId']);
        });

        it('买家登录查看售后流程', async function () {
            await ssReq.userLoginWithWx();;
            const purReturnListInfo = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id }).then(res => res.result.data.rows.find(obj => obj.bill.id == returnBill.result.data.val));
            // console.log(`purReturnListInfo=${JSON.stringify(purReturnListInfo)}`);
            expect(purReturnListInfo, `买家登录查看售后流程查不到单据`).not.to.be.undefined;
            let exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, saveDeliverRes, typeId: 5, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, purReturnListInfo);
        });

        it.skip('卖家确认收货后平台端退货列表查询', async function () {
            //管理员登录
            this.retries(2);//平台端退货列表更新flag慢
            await common.delay(1500);
            await ss.spAuth.staffLogin();
            let spReturnListInfo = await spTrade.getReturnBillListByAdmin({ pageSize: 10, pageNo: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate(), keyWords: { billNo: [purBillInfo.bill.billNo] } }).then(res => res.result.data.rows.find(obj => obj.id == returnBill.result.data.val));
            // console.log(`卖家确认收货后spReturnListInfo=${JSON.stringify(spReturnListInfo)}`);
            expect(spReturnListInfo, `卖家确认收货后平台端退货列表查询不到退货的单据`).not.to.be.undefined;
            let spReturnListExp = adminReturnListExp({ returnBill, purBillInfo, flag: 6, returnType: 1 });
            // console.log(`spReturnListExp=${JSON.stringify(spReturnListExp)}`);
            common.isApproximatelyEqualAssert(spReturnListExp, spReturnListInfo, ['flag']);
        });
        it.skip('卖家确认收货后平台端查看退货详情', async function () {
            //管理员登录
            await ss.spAuth.staffLogin();
            let spPurInfo = await spTrade.getReturnBillDetailByAdmin({ id: returnBill.result.data.val, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`卖家确认收货后spPurInfo=${JSON.stringify(spPurInfo)}`);
            let sellerExp = adminReturnInfoExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 3, returnType: 1, saveDeliverRes, receiveInfo
            });
            // console.log(`sellerExp=${JSON.stringify(sellerExp)}`);
            common.isApproximatelyEqualAssert(sellerExp, spPurInfo.result.data);
        });
        it('查询员工关联客户人数本门店销售统计', async function () {
            this.retries(5);
            await ssReq.ssSellerLogin();
            let salesBefore = _.cloneDeep(sales);
            // if (salesBefore.total != 20) {
            //     salesBefore.total = Number(salesBefore.total) - 1;
            //     salesBefore.count = Number(salesBefore.count) - 1;
            // };
            salesBefore.total = Number(salesBefore.total) - 1;
            salesBefore.count = Number(salesBefore.count) - 1;
            await common.delay(2000);
            let salesRes = await ss.spsales.getSalesForEmployee({ userId: sellerInfo.userId, pageNo: 1, pageSize: 20, }).then(res => res.result.data);
            delete salesRes.rows;
            // console.log(selesCopy.rows[1], salesRes.rows[0]);
            // common.isApproximatelyEqualAssert(selesCopy.rows[1], salesRes.rows[0]);
            console.log(salesBefore, salesRes);
            common.isApproximatelyEqualAssert(salesBefore, salesRes);
        });
        it('员工关联客户人数本门店销售统计概览', async function () {
            this.retries(5);
            let salesOverviewBefore = _.cloneDeep(salesOverview);
            await ssReq.ssSellerLogin();
            salesOverviewBefore.totalMoney = common.sub(salesOverviewBefore.totalMoney, payRes.params.jsonParam.payMoney);
            salesOverviewBefore.money = common.sub(salesOverviewBefore.money, payRes.params.jsonParam.payMoney);
            salesOverviewBefore.totalNum = common.sub(salesOverviewBefore.totalNum, purRes.params.jsonParam.orders[0].main.totalNum);
            await common.delay(2000);
            let salesOverviewPay = await ss.spsales.getSalesOverviewForEmployee({ userId: sellerInfo.userId }).then(res => res.result.data);
            delete salesOverviewPay.rows;
            console.log(salesOverviewBefore, salesOverviewPay);

            common.isApproximatelyEqualAssert(salesOverviewBefore, salesOverviewPay);
        });
        // bug链接：http://zentao.hzdlsoft.com:6082/zentao/bug-view-11809.html
        it('卖家从钱包流水中查看售后订单详情', async function () {
            const returnBill = await sp.spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id }).then(res => res.result.data.bill);
            expect(returnBill, `卖家从钱包流水中查看售后订单详情没有返回 typeId`).to.be.include({ typeId: 1 });
        });
    });

    describe.skip('买家删除采购单', function () {
        let purBillList;
        before(async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.userLoginWithWx();;
            //买家删除
            // spTrade.purFindBills
            purBillList = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            // console.log(purBillList.result.data.rows[0].bill.id);
            await ss.spsales.displayPurBill({ purBillIds: [purBillInfo.bill.id] });
        });
        it('买家采购单列表', async function () {
            const purBillListAfter = await spTrade.purFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 0 });
            expect(purBillList.result.data.total).to.equal(purBillListAfter.result.data.total + 1);
        });
    });

    describe.skip('卖家删除退货退款单', function () {
        let salesReturnBills;
        before(async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            //卖家删除
            salesReturnBills = await spTrade.findSalesReturnBills();
            // console.log(JSON.stringify(returnBillSeller));
            await ss.spsales.hideReturnBills({ returnBillIds: [returnBillSeller.bill.id] });
        });
        it('卖家退货退款列表', async function () {
            const findSalesReturnBillsAfter = await spTrade.findSalesReturnBills();
            expect(salesReturnBills.result.data.total).to.equal(findSalesReturnBillsAfter.result.data.total + 1);
        });
    });
});

describe('退款退货:卖家不同意-offline', function () {
    this.timeout(TESTCASE.timeout);
    let purBillInfo, returnBill, payRes, returnBillSeller, saveDeliverRes, returnBillDate, arr, receiveInfo, receivePayResult, payResult, sales, salesOverview, purRes, selesCopy;

    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.userLoginWithWx();
        const getListDresSpu = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
        const spuId = getListDresSpu.result.data.rows[0].id; //52363
        const getFullForBuyer = await spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
        // purRes = await spTrade.savePurBill(purJson);
        purRes = await billManage.createPurBill(purJson);
        console.log(`创建采购订单:${JSON.stringify(purRes)}`);
        payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money, payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, });
        // payRes = await spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
        console.log(`\n创建支付=${JSON.stringify(payRes)}`);
        payResult = await spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
        console.log(`\n支付回调=${JSON.stringify(payResult)}`);
        await common.delay(500);

        let k = 1, purBillListA;
        while (!purBillInfo) {
            if (k > 10) throw new Error(`采购单列表为undefined已超过10次`);
            purBillListA = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
            purBillInfo = purBillListA.result.data.rows.find(val => val.bill.billNo == purRes.result.data.rows[0].billNo);
            await common.delay(500);
            console.log(`\n 第${k}次查询列表`);
            k++;
        };
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        expect(purBillInfo, `待发货列表未找到id为${purRes.result.data.rows[0].billNo}的订单`).to.not.be.undefined;

        // 卖家发货
        await ssReq.ssSellerLogin();
        //列表skus只取单据sku中的一条数据。。
        const qlRes = await spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
        // console.log(`qlRes=${JSON.stringify(qlRes)}`);
        const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
        //查询单据详情
        qfRes = await spTrade.salesFindBillFull({ id: salesListRes.bill.id });
        const details = qfRes.result.data.skus.map((sku) => {
            return { salesDetailId: sku.id, num: sku.skuNum };
        });
        const logisList = await spconfb.findLogisList1({ cap: 1 });
        const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
        await spTrade.deliverSalesBill({
            main: {
                logisCompId: logisList.result.data.rows[randonNum].id,
                logisCompName: logisList.result.data.rows[randonNum].name,
                waybillNo: common.getRandomNumStr(12),
                buyerId: qlRes.result.data.rows[0].bill.buyerId,
                // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
            },
            details: details
        });
        await ssReq.userLoginWithWx();
        let a = 1, purBillList1;
        while (!purBillInfo) {
            if (a > 10) throw new Error(`采购单列表为undefined已超过10次`);
            purBillList1 = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 3 });
            purBillInfo = purBillList1.result.data.rows.find(val => val.bill.id == purRes.result.data.rows[0].billId);
            await common.delay(500);
            console.log(`\n 第${a}次查询列表`);
            a++;
        };
        // const purBillList1 = await spTrade.purFindBills({ pageSize: 20, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 3 });
        //为了避免选择已经发生过退款操作的单子进行退款操作
        // purBillInfo = purBillList.result.data.rows.find(val => val.bill.backFlag == 0 && val.bill.shipPayKind == 0 && val.bill.payKind == 1 && val.bill.flag > 0);
        // console.log(`purBillList1=${JSON.stringify(purBillList1.result.data.rows)}`);

        expect(purBillInfo, `待收货列表未找到id为${purRes.result.data.rows[0].billId}的订单`).to.not.be.undefined;
        //买家确认收货
        // await spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
        // 开单失败则结束当前脚本
        if (typeof (purBillInfo) == 'undefined') {
            console.warn(`待收货列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
            this.parent ? this.parent.skip() : this.skip();
        };
        // 买家收货地址
        receiveInfo = await spMdm.getUserDefaultRecInfo({ typeId: 1 }).then(res => res.result.data.recInfo);
        //退货原因（字典类别2005）
        BASICDATA['3003'] = await spugr.getDictList({ typeId: '3003', flag: 1 }).then((res) => res.result.data.rows);
        //仅退款的原因要从这个字典里面取
        BASICDATA['3006'] = await spugr.getDictList({ typeId: '3006', flag: 1 }).then((res) => res.result.data.rows);
        // returnReason = BASICDATA['2012'][common.getRandomNum(0, BASICDATA['2012'].length - 1)];

        await ssReq.ssSellerLogin();

        //获取参数：订单未发货多久可以直接退款
        returnBillDate = await ss.config.getParamInfo({ ownerKind: 4, ownerId: LOGINDATA.tenantId, code: 'bill_confirm_time_out', domainKind: 'system' }).then(res => res.result.data.val);
        // console.log(returnBillDate);
    });
    describe('买家创建退款退货申请单-退货退款', function () {
        before('创建退款申请', async function () {
            await ssReq.userLoginWithWx();
            const returnBillJson = await basicJson.returnBillJson2({ purBillInfo });
            returnBill = await spTrade.saveReturnBill(returnBillJson);
            returnBill.params.jsonParam.returnDate = returnBillDate;
            // console.log(`returnBill=${JSON.stringify(returnBill)}`);
            console.log(`\n退款退货申请单:${JSON.stringify(returnBill)}`);

        });
        it('卖家退款校验', async function () {
            this.retries(5);
            await ssReq.ssSellerLogin();
            let res = await spTrade.findSalesReturnBills({ pageSize: 10, pageNo: 1 });
            let exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0], ['id', 'rem']);
            returnBillSeller = res.result.data.rows[0];
        });

        it('买家查看采购订单的售后列表', async function () {
            await ssReq.userLoginWithWx();
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            const exp = getBuyerExp({ purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1 });  //要查发起申请后的预期结果，所以typeId传6.
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });

        it('卖家查看退货列表', async function () {
            await ssReq.ssSellerLogin();
            const exp = getSellerListExp({ returnBill, purBillInfo, flag: 3, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, returnBillSeller, ['id']);
        });
        it('卖家查看退货单详情', async function () {
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            const exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 6, returnType: 1
            });
            console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['billId', 'id']);
        });
    });

    describe('卖家审核退货申请', function () {
        before(async function () {
            await ssReq.ssSellerLogin();
            const dictList = await ss.config.getSpgDictList({ typeId: 3004, flag: 1 }).then(res => res.result.data.rows);
            console.log(`\n dictList=${JSON.stringify(dictList)}`);
            // 卖家审核
            const checkReturnRes = await spTrade.checkReturnBill({
                id: returnBillSeller.bill.id,
                checkResult: 0, //不同意
                rejectReason: dictList[common.getRandomNum(0, dictList.length - 1)].id
            });
            console.log(`checkReturnRes=${JSON.stringify(checkReturnRes)}`);

        });
        it('审核后卖家查看退货列表', async function () {
            const qlRes = await spTrade.findSalesReturnBills({ searchToken: returnBillSeller.bill.id });
            const exp = getSellerListExp({ purBillInfo, returnBill, flag: 4, returnType: 1 });
            common.isApproximatelyEqualAssert(exp, qlRes.result.data.rows[0], ['id']);
        })
        it('审核后卖家查看退货详情', async function () {
            //单据状态，-1 已删除，0 已取消，1 正常（不使用）。添加状态：-2 彻底删除，2 处理中（暂不使用），3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
            const res = await spTrade.getReturnBillDetail({ id: returnBillSeller.bill.id });
            const exp = getSellerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1,
            });
            exp.bill.flag = 98;
            exp.bill.frontFlag = 98;
            exp.bill.frontFlagName = '卖家不同意';
            exp.flows[1].flowName = '卖家不同意';
            exp.flows[1].newFlag = 98;
            exp.flows[1].newFlagName = '卖家不同意';
            exp.flows[1].typeId = 2;
            console.log(res.result.data);
            common.isApproximatelyEqualAssert(exp, res.result.data, ['id', 'billId']);
        });

        it('发货前买家查看售后列表', async function () {
            await common.delay(500);
            await ssReq.userLoginWithWx();
            const res = await spTrade.getPurReturnBills({ purId: purBillInfo.bill.id });
            let exp = getBuyerExp({
                purBillInfo, saveReturnBillRes: returnBill, typeId: 1, returnType: 1
            });
            exp.bill.flag = 98;
            exp.bill.frontFlag = 98;
            exp.bill.frontFlagName = '卖家不同意';
            exp.flows[2].flowName = '卖家不同意';
            exp.flows[2].newFlag = 98;
            exp.flows[2].newFlagName = '卖家不同意';
            exp.flows[2].typeId = 2;
            exp.flows[3].flowName = '售后结束';
            exp.flows[3].newFlag = 98;
            exp.flows[3].typeId = 2;
            console.log(res.result.data.rows[0].flows);
            common.isApproximatelyEqualAssert(exp, res.result.data.rows[0]);
        });
    });

});

/**
 * 此方法主要用来获取买家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} typeId：需要得到买家哪一步的预期结果 1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  saveDeliverRes:买家发货返回的JSON
*/
function getBuyerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };
    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[5]);
                break;
            case 3:
                flows = getBuyerExpJson(1)[6].concat(getBuyerExpJson(1)[1], getBuyerExpJson(1)[4], getBuyerExpJson(1)[4]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '退货完成';
                break;
        };
    } else {
        switch (params.typeId) {
            case 6:
                flows = getBuyerExpJson(0)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退款';
                break;
            case 1:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 3:
                flows = getBuyerExpJson(0)[6].concat(getBuyerExpJson(0)[1]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '交易完成';
                // flows = getBuyerExpJson(params.saveReturnBillRes, 0)[6].concat(getBuyerExpJson(params.saveReturnBillRes, 0)[1]);
                // bill.flag = 4;   //线上7  测试4
                // bill.frontFlag = 4;
                // bill.frontFlagName = '交易完成';
                break;
        };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, flows, skus: params.purBillInfo.skus };

};

/**
 * 获取买家所有状态的JSON
*/
function getBuyerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }, {
            flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            flowName: '等待买家寄出货品',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            flowName: '等待卖家确认收货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }, {
            flowName: '卖家已退款，3-5天内到账',
            oldFlag: 5,
            newFlag: 6,
            typeId: 5,
        }],
        3: [{
            flowName: '退款成功',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            flowName: '申请退款',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }, {
            flowName: '等待卖家处理...',
            oldFlag: 3,
            newFlag: 3,
            typeId: 6,
        }],
        1: [{
            flowName: '卖家已同意，退款中',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }, {
            flowName: '卖家已退款，3-5天内到账',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        3: [{
            flowName: '退款成功',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/**
 * 此方法主要用来获取卖家的预期结果，typeId是买家单据的状态，根绝传递的状态返回对应的预期结果；
 * @param {Object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.saveDeliverRes:买家发货返回的JSON
*/
function getSellerExp(params) {
    let flows = [];
    let commonJson = {
        billId: params.saveReturnBillRes.result.data.val,
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        rem: params.saveReturnBillRes.params.jsonParam.main.rem,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
        remind: `如果在[${params.saveReturnBillRes.params.jsonParam.returnDate}]天内没有操作系统将默认同意退货`,
        deliverRem: '',
        logisCompId: '',
        logisCompName: '',
        waybillNo: '',
        returnReason: params.saveReturnBillRes.params.jsonParam.main.returnReasonName,
    };
    if (params.saveDeliverRes) {
        commonJson.deliverRem = params.saveDeliverRes.params.jsonParam.deliverRem;
        commonJson.logisCompId = params.saveDeliverRes.params.jsonParam.logisCompId;
        commonJson.logisCompName = params.saveDeliverRes.params.jsonParam.logisCompName;
        commonJson.waybillNo = params.saveDeliverRes.params.jsonParam.waybillNo;
    };

    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.saveReturnBillRes.params._tid,
        id: params.saveReturnBillRes.result.data.val,
        totalMoney: params.saveReturnBillRes.params.jsonParam.main.totalMoney,
        // totalNum: params.saveReturnBillRes.params.jsonParam.main.totalNum,
    };

    if (params.returnType == 1) {
        switch (params.typeId) {
            case 6:
                flows = getSellerExpJson(1)[6];
                bill.flag = 3;
                bill.frontFlag = 3;
                bill.frontFlagName = '买家申请退货退款';
                break;
            case 1:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1]);
                bill.flag = 4;
                bill.frontFlag = 4;
                bill.frontFlagName = '买家退货中';
                break;
            case 4:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4]);
                bill.flag = 5;
                bill.frontFlag = 5;
                bill.frontFlagName = '买家已发货';
                break;
            case 5:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5]);
                bill.flag = 6;
                bill.frontFlag = 6;
                bill.frontFlagName = '卖家已确认收货';
                break;
            case 3:
                flows = getSellerExpJson(1)[6].concat(getSellerExpJson(1)[1], getSellerExpJson(1)[4], getSellerExpJson(1)[5], getSellerExpJson(1)[3]);
                bill.flag = 7;
                bill.frontFlag = 7;
                bill.frontFlagName = '退货完成';
                break;
        };
    } else {
        // switch (params.typeId) {
        //     case 6:
        //         flows = getSellerExpJson(0)[6];
        //         bill.flag = 3;
        //         bill.frontFlag = 3;
        //         bill.frontFlagName = '买家申请退款';
        //         break;
        //     case 3:
        //         flows = getSellerExpJson(0)[6].concat(getSellerExpJson(0)[1], getSellerExpJson(0)[3]);
        //         bill.flag = 7
        //         bill.frontFlag = 7;
        //         bill.frontFlagName = '退款完成';
        //         break;
        // };
    };
    for (let i = 0; i < flows.length; i++) {
        Object.assign(flows[i], commonJson);
    };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, flows, skus: params.purBillInfo.skus };
};

/**
 * 获取卖家所有状态的JSON
*/
function getSellerExpJson(returnType) {
    let returnBillJson = {
        6: [{
            flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家申请退货退款',
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退货退款',
            newFlagName: '买家退货中',
        }, {
            flowName: '等待买家退货',
            oldFlag: 3,
            newFlag: 4,
            typeId: 1,
        }],
        4: [{
            flowName: '买家已发货',
            typeId: 4,
            oldFlag: 4,
            newFlag: 5,
            oldFlagName: '买家退货中',
            newFlagName: '买家已发货',
        }],
        5: [{
            flowName: '卖家已确认收货',
            typeId: 5,
            oldFlag: 5,
            newFlag: 6,
            oldFlagName: '买家已发货',
            newFlagName: '卖家已确认收货',
        }],
        3: [{
            flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 6,
            newFlag: 7,
            oldFlagName: '卖家已确认收货',
            newFlagName: '退货完成',
        }]
    };
    let onlyReturnMoney = {
        6: [{
            flowName: '发起申请',
            typeId: 6,
            oldFlag: 3,
            newFlag: 3,
            oldFlagName: '买家申请退款',
            newFlagName: '买家申请退款',
        }],
        1: [{
            flowName: '卖家已同意',
            typeId: 1,
            oldFlag: 3,
            newFlag: 4,
            oldFlagName: '买家申请退款',
            newFlagName: '卖家退款中',
        }],
        3: [{
            flowName: '卖家已退款',
            typeId: 3,
            oldFlag: 4,
            newFlag: 7,
            oldFlagName: '卖家退款中',
            newFlagName: '退款完成',
        }]
    };
    if (returnType == 1) {
        return returnBillJson;
    } else {
        return onlyReturnMoney;
    }
};

/**
 * 获取卖家列表的预期结果
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function getSellerListExp(params) {
    let bill = {
        billNo: params.purBillInfo.bill.billNo,
        buyerId: params.returnBill.params._tid,
        id: params.returnBill.result.data.val,
        purBillId: params.returnBill.params.jsonParam.main.purBillId,
        salesBillId: params.purBillInfo.bill.salesBillId,
        totalMoney: params.returnBill.params.jsonParam.main.totalMoney,
        totalNum: params.returnBill.params.jsonParam.main.totalNum
    };

    // switch (params.flag) {
    //     case 3:
    //         bill.flag = 3;
    //         bill.frontFlag = 3;
    //         params.returnType == 1 ? bill.frontFlagName = '买家申请退货退款' : bill.frontFlagName = '买家申请退款';
    //         break;
    //     case 4:
    //         bill.flag = 4;
    //         bill.frontFlag = 4;
    //         bill.frontFlagName = '买家退货中';
    //         break;
    //     case 5:
    //         bill.flag = 5;
    //         bill.frontFlag = 5;
    //         bill.frontFlagName = '买家已发货';
    //         break;
    //     case 6:
    //         bill.flag = 6;
    //         bill.frontFlag = 6;
    //         bill.frontFlagName = '卖家已确认收货';
    //     bill.flag = 7;
    //     bill.frontFlag = 7;
    //     params.returnType == 1 ? bill.frontFlagName = '退货完成' : bill.frontFlagName = '退款完成';
    // };
    params.purBillInfo.skus.map(res => delete res.id);
    return { bill, skus: params.purBillInfo.skus }
};

/**
 * 管理员查看退货详情
 * @param {object} params.purBillInfo 需要退货的这个采购单列表
 * @param {Object}  params.saveReturnBillRes:新增退货返回的JSON
 * @param {Integer} params.typeId：需要得到买家哪一步的预期结果  1 卖家已同意 2 卖家不同意 3 卖家已退款 4 买家已发货 5 卖家已确认收货 6 发起申请 7 买家已撤销
 * @param {Object}  params.returnType
 */
function adminReturnInfoExp(params) {
    const mapField = 'billNo;buyerRem;confirmTime;logisCompid;money;payKind;purBillId=id;payTime;proTime;procFlag;salesBillId;shipFeeMoney;totalMoney;totalNum;waybillNo';
    let exp = getSellerExp(params);
    let bill = Object.assign(format.dataFormat(params.purBillInfo.bill, mapField), {
        id: params.saveReturnBillRes.result.data.val,
        buyerId: params.saveReturnBillRes.params._tid,
        favourtotalMoney: common.add(params.purBillInfo.bill.favorMoney, params.purBillInfo.bill.shopCoupsMoney),//优惠总金额
    });

    exp.bill = bill;
    exp.bill.receiveAddress = params.receiveInfo.ecCaption.addrId;
    exp.bill.receiveName = params.receiveInfo.linkMan;
    exp.bill.receivePhone = params.receiveInfo.telephone;
    return exp;
};

/**
 * 获取平台端退货列表查询期望值
 * @param {object} params.purBillInfo 退货的采购单列表
 * @param {object} params.returnBill 退货方法返回的json 包括params和result
 * @param {Integer} params.flag 生成当前状态的预期结果，3 退货申请已生成，4 卖家已同意，5 买家已发货，6 卖家已确认收货，7 卖家已退款，98 卖家不同意，99 退货申请失败
*/
function adminReturnListExp(params) {
    const mapField = 'sellerInfo.clusterCode;sellerId=tenantId;sellerName=tenantName;sellerUnitId=unitId';
    let sellerList = getSellerListExp(params);
    let exp = Object.assign(sellerList.skus[0], sellerList.bill, format.dataFormat(params.purBillInfo.trader, mapField));
    exp.typeId = params.returnBill.params.jsonParam.main.typeId;
    exp.rem = params.returnBill.params.jsonParam.main.rem
    exp.proTime = params.returnBill.opTime;
    return exp;
};