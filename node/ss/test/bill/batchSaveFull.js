const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const spMdm = require('../../../reqHandler/sp/biz_server/spmdm');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const messageManage = require('../../help/messageManage');
// const employeeManage = require('../../help/employeeManage');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');
const ssConfigParam = require('../../help/configParamManager');
const ssCaps = require('../../../reqHandler/ss/ssCaps');

describe('批量发货', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, purRes, checkPurBillFlag, getFullForBuyer, salesListRes, styleInfoBefore, toBeDeliverNum, salesOverview, rate = 0.006,
        money, sales, spuTitle, logisList, deliverSalesBill;
    /** 钱包首页数据 */
    let homePagedata = {};
    before(async function () {
        //卖家登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        const expRemindPrintFlag = await ssConfigParam.getSpgParamInfo({ ownerId: LOGINDATA.tenantId, code: 'exp_remind_print_flag' });
        console.log(expRemindPrintFlag);
        await expRemindPrintFlag.updateParam({ val: 1 });

        await ssReq.userLoginWithWx();
        const getListDresSpu = await ss.spchb.getDresSpuList({
            tenantId: sellerInfo.tenantId,
            orderBy: 'marketDate',
            orderByDesc: true
        });
        const spuId = getListDresSpu.result.data.rows[0].id;
        spuTitle = getListDresSpu.result.data.rows[0].title;
        getFullForBuyer = await sp.spdresb.getFullForBuyer({
            spuId: spuId,
            _tid: sellerInfo.tenantId,
            _cid: sellerInfo.clusterCode
        });
        console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

        await spMdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({
            styleInfo: getFullForBuyer
        });

        await ssReq.ssSellerLogin();
        await common.delay(500);
        //卖家商品详情
        styleInfoBefore = await sp.spdresb.getFullById({
            id: purJson.orders[0].details[0].spuId
        });
        // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore.result.data)}`);

        await ssReq.userLoginWithWx();

        purRes = await billManage.createPurBill(purJson);
        // purRes = await sp.spTrade.savePurBill(purJson);
        console.log(`purRes=${JSON.stringify(purRes)}`);
        money = parseFloat(common.mul(purRes.params.jsonParam.orders[0].main.totalMoney, (1 - rate)).toFixed(2));
        console.log(money);
        // 开单失败则结束当前脚本
        if (purRes.result.data.rows[0].isSuccess != 1) {
            console.warn(`开单失败:${JSON.stringify(purRes.result.data.rows[0])}`);
            this.parent ? this.parent.skip() : this.skip();
        };

        await ssReq.ssSellerLogin();
        await common.delay(500);
    });

    describe('买家支付', function () {
        let payRes, salesBillInfo, recentBuyShopList;
        before(async function () {
            await ssReq.userLoginWithWx();
            // console.log(LOGINDATA);
            // {"payType":5,"payMethod":2,"hashKey":"20190709103046468961","payerOpenId":"oJM315T183V7yCCzQsuGYKTDyDYg","payMoney":585,"orderIds":[209019]}
            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: LOGINDATA.wxOpenId,
                orderIds: [purRes.result.data.rows[0].billId],
                payMoney: purRes.params.jsonParam.orders[0].main.money
            });
            // console.log(`payRes=${JSON.stringify(payRes)}`);
            const res = await sp.spTrade.receivePayResult({
                mainId: payRes.result.data.payDetailId,
                amount: payRes.params.jsonParam.payMoney,
                purBillId: purRes.result.data.rows[0].billId
            }).then(res => res);
            console.log(`\n支付回调:${JSON.stringify(res)}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
        })
        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();
            await messageManage.messageCenter({
                billNo: purRes.result.data.rows[0].billNo,
                tagOneIn: 72,
                text: '买家已经付款',
                title: '买家已付款'
            });
        });
        it('卖家销售单列表', async function () {
            // await common.delay(5000);
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(1); //已付款
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(2); //未发货
        });
        it('卖家商品详情-销售数量和销售金额', async function () {
            this.retries(2);
            await common.delay(500);
            // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
            let styleInfoAfter = await sp.spdresb.getFullById({
                id: purRes.params.jsonParam.orders[0].details[0].spuId
            });
            let exp = common.addObject({
                stockNum: -purRes.params.jsonParam.orders[0].main.totalNum,
                salesNum: purRes.params.jsonParam.orders[0].main.totalNum,
                salesMoney: purRes.params.jsonParam.orders[0].main.totalMoney
            }, styleInfoBefore.result.data.spu);
            common.isApproximatelyEqualAssert(exp, styleInfoAfter.result.data.spu, ['sessionId', 'ver', 'channelIds', 'updatedDate']);
        });
        it('卖家销售单详情', async function () {
            salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            });
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(2);
        });
    });

    describe('卖家发货', function () {
        let qfRes;
        before(async function () {
            await ssReq.ssSellerLogin();
            //列表skus只取单据sku中的一条数据。。
            const qlRes = await sp.spTrade.salesFindBills({
                pageSize: 10,
                pageNo: 1,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 2
            });
            // console.log(`qlRes=${JSON.stringify(qlRes)}`);
            salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            //查询单据详情
            qfRes = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            });
            const details = qfRes.result.data.skus.map((sku) => {
                return {
                    salesDetailId: sku.id,
                    num: sku.skuNum
                };
            });
            logisList = await sp.spconfb.findLogisList1({ cap: 1 }).then(res => res.result.data.rows);
            console.log(`\nlogisList=${JSON.stringify(logisList)}`);

            const logis = logisList.find(obj => obj.name == '中通快递');
            const expSheetAccount = {
                expName: '中通快递',
                expId: logis.id,
                expAccount: ssCaps.expSheetAccount.expAccount,
                expPassword: ssCaps.expSheetAccount.expPassword,
                defaultFlag: 1
            }
            console.log(expSheetAccount);
            await ss.print.saveExpSheetAccount(expSheetAccount).then(res => res.result.data.id);

            // const expSheetAccountRes = await ss.print.findExpSheetAccount().then(res => res.result.data.rows[0]);
            const shipAddressRes = await ss.print.findShipAddress().then(res => res.result.data.rows[0]);
            const availablePrinters = await ss.print.findAvailablePrinters({ printWay: 2 }).then(res => res.result.data.rows[0]);
            console.log(`shipAddressRes=${JSON.stringify(shipAddressRes)}`);
            console.log(`availablePrinters=${JSON.stringify(availablePrinters)}`);

            console.log(ssCaps.expSheetAccount);

            deliverSalesBill = await ss.spsales.batchSaveDeliverBill({
                billList: [{
                    main: {
                        buyerId: qlRes.result.data.rows[0].bill.buyerId,
                        hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
                        shipFeeMoney: 0 //运费。如果不传，默认为0
                    },
                    details: details
                }],
                print: {
                    expAccount: ssCaps.expSheetAccount.expAccount,
                    expName: ssCaps.expSheetAccount.expName,
                    expId: logis.id,
                    shipId: shipAddressRes.id,
                    printerId: availablePrinters.id
                }
            })
            console.log(`deliverSalesBill=${JSON.stringify(deliverSalesBill)}`);

        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == qfRes.result.data.bill.billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(3);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: qfRes.result.data.bill.id
            }); //
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(3);
        });
        it('卖家商品详情-库存', async function () {
            this.retries(5);
            await common.delay(2000);
            let styleInfoAfter = await sp.spdresb.getFullById({
                id: purRes.params.jsonParam.orders[0].details[0].spuId
            });
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // console.log(`purRes=${JSON.stringify(purRes)}`);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            common.isApproximatelyEqualAssert({
                stockNum: common.sub(styleInfoBefore.result.data.spu.stockNum, purRes.params.jsonParam.orders[0].main.totalNum)
            }, styleInfoAfter.result.data.spu);
        });
    });


    describe.skip('买家收货', function () {
        let purInfo;
        before(async function () {
            await ssReq.userLoginWithWx();
            // await common.delay(3000);
            //买家确认收货
            await sp.spTrade.confirmReceipt({
                purBillIds: [purRes.result.data.rows[0].billId]
            });
            // console.log(`确认收货：=${JSON.stringify(res)}`);
        });
        it('确认收货后-延迟收货', async function () {
            const res = await sp.spTrade.extendedReturn({
                id: purRes.result.data.rows[0].billId,
                check: false
            });
            expect(res.result).to.includes({
                msgId: 'spbillflag_after_confirm'
            });
        });
        it('根据货品查询订单', async function () {
            const purBillList = await sp.spTrade.purFindBills({
                searchToken: spuTitle,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 4
            });
            console.log(`\n purBillList.result.data.rows=${JSON.stringify(purBillList)}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            console.log(`\n putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('买家采购单列表', async function () {
            const purBillList = await sp.spTrade.purFindBills({
                pageSize: 20,
                pageNo: 1,
                orderBy: 'proTime',
                orderByDesc: true,
                statusType: 4
            });
            // console.log(`purBillList.result.data.rows[0]=${JSON.stringify(purBillList.result.data.rows[0])}`);
            let purBillInfo = purBillList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`putBillInfo=${JSON.stringify(purBillInfo)}`);
            expect(purBillInfo.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('买家采购单详情', async function () {
            purInfo = await sp.spTrade.findPurBillFull({
                id: purRes.result.data.rows[0].billId
            });
            console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);

            common.isApproximatelyEqualAssert(purRes.params.jsonParam, purInfo.result.data);
            expect(purInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(4);
        })
        it('卖家消息中心', async function () {
            //卖家登录
            await ssReq.ssSellerLogin();
            await messageManage.messageCenter({
                billNo: purRes.result.data.rows[0].billNo,
                tagOneIn: 77,
                text: '买家已经确认收货',
                title: '收货提醒'
            });
        });
        it('卖家销售单列表', async function () {
            const salesList = await sp.spTrade.salesFindBills({
                pageSize: 10,
                orderByDesc: true,
                orderBy: 'proTime',
                statusType: 0
            }); //purRes.result.data.rows[0].billId
            salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);
            // console.log(`salesListRes=${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.frontFlag, '单据状态错误').to.equal(4);
        });
        it('卖家销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({
                id: salesListRes.bill.id
            }); //
            // console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.frontFlag, '单据状态错误').to.equal(4);
        });
    });

});
