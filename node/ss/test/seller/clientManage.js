const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');
const pinyin = require("pinyin4js");

describe('客户管理', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
    });

    describe('新建买家登录', async function () {
        let sellerCountBefore, relationMallMemberCountBefore, mobile = common.getRandomMobile(), clientInfo, sellerLogsByDayBefore, memberListBefore;
        before(async function () {
            await ssReq.ssSellerLogin();
            //  totalVisitCount	long	全部访客 curDayAddVisitCount	long	当日新增 curDayTotalVisitCount	long	当日全部
            sellerCountBefore = await ss.up.getSellerCount().then(res => res.result.data);
            // relationCunstomCount	int	已关联商陆花客户数 payBalanceCount	long	已开启余额数 payArrearsCount	long	已开启欠款数
            relationMallMemberCountBefore = await ss.spmdm.statisticsRelationMallMemberCount().then(res => res.result.data);
            await ssReq.ssClientLogin({ sellerId: sellerInfo.userId, tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
            await ssReq.ssSellerLogin();
        });
        it('访客统计', async function () {
            this.retries(3);
            await common.delay(1000);
            const sellerCount = await ss.up.getSellerCount().then(res => res.result.data);
            const exp = {
                totalVisitCount: sellerCountBefore.totalVisitCount + 1,
                curDayAddVisitCount: sellerCountBefore.curDayAddVisitCount + 1,
                curDayTotalVisitCount: sellerCountBefore.curDayTotalVisitCount + 1,
            };
            common.isApproximatelyEqualAssert(exp, sellerCount);
        });
        it('当前账号下的绑定的客户统计信息', async function () {
            const relationMallMemberCount = await ss.spmdm.statisticsRelationMallMemberCount();
            common.isApproximatelyEqualAssert(relationMallMemberCountBefore, relationMallMemberCount);
        });
        it('客户列表', async function () {
            memberListBefore = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data.rows);
            expect(memberListBefore.find(obj => obj.custUserId == clientInfo.userId), '按访问时间倒序的客户列表里未找到新建客户').to.not.be.undefined;
        });
        it('访客记录按天隔离', async function () {
            this.retries(3);
            await common.delay(1000);
            sellerLogsByDayBefore = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows.find(obj => obj.tenantId == clientInfo.tenantId && obj.isNew == 1));
            expect(sellerLogsByDayBefore, '访客记录按天隔离列表里未找到新建客户').to.not.be.undefined;
        });
        it('访客记录', async function () {
            const sellerLogs = await ss.up.findSellerLogs({ tenantId: clientInfo.tenantId, orderBy: 'visitTime', orderByDesc: true });
            expect(sellerLogs.result.data.rows[0].isNew, '访客记录isNew不为1').to.equal(1);
        });
        it('访客记录按天隔离:买家再次登录', async function () {
            await ssReq.ssClientLogin({ sellerId: sellerInfo.userId, tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            await ssReq.ssSellerLogin();
            const sellerLogsByDay = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows.find(obj => obj.tenantId == clientInfo.tenantId && obj.isNew == 1));
            expect(sellerLogsByDay.mallMember.lastVisitTime, '访客记录按天隔离列表lastVisitTime未更新').to.not.equal(sellerLogsByDayBefore.mallMember.lastVisitTime);
        });
        it('客户列表:买家再次登录', async function () {
            const memberList = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data.rows.find(obj => obj.custUserId == clientInfo.userId));
            expect(memberList.lastVisitTime, '客户列表lastVisitTime未更新').to.not.equal(memberListBefore.lastVisitTime);
        });
        describe('修改客户适用价格', async function () {
            let spuId, code, originalPubPrice;
            before(async function () {
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                const getListDresSpu = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
                spuId = getListDresSpu.result.data.rows[0].id;
                code = getListDresSpu.result.data.rows[0].code;
                console.log(spuId, code);
                await ssReq.ssSellerLogin();
                const dresFull = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
                // console.log(`\n dresFull=${JSON.stringify(dresFull.originalPrices.pubPrice)}`);
                originalPubPrice = dresFull.originalPrices.pubPrice ? dresFull.originalPrices.pubPrice : dresFull.spu.pubPrice;
                console.log(originalPubPrice);
            });
            describe('修改为非默认价', async function () {
                let priceCodeValue;
                before(async function () {
                    //修改为非默认价
                    await ssReq.ssSellerLogin();
                    const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
                    console.log(`\n priceList=${JSON.stringify(priceList)}`);
                    priceList.forEach(price => {
                        if (price.codeName != "默认价") {
                            priceCodeValue = price.codeValue;
                            return;
                        }
                    });
                    if (priceCodeValue == undefined) {
                        console.warn(`没有除默认价以外的其他价格类型`);
                        this.parent ? this.parent.skip() : this.skip();
                    };
                    const res = await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: priceCodeValue })
                    console.log(`\n res=${JSON.stringify(res)}`);
                });
                it('客户列表', async function () {
                    const memberList = await ss.spmdm.getMemberList({ custUserId: clientInfo.userId }).then(res => res.result.data.rows[0]);
                    console.log(`\n memberList=${JSON.stringify(memberList)}`);
                    expect(memberList.validPrice, 'validPrice与设置的价格不符').to.equal(priceCodeValue);
                });
                it('买家查询商品详情', async function () {
                    await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                    const getFullForBuyer = await sp.spdresb.getFullForBuyer({ buyerId: clientInfo.tenantId, spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer.result.data.spu.pubPrice)}`);
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer.result.data.spu.defaultPrice)}`);
                    expect(originalPubPrice, 'defaultPrice与预期不一致').to.equal(getFullForBuyer.result.data.spu.defaultPrice);
                });
                it('买家查询全部商品列表', async function () {
                    const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, code: code }).then(res => res.result.data.rows);
                    console.log(`\n dresList=${JSON.stringify(dresList)}`);
                    expect(originalPubPrice, 'defaultPrice与预期不一致').to.equal(dresList[0].defaultPrice);
                });
            });
            describe('修改为默认价', async function () {
                let priceCodeValue;
                before(async function () {
                    //修改为非默认价
                    await ssReq.ssSellerLogin();
                    const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
                    priceList.forEach(price => {
                        if (price.codeName == "默认价") {
                            priceCodeValue = price.codeValue;
                            return;
                        }
                    });
                    if (priceCodeValue == undefined) {
                        console.warn(`没有默认价`);
                        this.parent ? this.parent.skip() : this.skip();
                    };
                    await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: priceCodeValue })
                });
                it('客户列表', async function () {
                    const memberList = await ss.spmdm.getMemberList({ custUserId: clientInfo.userId }).then(res => {
                        console.log(`res=${JSON.stringify(res)}`);
                        return res.result.data.rows[0]
                    });
                    console.log(`\n memberList=${JSON.stringify(memberList)}`);
                    expect(memberList.validPrice, 'validPrice与设置的价格不符').to.equal(priceCodeValue);
                });
                it('买家查询商品详情', async function () {
                    await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                    const getFullForBuyer = await sp.spdresb.getFullForBuyer({ buyerId: clientInfo.tenantId, spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer.result.data.spu)}`);
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer.result.data.spu.pubPrice)}`);
                    // console.log(`\n getFullForBuyer=${JSON.stringify(getFullForBuyer.result.data.spu.defaultPrice)}`);
                    expect(originalPubPrice, 'defaultPrice与预期不一致').to.equal(getFullForBuyer.result.data.spu.defaultPrice);
                });
                it('买家查询全部商品列表', async function () {
                    const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, code: code }).then(res => res.result.data.rows);
                    console.log(`\n dresList=${JSON.stringify(dresList.length)}`);
                    expect(originalPubPrice, 'defaultPrice与预期不一致').to.equal(dresList[0].defaultPrice);
                });
            });
        });
    });

    describe('老买家登录', async function () {
        let sellerCountBefore, relationMallMemberCountBefore, clientInfo;
        before(async function () {
            await ssReq.ssSellerLogin();
            sellerCountBefore = await ss.up.getSellerCount().then(res => res.result.data);
            relationMallMemberCountBefore = await ss.spmdm.statisticsRelationMallMemberCount().then(res => res.result.data);
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            await ssReq.ssSellerLogin();
        });
        it('访客统计', async function () {
            const sellerCount = await ss.up.getSellerCount().then(res => res.result.data);
            const exp = {
                curDayAddVisitCount: sellerCountBefore.curDayAddVisitCount,
            };
            common.isApproximatelyEqualAssert(exp, sellerCount);
        });
        it('当前账号下的绑定的客户统计信息', async function () {
            const relationMallMemberCount = await ss.spmdm.statisticsRelationMallMemberCount().then(res => res.result.data);
            common.isApproximatelyEqualAssert(relationMallMemberCountBefore, relationMallMemberCount);
        });
        it('客户列表', async function () {
            const memberList = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data.rows);
            expect(memberList.find(obj => obj.custUserId == clientInfo.userId), '按访问时间倒序的客户列表里未找到老客户').to.not.be.undefined;
        });
        it('访客记录按天隔离', async function () {
            const sellerLogsByDay = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows.find(obj => obj.tenantId == clientInfo.tenantId && obj.isNew == 0));
            expect(sellerLogsByDay, '访客记录按天隔离列表里未找到老客户').to.not.be.undefined;
        });
        it('访客记录', async function () {
            const sellerLogs = await ss.up.findSellerLogs({ tenantId: clientInfo.tenantId, orderBy: 'visitTime', orderByDesc: true });
            expect(sellerLogs.result.data.rows[0].isNew, '访客记录isNew不为0').to.equal(0);
        });
        //因新用户无下单记录，补货列表为空无法校验
        it('快速补货列表存在defaultPrice', async function () {
            await ssReq.userLoginWithWx();
            const recentBillList = await ss.sppur.findRecentBillSpu({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            console.log(`\n recentBillList=${JSON.stringify(recentBillList)}`);
            expect(recentBillList[0], '不存在defaultPrice').to.have.property('defaultPrice');
        });
    });

    describe('客户列表', function () {
        let listMember;
        before('查询现有列表', async () => {
            await ssReq.ssSellerLogin();
            listMember = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
        });

        describe('修改备注', function () {
            let getMemberByUserIdBefore, custUserId, userId, rem;
            before('修改备注', async () => {
                const randomNum = common.getRandomNum(0, listMember.length - 1);
                // console.log(randomNum);
                custUserId = listMember[randomNum].custUserId;
                userId = listMember[randomNum].id;
                rem = common.getRandomChineseStr(3);
                getMemberByUserIdBefore = await ss.spmdm.getMemberByUserId({ userId: custUserId });
                await ss.spmdm.updateMemberRem({ userId: userId, rem: rem });
                getMemberByUserIdBefore.result.data.rem = rem;
            });

            it('验证修改备注', async () => {
                const getMemberByUserId = await ss.spmdm.getMemberByUserId({ userId: custUserId });
                common.isApproximatelyEqualAssert(getMemberByUserIdBefore.result, getMemberByUserId.result, ['namePy', 'updatedDate', 'globalId']);
            });
        });
    });

    describe('分组管理', function () {
        let memberIds, groupId, addGroup, listGroup;
        before(async () => {
            await ssReq.ssSellerLogin();
            const listGroup = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
            for (const i in listGroup) {
                if (listGroup[i].name != '上新' || listGroup[i].name != '爆款') {
                    deleteGroup = await ss.spmdm.deleteMemberGroup({ groupId: listGroup[i].id });
                }
            }
            const listGroup2 = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
            console.log(`listGroup2=${JSON.stringify(listGroup2)}`);

            const listMember = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
            memberIds = listMember[0].id.toString();
            for (let i = 1; i < common.getRandomNum(1, listMember.length); i++) {
                memberIds = memberIds + ',' + listMember[i].id.toString();
            }
            //新增分组
            addGroup = await ss.spmdm.addMemberGroup({ name: `分组${common.getRandomStr(5)}`, memberIds: memberIds });
            console.log(`\n addGroup=${JSON.stringify(addGroup)}`);
            groupId = addGroup.result.data.id;
        });

        it('验证新增分组', async () => {
            listGroup = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
            console.log(`\n listGroup=${JSON.stringify(listGroup)}`);
            common.isApproximatelyEqualAssert(addGroup.result.data, listGroup.find(obj => obj.id == addGroup.result.data.id));
        });

        describe('验证分组排序', function () {
            let groupId2;
            before('新增分组2', async function () {
                await common.delay(1000);
                groupId2 = await ss.spmdm.addMemberGroup({ name: `分组${common.getRandomStr(5)}` }).then(res => res.result.data.id);
            });
            after('删除分组', async () => {
                await ss.spmdm.deleteMemberGroup({ groupId: groupId2 });
            });
            it('验证分组排序', async function () {
                let groupListRes = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
                console.log(`分组排序=${JSON.stringify(groupListRes)}`);
                expect(groupListRes[0].id, `客户分组按时间倒序排列错误`).to.be.equal(groupId2)
            });
        });

        describe('编辑分组名称', function () {
            let updateGroup, listGroup;
            before('编辑分组名称', async () => {
                updateGroup = await ss.spmdm.updateMemberGroup({ id: groupId, name: `分组修改${common.getRandomStr(5)}` });
                console.log(`\n updateGroup=${JSON.stringify(updateGroup)}`);
            });

            it('验证编辑分组名称', async () => {
                listGroup = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
                console.log(`\n listGroup=${JSON.stringify(listGroup)}`);
                common.isApproximatelyEqualAssert(updateGroup.result.data, listGroup.find(obj => obj.id == groupId));
            });
        });

        describe('变更客户', function () {
            let updateGroup, listGroup;
            before('变更客户', async () => {
                updateGroup = await ss.spmdm.updateMemberGroup({ id: groupId, memberIds: memberIds });
                // console.log(`updateGroup=${JSON.stringify(updateGroup)}`);
            });

            it('验证变更客户', async () => {
                listGroup = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
                // console.log(`\nlistGroup=${JSON.stringify(listGroup)}`);
                common.isApproximatelyEqualAssert(updateGroup.result.data, listGroup.find(obj => obj.id == groupId));
            });
        });

        describe('删除分组', function () {
            let index, listGroup;
            before('删除分组', async () => {
                await ss.spmdm.deleteMemberGroup({ groupId: groupId });
                // console.log(`\nlistGroup=${JSON.stringify(listGroup)}`);
            });

            it('验证删除分组', async () => {
                listGroup = await ss.spmdm.getMemberGroupList().then(res => res.result.data.rows);
                expect(listGroup.find(obj => obj.id == groupId), '找到了已删除的分组').to.be.undefined;
            });
        });

    });

    // namePy设置规则 rem>name>nickName
    describe('根据姓名首字母分组显示客户列表', async function () {
        let data;
        before(async function () {
            data = await ss.spmdm.getMemberListByNamePy({}).then(res => res.result.data);
        });
        it('数据校验', async function () {
            for (const key in data) {
                data[key].forEach(ele => expect(ele.namePy, `姓名首字母与分组不一致`).to.equal(key));
            }
        });
        // searchToken范围 name,nickName,rem,phone
        it('searchToken校验', async function () {
            const token = Object.keys(data).shift();
            const _data = await ss.spmdm.getMemberListByNamePy({ searchToken: token }).then(res => res.result.data);
            for (const key in _data) {
                if (['?', '#'].includes(key)) continue;
                _data[key].forEach(ele => expect(pinyin.convertToPinyinString(ele.name + ele.nickName + ele.rem, '', pinyin.WITHOUT_TONE).toLowerCase(), `searchToken筛选错误:\n\t${JSON.stringify(ele)}\n\t`).to.include(key));
            }
        });
    });

    describe('访问记录', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
        });
        it('筛选条件-flag=0校验', async function () {
            const sellerLogsByDay = await ss.up.findSellerLogsByDay({ flag: 0 }).then(res => {
                // console.log(res);
                return res.result.data.rows;
            });
            sellerLogsByDay.forEach(ele => expect(ele.mallMember).to.include({ flag: 0 }));
        });
        it('筛选条件-flag=1校验', async function () {
            const sellerLogsByDay = await ss.up.findSellerLogsByDay({ flag: 1 }).then(res => res.result.data.rows);
            sellerLogsByDay.forEach(ele => expect(ele.mallMember).to.include({ flag: 1 }));
        });
        // http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1443.html
        it('访客记录返回余额,欠款支付子单', async function () {
            const sellerLogsByDay = await ss.up.findSellerLogsByDay({ flag: 1 }).then(res => res.result.data.rows);
            // console.log(`sellerLogsByDay=${JSON.stringify(sellerLogsByDay)}`);

            sellerLogsByDay.forEach(ele => {
                const mallMember = ele.mallMember;
                expect(mallMember).to.have.property('paramList');
                console.log(mallMember);
                if (mallMember.slhId) {
                    common.isApproximatelyArrayAssert([{ v1: 'open_debt_payment' }, { v1: 'open_balance_payment' }, { v1: 'open_cust_offline_pay' }], mallMember.paramList);
                } else {
                    common.isApproximatelyArrayAssert([{ v1: 'open_cust_offline_pay' }], mallMember.paramList);
                }
            });
        });
    });

});