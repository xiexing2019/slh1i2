
const common = require('../../../lib/common');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');
const ssReq = require('../../help/ssReq');
const ssConfigParam = require('../../help/configParamManager');

// TODO 本用例集只做了参数的基本校验 业务流程的需要按场景展开设计用例
describe('个性化参数', function () {
    this.timeout(TESTCASE.timeout);

    before(async function () {

        await ssReq.ssSellerLogin({ code: ssAccount.seller6.mobile });
        await common.delay(500);
        // 获取当前个性化参数
        // const findProductParams = await paramshelper.findProductParams({ ownerId: LOGINDATA.tenantId }).then((res) => res.result.data.rows);
        // //  const productParams = [...findProductParams.values()].map(row => {
        // //      return {
        // //          code: row.code,
        // //          name: row.name,
        // //          val: row.val,
        // //      };
        // //  });
        // oldProductParams = _.cloneDeep(findProductParams);
        // console.log(productParams);
    });

    after('个性化参数与原来一致', async function () {
        // const findProductParams = await paramshelper.findProductParams({ ownerId: LOGINDATA.tenantId }).then((res) => res.result.data.rows);
        // // expect(findProductParams, '失败').to.equal(oldProductParams);
        // common.isApproximatelyEqualAssert(findProductParams, oldProductParams, ['updatedDate', 'updatedBy']);

    });

    describe('商品同步是否自动上架', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: LOGINDATA.tenantId, code: 'auto_shelves_after_synchronized' });
        });
        it('参数名称校验', async function () {
            expect(ssParam).to.includes({ name: '新同步货品自动上架' });
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('谁能看我的店铺', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'who_can_view_my_shop' });
            console.log(ssParam);
        });
        it('校验参数返回值', async function () {
            expect(ssParam).to.includes({ name: '访客认证', warnningTxt: '关闭认证即所有人均可进入店铺', });
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('语音播报', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'ss_notice_voice' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    // 确认已去除该参数
    describe.skip('分享展示价格', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'ss_dres_show_price_when_share' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    // 确认已去除该参数
    describe.skip('分享展示货品名称', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'ss_dres_show_name_when_share' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('库存小于0允许上架', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'allow_shelves_when_inventory_less_than_zero' });
            console.log(ssParam);
        });
        it('校验参数返回值', async function () {
            expect(ssParam).to.includes({ warnningTxt: '已上架的无库存商品将自动下架', });
            expect(ssParam.name).to.match(/(没有|无)库存允许上架/);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('是否可以线下支付', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'open_offline_payment' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('买家查询是否可以线下支付', async function () {
            await ssReq.userLoginWithWx();
            const paramInfo = await ssParam.getParamInfo({ sellerId: LOGINDATA.shopId, ownerId: LOGINDATA.tenantId });
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller6.mobile });
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('买家是否可以上门自提', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'goods_pick_up_by_self' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('买家查询是否可以上门自提', async function () {
            await ssReq.userLoginWithWx();
            const paramInfo = await ssParam.getParamInfo({ sellerId: LOGINDATA.shopId, ownerId: LOGINDATA.tenantId });
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller6.mobile });
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('是否推送每日报表', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'open_daily_report' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('是否可以支持售后', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpbParamInfo({ ownerId: LOGINDATA.tenantId, code: 'open_after_sale' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('买家查询是否可以支持售后', async function () {
            await ssReq.userLoginWithWx();
            await common.delay(500);
            const paramInfo = await ssParam.getParamInfo({ sellerId: LOGINDATA.shopId, ownerId: LOGINDATA.tenantId });
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller6.mobile });
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

    describe('是否顶部通知', async function () {
        let ssParam;
        before(async function () {
            ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: LOGINDATA.tenantId, code: 'show_top_message' });
            // console.log(ssParam);
        });
        it('修改参数为0', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
        it('修改参数为1', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller6.mobile });
            await ssParam.updateParam({ val: 1 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });

});