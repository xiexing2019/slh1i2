const ssReq = require('../../help/ssReq');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const common = require('../../../lib/common');
const spugr = require('../../../reqHandler/sp/global/spugr');
const spUp = require('../../../reqHandler/sp/biz_server/spUp');
const spConfig = require('../../../reqHandler/sp/global/spConfig');
const spCommon = require('../../../reqHandler/sp/global/spCommon');
const spAuth = require('../../../reqHandler/ss/confc/spAuth');
const format = require('../../../data/format');
const esSearchHelp = require('../../help/esSearchHelp');
const ssAccount = require('../../data/ssAccount');


describe('店内商品搜索', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, goodsSortDict;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        // await ssReq.spClientLogin();
        // clientInfo = _.cloneDeep(LOGINDATA);
        goodsSortDict = await spugr.getDictList({ typeId: '2022', flag: 1 }).then((res) => res.result.data.rows);
        // console.log(`goodsSortDict=${JSON.stringify(goodsSortDict)}`);
        // console.log(goodsSortDict);
        expect(goodsSortDict, `字典typeId=2022查询无结果`).to.have.lengthOf.above(0);
    });

    describe('店铺内排序规则', async function () {

        const arr = ['marketDate', 'pubPrice', 'sales_num=salesNum', 'showOrder'];
        let ruleSaveRes;
        after(async function () {
            await ssReq.ssSellerLogin();
            //修改店铺排序规则 0 ,查询店铺排序规则返回结果为空，
            await spugr.updateShopSortRule({ goodsSort: 0, });

        });
        //1、全局商品搜索 ec-spchb-dresSearch-searchDres
        //2、卖家查询商品列表 ec-spdresb-dresSpu-findSellerSpuList
        for (let i = 1; i < 3; i++) {//isDesc.length
            let isDesc = i == 1 ? true : false,
                descType = isDesc ? '降序' : '升序';
            for (let j = 0; j < arr.length; j++) {//
                const [orderBy, path = orderBy] = arr[j].split('=');
                before(async () => {
                    //设置排序规则
                    await ssReq.ssSellerLogin();
                    ruleSaveRes = await spugr.updateShopSortRule({ goodsSort: j + 1, sortType: i });
                });
                it('查询排序规则', async function () {
                    const findRuleRes = await spugr.findShopWithRule();
                    common.isApproximatelyEqualAssert(ruleSaveRes.params.jsonParam, findRuleRes.result.data);
                });
                it(`卖家查询商品列表按${arr[j]}字段${descType}验证,`, async function () {
                    await ssReq.ssSellerLogin();
                    const res = await spdresb.findSellerSpuList({ flags: 1, nameLike: '商品', orderBy, orderByDesc: isDesc });
                    // console.log(`res.result.data.rows[0]=${JSON.stringify(res.result.data.rows[0])}`);
                    esSearchHelp.orderAssert({ dataList: res.result.data.rows, path, orderByDesc: isDesc });
                });
            };
        };
    });
});