'use strict';
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const myShop = require('../../help/shop/myShop');
const basicJson = require('../../help/basicJson');


describe('电子面单', async function () {
    this.timeout(TESTCASE.timeout);
    let expSheetAccount, shipAddress, printer;
    SHOPDATA = new myShop();
    before('登录', async function () {
        await SHOPDATA.switchSeller();
        console.log(SHOPDATA);
    });
    describe('电子面单账户', async function () {
        before('新建', async function () {
            const expId = Number(`${Date.now()}${common.getRandomNum(0, 999)}`);
            expSheetAccount = {
                expName: `name${expId}`,
                expId: expId,
                expCode: `code${expId}`,
                expAccount: `ac${expId}`,
                expPassword: `pw${expId}`,
                remark: `rem${expId}`,
                defaultFlag: 1
            }
            console.log(expSheetAccount);
            const id = await ss.print.saveExpSheetAccount(expSheetAccount).then(res => res.result.data.id);
            console.log(`\n新建=${JSON.stringify(id)}`);
            expSheetAccount.id = id;
        });
        it('查询', async function () {
            const res = await ss.print.findExpSheetAccount().then(res => res.result.data.rows);
            console.log(`\n查询=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(expSheetAccount, res.find(obj => obj.id == expSheetAccount.id));
        });
        it('查询:根据物流商id', async function () {
            const res = await ss.print.findExpSheetAccount({ expId: expSheetAccount.expId }).then(res => res.result.data.rows);
            console.log(`\n查询:根据物流商id=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(expSheetAccount, res.find(obj => obj.id == expSheetAccount.id));
        });
        it('查询:根据id', async function () {
            const res = await ss.print.getExpSheetAccountById({ sheetAccountId: expSheetAccount.id }).then(res => res.result.data);
            console.log(`\n查询:根据id=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(expSheetAccount, res);
        });
        describe('编辑', async function () {
            let expSheetAccounts;
            before('编辑', async function () {
                const expId = Number(`${Date.now()}${common.getRandomNum(1000, 9999)}`);
                const expSheetAccount2 = {
                    expName: `name${expId}`,
                    expId: expId,
                    expCode: `code${expId}`,
                    expAccount: `ac${expId}`,
                    expPassword: `pw${expId}`,
                    remark: `rem${expId}`,
                    defaultFlag: 0
                };
                Object.assign(expSheetAccount, expSheetAccount2);
                console.log(expSheetAccount);
                const res = await ss.print.saveExpSheetAccount(expSheetAccount);
                console.log(`\n编辑=${JSON.stringify(res)}`);
            });
            it('查询', async function () {
                expSheetAccounts = await ss.print.findExpSheetAccount().then(res => res.result.data.rows);
                console.log(`\n查询=${JSON.stringify(expSheetAccounts)}`);
                common.isApproximatelyEqualAssert(expSheetAccount, expSheetAccounts.find(obj => obj.id == expSheetAccount.id));
            });
            it('查询:根据物流商id', async function () {
                const res = await ss.print.findExpSheetAccount({ expId: expSheetAccount.expId }).then(res => res.result.data.rows);
                console.log(`\n查询:根据物流商id=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(expSheetAccount, res.find(obj => obj.id == expSheetAccount.id));
            });
            it('查询:根据id', async function () {
                if (!expSheetAccounts) this.skip();
                const res = await ss.print.getExpSheetAccountById({ sheetAccountId: expSheetAccount.id }).then(res => res.result.data);
                console.log(`\n查询:根据id=${JSON.stringify(res)}`);
                // 夏宗青：只有一条数据的情况下 获取列表是0详情会改成1的   只有一条数据的话就会把他设置为默认状态
                if (!expSheetAccounts.find(obj => obj.defaultFlag == 1)) expSheetAccount.defaultFlag = 1;
                common.isApproximatelyEqualAssert(expSheetAccount, res);
            });
        });
        describe('删除', async function () {
            before('删除', async function () {
                const res = await ss.print.delExpSheetAccount({ id: expSheetAccount.id });
                console.log(`\n删除=${JSON.stringify(res)}`);
            });
            it('查询:根据物流商id', async function () {
                const res = await ss.print.findExpSheetAccount({ expId: expSheetAccount.expId }).then(res => res.result.data.rows);
                console.log(`\n查询:根据物流商id=${JSON.stringify(res)}`);
                expect(res.find(obj => obj.id == expSheetAccount.id), `物流网点账户列表存在物流商:${expSheetAccount.id}`).to.be.undefined;
            });
            it('查询:根据id', async function () {
                const res = await ss.print.getExpSheetAccountById({ sheetAccountId: expSheetAccount.id }).then(res => res.result.data);
                console.log(`\n查询:根据物流商id=${JSON.stringify(res)}`);
                expSheetAccount.flag = -1;
                common.isApproximatelyEqualAssert(expSheetAccount, res);
            });
        });
    });

    describe('发货地址', async function () {
        before('新增', async function () {
            const areaJson = await basicJson.getAreaJson();
            // console.log(areaJson);
            shipAddress = {
                shipName: common.getRandomChineseStr(3),
                mobile: common.getRandomMobile(),
                province: areaJson.provinceCode,
                city: areaJson.cityCode,
                district: areaJson.countyCode,
                detail: `xxx的家`,
                defaultFlag: 1
            }
            const id = await ss.print.saveShipAddress(shipAddress).then(res => res.result.data.id);
            console.log(`\n新建=${JSON.stringify(id)}`);
            shipAddress.id = id;
        });
        it('查询', async function () {
            const res = await ss.print.findShipAddress().then(res => res.result.data.rows);
            console.log(`\n查询=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(shipAddress, res.find(obj => obj.id == shipAddress.id));
        });
        it('查询:根据id', async function () {
            const res = await ss.print.getShipAddressById({ shipAddressId: shipAddress.id }).then(res => res.result.data);
            console.log(`\n查询:根据id=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(shipAddress, res);
        });
        describe('编辑', async function () {
            before('编辑', async function () {
                const areaJson = await basicJson.getAreaJson();
                // console.log(areaJson);
                const shipAddress2 = {
                    shipName: common.getRandomChineseStr(3),
                    mobile: common.getRandomMobile(),
                    province: areaJson.provinceCode,
                    city: areaJson.cityCode,
                    district: areaJson.countyCode,
                    detail: `luxx的家2`,
                    defaultFlag: 0
                };
                Object.assign(shipAddress, shipAddress2);
                const res = await ss.print.saveShipAddress(shipAddress);
                console.log(`\n编辑=${JSON.stringify(res)}`);
            });
            it('查询', async function () {
                const res = await ss.print.findShipAddress().then(res => res.result.data.rows);
                console.log(`\n查询=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(shipAddress, res.find(obj => obj.id == shipAddress.id));
            });
            it('查询:根据id', async function () {
                const res = await ss.print.getShipAddressById({ shipAddressId: shipAddress.id }).then(res => res.result.data);
                console.log(`\n查询:根据id=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(shipAddress, res);
            });
        });
        describe('删除', async function () {
            before('删除', async function () {
                const res = await ss.print.delShipAddress({ id: shipAddress.id });
                console.log(`\n删除=${JSON.stringify(res)}`);
            });
            it('查询', async function () {
                const res = await ss.print.findShipAddress().then(res => res.result.data.rows);
                console.log(`\n查询=${JSON.stringify(res)}`);
                expect(res.find(obj => obj.id == shipAddress.id), `发货地址列表存在: ${shipAddress.id}`).to.be.undefined;
            });
            it('查询:根据id', async function () {
                const res = await ss.print.getShipAddressById({ shipAddressId: shipAddress.id }).then(res => res.result.data);
                console.log(`\n查询: 根据id = ${JSON.stringify(res)}`);
                shipAddress.flag = -1;
                common.isApproximatelyEqualAssert(shipAddress, res);
            });
        });
    });

    describe('打印机', async function () {
        before('新增', async function () {
            printer = {
                printWay: common.getRandomNum(1, 2),
                printerType: common.getRandomStr(5),
                printerCode: common.getRandomStr(10),
                printerName: common.getRandomChineseStr(3),
            };
            console.log(printer);
            const id = await ss.print.savePrinterSetting(printer).then(res => res.result.data.id);
            console.log(`\n新建=${JSON.stringify(id)}`);
            printer.id = id;
        });
        it('查询', async function () {
            if (printer.printWay == 2) {
                const res = await ss.print.findAvailablePrinters().then(res => res.result.data.rows);
                console.log(`\n查询=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(printer, res.find(obj => obj.id == printer.id));
            }
        });
        it('查询:根据打印方式', async function () {
            const res = await ss.print.findAvailablePrinters({ printWay: printer.printWay }).then(res => res.result.data.rows);
            console.log(`\n查询:根据打印方式=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(printer, res.find(obj => obj.id == printer.id));
        });
        it('查询:根据id', async function () {
            const res = await ss.print.getPrinterById({ printerId: printer.id }).then(res => res.result.data);
            console.log(`\n查询:根据id=${JSON.stringify(res)}`);
            common.isApproximatelyEqualAssert(printer, res);
        });
        describe('编辑', async function () {
            before('编辑', async function () {
                const printer2 = {
                    printWay: common.getRandomNum(1, 2),
                    printerType: common.getRandomStr(5),
                    printerCode: common.getRandomStr(10),
                    printerName: common.getRandomChineseStr(3),
                };
                Object.assign(printer, printer2);
                console.log(printer);
                const res = await ss.print.savePrinterSetting(printer);
                console.log(`\n编辑=${JSON.stringify(res)}`);
            });
            it('查询', async function () {
                if (printer.printWay == 2) {
                    const res = await ss.print.findAvailablePrinters().then(res => res.result.data.rows);
                    console.log(`\n查询=${JSON.stringify(res)}`);
                    common.isApproximatelyEqualAssert(printer, res.find(obj => obj.id == printer.id));
                }
            });
            it('查询:根据打印方式', async function () {
                const res = await ss.print.findAvailablePrinters({ printWay: printer.printWay }).then(res => res.result.data.rows);
                console.log(`\n查询:根据打印方式=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(printer, res.find(obj => obj.id == printer.id));
            });
            it('查询:根据id', async function () {
                const res = await ss.print.getPrinterById({ printerId: printer.id }).then(res => res.result.data);
                console.log(`\n查询:根据id=${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(printer, res);
            });
        });
        describe('打印', async function () {
            let expSheetAccountRes, shipAddressRes;
            before('获取电子面单账户和发货地址', async function () {
                expSheetAccountRes = await ss.print.findExpSheetAccount().then(res => res.result.data.rows[0]);
                shipAddressRes = await ss.print.findShipAddress().then(res => res.result.data.rows[0]);
                console.log(expSheetAccountRes);
                console.log(shipAddressRes);
            });
            it('执行打快递面单打印', async function () {
                const res = await ss.print.findAvailablePrinters({
                    expName: expSheetAccountRes.expName,
                    expId: expSheetAccountRes.expId,
                    shipId: shipAddressRes.id,
                    printerName: printer.printerName,
                    printerId: printer.id,
                    recData: [{
                        billId: 391070,
                        mobile: 12200000000,
                        shipName: 'luxx',
                        province: shipAddressRes.province,
                        city: shipAddressRes.province,
                        district: shipAddressRes.province,
                        detail: shipAddressRes.province
                    }]
                });
                console.log(`\n执行打快递面单打印=${JSON.stringify(res)}`);
            });
            it('获取打印数据', async function () {
                const res = await ss.print.getPrintData();
                console.log(`\n获取打印数据=${JSON.stringify(res)}`);
            });
            it('不更换单号打印面单', async function () {
                const res = await ss.print.executeOriginalPrint({ billIds: [391070] });
                console.log(`\n不更换单号打印面单=${JSON.stringify(res)}`);
            });
        });
        describe('断开', async function () {
            before('断开', async function () {
                const res = await ss.print.delPrinter({ printerId: printer.id });
                console.log(`\n删除=${JSON.stringify(res)}`);
            });
            it('查询:根据打印方式', async function () {
                const res = await ss.print.findAvailablePrinters({ printWay: printer.printWay }).then(res => res.result.data.rows);
                console.log(`\n查询=${JSON.stringify(res)}`);
                expect(res.find(obj => obj.id == printer.id), `打印机列表存在: ${printer.id}`).to.be.undefined;
            });
            it('查询:根据id', async function () {
                const res = await ss.print.getPrinterById({ printerId: printer.id }).then(res => res.result.data);
                console.log(`\n查询: 根据id = ${JSON.stringify(res)}`);
                printer.flag = -1;
                common.isApproximatelyEqualAssert(printer, res);
            });
        });
    });

});