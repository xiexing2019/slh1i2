const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const superagent = require('superagent');
const ssCaps = require('../../../reqHandler/ss/ssCaps');
const caps = require('../../../data/caps');

describe.skip('钱包-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
    });
    it.skip('创建收款单', async function () {
        const res = await ss.fin.createPay({ payMoney: 1 });
        console.log(`\n创建收款单=${JSON.stringify(res)}`);

    });

    describe('提现', async function () {
        let finCashId, finCashMoney, finCashBillDetails, exp, finAcctFlowDailymoney;
        /** 钱包首页数据 */
        let homePagedata = {
            useAcctId: 0,
            /**总金额 */
            totalMoney: 0,
            /**可提现金额 */
            withdrawableMoney: 0,
            /**待结算金额 */
            settlementMoney: 0,
            /**银行处理中金额 */
            bankDealingMoney: 0,
            /**已提现金额 */
            totalWithdrawMenoy: 0
        };
        before(async function () {
            // 获取钱包首页初始值
            const homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(`homePageMoney=${JSON.stringify(homePageMoney)}`);
            Object.assign(homePagedata, homePageMoney);
            console.log(homePagedata);
            // 获取财务流水日结概览
            const res = await ss.fin.getFinAcctFlowDailyOverview({ ioFlag: 1, dateType: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            console.log(`=${JSON.stringify(res.result)}`);
            finAcctFlowDailymoney = res.result.data.data[common.getCurrentDate()].find(ele => ele.tradeType == '201') ? res.result.data.data[common.getCurrentDate()].find(ele => ele.tradeType == '201').money : 0;
            // res.result.data.data[common.getCurrentDate()][0].money;
            console.log(finAcctFlowDailymoney);
            // if (homePagedata.withdrawableMoney == 0) {
            //     console.warn(`可提现金额为0:${JSON.stringify(homePagedata)}`);
            //     this.parent ? this.parent.skip() : this.skip();
            // }
            finCashMoney = homePagedata.withdrawableMoney < 1 ? homePagedata.withdrawableMoney : 1;
            console.log(`finCashMoney=${JSON.stringify(finCashMoney)}`);
            if (caps.name != 'ss_online') {
                finCashId = await ss.fin.createFinCashBill({ money: finCashMoney }).then(res => {
                    console.log(res);
                    return res.result.data.val;
                });
            } else {
                finCashMoney = 0;
            }
            // console.log(finCashId, finCashmoney);
            exp = {
                bankDealingMoney: parseFloat((homePagedata.bankDealingMoney + finCashMoney).toFixed(2)),
                // useAcctId: homePagedata.useAcctId,
                totalWithdrawMenoy: parseFloat((homePagedata.totalWithdrawMenoy + finCashMoney).toFixed(2)),
                totalMoney: parseFloat((homePagedata.totalMoney - finCashMoney).toFixed(2)),
                settlementMoney: homePagedata.settlementMoney,
                withdrawableMoney: parseFloat((homePagedata.withdrawableMoney - finCashMoney).toFixed(2))
            }
            console.log(exp);
        });
        it('获取提现详情-offline', async function () {
            finCashBillDetails = await ss.fin.getFinCashBillDetails({ id: finCashId }).then(res => res.result.data);
            console.log(`\n finCashBillDetails = ${JSON.stringify(finCashBillDetails)}`);
            expect(exp.totalMoney, `剩余金额与预期${exp.totalMoney}不符`).to.equal(finCashBillDetails.remainMoney);
        });
        it('获取提现记录-offline', async function () {
            if (!finCashBillDetails) this.skip();
            const finCashBill = await ss.fin.getFinCashBill({ billNo: finCashBillDetails.billNo }).then(res => res.result.data.rows);
            console.log(`\n finCashBill = ${JSON.stringify(finCashBill)}`);
            expect(exp.totalMoney, '剩余金额与预期不符').to.equal(finCashBill[0].remainMoney);
        });
        it('首页金额', async function () {
            const homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            // common.isApproximatelyEqualAssert(exp, homePageMoney);
        });
        it('按日期获取账户日结总金额', async function () {
            const res = await ss.fin.getFinAcctDaily({ startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            console.log(`\n res = ${JSON.stringify(res)}`);
            // expect(exp.totalMoney, 'endBalMoney与预期不符').to.equal(res.result.data.rows[0].endBalMoney);
        });
        it('获取账户流水', async function () {
            const finAcctFlow = await ss.fin.getFinAcctFlow({ startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            // console.log(`\n finAcctFlow = ${JSON.stringify(finAcctFlow)}`);
            expect(exp.totalMoney, 'BalMoney与预期不符').to.equal(finAcctFlow.result.data.rows[0].data[0].balMoney);
            const res = await ss.fin.getFinAcctFlow({ orderId: finAcctFlow.result.data.rows[0].data[0].bizOrderDetailId, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            // console.log(`\n res = ${JSON.stringify(res)}`);
            expect(exp.totalMoney, 'orderId查询结果BalMoney与预期不符').to.equal(res.result.data.rows[0].data[0].balMoney);
        });
        it('获取财务流水日结概览', async function () {
            const res = await ss.fin.getFinAcctFlowDailyOverview({ ioFlag: 1, dateType: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            console.log(`\n res = ${JSON.stringify(res)}`);
            expect(finAcctFlowDailymoney + finCashMoney, 'endBalMoney与预期不符').to.equal(res.result.data.data[common.getCurrentDate()].find(ele => ele.tradeType == '201').money);
        });
        it('获取财务流水日结', async function () {
            //只能查<=昨天的数据 by:骆兴栋
            const res = await ss.fin.getFinAcctFlowDaily({ ioFlag: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
            console.log(`\n res = ${JSON.stringify(res)}`);
            expect(0, '财务流水日结当天的应为空').to.equal(res.result.data.total);
        });
        for (const ioFlag of [0, 1]) {
            it('获取流水总金额', async function () {
                // "tradeType": "交易类型100 经营性收入,101  经营性支出(如采购)，预留，暂时不用,103 手续费支出,105-退款支出,200-充值,201-提现,202-转入,203-转出"
                const finAcctFlowDailyOverview = await ss.fin.getFinAcctFlowDailyOverview({ ioFlag: ioFlag, dateType: 1, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() }).then(res => res.result.data.totalMoney);
                console.log(`\n finAcctFlowDailyOverview = ${JSON.stringify(finAcctFlowDailyOverview)}`);
                const res = await ss.fin.getTotalFlowMoney({ ioFlag: ioFlag, startDate: common.getCurrentDate(), endDate: common.getCurrentDate() });
                console.log(`\n res = ${JSON.stringify(res)}`);
                expect(finAcctFlowDailyOverview, '获取流水总金额与预期不符').to.equal(res.result.data.val);
            });
        }
        it('重新提现', async function () {
            const res = await ss.fin.afreshWithdraw({ check: false, id: finCashId });
            console.log(res);
            expect(res.result, '获取流水总金额与预期不符').to.include({ msg: "提现单不是提现驳回状态，不能重新提现" }, { msgId: "fin_cash_bill_flag_is_not_reject_so_not_afresh_withdraw" });
        });
    });

    describe('钱包定时器每日统计-offline', async function () {
        let con, dbResult, flag1 = false, flag2 = false;
        before(async function () {
            con = await ss.creatSqlPool({ dbName: 'spbMysql' });
            dbResult = await con.query(`SELECT io_flag AS ioFlag, SUM(money) AS money ,trade_type AS tradeType FROM spb002.fin_acct_flow WHERE unit_id = ${sellerInfo.unitId} AND pro_time LIKE '${common.getDateString([0, 0, -1])}%' GROUP BY io_flag ,trade_type ORDER BY io_flag ,trade_type`).then(res => res[0]);
            console.log(`\n dbResult=${JSON.stringify(dbResult)}`);
        });
        after(async function () {
            await con.end();
        });
        it('获取财务流水日结:收入', async function () {
            const res = await ss.fin.getFinAcctFlowDaily({ ioFlag: 0, startDate: common.getDateString([0, 0, -1]), endDate: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
            console.log(`\n res = ${JSON.stringify(res)}`);
            try {
                let index = 0;
                dbResult.forEach(re => {
                    if (re.ioFlag == 0) {
                        common.isApproximatelyEqualAssert(re, res[index]);
                        index++;
                    }
                });
                flag1 = true;
            } catch (error) {
                flag1 = false
                throw error;
            }

        });
        it('获取财务流水日结:支出', async function () {
            const res = await ss.fin.getFinAcctFlowDaily({ ioFlag: 1, startDate: common.getDateString([0, 0, -1]), endDate: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
            console.log(`\n res = ${JSON.stringify(res)}`);
            try {
                let index = 0;
                dbResult.forEach(re => {
                    if (re.ioFlag == 1) {
                        common.isApproximatelyEqualAssert(re, res[index]);
                        index++;
                    }
                });
                flag2 = true;
            } catch (error) {
                flag2 = false;
                throw error;
            }
        });
        describe('执行定时器', async function () {
            before(async function () {
                console.log(flag1, flag2);
                if (flag1 && flag2) this.parent ? this.parent.skip() : this.skip();
                const res = await ss.test.initTask({ taskCode: 'FinAcctTask', unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                console.log(`\n 定时任务FinAcctTask ${JSON.stringify(res)}`);
                dbResult = await con.query(`SELECT io_flag AS ioFlag, SUM(money) AS money ,trade_type AS tradeType FROM spb002.fin_acct_flow WHERE unit_id = ${sellerInfo.unitId} AND pro_time LIKE '${common.getDateString([0, 0, -1])}%' GROUP BY io_flag ,trade_type ORDER BY io_flag ,trade_type`).then(res => res[0]);
                console.log(`\n dbResult=${JSON.stringify(dbResult)}`);
            });
            it('获取财务流水日结:收入', async function () {
                const res = await ss.fin.getFinAcctFlowDaily({ ioFlag: 0, startDate: common.getDateString([0, 0, -1]), endDate: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
                console.log(`\n res = ${JSON.stringify(res)}`);
                let index = 0;
                dbResult.forEach(re => {
                    if (re.ioFlag == 0) {
                        common.isApproximatelyEqualAssert(re, res[index]);
                        index++;
                    }
                });
            });
            it('获取财务流水日结:支出', async function () {
                const res = await ss.fin.getFinAcctFlowDaily({ ioFlag: 1, startDate: common.getDateString([0, 0, -1]), endDate: common.getDateString([0, 0, -1]) }).then(res => res.result.data.rows);
                console.log(`\n res = ${JSON.stringify(res)}`);
                let index = 0;
                dbResult.forEach(re => {
                    if (re.ioFlag == 1) {
                        common.isApproximatelyEqualAssert(re, res[index]);
                        index++;
                    }
                });
            });
        });
    });

});
