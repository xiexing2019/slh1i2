const common = require('../../../lib/common');
const pageFragment = require('../../data/pageFragment');
const wschelper = require('../../../reqHandler/ss/spb/spdresup');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];
const basicJson = require('../../help/basicJson');
const dresManage = require('../../help/dresManage');


describe('店铺装修-test', function () {
    this.timeout(30000);
    let tenantId, sellerInfo, clientInfo, template1, template2, template3, template4;

    before('卖家先登录', async function () {
        await ssReq.ssSellerLogin();
        //console.log(LOGINDATA);
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    /**
     * spb.sc_page_fragment
     * 返回所有默认模板 typeId=0
     * 将模板中的图片替换为主营类目的图片 拱客户选择(前端)
     */
    describe('默认模板', async function () {
        let dictList = [];
        before(async function () {
            dictList = await ss.config.getSpgDictList({ typeId: 2010, flag: 1 }).then(res => res.result.data.rows);
        });
        it('默认模板查询', async function () {
            const pageList = await ss.spdresup.getDefPageFragmentList().then(res => res.result.data.rows);
            // expect(pageList).to.have.lengthOf(dictList.length);
        });
    });

    describe('模板一', async function () {
        let template = {};
        before(async function () {
            template = pageFragment.template1;

            template.homeData[0].data[0].data.docId = docData.image.shopBackground[0, 3].docUrl;
            const dresGroup = await ss.spdresb.listGroup().then(res => res.result.data.rows.slice(0, 2));
            template.homeData.push({ type: 'groupList', typeName: 'groupList', title: '货品分组', data: dresGroup.map(group => { return { bizType: 5, data: { name: group.name, id: group.id }, link: { url: group.id } } }) });
            await wschelper.savePageFragment({ content: template });
            // await wschelper.savePageFragment({ content: { "template": { "id": 1, "name": "模板一" }, "colorStyle": { "id": 4, "colors": ["#865C30", "#C8C8D1"] }, "homeData": [{ "type": "banner", "typeName": "banner", "title": "首页Banner", "data": [{ "bizType": 1, "data": { "docId": "https://sschk.hzecool.com/doc/v2/content.do?id=docx156102259718727-ssdoc12d.jpg&thumbFlag=0" }, "link": { "url": "" } }] }, { "type": "groupList", "typeName": "groupList", "title": "货品分组", "data": [{ "bizType": 5, "data": { "name": "上新", "id": 947 }, "link": { "url": "947" } }, { "bizType": 5, "data": { "name": "爆款", "id": 951 }, "link": { "url": "951" } }] }] } });
        });
        it('装修预览', async function () {
            const pageFragment = await wschelper.getPageFragment({ templateVer: 2 }).then(res => JSON.parse(res.result.data.content));
            common.isApproximatelyEqualAssert(template, pageFragment);
        });

    });

    describe('自定义商品详情', async function () {
        const dres = dresManage.setupDres();
        before(async function () {
            await ssReq.ssSellerLogin();
            const spuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
            const dresFull = await sp.spdresb.getFullById({ id: spuList[0].id }).then(res => res.result.data);
            dres.setBySeller(dresFull);
        });
        describe('修改自定义商品详情:不显示', async function () {
            const status = { goodsFabricStatus: 0, goodsBrandStatus: 0, goodsInvStatus: 0, goodsSeasonStatus: 0, goodsSalesVolumeStatus: 0, goodsWeightStatus: 0 };
            before(async function () {
                await ssReq.ssSellerLogin();
                await dres.saveGoodsDetail(status);
            });
            it('自定义商品详情查看', async function () {
                const goodsDetail = await ss.spdresup.getGoodsDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });
            it('装修预览', async function () {
                await ssReq.userLoginWithWx();
                const goodsDetail = await ss.spdresup.getPageFragment({ buyerId: LOGINDATA.tenantId, shopTenantId: sellerInfo.tenantId }).then(res => res.result.data.goodsDetail);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });
            it('买家查询商品详情', async function () {
                const goodsDetail = await dres.getDetailForBuyer().then(res => res.result.data.goodsDetail);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });

        });
        describe('修改自定义商品详情:显示', async function () {
            const status = { goodsFabricStatus: 1, goodsBrandStatus: 1, goodsInvStatus: 1, goodsSeasonStatus: 1, goodsSalesVolumeStatus: 1, goodsWeightStatus: 1 };
            before(async function () {
                await ssReq.ssSellerLogin();
                await dres.saveGoodsDetail(status);
            });
            it('自定义商品详情查看', async function () {
                const goodsDetail = await ss.spdresup.getGoodsDetail().then(res => res.result.data);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });
            it('装修预览', async function () {
                await ssReq.userLoginWithWx();
                const goodsDetail = await ss.spdresup.getPageFragment({ buyerId: LOGINDATA.tenantId, shopTenantId: sellerInfo.tenantId }).then(res => res.result.data.goodsDetail);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });
            it('买家查询商品详情', async function () {
                const goodsDetail = await dres.getDetailForBuyer().then(res => res.result.data.goodsDetail);
                common.isApproximatelyEqualAssert(status, goodsDetail);
            });
        });

    });

    describe.skip('', async function () {
        it('添加图片', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template1 });
            await common.delay(500);
            template1 = pageFragment.template1;
            //console.log(template1.homeData[0].data[0]);
            const picdata = {
                "bizType": 1,
                "data": {
                    "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549627889925-spdoc10d.jpg&thumbFlag=0"
                },
                "link": {

                }
            }
            template1.homeData[0].data[0] = picdata;
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            //console.log(jsonTemplate);
            expect(jsonTemplate.homeData[0].data[0].data.docId, '添加图片失败').to.equal(picdata.data.docId);
        });
        it('添加活动', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template2 });
            await common.delay(500);
            template2 = pageFragment.template2;
            //console.log(template1.homeData[0].data[0]);
            const eventdata = {
                "bizType": 3,
                "data": {
                    "id": 153,
                    "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15515128254189-spdoc10d.png&thumbFlag=0"
                },
                "link": {
                    "url": 153
                }
            }
            template2.homeData[1].data[0] = eventdata;
            await wschelper.savePageFragment({ content: template2 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            //console.log(jsonTemplate);
            expect(jsonTemplate.homeData[1].data[0].data.docId, '添加活动失败').to.equal(eventdata.data.docId);
        });
        it('添加-删除商品', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template1 });
            await common.delay(500);
            template1 = pageFragment.template1;
            //console.log(template1.homeData[0].data[0]);
            const spudata = {
                "bizType": 2,
                "data": {
                    "id": 37189,
                    "unitId": 65
                },
                "link": {
                    "url": 37189
                }
            }
            template1.homeData[1].data[1].data = null;
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            console.log(jsonTemplate);
            expect(jsonTemplate.homeData[1].data[0].data.id, '删除商品失败').to.equal(spudata.data.id);
        });
        it('切换模板1', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            expect(jsonTemplate.template.id, '切换模板1失败').to.equal(1);
        });
        it('切换模板2', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template2 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            expect(jsonTemplate.template.id, '切换模板2失败').to.equal(2);
        });
        it('切换模板3', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template3 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            expect(jsonTemplate.template.id, '切换模板3失败').to.equal(3);
        });
        it('切换模板4', async function () {
            await wschelper.savePageFragment({ content: pageFragment.template4 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            expect(jsonTemplate.template.id, '切换模板4失败').to.equal(4);
        });
        it('切换风格1', async function () {
            template1 = pageFragment.template1;
            template1.colorStyle = { id: 1, colors: ['#FF4C62', '#C8C8D1'] }
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            await common.delay(500);
            expect(jsonTemplate.colorStyle.id, '切换风格1失败').to.equal(1);
        });
        it('切换风格2', async function () {
            template1 = pageFragment.template1;
            template1.colorStyle = { id: 2, colors: ['#404040', '#C8C8D1'] }
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            await common.delay(500);
            expect(jsonTemplate.colorStyle.id, '切换风格2失败').to.equal(2);
        });
        it('切换风格3', async function () {
            template1 = pageFragment.template1;
            template1.colorStyle = { id: 3, colors: ['#FFAE29', '#C8C8D1'] }
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            await common.delay(500);
            expect(jsonTemplate.colorStyle.id, '切换风格3失败').to.equal(3);
        });
        it('切换风格4', async function () {
            template1 = pageFragment.template1;
            template1.colorStyle = { id: 4, colors: ['#865C30', '#C8C8D1'] }
            await wschelper.savePageFragment({ content: template1 });
            await common.delay(500);
            const tempResult = await wschelper.getPageFragment();
            const newTemplate = tempResult.result.data.content;
            const jsonTemplate = JSON.parse(newTemplate);
            await common.delay(500);
            expect(jsonTemplate.colorStyle.id, '切换风格4失败').to.equal(4);
        });
        it.skip('添加已存在商品', async function () {
            //await wschelper.savePageFragment({content: pageFragment.template4});
        });
    });
});