const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const sp = require('../../../reqHandler/sp');

describe('消息中心', async function () {
    this.timeout(TESTCASE.timeout);
    const tagOneIn = '71,72,73,74,75,76,77,78';

    before(async function () {
        await ssReq.ssSellerLogin();
    });

    describe('更新所有未读消息为已读', async function () {
        before(async function () {
            await sp.spmdm.updateAllUnreadMsgs();
        });
        it('查看未读消息数', async function () {
            const unreadCount = await sp.spmdm.getPullUnreadCount({ tagOneIn: tagOneIn }).then(res => res.result.data.val);
            expect(unreadCount).to.equal(0);
        });
        it('查询消息列表', async function () {
            const msgList = await sp.spmdm.pullMessagesList({ tagOneIn: tagOneIn, unread: 1 }).then(res => res.result.data.dataList);
            expect(msgList).to.have.lengthOf(0);
        });
        it('查询消息列表', async function () {
            const msgList = await sp.spmdm.pullMessagesList({ tagOneIn: tagOneIn }).then(res => res.result.data.dataList);
            msgList.forEach(msg => expect(msg).to.includes({ unread: 0 }));
        });
    });
});