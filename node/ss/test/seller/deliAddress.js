'use strict';
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const spmdm = require('../../../reqHandler/sp/biz_server/spmdm');
// const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const basicJson = require('../../help/basicJson');
const spugr = require('../../../reqHandler/sp/global/spugr');
const format = require('../../../data/format');
const ssAccount = require('../../data/ssAccount');


describe('卖家收货地址-test', function () {
	this.timeout(30000);
	let areaJson;
	let userInfo;
	before(async function () {
		await ssReq.userLoginWithWx();
		userInfo = _.cloneDeep(LOGINDATA);
	});
	describe('用户收货地址管理', function () {
		let saveUserRecInfoRes, recInfoId;
		before(async function () {
			areaJson = await basicJson.getAreaJson();
			saveUserRecInfoRes = await spmdm.saveUserRecInfo(await basicJson.addAddrJson(areaJson));
			recInfoId = saveUserRecInfoRes.result.data.val;
		});

		after('设置一个默认的收货地址', async function () {
			let json = await basicJson.addAddrJson(areaJson);
			json.recInfo.isDefault = 1;
			json.address.provinceCode = 110000;   //这里省市区写死，为了保证此地有运费规则可以覆盖到
			json.address.cityCode = 110100;
			json.address.countyCode = 110101;
			await spmdm.saveUserRecInfo(json);
		})

		it('1.获取用户收货地址列表验证', async function () {
			const recInfoList = await spmdm.getUserRecInfoList({ type: 1 });
			const info = recInfoList.result.data.rows.find(element => element.recInfo.id == recInfoId);

			if (info == undefined) {
				throw new Error('查询失败');
			} else {
				common.isApproximatelyEqualAssert(saveUserRecInfoRes.params.jsonParam, info, []);
				common.isApproximatelyEqualAssert(areaJson, info.address.ecCaption);
			};
		});


		it('2.收货地址详情查看验证', async function () {
			let getUserRecInfo = await spmdm.getUserRecInfoById({
				id: recInfoId
			});
			common.isApproximatelyEqualAssert(saveUserRecInfoRes.params.jsonParam, getUserRecInfo.result.data);
		});

		it('3.修改用户收货地址验证', async function () {

			//修改数据构造，这里修改地址、手机号、联系人
			let updateRecInfoJson = saveUserRecInfoRes.params.jsonParam;
			updateRecInfoJson.recInfo.id = saveUserRecInfoRes.result.data.val;
			updateRecInfoJson.address.detailAddr = `九堡${common.getRandomStr(5)}`;
			updateRecInfoJson.recInfo.linkMan = common.getRandomChineseStr(3);
			updateRecInfoJson.recInfo.telephone = common.getRandomMobile();
			//执行修改操作
			let updateRecInfoRes = await spmdm.saveUserRecInfo(updateRecInfoJson);
			let userRecInfoList = await spmdm.getUserRecInfoList({ type: 1 });

			//验证List列表
			for (let i = 0; i < userRecInfoList.result.data.rows.length; i++) {
				if (userRecInfoList.result.data.rows[i].recInfo.id == saveUserRecInfoRes.result.data.val) {
					common.isApproximatelyEqualAssert(updateRecInfoRes.params.jsonParam, userRecInfoList.result.data.rows[i], [], '');
					break;
				};
			};

			let getUserRecInfoById = await spmdm.getUserRecInfoById({
				id: saveUserRecInfoRes.result.data.val
			});
			common.isApproximatelyEqualAssert(updateRecInfoRes.params.jsonParam, getUserRecInfoById.result.data, [], '');
		});


		it('4.设置默认的收货地址验证', async function () {
			await spmdm.setDefaultUserRecInfo({
				id: saveUserRecInfoRes.result.data.val
			});
			let userRecInfoList = await spmdm.getUserRecInfoList({ type: 1 });

			for (let i = 0; i < userRecInfoList.result.data.rows.length; i++) {
				if (userRecInfoList.result.data.rows[i].recInfo.id == saveUserRecInfoRes.result.data.val) {
					expect(userRecInfoList.result.data.rows[i].recInfo.isDefault, '设置默认地址失败').to.be.equal(1);
				} else {
					expect(userRecInfoList.result.data.rows[i].recInfo.isDefault, '设置默认地址失败').to.be.equal(0);
				};
			};
			let userRecInfoById = await spmdm.getUserRecInfoById({
				id: saveUserRecInfoRes.result.data.val
			});

			expect(userRecInfoById.result.data.recInfo.isDefault, '设置默认值失败').to.be.equal(1);
		});
		// TODO 结构拆分细化
		it('5.删除收货地址验证', async function () {
			let userRecListBefore = await spmdm.getUserRecInfoList({ type: 1 });
			let saveUserRecInfoRes = await spmdm.saveUserRecInfo(await basicJson.addAddrJson(areaJson));
			await spmdm.deleteUserRecInfo({
				id: saveUserRecInfoRes.result.data.val
			});
			let lastgetUserRecInfoList = await spmdm.getUserRecInfoList({ type: 1 });
			common.isApproximatelyEqualAssert(userRecListBefore.result, lastgetUserRecInfoList.result);
		});

		it('6.获取用户默认收货地址验证', async function () {
			let saveUserRecInfoRes = await spmdm.saveUserRecInfo(await basicJson.addAddrJson(areaJson));
			await spmdm.setDefaultUserRecInfo({
				id: saveUserRecInfoRes.result.data.val
			});
			let getUserDefaultRecInfo = await spmdm.getUserDefaultRecInfo();
			expect(getUserDefaultRecInfo.result.data.recInfo.isDefault, '获取失败').to.be.equal(1);
		});

	});

	describe.skip('用户退货地址管理', function () {
		let saveRes;
		before('查询退货地址', async function () {
			await ssReq.spSellerLogin();
			let res = await spmdm.getUserRecInfoList({ type: 2 }).then(res => res.result.data.rows);

			if (res[0].recInfo.returnAddrType == 2) {
				common.isApproximatelyArrayAssert(res, await defaultReturnAddr());
			};

			//修改退货地址
			let json = await basicJson.addAddrJson(areaJson);
			json.recInfo.id = res[0].recInfo.id;
			json.recInfo.type = 2;
			json.recInfo.returnAddrType = 1;
			saveRes = await spmdm.saveUserRecInfo(json);
		});

		it('查看退货地址列表', async function () {
			let returnAddrList = await spmdm.getUserRecInfoList({ type: 2 }).then(res => res.result.data.rows);
			common.isApproximatelyArrayAssert(returnAddrList[0], saveRes.params.jsonParam);
		});

		it('查询退货地址详情', async function () {
			let returnAddrInfo = await spmdm.getUserRecInfoById({
				id: saveRes.result.data.val
			});
			common.isApproximatelyEqualAssert(saveRes.params.jsonParam, returnAddrInfo.result.data);
		});
	});
});

async function defaultReturnAddr() {
	let shopInfo = await spugr.getShopDetail().then(res => res.result.data);
	let str = 'provinceCode=provCode;cityCode=cityCode;countyCode=areaCode;';
	let address = format.dataFormat(shopInfo, 'provinceCode=provCode;cityCode=cityCode;countyCode=areaCode');
	return [{
		address: address,
		recInfo: {
			userId: LOGINDATA.userId,
			returnAddrType: 2,
			unitId: LOGINDATA.unitId,
		}
	}];
}