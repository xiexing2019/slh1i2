const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const billManage = require('../../../help/billManage');
const moment = require('moment');

describe('数据统计-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, con;
    before(async function () {
        con = await ss.creatSqlPool({ dbName: 'spbMysql' });
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

    });
    after(async function () {
        await con.end();
    });
    it('流量趋势统计:7天', async function () {
        const res = await ss.ssbi.getFlowTrendStat({ dateType: 0 }).then(res => res.result.data);
        console.log(`\n res=${JSON.stringify(res)}`);
        await assertResult(con, res, sellerInfo, 7);
    });
    it('流量趋势统计:30天', async function () {
        const res = await ss.ssbi.getFlowTrendStat({ dateType: 1 }).then(res => res.result.data);
        console.log(`\n res=${JSON.stringify(res)}`);
        await assertResult(con, res, sellerInfo, 30);
    });
    it('流量趋势统计:90天', async function () {
        const res = await ss.ssbi.getFlowTrendStat({ dateType: 2 }).then(res => res.result.data);
        console.log(`\n res=${JSON.stringify(res)}`);
        await assertResult(con, res, sellerInfo, 90);
    });
    it('每日每月流量统计:每日', async function () {
        const res = await ss.ssbi.getFlowStatByType({ dateType: 0 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('每日每月流量统计:每月', async function () {
        const res = await ss.ssbi.getFlowStatByType({ dateType: 1 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('交易趋势统计:7天', async function () {
        const res = await ss.ssbi.getTradeTrendStat({ dateType: 0 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('交易趋势统计:30天', async function () {
        const res = await ss.ssbi.getTradeTrendStat({ dateType: 0 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('交易趋势统计:90天', async function () {
        const res = await ss.ssbi.getTradeTrendStat({ dateType: 0 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('交易趋势统计:每日', async function () {
        const res = await ss.ssbi.getTradeStatByType({ dateType: 0 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    it('交易趋势统计:每月', async function () {
        const res = await ss.ssbi.getTradeStatByType({ dateType: 1 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });

});

assertResult = async function (con, res, sellerInfo, day = 7) {
    const result01 = await con.query(`SELECT sum(visitor_num) AS totalVisitorNum FROM up001.ss_shop_daily_report WHERE unit_id = ${sellerInfo.unitId} AND date_sub(curdate(), INTERVAL ${day} DAY) <= date(report_date) AND  to_days(report_date) != to_days(now())`).then(res => res[0][0]);
    console.log(`\n result01=${JSON.stringify(result01)}`);

    const result02 = await con.query(`SELECT count(distinct member_id) AS totalViews FROM up001.ss_buyer_visited_log 
    WHERE unit_id = ${sellerInfo.unitId} AND date_sub(curdate(), INTERVAL ${day} DAY) <= date(visit_time) 
    AND  to_days(visit_time) != to_days(now())`).then(res => res[0][0]);
    console.log(`\n result02=${JSON.stringify(result02)}`);
    common.isApproximatelyEqualAssert(res, Object.assign(result01, result02));

    res = res.resultMap;
    const result1 = await con.query(`SELECT report_date, visitor_num AS visitorNum FROM up001.ss_shop_daily_report WHERE unit_id = ${sellerInfo.unitId} AND date_sub(curdate(), INTERVAL ${day} DAY) <= date(report_date) AND  to_days(report_date) != to_days(now()) ORDER BY report_date`).then(res => res[0]);
    result1.forEach(re => {
        re.report_date = common.utcToLocaleDate(re.report_date, 'MM/dd');
    });
    console.log(`\n result1=${JSON.stringify(result1)}`);
    console.log(result1.length);
    const result2 = await con.query(`SELECT date(visit_time) report_date, count(distinct member_id) AS views FROM up001.ss_buyer_visited_log 
        WHERE unit_id = ${sellerInfo.unitId} AND date_sub(curdate(), INTERVAL ${day} DAY) <= date(visit_time) 
        AND  to_days(visit_time) != to_days(now()) GROUP BY report_date ORDER BY report_date`).then(res => res[0]);
    result2.forEach(re => {
        re.report_date = common.utcToLocaleDate(re.report_date, 'MM/dd');
    });
    console.log(`\n result2=${JSON.stringify(result2)}`);
    console.log(result2.length);
    if (day == 7) {
        result1.forEach(re => {
            console.log(res[re.report_date]);
            expect(res[re.report_date], `日期${re.report_date}的数据不存在`).to.not.be.undefined;
            common.isApproximatelyEqualAssert(re, res[re.report_date])
        });
        result2.forEach(re => {
            console.log(res[re.report_date]);
            common.isApproximatelyEqualAssert(re, res[re.report_date])
        });
    } else if (day == 30 || day == 90) {
        let flag, days = [];
        if (day == 30) {
            flag = 5;
        } else {
            flag = 15;
        }
        for (let index = day; index > 0; index = index - flag) {
            const beginDay = common.utcToLocaleDate(moment().subtract(index, 'days'), 'MM/dd');
            if (index > flag) {
                const endDay = common.utcToLocaleDate(moment().subtract(index - flag, 'days'), 'MM/dd');
                days.push(beginDay + '-' + endDay)
            } else {
                const endDay = common.utcToLocaleDate(moment().subtract(index - flag + 1, 'days'), 'MM/dd');
                days.push(beginDay + '-' + endDay)
            }
        }
        console.log(days);
        let exp = { visitorNum: 0, views: 0 };
        days.forEach(d => {
            exp.visitorNum = 0;
            exp.views = 0;
            Object.assign(exp, {})
            const arr = d.split('-');
            result1.forEach(re => {
                if (re.report_date >= arr[0] && re.report_date <= arr[1]) {
                    exp.visitorNum += re.visitorNum;
                    console.log(exp.visitorNum);
                } else {
                    console.log('没有这天的数据');
                }
            });
            result2.forEach(re => {
                if (re.report_date >= arr[0] && re.report_date <= arr[1]) {
                    exp.views += re.views;
                    console.log(exp.views);
                } else {
                    console.log('没有这天的数据');
                }
            });
            console.log(`day=${d}`, `exp=${JSON.stringify(exp)}`, `res[${d}]=${JSON.stringify(res[d])}`);
            expect(res[d].visitorNum, `日期${d}的数据不存在`).not.to.be.undefined;
            expect(res[d].visitorNum, `日期${d}的数据与预期不一致`).to.equal(exp.visitorNum);
            expect(res[d].views, `日期${d}的数据不存在`).not.to.be.undefined;
            expect(res[d].views, `日期${d}的数据与预期不一致`).to.equal(exp.views);
        });
        //昨天
        const yesterday = common.utcToLocaleDate(moment().subtract(1, 'days'), 'MM/dd');
        console.log(`yesterday=${yesterday}`);
        if (result1[result1.length - 1].report_date == yesterday) {
            expect(res[yesterday].visitorNum, `日期${yesterday}的visitorNum与预期不一致`).to.equal(result1[result1.length - 1].visitorNum);
        } else {
            console.log('没有昨天');
        }
        if (result2[result2.length - 1].report_date == yesterday) {
            expect(res[yesterday].views, `日期${yesterday}的views与预期不一致`).to.equal(result2[result2.length - 1].views);
        } else {
            console.log('没有昨天');
        }
    }
};