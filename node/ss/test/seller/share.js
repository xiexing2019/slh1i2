const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');

describe('卖家分享', function () {
    this.timeout(TESTCASE.timeout);
    let spuList, spuIds;
    before('卖家登录', async function () {
        await ssReq.ssSellerLogin();
        spuList = await sp.spdresb.findSellerSpuList().then(res => res.result.data.rows.map(obj => obj.id));
        spuIds = common.randomSort(spuList).slice(0, 3).toString();
    });
    it('获取单个商品分享二维码', async function () {
        const codeBaseRes = await ss.spmdm.getBatchShareQrCodeBase64ForWechat({ type: 'goods', spuIds: common.randomSort(spuList)[0] }).then(res => res.result.data.rows[0][res.params.spuIds]);
        expect(codeBaseRes, `分享商品失败`).to.be.include('data:image/png;base64');
    });
    it('获取单个分享菊花码', async function () {
        const codeBaseRes = await ss.spmdm.getBatchShareQrCodeBase64ForWechat({ type: 'goods', spuIds: common.randomSort(spuList)[0], isAppCode: true }).then(res => res.result.data.rows.find(obj => obj.spuId == res.params.spuIds));
        expect(codeBaseRes, `分享商品菊花码生成失败`).not.to.be.undefined;
    });
    it('获取多个商品分享二维码', async function () {
        const codeBaseRes = await ss.spmdm.getBatchShareQrCodeBase64ForWechat({ type: 'goods', spuIds: spuIds }).then(res => res.result.data.rows);
        codeBaseRes.forEach(obj => {
            expect(Object.values(obj).toString(), `分享商品失败`).to.be.include('data:image/png;base64');
        });
    });
    it('获取多个商品分享菊花码', async function () {
        const codeBaseRes = await ss.spmdm.getBatchShareQrCodeBase64ForWechat({ type: 'goods', spuIds: spuIds, isAppCode: true }).then(res => res.result.data.rows);
        codeBaseRes.forEach(obj => {
            expect(obj, `分享商品菊花码生成失败`).not.to.be.undefined;
        });
    });
});