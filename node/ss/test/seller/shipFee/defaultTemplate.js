const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const MyShop = require('../../../help/shop/myShop');
const shipFee = require('../../../help/shop/manage/shipFee');
const dresManage = require('../../../help/dresManage');
const basicJson = require('../../../help/basicJson');


describe('运费管理', function () {
    this.timeout(TESTCASE.timeout);
    const shopInfo = new MyShop();
    before(async function () {
        await shopInfo.switchSeller();
        await shopInfo.getShipFee();
        console.log(shopInfo);
        // console.log(shopInfo.shipFee);
    });
    after('删除模板', async function () {
        await shopInfo.switchSeller();
        const templateIds = [...shopInfo.shipFee.templateMap.keys()];
        for (let index = 0; index < templateIds.length - 2; index++) {
            // if (shopInfo.shipFee.defaultTemplateId == templateIds[index]) continue;
            await shopInfo.shipFee.deleteTemplate(templateIds[index]);
        }
    });
    describe('默认模板', async function () {
        const dresByApp = dresManage.setupDres({ type: 'app' });
        const dres = dresManage.setupDres();
        let templateId;
        before('新建模板并设为默认模板', async function () {
            const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 3, feeType: common.getRandomNum(1, 2) }, freeRule: { count: 2, shipFreeType: common.getRandomNum(1, 2) } });
            console.log(`\ntemplateJson=${JSON.stringify(templateJson)}`);
            templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
            await shopInfo.shipFee.updateFeeDefaultTemplate(templateId);
        });
        it('运费模板列表', async function () {
            const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), 0);
        });
        it('运费模板详情', async function () {
            const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
        });
        it('原运费、包邮规则', async function () {
            const shipRule = await sp.spconfb.findShipRule().then(res => res.result.data);
            shipRule.id = templateId;
            shipRule.spFeeRules = shipRule.feeRules;
            shipRule.spShipFeeRules = shipRule.freeRules;
            for (const freeRule of shipRule.spShipFeeRules) freeRule.shipProvIds = freeRule.provIds;
            console.log(`\n原运费、包邮规则=${JSON.stringify(shipRule)}`);
            shopInfo.shipFee.shipTemplateAssert(shipRule);
        });
        describe('独立app商品', async function () {
            before('新增商品', async function () {
                await dresManage.prePrepare();
                const dresJson = basicJson.styleJsonByApp({}, (spu) => {
                    delete spu.templateId;
                    spu.weight = 0.5;
                });
                // console.log(`\ndresJson=${JSON.stringify(dresJson)}`);
                await dresByApp.saveDresByApp(dresJson);
                // console.log(dres.spu);
                shopInfo.shipFee.updateTemplateCount(templateId, 1);
            });
            it('商品详情', async function () {
                const dresDetail = await dresByApp.getFullById();
                common.isApproximatelyEqualAssert(dresByApp.spu, dresDetail.spu);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
            });
        });
        describe('商品', async function () {
            before('新增商品', async function () {
                await dresManage.prePrepare();
                const dresJson = basicJson.styleJson({}, (spu) => {
                    delete spu.templateId;
                    spu.weight = 0.5;
                });
                await dres.saveDres(dresJson);
                // console.log(dres.spu);
                shopInfo.shipFee.updateTemplateCount(templateId, 1);
            });
            it('商品详情', async function () {
                const dresDetail = await dres.getFullById();
                common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
            });
        });
        // 部分用例依赖:独立app商品、商品
        describe('变更默认模板', async function () {
            let templateId2;
            before('新建模板并设为默认模板', async function () {
                const templateJson = await shipFee.mockTemplateFeeRuleJson();
                console.log(`\ntemplateJson=${JSON.stringify(templateJson)}`);
                templateId2 = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
                await shopInfo.shipFee.updateFeeDefaultTemplate(templateId2);
                console.log(shopInfo.shipFee);
                // await shopInfo.shipFee.updateFeeDefaultTemplate(shopInfo.shipFee.defaultTemplate.get(-3).pId, -3);
                console.log(shopInfo.shipFee);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId2);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId2), 0);
            });
            it('运费模板详情', async function () {
                const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId2);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
            });
            it('独立app商品', async function () {
                const dresDetail = await dresByApp.getFullById();
                common.isApproximatelyEqualAssert(dresByApp.spu, dresDetail.spu);
            });
            it('商品', async function () {
                const dresDetail = await dres.getFullById();
                common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
            });
            it('运费模板列表:旧默认模板', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), 0);
            });
            it('运费模板详情:旧默认模板', async function () {
                const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
            });
            describe('删除默认模板', async function () {
                before(async function () {
                    await shopInfo.shipFee.deleteTemplate(templateId2);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                    expect(templateFeeRules.find(obj => obj.id == templateId2), `运费模板列表存在模板${templateId2}`).to.be.undefined;
                    expect(shopInfo.shipFee.defaultTemplate.get(-1).pId).to.equal(shopInfo.shipFee.defaultTemplateId);
                });
            });
        });
    });
    describe('设置到付为默认模板', async function () {
        const templateId = -3;
        before('设置到付为默认模板', async function () {
            console.log(shopInfo.shipFee);
            await shopInfo.shipFee.updateFeeDefaultTemplate(shopInfo.shipFee.defaultTemplate.get(templateId).pId, -3);
        });
        it('运费模板列表', async function () {
            const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
            console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), 0);
        });
    });
});