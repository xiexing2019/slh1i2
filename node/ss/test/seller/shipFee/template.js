const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const MyShop = require('../../../help/shop/myShop');
const shipFee = require('../../../help/shop/manage/shipFee');


describe('运费管理', function () {
    this.timeout(TESTCASE.timeout);
    const shopInfo = new MyShop();
    before(async function () {
        await shopInfo.switchSeller();
        await shopInfo.getShipFee();
        console.log(shopInfo.shipFee);
        // console.log(ship);
    });
    after('删除模板', async function () {
        await shopInfo.switchSeller();
        const templateIds = [...shopInfo.shipFee.templateMap.keys()];
        for (let index = 0; index < templateIds.length - 2; index++) {
            // if (shopInfo.shipFee.defaultTemplateId == templateIds[index]) continue;
            await shopInfo.shipFee.deleteTemplate(templateIds[index]);
        }
    });
    describe('区域省份', async function () {
        let districtRegion;
        before('获取区域省份', async function () {
            districtRegion = await ss.config.getDistrictRegion().then(res => res.result.data.rows);
            console.log(`\n区域省份=${JSON.stringify(districtRegion)}`);
            expect(districtRegion, '区域省份为空').not.to.be.empty;
        });
        it('验证层级结构', async function () {
            districtRegion.forEach(region => {
                expect(region, `地区${region}不存在预期key值`).to.have.all.keys('provices', 'regionCode', 'regionName');
                region.provices.forEach(provice => {
                    expect(provice, `省份(直辖市，自治区等)${provice}不存在预期key值`).to.have.all.keys('codeName', 'codeValue');
                });
            });
        });
    });
    describe('自定义运费模板', async function () {
        let templateId;
        before('新增运费模板', async function () {
            const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 3, feeType: common.getRandomNum(1, 2) }, freeRule: { count: 2, shipFreeType: common.getRandomNum(1, 2) } });
            // console.log(`\ntemplateJson=${JSON.stringify(templateJson)}`);
            templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
            // console.log(shopInfo);
            // console.log(shopInfo.shipFee.templateMap.get(templateId));
        });
        it('运费模板列表', async function () {
            const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), 0);
        });
        it('运费模板详情', async function () {
            const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
        });

        describe('修改运费模板规则:新增规则', async function () {
            describe('新增计费规则', async function () {
                before('新增计费规则', async function () {
                    const feeRuleJson = await shopInfo.shipFee.mockFeeRuleJson(templateId);
                    // console.log(feeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spFeeRules: feeRuleJson });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
            describe('新增包邮规则', async function () {
                before('新增包邮规则', async function () {
                    const freeRuleJson = await shopInfo.shipFee.mockFreeRuleJson(templateId);
                    // console.log(freeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spShipFeeRules: freeRuleJson });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
        });
        describe('修改运费模板规则:修改规则', async function () {
            describe('修改计费规则', async function () {
                before('修改计费规则', async function () {
                    const rule = shopInfo.shipFee.getRandomRule(templateId, isFee = true);
                    const feeRuleJson = await shopInfo.shipFee.mockFeeRuleJson(templateId, { id: rule.id, deleteFlag: 0 });
                    // console.log(feeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spFeeRules: feeRuleJson }, { spFeeRules: [rule] });
                    // console.log(shopInfo.shipFee.templateMap.get(shipId));
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
            describe('修改包邮规则', async function () {
                before('修改包邮规则', async function () {
                    const rule = shopInfo.shipFee.getRandomRule(templateId, isFee = false);
                    const freeRuleJson = await shopInfo.shipFee.mockFreeRuleJson(templateId, { id: rule.id, deleteFlag: 0 });
                    // console.log(freeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spShipFeeRules: freeRuleJson }, { spShipFeeRules: [rule] });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
        });
        describe('修改运费模板规则:删除规则', async function () {
            describe('删除计费规则', async function () {
                before('删除计费规则', async function () {
                    const rule = shopInfo.shipFee.getRandomRule(templateId, isFee = true);
                    // console.log(`\n rule=${JSON.stringify(rule)}`);

                    const feeRuleJson = await shopInfo.shipFee.mockFeeRuleJson(templateId, { id: rule.id, deleteFlag: 1 });
                    // console.log(feeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spFeeRules: feeRuleJson }, { spFeeRules: [rule] });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
            describe('删除包邮规则', async function () {
                before('删除包邮规则', async function () {
                    const rule = shopInfo.shipFee.getRandomRule(templateId, isFee = false);
                    const freeRuleJson = await shopInfo.shipFee.mockFreeRuleJson(templateId, { id: rule.id, deleteFlag: 1 });
                    // console.log(freeRuleJson);
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, spShipFeeRules: freeRuleJson }, { spShipFeeRules: [rule] });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
        });

        describe('修改模板信息', async function () {
            describe('修改模板名称', async function () {
                before('修改模板名称', async function () {
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, temlateName: `运费模板修改${common.getRandomStr(5)}` });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });

            describe('关闭自定义包邮', async function () {
                before('关闭自定义包邮', async function () {
                    await shopInfo.shipFee.saveTemplateFeeRule({ id: templateId, state: 0 });
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == templateId), type = 0);
                });
                it('运费模板详情', async function () {
                    const templateFeeRuleDetails = await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRuleDetails);
                });
            });
        });
        describe('删除模板', async function () {
            before(async function () {
                await shopInfo.shipFee.deleteTemplate(templateId);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                expect(templateFeeRules.find(obj => obj.id == templateId), `运费模板列表存在模板${templateId}`).to.be.undefined;
            });
        });
    });
    describe('包邮到付模板', async function () {
        it('删除包邮模板', async function () {
            const res = await ss.spconfg.deleteTemplate({ check: false, templateId: shopInfo.shipFee.defaultTemplate.get(-1).pId })
            expect(res.result, `删除包邮模板${shopInfo.shipFee.defaultTemplate.get(-1).pId}居然成功了`).to.include({ msgId: "包邮到付模板不能被删除" });
        });
        it('删除到付模板', async function () {
            const res = await ss.spconfg.deleteTemplate({ check: false, templateId: shopInfo.shipFee.defaultTemplate.get(-3).pId })
            expect(res.result, `删除到付模板${shopInfo.shipFee.defaultTemplate.get(-3).pId}居然成功了`).to.include({ msgId: "包邮到付模板不能被删除" });
        });
    });
});

