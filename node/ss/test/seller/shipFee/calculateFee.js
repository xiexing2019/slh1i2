const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const myShop = require('../../../help/shop/myShop');
const shipFee = require('../../../help/shop/manage/shipFee');
const dresManage = require('../../../help/dresManage');
const wideDresManage = require('../../../help/wideDresManage');
const basicJson = require('../../../help/basicJson');
const ssReq = require('../../../help/ssReq');


describe('运费计算', function () {
    this.timeout(TESTCASE.timeout);
    const shopInfo = new myShop();
    let sellerInfo;
    before('登录并获取已存在运费模板', async function () {
        await shopInfo.switchSeller();
        const ship = await shopInfo.getShipFee();
        console.log(ship);
        // console.log(shopInfo.shipFee.defaultTemplate);
        sellerInfo = _.cloneDeep(shopInfo.getSellerInfo());
        // console.log(sellerInfo);

    });
    after('删除模板', async function () {
        await shopInfo.switchSeller();
        const templateIds = [...shopInfo.shipFee.templateMap.keys()];
        for (let index = 0; index < templateIds.length - 2; index++) {
            // if (shopInfo.shipFee.defaultTemplateId == templateIds[index]) continue;
            await shopInfo.shipFee.deleteTemplate(templateIds[index]);
        }
    });

    describe('商品', async function () {
        let template;
        const dres = dresManage.setupDres({ type: 'app' });
        before('新增商品', async function () {
            while (shopInfo.shipFee.templateMap.size < 3) {
                console.log(`已存在自定义模板数量:${shopInfo.shipFee.templateMap.size}`);
                const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 3, feeType: common.getRandomNum(1, 2) }, freeRule: { count: 2, shipFreeType: common.getRandomNum(1, 2) } });
                const templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
                await shopInfo.shipFee.templateFeeRuleDetails(templateId);
            }
            template = shopInfo.shipFee.getRandomTemplate();
            // console.log(template.id);
            await dresManage.prePrepare();
            const dresJson = basicJson.styleJsonByApp({}, (spu) => {
                spu.templateId = template.id;
                spu.templateName = template.temlateName;
                spu.weight = 0.5;
            });
            // console.log(`\ndresJson=${JSON.stringify(dresJson)}`);
            await dres.saveDresByApp(dresJson);
            // console.log(dres.spu);
            shopInfo.shipFee.updateTemplateCount(template.id, 1);
        });
        it('商品详情', async function () {
            const dresDetail = await dres.getFullById();
            common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
        });
        it('运费模板列表', async function () {
            const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template.id);
            shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template.id), type = 0);
        });
        describe('修改商品模板2', async function () {
            let template2;
            before('修改商品模板', async function () {
                template2 = shopInfo.shipFee.getRandomTemplate(template.id);
                // console.log(template2.id);
                await dres.updateSpuForSeller({ id: dres.id, templateId: template2.id, templateName: template2.temlateName });
                shopInfo.shipFee.updateTemplateCount(template.id, -1);
                shopInfo.shipFee.updateTemplateCount(template2.id, 1);
            });
            it('商品详情', async function () {
                const dresDetail = await dres.getFullById();
                common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template.id);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template.id), type = 0);
            });
            it('运费模板2列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template2.id);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template2.id), type = 0);
            });
            describe('删除绑定过商品的模板', async function () {
                before('删除绑定过商品的模板', async function () {
                    await shopInfo.switchSeller();
                    console.log('默认模板:', shopInfo.shipFee.defaultTemplateId);
                    await shopInfo.shipFee.deleteTemplate(template2.id);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    expect(templateFeeRules.find(obj => obj.id == template2.id), `运费模板列表不存在模板${template2.id}`).to.be.undefined;
                });
                it('运费模板列表:包邮', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == -1), 0);
                });
                it('商品详情', async function () {
                    const dresDetail = await dres.getFullById();
                    // console.log(JSON.stringify(dresDetail));
                    dres.spu.templateId = shopInfo.shipFee.defaultTemplate.get(-1).pId; //删除模板后，要将模板恢复到包邮
                    dres.spu.templateName = '包邮'; //删除模板后，要将模板恢复到包邮
                    common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
                });
            });
        });
        describe('修改商品模板3', async function () {
            let template3;
            before('修改商品模板', async function () {
                const oldTemplateId = dres.spu.templateId;
                console.log(dres.spu.templateId);
                template3 = shopInfo.shipFee.getRandomTemplate(oldTemplateId);
                console.log(template3);
                await dres.updateFreeTemplate({ templateId: template3.id, templateName: template3.temlateName });
                shopInfo.shipFee.updateTemplateCount(oldTemplateId, -1);
                shopInfo.shipFee.updateTemplateCount(template3.id, 1);
            });
            it('商品详情', async function () {
                const dresDetail = await dres.getFullById();
                common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
            });
            it('运费模板3列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template3.id);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template3.id), type = 0);
            });
        });
        describe('批量修改商品模板4', async function () {
            let template4, dresList;
            before('修改商品模板', async function () {
                const oldTemplateId = dres.spu.templateId;
                console.log(dres.spu.templateId);
                template4 = shopInfo.shipFee.getRandomTemplate(oldTemplateId);
                console.log(template4);
                dresList = await ssReq.getDresList(3);
                const spuIds = dresList.map(dres => dres.id);
                console.log(spuIds);
                await shopInfo.shipFee.batchUpdateFreeTemplate({ spuIds: spuIds, templateId: template4.id });
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template.id);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template.id), type = 0);
            });
            it('商品详情', async function () {
                for (const dres of dresList) {
                    const dresDetail = await dres.getFullById();
                    Object.assign(dres.spu, { templateId: template4.id, templateName: template4.temlateName });
                    common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
                }
            });
        });
        describe('一键更新商品运费模板', async function () {
            let template5;
            before('一键更新商品运费模板', async function () {
                template5 = shopInfo.shipFee.getRandomTemplate();
                await shopInfo.shipFee.quantityUpdateFreeTemplate(template5.id);
            });
            it('一个商品详情', async function () {
                const dresDetail = await dres.getFullById();
                dres.spu.templateId = template5.id;
                dres.spu.templateName = template5.temlateName;
                common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
            });
            it('运费模板列表', async function () {
                const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template5.id);
                shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == template5.id), type = 0);
            });
            describe('一键更新商品运费模板:包邮', async function () {
                let templateId, tid = -1;
                before('一键更新商品运费模板:包邮', async function () {
                    templateId = shopInfo.shipFee.defaultTemplate.get(tid).pId;
                    await shopInfo.shipFee.quantityUpdateFreeTemplate(templateId);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await shopInfo.shipFee.getTemplateFeeRules(template5.id);
                    common.isApproximatelyEqualAssert(shopInfo.shipFee.defaultTemplate.get(tid), templateFeeRules.find(obj => obj.id == tid));
                });
            });
        });
        describe('新增商品兼容templateId为1,3情况', async function () {
            describe('商品:包邮1', async function () {
                const dres = dresManage.setupDres();
                let templateId, tid = -1;
                before('新增商品', async function () {
                    templateId = shopInfo.shipFee.defaultTemplate.get(tid).pId;
                    await dresManage.prePrepare();
                    const dresJson = basicJson.styleJson({}, (spu) => {
                        spu.templateId = Math.abs(tid);
                    });
                    await dres.saveDres(dresJson);
                    // console.log(dres.spu);
                    shopInfo.shipFee.updateTemplateCount(templateId, 1);
                });
                it('商品详情', async function () {
                    const dresDetail = await dres.getFullById();
                    dres.spu.templateId = templateId;
                    common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == tid), 0);
                });
            });
            describe('独立app商品:到付3', async function () {
                const dresByApp = dresManage.setupDres({ type: 'app' });
                let templateId, tid = -3;
                before('新增商品', async function () {
                    templateId = shopInfo.shipFee.defaultTemplate.get(tid).pId;
                    await dresManage.prePrepare();
                    const dresJson = basicJson.styleJsonByApp({}, (spu) => {
                        spu.templateId = Math.abs(tid);
                    });
                    // console.log(`\ndresJson=${JSON.stringify(dresJson)}`);
                    await dresByApp.saveDresByApp(dresJson);
                    shopInfo.shipFee.updateTemplateCount(templateId, 1);
                });
                it('商品详情', async function () {
                    const dresDetail = await dresByApp.getFullById();
                    dresByApp.spu.templateId = templateId;
                    common.isApproximatelyEqualAssert(dresByApp.spu, dresDetail.spu);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == tid), 0);
                });
            });
            describe('泛行业商品:到付3', async function () {
                const dres = wideDresManage.setupDres({ type: 'app' });
                let templateId, tid = -3;
                before('新增商品', async function () {
                    templateId = shopInfo.shipFee.defaultTemplate.get(tid).pId;
                    const dictTypeId = await wideDresManage.setupSpec();
                    await wideDresManage.prePrepare({ 'dictTypeId': dictTypeId });
                    const json = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId }, (spu) => {
                        spu.templateId = Math.abs(tid);
                    });
                    console.log(`json=${JSON.stringify(json)}`);
                    // await dres.saveDres(json);
                    await dres.saveDresByApp(json);
                    shopInfo.shipFee.updateTemplateCount(templateId, 1);
                });
                it('商品详情', async function () {
                    const dresDetail = await dres.getFullById();
                    dres.spu.templateId = templateId;
                    common.isApproximatelyEqualAssert(dres.spu, dresDetail.spu);
                });
                it('运费模板列表', async function () {
                    const templateFeeRules = await ss.spconfg.getTemplateFeeRules().then(res => res.result.data.rows);
                    console.log(`\n运费模板列表=${JSON.stringify(templateFeeRules)}`);
                    shopInfo.shipFee.shipTemplateAssert(templateFeeRules.find(obj => obj.id == tid), 0);
                });
            });
        });
    });

    // 类型 50包邮, 30到付 0自定义
    describe('计算运费', async function () {
        let templateIds = [], dresList1 = [], dresList2 = [], templateJsons = [];
        before('创建运费模板', async function () {
            await shopInfo.switchSeller();
            // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            const dresList = await ssReq.getDresList(4); //前两个使用模板1，后两个使用模板2
            [dresList1, dresList2] = _.chunk(dresList, 2);
            for (const type of [1, 2]) {
                const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 6, feeType: type }, freeRule: { count: 2, shipFreeType: type } });
                templateJsons.push(templateJson);
                const templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
                templateIds.push(templateId);
                await shopInfo.shipFee.templateFeeRuleDetails(templateId);
            }
            //商品设置模板
            for (const dres of dresList1) await dres.updateSpuForSeller({ id: dres.id, templateId: templateIds[0] });
            for (const dres of dresList2) await dres.updateSpuForSeller({ id: dres.id, templateId: templateIds[1] });
        });
        after('删除创建的模板', async function () {
            await shopInfo.switchSeller();
            for (const id of templateIds) await shopInfo.shipFee.deleteTemplate(id);
        });
        describe(`使用自定义模板计算运费`, async function () {
            /** 只在运费规则中存在的省 */
            let onlyFeeProvIds = {};
            /** 既不在运费规则，也不在包邮规则，使用默认计费规则的省 */
            let lonelyProvIds = {};
            before('准备省份数据', async function () {
                for (let index = 0; index < 2; index++) {
                    onlyFeeProvIds[templateIds[index]] = common.arrRemoveSameEle([...shopInfo.shipFee.templateMap.get(templateIds[index]).spFeeRules.keys()], [...shopInfo.shipFee.templateMap.get(templateIds[index]).spShipFeeRules.keys()]);
                    const areaJson = await basicJson.getAreaJson();
                    const excludeFeeProvIds = common.arrRemoveSameEle(areaJson.proviceArr, [...shopInfo.shipFee.templateMap.get(templateIds[index]).spFeeRules.keys()]);
                    lonelyProvIds[templateIds[index]] = common.arrRemoveSameEle(excludeFeeProvIds, [...shopInfo.shipFee.templateMap.get(templateIds[index]).spShipFeeRules.keys()]);
                }
                console.log(onlyFeeProvIds);
                console.log(lonelyProvIds);
            });
            describe('满足包邮:第一条', async function () {
                let feeExp, fee;
                before('满足包邮', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    const freeRule1 = shopInfo.shipFee.templateMap.get(templateIds[0]).spShipFeeRules.get('110000');
                    const freeRule2 = shopInfo.shipFee.templateMap.get(templateIds[1]).spShipFeeRules.get('110000');
                    // console.log(freeRule1, freeRule2);
                    const order = {
                        provinceCode: '110000',
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: freeRule1.limitNum - 1, orderMoney: freeRule1.limitNum - 1 },
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: freeRule2.limitNum - 1, orderMoney: freeRule2.limitNum - 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                    console.log(fee);

                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('满足包邮:第一个模板使用非第一条包邮规则', async function () {
                let feeExp, fee;
                before('满足包邮', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    // templateJsons[0].spShipFeeRules
                    // _.union(a, b, c);
                    const provId = templateJsons[0].spShipFeeRules[1].shipProvIds.split(',')[0];
                    console.log(provId);
                    const freeRule1 = shopInfo.shipFee.templateMap.get(templateIds[0]).spShipFeeRules.get(provId);
                    let freeRule2 = shopInfo.shipFee.templateMap.get(templateIds[1]).spShipFeeRules.get(provId);
                    !freeRule2 && (freeRule2 = { limitNum: common.getRandomNum(2, 10) });
                    console.log(freeRule1, freeRule2);
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: freeRule1.limitNum - 1, orderMoney: freeRule1.limitNum - 1 },
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: freeRule2.limitNum - 1, orderMoney: freeRule2.limitNum - 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('不满足包邮', async function () {
                let feeExp, fee;
                before('不满足包邮', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    const freeRule1 = shopInfo.shipFee.templateMap.get(templateIds[0]).spShipFeeRules.get('110000');
                    const freeRule2 = shopInfo.shipFee.templateMap.get(templateIds[1]).spShipFeeRules.get('110000');
                    // console.log(freeRule1, freeRule2);
                    const order = {
                        provinceCode: '110000',
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: freeRule1.limitNum - 2, orderMoney: freeRule1.limitNum - 2 },
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: freeRule2.limitNum - 2, orderMoney: freeRule2.limitNum - 2 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('使用默认计费规则', async function () {
                let feeExp, fee;
                before('使用默认规则', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    const order = {
                        provinceCode: '999999',
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: common.getRandomNum(1, 10), orderMoney: common.getRandomNum(1, 10) },
                            { spuId: dresList1[1].id, orderNum: common.getRandomNum(1, 10), orderMoney: common.getRandomNum(1, 10) },
                            { spuId: dresList2[0].id, orderNum: common.getRandomNum(1, 10), orderMoney: common.getRandomNum(1, 10) },
                            { spuId: dresList2[1].id, orderNum: common.getRandomNum(1, 10), orderMoney: common.getRandomNum(1, 10) }]
                    };

                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('满足计费', async function () {
                let feeExp, fee;
                before('满足计费', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    const provIds = _.intersection(onlyFeeProvIds[templateIds[0]], onlyFeeProvIds[templateIds[1]]);
                    console.log(provIds);
                    const provId = provIds[common.getRandomNum(0, provIds.length - 1)];
                    console.log(provId);
                    const feeRule1 = shopInfo.shipFee.templateMap.get(templateIds[0]).spFeeRules.get(provId);
                    const feeRule2 = shopInfo.shipFee.templateMap.get(templateIds[1]).spFeeRules.get(provId);
                    console.log(feeRule1, feeRule2);
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: common.getRandomNum(feeRule1.startNum - 1, feeRule1.startNum * 3), orderMoney: common.getRandomNum(50, 100) },
                            { spuId: dresList1[1].id, orderNum: common.getRandomNum(feeRule1.startNum - 1, feeRule1.startNum * 3), orderMoney: common.getRandomNum(50, 100) },
                            { spuId: dresList2[0].id, orderNum: common.getRandomNum(feeRule2.startNum - 1, feeRule2.startNum * 3), orderMoney: common.getRandomNum(50, 100) },
                            { spuId: dresList2[1].id, orderNum: common.getRandomNum(feeRule2.startNum - 1, feeRule2.startNum * 3), orderMoney: common.getRandomNum(50, 100) }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('泛行业:满足计费', async function () {
                const dres = wideDresManage.setupDres({ type: 'app' });
                before('新建泛行业商品', async function () {
                    await shopInfo.switchSeller();
                    const dictTypeId = await wideDresManage.setupSpec();
                    await wideDresManage.prePrepare({ 'dictTypeId': dictTypeId });
                    const json = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId }, (spu, skus) => {
                        spu.templateId = templateIds[1];
                        skus.forEach(sku => {
                            sku.weight = 0.1;
                        });
                    });
                    console.log(json.skus);
                    await dres.saveDresByApp(json);
                    console.log(dres.skus);
                    shopInfo.shipFee.updateTemplateCount(templateIds[1], 1);
                });
                describe('满足计费', async function () {
                    let feeExp, fee;
                    before('满足计费', async function () {
                        await shopInfo.switchSeller();
                        const provIds = _.intersection(onlyFeeProvIds[templateIds[0]], onlyFeeProvIds[templateIds[1]]);
                        console.log(provIds);
                        const provId = provIds[common.getRandomNum(0, provIds.length - 1)];
                        console.log(provId);
                        const feeRule2 = shopInfo.shipFee.templateMap.get(templateIds[1]).spFeeRules.get(provId);
                        console.log(feeRule2);
                        const dresDetail = dres.getDetailExp();
                        // const skuDTOS = dresDetail.skus.map(obj => { return { skuId: obj.id, num: common.getRandomNum(1, 20) } });
                        const skuDTOS = [{ skuId: dresDetail.skus[0].id, num: common.div(feeRule2.startNum, dresDetail.skus[0].weight) }];
                        // console.log(skuDTOS);
                        const order = {
                            provinceCode: provId,
                            orderSpus: [
                                {
                                    spuId: dres.id,
                                    skuDTOS: skuDTOS,
                                    orderNum: skuDTOS.map(obj => obj.num).reduce((a, b) => common.add(a, b), 0),
                                    orderMoney: common.getRandomNum(50, 100)
                                }]
                        };
                        feeExp = await shopInfo.shipFee.getOrderFee(order);
                        await ssReq.userLoginWithWx();
                        fee = await ss.spdresb.getOrderFreight({
                            provinceCode: order.provinceCode, orders: [{
                                sellerId: sellerInfo.tenantId,
                                orderSpus: order.orderSpus
                            }]
                        });
                    });
                    it('运费校验', async function () {
                        common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                    });
                });
            });
            describe('泛行业:满足增量计费', async function () {
                const dres = wideDresManage.setupDres({ type: 'app' });
                before('新建泛行业商品', async function () {
                    await shopInfo.switchSeller();
                    const dictTypeId = await wideDresManage.setupSpec();
                    await wideDresManage.prePrepare({ 'dictTypeId': dictTypeId });
                    const json = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId }, (spu, skus) => {
                        spu.templateId = templateIds[1];
                        skus.forEach(sku => {
                            sku.weight = common.getRandomNum(0.1, 1, 1);
                        });
                    });
                    console.log(json.skus);
                    await dres.saveDresByApp(json);
                    console.log(dres.skus);
                    shopInfo.shipFee.updateTemplateCount(templateIds[1], 1);
                });
                describe('满足计费', async function () {
                    let feeExp, fee;
                    before('满足计费', async function () {
                        await shopInfo.switchSeller();
                        const provIds = _.intersection(onlyFeeProvIds[templateIds[0]], onlyFeeProvIds[templateIds[1]]);
                        console.log(provIds);
                        const provId = provIds[common.getRandomNum(0, provIds.length - 1)];
                        console.log(provId);
                        const dresDetail = dres.getDetailExp();
                        const skuDTOS = dresDetail.skus.map(obj => { return { skuId: obj.id, num: common.getRandomNum(1, 20) } });
                        console.log(skuDTOS);
                        // const skuDTOS = [{ skuId: dresDetail.skus[0].id, num: common.getRandomNum(1, 20) }, { skuId: dresDetail.skus[1].id, num: common.getRandomNum(1, 20) }];
                        const order = {
                            provinceCode: provId,
                            orderSpus: [
                                {
                                    spuId: dres.id,
                                    skuDTOS: skuDTOS,
                                    orderNum: skuDTOS.map(obj => obj.num).reduce((a, b) => common.add(a, b), 0),
                                    orderMoney: common.getRandomNum(50, 100)
                                }]
                        };
                        feeExp = await shopInfo.shipFee.getOrderFee(order);
                        await ssReq.userLoginWithWx();
                        fee = await ss.spdresb.getOrderFreight({
                            provinceCode: order.provinceCode, orders: [{
                                sellerId: sellerInfo.tenantId,
                                orderSpus: order.orderSpus
                            }]
                        });
                    });
                    it('运费校验', async function () {
                        common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                    });
                });
            });
        });
        describe('默认规则为空的模板', async function () {
            let templateId, spuId;
            /** 既不在运费规则，也不在包邮规则，使用默认计费规则的省 */
            let lonelyProvIds = [];
            before('创建运费模板', async function () {
                spuId = dresList1[0].id;
                await shopInfo.switchSeller();
                const templateJson = await shipFee.mockTemplateFeeRuleJson({ feeRule: { count: 1, }, freeRule: { count: 0 } });
                console.log(templateJson);
                templateJson.spFeeRules.forEach(feeRule => {
                    if (feeRule.defaultFlag) common.update(feeRule, { startProvCode: 0, provIds: '', startNum: 0, startFee: 0, addNum: 0, addFee: 0 });
                });
                // console.log(templateJson);
                templateId = await shopInfo.shipFee.saveTemplateFeeRule(templateJson);
                const areaJson = await basicJson.getAreaJson();
                lonelyProvIds = common.arrRemoveSameEle(areaJson.proviceArr, [...shopInfo.shipFee.templateMap.get(templateId).spFeeRules.keys()]);
                // console.log(lonelyProvIds);
                await shopInfo.shipFee.templateFeeRuleDetails(templateId);
                //商品设置模板
                await sp.spdresb.updateSpuForSeller({ id: spuId, templateId: templateId });
            });
            after('删除创建的模板', async function () {
                await shopInfo.switchSeller();
                await shopInfo.shipFee.deleteTemplate(templateId);
            });
            describe('使用默认计费规则', async function () {
                let feeExp, fee;
                before('使用默认规则', async function () {
                    await shopInfo.switchSeller();
                    // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                    const order = {
                        provinceCode: lonelyProvIds[common.getRandomNum(0, lonelyProvIds.length - 1)],
                        orderSpus: [{ spuId: spuId, orderNum: common.getRandomNum(1, 10), orderMoney: common.getRandomNum(50, 100) }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
        });
        describe('包邮到付模板:使用1、3', async function () {
            let provId = '110000';
            before('修改商品运费模板', async function () {
                await shopInfo.switchSeller();
                for (const dres of dresList1) await dres.updateSpuForSeller({ id: dres.id, templateId: 1 }); //包邮
                for (const dres of dresList2) await dres.updateSpuForSeller({ id: dres.id, templateId: 3 }); //到付
            });
            describe('包邮', async function () {
                let feeExp, fee;
                before('所有商品包邮', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('到付', async function () {
                let feeExp, fee;
                before('所有商品到付', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('包邮到付混合', async function () {
                let feeExp, fee;
                before('包邮到付混合', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
        });
        describe('包邮到付模板:使用pId', async function () {
            let provId = '110000';
            before('修改商品运费模板', async function () {
                await shopInfo.switchSeller();
                for (const dres of dresList1) await dres.updateSpuForSeller({ id: dres.id, templateId: shopInfo.shipFee.defaultTemplate.get(-1).pId }); //包邮
                for (const dres of dresList2) await dres.updateSpuForSeller({ id: dres.id, templateId: shopInfo.shipFee.defaultTemplate.get(-3).pId }); //到付
            });
            describe('包邮', async function () {
                let feeExp, fee;
                before('所有商品包邮', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('到付', async function () {
                let feeExp, fee;
                before('所有商品到付', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
            describe('包邮到付混合', async function () {
                let feeExp, fee;
                before('包邮到付混合', async function () {
                    await shopInfo.switchSeller();
                    const order = {
                        provinceCode: provId,
                        orderSpus: [
                            { spuId: dresList1[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList1[1].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[0].id, orderNum: 1, orderMoney: 1 },
                            { spuId: dresList2[1].id, orderNum: 1, orderMoney: 1 }]
                    };
                    feeExp = await shopInfo.shipFee.getOrderFee(order);

                    await ssReq.userLoginWithWx();
                    fee = await ss.spdresb.getOrderFreight({
                        provinceCode: order.provinceCode, orders: [{
                            sellerId: sellerInfo.tenantId,
                            orderSpus: order.orderSpus
                        }]
                    });
                });
                it('运费校验', async function () {
                    common.isApproximatelyEqualAssert(feeExp, fee.result.data, [], `运费${JSON.stringify(fee)}\n与预期${feeExp}不一致`);
                });
            });
        });
    });
});
