const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const roleManage = require('../../../help/roleManage');
const employeeManage = require('../../../help/employeeManage');
const authList = require('../../../data/menuAuthList');

/**
 * gm:管理员
 * bigBoss:老板
 * seller：店员
 * shopManage:店长
 */
describe('角色（职位）管理', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, menuAuthList;
    const role = roleManage.setupRole();
    before(async function () {
        //用户注册开通微商城，登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        //获取所有权限
        menuAuthList = await role.getlistByTree();
    });

    describe('权限', async function () {
        it('权限', async function () {
            console.log(`\n menuAuthList=${JSON.stringify(menuAuthList)}`);
            console.log(`\n authList=${JSON.stringify(authList)}`);
            expect(authList.length, `权限长度不一致,期望值${authList.length}`).to.equal(menuAuthList.length);
            authList.forEach(auth => {
                const menuAuth = menuAuthList.find(obj => obj.name == auth.name);
                common.isApproximatelyEqualAssert(auth, menuAuth, ['subItems']);
                expect(auth.subItems.length, `${auth.name}子权限长度不一致,期望值${auth.subItems.length}`).to.equal(menuAuth.subItems.length);
                // console.log(menuAuth);
                auth.subItems.forEach(item => {
                    common.isApproximatelyEqualAssert(item, menuAuth.subItems.find(obj => obj.name == item.name));
                });
            });
        });
    });

    describe('默认存在大老板，管理员，店长，店员角色', async function () {
        let roleList;
        before('角色列表', async function () {
            roleList = await role.getRoleListAtShop();
            console.log(`\n roleList=${JSON.stringify(roleList)}`);
        });
        it('存在大老板角色', async function () {
            expect(roleList.find(obj => obj.code == 'bigBoss'), `角色列表未找到code为bigBoss的角色`).to.not.be.undefined;
        });
        it('存在管理员角色', async function () {
            expect(roleList.find(obj => obj.code == 'gm'), `角色列表未找到code为gm的角色`).to.not.be.undefined;
        });
        it('存在店长角色', async function () {
            expect(roleList.find(obj => obj.code == 'shopmanager'), `角色列表未找到code为shopmanager的角色`).to.not.be.undefined;
        });
        it('存在店员角色', async function () {
            expect(roleList.find(obj => obj.code == 'seller'), `角色列表未找到code为seller的角色`).to.not.be.undefined;
        });
    });

    describe('自定义角色', async function () {
        const employee = employeeManage.setupEmployee();
        let roleParams;
        describe('新建角色', async function () {
            before(async function () {
                await ssReq.ssSellerLogin();
                const name = '职位' + common.getRandomStr(5);
                roleParams = { name: name, rem: '备注' + name, funcIds: [menuAuthList[0].id, menuAuthList[3].id] };
                //新建角色
                await role.newOrEditRole(roleParams);
                //新建员工
                await employee.newOrEditStaff({
                    userName: '员工' + common.getRandomStr(5),
                    mobile: common.getRandomMobile(),
                    rem: '员工备注' + common.getRandomStr(5),
                    roleId: role.id,
                });
            });
            it('角色列表', async function () {
                const roleList = await role.getRoleListAtShop();
                expect(roleList.find(obj => obj.id == role.id), `角色列表未找到id为${role.id}的角色`).to.not.be.undefined;
                common.isApproximatelyEqualAssert(roleParams, roleList.find(obj => obj.id == role.id));
            });
            it('角色权限详情', async function () {
                common.isApproximatelyArrayAssert(roleParams.funcIds, role.funcIds);
            });
            it('员工详情', async function () {
                const staffInfo = await employee.getStaffInfo();
                console.log(`\n staffInfo=${JSON.stringify(staffInfo)}`);
                common.isApproximatelyEqualAssert(employee.getEmployee(), staffInfo);
            });
            it('我的界面数据', async function () {
                await ssReq.ssSellerLogin({ code: employee.phone, shopName: sellerInfo.shopName });
                const uerSession = await ss.spauth.getUserSession({ userId: LOGINDATA.userId }).then(res => res.result.data);
                console.log(`uerSession=${JSON.stringify(uerSession)}`);
                expect(uerSession.customizeRoleName, '角色名与预期不一致').to.equal(role.name);
            });
        });

        describe('编辑角色', async function () {
            before(async function () {
                await ssReq.ssSellerLogin();
                const name = '职位修改' + common.getRandomStr(5);
                roleParams = { id: role.id, name: name, rem: '备注' + name, funcIds: [menuAuthList[1].id, menuAuthList[2].id] };
                await role.newOrEditRole(roleParams);
                // console.log(role);
            });
            it('角色列表', async function () {
                const roleList = await role.getRoleListAtShop();
                expect(roleList.find(obj => obj.id == role.id), `角色列表未找到id为${role.id}的角色`).to.not.be.undefined;
                common.isApproximatelyEqualAssert(roleParams, roleList.find(obj => obj.id == role.id));
            });
            it('角色权限详情', async function () {
                common.isApproximatelyArrayAssert(roleParams.funcIds, role.funcIds);
            });
            it('我的界面数据', async function () {
                await ssReq.ssSellerLogin({ code: employee.phone, shopName: sellerInfo.shopName });
                const uerSession = await ss.spauth.getUserSession({ userId: LOGINDATA.userId }).then(res => res.result.data);
                console.log(`uerSession=${JSON.stringify(uerSession)}`);
                expect(uerSession.customizeRoleName, '角色名与预期不一致').to.equal(role.name);
            });
        });

        describe('删除角色', async function () {
            describe('删除关联员工前删除角色', async function () {
                let deleteRole;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    deleteRole = await role.deleteRole({ check: false });
                    console.log(`\n deleteRole=${JSON.stringify(deleteRole)}`);
                });
                it('删除关联员工前删除角色,应失败', async function () {
                    expect(deleteRole.result, `角色列表居然找到了id为${role.val}的角色`).to.includes({ msgId: "role_user_exists" });
                });
                it('角色列表', async function () {
                    const roleList = await role.getRoleListAtShop();
                    expect(roleList.find(obj => obj.id == role.id), `角色列表未找到id为${role.id}的角色`).to.not.be.undefined;
                    common.isApproximatelyEqualAssert(roleParams, roleList.find(obj => obj.id == role.id));
                });
            });
            describe('删除关联员工后删除角色', async function () {
                before('删除员工，角色', async function () {
                    // await employee.deliverStaff({ check: false, userId: sellerInfo.userId });
                    const deleteStaff = await employee.deleteStaff();
                    console.log(`\n deleteStaff=${JSON.stringify(deleteStaff)}`);
                    deleteRole = await role.deleteRole();
                });
                it('角色列表', async function () {
                    const roleList = await role.getRoleListAtShop();
                    expect(roleList.find(obj => obj.id == role.id), `角色列表居然找到了id为${role.val}的角色`).to.be.undefined;
                });
                it('员工详情', async function () {
                    const staffInfo = await employee.getStaffInfo();
                    console.log(`\n staffInfo=${JSON.stringify(staffInfo)}`);
                    common.isApproximatelyEqualAssert(employee.getEmployee(), staffInfo);
                });
            });
        });
    });

    //修改，删除都可成功
    describe.skip('管理员', async function () {
        let gmRole;
        before(async function () {
            const roleList = await role.getRoleListAtShop();
            const gm = roleList.find(obj => obj.code == 'gm');
            gmRole = await roleManage.getRoleInfo(gm.id);
            console.log(gmRole);
        });

        describe('编辑角色', async function () {
            before('编辑角色，应失败', async function () {
                const name = '管理员职位修改' + common.getRandomStr(5);
                roleParams = { id: gmRole.id, name: name, rem: '备注' + name, funcIds: [menuAuthList[1].id, menuAuthList[2].id] };
                await gmRole.newOrEditRole(roleParams);
                console.log(gmRole);
            });
            it('角色列表：与原来一致', async function () {
                const roleList = await gmRole.getRoleListAtShop();
                expect(roleList.find(obj => obj.id == gmRole.id), `角色列表未找到id为${gmRole.id}的角色`).to.not.be.undefined;
                common.isApproximatelyEqualAssert(roleParams, roleList.find(obj => obj.id == gmRole.id));
            });
            it('角色详情：与原来一致', async function () {
                common.isApproximatelyArrayAssert(roleParams.funcIds, gmRole.funcIds);
            });
        });

        describe('删除角色', async function () {
            before('删除角色，应失败', async function () {
                await gmRole.deleteRole();
            });
            it('角色列表：与原来一致', async function () {
                const roleList = await gmRole.getRoleListAtShop();
                expect(roleList.find(obj => obj.id == gmRole.id), `角色列表未找到id为${gmRole.id}的角色`).to.not.be.undefined;
                common.isApproximatelyEqualAssert(roleParams, roleList.find(obj => obj.id == gmRole.id));
            });
            it('角色详情：与原来一致', async function () {
                common.isApproximatelyArrayAssert(roleParams.funcIds, gmRole.funcIds);
            });
        });
    });
});