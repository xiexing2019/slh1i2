const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const spmdm = require('../../../../reqHandler/sp/biz_server/spmdm');
const ssAccount = require('../../../data/ssAccount');
const basicJson = require('../../../help/basicJson');
const ssConfigParam = require('../../../help/configParamManager');
const employeeManage = require('../../../help/employeeManage');
const mockJsonParam = require('../../../../sp/help/mockJsonParam');
const billManage = require('../../../help/billManage');
const dresManage = require('../../../help/dresManage');
const roleManage = require('../../../help/roleManage');

describe('员工管理', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, sellerAInfo, roleArr, customSellerIsolation;
    const employeeA = employeeManage.setupEmployee();
    before(async function () {
        //用户A注册开通微商城，登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        //清空员工
        let staffList = await ss.spmdm.getStaffList().then(res => res.result.data.generalStaff);
        let staffNum = staffList.length;
        for (let index = staffNum - 1; index >= 0; index--) {
            // console.log(staffList[index]);
            await ss.spmdm.deliverStaff({ check: false, deliverUserId: staffList[index].id, userId: sellerInfo.userId });
            await ss.spmdm.deleteStaff({ userId: staffList[index].id, roleId: staffList[index].roleId });
            // await employeeA.deliverStaff({ userId: sellerInfo.userId });
            // await employeeB.deleteStaff();
        }
        staffList = await ss.spmdm.getStaffList().then(res => res.result.data.generalStaff);
        expect(0, '清空员工失败').to.be.equal(staffList.length);
        // if (staffList.maxShopManagerNum <= staffList.generalStaff.length) {
        //     console.warn(`员工数已满，套餐可用人数：${staffList.maxShopManagerNum}，已有人数${staffList.generalStaff.length}`);
        //     this.parent ? this.parent.skip() : this.skip();
        // }
        const role = roleManage.setupRole();
        const roleList = await role.getRoleListAtShop();
        // console.log(roleList);
        roleArr = [];
        roleList.forEach((role, index) => {
            if (role.code != 'gm' && role.code != 'bigBoss') {
                // roleList.splice(index, 1);
                roleArr.push({ code: role.code, id: role.id });
            }
        });
        // console.log(roleArr);
        // 角色列表没有除gm,bigboss以外的角色则结束当前脚本
        if (roleArr.length < 1) {
            console.warn(`角色列表没有除gm,bigboss以外的角色:${JSON.stringify(roleArr)}`);
            this.parent ? this.parent.skip() : this.skip();
        };
    });

    describe('员工新增', async function () {
        before(async function () {
            //新建员工A
            await employeeA.newOrEditStaff({
                userName: '员工A' + common.getRandomStr(5),
                mobile: common.getRandomMobile(),
                rem: '员工A备注' + common.getRandomStr(5),
                roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
            });
            // await employeeA.getStaffList();
        });
        it('员工列表', async function () {
            const staff = await employeeA.getStaffList();
            common.isApproximatelyEqualAssert(employeeA.getEmployee(), staff);
        });
        it('员工详情', async function () {
            const staffInfo = await employeeA.getStaffInfo();
            common.isApproximatelyEqualAssert(employeeA.getEmployee(), staffInfo);
        });
    });

    describe('员工编辑', async function () {
        describe('(备注、姓名、角色）编辑', async function () {
            before(async function () {
                await employeeA.newOrEditStaff({
                    userId: employeeA.userId,
                    userName: common.getRandomChineseStr(3),
                    mobile: employeeA.phone,
                    rem: '员工A备注修改' + common.getRandomStr(5),
                    roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
                    oldRoleId: employeeA.roleId
                });
            });
            it('员工列表', async function () {
                const staff = await employeeA.getStaffList();
                common.isApproximatelyEqualAssert(employeeA.getEmployee(), staff);
            });
            it('员工详情', async function () {
                const staffInfo = await employeeA.getStaffInfo();
                common.isApproximatelyEqualAssert(employeeA.getEmployee(), staffInfo);
            });
        });

    });

    describe('员工关联客户', async function () {
        let mobileClient = common.getRandomMobile(), clientListForEmployee;
        before('新建客户关联员工A', async function () {
            await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
            sellerAInfo = _.cloneDeep(LOGINDATA);
            console.log(employeeA);

            await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: mobileClient, appId: ssAccount.seller7.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
            const ranDate = `${common.getRandomNum(1970, 2018)}-${common.getRandomNum(1, 12)}-${common.getRandomNum(1, 28)}`;
            await employeeA.updateUser({
                id: clientInfo.userId, _cid: clientInfo.clusterCode, _tid: clientInfo.tenantId, brithday: ranDate, birthLunarFlag: 1, gender: 1, mobile: clientInfo.mobile, tenantId: clientInfo.tenantId
            });
            await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: clientInfo.mobile, appId: ssAccount.seller7.appId });
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log(`clientInfo=${JSON.stringify(clientInfo)}`);

            await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId, sellerId: sellerAInfo.userId });

            await ssReq.ssSellerLogin();
            console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

        });
        it('员工A关联客户列表', async function () {
            clientListForEmployee = await employeeA.getClientListForEmployee();
            console.log('打印客户信息期望值', employeeA.getClientListExp(clientInfo));
            console.log('打印客户信息', clientInfo);
            expect(employeeA.getEmployee().clientList[0], '关联客户失败').not.to.be.undefined;
            common.isApproximatelyEqualAssert(employeeA.getClientListExp(clientInfo), clientInfo, ['avatar']);
            // expect(clientInfo.avatar, `图片不相同`).to.include(employeeA.getEmployee().clientList[0].avatar)
        });
        it('员工A关联客户人数', async function () {
            await employeeA.getClientCountForEmployee();
            expect(1, '关联客户人数').to.be.equal(employeeA.clientCount);
        });
        //回滚到了客户列表为全部客户
        it.skip('员工A获取客户列表', async function () {
            await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
            const memberList = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data);
            console.log(`memberList=${JSON.stringify(memberList)}`);
            console.log(clientListForEmployee.length);
            expect(clientListForEmployee.length, '员工A关联客户列表与客户列表长度不一致').to.equal(memberList.total);
            expect(clientListForEmployee[0].id, '员工A关联客户列表与客户列表的客户不一致').to.equal(memberList.rows[0].custUserId);
        });
        it.skip('管理员获取客户列表', async function () {
            await ssReq.ssSellerLogin();
            const memberList = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data.rows);
            console.log(`memberList=${JSON.stringify(memberList)}`);
            expect(clientListForEmployee[0].id, '管理员获取客户列表找不到关联了员工A的客户').to.equal(memberList[0].custUserId);
            expect(memberList.find(obj => obj.custUserId == clientListForEmployee[0].id).custUserId, '管理员获取客户列表找不到关联了员工A的客户').to.equal(clientListForEmployee[0].id);
        });
        describe('员工B关联客户列表', async function () {
            let sellerBInfo, mobileB = common.getRandomMobile(), clientInfoX;
            const employeeB = employeeManage.setupEmployee();
            before('新建员工B', async function () {
                //新建员工B
                await employeeB.newOrEditStaff({
                    userName: '员工B' + common.getRandomStr(5),
                    mobile: mobileB,
                    rem: '员工B备注' + common.getRandomStr(5),
                    roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
                });
                //客户关联员工B
                await ssReq.ssSellerLogin({ code: mobileB, shopName: sellerInfo.shopName });
                sellerBInfo = _.cloneDeep(LOGINDATA);

                await ssReq.ssClientLogin({ tenantId: sellerBInfo.tenantId, mobile: mobileClient, appId: ssAccount.seller7.appId });
                clientInfo = _.cloneDeep(LOGINDATA);
                await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId, sellerId: sellerBInfo.userId });

                await ssReq.ssSellerLogin();
            });
            after(async function () {
                await ssReq.ssSellerLogin();
                if (!employeeB.userId) return;
                await employeeB.deliverStaff({ check: false, userId: sellerInfo.userId });
                await employeeB.deleteStaff();
                const staff = await employeeB.getStaffList();
                expect(staff, '员工列表居然找到了删掉的员工B').to.be.undefined;
            });
            describe('编辑员工B姓名与员工A一样', async function () {
                before(async function () {
                    await employeeB.newOrEditStaff({
                        userId: employeeB.userId,
                        userName: employeeA.name,
                        mobile: employeeB.phone,
                        rem: '员工A备注修改' + common.getRandomStr(5),
                        roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
                        oldRoleId: employeeB.roleId
                    });
                });
                it('员工列表', async function () {
                    const staff = await employeeB.getStaffList();
                    common.isApproximatelyEqualAssert(employeeB.getEmployee(), staff);
                });
                it('员工详情', async function () {
                    const staffInfo = await employeeB.getStaffInfo();
                    common.isApproximatelyEqualAssert(employeeB.getEmployee(), staffInfo);
                });
            });
            it('员工A关联客户列表', async function () {
                const clientListForEmployee = await employeeA.getClientListForEmployee();
                expect(clientListForEmployee, '关联客户列表为空').to.not.be.undefined;
                // console.log(employeeA.getEmployee().clientList[0]);
                // common.isApproximatelyEqualAssert(clientInfo, employeeA.getEmployee().clientList[0]);
                common.isApproximatelyEqualAssert(clientInfo, employeeA.getClientListExp(clientInfo), ['avatar']);
                // common.isApproximatelyEqualAssert(clientInfo, employeeA.getEmployee().clientList[0], ['avatar']);
                // expect(clientInfo.avatar, `图片不相同`).to.include(employeeA.getEmployee().clientList[0].avatar)
            });
            it('员工A关联客户人数', async function () {
                await employeeA.getClientCountForEmployee();
                expect(1, '关联客户人数').to.be.equal(employeeA.clientCount);
            });
            it('员工B关联客户列表', async function () {
                const clientListForEmployee = await employeeB.getClientListForEmployee();
                expect(clientListForEmployee, '关联客户列表竟然不为空').to.be.undefined;
            });
            it('员工B关联客户人数', async function () {
                await employeeB.getClientCountForEmployee();
                expect(0, '关联客户人数').to.be.equal(employeeB.clientCount);
            });
            //回滚到了客户列表为全部客户
            it.skip('员工B获取客户列表', async function () {
                await ssReq.ssSellerLogin({ code: employeeB.phone, shopName: sellerInfo.shopName });
                const memberList = await ss.spmdm.getMemberList({ orderBy: 'lastVisitTime', orderByDesc: true }).then(res => res.result.data);
                console.log(`memberList=${JSON.stringify(memberList)}`);
                expect(memberList.rows.find(obj => obj.custUserId == clientInfo.userId), '员工B关联客户列表找到了关联了员工A的客户').to.be.undefined;
            });
            describe('修改员工B的姓名为员工A', async function () {
                before('修改A为B的姓名', async function () {
                    await employeeB.newOrEditStaff({
                        userId: employeeB.userId,
                        userName: employeeA.name,
                        mobile: employeeB.phone,
                        rem: '员工B备注修改' + common.getRandomStr(5),
                        roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
                        oldRoleId: employeeB.roleId
                    });
                });
                it('员工列表', async function () {
                    const staff = await employeeB.getStaffList();
                    common.isApproximatelyEqualAssert(employeeB.getEmployee(), staff);
                });
                it('员工详情', async function () {
                    const staffInfo = await employeeB.getStaffInfo();
                    common.isApproximatelyEqualAssert(employeeB.getEmployee(), staffInfo);
                });
            });
            describe('转移员工B的单个客户给员工A', function () {
                before('新建客户关联员工B', async function () {
                    await ssReq.ssClientLogin({ sellerId: sellerBInfo.userId, tenantId: sellerBInfo.tenantId, mobile: common.getRandomMobile(), appId: ssAccount.seller7.appId });
                    clientInfoX = _.cloneDeep(LOGINDATA);
                });
                it('移交单个客户给员工A', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.spmdm.deliverStaffSingle({ userId: sellerAInfo.userId, custUserId: clientInfoX.userId });
                });
                it('员工B关联客户人数', async function () {
                    await ssReq.ssSellerLogin({ code: mobileB, shopName: sellerInfo.shopName });
                    await employeeB.getClientCountForEmployee();
                    expect(0, '关联客户人数').to.be.equal(employeeB.clientCount);
                });
                it('员工A关联客户人数', async function () {
                    await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
                    await employeeA.getClientCountForEmployee();
                    expect(2, '关联客户人数').to.be.equal(employeeA.clientCount);
                });
            });
            describe('员工B查看员工A的客户信息', async function () {
                before('员工B登录', async function () {
                    await ssReq.ssSellerLogin({ code: employeeB.phone, shopName: sellerInfo.shopName });
                });
                it('员工B查看客户列表', async function () {
                    // const res = await ss.spmdm.getMemberList({ searchKey: clientInfoX.nickName });
                    const ClientListB = await employeeB.getEmployee();
                    // console.log(ClientListB);
                    expect(0, '关联客户人数').to.be.equal(ClientListB.clientCount);
                });
                describe('客户X访问店铺-员工B查看访客', async function () {
                    let sellerBInfo, clientCount, orderList, purRes, orderInfo;
                    before('开启访客订单隔离,客户X访问店铺', async function () {
                        await ssReq.ssSellerLogin();

                        //开启访客订单隔离参数
                        customSellerIsolation = await ssConfigParam.getSpbParamInfo({ code: 'custom_seller_isolation', ownerId: sellerInfo.tenantId });
                        await customSellerIsolation.updateParam({ val: 1 });

                        //店员A登录
                        await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
                        sellerAInfo = _.cloneDeep(LOGINDATA);
                        //查询访客统计和查询订单,先取一次，下面做比较
                        clientCount = await ss.up.getSellerCount().then(res => res.result.data);
                        orderList = await sp.spTrade.salesFindBills().then(res => res.result);

                        await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: clientInfoX.mobile, appId: ssAccount.seller6.appId })
                        //下单流程，再此之前先创建一个收获地址，因为是新用户
                        const areaJson = await mockJsonParam.getAreaJson();
                        let json = await mockJsonParam.addAddrJson(areaJson);
                        json.recInfo.isDefault = 1;
                        json.address.provinceCode = 110000;   //这里省市区写死，为了保证此地有运费规则可以覆盖到
                        json.address.cityCode = 110100;
                        json.address.countyCode = 110101;
                        await spmdm.saveUserRecInfo(json);

                        //设置新增的地址为默认地址
                        // await spmdm.setDefaultUserRecInfo({
                        //     id: shipAddress.id, type: 1
                        // });

                        //下单（billmanage无法使用，目前使用最原始的下单流程）
                        const dres = dresManage.setupDres();
                        const getListDresSpu = await ss.spchb.getDresSpuList({
                            tenantId: sellerAInfo.tenantId,
                            orderBy: 'marketDate',
                            orderByDesc: true
                        });
                        const spuId = getListDresSpu.result.data.rows[0].id;
                        getFullForBuyer = await sp.spdresb.getFullForBuyer({
                            spuId: spuId,
                            _tid: sellerInfo.tenantId,
                            _cid: sellerInfo.clusterCode
                        });
                        dres.setByDetail(getFullForBuyer.result.data);
                        // console.log(dres);
                        // console.log(`getFullForBuyer=${JSON.stringify(getFullForBuyer)}`);

                        await spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                        // console.log(LOGINDATA.defAddressId);

                        const purJson = basicJson.purJson({
                            styleInfo: getFullForBuyer
                        });
                        purRes = await billManage.createPurBill(purJson);
                        // console.log('订单数据', purRes);

                        orderInfo = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId, srcType: 0, shopId: sellerInfo.shopId }).then(res => res.result.data);
                        // const bill = billManage.setUpPurBill();
                        console.log('订单信息', orderInfo);


                    });
                    after('老板关闭访客订单隔离参数', async function () {
                        await ssReq.ssSellerLogin();
                        await customSellerIsolation.updateParam({ val: 0 });
                    });
                    describe('员工B查询校验', async function () {
                        before('切换员工B登录', async function () {
                            await ssReq.ssSellerLogin({ code: employeeB.phone, shopName: sellerInfo.shopName });
                        });
                        it('查询访客记录', async function () {
                            const record = await ss.up.findSellerLogs().then(res => res.result.data.rows.find(obj => obj.id == clientInfoX.clusterId));
                            expect(record, '竟然可以查到员工A的访客记录').to.be.undefined;

                        });
                        it('查询访客记录按天隔离', async function () {
                            const dailyRecord = await ss.up.findSellerLogsByDay().then(res => res.result.data.rows.find(obj => obj.id == clientInfoX.clusterId));
                            expect(dailyRecord, '竟然可以查到员工A的访客记录').to.be.undefined;
                        });

                        it('查询访客统计', async function () {
                            const visitCount = await ss.up.getSellerCount().then(res => res.result.data);
                            common.isApproximatelyEqualAssert(visitCount, clientCount);
                        });
                        it.skip('搜索客户X', async function () {
                            const searchResult = await ss.spmdm.getMemberListByNamePy({ searchToken: '员工B' }).then(res => res.result.data);
                            expect(searchResult).to.be.undefined;
                        })
                        it('员工B查看销售单', async function () {
                            const orderListB = await sp.spTrade.salesFindBills().then(res => res.result);
                            expect(orderListB.count).to.be.equal(orderList.count);
                            expect(orderListB.data.rows.find(obj => obj.id == purRes.result.data.rows[0].billId)).to.be.undefined;
                        });
                        it('员工A查看销售单', async function () {
                            await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
                            const orderListBoss = await sp.spTrade.salesFindBills().then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                            common.isApproximatelyEqualAssert(orderListBoss, orderInfo, ['id', 'receiveAddress']);
                        });
                        it('老板查看销售单', async function () {
                            await ssReq.ssSellerLogin();
                            const orderListBoss = await sp.spTrade.salesFindBills().then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                            common.isApproximatelyEqualAssert(orderListBoss, orderInfo, ['id', 'receiveAddress']);
                        });
                        describe('退款-offline', async function () {
                            before('客户X退款', async function () {
                                //退款
                                await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: clientInfoX.mobile, appId: ssAccount.seller6.appId });
                                const payRes = await sp.spTrade.createPay({
                                    payType: 5,
                                    payMethod: 2,
                                    payerOpenId: LOGINDATA.wxOpenId,
                                    orderIds: [purRes.result.data.rows[0].billId],
                                    payMoney: purRes.params.jsonParam.orders[0].main.money
                                });
                                console.log(`payRes=${JSON.stringify(payRes)}`);
                                //支付
                                await sp.spTrade.receivePayResult({
                                    mainId: payRes.result.data.payDetailId,
                                    amount: payRes.params.jsonParam.payMoney,
                                    purBillId: purRes.result.data.rows[0].billId
                                });

                                await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: clientInfoX.mobile, appId: ssAccount.seller6.appId });
                                const purBillInfo = { purBillInfo: orderInfo, typeId: 0 };
                                const returnBillJson = await basicJson.returnBillJson2(purBillInfo);
                                const returnBill = await sp.spTrade.saveReturnBill(returnBillJson);
                                console.log(`returnBill=${JSON.stringify(returnBill)}`);

                                // returnBill.params.jsonParam.returnDate = returnBillDate;
                            });
                            it('员工A查看退货退款列表', async function () {
                                await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
                                const employeeArefund = await sp.spTrade.findSalesReturnBills().then(res => res.result.data.rows.find(obj => obj.billNo == orderInfo.bill.id));
                                // common.isApproximatelyArrayAssert(orderInfo, refund);
                            });
                            it('员工B查看退货退款列表', async function () {
                                await ssReq.ssSellerLogin({ code: employeeB.phone, shopName: sellerInfo.shopName });
                                const employeeBrefund = await sp.spTrade.findSalesReturnBills().then(res => res.result.data.rows.find(obj => obj.billNo == orderInfo.bill.id));
                                expect(employeeBrefund, '竟然找到了员工A的退款单').to.be.undefined;
                            });
                            it('老板查看退货退款列表', async function () {
                                await ssReq.ssSellerLogin();
                                const bossRefund = await sp.spTrade.findSalesReturnBills().then(res => res.result.data.rows.find(obj => obj.billNo == orderInfo.bill.id));
                            });
                        })


                    });


                });
            });
        });

    });

    describe('员工A移交客户、删除', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
        });
        describe('员工A移交客户前删除', async function () {
            before(async function () {
                const deleteStaff = await employeeA.deleteStaff({ check: false });
                expect(deleteStaff.result, '关联客户列表竟然为空').to.include({ msg: '请先移交客户' });
            });
            it('员工A关联客户列表', async function () {
                await employeeA.getClientListForEmployee();
                common.isApproximatelyEqualAssert(clientInfo, employeeA.getClientListExp(clientInfo), ['avatar']);
                // common.isApproximatelyEqualAssert(clientInfo, employeeA.getEmployee().clientList[0]);
                // common.isApproximatelyEqualAssert(clientInfo, employeeA.getEmployee().clientList[0], ['avatar']);
                // expect(clientInfo.avatar, `图片不相同`).to.include(employeeA.getEmployee().clientList[0].avatar)
            });
        });
        describe('员工A移交客户给管理员', async function () {
            before(async function () {
                await employeeA.deliverStaff({ userId: sellerInfo.userId });
                console.log(employeeA);
            });
            it('员工A关联客户列表', async function () {
                const clientListForEmployee = await employeeA.getClientListForEmployee();
                expect(clientListForEmployee, '关联客户列表竟然不为空').to.be.undefined;
            });
            it('员工A关联客户人数', async function () {
                await employeeA.getClientCountForEmployee();
                expect(0, '关联客户人数').to.be.equal(employeeA.clientCount);
            });
        });
        describe('员工A移交禁止访问客户B', async function () {
            let clientInfoB, mobileClient = common.getRandomMobile();
            before('新建客户B关联员工A', async function () {
                await ssReq.ssSellerLogin({ code: employeeA.phone, shopName: sellerInfo.shopName });
                sellerAInfo = _.cloneDeep(LOGINDATA);
                console.log(employeeA);

                await ssReq.ssClientLogin({ sellerId: sellerAInfo.userId, tenantId: sellerAInfo.tenantId, mobile: mobileClient, appId: ssAccount.seller7.appId });
                clientInfoB = _.cloneDeep(LOGINDATA);
                console.log(`clientInfoB=${JSON.stringify(clientInfoB)}`);
                await ss.spugr.getShopFromBuyer({ id: sellerInfo.shopId, sellerId: sellerAInfo.userId });

                await ssReq.ssSellerLogin();
                console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
            });
            it('员工A关联客户列表', async function () {
                await employeeA.getClientListForEmployee();
                common.isApproximatelyEqualAssert(clientInfoB, employeeA.getEmployee().clientList, ['avatar']);
                // expect(clientInfoB.avatar, `图片不相同`).to.include(employeeA.getEmployee().clientList[0].avatar)
            });
            it('员工A关联客户人数', async function () {
                await employeeA.getClientCountForEmployee();
                expect(1, '关联客户人数').to.be.equal(employeeA.clientCount);
            });
            describe('设置客户禁止访问', async function () {
                before(async function () {
                    await ssReq.ssSellerLogin();
                    const res = await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfoB.tenantId, flag: 0 })
                    console.log(`\n 设置客户禁止访问=${JSON.stringify(res)}`);
                });
                it('客户列表', async function () {
                    const memberList = await ss.spmdm.getMemberList({ custUserId: clientInfoB.userId }).then(res => res.result.data.rows[0]);
                    console.log(`\n memberList=${JSON.stringify(memberList)}`);
                    expect(memberList.flag, 'flag与设置的价格不符').to.equal(0);
                });
                describe('员工A移交客户给管理员', async function () {
                    before(async function () {
                        await employeeA.deliverStaff({ userId: sellerInfo.userId });
                        console.log(employeeA);
                    });
                    it('员工A关联客户列表', async function () {
                        const clientListForEmployee = await employeeA.getClientListForEmployee();
                        expect(clientListForEmployee, '关联客户列表竟然不为空').to.be.undefined;
                    });
                    it('员工A关联客户人数', async function () {
                        await employeeA.getClientCountForEmployee();
                        expect(0, '关联客户人数').to.be.equal(employeeA.clientCount);
                    });
                });
            });
        });
        describe('员工A删除', async function () {
            before(async function () {
                await ssReq.ssSellerLogin();
                // console.log(employeeA);
                await employeeA.deleteStaff();
            });
            it('员工列表', async function () {
                const staff = await employeeA.getStaffList();
                expect(staff, '员工列表居然找到了删掉的员工').to.be.undefined;
            });
        });
    });

    // 新增相同手机号的员工，权限为店员
    // bug#10764 提测单#1197
    describe('相同手机号新增店员', async function () {
        let newStaffInfo;
        const employeeA2 = employeeManage.setupEmployee();
        before('新增店员，手机号与被删除店员一致', async function () {
            //角色与最初新建的不一致
            await employeeA2.newOrEditStaff({
                userName: employeeA.name,
                mobile: employeeA.phone,
                rem: '店员A3备注' + common.getRandomStr(5),
                roleId: roleArr[1].id, // TODO roleArr直接取第2个元素的做法不保险 需要考虑别的方式
            });
        });
        after('删除店员', async function () {
            await employeeA2.deleteStaff();
        });
        // TODO 列表与详情接口需要分为2个it进行校验
        it('查询员工详情', async function () {
            newStaffInfo = await employeeA2.getStaffInfo();
            common.isApproximatelyEqualAssert(employeeA2.getEmployee(), newStaffInfo);
        });
        it('查询员工列表', async function () {
            // console.log(sellerInfo);
            const newStaff = await employeeA2.getStaffList();
            common.isApproximatelyEqualAssert(employeeA2.getEmployee(), newStaff);
        });
        it('我的界面数据,角色为变更后的店员', async function () {
            const storeFace = await ss.spauth.getUserSession({ userId: newStaffInfo.id }).then(res => res.result.data);
            // console.log(storeFace);
            expect(parseInt(storeFace.roleIds), '新增员工职位为原先职位').to.be.equal(newStaffInfo.roleId)
        });
    });

    describe.skip('管理员', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
        });

        describe('管理员编辑', async function () {
            before('编辑手机号、职位', async function () {
                //编辑手机号、角色（职位），应失败
                // await ss.spmdm.newOrEditStaff({userId:,roleId:,roleCode:,oldRoleId:});
            });
            it('管理员列表：与原来一致', async function () {
                await ss.spmdm.getStaffList();
            });
            it('管理员详情：与原来一致', async function () {
                // await ss.spmdm.getStaffInfo({userId:,roleId:});
            });
        });

        describe('管理员删除', async function () {
            before('删除管理员，应失败', async function () {
                // await ss.spmdm.deleteStaff({ userId: sellerAInfo.userId, roleId:});
            });
            it('员工列表：与原来一致', async function () {
                await ss.spmdm.getStaffList();
            });
            it('员工详情：与原来一致', async function () {
                // await ss.spmdm.getStaffInfo({userId:,roleId:});
            });
        });

        describe('管理员转让', async function () {
            before(async function () {
                //转让给员工B
                await ss.spmdm.transferStaff({ transferStaff: sellerInfo.userId, userId: sellerBInfo.userId });
            })
            it('员工列表', async function () {
                await ss.spmdm.getStaffList();
            });
            it('原管理员详情', async function () {
                //可能不存在
                // await ss.spmdm.getStaffInfo({userId:sellerInfo.userId,roleId:});
            });
            it('新管理员详情', async function () {
                //姓名、手机号、职位、关联员工
                // await ss.spmdm.getStaffInfo({userId:sellerBInfo.userId,roleId:});
            });
        });
    });

    describe.skip('账号购买', async function () {
        before('获取账号数量', async function () {
            //购买
        });
        it('获取账号数量', async function () {
            //员工列表接口
        });
    });
});
