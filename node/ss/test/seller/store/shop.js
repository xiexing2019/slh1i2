const ssReq = require('../../../help/ssReq');
const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
// require('../../../../report/htmlReport');

describe('卖家店铺', async function () {
    this.timeout(TESTCASE.timeout);

    before(async function () {
        await ssReq.ssSellerLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    /**
     * 修改spg.sp_shop的bizModelId 
     * 前端暂不支持修改,服务端不作限制
     */
    describe('店铺运营模式', async function () {
        describe('修改运营模式为零售', async function () {
            before(async function () {
                await ss.spugr.updateShopOperateModel({ shopId: LOGINDATA.tenantId, bizModelId: 2 });
            });
            it('获取店铺详情', async function () {
                const shopInfo = await sp.spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
                expect(shopInfo).to.includes({ bizModelId: 2 });
            });
        });
        describe('修改运营模式为批发', async function () {
            before(async function () {
                await ss.spugr.updateShopOperateModel({ shopId: LOGINDATA.tenantId, bizModelId: 1 });
            });
            it('获取店铺详情', async function () {
                const shopInfo = await sp.spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
                expect(shopInfo).to.includes({ bizModelId: 1 });
            });
        });
    });


    describe('维护价格体系', async function () {
        let priceType;
        before('修改价格', async function () {
            const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
            priceType = dictList.slice(-1)[0];
            priceType.codeName = `价格${common.getRandomStr(3)}`;
            await ss.config.saveDict(priceType);
        });
        it('查询价格列表', async function () {
            const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
            const _priceType = dictList.find(ele => ele.id == priceType.id);
            common.isApproximatelyEqualAssert(priceType, _priceType, ['updatedDate', 'updatedBy']);
        });
    });

    describe('主营类目', async function () {
        let masterClass;
        before(async function () {
            const dictList = await ss.config.getSpgDictList({ typeId: 2010, flag: 1 }).then(res => res.result.data.rows);
            masterClass = dictList[common.getRandomNum(0, dictList.length - 1)];
            // console.log(masterClass);
            await sp.spugr.updateShop({ id: LOGINDATA.tenantId, masterClassId: masterClass.codeValue });
        });
        it('获取店铺详情', async function () {
            const shopInfo = await sp.spugr.getShop({ id: LOGINDATA.tenantId }).then(res => res.result.data);
            expect(shopInfo).to.includes({ masterClassId: masterClass.codeValue, masterClassIdValid: true });
        });
    });

    // sp_shop, ss_set, ss_set_func, ss_set_func_cat
    // 等注册流程提测再完善测试点
    describe('店铺功能列表', async function () {
        it('分类获取店铺功能列表', async function () {
            const res = await ss.spugr.getShopSuiteFuncList({ tenantId: LOGINDATA.tenantId });
            // console.log(`res=${JSON.stringify(res)}`);
        });
    });



});