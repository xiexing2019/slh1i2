const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const ssReq = require('../../../help/ssReq');
const ssAccount = require('../../../data/ssAccount');
const getShop = require('../../../help/shop/getShop');
const ssConfigParam = require('../../../help/configParamManager');
const dresManage = require('../../../help/dresManage');
const basicJson = require('../../../help/basicJson');


describe('价格类别', async function () {
    this.timeout(TESTCASE.timeout);
    const sellerShop = getShop.getSellerShop();
    before('获取价格列表', async function () {
        await sellerShop.switchSeller();
        await sellerShop.getPriceType();
        // console.log(sellerShop.priceType);
    });

    after('启用所有价格', async function () {
        for (const type of sellerShop.priceType.priceTypes.values()) await type.startPriceTypeDict();
    });

    describe('价格类别', async function () {
        let type;
        before('选取价格类别', async function () {
            const id = [...sellerShop.priceType.priceTypes.keys()].find(key => key != 0 && key != 1);
            console.log(`\n选用了价格:${id}`);
            type = sellerShop.priceType.priceTypes.get(id);
            console.log(type);
        });

        //禅道地址：  http://zentao.hzdlsoft.com:6082/zentao/bug-view-13231.html
        describe('作废codeValue不为0的价格', async function () {
            let clientInfo;
            before('作废', async function () {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: ssAccount.clientAlternate.mobile, openId: ssAccount.clientAlternate.openId, shopId: sellerInfo.shopId });
                clientInfo = _.cloneDeep(LOGINDATA);
                await ssReq.ssSellerLogin();
                //强行关闭推广价
                ssDisPriceType = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_price_type', ownerId: sellerInfo.tenantId });
                await ssDisPriceType.updateParam({ val: 0 });
                const validPrice = await ss.spmdm.updateMemberValidPrice({ custTenantId: clientInfo.tenantId, validPrice: type.localPriceTypeId });
                await type.stopPriceTypeDict();
            });
            it('价格列表', async function () {
                await type.priceTypeListAssert();
            });
            describe('价格作废后指定默认价', async function () {
                before('指定默认价', async function () {
                    await ss.spmdm.updateMemberValidPrice({ custTenantId: clientInfo.tenantId, validPrice: 0 });
                });
                it('查询客户适用价', async function () {
                    const res = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
                    const clientVaild = res.find(obj => obj.custTenantId == clientInfo.tenantId);
                    // console.log('res', clientVaild);
                    expect(clientVaild.validPrice).to.be.equal(0);
                });
                it('价格列表', async function () {
                    await type.priceListAssert({ state: false });
                });
            });

            describe('启用codeValue不为0的价格', async function () {
                before('启用', async function () {
                    console.log(type);
                    await type.startPriceTypeDict();
                });
                it('价格列表', async function () {
                    await type.priceListAssert({ state: true });
                });
            });

            describe('修改codeValue不为0的价格', async function () {
                before('修改', async function () {
                    const selectItemIds = [...sellerShop.priceType.selectItems.keys()];
                    await type.updatePriceTypeDict({
                        codeName: `价格${type.outPriceTypeId}_${common.getRandomStr(3)}`,
                        props: { addition: common.getRandomNum(0, 50), percentage: common.getRandomNum(0, 1, 2), outPriceTypeId: selectItemIds[common.getRandomNum(0, selectItemIds.length - 1)] }, //0是默认价
                    }, sellerShop.priceType.selectItems);
                });
                it('价格列表', async function () {
                    await type.priceListAssert({ state: true });
                });
            });
        });


        describe.skip('设置取整', async function () {
            const pricedecOptions = [{ id: 0, name: "保留两位小数" }, { id: 1, name: "四舍五入取整" }, { id: 2, name: "向上取整" }, { id: 3, name: "向下取整" }];
            // const pricedecOptions = [{ id: 1, name: "四舍五入取整" }];
            let dictTypeId;
            before('设置价格取值', async function () {
                const selectItemIds = [...sellerShop.priceType.selectItems.keys()];
                for (const type of sellerShop.priceType.priceTypes.values()) {
                    await type.updatePriceTypeDict({
                        codeName: `价格${common.getRandomStr(3)}`,
                        props: { addition: common.getRandomNum(0, 50), percentage: common.getRandomNum(0, 1, 2), outPriceTypeId: selectItemIds[common.getRandomNum(0, selectItemIds.length - 1)] }, //0是默认价
                    }, sellerShop.priceType.selectItems);
                }
                await dresManage.prePrepare();

                // 初始化字典
                dictTypeId = await dresManage.setupSpec();
                await dresManage.prePrepare({ 'dictTypeId': dictTypeId }, (spu, skus) => {
                    spu.pubPrice = common.getRandomNum(100, 200);
                    for (let value = 1; value <= 5; value++) {
                        let price = `price${value}`;
                        delete spu[price];
                        skus.map(data => {
                            skus.pubPrice = spu.pubPrice;
                            delete data[price];
                        });
                    }
                });
            });
            after('恢复为保留两位小数', async function () {
                await sellerShop.priceType.updatePricedec(sellerShop, 0);
            });
            it('取整方式列表', async function () {
                const data = await sellerShop.priceType.getPriceTypeListInApp();
                common.isApproximatelyArrayAssert(pricedecOptions, data.pricedecOptions);
            });
            for (const option of pricedecOptions) {
                describe(`普通addSpuType为0,价格取整方式:${JSON.stringify(option)}`, async function () {
                    const dres = dresManage.setupDres({ type: 'app' });
                    before('新建商品', async function () {
                        await sellerShop.priceType.updatePricedec(sellerShop, option.id);
                        //保存商品
                        const json = basicJson.styleJsonByApp({},
                            (spu, skus) => {
                                spu.pubPrice = 199;
                                for (let value = 1; value <= 5; value++) {
                                    let price = `price${value}`;
                                    delete spu[price];
                                    skus.map(sku => {
                                        sku.pubPrice = 199;
                                        delete sku[price];
                                    });
                                }
                            });
                        // console.log(json);
                        await dres.saveDresByApp(json, { priceType: sellerShop.priceType });
                        // console.log(dres);
                    });
                    it('卖家查询商品详情', async function () {
                        const dresFull = await dres.getFullById();
                        // console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
                        // console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);
                        common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['priClassId', 'spec1', 'spec2', 'spec3', 'templateId']);
                    });
                    for (const opt of pricedecOptions) {
                        describe(`修改商品价格,价格取整方式:${JSON.stringify(opt)}`, async function () {
                            before('修改商品价格', async function () {
                                const json = {
                                    pubPrice: common.getRandomNum(100, 200, 2),
                                    skuChangeFlag: 0,
                                    skus: [...dres.skus.values()]
                                }
                                // for (let value = 1; value <= 5; value++) {
                                //     let price = `price${value}`;
                                // json[price] = common.getRandomNum(100, 200, 2);
                                json.skus.map(sku => {
                                    sku.pubPrice = json.pubPrice;
                                    // sku[price] = common.getRandomNum(100, 200, 2);
                                });
                                // }
                                console.log(`\n修改商品json=${JSON.stringify(json)}`);
                                await dres.updateSpuForSeller(json);
                                await dres.updatePricedec(sellerShop.priceType, sellerShop, opt.id);
                            });
                            it('卖家查询商品详情', async function () {
                                // this.retries(2);
                                await common.delay(2000);
                                // console.log(dres);
                                // await sellerShop.priceType.getExpForPrice(dres);
                                const dresFull = await dres.getFullById();
                                console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
                                // console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);
                                common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['priClassId', 'spec1', 'spec2', 'spec3', 'templateId'], dres.id);
                            });
                        });
                    }
                });
                describe(`泛行业addSpuType为1,价格取整方式:${JSON.stringify(option)}`, async function () {
                    const dres = dresManage.setupDres({ type: 'app' });
                    let dresJson;
                    before('新建商品', async function () {
                        //保存商品
                        dresJson = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId },
                            (spu, skus) => {
                                for (let value = 1; value <= 5; value++) {
                                    let price = `price${value}`;
                                    delete spu[price];
                                    skus.map(sku => {
                                        sku.pubPrice = common.getRandomNum(100, 200);
                                        delete sku[price];
                                    });
                                }
                                spu.pubPrice = (skus.map(sku => sku.pubPrice)).sort()[0];
                            });
                        console.log(`json=${JSON.stringify(dresJson)}`);
                        // await dres.saveDres(json);
                        await dres.saveDresByApp(dresJson, { priceType: sellerShop.priceType });
                    });
                    it('卖家查询商品详情', async function () {
                        const dresFull = await dres.getFullById();
                        // console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
                        // console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);
                        common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['pubPriceLow', 'pubPriceTop', 'priClassId', 'spec1', 'spec2', 'spec3', 'templateId'], dres.id);
                    });
                    for (const opt of pricedecOptions) {
                        describe(`修改商品价格,价格取整方式:${JSON.stringify(opt)}`, async function () {
                            before('修改商品价格', async function () {
                                for (let value = 1; value <= 5; value++) {
                                    let price = `price${value}`;
                                    delete dresJson.spu[price];
                                    dresJson.skus.map(sku => {
                                        sku.pubPrice = common.getRandomNum(100, 200);
                                        delete sku[price];
                                    });
                                }
                                dresJson.spu.pubPrice = (dresJson.skus.map(sku => sku.pubPrice)).sort()[0];
                                dresJson.id = dres.id;
                                dresJson.spu.id = dres.id;
                                console.log(`\ndresJson=${JSON.stringify(dresJson)}`);

                                await dres.saveDresByApp(dresJson, { priceType: sellerShop.priceType });
                                // console.log(`\n修改商品json=${JSON.stringify(json)}`);
                                // await dres.updateSpuForSeller(json);
                                await dres.updatePricedec(sellerShop.priceType, sellerShop, opt.id);
                            });
                            it('卖家查询商品详情', async function () {
                                // this.retries(2);
                                await common.delay(2000);
                                const dresFull = await dres.getFullById();
                                console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
                                // console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);
                                common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull, ['pubPriceLow', 'pubPriceTop', 'priClassId', 'spec1', 'spec2', 'spec3', 'templateId'], dres.id);
                            });
                        });
                    }
                });
            }
        });

    });
});
