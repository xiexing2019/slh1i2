const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const basicJson = require('../../../help/basicJson');

const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../data/doc.json')))[caps.name];

describe('银行卡', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async function () {
        //管理员登录
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
    });
    it('通过银行卡图片获取银行信息', async function () {
        const bankInfos = await ss.spugr.getBankInfoByImage({ docId: docData.card.cardImage.docId }).then(res => res.result.data.data.items);
        bankInfos.forEach(bankInfo => {
            if (bankInfo.item == '卡号') {
                expect(bankInfo, `获取卡号失败`).to.includes({ itemstring: docData.card.cardNum });
            } else if (bankInfo.item == '卡类型') {
                expect(bankInfo, `获取卡类型失败`).to.includes({ itemstring: '借记卡' });
            } else if (bankInfo.item == '卡名字') {
                expect(bankInfo, `获取卡名字失败`).to.includes({ itemstring: '中信贵宾卡(银联卡)' });
            } else if (bankInfo.item == '银行信息') {
                expect(bankInfo, `获取银行信息失败`).to.includes({ itemstring: '中信银行(03020000)' });
            }
        });
    });
    describe.skip('新增或修改商户', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
        });
        it('新增商户已有的商户', async function () {
            let merchantJson = basicJson.merchantJson();
            Object.assign(merchantJson, {
                name: '测试' + common.getRandomStr(5),
                contact: '申请人' + common.getRandomStr(5),
                mobile: common.getRandomMobile()
            });
            const newMerchat = await sp.spugr.createMerchat(merchantJson);
            console.log(`newMerchat=${JSON.stringify(newMerchat)}`);
        });
        it('修改商户', async function () {
            const merchant = await sp.spugr.findMerchant().then(res => res.result.data);
            let merchantJson = basicJson.merchantJson({ id: merchant.id });
            Object.assign(merchantJson, {
                name: '测试' + common.getRandomStr(5),
                contact: '申请人' + common.getRandomStr(5),
                mobile: common.getRandomMobile()
            });
            const newMerchat = await sp.spugr.createMerchat(merchantJson);
            console.log(`newMerchat=${JSON.stringify(newMerchat)}`);
        });
        it.skip('银行卡改绑', async function () {
            const merchant = await sp.spugr.findMerchant().then(res => res.result.data);
            const cardJson = basicJson.merchantJson({ id: merchant.id });
            // console.log(cardJson);
            const updateMerchantAndBank = await ss.spugr.updateMerchantAndBank(cardJson);
            console.log(`updateMerchantAndBank=${JSON.stringify(updateMerchantAndBank)}`);
        });
    });
});