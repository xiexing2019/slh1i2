const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const caps = require('../../../../data/caps');
const fs = require('fs');
const path = require('path');
const getShop = require('../../../help/shop/getShop');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../data/doc.json')))[caps.name];


describe('店铺图片', async function () {
    this.timeout(TESTCASE.timeout);
    const sellerShop = getShop.getSellerShop();
    // let sellerShop;
    before('登录', async function () {
        console.log(sellerShop);
        await sellerShop.switchSeller();
        console.log(LOGINDATA);
    });
    it('身份证识别', async function () {
        const res = await ss.spugr.idCardDetect({ frontUrl: docData.idCard.frontImage.docUrl, backUrl: docData.idCard.backImage.docUrl }).then(res => res.result.data);
        console.log(`res=${JSON.stringify(res)}`);
        common.isApproximatelyEqualAssert(docData.idCard.cardMsg, res);
    });
    it('店铺内照片', async function () {
        const res = await ss.spugr.getRandomShopInImg().then(res => res.result.data);
        console.log(`res=${JSON.stringify(res)}`);
        expect(res, `没有随机返回一张照片`).to.have.key('val');
        expect(res.val, `返回照片为空`).not.to.eql('');
    });
    it('店铺门头照', async function () {
        const res = await ss.spugr.getRandomShopOutImg({ shopName: common.getRandomChineseStr(10) }).then(res => res.result.data);
        console.log(`res=${JSON.stringify(res)}`);
        expect(res, `没有返回一张门头照`).to.have.key('val');
        expect(res.val, `返回照片为空`).not.to.eql('');
    });

});
