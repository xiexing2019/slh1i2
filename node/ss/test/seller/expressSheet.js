const ss = require('../../../reqHandler/ss');
const ssReq = require('../../help/ssReq');

const expNum = 544294162450;

/**
 * 根据快递单号智能匹配物流商信息
 * http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1618.html
 */
describe('物流商', async function () {
    this.timeout(30000);
    it('卖家根据快递单号智能匹配物流商信息', async function () {
        await ssReq.ssSellerLogin();
        await ss.print.intelFindMatchedExpress({ expNum }).then(res => {
            const data = res.result.data.rows.find(val => val.code == 'zhongtong');
            expect(data, `快递单号:${expNum},未匹配到物流商信息\n\t${JSON.stringify(res)}`).not.to.be.undefined;
        });
    });
    it('买家根据快递单号智能匹配物流商信息', async function () {
        await ssReq.userLoginWithWx();
        await ss.print.intelFindMatchedExpress({ expNum }).then(res => {
            const data = res.result.data.rows.find(val => val.code == 'zhongtong');
            expect(data, `快递单号:${expNum},未匹配到物流商信息\n\t${JSON.stringify(res)}`).not.to.be.undefined;
        });
    });
});