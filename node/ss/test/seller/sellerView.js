const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss/index');
const ssAccount = require('../../data/ssAccount');

describe('卖家查看买家访问商品记录', async function () {
    this.timeout(300000);
    let sellerInfo, clientInfo;
    let clientVisitInfo;
    let dresIds;
    let mobile = common.getRandomMobile();
    before('买家登录，访问某2个商品', async function () {
        const goodDTO = [];
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
        clientInfo = _.cloneDeep(LOGINDATA);

        dresIds = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows.slice(0, 2).map(data => data.id));
        for (let i = 0; i < dresIds.length; i++) {
            const dresFull = await spdresb.getFullForBuyer({ spuId: dresIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data)
            await spdresb.saveBuyerView({ spuId: dresIds[i], sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 1 });
            const spu = dresFull.spu;
            goodDTO.push({
                "spuId": dresIds[i],
                "date": common.getCurrentDate(),
                "picUrl": spu.coverUrl,
                "goodsName": spu.name,
                "goodsTitle": spu.title,
                "price": spu.pubPrice,
                "stockNum": spu.stockNum,
                "code": spu.code
            });
        }
        clientVisitInfo = {
            "browseDate": common.getCurrentDate(),
            "goodDTO": goodDTO
        };
    });

    it('卖家登录查看买家访问的这2个商品的记录表', async function () {
        await ssReq.ssSellerLogin();
        const dressVisits = await ss.up.sellerViewQueryByDate({ buyerId: clientInfo.tenantId }).then(res => res.result.data.rows);
        console.log(`dressVisits=${JSON.stringify(dressVisits)}`);

        const dto = dressVisits.find(data => data.goodsDTO.map(data => data.spuId == dresIds) && data.browseDate == common.getCurrentDate());
        expect(dto, '未找到买家访问过的商品记录').not.to.be.undefined;
        common.isApproximatelyEqualAssert(clientVisitInfo, dto);
    });

});

describe('同一个商品连续查看2次 只会有最近的一条记录', async function () {
    this.timeout(300000);
    let sellerInfo, clientInfo;
    let clientVisitInfo;
    let dresId;
    let mobile = common.getRandomMobile();
    before('买家登录，访问某2个商品', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
        clientInfo = _.cloneDeep(LOGINDATA);
        dresId = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows[0].id);

        const dresFull = await spdresb.getFullForBuyer({ spuId: dresId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data)
        await spdresb.saveBuyerView({ spuId: dresId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 1 });
        //延迟5分钟再查看同一个商品
        await common.delay(1000);
        await spdresb.saveBuyerView({ spuId: dresId, sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, type: 1 });
        clientVisitInfo = {
            "browseDate": common.getCurrentDate(),
            "goodDTO": {
                "spuId": dresId,
                "date": common.getCurrentDate(),
                "picUrl": dresFull.spu.coverUrl,
                "goodsName": dresFull.spu.name,
                "goodsTitle": dresFull.spu.title,
                "price": dresFull.spu.pubPrice,
                "stockNum": dresFull.spu.stockNum,
                "code": dresFull.spu.code
            }
        };
    });

    it('卖家登录查看买家访问的这个商品的记录表，应该只有1条', async function () {
        await ssReq.ssSellerLogin();
        const dressVisits = await ss.up.sellerViewQueryByDate({ buyerId: clientInfo.tenantId }).then(res => res.result.data.rows);

        const dto = dressVisits.find(data => data.goodsDTO.map(data => data.spuId == dresId) && data.browseDate == common.getCurrentDate());
        expect(dto, '未找到买家访问过的商品记录').not.to.be.undefined;
        common.isApproximatelyEqualAssert(clientVisitInfo, dto);
    });

});


