const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');

describe('套餐', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;

    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    it('套餐订购记录列表', async function () {
        const vasList = await ss.spugr.findVasBillList();
        console.log(`vasList=${JSON.stringify(vasList)}`);

    });

    describe('套餐', function () {
        let shopInfo;
        before('系统管理端获取卖家套餐', async () => {
            await ss.spAuth.staffLogin();
            const shopList = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName }).then(res => res.result.data.rows);
            const sellerShop = shopList.find(ele => ele.name == sellerInfo.shopName);
            expect(sellerShop, `查询不到店铺${sellerInfo.shopName}`).not.to.be.undefined;
            shopInfo = {
                setName: sellerShop.ecCaption.suiteId,
                endDate: common.stringToDate(sellerShop.endDate).format('Y-MM-DD'),
                startDate: common.stringToDate(sellerShop.startDate).format('Y-MM-DD'),
                saasType: 0,// 开通流程校验
            }
        });
        it('卖家获取套餐信息', async () => {
            await ssReq.ssSellerLogin();
            // common.delay(500);
            const shopSetInfo = await ss.spugr.getShopSetInfo();
            // console.log(`shopSetInfo=${JSON.stringify(shopSetInfo)}`);
            // setName：套餐名称，endDate：套餐到期日期，startDate：套餐购买日期
            common.isApproximatelyEqualAssert(shopInfo, shopSetInfo.result.data, ['saasType']);
        });
        it.skip('saasType校验', async function () {

        });
    });

});