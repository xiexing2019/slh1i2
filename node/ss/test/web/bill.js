const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssReq = require('../../help/ssReq');
const moment = require('moment');

describe('web订单', function () {
    this.timeout(30000);
    before('卖家登录', async () => {
        await ssReq.ssSellerLogin();
    });
    describe('卖家web订单明细导出', function () {
        it('卖家订单导出-30天以内', async function () {
            const startTime = moment().subtract(30, 'days').format('YYYY-MM-DD');
            const res = await ss.spsales.exportBillGroupByDetail({ startTime: startTime, endTime: common.getCurrentDate() });
            expect(res.result.data, `卖家订单导出失败`).to.have.own.property('val');
        });
        it('卖家订单导出-30天以上', async function () {
            const startTime = moment().subtract(31, 'days').format('YYYY-MM-DD');
            const res = await ss.spsales.exportBillGroupByDetail({ startTime: startTime, endTime: common.getCurrentDate(), check: false });
            expect(res.result, `卖家订单导出30天限制失效！`).to.be.include({ msgId: 'exprot_billDetail_over_time' })
        });
    });

});