const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssReq = require('../../help/ssReq');

describe('店铺列表-offline', async function () {
    this.timeout(30000);
    let sellerInfo, staffInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        await ss.spAuth.staffLogin();
        staffInfo = _.cloneDeep(LOGINDATA);
        console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });

    describe('修改店铺别名', async function () {
        let alias = common.getRandomChineseStr(3);
        before(async function () {
            await ss.spugr.updateAlias({ shopId: sellerInfo.shopId, alias: alias });
        });
        it('查询店铺列表', async function () {
            const shopList = await ss.spugr.getShopList({ aliasLike: alias }).then(res => res.result.data.rows);
            expect(shopList.find(ele => ele.name == sellerInfo.shopName && ele.alias == alias), '没有找到店铺').to.not.be.undefined;
        });
    });

    describe('更新店铺额外操作员数量', async function () {
        let extraManageNum = common.getRandomNum(1, 10), time, shopBefore;
        before('更新店铺额外操作员数量', async function () {
            shopBefore = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName }).then(res => res.result.data.rows.find(ele => ele.tenantId == sellerInfo.tenantId));
            console.log(`\nshopBefore=${JSON.stringify(shopBefore)}`);
            const updateExtraManageNum = await ss.spugr.updateExtraManageNum({ shopId: sellerInfo.shopId, extraManageNum: extraManageNum });
            console.log(`\nupdateExtraManageNum=${JSON.stringify(updateExtraManageNum)}`);
            time = updateExtraManageNum.opTime;
        });
        it('查询店铺额外操作员修改记录', async function () {
            const extraChangeLogLogs = await ss.spugr.findExtraChangeLogLogs({ shopId: sellerInfo.shopId }).then(res => res.result.data.rows);
            console.log(`\nextraChangeLogLogs=${JSON.stringify(extraChangeLogLogs)}`);
            common.isApproximatelyEqualAssert({
                createdBy: staffInfo.userId,
                createdDate: time,
                num: extraManageNum,
                shopTenantId: sellerInfo.shopId
            }, extraChangeLogLogs[0]);
        });
        it('查询店铺列表', async function () {
            const shop = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName }).then(res => res.result.data.rows.find(ele => ele.tenantId == sellerInfo.tenantId));
            console.log(`\nshop=${JSON.stringify(shop)}`);
            common.isApproximatelyEqualAssert({ totalShopManageNum: shopBefore.totalShopManageNum, extraManageNum: extraManageNum }, shop)
        });
        it('最大员工数量', async function () {
            await ssReq.ssSellerLogin();
            const maxShopManagerNum = await ss.spmdm.getStaffList().then(res => res.result.data.maxShopManagerNum);
            // console.log(maxShopManagerNum);
            expect(shopBefore.totalShopManageNum + extraManageNum, '操作员(员工)数量与预期不符').to.equal(maxShopManagerNum);
        });
    });

});