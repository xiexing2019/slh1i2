const common = require('../../../lib/common');
const pageFragment = require('../../data/pageFragment');
const wschelper = require('../../../reqHandler/ss/spb/spdresup');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');
const basicJson = require('../../help/basicJson');
const dresManage = require('../../help/dresManage');

const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name].image.shopBackground;

describe('微商城图库-offline', async function () {
    this.timeout(TESTCASE.timeout);
    describe('图库', async function () {
        let dictList, galleryJson, galleryId;
        before('新增', async function () {
            await ss.spAuth.staffLogin();
            //获取图库尺寸类型字典
            dictList = await ss.config.getSpgDictList({ typeId: 3027, flag: 1 }).then(res => res.result.data.rows);
            console.log(`\n dictList=${JSON.stringify(dictList)}`);
            galleryJson = {
                actContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }],
                bannerContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }],
                name: `图库${common.getRandomStr(5)}`,
                num: 2,
                sizeType: dictList[0].codeValue
            }
            galleryId = await ss.confg.createGallery(galleryJson).then(res => res.result.data.val);
            console.log(`galleryId=${JSON.stringify(galleryId)}`);
        });
        it('查询图库列表', async function () {
            const galleryList = await ss.confg.galleryList({ name: galleryJson.name, sizeType: dictList[0].codeValue }).then(res => res.result.data.rows);
            common.isApproximatelyEqualAssert(galleryJson, galleryList.find(obj => obj.id == galleryId));
        });
        describe('更新', async function () {
            before('更新', async function () {
                if (!galleryId) this.parent ? this.parent.skip() : this.skip();
                galleryJson = {
                    id: galleryId,
                    actContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }],
                    bannerContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }],
                    name: `图库${common.getRandomStr(5)}`,
                    num: 2,
                    sizeType: dictList[0].codeValue
                }
                await ss.confg.updateGallery(galleryJson);
            });
            it('查询图库列表', async function () {
                const galleryList = await ss.confg.galleryList({ name: galleryJson.name, sizeType: dictList[0].codeValue }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(galleryJson, galleryList.find(obj => obj.id == galleryId));
            });
        });
        describe('删除', async function () {
            before('删除', async function () {
                if (!galleryId) this.parent ? this.parent.skip() : this.skip();
                await ss.confg.deleteGallery({ id: galleryId });
            });
            it('查询图库列表', async function () {
                const galleryList = await ss.confg.galleryList({ name: galleryJson.name, sizeType: dictList[0].codeValue }).then(res => res.result.data.rows);
                expect(galleryList.find(obj => obj.id == galleryId), '查询图库列表居然查到了').to.be.undefined;
            });
        });
    });
    describe('分类图库', async function () {
        let groyGalleryId, subGroyGalleryId, parentCateName, subCateName, docId;
        before('登录', async function () {
            await ss.spAuth.staffLogin();
        });
        after('删除所有图库', async function () {
            const cateList = await ss.confg.findCategoryGallery().then(res => res.result.data.rows);
            const goryIds = cateList.map(obj => obj.id);
            for (let i = 0; i < goryIds.length; i++) {
                const res = await ss.confg.delCategoryGallery({ id: goryIds[i] }).then(res => res.result);
                console.log(res);

            }
            const cateLists = await ss.confg.findCategoryGallery().then(res => res.result.data.rows);
            expect(cateLists).to.have.lengthOf(0);
            console.log(cateLists);

        });
        describe('保存分类图库', async function () {
            let cateGoryExp, treeCateGotyExp, parentName = common.getRandomStr(2), subName = common.getRandomStr(3);
            before('保存分类图库', async function () {
                const CategoryGalleryJson = {
                    name: parentName,
                    type: 1,
                    content: [{ name: subName, docContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }] }]
                };
                res = await ss.confg.saveCategoryGallery(CategoryGalleryJson).then(res => res);
                console.log(`res=${JSON.stringify(res)}`);

                //父类名、子类名、父类id、子类id、图片id
                parentCateName = res.params.jsonParam.name;
                subCateName = res.params.jsonParam.content[0].name;
                const cateList = await ss.confg.findCategoryGallery({ name: subCateName }).then(res => res.result.data.rows);
                subGroyGalleryId = cateList[0].id;

                parentGroyGalleryId = res.result.data.val;
                docId = res.params.jsonParam.content[0].docContent[0].docId;
                updateGroyGallery(res);
                updateSubGroyGallery(res, subGroyGalleryId);

                treeCateGotyExp = {
                    id: subGroyGalleryId,
                    name: res.params.jsonParam.name,
                    parentId: groyGalleryId,
                    docContent: res.params.jsonParam.content.docContent,
                    subNodes: [],
                }
            });
            it('分类图库查询', async function () {
                const cateList = await ss.confg.findCategoryGallery({ name: parentCateName }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(getGroyGalleryExp(parentGroyGalleryId), cateList.find(obj => obj.id == parentGroyGalleryId));
            });
            it('查询分类图库树形结构（子类查询）', async function () {
                const cateList = await ss.confg.findCategoryGalleryTree({ type: 1, parentId: parentGroyGalleryId }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(getGroyGalleryTreeExp(subGroyGalleryId, parentGroyGalleryId), cateList.find(obj => obj.id == subGroyGalleryId));
            });
        })
        describe('修改分类图库', async function () {
            let fixCategoryGalleryJson;
            before('修改分类图库', async function () {
                fixCategoryGalleryJson = {
                    id: parentGroyGalleryId,
                    name: common.getRandomStr(2),
                    type: 1,
                    content: [{ id: subGroyGalleryId, name: subCateName, type: 1, docContent: [{ typeId: 1, docId: docId }] }]
                };
                const fixRes = await ss.confg.saveCategoryGallery(fixCategoryGalleryJson).then(res => res);
                updateGroyGallery(fixRes);
                updateSubGroyGallery(fixRes, subGroyGalleryId)

            });
            it('分类图库查询', async function () {
                const cateList = await ss.confg.findCategoryGallery({ name: fixCategoryGalleryJson.name }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(getGroyGalleryExp(parentGroyGalleryId), cateList.find(obj => obj.id == parentGroyGalleryId));
                // common.isArrayContainObject(cateList, FixcateGoryExp);
            });
            describe('图库图片替换', async function () {
                before('替换图片', async function () {
                    const fixDocCategoryGalleryJson = {
                        id: parentGroyGalleryId,
                        name: fixCategoryGalleryJson.name,
                        type: 1,
                        content: [{ id: subGroyGalleryId, name: subCateName, type: 1, docContent: [{ typeId: 1, docId: docData[common.getRandomNum(0, docData.length - 1)].docId }] }]
                    };
                    const fixRes = await ss.confg.saveCategoryGallery(fixDocCategoryGalleryJson).then(res => res);
                    updateGroyGallery(fixRes);
                    updateSubGroyGallery(fixRes, subGroyGalleryId)
                });
                it('分类图库查询', async function () {
                    const cateList = await ss.confg.findCategoryGallery({ name: fixCategoryGalleryJson.name }).then(res => res.result.data.rows);
                    common.isApproximatelyEqualAssert(getGroyGalleryExp(parentGroyGalleryId), cateList.find(obj => obj.id == parentGroyGalleryId));
                });
            });
        });
        describe('删除分类图库', async function () {
            before('删除分类图库', async function () {
                await ss.confg.delCategoryGallery({ id: parentGroyGalleryId })
            });
            it('分类图库查询', async function () {
                const cateList = await ss.confg.findCategoryGallery().then(res => res.result.data.rows);
                expect(cateList.find(obj => obj.id == parentGroyGalleryId), `分类图库未删除成功,${cateList}`).to.be.undefined;

            });
        });

    });

});



/**图库分类期望值
 * id = 图库id 
 */
function getGroyGalleryExp(id) {
    const groyGallery = groyGalleryMap.get(id);
    if (groyGallery == undefined) {
        throw new Error('未在groyGalleryMap找到图库信息')
    }
    return groyGallery;
};

/**树状图（子类）期望值
 * id = 图库id
 * parentId = 夫类id
 */
function getGroyGalleryTreeExp(id, parentId) {
    let groyGallery = groyGalleryMap.get(id);
    groyGallery.parentId = parentId;

    if (groyGallery == undefined) {
        throw new Error('未在groyGalleryMap找到图库信息')
    }
    return groyGallery;
};

/**更新图库分类 */
function updateGroyGallery(res) {
    const id = res.result.data.val;
    const groyGallery = groyGalleryMap.has(id) ? groyGalleryMap.get(id) : new GroyGalleryMap();
    groyGallery.id = id;
    groyGallery.name = res.params.jsonParam.name;
    groyGallery.type = res.params.jsonParam.type;
    groyGallery.parentId = 0;
    groyGallery.docContent = res.params.jsonParam.content.docContent;
    groyGalleryMap.set(id, groyGallery);
};

/**更新图库分类（子类）
 * id = 子类id
 */
function updateSubGroyGallery(res, id) {
    const groyGallery = groyGalleryMap.has(id) ? groyGalleryMap.get(id) : new GroyGalleryMap();
    groyGallery.id = id;
    groyGallery.name = res.params.jsonParam.content[0].name;
    groyGallery.type = res.params.jsonParam.content.type || 1;
    groyGallery.parentId = res.result.data.val;
    groyGallery.docContent = res.params.jsonParam.content[0].docContent;
    groyGalleryMap.set(id, groyGallery);
};

/**图库分类map */
function GroyGalleryMap() {
    /**图库id */
    this.id = '';
    /**图库名字 */
    this.name = '';
    /**图库类型 */
    this.type = '';
    /**父类id */
    this.parentId = 0;
    /**图库图片信息 */
    this.docContent = '';
};

const groyGalleryMap = new Map();