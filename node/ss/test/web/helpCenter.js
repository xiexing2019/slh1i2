const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssReq = require('../../help/ssReq');
const caps = require('../../../data/caps');
const fs = require('fs');
const path = require('path');

const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../data/doc.json')))[caps.name];

describe('帮助中心-offline', async function () {
    this.timeout(TESTCASE.timeout);

    describe('类别', async function () {
        let newCats;
        describe('新增', async function () {
            before('批量新增类别', async function () {
                await ss.spAuth.staffLogin();
                newCats = [{ name: '类别A' + common.getRandomStr(5), showOrder: 1 }, { name: '类别B' + common.getRandomStr(5), showOrder: 1 }, { name: '类别C' + common.getRandomStr(5), showOrder: 1 }];
                await ss.spugr.batchSaveHelpCenterCat(newCats);
            });
            it('查询帮助分类（模块）列表', async function () {
                for (const newCat of newCats) {
                    const cat = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.name == newCat.name));
                    console.log(`\n cat=${JSON.stringify(cat)}`);
                    expect(cat, '没有找到类别').to.not.be.undefined;
                    Object.assign(newCat, { id: cat.id });
                }
                console.log(`newCats=${JSON.stringify(newCats)}`);
            });
        });
        describe('修改', async function () {
            before('修改类别', async function () {
                await ss.spAuth.staffLogin();
                newCats.forEach(newCat => {
                    Object.assign(newCat, { name: '修改' + newCat.name });
                });
                console.log(`newCats=${JSON.stringify(newCats)}`);
                await ss.spugr.batchSaveHelpCenterCat(newCats);
            });
            it('查询帮助分类（模块）列表', async function () {
                for (const newCat of newCats) {
                    const cat = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.name == newCat.name));
                    console.log(`\n cat=${JSON.stringify(cat)}`);
                    expect(cat, '没有找到类别').to.not.be.undefined;
                    Object.assign(newCat, { id: cat.id });
                }
                console.log(`newCats=${JSON.stringify(newCats)}`);
            });
        });
        describe('删除', async function () {
            before('删除多个类别', async function () {
                await ss.spAuth.staffLogin();
                for (const newCat of newCats) {
                    await ss.spugr.delHelpCenterCat({ id: newCat.id });
                    // const catListRes = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.id == newCat.id));
                    // console.log(`\n catListRes=${JSON.stringify(catListRes)}`);
                    // expect(catListRes, '找到了类别').to.be.undefined;
                }
            });
            it('查询帮助分类（模块）列表', async function () {
                for (const newCat of newCats) {
                    // await ss.spugr.delHelpCenterCat({ id: newCat.id });
                    const catListRes = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.id == newCat.id));
                    console.log(`\n catListRes=${JSON.stringify(catListRes)}`);
                    expect(catListRes, '找到了类别').to.be.undefined;
                }
            });
        });
    });
    describe('文章/视频', async function () {
        let cat;
        before(async function () {
            //新增类别
            // await ssReq.ssSellerLogin();
            await ss.spAuth.staffLogin();
            const newCat = [{ name: '类别A' + common.getRandomStr(5), showOrder: 1 }];
            const helpCenterCat = await ss.spugr.batchSaveHelpCenterCat(newCat);
            console.log(`helpCenterCat=${JSON.stringify(helpCenterCat)}`);
            cat = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.name == newCat[0].name));
            console.log(`\n cat=${JSON.stringify(cat)}`);
            expect(cat, '没有找到类别').to.not.be.undefined;
        });
        after(async function () {
            //删除类别
            await ss.spAuth.staffLogin();
            await ss.spugr.delHelpCenterCat({ id: cat.id });
            const catListRes = await ss.spugr.getCatList().then(res => res.result.data.rows.find(obj => obj.id == cat.id));
            console.log(`\n catListRes=${JSON.stringify(catListRes)}`);
            expect(catListRes, '找到了类别').to.be.undefined;
        });
        describe('文章', async function () {
            let helpDoc;
            describe('新增', async function () {
                before('新增文章', async function () {
                    const doc = '文章' + common.getRandomStr(5);
                    helpDoc = { title: doc + common.getRandomStr(5), keyWords: doc, showOrder: 1, docContent: doc + common.getRandomStr(30), flag: 1, catId: cat.id }
                    await ss.spugr.saveHelpDoc(helpDoc);
                });
                it('查询文章列表', async function () {
                    const helpDoclistRes = await ss.spugr.getHelpDoclist().then(res => res.result.data[cat.name].find(obj => obj.title = helpDoc.title));
                    console.log(`\n helpDoclistRes=${JSON.stringify(helpDoclistRes)}`);
                    expect(helpDoclistRes, `文章列表没有找到${cat.name}类别`).to.not.be.undefined;
                    common.isApproximatelyEqualAssert(helpDoc, helpDoclistRes);
                    Object.assign(helpDoc, { id: helpDoclistRes.id });
                });
                it('根据id查询文章', async function () {
                    if (!helpDoc.id) this.skip();
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocById);
                });
                it('查询文章:标题', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ catId: cat.id, titleLike: helpDoc.title }).then(res => res.result.data.rows[0]);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocRes);
                });
                it('查询文章:关键词', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ catId: cat.id, titleLike: helpDoc.keyWords }).then(res => res.result.data.rows[0]);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocRes);
                });
            });
            describe('修改', async function () {
                before('修改文章', async function () {
                    if (!helpDoc.id) this.skip();
                    const doc = '文章修改' + common.getRandomStr(5);
                    helpDoc = { id: helpDoc.id, title: doc + common.getRandomStr(5), keyWords: doc, showOrder: 1, docContent: doc + common.getRandomStr(30), flag: 1, catId: cat.id }
                    await ss.spugr.saveHelpDoc(helpDoc);
                });
                it('查询文章列表', async function () {
                    const helpDoclistRes = await ss.spugr.getHelpDoclist().then(res => res.result.data[cat.name].find(obj => obj.title = helpDoc.title));
                    console.log(`\n helpDoclistRes=${JSON.stringify(helpDoclistRes)}`);
                    expect(helpDoclistRes, `文章列表没有找到${cat.name}类别`).to.not.be.undefined;
                    common.isApproximatelyEqualAssert(helpDoc, helpDoclistRes);
                    Object.assign(helpDoc, { id: helpDoclistRes.id });
                });
                it('根据id查询文章', async function () {
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocById);
                });
                it('查询文章:标题', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ catId: cat.id, titleLike: helpDoc.title }).then(res => res.result.data.rows[0]);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocRes);
                });
                it('查询文章:关键词', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ catId: cat.id, titleLike: helpDoc.keyWords }).then(res => res.result.data.rows[0]);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocRes);
                });
            });
            describe('删除', async function () {
                before('删除文章', async function () {
                    if (!helpDoc.id) this.skip();
                    await ss.spugr.delHelpDoc({ id: helpDoc.id });
                });
                it('查询文章列表', async function () {
                    const helpDoclistRes = await ss.spugr.getHelpDoclist().then(res => res.result.data[cat.name]);
                    console.log(`\n helpDoclistRes=${JSON.stringify(helpDoclistRes)}`);
                    expect(helpDoclistRes, `文章列表找到了${cat.name}类别`).to.be.undefined;
                });
                it('根据id查询文章', async function () {
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    expect(helpDocById, '结果不为空').to.be.empty;
                });
                it('查询文章:标题', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ catId: cat.id, titleLike: helpDoc.title }).then(res => res.result.data.rows);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    expect(helpDocRes, '结果不为空').to.be.empty;
                });
                it('查询文章:关键词', async function () {
                    const helpDocRes = await ss.spugr.findHelpDoc({ flag: 0, catId: cat.id, titleLike: helpDoc.keyWords }).then(res => res.result.data.rows);
                    console.log(`\n helpDocRes=${JSON.stringify(helpDocRes)}`);
                    expect(helpDocRes, '结果不为空').to.be.empty;
                });
            });
        });
        describe('视频', async function () {
            describe('新增', async function () {
                before('新增视频', async function () {
                    const doc = '视频' + common.getRandomStr(5);
                    helpDoc = { title: doc + common.getRandomStr(5), keyWords: doc, showOrder: 1, docId: docData.video.dres[0].videoId, docContent: doc + common.getRandomStr(30), flag: 1, catId: cat.id }
                    await ss.spugr.saveHelpDoc(helpDoc);
                });
                it('查询视频列表:标题', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.title }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    common.isApproximatelyEqualAssert(helpDoc, video);
                    Object.assign(helpDoc, { id: video.id });
                });
                it('查询视频列表:关键词', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.keyWords }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    common.isApproximatelyEqualAssert(helpDoc, video);
                });
                it('根据id查询文章', async function () {
                    if (!helpDoc.id) this.skip();
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocById);
                });
            });
            describe('修改', async function () {
                before('修改视频', async function () {
                    if (!helpDoc.id) this.skip();
                    const doc = '视频' + common.getRandomStr(5);
                    helpDoc = { id: helpDoc.id, title: doc + common.getRandomStr(5), keyWords: doc, showOrder: 1, docId: docData.video.dres[0].videoId, docContent: doc + common.getRandomStr(30), flag: 1, catId: cat.id }
                    await ss.spugr.saveHelpDoc(helpDoc);
                });
                it('查询视频列表:标题', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.title }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    common.isApproximatelyEqualAssert(helpDoc, video);
                });
                it('查询视频列表:关键词', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.keyWords }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    common.isApproximatelyEqualAssert(helpDoc, video);
                });
                it('根据id查询文章', async function () {
                    if (!helpDoc.id) this.skip();
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    common.isApproximatelyEqualAssert(helpDoc, helpDocById);
                });
            });
            describe('删除', async function () {
                before('删除视频', async function () {
                    if (!helpDoc.id) this.skip();
                    await ss.spugr.delHelpDoc({ id: helpDoc.id });
                });
                it('查询视频列表:标题', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.title }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    expect(video, '没有找到视频').to.be.undefined;
                });
                it('查询视频列表:关键词', async function () {
                    const video = await ss.spugr.getVideoList({ titleLike: helpDoc.keyWords }).then(res => res.result.data.rows[0]);
                    console.log(`\n video=${JSON.stringify(video)}`);
                    expect(video, '没有找到视频').to.be.undefined;
                });
                it('根据id查询文章', async function () {
                    const helpDocById = await ss.spugr.findHelpDocById({ id: helpDoc.id }).then(res => res.result.data);
                    console.log(helpDocById);
                    expect(helpDocById, '结果不为空').to.be.empty;
                });
            });
        });
    });
});