const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssReq = require('../../help/ssReq');

describe('数据中心统计-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        await ss.spAuth.staffLogin();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
    });
    after(async function () {
        const res = await sp.spugr.setShadowFlag({ id: sellerInfo.shopId, shadowFlag: 0, tenantType: 1 });
        console.log(`\n res=${JSON.stringify(res)}`);
    });
    describe('白名单', async function () {
        before(async function () {
            const res = await sp.spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 1, tenantType: 1 });
            console.log(`\n res=${JSON.stringify(res)}`);
        });
        it('店铺列表', async function () {
            const shop = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName }).then(res => res.result.data.rows.find(ele => ele.name == sellerInfo.shopName));
            console.log(`shop=${JSON.stringify(shop)}`);
            expect(shop.shadowFlag, `${sellerInfo.shopId}店铺白名单居然为未启动状态`).to.equal(1);
        });
        it('店铺访问排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 0, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `居然找到了${sellerInfo.shopId}店铺数据`).to.be.undefined;
            }
        });
        it('店铺订单排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 1, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `居然找到了${sellerInfo.shopId}店铺数据`).to.be.undefined;
            }
        });
        it('店铺绑定客户排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 2, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `居然找到了${sellerInfo.shopId}店铺数据`).to.be.undefined;
            }
        });
    });

    describe('非白名单', async function () {
        before(async function () {
            const res = await sp.spugr.setShadowFlag({ id: sellerInfo.tenantId, shadowFlag: 0, tenantType: 1 });
            console.log(`\n res=${JSON.stringify(res)}`);
        });
        it('店铺列表', async function () {
            const shop = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName }).then(res => res.result.data.rows.find(ele => ele.name == sellerInfo.shopName));
            console.log(`shop=${JSON.stringify(shop)}`);
            expect(shop.shadowFlag, `${sellerInfo.shopId}店铺白名单居然为启动状态`).to.equal(0);
        });
        it('店铺访问排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 0, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `前20排名里找不到${sellerInfo.shopId}店铺数据`).not.to.be.undefined;
            }
        });
        it('店铺订单排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 1, dateIn: [common.getDateString([0, -1, 0]), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `前20排名里找不到${sellerInfo.shopId}店铺数据`).not.to.be.undefined;
            }
        });
        it('店铺绑定客户排名接口', async function () {
            this.retries(2);
            await common.delay(500);
            const res = await ss.spbi.getShopRankStatistics({ orderType: 2, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
            if (res.result.data.rows) {
                expect(res.result.data.rows.find(obj => obj.shopId == sellerInfo.shopId), `前20排名里找不到${sellerInfo.shopId}店铺数据`).not.to.be.undefined;
            }
        });
    });

    describe('销售情况统计', function () {
        it('销售情况总计', async function () {
            let res = await ss.spbi.allSalesStatistics();
            console.log(JSON.stringify(res));
        });
        it('销售情况近期走势', async function () {
            let res = await ss.spbi.allSalesStatisticsTrend({ dateIn: ['2019-09-03', '2019-09-04'] });
            console.log(JSON.stringify(res));
        });
    });

    describe('场景推送统计', function () {
        it('获取公众号场景推送统计信息', async function () {
            let res = await ss.spbi.getDetailStatsByDate({ dateIn: ['2019-09-03', '2019-09-03'] });
            console.log(JSON.stringify(res));
        });
        it('获取一段时间场景统计占比信息', async function () {
            let res = await ss.spbi.getSceneStatsByDateIn({ dateIn: ['2019-09-03', '2019-09-03'] });
            console.log(JSON.stringify(res));
        });
    });

    describe('店铺排名', async function () {
        it('店铺列表', async function () {
            const shop = await ss.spugr.getShopList({ nameLike: sellerInfo.shopName });
            console.log(`shop=${JSON.stringify(shop)}`);
        });
        it('店铺访问排名接口', async function () {
            const res = await ss.spbi.getShopRankStatistics({ orderType: 0, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
        });
        it('店铺订单排名接口', async function () {
            const res = await ss.spbi.getShopRankStatistics({ orderType: 1, dateIn: [common.getDateString([0, -1, 0]), common.getCurrentDate()] });
            console.log(`res=${JSON.stringify(res)}`);
        });
        it('店铺绑定客户排名接口', async function () {
            // const res = await ss.spbi.getShopRankStatistics({ orderType: 2, dateIn: [common.getCurrentDate(), common.getCurrentDate()] });
            const res = await ss.spbi.getShopRankStatistics({ orderType: 0, dateIn: ["2019-06-05", "2019-06-05"] });
            console.log(`res=${JSON.stringify(res)}`);
        });
    });

});