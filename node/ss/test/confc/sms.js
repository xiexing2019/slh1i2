const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const ssReq = require('../../help/ssReq');
const smsManage = require('../../help/confc/sms');

/**
 * 短信模板+群发短信
 * @author lxx
 * 
 * @description 
 * 1. 短信群发模版的增删改查
 * 2. confc/emb_sms_template_definition
 * 3. emb/emb_sms_balance, emb_msg_send_log_107, emb_msg_107,单元区fin_acct_flow
 * 4. templateContent长度为55,超出提示模版内容太长 由于会自动填充‘退订回T。’所以长度不能超过50
 * 
 * 目前购买短信包流程还在开发中 测试群发短信需要在全局区的emb/emb_sms_balance中手动刷一条数据
 * 获取embGuid, id使用负数避免与正常数据冲突
 * 可购买短信包后，删除手动刷的数据
 * 
 * 提测单
 * http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1405.html
 */
describe('短信模板-offline', async function () {
    this.timeout(30000);
    const smsTemplate = smsManage.setupSmsTemplate();

    before('新增短信模板', async function () {
        // await ss.spAuth.staffLogin();
        await ssReq.ssSellerLogin();

        const json = mockTemplateJson();
        await smsTemplate.saveTemplate(json);
        console.log(smsTemplate);
    });

    it('获取embGuid', async function () {
        const res = await ss.spugr.getEmbGuid();
        console.log(`获取embGuid\n`, res);
    });

    it('短信模版查询', async function () {
        await smsTemplate.getFromListAssert();
    });

    describe('修改校验', async function () {
        before('修改模板', async function () {
            const json = mockTemplateJson();
            await smsTemplate.saveTemplate(json);
        });
        it('短信模版查询', async function () {
            await smsTemplate.getFromListAssert();
        });
    });

    describe.skip('群发短信校验', async function () {
        before(async function () {
            // 充值
            // const res = await smsManage.saveFinAcctFlowInfo(LOGINDATA);
            // console.log(`res=${JSON.stringify(res)}`);

            // 群发短信
            await smsManage.sendGroupSms({ smsTemplate });
        });
        it('查询发送记录', async function () {
            const smsLogList = await ss.spugr.queryGroupSmsLog().then(res => res.result.data.dataList);
            console.log(`smsLogList=${JSON.stringify(smsLogList)}`);

            const data = smsLogList.find(ele => ele.body.templateId == smsTemplate.templateCode);
            expect(data, `未查询到发送记录`).not.to.be.undefined;
            // common.isApproximatelyEqualAssert(, data);
        });
        it('查询发送记录', async function () {
            await ss.spAuth.staffLogin();
            const smsLogList = await ss.spugr.queryGroupSmsLogForWeb({ templateCodes: [smsTemplate.templateCode] }).then(res => res.result.data.dataList);
            console.log(`smsLogList=${JSON.stringify(smsLogList)}`);

            const data = smsLogList.find(ele => ele.templateCode == smsTemplate.templateCode);
            expect(data, `未查询到发送记录`).not.to.be.undefined;
            // common.isApproximatelyEqualAssert(, data);
        });
    });

    describe('删除校验', async function () {
        before('删除模板', async function () {
            await smsTemplate.deleteTemplate();
        });
        it('短信模版查询', async function () {
            await smsTemplate.getFromListAssert();
        });
    });

});

function mockTemplateJson() {
    const code = common.getRandomStr(5);
    return {
        bizType: 107,
        templateContent: `亲爱的客户,有一段时间没有来{code}了,我们最近上了很多新款,还有不定期优惠活动,快来看看吧!`,
        productCode: 'slhMallAdminWeb',//平台类型
        guid: 0,// 模板为0  每个发送者有各自的guid ec-spugr-sms-getEmbGuid获取
        defType: 1,
        platformCode: 'CL',//CL或TX
        templateName: `短信模板${code}`,
        templateCode: code,
    }
};