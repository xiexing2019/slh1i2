const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');

// 线上不能直接调op接口同步加offline 配置的帐套写死预发环境
describe('更新商陆花租户关联字段-offline', function () {
    this.timeout(30000);
    describe('更新商陆花租户关联字段(账套结转)', function () {
        let sellerInfo;
        before('卖家登录', async function () {
            await ssReq.ssSellerLogin({ code: 12911127426, shopName: '店铺HL2aM' });
            sellerInfo = _.cloneDeep(LOGINDATA);
        });
        it('更新商陆花租户关联字段', async function () {
            let res = await ss.op.updateSlhRelation({ tenantId: sellerInfo.tenantId, slhUnitId: 18445, slhShopId: 110309 });
            console.log(res);
        });
    });
});
