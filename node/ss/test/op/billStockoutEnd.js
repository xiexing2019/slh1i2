const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const basicJson = require('../../help/basicJson');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');


describe.skip('缺货终结', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before('卖家登录', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('全部终结', async function () {
        let purRes, salesBillInfo, stockoutReturnId, returnBillId;
        before(async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId });
            const spuId = dresSpuList.result.data.rows[0].id; //52363
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer });
            // purRes = await sp.spTrade.savePurBill(purJson);
            purRes = await billManage.createPurBill(purJson);
            console.log(purRes.result.data);
            const payRes = await sp.spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

            await ssReq.ssSellerLogin();
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            const salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);

            // 开单失败则结束当前脚本
            if (typeof salesListRes == 'undefined') {
                console.warn(`销售单列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
                this.parent ? this.parent.skip() : this.skip();
            };
            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id }).then(res => res.result.data);

            await ss.op.refreshToken({ appKey: ssAccount.seller6.appKey, appSecret: ssAccount.seller6.appSecret });
            const stockoutEndBillJson = basicJson.stockoutEndBillJson({ billInfo: salesBillInfo });
            // const stockoutReturn = await ss.opOrder.stockoutReturn({ jsonParam: stockoutEndBillJson, slhUnitId: 16545, mallShopTenantId: sellerInfo.tenantId });
            console.log(stockoutEndBillJson);
            stockoutReturnId = await ss.opOrder.stockoutReturn({ jsonParam: stockoutEndBillJson, mallShopTenantId: sellerInfo.tenantId }).then(res => res.result.data.returnId);
        });
        it('终结后卖家查看退货列表', async function () {
            const salesReturnBills = await sp.spTrade.findSalesReturnBills({ searchToken: stockoutReturnId });
            //"flag":8,"frontFlag":8,"frontFlagName":"缺货退款"
            expect(salesReturnBills.result.data.rows[0].bill).to.include({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
        });
        it('终结后卖家查看退货详情', async function () {
            const returnBillDetail = await sp.spTrade.getReturnBillDetail({ id: stockoutReturnId });
            //backPlatMoney":0,"billFrontFlag":11,flag":8,"frontFlag":8,"frontFlagName":"缺货退款"
            expect(returnBillDetail.result.data.bill).to.include({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
        });
        it('终结后买家查看订单的售后列表', async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const purReturnBills = await sp.spTrade.getPurReturnBills({ purId: purRes.result.data.rows[0].billId });
            expect(purReturnBills.result.data.rows[0].bill).to.includes({ billFlag: 11, billFrontFlag: 4, billFrontFlagName: "交易完成" }, { flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
        });
        it('终结后买家查看退货退款列表', async function () {
            const returnBill = await ss.sppur.findReturnBills().then(res => res.result.data.rows.find(obj => obj.bill.purBillId == purRes.result.data.rows[0].billId));
            returnBillId = returnBill.bill.id;
            expect(returnBill.bill).to.includes({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
        });
        it('终结后买家查看退货退款详情', async function () {
            if (!returnBillId) this.skip();
            const returnBillDetail = await ss.sppur.getReturnBillDetail({ id: returnBillId }).then(res => res.result.data);
            expect(returnBillDetail.bill).to.includes({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
        });
        describe('发起终结缺货单退款', async function () {
            before(async function () {


                await ssReq.ssSellerLogin();

                // console.log(`res=${JSON.stringify(res)}`);
            });
            it('查询退款单', async function () {
                const returnBill = await sp.spTrade.getReturnBillDetail({ id: stockoutReturnId }).then(res => res.result.data);

            });
        });
        describe('创建收款单', async function () {
            before(async function () {
                if (!stockoutReturnId) this.skip();

                await ssReq.ssSellerLogin();


                const refund = await ss.opOrder.stockoutRefund({ sellerId: sellerInfo.tenantId, returnId: stockoutReturnId }).then(res => res.result.data);
                const res = await ss.opOrder.createReceiptBill({ bizOrderId: stockoutReturnId, hashKey: `${Date.now()}${common.getRandomNum(100, 999)}`, payMoney: refund.totalMoney, payType: 2, payMethod: 2 });
                console.log(`res=${JSON.stringify(res)}`);

            });
            it('', async function () {
                const receiptBill = await ss.opOrder.getReceiptBillById({ bizOrderId: stockoutReturnId });
                console.log(`receiptBill=${JSON.stringify(receiptBill)}`);

            });
        });
    });

    describe('部分发货后终结', async function () {
        let purRes, salesBillInfo, syncDeliverJson;
        before(async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const spuId = dresSpuList.result.data.rows[0].id; //52363
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
            console.log(`\n purJson=${JSON.stringify(purJson)}`);

            // purRes = await sp.spTrade.savePurBill(purJson);
            purRes = await billManage.createPurBill(purJson);
            console.log(purRes.result.data);
            const payRes = await sp.spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

            await ssReq.ssSellerLogin();
            const salesList = await sp.spTrade.salesFindBills({ pageSize: 10, orderByDesc: true, orderBy: 'proTime', statusType: 0 });//purRes.result.data.rows[0].billId
            console.log(`salesList=${JSON.stringify(salesList)}`);
            const salesListRes = salesList.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);

            // 开单失败则结束当前脚本
            if (typeof salesListRes == 'undefined') {
                console.warn(`销售单列表未找到id为${purRes.result.data.rows[0].billId}的订单`);
                this.parent ? this.parent.skip() : this.skip();
            };
            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id }).then(res => res.result.data);
            console.log(`salesBillInfo=${JSON.stringify(salesBillInfo)}`);

        });
        describe('部分发货', async function () {
            let exp, salesDeliverId;
            before(async function () {
                await ssReq.ssSellerLogin();
                await ss.op.refreshToken({ appKey: ssAccount.seller6.appKey, appSecret: ssAccount.seller6.appSecret });
                syncDeliverJson = await basicJson.syncDeliverJson({ billInfo: salesBillInfo, part: true });
                const logisTrace = {
                    logisCompName: syncDeliverJson.main.logisCompName,
                    waybillNo: syncDeliverJson.main.waybillNo
                }, spus = [{
                    totalNum: 0
                }], details = [];
                syncDeliverJson.details.forEach((detail) => {
                    spus[0].totalNum += detail.num;
                    const sku = {};
                    sku.id = detail.salesDetailId;
                    sku.skuNum = detail.num;
                    details.push(sku);
                });
                exp = { logisTrace, spus, details };
                console.log(`\n exp=${JSON.stringify(exp)}`);
                salesDeliverId = await ss.opOrder.syncSaveSalesDeliver({ sellerId: sellerInfo.tenantId, salesBillId: purRes.result.data.rows[0].salesBillId, jsonParam: syncDeliverJson }).then(res => res.result.data.billId);
                // console.log(`\n salesDeliverId=${JSON.stringify(salesDeliverId)}`);
                await ssReq.ssSellerLogin();
            });
            it('卖家查询订单的发货单列表', async function () {
                const salesDeliver = await ss.spsales.getSalesDeliverList({ salesBillId: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data.rows[0]);
                console.log(`\n salesDeliverList=${JSON.stringify(salesDeliver)}`);
                const expect = Object.assign(syncDeliverJson.main, exp.spu);
                common.isApproximatelyEqualAssert(expect, salesDeliver);
            });
            it('卖家查看发货单详情', async function () {
                const salesDeliverDetail = await ss.spsales.getSalesDeliverDetail({ id: salesDeliverId, waybillNo: syncDeliverJson.main.waybillNo, logisCompId: syncDeliverJson.main.logisCompId }).then(res => res.result.data);
                console.log(`\n salesDeliverDetail=${JSON.stringify(salesDeliverDetail)}`);
                common.isApproximatelyEqualAssert(exp, salesDeliverDetail, ['details']);
                for (const i in exp.details) {
                    common.isApproximatelyEqualAssert(exp.details[i], salesDeliverDetail.details[i], ['id']);
                }
            });
            it('买家查询订单的发货单列表', async function () {
                await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                const purDeliver = await ss.spsales.getPurDeliverList({ sellerId: sellerInfo.tenantId, salesBillId: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data.rows[0]);
                console.log(`\n purDeliverList=${JSON.stringify(purDeliver)}`);
                const expect = Object.assign(syncDeliverJson.main, exp.spu);
                common.isApproximatelyEqualAssert(expect, purDeliver);
            });
            it('买家查看发货单详情', async function () {
                const purDeliverDetail = await ss.spsales.getPurDeliverDetail({ sellerId: sellerInfo.tenantId, id: salesDeliverId, waybillNo: syncDeliverJson.main.waybillNo, logisCompId: syncDeliverJson.main.logisCompId }).then(res => res.result.data);
                console.log(`\n purDeliverDetail=${JSON.stringify(purDeliverDetail)}`);
                common.isApproximatelyEqualAssert(exp, purDeliverDetail, ['details']);
                for (const i in exp.details) {
                    common.isApproximatelyEqualAssert(exp.details[i], purDeliverDetail.details[i], ['id']);
                }
            });
        });
        describe('部分终结', async function () {
            let stockoutReturnId, returnBillId;
            before(async function () {
                await ssReq.ssSellerLogin();
                await ss.op.refreshToken({ appKey: ssAccount.seller6.appKey, appSecret: ssAccount.seller6.appSecret });
                console.log('部分发货：');
                console.log(syncDeliverJson);
                const stockoutEndBillJson = basicJson.stockoutEndBillJson({ part: true, syncDeliverJson: syncDeliverJson, billInfo: salesBillInfo });
                // const stockoutReturn = await ss.opOrder.stockoutReturn({ jsonParam: stockoutEndBillJson, slhUnitId: 16545, mallShopTenantId: sellerInfo.tenantId });
                console.log('最后终结：');
                console.log(stockoutEndBillJson);
                // stockoutEndBillJson.main.totalNum = 0;
                // syncDeliverJson.details.forEach((detail, index) => {
                //     stockoutEndBillJson.details[index].num -= detail.num;
                //     stockoutEndBillJson.details[index].money = common.mul(stockoutEndBillJson.details[index].price, stockoutEndBillJson.details[index].num);
                //     stockoutEndBillJson.main.totalNum += stockoutEndBillJson.details[index].num;
                // });
                // console.log(stockoutEndBillJson);
                stockoutReturnId = await ss.opOrder.stockoutReturn({ jsonParam: stockoutEndBillJson, mallShopTenantId: sellerInfo.tenantId }).then(res => res.result.data.returnId);
            });
            it('终结后卖家查看退货列表', async function () {
                const salesReturnBills = await sp.spTrade.findSalesReturnBills({ searchToken: stockoutReturnId });
                console.log(`\n salesReturnBills=${JSON.stringify(salesReturnBills)}`);
                //"flag":8,"frontFlag":8,"frontFlagName":"缺货退款"
                expect(salesReturnBills.result.data.rows[0].bill).to.include({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
            });
            it('终结后卖家查看退货详情', async function () {
                const returnBillDetail = await sp.spTrade.getReturnBillDetail({ id: stockoutReturnId });
                console.log(`\n returnBillDetail=${JSON.stringify(returnBillDetail)}`);
                //backPlatMoney":0,"billFrontFlag":11,flag":8,"frontFlag":8,"frontFlagName":"缺货退款"
                expect(returnBillDetail.result.data.bill).to.include({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
            });
            it('终结后买家查看订单的售后列表', async function () {
                await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                const purReturnBills = await sp.spTrade.getPurReturnBills({ purId: purRes.result.data.rows[0].billId });
                expect(purReturnBills.result.data.rows[0].bill).to.includes({ billFlag: 11, billFrontFlag: 4, billFrontFlagName: "交易完成" }, { flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
            });
            it('终结后买家查看退货退款列表', async function () {
                const returnBill = await ss.sppur.findReturnBills().then(res => res.result.data.rows.find(obj => obj.bill.purBillId == purRes.result.data.rows[0].billId));
                returnBillId = returnBill.bill.id;
                expect(returnBill.bill).to.includes({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
            });
            it('终结后买家查看退货退款详情', async function () {
                if (!returnBillId) this.skip();
                const returnBillDetail = await ss.sppur.getReturnBillDetail({ id: returnBillId }).then(res => res.result.data);
                console.log(`\n returnBillDetail=${JSON.stringify(returnBillDetail)}`);
                expect(returnBillDetail.bill).to.includes({ flag: 8, frontFlag: 8, frontFlagName: '缺货退款' });
            });
        });
    });
});