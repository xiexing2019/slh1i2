'use strict';
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const basicJson = require('../../help/basicJson');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');


describe.skip('修改订单', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before('卖家登录', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });
    const edits = [
        { name: 'sku购买数量', item: { skuNum: true } },
        { name: 'sku购买个数', item: { skuSum: true } },
        { name: '货品价格', item: { price: true } },
        { name: '同spu不同价格', item: { difPrice: true } }];
    for (const edit of edits) {
        describe(`修改待发货订单:${edit.name}`, async function () {
            let purRes, salesBillInfo;
            before(async function () {
                await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
                const spuId = dresSpuList.result.data.rows[0].id; //52363
                const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
                console.log(`\n purJson=${JSON.stringify(purJson)}`);

                purRes = await billManage.createPurBill(purJson);
                console.log(purRes.result.data);
                const payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
                await ssReq.ssSellerLogin();
                const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
                // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
                const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

                salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);

            });
            describe(`修改待发货订单:${edit.name}`, async function () {
                let editBillJson;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    editBillJson = basicJson.editBillJson({ billInfo: salesBillInfo, edit: edit.item });
                    console.log(`\neditBillJson=${JSON.stringify(editBillJson)}`);
                    editBillJson.mallShopTenantId = sellerInfo.tenantId;
                    const editedBill = await ss.opOrder.syncEditedBill(editBillJson);
                    console.log(`\neditedBill=${JSON.stringify(editedBill)}`);
                });
                it('卖家销售单详情', async function () {
                    const res = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                    console.log(`\n卖家销售单详情=${JSON.stringify(res)}`);
                    //totalNum,spuPrice
                    expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                    editBillJson.detail.forEach(detail => {
                        const sku = res.skus.find(obj => obj.id == detail.id);
                        common.isApproximatelyEqualAssert(detail, sku);
                    });
                });
                it('买家采购单详情', async function () {
                    await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                    const res = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
                    console.log(`\n买家采购单详情=${JSON.stringify(res)}`);
                    expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                    editBillJson.detail.forEach(detail => {
                        const sku = res.skus.find(obj => obj.skuId == detail.skuId);
                        common.isApproximatelyEqualAssert(detail, sku, ['id']);
                    });
                });
                describe('订单列表', async function () {
                    //列表购买数量为订单第一个spu的购买数量
                    it('卖家销售单列表', async function () {
                        await ssReq.ssSellerLogin();
                        const res = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                        console.log(`\n卖家销售单列表=${JSON.stringify(res)}`);
                        expect(editBillJson.bill.totalNum, `列表购买数量于预期${editBillJson.bill.totalNum}不符`).to.equal(res.spus[0].totalNum);
                    });
                    it('买家采购单列表', async function () {
                        await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                        const res = await sp.spTrade.purFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                        console.log(`\n买家采购单列表=${JSON.stringify(res)}`);
                        expect(editBillJson.bill.totalNum, `列表购买数量于预期${editBillJson.bill.totalNum}不符`).to.equal(res.spu.totalNum);
                    });
                });
            });
        });
        describe(`修改部分发货订单:${edit.name}`, async function () {
            let purRes, salesBillInfo, syncDeliverJson;
            before(async function () {
                await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
                const spuId = dresSpuList.result.data.rows[0].id; //52363
                const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
                console.log(`\n purJson=${JSON.stringify(purJson)}`);

                purRes = await billManage.createPurBill(purJson);
                console.log(purRes.result.data);
                const payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
                await ssReq.ssSellerLogin();
                const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
                // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
                const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

                salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);
            });
            describe('部分发货', async function () {
                let exp, salesDeliverId;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    await ss.op.refreshToken({ appKey: ssAccount.seller6.appKey, appSecret: ssAccount.seller6.appSecret });
                    syncDeliverJson = await basicJson.syncDeliverJson({ billInfo: salesBillInfo, part: true });
                    const logisTrace = {
                        logisCompName: syncDeliverJson.main.logisCompName,
                        waybillNo: syncDeliverJson.main.waybillNo
                    }, spus = [{
                        totalNum: 0
                    }], details = [];
                    syncDeliverJson.details.forEach((detail) => {
                        spus[0].totalNum += detail.num;
                        const sku = {};
                        sku.id = detail.salesDetailId;
                        sku.skuNum = detail.num;
                        details.push(sku);
                    });
                    exp = { logisTrace, spus, details };
                    console.log(`\n exp=${JSON.stringify(exp)}`);
                    salesDeliverId = await ss.opOrder.syncSaveSalesDeliver({ sellerId: sellerInfo.tenantId, salesBillId: purRes.result.data.rows[0].salesBillId, jsonParam: syncDeliverJson }).then(res => res.result.data.billId);
                    // console.log(`\n salesDeliverId=${JSON.stringify(salesDeliverId)}`);
                    await ssReq.ssSellerLogin();
                });
                it('卖家订单部分发货详情', async function () {
                    const res = await ss.spsales.salesbillSendDetail({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                    console.log(`\n卖家订单部分发货详情=${JSON.stringify(res)}`);
                });
                it('买家订单部分发货详情', async function () {
                    await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                    const res = await ss.spsales.purbillSendDetail({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
                    console.log(`\n买家订单部分发货详情=${JSON.stringify(res)}`);
                });
                describe(`修改部分发货订单:${edit.name}`, async function () {
                    let editBillJson;
                    before('修改订单', async function () {
                        await ssReq.ssSellerLogin();
                        salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                        console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);
                        const salesbillSendDetail = await ss.spsales.salesbillSendDetail({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                        salesBillInfo.skus = salesbillSendDetail.unSendDetail.spus[0].skus;
                        editBillJson = basicJson.editBillJson({ billInfo: salesBillInfo, partDeliver: true, edit: edit.item });
                        console.log(`\neditBillJson=${JSON.stringify(editBillJson)}`);
                        editBillJson.mallShopTenantId = sellerInfo.tenantId;
                        const editedBill = await ss.opOrder.syncEditedBill(editBillJson);
                        console.log(`\neditedBill=${JSON.stringify(editedBill)}`);
                    });
                    it('卖家销售单详情', async function () {
                        const res = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                        console.log(`\n卖家销售单详情=${JSON.stringify(res)}`);
                        //totalNum,spuPrice
                        expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                        editBillJson.detail.forEach(detail => {
                            const sku = res.skus.find(obj => obj.id == detail.id);
                            common.isApproximatelyEqualAssert(detail, sku);
                        });
                    });
                    it('买家采购单详情', async function () {
                        await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                        const res = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
                        console.log(`\n买家采购单详情=${JSON.stringify(res)}`);
                        expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                        editBillJson.detail.forEach(detail => {
                            const sku = res.skus.find(obj => obj.skuId == detail.skuId);
                            common.isApproximatelyEqualAssert(detail, sku, ['id']);
                        });
                    });
                    it('卖家订单部分发货详情', async function () {
                        await ssReq.ssSellerLogin();
                        const res = await ss.spsales.salesbillSendDetail({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                        console.log(`\n卖家订单部分发货详情=${JSON.stringify(res)}`);
                        expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.unSendDetail.spus[0].skus.length);
                        editBillJson.detail.forEach(detail => {
                            const sku = res.unSendDetail.spus[0].skus.find(obj => obj.id == detail.id);
                            // if (sku.deliverNum && edit.item.skuNum) {
                            detail.num -= sku.deliverNum;
                            detail.money = common.mul(detail.num, detail.price);
                            // }
                            common.isApproximatelyEqualAssert(detail, sku);
                        });
                    });
                    it('买家订单部分发货详情', async function () {
                        await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                        const res = await ss.spsales.purbillSendDetail({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
                        console.log(`\n买家订单部分发货详情=${JSON.stringify(res)}`);
                        expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.unSendDetail.spus[0].skus.length);
                        editBillJson.detail.forEach(detail => {
                            const sku = res.unSendDetail.spus[0].skus.find(obj => obj.skuId == detail.skuId);
                            // detail.num -= sku.deliverNum;
                            // detail.money = common.mul(detail.num, detail.price);
                            common.isApproximatelyEqualAssert(detail, sku, ['id']);
                        });
                    });
                    describe('订单列表', async function () {
                        //列表购买数量为订单第一个spu的购买数量
                        it('卖家销售单列表', async function () {
                            await ssReq.ssSellerLogin();
                            const res = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                            console.log(`\n卖家销售单列表=${JSON.stringify(res)}`);
                            expect(editBillJson.bill.totalNum, `列表购买数量于预期${editBillJson.bill.totalNum}不符`).to.equal(res.spus[0].totalNum);
                        });
                        it('买家采购单列表', async function () {
                            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                            const res = await sp.spTrade.purFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                            console.log(`\n买家采购单列表=${JSON.stringify(res)}`);
                            expect(editBillJson.bill.totalNum, `列表购买数量于预期${editBillJson.bill.totalNum}不符`).to.equal(res.spu.totalNum);
                        });
                    });
                });
            });
        });
    }

    describe(`修改待发货订单:加商品`, async function () {
        let purRes, salesBillInfo;
        before(async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const spuId = dresSpuList.result.data.rows[0].id; //52363
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
            console.log(`\n purJson=${JSON.stringify(purJson)}`);

            purRes = await billManage.createPurBill(purJson);
            console.log(purRes.result.data);
            const payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await ssReq.ssSellerLogin();
            const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
            // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
            const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);

            const getFullForBuyer2 = await sp.spdresb.getFullForBuyer({ spuId: dresSpuList.result.data.rows[1].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            const spuPurJson = await billManage.mockPurParam(getFullForBuyer2, { count: 1, num: 1 });
            console.log(`\n=${JSON.stringify(spuPurJson)}`);
            spuPurJson.orders[0].details[0].skuPrice = spuPurJson.orders[0].details[0].price;
            spuPurJson.orders[0].details[0].skuNum = spuPurJson.orders[0].details[0].num;
            salesBillInfo.skus.push(spuPurJson.orders[0].details[0])
        });
        describe(`修改待发货订单:加商品`, async function () {
            let editBillJson;
            before(async function () {
                await ssReq.ssSellerLogin();
                editBillJson = basicJson.editBillJson({ billInfo: salesBillInfo, edit: { spu: true } });
                console.log(`\neditBillJson=${JSON.stringify(editBillJson)}`);
                editBillJson.mallShopTenantId = sellerInfo.tenantId;
                const editedBill = await ss.opOrder.syncEditedBill(editBillJson);
                console.log(`\neditedBill=${JSON.stringify(editedBill)}`);
            });
            it('卖家销售单详情', async function () {
                const res = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
                console.log(`\n卖家销售单详情=${JSON.stringify(res)}`);
                //totalNum,spuPrice
                expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                editBillJson.detail.forEach(detail => {
                    const sku = res.skus.find(obj => obj.skuId == detail.skuId);
                    common.isApproximatelyEqualAssert(detail, sku, ['id']);
                });
            });
            it('买家采购单详情', async function () {
                await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                const res = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
                console.log(`\n买家采购单详情=${JSON.stringify(res)}`);
                expect(editBillJson.detail.length, '修改后的订单sku个数与预期不一致').to.equal(res.skus.length);
                editBillJson.detail.forEach(detail => {
                    const sku = res.skus.find(obj => obj.skuId == detail.skuId);
                    common.isApproximatelyEqualAssert(detail, sku, ['id']);
                });
            });
            describe('订单列表', async function () {
                //列表购买数量为订单第一个spu的购买数量
                it('卖家销售单列表', async function () {
                    await ssReq.ssSellerLogin();
                    const res = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    console.log(`\n卖家销售单列表=${JSON.stringify(res)}`);
                    expect(editBillJson.bill.totalNum, `列表购买数量于预期${editBillJson.bill.totalNum}不符`).to.equal(res.bill.totalNum);
                });
                it('买家采购单列表', async function () {
                    await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
                    const res = await sp.spTrade.purFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    console.log(`\n买家采购单列表=${JSON.stringify(res)}`);
                    let totalNum = 0;
                    editBillJson.detail.forEach(detail => {
                        if (detail.spuId == editBillJson.detail[0].spuId) totalNum += detail.num;
                    });
                    expect(totalNum, `列表购买数量于预期${totalNum}不符`).to.equal(res.spu.totalNum);
                });
            });
        });
    });

    describe('使用线上支付', async function () {
        let purRes, salesBillInfo;
        before(async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const spuId = dresSpuList.result.data.rows[0].id; //52363
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
            console.log(`\n purJson=${JSON.stringify(purJson)}`);

            purRes = await billManage.createPurBill(purJson);
            console.log(purRes.result.data);

            const payRes = await sp.spTrade.createPay({ payType: 5, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
            await ssReq.ssSellerLogin();
            // const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
            // console.log(`\nfindSellerPayWay=${JSON.stringify(findSellerPayWay)}`);

            // // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
            // const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);
        });
        describe(`修改待发货订单`, async function () {
            let editedBill;
            before(async function () {
                await ssReq.ssSellerLogin();
                const editBillJson = basicJson.editBillJson({ billInfo: salesBillInfo, edit: { skuNum: true } });
                console.log(`\neditBillJson=${JSON.stringify(editBillJson)}`);
                editBillJson.mallShopTenantId = sellerInfo.tenantId;
                editBillJson.check = false;
                editedBill = await ss.opOrder.syncEditedBill(editBillJson);
                console.log(`\neditedBill=${JSON.stringify(editedBill)}`);
            });
            it('修改结果', async function () {
                expect(editedBill.result, '修改居然成功了').to.include({ msg: '非线下支付订单不支持改单' });
            });
        });
    });

    describe('删除所有明细', async function () {
        let purRes, salesBillInfo;
        before(async function () {
            await ssReq.userLoginWithWx({ tenantId: sellerInfo.tenantId });
            const dresSpuList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, orderBy: 'market_date', orderByDesc: true });
            const spuId = dresSpuList.result.data.rows[0].id; //52363
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, count: 3 });
            console.log(`\n purJson=${JSON.stringify(purJson)}`);

            purRes = await billManage.createPurBill(purJson);
            console.log(purRes.result.data);
            const payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });
            await ssReq.ssSellerLogin();
            const findSellerPayWay = await ss.sppur.findSellerPayWay().then(res => res.result.data.rows)
            // console.log(`\npurRes:${JSON.stringify(purRes.result.data.rows[0])}`);
            const verifyOfflineBill = await ss.spsales.verifyOfflineBill({ id: purRes.result.data.rows[0].salesBillId, accept: true, payWay: findSellerPayWay[common.getRandomNum(0, findSellerPayWay.length - 1)].v1 });

            salesBillInfo = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
            console.log(`\nsalesBillInfo=${JSON.stringify(salesBillInfo)}`);
        });
        describe(`修改待发货订单`, async function () {
            let editedBill;
            before(async function () {
                await ssReq.ssSellerLogin();
                editedBill = await ss.opOrder.syncEditedBill({
                    check: false,
                    mallShopTenantId: sellerInfo.tenantId,
                    bill: {
                        id: salesBillInfo.bill.id, totalNum: 0, originalMoney: 0, totalMoney: 0, money: 0,
                    }, detail: []
                });
                console.log(`\neditedBill=${JSON.stringify(editedBill)}`);
            });
            it('修改结果', async function () {
                expect(editedBill.result, '修改居然成功了').to.include({ msg: '请至少保留一条订单明细' });
            });
        });
    });
});