const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ssAccount = require('../../data/ssAccount');
const ss = require('../../../reqHandler/ss');

// 线上不能直接调op接口同步加offline
describe.skip('客户信息同步-offline', function () {
    let sellerInfo;
    this.timeout(30000);
    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });
    describe('同步适用价格', function () {
        let member, price, custLinkBackRes;
        before(async () => {
            // 查询客户列表
            member = await ss.spmdm.getMemberList().then(
                res => res.result.data.rows.find(data => data.name == ''));
            // 查询适用价格列表
            price = await ss.config.getSpbDictList({ typeId: 402 }).then(
                res => res.result.data.rows.find(data => ![member.validPriceName, '默认价'].includes(data.codeName)));
            // 同步积分到商城
            custLinkBackRes = await ss.opClient.custLinkBack({ id: member.id, slhId: 1, slhSellerId: sellerInfo.tenantId, bonus: common.getRandomNumStr(5), priceType: price.codeValue });
        });
        it('查询客户列表', async function () {
            let qlRes = await ss.spmdm.getMemberList({ custUserId: member.custUserId }).then(res => res.result.data.rows.find(data => data.bonus));
            expect(qlRes.bonus, '积分同步失败').to.equal(Number(custLinkBackRes.params.jsonParam.bonus));
            expect(qlRes.validPriceName, '适用价格同步失败').to.equal(price.codeName);
        });
        it('查询客户详情', async function () {
            let qfRes = await ss.spmdm.getMemberByUserId({ userId: member.custUserId }).then(res => res.result.data);
            expect(qfRes.bonus, '积分同步失败').to.equal(Number(custLinkBackRes.params.jsonParam.bonus));
            expect(qfRes.validPrice, '适用价格同步失败').to.equal(price.codeValue);
        });
    });
});
