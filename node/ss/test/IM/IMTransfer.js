'use strict';
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const ssAccount = require('../../data/ssAccount');
const spIM = require('../../../reqHandler/sp/biz_server/spIM');


describe('转接客服', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, startConvSales;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('转接客服', async function () {
        let acct;
        before(async function () {
            // 创建群聊
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: common.getRandomMobile(), appId: ssAccount.seller6.appId, scene: 1 });
            clientInfo = _.cloneDeep(LOGINDATA);
            await ssReq.ssSellerLogin();
            startConvSales = await ss.ssIM.startConvSales({ buyerUserId: clientInfo.userId }).then(res => res.result.data);
            acct = await ss.ssIM.findAllAcctsInShop({ sellerId: sellerInfo.tenantId }).then((res) => res.result.data.rows.find(obj => obj.isOnline == 1 && obj.userId != LOGINDATA.userId));
            console.log(acct);
            // expect(acct, '不存在其他在线的客服').to.not.be.undefined;
            if (acct == undefined) {
                console.warn(`没有其他的客服`);
                this.parent ? this.parent.skip() : this.skip();
            };
        });
        it.skip('客服列表', async function () {

        });

        it('转接其他客服聊天', async function () {
            await ss.ssIM.transferConv({ salesUnitId: acct.unitId, teamId: startConvSales.tid, salesCustSvcerIdTo: acct.userId, salesCustSvcerIdFrom: sellerInfo.userId })
        });

        // 目前已经放开限制 19-07-23  lxx
        it('原客服恢复聊天', async function () {
            const reStartConv = await spIM.reStartConv({ salesUnitId: sellerInfo.unitId, teamId: startConvSales.tid, salesCustSvcerId: sellerInfo.userId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // expect(reStartConv.result, '恢复聊天竟然成功了').to.includes({ msgId: 'reception_by_other' });
        });
    });
});