'use strict';
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const ssAccount = require('../../data/ssAccount');
const spIM = require('../../../reqHandler/sp/biz_server/spIM');
const ssConfigParam = require('../../help/configParamManager');


describe('卖家主动联系客户', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfoA, clientInfoB, startConvSalesA;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
    });

    describe('创建群聊A', async function () {
        before(async function () {
            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: common.getRandomMobile(), appId: ssAccount.seller6.appId, scene: 1 });
            clientInfoA = _.cloneDeep(LOGINDATA);
            await ssReq.ssSellerLogin();
        });

        it('创建群聊', async function () {
            startConvSalesA = await ss.ssIM.startConvSales({ buyerUserId: clientInfoA.userId }).then(res => {
                console.log(res);
                return res.result.data;
            });
            const exp = {
                buyerAccId: `${clientInfoA.clusterCode}_${clientInfoA.tenantId}_${clientInfoA.unitId}_${clientInfoA.userId}_1`,
                // sallerAccId: `${sellerInfo.clusterCode}_${sellerInfo.tenantId}_${sellerInfo.unitId}_${sellerInfo.userId}`
                // tid: 0
            };
            common.isApproximatelyEqualAssert(exp, startConvSalesA);
        });
    });

    describe('创建群聊B', async function () {
        before(async function () {
            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: common.getRandomMobile(), appId: ssAccount.seller6.appId, scene: 1 });
            clientInfoB = _.cloneDeep(LOGINDATA);
            await ssReq.ssSellerLogin();
        });

        it('创建群聊', async function () {
            const startConvSales = await ss.ssIM.startConvSales({ buyerUserId: clientInfoB.userId }).then(res => res.result.data);
            const exp = {
                buyerAccId: `${clientInfoB.clusterCode}_${clientInfoB.tenantId}_${clientInfoB.unitId}_${clientInfoB.userId}_1`,
                // sallerAccId: `${sellerInfo.clusterCode}_${sellerInfo.tenantId}_${sellerInfo.unitId}_${sellerInfo.userId}`,
                // tid: 0
            };
            common.isApproximatelyEqualAssert(exp, startConvSales);
        });
    });

    describe('切换回A', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
        });
        it('切换回A', async function () {
            await spIM.reStartConv({ salesUnitId: sellerInfo.unitId, teamId: startConvSalesA.tid, salesCustSvcerId: sellerInfo.userId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
        });
    });

    describe('双方无通信30分钟后由卖家主动发起聊天', async function () {
        let ssParam;
        before(async function () {
            await ssReq.ssSellerLogin();
            ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: LOGINDATA.tenantId, code: 'ss_im_seller_to_buyer_timeout', domainKind: 'system' });
        });
        after(async function () {
            await ssParam.returnOriginalParam();
        });
        it('修改参数', async function () {
            await ssParam.updateParam({ val: 0 });
            const paramInfo = await ssParam.getParamInfo();
            common.isApproximatelyEqualAssert(ssParam, paramInfo);
        });
    });
});