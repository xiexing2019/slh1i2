'use strict';
const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssAccount = require('../../data/ssAccount');

describe('买家主动联系卖家', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, startConvSales;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`\n sellerInfo=${JSON.stringify(sellerInfo)}`);

    });

    it('根据userId或者手机号获取网易云信账户信息', async function () {
        const res = await ss.ssIM.getNimAccountInfo({ userIds: [sellerInfo.userId] });
        // console.log(`res=${JSON.stringify(res)}`);
    });

    describe('创建群聊', async function () {
        before(async function () {
            await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: `${common.getRandomMobile()}`, appId: ssAccount.seller6.appId, scene: 1 });
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log(`\n clientInfo=${JSON.stringify(clientInfo)}`);
        });

        it('创建群聊', async function () {
            startConvSales = await sp.spIM.startConv({ buyerId: LOGINDATA.userId, salesUnitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId }).then(res => {
                console.log(res);
                return res.result.data;
            });
            const exp = {
                buyerAccId: `${clientInfo.clusterCode}_${clientInfo.tenantId}_${clientInfo.unitId}_${clientInfo.userId}`,
                sallerAccId: sellerInfo.extProps.im.imAccid,
                // tid: 0
            };
            // console.log(`\n exp=${JSON.stringify(exp)}`);
            console.log(`\n startConvSales=${JSON.stringify(startConvSales)}`);
            common.isApproximatelyEqualAssert(exp, startConvSales, ['sallerAccId', 'buyerAccId']);
        });

        it('客服在客服列表中在线', async function () {
            const acct = await ss.ssIM.findAllAcctsInShop({ sellerId: sellerInfo.tenantId }).then((res) => res.result.data.rows.find(obj => obj.userId == Number(startConvSales.sallerAccId.split("_")[3])));
            expect(acct, '返回的客服在客服列表中不存在').to.not.be.undefined;
            expect(acct.isOnline, '该客服状态不为在线').to.equal(1);
        });
        // comment: 出现异常，调接口手动停止
        it.skip('手动停止服务记录-offline', async function () {
            await ssReq.ssSellerLogin();
            await ss.ssIM.terminateServiceRecord({ tenantId: sellerInfo.tenantId, custId: clientInfo.userId });
            console.log(`res=${JSON.stringify(res)}`);
        });

        describe.skip('主客服下线', async function () {
            before(async function () {
                await ssReq.ssSellerLogin();
                await sp.spugr.spLogOut();
                await ssReq.ssClientLogin({ tenantId: ssAccount.seller6.tenantId, mobile: `${common.getRandomMobile()}`, appId: ssAccount.seller6.appId, scene: 1 });
            });
            it('客服在客服列表中不在线', async function () {
                const acct = await ss.ssIM.findAllAcctsInShop({ sellerId: sellerInfo.tenantId }).then((res) => res.result.data.rows.find(obj => obj.userId == Number(startConvSales.sallerAccId.split("_")[3])));
                console.log(`\n acct=${JSON.stringify(acct)}`);
                expect(acct, '返回的客服在客服列表中不存在').to.not.be.undefined;
                expect(acct.isOnline, '该客服状态不为在线').to.equal(0);
            });
            it('主客服下线，客户要转交给其他客服', async function () {
                // const reStartConv = await sp.spIM.reStartConv({ salesUnitId: sellerInfo.unitId, teamId: startConvSales.tid, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                // console.log(`\n reStartConv=${JSON.stringify(reStartConv)}`);
                const startConvSales2 = await sp.spIM.startConv({ buyerId: LOGINDATA.userId, salesUnitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId }).then(res => res.result.data);
                console.log(`\n startConvSales2=${JSON.stringify(startConvSales2)}`);
                expect(startConvSales2.sallerAccId, '没有转接其他客服').not.to.equal(sellerInfo.extProps.im.imAccid && startConvSales.sallerAccId);
            });

        });
    });

    it.skip('定时任务ServiceRecordUpdateTimerTask', async function () {
        //db: sp_service_record
        const res = await ss.test.initTask({ taskCode: 'ServiceRecordUpdateTimerTask', unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
        console.log(`\n 定时任务ServiceRecordUpdateTimerTask ${JSON.stringify(res)}`);
    });

    it.skip('更新某个机器人的建群数量', async function () {
        const res = await ss.ssIM.updateCreateTeamCount({ accid: 'spb13d_921_917_1_rbt' });
        console.log(`res=${JSON.stringify(res)}`);
    });

});
