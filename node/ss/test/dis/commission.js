const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const ssConfigParam = require('../../help/configParamManager');
const ssAccount = require('../../data/ssAccount');
const billManage = require('../../help/billManage');
const disPartner = require('../../help/dis/disBase');
const commissions = require('../../help/dis/disCommiss');
const uuid = require('uuid');

describe('佣金设置-offline', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, mobile = common.getRandomMobile(), rate = common.getRandomNum(1, 10), realName = common.getRandomChineseStr(3),
        ssDistributeFlag, ssBrokerageRate, ssdislevelOpenFlag, disId;
    const commiss = commissions.setupDis();
    before('开启推广，开启佣金', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        sellerInfo = _.cloneDeep(LOGINDATA);
        //推广商开关参数
        ssDistributeFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_distribute_flag', ownerId: sellerInfo.tenantId });
        //佣金开关参数
        ssBrokerageRate = await ssConfigParam.getSpbParamInfo({ code: 'ss_brokerage_rate', ownerId: sellerInfo.tenantId });
        //等级开关参数
        ssdislevelOpenFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_level_open_flag', ownerId: sellerInfo.tenantId });
        await ssDistributeFlag.updateParam({ val: 1 });
        await ssBrokerageRate.updateParam({ val: 1 });

        // TODO 改成显试角色切换
        disId = await commiss.applyForPromotion();
    });
    after('删除分销商', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        ssDisPriceType = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_price_type', ownerId: sellerInfo.tenantId });
        await ssDisPriceType.updateParam({ val: 0 });
        await commiss.delDisPartner({ id: disId });
        const res = await commiss.findDisPartnerGroup({ isRelateLevel: 0 }).then(res => res);
        const groupIds = res.result.data.rows.map(obj => obj.groupId);
        try {
            for (let i = 0; i < groupIds.length; i++) {
                await commiss.delDisPartnerGroup({ groupId: groupIds[i] });
            }
        } catch (err) {
            throw new Error(`删除无效:${err}`)
        }


    });
    it('查看分销商详情', async function () {
        const data = await commiss.disPartnerDetail({ disId: disId }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(commiss.getDisPartnerDetailExp(), data);
    });

    describe('单人单商品', async function () {
        let disSpu, newlySpuId;
        before('新增商品', async function () {
            await commiss.saveNewDres();
        });
        // TODO disSpu应该在saveNewDres中返回赋值，本用例只做数据校验
        it('查询推广商品列表', async function () {
            const disList = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
            disSpu = disList.find(obj => obj.spuId == commiss.spuId);
            newlySpuId = commiss.spuId
            common.isApproximatelyEqualAssert(commiss.getNewlyDresExp(newlySpuId), disSpu);
        });
        describe('不开启等级-单商品', async function () {


            //     // TODO 封装至Commission 并添加等级初始化逻辑
            //     //这一步是为了初始化等级V1
            //     await ss.dis.createDisMarket();
            // });
            describe('不开启等级-设置单人单商品的佣金', async function () {
                before('设置佣金不加锁', async function () {
                    await commiss.updateDisSpu({ lockFlag: 0, brokerageRate: 3, dresList: [commiss.getNewlyDresExp(newlySpuId)] });
                });

                it('查询推广商商品列表', async function () {
                    const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), data.find(obj => obj.spuId == commiss.spuId));
                    // expect(data.find(obj => obj.spuId == commiss.spuId).levels[0].brokerageRate).to.be.equal(commiss.getDisListExp(commiss.spuId).brokerageRate)
                });
                it('查看推广商品详情', async function () {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: commiss.spuId }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), data);
                });

            });
            describe('不开启等级-修改单人单商品的佣金', async function () {
                before('修改佣金比例', async function () {
                    await commiss.updateDisSpu({ lockFlag: 0, brokerageRate: 5, dresList: [commiss.getNewlyDresExp(newlySpuId)] });
                });
                it('查询推广商商品列表', async function () {
                    disList = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), disList.find(obj => obj.spuId == commiss.spuId));
                    // expect(disList.find(obj => obj.spuId == commiss.spuId).levels[0].brokerageRate).to.be.equal(commiss.getDisListExp(commiss.spuId).brokerageRate)
                });
                it('查看推广商品详情', async function () {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: commiss.spuId }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), data);
                });

            });
            describe('不开启等级-单人单商品锁佣金', async function () {
                before('设置佣金加锁的商品', async function () {
                    await commiss.updateDisSpu({ brokerageRate: 5, lockFlag: 1, dresList: [commiss.getNewlyDresExp(newlySpuId)] });
                });
                it('查询推广商商品列表', async function () {
                    const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
                    // console.log(data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), data.find(obj => obj.spuId == commiss.spuId));
                    // expect(data.find(obj => obj.spuId == commiss.spuId).levels[0].brokerageRate).to.be.equal(commiss.getDisListExp(commiss.spuId).brokerageRate);
                });
                it('查看推广商品详情', async function () {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: commiss.spuId }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(commiss.spuId), data);
                });
            });
            describe('不开启等级-单人单商品解锁佣金', async function () {
                before('设置佣金加锁的商品', async function () {
                    await commiss.updateDisSpu({ brokerageRate: 5, lockFlag: 0, dresList: [commiss.getNewlyDresExp(newlySpuId)] })
                });
                it('查询推广商商品列表', async function () {
                    const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(newlySpuId), disList.find(obj => obj.spuId == commiss.spuId));
                    // expect(data.find(obj => obj.spuId == commiss.spuId).levels[0].brokerageRate).to.be.equal(commiss.getDisListExp(commiss.spuId).brokerageRate);
                });
                it.skip('查看推广商品详情', async function () {
                    const data = await commiss.disPartnerSpuDetail({ spuId: newlySpuId }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(newlySpuId), data);
                });
            });

        });
        describe('不开启等级-设置全局的佣金', async function () {
            before('关闭等级', async function () {
                await ssdislevelOpenFlag.updateParam({ val: 0 });
                //不开启等级-设置全局佣金
                await commiss.saveOwnerVal({
                    ownerKind: 6,
                    "data": [{ ownerId: sellerInfo.tenantId, "domainKind": 'business', "code": "ss_distribute_flag", "val": "1" },
                    { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_brokerage_rate", "val": "12" },
                    { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_auto_agree_flag", "val": "1" },
                    { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_act_spu_brokerage_flag", "val": "0" },
                    { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_level_open_flag", "val": "0" }]
                });
            });
            it('查看推广商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows);
                data.forEach(obj => expect(obj.levels[0].brokerageRate).to.be.equal(12));
                // data.forEach(obj => expect(obj.brokerageRate).to.be.equal(12));
            });
        });

        describe('不开启等级-修改单商品佣金（等级比例接口修改）', async function () {
            before('修改单商品比例', async function () {
                const resBro = await commiss.updateSpuLevelRate({ spus: [commiss.getNewlyDresExp(newlySpuId)], levels: [{ id: -1, brokerageRate: 8 }], levelName: "All" });
            });
            it('查看推广商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == newlySpuId));
                common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('All').levels);
                common.isApproximatelyEqualAssert(data, commiss.getDisOwnDresInfoExp(newlySpuId))
            });

        });

        /**这里是为了校验（单）修改完了，再修改（多），比例是否被多覆盖 */
        describe('开启等级-修改多对单', async function () {
            let groupId2, ranGroup = common.getRandomStr(3);
            before('开启等级，修改多', async function () {
                await ssdislevelOpenFlag.updateParam({ val: 1 });
                //生成V1等级
                await ss.dis.createDisMarket();
                //查看等级列表，获取V1信息
                await commiss.findDisPartnerLevel();

                //先增加一个组2为等级V2落地
                const res = await commiss.saveNewgroup({ groupName: `G${ranGroup}` }).then(res => res);
                groupId2 = res.result.data.groupId;
                const groupName2 = res.params.groupName;

                const levelRes = await commiss.saveDisPartnerLevel({ disLevel: [{ choiceType: 'newly', levelName: 'V2', groupName: groupName2, groupId: groupId2, brokerageRate: 6 }] }).then(res => res);

                //获取V2信息
                levelList = await commiss.findDisPartnerLevel().then(res => res.result.data.rows);
                const V2 = levelList.find(obj => obj.levelName == 'V2')

                await commiss.updateSpuLevelRate({ spus: [commiss.getNewlyDresExp(newlySpuId)], levels: [{ id: V2.id, brokerageRate: 8 }], levelName: 'V2' });


            });
            after('删除等级和分组', async function () {
                await commiss.saveDisPartnerLevel();
                await commiss.delDisPartnerGroup({ groupId: groupId2 });
                await ssdislevelOpenFlag.updateParam({ val: 0 });
            });
            it('查看分销商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == newlySpuId));
                common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('V2').levels);
                common.isApproximatelyEqualAssert(commiss.getDisOwnDresInfoExp(newlySpuId), data);
                // expect(commiss.getDisOwnDresInfoExp(spuId)).to.be.equal(spuId);
            });
            it('查看分销商品详情', async function () {
                const data = await commiss.disPartnerSpuDetail({ spuId: newlySpuId }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(commiss.getNewlyDresExp(newlySpuId), data)
                common.isApproximatelyEqualAssert(commiss.getDisLevelDataExp('V2'), data.levels);
            });
        });

    })

    describe('单人多商品', async function () {
        let disList, spuIds, newlySpuId;
        before('不开启等级-查询商品列表', async function () {
            ssdislevelOpenFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_level_open_flag', ownerId: sellerInfo.tenantId });
            // await ssdislevelOpenFlag.updateParam({ val: 1 });
            // await ss.dis.createDisMarket();
            disList = await commiss.findDisPartnerSpu({ pageSize: 3 }).then(res => res.result.data.rows);
            spuIds = disList.map(obj => obj.spuId);

        });
        describe('不开启等级-单人批量设置佣金', async function () {
            before('批量设置商品佣金', async function () {
                await commiss.updateDisSpu({ lockFlag: 0, dresList: disList, brokerageRate: 3 });
            });
            it('查询推广商商品列表', async function () {
                const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id, pageSize: 3 }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds), data);
            });
            it('查看推广商品详情', async function () {
                for (i = 0; i < spuIds.length; i++) {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: spuIds[i] }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds[i]), data);
                };
            });
        });
        describe('不开启等级-单人批量设置佣金加锁', async function () {
            before('批量设置商品加锁', async function () {
                await commiss.updateDisSpu({ brokerageRate: 6.5, dresList: disList, lockFlag: 1 });
            });
            it('查看推广商品列表', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows.find(obj => obj.spuId == spuIds[i]));
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds[i]), data);
                };
            });
            it('查看推广商品详情', async function () {
                for (i = 0; i < spuIds.length; i++) {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: spuIds[i] }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds[i]), data);
                }
            });
        });
        describe('不开启等级-单人批量设置佣金加锁-包含已加锁的', async function () {
            let newSpuId;
            before('批量设置商品加锁', async function () {
                newSpuId = await commiss.saveNewDres();
                //单商品锁佣金
                await commiss.updateDisSpu({ brokerageRate: 6, lockFlag: 1, dresList: [commiss.getNewlyDresExp(newSpuId)] });
                const lockList = _.cloneDeep(disList);
                lockList.push(commiss.getNewlyDresExp(newSpuId));
                await commiss.updateDisSpu({ brokerageRate: 6.5, dresList: lockList, lockFlag: 1 });
            });
            it('查询推广商商品列表', async function () {
                const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id, searchToken: commiss.getNewlyDresExp(newSpuId).title }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(commiss.getDisListExp(newSpuId), data);
            });
            it('查看推广商品详情', async function () {
                const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: newSpuId }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(commiss.getDisListExp(newSpuId), data);

            });
        });
        describe('不开启等级-单人批量解锁商品', async function () {
            before('批量设置佣金商品解锁', async function () {
                const a = await commiss.updateDisSpu({ brokerageRate: 6.5, dresList: disList, lockFlag: 0 });
            });
            it('查看推广商品列表', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows.find(obj => obj.spuId == spuIds[i]));
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds[i]), data);
                };
            });
            it('查看推广商品详情', async function () {
                for (i = 0; i < spuIds.length; i++) {
                    const data = await commiss.disPartnerSpuDetail({ disId: commiss.id, spuId: spuIds[i] }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getDisListExp(spuIds[i]), data);
                }
            });
        });
        describe('不开启等级-修改多商品佣金（等级比例接口修改）', async function () {
            before('修改单商品比例', async function () {
                const resBro = await commiss.updateSpuLevelRate({ spus: disList, levels: [{ id: -1, brokerageRate: 3 }], levelName: "All" });
            });
            it('查看推广商品列表', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == spuIds[i]));

                    common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('All').levels);
                    common.isApproximatelyEqualAssert(data, commiss.getDisOwnDresInfoExp(spuIds[i]))
                };
            });
            it('查看推广商品详情', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const data = await commiss.disPartnerSpuDetail({ spuId: spuIds[i] }).then(res => res.result.data);
                    common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('All').levels);
                    common.isApproximatelyEqualAssert(data, commiss.getDisOwnDresInfoExp(spuIds[i]))
                };
            });
        });
    });

    describe('多人单商品', async function () {
        let spuList, spuIds, ssdislevelOpenFlag, comId2, groupId, ranGroup, groupId2;
        const commiss2 = commissions.setupDis();
        before('开启等级参数，查询商品列表', async function () {
            //创建第二个推广商
            await commiss2.applyForPromotion();
            comId2 = commiss2.id
            //开启等级功能
            ssdislevelOpenFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_level_open_flag', ownerId: sellerInfo.tenantId });
            await ssdislevelOpenFlag.updateParam({ val: 1 });
            //生成V1等级
            await ss.dis.createDisMarket();
            spuList = await commiss.dresSpuList({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            if (spuList == undefined) {
                throw new Error('商品列表为空')
            }
        });
        describe('加入组', async function () {
            ranGroup = common.getRandomStr(3)
            before('新建分组', async function () {
                groupId = await commiss.saveNewgroup({ groupName: `组${ranGroup}` }).then(res => res.result.data.groupId);
            });
            it('查看分组列表', async function () {
                const res = await commiss.findDisPartnerGroup({ isRelateLevel: 0 });
                common.isArrayContainObject(res.result.data.rows, commiss.getFindDisPartnerGroupExp(groupId))
            });
            describe('将推广商关联分组', async function () {
                before(async function () {
                    //关联第一个人
                    await commiss.updateDisInfo({ groupId: groupId });
                    // const groupId = commiss.groupId
                    //关联第二个人
                    // await commiss.updateDisInfo({ groupId: groupId });
                });
                it('查看分组详情', async function () {
                    const res = await commiss.disGroupDetail({ groupId: groupId });
                    common.isApproximatelyEqualAssert(commiss.getFindDisPartnerGroupExp(groupId), res.result.data);
                });
            });
            //补充用例
            describe.skip('分组没有关联等级，该分组内的用户默认使用v1的比例', async function () {
                it('查看分销商品列表', async function () {
                    const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows);
                    data.forEach(obj => {
                        common.isArrayContainObjectAssert(obj.levels, commiss.getDisPartnerSpuExp('V1').levels);
                        // common.isApproximatelyEqualAssert(commiss.getDisOwnDresInfoExp(obj.spuId), obj);
                    });
                });
            });

        });

        describe('开启等级-等级关联分组', async function () {
            let groupList, levelList;
            before('查看分组和等级列表', async function () {
                groupList = await commiss.findDisPartnerGroup().then(res => res.result.data.rows);
                await commiss.findDisPartnerLevel();
                //关联分组
                await commiss.updateDisInfo({ groupId: groupId });
            });


            describe('新增等级', async function () {
                let levelRes;
                before('新增等级', async function () {
                    //先增加一个组2为等级V2落地
                    const res = await commiss.saveNewgroup({ groupName: `G${ranGroup}` }).then(res => res);
                    groupId2 = res.result.data.groupId;
                    const groupName2 = res.params.groupName;
                    // TODO 这里需要夏宗青返回等级id
                    levelRes = await commiss.saveDisPartnerLevel({ disLevel: [{ choiceType: 'newly', levelName: 'V2', groupName: groupName2, groupId: groupId2, brokerageRate: 6 }] }).then(res => res);
                    console.log(levelRes);

                });
                it('查看等级列表', async function () {
                    const res = await commiss.findDisPartnerLevel();
                    common.isApproximatelyEqualAssert(commiss.getDisLevelCountExp(), res.result.data)
                    common.isArrayContainObject(res.result.data.rows, commiss.getDisLevelDataExp('V2'))
                });

            });

        });
        describe('修改单个商品的比例', async function () {
            let levelList, spuId, V2;
            before('修改价格', async function () {
                //获取推广员等级
                levelList = await commiss.findDisPartnerLevel().then(res => res.result.data.rows);
                V2 = levelList.find(obj => obj.levelName == 'V2')
                spuId = await commiss.saveNewDres();
                await commiss.updateSpuLevelRate({ spus: [commiss.getNewlyDresExp(spuId)], levels: [{ id: V2.id, brokerageRate: 8 }], levelName: 'V2' });
            });
            it('查看分销商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == spuId));
                common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('V2').levels);
                common.isApproximatelyEqualAssert(commiss.getDisOwnDresInfoExp(spuId), data);
                // expect(commiss.getDisOwnDresInfoExp(spuId)).to.be.equal(spuId);
            });
            it('查看分销商品详情', async function () {
                const data = await commiss.disPartnerSpuDetail({ spuId: spuId }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(commiss.getNewlyDresExp(spuId), data)
                common.isApproximatelyEqualAssert(commiss.getDisLevelDataExp('V2'), data.levels);
            });
            //补充用例
            describe('开启等级-修改等级佣金比例为0', async function () {
                before('修改等级佣金比例', async function () {
                    await commiss.updateSpuLevelRate({ spus: [commiss.getNewlyDresExp(spuId)], levels: [{ id: V2.id, brokerageRate: 0 }], levelName: 'V2' });
                });

                it('查看分销商品列表', async function () {
                    const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == spuId));
                    common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('V2').levels);
                    common.isApproximatelyEqualAssert(commiss.getDisOwnDresInfoExp(spuId), data);
                    // expect(commiss.getDisOwnDresInfoExp(spuId)).to.be.equal(spuId);
                });
                it('查看分销商品详情', async function () {
                    const data = await commiss.disPartnerSpuDetail({ spuId: spuId }).then(res => res.result.data);
                    common.isApproximatelyEqualAssert(commiss.getNewlyDresExp(spuId), data)
                    common.isApproximatelyEqualAssert(commiss.getDisLevelDataExp('V2'), data.levels);
                });
            });

        });

        describe('删除等级V2和分组2', async function () {
            //必须先删除等级，再删除分组
            describe('等级', async function () {
                before('删除等级', async function () {
                    await commiss.saveDisPartnerLevel()
                });
                it('查看等级列表', async function () {
                    const res = await commiss.findDisPartnerLevel();
                    const levelV2 = res.result.data.rows.find(obj => obj.levelName == 'V2');
                    expect(levelV2).to.be.undefined;
                });
            });
            describe('分组', async function () {
                before('删除分组', async function () {
                    await commiss.delDisPartnerGroup({ groupId: groupId2 });
                });
                it('查看分组列表', async function () {
                    const res = await commiss.findDisPartnerGroup({ isRelateLevel: 0 });
                    const gp2 = res.result.data.rows.find(obj => obj.groupId == groupId2)
                    expect(gp2).to.be.undefined;
                });
            });

        });
        describe.skip('加锁商品后下架', async function () {
            before('加锁商品，下架商品', async function () {
                await commiss.updateDisSpu({ lockFlag: 1 });
                await ss.spDresb.offMarket({ ids: [spuIds[0]] })
            });
            it('查看分销商品详情', async function () {
                await commiss.disPartnerSpuDetail()
            });


        });
        describe.skip('已锁下架商品再次上架', async function () {
            before('上架商品', async function () {
                await ss.spDresb.onMarket({ ids: [spuIds[0]] });
            });
            it('查看分销商品详情', async function () {
                await commiss.disPartnerSpuDetail();
            });
        });

    });

    describe('多人多商品', async function () {
        let spuIds, ssdislevelOpenFlag, groupId2, groupId3;
        before('开启等级', async function () {
            ssdislevelOpenFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_level_open_flag', ownerId: sellerInfo.tenantId });
            await ssdislevelOpenFlag.updateParam({ val: 1 });
            spuList = await commiss.dresSpuList({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            if (spuList == undefined) {
                throw new Error('商品列表为空')
            }
            spuIds = spuList.map(spuId => spuId.id);
        });
        after('删除等级和分组', async function () {
            await commiss.saveDisPartnerLevel();

            await commiss.delDisPartnerGroup({ groupId: groupId3 });
        });
        describe('开启等级-修改等级佣金比例', async function () {
            let List, ranGroup = common.getRandomStr(3), spuIds;
            before('修改等级佣金比例', async function () {
                //新增分组
                const res = await commiss.saveNewgroup({ groupName: `组${ranGroup}` }).then(res => res);
                groupId2 = res.result.data.groupId;
                const groupName2 = res.params.groupName;
                //查询等级列表(为了获得存储V1)
                await commiss.findDisPartnerLevel()
                levelRes = await commiss.saveDisPartnerLevel({ disLevel: [{ choiceType: 'newly', levelName: 'V2', groupName: groupName2, groupId: groupId2, brokerageRate: 9 }] }).then(res => res);
                levelList = await commiss.findDisPartnerLevel().then(res => res.result.data.rows);
                spuIds = levelList.map(obj => obj.spuId);
                const V2 = levelList.find(obj => obj.levelName == 'V2')
                // spuId = await commiss.saveNewDres();
                const disList = await commiss.findDisPartnerSpu({ pageSize: 10, pageNo: 1 }).then(res => res.result.data.rows);
                spuIds = disList.map(obj => obj.spuId);
                //修改等级佣金比例
                await commiss.updateSpuLevelRate({ spus: disList, levels: [{ id: V2.id, brokerageRate: 8 }], levelName: 'V2' });
            });

            it('查看推广商品列表', async function () {
                for (let i of spuIds) {
                    const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == i));
                    common.isArrayContainObjectAssert(data.levels, commiss.getDisPartnerSpuExp('V2').levels);
                    common.isApproximatelyEqualAssert(data, commiss.getDisOwnDresInfoExp(i))
                };
            });
            it('查看推广商品详情', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const data = await commiss.disPartnerSpuDetail({ spuId: spuIds[i] }).then(res => res.result.data);
                    // common.isApproximatelyEqualAssert(commiss.getDisPartnerSpuExp('V2'), data);
                    common.isApproximatelyEqualAssert(commiss.getDisOwnDresInfoExp(spuIds[i]), data)
                    common.isApproximatelyEqualAssert(commiss.getDisLevelDataExp('V2'), data.levels);
                }
            });

        });

        describe('修改V2关联组3', async function () {
            before('修改V2分组', async function () {
                const ranGroup = common.getRandomStr(3);
                const res = await commiss.saveNewgroup({ groupName: `组${ranGroup}` }).then(res => res);
                groupId3 = res.result.data.groupId;
                const groupName = res.params.groupName;
                //这一步是为了获取V1等级的信息
                await commiss.findDisPartnerLevel()
                levelRes = await commiss.saveDisPartnerLevel({ disLevel: [{ choiceType: 'newly', levelName: 'V2', groupName: groupName, groupId: groupId3, brokerageRate: 10 }] }).then(res => res);
                // await commiss.updateDisInfo({ groupId: groupId3 });
            });
            it('查看推广等级列表', async function () {
                const res = await commiss.findDisPartnerLevel().then(res => res);

                common.isApproximatelyEqualAssert(commiss.getDisLevelCountExp(), res);
                common.isApproximatelyEqualAssert(commiss.getDisLevelDataExp('V2'), res.result.data.rows.find(obj => obj.levelName == 'V2'));
            });

        });

        describe.skip('切换分组', async function () {
            before('迁移客户至新分组', async function () {

                await commiss.updateDisInfo({ groupId: groupId3 });
            });
            it('查看分组详情', async function () {
                const res = await commiss.disGroupDetail({ groupId: groupId3 });
                common.isApproximatelyEqualAssert(commiss.getFindDisPartnerGroupExp())
                // common.isApproximatelyEqualAssert(commiss.getFindDisGroupDetailCountExp(), res.result.data);
                const value = res.result.data.disList.find(obj => obj.realName = commiss.realName)
                expect(value).to.not.be.undefined;
            });
            it('查看推广商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(commiss.getDisPartnerSpuExp('V2'), data);
            });
        });

        describe('关闭等级-指定全局佣金', async function () {
            before('指定全局佣金', async function () {
                const disList = await commiss.findDisPartnerSpu({ pageSize: 3 }).then(res => res.result.data.rows);
                await ssdislevelOpenFlag.updateParam({ val: 0 });
                await commiss.updateDisSpu({ lockFlag: 0, brokerageRate: 3, dresList: disList });
            });
            it('查看推广商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows);
                // data.forEach(obj => expect(obj.brokerageRate)).to.be.equal(3);
                // expect(data.forEach(obj => obj.brokerageRate)).to.be.equal(3);
            });
        });
        describe.skip('关闭等级-设置单人单商品的佣金', async function () {
            let spuList;
            before('已被设置等级比例的商品，设置单人单商品佣金比例', async function () {
                spuList = await commiss.findDisPartnerSpu().then(res => res.result.data.rows);
                await commiss.updateDisSpu({ lockFlag: 0, brokerageRate: 4, dresList: [spuList[1]] });
            });

            it('查询推广商商品列表', async function () {
                const data = await commiss.findDisPartnerSpu({ disPartnerId: commiss.id }).then(res => res.result.data.rows);
                common.isApproximatelyEqualAssert(commiss.getDisListExp([spuList[1]].spuId), data.find(obj => obj.spuId == spuList[1].spuId));
                // expect(data.find(obj => obj.spuId == commiss.spuId).levels[0].brokerageRate).to.be.equal(commiss.getDisListExp(commiss.spuId).brokerageRate)
            });
            it('查看推广商品详情', async function () {
                const data = await commiss.disPartnerSpuDetail({ spuId: spuList[1].spuId }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(commiss.getDisListExp(spuList[1].spuId), data);
            });

        });
    });

    describe.skip('下单', async function () {
        let clientInfo;
        before('买家登录下单', async function () {
            await ssReq.ssClientLogin();
            clientInfo = _.cloneDeep(LOGINDATA);
            const bill = billManage.setUpPurBill();
            await bill.savePurBill();

        });
        describe('推广商查看', async function () {
            before('推广商登录', async function () {

            });
            it('查询订单', async function () {

            });
        });
        describe('卖家查看', async function () {
            before('卖家登录', async function () {

            });
            it('查询订单', async function () {

            });
        });
    });

    describe.skip('库存', async function () {
        let invNumInfo;
        before('查看商品库存信息', async function () {
            //新增商品
            await commiss.saveNewDres();
            const spuList = await commiss.findDisPartnerSpu({ pageSize: 3 }).then(res => res.result.data.rows);
            // const spuList = await commiss.dresSpuList({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
            const spuIds = spuList.map(spuId => spuId.id);

            invNumInfo = await commiss.findDisSkus();
            //保存推广自定义库存（这一步是为了获得自定义库存id）
            const ress = await commiss.saveDisSkus({ spuId: commiss.spuId, skus: [{ skuId: invNumInfo.result.data.invList[0].skuId, invNum: invNumInfo.result.data.invList[0].skuNum }] });
        });
        describe('自定义推广库存', async function () {
            before('自定义库存', async function () {
                const invNumInfo1 = await commiss.findDisSkus();
                const res = await commiss.saveDisSkus({ spuId: commiss.spuId, skus: [{ skuId: invNumInfo1.result.data.invList[1].skuId, id: invNumInfo1.result.data.invList[1].id, invNum: 50000 }] });

            });
            it('查看分销商品列表', async function () {
                const data = await commiss.findDisPartnerSpu().then(res => res.result.data.rows.find(obj => obj.spuId == commiss.spuId));
                // common.isApproximatelyEqualAssert(commiss.getDisPartnerSpuExp(), data);
            });
            it('查看分销商品详情', async function () {
                const data = await commiss.disPartnerSpuDetail().then(res => res.result.data);
                // common.isApproximatelyEqualAssert(commiss.getDisPartnerSpuExp(), data);
            });
        });
    });
});
