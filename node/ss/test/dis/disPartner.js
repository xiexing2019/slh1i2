const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const ssConfigParam = require('../../help/configParamManager');
const ssAccount = require('../../data/ssAccount');
const DisPartner = require('../../help/dis/disBase');

describe('推广商', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, mobile = common.getRandomMobile(), rate = common.getRandomNum(1, 10), realName = common.getRandomChineseStr(3);
    const dis = new DisPartner();
    //  是否开通推广 佣金比例 推广价
    let ssDistributeFlag, ssBrokerageRate;
    before('个性化参数', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        sellerInfo = _.cloneDeep(LOGINDATA);
        ssDistributeFlag = await ssConfigParam.getSpbParamInfo({ code: 'ss_distribute_flag', ownerId: sellerInfo.tenantId });
        ssBrokerageRate = await ssConfigParam.getSpbParamInfo({ code: 'ss_brokerage_rate', ownerId: sellerInfo.tenantId });
        // ssDisPriceType = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_price_type', ownerId: sellerInfo.tenantId });
        //默认关闭自动同意申请
        await ss.config.saveOwnerVal({
            ownerKind: 6,
            "data": [{ "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_auto_agree_flag", "val": "0" }]
        });
    });
    after('删除该分销商', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
        ssDisPriceType = await ssConfigParam.getSpbParamInfo({ code: 'ss_dis_price_type', ownerId: sellerInfo.tenantId });
        await ssDisPriceType.updateParam({ val: 0 });
        await dis.delDisPartner({ id: dis.id });

    });

    describe('卖家关闭推广参数', function () {
        before('卖家关闭推广参数', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
            await ssDistributeFlag.updateParam({ val: 0 });
            console.log(sellerInfo.tenantId);
        });

        describe('申请成为推广商', function () {
            let userInfo;
            before('买家登录', async function () {
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId, scene: 1 });
                userInfo = _.cloneDeep(LOGINDATA);

                const disResult = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                if (disResult.validFlag != 0) {
                    throw new Error(`买家已申请过推广,validFlag:${JSON.stringify(disResult)},mobile:${mobile}`);
                }
            });
            it('申请成为推广商', async function () {
                const res = await dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: realName, mobile: userInfo.mobile, sessionId: userInfo.sessionId, openId: userInfo.wxOpenId, check: false });
                console.log(`res=${JSON.stringify(res)}`);
                expect(res.result, `卖家未开启推广商,居然申请成功了!`).to.includes({ msgId: 'not_open_dis_function' });
            });
        });
    });

    describe('卖家开启推广参数', function () {
        let sellerInfo, userInfo;
        before('卖家开启推广参数', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
            sellerInfo = _.cloneDeep(LOGINDATA);
            await ssDistributeFlag.updateParam({ val: 1 });
            await ssBrokerageRate.updateParam({ val: rate });
            //获取价格参数
            // const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
            // priceType = dictList[common.getRandomNum(0, dictList.length - 1)].codeValue;
            // await ssDisPriceType.updateParam({ val: priceType });
            await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: mobile, appId: ssAccount.seller6.appId });
            userInfo = _.cloneDeep(LOGINDATA);
            // const disResult = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
            // if (disResult.validFlag != 0) {
            //     throw new Error(`买家已申请过推广,validFlag:${JSON.stringify(disResult)},mobile:${mobile}`);
            // }
        });

        it('买家申请状态查询-未申请', async function () {
            const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
            common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
        });

        describe('卖家拒绝申请', function () {
            let mobile1, userInfo1;
            before('买家申请成为分销商', async function () {
                mobile1 = common.getRandomMobile();
                await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: mobile1, appId: ssAccount.seller6.appId });
                userInfo1 = _.cloneDeep(LOGINDATA);
                await common.delay(200);
                await dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: realName, mobile: userInfo1.mobile, openId: userInfo.wxOpenId });
            });

            it('卖家查看推广商列表', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                const data = await dis.listDisPartner({ auditFlag: 0 });
                console.log(data.title);

                common.isApproximatelyEqualAssert(dis.getDisPartnerInfoExp(), data);
            });

            it('买家申请状态查询-申请中', async function () {
                await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: mobile1, appId: ssAccount.seller6.appId });
                // const data = await ss.dis.getDisPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId }).then(res => res);
                const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                console.log(data);

                common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
            });

            // 卖家校验
            describe('拒绝申请', function () {
                before(async () => {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    await dis.verifyDisApply({ auditFlag: 3 });

                });
                it('卖家查询-待审核列表', async function () {
                    const data = await dis.listDisPartner({ auditFlag: 3 });
                    common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
                });
                it('买家申请状态查询', async function () {
                    await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: mobile1, appId: ssAccount.seller6.appId });
                    // console.log(LOGINDATA);


                    const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                    common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
                });
            });
            describe('拒绝后用邀请码申请', function () {
                let invCode;
                before('卖家生成二维码', async () => {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    invCode = await dis.invitationCodeBuilder();
                    console.log(invCode);
                });
                it('买家通过二维码申请', async function () {
                    await ssReq.ssClientLogin({ shopId: sellerInfo.shopId, mobile: mobile1, appId: ssAccount.seller6.appId });
                    console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);

                    // console.log(LOGINDATA);

                    await dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: realName, mobile: userInfo1.mobile, openId: userInfo.wxOpenId, invCode: invCode });
                });
                it.skip('买家点击分销中心查询申请状态', async function () {
                    const data = dis.disPartnerCustFlag();
                    common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
                })
            });
        });


        describe('申请成为推广商', function () {
            let newMobile = common.getRandomMobile(), newRealName = common.getRandomChineseStr(3);
            before('买家登录', async function () {
                // console.log(mobile);
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
                userInfo2 = _.cloneDeep(LOGINDATA);
                await common.delay(500);
                // const disResult = await dis.disPartnerCustFlag();
                // if (disResult.validFlag != 0) {
                //     throw new Error(`买家已申请过推广,validFlag:${JSON.stringify(disResult)},mobile:${mobile}`);
                // }
                await dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: newRealName, mobile: userInfo2.mobile, openId: userInfo2.wxOpenId });
            });
            // TODO 全字段校验 下同
            it('卖家查询推广商列表', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                const res = await dis.listDisPartner({ auditFlag: 0 });
                common.isApproximatelyEqualAssert(dis.getDisPartnerInfoExp(), res);

            });
            it('卖家查看推广商详情', async function () {
                const res = await dis.disPartnerDetail();
                common.isApproximatelyEqualAssert(dis.getDisPartnerInfoExp(), res);
            });
            it('买家-申请状态查询', async function () {
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
                const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                console.log('买家打印申请', data);

                common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
            });

            describe('卖家审核通过', async function () {
                before('卖家审核通过', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    await dis.verifyDisApply({ auditFlag: 1, shopName: sellerInfo.shopName });
                });

                it('卖家查询推广商列表', async function () {
                    const res = await dis.listDisPartner({ auditFlag: 1 });
                    common.isApproximatelyEqualAssert(dis.getDisPartnerInfoExp(), res);
                });

                it('买家-申请状态查询', async function () {
                    await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
                    const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                    common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
                });

            });
            //用例对接补充场景
            describe('自动通过申请', async function () {
                before('开启参数', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    await ss.config.saveOwnerVal({
                        ownerKind: 6,
                        "data": [{ ownerId: sellerInfo.tenantId, "domainKind": 'business', "code": "ss_distribute_flag", "val": "1" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_brokerage_rate", "val": "12" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_auto_agree_flag", "val": "1" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_act_spu_brokerage_flag", "val": "0" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_level_open_flag", "val": "0" }]
                    });
                });
                after('关闭参数', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    await ss.config.saveOwnerVal({
                        ownerKind: 6,
                        "data": [{ ownerId: sellerInfo.tenantId, "domainKind": 'business', "code": "ss_distribute_flag", "val": "1" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_brokerage_rate", "val": "12" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_auto_agree_flag", "val": "0" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_act_spu_brokerage_flag", "val": "0" },
                        { "ownerId": sellerInfo.tenantId, "domainKind": "business", "code": "ss_dis_level_open_flag", "val": "0" }]
                    });
                });
                describe('新买家申请成为推广官', async function () {
                    let userInfo, newMobile2 = common.getRandomMobile(), realName2 = common.getRandomChineseStr(3);
                    before('申请成为推广官', async function () {
                        await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: newMobile2, appId: ssAccount.seller6.appId, scene: 1 });
                        userInfo = _.cloneDeep(LOGINDATA);
                        await dis.applyDistribute({ sellerTenantId: sellerInfo.tenantId, realName: realName2, mobile: userInfo.mobile, sessionId: userInfo.sessionId, openId: userInfo.wxOpenId, check: false });
                        await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    });

                    it('卖家查询推广商列表', async function () {
                        const res = await dis.listDisPartner({ auditFlag: 1 });
                        common.isApproximatelyEqualAssert(dis.getDisPartnerInfoExp(), res);
                    });

                    it('申请状态查询', async function () {
                        await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: newMobile, appId: ssAccount.seller6.appId, scene: 1 });
                        const data = await dis.disPartnerCustFlag({ sellerTenantId: sellerInfo.tenantId });
                        common.isApproximatelyEqualAssert(dis.getDisPartnerFlagExp(), data);
                    });

                });

            });

        });

        describe('成为了推广商,四处看看', function () {
            before('买家切换推广商', async function () {
                await ssReq.ssClientLogin({ mobile: mobile });
                await ssReq.switchDisPartner({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            });
            it.skip('获取推广商二维码', async function () {
                console.log(sellerInfo.tenantId);

                const res = await dis.appCodeForShopSeller({ sellerTenantId: sellerInfo.tenantId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, type: 2 });
                expect(res.val).not.to.be.undefined;
                // console.log(`res=${JSON.stringify(res)}`);
            });
            // TODO 第三部分需要补充数据校验
            it('查询推广中心首页数据', async function () {
                const data = await dis.disStatisticsData({ sellerTenantId: sellerInfo.tenantId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, type: 2 });
                console.log(`data=${JSON.stringify(data)}`);

                common.isApproximatelyEqualAssert(dis.getDisStatisticsDataExp(), data);

            });

            it('我的客户列表', async function () {
                const data = await dis.myCustList({ sellerTenantId: sellerInfo.tenantId, lastVisitTime: true, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                expect(data.total, '新推广商，除自己外有客户').to.be.equal(0);
            });

            it('查看订单列表', async function () {
                const data = await dis.findDisOrders({ sellerTenantId: sellerInfo.tenantId, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
                expect(data.total, '新推广商，有订单').to.be.equal(0);
            });

            it.skip('获取推广总价', async function () {
                const res = await dis.totalBrokerage();
                expect(res.data.val, '新推广商，有总价').to.be.equal(0);
            });
            describe('卖家停用该分销商', async function () {
                before('卖家登录后停用', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                    await dis.updateDisInfo({ id: dis.id, flag: 0, realName: dis.realName, mobile: dis.mobile })
                });
                after('再次启用该推广员', async function () {
                    await dis.updateDisInfo({ id: dis.id, flag: 1, realName: dis.realName, mobile: dis.mobile })
                });
                it('查看分销商详情', async function () {
                    const disDetail = await dis.disPartnerDetail({ disId: dis.id });
                    common.isApproximatelyEqualAssert(disDetail, dis.getDisPartnerInfoExp());

                });
            });

        });

        // TODO 移到第三部分
        describe('提现', function () {
            let getAccount, withdrawId;
            before('买家切换推广商', async function () {
                await ssReq.switchDisPartner({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
            });
            it('查询账户', async function () {
                getAccount = await ss.dis.getAccount({ disId: dis.id, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
            });

            it('申请提现', async function () {
                await ss.dis.applyWithdraw({ disId: dis.id, applyMoney: getAccount.result.data.withdrawableAmount, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
            });

            it('卖家提现申请列表：待审核', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                const withdrawApplyList = await ss.dis.getWithdrawApplyList({ flag: 0 }).then(res => res.result.data.rows);
                withdraw = withdrawApplyList.find(obj => obj.disPartnerId == dis.id);
                // console.log(`\n withdraw =${JSON.stringify(withdraw)}`);

            });

            it('卖家审核', async function () {
                // 申请状态 0：待审核，1：已打款，2：待打款，3：拒绝
                // 目前只允许：待审核->拒绝；待审核->待打款；待打款->已打款；三种状态变更方式
                await ss.dis.apprWithdraw({ id: withdraw.id, flag: 2, applyMoney: getAccount.result.data.withdrawableAmount, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
            });


            it('卖家提现申请列表：待打款', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                const withdrawApplyList = await ss.dis.getWithdrawApplyList({ flag: 2 }).then(res => res.result.data.rows);
                withdraw = withdrawApplyList.find(obj => obj.disPartnerId == dis.id);
                // console.log(`\n withdraw =${JSON.stringify(withdraw)}`);
            });

            it('卖家打款', async function () {
                // 申请状态 0：待审核，1：已打款，2：待打款，3：拒绝
                // 目前只允许：待审核->拒绝；待审核->待打款；待打款->已打款；三种状态变更方式
                await ss.dis.apprWithdraw({ id: withdraw.id, flag: 1, applyMoney: getAccount.result.data.withdrawableAmount, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });
            });

            it('卖家提现申请列表：已打款', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.seller8.mobile, shopName: ssAccount.seller8.shopName, appKey: ssAccount.seller8.appKey, appSecret: ssAccount.seller8.appSecret });
                const withdrawApplyList = await ss.dis.getWithdrawApplyList({ flag: 1 }).then(res => res.result.data.rows);
                withdraw = withdrawApplyList.find(obj => obj.disPartnerId == dis.id);
                // console.log(`\n withdraw =${JSON.stringify(withdraw)}`);
            });

            it('推广商提现记录', async function () {
                await ssReq.switchDisPartner({ tenantId: sellerInfo.tenantId, mobile: mobile, appId: ssAccount.seller6.appId });
                const getDisWithdrawDetailList = await ss.dis.getDisWithdrawDetailList({ disId: dis.id, applyMoney: getAccount.result.data.withdrawableAmount, _cid: sellerInfo.clusterCode, _tid: sellerInfo.tenantId });

            });
        });
    })

    // TODO 第三部分补充
    describe('再次关闭推广参数', function () {

    });

});