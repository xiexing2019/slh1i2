const common = require('../../../../lib/common');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const timeLimitManage = require('../../../help/act/timeLimit');
const dresManage = require('../../../help/dresManage');
const dresSpuGroup = require('../../../help/spdresb/dresSpuGroup');
const spdresup = require('../../../../../node/reqHandler/ss/spb/spdresup');

/**
 * 限时折扣
 * 属于全局营销活动，如限时折扣、限时减免、会员日
 * 
 * 微商城营销体系设计
 * http://192.168.0.251:8080/index.php/apps/onlyoffice/11828?filePath=%2Fec-doc%2F05.项目组%2F04.衣科商城%2F10.设计%2F应用组%2F微商城营销体系设计.docx
 */
// (async function () {
//     await ssReq.ssSellerLogin();

//     const actList = await ss.act.timeLimit.getMarketList({ flag: 3 }).then(res => res.result.data.rows);
//     console.log(`actList=${JSON.stringify(actList)}`);

//     for (let index = 0; index < actList.length; index++) {
//         const element = actList[index];
//         await ss.act.timeLimit.deleteRule({ id: element.id });
//     }

//     // const spuInfo = await ss.act.marketBase.translateSpuPartAct({ spuKey: 'spuId', setField: 'actInfos', unitId: LOGINDATA.unitId });
//     // console.log(`spuInfo=${JSON.stringify(spuInfo)}`);


// })();

describe('限时折扣', function () {
    this.timeout(30000);
    let seller, client;
    const act = timeLimitManage.setupTimeLimit();

    /** 修改活动商品时使用 */
    let dresList1 = [], dresList2 = [];
    before('保存限时活动', async function () {
        await ssReq.userLoginWithWx();
        client = _.cloneDeep(LOGINDATA);

        await ssReq.ssSellerLogin();
        seller = _.cloneDeep(LOGINDATA);
        await timeLimitManage.deleteOrDisableAllAct();

        const dresList = await act.getDresList(5);
        [dresList1, dresList2] = _.chunk(dresList, 3);

        // console.log(`dresList=${JSON.stringify(dresList)}`);

        const actJson = timeLimitManage.mockActJson(dresList1);
        await act.saveFull(actJson, dresList1);
        console.log(act);
        await common.delay(1000);

    });

    it('限时折扣列表查询', async function () {
        this.retries(2);
        await common.delay(500);
        const _act = await act.getActList();
        const exp = act.getActBase();
        common.isApproximatelyEqualAssert(exp, _act, ['spuList']);
        common.isApproximatelyArray(exp.spuList, _act.spuList, ['id']);
    });

    it('限时/秒杀活动详情', async function () {
        this.retries(2);
        await common.delay(500);
        const _act = await act.getById();
        common.isApproximatelyEqualAssert(act.getActBase(), _act);
    });

    it('获取活动分享页面数据', async function () {
        const data = await ss.spdresup.getSharePageDataForActivity({ activityId: act.id, shopId: seller.shopId }).then(res => res.result.data);
        expect(data).to.have.property('QRCodeDocId');
        expect(data).to.have.property('shopLogoPic');
    });

    // 切换为买家
    it('查询商品详情', async function () {
        await ssReq.userLoginWithWx();
        const spus = [...act.spuMap.values()];
        for (let index = 0; index < spus.length; index++) {
            const spu = spus[index];
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spu.spuId, _tid: seller.tenantId, _cid: seller.clusterCode })
                .then(res => res.result.data);
            // console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
            common.isApproximatelyEqualAssert(act.getDresActInfo({ spuId: spu.spuId }), dresDetail.activity, ['invType']);
        }
    });

    it('小程序专题活动获取活动列表(实时)', async function () {
        await act.findMarketForActivityAssert({ sellerTenantId: seller.tenantId });
    });

    it.skip('服务端分享商品海报-限时折扣分享', async function () {
        const dresSpu = [...act.spuMap.values()][0];
        const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: dresSpu.spuId, _tid: seller.tenantId, _cid: seller.clusterCode })
            .then(res => res.result.data);
        const res = await ss.up.shareSku({ type: 3, shopId: seller.tenantId, spuId: dresSpu.spuId, spuPrice: dresDetail.activity.discountAmount, spuOrgPrice: dresDetail.activity.pubPrice, shareScene: 1 });
        console.log(`res=${JSON.stringify(res)}`);
        expect(res.result.data.val).not.to.be.null;
    });

    it('获取商品小视频', async function () {
        const dresList = await ss.spdresb.findDresFileList({ sellerTenantId: seller.tenantId }).then(res => res.result.data.rows);
        const actDresSpuIds = [...act.spuMap.keys()];
        actDresSpuIds.forEach(spuId => {
            const dres = dresList.find(data => data.spuId == spuId);
            dres && expect(dres).to.includes({ actLabels: '限时折扣', afterDiscountPrice: act.getDresActInfo({ spuId }).afterDiscountPrice });
        });
    });

    it('卖家查询买家访问商品信息', async function () {
        const dres = act.spuMap.get(dresList1[0].id);
        console.log(`dres=${JSON.stringify(dres)}`);
        await sp.spdresb.saveBuyerView({ spuId: dres.spuId, sellerId: seller.tenantId, sellerUnitId: seller.unitId, type: 1 });

        await ssReq.ssSellerLogin();
        const dressVisits = await ss.up.sellerViewQueryByDate({ buyerId: client.tenantId }).then(res => res.result.data.rows);
        const _dres = dressVisits.find(ele => ele.browseDate == common.getCurrentDate()).goodsDTO.find(ele => ele.spuId == dres.spuId);
        expect(_dres).to.includes({ activeFlag: act.flag, activeName: '限时折扣' });
    });

    // 切换为卖家
    it('查看单个活动商品信息', async function () {
        await ssReq.ssSellerLogin();
        for (const spu of [...act.spuMap.values()]) {
            const dresDetail = await act.findFullMarketSpu({ spuId: spu.spuId });
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            common.isApproximatelyEqualAssert(spu, dresDetail, ['id']);
        };
    });

    describe.skip('商品分组', async function () {
        let spuIds = [];
        const newGroup = dresSpuGroup.setupDresSpuGroup();
        before('新建商品分组并加入活动商品', async function () {
            await ssReq.ssSellerLogin();
            // [...act.spuMap.keys()].slice(0, 1).forEach(dresSpu => spuIds.push(dresSpu.id));
            await newGroup.addGroup({ groupName: `分组${common.getRandomStr(5)}`, dresSpuIds: [...act.spuMap.keys()].toString() });
        });
        it('查询分组列表', async () => {
            const groupInfo = await newGroup.findGroupInfoFromList();
            console.log(`groupInfo=${JSON.stringify(groupInfo)}`);
            common.isApproximatelyEqualAssert(newGroup.getListExp(), groupInfo);
        });
        it('买家查询商品分组详情', async function () {
            await ssReq.userLoginWithWx();
            const buyerGroupDetail = await ss.spdresb.getSpuListByGroupId({ groupId: newGroup.id, buyerId: LOGINDATA.tenantId, tenantId: seller.tenantId }).then((res) => res.result);
            console.log(`\n buyerGroupDetail=${JSON.stringify(buyerGroupDetail)}`);
        });
    });

    describe('店铺装修-活动商品', async function () {
        let spuId;
        before('店铺装修-活动商品', async function () {
            await ssReq.ssSellerLogin();
            spuId = dresList1[0].id;
            let param = { "content": { "homeData": [{ "data": [{ "bizType": 2, "data": { "id": spuId } }] }] } };

            await spdresup.savePageFragment(param);


        });
        it('微信查看', async function () {
            await ssReq.userLoginWithWx();
            client = _.cloneDeep(LOGINDATA);
            let content = await spdresup.getPageFragment({ custUserId: client.userId, unitId: seller.unitId, templateVer: 5 }).then(res => res.result.data.content);
            content = JSON.parse(content);
            const homeData = content.homeData;
            const activitySpuDTO = homeData.map(data =>
                data.data.find(data => data.bizType == 2 && data.data.id == spuId).data.activitySpuDTO
            );
            common.isApproximatelyEqualAssert(act.getDresActInfo({ spuId: spuId }), activitySpuDTO);

        });

    });

    describe('卖家商品列表-活动商品', async function () {
        describe('活动商品增加活动字段', async function () {
            let dresSpuIds, dresDetail;
            before('查询商品列表', async function () {
                await ssReq.ssSellerLogin();
                dresSpuIds = dresList1.map(obj => obj.id);
                // dresDetail = await sp.spdresb.findSellerSpuList({ pageNo: 1, pageSize: 10, outSpuId: dresSpuIds[0], flags: 1, orderBy: 'market_date', orderByDesc: false, filterActive: 1 }).then(res => res.params)
                dresDetail = await sp.spdresb.findSellerSpuList({ pageNo: 1, pageSize: 10, outSpuId: dresSpuIds[0], flags: 1, orderBy: 'market_date', orderByDesc: false, filterActive: 1 }).then(res => res.result.data.rows);
            });
            it('活动商品名称', async function () {
                //断言有活动字段
                expect(dresDetail[0], `${dresDetail[0].activeName}`).to.have.own.property('activeName');
            });
            it('活动商品的活动id为1', async function () {
                expect(dresDetail[0], `${dresDetail[0].activeFlag}`).to.include({ activeFlag: 1 });
                // expect(dresDetail[0].activeFlag, 'activeFlag不为1').to.be.equal(1)
            });
        });
        describe.skip('活动商品沉底', async function () {
            let dresSpuList, dresSpuList2, pn = [1, 2], catList, catList2;
            before('查询商品列表沉底', async function () {
                dresSpuList = await sp.spdresb.findSellerSpuList({ pageNo: pn[0], pageSize: 10, flags: 1, orderBy: 'market_date', orderByDesc: false, filterActive: 1 }).then(res => res.result.data.rows);
            });
            it('商品已沉底', async function () {
                let a = dresSpuList.find(obj => obj.activeFlag == 1);

                if (a == undefined) {
                    dresSpuList2 = await sp.spdresb.findSellerSpuList({ pageNo: pn[1], pageSize: 10, flags: 1, orderBy: 'market_date', orderByDesc: false, filterActive: 1 }).then(res => res.result.data.rows);
                    let b = dresSpuList2.find(obj => obj.activeFlag == 1);
                    if (b != undefined) {
                        catList2 = dresSpuList2.slice(dresSpuList2.findIndex(b));
                        for (i = 0; i < catList2.length; i++) {
                            expect(catList2[i]).to.include({ activeFlag: 1 });
                        }
                    }
                } else {
                    catList = dresSpuList.slice(dresSpuList.findIndex(a));
                    for (i = 0; i < catList.length; i++) {
                        expect(catList[i]).to.include({ activeFlag: 1 });
                    }
                }
            });
        })


    });

    describe('修改活动', async function () {
        before(async function () {

        });
        it('限时/秒杀活动详情', async function () {
            const _act = await act.getById();
            common.isApproximatelyEqualAssert(act.getActBase(), _act);
        });
    });

    describe('修改活动商品', async function () {
        describe('批量添加商品', async function () {
            before('批量添加商品', async function () {
                await ssReq.ssSellerLogin();
                await act.batchAddSpu({ dresList: dresList2 });
            });
            it('查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const spus = [...act.spuMap.values()];
                for (let index = 0; index < spus.length; index++) {
                    const spu = spus[index];
                    const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spu.spuId, _tid: seller.tenantId, _cid: seller.clusterCode })
                        .then(res => res.result.data);
                    // console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
                    common.isApproximatelyEqualAssert(act.getDresActInfo({ spuId: spu.spuId }), dresDetail.activity, ['invType']);
                }
            });
        });
        describe('修改活动商品信息', async function () {
            before('批量删除商品', async function () {
                await ssReq.ssSellerLogin();
                // await act.batchDeleteSpu({ spuIds: dresList2 });
            });
            it('', async function () {

            });
        });
        describe('批量删除商品', async function () {
            before('批量删除商品', async function () {
                await ssReq.ssSellerLogin();
                await act.batchDeleteSpu({ spuIds: dresList2 });
            });
            it('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                for (const dres of dresList2) {
                    const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: dres.id, _tid: seller.tenantId, _cid: seller.clusterCode }).then(res => res.result.data);
                    // console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                    expect(getFullForBuyer.activity, '删除商品后,商品依然含有活动信息').to.be.undefined;
                };
            });
        });
    });

    describe('失效活动', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
            await act.disableRule();
        });
        it('限时/秒杀活动详情', async function () {
            const _act = await act.getById();
            console.log(`_act=${JSON.stringify(_act)}`);

            common.isApproximatelyEqualAssert(act.getActBase(), _act);
        });
        it('小程序专题活动获取活动列表(实时)', async function () {
            await act.findMarketForActivityAssert({ sellerTenantId: seller.tenantId });
        });
    });

    describe('删除活动', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
            await act.deleteRule();
        });
        it('限时/秒杀活动详情', async function () {
            const _act = await act.getById();
            common.isApproximatelyEqualAssert(act.getActBase(), _act);
        });
        it('小程序专题活动获取活动列表(实时)', async function () {
            await act.findMarketForActivityAssert({ sellerTenantId: seller.tenantId });
        });
    });

});




