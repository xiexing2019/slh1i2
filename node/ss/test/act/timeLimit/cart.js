const common = require('../../../../lib/common');
const timeLimitManage = require('../../../help/act/timeLimit');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');
const basicJson = require('../../../help/basicJson');
const billManage = require('../../../help/billManage');
const messageManage = require('../../../help/messageManage');

describe('限时购购物车', async function () {
    this.timeout(30000);
    let sellerInfo;
    const act = timeLimitManage.setupTimeLimit();
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        await timeLimitManage.deleteOrDisableAllAct();
    });

    describe('加入购物车', async function () {
        let dresList = [], purRes;
        before('创建活动:进行中', async function () {
            dresList = await act.getDresList(2);
            // console.log(`\n dresList=${JSON.stringify(dresList[0])}`);
            const actJson = timeLimitManage.mockActJson(dresList);

            await act.saveFull(actJson, dresList);
            // console.log(mySecKill)
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            if (act.id) await act.deleteRule();
        });
        describe('添加商品到购物车', async function () {
            let carts = [];
            before(async function () {
                await ssReq.userLoginWithWx();
                await sp.spTrade.emptyCart();

                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                console.log(`\n dresFull=${JSON.stringify(dresFull)}`);
                const cartJson = basicJson.cartJson2(dresFull);
                carts = cartJson.carts;
                const saveCartRes = await sp.spTrade.saveCartInBatchs(cartJson);
                console.log(`\n saveCartInBatchs=${JSON.stringify(saveCartRes)}`);
            });
            it('查询购物车', async function () {
                await common.delay(1000);
                const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 });
                console.log(`\ncartList=${JSON.stringify(cartList)}`);
                console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
                expect(carts[0].actId, '购物车列表的活动id与预期不一致').to.equal(cartList.result.data.rows[0].carts[0].actId);
                common.isApproximatelyEqualAssert(act.getDresActInfo({ spuId: dresList[0].id }), cartList.result.data.rows[0].spu.actInfo);
            });
        });
    });

});