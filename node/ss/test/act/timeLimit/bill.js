const common = require('../../../../lib/common');
const timeLimitManage = require('../../../help/act/timeLimit');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');
const basicJson = require('../../../help/basicJson');
const billManage = require('../../../help/billManage');
const format = require('../../../../data/format');
const ssConfigParam = require('../../../help/configParamManager');
const ssAccount = require('../../../data/ssAccount');

describe('限时购下单', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [];
    // const dres = dresManage.setupDres({ type: 'app' });
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
        timeLimitManage.deleteOrDisableAllAct();
        const act = timeLimitManage.setupTimeLimit();
        dresList = await act.getDresList(2);
    });

    describe('下单,活动:进行中', async function () {
        let timeLimitJson, purRes;
        const act = timeLimitManage.setupTimeLimit();
        before('创建活动:进行中', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            timeLimitJson = timeLimitManage.mockActJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
            });
            console.log(`timeLimitJson=${JSON.stringify(timeLimitJson)}`);
            await act.saveFull(timeLimitJson, dresList);
            // console.log(act);
            console.log('限时活动 %j', act);
        });
        after('删除活动', async function () {
            if (act.id) {
                await ssReq.ssSellerLogin();
                await ss.act.timeLimit.deleteRule({ check: false, id: act.id });
                timeLimitManage.deleteOrDisableAllAct();
            }
        });
        describe('下单', async function () {
            let styleInfoBefore, salesListRes, dresDetail;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                dresDetail = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                // console.log(`\ngetFullForBuyer=${JSON.stringify(dresDetail)}`);
                expect(dresDetail.activity, `商品详情的活动信息activity不存在${JSON.stringify(dresDetail)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);

                await ssReq.ssSellerLogin();
                //卖家商品详情
                styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
                console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);

                await ssReq.userLoginWithWx();
                purRes = await billManage.createPurBill(purJson);
                console.log(`\npurRes=${JSON.stringify(purRes)}`);
                await ssReq.ssSellerLogin();
            });
            it('查询销售单列表', async function () {
                salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.payFlag).to.equal(0);
            });
            it('查询单据详情', async function () {
                //销售单id 非采购单
                const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
                expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
            });
            it('卖家商品详情', async function () {
                let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
                console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                // , 'occupyNum', 'stockNum'
                common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
            });
            it('买家活动订单未支付提示', async function () {
                await ssReq.userLoginWithWx();
                let actBillIndex = await ss.sppur.findUnpaidActBillIndex({ sellerId: sellerInfo.tenantId });
                console.log(actBillIndex);
                const exp = { alertMsg: `您有限时折扣订单还未支付，请尽快支付享受优惠` };
                common.isApproximatelyEqualAssert(exp, actBillIndex.result.data);
            });
            describe('再来一单', async function () {
                before('再来一单', async function () {
                    await sp.spTrade.emptyCart();
                    const res = await ss.sppur.addBillItem2Cart({ purBillId: purRes.result.data.rows[0].billId });
                    console.log(`\n再来一单=${JSON.stringify(res)}`);
                });
                it('查询购物车', async function () {
                    const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                    console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                    purRes.params.jsonParam.orders[0].details.forEach(detail => {
                        common.isApproximatelyEqualAssert({ skuId: detail.skuId, skuNum: detail.num }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                    });
                    common.isApproximatelyEqualAssert(dresDetail.activity, cartList[0].spu.actInfo);
                });
            });
        });
    });

    describe('下单,活动:未开始', async function () {
        let timeLimitJson;
        const act = timeLimitManage.setupTimeLimit();
        before('创建活动:未开始', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            timeLimitJson = timeLimitManage.mockActJson(dresList, {
                startDate: moment().add(2, 'hours').format('YYYY-MM-DD HH:mm:ss'),
            });
            console.log(`timeLimitJson=${JSON.stringify(timeLimitJson)}`);
            await act.saveFull(timeLimitJson, dresList);
            // console.log(act);
            console.log(`\n act.getActBase()=${JSON.stringify(act.getActBase())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: act.id });
            timeLimitManage.deleteOrDisableAllAct();
        });
        describe('下单', async function () {
            let styleInfoBefore, salesListRes, purRes;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(dresDetail)}`);
                expect(dresDetail.activity, `商品详情的活动信息activity不存在${JSON.stringify(dresDetail)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purRes = await sp.spTrade.savePurBill(purJson);
            });
            it('判断订单状态', async function () {
                let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
                while (!checkPurBillFlag.failedMsg) {
                    if (i > 10) throw new Error(`判断订单状态已超过10次`);
                    checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
                    await common.delay(500);
                    console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
                    i++;
                };
                expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: '商品价格已变更,请刷新后重试\n' });
            });
        });
    });

    describe('下单,活动:限购', async function () {
        let timeLimitJson;
        const act = timeLimitManage.setupTimeLimit();
        before('创建活动', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            timeLimitJson = timeLimitManage.mockActJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                limitType: 1, limitNum: 2 //每人限购两个
            });
            console.log(`timeLimitJson=${JSON.stringify(timeLimitJson)}`);
            await act.saveFull(timeLimitJson, dresList);
            // console.log(act);
            console.log(`\n act=${JSON.stringify(act.getActBase())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: act.id });
            timeLimitManage.deleteOrDisableAllAct();
        });
        describe('下单0', async function () {
            let styleInfoBefore, salesListRes, getFullForBuyer;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);

                purRes = await billManage.createPurBill(purJson);
                payRes = await sp.spTrade.createPay({
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [purRes.result.data.rows[0].billId],
                    payMoney: purRes.params.jsonParam.orders[0].main.money
                });
            });
            it('买家查询商品详情', async function () {
                await common.delay(5000);
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\n buyNum=${JSON.stringify(dresFull.activity)}`);
            });
            describe('下单1', async function () {
                let salesListRes, purRes;
                before('买家获取商品信息', async function () {
                    await ssReq.userLoginWithWx();
                    await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                    const purJson = await billManage.mockPurParam(getFullForBuyer, { count: common.add(act.limitVal.limitNum, 1), num: 1 });
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purRes = await sp.spTrade.savePurBill(purJson);
                });
                it('判断订单状态', async function () {
                    let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
                    while (!checkPurBillFlag.failedMsg) {
                        if (i > 10) throw new Error(`判断订单状态已超过10次`);
                        checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
                        await common.delay(500);
                        console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
                        i++;
                    };
                    expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: '购买数量超出了最大限购数\n' });
                });
                it('买家查询商品详情', async function () {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                    console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                });
                describe('下单2', async function () {
                    let salesListRes, purRes;
                    before('买家获取商品信息', async function () {
                        await ssReq.userLoginWithWx();
                        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                        const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purRes = await billManage.createPurBill(purJson);
                    });
                    it('买家查询商品详情', async function () {
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                        console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                    });
                });
            });
        });
    });

    describe('下单,活动:叠加优惠券', async function () {
        let timeLimitJson, purRes;
        const act = timeLimitManage.setupTimeLimit();
        before('创建活动:进行中', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            timeLimitJson = timeLimitManage.mockActJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
                limitVal: { limitType: 1, limitNum: 2 },
            });
            console.log(`timeLimitJson=${JSON.stringify(timeLimitJson)}`);
            await act.saveFull(timeLimitJson, dresList);
            // console.log(act);
            console.log(`\n act.getActBase()=${JSON.stringify(act.getActBase())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: act.id });
            timeLimitManage.deleteOrDisableAllAct();
        });
        describe('下单,活动:叠加优惠券', async function () {
            let styleInfoBefore, salesListRes;
            before('买家获取商品信息', async function () {
                await ssReq.ssSellerLogin();
                couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
                // 领取优惠券
                await ssReq.userLoginWithWx();
                await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });

                const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: getFullForBuyer.activity.afterDiscountPrice, applicableSpus: [{ spuId: getFullForBuyer.spu.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice }] });
                console.log(`billInfo=${JSON.stringify(billInfo)}`);
                const couponInfo = await ss.sscoupb.findUseableCouponsByShop(billInfo).then(res => res.result.data.rows.find(obj => obj.couponId == couponId));
                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1, couponInfo: couponInfo });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                console.log(`\npurJson=${JSON.stringify(purJson)}`);

                await ssReq.ssSellerLogin();
                //卖家商品详情
                styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
                console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
                await ssReq.userLoginWithWx();
                purRes = await billManage.createPurBill(purJson);
                await ssReq.ssSellerLogin();
            });
            it('查询销售单列表', async function () {
                salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.payFlag).to.equal(0);
            });
            it('查询单据详情', async function () {
                //销售单id 非采购单
                const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
                expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
            });
            it('卖家商品详情', async function () {
                let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
                // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                // styleInfoBefore.skus[0].occupyNum += 1;
                // styleInfoBefore.spu.stockNum -= 1;
                common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
            });
        });
    });

    describe('下单,活动:适用价', async function () {
        let timeLimitJson, clientInfo, levelPrice;
        const act = timeLimitManage.setupTimeLimit();
        before('创建活动:进行中', async function () {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: ssAccount.clientAlternate.mobile, openId: ssAccount.clientAlternate.openId });
            clientInfo = _.cloneDeep(LOGINDATA);
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
            // console.log(`\n priceList=${JSON.stringify(priceList)}`);
            levelPrice = priceList.find(price => price.codeValue != 0).codeValue;
            const price = priceList.find(price => price.codeValue != levelPrice);
            console.log(price);

            await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: price.codeValue });
            timeLimitJson = timeLimitManage.mockActJson(dresList, {
                levelPrice: [levelPrice],
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
            });
            console.log(`timeLimitJson=${JSON.stringify(timeLimitJson)}`);
            await act.saveFull(timeLimitJson, dresList);
            // console.log(act);
            console.log('限时活动 %j', act);
        });
        after('删除活动', async function () {
            if (act.id) {
                await ssReq.ssSellerLogin();
                await ss.act.timeLimit.deleteRule({ check: false, id: act.id });
                timeLimitManage.deleteOrDisableAllAct();
            }
        });
        it('买家获取商品信息', async function () {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: ssAccount.clientAlternate.mobile, openId: ssAccount.clientAlternate.openId });
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            expect(dresDetail.activity, `商品详情的活动信息activity应不存在:${JSON.stringify(dresDetail)}`).to.be.undefined;
        });
        describe('修改买家适用价格与活动适用价格一致', async function () {
            before('修改买家适用价格', async function () {
                await ssReq.ssSellerLogin();
                await ss.spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: levelPrice });
            });
            it('买家获取商品信息', async function () {
                await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: ssAccount.clientAlternate.mobile, openId: ssAccount.clientAlternate.openId });
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: act.getActBase().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                expect(dresDetail.activity, `商品详情的活动信息activity应存在:${JSON.stringify(dresDetail)}`).not.to.be.undefined;
            });
        });
    });
});