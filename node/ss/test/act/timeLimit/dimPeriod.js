const common = require('../../../../lib/common');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssReq = require('../../../help/ssReq');
const timeLimitManage = require('../../../help/act/timeLimit');
const moment = require('moment');

/**
 * tips
 * 1. 设置活动重复周期时,活动的startDate与endDate传YYYY-MM-DD的格式
 * 2. 设置了周期后,卖家查看以活动状态为准,买家以商品的活动周期为准
 */
describe('限时折扣-周期校验', async function () {
    this.timeout(30000);
    let seller, dresList;
    const act = timeLimitManage.setupTimeLimit();

    before('保存限时活动', async function () {
        await ssReq.ssSellerLogin();
        seller = _.cloneDeep(LOGINDATA);
        await timeLimitManage.deleteOrDisableAllAct();

        dresList = await act.getDresList(5);

        // console.log(`dresList=${JSON.stringify(dresList)}`);

        const actJson = timeLimitManage.mockActJson(dresList);
        await act.saveFull(actJson, dresList);
        console.log(act);
    });

    after(async function () {
        if (act.id) {
            await ssReq.ssSellerLogin();
            await act.deleteRule();
        }
    });

    const typeNames = ['无周期', '每天', '每月', '每周'];
    const timeNames = ['外', '内'];
    for (let type = 2; type < 4; type++) {
        for (let isContains = 0; isContains < 2; isContains++) {
            describe(`${typeNames[type]}-时间范围${timeNames[isContains]}`, async function () {
                before('修改活动重复周期', async function () {
                    await ssReq.ssSellerLogin();
                    const actJson = timeLimitManage.mockActJson(dresList);
                    actJson.id = act.id;
                    timeLimitManage.setPeriod({ actJson, type, isContains });
                    await act.saveFull(actJson);
                    await common.delay(2000);
                });
                it('限时/秒杀活动详情', async function () {
                    const _act = await act.getById();
                    console.log(`\n_act=${JSON.stringify(_act)}`);
                    common.isApproximatelyEqualAssert(act.getActBase(), _act);
                });
                it('查询商品详情', async function () {
                    await ssReq.userLoginWithWx();
                    const spu = [...act.spuMap.values()][0];
                    const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spu.spuId, _tid: seller.tenantId, _cid: seller.clusterCode })
                        .then(res => res.result.data);
                    console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
                    console.log(`\nexp=${JSON.stringify(act.getDresActInfo({ spuId: spu.spuId }))}`);
                    common.isApproximatelyEqualAssert(act.getDresActInfo({ spuId: spu.spuId }), dresDetail.activity, ['invType', 'actFlag']);
                });
            });
        }
    }

});
