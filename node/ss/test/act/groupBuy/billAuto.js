const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssCaps = require('../../../../reqHandler/ss/ssCaps')
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupBuyManager = require('../../../help/act/groupBuy');
const billManage = require('../../../help/billManage');
const ssAccount = require('../../../data/ssAccount');

describe('拼团活动-自动拼团-offline', function () {
    this.timeout(30000);
    let dres, sellerInfo, styleInfoBefore, orderRes, selesListRes, actJson, clientInfo, homePageMoneyBefore;
    const groupBuyAct = groupBuyManager.setupGroupBuy();
    before('新增拼团活动', async () => {
        // 不限购 2人团 9.99元 自动拼团
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
        //删除拼图活动
        for (let flag of [-2, 1, 2, 3]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }
        //新增活动商品
        await dresManage.prePrepare();
        const json = basicJson.styleJson();
        spuId = await sp.spdresb.saveFull(json).then(res => res.result.data.spuId);
        dres = await groupBuyAct.getDresList(1).then(res => res.find(val => val.id));
        actJson = await groupBuyManager.getGroupBuyJson(dres);
        // 新增拼团
        await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
        homePageMoneyBefore = await ss.fin.getMoneyHomePage().then(res => res.result.data);
        console.log(homePageMoneyBefore);
    });
    it('卖家查询拼团活动列表', async function () {
        await ssReq.ssSellerLogin();
        await common.delay(5000);
        let actList = await groupBuyAct.marketGroupBuyList();
        let exp = groupBuyAct.getGroupBuyExp();
        common.isApproximatelyEqualAssert(exp, actList);
    });
    after('删除拼团活动', async () => {
        await ssReq.ssSellerLogin();
        for (let flag of [-2, 1, 2, 3]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }
    });
    describe('买家开团-下单', function () {
        let orderRunListRes;
        before('买家查询团购商品信息', async () => {
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            await ssReq.ssSellerLogin();
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // 下单
            await ssReq.userLoginWithWx();
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            // groupBuyAct.updateGroupBuyStatistics1(orderRes);
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('卖家查询销售单列表', async function () {
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('卖家查询单据详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家查询商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            // , 'occupyNum', 'stockNum'
            // styleInfoBefore.skus[0].occupyNum += 1; // 创建支付的时候才锁库存
            // styleInfoBefore.spu.stockNum -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count).to.be.equal(0);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `买家开团下单未支付，就能在拼团订单列表中查询到`).to.be.equal(0);
        });
        it.skip('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            let orderRunListResExp = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExp.groupBuyOrderResultList.items[0].userId = clientInfo.tenantId;
            orderRunListResExp.groupBuyOrderResultList.items[0].userName = clientInfo.userName;
            orderRunListResExp.groupBuyOrderResultList.items[0].purBillId = orderRes.result.data.rows[0].billId;
            orderRunListResExp.groupBuyOrderResultList.items[0].salesBillId = selesListRes.bill.id;
            // console.log(`\norderRunListRes = ${JSON.stringify(orderRunListRes)}`);
            console.log(`\norderRunListResExp = ${JSON.stringify(orderRunListResExp)}`);
            common.isApproximatelyEqualAssert(orderRunListResExp, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList[0], ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it.skip('买家查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
    });
    describe('买家支付订单', function () {
        let payRes, orderRunListRes, selesListRes;
        before(async () => {
            await ssReq.userLoginWithWx();
            payRes = await groupBuyAct.creatPay({
                payParams: {
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [orderRes.result.data.rows[0].billId],
                    payMoney: orderRes.params.jsonParam.orders[0].main.money,
                },
                orderRes: orderRes
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
        });
        it('卖家查询商品详情', async function () {
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum += 1; // 创建支付的时候才锁库存
            styleInfoBefore.spu.stockNum -= 1;
            styleInfoBefore.skus[0].num -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('支付回调', async function () {
            await ssReq.userLoginWithWx();
            const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
        });
        it('卖家查询商品详情', async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            let exp = _.cloneDeep(styleInfoBefore);
            exp.skus[0].occupyNum -= 1; // 支付完成解锁库存
            exp.skus[0].ssNum -= 1;
            // exp.skus[0].num -= 1;
            exp.spu.salesNum += 1;
            // styleInfoBefore.spu.stockNum += 1;
            exp.spu.salesMoney += groupBuyAct.activityPrice;
            common.isApproximatelyEqualAssert(exp, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(3000);
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 7 })  // statusType: 7 表示订单状态：代成团
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            // console.log(selesListRes);
            expect(selesListRes.bill).to.be.include({ payFlag: 1 });
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
        });
        it('卖家查询拼团活动列表--进行中', async function () {
            const actInfo = await groupBuyAct.marketGroupBuyList();
            const exp = _.cloneDeep(groupBuyAct.getGroupBuyExp());
            exp.spus[0].stockNum -= 1;
            common.isApproximatelyEqualAssert(exp, actInfo);
        });
        it('查询拼团活动数据', async function () {
            const statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${JSON.stringify(statisticsExp)}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询小团列表', async function () {
            this.retries(3);
            await common.delay(3000);
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count).to.be.equal(1);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3);
            await common.delay(5000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoneyBefore);
            exp.totalMoney = common.add(exp.totalMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            exp.settlementMoney = common.add(exp.settlementMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
        it('买家查询拼团中订单列表', async function () {
            this.retries(3);
            await common.delay(3000);
            await ssReq.userLoginWithWx();
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            // console.log(`\n买家查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            let orderRunListResExp = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExp.groupBuyOrderResultList.items[0].userId = clientInfo.userId;
            orderRunListResExp.groupBuyOrderResultList.items[0].userName = clientInfo.nickName;
            orderRunListResExp.groupBuyOrderResultList.items[0].purBillId = orderRes.result.data.rows[0].billId;
            orderRunListResExp.groupBuyOrderResultList.items[0].salesBillId = selesListRes.bill.id;
            // console.log(`\norderRunListRes = ${JSON.stringify(orderRunListResExp)}`);
            common.isApproximatelyEqualAssert(orderRunListResExp, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList[0], ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items[0]);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it('买家查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            // console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            // console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
    });
    describe('使拼团失效,触发自动拼团', function () {
        before('使拼团失效', async () => {
            await ssReq.ssSellerLogin();
            await groupBuyAct.marketGroupBuyInvalid();
        });
        it('卖家查询拼团活动列表--已结束', async function () {
            const actInfo = await groupBuyAct.marketGroupBuyList();
            const exp = _.cloneDeep(groupBuyAct.getGroupBuyExp());
            exp.spus[0].stockNum -= 1;
            common.isApproximatelyEqualAssert(exp, actInfo);
        });
        it('查询拼团活动数据', async function () {
            this.retries(3);
            await common.delay(3000);
            let statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics.data);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${statisticsExp}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询小团列表', async function () {
            this.retries(3);
            await common.delay(3000);
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 1 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count).to.be.equal(1);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3);
            await common.delay(5000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoneyBefore);
            exp.totalMoney = common.add(exp.totalMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            exp.settlementMoney = common.add(exp.settlementMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
        it('卖家查询销售单列表', async function () {
            await common.delay(5000);
            let selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 2 })  // statusType: 2 表示订单状态：未发货
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            console.log(`selesListRes = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill).to.be.include({ flag: 3 });
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill).to.be.include({ flag: 3 });
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `自动拼团成功后，还能在拼团订单列表中查询到`).to.be.equal(0);
        });
    });
    describe('拼团订单跨店铺查看-offline', function () {
        before('买家切换到门店7', async () => {
            await ssReq.userLoginWithWx({ shopId: ssAccount.seller7.tenantId });
        });
        it('查询订单列表', async function () {
            const res = await sp.spTrade.purFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 });
            expect(res.result.data.total, `拼团订单跨店铺失败`).be.be.equal(1);
        });
    });
});