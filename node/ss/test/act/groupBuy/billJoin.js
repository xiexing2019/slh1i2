const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssCaps = require('../../../../reqHandler/ss/ssCaps');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupBuyManager = require('../../../help/act/groupBuy');
const billManage = require('../../../help/billManage');
const ssAccount = require('../../../data/ssAccount');

describe('拼团活动-凑团-offline', function () {
    this.timeout(30000);
    let dres, sellerInfo, actJson, clientInfoA, clientInfoB, styleInfoBefore, orderRes, mobile = common.getRandomMobile(), orderRunListRes, orderRunListResExpA, homePageMoneyBefore;
    console.log(mobile);

    const groupBuyAct = groupBuyManager.setupGroupBuy();
    before('新增拼团活动', async () => {
        // 限购 2人团 9.99元 凑团
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
        //删除拼图活动
        for (let flag of [-2, 1, 2, 3]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }

        dres = await groupBuyAct.getDresList(1).then(res => res.find(val => val.id));
        // console.log(`dresInfo = ${JSON.stringify(dres)}`);
        actJson = await groupBuyManager.getGroupBuyJson(dres);
        actJson.limitedNum = 1;
        await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
        homePageMoneyBefore = await ss.fin.getMoneyHomePage().then(res => res.result.data);
        console.log(homePageMoneyBefore);
    });
    after('删除拼团活动', async () => {
        await ssReq.ssSellerLogin();
        for (let flag of [1, 2, 3, -2]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            // console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }
    });
    it('卖家查询拼团活动列表', async function () {
        let actList = await groupBuyAct.marketGroupBuyList();
        // console.log(actList);
        let exp = groupBuyAct.getGroupBuyExp();
        common.isApproximatelyEqualAssert(exp, actList);
    });
    describe('买家A开团-下单', function () {
        let orderRunListRes, selesListRes, dresDetail;
        before('买家查询团购商品信息', async () => {
            await ssReq.userLoginWithWx();
            clientInfoA = _.cloneDeep(LOGINDATA);
            // console.log(`clientInfoA = ${JSON.stringify(clientInfoA)}`);
            // console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            await ssReq.ssSellerLogin();
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // 下单
            await ssReq.userLoginWithWx();
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            // groupBuyAct.updateGroupBuyStatistics1(orderRes);
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('卖家查询销售单列表', async function () {
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('卖家查询单据详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家查询商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            // , 'occupyNum', 'stockNum'
            // styleInfoBefore.skus[0].occupyNum += 1;  // 创建支付的时候才锁库存
            // styleInfoBefore.spu.stockNum -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`买家查询拼团列表 = ${JSON.stringify(qlRes)}`);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `买家开团下单未支付，就能在拼团订单列表中查询到`).to.be.equal(0);
        });
        it.skip('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            orderRunListResExpA = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExpA.groupBuyOrderResultList.items[0].userId = clientInfoA.userId;
            orderRunListResExpA.groupBuyOrderResultList.items[0].userName = clientInfoA.nickName;
            orderRunListResExpA.groupBuyOrderResultList.items[0].purBillId = orderRes.result.data.rows[0].billId;
            orderRunListResExpA.groupBuyOrderResultList.items[0].salesBillId = selesListRes.bill.id;
            // console.log(`\norderRunListRes = ${JSON.stringify(orderRunListRes)}`);
            console.log(`\norderRunListResExp = ${JSON.stringify(orderRunListResExpA)}`);
            common.isApproximatelyEqualAssert(orderRunListResExpA, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExpA.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList[0], ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExpA.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items[0]);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it.skip('买家查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
        describe('再来一单', async function () {
            before('再来一单', async function () {
                await ssReq.userLoginWithWx();
                await sp.spTrade.emptyCart();
                const res = await ss.sppur.addBillItem2Cart({ purBillId: orderRes.result.data.rows[0].billId });
                console.log(`\n再来一单=${JSON.stringify(res)}`);
            });
            it('查询购物车', async function () {
                const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows);
                console.log(`\n购物车列表=${JSON.stringify(cartList)}`);
                orderRes.params.jsonParam.orders[0].details.forEach(detail => {
                    common.isApproximatelyEqualAssert({ skuId: detail.skuId, skuNum: detail.num }, cartList[0].carts.find(obj => obj.skuId == detail.skuId));
                });
                dresDetail.activity.completeBuy += orderRes.params.jsonParam.orders[0].main.totalNum;
                common.isApproximatelyEqualAssert(dresDetail.activity, cartList[0].spu.actInfo);
            });
        });
    });
    describe('买家A支付订单', function () {
        let payRes, selesListRes;
        before(async () => {
            await ssReq.userLoginWithWx();
            payRes = await groupBuyAct.creatPay({
                payParams: {
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [orderRes.result.data.rows[0].billId],
                    payMoney: orderRes.params.jsonParam.orders[0].main.money,
                },
                orderRes: orderRes
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
        });
        it('卖家查询商品详情', async function () {
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum += 1; // 创建支付的时候才锁库存
            styleInfoBefore.skus[0].num -= 1; // 创建支付的时候才锁库存
            styleInfoBefore.spu.stockNum -= 1;
            // styleInfoBefore.spu.num -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('支付回调', async function () {
            await ssReq.userLoginWithWx();
            const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
        });
        it('卖家查询商品详情', async function () {
            await common.delay(5000);
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum -= 1; // 支付完成解锁库存
            styleInfoBefore.skus[0].ssNum -= 1;
            // styleInfoBefore.skus[0].num -= 1;
            styleInfoBefore.spu.salesNum += 1;
            // styleInfoBefore.spu.stockNum += 1;
            // styleInfoBefore.spu.salesMoney += 9.99;
            styleInfoBefore.spu.salesMoney = common.add(styleInfoBefore.spu.salesMoney, groupBuyAct.activityPrice);
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(3000);
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(1);
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
        });
        it('查询拼团活动数据', async function () {
            const statistics = await groupBuyAct.marketGroupBuyStatistics();
            // console.log(statistics);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            // console.log(`\nstatisticsExp = ${JSON.stringify(statisticsExp)}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3)
            await ssReq.ssSellerLogin();
            await common.delay(1000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoneyBefore);
            // exp.totalMoney = common.add(exp.totalMoney, 9.97);
            exp.totalMoney = common.add(exp.totalMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            exp.settlementMoney = common.add(exp.settlementMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            // exp.settlementMoney = common.add(exp.settlementMoney, 9.97);
            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
        it('卖家查询拼团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`买家查询拼团列表 = ${JSON.stringify(qlRes)}`);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            orderRunListResExpA = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExpA.groupBuyOrderResultList.items[0].userId = clientInfoA.userId;
            orderRunListResExpA.groupBuyOrderResultList.items[0].userName = clientInfoA.nickName;
            orderRunListResExpA.groupBuyOrderResultList.items[0].purBillId = orderRes.result.data.rows[0].billId;
            orderRunListResExpA.groupBuyOrderResultList.items[0].salesBillId = selesListRes.bill.id;
            orderRunListResExpA.groupBuyOrderResultList.items[0].flag = 0;
            console.log(`\norderRunListRes = ${JSON.stringify(orderRunListResExpA)}`);
            common.isApproximatelyEqualAssert(orderRunListResExpA, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExpA.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList[0], ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExpA.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items[0]);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it('买家查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
    });
    describe('买家B凑团-下单', function () {
        let orderRunListRes, selesListRes;
        before('买家查询团购商品信息', async () => {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            clientInfoB = _.cloneDeep(LOGINDATA);
            clientInfoB.wxOpenId = 'oz2XI5Zz1wMpwKFxN1tNQm7WGgYo';
            console.log(`clientInfoB = ${JSON.stringify(clientInfoB)}`);

            let areaJson = await basicJson.getAreaJson();
            let json = await basicJson.addAddrJson(areaJson);
            json.recInfo.isDefault = 1;
            json.address.provinceCode = 110000;   //这里省市区写死，为了保证此地有运费规则可以覆盖到
            json.address.cityCode = 110100;
            json.address.countyCode = 110101;
            await sp.spmdm.saveUserRecInfo(json);
            console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId, marketGroupBuyOrderId: [...groupBuyAct.groupOrderMap.values()][0].id };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            await ssReq.ssSellerLogin();
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // 下单
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            // groupBuyAct.updateGroupBuyStatisticsB(orderRes);
            // [...groupBuyAct.groupOrderMap.values()][0].flag = 1;
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('卖家查询销售单列表', async function () {
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('卖家查询单据详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家查询商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            // let styleInfoAfter = await getDetailStyleInv(orderRes.params.jsonParam.orders[0].details);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            // , 'occupyNum', 'stockNum'
            // styleInfoBefore.skus[0].occupyNum += 1;  // 创建支付的时候才锁库存
            // styleInfoBefore.spu.stockNum -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('查询拼团活动数据', async function () {
            const statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${JSON.stringify(statisticsExp)}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('买家B查询拼团中订单列表', async function () {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家B查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            let orderRunListResExpB = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExpB.buySuccessNum = 2;
            orderRunListResExpB.groupBuyOrderResultList.groupJoinNum = 2;
            orderRunListResExpB.joinGroupBuyPersonNum += 1;
            orderRunListResExpB.groupJoinNum += 1;
            // let itemsB = {
            //     actId: groupBuyAct.id,
            //     flag: -1,
            //     leaderFlag: 0,
            //     userId: clientInfoB.userId,
            //     userName: clientInfoB.nickName,
            //     purBillId: orderRes.result.data.rows[0].billId,
            //     salesBillId: selesListRes.bill.id
            // };
            // console.log(`\norderRunListResExpB= ${JSON.stringify(orderRunListResExpB)}`);
            orderRunListResExpB.groupBuyOrderResultList.items[0] = orderRunListResExpA.groupBuyOrderResultList.items[0]
            // orderRunListResExpB.groupBuyOrderResultList.items.push(itemsB);
            console.log(`\norderRunListResExpB = ${JSON.stringify(orderRunListResExpB)}`);
            common.isApproximatelyEqualAssert(orderRunListResExpB, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExpB.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList, ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExpB.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items[0]);
            // common.isApproximatelyEqualAssert(orderRunListResExpB.groupBuyOrderResultList.items[1], orderRunListRes.data.groupBuyOrderResultList[0].items[1]);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it('买家B查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
    });
    describe('买家B支付订单', function () {
        let payRes, selesListRes;
        before(async () => {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });

            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: clientInfoB.wxOpenId,
                orderIds: [orderRes.result.data.rows[0].billId],
                payMoney: orderRes.params.jsonParam.orders[0].main.money,
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
        });
        it('卖家查询商品详情', async function () {
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum += 1; // 创建支付的时候才锁库存
            styleInfoBefore.spu.stockNum -= 1;
            styleInfoBefore.skus[0].num -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'ecCaption']);
        });
        it('支付回调', async function () {
            await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: mobile });
            const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
            groupBuyAct.updateGroupBuyStatisticsB(orderRes);
        });
        it('卖家查询商品详情', async function () {
            await common.delay(5000);
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum -= 1; // 支付完成解锁库存
            styleInfoBefore.skus[0].ssNum -= 1;
            // styleInfoBefore.skus[0].num -= 1;
            styleInfoBefore.spu.salesNum += 1;
            // styleInfoBefore.spu.stockNum += 1;
            styleInfoBefore.spu.salesMoney = common.add(styleInfoBefore.spu.salesMoney, 9.99);
            console.log(`styleInfoAfter = ${JSON.stringify(styleInfoAfter)}`);
            console.log(`styleInfoBefore = ${JSON.stringify(styleInfoBefore)}`);
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'priClassId']);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin();
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(1);
        });
        it('卖家查询单据详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
        });
        it('查询拼团活动数据', async function () {
            await common.delay(3000);
            await ssReq.ssSellerLogin();
            let statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics.data);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${JSON.stringify(statisticsExp)}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询拼团活动列表--已结束', async function () {
            const actInfo = await groupBuyAct.marketGroupBuyList();
            const exp = groupBuyAct.getGroupBuyExp();
            exp.spus[0].stockNum -= 2;
            common.isApproximatelyEqualAssert(exp, actInfo);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 1 });
            console.log(`买家查询小团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.rows.length, `拼团失败`).to.be.equal(1);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `自动拼团成功后，还能在拼团订单列表中查询到`).to.be.equal(0);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3)
            await ssReq.ssSellerLogin();
            await common.delay(1000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoneyBefore);
            console.log(`exp = ${JSON.stringify(exp)}`);

            // exp.totalMoney = common.add(exp.totalMoney, 19.94);
            exp.totalMoney = common.add(exp.totalMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate)) * 2).toFixed(2));
            exp.settlementMoney = common.add(exp.settlementMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate)) * 2).toFixed(2));
            // exp.settlementMoney = common.add(exp.settlementMoney, 19.94);

            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
    });
});