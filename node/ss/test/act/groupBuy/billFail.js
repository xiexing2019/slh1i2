const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssCaps = require('../../../../reqHandler/ss/ssCaps');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupBuyManager = require('../../../help/act/groupBuy');
const billManage = require('../../../help/billManage');
const ssAccount = require('../../../data/ssAccount');

describe('拼团活动-拼团失败-offline', function () {
    this.timeout(30000);
    let dres, sellerInfo, actJson, clientInfoA, clientInfoB, styleInfoBefore, orderRes, mobile = common.getRandomMobile(), orderRunListRes, orderRunListResExpA, homePageMoney1;
    console.log(mobile);

    const groupBuyAct = groupBuyManager.setupGroupBuy();
    before('新增拼团活动', async () => {
        // 限购 2人团 9.99元 凑团
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
        //删除拼图活动
        for (let flag of [-2, 1, 2, 3]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }

        // 新增活动商品
        await dresManage.prePrepare();
        const json = basicJson.styleJson();
        spuId = await sp.spdresb.saveFull(json).then(res => res.result.data.spuId);
        dres = await groupBuyAct.getDresList(1).then(res => res.find(val => val.id));
        actJson = await groupBuyManager.getGroupBuyJson(dres);
        actJson.limitedNum = 1;
        actJson.autoClusterFlag = 0;
        await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
        homePageMoney1 = await ss.fin.getMoneyHomePage().then(res => res.result.data);
        console.log(homePageMoney1);
    });
    it('卖家查询拼团活动列表', async function () {
        let actList = await groupBuyAct.marketGroupBuyList();
        // console.log(actList);
        let exp = groupBuyAct.getGroupBuyExp();
        common.isApproximatelyEqualAssert(exp, actList);
    });
    after('删除拼团活动', async () => {
        await ssReq.ssSellerLogin();
        for (let flag of [1, 2, 3, -2]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            // console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }
    });
    describe('买家开团-下单超出限购', function () {
        before('买家查询团购商品信息', async () => {
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 2, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            await ssReq.ssSellerLogin();
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // 下单
            await ssReq.userLoginWithWx();
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            // groupBuyAct.updateGroupBuyStatistics1(orderRes)
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('超出限购下单失败', async function () {
            expect(orderRes.result.data.rows[0].failedMsg).to.be.include('购买数量超出了最大限购数');
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 2 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count, `下单超出限购,小团失败的列表没有加上去`).to.be.equal(1);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `买家下单失败，就能在拼团订单列表中查询到`).to.be.equal(0);
        });
    });
    describe('买家开团-下单', function () {
        let orderRunListRes;
        before('买家查询团购商品信息', async () => {
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            await ssReq.ssSellerLogin();
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            // 下单
            await ssReq.userLoginWithWx();
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            // groupBuyAct.updateGroupBuyStatistics1(orderRes);
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('卖家查询销售单列表', async function () {
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('卖家查询单据详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家查询商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            // let styleInfoAfter = await getDetailStyleInv(orderRes.params.jsonParam.orders[0].details);
            // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
            // , 'occupyNum', 'stockNum'
            // styleInfoBefore.skus[0].occupyNum += 1;  // 创建支付的时候猜锁库存
            // styleInfoBefore.spu.stockNum -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count).to.be.equal(0);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `买家开团下单未支付，就能在拼团订单列表中查询到`).to.be.equal(0);
        });
    });
    describe('买家支付订单', function () {
        let payRes, orderRunListRes, selesListRes;
        before(async () => {
            await ssReq.userLoginWithWx();
            payRes = await groupBuyAct.creatPay({
                payParams: {
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [orderRes.result.data.rows[0].billId],
                    payMoney: orderRes.params.jsonParam.orders[0].main.money,
                },
                orderRes: orderRes
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
        });
        it('卖家查询商品详情', async function () {
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum += 1; // 创建支付的时候才锁库存
            styleInfoBefore.spu.stockNum -= 1;
            styleInfoBefore.skus[0].num -= 1;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('支付回调', async function () {
            await ssReq.userLoginWithWx();
            const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney });
            console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
            await common.delay(5000);
        });
        it('卖家查询商品详情', async function () {
            await common.delay(3000);
            await ssReq.ssSellerLogin();
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            styleInfoBefore.skus[0].occupyNum -= 1; // 支付完成解锁库存
            styleInfoBefore.skus[0].ssNum -= 1;
            // styleInfoBefore.skus[0].num -= 1;
            styleInfoBefore.spu.salesNum += 1;
            // styleInfoBefore.spu.stockNum += 1;
            styleInfoBefore.spu.salesMoney += groupBuyAct.activityPrice;
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });
        it('卖家查询销售单列表-全部', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(3000);
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            // expect(selesListRes.bill.payFlag).to.be.equal(1);
            expect(selesListRes.bill, `拼团销售单状态显示错误`).to.be.include({ payFlag: 1, frontFlagName: "待成团" });
        });
        it('卖家查询销售单列表-待成团', async function () {
            await ssReq.ssSellerLogin();
            await common.delay(3000);
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 7 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            // expect(selesListRes.bill.payFlag).to.be.equal(1);
            console.log(`卖家查询销售单列表-待成团 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill).to.be.include({ frontFlagName: "待成团" });
        });
        it('卖家查询销售单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(1);
        });
        it('卖家查询拼团活动列表--进行中', async function () {
            const actInfo = await groupBuyAct.marketGroupBuyList();
            const exp = _.cloneDeep(groupBuyAct.getGroupBuyExp());
            exp.spus[0].stockNum -= 1;
            common.isApproximatelyEqualAssert(exp, actInfo);
        });
        it('卖家查询拼团活动数据', async function () {
            const statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${JSON.stringify(statisticsExp)}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 0 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count).to.be.equal(1);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3)
            await common.delay(5000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoney1);
            exp.totalMoney = common.add(exp.totalMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            exp.settlementMoney = common.add(exp.settlementMoney, parseFloat(common.mul(groupBuyAct.activityPrice, (1 - ssCaps.rate))).toFixed(2));
            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            orderRunListRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(orderRunListRes)}`);
            let orderRunListResExp = groupBuyAct.getGroupBuyOrderResultListExp(actJson);
            orderRunListResExp.groupBuyOrderResultList.items[0].userId = clientInfo.userId;
            orderRunListResExp.groupBuyOrderResultList.items[0].userName = clientInfo.nickName;
            orderRunListResExp.groupBuyOrderResultList.items[0].purBillId = orderRes.result.data.rows[0].billId;
            orderRunListResExp.groupBuyOrderResultList.items[0].salesBillId = selesListRes.bill.id;
            console.log(`\norderRunListRes = ${JSON.stringify(orderRunListResExp)}`);
            common.isApproximatelyEqualAssert(orderRunListResExp, orderRunListRes.data, ['groupBuyOrderResultList']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList, orderRunListRes.data.groupBuyOrderResultList[0], ['items']);
            common.isApproximatelyEqualAssert(orderRunListResExp.groupBuyOrderResultList.items[0], orderRunListRes.data.groupBuyOrderResultList[0].items[0]);
            groupBuyAct.updateGroupBuyOrder(orderRunListRes);
        });
        it('买家查询拼团中订单详情', async function () {
            let orderRunDetailRes = await groupBuyAct.findGroupBuyOrderDetail({ sellerId: sellerInfo.tenantId, actOrderId: orderRunListRes.data.groupBuyOrderResultList[0].id });
            console.log(`\n买家查询拼团中订单详情=${JSON.stringify(orderRunDetailRes)}`);
            let orderRunDetailResExp = groupBuyAct.getGroupBuyOrderResultDetailExp();
            console.log(`\n买家查询拼团中订单详情Exp = ${JSON.stringify(orderRunDetailResExp)}`);
            common.isApproximatelyEqualAssert(orderRunDetailResExp, orderRunDetailRes.data);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList, orderRunDetailRes.data.groupBuyOrderResultList[0], ['items']);
            // common.isApproximatelyEqualAssert(orderRunDetailResExp.groupBuyOrderResultList.items, orderRunDetailRes.data.groupBuyOrderResultList[0].items);
        });
        it('买家查询采购单列表', async function () {
            const purListRes = await sp.spTrade.purFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            console.log(`买家查询采购单列表 = ${JSON.stringify(purListRes)}`);
            expect(purListRes.marketDetail, `买家拼团订单列表显示的不是邀请好友`).to.be.include({ flag: 0, itemFlag: 0 }) // flag=0 和itemFlag=0 前端显示邀请好友
        });
    });
    describe('使拼团失效,拼团失败', function () {
        before('使拼团失效', async () => {
            await ssReq.ssSellerLogin();
            await groupBuyAct.marketGroupBuyInvalid({ status: 'fail' });
        });
        it('卖家查询拼团活动列表--已结束', async function () {
            const actInfo = await groupBuyAct.marketGroupBuyList();
            const exp = _.cloneDeep(groupBuyAct.getGroupBuyExp());
            exp.spus[0].stockNum -= 1;
            common.isApproximatelyEqualAssert(exp, actInfo);
        });
        it('卖家查询拼团活动数据', async function () {
            let statistics = await groupBuyAct.marketGroupBuyStatistics();
            console.log(statistics.data);
            const statisticsExp = [...groupBuyAct.groupBuyStatisticsMap.values()][0];
            console.log(`\nstatisticsExp = ${statisticsExp}`);
            common.isApproximatelyEqualAssert(statisticsExp, statistics.data);
        });
        it('卖家查询小团列表', async function () {
            let qlRes = await groupBuyAct.marketGroupBuyOrderList({ flag: 2 });
            console.log(`卖家查询拼团列表 = ${JSON.stringify(qlRes)}`);
            expect(qlRes.data.count, `拼团失败,小团失败数量没有加上去`).to.be.equal(2);
        });
        it('卖家查询账户余额-offline', async function () {
            this.retries(3)
            await common.delay(5000);
            let homePageMoney = await ss.fin.getMoneyHomePage().then(res => res.result.data);
            console.log(homePageMoney);
            let exp = _.cloneDeep(homePageMoney1);
            // exp.totalMoney = common.sub(exp.totalMoney, 0.03);
            // exp.settlementMoney = common.sub(exp.settlementMoney, 0.03);
            common.isApproximatelyEqualAssert(exp, homePageMoney, ['totalBalMoney']); //totalBalMoney 中台自测使用
        });
        it('卖家查询销售单列表-offline', async function () {
            this.retries(3);
            await common.delay(5000);
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
            console.log(`selesListRes = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill).to.be.include({ flag: 0 });
        });
        it('卖家查询销售单详情-offline', async function () {
            this.retries(3);
            await common.delay(5000);
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id });//
            console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill).to.be.include({ flag: 0 });
        });
        it('卖家查询商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: orderRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            // styleInfoBefore.skus[0].occupyNum -= 1; // 支付完成解锁库存
            // styleInfoBefore.skus[0].ssNum += 1;
            // styleInfoBefore.spu.salesNum += 1;
            // styleInfoBefore.spu.stockNum += 1;
            // styleInfoBefore.spu.salesMoney -= 9.99;  // classId 怎么没有了
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy', 'priClassId', 'classId']);
        });
        it('买家查询拼团中订单列表', async function () {
            await ssReq.userLoginWithWx();
            let qlRes = await groupBuyAct.findGroupBuyOrderRunningList({ sellerId: sellerInfo.tenantId });
            console.log(`\n买家查询拼团中订单列表=${JSON.stringify(qlRes)}`);
            expect(qlRes.data.groupBuyOrderResultList.length, `自动拼团成功后，还能在拼团订单列表中查询到`).to.be.equal(0);
        });
    });
});