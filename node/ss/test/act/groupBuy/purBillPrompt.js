const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupBuyManager = require('../../../help/act/groupBuy');
const billManage = require('../../../help/billManage');
const ssAccount = require('../../../data/ssAccount');

describe('买家活动订单未支付提示信息-offline', function () {
    this.timeout(30000);
    let dres, sellerInfo, actJson, clientInfoA, clientInfoB, styleInfoBefore, orderRes, mobile = common.getRandomMobile(), orderRunListRes, orderRunListResExpA, homePageMoney1;
    console.log(mobile);

    const groupBuyAct = groupBuyManager.setupGroupBuy();
    before('新增拼团活动', async () => {
        // 限购 2人团 9.99元 凑团
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(sellerInfo);
        //删除拼图活动
        for (let flag of [-2, 1, 2, 3]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }

        // 新增活动商品
        await dresManage.prePrepare();
        const json = basicJson.styleJson();
        spuId = await sp.spdresb.saveFull(json).then(res => res.result.data.spuId);
        dres = await groupBuyAct.getDresList(1).then(res => res.find(val => val.id));
        actJson = await groupBuyManager.getGroupBuyJson(dres);
        actJson.limitedNum = 1;
        actJson.autoClusterFlag = 0;
        await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
        homePageMoney1 = await ss.fin.getMoneyHomePage().then(res => res.result.data);
        console.log(homePageMoney1);
    });
    it('卖家查询拼团活动列表', async function () {
        let actList = await groupBuyAct.marketGroupBuyList();
        // console.log(actList);
        let exp = groupBuyAct.getGroupBuyExp();
        common.isApproximatelyEqualAssert(exp, actList);
    });
    after('删除拼团活动', async () => {
        await ssReq.ssSellerLogin();
        for (let flag of [1, 2, 3, -2]) {
            const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
            // console.log(idList);
            for (let id of idList) {
                await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
            }
        }
    });
    describe('买家开团-下单', function () {
        let orderRunListRes;
        before('买家查询团购商品信息', async () => {
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            console.log([...groupBuyAct.spuMap.keys()]);
            // 生成下单参数
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: [...groupBuyAct.spuMap.keys()][0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.groupBuyOrder = { actId: groupBuyAct.id, sellerId: sellerInfo.tenantId };
            console.log(`\npurJson=${JSON.stringify(purJson)}`);
            //卖家商品详情
            // 下单
            await ssReq.userLoginWithWx();
            orderRes = await groupBuyAct.groupBuyOrderItem(purJson);
            groupBuyAct.updateGroupBuyStatistics1(orderRes);
            console.log((`\norderRes=${JSON.stringify(orderRes)}`));
            await ssReq.ssSellerLogin();
        });
        it('买家活动订单未支付提示', async function () {
            await ssReq.userLoginWithWx();
            let actBillIndex = await ss.sppur.findUnpaidActBillIndex({ sellerId: sellerInfo.tenantId });
            console.log(actBillIndex);
            const exp = { billId: orderRes.result.data.rows[0].billId, alertMsg: `您有拼团订单还未支付，请尽快支付享受优惠` };
            common.isApproximatelyEqualAssert(exp, actBillIndex.result.data);
        });
    });
    describe('使拼团失效,拼团失败', function () {
        before('使拼团失效', async () => {
            await ssReq.ssSellerLogin();
            await groupBuyAct.marketGroupBuyInvalid({ status: 'fail' });
        });
        it('买家活动订单未支付提示', async function () {
            await ssReq.userLoginWithWx();
            let actBillIndex = await ss.sppur.findUnpaidActBillIndex({ sellerId: sellerInfo.tenantId });
            console.log(actBillIndex);
            expect(actBillIndex.result.data, `活动失效后,没有订单未支付提示`).to.be.undefined;
        });
    });
});