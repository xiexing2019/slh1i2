const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupBuyManager = require('../../../help/act/groupBuy');
const moment = require('moment');


describe('拼团活动-offline', function () {
    this.timeout(30000);
    let spuId, dres, sellerInfo;
    const groupBuyAct = groupBuyManager.setupGroupBuy();

    describe('卖家端', function () {
        before(async () => {
            await ssReq.ssSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
            // 新增活动商品
            await dresManage.prePrepare();
            const json = basicJson.styleJson();
            spuId = await sp.spdresb.saveFull(json).then(res => res.result.data.spuId);
            dres = await groupBuyAct.getDresList(1).then(res => res.find(val => val.id));
            // console.log(`dres=${JSON.stringify(dres)}`);
        });
        after('删除拼团', async function () {
            await ssReq.ssSellerLogin();
            for (let flag of [1, 2, 3, -2]) {
                const idList = await ss.act.groupBuy.marketGroupBuyList({ flag: flag, pageSize: 0 }).then(res => res.result.data.rows.map(data => data.id));
                // console.log(idList);
                for (let id of idList) {
                    await ss.act.groupBuy.marketGroupBuyInvalid({ actId: id });
                    await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: id });
                }
            }
        });
        describe('创建拼团活动', function () {
            before(async () => {
                const actJson = await groupBuyManager.getGroupBuyJson(dres);
                await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
            });
            it('查询拼团活动列表', async function () {
                const actInfo = await groupBuyAct.marketGroupBuyList();
                const exp = groupBuyAct.getGroupBuyExp();
                // console.log("拼团列表：", exp, actInfo);
                common.isApproximatelyEqualAssert(exp, actInfo);
            });
            it('查询拼团活动详情', async function () {
                const actFull = await groupBuyAct.marketGroupBuyDetail();
                const exp = groupBuyAct.getGroupBuyExp();
                // console.log("拼团详情：", exp, actFull);
                common.isApproximatelyEqualAssert(exp, actFull);
            });
            it('买家查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const spuId = [...groupBuyAct.spuMap.values()][0].spuId;
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    .then(res => res.result.data);
                // console.log(`dresDetail = ${JSON.stringify(dresDetail)}`);

                const exp = groupBuyAct.getDresActInfo({ spuId: spuId });
                common.isApproximatelyEqualAssert(exp, dresDetail.activity, ['invType']);
            });
            it('查询单个活动商品信息', async function () {
                await ssReq.ssSellerLogin();
                const spu = [...groupBuyAct.spuMap.values()][0];
                const dresDetail = await groupBuyAct.findFullMarketSpu({ spuId: spu.spuId });
                // console.log(spu, dresDetail.spu);
                common.isApproximatelyEqualAssert(spu, dresDetail.spu, ['id']);
            });
            it('拼团数据展示', async function () {
                await groupBuyAct.marketGroupBuyStatistics();
            });
        });
        describe('修改拼团活动', function () {
            before(async () => {
                // dres修改为B商品
                await ssReq.ssSellerLogin();
                const updateActJson = await groupBuyManager.updateGroupBuyJson({ id: groupBuyAct.id }, dres);
                await groupBuyAct.addOrUpdateMarketGroupBuy(updateActJson, dres);
            });
            it('查询拼团活动列表', async function () {
                const actInfo = await groupBuyAct.marketGroupBuyList();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actInfo);
            });
            it('查询拼团活动详情', async function () {
                const actFull = await groupBuyAct.marketGroupBuyDetail();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actFull);
            });
            it('买家查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const spuId = [...groupBuyAct.spuMap.values()][0].spuId;
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    .then(res => res.result.data);
                const exp = groupBuyAct.getDresActInfo({ spuId: spuId });
                // console.log(exp, dresDetail.activity);
                common.isApproximatelyEqualAssert(exp, dresDetail.activity, ['invType']);
            });
            it('查询单个活动商品信息', async function () {
                await ssReq.ssSellerLogin();
                const spu = [...groupBuyAct.spuMap.values()][0];
                const dresDetail = await groupBuyAct.findFullMarketSpu({ spuId: spu.spuId });
                // console.log(spu, dresDetail.spu);
                common.isApproximatelyEqualAssert(spu, dresDetail.spu, ['id']);
            });
            // // 查询A商品 不该含有活动信息  getFullForBuyer
            // it('', async function () {

            // });
        });
        describe('未结束的拼团不能删除', function () {
            let res;
            before('删除拼团活动', async () => {
                res = await ss.act.groupBuy.deleteMarketGroupBuyById({ actId: groupBuyAct.id, check: false });
            });
            it('未结束的拼团不能删除', async function () {
                expect(res.result.msgId, `未结束的拼团活动被删除了`).to.be.equal('market_not_end_and_not_delete');
            })
            it('查询拼团活动列表', async function () {
                const actInfo = await groupBuyAct.marketGroupBuyList();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actInfo);
            });
            it('查询拼团活动详情', async function () {
                const actFull = await groupBuyAct.marketGroupBuyDetail();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actFull);
            });
        });
        describe('使拼团活动失效', function () {
            before(async () => {
                await groupBuyAct.marketGroupBuyInvalid();
            });
            it('查询拼团活动列表', async function () {
                const actInfo = await groupBuyAct.marketGroupBuyList();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actInfo);
            });
            it('查询拼团活动详情', async function () {
                const actFull = await groupBuyAct.marketGroupBuyDetail();
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actFull);
            });
        });
        describe('删除拼团活动', function () {
            before(async () => {
                await groupBuyAct.deleteMarketGroupBuyById();
            });
            it('查询拼团活动列表', async function () {
                const actInfo = await groupBuyAct.marketGroupBuyList();
                console.log(JSON.stringify(actInfo));
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actInfo);
            });
            it('查询拼团活动详情', async function () {
                const actFull = await groupBuyAct.marketGroupBuyDetail();
                console.log(JSON.stringify(actFull));
                const exp = groupBuyAct.getGroupBuyExp();
                common.isApproximatelyEqualAssert(exp, actFull);
            });
        });
        describe('创建预热活动', function () {
            let sellerInfo;
            const groupBuyAct = groupBuyManager.setupGroupBuy();
            before(async () => {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                const actJson = await groupBuyManager.getGroupBuyJson(dres);
                actJson.startDate = moment().add(1, 'h').format('YYYY-MM-DD HH:mm:ss')
                await groupBuyAct.addOrUpdateMarketGroupBuy(actJson, dres);
            });
            it('买家查询商品详情', async function () {
                let i = 1, dresDetail, exp;
                await ssReq.userLoginWithWx();
                const spuId = [...groupBuyAct.spuMap.values()][0].spuId;
                dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    .then(res => res.result.data);
                while (dresDetail.activity.actFlag != 2) {
                    if (i > 10) throw new Error(`判断活动状态已超过10次`);
                    const spuId = [...groupBuyAct.spuMap.values()][0].spuId;
                    dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                        .then(res => res.result.data);
                    await common.delay(500);
                    // console.log(`dresDetail = ${JSON.stringify(dresDetail)}`);
                    i++;
                };
                exp = groupBuyAct.getDresActInfo({ spuId: spuId });
                exp.actFlag = 2;
                expect(dresDetail).to.have.own.property('activity');
                common.isApproximatelyEqualAssert(exp, dresDetail.activity, ['invType']);
            });
            describe('修改活动开始时间和活动价格', function () {
                before(async () => {
                    await ssReq.ssSellerLogin();
                    const updateActJson = await groupBuyManager.updateGroupBuyJson({ id: groupBuyAct.id }, dres);
                    updateActJson.activityPrice = 500;
                    await groupBuyAct.addOrUpdateMarketGroupBuy(updateActJson, dres);
                });
                it('卖家查询拼团活动列表', async function () {
                    const actInfo = await groupBuyAct.marketGroupBuyList();
                    console.log(JSON.stringify(actInfo));
                    const exp = groupBuyAct.getGroupBuyExp();
                    common.isApproximatelyEqualAssert(exp, actInfo);
                });
            });
        });
    });

});