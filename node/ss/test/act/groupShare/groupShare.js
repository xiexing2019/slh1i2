const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupShareManager = require('../../../help/act/groupShare');
const moment = require('moment');
const ssAccount = require('../../../data/ssAccount');
const slh1 = require('../../../../reqHandler/slh1');

describe('群接龙', function () {
    this.timeout(TESTCASE.timeout)
    let sellInfo, dresList, dresFull, saasDresFull;
    const groupShareAct = groupShareManager.setupGroupShare();
    before('卖家登录,新增一个款号库存', async () => {
        await groupShareManager.addNewStyle();
        await common.delay(2000);
        // 卖家小程序登录后切换到卖家身份
        await ssReq.wxAppSwitchSeller({ mobile: ssAccount.saas.mobile, tenantId: ssAccount.saas.tenantId });
        dresList = await groupShareAct.getDresList(1);
        dresFull = await sp.spdresb.getFullById({ id: dresList[0].id }).then(res => res.result.data);
        console.log(`商品详情=${JSON.stringify(dresFull)}`);
        await groupShareManager.saasLogin();
        saasDresFull = await slh1.slh1MetManager.queryGoodFull({ pk: dresList[0].spu.slhId });
        console.log(`商品详情=${JSON.stringify(dresFull)}`);
        await ssReq.wxAppSwitchSeller({ mobile: ssAccount.saas.mobile, tenantId: ssAccount.saas.tenantId });
    });
    it.skip('群接龙活动预览', async function () {
        // await ssReq.wxAppSwitchSeller({ mobile: ssAccount.saas.mobile, tenantId: ssAccount.saas.tenantId });
        const actPrepareInfo = await groupShareAct.getActPrepareInfo({ slhSpuIds: `${dresList[0].spu.slhId}` });
        const actPrepareInfoExp = groupShareAct.getActPrepareInfoExp(dresFull, saasDresFull);
        console.log('活动预览', JSON.stringify(actPrepareInfo));
        // console.log('活动预览Exp', JSON.stringify(actPrepareInfoExp));
        common.isApproximatelyEqualAssert(actPrepareInfoExp, actPrepareInfo, ['weight']); // 添加已经开过群接龙活动的商品，名称为上次活动名称
    });
    it('新增群接龙活动', async function () {
        const actJson = groupShareManager.getGroupShareJson(dresList);
        await groupShareAct.saveGroupShare(actJson, dresList);
    });
    it('查询群接龙活动列表', async function () {
        const actList = await groupShareAct.findGroupShareList();
        const actListExp = groupShareAct.getGroupShareListExp();
        // console.log(`群接龙活动列表= ${JSON.stringify(actList)}`);
        // console.log(`群接龙活动列表Exp= ${JSON.stringify(actListExp)}`);
        common.isApproximatelyEqualAssert(actListExp, actList, ['weight']);
    });
    it('查询群接龙活动详情', async function () {
        let actDetail = await groupShareAct.getGroupShareFullInfo();
        const actDetailExp = groupShareAct.getGroupShareListExp();
        // console.log(`群接龙活动详情= ${JSON.stringify(actDetail)}`);
        // console.log(`群接龙活动详情Exp= ${JSON.stringify(actDetailExp)}`);
        common.isApproximatelyEqualAssert(actDetailExp, actDetail, ['weight']);
        common.isApproximatelyArrayAssert(actDetailExp.actImgs, JSON.parse(actDetail.actImgs), ['weight']);
    });
});
