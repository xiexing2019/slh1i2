const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const basicJson = require('../../../help/basicJson');
const dresManage = require('../../../help/dresManage');
const groupShareManager = require('../../../help/act/groupShare');
const billManage = require('../../../help/billManage');
const ssAccount = require('../../../data/ssAccount');


describe('群接龙下单', function () {
    this.timeout(TESTCASE.timeout);
    let dresList, sellerInfo, orderRes, payRes, purJson, salesListRes;
    const groupShareAct = groupShareManager.setupGroupShare();
    before('买家创建采购单', async () => {
        await groupShareManager.addNewStyle();
        await common.delay(1000);

        await ssReq.wxAppSwitchSeller({ mobile: ssAccount.saas.mobile, tenantId: ssAccount.saas.tenantId });
        sellerInfo = _.cloneDeep(LOGINDATA);
        dresList = await groupShareAct.getDresList(1);
        const actJson = groupShareManager.getGroupShareJson(dresList);
        await groupShareAct.saveGroupShare(actJson, dresList);
        // 生成下单参数
        purJson = await groupShareAct.getPurJson(sellerInfo);
        // 下单
        await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId, openId: ssAccount.saas.openId, });
        orderRes = await billManage.createPurBill(purJson);
        console.log((`\norderRes=${JSON.stringify(orderRes)}`));
        groupShareAct.updateSku(purJson);
    });
    it('买家支付采购单', async function () {
        await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId, openId: ssAccount.saas.openId, });
        payRes = await sp.spTrade.createPay({
            payType: 5,
            payMethod: 2,
            payerOpenId: LOGINDATA.wxOpenId,
            orderIds: [orderRes.result.data.rows[0].billId],
            payMoney: orderRes.params.jsonParam.orders[0].main.money,
            channelCodeSuffix: 'groupShare',
        });
        // console.log(`payRes=${JSON.stringify(payRes)}`);
        const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, merchantEnable: 0 });
        // console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
    });
    it('查询群接龙活动详情', async function () {
        await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId, openId: ssAccount.saas.openId, });
        let actDetail = await groupShareAct.getGroupShareFullInfo({ sellerUnitId: sellerInfo.unitId });
        const actDetailExp = groupShareAct.getGroupShareListExp(purJson);
        common.isApproximatelyEqualAssert(actDetailExp, actDetail, ['weight']);
    });
    it('卖家查询订单列表', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        salesListRes = await sp.spTrade.salesFindBills({ searchToken: orderRes.result.data.rows[0].billNo }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == orderRes.result.data.rows[0].billNo));
        console.log(JSON.stringify(salesListRes));
        common.isApproximatelyEqualAssert(purJson.orders[0].details[0], salesListRes.skus[0]);
        common.isApproximatelyEqualAssert({ actType: 6, actTypeName: '群接龙' }, salesListRes.marketDetail);
    });
    it('卖家查询订单详情', async function () {
        const salesDetailRes = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id }).then(res => res.result.data);
        console.log(JSON.stringify(salesDetailRes));
        common.isApproximatelyEqualAssert(purJson.orders[0].details[0], salesDetailRes.skus[0]);
        common.isApproximatelyEqualAssert({ actType: 6, actTypeName: '群接龙' }, salesDetailRes.marketDetail);
    });
});