const common = require('../../../../lib/common');
const secKillHelp = require('../../../help/act/secKill');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');

describe.skip('秒杀活动', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [];
    const mySecKill = secKillHelp.setupSecKill();
    // const dres = dresManage.setupDres({ type: 'app' });
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // console.log(LOGINDATA);
        // await dresManage.prePrepare();
        // const json = basicJson.styleJsonByApp();
        // await dres.saveDresByApp(json);
        secKillHelp.deleteOrDisableAllAct();
        dresList = await mySecKill.getDresList(6);
        // console.log(`\n dresList.length=${JSON.stringify(dresList.length)}`);
    });
    after(async function () {
        await ssReq.ssSellerLogin();
        await ss.act.timeLimit.deleteRule({ id: mySecKill.id });
    });
    describe('活动', async function () {
        let secKillJson;
        before(async function () {
            // dresList.push(await dres.getFullById());
            secKillJson = secKillHelp.getSecKillJson(dresList.slice(0, 2), {
                startDate: moment().add(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                coupFlag: 1,
                preHeatFlag: 1,
                execVal: { execType: 1, execNum: 2, execExtraKind: 8 },
                limitVal: { limitType: 1, limitNum: 10 },
                invType: 1,
                num: 100,
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList.slice(0, 2));
            console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        it('卖家秒杀列表', async function () {
            const secKillInfo = await mySecKill.getSecKillList();
            console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
            common.isApproximatelyEqualAssert(mySecKill.getSecKill(), JSON.parse(JSON.stringify(secKillInfo)), ['spuList']);
        });
        it('秒杀详情', async function () {
            const secKillInfo = await mySecKill.getById();
            // console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
            console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
            common.isApproximatelyEqualAssert(mySecKill.getSecKill(), secKillInfo);
        });
        describe('商品', async function () {
            it('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                for (const spu of mySecKill.getSecKill().spuList) {
                    const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spu.spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                    console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                    const dresActInfo = mySecKill.getDresActInfo({ spuId: spu.spuId });
                    console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                    expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer.activity)}`).not.to.be.undefined;
                    common.isApproximatelyEqualAssert(dresActInfo, getFullForBuyer.activity);
                };
            });
            it('查看单个活动商品信息', async function () {
                await ssReq.ssSellerLogin();
                for (const spu of mySecKill.getSecKill().spuList) {
                    const spuFull = await mySecKill.findFullMarketSpu({ spuId: spu.spuId });
                    // console.log(`\n mySecKill.getSecKill().spuList[0]=${JSON.stringify(spu)}`);
                    console.log(`\n spuFull=${JSON.stringify(spuFull)}`);
                    common.isApproximatelyEqualAssert(spu, spuFull, ['id', 'flag']);
                    spu.skuInvList.forEach(skuInv => {
                        const sku = spuFull.invList.find(obj => obj.skuId = skuInv.skuId);
                        expect(sku, 'sku不存在').not.to.be.undefined;
                        expect(skuInv.invNum, 'sku的自定义库存数量与预期不一致').to.be.equal(sku.invNum);
                    });
                };
            });
            describe('批量', async function () {
                let batchSpuIdList = [];
                before(async function () {
                    await ssReq.ssSellerLogin();
                    batchSpuIdList = dresList.slice(2, 4);
                });
                describe('批量添加商品', async function () {
                    before('批量添加商品', async function () {
                        await ssReq.ssSellerLogin();
                        await mySecKill.batchAddSpu({ dresList: batchSpuIdList });
                    });
                    it('卖家秒杀列表', async function () {
                        for (const spuId of batchSpuIdList.map(spu => spu.id)) {
                            const secKillInfo = await mySecKill.getSecKillList();
                            // console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
                            expect(1, '商品详情的活动信息activity不存在').to.equal(secKillInfo.spuList.find(obj => obj.spuId == spuId).flag);
                        };
                    });
                    it('买家获取商品信息', async function () {
                        await ssReq.userLoginWithWx();
                        for (const spuId of batchSpuIdList.map(spu => spu.id)) {
                            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                            // console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                            expect(getFullForBuyer.activity, '商品详情的活动信息activity不存在').not.to.be.undefined;
                            const dresActInfo = mySecKill.getDresActInfo({ spuId: spuId });
                            // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                            common.isApproximatelyEqualAssert(dresActInfo, getFullForBuyer.activity);
                        };
                    });
                });
                describe('批量删除商品', async function () {
                    before('批量删除商品', async function () {
                        await ssReq.ssSellerLogin();
                        await mySecKill.batchDeleteSpu({ spuIds: batchSpuIdList });
                    });
                    it('卖家秒杀列表', async function () {
                        for (const spuId of batchSpuIdList.map(spu => spu.id)) {
                            const secKillInfo = await mySecKill.getSecKillList();
                            // console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
                            expect(secKillInfo.spuList.find(obj => obj.spuId == spuId), `卖家秒杀列表存在已删除的商品${spuId}`).to.be.undefined;
                        };
                    });
                    it('买家获取商品信息', async function () {
                        await ssReq.userLoginWithWx();
                        for (const spuId of batchSpuIdList.map(spu => spu.id)) {
                            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                            // console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer.activity)}`);
                            expect(getFullForBuyer.activity, '商品详情的活动信息activity居然存在').to.be.empty;
                        };
                    });
                });
            });
        });
        describe('修改活动', async function () {
            before(async function () {
                await ssReq.ssSellerLogin();
                secKillJson = secKillHelp.getSecKillJson(dresList.slice(4, 6), {
                    startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                    execVal: { execType: 5, execNum: 1, execExtraKind: 8 },
                });
                secKillJson.id = mySecKill.id;
                console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
                await mySecKill.saveFullSecKill(secKillJson, dresList.slice(4, 6));
                console.log(mySecKill);
                console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
            });
            it('卖家秒杀列表', async function () {
                const secKillInfo = await mySecKill.getSecKillList();
                // console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
                // console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
                common.isApproximatelyEqualAssert(mySecKill.getSecKill(), secKillInfo, ['spuList']);
            });
            it('秒杀详情', async function () {
                const secKillInfo = await mySecKill.getById();
                // console.log(`\n secKillInfo=${JSON.stringify(secKillInfo)}`);
                common.isApproximatelyEqualAssert(mySecKill.getSecKill(), secKillInfo);
            });
            describe('商品', async function () {
                it('买家获取商品信息', async function () {
                    await ssReq.userLoginWithWx();
                    for (const spu of mySecKill.getSecKill().spuList) {
                        const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spu.spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                        console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                        const dresActInfo = mySecKill.getDresActInfo({ spuId: spu.spuId });
                        console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                        expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer.activity)}`).not.to.be.undefined;
                        common.isApproximatelyEqualAssert(dresActInfo, getFullForBuyer.activity);
                    };
                });
                it('查看单个活动商品信息', async function () {
                    await ssReq.ssSellerLogin();
                    console.log(mySecKill);
                    for (const spu of mySecKill.getSecKill().spuList) {
                        const spuFull = await mySecKill.findFullMarketSpu({ spuId: spu.spuId });
                        // console.log(`\n spuFull=${JSON.stringify(spuFull)}`);
                        common.isApproximatelyEqualAssert(spu, spuFull, ['id', 'flag']);
                        spu.skuInvList.forEach(skuInv => {
                            const sku = spuFull.invList.find(obj => obj.skuId = skuInv.skuId);
                            expect(sku, 'sku不存在').not.to.be.undefined;
                            console.log(skuInv.invNum, sku.invNum);
                            expect(skuInv.invNum, 'sku的自定义库存数量与预期不一致').to.be.equal(sku.invNum);
                        });
                    };
                });
            });
        });
    });
    describe('新建活动选择重复商品', async function () {
        let secKillJson;
        before(async function () {
            // dresList.push(await dres.getFullById());
            // console.log(`\n dresList.length=${JSON.stringify(dresList.length)}`);
            secKillJson = secKillHelp.getSecKillJson(dresList.slice(0, 2));
            // console.log(`\nsecKillJson=${JSON.stringify(secKillJson)}`);
        });
        it('新建活动', async function () {
            secKillJson.check = false;
            const saveFull = await mySecKill.saveFullSecKill(secKillJson, dresList.slice(0, 2));
            console.log(saveFull);
            expect(saveFull, `创建活动成功${saveFull}`).to.include({ msg: '该商品已参与活动' });
        });
    });
});