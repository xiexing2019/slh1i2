const common = require('../../../../lib/common');
const secKillHelp = require('../../../help/act/secKill');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');
const basicJson = require('../../../help/basicJson');
const billManage = require('../../../help/billManage');
const format = require('../../../../data/format');

describe.skip('秒杀下单', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [];
    // const dres = dresManage.setupDres({ type: 'app' });
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        secKillHelp.deleteOrDisableAllAct();
        const mySecKill = secKillHelp.setupSecKill();
        dresList = await mySecKill.getDresList(2);
    });
    describe('下单,活动:进行中', async function () {
        let secKillJson, purRes;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动:进行中', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单', async function () {
            let styleInfoBefore, salesListRes;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                // const areaJson = await basicJson.getAreaJson();
                // const json = await basicJson.addAddrJson(areaJson);
                // json.recInfo.isDefault = 1;
                // const val = await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);
                // await sp.spugr.changeMobile({ check: false, mobile: '12209898909', tentantId: sellerInfo.tenantId });
                const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);

                await ssReq.ssSellerLogin();
                //卖家商品详情
                styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
                console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
                await ssReq.userLoginWithWx();
                purRes = await billManage.createPurBill(purJson);
                await ssReq.ssSellerLogin();
            });
            it('查询销售单列表', async function () {
                salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
                expect(salesListRes.bill.payFlag).to.equal(0);
            });
            it('查询单据详情', async function () {
                //销售单id 非采购单
                const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
                expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
            });
            it('卖家商品详情', async function () {
                let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
                console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                // , 'occupyNum', 'stockNum'
                styleInfoBefore.skus[0].occupyNum += 1;
                styleInfoBefore.spu.stockNum -= 1;
                common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
            });
            it('买家活动订单未支付提示', async function () {
                await ssReq.userLoginWithWx();
                let actBillIndex = await ss.sppur.findUnpaidActBillIndex({ sellerId: sellerInfo.tenantId });
                console.log(actBillIndex);
                const exp = { billId: orderRes.result.data.rows[0].billId, alertMsg: `您有秒杀订单还未支付，请尽快支付享受优惠` };
                common.isApproximatelyEqualAssert(exp, actBillIndex.result.data);
            });
        });
    });
    describe('下单,活动:未开始', async function () {
        let secKillJson;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动:未开始', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().add(2, 'hours').format('YYYY-MM-DD HH:mm:ss'),
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单', async function () {
            let styleInfoBefore, salesListRes, purRes;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                purRes = await sp.spTrade.savePurBill(purJson);
            });
            it('判断订单状态', async function () {
                let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
                while (!checkPurBillFlag.failedMsg) {
                    if (i > 10) throw new Error(`判断订单状态已超过10次`);
                    checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
                    console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
                    i++;
                };
                expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: '商品价格已变更,请刷新后重试\n' });
            });
        });
    });
    describe('下单,活动:限购', async function () {
        let secKillJson;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                limitVal: { limitType: 1, limitNum: 2 } //每人限购两个
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单0', async function () {
            let styleInfoBefore, salesListRes, getFullForBuyer;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);

                purRes = await billManage.createPurBill(purJson);
                payRes = await sp.spTrade.createPay({
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [purRes.result.data.rows[0].billId],
                    payMoney: purRes.params.jsonParam.orders[0].main.money
                });
            });
            it('买家查询商品详情', async function () {
                await common.delay(5000);
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\n buyNum=${JSON.stringify(dresFull.activity)}`);
            });
            describe('下单1', async function () {
                let salesListRes, purRes;
                before('买家获取商品信息', async function () {
                    await ssReq.userLoginWithWx();
                    await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                    const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 2, num: 1 });
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purRes = await sp.spTrade.savePurBill(purJson);
                });
                it('判断订单状态', async function () {
                    let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
                    while (!checkPurBillFlag.failedMsg) {
                        if (i > 10) throw new Error(`判断订单状态已超过10次`);
                        checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
                        console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
                        i++;
                    };
                    expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: '购买数量超出了最大限购数\n' });
                });
                it('买家查询商品详情', async function () {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                    console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                });
                describe('下单2', async function () {
                    let salesListRes, purRes;
                    before('买家获取商品信息', async function () {
                        await ssReq.userLoginWithWx();
                        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                        const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purRes = await billManage.createPurBill(purJson);
                    });
                    it('买家查询商品详情', async function () {
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                        console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                    });
                });
            });
        });
    });
    describe.skip('下单,活动:自定义库存', async function () {
        let secKillJson;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                invType: 1,
                num: 2
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单0', async function () {
            let styleInfoBefore, salesListRes, getFullForBuyer;
            before('买家获取商品信息', async function () {
                await ssReq.userLoginWithWx();
                getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                // console.log(`\npurJson=${JSON.stringify(purJson)}`);

                purRes = await billManage.createPurBill(purJson);
                console.log(`\npurJson=${JSON.stringify(purRes)}`);
                payRes = await sp.spTrade.createPay({
                    payType: 5,
                    payMethod: 2,
                    payerOpenId: LOGINDATA.wxOpenId,
                    orderIds: [purRes.result.data.rows[0].billId],
                    payMoney: purRes.params.jsonParam.orders[0].main.money
                });
            });
            it('买家查询商品详情', async function () {
                await common.delay(5000);
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\n buyNum=${JSON.stringify(dresFull)}`);
                purRes.params.jsonParam.orders[0].details.forEach(detail => {
                    const skuBefore = getFullForBuyer.skus.find(obj => obj.id == detail.skuId);
                    skuBefore.occupyNum += detail.num;
                    skuBefore.num -= detail.num;
                    const sku = dresFull.skus.find(obj => obj.id == detail.skuId);
                    common.isApproximatelyEqualAssert(skuBefore, sku, ['pubPrice']);

                    const skuInvBefore = getFullForBuyer.activity.skuInvList.find(obj => obj.skuId == detail.skuId);
                    skuInvBefore.lockNum += detail.num;
                    skuInvBefore.invNum -= detail.num;
                    const skuInv = dresFull.activity.skuInvList.find(obj => obj.skuId == detail.skuId);
                    common.isApproximatelyEqualAssert(skuInvBefore, skuInv);
                });
            });
            describe.skip('下单1', async function () {
                let salesListRes, purRes;
                before('买家获取商品信息', async function () {
                    await ssReq.userLoginWithWx();
                    await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                    const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 2, num: 1 });
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                    // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                    purRes = await sp.spTrade.savePurBill(purJson);
                });
                it('判断订单状态', async function () {
                    let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
                    while (!checkPurBillFlag.failedMsg) {
                        if (i > 10) throw new Error(`判断订单状态已超过10次`);
                        checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
                        console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
                        i++;
                    };
                    expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: '您所购买的商品库存不足\n' });
                });
                it('买家查询商品详情', async function () {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                    console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                });
                describe('下单2', async function () {
                    let salesListRes, purRes;
                    before('买家获取商品信息', async function () {
                        await ssReq.userLoginWithWx();
                        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                        const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                        // console.log(`\npurJson=${JSON.stringify(purJson)}`);
                        purRes = await billManage.createPurBill(purJson);
                    });
                    it('买家查询商品详情', async function () {
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                        console.log(`\n buyNum=${JSON.stringify(dresFull.activity.buyNum)}`);
                    });
                });
            });
        });
    });
    describe('下单,活动:叠加优惠券', async function () {
        let secKillJson, purRes;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动:进行中', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                coupFlag: 1,
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
                limitVal: { limitType: 1, limitNum: 2 },
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单,活动:叠加优惠券', async function () {
            let getFullForBuyer, purJson, couponId;
            before('买家获取商品信息', async function () {
                await ssReq.ssSellerLogin();
                couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
                // 领取优惠券
                await ssReq.userLoginWithWx();
                await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
                getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: getFullForBuyer.activity.afterDiscountPrice, applicableSpus: [{ spuId: getFullForBuyer.spu.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice }] });
                console.log(`billInfo=${JSON.stringify(billInfo)}`);
                const couponInfo = await ss.sscoupb.findUseableCouponsByShop(billInfo).then(res => res.result.data.rows.find(obj => obj.couponId == couponId));
                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1, couponInfo: couponInfo });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                console.log(`\npurJson=${JSON.stringify(purJson)}`);
            });
            it('商品可用优惠券列表', async function () {
                const findValidCoupons = await ss.sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getFullForBuyer.id, actId: mySecKill.id });
                console.log(`\nfindValidCoupons=${JSON.stringify(findValidCoupons)}`);
                expect(findValidCoupons.result.data.rows, `商品可用优惠券列表不为空`).not.to.be.empty;
            });
            it('按门店获取订单最大优惠', async function () {
                const findFavorableCouponByShop = await ss.sscoupb.findFavorableCouponByShop({ jsonParam: [{ sellerTenantId: sellerInfo.tenantId, totalSum: purJson.orders[0].main.totalMoney, applicableSpus: [{ spuId: getFullForBuyer.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice, actId: mySecKill.id }] }] });
                console.log(`\nfindFavorableCouponByShop=${JSON.stringify(findFavorableCouponByShop)}`);
                expect(findFavorableCouponByShop.result.data[sellerInfo.tenantId].couponId, `按门店获取订单最大优惠不为空`).not.to.equal(0);
            });
            it('可用优惠券列表', async function () {
                const findUseableCouponsByShop = await ss.sscoupb.findUseableCouponsByShop({ sellerTenantId: sellerInfo.tenantId, totalSum: purJson.orders[0].main.totalMoney, applicableSpus: [{ spuId: getFullForBuyer.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice, actId: mySecKill.id }] })
                console.log(`\nfindUseableCouponsByShop=${JSON.stringify(findUseableCouponsByShop)}`);
                expect(findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId != 0), `可用优惠券列表不为空`).not.to.be.undefined;
            });
            describe('下单', async function () {
                let styleInfoBefore, salesListRes;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    //卖家商品详情
                    styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
                    console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
                    await ssReq.userLoginWithWx();
                    purRes = await billManage.createPurBill(purJson);
                    await ssReq.ssSellerLogin();
                });
                it('查询销售单列表', async function () {
                    salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
                    expect(salesListRes.bill.payFlag).to.equal(0);
                });
                it('查询单据详情', async function () {
                    //销售单id 非采购单
                    const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                    // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
                    expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
                });
                it('卖家商品详情', async function () {
                    let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                    // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
                    // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                    styleInfoBefore.skus[0].occupyNum += 1;
                    styleInfoBefore.spu.stockNum -= 1;
                    common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
                });
            });
        });
    });
    describe('下单,活动:不叠加优惠券', async function () {
        let secKillJson, purRes;
        const mySecKill = secKillHelp.setupSecKill();
        before('创建活动:进行中', async function () {
            await common.delay(1000);
            await ssReq.ssSellerLogin();
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                coupFlag: 0,
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
                limitVal: { limitType: 1, limitNum: 2 },
            });
            console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
            secKillHelp.deleteOrDisableAllAct();
        });
        describe('下单,活动:叠加优惠券', async function () {
            let getFullForBuyer, purJson, couponId;
            before('买家获取商品信息', async function () {
                await ssReq.ssSellerLogin();
                couponId = await ss.sscoupb.createCoupon(await ss.sscoupb.setCouponData({ cardType: 0 })).then(res => res.result.data.val);
                // 领取优惠券
                await ssReq.userLoginWithWx();
                await ss.sscoupb.receiveCoupon({ tenantId: sellerInfo.tenantId, coupons: [{ couponId: couponId, receiveChannelType: 1 }] });
                getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: mySecKill.getSecKill().spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
                expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

                const billInfo = format.packJsonParam({ sellerTenantId: sellerInfo.tenantId, totalSum: getFullForBuyer.activity.afterDiscountPrice, applicableSpus: [{ spuId: getFullForBuyer.spu.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice }] });
                console.log(`billInfo=${JSON.stringify(billInfo)}`);
                const couponInfo = await ss.sscoupb.findUseableCouponsByShop(billInfo).then(res => res.result.data.rows.find(obj => obj.couponId == couponId));
                await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
                purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1, couponInfo: couponInfo });
                purJson.orders[0].main.sellerId = sellerInfo.tenantId;
                console.log(`\npurJson=${JSON.stringify(purJson)}`);
            });
            it('商品可用优惠券列表', async function () {
                const findValidCoupons = await ss.sscoupb.findValidCoupons({ tenantId: sellerInfo.tenantId, spuId: getFullForBuyer.id, actId: mySecKill.id });
                console.log(`\nfindValidCoupons=${JSON.stringify(findValidCoupons)}`);
                expect(findValidCoupons.result.data.rows, `商品可用优惠券列表不为空`).to.be.empty;
            });
            it('按门店获取订单最大优惠', async function () {
                const findFavorableCouponByShop = await ss.sscoupb.findFavorableCouponByShop({ jsonParam: [{ sellerTenantId: sellerInfo.tenantId, totalSum: purJson.orders[0].main.totalMoney, applicableSpus: [{ spuId: getFullForBuyer.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice, actId: mySecKill.id }] }] });
                console.log(`\nfindFavorableCouponByShop=${JSON.stringify(findFavorableCouponByShop)}`);
                expect(findFavorableCouponByShop.result.data[sellerInfo.tenantId].couponId, `按门店获取订单最大优惠不为空`).to.equal(0);
            });
            it('可用优惠券列表', async function () {
                const findUseableCouponsByShop = await ss.sscoupb.findUseableCouponsByShop({ sellerTenantId: sellerInfo.tenantId, totalSum: purJson.orders[0].main.totalMoney, applicableSpus: [{ spuId: getFullForBuyer.id, spuPrice: getFullForBuyer.activity.afterDiscountPrice, actId: mySecKill.id }] })
                console.log(`\nfindUseableCouponsByShop=${JSON.stringify(findUseableCouponsByShop)}`);
                expect(findUseableCouponsByShop.result.data.rows.find(obj => obj.couponId != 0), `可用优惠券列表不为空`).to.be.undefined;
            });
            //使用优惠券下单还是可以下的
            describe.skip('下单', async function () {
                let styleInfoBefore, salesListRes;
                before(async function () {
                    await ssReq.ssSellerLogin();
                    //卖家商品详情
                    styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
                    console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
                    await ssReq.userLoginWithWx();
                    purRes = await billManage.createPurBill(purJson);
                    await ssReq.ssSellerLogin();
                });
                it('查询销售单列表', async function () {
                    salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
                    // console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
                    expect(salesListRes.bill.payFlag).to.equal(0);
                });
                it('查询单据详情', async function () {
                    //销售单id 非采购单
                    const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
                    // console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
                    expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
                });
                it('卖家商品详情', async function () {
                    let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
                    // let styleInfoAfter = await getDetailStyleInv(purRes.params.jsonParam.orders[0].details);
                    // console.log(`styleInfoAfter=${JSON.stringify(styleInfoAfter)}`);
                    styleInfoBefore.skus[0].occupyNum += 1;
                    styleInfoBefore.spu.stockNum -= 1;
                    common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
                });
            });
        });
    });
});