const common = require('../../../../lib/common');
const secKillHelp = require('../../../help/act/secKill');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');
const basicJson = require('../../../help/basicJson');
const billManage = require('../../../help/billManage');
const messageManage = require('../../../help/messageManage');

describe.skip('秒杀购物车', async function () {
    this.timeout(30000);
    const mySecKill = secKillHelp.setupSecKill();
    let sellerInfo, dresList = [], secKillJson, carts0;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        secKillHelp.deleteOrDisableAllAct()
        dresList = await mySecKill.getDresList(2);
        await ssReq.userLoginWithWx();
        // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
        await sp.spTrade.emptyCart();
    });
    describe('添加商品到购物车', async function () {
        before(async function () {
            await ssReq.userLoginWithWx();
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList[0].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(`\n dresFull=${JSON.stringify(dresFull)}`);
            expect(dresFull.result.data.activity, `商品详情的活动信息activity不存在${JSON.stringify(dresFull.result.data.activity)}`).to.be.empty;
            const cartJson = basicJson.cartJson2(dresFull);
            carts0 = cartJson.carts;
            const saveCartInBatchs = await sp.spTrade.saveCartInBatchs(cartJson);
            console.log(`\n saveCartInBatchs=${JSON.stringify(saveCartInBatchs)}`);
            Object.assign(carts0[0], { id: saveCartInBatchs.result.data.rows[0].id })
        });
        it('查询购物车', async function () {
            const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 });
            console.log(`\ncartList=${JSON.stringify(cartList)}`);
            // console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
            // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
            common.isApproximatelyEqualAssert(carts0[0], cartList.result.data.rows[0].carts[0]);
        });
    });
    describe('创建活动', async function () {
        before('创建活动:进行中', async function () {
            await ssReq.ssSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);
            dresList = await mySecKill.getDresList(2);
            // console.log(`\n dresList=${JSON.stringify(dresList[0])}`);
            secKillJson = secKillHelp.getSecKillJson(dresList, {
                startDate: moment().subtract(1, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                execVal: { execType: 1, execNum: 9, execExtraKind: 8 },
                limitVal: { limitType: 1, limitNum: 2 },
            });
            // console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
            await mySecKill.saveFullSecKill(secKillJson, dresList);
            // console.log(mySecKill);
            // console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
        });
        after(async function () {
            await ssReq.ssSellerLogin();
            await ss.act.timeLimit.deleteRule({ check: false, id: mySecKill.id });
        });
        it('查询已加入购物车的商品', async function () {
            await ssReq.userLoginWithWx();
            const cart = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows.find(obj => obj.id = carts0[0].id));
            console.log(`\ncartList=${JSON.stringify(cart)}`);
            // console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
            const dresActInfo = mySecKill.getDresActInfo({ spuId: cart.carts[0].spuId });
            // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
            common.isApproximatelyEqualAssert(Object.assign(carts0[0], { actInfo: dresActInfo }), cart.carts[0]);
        });
        describe('添加商品到购物车', async function () {
            let carts;
            before(async function () {
                await ssReq.userLoginWithWx();
                // console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
                const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dresList[1].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                // console.log(`\n dresFull=${JSON.stringify(dresFull)}`);
                expect(dresFull.result.data.activity, `商品详情的活动信息activity不存在${JSON.stringify(dresFull.result.data.activity)}`).to.not.be.empty;
                const cartJson = basicJson.cartJson2(dresFull);
                carts = cartJson.carts;
                const saveCartInBatchs = await sp.spTrade.saveCartInBatchs(cartJson);
                console.log(`\n saveCartInBatchs=${JSON.stringify(saveCartInBatchs)}`);
                Object.assign(carts[0], { id: saveCartInBatchs.result.data.rows[0].id })
            });
            it('查询购物车', async function () {
                const cart = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows.find(obj => obj.id = carts[0].id));
                console.log(`\ncartList=${JSON.stringify(cart)}`);
                // console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
                const dresActInfo = mySecKill.getDresActInfo({ spuId: cart.carts[0].spuId });
                // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                common.isApproximatelyEqualAssert(Object.assign(carts[0], { actInfo: dresActInfo }), cart.carts[0]);
            });
            describe('修改活动后查询购物车', async function () {
                before('修改活动', async function () {
                    await ssReq.ssSellerLogin();
                    secKillJson = secKillHelp.getSecKillJson([], {
                        startDate: moment().subtract(3, 'hours').format('YYYY-MM-DD HH:mm:ss'),
                        coupFlag: 1,
                        preHeatFlag: 1,
                        execVal: { execType: 1, execNum: 2, execExtraKind: 8 },
                        limitVal: { limitType: 1, limitNum: 10 },
                    });
                    secKillJson.id = mySecKill.id;
                    console.log(`secKillJson=${JSON.stringify(secKillJson)}`);
                    await mySecKill.saveFullSecKill(secKillJson, dresList.slice(4, 6));
                    console.log(mySecKill);
                    console.log(`\n mySecKill.getSecKill()=${JSON.stringify(mySecKill.getSecKill())}`);
                });
                it('查询购物车', async function () {
                    await ssReq.userLoginWithWx();
                    const cart = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows.find(obj => obj.id = carts[0].id));
                    console.log(`\ncartList=${JSON.stringify(cart)}`);
                    // console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
                    const dresActInfo = mySecKill.getDresActInfo({ spuId: cart.carts[0].spuId });
                    // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                    common.isApproximatelyEqualAssert(Object.assign(carts[0], { actInfo: dresActInfo }), cart.carts[0]);
                });
            });
            describe('删除活动后查询购物车', async function () {
                before('删除活动', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.act.timeLimit.deleteRule({ id: mySecKill.id });
                });
                it('查询购物车', async function () {
                    await ssReq.userLoginWithWx();
                    const cart = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows.find(obj => obj.id = carts[0].id));
                    console.log(`\ncartList=${JSON.stringify(cart)}`);
                    // console.log(`\n carts[0].actinfo=${JSON.stringify(carts)}`);
                    const dresActInfo = mySecKill.getDresActInfo({ spuId: cart.carts[0].spuId });
                    // console.log(`\ndresActInfo=${JSON.stringify(dresActInfo)}`);
                    common.isApproximatelyEqualAssert(Object.assign(carts[0], { actInfo: dresActInfo }), cart.carts[0]);
                });
            });
        });
    });
});