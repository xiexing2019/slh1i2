const common = require('../../../../lib/common');
const spAuth = require('../../../../reqHandler/ss/confc/spAuth');
const ss = require('../../../../reqHandler/ss');
const ssAccount = require("../../../data/ssAccount");
const ssReq = require('../../../help/ssReq');

describe('时长-offline', async function () {
    this.timeout(60000);
    let sellerInfo, durData, ranTime = common.getRandomNum(10, 1440);
    before('获取卖家当前直播时长', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        durData = await ss.fin.getAcctStatistics({ acctType: 1301 }).then(res => res.result.data);
    });
    describe('赠送时长', async function () {
        let list, timeOrder, presentTime;
        before('web端登录赠送时长', async function () {
            await spAuth.staffLogin({ code: ssAccount.ecStaff.code, pass: ssAccount.ecStaff.pass });
            await ss.live.presentLiveTime({ tenantId: sellerInfo.tenantId, time: ranTime });

            await ssReq.ssSellerLogin();
        });
        it('是否还是首次购买', async function () {
            const order = await ss.live.getLiveTimeVasBills({ pageNo: 1, pageSize: 10 }).then(res => res.result.data.rows);
            if (order.length != 0) {
                const firstBuy = await ss.live.getFirstBuyLiveTimeFlag().then(res => res.result);
                common.isApproximatelyEqualAssert(firstBuy.data, { val: 1 });
            } else {
                throw new Error('赠送时长后，仍然为首次购买')
            }
        });
        it('查询直播时长账户总额', async function () {
            const data = await ss.fin.getAcctStatistics({ acctType: 1301 }).then(res => res.result.data);
            expect(data, '赠送时长不成功').to.includes({ totalAmount: durData.totalAmount + ranTime });
        });
        it('查询时长账户流水', async function () {
            list = await ss.fin.findFinAcctFlowTypes({ acctType: 1301 }).then(res => res.result.data.rows);
            timeOrder = await ss.live.getLiveTimeVasBills({ pageNo: 1, pageSize: 10 }).then(res => res.result.data.rows);
            // console.log(list);
            if (timeOrder.length != 0) {
                presentTime = list.find(obj => obj.bizOrderId == timeOrder[0].id);
                expect(presentTime, '时长流水未查询到赠送的直播时长').to.includes({ tenantId: sellerInfo.tenantId, amount: ranTime, ioFlag: 0 });
            } else {
                throw new Error('订单列表无数据')
            };

        });
        it('查询时长订单列表', async function () {
            const findPersent = timeOrder.find(obj => obj.id == list[0].bizOrderId);
            //这里的num写死，因为服务端把赠送的时长都返回0
            expect(findPersent, `赠送时长为${findPersent.num}`).to.includes({ num: 0 });
        });
    });



});
