const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const liveHelp = require('../../../help/liveHelp');
const liveManage = require('../../../help/act/live');

describe('直播', function () {
    this.timeout(60000);
    let dresSpuList, liveTaskRes, sellerInfo;
    const live = liveManage.setupLive();

    describe('卖家', function () {
        let liveRoomInfo, dresList;
        // const live = liveManage.setupLive();
        before('卖家登录', async () => {
            await ssReq.ssSellerLogin();
            sellerInfo = _.cloneDeep(LOGINDATA);

            // 获取直播任务 如果直播任务未结束--结束直播
            let lastLiveData = await live.getLastLiveTask();
            if (lastLiveData.flag == 1) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: -1 });
            } else if ([2, 3, 4].includes(lastLiveData.flag)) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: 0 });
            }
            // 查询spu列表
            dresList = await live.getDresList(5);
            // console.log(`dresList = ${JSON.stringify(dresList[0])}`);
            dresSpuList = dresList.map(obj => obj.id);
            console.log(dresSpuList);
            // 获取直播间信息
            await live.getLiveRoomInfo();
        });
        after('结束/删除直播任务', async function () {
            await ssReq.ssSellerLogin();
            let lastLiveData = await live.getLastLiveTask();
            if (lastLiveData.flag == 1) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: -1 });
            } else if ([2, 3, 4].includes(lastLiveData.flag)) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: 0 });
            }
        });
        describe('保存直播任务', function () {
            before(async () => {
                liveTaskRes = await live.saveLiveTask({ spus: dresSpuList.slice(0, 2).toString(), execVal: { execType: 5, execNum: 10 } }, dresList.slice(0, 2));
                // console.log(`live = ${JSON.stringify(live)}`);
            });
            it('获取直播任务', async function () {
                const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                // console.log(`lastLive = ${JSON.stringify(lastLive)}`);
                // console.log(`exp = ${JSON.stringify(live.getLastLiveTaskExp())}`);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive);
            });
            it('查询直播商品列表', async function () {
                const liveDresList = await live.liveSpusList();
                console.log(`liveDresList = ${JSON.stringify(liveDresList)}`);
                console.log(`\nlive.spuList = ${JSON.stringify(live.spuList)}`);
                common.isApproximatelyArrayAssert(live.spuList, liveDresList.rows, ['execVal', 'invNum']);
            });
            it('买家查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: live.spuList[0].spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data)
                // console.log('商品详情信息1 = ', JSON.stringify(dresDetail));
                const exp = live.getDresActExp(live.spuList[0].spuId);
                // console.log(`exp=`, exp);
                common.isApproximatelyEqualAssert(exp, dresDetail.activity);
            });
            it.skip('修改直播', async function () {
                editLiveTaskRes = await live.saveLiveTask({ id: live.id, execVal: { execType: 5, execNum: 20 } });
                let editLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                // console.log(`editLive = ${JSON.stringify(editLive)}`);
                // console.log(`exp = ${JSON.stringify(live.getLastLiveTaskExp())}`);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), editLive);
            });
        });
        describe('新增直播活动商品', async function () {
            let dresSpuList, dresSpuIds, liveSpuList, skuIds;
            describe('添加新的商品', async function () {
                before('新增活动商品', async function () {
                    await ssReq.ssSellerLogin();
                    await live.addLiveSpus(dresList[2]);
                    console.log(dresList[2].spu.id);
                });
                it('查询直播商品列表', async function () {
                    const liveDresList = await live.liveSpusList();
                    // console.log(`liveDresList = ${JSON.stringify(liveDresList)}`);
                    // console.log(`\nlive.spuList = ${JSON.stringify(live.spuList)}`);
                    common.isApproximatelyArrayAssert(live.spuList, liveDresList.rows, ['execVal', 'invNum']);
                });
                it('买家查询商品详情', async function () {
                    await ssReq.userLoginWithWx();
                    const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: dresList[2].spu.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data)
                    // console.log('商品详情信息1 = ', JSON.stringify(dresDetail));
                    const exp = live.getDresActExp(dresList[2].spu.id);
                    // console.log(`exp=`, exp);
                    common.isApproximatelyEqualAssert(exp, dresDetail.activity);
                });
                it('重复添加同一个商品', async function () {
                    await ssReq.ssSellerLogin();
                    let addRes = await live.addLiveSpus(dresList[2], { check: false });
                    expect(addRes.result.msgId).to.be.equal('act_spu_already_exist');
                });
            });
            describe('修改直播商品-修改价格', async function () {
                before('修改活动价格', async function () {
                    await live.liveSpusList();
                    let spu = live.spuList.find(obj => obj.spuId == dresList[2].spu.id);
                    // console.log(`\nspu = ${JSON.stringify(spu)}`);
                    await live.addLiveSpus(dresList[2], { id: spu.id, execVal: { execType: 5, execNum: 1 } });
                });
                it('查询直播商品列表', async function () {
                    const liveDresList = await live.liveSpusList();
                    // console.log(`liveDresList = ${JSON.stringify(liveDresList)}`);
                    // console.log(`\nlive.spuList = ${JSON.stringify(live.spuList)}`);
                    common.isApproximatelyArrayAssert(live.spuList, liveDresList.rows, ['execVal', 'invNum']);
                });
                it('买家查询商品详情', async function () {
                    await ssReq.userLoginWithWx();
                    const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: dresList[2].spu.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data)
                    // console.log('商品详情信息1 = ', JSON.stringify(dresDetail));
                    const exp = live.getDresActExp(dresList[2].spu.id);
                    console.log(`exp=`, exp);
                    common.isApproximatelyEqualAssert(exp, dresDetail.activity);
                });
            });
        });
        describe('修改直播任务', function () {
            let editLiveTaskRes;
            before(async () => {
                await ssReq.ssSellerLogin();
                editLiveTaskRes = await live.saveLiveTask({ id: live.id });
                // console.log(`live = ${JSON.stringify(live)}`);
            });
            it('获取直播任务', async function () {
                const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                let res = await live.liveSpusList();
                // console.log(`res = ${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive);
                common.isApproximatelyArrayAssert(live.liveSpus, lastLive.liveSpus)
            });
            it('修改直播(减少spu)', async function () {
                let subLiveTaskRes = await live.saveLiveTask({ id: live.id, spus: dresSpuList[2].toString() });
                const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                // console.log(lastLive);
                let res = await live.liveSpusList();
                // console.log(`res = ${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive);
                common.isApproximatelyArrayAssert(live.liveSpus, lastLive.liveSpus)
            });
            it('修改直播(增加spu)', async function () {
                let addLiveTaskRes = await live.saveLiveTask({ id: live.id, spus: dresSpuList.toString() });
                const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                // console.log(lastLive);
                let res = await live.liveSpusList();
                // console.log(`res = ${JSON.stringify(res)}`);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive);
                common.isApproximatelyArrayAssert(live.liveSpus, lastLive.liveSpus)
            });
            it.skip('批量修改商品价格', async function () {
                let editLiveTaskRes = await live.saveLiveTask({ id: live.id, spus: dresSpuList.toString(), execVal: { execType: 5, execNum: 1 } });
                console.log(editLiveTaskRes);
                const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                let liveDres = await live.getLiveTaskSpus({ taskId: liveTaskRes.result.data.id, pageSize: 5 }).then(res => res.result.data.rows);
                console.log('lb', liveDres);
                // let liveSpusPrice = editLiveTaskRes.map(obj => obj.pubPrice)
                // console.log('nnsns', liveSpusPrice);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive);
            });

        });
        describe('操作直播', function () {
            describe('开始直播', function () {
                before(async () => {
                    await ss.live.operateLiveTask({ id: live.id, opType: 3 });
                });
                it('获取直播任务', async function () {
                    let lastLiveNew = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveNew.flag, `开始直播操作失败`).to.be.equal(3);
                });
                it('开始直播后不允许删除直播', async function () {
                    let operateRes = await live.operateLiveTask({ id: live.id, opType: -1, check: false });
                    expect(operateRes.result.msg, '已经开始直播后，居然能删除直播').to.be.equal('只允许删除未开始的直播任务！');
                });
                it('获取直播任务', async function () {
                    let lastLiveRes = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveRes.flag, `删除直播操作失败以后修改了直播状态为: ${lastLiveRes.flag}`).to.be.equal(3);
                });
            });
            describe('直播弹商品', function () {
                before(async () => {
                    let popRes = await live.popSpus({ taskId: live.id, spuIds: dresSpuList[0].toString() });
                });
                it('获取直播任务', async function () {
                    let lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                    expect(lastLive.liveSpus[0].extFlag, `第一次弹出商品后，商品列表弹出状态变为${lastLive.liveSpus[0].extFlag}`).to.be.equal(1);
                });
                describe('再次弹出其他商品', function () {
                    before(async () => {
                        await live.popSpus({ taskId: live.id, spuIds: dresSpuList[1].toString() });
                    });
                    it('获取直播任务', async function () {
                        let lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                        expect(lastLive.liveSpus[0].extFlag, `再次弹出其他商品后，商品列表已经弹出的商品状态变为${lastLive.liveSpus[0].extFlag}`).to.be.equal(2);
                        expect(lastLive.liveSpus[1].extFlag, `再次弹出商品后，商品列表弹出状态变为${lastLive.liveSpus[1].extFlag}`).to.be.equal(1);
                    });
                    it('弹出-弹出中的商品', async function () {
                        await live.popSpus({ taskId: live.id, spuIds: dresSpuList[1].toString() });
                        let lastLiveRes = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                        expect(lastLiveRes.liveSpus[1].extFlag, `弹出-弹出中的商品失败,商品状态变为:${lastLiveRes.liveSpus[1].extFlag}`).to.be.equal(1);
                    });
                    it('弹出-已弹出的商品', async function () {
                        await live.popSpus({ taskId: live.id, spuIds: dresSpuList[0].toString() });
                        let lastLiveRes = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                        expect(lastLiveRes.liveSpus[0].extFlag, `弹出-已弹出的商品失败,商品状态变为:${lastLiveRes.liveSpus[0].extFlag}`).to.be.equal(1);
                    });
                });

            });
            describe('暂停直播', function () {
                before(async () => {
                    await live.operateLiveTask({ id: live.id, opType: 4 });
                });
                it('获取直播任务', async function () {
                    let lastLiveRes = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveRes.flag, `暂停直播操作失败`).to.be.equal(4);
                });
                it('暂停后不允许删除直播', async function () {
                    let operateRes = await live.operateLiveTask({ id: live.id, opType: -1, check: false });
                    expect(operateRes.result.msg, `不允许删除的情况下，直播被删除`).to.be.equal('只允许删除未开始的直播任务！');
                });
                it('暂停后重新开始直播', async function () {
                    await live.operateLiveTask({ id: live.id, opType: 3 });
                    let lastLiveNew = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveNew.flag, `开始直播操作失败`).to.be.equal(3);
                });
            });
            describe('结束直播', function () {
                before(async () => {
                    // 加入买家加入聊天室
                    await ssReq.userLoginWithWx();
                    await ss.live.enterGroup({ sellerUnitId: sellerInfo.unitId, groupId: live.timGroupId, nickName: 'test' });
                    await common.delay(2000);
                    await ssReq.ssSellerLogin();
                    await live.operateLiveTask({ id: live.id, opType: 0 });
                    console.log(live.flag);
                });
                it('获取直播任务', async function () {
                    let lastLiveNew = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveNew.flag, `结束直播操作失败`).to.be.equal(0);
                });
                it('结束直播后不允许删除直播', async function () {
                    let operateRes = await live.operateLiveTask({ id: live.id, opType: -1, check: false });
                    let lastLiveNew = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(operateRes.result.msg).to.be.equal('直播任务已经结束或取消，不允许操作！');
                    expect(lastLiveNew.flag, `不允许删除的情况下，直播被删除`).to.be.equal(0);
                });
                it('获取人数统计', async function () {
                    let liveStatus = await live.getLiveTaskStats();
                    console.log(liveStatus);
                    expect(liveStatus.watchNum, `直播结束，人数统计失败`).to.equal(1);
                });
            });
            describe('正常删除直播', function () {
                let liveTask;
                before(async () => {
                    // 保存直播任务
                    await ssReq.ssSellerLogin();
                    liveTask = await liveHelp.saveLiveTask({ liveRoomId: live.liveRoomId, timGroupId: live.timGroupId, robotNum: 500, spus: dresSpuList.toString() }).then(res => res.result.data);
                    // 删除直播
                    await live.operateLiveTask({ id: liveTask.id, opType: -1 });
                    console.log(live.flag);
                });
                it('获取直播任务', async function () {
                    let lastLiveRes = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId });
                    expect(lastLiveRes.flag, `删除直播失败`).to.be.equal(0);
                });
            });
        });
        describe('买家', function () {
            let sellerInfo, clientInfo, lastLive;
            before('买家登录', async () => {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                await ssReq.userLoginWithWx();
                clientInfo = _.cloneDeep(LOGINDATA);
            });
            it('获取直播任务', async function () {
                lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                console.log(lastLive);
                common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive, ['extFlag', 'extValue']); // extValue 商品弹出时间暂时不处理
            });

            describe('买家点赞', function () {
                let likeNumBefore;
                before('点赞直播间', async function () {
                    await ssReq.ssSellerLogin();
                    await live.getLiveRoomInfo();
                    likeNumBefore = _.cloneDeep(live.likeNum);

                    await ssReq.userLoginWithWx();
                    await live.likeLiveRoom({ roomId: lastLive.liveRoomId, timUserName: clientInfo.userName });
                });
                it('卖家查询直播间点赞数量', async function () {
                    await ssReq.ssSellerLogin();
                    const roomInfo = await live.getLiveRoomInfo();
                    // expect(, ).to.be.equal(likeNumBefore + 1);
                    expect(roomInfo.likeNum, `点赞失败:点赞后错误`).to.equal(live.likeNum);
                });
            });

            describe('关注店铺', function () {
                before('买家关注店铺', async function () {
                    await ssReq.userLoginWithWx();
                    const attention = await live.checkShopFavorState({ shopId: sellerInfo.shopId });
                    if (attention.data.val == 0) {
                        await sp.spUp.addFavorShop({ shopId: sellerInfo.shopId, shopName: sellerInfo.shopName });
                    }
                });
                it('查询店铺关注情况', async function () {
                    const attention = await live.checkShopFavorState({ shopId: sellerInfo.shopId });
                    expect(attention.data.val, `查询店铺关注情况失败`).to.be.equal(1);
                });
                describe('取消关注', async function () {
                    before('取消关注', async function () {
                        await sp.spUp.cancelFavorShop({ shopId: sellerInfo.shopId });
                    });
                    it('查询店铺关注情况', async function () {
                        const attention = await live.checkShopFavorState({ shopId: sellerInfo.shopId });
                        expect(attention.data.val, `查询店铺关注情况失败`).to.be.equal(0);
                    });
                });
            });

            describe.skip('游客登录', function () {
                let guestInfo;
                before('游客登录', async function () {
                    await ssReq.ssClientGuestLogin({ tenantId: sellerInfo.tenantId, sellerId: sellerInfo.id, mobile: common.getRandomMobile() });
                    guestInfo = _.cloneDeep(LOGINDATA);
                });
                it('获取直播任务', async function () {
                    const lastLive = await live.getLastLiveTask({ sellerUnitId: sellerInfo.unitId, spuFlag: 1 });
                    // console.log(lastLive);
                    common.isApproximatelyEqualAssert(live.getLastLiveTaskExp(), lastLive, ['extFlag']);
                });
                it('获取云通信登录账号与签名', async function () {
                    let userSigRes = await ss.live.getTimUserSig({ userType: 0 });
                    // console.log(userSigRes);
                    expect(userSigRes.result.data.userSig).to.not.be.null;
                    expect(userSigRes.result.data.userId).to.not.be.null;
                });
                it('加入直播聊天室-买家', async function () {
                    let enterRes = await ss.live.enterGroup({ sellerUnitId: sellerInfo.unitId, groupId: live.timGroupId, nickName: guestInfo.nickName });
                    expect(enterRes.result.msg, '加入直播聊天室失败').to.equal('成功');
                });
            });
        });
    });
});