const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const basicJson = require('../../../help/basicJson');
const ssAccount = require('../../../data/ssAccount');
const liveHelp = require('../../../help/liveHelp');

describe('聊天室', function () {
    this.timeout(30000);
    let sellerInfo, liveRoomInfo, dresSpuList;
    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // 查询商品列表
        dresSpuList = await sp.spdresb.findSellerSpuList({ pageSize: 5 }).then(res => res.result.data.rows.map(data => data.id));
        // 获取直播间信息
        liveRoomInfo = await ss.live.getLiveRoomInfo().then(res => res.result.data.rows).then(res => res.find(data => data.id));
    });
    describe('买家', function () {
        before('买家登录', async function () {
            await ssReq.userLoginWithWx();
        });
        it('获取云通信登录账号与签名', async function () {
            let userSigRes = await ss.live.getTimUserSig({ userType: 0 });
            // console.log(userSigRes);
            expect(userSigRes.result.data.userSig).to.not.be.null;
            expect(userSigRes.result.data.userId).to.not.be.null;
        });
        it('加入直播聊天室-买家', async function () {
            let enterRes = await ss.live.enterGroup({ sellerUnitId: sellerInfo.unitId, groupId: liveRoomInfo.timGroupId, nickName: 'test' });
            // console.log(enterRes);
            expect(enterRes.result.msg, '加入直播聊天室失败').to.equal('成功');
        });
        it('退出直播聊天室-买家', async function () {
            let leaveRes = await ss.live.leaveGroup({ sellerUnitId: sellerInfo.unitId, groupId: liveRoomInfo.timGroupId, nickName: 'test' });
            // console.log(leaveRes);
            expect(leaveRes.result.msg, '加入直播聊天室失败').to.equal('成功');
        });
        it('用户发送下单消息-下单', async function () {
            let orderMsg = await ss.live.sendUserOrderMsg({ groupId: liveRoomInfo.timGroupId, nickName: common.getRandomChineseStr(), spuIds: dresSpuList.toString(), orderType: 0 });
            expect(orderMsg.result.msg, '下单消息发送失败').to.equal('成功');
        });
        it('用户发送下单消息-购物车', async function () {
            let orderMsg = await ss.live.sendUserOrderMsg({ groupId: liveRoomInfo.timGroupId, nickName: common.getRandomChineseStr(), spuIds: dresSpuList.toString(), orderType: 1 });
            expect(orderMsg.result.msg, '加入购物车消息发送失败').to.equal('成功');
        });
    });
    describe('卖家', function () {
        before('卖家登录', async () => {
            await ssReq.ssSellerLogin();
        });
        it('获取云通信登录账号与签名', async function () {
            let userSigRes = await ss.live.getTimUserSig({ userType: 1 });
            // console.log(userSigRes);
            expect(userSigRes.result.data.userSig).to.not.be.null;
            expect(userSigRes.result.data.userId).to.not.be.null;
        });
        it('加入直播聊天室-卖家', async function () {
            let enterRes = await ss.live.enterGroup({ groupId: liveRoomInfo.timGroupId });
            // console.log(enterRes);
            expect(enterRes.result.msg, '加入直播聊天室失败').to.equal('成功');
        });
        it('退出直播间聊天室-卖家', async function () {
            let leaveRes = await ss.live.leaveGroup({ groupId: liveRoomInfo.timGroupId });
            // console.log(leaveRes);
            expect(leaveRes.result.msg, '加入直播聊天室失败').to.equal('成功');
        });

    });
});