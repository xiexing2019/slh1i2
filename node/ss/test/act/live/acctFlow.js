const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const uuid = require('uuid');

describe('直播账户流水', async function () {
    this.timeout(30000);
    let seller, startRecord;

    before(async function () {
        await ssReq.ssSellerLogin();
        seller = _.cloneDeep(LOGINDATA);
    });

    it('获取直播账户信息', async function () {
        const res = await ss.fin.getAcctStatistics({ acctType: 1301 });
        startRecord = res.result.data;
    });

    describe('充值', async function () {
        let changeRes;
        before(async function () {
            changeRes = await ss.fin.saveFinAcctFlowInfo({
                unitId: seller.unitId,
                tenantId: seller.tenantId,
                acctType: 1301,
                name: '直播时长充值',
                amount: 150,
                bizType: 1204,
                bizOrderId: common.getRandomNumStr(5),
                ioFlag: 0,
                tradeType: 200,
                hashKey: `${Date.now()}${uuid.v4()}`
            });
            console.log(`changeRes=${JSON.stringify(changeRes)}`);
        });
        it('获取账户直播流水', async function () {
            const list = await ss.fin.findFinAcctFlowTypes({ acctType: 1301 }).then(res => res.result.data.rows);
            const data = list.find(ele => ele.bizOrderId == changeRes.params.jsonParam.bizOrderId);
            common.isApproximatelyEqualAssert(changeRes.params.jsonParam, data, ['hashKey', 'unitId']); // 财务中台的unitId与微商城的unitId不一样 财务中台的unitId，是商户id，即merchantId
        });
        it('获取直播账户信息', async function () {
            const data = await ss.fin.getAcctStatistics({ acctType: 1301 }).then(res => res.result.data);
            expect(data).to.includes({ totalAmount: startRecord.totalAmount + 150 });
        });
    });

    describe('消费', async function () {
        let changeRes;
        before(async function () {
            changeRes = await ss.fin.saveFinAcctFlowInfo({
                unitId: seller.unitId,
                tenantId: seller.tenantId,
                acctType: 1301,
                name: '直播时长消费',
                amount: 50,
                bizType: 1204,
                bizOrderId: common.getRandomNumStr(5),
                ioFlag: 1,
                tradeType: 101,
                hashKey: `${Date.now()}${uuid.v4()}`
            });
            console.log(`changeRes=${JSON.stringify(changeRes)}`);
        });
        it('获取账户直播流水', async function () {
            const list = await ss.fin.findFinAcctFlowTypes({ acctType: 1301 }).then(res => res.result.data.rows);
            console.log(`list=${JSON.stringify(list)}`);

            const data = list.find(ele => ele.bizOrderId == changeRes.params.jsonParam.bizOrderId);
            common.isApproximatelyEqualAssert(changeRes.params.jsonParam, data, ['hashKey', 'unitId']);
        });
        it('获取直播账户信息', async function () {
            const data = await ss.fin.getAcctStatistics({ acctType: 1301 }).then(res => res.result.data);
            expect(data).to.includes({ totalAmount: startRecord.totalAmount + 100 });
        });
    });

});