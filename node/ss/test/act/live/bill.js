const common = require('../../../../lib/common');
const secKillHelp = require('../../../help/act/secKill');
const dresManage = require('../../../help/shopManager');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssReq = require('../../../help/ssReq');
const moment = require('moment');
const basicJson = require('../../../help/basicJson');
const billManage = require('../../../help/billManage');
const format = require('../../../../data/format');
const liveManage = require('../../../help/act/live');

describe('直播下单', async function () {
    this.timeout(30000);
    let sellerInfo, dresList = [], purRes;
    const live = liveManage.setupLive();
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        secKillHelp.deleteOrDisableAllAct();

        let liveId = await ss.live.getLastLiveTask().then(res => res.result.data);
        console.log(liveId);
        if (liveId.flag == 1) {
            await ss.live.operateLiveTask({ id: liveId.id, opType: -1 });
        } else if ([2, 3, 4].includes(liveId.flag)) {
            await ss.live.operateLiveTask({ id: liveId.id, opType: 0 });
        }

        // 查询spu列表
        dresList = await live.getDresList(2);
        // console.log(`dresList = ${JSON.stringify(dresList)}`);

        let spuIds = dresList.map(obj => obj.id);
        console.log(spuIds);
        // 新建直播任务
        await live.getLiveRoomInfo();
        await live.saveLiveTask({ spus: spuIds.toString(), execVal: { execType: 5, execNum: 10 } }, dresList);
        await ss.live.operateLiveTask({ id: live.id, opType: 3 });
        // console.log(JSON.stringify(live));
        await common.delay(1000);
    });
    after('结束删除直播任务', async function () {
        await ssReq.ssSellerLogin();
        let liveId = await ss.live.getLastLiveTask().then(res => res.result.data);
        if (liveId.flag == 1) {
            await ss.live.operateLiveTask({ id: liveId.id, opType: -1 });
        } else if ([2, 3, 4].includes(liveId.flag)) {
            await ss.live.operateLiveTask({ id: liveId.id, opType: 0 });
        }
    });
    it('买家查询直播商品列表', async function () {
        await ssReq.userLoginWithWx();
        let dresList = await live.liveSpusList({ sellerUnitId: sellerInfo.unitId });
        console.log(`买家查询直播商品列表 = ${JSON.stringify(dresList)}`);
        console.log(`\n${JSON.stringify(live.liveSpus)}`);
        common.isApproximatelyArrayAssert(live.liveSpus, dresList.rows);
    });
    describe('下单', function () {
        let secKillJson, styleInfoBefore, salesListRes;
        before('买家获取商品信息', async function () {
            await ssReq.userLoginWithWx();

            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: live.spus.split(',')[0], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            // console.log(`\ngetFullForBuyer=${JSON.stringify(getFullForBuyer)}`);
            expect(getFullForBuyer.activity, `商品详情的活动信息activity不存在${JSON.stringify(getFullForBuyer)}`).not.to.be.undefined;

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(getFullForBuyer, { count: 1, num: 1 });
            // console.log(`\npurJson=${JSON.stringify(purJson)}`);
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            // console.log(`\npurJson=${JSON.stringify(purJson)}`);

            await ssReq.ssSellerLogin();
            //卖家商品详情
            styleInfoBefore = await sp.spdresb.getFullById({ id: purJson.orders[0].details[0].spuId }).then(res => res.result.data);;
            // console.log(`styleInfoBefore=${JSON.stringify(styleInfoBefore)}`);
            await ssReq.userLoginWithWx();
            purRes = await billManage.createPurBill(purJson);
            await ssReq.ssSellerLogin();
        });
        it('查询销售单列表', async function () {
            salesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            console.log(`销售单列表:${JSON.stringify(salesListRes)}`);
            expect(salesListRes.bill.payFlag).to.equal(0);
        });
        it('查询单据详情', async function () {
            //销售单id 非采购单
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });//
            console.log(`销售单详情:${JSON.stringify(salesBillInfo)}`);
            expect(salesBillInfo.result.data.bill.payFlag).to.equal(0);
        });
        it('卖家商品详情', async function () {
            let styleInfoAfter = await sp.spdresb.getFullById({ id: purRes.params.jsonParam.orders[0].details[0].spuId }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(styleInfoBefore, styleInfoAfter, ['ver', 'channelIds', 'updatedDate', 'updatedBy']);
        });

    });

    describe('买家支付订单', function () {
        let payRes, orderRunListRes, selesListRes;
        before(async () => {
            await ssReq.userLoginWithWx();
            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: LOGINDATA.wxOpenId,
                orderIds: [purRes.result.data.rows[0].billId],
                payMoney: purRes.params.jsonParam.orders[0].main.money,
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
        });
        it('支付回调', async function () {
            const receivePayRes = await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });
            console.log(`\n支付回调:${JSON.stringify(receivePayRes)}`);
        });
    });
});