
const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const liveHelp = require('../../../help/liveHelp');
const liveManage = require('../../../help/act/live');

describe.skip('直播回放', async function () {
    this.timeout(60000);
    let dresSpuList, liveTaskRes, sellerInfo, clientInfo, recordId;
    const live = liveManage.setupLive();

    before('创建直播', async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        // 获取直播任务 如果直播任务未结束--结束直播
        let lastLiveData = await live.getLastLiveTask();
        if (lastLiveData.flag == 1) {
            await live.operateLiveTask({ id: lastLiveData.id, opType: -1 });
        } else if ([2, 3, 4].includes(lastLiveData.flag)) {
            await live.operateLiveTask({ id: lastLiveData.id, opType: 0 });
        }
        // 查询spu列表
        dresList = await live.getDresList(5);
        // console.log(`dresList = ${JSON.stringify(dresList[0])}`);
        dresSpuList = dresList.map(obj => obj.id);
        console.log(dresSpuList);
        // 获取直播间信息
        await live.getLiveRoomInfo();
        //创建直播
        liveTaskRes = await live.saveLiveTask({ spus: dresSpuList.slice(0, 2).toString(), execVal: { execType: 5, execNum: 10 } }, dresList.slice(0, 2));
    });
    after('结束/删除直播任务', async function () {
        await ssReq.ssSellerLogin();
        let lastLiveData = await live.getLastLiveTask();
        if (lastLiveData.flag == 1) {
            await live.operateLiveTask({ id: lastLiveData.id, opType: -1 });
        } else if ([2, 3, 4].includes(lastLiveData.flag)) {
            await live.operateLiveTask({ id: lastLiveData.id, opType: 0 });
        }
    });
    describe('开始录制', async function () {
        before(async function () {
            recordId = await live.startRecord({ taskId: live.id, spuId: dresList });
            expect(recordId).to.not.be.undefined;
        });
        it('取消录制', async function () {
            const res = await live.endRecord({ recordId: live.recordId }).then(res => res.result);
            expect(res.msg, '取消失败').to.be.equal('成功')
        });
        it('取消录制后开始录制', async function () {
            const recordId1 = await live.startRecord({ taskId: live.id, spuId: dresList });
            expect(recordId1).to.not.be.equal(recordId);
        });
    });
    describe('结束录制', async function () {
        before(async function () {
            recordId = await live.endRecord({ recordId: live.recordId });
        });
        describe('卖家查看录播', async function () {
            before('卖家登录', async function () {
                await ssReq.ssSellerLogin();
            });
            it('查看录播列表', async function () {
                await live.findHistoryLiveRecord({ nameLike: live.title });
            });
            it('查看录播详情', async function () {
                await live.findHistoryLiveRecord({ liveTaskId: live.id });
            });
        });
        describe('买家查看录播', async function () {
            before('买家登录', async function () {
                await ssReq.userLoginWithWx();
            });
            it('查看录播列表', async function () {
                await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, nameLike: live.title });
            });
            it('查看录播详情', async function () {
                await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, liveTaskId: live.id });
            });
        });
        describe('卖家修改录播状态', async function () {
            describe('隐藏', async function () {
                before('隐藏录播', async function () {
                    await live.operateLiveTaskRecord({ liveTaskIId: live.id, recordIds: [live.recordId].toString, operateType: 0 })
                });
                describe('卖家查看录播', async function () {
                    before('卖家登录', async function () {
                        await ssReq.ssSellerLogin();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ liveTaskId: live.id });
                    });
                });
                describe('买家查看录播', async function () {
                    before('买家登录', async function () {
                        await ssReq.userLoginWithWx();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, liveTaskId: live.id });
                    });
                });
            });
            describe('显示', async function () {
                before('显示录播', async function () {
                    await live.operateLiveTaskRecord({ liveTaskIId: live.id, recordIds: [live.recordId].toString, operateType: 1 })
                });
                describe('卖家查看录播', async function () {
                    before('卖家登录', async function () {
                        await ssReq.ssSellerLogin();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ liveTaskId: live.id });
                    });
                });
                describe('买家查看录播', async function () {
                    before('买家登录', async function () {
                        await ssReq.userLoginWithWx();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, liveTaskId: live.id });
                    });
                });
            });
            describe('删除', async function () {
                before('删除录播', async function () {
                    await live.operateLiveTaskRecord({ liveTaskIId: live.id, recordIds: [live.recordId].toString, operateType: -1 })
                });
                describe('卖家查看录播', async function () {
                    before('卖家登录', async function () {
                        await ssReq.ssSellerLogin();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ liveTaskId: live.id });
                    });
                });
                describe('买家查看录播', async function () {
                    before('买家登录', async function () {
                        await ssReq.userLoginWithWx();
                    });
                    it('查看录播列表', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, nameLike: live.title });
                    });
                    it('查看录播详情', async function () {
                        await live.findHistoryLiveRecord({ sellerUnitId: sellerInfo.unitId, liveTaskId: live.id });
                    });
                });
            });
        });
    });
    describe('直播任务已结束开始录制', async function () {
        before(async function () {
            await ssReq.ssSellerLogin();
            let lastLiveData = await live.getLastLiveTask();
            if (lastLiveData.flag == 1) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: -1 });
            } else if ([2, 3, 4].includes(lastLiveData.flag)) {
                await live.operateLiveTask({ id: lastLiveData.id, opType: 0 });
            }
        });
        after('结束录制', async function () {
            await ssReq.ssSellerLogin();
            const res = await live.endRecord({ recordId: live.recordId }).then(res => res);
            expect(res.msg).to.be.equal('成功')
        });
        it('开始录制', async function () {
            const recordId2 = await live.startRecord({ taskId: live.id, spuId: dresList }).then(res => res.result);
            expect(recordId2.msg).to.not.be.equal('成功')
        });

    });
});
