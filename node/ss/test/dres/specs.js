const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const dresManage = require('../../help/dresManage');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const specsManage = require('../../help/spdresb/specs');

/**
 * 规格属性类型新增
 *
 * 功能影响：spu的规格属性自定义功能。对应属性值采用字典
 * 涉及表：spb字库中的dres_class_unit_prop
 *
 * 特殊说明：
 * 预设的名称含“颜色，面料，尺码，季节，风格”等不能修改，也不能被修改成，
 * 类别归属的规格属性不能修改，
 * 展示名同一分类下不能重复，重复名称的被禁用规格属性会新增报存会导致启用
 *
 * 查询到没有规格属性数据的时候会对其进行初始化
 * 初始化规则:
 * 1、店铺行业是默认行业（现全部都是默认行业） ，会初始化 服装类的规格属性进去含 规格 颜色 尺码  含属性 面料 季节 颜色
 * 2、店铺行业是选定行业，会初始化对应设定的默认规格属性进去，（暂无）
 */
describe('规格属性类型', async function () {
    this.timeout(30000);
    const specs = specsManage.setupDresSpecs();
    before('新增规格属性类型', async function () {
        await ssReq.ssSellerLogin();

        await ss.spdresb.findSpecsList({ typeId: 1 });

        await specs.saveFull({ propCaption: `规格${common.getRandomStr(5)}`, typeId: common.getRandomNum(1, 3) });
        console.log(specs);
    });

    after(async function () {
        if (specs.id) specs.disableSpecs();
    });

    it('货品规格属性列表', async function () {
        const data = await specs.getFullFromList();
        common.isApproximatelyEqualAssert(specs.getExp(), data);
    });

    describe('停用规格属性', async function () {
        before(async function () {
            await specs.disableSpecs();
        });
        it('货品规格属性列表', async function () {
            const data = await specs.getFullFromList();
            common.isApproximatelyEqualAssert(specs.getExp(), data);
        });
    });

    describe('启用规格属性', async function () {
        before(async function () {
            await specs.ableSpecs();
        });
        it('货品规格属性列表', async function () {
            const data = await specs.getFullFromList();
            common.isApproximatelyEqualAssert(specs.getExp(), data);
        });
    });


});