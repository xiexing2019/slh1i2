const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const ssConfigParam = require('../../help/configParamManager');
const mysql = require('../../../reqHandler/sp/mysql');
const MyShop = require('../../help/shop/myShop');

describe('商品上下架', function () {
    this.timeout(TESTCASE.timeout);
    const myShop = new MyShop();
    let classId = 1013;

    before(async function () {
        await myShop.switchSeller();
        await dresManage.prePrepare();
        console.log(`\nBASICDATA=${JSON.stringify(BASICDATA)}`);
        await myShop.getConfigParam({ code: 'auto_shelves_after_synchronized', ownerId: LOGINDATA.tenantId }, 'spb');
        await myShop.getConfigParam({ code: 'allow_picture_exists_shelves', ownerId: LOGINDATA.tenantId }, 'spb');
        await myShop.getConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', ownerId: LOGINDATA.tenantId }, 'spb');
        console.log(myShop);
    });

    // 其他用例都默认需要能够自动上架 即flag=1
    // 因此最后需要将参数都置为打开状态(不考虑使用还原)
    after('打开新同步货品自动上架，没有图片允许，库存<=0允许、价格为0允许上架参数', async function () {
        await myShop.updateConfigParam({ code: 'auto_shelves_after_synchronized', val: 1 });
        await myShop.updateConfigParam({ code: 'allow_picture_exists_shelves', val: 1 });
        await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
    });

    describe('新同步货品自动上架参数', async function () {
        describe('开启', async function () {
            const dres = dresManage.setupDres();
            before(async function () {
                await myShop.updateConfigParam({ code: 'auto_shelves_after_synchronized', val: 1 });
                await dres.saveDres(basicJson.styleJson({ classId: classId }));
                // console.log(dres);
            });
            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });
        describe('关闭', async function () {
            const dres = dresManage.setupDres();
            before(async function () {
                await myShop.updateConfigParam({ code: 'auto_shelves_after_synchronized', val: 0 });
                await common.delay(2000);
                await dres.saveDres(basicJson.styleJson({ classId: classId }));
                // console.log(dres);
            });
            it('查询待上架列表', async function () {
                this.retries(5);
                await common.delay(2000);
                //上架：1， 下架：-4，待上架：2
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                dres.spu.flag = 2;
                dres.spu.marketFailure = '没有设置自动上架';
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
            describe('手动上架', async function () {
                before('上架商品', async function () {
                    await dres.onMarket();
                });
                it('查询已上架列表', async function () {
                    const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                    common.isApproximatelyEqualAssert(dres.spu, dresSpu);
                });
            });
        });
    });

    describe('货品无图片允许上架', async function () {
        describe('开启', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.updateConfigParam({ code: 'allow_picture_exists_shelves', val: 1 });
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu) => {
                    delete spu.docContent;
                    delete spu.docHeader;
                }));
            });
            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
            it('关闭货品无图片允许上架参数待上架列表', async function () {
                await myShop.updateConfigParam({ code: 'allow_picture_exists_shelves', val: 0 });
                await common.delay(1000);
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                dres.spu.flag = 2;
                dres.spu.marketFailure = '不允许图片为空上架';
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });
        describe('关闭', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.updateConfigParam({ code: 'allow_picture_exists_shelves', val: 0 });
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu) => {
                    delete spu.docContent;
                    delete spu.docHeader;
                }));
            });
            it('查询待上架列表', async function () {
                //上架：1， 下架：-4，待上架：2
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                dres.spu.flag = 2;
                dres.spu.marketFailure = '不允许图片为空上架';
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
            describe('手动上架', async function () {
                it('上架商品-不允许上架', async function () {
                    let res = await sp.spdresb.onMarket({ ids: [dres.id], check: false });
                    expect(res.result.msg, `图片为空不允许上架`).to.be.include('不允许图片为空上架');
                });
            });
        });
    });

    describe('没有库存允许上架', async function () {
        describe('开启', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 1 });
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu, skus) => {
                    skus.forEach((sku) => {
                        sku.num = 0;
                        sku.ssNum = 0;
                    });
                }));
            });
            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
            it('买家查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const dresFull = await dres.getDetailForBuyer();
                console.log(`\n 买家查询商品详情=${JSON.stringify(dresFull)}`);
                common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
            });
        });
        describe('关闭', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.switchSeller();
                await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu, skus) => {
                    skus.forEach((sku) => {
                        sku.num = 0;
                        sku.ssNum = 0;
                    });
                }));
            });
            it('查询待上架列表', async function () {
                //上架：1， 下架：-4，待上架：2
                this.retries(3);
                await common.delay(1000);
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                dres.spu.flag = 2;
                dres.spu.marketFailure = '库存不足';
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
            it('买家查询商品详情', async function () {
                await ssReq.userLoginWithWx();
                const dresFull = await dres.getDetailForBuyer();
                console.log(`\n 买家查询商品详情=${JSON.stringify(dresFull)}`);
                common.isApproximatelyEqualAssert(dres.getBuyerDetailExp({ zeroCanOn: 0 }), dresFull.result.data);
            });
        });
    });

    describe('价格为0', async function () {
        describe('新增价格为0商品', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.switchSeller();
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu, skus) => {
                    skus.forEach((sku) => {
                        sku.pubPrice = 0;
                    });
                    spu.pubPrice = 0;
                }));
            });
            it('查询待上架列表', async function () {
                dres.spu.flag = 2;
                dres.spu.marketFailure = '发布价未大于0';
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });
    });

    describe('价格为0,无图,无库存', async function () {
        describe('新增价格为0商品', async function () {
            const dres = dresManage.setupDres({ type: 'app' });
            before(async function () {
                await myShop.updateConfigParam({ code: 'allow_picture_exists_shelves', val: 0 });
                await myShop.updateConfigParam({ code: 'allow_shelves_when_inventory_less_than_zero', val: 0 });
                await dres.saveDresByApp(basicJson.styleJsonByApp({}, (spu, skus) => {
                    delete spu.docContent;
                    delete spu.docHeader;
                    skus.forEach((sku) => {
                        sku.num = 0;
                        sku.ssNum = 0;
                        sku.pubPrice = 0;
                    });
                    spu.pubPrice = 0;
                }));
            });
            it('查询待上架列表', async function () {
                await common.delay(10000);
                dres.spu.flag = 2;
                // dres.spu.marketFailure = '不允许图片为空上架,发布价未大于0'; //库存不足,发布价未大于0
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['marketFailure']);
                console.log(dresSpu.marketFailure);
                expect(['不允许图片为空上架,发布价未大于0', '库存不足,发布价未大于0', '发布价未大于0', '不允许图片为空上架,库存不足,发布价未大于0']).to.include(dresSpu.marketFailure);
            });
        });
    });
});