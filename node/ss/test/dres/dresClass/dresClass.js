const common = require('../../../../lib/common');
const getShop = require('../../../help/shop/getShop');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssReq = require('../../../help/ssReq');
const dresManage = require('../../../help/dresManage');
const dresClassManage = require('../../../help/dres/dresClassManage');
const esSearchHelp = require('../../../help/esSearchHelp');
const basicJson = require('../../../help/basicJson');

const fs = require('fs');
const path = require('path');
const caps = require('../../../../data/caps');
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../data/doc.json')))[caps.name];

/**
 * 商城分类功能，做了分类管理，商品添加分类，和货品分类查询的操作
 * http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1527.html
 *
 * story#4489 商品分类支持单个分类隐藏
 * http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1630.html
 *
 */
describe('商品类别', async function () {
    this.timeout(TESTCASE.timeout);
    const sellerShop = getShop.getSellerShop();
    let spuIds = [];
    before('登录', async function () {
        await sellerShop.switchSeller();

        // 获取商品列表 后续配置分类时使用
        spuIds = await sp.spdresb.findSellerSpuList({ flags: 1 }).then(res => {
            return res.result.data.rows.slice(0, 5).map(dres => dres.id);
        });
    });

    // 分类维护
    describe('新建一级分类', async function () {
        const dresClass1 = dresClassManage.setupDresClass();
        const level = 1;
        before('新建', async function () {
            await dresClass1.saveFullClass({
                name: `1级类别${common.getRandomStr(5)}`,
                parentId: 0,
                level: level
            });
            console.log(dresClass1);
        });

        after('删除', async function () {
            await sellerShop.switchSeller();

            await dresClass1.deleteFullClass({ check: false });
        });

        it('卖家分类列表树', async function () {
            await dresClass1.dresSpuClassTreeAssert();
        });

        it('判断类目相关状态:能否新增', async function () {
            await dresClass1.judgeFullClassAssert({ typeId: 1 });
        });

        it('判断类目相关状态:能否删除', async function () {
            await dresClass1.judgeFullClassAssert({ typeId: 2 });
        });

        it('判断类目相关状态:下属跳转', async function () {
            await dresClass1.judgeFullClassAssert({ typeId: 3 });
        });

        it('判断类目相关状态:关联货品', async function () {
            await dresClass1.judgeFullClassAssert({ typeId: 4 });
        });

        it('买家分类列表树(小程序)', async function () {
            await ssReq.userLoginWithWx();
            await dresClass1.findDresSpuClassTreeByBuyerAssert();
        });

        describe('编辑', async function () {
            before('编辑', async function () {
                await sellerShop.switchSeller();
                await dresClass1.updateFullClass({
                    id: dresClass1.id,
                    name: `1级类别修改${common.getRandomStr(5)}`,
                });
            });
            it('卖家分类列表树', async function () {
                await dresClass1.dresSpuClassTreeAssert();
            });
        });

        describe('新建二级分类', async function () {
            const dresClass2 = dresClassManage.setupDresClass();
            const level = 2;
            before('新建', async function () {
                await dresClass2.saveFullClass({
                    name: `2级类别${common.getRandomStr(5)}`,
                    parentId: dresClass1.id,
                    level: level
                }, { parentDresClass: dresClass1 });
                console.log(dresClass2);

                dresClass1.subDresClass.add(dresClass2);
            });

            after('删除', async function () {
                await sellerShop.switchSeller();
                await dresClass2.deleteFullClass({ check: false });
            });

            it('二级类目:卖家分类列表树校验', async function () {
                await dresClass2.dresSpuClassTreeAssert();
            });

            it('判断一级类目相关状态:能否新增', async function () {
                await dresClass1.judgeFullClassAssert({ typeId: 1 });
            });

            it('判断一级类目相关状态:能否删除', async function () {
                await dresClass1.judgeFullClassAssert({ typeId: 2 });
            });

            it('判断一级类目相关状态:下属跳转', async function () {
                await dresClass1.judgeFullClassAssert({ typeId: 3 });
            });

            it('判断一级类目相关状态:关联货品', async function () {
                await dresClass1.judgeFullClassAssert({ typeId: 4 });
            });

            it('买家分类列表树(小程序)', async function () {
                await ssReq.userLoginWithWx();
                await dresClass2.findDresSpuClassTreeByBuyerAssert();
            });

            describe('编辑', async function () {
                before('编辑', async function () {
                    await sellerShop.switchSeller();
                    await dresClass2.updateFullClass({
                        id: dresClass2.id,
                        name: `2级类别修改${common.getRandomStr(5)}`,
                    });
                });
                it('卖家分类列表树', async function () {
                    await dresClass2.dresSpuClassTreeAssert();
                });
            });

            describe('新建三级分类', async function () {
                const dresClass3 = dresClassManage.setupDresClass();
                const level = 3;
                before('新建', async function () {
                    await dresClass3.saveFullClass({
                        name: `3级类别${common.getRandomStr(5)}`,
                        classDocId: docData.image.dres[0].docId,
                        parentId: dresClass2.id,
                        level: level
                    }, { parentDresClass: dresClass2 });
                    console.log(dresClass3);

                    dresClass2.subDresClass.add(dresClass3);
                });

                after('删除', async function () {
                    await sellerShop.switchSeller();
                    await dresClass3.deleteFullClass({ check: false });
                });

                it('卖家分类列表树', async function () {
                    await dresClass3.dresSpuClassTreeAssert();
                });

                it('判断三级类目相关状态:能否新增', async function () {
                    await dresClass3.judgeFullClassAssert({ typeId: 1 });
                });

                it('判断三级类目相关状态:能否删除', async function () {
                    await dresClass3.judgeFullClassAssert({ typeId: 2 });
                });

                it('判断三级类目相关状态:下属跳转', async function () {
                    await dresClass3.judgeFullClassAssert({ typeId: 3 });
                });

                it('判断三级类目相关状态:关联货品', async function () {
                    await dresClass3.judgeFullClassAssert({ typeId: 4 });
                });

                it('买家分类列表树(小程序)', async function () {
                    await ssReq.userLoginWithWx();
                    await dresClass3.findDresSpuClassTreeByBuyerAssert();
                });
                describe('新增商品绑定三级分类', async function () {
                    let newDres, sellerInfo;
                    before('保存商品', async function () {
                        await ssReq.ssSellerLogin();
                        sellerInfo = _.cloneDeep(LOGINDATA);
                        await dresManage.prePrepare();
                        const json = basicJson.styleJson({ classId: dresClass3.id });
                        console.log(`json=${JSON.stringify(json)}`);

                        newDres = await sp.spdresb.saveFull(json);
                    });

                    it('卖家查看商品列表', async function () {
                        const addSpu = await sp.spdresb.findSellerSpuList({ outSpuId: newDres.result.data.spuId }).then(res => res.result.data.rows[0]);
                        const exp = newDres.result.data;
                        common.isApproximatelyEqualAssert(exp, addSpu);
                    });
                    it('买家查看商品详情', async function () {
                        await ssReq.userLoginWithWx();
                        const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: newDres.result.data.spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                        const exp = newDres.result.data;
                        // console.log('买家商品详情', dresDetail);
                        common.isApproximatelyEqualAssert(exp, dresDetail.spu);
                    });

                });

                describe('编辑', async function () {
                    before('编辑', async function () {
                        await sellerShop.switchSeller();
                        await dresClass3.updateFullClass({
                            id: dresClass3.id,
                            classDocId: docData.image.dres[1].docId,
                            name: `3级类别修改${common.getRandomStr(5)}`,
                        });
                    });
                    it('卖家分类列表树', async function () {
                        await dresClass3.dresSpuClassTreeAssert();
                    });
                });

                describe('新增分类到多个商品中', async function () {
                    let sellerInfo;
                    before(async function () {
                        await ssReq.ssSellerLogin();
                        sellerInfo = _.cloneDeep(LOGINDATA);
                        await dresClass3.addSpuForClass({ spuIds });
                        await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                    });
                    it('卖家查询商品详情', async function () {
                        for (const spuId of spuIds) {
                            const dresDetail = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
                            expect(dresDetail.spu.classIds).to.includes(dresClass3.id);
                        }
                    });

                    it('判断三级类目相关状态:能否新增', async function () {
                        await dresClass3.judgeFullClassAssert({ typeId: 1 });
                    });

                    it('判断三级类目相关状态:能否删除', async function () {
                        await dresClass3.judgeFullClassAssert({ typeId: 2 });
                    });

                    it('判断三级类目相关状态:下属跳转', async function () {
                        await dresClass3.judgeFullClassAssert({ typeId: 3 });
                    });

                    it('判断三级类目相关状态:关联货品', async function () {
                        await dresClass3.judgeFullClassAssert({ typeId: 4 });
                    });

                    describe('对买家设置2级分类类目-隐藏', async function () {
                        before(async function () {
                            await dresClass2.hideDresClass({ hideFlag: 1 });
                        });
                        it('卖家分类列表树-2级分类校验', async function () {
                            await dresClass2.dresSpuClassTreeAssert();
                        });
                        it('卖家分类列表树-3级分类校验', async function () {
                            await dresClass3.setSonClassSpuNum(dresClass2.sonSpuNums);
                            await dresClass3.dresSpuClassTreeAssert();
                        });
                        it('买家分类列表树-2级分类校验', async function () {
                            await ssReq.userLoginWithWx();
                            await dresClass2.findDresSpuClassTreeByBuyerAssert();
                        });

                    });

                    describe('对买家设置2级分类类目-不隐藏', async function () {
                        before(async function () {
                            await sellerShop.switchSeller();
                            await dresClass2.hideDresClass({ hideFlag: 0 });
                            await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                        });
                        it('卖家分类列表树-2级分类校验', async function () {
                            await dresClass2.dresSpuClassTreeAssert();
                        });
                        it('卖家分类列表树-3级分类校验', async function () {
                            await dresClass3.setSonClassSpuNum(dresClass2.sonSpuNums);
                            await dresClass3.dresSpuClassTreeAssert();
                        });
                        it('买家分类列表树-2级分类校验', async function () {
                            await ssReq.userLoginWithWx();
                            await dresClass2.findDresSpuClassTreeByBuyerAssert();
                        });
                        it('买家分类列表树-3级分类校验', async function () {
                            await dresClass3.findDresSpuClassTreeByBuyerAssert();
                        });
                    });
                });

                describe('新增多个分类到商品中', async function () {
                    before(async function () {
                        await sellerShop.switchSeller();
                        await dresClass3.addClassForSpu({ spuId: spuIds[0] });
                    });
                    it('卖家查询商品详情', async function () {
                        const dresDetail = await sp.spdresb.getFullById({ id: spuIds[0] }).then(res => res.result.data);
                        expect(dresDetail.spu.classIds).to.includes(dresClass3.id);
                    });
                });

                describe('新建同名分类', async function () {
                    const msg = '同一分类层级下分类名称不可重复，请确认';
                    it('同一分类层级下新建同名分类', async function () {
                        const res = await ss.spdresb.saveFullClass({
                            check: false,
                            name: dresClass3.name,
                            parentId: dresClass2.id,
                            level: 3
                        });
                        console.log(`\n 同一分类层级下新建同名分类=${JSON.stringify(res)}`);
                        common.isApproximatelyEqualAssert({ msg: msg, msgId: 'class_name_repeat_error' }, res.result);
                    });
                    describe('不同一分类层级下新建同名分类', async function () {
                        const dresClassSameName = dresClassManage.setupDresClass();
                        before('新建', async function () {
                            await dresClassSameName.saveFullClass({
                                name: dresClass3.name,
                                parentId: dresClass1.id,
                                level: 2
                            }, { parentDresClass: dresClass1 });
                            console.log(dresClassSameName);
                        });

                        after('删除', async function () {
                            await dresClassSameName.deleteFullClass();
                        });

                        it('卖家分类列表树', async function () {
                            await dresClassSameName.dresSpuClassTreeAssert();
                        });
                    });
                });

                describe('删除分类', async function () {
                    const msg = '当前分类存在有效的子分类，请先删除下属子分类，再进行删除';
                    it('删除存在有效的子分类', async function () {
                        const res = await dresClass1.deleteFullClass({ check: false });
                        common.isApproximatelyEqualAssert({ msg: msg, msgId: msg }, res.result);
                    });
                    describe('删除不存在有效的子分类', async function () {
                        before(async function () {
                            await dresClass3.deleteFullClass();
                        });
                        it('卖家查询商品详情', async function () {
                            for (const spuId of spuIds) {
                                const dresDetail = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
                                expect(dresDetail.spu.classIds).not.to.includes(dresClass3.id);
                            }
                        });
                    });
                });
            });
        });
    });

    describe('类目顺序', async function () {
        let dresClasses = [], num = 3;
        before('设置类目顺序', async function () {
            await sellerShop.switchSeller();
            for (let index = 0; index < num; index++) {
                const dresClass = dresClassManage.setupDresClass();
                await dresClass.saveFullClass({ name: `1级类别${common.getRandomStr(5)}`, parentId: 0, level: 1 });
                console.log(dresClass);
                dresClasses.push(dresClass);
            }
            const showOrders = [];
            dresClasses.forEach((dresClass, index) => {
                const showOrder = num - index;
                showOrders.push({ id: dresClass.id, showOrder: showOrder });
                dresClass.showOrder = showOrder;
            });
            const res = await ss.spdresb.updateClassShowOrder({ showOrders: showOrders });
            console.log(`\n 编辑类目顺序=${JSON.stringify(res)}`);
        });
        after('删除', async function () {
            await sellerShop.switchSeller();
            for (const dresClass of dresClasses) await dresClass.deleteFullClass({ check: false });
        });
        it('卖家分类列表树', async function () {
            for (const dresClass of dresClasses) await dresClass.dresSpuClassTreeAssert();
        });
        it('卖家分类列表树顺序', async function () {
            const dresSpuClassTree = await dresClasses[0].findDresSpuClassTree();
            esSearchHelp.sortAssert(dresSpuClassTree, 'showOrder', false);
        });
        it('买家分类列表树(小程序)', async function () {
            await ssReq.userLoginWithWx();
            const dresSpuClassTree = await dresClasses[0].findDresSpuClassTreeByBuyer();
            // esSearchHelp.sortAssert(dresSpuClassTree, 'showOrder', false);
        });
        describe('排序', async function () {
            const dresClass2 = dresClassManage.setupDresClass();
            let dresClass1, dresClasses2 = [], num = 3, level = 2;
            before('新建', async function () {
                await sellerShop.switchSeller();
                dresClass1 = dresClasses[0];
                for (let index = 0; index < num; index++) {
                    const dresClass = dresClassManage.setupDresClass();
                    await dresClass.saveFullClass({ name: `2级类别${common.getRandomStr(5)}`, parentId: dresClass1.id, level: level });
                    console.log(dresClass);
                    dresClasses2.push(dresClass);
                    dresClass1.subDresClass.add(dresClass);
                }
                const showOrders = [];
                dresClasses2.forEach((dresClass, index) => {
                    const showOrder = num - index;
                    showOrders.push({ id: dresClass.id, showOrder: showOrder });
                    dresClass.showOrder = showOrder;
                });
                const res = await ss.spdresb.updateClassShowOrder({ showOrders: showOrders });
                console.log(`\n 编辑类目顺序=${JSON.stringify(res)}`);

                const dresClass2 = dresClasses2.find(obj => obj.showOrder == 1);
                console.log(dresClass2);
                const dresClass3 = dresClassManage.setupDresClass();
                await dresClass3.saveFullClass({
                    name: `3级类别${common.getRandomStr(5)}`,
                    classDocId: docData.image.dres[0].docId,
                    parentId: dresClass2.id,
                    level: 3
                }, { parentDresClass: dresClass2 });
                dresClass2.subDresClass.add(dresClass3);
                console.log(dresClass2);
            });
            it('买家分类列表树(小程序)', async function () {
                await ssReq.userLoginWithWx();
                const dresSpuClassTree = await dresClasses[0].findDresSpuClassTreeByBuyer({ parentId: dresClass1.id });
                dresClassManage.sortSpuClassTreeByBuyerAssert(dresSpuClassTree);
            });
        });
    });

    describe('增量增加/移除商品到分类中', async function () {
        const dresClass11 = dresClassManage.setupDresClass();
        const level = 1;
        before('新增分类', async function () {
            await sellerShop.switchSeller();
            await dresClass11.saveFullClass({ name: `1级类别${common.getRandomStr(5)}`, parentId: 0, level: level });
        });
        after('删除', async function () {
            await sellerShop.switchSeller();
            await dresClass11.deleteFullClass({ check: false });
        });
        describe('增量增加商品到分类中', async function () {
            let sellerInfo;
            before('增量增加商品到分类中', async function () {
                await ssReq.ssSellerLogin();
                sellerInfo = _.cloneDeep(LOGINDATA);
                await dresClass11.addSpuToClass({ spuIds: spuIds, typeId: 0 });
                await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
            });

            it('卖家分类列表树', async function () {
                await dresClass11.setSonClassSpuNum(0);
                await dresClass11.dresSpuClassTreeAssert();
            });

            it('买家分类列表树(小程序)', async function () {
                await ssReq.userLoginWithWx();
                await dresClass11.findDresSpuClassTreeByBuyerAssert();
            });
            describe('分类无商品隐藏分类', async function () {
                let sellerInfo;
                before('删除类目里的spu', async function () {
                    await ssReq.ssSellerLogin();
                    sellerInfo = _.cloneDeep(LOGINDATA);
                    await dresClass11.updateFullClass({ id: dresClass11.id, name: dresClass11.name, spuIds: '' });
                    console.log('打印有效款数', dresClass11.validNum);

                    await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                });
                after('切换为卖家', async function () {
                    await ssReq.ssSellerLogin();
                });
                it('卖家分类列表树-1级分类校验', async function () {
                    // const res = await ss.spdresb.findDresSpuClassTree({ nameLike: '1级类别' }).then(res => res.result.data.rows.id == dresClass1.id);
                    // expect(res, '竟然没有隐藏类目').to.be.false;
                    // console.log('列表树', res);
                    await dresClass11.dresSpuClassTreeAssert();
                });
                it('买家分类列表树-1级分类校验', async function () {
                    await ssReq.userLoginWithWx();
                    // const res = await ss.spdresb.findDresSpuClassTreeByBuyer({ sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, nameLike: '1级类别' }).then(res => res.result.data.rows.id == dresClass1.id);
                    // expect(res, '竟然没有隐藏类目').to.be.false;
                    // console.log('列表树', res);
                    await dresClass11.findDresSpuClassTreeByBuyerAssert();
                });
            });
        });

        describe('新建二级分类', async function () {
            const dresClass22 = dresClassManage.setupDresClass();
            const level = 2;
            before('新建', async function () {
                await sellerShop.switchSeller();

                await dresClass22.saveFullClass({
                    name: `2级类别${common.getRandomStr(5)}`,
                    parentId: dresClass11.id,
                    level: level
                }, { parentDresClass: dresClass11 });

                dresClass11.subDresClass.add(dresClass22);
            });

            after('删除', async function () {
                await sellerShop.switchSeller();
                await dresClass11.deleteFullClass({ check: false });
            });

            it('二级类目:卖家分类列表树校验', async function () {
                await dresClass22.dresSpuClassTreeAssert();
            });
            it('二级类目:买家分类列表树(小程序)', async function () {
                await ssReq.userLoginWithWx();
                await dresClass11.findDresSpuClassTreeByBuyerAssert();
            });
            describe('增量增加商品到分类中', async function () {
                before('增量增加商品到分类中', async function () {
                    await ssReq.ssSellerLogin();
                    sellerInfo = _.cloneDeep(LOGINDATA);
                    await dresClass22.addSpuToClass({ spuIds: spuIds, typeId: 0 });
                    //同步
                    await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                });

                it('卖家查询商品列表', async function () {
                    for (spuId of spuIds) {
                        const dresListRes = await sp.spdresb.findSellerSpuList({ outSpuId: spuId, priClassId: dresClass22.id }).then(res => res.result.data.rows.find(data => data.id == spuId));
                        expect(dresListRes.classIds, `商品列表中查询不到新增分类的商品`).to.be.includes(dresClass22.id);
                    }
                });

                it('卖家查询商品详情', async function () {
                    for (spuId of spuIds) {
                        const dresDetail = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
                        expect(dresDetail.spu.classIds, `增量增加商品到分类失败`).to.be.includes(dresClass22.id);
                    }
                });
                describe('分类无商品隐藏分类', async function () {
                    let sellerInfo;
                    before('删除类目里的spu', async function () {
                        await ssReq.ssSellerLogin();
                        sellerInfo = _.cloneDeep(LOGINDATA);
                        await dresClass22.updateFullClass({ id: dresClass22.id, name: dresClass22.name, spuIds: '' });
                        await sp.spdresb.startExportEs({ syncType: 1, unitId: sellerInfo.unitId, _cid: sellerInfo.clusterCode });
                    });
                    after('切换为卖家', async function () {
                        await ssReq.ssSellerLogin();
                    });
                    it('卖家分类列表树-2级分类校验', async function () {
                        await ssReq.ssSellerLogin();
                        // const res = await ss.spdresb.findDresSpuClassTree({ nameLike: '2级类别' }).then(res => res.result.data.rows.id == dresClass2.id);
                        // expect(res, '竟然没有隐藏类目').to.be.false;
                        // console.log('列表树', res);
                        await dresClass22.dresSpuClassTreeAssert();
                    });
                    it('买家分类列表树-2级分类校验', async function () {
                        await ssReq.userLoginWithWx();
                        // const res = await ss.spdresb.findDresSpuClassTreeByBuyer({ sellerId: sellerInfo.tenantId, sellerUnitId: sellerInfo.unitId, nameLike: '2级类别' }).then(res => res.result.data.rows.id == dresClass2.id);
                        // expect(res, '竟然没有隐藏类目').to.be.false;
                        // console.log('列表树', res);
                        await dresClass22.findDresSpuClassTreeByBuyerAssert();
                    });
                });
            });
        });


    });

});