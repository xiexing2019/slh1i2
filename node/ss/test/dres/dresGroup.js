const common = require('../../../lib/common');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const dresSpuGroup = require('../../help/spdresb/dresSpuGroup');


describe('商品分组', function () {
    this.timeout(TESTCASE.timeout);

    let spuIds = [], len = 3, sellerInfo;
    const newGroup = dresSpuGroup.setupDresSpuGroup();
    before('新增分组', async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        //准备商品数据
        const dresList = await spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
        dresList.slice(0, len).forEach(dresSpu => spuIds.push(dresSpu.id));

        // 新增分组
        await newGroup.addGroup({ groupName: `分组${common.getRandomStr(5)}`, dresSpuIds: spuIds.toString() });
    });

    it('查询分组列表', async () => {
        const groupInfo = await newGroup.findGroupInfoFromList();
        console.log(`groupInfo=${JSON.stringify(groupInfo)}`);
        common.isApproximatelyEqualAssert(newGroup.getListExp(), groupInfo);
    });
    it('分组详情', async () => {
        const groupInfo = await newGroup.findGroupInfo();
        console.log(`groupInfo=${JSON.stringify(groupInfo)}`);
        common.isApproximatelyEqualAssert(newGroup.getDetailExp(), groupInfo);
    });

    describe('更新分组', function () {
        before('更新分组', async () => {
            spuIds.pop();
            await newGroup.addGroup({ groupName: `分组修改${common.getRandomStr(5)}`, dresSpuIds: spuIds.toString() });
        });
        it('查询分组列表', async () => {
            const groupInfo = await newGroup.findGroupInfoFromList();
            common.isApproximatelyEqualAssert(newGroup.getListExp(), groupInfo);
        });
        it('分组详情', async () => {
            const groupInfo = await newGroup.findGroupInfo();
            common.isApproximatelyEqualAssert(newGroup.getDetailExp(), groupInfo);
        });
    });

    describe('更新分组展示位', async function () {
        before('更新分组展示位', async () => {
            await newGroup.updateShowPlace({ showPlace: common.getRandomNum(1, 3) });
        });
        it('查询分组列表', async () => {
            const groupInfo = await newGroup.findGroupInfoFromList();
            common.isApproximatelyEqualAssert(newGroup.getListExp(), groupInfo);
        });
        it('分组详情', async () => {
            const groupInfo = await newGroup.findGroupInfo();
            common.isApproximatelyEqualAssert(newGroup.getDetailExp(), groupInfo);
        });
    });

    describe('添加商品到分组', function () {
        let dres, dresSpuGroupIds = [];
        before('添加商品到分组', async () => {
            const dresList = await spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
            dres = dresList[0];
            const groupList = await ss.spdresb.listGroup().then((res) => res.result.data.rows);
            groupList.slice(0, len).forEach(group => dresSpuGroupIds.push(group.id));
            await ss.spdresb.addDresSpus({ dresSpuId: dres.id, dresSpuGroupIds: dresSpuGroupIds.toString() })
        });

        it('货品详细', async () => {
            const dresInGroups = await spdresb.getFullById({ id: dres.id }).then((res) => res.result.data.spu.groupInfo.groupIds);
            common.isArrayContainArrayAssert(dresSpuGroupIds, dresInGroups);
        });

        it('分组货品详情', async () => {
            for (const groupId of dresSpuGroupIds) {
                const dresGroupDetailList = await ss.spdresb.listDresSpuGroupDetail({ id: groupId }).then((res) => res.result.data.rows);
                expect(dresGroupDetailList.find(obj => obj.id == dres.id), `id为${groupId}分组列表没找到了id为${dres.id}的商品`).to.not.be.undefined;
            }
        });
    });

    describe('商品分组详情', function () {
        let groupDetail;
        before('卖家查询商品分组详情', async () => {
            groupDetail = await ss.spdresb.listDresSpuGroupDetail({ id: newGroup.id }).then((res) => res.result.data.rows);
            console.log(`groupDetail=${JSON.stringify(groupDetail)}`);
            await ssReq.userLoginWithWx();
        });
        it('买家查询商品分组详情', async () => {
            const buyerGroupDetail = await ss.spdresb.getSpuListByGroupId({ groupId: newGroup.id, buyerId: LOGINDATA.tenantId, tenantId: sellerInfo.tenantId }).then((res) => res.result.data.rows);
            console.log(`buyerGroupDetail=${JSON.stringify(buyerGroupDetail)}`);
            common.isApproximatelyArray(buyerGroupDetail, groupDetail, ['marketDays']);
        });
    });

    describe('删除分组', function () {
        before('删除分组', async () => {
            await ssReq.ssSellerLogin();
            await newGroup.deleteGroup();
        });
        it('查询分组列表', async () => {
            const groupInfo = await ss.spdresb.listGroup().then((res) => res.result.data.rows.find(obj => obj.id == newGroup.id));
            expect(groupInfo, `分组列表居然找到了id为${newGroup.id}的商品`).to.be.undefined;
        });
        it('分组详情', async () => {
            const groupInfo = await newGroup.findGroupInfo();
            common.isApproximatelyEqualAssert(newGroup.getDetailExp(), groupInfo);
        });
    });

});