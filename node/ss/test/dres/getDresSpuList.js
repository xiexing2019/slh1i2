const spchb = require('../../../reqHandler/ss/spb/spchb');
const spdresb = require('../../../reqHandler/sp/biz_server/spdresb');
const ssReq = require('../../help/ssReq');
const dresManager = require('../../help/dresManage');
const basicJson = require('../../help/basicJson');
const common = require('../../../lib/common');

describe.skip('过滤查询全部商品列表（买家用）接口', async function () {
    this.timeout(30000);
    const newDresList = [];
    let sellerInfo, newDresListIds;
    before('卖家登录，创建货品', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        await dresManager.prePrepare();

        for (let i = 0; i < 2; i++) {
            const dres = dresManager.setupDres();
            const json = basicJson.styleJson({ brandId: BASICDATA['606'][0].codeValue, season: BASICDATA['613'][0].codeValue });
            await dres.saveDres(json);
            newDresList.push(dres);
        }
        for (let i = 0; i < 2; i++) {
            const dres = dresManager.setupDres();
            const json = basicJson.styleJson({ brandId: BASICDATA['606'][1].codeValue, season: BASICDATA['613'][1].codeValue });
            await dres.saveDres(json);
            newDresList.push(dres);
        }
        for (let i = 0; i < 2; i++) {
            const dres = dresManager.setupDres();
            const json = basicJson.styleJson({ brandId: BASICDATA['606'][2].codeValue, season: BASICDATA['613'][2].codeValue });
            await dres.saveDres(json);
            newDresList.push(dres);
        }
        newDresListIds = newDresList.map(data => data.id);

        await ssReq.userLoginWithWx();
    });
    it('单独查询某一个品牌的商品记录', async function () {
        const dresList = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, brandIds: BASICDATA['606'][0].codeValue, orderBy: 'created_date', orderByDesc: true }).then(res => res.result.data.rows);
        const spuIds = dresList.map(dres => dres.id);
        common.isArrayContainArrayAssert(newDresListIds.slice(0, 2), spuIds);
    });
    it('合并查询2个品牌的商品记录', async function () {
        const dresList = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, brandIds: `${BASICDATA['606'][0].codeValue},${BASICDATA['606'][1].codeValue}`, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        const spuIds = dresList.map(dres => dres.id);
        common.isArrayContainArrayAssert(newDresListIds.slice(0, 4), spuIds);
    });
    it('单独查询某一个季节的商品记录', async function () {
        const dresList = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, season: BASICDATA['613'][0].codeValue, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        const spuIds = dresList.map(dres => dres.id);
        common.isArrayContainArrayAssert(newDresListIds.slice(0, 2), spuIds);
    });
    it('合并查询某2个季节的商品记录', async function () {
        const dresList = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, seasons: `${BASICDATA['613'][0].codeValue},${BASICDATA['613'][1].codeValue}`, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        const spuIds = dresList.map(dres => dres.id);
        common.isArrayContainArrayAssert(newDresListIds.slice(0, 4), spuIds);
    });
    it('组合查询2个品牌+1个季节的商品记录', async function () {
        const dresList = await spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, brandIds: `${BASICDATA['606'][0].codeValue},${BASICDATA['606'][1].codeValue}`, season: BASICDATA['613'][0].codeValue, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        const spuIds = dresList.map(dres => dres.id);
        common.isArrayContainArrayAssert(newDresListIds.slice(0, 2), spuIds);
    });

});