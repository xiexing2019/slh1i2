const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const dresManage = require('../../help/dresManage');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');


describe('一键补货', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo;
    before(async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        await ssReq.userLoginWithWx();
    });

    it('快速补货列表', async function () {
        const recentBillList = await ss.sppur.findRecentBillSpu({ tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);
        console.log(`\n recentBillList=${JSON.stringify(recentBillList)}`);
        for (const recentBill of recentBillList) {
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: recentBill.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data.spu);
            // dresFull.name = dresFull.title; //recentBill的name属性为dres的title
            common.isApproximatelyEqualAssert(dresFull, recentBill, ['unitId', 'pubPrice', 'coverUrl', 'marketDays', 'code', 'name'], `补货:${JSON.stringify(recentBill)}\n spu:${JSON.stringify(dresFull)}`);
        };
    });
});