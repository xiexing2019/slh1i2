const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const dresSpuGroup = require('../../help/spdresb/dresSpuGroup');
const dresManager = require('../../../../node/ss/help/dresManage');
const basicJson = require('../../../../node/ss/help/basicJson');
const spdresup = require('../../../../node/reqHandler/ss/spb/spdresup');
const spmdm = require('../../../../node/reqHandler/ss/spb/spmdm');

//提测单1235  使用分组装修的时候同样展示价格对比
describe('商品分组id获取商品列表，查看默认价和VIP价比对', function () {
    this.timeout(TESTCASE.timeout);
    const group = dresSpuGroup.setupDresSpuGroup();
    const dresList = [];
    const spuList = [];
    let clientInfo;
    let sellerInfo;

    before('数据准备：新建2个货品、新建分组、2货品归入该分组、应用模板1加入该分组、客户改适用价为vip价', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        //新建2个货品
        await dresManager.prePrepare();
        for (let i = 0; i < 2; i++) {
            const dres = dresManager.setupDres();
            const json = basicJson.styleJson();
            await dres.saveDres(json);
            dresList.push(dres);

        }

        // 新建分组
        await group.addGroup({ groupName: `新分组${common.getRandomStr(5)}`, dresSpuIds: dresList.map(data => data.id).toString() });
        //模板一添加分组操作，把刚才新建的分组加入进去
        await spdresup.savePageFragment({ content: { homeData: { data: { bizType: 5, data: { id: group.id, name: group.name } } } } })
        //客户登录，让卖家得到客户访客信息
        await ssReq.userLoginWithWx();
        clientInfo = _.cloneDeep(LOGINDATA);
        //卖家再登录，改客户适用价为‘会员价1’，
        await ssReq.ssSellerLogin();
        await spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: 1 })
    });

    after(async function () {
        await ssReq.ssSellerLogin();
        await spmdm.batchUpdateMemberValidPriceAndFlag({ custTenantId: clientInfo.tenantId, validPrice: 0 })
    });

    it('用户登录小程序查看首页分组列表价格展示，defaultPrice应更新为原默认价，pubPrice更新为会员价1', async function () {
        await ssReq.userLoginWithWx();
        let groupSpuList = await ss.spdresb.getSpuListByGroupId({ groupId: group.id, buyerId: clientInfo.tenantId, tenantId: sellerInfo.tenantId }).then(res => res.result.data.rows);

        console.log(JSON.stringify(groupSpuList[0]));

        //两个商品的defaultPirce应该都更新为默认价
        expect(groupSpuList[0].defaultPrice).to.be.equal(dresList[0].spu.pubPrice);
        expect(groupSpuList[1].defaultPrice).to.be.equal(dresList[1].spu.pubPrice);

        //两个商品的pubPrice应该更新为会员价1
        expect(groupSpuList[0].pubPrice).to.be.equal(dresList[0].spu.price1);
        expect(groupSpuList[1].pubPrice).to.be.equal(dresList[1].spu.price1);
    });

})