const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const billManage = require('../../help/billManage');
const cartManage = require('../../help/cartManage');
const dresManage = require('../../help/dresManage');


describe('商品', function () {
	this.timeout(TESTCASE.timeout);
	let sellerInfo, styleRes, styleFullRes, classId, updateRes;
	const dres = dresManage.setupDres();

	before('卖家登录后保存商品', async () => {
		await ssReq.ssSellerLogin();
		// console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
		sellerInfo = _.cloneDeep(LOGINDATA);

		// 初始化商品基础信息
		await dresManage.prePrepare();

		//保存商品
		const json = basicJson.styleJson({ classId: classId });
		// console.log(`json=${JSON.stringify(json)}`);
		await dres.saveDres(json);
		// console.log(dres);
	});

	//卖家查询
	it('卖家查询商品列表', async function () {
		const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
		console.log(`dresFull=${JSON.stringify(dresSpu)}`);
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('卖家查询租户商品信息', async function () {
		const dresFull = await dres.getFullById();
		// console.log(`dresFull=${JSON.stringify(dresFull)}`);
		common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
	});

	// it('视频转码优化', async function () {
	// 	await dres.upgradeDressVideo();
	// });

	it('买家查询商品列表', async function () {
		await ssReq.userLoginWithWx();

		const dresSpu = await dres.findBuyerSpuList();
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('买家查询商品详情', async function () {
		const dresFull = await dres.getDetailForBuyer();
		common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
	});

	it.skip('3.修改spu', async function () {
		const editRes = await sp.spdresb.editDresSpu({
			id: styleRes.result.data.spuId,
			isAllowReturn: 1,
			rem: `修改spu备注${common.getRandomStr(3)}`
		});
		let getFullByIdRes = await sp.spdresb.getFullById({ id: editRes.params.jsonParam.id });
		common.isApproximatelyEqualAssert(getFullByIdRes.result.data.spu, editRes.params.jsonParam);
	});

	describe.skip('覆盖更新sku库存', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			const skus = [...dres.skus.keys()].slice(0, 2).map(ele => { return { code: ele, num: common.getRandomNum(1000, 2000) } });
			// console.log(`skus=${JSON.stringify(skus)}`);
			await dres.updateDresSkuNum({ skus });
		});
		it('卖家查询商品详情', async function () {
			const dresFull = await dres.getFullById();
			common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
		});
	});

	// 暂时去除  skus更新需要重构 dev分支解决
	describe.skip('卖家修改商品信息', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			const updateJson = basicJson.styleJson({ classId });
			updateJson.id = updateJson.spu.id = dres.id;
			const _skus = [...dres.skus.values()];
			updateJson.skus.forEach((data, index) => {
				data.id = _skus[index].id;
				data.num = common.add(data.num, 1000);
			});
			await dres.saveDres(updateJson);
			// console.log(dres);
		});
		it('卖家查询租户商品信息', async function () {
			// const dresFull = await dres.getFullById();
			// console.log(`dresFull=${JSON.stringify(dresFull)}`);
			// // 更新商品信息 补充保存时jsonParam中没有涉及的字段
			// const dresExp = _.cloneDeep(dres);
			// dres.setByDetail(dresFull);
			// common.isApproximatelyEqualAssert(dresExp.getDetailExp(), dresFull);
		});
		it('卖家查询商品列表', async function () {
			const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
			common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['namePy', 'stockNum', 'num']);
		});
		it('买家查询商品列表', async function () {
			await ssReq.userLoginWithWx();

			const dresSpu = await dres.findBuyerSpuList();
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品详情', async function () {
			const dresFull = await dres.getDetailForBuyer();
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data, ['namePy', 'stockNum', 'num']);
		});
	});

	it.skip('5.修改商品信息', async function () {
		let updateJson = basicJson.styleJson({ classId: classId });
		updateJson.id = updateJson.spu.id = styleRes.result.data.spuId;

		updateJson.skus.forEach((res) => {
			res.num += 1000;
		});
		updateRes = await sp.spdresb.saveFull(updateJson);

		let dresInfo = await sp.spdresb.getFullById({ id: styleRes.result.data.spuId });

		common.isApproximatelyEqualAssert(updateRes.params.jsonParam, dresInfo.result.data, ['id']);
	});

	describe('按手购买设置', async function () {
		before('设置按手购买价格类型', async function () {
			await ssReq.ssSellerLogin();
			// 价格类型 402
			const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
			const priceTypes = dictList.slice(0, 2).map(ele => ele.codeValue);
			await dres.updateSpuForSeller({ pricesInHand: { priceTypes: priceTypes.join() } });
		});
		it('卖家查询商品详情', async function () {
			const dresDetail = await dres.getFullById();
			// console.log(`dresDetail=${JSON.stringify(dresDetail)}`);
			common.isApproximatelyEqualAssert(dres.getDetailExp(), dresDetail, ['stockNum']);
		});
		it('买家查询商品详情', async function () {
			await ssReq.userLoginWithWx();
			const dresDetail = await dres.getDetailForBuyer();
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresDetail.result.data, ['stockNum', 'num']);
		});
	});

	describe('货品起售数量', async function () {
		before('设置商品起售数', async function () {
			await ssReq.ssSellerLogin();
			// 价格类型 402
			const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
			const priceTypes = dictList.slice(0, 2).map(ele => ele.codeValue);
			await dres.updateSpuForSeller({ pricesStart: { priceTypes: priceTypes.join(), buyNums: common.getRandomNum(2, 5) } });
		});
		it('卖家查询商品详情', async function () {
			const dresDetail = await dres.getFullById();
			common.isApproximatelyEqualAssert(dres.getDetailExp(), dresDetail, ['stockNum']);
		});
		it('买家查询商品详情', async function () {
			await ssReq.userLoginWithWx();
			const dresDetail = await dres.getDetailForBuyer();
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresDetail.result.data, ['stockNum', 'num']);
		});
	});

	describe.skip('商品下架', function () {

		describe('卖家商品下架-test', function () {
			let clientInfo, purRes;

			before('卖家下架商品', async function () {
				await ssReq.userLoginWithWx();
				await common.delay(500);
				clientInfo = _.cloneDeep(LOGINDATA);
				//获取用户默认收货地址
				await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
				// console.log(LOGINDATA);
				const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: styleRes.result.data.spuId });
				purRes = await billManage.createPurBill(basicJson.purJson({ styleInfo: getFullForBuyer }));

				getFullForBuyer.count = 1;
				await sp.spTrade.emptyCart();
				const saveCartInBatchs = await sp.spTrade.saveCartInBatchs(basicJson.cartJson2(getFullForBuyer));
				// console.log('%j', saveCartInBatchs);

				await ssReq.ssSellerLogin();
				await sp.spdresb.offMarket({
					ids: [styleRes.result.data.spuId]
				});
			});

			it('1.卖家查询商品详情', async function () {
				const res = await sp.spdresb.getFullById({ id: styleRes.result.data.spuId });
				// console.log(`商品详细信息=${JSON.stringify(res)}`);
				expect(res.result.data.spu.flag).to.be.equal(-4);
			});

			it('2.卖家查询商品列表', async function () {
				let dresInfo = await sp.spdresb.getFullById({ id: styleRes.result.data.spuId });
				const res = await sp.spdresb.findSellerSpuList({ pageSize: 20, nameLike: dresInfo.result.data.spu.code, flag: -4 });
				// console.log(res);
				let info = res.result.data.rows.find(element => element.id == styleRes.result.data.spuId);
				expect(info.flag, '商品下架后列表中的flag未改变').to.be.equal(-4);
			});

			//卖家自己下架 spuFlag =-4，管理员下架 spuFlag=-2
			it('3.购物车列表查看', async function () {
				// console.log(`styleRes=${JSON.stringify(styleRes)}`);
				await ssReq.userLoginWithWx();
				await common.delay(1000);
				let cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 });
				cartList.result.data.rows[0].carts.forEach(res =>
					expect(res.spuFlag).to.be.equal(-4));
				await sp.spTrade.emptyCart();
			});

			it('5.下架后，已经生成的采购单后续操作', async function () {
				await ssReq.userLoginWithWx();
				let payRes = await sp.spTrade.createPay({ orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money });

				await sp.spTrade.receivePayResult({ mainId: payRes.result.data.payDetailId, amount: payRes.params.jsonParam.payMoney, purBillId: purRes.result.data.rows[0].billId });

				await ssReq.ssSellerLogin();
				await common.delay(500);
				const qlRes = await sp.spTrade.salesFindBills({ pageSize: 10, pageNo: 1, orderBy: 'proTime', orderByDesc: true, statusType: 2 });
				const salesListRes = qlRes.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo);

				const qfRes = await sp.spTrade.salesFindBillFull({ id: salesListRes.bill.id });
				const details = qfRes.result.data.skus.map((sku) => {
					return { salesDetailId: sku.id, num: sku.skuNum };
				});
				const logisList = await sp.spconfb.findLogisList1({ cap: 1 });
				await sp.spTrade.deliverSalesBill({
					main: {
						logisCompId: logisList.result.data.rows[0].id,
						waybillNo: common.getRandomNumStr(12),
						buyerId: clientInfo.tenantId,
						shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
						hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
					},
					details: details
				});
				await ssReq.userLoginWithWx();
				await sp.spTrade.confirmReceipt({ purBillIds: [purRes.result.data.rows[0].billId] });
			});

			//重新保存商品是不能上架商品的
			it('6.重新上架--保存商品重新上架', async function () {
				await ssReq.ssSellerLogin();
				let saveFull = _.cloneDeep(styleRes.params.jsonParam);
				saveFull.id = styleRes.result.data.spuId;
				saveFull.spu.id = styleRes.result.data.spuId;
				//console.log(`saveFull=${JSON.stringify(saveFull)}`);
				let saveStyleRes = await sp.spdresb.saveFull(saveFull);
				const res = await sp.spdresb.getFullById({ id: saveStyleRes.result.data.spuId });
				expect(res.result.data.spu.flag, '重新保存下架的商品又重新上架了').to.be.equal(-4);
			});

			describe('卖家重新上架商品', async function () {
				before(async function () {
					await sp.spdresb.onMarket({
						ids: [styleRes.result.data.spuId]
					});
				});
				it('卖家查询商品状态', async function () {
					const dresFull = await sp.spdresb.getFullById({ id: styleRes.result.data.spuId }).then(res => res.result.data);
					expect(dresFull.spu.flag, '上架失败').to.be.equal(1);
				});
			});
		});

	});

});

describe('批量设置起批量、起订数', async function () {
	this.timeout(TESTCASE.timeout);
	let sellerInfo, dresList, priceTypes, spuIds = [], shoppingCart;
	before('获取商品列表,价格，加入购物车', async function () {
		await ssReq.ssSellerLogin();
		sellerInfo = _.cloneDeep(LOGINDATA);
		dresList = await ssReq.getDresList(2);
		// console.log(dresList);
		dresList.forEach(dres => spuIds.push(dres.id));

		// 获取价格类型字典
		const dictList = await ss.config.getSpbDictList({ typeId: 402, flag: 1 }).then(res => res.result.data.rows);
		priceTypes = dictList.slice(0, 7).map(ele => ele.codeValue);

		//加入购物车
		await ssReq.userLoginWithWx();
		shoppingCart = cartManage.setupShoppingCart(sellerInfo);
		await shoppingCart.emptyCart();
		for (const dres of dresList) {
			const dresFull = await sp.spdresb.getFullForBuyer({ spuId: dres.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
			const cartJson = shoppingCart.makeCartJson(dresFull, { type: 2, count: 1, num: 1 });
			await shoppingCart.saveCartInBatchs(cartJson);
		}
	});
	describe('按手购买', async function () {
		before('设置按手购买价格类型', async function () {
			await ssReq.ssSellerLogin();
			await ss.spdresb.batchSetPricesSalesParams({ spuIds: spuIds.join(), priceTypes: priceTypes.join(), typeId: 1 });
			dresList.forEach(dres => dres.updateSpu({ pricesInHand: { priceTypes: priceTypes.join() } }));
		});
		it('卖家查询商品详情', async function () {
			for (const dres of dresList) {
				const dresDetail = await dres.getFullById();
				// console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
				common.isApproximatelyEqualAssert(dres.getDetailExp(), dresDetail);
			}
		});
		it('买家查询商品详情', async function () {
			await ssReq.userLoginWithWx();
			for (const dres of dresList) {
				const dresDetail = await dres.getDetailForBuyer();
				common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresDetail.result.data);
			}
		});
	});
	describe('起售数', async function () {
		let buyNums = common.getRandomNum(2, 5);
		before('设置商品起售数', async function () {
			await ssReq.ssSellerLogin();
			await ss.spdresb.batchSetPricesSalesParams({ spuIds: spuIds.join(), priceTypes: priceTypes.join(), typeId: 0, buyNums: buyNums });
			dresList.forEach(dres => dres.updateSpu({ pricesStart: { priceTypes: priceTypes.join(), buyNums: buyNums } }));
		});
		it('卖家查询商品详情', async function () {
			for (const dres of dresList) {
				const dresDetail = await dres.getFullById();
				// console.log(`\ndresDetail=${JSON.stringify(dresDetail)}`);
				common.isApproximatelyEqualAssert(dres.getDetailExp(), dresDetail);
			}
		});
		it('买家查询商品详情', async function () {
			await ssReq.userLoginWithWx();
			for (const dres of dresList) {
				const dresDetail = await dres.getDetailForBuyer();
				common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresDetail.result.data);
			}
		});
		it('买家查询购物车', async function () {
			await shoppingCart.cartListAssert();
			// const cartList = await shoppingCart.getCartList();
			// // console.log(`\ncartList=${JSON.stringify(cartList)}`);
			// for (const dres of dresList) {
			// 	common.isApproximatelyEqualAssert({ spuId: dres.id, startBuyNums: buyNums }, cartList[0].carts.find(obj => obj.spuId == dres.id));
			// }
		});
	});

});

function onMarketListExp(params) {
	let exp = {
		id: params.id,
		spuTitle: params.styleRes.params.jsonParam.spu.title,
		reqRem: params.reqRem,
		flag: params.flag,
		checkRem: params.checkRem ? params.checkRem : '',
		sellerId: params.sellerInfo.tenantId,
		sellerUnitId: params.sellerInfo.unitId
	};
	return exp;
};

function onMarketDetailExp(params) {
	let exp = {
		main: {
			flag: params.flag,
			checkRem: params.checkRem ? params.checkRem : '',
			sellerId: params.sellerInfo.tenantId,
			sellerUnitId: params.sellerInfo.unitId,
			reqRem: params.reqRem,
			spuTitle: params.styleRes.params.jsonParam.spu.title
		}, details:
			[{
				spuId: params.styleRes.result.data.spuId,
				spuTitle: params.styleRes.params.jsonParam.spu.title,
				reqId: params.id
			}]
	};
	return exp;
};

function dataCombine(obj) {
	let spu = _.cloneDeep(obj);
	spu.title = `${spu.title}(${spu.name})`;
	return spu;
};