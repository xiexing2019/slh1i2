const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const dresManage = require('../../help/dresManage');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');


describe('改价', function () {
    this.timeout(TESTCASE.timeout);
    let spuIds = [], dresList = [], priceValues = [], sellerInfo, dresFullBack = [];

    before(async () => {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);

        const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
        priceList.forEach(price => priceValues.push(price.codeValue));
        dresList = await ssReq.getDresList(3);
        // console.log(dresList);
        spuIds = dresList.map(dres => dres.id);
        console.log(spuIds);
    });

    //商品创建时，如价格为0，则原价永远为0，调详情的接口也不会修改
    describe('单个商品改价', async function () {
        let dresFull;
        before(async function () {
            await dresList[0].updateSpuPrice(common.getRandomNum(1, 10));
            dresFull = await dresList[0].getFullById();
        });
        it('商品详情', async function () {
            common.isApproximatelyEqualAssert(dresFull, dresList[0].getDetailExp(), ['ssNum']);
        });
    });

    describe('批量改价', async function () {

        describe('批量改价:折扣价', async function () {
            let discount;
            const dresFullOld = [];
            before(async function () {
                for (let i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    const dres = _.cloneDeep(dresFull);
                    dresFullOld.push(dres);
                };
                discount = common.getRandomNum(1, 9) / 10;
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 0, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (let i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.userLoginWithWx();
                for (let i in spuIds) {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                    const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                }
            });
            describe('批量改价:恢复原价', function () {
                before('恢复原价', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: common.getRandomNum(-10, 10) });
                    await common.delay(1000);
                });
                it('卖家查看商品详情', async function () {
                    for (let i in spuIds) {
                        const dresFull = await dresList[i].getFullById();
                        const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 2);
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                    }
                });
                it('买家端查看商品详情', async function () {
                    await ssReq.userLoginWithWx();
                    for (let i in spuIds) {
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
                        const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 2);
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                    }
                });
            });
        });
        describe('批量改价:优惠价', async function () {
            let discount;
            const dresFullOld = [];
            before(async function () {
                await ssReq.ssSellerLogin();
                for (let i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    const dres = _.cloneDeep(dresFull);
                    dresFullOld.push(dres);
                };
                discount = common.getRandomNum(-20, 20);
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 1, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (const i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 1);
                    common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.userLoginWithWx();
                for (let i in spuIds) {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 1);
                    common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                }
            });
            describe('批量改价:恢复原价', function () {
                before('恢复原价', async function () {
                    await ssReq.ssSellerLogin();
                    await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: common.getRandomNum(-10, 10) });
                    await common.delay(1000);
                });
                it('卖家查看商品详情', async function () {
                    for (let i in spuIds) {
                        const dresFull = await dresList[i].getFullById();
                        const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 2);
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                    }
                });
                it('买家端查看商品详情', async function () {
                    await ssReq.userLoginWithWx();
                    for (let i in spuIds) {
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                        const dresExp = modifyPrice(priceValues, dresFullOld[i], discount, 2);
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price', 'pubPriceTop', 'pubPriceLow', 'topPrice', 'lowPrice'], `商品spuId为:${spuIds[i]}`);
                    }
                });
            });
        });
    });
});


/**
 * 修改价格
 * @description
 * @param {object} params
 * @param {object} params.codeValues
 * @param {object} params.dresFull
 * @param {object} params.discount
 * @param {object} params.preferentialMode 0 折扣价 1 优惠价 2 恢复原价
 */
function modifyPrice(codeValues, dres, discount, preferentialMode) {
    const dresFull = _.cloneDeep(dres);
    if (preferentialMode == 2) {
        let originalPrices = JSON.parse(JSON.stringify(dresFull.originalPrices).replace(/vip/g, 'price'));
        console.log(originalPrices);
        // console.log(dresFull.spu);
        for (const price of ['pubPrice', 'price1', 'price2', 'price3', 'price4', 'price5']) originalPrices[price] = originalPrices[price] ? originalPrices[price] : dresFull.spu[price];
        console.log(originalPrices);
        // dresFull.skus.forEach(data => common.update(data, originalPrices));
        dresFull.skus.forEach(data => Object.assign(data, originalPrices));
        common.update(dresFull.spu, originalPrices);
        // dresFull.splitSkus.forEach(data => common.update(data, originalPrices));
        dresFull.splitSkus.forEach(data => Object.assign(data, originalPrices));
        return dresFull;
    }
    for (let value of codeValues) {
        let price = value == 0 ? 'pubPrice' : `price${value}`;
        if (preferentialMode == 0) {
            dresFull.skus.map(data => data[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3));
            dresFull.splitSkus.map(data => data[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3)));
        } else if (preferentialMode == 1) {
            dresFull.skus.map(data => data[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3));
            dresFull.splitSkus.map(data => data[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3)));
        } else {
            console.warn(`不存在preferentialMode为${preferentialMode}`);
        }
    }
    // dresFull.spu.pubPriceTop = dresFull.spu.pubPrice > dresFull.spu.pubPriceTop ? dresFull.spu.pubPrice : dresFull.spu.pubPriceTop;
    // dresFull.spu.pubPriceLow = dresFull.spu.pubPrice < dresFull.spu.pubPriceLow ? dresFull.spu.pubPrice : dresFull.spu.pubPriceLow;
    return dresFull;
};