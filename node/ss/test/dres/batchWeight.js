const ssReq = require('../../help/ssReq');
const common = require('../../../lib/common');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const basicJson = require('../../help/basicJson');

describe('批量修改spu重量', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, clientInfo, newDres, classId, num = common.getRandomNum(1, 20);
    const dres = dresManage.setupDres(), weight = 4;
    before('新增货品', async function () {
        await ssReq.ssSellerLogin();
        sellerInfo = _.cloneDeep(LOGINDATA);
        // 初始化商品基础信息
        await dresManage.prePrepare();
        //保存商品
        let json = basicJson.styleJson({ classId: classId });
        json.spu.weight = weight - 1;
        // console.log(`json=${JSON.stringify(json)}`);
        newDres = await sp.spdresb.saveFull(json);
    });
    describe('批量修改单个spu重量', async function () {
        before('批量修改单个', async function () {
            const fixWeight = await sp.spdresb.batchSetGoodsWeight({ spuIds: [newDres.result.data.spuId].toString(), weight: weight }).then(res => res);
        });
        describe('卖家查询', async function () {
            before('卖家登录', async function () {
                await ssReq.ssSellerLogin();
            });
            it('查询商品列表-卖家', async function () {
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ outSpuId: newDres.result.data.spuId }).then(res => res.result.data.rows.find(spu => spu.id == newDres.result.data.spuId));
                // console.log('卖家商品列表', sellerSpuList);
                common.isApproximatelyEqualAssert(dresExp(newDres, weight), sellerSpuList);
            });
            it('查询商品详情-卖家', async function () {
                const dresDetail = await sp.spdresb.getFullById({ id: newDres.result.data.spuId }).then(res => res.result.data);
                // console.log('卖家商品列表', dresDetail);
                dresDetail.skus.forEach(obj => { expect(weight, `卖家列表的重量不为${weight}`).to.be.equal(obj.weight) });
                common.isApproximatelyEqualAssert(dresExp(newDres, weight), dresDetail.spu);
            });
        });
        describe('买家查询', async function () {
            before('买家登录', async function () {
                await ssReq.userLoginWithWx();
                clientInfo = _.cloneDeep(LOGINDATA);
            });
            it('查询商品列表-买家', async function () {
                const dresSpu = await ss.spchb.getDresSpuList({ name: newDres.params.jsonParam.spu.name, tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows);
                // console.log('买家商品列表', dresSpu);
                common.isApproximatelyEqualAssert(dresExp(newDres, weight), dresSpu.find(obj => obj.id == newDres.result.data.spuId));
            });
            it('查询商品详情-买家', async function () {
                const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: newDres.result.data.spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                // console.log('买家商品详情', dresDetail);
                dresDetail.skus.forEach(obj => { expect(weight, `买家列表的重量不为${weight}`).to.be.equal(obj.weight) });
                common.isApproximatelyEqualAssert(dresExp(newDres, weight), dresDetail.spu);
            });
        });
    });

    describe('批量修改多个-5个', async function () {
        let sellerSpuList, spuIds, spuName;
        before('批量修改多个', async function () {
            await ssReq.ssSellerLogin();
            sellerSpuList = await sp.spdresb.findSellerSpuList({ pageNo: num, pageSize: 5, flags: 1 }).then(res => res.result.data.rows)
            spuIds = sellerSpuList.map(obj => obj.id);
            spuName = sellerSpuList.map(obj => obj.name);
            const fixWeight = await sp.spdresb.batchSetGoodsWeight({ spuIds: spuIds.toString(), weight: weight }).then(res => res);
        });
        describe('卖家查询', async function () {
            before('卖家登录', async function () {
                await ssReq.ssSellerLogin();
            });
            it('查询商品列表-卖家', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const sellerSpuList1 = await sp.spdresb.findSellerSpuList({ outSpuId: spuIds[i] }).then(res => res.result.data.rows);
                    sellerSpuList.forEach(obj => {
                        let spu = _.cloneDeep(obj);
                        spu.weight = weight;
                        common.isApproximatelyEqualAssert(spu, sellerSpuList1);
                    })
                };
            });
            it('查询商品详情-卖家', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const dresDetail = await sp.spdresb.getFullById({ id: spuIds[i] }).then(res => res.result.data);
                    dresDetail.skus.forEach(obj => { expect(weight, `卖家列表的重量不为${weight}`).to.be.equal(obj.weight) });
                    let spu = _.cloneDeep(sellerSpuList[i]);
                    spu.weight = weight;
                    spu.weightStr = `${weight}kg`;
                    common.isApproximatelyEqualAssert(spu, dresDetail.spu, ['classIds']);
                };
            });
        });
        describe('买家查询', async function () {
            before('买家登录', async function () {
                await ssReq.userLoginWithWx();
            });
            it('查询商品列表-买家', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const dresSpu = await ss.spchb.getDresSpuList({ name: spuName[i], tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows.find(obj => obj.id == spuIds[i]));
                    await common.delay(500);
                    // console.log('打印=', dresSpu);
                    // const dresSpu = await ss.spchb.getDresSpuList({ name: spuName[i], tenantId: sellerInfo.tenantId, orderBy: 'marketDate', orderByDesc: true }).then(res => res.result.data.rows[0]);
                    let spu = _.cloneDeep(sellerSpuList[i]);
                    spu.weight = weight;
                    common.isApproximatelyEqualAssert(spu, dresSpu, ['marketDays', 'readNum']);
                };
            });
            it('查询商品详情-买家', async function () {
                for (let i = 0; i < spuIds.length; i++) {
                    const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: sellerSpuList[i].id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
                    dresDetail.skus.forEach(obj => { expect(weight, `卖家列表的重量不为${weight}`).to.be.equal(obj.weight) });
                    let spu = _.cloneDeep(sellerSpuList[i]);
                    spu.weight = weight;
                    // console.log(dresDetail);
                    common.isApproximatelyEqualAssert(spu, dresDetail, ['marketDays', 'readNum']);
                };
            });
        });
    });
});

function dresExp(newDres, weight) {
    let spu = newDres.params.jsonParam.spu;
    spu.id = newDres.result.data.spuId;
    spu.weight = weight;
    spu.weightStr = `${weight}kg`;
    return spu;
};