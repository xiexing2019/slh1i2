const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const dresManage = require('../../help/dresManage');

describe('泛行业商品(独立app)', function () {
	this.timeout(TESTCASE.timeout);
	let dictTypeId;
	const dres = dresManage.setupDres({ type: 'app' });

	before('卖家登录后保存商品', async () => {
		await ssReq.ssSellerLogin();
		sellerInfo = _.cloneDeep(LOGINDATA);
		// 初始化字典
		dictTypeId = await dresManage.setupSpec();
		console.log(`dictTypeId=${dictTypeId}`);
		await dresManage.prePrepare({ 'dictTypeId': dictTypeId });
		//保存商品
		const json = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId });
		console.log(`json=${JSON.stringify(json)}`);
		// await dres.saveDres(json);
		await dres.saveDresByApp(json);
		console.log(dres.spu);
	});

	//卖家查询
	it('卖家查询商品列表', async function () {
		const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('卖家查询租户商品信息', async function () {
		const dresFull = await dres.getFullById();
		console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
		console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);

		common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
	});

	//提测单：增加一种重量的筛选查询方式 http://zentao.hzdlsoft.com:6082/zentao/testtask-view-1711.html
	it('卖家查询商品列表(按重量筛选)', async function () {
		const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId, weightGte: dres.spu.weightStr.split('-')[0], weightLte: (dres.spu.weightStr.split('-')[1]).split('k')[0] });
		console.log(`saaaa=${JSON.stringify(dresSpu)}`);
		expect(dresSpu).to.not.be.undefined;
		// common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('买家查询商品列表', async function () {
		await ssReq.userLoginWithWx();
		const dresSpu = await dres.findBuyerSpuList();
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('买家查询商品详情', async function () {
		const dresFull = await dres.getDetailForBuyer();
		console.log(`dresFull=${JSON.stringify(dresFull)}`);
		common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
	});

	describe('卖家修改商品信息', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			const updateJson = basicJson.wideStyleJsonByApp({ 'dictTypeId': dictTypeId });
			updateJson.id = updateJson.spu.id = dres.id;
			// const _skus = [...dres.skus.values()];
			// updateJson.skus.forEach((data, index) => {
			// 	data.id = _skus[index].id;
			// 	data.num = common.add(data.num, 1000);
			// });

			await dres.saveDresByApp(updateJson);
			// console.log(dres);
		});
		it('卖家查询租户商品信息', async function () {
			// const dresFull = await dres.getFullById();
			// console.log(`dresFull=${JSON.stringify(dresFull)}`);
			// // 更新商品信息 补充保存时jsonParam中没有涉及的字段
			// const dresExp = _.cloneDeep(dres);
			// dres.setByDetail(dresFull);
			// common.isApproximatelyEqualAssert(dresExp.getDetailExp(), dresFull, ['namePy']);
		});
		it('卖家查询商品列表', async function () {
			const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品列表', async function () {
			await ssReq.userLoginWithWx();

			const dresSpu = await dres.findBuyerSpuList();
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品详情', async function () {
			const dresFull = await dres.getDetailForBuyer();
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
		});
	});

	describe.skip('服务端分享商品海报', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			await dres.shareSku();
		});
		// shareSku中直接验证  留一个空it 用于触发
		it('数据校验', async function () {

		});
	});

});


function onMarketListExp(params) {
	let exp = {
		id: params.id,
		spuTitle: params.styleRes.params.jsonParam.spu.title,
		reqRem: params.reqRem,
		flag: params.flag,
		checkRem: params.checkRem ? params.checkRem : '',
		sellerId: params.sellerInfo.tenantId,
		sellerUnitId: params.sellerInfo.unitId
	};
	return exp;
};

function onMarketDetailExp(params) {
	let exp = {
		main: {
			flag: params.flag,
			checkRem: params.checkRem ? params.checkRem : '',
			sellerId: params.sellerInfo.tenantId,
			sellerUnitId: params.sellerInfo.unitId,
			reqRem: params.reqRem,
			spuTitle: params.styleRes.params.jsonParam.spu.title
		}, details:
			[{
				spuId: params.styleRes.result.data.spuId,
				spuTitle: params.styleRes.params.jsonParam.spu.title,
				reqId: params.id
			}]
	};
	return exp;
};


function dataCombine(obj) {
	let spu = _.cloneDeep(obj);
	spu.title = `${spu.title}(${spu.name})`;
	return spu;
};