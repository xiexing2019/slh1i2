const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const ssConfigParam = require('../../help/configParamManager');
const mysql = require('../../../reqHandler/sp/mysql');

describe('商品上下架', function () {
    this.timeout(TESTCASE.timeout);
    const dres = dresManage.setupDres();
    let classId = 1013;
    // 个性化参数
    let autoShelvesAfterCreate, allowPictureExistsShelves, allowShelvesWhenInventoryLessThanZero, allowShelvesWhenPriceZero;
    // 修改dresJson
    const changeDres = (spu, skus) => {
        // delete spu.docContent;
        // delete spu.docHeader;
        skus.forEach((sku) => {
            sku.num = 0;
            sku.pubPrice = 0;
        });
        spu.pubPrice = 0;
    };
    const newDres = dresManage.setupDres({ type: 'app' });

    before(async function () {
        await ssReq.ssSellerLogin();
        await dresManage.prePrepare();

        // autoShelvesAfterCreate = await ssConfigParam.getSpbParamInfo({ code: 'auto_shelves_after_create', ownerId: LOGINDATA.tenantId });
        allowPictureExistsShelves = await ssConfigParam.getSpbParamInfo({ code: 'allow_picture_exists_shelves', ownerId: LOGINDATA.tenantId });
        allowShelvesWhenInventoryLessThanZero = await ssConfigParam.getSpbParamInfo({ code: 'allow_shelves_when_inventory_less_than_zero', ownerId: LOGINDATA.tenantId });
        // allowShelvesWhenPriceZero = await ssConfigParam.getSpbParamInfo({ code: 'allow_shelves_when_price_zero', ownerId: LOGINDATA.tenantId });
    });

    // 其他用例都默认需要能够自动上架 即flag=1
    // 因此最后需要将参数都置为打开状态(不考虑使用还原)
    after('打开自动上架，没有图片允许，库存<=0允许、价格为0允许上架参数', async function () {
        // await autoShelvesAfterCreate.updateParam({ val: 1 });
        await allowPictureExistsShelves.updateParam({ val: 1 });
        await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
        // await allowShelvesWhenPriceZero.updateParam({ val: 1 });
    });

    describe.skip('自动上架参数', async function () {
        describe('自动上架', async function () {
            before(async function () {
                await autoShelvesAfterCreate.updateParam({ val: 1 });
                // console.log(autoShelvesAfterCreate);

                await dres.saveDres(basicJson.styleJson({ classId: classId }));
                // console.log(dres);
            });
            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

        describe('不自动上架', async function () {
            before(async function () {
                await autoShelvesAfterCreate.updateParam({ val: 0 });
                // const paramInfo = await autoShelvesAfterCreate.getParamInfo();
                // console.log(paramInfo);

                await dres.saveDres(basicJson.styleJson({ classId: classId }));
                // console.log(dres.id);
            });
            it('查询待上架列表', async function () {
                //上架：1， 下架：-4，待上架：2
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresSpu=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });
    });

    describe.skip('手动上架', async function () {
        before(async function () {
            // await autoShelvesAfterCreate.updateParam({ val: 0 });
        });

        describe('没有图片允许，库存<=0允许、价格为0允许上架', async function () {
            before(async function () {
                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                // await allowShelvesWhenPriceZero.updateParam({ val: 1 });
                //新建无图片，无库存、价格为0商品
                const json = basicJson.styleJson({ classId: classId }, changeDres);
                // console.log(`json=${JSON.stringify(json)}`);
                await dres.saveDres(json);
                await dres.onMarket();
            });
            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

        describe('没有图片不允许，库存<=0允许、价格为0允许上架', async function () {
            before(async function () {
                await allowPictureExistsShelves.updateParam({ val: 0 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                // await allowShelvesWhenPriceZero.updateParam({ val: 1 });
                //新建无图片，无库存、价格为0商品
                const json = basicJson.styleJson({ classId: classId }, changeDres);
                await dres.saveDres(json);
                await dres.onMarket();
            });
            it('查询待上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

        describe('没有图片允许，库存<=0不允许、价格为0允许上架', async function () {
            before(async function () {
                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 0 });
                // await allowShelvesWhenPriceZero.updateParam({ val: 1 });
                //新建无图片，无库存、价格为0商品
                const json = basicJson.styleJson({ classId: classId }, changeDres);
                await dres.saveDres(json);
                await dres.onMarket();
            });
            it('查询待上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

        describe('没有图片允许，库存<=0允许、价格为0不允许上架', async function () {
            before(async function () {
                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                // await allowShelvesWhenPriceZero.updateParam({ val: 0 });
                //新建无图片，无库存、价格为0商品
                const json = basicJson.styleJson({ classId: classId }, changeDres);
                await dres.saveDres(json);
                await dres.onMarket();
            });
            it('查询待上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

    });

    describe.skip('自动下架', async function () {
        beforeEach(async function () {
            await autoShelvesAfterCreate.updateParam({ val: 1 });
            await allowPictureExistsShelves.updateParam({ val: 1 });
            await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
            await allowShelvesWhenPriceZero.updateParam({ val: 1 });
        });

        describe('没有图片允许，库存<=0允许、价格为0允许上架', async function () {
            before(async function () {
                //修改无图片，无库存、价格为0商品
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
                // console.log(sellerSpuList[0].id);
                await sp.spdresb.updateSpuForSeller({ id: sellerSpuList[0].id, docHeader: '[]', docContent: '[]', pubPrice: 0 });
                const dresDetail = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(dresDetail);
                const updateJson = basicJson.styleJson({ classId: dresDetail.spu.classId }, changeDres);
                updateJson.id = updateJson.spu.id = dres.id;
                const _skus = [...dres.skus.values()];
                updateJson.skus.forEach((data, index) => {
                    if (typeof (_skus[index]) == 'undefined') {
                        // delete updateJson.skus[index];
                        updateJson.skus.splice(index, 1)
                    } else {
                        data.id = _skus[index].id;
                    }
                });
                await dres.saveDres(updateJson);
                const dresDetail2 = await sp.spdresb.getFullById({ id: dres.id }).then(res => res.result.data);
                dres.setByDetail(dresDetail2);

                // 没有图片允许，库存<=0允许、价格为0允许上架
                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                await allowShelvesWhenPriceZero.updateParam({ val: 1 });
            });

            it('查询已上架列表', async function () {
                const dresSpu = await dres.findSellerSpuList({ flag: 1 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });
        });

        describe('没有图片不允许，库存<=0允许、价格为0允许上架', async function () {
            before(async function () {
                //修改无图片，无库存、价格为0商品
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
                // console.log(sellerSpuList[0].id);
                await sp.spdresb.updateSpuForSeller({ id: sellerSpuList[0].id, docHeader: '[]', docContent: '[]', pubPrice: 0 });
                const dresDetail = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(dresDetail);
                const updateJson = basicJson.styleJson({ classId: dresDetail.spu.classId }, changeDres);
                updateJson.id = updateJson.spu.id = dres.id;
                const _skus = [...dres.skus.values()];
                updateJson.skus.forEach((data, index) => {
                    if (typeof (_skus[index]) == 'undefined') {
                        // delete updateJson.skus[index];
                        updateJson.skus.splice(index, 1)
                    } else {
                        data.id = _skus[index].id;
                    }
                });
                await dres.saveDres(updateJson);
                const dresDetail2 = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                // console.log(getFullById2);
                dres.setByDetail(dresDetail2);

                // 没有图片不允许，库存<=0允许、价格为0允许上架
                await allowPictureExistsShelves.updateParam({ val: 0 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                await allowShelvesWhenPriceZero.updateParam({ val: 1 });
            });

            it('查询待上架列表', async function () {
                //上架：1， 下架：-4
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu);
            });

            it('查询上架列表', async function () {
                //上架：1， 下架：-4
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data);
                // console.log(sellerSpuList);
                sellerSpuList.rows.forEach((spu) => {
                    expect(spu, '上架列表存在无图片商品').to.not.includes({ coverUrl: ' ' });
                });
            });
        });

        describe('没有图片允许，库存<=0不允许、价格为0允许上架', async function () {
            before(async function () {
                //修改无图片，无库存、价格为0商品
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
                // console.log(sellerSpuList[0].id);
                await sp.spdresb.updateSpuForSeller({ id: sellerSpuList[0].id, docHeader: '[]', docContent: '[]', pubPrice: 0 });
                const getFullById = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(getFullById);
                const updateJson = basicJson.styleJson({ classId: getFullById.spu.classId }, changeDres);
                updateJson.id = updateJson.spu.id = dres.id;
                const _skus = [...dres.skus.values()];
                updateJson.skus.forEach((data, index) => {
                    if (typeof (_skus[index]) == 'undefined') {
                        // delete updateJson.skus[index];
                        updateJson.skus.splice(index, 1)
                    } else {
                        data.id = _skus[index].id;
                    }
                });
                await dres.saveDres(updateJson);
                const getFullById2 = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(getFullById2);

                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 0 });
                await allowShelvesWhenPriceZero.updateParam({ val: 1 });
            });

            it('查询待上架列表', async function () {
                //上架：1， 下架：-4
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['namePy']);
            });

            it('查询上架列表', async function () {
                //上架：1， 下架：-4
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data);
                // console.log(sellerSpuList);
                sellerSpuList.rows.forEach((spu) => {
                    expect(spu.stockNum, '上架列表存在库存小于等于商品').to.be.above(0);
                });
            });
        });

        describe('没有图片允许，库存<=0允许、价格为0不允许上架', async function () {
            before(async function () {
                //修改无图片，无库存、价格为0商品
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
                // console.log(sellerSpuList[0].id);
                await sp.spdresb.updateSpuForSeller({ id: sellerSpuList[0].id, docHeader: '[]', docContent: '[]', pubPrice: 0 });
                const getFullById = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(getFullById);
                const updateJson = basicJson.styleJson({ classId: getFullById.spu.classId }, changeDres);
                updateJson.id = updateJson.spu.id = dres.id;
                const _skus = [...dres.skus.values()];
                updateJson.skus.forEach((data, index) => {
                    if (typeof (_skus[index]) == 'undefined') {
                        // delete updateJson.skus[index];
                        updateJson.skus.splice(index, 1)
                    } else {
                        data.id = _skus[index].id;
                    }
                });
                await dres.saveDres(updateJson);
                const getFullById2 = await sp.spdresb.getFullById({ id: sellerSpuList[0].id }).then(res => res.result.data);
                dres.setByDetail(getFullById2);

                await allowPictureExistsShelves.updateParam({ val: 1 });
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 1 });
                await allowShelvesWhenPriceZero.updateParam({ val: 0 });
            });

            it('查询待上架列表', async function () {
                //上架：1， 下架：-4
                const dresSpu = await dres.findSellerSpuList({ flag: 2 });
                // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
                common.isApproximatelyEqualAssert(dres.spu, dresSpu, ['namePy']);
            });

            it('查询上架列表', async function () {
                //上架：1， 下架：-4
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data);
                // console.log(sellerSpuList);
                sellerSpuList.rows.forEach((spu) => {
                    expect(spu.pubPrice, '上架列表存在价格为0商品').to.be.above(0);
                });
            });
        });

    });

    describe('手动下架', async function () {
        before(async function () {
            const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
            const spuId = sellerSpuList[0].id;
            await sp.spdresb.offMarket({ ids: [spuId] });
            const getFullById = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result.data);
            dres.setByDetail(getFullById);
        });

        it('查询已下架列表', async function () {
            //上架：1， 下架：-4
            const dresSpu = await dres.findSellerSpuList({ flag: -4 });
            // console.log(`dresFull=${JSON.stringify(dresSpu)}`);
            common.isApproximatelyEqualAssert(dres.spu, dresSpu);
        });
    });

    // 下架与删除不会修改sku的flag
    describe('删除下架商品', async function () {
        let clientInfo;
        const dresOff = dresManage.setupDres();
        before('新增商品并手动下架后删除', async function () {
            await ssReq.ssSellerLogin();
            const seller = _.cloneDeep(LOGINDATA);
            await dresOff.saveDres(basicJson.styleJson({ classId: classId }));
            //增加商品访问记录
            await ssReq.userLoginWithWx();
            clientInfo = _.cloneDeep(LOGINDATA);
            await sp.spdresb.saveBuyerView({ spuId: dresOff.id, sellerId: seller.tenantId, sellerUnitId: seller.unitId, type: 1 });

            await ssReq.ssSellerLogin();
            await dresOff.offMarket();
            await ss.spdresb.deleteOffMarket({ ids: [dresOff.id] });
        });
        it('查询租户商品信息', async function () {
            const dresDetail = await dresOff.getFullById();
            expect(dresDetail.spu).to.includes({ flag: -1 });
        });
        // bug链接：http://zentao.hzdlsoft.com:6082/zentao/bug-view-11501.html
        it('卖家查询买家访问记录', async function () {
            const res = await ss.up.sellerViewQueryByDate({ buyerId: clientInfo.tenantId });
            console.log(`买家查询买家访问记录= ${JSON.stringify(res)}`);
        });
    });

    describe('批量删除待上架商品', async function () {
        let spuIds = [], codesNew = [];
        before(async function () {
            await ssReq.ssSellerLogin();

            const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 2 }).then(res => res.result.data.rows);
            if (sellerSpuList.length == 0) {
                console.log('不存在待上架商品');
                this.skip();
            }
            sellerSpuList.slice(0, 3).forEach(data => {
                spuIds.push(data.id);
                codesNew.push(`${data.code}_${data.id}`);// 删除后  数据库中商品code会添加spuId,不影响接口返回
            });

            await ss.spdresb.deleteOffMarket({ ids: spuIds }).then(res => res.result);
        });


        it('查询租户商品信息', async function () {
            for (let i = 0; i < spuIds.length; i++) {
                const spuInfo = await sp.spdresb.getFullById({ id: spuIds[i] }).then(res => res.result.data.spu);
                expect(spuInfo).to.includes({ flag: -1 });
            }
        });
        it('查询更新的code-offline', async function () {
            //删除后，会更新款号code为 code_spuId
            for (let i = 0; i < spuIds.length; i++) {
                const spbSql = await mysql({ dbName: 'spbMysql' });
                const code = await spbSql.query(`select code from spb002.dres_spu where id=${spuIds[i]}`).then(res => res[0][0]['code']);
                await spbSql.end();
                expect(code, `code未更新成code_spuId形式。spuId=${spuIds[i]}`).to.equal(`${codesNew[i]}`);
            }
        });
    });

    describe('批量删除卖家下架商品', async function () {
        let spuIds = [];
        before(async function () {
            let sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: -4 }).then(res => res.result.data.rows);
            if (sellerSpuList.length == 0) {
                console.log('不存在卖家下架的商品');
                this.skip();
            }
            spuIds = sellerSpuList.slice(0, 3).map(data => data.id);

            await ss.spdresb.deleteOffMarket({ ids: spuIds });
        });
        it('查询租户商品信息', async function () {
            for (let i = 0; i < spuIds.length; i++) {
                const spuInfo = await sp.spdresb.getFullById({ id: spuIds[i] }).then(res => res.result.data.spu);
                expect(spuInfo).to.includes({ flag: -1 });
            }
        });
    });

    describe('批量上下架', async function () {
        const spuIds = [], len = 3;
        before('选择货品', async function () {
            const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
            sellerSpuList.slice(0, len).forEach(dresSpu => spuIds.push(dresSpu.id));
        });
        describe('批量下架', async function () {
            before('批量下架', async function () {
                await sp.spdresb.offMarket({ ids: spuIds });
            });
            it('查询已下架列表', async function () {
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: -4, orderBy: 'updatedDate', orderByDesc: true }).then(res => res.result.data.rows);
                for (let index = 0; index < len; index++) {
                    const dresSpu = sellerSpuList.find(obj => obj.id == spuIds[index]);
                    expect(dresSpu, `已下架列表未找到id为${spuIds[index]}的商品`).to.not.be.undefined;
                }
            });
        });
        describe('批量上架', async function () {
            before('批量上架', async function () {
                await sp.spdresb.onMarket({ ids: spuIds });
            });
            it('查询已上架列表', async function () {
                const sellerSpuList = await sp.spdresb.findSellerSpuList({ flag: 1 }).then(res => res.result.data.rows);
                for (let index = 0; index < len; index++) {
                    const dresSpu = sellerSpuList.find(obj => obj.id == spuIds[index]);
                    expect(dresSpu, `已上架列表未找到id为${spuIds[index]}的商品`).to.not.be.undefined;
                }
            });
        });
        describe('库存不足时，上架商品', async function () {
            let dresInfo, sellerSpuList2, negativeSkus = [];
            let vaildSpuIds = new Array();
            before('下架库存为负的商品', async function () {
                await ssReq.ssSellerLogin();

                // 库存为负不允许上架
                await allowShelvesWhenInventoryLessThanZero.updateParam({ val: 0 });
                sellerSpuList2 = await sp.spdresb.findSellerSpuList({ pageNo: 1, pageSize: 10, flags: 1, opType: 'Gt', invNum: 0 }).then(res => res.result.data.rows);
                // 新增一件商品
                await dresManage.prePrepare();
                const json = basicJson.styleJsonByApp({}, (spu, skus) => { skus.forEach(sku => { sku.num = sku.ssNum = -1 }) });
                await newDres.saveDresByApp(json);
                // console.log(newDres.spu);

            })
            after('下架新增的商品', async function () {
                await sp.spdresb.offMarket({ ids: [newDres.spu.id] });
            });
            it('批量上架，包含负库存商品', async function () {
                //上架多件商品，包含该下架的商品
                //查询10件上架的商品

                for (let a of sellerSpuList2) {
                    if (a.stockNum > 0) {
                        vaildSpuIds.push(a.id)
                    }
                }
                let vaildSpuIds2 = [...vaildSpuIds]
                vaildSpuIds2.push(newDres.spu.id);
                const updres = await sp.spdresb.onMarket({ ids: common.dedupe(vaildSpuIds2), check: false }).then(res => res.result);
                //断言上架商品中不包含下架商品的id

                common.isApproximatelyArrayAssert(vaildSpuIds, updres.data.successIds);
            });

        });
    });

});