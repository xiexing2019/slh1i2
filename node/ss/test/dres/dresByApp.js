const common = require('../../../lib/common');
const basicJson = require('../../help/basicJson');
const ssReq = require('../../help/ssReq');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const dresManage = require('../../help/dresManage');
const billManage = require('../../help/billManage');
const cartManage = require('../../help/cartManage');

describe('商品(独立app)', function () {
	this.timeout(TESTCASE.timeout);
	const dres = dresManage.setupDres({ type: 'app' });

	before('卖家登录后保存商品', async () => {
		await ssReq.ssSellerLogin();
		// console.log(`LOGINDATA=${JSON.stringify(LOGINDATA)}`);
		sellerInfo = _.cloneDeep(LOGINDATA);
		await dresManage.prePrepare();

		//保存商品
		const json = basicJson.styleJsonByApp();
		// console.log(`json=${JSON.stringify(json)}`);
		// await dres.saveDres(json);
		await dres.saveDresByApp(json);
		console.log(dres.spu);
	});

	//卖家查询
	it('卖家查询商品列表', async function () {
		const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('卖家查询租户商品信息', async function () {
		const dresFull = await dres.getFullById();
		console.log(`\ndresFull=${JSON.stringify(dresFull)}`);
		console.log(`\ndres.getDetailExp()=${JSON.stringify(dres.getDetailExp())}`);

		common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
	});

	it('买家查询商品列表', async function () {
		await ssReq.userLoginWithWx();

		const dresSpu = await dres.findBuyerSpuList();
		common.isApproximatelyEqualAssert(dres.spu, dresSpu);
	});

	it('买家查询商品详情', async function () {
		const dresFull = await dres.getDetailForBuyer();
		console.log(`dresFull=${JSON.stringify(dresFull)}`);
		common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
	});

	describe('卖家修改商品信息', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			const updateJson = basicJson.styleJsonByApp();
			updateJson.id = updateJson.spu.id = dres.id;
			// const _skus = [...dres.skus.values()];
			// updateJson.skus.forEach((data, index) => {
			// 	data.id = _skus[index].id;
			// 	data.num = common.add(data.num, 1000);
			// });

			await dres.saveDresByApp(updateJson);
			// console.log(dres);
		});
		it('卖家查询租户商品信息', async function () {
			// const dresFull = await dres.getFullById();
			// console.log(`dresFull=${JSON.stringify(dresFull)}`);
			// // 更新商品信息 补充保存时jsonParam中没有涉及的字段
			// const dresExp = _.cloneDeep(dres);
			// dres.setByDetail(dresFull);
			// common.isApproximatelyEqualAssert(dresExp.getDetailExp(), dresFull, ['namePy']);
		});
		it('卖家查询商品列表', async function () {
			const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品列表', async function () {
			await ssReq.userLoginWithWx();

			const dresSpu = await dres.findBuyerSpuList();
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品详情', async function () {
			const dresFull = await dres.getDetailForBuyer();
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
		});
	});
	describe('设置sku售罄', async function () {
		let keys, keys1, keys2;
		before('设置sku售罄', async function () {
			await ssReq.ssSellerLogin();
			keys = [...dres.skus.keys()];
			[keys1, keys2] = _.chunk(keys, Math.ceil(keys.length / 2));
			// console.log(keys1);
			// console.log(keys2);
			await dres.updateSkuSoldOutFlag({ validIds: keys1, inValidIds: keys2 });
		});
		after('设置sku不售罄', async function () {
			await ssReq.ssSellerLogin();
			await dres.updateSkuSoldOutFlag({ validIds: [], inValidIds: keys });
		});
		//卖家查询
		it('卖家查询商品列表', async function () {
			await ssReq.ssSellerLogin();
			const dresSpu = await dres.findSellerSpuList({ classId: dres.spu.classId });
			console.log(`\n 卖家查询商品列表=${JSON.stringify(dresSpu)}`);
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('卖家查询租户商品信息', async function () {
			const dresFull = await dres.getFullById();
			console.log(`\n 卖家查询租户商品信息=${JSON.stringify(dresFull)}`);
			common.isApproximatelyEqualAssert(dres.getDetailExp(), dresFull);
		});
		it('买家查询商品列表', async function () {
			await ssReq.userLoginWithWx();
			const dresSpu = await dres.findBuyerSpuList();
			console.log(`\n 买家查询商品列表=${JSON.stringify(dresSpu)}`);
			common.isApproximatelyEqualAssert(dres.spu, dresSpu);
		});
		it('买家查询商品详情', async function () {
			const dresFull = await dres.getDetailForBuyer();
			console.log(`\n 买家查询商品详情=${JSON.stringify(dresFull)}`);
			common.isApproximatelyEqualAssert(dres.getBuyerDetailExp(), dresFull.result.data);
		});
		describe('购物车下单', function () {
			let shoppingCart;
			before('加入购物车', async function () {
				await ssReq.userLoginWithWx();
				shoppingCart = cartManage.setupShoppingCart(sellerInfo);
				await shoppingCart.emptyCart();
				const dresFull = await dres.getDetailForBuyer().then(res => res.result.data);
				const cartJson = shoppingCart.makeCartJson(dresFull, { type: 1, skus: [{ skuId: keys1[0], num: 1 }] });
				console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
				await shoppingCart.saveCartInBatchs(cartJson);
				shoppingCart.spuCarts.get(dres.id).carts.get(keys1[0]).flag = -1;
			});
			it('查询购物车', async function () {
				await shoppingCart.cartListAssert();
			});
			describe('下单', async function () {
				let purRes;
				before(async function () {
					await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
					const purJson = billManage.mockPurParam(shoppingCart);
					// console.log(`purJson=${JSON.stringify(purJson)}`);
					// savePurBill = await billManage.createPurBill(purJson);
					purRes = await sp.spTrade.savePurBill(purJson);
				});
				it('判断订单状态', async function () {
					let i = 1, checkPurBillFlag = { purBillFlag: purRes.result.data.rows[0].purBillFlag };
					while (!checkPurBillFlag.failedMsg) {
						if (i > 10) throw new Error(`判断订单状态已超过10次`);
						await common.delay(500)
						checkPurBillFlag = await ss.sppur.checkPurBillFlag({ purBillId: purRes.result.data.rows[0].billId }).then(res => res.result.data.rows[0]);
						console.log(`\n checkPurBillFlag=${JSON.stringify(checkPurBillFlag)}`);
						i++;
					};
					const cart = shoppingCart.spuCarts.get(dres.id).carts.get(keys1[0]);
					console.log(cart.spec1Name, cart.spec2Name, cart.spec3Name);
					const specArr = [];
					[cart.spec1Name, cart.spec2Name, cart.spec3Name].forEach(ele => {
						if (ele) specArr.push(ele);
					})
					expect(checkPurBillFlag, '商品详情的活动信息activity不存在').to.include({ failedMsg: `[${dres.spu.title}]的商品[${specArr.toString()}]已售罄` });
				});
			});
		});
	});

	describe.skip('服务端分享商品海报', async function () {
		before(async function () {
			await ssReq.ssSellerLogin();
			await dres.shareSku();
		});
		// shareSku中直接验证  留一个空it 用于触发
		it('数据校验', async function () {

		});
	});

});


function onMarketListExp(params) {
	let exp = {
		id: params.id,
		spuTitle: params.styleRes.params.jsonParam.spu.title,
		reqRem: params.reqRem,
		flag: params.flag,
		checkRem: params.checkRem ? params.checkRem : '',
		sellerId: params.sellerInfo.tenantId,
		sellerUnitId: params.sellerInfo.unitId
	};
	return exp;
};

function onMarketDetailExp(params) {
	let exp = {
		main: {
			flag: params.flag,
			checkRem: params.checkRem ? params.checkRem : '',
			sellerId: params.sellerInfo.tenantId,
			sellerUnitId: params.sellerInfo.unitId,
			reqRem: params.reqRem,
			spuTitle: params.styleRes.params.jsonParam.spu.title
		}, details:
			[{
				spuId: params.styleRes.result.data.spuId,
				spuTitle: params.styleRes.params.jsonParam.spu.title,
				reqId: params.id
			}]
	};
	return exp;
};


function dataCombine(obj) {
	let spu = _.cloneDeep(obj);
	spu.title = `${spu.title}(${spu.name})`;
	return spu;
};