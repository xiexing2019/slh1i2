const common = require('../../../lib/common');
const ssReq = require('../../help/ssReq');
const dresManage = require('../../help/dresManage');
const ss = require('../../../reqHandler/ss');
const sp = require('../../../reqHandler/sp');
const basicJson = require('../../help/basicJson');
const ssAccount = require('../../data/ssAccount');
const ssConfigParam = require('../../help/configParamManager');


describe('新款保护', function () {
	this.timeout(TESTCASE.timeout);
	let sellerInfo, listMember, styleRes, memberIds = [];
	const dres = dresManage.setupDres({ type: 'app' });

	before('卖家登录后保存商品', async () => {
		// await ssReq.ssSellerLogin({ code: '18212345677', shopName: '诗文小铺' });
		await ssReq.ssSellerLogin();
		tenantId = LOGINDATA.tenantId;
		sellerInfo = _.cloneDeep(LOGINDATA);
		const ssNewDresSpuProtect = await ssConfigParam.getSpgParamInfo({ code: 'ss_new_dres_spu_protect', ownerId: LOGINDATA.tenantId });
		await ssNewDresSpuProtect.updateParam({ val: 1 });
		await dresManage.prePrepare();
		const json = basicJson.styleJsonByApp();
		await dres.saveDresByApp(json);
		//选择可见人员
		listMember = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
		// console.log(`=${JSON.stringify(listMember)}`);
	});

	it('卖家根据新款保护查询', async function () {
		const spu = await sp.spdresb.findSellerSpuList({ isProctect: 0, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows.find(obj => obj.id == dres.id));
		expect(spu, '该商品不在新款保护设为0列表里').to.not.be.undefined;
	});

	describe('商品增加权限', function () {
		before('设置指定人可见', async function () {
			listMember.slice(0, 3).forEach(member => { memberIds.push(member.id); });
			const addAuth = await ss.spdresb.addAuth({ authType: 1, spuIds: dres.id, memberIds: memberIds.toString() });
			// console.log(`\n addAuth=${JSON.stringify(addAuth)}`);
		});

		it('买家查询货品', async function () {
			await ssReq.ssClientLogin({ tenantId: sellerInfo.tenantId, mobile: common.getRandomMobile(), appId: ssAccount.seller6.appId, scene: 1 });
			const dresList = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, code: dres.spu.code }).then(res => res.result.data.rows);
			// console.log(`\n getListDresSpu=${JSON.stringify(dresList)}`);
			expect(dresList[0].isProtected, '新款保护状态不为true').to.equal(true);
		});

		it('卖家根据新款保护查询', async function () {
			await ssReq.ssSellerLogin();
			const spu = await sp.spdresb.findSellerSpuList({ isProctect: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows.find(obj => obj.id == dres.id));
			expect(spu, '该商品不在新款保护设为1列表里').to.not.be.undefined;
		});
	});
});