const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ssAccount = require('../../../data/ssAccount');
const saasBatchSkuManager = require('../../../help/saas/saasBatchSkuManager');
const cartManage = require('../../../help/cartManage');

describe('saas同步商品按sku设置不同的价格', function () {
    this.timeout(TESTCASE.timeout);
    const saasBatchSku = saasBatchSkuManager.setupSaas();
    let shoppingCart;
    before('saas登录,新增货品,修改sku价格', async function () {
        await saasBatchSku.slh1Login();
        await saasBatchSku.addNewDress();
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        saasBatchSku.sellerInfo = _.cloneDeep(LOGINDATA);
        await saasBatchSku.updateSkuPrice();
    });
    describe('卖家', function () {
        before('登录', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        });
        it('卖家查询商品列表', async function () {
            await saasBatchSku.sellerDressListAssert();
        });
        it('卖家查询商品详情', async function () {
            await saasBatchSku.sellerDressFullAssert();
        });
    });
    describe('买家', function () {
        before('登录添加购物车', async function () {
            await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId, appId: ssAccount.saas.appId });
            shoppingCart = cartManage.setupShoppingCart(saasBatchSku.sellerInfo);
            await shoppingCart.emptyCart();
            const cartJson = shoppingCart.makeCartJson(saasBatchSku.dres, { type: 2, count: 2, num: 3 });
            // console.log(`\ncartJson=${JSON.stringify(cartJson)}`);
            await shoppingCart.saveCartInBatchs(cartJson);
            console.log('`````', shoppingCart.spuCarts.get(saasBatchSku.dres.spu.id).carts.values());
        });
        it('买家查询商品列表', async function () {
            await saasBatchSku.buyerDressListAssert();
        });
        it('买家查询商品详情', async function () {
            await saasBatchSku.buyerDressfullAssert();
        });
        it('查询购物车', async function () {
            this.retries(2);
            await common.delay(1000);
            await shoppingCart.cartListAssert();
            // console.log('`````', JSON.stringify(shoppingCart));

        });
    });

    describe('关闭参数-货品同步是否允许价格覆盖', function () {
        before('关闭参数', async function () {
            await saasBatchSku.setConfigParam({ code: 'allow_sync_spu_price_cover', val: 0, region: 'spg' });
            await saasBatchSku.slh1Login();
            await saasBatchSku.addNewDress({ pk: `${saasBatchSku.dres.spu.slhId}` });
            await saasBatchSku.updateShopingCart(shoppingCart.spuCarts);
        });
        describe('卖家', function () {
            before('登录', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            });
            it('卖家查询商品列表', async function () {
                await saasBatchSku.sellerDressListAssert();
            });
            it('卖家查询商品详情', async function () {
                await saasBatchSku.sellerDressFullAssert();
            });
        });
        describe('买家', function () {
            before('登录', async function () {
                await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId, appId: ssAccount.saas.appId });
            });
            it('买家查询商品列表', async function () {
                await saasBatchSku.buyerDressListAssert();
            });
            it('买家查询商品详情', async function () {
                await saasBatchSku.buyerDressfullAssert();
            });
            it('查询购物车', async function () {
                this.retries(2);
                await common.delay(1000);
                await shoppingCart.cartListAssert();
            });
        });
    });

    describe('开启参数-货品同步是否允许价格覆盖', function () {
        before('开启参数', async function () {
            await saasBatchSku.setConfigParam({ code: 'allow_sync_spu_price_cover', val: 1, region: 'spg' });
            await saasBatchSku.slh1Login();
            await saasBatchSku.addNewDress({ pk: `${saasBatchSku.dres.spu.slhId}` });
            await saasBatchSku.updateShopingCart(shoppingCart.spuCarts, flag = 1);
        });
        describe('卖家', function () {
            before('登录', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            });
            it('卖家查询商品列表', async function () {
                await saasBatchSku.sellerDressListAssert(saasBatchSku.dresBefore);
            });
            it('卖家查询商品详情', async function () {
                await saasBatchSku.sellerDressFullAssert(saasBatchSku.dresBefore);
            });
        });
        describe('买家', function () {
            before('登录', async function () {
                await ssReq.userLoginWithWx({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId, appId: ssAccount.saas.appId });
            });
            it('买家查询商品列表', async function () {
                await saasBatchSku.buyerDressListAssert(saasBatchSku.dresBefore);
            });
            it('买家查询商品详情', async function () {
                await saasBatchSku.buyerDressfullAssert(saasBatchSku.dresBefore);
            });
            it('查询购物车', async function () {
                console.log('1111', JSON.stringify(saasBatchSku.dresBefore));

                this.retries(2);
                await common.delay(1000);
                await shoppingCart.cartListAssert();
            });
        });
    });

});