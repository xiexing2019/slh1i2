const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const saasBaseManger = require('../../../help/saas/saasBase');
const ssConfigParam = require('../../../help/configParamManager');

describe('设置拆色拆码库存', function () {
    this.timeout(TESTCASE.timeout);
    const saas = saasBaseManger.setupSaas();
    before('saas新增均色均码货品同步到商城,并拆色拆码', async function () {
        await saasBaseManger.slh1Login();
        await saas.addNewDress({ isJunSe: true });
        await saas.updateSpuForSeller();
    });
    it('卖家查询商品列表', async function () {
        await saas.sellerDressListAssert();
    });
    it('卖家查询商品详情', async function () {
        await saas.sellerDressFullAssert();
    });
    describe('设置拆色拆码库存', function () {
        before('设置拆色拆码库存', async function () {
            await saas.setSplitSkuStockNum();
        });
        it('查询商品详情', async function () {
            await saas.sellerDressFullAssert({ detail: true });
        });
        it('设置拆色拆码库存大于总库存', async function () {
            await saas.setSplitSkuStockNum({ stockNum: 'top' });
        });
        it('设置拆色拆码库存小于于总库存', async function () {
            await saas.setSplitSkuStockNum({ stockNum: 'low' });
        });
    });
    describe('设置拆色拆码售罄标志:1', function () {
        before('设置拆色拆码售罄标识', async function () {
            await saas.setSplitSoldFlag({ soldOutFlag: 1 });
        });
        it('查询商品详情', async function () {
            await saas.sellerDressFullAssert({ detail: true });
        });
    });
    describe('设置拆色拆码售罄标志:0', function () {
        before('设置拆色拆码售罄标识', async function () {
            await saas.setSplitSoldFlag({ soldOutFlag: 0 });
        });
        it('查询商品详情', async function () {
            await saas.sellerDressFullAssert({ detail: true });
        });
    });

});