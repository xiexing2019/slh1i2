const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');

describe('改价', function () {
    this.timeout(TESTCASE.timeout);
    let spuIds = [], dresList = [], priceValues = [], sellerInfo, dresFullBack = [];

    before(async () => {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        // await ss.ssugr.synDresForSaas({ spuSyncDays: 5 });
        // await common.delay(5000);

        const priceList = await ss.spdresb.getPriceDict().then(res => res.result.data.prices);
        priceList.forEach(price => priceValues.push(price.codeValue));
        const spuList = await sp.spdresb.findSellerSpuList({ flags: 1, orderBy: 'market_date', orderByDesc: true }).then(res => res.result.data.rows);
        for (const i in spuList.slice(0, 3)) {
            spuIds.push(spuList[i].id);
            const dresFull = await sp.spdresb.getFullById({ id: spuList[i].id }).then(res => res.result.data);
            dresFullBack.push(dresFull);
            const dres = dresManage.setupDres();
            dres.setByDetail(dresFull);
            dresList[i] = dres;
        }
        console.log(spuIds);
        console.log(`开始时候查询的价格= ${JSON.stringify(dresFullBack)}`);
    });

    describe('批量改价', async function () {

        describe('批量改价:折扣价', async function () {
            let discount;
            const dresFullOld = [];
            before(async function () {
                for (let i in spuIds) {
                    dresFullOld.push(await dresList[i].getFullById());
                }
                discount = common.getRandomNum(1, 9) / 10;
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 0, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (let i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price']);
                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
                for (let i in spuIds) {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price']);
                }
            });
            describe('批量改价:恢复原价', function () {
                before('恢复原价', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                    // let res = await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: common.getRandomNum(-10, 10) });
                    let res = await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: 0 });
                    console.log(JSON.stringify(res));
                    await common.delay(1000);
                });
                it('卖家查看商品详情', async function () {
                    for (let i in spuIds) {
                        const dresExp = modifyPrice(priceValues, dresFullBack[i], discount, 2);
                        const dresFull = await dresList[i].getFullById();
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);
                    }
                });
                it('买家端查看商品详情', async function () {
                    await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
                    for (let i in spuIds) {
                        const dresExp = modifyPrice(priceValues, dresFullBack[i], discount, 2);
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);
                    }
                });
            });
        });
        describe('批量改价:优惠价', async function () {
            let discount;
            const dresFullOld = [];
            let sellerInfo;
            before(async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                sellerInfo = _.cloneDeep(LOGINDATA);

                for (const i in spuIds) {
                    dresFullOld.push(await dresList[i].getFullById());
                }

                discount = common.getRandomNum(-20, 20);
                await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 1, preferentialPrice: discount });
            });
            it('商品详情', async function () {
                for (const i in spuIds) {
                    const dresFull = await dresList[i].getFullById();
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 1);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType', 'price']);
                }
            });
            it('买家端查看商品详情', async function () {
                await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
                for (let i in spuIds) {
                    const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                    dresFullOld[i] = modifyPrice(priceValues, dresFullOld[i], discount, 0);
                    common.isApproximatelyEqualAssert(dresFullOld[i], dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);
                }
            });
            describe('批量改价:恢复原价', function () {
                before('恢复原价', async function () {
                    await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                    // await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: common.getRandomNum(-10, 10) });
                    await ss.spdresb.batchPreferentialPrice({ spuIds: spuIds.toString(), codeValue: priceValues.toString(), preferentialMode: 2, preferentialPrice: 0 });
                    await common.delay(1000);
                });
                it('卖家查看商品详情', async function () {
                    for (let i in spuIds) {
                        const dresExp = modifyPrice(priceValues, dresFullBack[i], discount, 2);
                        const dresFull = await dresList[i].getFullById();
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);
                    }
                });
                it('买家端查看商品详情', async function () {
                    await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
                    for (let i in spuIds) {
                        const dresExp = modifyPrice(priceValues, dresFullBack[i], discount, 2);
                        const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuIds[i], _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode })
                        common.isApproximatelyEqualAssert(dresExp, dresFull, ['updatedBy', 'updatedDate', 'ver', 'priceChangedSet', 'priceChangedType']);
                    }
                });
            });
        });
    });
});

/**
 * 修改价格
 * @description
 * @param {object} params
 * @param {object} params.codeValues
 * @param {object} params.dresFull
 * @param {object} params.discount
 * @param {object} params.preferentialMode 0 折扣价 1 优惠价
 */
function modifyPrice(codeValues, dres, discount, preferentialMode) {
    const dresFull = _.cloneDeep(dres);
    if (preferentialMode == 2) {
        let originalPrices = JSON.parse(JSON.stringify(dresFull.originalPrices).replace(/vip/g, 'price'));
        console.log(originalPrices);
        for (const price of ['pubPrice', 'price1', 'price2', 'price3', 'price4', 'price5']) originalPrices[price] = originalPrices[price] ? originalPrices[price] : dresFull.spu[price];
        console.log(originalPrices);
        dresFull.skus.forEach(data => common.update(data, originalPrices));
        common.update(dresFull.spu, originalPrices);
        dresFull.splitSkus.forEach(data => common.update(data, originalPrices));
        let priceList = Object.values(originalPrices).sort();
        dresFull.spu.price = dresFull.spu.topPrice = dresFull.spu.pubPriceLow = dresFull.spu.pubPriceTop = priceList[priceList.length - 1];
        dresFull.spu.lowPrice = priceList[0];

        return dresFull;
    }

    for (let value of codeValues) {
        let price = value == 0 ? 'pubPrice' : `price${value}`;
        if (preferentialMode == 0) {
            dresFull.skus.map(data => data[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3));
            // dresFull.splitSkus.map(data => data[price] = parseFloat(common.mul(dresFull.spu[price], discount).toFixed(3)));
            (dresFull.splitSkus.length != 0) && (dresFull.splitSkus.map(data => data[price] = parseFloat(common.mul(data[price], discount).toFixed(3))));
        }
        else if (preferentialMode == 1) {
            dresFull.skus.map(data => data[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3)));
            dresFull.spu[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3));
            // dresFull.splitSkus.map(data => data[price] = parseFloat(common.sub(dresFull.spu[price], discount).toFixed(3)));
            (dresFull.splitSkus.length != 0) && (dresFull.splitSkus.map(data => data[price] = parseFloat(common.sub(data[price], discount).toFixed(3))));
        } else {
            console.warn(`不存在preferentialMode为${preferentialMode}`);
        }
    }
    if (preferentialMode == 0) {
        dresFull.spu.pubPriceLow = parseFloat(common.mul(dresFull.spu.pubPriceLow, discount).toFixed(3));
        dresFull.spu.pubPriceTop = parseFloat(common.mul(dresFull.spu.pubPriceTop, discount).toFixed(3));
        dresFull.spu.topPrice = parseFloat(common.mul(dresFull.spu.topPrice, discount).toFixed(3));
        dresFull.spu.lowPrice = parseFloat(common.mul(dresFull.spu.lowPrice, discount).toFixed(3));
    }
    else if (preferentialMode == 1) {
        dresFull.spu.pubPriceLow = parseFloat(common.sub(dresFull.spu.pubPriceLow, discount).toFixed(3));
        dresFull.spu.pubPriceTop = parseFloat(common.sub(dresFull.spu.pubPriceTop, discount).toFixed(3));
        dresFull.spu.topPrice = parseFloat(common.sub(dresFull.spu.topPrice, discount).toFixed(3));
        dresFull.spu.lowPrice = parseFloat(common.sub(dresFull.spu.lowPrice, discount).toFixed(3));
    }

    return dresFull;
};