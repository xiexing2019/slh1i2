const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ssAccount = require('../../../data/ssAccount');
const saasBaseManger = require('../../../help/saas/saasBase');
const cartManage = require('../../../help/cartManage');
const getShop = require('../../../help/shop/getShop');
const ss = require('../../../../reqHandler/ss');

// http://zentao.hzdlsoft.com:6082/zentao/bug-view-13269.html
describe.skip('价格管理同步', function () {
    this.timeout(TESTCASE.timeout);
    const saas = saasBaseManger.setupSaas();
    const sellerShop = getShop.getSellerShop({ seller: 'saas' });
    let priceList, sellerInfo, valList;
    before('商城卖家登录', async function () {
        sellerInfo = await sellerShop.switchSeller({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        valList = await ss.spdresb.getPriceTypeListInApp().then(res => res.result.data.pricedecOptions.map(ele => ele.id));
        await sellerShop.getConfigParam({ code: 'custom_rounding_method', ownerId: sellerInfo.tenantId });
        for (val of valList) {
            describe(`修改价格展示方式-${val}`, function () {
                before('修改价格展示方式', async function () {
                    await sellerShop.switchSeller({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                    // await sellerShop.getConfigParam({ code: 'custom_rounding_method', ownerId: sellerInfo.tenantId });
                    await sellerShop.updateConfigParam({ code: 'custom_rounding_method', val: val });
                    priceList = await ss.spdresb.getPriceTypeListInApp().then(res => res.result.data);
                    priceList.pricedecOptions.forEach(ele => (ele.name == '保留两位小数') && (ele.name = '保留2位小数'))
                });
                it('saas价格管理', async function () {
                    await saas.slh1Login();
                    let saasPriceRes = await saas.getPriceSetting();
                    common.isApproximatelyEqualAssert(priceList, saasPriceRes);
                });
            });
        }
    });
    it('', async function () {

    });

});