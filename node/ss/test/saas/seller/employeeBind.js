const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const basicJsonparam = require('../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../data/getBasicData');
const caps = require('../../../../data/caps');
const ssConfigParam = require('../../../help/configParamManager');
const billManage = require('../../../help/billManage');
const basicJson = require('../../../help/basicJson');
const fs = require('fs');
const path = require('path');
const dresManage = require('../../../help/dresManage');
const salesRequestHandler = require('../../../../slh1/help/salesHelp/salesRequestHandler');
const mainReqHandler = require('../../../../slh1/help/basicInfoHelp/mainDataReqHandler');
const basiceJson = require('../../../../slh1/help/basiceJsonparam');
const employeeManage = require('../../../help/employeeManage');
const roleManage = require('../../../help/roleManage');


describe('新建微商城员工', async function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, roleArr;
    const employee = employeeManage.setupEmployee();
    before('新建微商城员工', async function () {
        //用户A注册开通微商城，登录
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);

        const role = roleManage.setupRole();
        const roleList = await role.getRoleListAtShop();
        // console.log(roleList);
        roleArr = [];
        roleList.forEach((role, index) => {
            if (role.code != 'gm' && role.code != 'bigBoss') {
                // roleList.splice(index, 1);
                roleArr.push({ code: role.code, id: role.id });
            }
        });
        // console.log(roleArr);
        // 角色列表没有除gm,bigboss以外的角色则结束当前脚本
        if (roleArr.length < 1) {
            console.warn(`角色列表没有除gm,bigboss以外的角色:${JSON.stringify(roleArr)}`);
            this.parent ? this.parent.skip() : this.skip();
        };
        //新建员工A
        await employee.newOrEditStaff({
            userName: '员工A' + common.getRandomStr(5),
            mobile: common.getRandomMobile(),
            rem: '员工A备注' + common.getRandomStr(5),
            roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
        });
    });
    after('删除微商城员工', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        await employee.deleteStaff();
    });
    it('员工详情', async function () {
        const staffInfo = await employee.getStaffInfo();
        common.isApproximatelyEqualAssert(employee.getEmployee(), staffInfo);
    });
    describe('slh新建员工', async function () {
        this.timeout(TESTCASE.timeout);
        let depidList, roleList, saveStaffRes, staffInfo, editStaffRes,
            clerkTypes = common.randomSort([{ 0: '普通店员' }, { 1: '店长' }]);
        before('slh新建员工', async function () {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });
            await getBasticData.getBasicID();
            depidList = await common.callInterface('cs-curdetname', {
                epid: LOGINDATA.epid
            }).then(res => res.dataList);
            console.log(`\ndepidList=${JSON.stringify(depidList)}`);

            roleList = await mainReqHandler.getRoleList(LOGINDATA.epid).then(res => common.randomSort(res.result.dataList));
            //新增员工
            saveStaffRes = await mainReqHandler.saveStaff(basiceJson.saveStaffJson({
                code: employee.phone,
                roleids: roleList[0].id,
                depid: depidList.find(obj => obj.name == '默认店').id,
                mobile: employee.phone,
                // mallStaffFlag: 1,
                clerktype: Object.keys(clerkTypes[0]).toString(),
            }));
            // console.log(`saveStaffRes=${JSON.stringify(saveStaffRes)}`);
            staffInfo = await mainReqHandler.getStaffInfo({ pk: saveStaffRes.result.pk });
            console.log(`\nstaffInfo=${JSON.stringify(staffInfo)}`)
            await common.loginDo({ logid: saveStaffRes.params.jsonparam.code });
            await ss.spugr.slhUserLogin({
                token: sellerInfo.accessToken,
                code: LOGINDATA.code,
                roleCodes: LOGINDATA.rolecode,
                slhSessionId: LOGINDATA.sessionid,
                tenantId: sellerInfo.tenantId,
                slhUserId: LOGINDATA.id,
                mobile: employee.phone,
            });
        });
        it('员工详情', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const staffInfo = await employee.getStaffInfo();
            console.log(`\nstaffInfo.slhFlag=${JSON.stringify(staffInfo.slhFlag)}`);
            expect(staffInfo, '商陆花flag不为一').to.include({ slhFlag: 1 });
        });
        it('商陆花停用员工', async function () {
            await common.loginDo({ epid: ssAccount.saas.epid });
            const disableStaff = await mainReqHandler.disableStaff({ check: false, pk: saveStaffRes.result.pk });
            // expect(disableStaff.result, '商陆花应不能删除微商城操作员').to.include({ error: '请使用衣科商城app维护商城操作员' });
            expect(disableStaff.result, '商陆花删除微商城操作员失败').to.include({ val: 'ok' });
        });
    });
});


describe('slh新建员工', async function () {
    this.timeout(TESTCASE.timeout);
    let depidList, roleList, saveStaffRes, staffInfo, editStaffRes, mobile = common.getRandomMobile(), clerkTypes = common.randomSort([{ 0: '普通店员' }, { 1: '店长' }]);
    const employee = employeeManage.setupEmployee();
    before('slh新建员工', async function () {
        await caps.updateEnvByName({ name: ssAccount.saas.ename });
        caps.epid = ssAccount.saas.epid;
        await common.loginDo({ epid: ssAccount.saas.epid });
        await getBasticData.getBasicID();
        depidList = await common.callInterface('cs-curdetname', {
            epid: LOGINDATA.epid
        }).then(res => res.dataList);
        console.log(`\ndepidList=${JSON.stringify(depidList)}`);

        roleList = await mainReqHandler.getRoleList(LOGINDATA.epid).then(res => common.randomSort(res.result.dataList));
        //新增员工
        saveStaffRes = await mainReqHandler.saveStaff(basiceJson.saveStaffJson({
            code: mobile,
            roleids: roleList[0].id,
            depid: depidList.find(obj => obj.name == '默认店').id,
            mobile: mobile,
            mallStaffFlag: 1,
            clerktype: Object.keys(clerkTypes[0]).toString(),
        }));
        // console.log(`saveStaffRes=${JSON.stringify(saveStaffRes)}`);
        staffInfo = await mainReqHandler.getStaffInfo({ pk: saveStaffRes.result.pk });
        console.log(`\nstaffInfo=${JSON.stringify(staffInfo)}`)
    });
    after('删除微商城员工', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        await employee.deleteStaff();
    });
    describe('新建微商城员工', async function () {
        this.timeout(TESTCASE.timeout);
        let sellerInfo, roleArr = [];
        before('新建微商城员工', async function () {
            //用户A注册开通微商城，登录
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            sellerInfo = _.cloneDeep(LOGINDATA);
            // console.log(`sellerInfo=${JSON.stringify(sellerInfo)}`);
            const role = roleManage.setupRole();
            const roleList = await role.getRoleListAtShop();
            // console.log(roleList);
            roleList.forEach((role, index) => {
                if (role.code != 'gm' && role.code != 'bigBoss') roleArr.push({ code: role.code, id: role.id });
            });
            // console.log(roleArr);
            // 角色列表没有除gm,bigboss以外的角色则结束当前脚本
            if (roleArr.length < 1) {
                console.warn(`角色列表没有除gm,bigboss以外的角色:${JSON.stringify(roleArr)}`);
                this.parent ? this.parent.skip() : this.skip();
            };
            //新建员工
            await employee.newOrEditStaff({
                userName: '员工A' + common.getRandomStr(5),
                mobile: mobile,
                rem: '员工A备注' + common.getRandomStr(5),
                roleId: roleArr[common.getRandomNum(0, roleArr.length - 1)].id,
            });
            await common.loginDo({ logid: saveStaffRes.params.jsonparam.code })
            await ss.spugr.slhUserLogin({
                token: sellerInfo.accessToken,
                code: LOGINDATA.code,
                roleCodes: LOGINDATA.rolecode,
                slhSessionId: LOGINDATA.sessionid,
                tenantId: sellerInfo.tenantId,
                slhUserId: LOGINDATA.id,
                mobile: employee.phone,
            });
        });
        it('员工详情', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const staffInfo = await employee.getStaffInfo();
            // console.log(`\nstaffInfo.slhFlag=${JSON.stringify(staffInfo.slhFlag)}`);
            expect(staffInfo, '商陆花flag不为一').to.include({ slhFlag: 1 });
        });
        it('商陆花停用员工', async function () {
            //停用员工
            await common.loginDo({ epid: ssAccount.saas.epid });
            const disableStaff = await mainReqHandler.disableStaff({ check: false, pk: saveStaffRes.result.pk });
            // expect(disableStaff.result, '商陆花应不能删除微商城操作员').to.include({ error: '请使用衣科商城app维护商城操作员' });
            expect(disableStaff.result, '商陆花删除微商城操作员失败').to.include({ val: 'ok' });
        });
    });
});