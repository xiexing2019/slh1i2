const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const basicInfoReqHandler = require('../../../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const basicJsonparam = require('../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../data/getBasicData');
const caps = require('../../../../data/caps');
const format = require('../../../../data/format');
const custManager = require('../../../../reqHandler/slh1/index');
const ssConfigParam = require('../../../help/configParamManager');
const slhCustomerManager = require('../../../../reqHandler/slh1/customerManager');
const fs = require('fs');
const path = require('path');
const dresManage = require('../../../help/dresManage');
const slh1 = require('../../../../reqHandler/slh1');


//文档信息
const docData = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../data/doc.json')))[caps.name];
const image = _.cloneDeep(docData.image.model[common.getRandomNum(0, 9)]);
const video = _.cloneDeep(docData.video.dres[0]);




describe('二维码绑定saas', function () {
    this.timeout(30000);
    let bindData, sellerInfo, saasCustomerId, saasData, clientInfo, mobileNum, qlRes, qfRes, styleJson, newStyle;
    mobileNum = common.getRandomMobile();
    console.log(mobileNum);

    before(async () => {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
    });
    after('同步商品', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        await ss.ssugr.synDresForSaas({ spuSyncDays: 5 });
    });
    describe.skip('老版才能新增门店', function () {
        let bossInfo;
        before('老板登录', async () => {
            await ssReq.ssSellerLogin({ code: 13486197911, shopName: ssAccount.saas.shopName });
            bossInfo = _.cloneDeep(LOGINDATA);
        });
        it('老板有新增门店权限', async function () {
            expect(bossInfo, `出错了，老板没有新增门店权限！`).to.be.include({ companyOwner: 1 });
        });
        it.skip('不是老板没有新增门店权限', async function () {
            await ssReq.ssSellerLogin();
            let sellerInfo = _.cloneDeep(LOGINDATA);
            expect(sellerInfo, `出错了，不是老板不应该有新增门店权限！`).to.be.not.include({ companyOwner: 1 });
        });
    });

    describe('saas绑定二维码', function () {
        before('卖家登录', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        });
        it('saas获取绑定二维码', async function () {
            bindData = await ss.ssugr.getBindCodeUrl({ saasType: 1, saasShopId: ssAccount.saas.saasShopId, saasShopName: ssAccount.saas.saasShopName, saasUnitId: ssAccount.saas.epid, saasSn: ssAccount.saas.saasSn }).then(res => res.result.data);
            expect(bindData.codeUrl).to.not.be.null;
            expect(bindData.bindCode).to.not.be.null;
        });
        it('查询saas开通情况', async function () {
            let qfRes = await ss.ssugr.getDwxxStatusByPhone().then(res => res.result.data);
            expect(qfRes.val, `查询saas开通情况失败`).to.equal('1');
        });
        it('绑定码绑定saas', async function () {
            let bindByCodeRes = await ss.ssugr.bindByCode({ bindCode: bindData.bindCode, check: false }).then(res => res.result);
            console.log(bindByCodeRes);
            expect(bindByCodeRes.code, `已经绑定了saas的居然可以再次绑定`).to.equal(-10);
        });
        it('saas获取门店绑定关联', async function () {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });
            let res = await slhCustomerManager.getShopBind();
            console.log(res.result.data);
            expect(res.result.data.shopName, `saas绑定失败`).to.be.equal('默认店');
        });
    });

    describe('同步saas商品到商城', function () {
        let exp;
        before('saas新增款号-同步saas商品到商城', async () => {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });
            await getBasticData.getBasicID();

            styleJson = basicJsonparam.addGoodJson({ fileid: ssAccount.saas.fileid });
            newStyle = await basicInfoReqHandler.editStyle({ jsonparam: styleJson });
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            // await ss.ssugr.synDresForSaas({ spuSyncDays: 1 });
        });
        it('新增款号库存', async function () {
            await common.loginDo({ epid: ssAccount.saas.epid });
            let json = basicJsonparam.purchaseJson();
            json.details.forEach(ele => {
                ele.styleid = newStyle.result.val;
                ele.matCode = styleJson.code;
            });
            await common.editBilling(json);
        });
        it('查询商品列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            this.retries(3);
            await common.delay(10000);
            qlRes = await sp.spdresb.findSellerSpuList({ nameLike: newStyle.params.code })
                .then(res => res.result.data.rows.find(data => data.slhId == newStyle.result.val));
            exp = format.dataFormat(newStyle.params, 'discount;name;namePy=name;title=name;code;pubPrice=stdprice1;isAllowReturn=isallowreturn;spec1IdList=sizeids')
            exp.spec1IdList = exp.spec1IdList.split(',');
            // console.log(exp, qlRes);
            common.isApproximatelyEqualAssert(exp, qlRes);
        });
        it('查询商品详情', async function () {
            qfRes = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result);
            console.log(JSON.stringify(qfRes));

            common.isApproximatelyEqualAssert(exp, qfRes.data.spu);
        });
    });

    describe('视频/货品图片修改支持回传到saas', async function () {
        let dresBefore;
        before('商城开启回传参数', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'dres_spu_save_sync_to_slh' });
            await ssParam.updateParam({ val: 1 });
            await common.loginDo({ epid: ssAccount.saas.epid });
            dresBefore = await slh1.slh1MetManager.queryGoodFull({ pk: qfRes.data.spu.slhId });
        });
        after('商城关闭回传参数', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'dres_spu_save_sync_to_slh' });
            await ssParam.updateParam({ val: 0 });
        });
        describe('修改商品图片', function () {
            before('商城修改商品图片', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                image.typeId = 1;
                await sp.spdresb.updateSpuForSeller({ id: qfRes.data.id, docHeader: [image], docContent: [image] });
            });
            it('商城查看商品详情', async function () {
                const dresDetail = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result.data);
                const dres = JSON.parse(dresDetail.spu.docContent);
                common.isApproximatelyEqualAssert(image, dres[0]);
            });
            it('saas查看商品详情', async function () {
                this.retries(3);
                await common.delay(1000);
                await common.loginDo({ epid: ssAccount.saas.epid });
                const dresDetail = await slh1.slh1MetManager.queryGoodFull({ pk: qfRes.data.spu.slhId });
                console.log(`dresDetail.fileid= ${dresDetail.fileid}`);
                console.log(`dresBefore.fileid= ${dresBefore.fileid}`);
                expect(dresDetail.fileid, `商城修改图片同步到saas失败`).to.not.be.equal(dresBefore.fileid);
            });
        });
        describe('修改商品视频', async function () {
            before('商城修改商品视频', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                video.typeId = 3;
                await sp.spdresb.updateSpuForSeller({ id: qfRes.data.id, docHeader: [image, video], docContent: [image, video] });
            });
            it('商城查看商品详情', async function () {
                const dresDetail = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result.data);
                const dres = JSON.parse(dresDetail.spu.docContent);
                common.isApproximatelyEqualAssert(video, dres[1]);
            });
            it('saas查看商品详情', async function () {
                this.retries(3);
                await common.delay(500);
                await common.loginDo({ epid: ssAccount.saas.epid });
                const dresDetail = await slh1.slh1MetManager.queryGoodFull({ pk: qfRes.data.spu.slhId });
                expect(dresDetail.videoId, `商城修改视频同步到saas失败`).to.be.not.equal(dresBefore.videoId);
            });
        });
    });

    describe('货品同步时是否允许价格覆盖', function () {
        let ssParam, dres;
        before(async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'allow_sync_spu_price_cover' });
            await ssParam.updateParam({ val: 1 });
            dres = dresManage.setupDres();
            dres.setByDetail(qfRes.data);
        });
        it('商城查询商品价格', async function () {
            let ssEditPriceRes = await dres.getFullById();
            // console.log(`商城改价前查询价格结果=${JSON.stringify(ssEditPriceRes)}`);
        });
        it('商城修改商品价格', async function () {
            let addPrice = common.getRandomNum(1, 10);
            await dres.updateSpuPrice(addPrice);
            let ssEditPriceRes = await dres.getFullById();
            // console.log(`商城改价后查询价格结果=${JSON.stringify(ssEditPriceRes.spu)}`);
            let exp = styleJson.stdprice1 + addPrice;
            expect(ssEditPriceRes.spu.pubPrice).to.be.equal(exp);
        });
        it('saas修改商品价格', async function () {
            await common.loginDo({ epid: ssAccount.saas.epid });
            styleJson.pk = newStyle.result.val;
            let res = await basicInfoReqHandler.editStyle({ jsonparam: styleJson });
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            // await ss.ssugr.synDresForSaas({ spuSyncDays: 1 });
        });
        it('商城查询商品价格', async function () {
            this.retries(3);
            await common.delay(3000);
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            let dresPrice = await dres.getFullById();
            console.log(dresPrice.spu.pubPrice);
            expect(dresPrice.spu.pubPrice, `saas修改价格后同步失败`).to.be.equal(styleJson.stdprice1)
        });
    });

    /**
     * 1、商城店铺开通 余额支付、线下支付
     * 2、商城新增客户、给买家开通余额支付、
     * 3、商陆花新增客户、客户绑定、客户充值
     * 4、商城买家下单支付----商陆花 创建 销售订货单   余额 没有减少
     * 5、商城卖家发货 ---- 商陆花  创建 销售单     余额减少
     */
    describe('商城客户和saas客户关联', function () {
        let user;
        before(async () => {
            // 商城开通余额支付、线下支付、注册新买家
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const open_balance_payment = await ssConfigParam.getSpbParamInfo({ code: 'open_balance_payment', ownerId: LOGINDATA.tenantId });
            await open_balance_payment.updateParam({ val: 1 });
            const open_offline_payment = await ssConfigParam.getSpgParamInfo({ code: 'open_offline_payment', ownerId: LOGINDATA.tenantId });
            await open_offline_payment.updateParam({ val: 1 });
            // 货品无图片是否允许上架
            const allow_picture_exists_shelves = await ssConfigParam.getSpgParamInfo({ code: 'allow_picture_exists_shelves', ownerId: LOGINDATA.tenantId });
            await allow_picture_exists_shelves.updateParam({ val: 1 });
            // await ssReq.ssClientLogin({ mobile: mobileNum, tenantId: sellerInfo.tenantId, sellerId: sellerInfo.id });

            await ssReq.ssClientLogin({ mobile: mobileNum, tenantId: sellerInfo.tenantId, sellerId: sellerInfo.userId });
            clientInfo = _.cloneDeep(LOGINDATA);
            // console.log(clientInfo);

            // 登录saas、新增客户
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });
            saasData = _.cloneDeep(LOGINDATA);
            await getBasticData.getBasicID();
            saasCustomerId = await custManager.slh1CustomerManager.saveCustomerFull(Object.assign({ phone: mobileNum }, basicJsonparam.addCustJson())).then(res => res.result.val);
            console.log(`saasCustomerId:${saasCustomerId}`);

        });
        it('saas手动做线上客户与本地客户的关联', async function () {
            await slhCustomerManager.linkDwxxWithCust({ shopId: saasData.invid, custId: clientInfo.userId, dwId: saasCustomerId });
        });
        it('给客户开通余额支付', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.spmdm.saveMemberParams([{ code: 'open_balance_payment', val: 1 }], { custTenantId: clientInfo.tenantId });
        });
        it('给客户开通线下支付', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.spmdm.saveMemberParams([{ code: 'open_debt_payment', val: 1 }], { custTenantId: clientInfo.tenantId });
        });
        describe('批量保存会员参数信息-关闭', async function () {
            //  余额支付  欠款支付
            let openBalancePayment, openDebtPayment;
            before(async function () {
                // await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                const res = await ss.spmdm.batchSaveMemberParams({ custTenantId: clientInfo.tenantId, jsonParam: [{ code: 'open_balance_payment', val: 0 }, { code: 'open_debt_payment', val: 0 }] });
                console.log('%j', res);
            });
            // open_balance_payment会发消息,通知更新余额 数据更新会比较慢
            it.skip('根据id查询客户详细信息', async function () {
                this.retries(20);
                await common.delay(3000);
                const userInfo = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId }).then(res => res.result.data);
                console.log('%j', userInfo);
                common.isArrayContainArrayAssert([{ v1: 'open_debt_payment', v2: 0 }, { v1: 'open_balance_payment', v2: 0 }], userInfo.paramList);
            });
        });
        describe('批量保存会员参数信息-开启', async function () {
            //  余额支付  欠款支付
            let openBalancePayment, openDebtPayment;
            before(async function () {
                // await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                const res = await ss.spmdm.batchSaveMemberParams({ custTenantId: clientInfo.tenantId, jsonParam: [{ code: 'open_balance_payment', val: 1 }, { code: 'open_debt_payment', val: 1 }] });
                console.log('%j', res);
            });
            it('根据id查询客户详细信息', async function () {
                this.retries(2);
                await common.delay(1000);
                const userInfo = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId }).then(res => res.result.data);
                common.isArrayContainArrayAssert([{ v1: 'open_debt_payment', v2: 1 }, { v1: 'open_balance_payment', v2: 1 }], userInfo.paramList);
            });
        });
    });
    // bug链接：http://zentao.hzdlsoft.com:6082/zentao/bug-view-11422.html
    describe('从未绑定saas的门店切换到绑定saas门店', function () {
        let sellerInfo2;
        before('登录未绑定saas的门店后切换到saas门店', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName2 });
            sellerInfo2 = _.cloneDeep(LOGINDATA);
            await ss.spugr.changeUserShop({ newUnitId: sellerInfo.unitId, mobile: ssAccount.saas.mobile });
        });
        it('卖家根据id查询客户详细信息', async function () {
            const userDetail = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId });
            console.log(`卖家根据id查询客户详细信息=${JSON.stringify(userDetail)}`);
            common.isApproximatelyArrayAssert(paramListExp(), userDetail.result.data.paramList);
        });
    });

    describe('商城改绑saas店铺', function () {
        let shopList, shopA, shopM;
        before(async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        });
        after('同步款号库存默认店', async function () {
            await common.delay(2000);
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.ssugr.syncShopInvIds({ slhInvIds: [shopM.slhShopId] });
        });
        it('查询绑定saas店铺同账套下的店铺列表', async function () {
            shopList = await ss.ssugr.findSaasShopList().then(res => res.result.data.rows);
            console.log(`绑定saas店铺同账套下的店铺列表 = ${JSON.stringify(shopList)}`);
        });
        it('获取验证码', async function () {
            let res = await ss.spugr.sendCodeToCompany();
            expect(res.result.data.val, `验证码发送失败`).not.to.be.null;
        });
        it('验证验证码', async function () {
            await ss.ssugr.checkCaptcha({ captcha: common.getRandomNumStr(5) });
        });
        it('saas店铺改绑门店A', async function () {
            shopA = shopList.find(obj => obj.slhShopName == '门店A');
            delete shopA.syncInvFlag;
            console.log(`shopA=${JSON.stringify(shopA)}`);
            let res = await ss.ssugr.changeBindShop(shopA);
            console.log(`改绑res = ${JSON.stringify(res)}`);
        });
        it.skip('同步款号库存', async function () {
            this.retries(3)
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            let res = await ss.ssugr.syncShopInvIds({ slhInvIds: [shopA.slhShopId], _tid: LOGINDATA.clusterCode, _cid: LOGINDATA.tenantId });
            console.log(JSON.stringify(res));
            await common.delay(5000);
            const styleInfo = await sp.spdresb.getFullById({ id: qlRes.id }).then(res => res.result);
            expect(styleInfo.data.spu.stockNum, `同步门店A的库存失败`).to.be.equal(0);
        });
        it('saas店铺改绑默认店', async function () {
            await common.delay(1000);
            shopM = shopList.find(obj => obj.slhShopName == '默认店');
            delete shopM.syncInvFlag;
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            let res = await ss.ssugr.changeBindShop(shopM);
            console.log(`改绑res = ${JSON.stringify(res)}`);
        });
        it('saas获取门店绑定关联', async function () {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });
            let res = await slhCustomerManager.getShopBind();
            expect(res.result.data.shopName, `saas绑定失败`).to.be.equal('默认店');
        });
        it('saas店铺解绑', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const unBindRes = await ss.ssugr.unBindShop({ check: false });
            expect(unBindRes.result).to.be.include({ "msg": "您还有订单未发货，请先发货后再完成解绑", "msgId": "unbind_shop_bill_num_check_fail" });
        });
    });

});

function paramListExp(params) {
    return [
        {
            "v1": "open_debt_payment",
            "v2": 1,
            "first": "open_debt_payment",
            "second": 1
        },
        {
            "v1": "open_balance_payment",
            "v2": 1,
            "first": "open_balance_payment",
            "second": 1
        }, {
            "v1": "open_cust_offline_pay",
            "v2": 1,
            "first": "open_cust_offline_pay",
            "second": 1
        }
    ]
};