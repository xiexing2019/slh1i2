const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const basicJsonparam = require('../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../data/getBasicData');
const caps = require('../../../../data/caps');
const ssConfigParam = require('../../../help/configParamManager');
const billManage = require('../../../help/billManage');
const basicJson = require('../../../help/basicJson');
const fs = require('fs');
const path = require('path');
const dresManage = require('../../../help/dresManage');
const salesRequestHandler = require('../../../../slh1/help/salesHelp/salesRequestHandler');
const slh1 = require('../../../../reqHandler/slh1');


describe('上次价', function () {
    this.timeout(30000);
    let sellerInfo, saasBillDetail, saasDresList, spuId;
    before('商城都开启上次价参数', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        // 商城开启上次价
        const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'sysc_sssa_last_price' });
        let res = await ssParam.updateParam({ val: 1 });
        // saas开启上次价
        await caps.updateEnvByName({ name: ssAccount.saas.ename });
        caps.epid = ssAccount.saas.epid;
        await common.loginDo({ epid: ssAccount.saas.epid });
        await getBasticData.getBasicID();
        await common.setGlobalParam('sales_use_lastsaleprice', 1);
    });
    after('关闭上次价', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        // 商城关闭上次价
        const ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'sysc_sssa_last_price' });
        let res = await ssParam.updateParam({ val: 0 });
    });
    it('saas销售开单', async function () {
        await common.loginDo({ epid: ssAccount.saas.epid });
        saasDresList = await slh1.slh1MetManager.queryGoodList({ flag: 0 }, true).then(res => res.dataList);  // flag 是否停用 0 否 1 是  将查询到的商品列表第一条用来开单
        // console.log(`saasDresList= ${JSON.stringify(saasDresList)}`);
        json = basicJsonparam.lastPriceJson(saasDresList[0]);
        json.dwid = ssAccount.saas.saasCustomerId;
        const billId = await common.editBilling(json).then(res => res.result.pk);
        saasBillDetail = await salesRequestHandler.salesQueryBilling(billId).then(res => res.result.details.find(data => data.price));
        // console.log(JSON.stringify(saasBillDetail));
    });

    describe('上次价-商品详情', function () {
        before('查询商品列表', async function () {
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            spuId = await ss.spchb.getDresSpuList({ tenantId: sellerInfo.tenantId, code: saasDresList[0].code }).then(res => res.result.data.rows[0].id);
        });
        it('查询商品详情', async function () {
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            console.log(`商品详情 = ${JSON.stringify(getFullForBuyer)}`);
            expect(getFullForBuyer.spu.lastPrice.toString(), '商品详情上次价同步失败').to.be.equal(saasBillDetail.price);
        });
    });

    describe('上次价-购物车', function () {
        let carts0;
        before('清空购物车', async function () {
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            await sp.spTrade.emptyCart();
        });
        it('加入购物车', async function () {
            const dresFull = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            let cartJson = basicJson.cartJson2(dresFull);
            cartJson.carts[0].extPropJson = { lastPriceFlag: 1 };
            cartJson.carts[0].skuPrice = saasBillDetail.price;
            // console.log(`购物车JSON = ${JSON.stringify(cartJson)}`);
            const saveCartInBatchs = await sp.spTrade.saveCartInBatchs(cartJson);
            // console.log(`\n saveCartInBatchs=${JSON.stringify(saveCartInBatchs)}`);
            carts0 = cartJson.carts;
        });
        it('查询购物车', async function () {
            const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows[0]);
            // console.log(`\ncartList=${JSON.stringify(cartList)}`);
            common.isApproximatelyEqualAssert(carts0[0], cartList.carts[0]);
        });
        describe('修改商品价格后查看购物车', function () {
            let dres;
            before('修改商品价格', async () => {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                let qfRes = await sp.spdresb.getFullById({ id: spuId }).then(res => res.result);
                dres = dresManage.setupDres();
                dres.setByDetail(qfRes.data);
                let addPrice = common.getRandomNum(1, 10);
                await dres.updateSpuPrice(addPrice);
            });
            it('查询购物车', async function () {
                await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
                const cartList = await ss.spcart.getShoppingCartList({ shopId: sellerInfo.tenantId, pageSize: 0 }).then(res => res.result.data.rows[0]);
                // console.log(`\ncartList=${JSON.stringify(cartList)}`);
                common.isApproximatelyEqualAssert(carts0[0], cartList.carts[0]);
            });
        });
    });

    describe('上次价-订单', function () {
        let purRes;
        before('创建采购单', async () => {
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const dresDetail = await sp.spdresb.getFullForBuyer({ spuId: spuId, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode }).then(res => res.result.data);
            console.log(JSON.stringify(dresDetail));
            dresDetail.spu.pubPrice = dresDetail.spu.lastPrice;
            dresDetail.skus.forEach(obj => { obj.pubPrice = dresDetail.spu.lastPrice });
            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = await billManage.mockPurParam(dresDetail, { count: 1, num: 1 });
            purJson.orders[0].main.sellerId = sellerInfo.tenantId;
            purJson.orders[0].details[0].extPropJson = { lastPriceFlag: 1 };
            purRes = await billManage.createPurBill(purJson);
            // console.log((`\npurRes=${JSON.stringify(purRes)}`));
        });
        it('买家查询订单列表', async function () {
            const qlRes = await sp.spTrade.purFindBills({ searchToken: purRes.result.data.rows[0].billNo }).then(res => res.result.data.rows[0]);
            // console.log(`\n 买家采购单列表=${JSON.stringify(qlRes)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details[0], qlRes.skus[0]);
        });
        it('买家采购单详情', async function () {
            const purInfo = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            // console.log(`\n 买家采购单详情=${JSON.stringify(purInfo)}`);
            common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details[0], purInfo.skus[0]);
        });
        describe('卖家查询销售单', function () {
            let salesRes;
            it('查询销售单列表', async function () {
                await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
                salesRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo }).then(res => res.result.data.rows[0]);
                common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details[0], salesRes.skus[0]);
            });
            it('查询单据详情', async function () {
                const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: salesRes.bill.id }).then(res => res.result.data);
                common.isApproximatelyEqualAssert(purRes.params.jsonParam.orders[0].details[0], salesBillInfo.skus[0]);
            });
        });
    });

});