const common = require('../../../../../lib/common');
const ssReq = require('../../../../help/ssReq');
const sp = require('../../../../../reqHandler/sp');
const ss = require('../../../../../reqHandler/ss');
const ssAccount = require('../../../../data/ssAccount');
const basicJsonparam = require('../../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../../data/getBasicData');
const caps = require('../../../../../data/caps');
const format = require('../../../../../data/format');
const billManage = require('../../../../help/billManage');
const basicJson = require('../../../../help/basicJson');
const ssConfigParam = require('../../../../help/configParamManager');
const slh1 = require('../../../../../reqHandler/slh1');
const saasBaseManger = require('../../../../help/saas/saasBase');

describe('线下支付订单', function () {
    this.timeout(30000);
    let sellerInfo, ssParam, clientInfo;
    before('关闭自动审核,开启线下支付', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
        clientInfo = _.cloneDeep(LOGINDATA);
        // 商城关闭自动审核
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        ssParam = await ssConfigParam.getSpgParamInfo({ ownerId: sellerInfo.tenantId, code: 'offline_pay_auto_audit' });
        await ssParam.updateParam({ val: 0 });
        await ss.spmdm.saveMemberParams([{ code: 'open_cust_offline_pay', val: 1 }], { custTenantId: clientInfo.tenantId });
    });

    describe('人工审核', function () {
        let purRes, payRes, qlRes, clientInfo, selesListRes, deposit = common.getRandomNum(1, 10);
        before('创建采购单', async () => {
            qlRes = await sp.spdresb.findSellerSpuList({ flags: 1, orderBy: 'createdDate', orderByDesc: true })
                .then(res => res.result.data.rows[0]);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: qlRes.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(JSON.stringify(getFullForBuyer));
            const areaJson = await basicJson.getAreaJson();
            const json = await basicJson.addAddrJson(areaJson);
            json.recInfo.isDefault = 1;
            await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, payKind: 3 });
            purRes = await billManage.createPurBill(purJson);
            // console.log(`创建采购单= ${JSON.stringify(purRes)}`);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            // console.log(`销售单列表 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('saas给客户充值', async function () {
            await caps.updateEnvByName({ name: ssAccount.saas.ename });
            caps.epid = ssAccount.saas.epid;
            await common.loginDo({ epid: ssAccount.saas.epid });

            await getBasticData.getBasicID();
            let json = basicJsonparam.rechargeJson();
            json.dwid = ssAccount.saas.saasCustomerId;
            await common.editBilling(json);
        });
        it('创建支付', async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            let payRes = await sp.spTrade.createPay({ payType: -99, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money, appId: LOGINDATA.extProps.wxAppId });
            // console.log(payRes);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            // console.log(`销售单列表 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill.payFlag).to.be.equal(-1);
        });
        it('设置定金', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.spsales.offlineBillSetDeposit({ id: selesListRes.bill.id, deposit: deposit });
        });
        it('审核线下支付', async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.spsales.verifyOfflineBill({ id: selesListRes.bill.id, accept: true, payWay: 1 });
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            // console.log(`销售单列表 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill.payFlag).to.be.equal(5);
        });
        it('买家查询采购单详情', async function () {
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const sellRes = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            console.log(`采购单详情= ${JSON.stringify(sellRes)}`);
            sellRes.skus.forEach(obj => { expect(obj, `采购单spuFlag不等于1或不存在`).to.be.include({ spuFlag: 1 }) });
        });
        it('saas查询销售订货列表', async function () {
            await common.loginDo({ epid: ssAccount.saas.epid });
            let saasOrderListRes = await slh1.slh1SelesOrderManager.querySelesOrderList().then(res => res.result.dataList.find(obj => obj.mallBillNo == purRes.result.data.rows[0].billNo));
            console.log(JSON.stringify(saasOrderListRes));
            expect(saasOrderListRes, `线下订单定金支付同步失败`).to.be.include({ paysum: `${deposit}` });
        });
    });

    describe('自动审核', function () {
        let qlRes, selesListRes;
        before('开启自动审核', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ssParam.updateParam({ val: 1 });
            qlRes = await sp.spdresb.findSellerSpuList({ flags: 1, orderBy: 'createdDate', orderByDesc: true })
                .then(res => res.result.data.rows[0]);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            // clientInfo = _.cloneDeep(LOGINDATA);

            const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: qlRes.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });
            // console.log(JSON.stringify(getFullForBuyer));
            const areaJson = await basicJson.getAreaJson();
            const json = await basicJson.addAddrJson(areaJson);
            json.recInfo.isDefault = 1;
            await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);

            await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
            const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, payKind: 3 });
            purRes = await billManage.createPurBill(purJson);
        });
        after('关闭自动审核', async () => {
            await ssParam.updateParam({ val: 0 });
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            // console.log(`销售单列表 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill.payFlag).to.be.equal(0);
        });
        it('创建支付', async function () {
            this.retries(3);
            await common.delay(1000);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            let payRes = await sp.spTrade.createPay({ payType: -99, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money, appId: LOGINDATA.extProps.wxAppId });
            // console.log(payRes);
        });
        it('卖家查询销售单列表', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            selesListRes = await sp.spTrade.salesFindBills({ searchToken: purRes.result.data.rows[0].billNo, statusType: 0 })
                .then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo));
            // console.log(`销售单列表 = ${JSON.stringify(selesListRes)}`);
            expect(selesListRes.bill.payFlag).to.be.equal(5);
        });
        it('买家查询采购单详情', async function () {
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const sellRes = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
            console.log(`采购单详情= ${JSON.stringify(sellRes)}`);
            sellRes.skus.forEach(obj => { expect(obj, `采购单spuFlag不等于1或不存在`).to.be.include({ spuFlag: 1 }) });
        });
    });

    // http://zentao.hzdlsoft.com:6082/zentao/bug-view-13307.html
    describe('支付欠款--offline', function () {
        const saas = saasBaseManger.setupSaas();
        let purBillRes, payRes;
        before('saas新增欠款单', async function () {
            await saasBaseManger.slh1Login();
            await getBasticData.getBasicID();
            let json = basicJsonparam.salesJson();
            json.card = json.cash = json.remit = 0;
            json.dwid = ssAccount.saas.saasCustomerId;
            purBillRes = await saas.addSalesOrder(json);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId });
        });
        it('买家支付欠款', async function () {
            payRes = await sp.spTrade.createPay({
                payType: 5,
                payMethod: 2,
                payerOpenId: LOGINDATA.wxOpenId,
                orderIds: [purBillRes.bill.id],
                payMoney: purBillRes.bill.money
            });
            console.log(`payRes=${JSON.stringify(payRes)}`);
            await sp.spTrade.receivePayResult({
                mainId: payRes.result.data.payDetailId,
                amount: payRes.params.jsonParam.payMoney,
                purBillId: purBillRes.bill.id
            });
        });
    });
});