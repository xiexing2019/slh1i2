const common = require('../../../../../lib/common');
const ssReq = require('../../../../help/ssReq');
const ss = require('../../../../../reqHandler/ss');
const ssAccount = require('../../../../data/ssAccount');
const ssConfigParam = require('../../../../help/configParamManager');


describe('线下支付', function () {
    this.timeout(TESTCASE.timeout);
    let sellerInfo, open_offline_payment;

    before('卖家开启新客户线下支付', async () => {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        open_offline_payment = await ssConfigParam.getSpgParamInfo({ code: 'open_offline_payment', ownerId: LOGINDATA.tenantId });
        await open_offline_payment.updateParam({ val: 1 });
    });

    after('卖家关闭新客户线下支付', async () => {
        await open_offline_payment.updateParam({ val: 0 });
    });

    describe('卖家开启新客户线下支付', function () {
        let clientInfo;
        before('商城新增客户', async () => {
            await ssReq.ssClientLogin({ mobile: common.getRandomMobile(), tenantId: sellerInfo.tenantId, sellerId: sellerInfo.userId });
            clientInfo = _.cloneDeep(LOGINDATA);
        });
        it('根据id查询客户详细信息', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const userInfo = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId }).then(res => res.result.data.paramList.find(obj => obj.v1 == 'open_cust_offline_pay'));
            common.isApproximatelyEqualAssert(userInfo, { v1: 'open_cust_offline_pay', v2: 1, first: 'open_cust_offline_pay', second: 1 });
        });
    });

    describe('卖家关闭新客户线下支付', function () {
        let clientInfo;
        before('商城新增客户', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await open_offline_payment.updateParam({ val: 0 });
            await ssReq.ssClientLogin({ mobile: common.getRandomMobile(), tenantId: sellerInfo.tenantId, sellerId: sellerInfo.userId });
            clientInfo = _.cloneDeep(LOGINDATA);
        });
        it('根据id查询客户详细信息', async function () {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            const userInfo = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId }).then(res => res.result.data.paramList.find(obj => obj.v1 == 'open_cust_offline_pay'));
            common.isApproximatelyEqualAssert(userInfo, { v1: 'open_cust_offline_pay', v2: 0, first: 'open_cust_offline_pay', second: 0 });
        });
        it('给客户开通线下支付', async function () {
            await ss.spmdm.saveMemberParams([{ code: 'open_cust_offline_pay', val: 1 }], { custTenantId: clientInfo.tenantId });
        });
        it('根据id查询客户详细信息', async function () {
            const userInfo = await ss.spmdm.getMemberByUserId({ userId: clientInfo.userId }).then(res => res.result.data.paramList.find(obj => obj.v1 == 'open_cust_offline_pay'));
            common.isApproximatelyEqualAssert(userInfo, { v1: 'open_cust_offline_pay', v2: 1, first: 'open_cust_offline_pay', second: 1 });
        });
    });

    describe('批量批量开启线下支付', function () {
        before('批量开启线下支付', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            let res = await ss.spmdm.batchSaveMembersParamWithCode([{ code: 'open_cust_offline_pay', val: '1' }]);
            console.log(JSON.stringify(res));

        });
        it('查询客户列表', async function () {
            const clientList = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
            clientList.forEach(ele => {
                let exp = ele.paramList.find(obj => obj.v1 == 'open_cust_offline_pay');
                common.isApproximatelyEqualAssert(exp, { v1: "open_cust_offline_pay", v2: 1 });
            });
        });
    });

    describe('批量批量关闭线下支付', function () {
        before('批量关闭线下支付', async () => {
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
            await ss.spmdm.batchSaveMembersParamWithCode([{ code: 'open_cust_offline_pay', val: '0' }]);
        });
        it('查询客户列表', async function () {
            const clientList = await ss.spmdm.getMemberList().then(res => res.result.data.rows);
            clientList.forEach(ele => {
                let exp = ele.paramList.find(obj => obj.v1 == 'open_cust_offline_pay');
                common.isApproximatelyEqualAssert(exp, { v1: "open_cust_offline_pay", v2: 0 });
            });
        });
    });
});