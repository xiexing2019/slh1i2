const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ss = require('../../../../reqHandler/ss');
const ssAccount = require('../../../data/ssAccount');
const basicJsonparam = require('../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../data/getBasicData');
const caps = require('../../../../data/caps');
const format = require('../../../../data/format');
const billManage = require('../../../help/billManage');
const basicJson = require('../../../help/basicJson');
const slh1 = require('../../../../reqHandler/slh1');
const saasManager = require('../../../help/saas/saasBase');

describe('余额支付', function () {
    this.timeout(30000);
    const saas = saasManager.setupSaas();
    let purRes, salesListRes, salesBillFull, sellerInfo, qlRes, clientInfo;
    before(async () => {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);

        qlRes = await sp.spdresb.findSellerSpuList({ flags: 1, orderBy: 'createdDate', orderByDesc: true })
            .then(res => res.result.data.rows[0]);
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
        clientInfo = _.cloneDeep(LOGINDATA);
        // console.log(clientInfo);

        const getFullForBuyer = await sp.spdresb.getFullForBuyer({ spuId: qlRes.id, _tid: sellerInfo.tenantId, _cid: sellerInfo.clusterCode });

        const areaJson = await basicJson.getAreaJson();
        const json = await basicJson.addAddrJson(areaJson);
        json.recInfo.isDefault = 1;
        await sp.spmdm.saveUserRecInfo(json).then(res => res.result.data.val);

        await sp.spmdm.getUserDefaultRecInfo().then((res) => LOGINDATA.defAddressId = res.result.data.recInfo.id);
        const purJson = basicJson.purJson({ styleInfo: getFullForBuyer, payKind: 1 });
        purRes = await billManage.createPurBill(purJson);
        // 开通余额支付
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        await ss.spmdm.saveMemberParams([{ code: 'open_balance_payment', val: 1 }], { custTenantId: clientInfo.tenantId });
    });
    it('saas给客户充值', async function () {
        await caps.updateEnvByName({ name: ssAccount.saas.ename });
        caps.epid = ssAccount.saas.epid;
        await common.loginDo({ epid: ssAccount.saas.epid });

        await getBasticData.getBasicID();
        let json = basicJsonparam.rechargeJson();
        json.dwid = ssAccount.saas.saasCustomerId;
        await common.editBilling(json);
    });
    it('创建支付', async function () {
        this.retries(5);
        await common.delay(10000);   // 审核环境不加等待和重试就提示店铺未开通余额支付
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
        payRes = await sp.spTrade.createPay({ payType: -98, payMethod: 2, payerOpenId: LOGINDATA.wxOpenId, orderIds: [purRes.result.data.rows[0].billId], payMoney: purRes.params.jsonParam.orders[0].main.money, appId: LOGINDATA.extProps.wxAppId });
        // console.log(payRes);
    });
    it('saas查询销售订货', async function () {
        await common.loginDo({ epid: ssAccount.saas.epid });
        let exp = format.dataFormat(purRes.params.jsonParam.orders[0].main, 'num=totalNum;restnum=totalNum;totalsum=totalMoney')
        let qlRes = await slh1.slh1SelesOrderManager.querySelesOrderList({ clientid: ssAccount.saas.saasCustomerId }).then(res => res.result.dataList.find(data => data.id));
        // console.log(exp, qlRes);
        common.isApproximatelyEqualAssert(exp, qlRes);
    });
    it('卖家发货', async function () {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        salesBillFull = await sp.spTrade.salesFindBillFull({ id: purRes.result.data.rows[0].salesBillId }).then(res => res.result.data);
        const details = salesBillFull.skus.map((sku) => {
            return { salesDetailId: sku.id, num: sku.skuNum };
        });
        const logisList = await sp.spconfb.findLogisList1({ cap: 1 });
        const randonNum = common.getRandomNum(1, logisList.result.data.rows.length - 1);
        await sp.spTrade.deliverSalesBill({
            main: {
                logisCompId: logisList.result.data.rows[randonNum].id,
                logisCompName: logisList.result.data.rows[randonNum].name,
                waybillNo: common.getRandomNumStr(12),
                buyerId: clientInfo.tenantId,
                // shipFeeMoney: 0,// purRes.params.jsonParam.orders[0].main.shipFeeMoney,
                hashKey: `${Date.now()}${common.getRandomNumStr(3)}`,
            },
            details: details
        });
    });
    it('saas查询销售开单', async function () {
        await common.loginDo({ epid: ssAccount.saas.epid })
        let exp = format.dataFormat(purRes.params.jsonParam.orders[0].main, 'num=totalNum;restnum=totalNum;totalsum=totalMoney')
        salesListRes = await slh1.slh1SelesManager.querySelesList({ dwid: ssAccount.saas.saasCustomerId }).then(res => res.result.dataList.find(data => data.id));
        console.log(exp, salesListRes);
        common.isApproximatelyEqualAssert(exp, salesListRes);
    });
    it('买家查询采购单详情', async function () {
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
        const sellRes = await sp.spTrade.findPurBillFull({ id: purRes.result.data.rows[0].billId }).then(res => res.result.data);
        console.log(`采购单详情= ${JSON.stringify(sellRes)}`);
        sellRes.skus.forEach(obj => { expect(obj, `采购单spuFlag不等于1或不存在`).to.be.include({ spuFlag: 1 }) });
    });

    describe('saas取消发货', function () {
        let selesListRes;
        before('saas作废销售单据', async function () {
            await saas.slh1Login();
            await saas.cancelSaleoutBill({ pk: salesListRes.id });
            await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        });
        it('商城查询订单列表-待发货', async function () {
            this.retries(3);
            await common.delay(3000);
            selesListRes = await sp.spTrade.salesFindBills({ statusType: 2 }).then(res => res.result.data.rows.find(obj => obj.bill.billNo == purRes.result.data.rows[0].billNo)); //statusType: 2 待发货
            expect(selesListRes).to.not.be.undefined;
        });
        it('商城查询订单详情', async function () {
            const salesBillInfo = await sp.spTrade.salesFindBillFull({ id: selesListRes.bill.id }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(salesBillFull, salesBillInfo, ['logisCompName', 'logisCompid', 'ver', 'waybillNo']);
        });
    });
});