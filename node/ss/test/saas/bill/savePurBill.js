const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const basicJsonparam = require('../../../../slh1/help/basiceJsonparam');
const getBasticData = require('../../../../data/getBasicData');
const caps = require('../../../../data/caps');
const format = require('../../../../data/format');
const salesRequestHandler = require('../../../../slh1/help/salesHelp/salesRequestHandler');

describe('订单同步', function () {
    this.timeout(30000);
    let sellerInfo;
    before(async () => {
        await ssReq.ssSellerLogin({ code: ssAccount.saas.mobile, shopName: ssAccount.saas.shopName });
        sellerInfo = _.cloneDeep(LOGINDATA);
        await caps.updateEnvByName({ name: ssAccount.saas.ename });
        caps.epid = ssAccount.saas.epid;
        await common.loginDo({ epid: ssAccount.saas.epid });
        await getBasticData.getBasicID();

    });
    describe('同步销售单', function () {
        let billRes, json;
        before('saas创建销售单', async () => {
            json = basicJsonparam.salesJson();
            json.dwid = ssAccount.saas.saasCustomerId;
            let billId = await common.editBilling(json).then(res => res.result.pk);
            billRes = await salesRequestHandler.salesQueryBilling(billId).then(res => res.result);
            console.log(`saas销售单详情= ${JSON.stringify(billRes)}`);
            console.log(billRes.totalnum);
        });
        it('商城买家查询线下单', async function () {
            // await common.delay(2000);
            this.retries(3);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const purDetailId = await sp.spTrade.purFindBills({ orderByDesc: true, orderBy: 'proTime', statusType: 0, srcType: 4 })
                .then(res => res.result.data.rows[0].bill.id);
            const billFull = await sp.spTrade.findPurBillFull({ id: purDetailId });
            console.log(JSON.stringify(billFull));
            const spuExp = { puPrice: json.details[0].price, totalNum: billRes.totalnum };
            const skuExp = format.dataFormat(billRes.details[0], 'spec1Name=show_sizeid;kuNum=num;skuMoney=total;skuPrice=price;spec2Name=show_colorid;spuCode=stylecode');
            common.isApproximatelyEqualAssert(spuExp, billFull.result.data.spu);
            common.isApproximatelyEqualAssert(skuExp, billFull.result.data.skus[0]);
        });
    });
    describe('同步货品为空的销售单', function () {
        let billRes, json;
        before('saas创建销售单', async () => {
            await common.loginDo({ epid: ssAccount.saas.epid });
            json = basicJsonparam.rechargeJson();
            json.dwid = ssAccount.saas.saasCustomerId;
            let billId = await common.editBilling(json).then(res => res.result.pk);
            billRes = await salesRequestHandler.salesQueryBilling(billId).then(res => res.result);
            console.log(billRes.totalnum);
        });
        it('商城买家查询线下单', async function () {
            // await common.delay(2000);
            this.retries(3);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const purDetailId = await sp.spTrade.purFindBills({ orderByDesc: true, orderBy: 'proTime', statusType: 0, srcType: 4 })
                .then(res => res.result.data.rows[0].bill.id);
            const billFull = await sp.spTrade.findPurBillFull({ id: purDetailId });
            console.log(billFull.result.data.skus);

            expect(billFull.result.data.skus.length, `同步货品为空的销售单失败`).to.be.equal(0);
        });
    });

    describe('同步退货单', function () {
        let billRes, json;
        before('saas创建退货单', async () => {
            await common.loginDo({ epid: ssAccount.saas.epid });
            json = basicJsonparam.returnBackJson();
            json.dwid = ssAccount.saas.saasCustomerId;
            let billId = await common.editBilling(json).then(res => res.result.pk);
            billRes = await salesRequestHandler.salesQueryBilling(billId).then(res => res.result);
            console.log(billRes.totalnum);
        });
        it('商城买家查询线下', async function () {
            // await common.delay(2000);
            this.retries(3);
            await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: sellerInfo.tenantId });
            const purDetailId = await sp.spTrade.purFindBills({ orderByDesc: true, orderBy: 'proTime', statusType: 0, srcType: 4 })
                .then(res => res.result.data.rows[0].bill.id);
            const billFull = await sp.spTrade.findPurBillFull({ id: purDetailId });
            const spuExp = { puPrice: json.details[0].price, totalNum: billRes.totalnum };
            const skuExp = format.dataFormat(billRes.details[0], 'spec1Name=show_sizeid;kuNum=num;skuMoney=total;skuPrice=price;spec2Name=show_colorid;spuCode=stylecode');
            common.isApproximatelyEqualAssert(spuExp, billFull.result.data.spu);
            common.isApproximatelyEqualAssert(skuExp, billFull.result.data.skus[0]);
        });
    });
});
