const common = require('../../../../lib/common');
const ssReq = require('../../../help/ssReq');
const dresManage = require('../../../help/dresManage');
const ss = require('../../../../reqHandler/ss');
const sp = require('../../../../reqHandler/sp');
const ssAccount = require('../../../data/ssAccount');
const saasBaseManger = require('../../../help/saas/saasBase');
const ssConfigParam = require('../../../help/configParamManager');

describe('买家', function () {
    this.timeout(TESTCASE.timeout);
    before('买家登录', async function () {
        await ssReq.ssClientLogin({ mobile: ssAccount.saas.clientMobile, tenantId: ssAccount.saas.tenantId });
    });
    it('获取对账单-按详情', async function () {
        const pdfRes = await ss.spmdm.getSlhPdf({ tenantId: ssAccount.saas.tenantId, type: 0 }).then(res => res.result.data.val);
        console.log(`获取对账单-按详情:`, pdfRes);
        expect(JSON.parse(pdfRes).pdfUrl, `获取pdf对账单失败`).not.to.be.undefined;
    });
    it('获取对账单-按批次', async function () {
        const pdfRes = await ss.spmdm.getSlhPdf({ tenantId: ssAccount.saas.tenantId, type: 1 }).then(res => res.result.data.val);
        console.log(`获取对账单-按批次`, pdfRes);
        expect(JSON.parse(pdfRes).pdfUrl, `获取pdf对账单失败`).not.to.be.undefined;
    });

});