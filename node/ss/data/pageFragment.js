const templatefull = {
    "template4": {
        "template": {
            "id": 4,
            "name": "模板四"
        },
        "colorStyle": {
            "id": 4,
            "colors": [
                "#865C30",
                "#C8C8D1"
            ]
        },
        "homeData": [
            {
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "coverUrl": "docs15514355902925-16237-spdoc10d.do",
                            "readNum": 917,
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15528945428257-spdoc10d.jpg&thumbFlag=0",
                            "name": "今天",
                            "pubPrice": 0.01,
                            "unitId": 65,
                            "weight": 0.3,
                            "id": 21173
                        },
                        "link": {
                            "url": "21173"
                        }
                    }
                ],
                "extProps": {
                    "layout": "0"
                },
                "title": "图片",
                "type": "banner",
                "typeName": "图片"
            },
            {
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15528944858241-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15528944858241-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15547034679177-spdoc10d.png&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15528944858241-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "extProps": {
                    "layout": "4"
                },
                "title": "活动",
                "type": "activityList",
                "typeName": "活动"
            },
            {
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15528944118217-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "extProps": {
                    "layout": "1"
                },
                "title": "活动",
                "type": "activityList",
                "typeName": "活动"
            },
            {
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "http://wxoss.slhdb2b.com/dividingLine.png"
                        },
                        "link": {
                            "url": ""
                        }
                    }
                ],
                "extProps": {
                    "layout": "0"
                },
                "title": "分割区域title",
                "type": "divisionImg",
                "typeName": "分割区域"
            },
            {
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "id": 21173,
                            "unitId": 65
                        },
                        "link": {
                            "url": "21173"
                        }
                    }
                ],
                "extProps": {
                    "layout": "1"
                },
                "title": "货品",
                "type": "goodsList",
                "typeName": "货品"
            }
        ]
    },
    "template1": {
        "template": {
            "id": 1,
            "name": "模板一"
        },
        "colorStyle": {
            "id": 4,
            "colors": [
                "#865C30",
                "#C8C8D1"
            ]
        },
        "homeData": [
            {
                "type": "banner",
                "typeName": "banner",
                "title": "首页Banner",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549624939921-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {
                            "url": ""
                        }
                    }
                ]
            }
        ]
    },
    "template2": {
        "template": {
            "id": 2,
            "name": "模板二"
        },
        "colorStyle": {
            "id": 4,
            "colors": [
                "#865C30",
                "#C8C8D1"
            ]
        },
        "homeData": [
            {
                "type": "banner",
                "typeName": "图片",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549627889925-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "title": "图片"
            },
            {
                "type": "activityList",
                "typeName": "活动",
                "data": [
                    {
                        "bizType": 3,
                        "data": {
                            "id": 153,
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549629019929-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {
                            "url": 153
                        }
                    },
                    {
                        "bizType": 2,
                        "data": {
                            "id": 38269,
                            "unitId": 65,
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549629389949-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {
                            "url": 38269
                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549629699953-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549630459965-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549630569969-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549630669973-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "title": "活动",
                "extProps": {
                    "layout": 6
                }
            },
            {
                "type": "divisionImg",
                "typeName": "分割区域",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "http://wxoss.slhdb2b.com/dividingLine.png"
                        },
                        "link": {
                            "url": ""
                        }
                    }
                ],
                "title": "分割区域title"
            },
            {
                "type": "goodsList",
                "typeName": "货品",
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "id": 38205,
                            "unitId": 65
                        },
                        "link": {
                            "url": 38205
                        }
                    },
                    {
                        "bizType": 2,
                        "data": {
                            "id": 37877,
                            "unitId": 65
                        },
                        "link": {
                            "url": 37877
                        }
                    }
                ],
                "title": "货品",
                "extProps": {
                    "layout": 1
                }
            },
            {
                "type": "activityList",
                "typeName": "活动",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549629979957-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "title": "活动",
                "extProps": {
                    "layout": 0
                }
            },
            {
                "type": "goodsList",
                "typeName": "货品",
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "id": 38229,
                            "unitId": 65
                        },
                        "link": {
                            "url": 38229
                        }
                    },
                    {
                        "bizType": 2,
                        "data": {
                            "id": 37417,
                            "unitId": 65
                        },
                        "link": {
                            "url": 37417
                        }
                    }
                ],
                "title": "货品",
                "extProps": {
                    "layout": 1
                }
            }
        ]
    },
    "template3": {
        "template": {
            "id": 3,
            "name": "模板三"
        },
        "colorStyle": {
            "id": 4,
            "colors": [
                "#865C30",
                "#C8C8D1"
            ]
        },
        "homeData": [
            {
                "type": "banner",
                "typeName": "图片",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549633239981-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "title": "图片"
            },
            {
                "type": "activityList",
                "typeName": "活动",
                "data": [
                    {
                        "bizType": 3,
                        "data": {
                            "id": 153,
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549633779985-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {
                            "url": 153
                        }
                    }
                ],
                "title": "活动",
                "extProps": {
                    "layout": 1
                }
            },
            {
                "type": "activityList",
                "typeName": "活动",
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "id": 38297,
                            "unitId": 65,
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549636659993-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {
                            "url": 38297
                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx15549636869997-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    },
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "https://sst.hzecool.com/doc/v2/content.do?id=docx155496370410001-spdoc10d.jpg&thumbFlag=0"
                        },
                        "link": {

                        }
                    }
                ],
                "title": "活动",
                "extProps": {
                    "layout": 3
                }
            },
            {
                "type": "divisionImg",
                "typeName": "分割区域",
                "data": [
                    {
                        "bizType": 1,
                        "data": {
                            "docId": "http://wxoss.slhdb2b.com/dividingLine.png"
                        },
                        "link": {
                            "url": ""
                        }
                    }
                ],
                "title": "分割区域title"
            },
            {
                "type": "goodsList",
                "typeName": "货品",
                "data": [
                    {
                        "bizType": 2,
                        "data": {
                            "id": 37889,
                            "unitId": 65
                        },
                        "link": {
                            "url": 37889
                        }
                    },
                    {
                        "bizType": 2,
                        "data": {
                            "id": 37145,
                            "unitId": 65
                        },
                        "link": {
                            "url": 37145
                        }
                    }
                ],
                "title": "货品",
                "extProps": {
                    "layout": 1
                }
            }
        ]
    }
};

module.exports = templatefull;