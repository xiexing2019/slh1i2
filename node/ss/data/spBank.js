
let spBank = module.exports = {};

/**
 *  中信个人账户
 */
spBank.personalAcct = [
    { name: '在市谋', settleAcctNo: '6217730700880774' },//371329198804011179
    { name: '慈增修', settleAcctNo: '6217730700491713' },
    { name: '汝棠皓', settleAcctNo: '6217730700880790' },
];

/**
 * 中信企业账户
 */
spBank.businessAcct = [
    { name: '小夏', settleAcctNo: '8110701013101246362' },
    { name: '小宇', settleAcctNo: '8110701013601246360' },
    { name: '小雷', settleAcctNo: '8110701012901246359' },
];

//中信真实银行卡及持卡人姓名数组
spBank.bank = [
    { name: '扶男', settleAcctNo: '6229980700041788' },
    { name: '彭黯沈', settleAcctNo: '6226900706032303' },
    { name: '于呻铆', settleAcctNo: '6226980700523440' },
    { name: '扶男', settleAcctNo: '6226900718817568' },
    { name: '夏阮陇', settleAcctNo: '6226900711068771' },
    { name: '巴扎钥', settleAcctNo: '6226980700261462' },
    { name: '尹焰', settleAcctNo: '6217680700189637' },
    { name: '文安刹', settleAcctNo: '6217730702747658' },
    { name: '洪泌', settleAcctNo: '6226900718373323' },
    { name: '巫廖跟', settleAcctNo: '6226900718502566' }];

