const name = require('../../data/caps').name;

/**
 *
 * confc.ec_staff
 *
 * seller6 绑定saas
 * seller7 独立app
 * appId存在表格：sp-saas
 */
const account = {
    ss_dev: {
        seller6: { mobile: '15727350001', shopName: '艺术设计', appId: 'wx4bcc55bd3ce7e912', tenantId: 529, appKey: '9c6D4YGXNLQoSqArII', appSecret: '2b8d6e8b-a4e1-4270-ac14-93b50a633ef4' },
        // seller6: { mobile: '13712345678', shopName: '小乖乖', appId: 'wx4bcc55bd3ce7e912', tenantId: 137, appKey: '9c6D4YGXNLQoSqArII', appSecret: '2b8d6e8b-a4e1-4270-ac14-93b50a633ef4' },
        sellerPerformance: { mobile: '13712345678', shopName: '小乖乖', appId: 'wx4bcc55bd3ce7e912', tenantId: 137, appKey: '9c6D4YGXNLQoSqArII', appSecret: '2b8d6e8b-a4e1-4270-ac14-93b50a633ef4' },
        client: { mobile: '12175051891', openId: 'ooaMf0SmEWOi-lKjhxu8KyA5BMbc' },
    },
    ss_test: {
        //lq私人账号
        // seller6: { mobile: '12910111390', shopName: '店铺ZYGir', appId: 'wxbf7dc3cc240d8d1e', tenantId: 28814 }, //非独立部署
        // seller6: { mobile: '12910108763', shopName: '店铺fgkEC', appId: 'wxbf7dc3cc240d8d1e', tenantId: 28654 }, //独立部署
        // seller6: { mobile: '18800000000', shopName: '啦啦啦', appId: 'wxbf7dc3cc240d8d1e', tenantId: 925, appKey: '2CW03eA9ZuGaQ8E4f9', appSecret: '06e31151-ac8a-4bf4-b49e-84d3ccbadd74' },
        seller6: { mobile: '18800000000', shopName: '罗胖胖', appId: 'wxbf7dc3cc240d8d1e', tenantId: 28586, appKey: '2CW03eA9ZuGaQ8E4f9', appSecret: '06e31151-ac8a-4bf4-b49e-84d3ccbadd74' },
        seller7: { mobile: '12907099151', shopName: '店铺hwF5V', appId: 'wxbf7dc3cc240d8d1e', tenantId: 8586 },
        //推广专用(独立app账号)
        seller8: { mobile: '12911018515', shopName: '云衣购', appId: 'wx4bcc55bd3ce7e912', tenantId: 35742, appKey: 'CZ5y7pikvPwww6MIeo', appSecret: 'cd17f749-6ebf-416e-aab8-df9ece270e30' },
        seller9: { mobile: '13600001234', shopName: '测试大店', appId: 'wx4bcc55bd3ce7e912', appKey: '0ICZi7xabHktGAaTox', tenantId: 1546, appSecret: 'c453e3a0-2f8f-496c-a3a1-a79ef2700e17' },
        //saasId为2的卖家
        seller10: { mobile: '13738390532', shopName: 'hit&run', appId: 'wx95c02f84bbc6fbae', tenantId: 1594, appKey: 'thoSffVa2rDMn9tagQ', appSecret: '1a87cb5a-fb4f-4624-9c6e-cccda7716771' },
        // client: { mobile: '13221197273', openId: 'oJM315WVczcaeN9j6UAklC_sKE5g' },
        // disSeller6: { mobile: '18880000000', appId: 'wx4bcc55bd3ce7e912' },
        // client: { mobile: '14785260852', openId: 'ooaMfOYb814aJAc7y8X0AXjMlW14' }, //小方
        client: { mobile: '12175051892', openId: '12175051892openId' }, //client: { mobile: '12175051891', openId: 'ooaMf0SmEWOi-lKjhxu8KyA5BMbc' },
        clientAlternate: { mobile: '12910156495', openId: '12910156495openId' },
        // saas配置 saasShopAId，saasShopAName是为了测试开通店铺的时候就绑定saas，用saas门店A绑定
        saas: { mobile: '12907255624', shopName: 'saas同步店铺', shopName2: 'saas同步店铺2', tenantId: 13682, clientMobile: '12908216997', saasCustomerId: '29327781', openId: 'JM315fRMU40w4RwdQkzR6bgzi5M', saasShopId: 106829, saasShopName: '默认店', saasShopIdA: 106869, saasShopNameA: '门店A', epid: '16885', ename: 'adev1', fileid: 173285937, appId: 'wxbf7dc3cc240d8d1e', appKey: '2CW03eA9ZuGaQ8E4f9', appSecret: '06e31151-ac8a-4bf4-b49e-84d3ccbadd74', saasSn: '1400221154483' },
        ecStaff: { code: 'autotest', pass: '000000' },
        sellerPerformance: { mobile: '12907174928', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 10222 }
    },
    // sspre
    ss_test2: {
        seller6: { mobile: '19900000000', shopName: '宝贝宝贝', appId: 'wxbf7dc3cc240d8d1e', tenantId: 1280, appKey: 'r1J00EbDyex4vvPDzv', appSecret: 'ac3eeac4-68e1-4fd4-9e0e-3366cf35c1a2' },
        seller7: { mobile: '19900000000', shopName: '宝贝宝贝', appId: 'wxbf7dc3cc240d8d1e', tenantId: 1280 },
        client: { mobile: '12175051891', openId: 'ooaMf0SmEWOi-lKjhxu8KyA5BMbc' },
        clientAlternate: { mobile: '12910156495', openId: '12910156495openId' },
        ecStaff: { code: 'admin', pass: '000000' },
        sellerPerformance: { mobile: '12908013159', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912' }
    },
    ss_chk: {
        seller4: { mobile: '15121212323', shopName: '可惜啦', appId: 'wx4bcc55bd3ce7e912' },//审核环境测试店铺
        seller6: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929, appKey: 'IIaOK7BuKseHJN24b0', appSecret: '281f57de-a3ff-41e7-8191-d896efc157c1' },
        seller7: { mobile: '17700000000', shopName: '鹿岛', appId: 'wx4bcc55bd3ce7e912', tenantId: 929 },
        client: { mobile: '13175051890', openId: 'oJM315T183V7yCCzQsuGYKTDyDYg' },
        clientAlternate: { mobile: '12910156495', openId: '12910156495openId' },
        // saas配置 saasShopAId，saasShopAName是为了测试开通店铺的时候就绑定saas，用saas门店A绑定
        saas: { mobile: '12908015578', shopName: 'saas同步店铺', shopName2: 'saas同步店铺2', tenantId: 20999, clientMobile: '12908080215', saasCustomerId: '29322133', openId: 'JM315fRMU40w4RwdQkzR6bgzi5M', epid: '17005', ename: 'app1', saasShopId: 107273, saasShopName: '默认店', saasShopIdA: 108649, saasShopNameA: '门店A', fileid: 173306301, appId: 'wxbf7dc3cc240d8d1e', appKey: '2CW03eA9ZuGaQ8E4f9', appSecret: '06e31151-ac8a-4bf4-b49e-84d3ccbadd74', saasSn: '1421044620419' },
        ecStaff: { code: 'admin', pass: '000000' },
        sellerPerformance: { mobile: '12907188577', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 6787 },
    },
    // sspre2
    ss_pre: {
        // seller6: { mobile: '12908284782', shopName: '店铺N0FAz', appId: 'wx4bcc55bd3ce7e912', appKey: 'TfkPO1UaROU826eNf1', appSecret: '708e2704-68d4-403b-b50c-3518d66fb36b', tenantId: 88429 },
        seller6: { mobile: '12910153774', shopName: '店铺2ABMi', appId: 'wx4bcc55bd3ce7e912', appKey: 'TfkPO1UaROU826eNf1', appSecret: '708e2704-68d4-403b-b50c-3518d66fb36b', tenantId: 91645 },
        seller7: { mobile: '12907094084', shopName: '店铺UQSPp', appId: 'wxbf7dc3cc240d8d1e', tenantId: 89421 },
        //推广专用(独立app)
        seller8: { mobile: '12911016361', shopName: '推广店', appId: 'wx4bcc55bd3ce7e912', appKey: 'TfkPO1UaROU826eNf1', appSecret: '708e2704-68d4-403b-b50c-3518d66fb36b', tenantId: 96329 },
        // saas配置 saasShopAId，saasShopAName是为了测试开通店铺的时候就绑定saas，用saas门店A绑定
        saas: { mobile: '12910151192', shopName: 'saas同步店铺', shopName2: 'saas同步店铺2', tenantId: 91653, clientMobile: '12910306478', saasCustomerId: '29398625', openId: 'oWy_j5NJTCDwIws9asSxo5kJbjc4', epid: '18033', ename: 'app3', saasShopId: 108769, saasShopName: '默认店', saasShopIdA: 108773, saasShopNameA: '门店A', fileid: 173306301, saasSn: '1464847609641' },
        client: { mobile: '12908200006', openId: '12908200006openId' },
        clientAlternate: { mobile: '12910156495', openId: '12910156495openId' },
        ecStaff: { code: '000', pass: '000000' }
    },
    ss_online: {
        seller6: { mobile: '12200000000', shopName: '大可可', appId: 'wxc09fcf8155edcecd', tenantId: 382413, appKey: 'T8ZgT5uHB3VfsvB098', appSecret: '96ce636b-6ea2-4f13-bb17-a8767c3df394' },
        seller7: { mobile: '12200000000', shopName: '大可可', appId: 'wxc09fcf8155edcecd', tenantId: 382413 },
        //推广专用
        seller8: { mobile: '12911041479', shopName: '男人装', appId: 'wxc09fcf8155edcecd', tenantId: 102166, appKey: 'T8ZgT5uHB3VfsvB098', appSecret: '96ce636b-6ea2-4f13-bb17-a8767c3df394' },
        client: { mobile: '17691001130', openId: 'oWy_j5BPrpSDHCoTlB438kcfWTqA' },
        clientAlternate: { mobile: '12910156495', openId: '12910156495openId' },
        // saas 线上店铺请勿修改信息
        saas: { mobile: '13486187916', shopName: '小赞', shopName2: '大山家', tenantId: 65102, clientMobile: '17857346303', saasCustomerId: '142369187', epid: '272227', openId: 'oWy_j5NJTCDwIws9asSxo5kJbjc4', ename: 'saas_online', saasShopId: 200259, saasShopName: '默认店', fileid: 212228307, saasSn: '1421044620419' },
        // 线上定时执行
        trunkSeller: { mobile: '12910162642', shopName: '店铺Q6NTk', appId: 'wx4bcc55bd3ce7e912', tenantId: 85874 },
        trunkClient: { mobile: '13750850013', openId: 'oWy_j5Gsv_bKQpKbc8Wh7qEUUjuU', },
        ecStaff: { code: 'fengdg', pass: '000000', sessionId: 'ctr-ssg02-6-96ECECCF-0F49-8AEE-95B0-D93469669BE9' },
        sellerPerformance: { mobile: '12210000000', shopName: '性能测试店铺', appId: 'wx4bcc55bd3ce7e912', tenantId: 14294 },
    }
};



module.exports = account[name];