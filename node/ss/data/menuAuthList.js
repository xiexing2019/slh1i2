const menuAuthList = [
    {
        "code": "shopManagement",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "shopManagement-basicSetting",
                "ecCaption": {},
                "name": "基础设置",
                "id": "100-10"
            },
            {
                "code": "shopManagement-backcardSetting",
                "ecCaption": {},
                "name": "银行卡设置",
                "id": "100-20"
            },
            {
                "code": "shopManagement-logisFeeSetting",
                "ecCaption": {},
                "name": "运费设置",
                "id": "100-30"
            },
            {
                "code": "shopManagement-decorateSetting",
                "ecCaption": {},
                "name": "店铺装修",
                "id": "100-40"
            },
            {
                "code": "shopManagement-personalizationSetting",
                "ecCaption": {},
                "name": "个性化设置",
                "id": "100-50"
            },
            {
                "code": "shopManagement-staffManagement",
                "ecCaption": {},
                "name": "员工管理",
                "id": "100-60"
            },
            {
                "code": "shopManagement-positionManagement",
                "ecCaption": {},
                "name": "职位管理",
                "id": "100-70"
            },
            {
                "code": "shopManagement-classManagement",
                "role": "shopmanager,gm,bigBoss",
                "ecCaption": {},
                "name": "分类管理",
                "id": "100-80"
            }
        ],
        "hasChild": 1,
        "name": "店铺管理",
        "id": "100"
    },
    {
        "code": "goodsManagement",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "goodsManagement-puton",
                "ecCaption": {},
                "name": "上架",
                "id": "200-10"
            },
            {
                "code": "goodsManagement-putoff",
                "ecCaption": {},
                "name": "下架",
                "id": "200-20"
            },
            {
                "code": "goodsManagement-edit",
                "ecCaption": {},
                "name": "编辑",
                "id": "200-30"
            },
            {
                "code": "goodsManagement-adjustPrice",
                "ecCaption": {},
                "name": "调价",
                "id": "200-40"
            },
            {
                "code": "goodsManagement-permission",
                "ecCaption": {},
                "name": "权限",
                "id": "200-50"
            },
            {
                "code": "goodsManagement-share",
                "ecCaption": {},
                "name": "分享",
                "id": "200-60"
            },
            {
                "code": "goodsManagement-delete",
                "ecCaption": {},
                "name": "删除",
                "id": "200-70"
            }
        ],
        "hasChild": 1,
        "name": "货品管理",
        "id": "200"
    },
    {
        "code": "billManagement",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "billManagement-delivery",
                "ecCaption": {},
                "name": "发货",
                "id": "300-10"
            },
            {
                "code": "billManagement-adjustPrice",
                "ecCaption": {},
                "name": "改价",
                "id": "300-20"
            },
            {
                "code": "billManagement-afterSale",
                "ecCaption": {},
                "name": "售后",
                "id": "300-30"
            },
            {
                "code": "billManagement-logis",
                "ecCaption": {},
                "name": "查看物流",
                "id": "300-40"
            }
        ],
        "hasChild": 1,
        "name": "订单管理",
        "id": "300"
    },
    {
        "code": "saleManagement",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "saleManagement-coupon",
                "ecCaption": {},
                "name": "优惠券",
                "id": "400-10"
            },
            {
                "code": "saleManagement-theme",
                "ecCaption": {},
                "name": "专题栏目",
                "id": "400-20"
            },
            {
                "code": "saleManagement-secKill",
                "ecCaption": {},
                "name": "限时折扣",
                "id": "400-30"
            },
            {
                "code": "saleManagement-groupBuy",
                "ecCaption": {},
                "name": "拼团",
                "id": "400-40"
            }
        ],
        "hasChild": 1,
        "name": "营销中心",
        "id": "400"
    },
    {
        "code": "customSetting",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "customSetting-personalizationLabel",
                "ecCaption": {},
                "name": "自定义标签",
                "id": "500-10"
            },
            {
                "code": "customSetting-adjustPermission",
                "ecCaption": {},
                "name": "修改访问权限",
                "id": "500-20"
            },
            {
                "code": "customSetting-adjustPrice",
                "ecCaption": {},
                "name": "修改适用价格",
                "id": "500-30"
            }
        ],
        "hasChild": 1,
        "name": "客户管理",
        "id": "500"
    },
    {
        "code": "wallet",
        "parentId": "0",
        "ecCaption": {},
        "subItems": [
            {
                "code": "wallet-cashOut",
                "ecCaption": {},
                "name": "提现",
                "id": "600-10"
            },
            {
                "code": "wallet-tradeInfo",
                "ecCaption": {},
                "name": "交易记录",
                "id": "600-20"
            },
            {
                "code": "wallet-account",
                "ecCaption": {},
                "name": "账单",
                "id": "600-30"
            }
        ],
        "hasChild": 1,
        "name": "钱包",
        "id": "600"
    }
];

module.exports = menuAuthList;