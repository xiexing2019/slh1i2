const name = require('../../data/caps').name;

/**
 *上下游，总部，分销商的shopId
 */
const account = {
    cs3d2: {
        gm1: { mobile: '12900000001', shopName: '总部默认门店总经理', shopId: 813509 },
        gm2: { mobile: '12900000002', shopName: '总部门店二总经理', shopId: 909765 },
        distributor1: { mobile: '12345678912', shopName: '下游分销商', shopId: 896365 },
        distributor2: { mobile: '15988413448', shopName: '杭州万瑞分销商', shopId: 813521 }
    },
    cs3d1: {
        gm1: { mobile: '18760950926', shopName: '总部默认门店总经理', shopId: 775669 },
        gm2: { mobile: '18700000005', shopName: '总部门店二总经理', shopId: 775729 },
        distributor1: { mobile: '18700000000', shopName: '总部下游-睿智分销商', shopId: 775681 },
        distributor2: { mobile: '18700000003', shopName: '总部下游-弟弟分销商', shopId: 775717 }
    }
};



module.exports = account[name];