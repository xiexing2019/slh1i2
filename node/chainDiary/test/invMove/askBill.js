const common = require('../../../lib/common');
const chainReq = require('../../help/chainReq');
const chainTrade = require('../../../reqHandler/chainDiary/chainTrade');
const getExpByBill = require('../../../shopDiary/help/getExpByBill');
const billExpHelp = require('../../help/billExpHelp');
const basicJson = require('../../help/basicJson');
const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');
const chainData = require('../../data/chainData');

describe('要货单', function () {
    this.timeout(30000);
    let saveRes;
    before(async () => {
        await chainReq.login({
            seq: '003'
        });
        //杭州万瑞分销商登录
    });

    describe('新增要货单', function () {
        let qfRes;
        before(async () => {
            const askBillJson = basicJson.askBillJson({ shopId: chainData.distributor1.shopId });
            console.log(askBillJson);
            //{ "shopId": 813521, "targetShopId": 896365, "totalNum": 1, "totalMoney": 200, "bizDate": "2019-09-23", "rem": "", "billDetails": [{ "unitId": 22157, "tenantSpuId": 263569, "colorId": 3, "sizeId": 1, "commitNum": 1, "price": 200, "realPrice": 200, "money": 200 }] };
            saveRes = await chainTrade.saveAskBill(askBillJson);
            await common.delay(500);
        });
        it('查看要货单详情', async function () {
            qfRes = await chainTrade.getAskBillFull({ id: saveRes.result.data.id });
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, qfRes.result.data);
        });
        it('查询要货单列表', async function () {
            const salesList = await chainTrade.getAskBillList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows.find(obj => obj.billNo == saveRes.result.data.billNo));
            const exp = getExpByBill.getAskBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, salesList);
        });
        describe('要货单审核', function () {
            before(async () => {
                await chainTrade.auditAskBill({ id: saveRes.result.data.id, auditFlag: 1 });
            });
            it('查看要货单详情', async function () {
                const qfRes2 = await chainTrade.getAskBillFull({ id: saveRes.result.data.id });
                expect(qfRes2.result.data.auditFlag, `审核要货单后，详情查询auditFlag不为1`).to.equal(1);
            });
            it('查询要货单列表', async function () {
                const salesList2 = await chainTrade.getAskBillList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows.find(obj => obj.billNo == saveRes.result.data.billNo));
                expect(salesList2.auditFlag, `停用模板后，列表查询auditFlag不为1`).to.equal(1);
            });
        });

        describe('要货单转调拨单', function () {
            let askBillDTL, invMoveJson, invMoveBill;
            before(async () => {
                await chainReq.login({
                    seq: '002'
                });
                //下游分销商登录
                askBillDTL = await chainTrade.getAskBillFull({ id: saveRes.result.data.id });
                invMoveJson = getAskBillToMoveExp(askBillDTL.result.data);
                invMoveJson.billDetails[0].relDetId = askBillDTL.result.data.billDetails[0].id;
                invMoveJson.billDetails[0].realPrice = askBillDTL.result.data.billDetails[0].price;
                invMoveJson.billDetails[0].id = '';
                invMoveBill = await chainTrade.saveMoveOutBill(invMoveJson);
            });
            it('查看要货单详情', async function () {
                const qfRes3 = await chainTrade.getAskBillFull({ id: saveRes.result.data.id });
                expect(qfRes3.result.data.doneFlag, `全部发货后，详情查询doneFlag不为2`).to.equal(2);
            });
            it('查询要货单列表', async function () {
                const salesList3 = await chainTrade.getAskBillList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows.find(obj => obj.billNo == saveRes.result.data.billNo));
                expect(salesList3.doneFlag, `全部发货后，列表查询doneFlag不为2`).to.equal(2);
            });
            it('查看调拨单详情', async function () {
                const moveBill = await chainTrade.getMoveOutBillFull({ id: invMoveBill.result.data.val });
                expect(moveBill.result.data.flag, `全部发货后，调拨详情flag不对`).to.equal(1);
            });
            it('查看要货调拨列表', async function () {
                const moveBillList = await chainTrade.getMoveOutBillList({ inOutType: 1 }).then(res => res.result.data.rows.find(obj => obj.id == invMoveBill.result.data.val));
                expect(moveBillList, `全部发货后，调拨列表查不到这条单据`).to.not.be.undefined;
            });
        });
        describe('修改要货单', function () {
            let sfRes, updateRes, json1, json2, qfupDate;
            before(async () => {
                await chainReq.login({
                    seq: '003'
                });
                //杭州万瑞分销商登录
                json1 = basicJson.askBillJson({ shopId: chainData.distributor1.shopId });
                sfRes = await chainTrade.saveAskBill(json1);
                json2 = basicJson.askBillJson({ shopId: chainData.distributor1.shopId });
                json2.id = sfRes.result.data.id; //修改时必传单据id
                json2.ownerId = sfRes.result.data.deliver;
                await common.delay(5000);
                updateRes = await chainTrade.saveAskBill(json2);
                await common.delay(500);
            });

            it('查看要货单详情', async function () {
                qfupDate = await chainTrade.getAskBillFull({ id: updateRes.result.data.id });
                common.isApproximatelyEqualAssert(updateRes.params.jsonParam, qfupDate.result.data);
            });
            it('查询要货单列表', async function () {
                const salesList = await chainTrade.getAskBillList({ billNo: updateRes.result.data.billNo }).then(res => res.result.data.rows.find(obj => obj.billNo == updateRes.result.data.billNo));
                const exp = getExpByBill.getAskBillListExp(qfupDate.result.data);
                common.isApproximatelyEqualAssert(exp, salesList);
            });
            describe('终结要货单', function () {
                let qfRes4, salesList4;
                before(async function () {
                    await chainTrade.terminateAskBill({ id: updateRes.result.data.id });
                    //终结要货单
                    qfRes4 = await chainTrade.getAskBillFull({ id: updateRes.result.data.id });
                    salesList4 = await chainTrade.getAskBillList({ billNo: updateRes.result.data.billNo }).then(res => res.result.data.rows.find(obj => obj.billNo == updateRes.result.data.billNo));
                });
                it('查询要货单列表', async function () {
                    expect(qfRes4.result.data.doneFlag, `终结要货单后，详情查询doneFlag不为3`).to.equal(3);
                });
                it('查询要货单详情', async function () {
                    expect(salesList4.doneFlag, `终结要货单后，列表查询doneFlag不为3`).to.equal(3);
                });
            });
        });
    });


});

function getAskBillToMoveExp(obj) {
    let exp = {
        srcType: 2,
        relBillId: obj.id,
        unitId: LOGINDATA.unitId,
        shopId: obj.targetShopId,
        otherShopId: obj.shopId,
        bizDate: common.getCurrentDate(),
        deliver: LOGINDATA.userId,
        totalNum: obj.totalNum,
        totalMoney: obj.totalMoney,
        billDetails: obj.billDetails
    };
    return exp;
};