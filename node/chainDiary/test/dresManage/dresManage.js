const styleReqHandler = require('../../../reqHandler/shopDiary/styleReqHandler');
const chainReq = require('../../help/chainReq');
const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');
const basicJson = require('../../../shopDiary/help/json/basicJson');
const common = require('../../../lib/common');
const esSearchHelp = require('../../../sp/help/esSearchHelp');
const slh2 = require('../../../reqHandler/slh2');

describe('商品', function () {
    this.timeout(30000);
    let saveRes, styleInfo;
    before('新增商品', async function () {
        await chainReq.login();
        const saveDresJson = await basicJson.addGoodJson();
        saveRes = await slh2.dres.saveStyle(saveDresJson);
    });

    after('停用', async function () {
        await slh2.dres.disableStyle({ ids: saveRes.result.data.val, shopId: LOGINDATA.shopId });
    });

    it('添加名称相同的商品', async function () {
        const json = await basicJson.addGoodJson();
        json.spuCommonDto.code = saveRes.params.jsonParam.spuCommonDto.code;
        json.check = false;
        const res = await slh2.dres.saveStyle(json);
        expect(res.result).to.includes({ "msgId": "style_code_not_allow_repeat" });
    });

    it('查看商品详情', async function () {
        styleInfo = await slh2.dres.getStyleInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(saveRes.params.jsonParam, styleInfo);
    });

    it('查看商品列表', async function () {
        const styleList = await slh2.dres.getStyleInfoList({ searchToken: styleInfo.spuCommonDto.name }).then(res => res.result.data.rows);
        const info = styleList.find(val => val.id == saveRes.result.data.val);
        common.isApproximatelyEqualAssert(styleInfo, info, ['id']);
    });

    it('排序-invNum', async function () {
        const styleList = await slh2.dres.getStyleInfoList({ orderBy: 'invNum', orderByDesc: false, marketDateStart: '' }).then(res => res.result.data.rows);
        esSearchHelp.orderAssert({ dataList: styleList, path: 'invNum', orderByDesc: false });
    });

    it('排序-上架时间', async function () {
        const styleList = await slh2.dres.getStyleInfoList({ orderBy: 'marketDate', orderByDesc: true, marketDateStart: '' }).then(res => res.result.data.rows);
        esSearchHelp.orderAssert({ dataList: styleList, path: 'marketDate', orderByDesc: true });
    });

    it('停用商品', async function () {
        await slh2.dres.disableStyle({ ids: saveRes.result.data.val, shopId: LOGINDATA.shopId });
        const res = await slh2.dres.getStyleInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
        expect(res.spuCommonDto.flag).to.be.equal(0);
    });

    it('启用商品', async function () {
        await slh2.dres.enableStyle({ ids: saveRes.result.data.val, shopId: LOGINDATA.shopId });
        const res = await slh2.dres.getStyleInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
        expect(res.spuCommonDto.flag).to.be.equal(1);
    });

    describe('修改商品', function () {
        let updateRes;
        before('修改商品', async function () {
            const updateJson = _.cloneDeep(saveRes.params.jsonParam);
            updateJson.spuCommonDto.id = saveRes.result.data.val;
            updateJson.spuCommonDto.stdprice1 = Number(updateJson.spuCommonDto.stdprice1) + 50;
            updateRes = await slh2.dres.saveStyle(updateJson);
        });

        it('查询商品详情', async function () {
            const dresInfo = await slh2.dres.getStyleInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
            common.isApproximatelyEqualAssert(updateRes.params.jsonParam, dresInfo);
        });
    });
});