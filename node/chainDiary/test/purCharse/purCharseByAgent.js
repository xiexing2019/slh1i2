const chainReq = require('../../help/chainReq');
const common = require('../../../lib/common');
const chainTrade = require('../../../reqHandler/chainDiary/chainTrade');
const chainCaps = require('../../../reqHandler/chainDiary/chainCaps');
const caps = require('../../../data/caps');
const getBasicData = require('../../../data/getBasicData');
const chainMdm = require('../../../reqHandler/chainDiary/chainMdm');
const getExpByBill = require('../../../shopDiary/help/getExpByBill');
const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const slh2 = require('../../../reqHandler/slh2');

describe.skip('按分销商入库', function () {
    this.timeout(30000);
    let dwidFxs, saveRes, purInfo;
    before('', async function () {
        await common.loginDo({ epid: chainCaps.epid[caps.name] });
        await getBasicData.getBasicID();

        //客户分销商对应真正的分销商
        const custInfo = await chainMdm.getCustList({ nameLike: '小王' }).then(res => res.result.data.rows);
        dwidFxs = custInfo.find(val => val.name == '小王').id;

        const json = salesJson({ dwid: dwidFxs });
        saveRes = await common.editBilling(json);
        console.log(saveRes);
        await chainMdm.logOut();
    });

    it('分销入库详情', async function () {
        await chainReq.login();
        purInfo = await chainTrade.purchaseInfoByAgent({ salesBillId: saveRes.result.pk }).then(res => res.result.data);
        console.log(`purInfo : ${JSON.stringify(purInfo)}`);
        common.isApproximatelyEqualAssert(saveRes.params, purInfo);
    });

    it.skip('分销入库列表查询', async function () {
        const res = await chainTrade.purchaseListByAgent({ billNo: saveRes.result.billno });
        const info = res.result.data.rows.find(val => val.billNo == saveRes.result.billno);
        const exp = getExpByBill.getPurBillListExp(purInfo);
        common.isApproximatelyEqualAssert(exp, info, ['compId']);
    });

    describe('分销入库保存', function () {
        let purRes, purInfo;
        before('分销入库', async function () {
            purRes = await chainTrade.savePurchaseFromAgent({ id: saveRes.result.pk });
        });

        it('采购单详情', async function () {
            purInfo = await slh2.trade.getPurchaseBillFull({
                isPend: 0,
                id: purRes.result.data.val
            });
            common.isApproximatelyEqualAssert(saveRes.params, purInfo.result.data);
        });
    });


});

function salesJson(params) {
    let json = Object.assign({
        "interfaceid": "sf-14211-1",
        "dwid": '',
        "cash": 400,
        "srcType": 1, //1 正常开单，2 按订货开单，3 按配货开单, 4 商圈单据
        "details": [{
            "num": 2,
            "sizeid": "M",
            "matCode": "Agc001",
            "colorid": "BaiSe",
            "price": 200,
            "discount": 1,
        }],
    }, params);
    return json;
};