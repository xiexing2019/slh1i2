const billReqHandler = require('../../../reqHandler/shopDiary/bill');
const chainReq = require('../../help/chainReq');
const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');
const basicJson = require('../../../shopDiary/help/json/basicJson');
const common = require('../../../lib/common');
const getExpByBill = require('../../../shopDiary/help/getExpByBill');
const slh2 = require('../../../reqHandler/slh2');


describe('采购入库', function () {
    this.timeout(30000);
    let sfRes, qfRes, invAft, changeNum;
    it('新增采购入库单', async function () {
        await chainReq.login();
        const json = basicJson.purJson({ details: [{ num: 5 }, { num: 10 }] });

        invBefore = await mainReqHandler.getInvSkuByStyleId(json.details[0].tenantSpuId);

        sfRes = await billReqHandler.savePurchaseBill({
            jsonParam: json
        });
    });

    it('查询入库单详情', async function () {
        qfRes = await slh2.trade.getPurchaseBillFull({
            isPend: 0,
            id: sfRes.result.data.val
        });
        common.isApproximatelyEqualAssert(sfRes.params.jsonParam, qfRes.result.data);
    });

    it('入库单列表', async function () {
        const purList = await slh2.trade.getPurchaseBillList({ billNo: sfRes.result.data.billNo });
        const info = purList.result.data.rows.find(val => val.billNo == sfRes.result.data.billNo);
        const exp = getExpByBill.getPurBillListExp(qfRes.result.data);
        common.isApproximatelyEqualAssert(exp, info);
    });

    it('查询库存', async function () {
        changeNum = getExpByBill.getInvSkuExpByStyleId(qfRes.result.data);
        const exp = common.addObject(invBefore.result.data, changeNum);
        invAft = await mainReqHandler.getInvSkuByStyleId(qfRes.result.data.details[0].tenantSpuId);
        common.isApproximatelyEqualAssert(exp, invAft.result.data);
    });

    describe('作废采购单', function () {

        before('作废采购单', async function () {
            //作废采购单
            await slh2.trade.deletePurchaseBill({
                isPend: 0,
                id: sfRes.result.data.val
            });
        });

        it('查询采购单详情', async function () {
            const purInfo = await slh2.trade.getPurchaseBillFull({
                isPend: 0,
                id: sfRes.result.data.val
            });
            expect(purInfo.result.data.main.flag, `作废后采购详情的状态字段错误`).to.be.equal(-1);
        });

        it('查询采购单列表', async function () {
            const purList = await slh2.trade.getPurchaseBillList({ billNo: sfRes.result.data.billNo, flag: -1 });
            const info = purList.result.data.rows.find(val => val.billNo == sfRes.result.data.billNo);
            expect(info.flag, `作废后采购单列表的状态错误`).to.be.equal(-1);
        });

        it('查询库存', async function () {
            const res = await mainReqHandler.getInvSkuByStyleId(qfRes.result.data.details[0].tenantSpuId);
            const exp = common.subObject(invAft.result.data, changeNum);
            common.isApproximatelyEqualAssert(exp, res.result.data);
        });
    });

    describe('采购退货', function () {
        let returnBillRes, purInfo;
        before('创建采购退货单', async function () {
            await chainReq.login();
            const json = basicJson.purJson({ details: [{ num: -5 }, { num: -10 }] });
            //获取库存
            invBefore = await mainReqHandler.getInvSkuByStyleId(json.details[0].tenantSpuId);

            returnBillRes = await billReqHandler.savePurchaseBill({
                jsonParam: json
            });
        });

        it('查看采购单详情', async function () {
            purInfo = await slh2.trade.getPurchaseBillFull({
                isPend: 0,
                id: returnBillRes.result.data.val
            });
            common.isApproximatelyEqualAssert(returnBillRes.params.jsonParam, purInfo.result.data);
        });

        it('入库单列表', async function () {
            const purList = await slh2.trade.getPurchaseBillList({ billNo: returnBillRes.result.data.billNo });
            const info = purList.result.data.rows.find(val => val.billNo == returnBillRes.result.data.billNo);
            const exp = getExpByBill.getPurBillListExp(purInfo.result.data);
            common.isApproximatelyEqualAssert(exp, info);
        });

        it('库存查询', async function () {
            const changeNum = getExpByBill.getInvSkuExpByStyleId(purInfo.result.data);
            const exp = common.addObject(invBefore.result.data, changeNum);
            const res = await mainReqHandler.getInvSkuByStyleId(purInfo.result.data.details[0].tenantSpuId);
            common.isApproximatelyEqualAssert(exp, res.result.data);
        });
    });
});
