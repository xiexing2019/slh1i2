const chainReq = require('../../help/chainReq');
const common = require('../../../lib/common');
const chainTrade = require('../../../reqHandler/chainDiary/chainTrade');
const styleReqHandler = require('../../../reqHandler/shopDiary/styleReqHandler');
const chainMdm = require('../../../reqHandler/chainDiary/chainMdm');
const slh2 = require('../../../reqHandler/slh2');

describe('首页数据验证', function () {
    this.timeout(30000);
    let homePageData;
    before('获取首页数据', async function () {
        await chainReq.login();
        homePageData = await chainTrade.getHomePageDate({ shopId: LOGINDATA.shopId });
    });

    it('库存信息', async function () {
        const invNum = homePageData.result.data.invDiary;
        const dresList = await slh2.dres.getStyleInfoList({ marketDateStart: '' }).then(res => res.result.data);
        let stockMoney = 0;
        dresList.rows.forEach(val => {
            stockMoney += common.mul(val.invNum, val.purPrice);
        });

        //库存变化
        const salesList = await chainTrade.getSalesList({ pageSize: 100, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
        const purList = await slh2.trade.getPurchaseBillList({ pageSize: 100, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
        common.isApproximatelyEqualAssert({ stockMoney, stockNum: dresList.sum.invNum, addNum: common.sub(purList.result.data.sum.totalNum, salesList.result.data.sum.totalNum) }, invNum);
    });

    it('会员信息', async function () {
        const orgCustSumDiaryDTO = homePageData.result.data.orgCustSumDiaryDTO;
        const custList = await chainMdm.getCustList({ flag: 1, pageSize: 0 }).then(res => res.result.data.rows);
        const custArr = [];
        custList.forEach(res => {
            if (res.createdDate >= common.getCurrentDate()) {
                custArr.push(res);
            };
        });
        expect(orgCustSumDiaryDTO.addNum).to.be.equal(custArr.length);
    });

    it('销售数', async function () {
        const salesDiary = homePageData.result.data.salesDiary;
        const salesList = await chainTrade.getSalesList({ pageSize: 100, proDateGte: common.getCurrentDate(), shopId: LOGINDATA.shopId, flag: 1, billSubType: "sale", orderBy: "billNo desc" });
        //console.log(`=${JSON.stringify(salesList.params)}`);
        const exp = { salesMoney: salesList.result.data.sum.salesMoney, salesNum: salesList.result.data.sum.salesNum, salesBillNum: salesList.result.data.count };
        console.log(exp);
        console.log(salesDiary);
        common.isApproximatelyEqualAssert(exp, salesDiary);
    });

    it('采购', async function () {
        const purInfo = homePageData.result.data.invDiary;
        const purList = await slh2.trade.getPurchaseBillList({ pageSize: 0, proDateGte: common.getCurrentDate(), proDateLte: common.getCurrentDate() });
        const alreadyPurNum = purList.result.data.sum.totalNum;
        expect(purInfo.alreadyPurNum).to.be.equal(alreadyPurNum);
    });
});