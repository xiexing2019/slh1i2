const common = require('../../../lib/common');
const basicJson = require('../../../shopDiary/help/json/basicJson');
const chainReq = require('../../help/chainReq');
const chainTrade = require('../../../reqHandler/chainDiary/chainTrade');
const getExpByBill = require('../../../shopDiary/help/getExpByBill');
const billExpHelp = require('../../help/billExpHelp');
const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');
const styleReqHandler = require('../../../reqHandler/shopDiary/styleReqHandler');
const slh2 = require('../../../reqHandler/slh2');

describe('销售开单', function () {
    this.timeout(30000);
    let saveRes;
    before('', async function () {
        await chainReq.login();
    });

    describe('新增销售单', function () {
        let qfRes, invBefore;
        before('新增销售单', async function () {
            const salesBillJson = await basicJson.salesJson({ details: [{ num: 5 }, { num: 10 }] });
            invBefore = await chainTrade.getInvOverStore({ tenantSpuId: salesBillJson.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            saveRes = await chainTrade.saveSalesBill(salesBillJson);
            await common.delay(500);
            qfRes = await chainTrade.getSalesInfo({ id: saveRes.result.data.val, isPend: 0 });
            //console.log(`qfRes=${JSON.stringify(qfRes)}`);
        });

        it('查看销售单详情', async function () {
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, qfRes.result.data);
        });

        it('查询销售单列表', async function () {
            const salesList = await chainTrade.getSalesList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows);
            const info = salesList.find(val => val.billNo == saveRes.result.data.billNo);
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, info, ['ecCaption']);
        });

        it('检查库存', async function () {
            //this.retries(5);
            //await common.delay(500);
            let expInvChange = 0;
            const exp = billExpHelp.getInvSkuExpByStyleId(qfRes.result.data);
            const expArray = Object.values(exp);
            console.log(expArray);
            expArray.forEach(element => {
                expInvChange += element.stockNum;
            });
            console.log(expInvChange);
            const invAft = await chainTrade.getInvOverStore({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            //console.log(invBefore);
            //console.log(`invAft=${JSON.stringify(invAft)}`);
            console.log(invBefore);
            console.log(invAft);
            expect(common.add(invBefore, expInvChange), '库存异常').to.equal(invAft);
        });
        it('库存流水', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == saveRes.result.data.val && obj.bizType == 1100));
            //console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
            //let exp = billExpHelp.getInvFlowListExp(qfRes.result.data);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //common.isApproximatelyEqualAssert(exp[saveRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            expect(-qfRes.result.data.main.totalNum, '流水数量不对').to.equal(invFlowList.addNum);
        });

        describe.skip('修改销售单', function () {
            let updateRes;
            before('修改销售单', async function () {
                json = basicJson.salesJson();
                json.main.id = saveRes.result.data.val; //修改时必传单据id
                json.main.ver = saveRes.result.data.ver; //修改时必传
                updateRes = await chainTrade.saveSalesBill(json);
            });

            it('查看销售单详情', async function () {
                const exp = _.cloneDeep(updateRes.params.jsonParam);
                exp.main.salesNum = exp.main.deliverNum = exp.main.totalNum;
                const res = await chainTrade.getSalesInfo({ id: saveRes.result.data.val, isPend: 0 });
                common.isApproximatelyEqualAssert(exp, res.result.data, ['ver', 'collectedFlag']);
            });
        });
    });

    describe('修改销售单', function () {
        let sfRes, qfRes, qfRes2, qlRes, invSku1, invSku2, json;
        before(async () => {
            const salesBillJson = await basicJson.salesJson({ details: [{ num: 10 }, { num: 20 }] });
            invBefore = await chainTrade.getInvOverStore({ tenantSpuId: salesBillJson.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            saveRes = await chainTrade.saveSalesBill(salesBillJson);
            await common.delay(500);
            qfRes = await chainTrade.getSalesInfo({ id: saveRes.result.data.val, isPend: 0 });

            let params = {
                rem: '修改销售单',
            };
            json = basicJson.salesJson(params);
            json.main.id = saveRes.result.data.val; //修改时必传单据id
            json.main.ver = saveRes.result.data.ver; //修改时必传
            //修改销售单
            sfRes = await chainTrade.saveSalesBill({
                jsonParam: json
            });
            //销售单列表.开单后库存,供应商积分和余额
            qfRes2 = await chainTrade.getSalesInfo({
                isPend: 0,
                id: sfRes.result.data.val
            });
            await common.delay(10000);
        });
        it('查看销售单详情', async function () {
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, qfRes.result.data);
        });

        it('查询销售单列表', async function () {
            const salesList = await chainTrade.getSalesList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows);
            const info = salesList.find(val => val.billNo == saveRes.result.data.billNo);
            const exp = getExpByBill.getSalesBillListExp(qfRes2.result.data);
            common.isApproximatelyEqualAssert(exp, info, ['ecCaption']);
        });

        it('检查库存', async function () {
            //this.retries(5);
            //await common.delay(500);
            let expInvChange = 0;
            const exp = billExpHelp.getInvSkuExpByStyleId(qfRes2.result.data);
            const expArray = Object.values(exp);
            console.log(expArray);
            expArray.forEach(element => {
                expInvChange += element.stockNum;
            });
            console.log(expInvChange);
            const invAft = await chainTrade.getInvOverStore({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            console.log(invBefore);
            console.log(invAft);
            expect(common.add(invBefore, expInvChange), '库存异常').to.equal(invAft);
        });
        it('库存流水', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == saveRes.result.data.val && obj.bizType == 1100));
            expect(-qfRes2.result.data.main.totalNum, '流水数量不对').to.equal(invFlowList.addNum);
        });
    });

    describe('作废销售单', function () {
        let qfRes, invBefore;
        before('作废销售单', async function () {
            invBefore = await chainTrade.getInvOverStore({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            await chainTrade.deleteBill({ isPend: 0, id: saveRes.result.data.val });  //ispend 是否挂单
            await common.delay(500);
        });

        it('查询销售单列表', async function () {
            const salesList = await chainTrade.getSalesList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows);
            const info = salesList.find(val => val.billNo == saveRes.result.data.billNo);
            expect(info.flag).to.be.equal(-1);
        });

        it('查询销售单详情', async function () {
            qfRes = await chainTrade.getSalesInfo({ id: saveRes.result.data.val, isPend: 0 });
            expect(qfRes.result.data.main.flag).to.be.equal(-1);
        });

        it('库存检查', async function () {
            let expInvChange = 0;
            const exp = billExpHelp.getInvSkuExpByStyleId(qfRes.result.data);
            const expArray = Object.values(exp);
            expArray.forEach(element => {
                expInvChange += element.stockNum;
            });
            const invAft = await chainTrade.getInvOverStore({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            //console.log(`invAft=${JSON.stringify(invBefore)}`);
            //console.log(exp);
            //console.log(`invAft=${JSON.stringify(invAft)}`);
            expect(common.sub(invBefore, expInvChange), '库存异常').to.equal(invAft);
        });
        it('库存流水验证', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: saveRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == saveRes.result.data.val && obj.bizType == 1100));
            expect(invFlowList, '作废销售单后，库存流水仍然可以查到').to.be.undefined;
        });
    });

    describe('销售退货', function () {
        let saveReturnBillRes, qfRes, invBefore;
        before('新增销售退货单', async function () {
            const json = await basicJson.salesJson({ details: [{ num: -5 }, { num: -10 }] });
            invBefore = await chainTrade.getInvOverStore({ tenantSpuId: json.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            saveReturnBillRes = await chainTrade.saveSalesBill(json);
            await common.delay(500);
        });

        it('查看销售单详情', async function () {
            qfRes = await chainTrade.getSalesInfo({ id: saveReturnBillRes.result.data.val, isPend: 0 });
            //console.log(`qfRes=${JSON.stringify(qfRes)}`);
            common.isApproximatelyEqualAssert(saveReturnBillRes.params.jsonParam, qfRes.result.data);
        });

        it('查询销售单列表', async function () {
            const salesList = await chainTrade.getSalesList({ billNo: saveReturnBillRes.result.data.billNo }).then(res => res.result.data.rows);
            const info = salesList.find(val => val.billNo == saveReturnBillRes.result.data.billNo);
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, info, ['ecCaption']);
        });

        it('库存检查', async function () {
            let expInvChange = 0;
            const exp = billExpHelp.getInvSkuExpByStyleId(qfRes.result.data);
            const expArray = Object.values(exp);
            expArray.forEach(element => {
                expInvChange += element.stockNum;
            });
            const invAft = await chainTrade.getInvOverStore({ tenantSpuId: saveReturnBillRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows[0].stockNum);
            //console.log(invBefore);
            //console.log(exp);
            //console.log(`invAft=${JSON.stringify(invAft)}`);
            expect(common.add(invBefore, expInvChange), '库存异常').to.equal(invAft);
        });
        it('库存流水', async function () {
            let invFlowList = await slh2.inv.getInvFlowList({ tenantSpuId: saveReturnBillRes.params.jsonParam.details[0].tenantSpuId }).then(res => res.result.data.rows.find(obj => obj.billId == saveReturnBillRes.result.data.val && obj.bizType == 1100));
            //console.log(`invFlowList=${JSON.stringify(invFlowList)}`);
            //let exp = billExpHelp.getInvFlowListExp(qfRes.result.data);
            //console.log(`exp=${JSON.stringify(exp)}`);
            //common.isApproximatelyEqualAssert(exp[saveRes.params.jsonParam.details[0].tenantSpuId], invFlowList);
            expect(-qfRes.result.data.main.totalNum, '流水数量不对').to.equal(invFlowList.addNum);
        });
        it.skip('账款检查', async function () {

        })
    });

    describe.skip('挂单', function () {
        let saveRes, qfRes;
        before('新增挂单', async function () {
            const salesBillJson = await basicJson.salesJson({ isPend: 1 });
            invBefore = await mainReqHandler.getInvSkuByStyleId(salesBillJson.details[0].tenantSpuId).then(res => res.result.data);

            saveRes = await chainTrade.saveSalesBill(salesBillJson);
        });

        it('查询挂单详情', async function () {
            qfRes = await chainTrade.getSalesInfo({ id: saveRes.result.data.val, isPend: 1 });
            common.isApproximatelyEqualAssert(saveRes.params.jsonParam, qfRes.result.data);
        });

        it('查询挂单列表', async function () {
            const salesList = await chainTrade.pendList({ billNo: saveRes.result.data.billNo }).then(res => res.result.data.rows);
            const info = salesList.find(val => val.billNo == saveRes.result.data.billNo);
            const exp = getExpByBill.getSalesBillListExp(qfRes.result.data);
            common.isApproximatelyEqualAssert(exp, info, ['ecCaption']);
        });
    });
});