const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');
const chainReq = require('../../help/chainReq');
const basicJson = require('../../../shopDiary/help/json/basicJson');
const chainTrade = require('../../../reqHandler/chainDiary/chainTrade');
const common = require('../../../lib/common');

describe('客户的余额和积分', function () {
    let balanceBefore, balanceAft;
    this.timeout(30000);
    before('查询客户余额和积分', async function () {
        await chainReq.login();
        balanceBefore = await mainReqHandler.getBalanceAndScore({ traderKind: 1, traderCap: 1, traderId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
    });

    it('本次欠款，查看余额变化', async function () {
        const json = basicJson.salesJson({ details: [{ num: 3 }, { num: 6 }] });
        json.payways = [];
        await chainTrade.saveSalesBill(json);
        balanceAft = await mainReqHandler.getBalanceAndScore({ traderKind: 1, traderCap: 1, traderId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        let exp = 0;
        json.details.forEach(res => {
            exp += common.mul(res.num, res.realPrice);
        });
        expect(balanceAft.result.data.balance).to.be.equal(common.sub(balanceBefore.result.data.balance, exp));
    });

    it('本次余款,查看余额变化', async function () {
        const overMoney = 1000;
        const json = basicJson.salesJson({ details: [{ num: 3 }, { num: 6 }] });
        json.payways[0].money += overMoney;
        await chainTrade.saveSalesBill(json);
        const balanceRes = await mainReqHandler.getBalanceAndScore({ traderKind: 1, traderCap: 1, traderId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        expect(balanceRes.result.data.balance).to.be.equal(common.add(balanceAft.result.data.balance, overMoney));
    });

    it('本单不余不欠，查看余额变化', async function () {
        const balanceBef = await mainReqHandler.getBalanceAndScore({ traderKind: 1, traderCap: 1, traderId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        const json = basicJson.salesJson({ details: [{ num: 3 }, { num: 6 }] });
        await chainTrade.saveSalesBill(json);
        const balanceAft = await mainReqHandler.getBalanceAndScore({ traderKind: 1, traderCap: 1, traderId: BASICDATA.ids.dwidXw, shopId: LOGINDATA.shopId });
        expect(balanceBef.result.data.balance).to.be.equal(balanceAft.result.data.balance);
    });
});