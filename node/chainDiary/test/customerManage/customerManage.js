
const common = require('../../../lib/common');
const chainReq = require('../../help/chainReq');
const sspdMainReq = require('../../../reqHandler/shopDiary/mainReqHandler')
const basicJson = require('../../help/basicJson');
const chainMdm = require('../../../reqHandler/chainDiary/chainMdm');
const mainReqHandler = require('../../../reqHandler/shopDiary/mainReqHandler');

describe('客户管理', function () {
    let saveRes;
    this.timeout(30000);
    before('新增客户', async function () {
        await chainReq.login();
        //await mainReqHandler.getChainDiaryData();

        const saveCustJson = basicJson.saveCustJson();
        saveRes = await chainMdm.saveCustomer(saveCustJson);
    });

    it('新增相同名称的客户', async function () {
        const json = _.cloneDeep(saveRes.params.jsonParam);
        json.check = false;
        const res = await chainMdm.saveCustomer(json);
        expect(res.result).to.includes({ "msgId": "ec_mdm_org_name_exist1" });
    });

    it('名字包含特殊字符', async function () {
        const json = basicJson.saveCustJson();
        json.org.name = `!@#'`;
        json.check = false;
        const res = await chainMdm.saveCustomer(json);
        expect(res.result).to.includes({ "msgId": "error_exist_illegal_char" });
    });

    it('查询客户列表', async function () {
        const custList = await chainMdm.getCustList({ nameLike: saveRes.params.jsonParam.org.name }).then(res => res.result.data.rows);
        const info = custList.find(val => val.id == saveRes.result.data.val);
    });

    it('查询客户详情', async function () {
        const custInfo = await chainMdm.getCustInfo({ id: saveRes.result.data.val }).then(res => res.result.data);
        const exp = _.cloneDeep(saveRes.params.jsonParam);
        exp[1].priceType = 9; //默认是9
        common.isApproximatelyEqualAssert(exp[1], custInfo[1], ['authCode']);
    });

    it('删除客户', async function () {
        await chainMdm.delCust({ id: saveRes.result.data.val, must: true }); //物理删除，方便数据还原
        const custList = await chainMdm.getCustList({ nameLike: saveRes.params.jsonParam.org.name }).then(res => res.result.data.rows);
        expect(custList.length).to.be.equal(0);
    });
});