const customerReqHandler = require('../../../reqHandler/shopDiary/customerReqHandler');
const chainReq = require('../../help/chainReq');
const basicJson = require('../../../shopDiary/help/json/basicJson');
const common = require('../../../lib/common');
const chainMdm = require('../../../reqHandler/chainDiary/chainMdm');
const slh2 = require('../../../reqHandler/slh2');

/**
 * 供应商相关用例
 */
describe('供应商管理', function () {
    let sfRes;
    this.timeout(30000);
    before('新增厂商', async function () {
        await chainReq.login();
        const supplierJson = basicJson.addSupplierJson();
        sfRes = await slh2.mdm.addSupplier(supplierJson);
    });

    after('停用供应商', async function () {
        await chainMdm.disableSupplier({ id: sfRes.result.data.val });
    });

    it('新增同名的供应商', async function () {
        const params = _.cloneDeep(sfRes.params.jsonParam);
        params.check = false;
        const res = await slh2.mdm.addSupplier(params);
        expect(res.result).to.includes({ msgId: 'ec_mdm_org_name_exist1' });
    });

    it('名称中包含特殊字符', async function () {
        const supplierJson = basicJson.addSupplierJson({ name: `@!$123` });
        supplierJson.check = false;
        const res = await slh2.mdm.addSupplier(supplierJson);
        expect(res.result).to.includes({ msgId: "error_exist_illegal_char" });

    });

    it('查询厂商列表', async function () {
        const supplierList = await slh2.mdm.getSupplierList({ nameLike: sfRes.params.jsonParam.org.name });
        const info = supplierList.result.data.rows.find(val => val.id == sfRes.result.data.val);
        common.isApproximatelyEqualAssert(sfRes.params.jsonParam, info);
    });

    it('查询供应商详情', async function () {
        const supplierInfo = await slh2.mdm.getSupplierInfo({ id: sfRes.result.data.val }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(sfRes.params.jsonParam, supplierInfo);
    });

    it('修改供应商', async function () {
        const updateJson = _.cloneDeep(sfRes.params.jsonParam);
        updateJson.org.id = sfRes.result.data.val;
        updateJson.org.name = `厂商${common.getRandomStr(4)}`;
        updateJson.org.code = common.getRandomNumStr(6);
        updateJson.org.remark = `备注${common.getRandomStr(5)}`;
        updateJson.address.detailAddr = `滨江星耀城2期`;
        const updateRes = await slh2.mdm.addSupplier(updateJson);

        const supplierInfo = await slh2.mdm.getSupplierInfo({ id: sfRes.result.data.val }).then(res => res.result.data);
        common.isApproximatelyEqualAssert(updateRes.params.jsonParam, supplierInfo);
    });

    it('供应商账款', async function () {
        const res = await slh2.trade.getSuppSimpleAcctCheck({ compId: sfRes.result.data.val, shopId: LOGINDATA.shopId });
        expect(res.result.data, `新增的供应商账款不为0${res.result.data}`).to.includes({ total: 0, initBalance: 0, endMoney: 0, beginMoney: 0 });
    });

    it('停用供应商', async function () {
        await chainMdm.disableSupplier({ id: sfRes.result.data.val });
        const flag = await slh2.mdm.getSupplierInfo({ id: sfRes.result.data.val }).then(res => res.result.data.org.flag);
        expect(flag, `停用了供应商，flag字段没有变为0`).to.be.equal(0);
    });

    it('启用供应商', async function () {
        await chainMdm.enableSupplier({ id: sfRes.result.data.val });
        const flag = await slh2.mdm.getSupplierInfo({ id: sfRes.result.data.val }).then(res => res.result.data.org.flag);
        expect(flag, `启用了供应商，flag字段没有变为1`).to.be.equal(1);
    });
});