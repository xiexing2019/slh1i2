
const common = require('../../lib/common');

let basicJson = module.exports = {};

basicJson.saveCustJson = function () {

    const json = {
        1: {
            typeId: 0,
            priceType: '',
            discount: 1,
            rank: 0,
            vipCardNum: '',
            bodyHeight: '',
            bodyWeight: '',
            bodyBust: '',
            bodyWaistline: '',
            bodyHips: '',
            footSizeId: '',
            professionId: '',
            dressStyleId: '',
            dressColorId: '',
            lining: ''
        },
        org: {
            name: `客户${common.getRandomStr(6)}`,
            organType: 1, //组织类型
            capability: 1
        },
        owner: {
            mobile: common.getRandomMobile(),
            birthday: '',
            gender: 1,
        },
        address: {
            detailAddr: '和平大厦'
        }
    };
    return json;

};

basicJson.askBillJson = function (params = {}) {
    let jsonParam = {
        shopId: LOGINDATA.shopId,
        targetShopId: params.shopId || 775681,//对方门店id*
        //otherInvId: BASICDATA['门店二'].shopId,
        bizDate: common.getCurrentDate(),
        //deliver: LOGINDATA.userId,
        // receiver: "接收人",
        totalNum: 1,    //总数量
        totalMoney: 200,  //总金额
        rem: '要货单备注',
        billDetails: [],
    };

    const styleInfo = params.styleInfo || BASICDATA.styleInfo.agc001;
    if (styleInfo.spuBarcodes && styleInfo.spuBarcodes.length < 1) {
        throw new Error(`款号${styleInfo.spuCommonDto.code}未维护条码,请检查}`)
    };
    const barCodes = common.randomSort(styleInfo.spuBarcodes);
    jsonParam.billDetails = new Array(params.count || 1).fill({}).map((detail, index) => {
        return {
            unitId: LOGINDATA.unitId,
            tenantSpuId: styleInfo.tenantSpuId,
            colorId: barCodes[index].colorId,
            sizeId: barCodes[index].sizeId,
            commitNum: 1,
            price: styleInfo.spuCommonDto.price,
            money: styleInfo.spuCommonDto.price,
            rem: `备注${index}`,
            // rowId: "尺码表头行id  尺码表头情况下必填"
        };
    });
    return jsonParam;
};