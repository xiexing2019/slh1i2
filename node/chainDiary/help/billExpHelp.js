const common = require('../../lib/common');
const format = require('../../data/format');


let billExpHelp = module.exports = {};

billExpHelp.getInvSkuExpByStyleId = (obj) => {
    let exp = {},
        invKey;
    obj.details.forEach(element => {
        invKey = `${element.tenantSpuId}_${element.colorId}_${element.sizeId}`;
        exp[invKey] = {
            invKey: invKey,
            movingNum: 0, //在途数 笑铺没调拨，后续再补
            stockNum: obj.main.srcType == 1 ? -element.num : element.num, //库存数,销售单时库存减少
        };
    });
    return exp;
};

//库存流水期望值
/**
 * @param {Object} obj qf.result.data
 * @param {Object} 
 */
billExpHelp.getInvFlowListExp = (obj) => {
    let flowListExp = {};
    let exp = format.dataFormat(obj.main, 'bizDate=proDate;billId=id;unitId;shopId;billNo');//rem有问题
    exp.bizType = obj.main.subType == 1301 ? 1300 : obj.main.subType;
    obj.details.forEach(ele => {
        let detailInOutType;
        if ((exp.bizType == 1112 && ele.num >= 0) || (exp.bizType == 1300 && ele.num < 0)) {
            detailInOutType = 1;
        } else { detailInOutType = 2; };

        if (!flowListExp[ele.tenantSpuId]) {
            // console.log(`\nexp1=${JSON.stringify(exp)}`);
            flowListExp[ele.tenantSpuId] = Object.assign({}, exp, {
                tenantSpuId: ele.tenantSpuId,
                code: ele.dresCode,
                name: ele.dresName,
                addNum: 0,//重新计算 
                skuFlows: [],
            });
        };
        flowListExp[ele.tenantSpuId].skuFlows.unshift({
            colorId: ele.colorId,
            sizeId: ele.sizeId,
            num: Math.abs(ele.num),
            inOutType: detailInOutType,//1 出库  2 入库
        });
        flowListExp[ele.tenantSpuId].addNum = exp.bizType == 1112 ? common.sub(flowListExp[ele.tenantSpuId].addNum, ele.num) : common.add(flowListExp[ele.tenantSpuId].addNum, ele.num);
    });
    for (let value of Object.values(flowListExp)) {
        if (value.addNum == 0) {
            value.inOutType = value.bizType == 1112 ? 1 : 2;
        } else {
            value.inOutType = value.addNum > 0 ? 2 : 1;
        };
    };
    return flowListExp;
};