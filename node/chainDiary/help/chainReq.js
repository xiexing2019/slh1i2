const common = require('../../lib/common');
const chainCaps = require('../../reqHandler/chainDiary/chainCaps');
const caps = require('../../data/caps');
const chainMdm = require('../../reqHandler/chainDiary/chainMdm');
const mainReqHandler = require('../../reqHandler/shopDiary/mainReqHandler');
let chainReq = module.exports = {};



/** 
* 连锁日记登陆
* @param {string} seq 工号(按自动化对店员角色工号的约定 000:老板 004:店长)
*/
chainReq.login = async function ({ seq = '000' } = {}) {

    if (!chainCaps.mobiles[caps.name]) throw new Error(`${caps.name}没有连锁日记的手机号，请检查chainCaps文件！`);
    const mobile = chainCaps.mobiles[caps.name][seq];
    //更新unitid 
    try {
        const bcdUnit = await common.apiDo({ apiKey: 'ec-config-ugr-unit-findDeviceUnit', jsonParam: { deviceNo: mobile, bdomainCode: 'slh' } }).then(res => res.result.data.rows.find(val => val.name == chainCaps.mobiles[caps.name]['unitName']));
        caps.unitId = bcdUnit.id;
    } catch (error) {
        throw new Error('更新unitid失败了');
    };
    //不重复登陆,如果unitid 和mobile和LOGINDATA一样，就不重复登陆了
    if (LOGINDATA.mobile == mobile && LOGINDATA.unitId == caps.unitId) return;

    const loginParams = Object.assign({
        mobile,
        mustVerify: false,
        verifyCode: '0000',
        unitId: caps.unitId,
        productCode: chainCaps.defParams.productCode,
        productVersion: chainCaps.defParams.productVersion,
    });
    await chainMdm.chainLogin(loginParams);
    await mainReqHandler.getChainDiaryData();
};
