'use,strict';
const basiceJson = require('../../slh1/help/basiceJsonparam');
const basicInfoReqHandler = require('../../slh1/help/basicInfoHelp/basicInfoReqHandler');
const getBasicData = require('../../data/getBasicData');
const common = require('../../lib/common');
const format = require('../../data/format');
const printReqHandler = require('../../slh1/help/printHelp/printRequestHandler');
const reqHandler = require('../../slh1/help/reqHandlerHelp');

(async () => {
	//一店
	await common.loginDo({
		dlProductCode: 'web',
	});

	let styleInfo = await getStyleInfo('104983381'); //获取款号信息,获取不到则新增
	await getBasicData.getBankAccountIds(); //获取银行id
	BASICDATA.specGoodsList = await getBasicData.getSpecGoodsList().then((res) => res.dataList); //特殊货品

	await dataFilling(styleInfo);

	//二店
	await common.loginDo({
		dlProductCode: 'web',
		logid: '100'
	});
	await getBasicData.getBankAccountIds(); //获取银行id
	await dataFilling(styleInfo);

	console.log(`执行完成`);
})();

//新增颜色尺码5*5的货品
async function editStyle() {
	// 获取基础信息[颜色: 601, 尺码: 605, 货品类别]
	let res = await Promise.all([common.callInterface('cs-dict-list', {
		delflag: 0,
		typeid: 601,
	}), common.callInterface('ql-1509', {
		delflag: 0,
	})]);
	res[2] = await common.callInterface('cs-dict-list', {
		delflag: 0,
		typeid: 605,
	});
	// console.log(`res = ${JSON.stringify(res)}`);

	let [colorIds, classIds, sizeIds] = res.map(obj =>
		obj.dataList.map(info => info.sid || info.id));

	//新增货品
	let json = basiceJson.addGoodJson();
	json.colorids = colorIds.slice(0, 5).join(',');
	json.sizeids = sizeIds.slice(0, 5).join(',');
	json.classid = classIds[0];

	let styleRes = await basicInfoReqHandler.editStyle({
		jsonparam: json,
	});
	return styleRes;
};
async function getStyleInfo(pk) {
	let styleInfo = {};
	try {
		styleInfo = await common.fetchMatInfo(pk);
	} catch (e) {
		let styleRes = await editStyle();
		styleInfo = await common.fetchMatInfo(styleRes.pk);
	};
	return styleInfo;
};
async function dataFilling(styleInfo) {
	// let res = await common.callInterface('cs-getpk-dwxx', {
	// 	name: 'Vell',// fy
	// });
	// console.log(`res = ${JSON.stringify(res)}`);
	let companyId = '50798309', //Vell
		clientId = '50797525'; //fy

	let colorSize = [];
	styleInfo.colorids.split(',').forEach((color) => {
		styleInfo.sizeids.split(',').forEach((size) => colorSize.push([color, size]));
	});
	colorSize = common.randomSort(colorSize);

	//新增入库单
	let purJson = {
		interfaceid: 'sf-14212-1',
		dwid: companyId,
		cash: 0,
		details: [],
	};
	colorSize.slice(0, 10).forEach((arr, index) => {
		purJson.details[index] = {
			styleid: styleInfo.pk,
			num: common.getRandomNum(5, 10),
			colorid: arr[0],
			sizeid: arr[1],
			price: 100,
		};
		purJson.cash += purJson.details[index].num * 100;
	});
	await common.editBilling(purJson);

	//新增销售单
	let pks = [];
	for (let i = 0, length = purJson.details.length; i < length; i++) {
		let salesJson = {
			interfaceid: 'sf-14211-1',
			dwid: clientId,
			details: [purJson.details[i]],
			srcType: 1,
		};
		salesJson.details[0].price = 200;
		salesJson.cash = salesJson.details[0].num * 200;
		let sfRes = await common.editBilling(salesJson);
		pks.push(sfRes.result.pk);
		await printReqHandler.getPrint(sfRes);
	};

	//修改销售单
	let qfRes = await reqHandler.queryBilling({
		interfaceid: 'qf-14211-1',
		pk: pks[0],
	});
	qfRes.result.interfaceid = 'sf-14211-1';
	qfRes.result.remark = '修改单据';
	await common.editBilling(qfRes.result);
};
